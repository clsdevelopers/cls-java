package autoDeleter;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import autoTest.AutoTest;
import autoTest.Dashboard;
import autoTest.AutoTest.Browser;

public class AutoDeleter {
	
	public static void deleteAllDocuments(Browser browser, AutoTest.LoginInfo login){
		try{
			deleteAllDocuments(AutoTest.getWebDriverForBrowser(browser), login);
		} catch(Exception e){
			e.printStackTrace();
		}
	}
	
	private static void deleteAllDocuments(WebDriver driver, AutoTest.LoginInfo login) throws Exception{
		AutoTest.loginAndGoToDashboard(driver, login);
		Thread.sleep(1000);
		WebElement searchBox = driver.findElement(By.id("query"));
		searchBox.clear();
		searchBox.sendKeys(login.getUsername());
		searchBox.findElement(By.id("query")).submit();
		deleteAllOnCurrentList(driver);
		driver.findElement(By.cssSelector("li#finalize-tab a")).click();
		deleteAllOnCurrentList(driver);
		driver.close();
	}
	
	private static void deleteAllOnCurrentList(WebDriver driver) throws InterruptedException{
		WebElement element;
		while ((element = tryGetWebElement(driver, By.xpath("//a[contains(text(),'Delete')]"))) != null) {
			element.click();
			System.out.println("Element clicked!");
			new WebDriverWait(driver, 5).until(ExpectedConditions.alertIsPresent());
			driver.switchTo().alert().accept();
			new WebDriverWait(driver, 5).until(ExpectedConditions.alertIsPresent());
			driver.switchTo().alert().accept();
			Dashboard.waitForDashboardToLoad(driver);
			Thread.sleep(500);
		}
	}
	
	private static WebElement tryGetWebElement(WebDriver driver, By by){
		try{
			List<WebElement> elements = driver.findElements(by);
			for(WebElement element : elements)
				if(element.isDisplayed()) return element;
			return null;
		} catch(Exception e){
			return null;
		}
	}
}
