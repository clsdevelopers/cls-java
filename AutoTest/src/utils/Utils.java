package utils;

public class Utils {
	
	/**
	 * Utility function.  Given a string, if the given string ends with a number, 
	 * the returned string will be the same, but with the number incremented by one.  
	 * If the given string doesn't end with a number, it will be returned with "_0" appended to it.  
	 * @param curName
	 * @return
	 */
	public static String getNextName(String curName){
		String endingNumberStr = getEndingNumber(curName);
		int index = tryParse(endingNumberStr);
		if(index == -1) return curName+"_0";
		return curName.substring(0, curName.length() - endingNumberStr.length()) + (index +1);
		
	}

	private static String getEndingNumber(String str){
		if(str.length() == 0) return "";
		int lastIndex = str.length() - 1;
		char end = str.charAt(lastIndex);
		if(Character.isDigit(end)){
			return getEndingNumber(str.substring(0, lastIndex)) + end;
		} else {
			return "";
		}
	}

	private static int tryParse(String str){
		try{
			return Integer.parseUnsignedInt(str);
		} catch (Exception e){
			return -1;
		}
	}

}
