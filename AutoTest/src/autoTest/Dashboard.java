package autoTest;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import autoTest.AutoTest.Browser;
import utils.Utils;

/**
 * This class contains static functions that use the WebDriver when on the Dashboard page.
 * 
 * These functions require selenium to be on the dashboard.
 * @author Steven Miller
 *
 */
public class Dashboard {
	/**
	 * This string is the "value" attribute of the New Document button to use
	 */
	private static final String VALUE_NEW_DOCUMENT_BUTTON = "New Solicitation";

	/**
	 * This is the ID of the page controls in the lower right corner of the dashboard.  These only appear once all the documents
	 * are in the list, so it is a good thing to wait for before clicking
	 */
	private static final String ID_PAGE_CONTROLS = "ulPaginationInProgres";
	
//	/**
//	 * Reads the document list (only the current page)
//	 * @param driver
//	 * @param tableName
//	 * @return a mapping of document names to their WebElements
//	 */
//	static Map<String, WebElement> parseDocumentTable(WebDriver driver, String tableName) {
//		WebElement tableBody = driver.findElement(By.id(tableName)).findElement(By.tagName("tbody"));
//		List<WebElement> rows = tableBody.findElements(By.tagName("tr"));
//		HashMap<String, WebElement> docList = new HashMap<>();
//		for (WebElement e : rows) {
//			docList.put(e.findElement(By.className(CLASS_DOC_TITLE)).getText(), e);
//		}
//	
//		return docList;
//	}

	/**
	 * Creates a new document. Caution: this function does not verify if the document name is unique! &nbsp;
	 * When this function returns, we will be ready to start the interview
	 * @param driver
	 * @param docName
	 * @param docNum
	 * @throws InterruptedException 
	 */
	static void createNewDocument(WebDriver driver, String docName, String docNum) throws InterruptedException {
		driver.findElement(Bye.attribute("input", "value", VALUE_NEW_DOCUMENT_BUTTON)).click();
	
		fillInDocumentInformationAndContinue(driver, docName, docNum);
	
		Interview.waitForInterviewToLoad(driver);
	}
	
	static void fillInDocumentInformationAndContinue(WebDriver driver, String docName, String docNum) throws InterruptedException{
		fillInDocName(driver, docName);
		
		boolean retry = false;
		String location = driver.getCurrentUrl();
		do{
			if(retry) docNum = Utils.getNextName(docNum);
			fillInDocNum(driver, docNum);
			driver.findElement(By.id("btnCreate")).click();
			retry = true;
			Thread.sleep(500);
		}while(driver.getCurrentUrl().equals(location));
		AutoTest.log.println("Creating new document: "+docName+" (Number: "+docNum+")");
	}
	
	private static void fillInDocName(WebDriver driver, String docName){
		driver.findElement(By.id("acquisition_title")).clear();
		driver.findElement(By.id("acquisition_title")).sendKeys(docName);
	}
	
	private static void fillInDocNum(WebDriver driver, String docNum){
		driver.findElement(By.id("document_number")).clear();
		driver.findElement(By.id("document_number")).sendKeys(docNum);
	}

	/**
	 * Pauses until the Dashboard (specifically the document list) is done loading
	 * @param driver
	 * @throws InterruptedException 
	 */
	public static void waitForDashboardToLoad(WebDriver driver) throws InterruptedException{
		// This waits for the next/previous page buttons to appear, meaning that
		// the doc list is done loading
		long time = System.currentTimeMillis();
		WebDriverWait wait = new WebDriverWait(driver, 10);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(ID_PAGE_CONTROLS)));
		if(AutoTest.currentBrowser() == Browser.CHROME){
			while(System.currentTimeMillis() - time < 2000) Thread.sleep(1);
		}
	}

}
