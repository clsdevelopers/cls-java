package autoTest;

import java.io.File;
import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Pattern;

/**
 * This class reads and parses the interview scripts
 * @author Steven Miller
 *
 */
public class InterviewReader implements Iterable<InterviewReader.Answer>{
	private static final char[] escapes = new char[]{
			'!', '\"', '#', '$', '%', '&', '\'', '(', ')', '*', '+', ',', '-', '.', '/', ':', ';', '<', '=', '>', '?', '@',
			'[', '^', '`', '{', '}', '~'//, '\\'
	};
	
	
	private LinkedList<Answer> answers;
	
	public InterviewReader(String filename) throws Exception{
		Scanner scanner = new Scanner(new File(filename));
		answers = new LinkedList<>();
		int lineNumber = 1;
		while(scanner.hasNextLine()){
			String line = scanner.nextLine().trim();
			if(!line.startsWith("#") && !line.isEmpty()){
				answers.add(new Answer(parseLine(line), lineNumber));
			}
			lineNumber++;
		}
		scanner.close();
	}
	
	public int getLineCount(){
		return answers.size();
	}

	@Override
	public Iterator<Answer> iterator() {
		return answers.iterator();
	}
	
	private static List<String> parseLine(String line){
		String[] split = line.trim().split(Pattern.quote("|"));
		for(int i = 0; i< split.length; i++){
			for(char c : escapes){
				split[i] = split[i].replace(c+"", "\\"+c);
			}
		}
		return Arrays.asList(split);
	}
	
	/**
	 * This class represents an Answer to an interview Question.  It also stores
	 * the line in the script that generated it for debugging and error reporting purposes
	 * @author smiller
	 *
	 */
	public class Answer{
		private List<String> answers;
		private int lineNumber;
		
		private Answer(List<String> ans, int line){
			setAnswers(ans);
			setLineNumber(line);
		}
		
		public List<String> getAnswers() {
			return answers;
		}
		private void setAnswers(List<String> answers) {
			this.answers = answers;
		}
		public int getLineNumber() {
			return lineNumber;
		}
		private void setLineNumber(int lineNumber) {
			this.lineNumber = lineNumber;
		}
	}
}
