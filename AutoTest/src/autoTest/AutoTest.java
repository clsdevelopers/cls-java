package autoTest;

import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.net.URISyntaxException;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;
import java.util.zip.ZipException;

import javax.swing.JProgressBar;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

import ui.AutoTestPanel;

/**
 * 'main class' for the AutoTest program. <br>
 * (not actually the main, but it's where the selenium stuff starts)
 * @author Steven Miller
 *
 */
public class AutoTest {
	private static final String LABEL_AGREE_BUTTON = "I Agree";

	public static final long TIME_OUT_TIME = 5;
	
	static PrintStream log;

	private static Browser curBrowser;
	
	static Semaphore mutex;

	static volatile boolean stopTest;
	
	static volatile boolean isPaused = false;
	
	/**
	 * This function begins the test
	 * @param baseUrl The base CLS url (ie https://dataload.clause-logic.com/cls/)
	 * @param username The username to use when logging in
	 * @param password The password to use when logging in
	 * @param filename The interview script file to use
	 * @param docName The document name
	 * @param docNum The document number
	 * @param jpb An optional JProgressBar to monitor the run
	 */
	public static void runTest(LoginInfo login, String filename, Document doc, JProgressBar jpb, Browser browser, EndAction endAction){
		mutex = new Semaphore(1, true);
		stopTest = false;
		boolean closed = false;
		WebDriver driver = null;
		curBrowser = browser;
		try{
		
			log = AutoTestPanel.log;
			if(log == null){
				log = System.out;
			}
			
			
			if(jpb!=null) jpb.setIndeterminate(true);
			
			driver = getWebDriverForBrowser(browser);

			driver.manage().timeouts().implicitlyWait(TIME_OUT_TIME, TimeUnit.SECONDS);

			run(driver, login, doc, filename, endAction, jpb);
			if(endAction == EndAction.SAVE_AND_QUIT){
				driver.close();
				Thread.sleep(3000);
				closed = true;//this ensures that we don't force-quit the driver in the finally block if the run finished cleanly
			}
		} catch (Exception e) {
			log.println("An exception occured:");
			e.printStackTrace(log);
		} finally {
			if(!closed && driver!=null && endAction == EndAction.SAVE_AND_QUIT) driver.quit();
		}
	}
	
	public static void pauseTest(){
		if(isPaused) return;
		isPaused = true;
		try {
			mutex.acquire();
		} catch (InterruptedException e) {
			log.println("Error; could not pause the test");
//			e.printStackTrace(log);
		}
	}
	
	public static void resumeTest(){
		if(!isPaused) return;
		isPaused = false;
		mutex.release();
	}
	
	public static void stopTest(){
		pauseTest();
		stopTest = true;
		resumeTest();
	}

	/**
	 * Performs the run using the given WebDriver and parameters
	 * @param driver
	 * @param baseUrl
	 * @param defDocName
	 * @param docNum
	 * @param filename
	 * @param endAction 
	 * @param username
	 * @param password
	 * @param jpb
	 * @throws Exception
	 */
	private static void run(WebDriver driver, LoginInfo login, Document doc, String filename, EndAction endAction, JProgressBar jpb) throws Exception {
		if(doc.useLaunchUrl){
			driver.get(doc.getLaunchUrl());
			Interview.waitForInterviewToLoad(driver);
		} else {
			loginAndGoToDashboard(driver, login);
			Dashboard.createNewDocument(driver, doc.getName(), doc.getNumber());
		}
		
		try{
			Interview.doInterview(driver, filename, jpb);
		} catch (Exception e){
			log.println("Exception occured in interview:");
			e.printStackTrace(log);
			if(endAction != EndAction.NOTHING) log.println("Attempting to save interview...");
		} finally{
			try{
				switch(endAction){
				case SAVE_AND_QUIT:
					if(doc.useLaunchUrl){//when using an API document, there is no save and quit button
						Interview.save(driver);
					} else {
						Interview.saveAndQuit(driver);
					}
					break;
				case SAVE_ONLY:
					Interview.save(driver);
					break;
				default:
					break;
				}
			} catch (Exception e){
				log.println("Error occured while saving interview!");
				e.printStackTrace(log);
			}
		}

	}

	/**
	 * Logs in and goes to the dashboard, this will be the first WebDriver action, so it also navigates to the proper url
	 * @param driver
	 * @param baseUrl
	 * @param username
	 * @param password
	 * @throws Exception
	 */
	public static void loginAndGoToDashboard(WebDriver driver, LoginInfo login) throws Exception {
		driver.get(login.getBaseURL() + "index.html");
		driver.findElement(By.linkText(LABEL_AGREE_BUTTON)).click();
		driver.findElement(By.id("email")).clear();
		driver.findElement(By.id("email")).sendKeys(login.getUsername());
		driver.findElement(By.id("password")).clear();
		driver.findElement(By.id("password")).sendKeys(login.getPassword());
		driver.findElement(By.name("commit")).click();

		Dashboard.waitForDashboardToLoad(driver);
	}
	
	private static void prepareChromeDriver() throws ZipException, URISyntaxException, IOException{
		boolean isWindows = System.getProperty("os.name").startsWith("Windows");
		String executableName = isWindows ? "chromedriver.exe" : "chromedriver";
		File chromeDriverURI = DriverManager.getDriver(executableName);
		System.setProperty("webdriver.chrome.driver", chromeDriverURI.getPath());
	}
	
	private static void prepareIEDriver(boolean x64) throws ZipException, URISyntaxException, IOException{
		String executableName = x64? "IEDriverServer64.exe" : "IEDriverServer.exe";
		File ieDriverURI = DriverManager.getDriver(executableName);
		System.setProperty("webdriver.ie.driver", ieDriverURI.getPath());
	}
	
	public static Browser currentBrowser(){
		return curBrowser;
	}
	
	public static WebDriver getWebDriverForBrowser(Browser b) throws ZipException, URISyntaxException, IOException{
		switch(b){
		case FIREFOX:
			return new FirefoxDriver();
		case CHROME:
			prepareChromeDriver();
			WebDriver driver = new ChromeDriver();
			driver.manage().window().maximize();//might fix issues on OSX?
			return driver;
		case IE:
			prepareIEDriver(false);
			return new InternetExplorerDriver();
		case IE64:
			prepareIEDriver(true);
			return new InternetExplorerDriver();
		}
		
		//this code is unreachable, but whatever
		return null;
	}
	
	public static class LoginInfo{
		private String username, password, baseURL;
		
		public LoginInfo(String username, String password, String baseUrl){
			setUsername(username);
			setPassword(password);
			setBaseURL(baseUrl);
		}

		public String getUsername() {
			return username;
		}

		public void setUsername(String username) {
			this.username = username;
		}

		public String getPassword() {
			return password;
		}

		public void setPassword(String password) {
			this.password = password;
		}

		public String getBaseURL() {
			return baseURL;
		}

		public void setBaseURL(String baseURL) {
			this.baseURL = baseURL;
		}
	}
	
	public static class Document{
		private String name, number, launchUrl;
		private boolean useLaunchUrl;
		
		public Document(String docName, String docNum){
			setName(docName);
			setNumber(docNum);
			setUseLaunchUrl(false);
			setLaunchUrl(null);
		}
		
		public Document(String launchUrl){
			setName(null);
			setNumber(null);
			this.setLaunchUrl(launchUrl);
			setUseLaunchUrl(true);
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getNumber() {
			return number;
		}

		public void setNumber(String number) {
			this.number = number;
		}

		public boolean useLaunchUrl() {
			return useLaunchUrl;
		}

		public void setUseLaunchUrl(boolean useLaunchUrl) {
			this.useLaunchUrl = useLaunchUrl;
		}

		public String getLaunchUrl() {
			return launchUrl;
		}

		public void setLaunchUrl(String launchUrl) {
			this.launchUrl = launchUrl;
		}
	}
	
	public static enum Browser{
		FIREFOX, CHROME, IE, IE64
	}
	
	public static enum EndAction{
		SAVE_AND_QUIT, SAVE_ONLY, NOTHING
	}

}
