package autoTest;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.zip.ZipException;

public class DriverManager {
	public static File getDriver(String exeName) throws URISyntaxException, ZipException, IOException {
		URL url = DriverManager.class.getClassLoader().getResource("autoTest/"+exeName);
		File out = File.createTempFile("WebDriver", ""+System.currentTimeMillis());
		FileOutputStream output = new FileOutputStream(out);
		InputStream input = url.openStream();
		byte [] buffer = new byte[4096];
		int bytesRead = input.read(buffer);
		while (bytesRead != -1) {
		    output.write(buffer, 0, bytesRead);
		    bytesRead = input.read(buffer);
		}
		output.close();
		input.close();
		
		out.deleteOnExit();
		out.setExecutable(true);
		return out;
	}
}
