package autoTest;

import org.openqa.selenium.By;


/**
 * This class contains some custom functions to be used in place of the static By functions
 * @author smiller
 *
 */
public class Bye {

	public static By attribute(String tag, String name, String value){
		return By.cssSelector(tag + "[" + name + "=\'" + value + "\']");
	}

}
