package autoTest;

import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.swing.JProgressBar;

import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import autoTest.AutoTest.Browser;
import autoTest.InterviewReader.Answer;

/**
 * This class contains functions to control the WebDriver when on the Interview page
 * 
 * all functions in this class require selenium to be on the interview page
 * note for future readers: 'aqd' means Active Question Div, it is the div that contains the currently active question,
 * the input fields for it, and the previous and next buttons
 * @author Steven Miller
 *
 */

public class Interview {
	private static final String ID_BTN_SAVE_CONTINUE = "btnSaveContinue";

	private static final String ID_AUTO_SAVE_MESSAGE = "spanAutoSaveDisplay";

	/**
	 * This string is the ID of the "please wait" dialog that we need to wait for when we create/resume a document
	 *  (while loading the interview page)
	 */
	private static final String ID_PLEASE_WAIT_DIALOG = "pleaseWaitDialog";
	
	/**
	 * XPath used to locate the current question title from the aqd (this might break with structure changes)
	 */
	private static final String QUESTION_XPATH = "../../div[@class=\'panel-heading\']//td";
	
	/**
	 * the text that's on the next button
	 */
	private static final String NEXT_BUTTON_TEXT = "Next";
	
	/**
	 * css selector for the 'next' button, relative to the aqd (also returns the 'previous' button)
	 */
//	private static final String NEXT_BUTTON_SELECTOR = "button.btn-primary[type=\'button\']";
	private static final String NEXT_BUTTON_SELECTOR = "../../div[@class=\'panel-heading\']//button";
	
	/**
	 * the id of the "save and exit" button
	 */
	private static final String ID_QUIT_BUTTON = "btnSaveExit";
	
	/**
	 * this is the ID of the lowest div that all aqds have in common
	 */
	private static final String ID_TOP_DIV = "divContent";
	
	/**
	 * this string is the cssSelector used to find the aqd
	 */
	private static final String QUESTION_DIV_SELECTOR = "div#"+ID_TOP_DIV+" > div:not([style*=\'display:none\']) > div.panel-body div.panel-body > div:not([style*=\'none\'])";

	/**
	 * finds and clicks the save and quit button
	 * @param driver
	 * @throws InterruptedException 
	 */
	public static void saveAndQuit(WebDriver driver) throws InterruptedException{
		waitForAutoSave(driver);
		driver.findElement(By.id(ID_QUIT_BUTTON)).click();
		Dashboard.waitForDashboardToLoad(driver);
	}
	
	/**
	 * Locates the aqd
	 * @param driver
	 * @return
	 */
	public static WebElement findActiveQuestionDiv(WebDriver driver){
		List<WebElement> panelbodies = driver.findElements(By.cssSelector(QUESTION_DIV_SELECTOR));
		for (WebElement e : panelbodies){
			if(e.isDisplayed()){
				return e;
			}
		}
		return null;
	}
	
	/**
	 * Locates the 'Next' button
	 * @param aqd
	 * @return The next button, or null if it's not found
	 */
	public static WebElement findNextButton(WebElement aqd){
		//unfortunately, this will get both the next and the previous buttons
		List<WebElement> list = aqd.findElements(By.xpath(NEXT_BUTTON_SELECTOR));
		//only return the button actually labeled 'next'
		for(WebElement e : list){
			if(e.getText().equals(NEXT_BUTTON_TEXT)) return e;
		}
		AutoTest.log.println("Error! could not find Next button!");
		AutoTest.log.println("Question: "+getQuestion(aqd) + " > " + getSubQuestion(aqd));
		return null;
	}
	
	/**
	 * Returns the current question (top line).  This method is rather slow and should only be used for
	 * debugging or error reporting
	 * @param aqd
	 * @return The question title, or "[Question Text not Found]"
	 */
	public static String getQuestion(WebElement aqd){
		try{
			return aqd.findElement(By.xpath(QUESTION_XPATH)).getText();
		}catch (Exception e){
			return "[Question Text not Found]";
		}
	}
	
	public static void waitForInterviewToLoad(WebDriver driver) throws InterruptedException{
		new WebDriverWait(driver, 10)
			.until(ExpectedConditions.invisibilityOfElementLocated(By.id(ID_PLEASE_WAIT_DIALOG)));
		new WebDriverWait(driver, 10)
			.until(ExpectedConditions.invisibilityOfElementLocated(By.className("fade")));
		if(AutoTest.currentBrowser() == Browser.CHROME) {
			Thread.sleep(5000);
		}
	}
	
	/**
	 * Returns the current sub question (line directly above the answer choices).  This method is rather slow and should only be used for
	 * debugging or error reporting
	 * @param aqd
	 * @return The sub question title, or "[Sub Question Text not Found]"
	 */
	public static String getSubQuestion(WebElement aqd){
		try{
			return aqd.findElement(By.cssSelector("fieldset > legend")).getText();
		}catch (Exception e){
			return "[Sub Question Text not Found]";
		}
	}
	
	/**
	 * Returns the text box if this question is a numeric question.  
	 * Ensure that we're on a numeric question prior to calling this function
	 * @param aqd
	 * @return the text box's WebElement
	 */
	public static WebElement findTextBox(WebElement aqd){
		return aqd.findElement(Bye.attribute("input", "type", "text"));
	}
	
	/**
	 * finds the button corresponding to the given answer, throws an exception if it's not found
	 * 
	 * this function has been replaced by <code>findButtonsForAnswerChoices()</code> because it is faster to find
	 * all the buttons with one css selector than to repeatedly call this function
	 * @param aqd
	 * @param option the exact text of the answer
	 * @return the button
	 */
//	public static WebElement findButtonForAnswerChoice(WebElement aqd, String option){
//		return aqd.findElement(Bye.attribute("input", "value", option));
//	}
	
	/**
	 * locates the buttons for the given <code>Answer</code>
	 * @param aqd
	 * @param answers
	 * @return a list of the buttons found, the order might not match the given order
	 */
	public static List<WebElement> findButtonsForAnswerChoices(WebElement aqd, Answer answers){
		String selector = "";
		for(String opt : answers.getAnswers()){
			selector+="input[value=\'"+opt+"\'], ";
		}
		selector = selector.substring(0, selector.length()-2);
		List<WebElement> result = aqd.findElements(By.cssSelector(selector));
		if(answers.getAnswers().size() != result.size()){
			//ERROR
			AutoTest.log.println("ERROR: " + (answers.getAnswers().size() - result.size()) +" of the following button(s) could not be found:");
			AutoTest.log.println("\t"+String.join(", ", answers.getAnswers()));
			AutoTest.log.println("Question: "+getQuestion(aqd) +" > "+getSubQuestion(aqd));
			AutoTest.log.println("Script line: "+answers.getLineNumber());
			throw new RuntimeException("Could not locate all answers!");
		}
		return result;
	}
	
	/**
	 * note: this method just calls <code>currentQuestionIsTranscript()</code>
	 * and <code>currentQuestionIsNumeric()</code> and returns true if both return false
	 * 
	 * @param driver
	 * @param aqd
	 * @return true if the given aqd represents a multiple choice question
	 */
	public static boolean currentQuestionIsMultipleChoice(WebDriver driver, WebElement aqd){
		//is this a transcript page?
		if(currentQuestionIsTranscript(driver, aqd)) return false;
		
		if(currentQuestionIsNumeric(driver, aqd))return false;
		
		return true;
	}
	
	/**
	 * 
	 * @param driver
	 * @param aqd
	 * @return true if the current question is a transcript page
	 */
	public static boolean currentQuestionIsTranscript(WebDriver driver, WebElement aqd){
		driver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);
		List<WebElement> elements = aqd.findElements(By.cssSelector("fieldset > legend"));
		driver.manage().timeouts().implicitlyWait(AutoTest.TIME_OUT_TIME, TimeUnit.SECONDS);
		if(elements.isEmpty()) return false;
		return elements.get(0).getText().equalsIgnoreCase("TRANSCRIPT");
	}
	
	/**
	 * 
	 * @param driver
	 * @param aqd
	 * @return true if the current question is a numeric question (fill-in-the-blank)
	 */
	public static boolean currentQuestionIsNumeric(WebDriver driver, WebElement aqd){
		driver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);
		List<WebElement> elements = aqd.findElements(Bye.attribute("input", "type", "text"));
		driver.manage().timeouts().implicitlyWait(AutoTest.TIME_OUT_TIME, TimeUnit.SECONDS);
		return !elements.isEmpty();
	}
	
	/**
	 * Performs the interview specified by the given script file.
	 * Does not save or do anything when the script finishes
	 * @param driver the WebDriver
	 * @param filename path to an interview script
	 * @param jpb an optional JProgressBar to track the progress of the interview
	 * @throws Exception
	 */
	public static void doInterview(WebDriver driver, String filename, JProgressBar jpb) throws Exception{
		//step 1. read the interview script
		//see InterviewReader.java
		AutoTest.log.println("Reading interview script...");
		InterviewReader interviewReader = new InterviewReader(filename);
		Iterator<InterviewReader.Answer> it = interviewReader.iterator();
		while(it.hasNext()){
			System.out.println(it.next().getAnswers().toString());
		}
		int questionCount = interviewReader.getLineCount();
		if(jpb!=null) {//update the progress bar, if present
			jpb.setIndeterminate(false);
			jpb.setMinimum(0);
			jpb.setMaximum(questionCount);
			jpb.setValue(0);
		}
		
		//step 2. answer the questions
		AutoTest.log.println("Starting interview");
		for(InterviewReader.Answer answers : interviewReader){
			try{
				AutoTest.mutex.acquire();
				if(AutoTest.stopTest) return;
				try{
					answerQuestion(driver, answers, jpb);
				} finally {
					AutoTest.mutex.release();
				}
			} catch(InterruptedException e){
				return;
			}
		}
		AutoTest.log.println("Interview complete");
		if(jpb!=null) jpb.setIndeterminate(true);
	}

	private static void answerQuestion(WebDriver driver, InterviewReader.Answer answers, JProgressBar jpb) {
		//step 1. find the active question div
		WebElement aqd = findActiveQuestionDiv(driver);
		
		//step 2. If this is a transcript page, click next and repeat step 1
		if(currentQuestionIsTranscript(driver, aqd)){
//			waitForAutoSave(driver);
			findNextButton(aqd).click();
			answerQuestion(driver, answers, jpb);
			return;
		}
		
		//step 3. read the questions, and click on the ones that we're supposed to (or type)
		if(currentQuestionIsNumeric(driver, aqd)){//we need to type in an answer
			WebElement textBox = findTextBox(aqd);
			textBox.clear();
			for(String s : answers.getAnswers()) textBox.sendKeys(s);
		} else {//this is a multiple choice question
			List<WebElement> buttonsToClick = findButtonsForAnswerChoices(aqd, answers);
			for(WebElement e : buttonsToClick) e.click();
		}
		
		//step 4. click next
		findNextButton(aqd).click();
		
		//update the progress bar, if present
		if(jpb!=null) jpb.setValue(jpb.getValue() + 1);
	}
	

	private static void waitForAutoSave(WebDriver driver) {
		long time = System.currentTimeMillis();
		try{
			WebDriverWait wait = new WebDriverWait(driver, 10);
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(ID_AUTO_SAVE_MESSAGE)));
			while(System.currentTimeMillis()-time < 1000) Thread.sleep(1);
		} catch(TimeoutException | InterruptedException e){
			AutoTest.log.println("Warning: AutoSave message never appeared!");
			return;
		}
	}

	public static void save(WebDriver driver) {
		waitForAutoSave(driver);
		driver.findElement(By.id(ID_BTN_SAVE_CONTINUE)).click();
	}
	
	
}
