package ui;

import java.awt.BorderLayout;

import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.BoxLayout;
import javax.swing.border.TitledBorder;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.filechooser.FileNameExtensionFilter;

import autoTest.AutoTest;
import autoTest.AutoTest.Browser;
import autoTest.AutoTest.EndAction;

import javax.swing.border.EtchedBorder;
import javax.swing.AbstractButton;
import javax.swing.Box;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JPasswordField;
import java.awt.Component;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JProgressBar;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.Enumeration;
import java.util.prefs.Preferences;

import javax.swing.JTextArea;
import javax.swing.JScrollPane;
import javax.swing.JRadioButton;
import javax.swing.ButtonGroup;
import javax.swing.JTabbedPane;

@SuppressWarnings("serial")
public class AutoTestPanel extends JPanel implements ActionListener {

	private JTextField usernameField;
	private JPasswordField passwordField;
	private JTextField clsAddressField;
	private JTextField sourceFileField;
	private JTextField docNameField;
	private JTextField docNumField;
	private JTextArea textArea;
	private JProgressBar progressBar;
	private JButton btnRun;
	private JButton btnBrowse;
	private JFileChooser jfc;
	private File sourceFile;
	private JPanel settingsPanel;
	
	public static PrintStream log;
	private final ButtonGroup browserButtonGroup = new ButtonGroup();
	private final ButtonGroup endActionButtonGroup = new ButtonGroup();
	
	private AutoTest.Browser selectedBrowser = Browser.FIREFOX;
	private AutoTest.EndAction endAction = EndAction.SAVE_AND_QUIT;
	
	public AutoTest.Browser getSelectedBrowser() {
		return selectedBrowser;
	}

	private static final FileNameExtensionFilter INTERVIEW_FILTER = new FileNameExtensionFilter("Interview Scripts", "interview");
	private JButton btnStop;
	private JButton btnPause;
	private JTextField launchUrlField;
	private JTabbedPane tabbedPane;

	/**
	 * Create the frame.
	 */
	public AutoTestPanel() {
		jfc = new JFileChooser();
		jfc.setDialogTitle("Choose an interview script...");
		jfc.setMultiSelectionEnabled(false);
		
		this.setBorder(new EmptyBorder(5, 5, 5, 5));
		this.setLayout(new BorderLayout(0, 0));
		
		settingsPanel = new JPanel();
		add(settingsPanel, BorderLayout.NORTH);
		settingsPanel.setLayout(new BoxLayout(settingsPanel, BoxLayout.Y_AXIS));
		
		tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setBorder(new TitledBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null), "CLS Info", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		settingsPanel.add(tabbedPane);
		
		JPanel loginPanel = new JPanel();
		loginPanel.setBorder(new EmptyBorder(0, 5, 5, 5));
		tabbedPane.addTab("UI Login", null, loginPanel, null);
		loginPanel.setLayout(new GridLayout(3, 1, 0, 5));
		
		JPanel userInfoPanel = new JPanel();
		loginPanel.add(userInfoPanel);
		userInfoPanel.setLayout(new GridLayout(0, 2, 5, 0));
		
		Box usernameBox = Box.createHorizontalBox();
		userInfoPanel.add(usernameBox);
		
		JLabel lblUsername = new JLabel("Username");
		usernameBox.add(lblUsername);
		
		Component horizontalStrut_1 = Box.createHorizontalStrut(20);
		usernameBox.add(horizontalStrut_1);
		
		usernameField = new JTextField();
		usernameBox.add(usernameField);
		usernameField.setText(Preferences.userNodeForPackage(AutoTest.class).get("username", ""));
		usernameField.getDocument().addDocumentListener(new DocumentListener() {
			@Override
			public void removeUpdate(DocumentEvent e) {
				Preferences.userNodeForPackage(AutoTest.class).put("username", usernameField.getText());
			}
			@Override
			public void insertUpdate(DocumentEvent e) {
				Preferences.userNodeForPackage(AutoTest.class).put("username", usernameField.getText());
			}
			@Override
			public void changedUpdate(DocumentEvent e) {
				Preferences.userNodeForPackage(AutoTest.class).put("username", usernameField.getText());
			}
		});
		
		Box passwordBox = Box.createHorizontalBox();
		userInfoPanel.add(passwordBox);
		
		JLabel lblPassword = new JLabel("Password");
		passwordBox.add(lblPassword);
		
		Component horizontalStrut_2 = Box.createHorizontalStrut(20);
		passwordBox.add(horizontalStrut_2);
		
		passwordField = new JPasswordField();
		passwordBox.add(passwordField);
		
		Box addressBox = Box.createHorizontalBox();
		loginPanel.add(addressBox);
		
		JLabel lblClsAddress = new JLabel("CLS Address");
		addressBox.add(lblClsAddress);
		
		Component horizontalStrut = Box.createHorizontalStrut(20);
		addressBox.add(horizontalStrut);
		
		clsAddressField = new JTextField();
		clsAddressField.setText("https://dataload.clause-logic.com/cls/");
		addressBox.add(clsAddressField);
		clsAddressField.setColumns(10);
		
		JPanel docInfoPanel = new JPanel();
		loginPanel.add(docInfoPanel);
		docInfoPanel.setLayout(new GridLayout(0, 2, 5, 0));
		
		Box docNameBox = Box.createHorizontalBox();
		docInfoPanel.add(docNameBox);
		
		JLabel docNameLabel = new JLabel("Document Title");
		docNameBox.add(docNameLabel);
		
		Component horizontalStrut_4 = Box.createHorizontalStrut(20);
		docNameBox.add(horizontalStrut_4);
		
		docNameField = new JTextField();
		docNameBox.add(docNameField);
		
		Box docNumBox = Box.createHorizontalBox();
		docInfoPanel.add(docNumBox);
		
		JLabel docNumLabel = new JLabel("Document Number");
		docNumBox.add(docNumLabel);
		
		Component horizontalStrut_5 = Box.createHorizontalStrut(20);
		docNumBox.add(horizontalStrut_5);
		
		docNumField = new JTextField();
		docNumBox.add(docNumField);
		docNumField.setColumns(10);
		
		JPanel launchUrlPanel = new JPanel();
		launchUrlPanel.setBorder(new EmptyBorder(0, 5, 5, 5));
		tabbedPane.addTab("Launch URL", null, launchUrlPanel, null);
		launchUrlPanel.setLayout(new BorderLayout(0, 0));
		
		Box horizontalBox_4 = Box.createHorizontalBox();
		launchUrlPanel.add(horizontalBox_4, BorderLayout.NORTH);
		
		JLabel lblLaunchUrl = new JLabel("Launch URL");
		horizontalBox_4.add(lblLaunchUrl);
		
		Component horizontalStrut_8 = Box.createHorizontalStrut(20);
		horizontalBox_4.add(horizontalStrut_8);
		
		launchUrlField = new JTextField();
		launchUrlField.setColumns(10);
		horizontalBox_4.add(launchUrlField);
		
		JPanel interviewPanel = new JPanel();
		settingsPanel.add(interviewPanel);
		interviewPanel.setBorder(new TitledBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null), "Interview Settings", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		interviewPanel.setLayout(new GridLayout(0, 1, 0, 5));
		
		Box sourceFileBox = Box.createHorizontalBox();
		interviewPanel.add(sourceFileBox);
		
		JLabel lblSourceFile = new JLabel("Interview Script");
		sourceFileBox.add(lblSourceFile);
		
		Component horizontalStrut_3 = Box.createHorizontalStrut(20);
		sourceFileBox.add(horizontalStrut_3);
		
		sourceFileField = new JTextField();
		sourceFileField.setEditable(false);
		lblSourceFile.setLabelFor(sourceFileField);
		sourceFileBox.add(sourceFileField);
		
		btnBrowse = new JButton("Browse...");
		sourceFileBox.add(btnBrowse);
		btnBrowse.addActionListener(this);
		
		JPanel browserSettingsPanel = new JPanel();
		browserSettingsPanel.setBorder(new TitledBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null), "Browser Settings", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		settingsPanel.add(browserSettingsPanel);
		browserSettingsPanel.setLayout(new GridLayout(0, 1, 0, 0));
		
		Box horizontalBox = Box.createHorizontalBox();
		browserSettingsPanel.add(horizontalBox);
		
		JLabel lblBrowser = new JLabel("Browser:");
		horizontalBox.add(lblBrowser);
		
		JRadioButton rdbtnFirefox = new JRadioButton("Firefox");
		browserButtonGroup.add(rdbtnFirefox);
		rdbtnFirefox.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				selectedBrowser = Browser.FIREFOX;
			}
		});
		rdbtnFirefox.setSelected(true);
		horizontalBox.add(rdbtnFirefox);
		
		JRadioButton rdbtnChrome = new JRadioButton("Chrome");
		browserButtonGroup.add(rdbtnChrome);
		rdbtnChrome.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				selectedBrowser = Browser.CHROME;
			}
		});
		horizontalBox.add(rdbtnChrome);
		
		//IE: Windows only!
		if (System.getProperty("os.name").startsWith("Windows")) {
			JRadioButton rdbtnInternetExplorer = new JRadioButton("Internet Explorer");
			browserButtonGroup.add(rdbtnInternetExplorer);
			rdbtnInternetExplorer.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					selectedBrowser = Browser.IE;
				}
			});
			horizontalBox.add(rdbtnInternetExplorer);
			JRadioButton rdbtnInternetExplorer64 = new JRadioButton("Internet Explorer 64-Bit (Strongly Discouraged)");
			rdbtnInternetExplorer64.setToolTipText("Due to some problems with Internet Explorer 64-Bit, it is EXTREMELY slow");
			browserButtonGroup.add(rdbtnInternetExplorer64);
			rdbtnInternetExplorer64.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					selectedBrowser = Browser.IE64;
				}
			});
			horizontalBox.add(rdbtnInternetExplorer64);
		}
		
		Box horizontalBox_1 = Box.createHorizontalBox();
		browserSettingsPanel.add(horizontalBox_1);
		
		JLabel lblActionWenFinished = new JLabel("Action When Finished:");
		horizontalBox_1.add(lblActionWenFinished);
		
		JRadioButton rdbtnSaveAndQuit = new JRadioButton("Save and Quit (close window)");
		rdbtnSaveAndQuit.setToolTipText("When the script is finished, AutoTest will click \"Save and Quit\", then close the browser window");
		endActionButtonGroup.add(rdbtnSaveAndQuit);
		rdbtnSaveAndQuit.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				endAction = EndAction.SAVE_AND_QUIT;
			}
		});
		rdbtnSaveAndQuit.setSelected(true);
		horizontalBox_1.add(rdbtnSaveAndQuit);
		
		JRadioButton rdbtnSaveOnly = new JRadioButton("Save Only");
		rdbtnSaveOnly.setToolTipText("When the script is finished, AutoTest will click \"Save\" and then exit, leaving the browser at the interview page");
		endActionButtonGroup.add(rdbtnSaveOnly);
		rdbtnSaveOnly.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				endAction = EndAction.SAVE_ONLY;
			}
		});
		horizontalBox_1.add(rdbtnSaveOnly);
		
		JRadioButton rdbtnDoNothing = new JRadioButton("Do Nothing");
		rdbtnDoNothing.setToolTipText("When AutoTest reaches the end of the interview script, it will exit, leaving the browser window open");
		endActionButtonGroup.add(rdbtnDoNothing);
		rdbtnDoNothing.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				endAction = EndAction.NOTHING;
			}
		});
		horizontalBox_1.add(rdbtnDoNothing);
		
		JPanel bottomPanel = new JPanel();
		add(bottomPanel, BorderLayout.SOUTH);
		bottomPanel.setLayout(new BorderLayout(0, 0));
		
		JPanel buttonPanel = new JPanel();
		bottomPanel.add(buttonPanel, BorderLayout.CENTER);
		buttonPanel.setLayout(new BoxLayout(buttonPanel, BoxLayout.X_AXIS));
		
		btnStop = new JButton("Stop");
		btnStop.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new Thread(new Runnable() {
					@Override
					public void run() {
						JButton source = (JButton)e.getSource();
						source.setEnabled(false);
						log.println("Stopping test...");
						AutoTest.stopTest();
					}
				}).start();
			}
		});
		btnStop.setEnabled(false);
		buttonPanel.add(btnStop);
		
		Component horizontalGlue = Box.createHorizontalGlue();
		buttonPanel.add(horizontalGlue);
		
		btnRun = new JButton("Run");
		btnRun.setToolTipText("Run the script");
		buttonPanel.add(btnRun);
		
		Component horizontalGlue_1 = Box.createHorizontalGlue();
		buttonPanel.add(horizontalGlue_1);
		
		btnPause = new JButton("Pause");
		btnPause.setEnabled(false);
		btnPause.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new Thread(new Runnable() {
					@Override
					public void run() {
						JButton source = (JButton) e.getSource();
						if(source.getText().equals("Pause")){
							log.print("Pausing...");
							source.setEnabled(false);
							source.setText("Resume");
							AutoTest.pauseTest();
							log.println("Done!");
							source.setEnabled(true);
						} else {
							log.print("Resuming...");
							source.setEnabled(false);
							source.setText("Pause");
							AutoTest.resumeTest();
							log.println("Done!");
							source.setEnabled(true);
						}
					}
				}).start();

			}
		});
		buttonPanel.add(btnPause);
		btnRun.addActionListener(this);
		
		JPanel progressBarPanel = new JPanel();
		bottomPanel.add(progressBarPanel, BorderLayout.SOUTH);
		progressBarPanel.setLayout(new BoxLayout(progressBarPanel, BoxLayout.X_AXIS));
		
		progressBar = new JProgressBar();
		progressBarPanel.add(progressBar);
		
		textArea = new JTextArea();
		textArea.setEditable(false);
		
		JScrollPane scrollPane = new JScrollPane(textArea);
		scrollPane.setBorder(null);
		add(scrollPane, BorderLayout.CENTER);
		scrollPane.setViewportBorder(new TitledBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null), "Output", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		
		
		
		log = new PrintStream(new OutputStream(){
			@Override
			public void write(int b) throws IOException {
				textArea.append(String.valueOf((char)b));
				textArea.setCaretPosition(textArea.getDocument().getLength());
			}
			
		});
		
		log.println("Ready");
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == btnBrowse){
			jfc.setFileFilter(INTERVIEW_FILTER);
			int returnVal = jfc.showOpenDialog(this);
			if(returnVal == JFileChooser.APPROVE_OPTION){
				File src = jfc.getSelectedFile();
				sourceFileField.setText(src.getAbsolutePath());
				sourceFile = src;
			}
		} else if (e.getSource() == btnRun){
			if(sourceFile == null){
				JOptionPane.showMessageDialog(this, "No interview script selected!", "Error", JOptionPane.ERROR_MESSAGE);
				return;
			}
			if(!sourceFile.exists()){
				JOptionPane.showMessageDialog(this, "Interview script could not be found!", "Error", JOptionPane.ERROR_MESSAGE);
				return;
			}
			if(!sourceFile.canRead()){
				JOptionPane.showMessageDialog(this, "Interview script could not be read!", "Error", JOptionPane.ERROR_MESSAGE);
				return;
			}
			
			setUserInteractionEnabled(false);
			log.println("Running...");
			
			//the actual work needs to be done in its own thread; never do work on the awt event thread
			new Thread(new Runnable() {
				public void run() {
					try {
						if(tabbedPane.getSelectedIndex()==0){//use ui login
						AutoTest.Document document = new AutoTest.Document(docNameField.getText(), docNumField.getText());
						AutoTest.runTest(
								getLoginInfo(), 
								sourceFile.getAbsolutePath(), 
								document, 
								progressBar, 
								selectedBrowser,
								endAction);
						} else {//use launch url
							AutoTest.Document document = new AutoTest.Document(launchUrlField.getText());
							AutoTest.runTest(
									null, 
									sourceFile.getAbsolutePath(), 
									document, 
									progressBar, 
									selectedBrowser,
									endAction);
						}
					} finally {
						setUserInteractionEnabled(true);
						progressBar.setValue(0);
						progressBar.setIndeterminate(false);
						btnPause.setText("Pause");
						log.println("Run completed.\n\nReady.");
					}
				}
			}).start();
		}
		
	}
	
	/**
	 * Enables/Disables all the interactive parts of the ui at once
	 * @param enabled
	 */
	private void setUserInteractionEnabled(boolean enabled){
		btnRun.setEnabled(enabled);
		btnBrowse.setEnabled(enabled);
		usernameField.setEnabled(enabled);
		passwordField.setEnabled(enabled);
		clsAddressField.setEnabled(enabled);
		docNameField.setEnabled(enabled);
		docNumField.setEnabled(enabled);
		
		Enumeration<AbstractButton> buttons = browserButtonGroup.getElements();
		while(buttons.hasMoreElements())buttons.nextElement().setEnabled(enabled);
		
		buttons = endActionButtonGroup.getElements();
		while(buttons.hasMoreElements())buttons.nextElement().setEnabled(enabled);
		
		btnPause.setEnabled(!enabled);
		btnStop.setEnabled(!enabled);
	}
	
	public AutoTest.LoginInfo getLoginInfo(){
		return new AutoTest.LoginInfo(usernameField.getText(), String.valueOf(passwordField.getPassword()), clsAddressField.getText());
	}
}
