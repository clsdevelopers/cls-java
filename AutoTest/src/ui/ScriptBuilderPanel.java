package ui;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.PrintWriter;
import java.io.StringWriter;

import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.BoxLayout;
import javax.swing.border.TitledBorder;
import javax.swing.filechooser.FileNameExtensionFilter;

import autoTestBuilder.AutoTestBuilder;

import javax.swing.border.EtchedBorder;
import javax.swing.JTextField;

import java.awt.Component;
import javax.swing.Box;

@SuppressWarnings("serial")
public class ScriptBuilderPanel extends JPanel implements ActionListener {

	private static final FileNameExtensionFilter INTERVIEW_FILE_FILTER = new FileNameExtensionFilter("Interview Scripts", "interview");
	private static final FileNameExtensionFilter HTML_FILE_FILTER = new FileNameExtensionFilter("Html Files", "htm", "html");
	private JTextField sourceFileField;
	private JTextField destFileField;
	private JButton btnBrowseSource;
	private JButton btnBrowseDest;
	private JFileChooser jfc;
	private JButton btnGenerate;
	private File sourceFile, destFile;
	private JProgressBar progressBar;

	/**
	 * Create the frame.
	 */
	public ScriptBuilderPanel() {
		jfc = new JFileChooser();
		jfc.setDialogTitle("Choose a file...");
		jfc.setMultiSelectionEnabled(false);
		
		this.setBorder(new EmptyBorder(5, 5, 5, 5));
		this.setLayout(new BorderLayout(0, 0));
		
		JPanel bottomPanel = new JPanel();
		this.add(bottomPanel, BorderLayout.SOUTH);
		bottomPanel.setLayout(new BorderLayout(0, 0));
		
		JPanel buttonPanel = new JPanel();
		bottomPanel.add(buttonPanel, BorderLayout.CENTER);
		
		btnGenerate = new JButton("Generate");
		buttonPanel.add(btnGenerate);
		btnGenerate.addActionListener(this);
		
		JPanel progressBarPanel = new JPanel();
		bottomPanel.add(progressBarPanel, BorderLayout.SOUTH);
		progressBarPanel.setLayout(new BoxLayout(progressBarPanel, BoxLayout.X_AXIS));
		
		progressBar = new JProgressBar();
		progressBarPanel.add(progressBar);
		
		Box mainContentBox = Box.createVerticalBox();
		this.add(mainContentBox, BorderLayout.NORTH);
		
		JPanel sourceLinePanel = new JPanel();
		mainContentBox.add(sourceLinePanel);
		sourceLinePanel.setBorder(new TitledBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null), "Source File", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		sourceLinePanel.setLayout(new BoxLayout(sourceLinePanel, BoxLayout.X_AXIS));
		
		sourceFileField = new JTextField();
		sourceLinePanel.add(sourceFileField);
		sourceFileField.setEditable(false);
		sourceFileField.setColumns(100);
		
		btnBrowseSource = new JButton("Browse...");
		sourceLinePanel.add(btnBrowseSource);
		btnBrowseSource.addActionListener(this);
		
		JPanel destLinePanel = new JPanel();
		mainContentBox.add(destLinePanel);
		destLinePanel.setBorder(new TitledBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null), "Destination File", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		destLinePanel.setLayout(new BoxLayout(destLinePanel, BoxLayout.X_AXIS));
		
		destFileField = new JTextField();
		destLinePanel.add(destFileField);
		destFileField.setEditable(false);
		destFileField.setColumns(100);
		
		btnBrowseDest = new JButton("Browse...");
		destLinePanel.add(btnBrowseDest);
		btnBrowseDest.addActionListener(this);
		
		Component verticalGlue = Box.createVerticalGlue();
		mainContentBox.add(verticalGlue);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == btnBrowseSource){
			jfc.setFileFilter(HTML_FILE_FILTER);
			jfc.removeChoosableFileFilter(INTERVIEW_FILE_FILTER);
			if(sourceFile!=null){
				jfc.setSelectedFile(sourceFile);
			}
			int returnVal = jfc.showOpenDialog(this);
			if(returnVal == JFileChooser.APPROVE_OPTION){
				File src = jfc.getSelectedFile();
				sourceFileField.setText(src.getAbsolutePath());
				sourceFile = src;
//				if(destFile == null && src.getAbsolutePath().contains(".")){
				if(src.getAbsolutePath().contains(".")){
					String path = sourceFile.getAbsolutePath();
					destFile = new File(path.substring(0, path.lastIndexOf('.')) + ".interview");
					destFileField.setText(destFile.getAbsolutePath());
				}
			}
		} else if(e.getSource() == btnBrowseDest){
			jfc.setFileFilter(INTERVIEW_FILE_FILTER);
			jfc.removeChoosableFileFilter(HTML_FILE_FILTER);
			if(sourceFile!=null && destFile == null && sourceFile.getAbsolutePath().contains(".")){
				String path = sourceFile.getAbsolutePath();
				jfc.setSelectedFile(new File(path.substring(0, path.lastIndexOf('.')) + ".interview"));
			}
			if(destFile != null){
				jfc.setSelectedFile(destFile);
			}
			int returnVal = jfc.showSaveDialog(this);
			if(returnVal == JFileChooser.APPROVE_OPTION){
				File src = jfc.getSelectedFile();
				if(!src.getName().endsWith(".interview")){
					src = new File(src.getAbsolutePath()+".interview");
				}
				destFileField.setText(src.getAbsolutePath());
				destFile = src;
			}
		} else if(e.getSource() == btnGenerate){
			if(destFile == null || sourceFile == null){
				JOptionPane.showMessageDialog(null, "You need to choose both an input and output file", "Error", JOptionPane.ERROR_MESSAGE);
				return;
			}
			
			btnBrowseSource.setEnabled(false);
			btnBrowseDest.setEnabled(false);
			btnGenerate.setEnabled(false);
			
			Component parent = this;
			//Do the generate stuff!
			new Thread(new Runnable() {
				public void run() {
					try{
						if(sourceFile.exists()){
							int retval = JOptionPane.showConfirmDialog(null, "Are you sure you want to overwrite "+destFile.getName()+"?", "File already exists!", JOptionPane.YES_NO_OPTION);
							if(retval != JOptionPane.YES_OPTION) return;
						}
						AutoTestBuilder.generate(sourceFile, destFile, progressBar);
						JOptionPane.showMessageDialog(parent, "File successfully created!", "Done", JOptionPane.PLAIN_MESSAGE);
					} catch (Exception e){
						StringWriter strw = new StringWriter();
						PrintWriter writer = new PrintWriter(strw);
						writer.append("An exception occured:\n");
						e.printStackTrace(writer);
						writer.flush();
						
						JTextArea area = new JTextArea();
						area.setText(strw.toString());
						area.setEditable(false);
						JScrollPane scrollpane = new JScrollPane(area);
						
						JOptionPane.showMessageDialog(parent, scrollpane, "Error", JOptionPane.ERROR_MESSAGE);
					} finally {
						progressBar.setValue(0);
						progressBar.setIndeterminate(false);
						btnBrowseSource.setEnabled(true);
						btnBrowseDest.setEnabled(true);
						btnGenerate.setEnabled(true);
					}
				}
			}).start();
			
			
			
		}
		
	}

}
