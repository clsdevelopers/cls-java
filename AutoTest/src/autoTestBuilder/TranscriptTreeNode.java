package autoTestBuilder;

import java.util.LinkedList;
import java.util.List;

public class TranscriptTreeNode {
		private String label;
		private List<TranscriptTreeNode> children;
		
		public String getLabel() {
			return label;
		}
		public void setLabel(String label) {
			this.label = label;
		}
		public List<TranscriptTreeNode> getChildren() {
			return children;
		}
		public void setChildren(List<TranscriptTreeNode> children) {
			this.children = children;
		}
		public void addChild(TranscriptTreeNode newChild){
			this.children.add(newChild);
		}
		
		public TranscriptTreeNode(){
			this("[UNKNOWN]");
		}
		
		public TranscriptTreeNode(String label){
			this.label = label;
			this.children = new LinkedList<>();
		}
		
		public boolean isLeafNode(){
			return children.isEmpty();
		}
}
