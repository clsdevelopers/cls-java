package autoTestBuilder;

import java.io.File;
import java.io.PrintStream;
import java.text.DateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;

import javax.swing.JProgressBar;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;

/**
 * This class contains all the functions needed to parse HTM/HTML files into interview files
 * @author Steven Miller
 *
 */
public class AutoTestBuilder {
	/**
	 * css selector to find the transcriptBody div
	 */
	private static final String SELECTOR_TRANSCRIPT_BODY_DIV = "div#transcriptBody";

	/**
	 * The name to give to the root node, will appear before the first question as a comment
	 */
	private static final String ROOT_NODE_NAME = "Transcript";
	
	/**
	 * the PrintStream associated with the output file
	 */
	private static PrintStream stream;
	
	/**
	 * these tags are ignored
	 */
	private static final List<String> IGNORED_TAGS = Arrays.asList(new String[] {"div", "button"});
	
	/**
	 * these tags can contain text
	 * note: when adding to this list, place the more-likely ones first; it will speed up the parse
	 */
	private static final List<String> TEXT_OVERRIDE = Arrays.asList(new String[] {"span", "p", "a", "h1", "h2", "h3", "h4"});//do I need to add i, u, or b?

	/**
	 * Main function to take the sourceFile, parse it, and store the result in the destFile
	 * @param sourceFile
	 * @param destFile
	 * @param jpb
	 * @throws Exception
	 */
	public static void generate(File sourceFile, File destFile, JProgressBar jpb) throws Exception{
		if (jpb != null) jpb.setIndeterminate(true);
		stream = new PrintStream(destFile);
		
		System.out.println("Starting WebDriver...");
		WebDriver driver = new HtmlUnitDriver(false);
		driver.manage().timeouts().implicitlyWait(0, TimeUnit.NANOSECONDS);
		System.out.println("Loading file...");
		String url = sourceFile.toURI().toURL().toExternalForm();
		url = url.replaceAll(Pattern.quote("%20"), " ");
		System.out.println(url);
		driver.get(url);
		
		System.out.println(driver.getCurrentUrl());
		
		//write the header on the generated file
		stream.println("#Generated file for AutoTest");
		stream.println("#Generated: " + DateFormat.getDateTimeInstance().format(new Date()));
		stream.println("#from " + sourceFile.getAbsolutePath());
		
		//first we check if this is a copy/paste from CLS, if it is, there will be a transcriptBody div
		//data taken directly from CLS needs to be parsed differently than data that went through Word first
		WebElement transcriptBody = getTranscriptBody(driver);
		TranscriptTreeNode rootNode;
		if(transcriptBody == null){
			System.out.println("Standard file");
			rootNode = parseStandardFile(driver, jpb);
		} else{
			System.out.println("CLS transcript file");
			//in a cls file, every li gets processed (or at least the vast majority), so it makes a simple way to gauge the progress of the parse
			if(jpb!=null) jpb.setMaximum(transcriptBody.findElements(By.tagName("li")).size());
			if(jpb!=null) jpb.setIndeterminate(false);
			if(jpb!=null) jpb.setValue(0);
			rootNode = parseClsFile(transcriptBody, jpb);
		}
		
		printTree(rootNode, 0);
		
		//if nothing got added to the root node, we know that something went wrong
		if(rootNode.isLeafNode()){
			throw new RuntimeException("The document could not be parsed!");
		}
		
		writeTree(rootNode);
		
		
		driver.quit();
		stream.flush();
		stream.close();
	}
	
	/**
	 * Parses a cls-formatted file into a tree
	 * @param transcriptBody the WebElement of the transcriptBody
	 * @param jpb
	 * @return
	 */
	private static TranscriptTreeNode parseClsFile(WebElement transcriptBody, JProgressBar jpb) {
		List<WebElement> lists = transcriptBody.findElements(By.xpath("./li"));
		TranscriptTreeNode rootNode = new TranscriptTreeNode(ROOT_NODE_NAME);
		
		for(WebElement child : lists){
			rootNode.addChild(buildClsTree(child, jpb));
		}
		
		return rootNode;
	}
	
	/**
	 * recursive function that builds each node of the tree
	 * @param root
	 * @param jpb
	 * @return
	 */
	private static TranscriptTreeNode buildClsTree(WebElement root, JProgressBar jpb){
		if(jpb!=null) jpb.setValue(jpb.getValue() + 1);
		List<WebElement> children = root.findElements(By.xpath("./ul/li"));
		TranscriptTreeNode node = new TranscriptTreeNode(getClsText(root));
		if(children.isEmpty()){
			return node;
		} else {
			for(WebElement child : children){
				node.addChild(buildClsTree(child, jpb));
			}
		}
		
		return node;
	}
	
	/**
	 * Gets the text of an element (only for use when parsing CLS files).  returns <code>root.getText()</code> if it can't find the text
	 * @param root
	 * @return
	 */
	private static String getClsText(WebElement root){
		for(String s : TEXT_OVERRIDE){
			List<WebElement> items = root.findElements(By.xpath("./"+s));
			if(!items.isEmpty()) return items.get(0).getText();
		}
		return root.getText();
	}

	/**
	 * Performs the 'first layer' parse of a standard file.  
	 * @param root
	 * @param rootLabel
	 * @return
	 */
	private static TranscriptTreeNode buildTree(WebElement root, String rootLabel){
		List<WebElement> children = root.findElements(By.xpath("./*"));
		TranscriptTreeNode rootNode = new TranscriptTreeNode(rootLabel);
		
		WebElement lastElement = null;
		for(WebElement child : children){
			if(child.getTagName().equals("ul")){
				if(lastElement == null){
					System.out.println("NULL lastElement on "+getText(child));
				}
				rootNode.addChild(buildSubTree(child, getText(lastElement)));
				
			} else {
				lastElement = child;
			}
		}
		
		return rootNode;
	}
	/**
	 * builds non-root nodes from standard files.  Called by <code>buildTree()</code>
	 * @param root
	 * @param rootLabel
	 * @return A TranscriptTreeNode
	 */
	//this has to be a different function because the root trees don't start with answers
	private static TranscriptTreeNode buildSubTree(WebElement root, String rootLabel){
		List<WebElement> children = root.findElements(By.xpath("./*"));
		TranscriptTreeNode rootNode = new TranscriptTreeNode(rootLabel);
		
		WebElement lastElement = null;
		for(WebElement child : children){
			if(child.getTagName().equals("li")){
				if(lastElement!=null){
					rootNode.addChild(new TranscriptTreeNode(getText(lastElement)));
				}
				lastElement = child;
			} else {
				rootNode.addChild(buildSubTree(child, getText(lastElement)));
				lastElement = null;
			}
		}
		
		if(lastElement != null) rootNode.addChild(new TranscriptTreeNode(getText(lastElement)));
		
		return rootNode;
	}
	
	/**
	 * Parses a standard file
	 * @param driver
	 * @param jpb
	 * @return the root node
	 */
	private static TranscriptTreeNode parseStandardFile(WebDriver driver, JProgressBar jpb){
		//first we find all possible lists
		List<WebElement> rawLists = driver.findElements(By.cssSelector("div > *"));
		System.out.println(rawLists.size() + " raw list(s) found");
		
		//then we filter out all the elements that can't possibly be lists
		List<WebElement> lists = new LinkedList<WebElement>();
		for(WebElement e : rawLists){
			if(!IGNORED_TAGS.contains(e.getTagName())){
				lists.add(e);
			}
		}
		
		//some debug printing; ideally, lists.size() should equal the number of cls question categories (14, I think?)
		System.out.println(lists.size() + " list(s) found after filtering");
		for(WebElement e : lists){
			System.out.println(e + getText(e));
		}
		
		if(lists.size() <= 1){//If we don't find any lists, or only one, we know the rest of the parse will fail
			throw new RuntimeException("The document could not be parsed correctly!");
		}
		
		TranscriptTreeNode rootNode = new TranscriptTreeNode(ROOT_NODE_NAME);
		
		if (jpb != null) {
			jpb.setMaximum(lists.size());
			jpb.setMinimum(0);
			jpb.setValue(0);
			jpb.setIndeterminate(false);
		}
		
		
		//okay, here's the really complicated part of the parse, thankfully it's only 12 lines
		WebElement lastElement = null;
		boolean inHeader = true; //this bool allows us to print any titles/info above the transcript
		for(WebElement child : lists){
			if(TEXT_OVERRIDE.contains(child.getTagName())){
				if(inHeader && lastElement != null)stream.println("#"+getText(lastElement));
				if(getText(child) != null)
					lastElement = child;
			} else if(child.getTagName().equals("ul") || child.findElement(By.xpath("..")).getAttribute("id").equals("transcriptBody")){//ignore the second part of this condition, it will never happen, I should probably remove it
				inHeader = false;
				rootNode.addChild(buildTree(child, getText(lastElement)));
			}
			if (jpb != null) jpb.setValue(jpb.getValue() + 1);
		}
		
		return rootNode;
	}
	
	/**
	 * gets the text of an element for a standard parse, after stripping out any strange symbols
	 * @param node
	 * @return
	 */
	private static String getText(WebElement node){
		if(node == null) return null;
		try{
			if(node.getTagName().equals("li") || TEXT_OVERRIDE.contains(node.getTagName())) return node.getText();
			return node.findElement(By.xpath(".//span[not(contains(@style, \'Symbol\'))]")).getText();
		} catch (Exception e){
//			System.out.println("could not find text for "+ node.toString() + " e:"+e.getMessage());
			return null;
		}
	}
	
	/**
	 * this is a recursive debug function that just prints the contents of the tree
	 * @param node
	 * @param indent
	 */
	private static void printTree(TranscriptTreeNode node, int indent){
		System.out.println(genIndent(indent)+node.getLabel());
		if(!node.isLeafNode()){
			for(TranscriptTreeNode child : node.getChildren()){
				printTree(child, indent+1);
			}
		}
	}
	
	/**
	 * used by <code>printTree()</code>
	 * @param indent
	 * @return
	 */
	private static String genIndent(int indent){
		String out = "";
		for(int i = 0; i < indent; i++){
			out+="\t";
		}
		return out;
	}
	
	/**
	 * Writes the tree to the file
	 * @param root
	 */
	private static void writeTree(TranscriptTreeNode root){
		stream.println("#"+root.getLabel());
		List<String> answers = new LinkedList<String>();
		for(TranscriptTreeNode child : root.getChildren()){
			if(child.isLeafNode()) answers.add(child.getLabel());
		}
		if(!answers.isEmpty())stream.println(String.join("|", answers));
		for(TranscriptTreeNode child : root.getChildren()){
			if(!child.isLeafNode())writeTree(child);
		}
	}
	
	/**
	 * Finds the transcriptBody div, if it exists, returns null otherwise
	 * @param driver
	 * @return
	 */
	private static WebElement getTranscriptBody(WebDriver driver){
		try{
			return driver.findElement(By.cssSelector(SELECTOR_TRANSCRIPT_BODY_DIV));
		} catch (Exception e){
			return null;
		}
	}

}
