package apiDocCreator;

import java.util.HashMap;
import java.util.Map;

import org.json.JSONObject;

import utils.Utils;

public class ApiDocumentCreater {
	
	private static final Map<String, String> AUTH_HEADERS;
	static{
		AUTH_HEADERS = new HashMap<String, String>();
		AUTH_HEADERS.put("Accept", "application/json");
		AUTH_HEADERS.put("Content-Type", "application/json");
	}
	
	public static String getLaunchUrl(String urlPrefix, String clientId, String clientSecret, String documentName, String documentNumber){
		String accessToken = getAuthToken(urlPrefix, clientId, clientSecret);
		if(accessToken == null) return null;
		Map<String, String> headers = new HashMap<>();
		headers.put("Content-Type", "application/json");
		headers.put("Authorization", "bearer " + accessToken);
		
		int documentId = getNewDocument(urlPrefix, documentName, documentNumber, headers);
		if(documentId == -1) return null;
		
		int interviewId = getNewInterview(urlPrefix, documentId, headers);
		if(interviewId == -1) return null;
		
		return getLaunchUrl(urlPrefix, interviewId, headers);
	}
	
	private static String getAuthToken(String urlPrefix, String clientId, String clientSecret){
		JSONObject body = new JSONObject();
		body.put("grant_type", "client_credentials");
		body.put("client_id", clientId);
		body.put("client_secret", clientSecret);
		
		
		
		RestRequest req = new RestRequest(urlPrefix+"api/oauth2/token", "POST", body.toString(), AUTH_HEADERS);
		if(req.send() == 200){
			JSONObject result = req.getResponse();
			return result.getString("access_token");
		} else {
			return null;
		}
	}
	
	private static int getNewDocument(String urlPrefix, String documentName, String documentNumber, Map<String, String> headers){
		JSONObject body = new JSONObject();
		body.put("document_type", "S");
		body.put("acquisition_title", documentName);
		body.put("document_number", documentNumber);
		
		RestRequest req = new RestRequest(urlPrefix+"api/v1/documents", "POST", body.toString(), headers);
		int retval = req.send();
		if(retval == 200){
			JSONObject response = req.getResponse();
			JSONObject document = response.optJSONObject("document");
			if(document == null) return -1;
			return document.optInt("id");
		} else if(retval == 400){
			JSONObject response = req.getResponse();
			String code = response.optString("document_number");
			if(code == null || !code.equals("has already been taken")) return -1;
			return getNewDocument(urlPrefix, documentName, Utils.getNextName(documentNumber), headers);
		} else {
			return -1;
		}
	}
	
	private static int getNewInterview(String urlPrefix, int docId, Map<String, String> headers){
		JSONObject body = new JSONObject();
		body.put("document_id", docId);
		RestRequest req = new RestRequest(urlPrefix + "api/v1/interviews", "POST", body.toString(), headers);
		if(req.send() == 200){
			JSONObject response = req.getResponse();
			JSONObject launcher = response.optJSONObject("interview_launcher");
			if(launcher == null) return -1;
			return launcher.getInt("id");
		} else {
			return -1;
		}
	}
	private static String getLaunchUrl(String urlPrefix, int interviewId, Map<String, String> headers){
		RestRequest req = new RestRequest(urlPrefix + "api/v1/interviews/"+interviewId+"/launch_url", "GET", null, headers);
		if(req.send() == 200){
			JSONObject response = req.getResponse();
			String result = response.optString("launch_url");
			if(result.equals("")) return null;
			return result;
		} else {
			return null;
		}
	}
}
