package apiDocCreator;

import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Map;

import org.json.JSONObject;
import org.json.JSONTokener;

public class RestRequest {
	private JSONObject response;
	private Map<String, String> headers;
	private String address, requestType, body;
	
	public RestRequest(String address, String requestType, String body, Map<String, String> headers){
		this.address = address;
		this.requestType = requestType;
		this.headers = headers;
		this.body = body;
	}
	
	public int send(){
		
	    try {
	        URL url = new URL(address);

	        HttpURLConnection connection = (HttpURLConnection) url.openConnection();

	        connection.setDoInput(true);
	        connection.setDoOutput(true);
	        connection.setRequestMethod(requestType);
	        for(Map.Entry<String, String> pair : headers.entrySet()){
	        	connection.setRequestProperty(pair.getKey(), pair.getValue());
	        }
	        
	        System.out.println("Sending "+requestType+" request to " + address);
	        boolean isGet = requestType.equals("GET");
	        if(!isGet){
	        	System.out.println(" with payload: \n"+body);
	        	OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream(), "UTF-8");
	        	if(body!=null && !isGet)writer.write(body);
	        	writer.close();
	        }
	        InputStream instream;
	        try{
	        	instream = connection.getInputStream();
	        } catch(Exception e){
	        	instream = connection.getErrorStream();
	        }
	        int responseCode = connection.getResponseCode();
	        
	        try{
	        	response = new JSONObject(new JSONTokener(instream));
	        } catch(Exception e){
	        	response = new JSONObject();
	        }
	        if(instream != null)instream.close();
	        connection.disconnect();
	        return responseCode;
	    } catch (Exception e) {
	            e.printStackTrace();
	    }
	    return -1;
	}
	
	public JSONObject getResponse() {
		return response;
	}
	
	static{
		System.setProperty("jsse.enableSNIExtension", "false");
	}
}
