/*
	
    This script loads the ECFR clauses 
		and also manually downloaded clauses
*/
    
truncate table temp_official_clauses;
truncate table temp_official_clauses_alt;

LOAD DATA INFILE '/Users/kdesautels/desktop/official_clauses.csv' 
INTO TABLE temp_official_clauses 
CHARACTER SET UTF8
FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"' ESCAPED BY '"'
LINES TERMINATED BY '\n' (
	clause_number,
    title,
    prescription,
    content,
    contentHTML,
	contentHTMLFillins,
    uri);
    
LOAD DATA INFILE '/Users/kdesautels/desktop/official_clauses_alts.csv' 
INTO TABLE temp_official_clauses_alt 
CHARACTER SET UTF8
FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"' ESCAPED BY '"'
LINES TERMINATED BY '\n' 
	(
	clause_number,
    alt_number,
    instructions,
    content,
    contentHTML,
    contentHTMLFillins
    );

--

update temp_clauses_with_table_fill_ins_kyle 
set contentHTML = replace(contentHTML,'â','—'), contentHTMLFillins = replace(contentHTMLFillins,'â','—');

update temp_clauses_with_table_fill_ins_kyle 
set contentHTML = replace(contentHTML,'â','"'),contentHTMLFillins = replace(contentHTMLFillins,'â','"');

update temp_clauses_with_table_fill_ins_kyle 
set contentHTML = replace(contentHTML,'â','"'), contentHTMLFillins = replace(contentHTMLFillins,'â','"');

update temp_clauses_with_table_fill_ins_kyle 
set contentHTML = replace(contentHTML,'â','"'), contentHTMLFillins = replace(contentHTMLFillins,'â','"');

update temp_clauses_with_table_fill_ins_kyle 
set contentHTML = replace(contentHTML,'â?¡',''), contentHTMLFillins = replace(contentHTMLFillins,'â?¡','');

update temp_clauses_with_table_fill_ins_kyle 
set contentHTML = replace(contentHTML,'Â Â',''), contentHTMLFillins = replace(contentHTMLFillins,'Â Â','');


update temp_clauses_with_table_fill_ins_kyle 
set contentHTML = replace(contentHTML,'“','"'), contentHTMLFillins = replace(contentHTMLFillins,'“','"');


update temp_clauses_with_table_fill_ins_kyle 
set contentHTML = replace(contentHTML,'”','"'), contentHTMLFillins = replace(contentHTMLFillins,'”','"');

--


-- Manual replaces (namely table fillins)

update temp_official_clauses c
join temp_clauses_with_table_fill_ins_kyle manual
	on manual.clause_number = c.clause_number
set c.contentHTMLFillins = replace(convert(manual.contentHTMLFillins USING latin1),'<meta http-equiv="content-type" content="text/html; charset=utf-8">','');

update temp_official_clauses_alt c
join temp_clauses_with_table_fill_ins_kyle manual
	on manual.clause_number = concat(c.clause_number, ' ', alt_number)
set c.contentHTMLFillins = replace(convert(manual.contentHTMLFillins USING latin1),'<meta http-equiv="content-type" content="text/html; charset=utf-8">','');

-- ----------

-- Manually added clauses

INSERT INTO `temp_official_clauses_alt` (`clause_number`,`alt_number`,`instructions`,`content`,`contentHTML`) VALUES (
	'252.219-7003',
    'Alternate I',
    'Alternate I (OCT 2014) As prescribed in 219.708(b)(1)(A)(2), substitute the following paragraph (f)(1)(i) for (f)(1)(i) in the basic clause: (f)(1)(i) The Standard Form 294 Subcontracting Report for Individual Contracts shall be submitted in accordance with the instructions on that form; paragraph (f)(2)(i) is inapplicable.',
    'Alternate I (OCT 2014) As prescribed in 219.708(b)(1)(A)(2), substitute the following paragraph (f)(1)(i) for (f)(1)(i) in the basic clause: (f)(1)(i) The Standard Form 294 Subcontracting Report for Individual Contracts shall be submitted in accordance with the instructions on that form; paragraph (f)(2)(i) is inapplicable.',
	'<p>(f)(1)(i) The Standard Form 294 Subcontracting Report for Individual Contracts shall be submitted in accordance with the instructions on that form; paragraph (f)(2)(i) is inapplicable.</p>');


INSERT INTO `temp_official_clauses` (`clause_number`,`title`,`prescription`,`uri`,`content`,`contentHTML`,`contentHTMLFillIns`,`contentHTMLProcessed`) VALUES (
	'252.204-0001',
    'Line Item Specific: Single Funding.',
    '',
    'http://farsite.hill.af.mil/reghtml/regs/far2afmcfars/fardfars/Dfars/PGI%20204_71.htm#P1058_24821',
    'The payment office shall make payment using the ACRN funding of the line item being billed. (2) Line item specific: sequential ACRN order. If there is more than one ACRN within a contract line item (i.e., informational subline items contain separate ACRNs),and the contracting officer intends funds to be liquidated in ACRN order, insert the following:',
	'<p><font face="Times New Roman" size="+0">The payment office shall make payment using the ACRN funding of the line item being billed.</font></p> <p><font face="Times New Roman" size="+0">(2)  <i>Line item specific: sequential ACRN order</i>.  If there is more than one ACRN within a contract line item (i.e., informational subline items contain separate ACRNs),and the contracting officer intends funds to be liquidated in ACRN order, insert the following:</font></p>',
    NULL,
    NULL);


insert into temp_official_clauses (clause_number, title, prescription, uri, content, contentHTML, contentHTMLFillins)
select clause_number, title, prescription, uri, content, contentHTML, contentHTMLFillins
from temp_official_clauses_kyle k
where not exists ( select 1 from temp_official_clauses where clause_number = k.clause_number );

insert into temp_official_clauses_alt (clause_number, alt_number, instructions, content, contentHTML, contentHTMLFillins)
select clause_number, alt_number, instructions, content, contentHTML, contentHTMLFillins
from temp_official_clauses_alt_kyle k
where not exists ( select 1 from temp_official_clauses_alt where clause_number = k.clause_number and alt_number = k.alt_number );


