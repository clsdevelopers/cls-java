/*
	This script loads the FAR/DFARS clauses from spreadhseet

*/

truncate table raw_clauses;


LOAD DATA INFILE '/Users/kdesautels/desktop/FAR.csv' 
INTO TABLE raw_clauses
CHARSET LATIN1
FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"' ESCAPED BY '"'
LINES TERMINATED BY 'EOL\\n' (
	SECTION, 
	IBR_OR_FULL_TEXT,
    FILL_IN, 
    CLAUSE, 
    EFFECTIVE_CLAUSE_DATE ,
    CLAUSE_PRESCRIPTION , 
    PRESCRIPTION_TEXT , 
    CONDITIONS, 
    RULE,
    ADDITIONAL_CONDITIONS,
    PRESC_OR_CLAUSE);
    
    
LOAD DATA INFILE '/Users/kdesautels/desktop/DFAR.csv' 
INTO TABLE raw_clauses 
CHARSET LATIN1
FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"' ESCAPED BY '"'
LINES TERMINATED BY 'EOL\\n' (
	SECTION, 
	IBR_OR_FULL_TEXT,
	FILL_IN,
	CLAUSE,
    EFFECTIVE_CLAUSE_DATE ,
    CLAUSE_PRESCRIPTION ,
    PRESCRIPTION_TEXT ,
    CONDITIONS,
    RULE,
    ADDITIONAL_CONDITIONS);    

update raw_clauses set clause = trim(clause);
update raw_clauses set section = replace(section,char(10),'');
update raw_clauses set clause = replace(clause,'ATERNATE','ALTERNATE');

CALL normalize_string('raw_clauses','conditions', '\\\\n');
CALL normalize_string('raw_clauses','conditions', ' ');
CALL normalize_string('raw_clauses','rule', ' ');
CALL normalize_string('raw_clauses','clause', ' ');
CALL normalize_string('raw_clauses','clause_prescription', ' ');


update raw_clauses set clause_prescription = replace(clause_prescription, ' ','');

update raw_clauses set effective_clause_date = trim(effective_clause_date);
update raw_clauses set clause = replace(clause,' ',' ');

update raw_clauses
set clause_number = 
	(
	select ifnull(alternate_number,main_clause_number)
	from
		(
		select 
			id,
			substring_index(clause,' ',1) main_clause_number, clause,
			case 
				when upper(substring_index(clause,' ',3)) like '%ALTERNATE%' and clause not like '%ALTERNATE PRESERV%' and clause not like '%ALTERNATE A%' 
                    then substring_index(clause,' ',3)  
				when substring_index(clause,'ALTERNATE ',-1) not like '% %' 
					then concat(substring_index(clause,' ',1),' ALTERNATE ', substring_index(clause,'ALTERNATE ', -1))
			end alternate_number
		from raw_clauses
		) as sub
	where raw_clauses.id = sub.id
    );
    
update raw_clauses set clause_number = replace(clause_number,',','');

update raw_clauses set clause_number = replace(clause_number,',','');
update raw_clauses set clause_number = replace(clause_number,'Alternate A','');


update raw_clauses set clause_prescription = replace(replace(replace(clause_prescription, ' ',''),' ',''),'€','');

update raw_clauses
set clause_number = concat(clause_number, ' ', substring(clause,locate('(DEVIATION ', clause)))
where clause like '%(DEVIATION%';

update raw_clauses 
set clause_number = replace(clause_number,'\\n','');
/*
update raw_clauses
set clause_number = replace(clause_number,'-O00','-000')
where clause_number like '%DEVIATION%';
*/

