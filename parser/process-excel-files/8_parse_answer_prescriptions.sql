drop procedure if exists normalize_prescriptions;

DELIMITER $$
 
CREATE PROCEDURE normalize_prescriptions ()
BEGIN
 
DECLARE v_finished INTEGER DEFAULT 0;
DECLARE v_id INTEGER DEFAULT 0;
DECLARE v_prescriptions varchar(10000) DEFAULT ""; 
DECLARE INDX INT;
 DECLARE SLICE nvarchar(4000); 

 
DEClARE prescription_cursor CURSOR FOR
select id, col002
from raw_questions 
where col002 != ''; 
 
-- declare NOT FOUND handler
DECLARE CONTINUE HANDLER
        FOR NOT FOUND SET v_finished = 1;
 
OPEN prescription_cursor;
 
get_prescription: LOOP
  SET INDX = 1;
	FETCH prescription_cursor INTO v_id, v_prescriptions;
	 
	MyLoop: WHILE INDX != 0 DO
		 -- GET THE INDEX OF THE FIRST OCCURENCE OF THE SPLIT CHARACTER
		 SET INDX = LOCATE('\\n',v_prescriptions);
		 -- NOW PUSH EVERYTHING TO THE LEFT OF IT INTO THE SLICE VARIABLE
		 IF INDX !=0 THEN
		  SET SLICE = LEFT(v_prescriptions,INDX - 1);
		 ELSE
		  SET SLICE = v_prescriptions;
		 END IF;
		 -- PUT THE ITEM INTO THE RESULTS SET
		 INSERT INTO answers_prescriptions_xref(raw_id, prescription) VALUES(v_id,SLICE);
		 -- CHOP THE ITEM REMOVED OFF THE MAIN STRING
		 SET v_prescriptions = RIGHT(v_prescriptions,CHAR_LENGTH(v_prescriptions) - INDX-1);
		 -- BREAK OUT IF WE ARE DONE
		 IF LENGTH(v_prescriptions) = 0 THEN
		  LEAVE MyLoop;
		 END IF;
	END WHILE;
    
	IF v_finished = 1 THEN
		LEAVE get_prescription;
	END IF;
	
END LOOP get_prescription;
 
CLOSE prescription_cursor;
 
END$$
 
DELIMITER ;



DELETE FROM answers_prescriptions_xref;
CALL normalize_prescriptions();

update answers_prescriptions_xref set prescription = trim(replace(replace(prescription, ' ',''),' ',''));
update answers_prescriptions_xref set prescription = replace(prescription, 'Ê','');
