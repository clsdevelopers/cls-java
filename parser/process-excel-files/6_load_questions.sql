/*

	This script loads questions, possible answers, rules, etc from the questions spreadsheet
    
*/

/* Remove previous data */
delete from answers_prescriptions_xref;
delete from temp_possible_answers;
delete from temp_questions;
delete from raw_questions;

/* Reset sequences */
ALTER TABLE answers_prescriptions_xref AUTO_INCREMENT = 1;
ALTER TABLE temp_possible_answers AUTO_INCREMENT = 1;
ALTER TABLE temp_questions AUTO_INCREMENT = 1;
ALTER TABLE raw_questions AUTO_INCREMENT = 1;


/* Load data in from flat files */
LOAD DATA INFILE '/Users/kdesautels/desktop/questions.csv' 
INTO TABLE raw_questions 
CHARACTER SET LATIN1
FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"' ESCAPED BY '"'
LINES TERMINATED BY '\r' (col001, col002, @ignore, @ignore, @ignore, tab_name)
SET category = 'Baseline';

LOAD DATA INFILE '/Users/kdesautels/desktop/questions.csv' 
INTO TABLE raw_questions
CHARACTER SET LATIN1
FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"' ESCAPED BY '"'
LINES TERMINATED BY '\r' (@ignore, @ignore, @ignore, col001, col002, tab_name)
SET category = 'Follow-on' ;


/* Trim whitespace from ends */
update raw_questions set col001 = trim(col001); 

/* Normalize double newlines with single newline */
CALL normalize_string('raw_questions','col001', '\\\\n');

/* Remove leading newlines  */
update raw_questions set col001 = substring(col001,3,length(col001)-2) where substring(col001,1,2) = '\\n'; 
update raw_questions set col001 = substring(col001,3,length(col001)-2) where substring(col001,1,2) = '\\n'; 
update raw_questions set col001 = substring(col001,3,length(col001)-2) where substring(col001,1,2) = '\\n';

/* Remove trailing newlines */
update raw_questions set col001 = substring(col001,1,length(col001)-2) where substring(col001,-2) = '\\n'; 
update raw_questions set col001 = substring(col001,1,length(col001)-2) where substring(col001,-2) = '\\n'; 
update raw_questions set col001 = substring(col001,1,length(col001)-2) where substring(col001,-2) = '\\n'; 

--
    
update raw_questions
set col001 = replace(col001,'\\nINSPECTION AND ACCEPTANCE REQUIREMENTS]','\\n[INSPECTION AND ACCEPTANCE REQUIREMENTS]');
update raw_questions
set col001 = replace(col001,'\\n INSPECTION AND ACCEPTANCE REQUIREMENTS]','\\n[INSPECTION AND ACCEPTANCE REQUIREMENTS]');

update raw_questions
set col001 = replace(col001,'[EXEMPTIONS}','[EXEMPTIONS]');
update raw_questions
set col001 = replace(col001,'[TRANSLATION TO ANOTHER LANGUAGE[','[TRANSLATION TO ANOTHER LANGUAGE]');
update raw_questions
set col001 = replace(col001,'[TRADE AGREEMENT APPLICABILITY\\n','[TRADE AGREEMENT APPLICABILITY]\\n');
update raw_questions
set col001 = replace(col001,'[REPORTS AND DRAWINGS\n','[REPORTS AND DRAWINGS]\n');
update raw_questions
set col001 = replace(col001,'\\nINTRASTATE MOVEMENT]','\\n[INTRASTATE MOVEMENT]');
update raw_questions
set col001 = replace(col001,'[DISMANTLING PAYMENT ARRANGEMENT[','[DISMANTALLING PAYMENT ARRANGEMENT]');

update raw_questions
set col001 = replace(col001,'\\nFULL AND OPEN EXCEPTION]','\\n[FULL AND OPEN EXCEPTION]');

/* Begin Parser*/
insert into temp_questions 
	(
    raw_id, question_name, question_text, question_type, rule, normal_rule, type, category
    )        
select 
	 id, question_name, trim(question_text) question_text,
	 case 
		when 
			(
				select trim(upper(col001))
                from raw_questions
                where id = layer1.id + 1
            ) in ('NO', 'YES')
            then 'Boolean'
		when 
			( type regexp '.*(MAY|MUST|HAS TO) SELECT ONE.*' and type not like '%OR MORE%' )
			or type regexp '.*(MAY|MUST|HAS TO) SELECT EITHER.*' 
			then 'Single Answer'
        when type regexp '.*((MAY|MUST|HAS TO) )*SELECT ONE OR MORE.*' 
			or type regexp '.*(MAY|MUST|HAS TO) SELECT AT LEAST ONE.*' 
            or type like '%CAN SELECT ALL%' 
            or type like '%MANY%' 
            or type like '%AT LEAST%'
			then 'Multiple Answer'
		when type regexp '.*(MAY|MUST|HAS TO) FILL IN A (MIN|NUMBER)*.*' then 'Numeric'
	 end question_type,
     rule,
	 rule normal_rule,
	 type,
     category
from
	(
	select 
		id,
		substr(col001,instr(col001,'['),instr(col001,']')-instr(col001,'[')+1) question_name, 
		case 
			when 
				trim(col001) not like 'ASK%' 
                and trim(col001) not like '(ASK%' 
				and trim(col001) not like 'ONLY ASK%'
                then substr(col001,1, instr(col001,'\\n')-1)
			else
				substring_index(substring_index(col001,'\\n', 2),'\\n',-1)
		end question_text, 
		substring_index(col001, 'USER ', -1) type,
		case 
			when trim(col001) like 'ASK%' or trim(col001) like '(ASK%' 
			then trim(substring_index(col001,'\\n', 1))
		end rule,
		col001, col002, category
	from raw_questions q
	where col002 ='' and col001 != ''
		and col001 LIKE '%[%]%'
		and exists
		(
		select 1
		from raw_questions prev
		where prev.id + 1 = q.id 
			and col001 = '' and col002 = ''
		)
 ) as layer1;

    
    

insert into temp_possible_answers
	(
    question_id,
    choice_text,
    raw_id
    )
select 
	q.id, trim(choice.col001) "choice text", choice.id
from temp_questions q
join raw_questions r_q
	on q.raw_id = r_q.id
join raw_questions choice
	on choice.id > r_q.id
    and choice.id <
		(
        select min(next_question.id)
        from raw_questions next_question
        where next_question.id > q.raw_id
			and (col001 = '' and col002 = '' )
        )
	and not exists  /* Exclude "baseline questions" (i.e. questions expressed with sub-types) */
		(
        select 1
		from raw_questions next_line
        where
			(
            col001 REGEXP '^[0-9]+\\. ' = 1
			)
            and next_line.id = r_q.id + 1
        )
where ifnull(question_type,'')  not in ('Numeric');


insert into temp_possible_answers
	(
    question_id,
    choice_text,
    raw_id
    )
select 
	q.id, trim(substring_index(choice.col001,'\\n',1)) "choice text", choice.id
from temp_questions q
join raw_questions r_q
	on q.raw_id = r_q.id
    and exists  
		(
        select 1
		from raw_questions next_line
        where
			(
            col001 like '1. %'
			)
            and next_line.id = r_q.id + 1
        )
join raw_questions choice
	on choice.id > r_q.id
    and ( choice.col001 REGEXP '^[0-9]+\\. ' = 1 )
    and choice.id <
		(
        select min(next_question.id)
        from raw_questions next_question
        where 
			( col001 = '' and col002 = '' )
			and next_question.id > q.raw_id
        );

delete from temp_possible_answers where trim(choice_text) = '';

update temp_possible_answers 
set choice_text = trim(right(choice_text, length(choice_text) - locate('. ', choice_text))) 
where choice_text regexp '^([[:digit:]]|[[:alpha:]])+\\. ';

update temp_possible_answers 
set choice_text = trim(replace(choice_text,'\\n','')) 
where choice_text regexp '\\n';


-- Layer 2 Questions

insert into temp_questions 
	(
    raw_id, question_name, question_text, question_type, parsed_rule, rule, type
    )
select 
	id, 
    case when question_name='' then concat('[', q_name, ']') else question_name end, 
    question_text, 
    case 
		when 
			( type regexp '.*(MAY|MUST|HAS TO) SELECT ONE.*' and type not like '%OR MORE%' )
			or type regexp '.*(MAY|MUST|HAS TO) SELECT EITHER.*' 
			then 'Single Answer'
        when type regexp '.*((MAY|MUST|HAS TO) )*SELECT ONE OR MORE.*' 
			or type regexp '.*(MAY|MUST|HAS TO) SELECT AT LEAST ONE.*' 
            or type like '%CAN SELECT ALL%' 
            or type like '%MANY%' 
            or type like '%AT LEAST%'
			then 'Multiple Answer'
		when type regexp '.*(MUST|HAS TO) FILL IN A (MIN|NUMBER)*.*' then 'Numeric'
	 end question_type , parsed_rule, rule, type
from
	(
	select 
		distinct 
		choice.id, 
        substr(choice.col001,instr(choice.col001,'['),instr(choice.col001,']')-instr(choice.col001,'[')+1) question_name,
		concat('Select ', lower(trim(substring_index(substring_index(choice.col001,'\\n',1),'. ',-1))),' subtype(s):') question_text,
		concat('"', ans.choice_text,'" = ', q.question_name) parsed_rule,
        substring_index(choice.col001, 'USER ', -1) type,
        choice.col001 rule,
        upper(trim(left(substring_index(substring_index(choice.col001,'\\n',1),'. ',-1),48))) q_name
	from temp_questions q
	join raw_questions r_q
		on q.raw_id = r_q.id
	join raw_questions next_line
		 on next_line.col001 like '1. %'
		 and next_line.id = r_q.id + 1
	join raw_questions choice
		on choice.id > r_q.id
		and ( choice.col001 REGEXP '^[0-9]+\\. ' = 1 )
		and choice.id <
			(
			select min(next_question.id)
			from raw_questions next_question
			where ( col001 = '' and col002 = '' )
				and next_question.id > q.raw_id
			)
	join temp_possible_answers ans
		on ans.raw_id = choice.id
	join raw_questions choice_2
		on choice_2.id > choice.id
		and ( choice_2.col001 REGEXP '^[a-z]\\. ' = 1 )
		and choice_2.id <
			(
			select min(next_choice.id)
			from raw_questions next_choice
			where 
				next_choice.id > choice.id
				and
					(
					( next_choice.col001 REGEXP '^[0-9]+\\. ' = 1 ) 
					or ( next_choice.col001 = '' and next_choice.col002 = '' )
				)
			) 
	) as sub;

/* Level 2 possible answers */
insert into temp_possible_answers
	(
    question_id,
    choice_text,
    raw_id
    )
select 
	quest.id,
    trim(right(substring_index(choice_2.col001,'\\n',1), length(substring_index(choice_2.col001,'\\n',1)) - locate('. ', choice_2.col001))) "choice 2 text",
	choice_2.id
from temp_questions q
join raw_questions r_q
	on q.raw_id = r_q.id
join raw_questions next_line
	on next_line.col001 like '1. %'
    and next_line.id = r_q.id + 1
join raw_questions choice
	on choice.id > r_q.id
    and ( choice.col001 REGEXP '^[0-9]+\\. ' = 1 )
    and choice.id <
		(
        select min(next_question.id)
        from raw_questions next_question
        where ( col001 = '' and col002 = '' )
            and next_question.id > q.raw_id
        )
join temp_questions quest
	on quest.raw_id = choice.id
join raw_questions choice_2
	on choice_2.id > choice.id
    and ( choice_2.col001 REGEXP '^[a-z]\\. ' = 1 )
    and choice_2.id <
		(
        select min(next_choice.id)
        from raw_questions next_choice
        where 
			next_choice.id > choice.id
			and
				(
                ( next_choice.col001 REGEXP '^[0-9]+\\. ' = 1 ) 
				or ( next_choice.col001 = '' and next_choice.col002 = '' )
			)
        );
        
--        
update temp_possible_answers set choice_text = trim(right(choice_text, length(choice_text) - locate('. ', choice_text))) where choice_text regexp '^([[:digit:]]|[[:alpha:]])+\\. ';
update temp_possible_answers set choice_text = trim(replace(choice_text,'\\n','')) where choice_text regexp '\\n';
--
/* Level 3 questions */

insert into temp_questions 
	(
    raw_id, question_name, question_text, question_type, parsed_rule, rule, type
    )
select 
	id, 
    case when question_name='' then concat('[', q_name, ']') else question_name end, 
    question_text, 
    case 
		when 
			( type regexp '.*(MAY|MUST|HAS TO) SELECT ONE.*' and type not like '%OR MORE%' )
			or type regexp '.*(MAY|MUST|HAS TO) SELECT EITHER.*' 
			then 'Single Answer'
        when type regexp '.*((MAY|MUST|HAS TO) )*SELECT ONE OR MORE.*' 
			or type regexp '.*(MAY|MUST|HAS TO) SELECT AT LEAST ONE.*' 
            or type like '%CAN SELECT ALL%' 
            or type like '%MANY%' 
            or type like '%AT LEAST%'
			then 'Multiple Answer'
		when type regexp '.*(MAY|MUST|HAS TO) FILL IN A (MIN|NUMBER)*.*' then 'Numeric'
	 end question_type , parsed_rule, rule, type
from
	(
	select 
		distinct 
		choice.id, 
        substr(choice.col001,instr(choice.col001,'['),instr(choice.col001,']')-instr(choice.col001,'[')+1) question_name,
		concat('Select ', lower(trim(substring_index(substring_index(choice.col001,'\\n',1),'. ',-1))),' subtype(s):') question_text,
		concat('"', ans.choice_text,'" = ', q.question_name) parsed_rule,
        substring_index(choice.col001, 'USER ', -1) type,
        choice.col001 rule,
        upper(trim(left(substring_index(substring_index(choice.col001,'\\n',1),'. ',-1),48))) q_name
	from temp_questions q
	join raw_questions r_q
		on q.raw_id = r_q.id
	join raw_questions next_line
		 on next_line.col001 like 'a. %'
		 and next_line.id = r_q.id + 1
	join raw_questions choice
		on choice.id > r_q.id
		and ( choice.col001 REGEXP '^[a-z]\\. ' = 1 )
		and choice.id <
			(
			select min(next_question.id)
			from raw_questions next_question
			where 
				(
                ( col001 = '' and col002 = '' )
				or ( col001 REGEXP '^[0-9]+\\. ' = 1 )
                )
				and next_question.id > q.raw_id
			)
	join temp_possible_answers ans
		on ans.raw_id = choice.id
	join raw_questions choice_2
		on choice_2.id > choice.id
		and ( choice_2.col001 REGEXP '^\\((XC|XL|L?X{0,3})(IX|IV|V?I{0,3})\\) ' = 1 )
		and choice_2.id <
			(
			select min(next_choice.id)
			from raw_questions next_choice
			where 
				next_choice.id > choice.id
				and
					(
					( next_choice.col001 REGEXP '^[a-z]\\. ' = 1 ) 
                    or ( next_choice.col001 REGEXP '^[0-9]+\\. ' = 1 ) 
					or ( next_choice.col001 = '' and next_choice.col002 = '' )
				)
			) 
	) as sub;        


/* Level 3 possible answers */
insert into temp_possible_answers
	(
    question_id,
    choice_text,
    raw_id
    )
select 
	quest.id,
    trim(right(substring_index(choice_2.col001,'\\n',1), length(substring_index(choice_2.col001,'\\n',1)) - locate(') ', choice_2.col001))) "choice 2 text",
	choice_2.id
from temp_questions q
join raw_questions r_q
	on q.raw_id = r_q.id
join raw_questions next_line
	on next_line.col001 like 'a. %'
    and next_line.id = r_q.id + 1
join raw_questions choice
	on choice.id > r_q.id
    and ( choice.col001 REGEXP '^[a-z]\\. ' = 1 )
    and choice.id <
		(
        select min(next_question.id)
			from raw_questions next_question
			where 
				(
                ( col001 = '' and col002 = '' )
				or ( col001 REGEXP '^[0-9]+\\. ' = 1 )
                )
				and next_question.id > q.raw_id
        )
join temp_questions quest
	on quest.raw_id = choice.id
join raw_questions choice_2
	on choice_2.id > choice.id
    and ( choice_2.col001 REGEXP '^\\((XC|XL|L?X{0,3})(IX|IV|V?I{0,3})\\) ' = 1 )
    and choice_2.id <
		(
        select min(next_choice.id)
        from raw_questions next_choice
        where 
			next_choice.id > choice.id
			and
				(
                ( next_choice.col001 REGEXP '^[a-z]\\. ' = 1 ) 
                or ( next_choice.col001 REGEXP '^[0-9]+\\. ' = 1 ) 
				or ( next_choice.col001 = '' and next_choice.col002 = '' )
			)
        );

--
update temp_possible_answers set choice_text = trim(right(choice_text, length(choice_text) - locate('. ', choice_text))) where choice_text regexp '^([[:digit:]]|[[:alpha:]])+\\. ';
update temp_possible_answers set choice_text = trim(replace(choice_text,'\\n','')) where choice_text regexp '\\n';
--

/* Level 4 questions */
insert into temp_questions 
	(
    raw_id, question_name, question_text, question_type, parsed_rule, rule, type
    )
select 
	id, 
    case when question_name='' then concat('[', q_name, ']') else question_name end, 
    question_text, 
    case 
		when 
			( type regexp '.*(MAY|MUST|HAS TO) SELECT ONE.*' and type not like '%OR MORE%' )
			or type regexp '.*(MAY|MUST|HAS TO) SELECT EITHER.*' 
			then 'Single Answer'
        when type regexp '.*((MAY|MUST|HAS TO) )*SELECT ONE OR MORE.*' 
			or type regexp '.*(MAY|MUST|HAS TO) SELECT AT LEAST ONE.*' 
            or type like '%CAN SELECT ALL%' 
            or type like '%MANY%' 
            or type like '%AT LEAST%'
			then 'Multiple Answer'
		when type regexp '.*(MAY|MUST|HAS TO) FILL IN A (MIN|NUMBER)*.*' then 'Numeric'
	 end question_type , parsed_rule, rule, type
from
	(
	select 
		distinct 
		choice.id, 
        substr(choice.col001,instr(choice.col001,'['),instr(choice.col001,']')-instr(choice.col001,'[')+1) question_name,
		concat('Select ', lower(trim(substring_index(substring_index(choice.col001,'\\n',1),') ',-1))),' subtype(s):') question_text,
		concat('"', ans.choice_text,'" = ', q.question_name) parsed_rule,
        substring_index(choice.col001, 'USER ', -1) type,
        choice.col001 rule,
        upper(trim(left(substring_index(substring_index(choice.col001,'\\n',1),') ',-1),48))) q_name
	from temp_questions q
	join raw_questions r_q
		on q.raw_id = r_q.id
	join raw_questions next_line
		 on next_line.col001 like '(i) %'
		 and next_line.id = r_q.id + 1
	join raw_questions choice
		on choice.id > r_q.id
		and ( choice.col001 REGEXP '^\\((XC|XL|L?X{0,3})(IX|IV|V?I{0,3})\\) ')
		and choice.id <
			(
			select min(next_question.id)
			from raw_questions next_question
			where 
				(
                ( col001 = '' and col002 = '' )
				or ( col001 REGEXP '^[0-9]+\\. ' )
                or ( col001 REGEXP '^[a-z]\\. ' )
                )
				and next_question.id > q.raw_id
			)
	join temp_possible_answers ans
		on ans.raw_id = choice.id
	join raw_questions choice_2
		on choice_2.id > choice.id
		and ( choice_2.col001 REGEXP '^\\(A\\) ' )
		and choice_2.id <
			(
			select min(next_choice.id)
			from raw_questions next_choice
			where 
				next_choice.id > choice.id
				and
					(
					( next_choice.col001 REGEXP '^[a-z]\\. ' ) 
                    or ( next_choice.col001 REGEXP '^[0-9]+\\. ' ) 
                    or ( next_choice.col001 REGEXP '^\\((XC|XL|L?X{0,3})(IX|IV|V?I{0,3})\\) ' )
					or ( next_choice.col001 = '' and next_choice.col002 = '' )
				)
			) 
	) as sub;        

/* Level 4 possible answers */
insert into temp_possible_answers
	(
    question_id,
    choice_text,
    raw_id
    )
select 
	quest.id,
    trim(right(substring_index(choice_2.col001,'\\n',1), length(substring_index(choice_2.col001,'\\n',1)) - locate(') ', choice_2.col001))) "choice 2 text",
	choice_2.id
from temp_questions q
	join raw_questions r_q
		on q.raw_id = r_q.id
	join raw_questions next_line
		 on next_line.col001 like '(i) %'
		 and next_line.id = r_q.id + 1
	join raw_questions choice
		on choice.id > r_q.id
		and ( choice.col001 REGEXP '^\\((XC|XL|L?X{0,3})(IX|IV|V?I{0,3})\\) ')
		and choice.id <
			(
			select min(next_question.id)
			from raw_questions next_question
			where 
				(
                ( col001 = '' and col002 = '' )
				or ( col001 REGEXP '^[0-9]+\\. ' )
                or ( col001 REGEXP '^[a-z]\\. ' )
                )
				and next_question.id > q.raw_id
			)
	join temp_questions quest
		on quest.raw_id = choice.id
	join raw_questions choice_2
		on choice_2.id > choice.id
		and ( choice_2.col001 REGEXP '^\\([A-Z]) ' )
		and choice_2.id <
			(
			select min(next_choice.id)
			from raw_questions next_choice
			where 
				next_choice.id > choice.id
				and
					(
					( next_choice.col001 REGEXP '^[a-z]\\. ' ) 
                    or ( next_choice.col001 REGEXP '^[0-9]+\\. ' ) 
                    or ( next_choice.col001 REGEXP '^\\((IX|IV|V?I{0,3})\\) ' )
					or ( next_choice.col001 = '' and next_choice.col002 = '' )
				)
			);

--

update temp_possible_answers 
set choice_text = trim(right(choice_text, length(choice_text) - locate('. ', choice_text)))
where choice_text  REGEXP '^[a-z]\\. ';


update temp_possible_answers 
set choice_text = trim(right(choice_text, length(choice_text) - locate('. ', choice_text)))
where choice_text  REGEXP '^[0-9]+\\. ';

update temp_possible_answers 
set choice_text = trim(right(choice_text, length(choice_text) - locate(') ', choice_text))) 
where choice_text REGEXP '^\\((XC|XL|L?X{0,3})(IX|IV|V?I{0,3})\\) ';

update temp_possible_answers 
set choice_text = trim(right(choice_text, length(choice_text) - locate(') ', choice_text))) 
where choice_text  REGEXP '^\\([A-Z]\\) ';

CALL normalize_string('temp_questions','question_text', ' ');

update temp_possible_answers
set choice_text = trim(choice_text);


-- Set Group/Tab Names
update temp_questions
set group_name = ( select trim(tab_name) from raw_questions where id = temp_questions.raw_id );


-- "Waterfall" questions are implicit questions denoted in spreadsheet with "1.", "a.", or "(i)"
update temp_questions q
set category = 'Waterfall'
where 
	(
    select 1
    from raw_questions
    where id = q.raw_id
		and 
			(
            ( trim(col001) REGEXP '^[0-9]+\\. ' ) 
            or ( trim(col001) REGEXP '^[a-z]\\. ' ) 
            or ( trim(col001) REGEXP '^\\((IX|IV|V?I{0,3})\\) ' )
            )
    )
    or 
    (
    category = 'Follow-on'
	and exists
		(
        select 1
		from raw_questions
		where id = q.raw_id + 1
			and 
				(
				( trim(col001) REGEXP '^[0-9]+\\. ' ) 
				or ( trim(col001) REGEXP '^[a-z]\\. ' ) 
				or ( trim(col001) REGEXP '^\\((IX|IV|V?I{0,3})\\) ' )
				)
		)
    );


--

-- Normalize expressions for display rules before deriving parsed rules

update temp_questions 
set normal_rule = 
	replace(normal_rule,
		'ASK THE QUESTION ONLY AFTER THE USER SELECTS ONE OR MORE SUPPLY ITEMS',
		'ASK ONLY IF THE USER SELECTS "YES" TO "SUPPLIES"')
where normal_rule like '%ASK THE QUESTION ONLY AFTER THE USER SELECTS ONE OR MORE SUPPLY ITEMS%';

update temp_questions 
set normal_rule = 
	replace(normal_rule,
		'ASK THE QUESTION ONLY AFTER THE USER SELECTS ONE OR MORE SERVICE ITEMS',
		'ASK ONLY IF THE USER SELECTS "YES" TO "SERVICES"')
where normal_rule like '%ASK THE QUESTION ONLY AFTER THE USER SELECTS ONE OR MORE SERVICE ITEMS%';

update temp_questions
set normal_rule = 'ASK ONLY AFTER USER SELECTS "COMPETITION" UNDER "PROCEDURES" AND "SOLICITATION" UNDER "DOCUMENT TYPE"'
where question_name = '[UK OFFER]';

update temp_questions
set normal_rule = replace(normal_rule, '"SUPPLIES CLASSIFICATION"','"SUPPLY CLASSIFICATION"')
where question_name = '[RECOVERED MATERIAL ESTIMATES]';

update temp_questions
set normal_rule = 'ASK ONLY IF USER SELECTS "GOVERNMENT SUPPLY SOURCE INFORMATION/AUTHORIZATION APPLIES" UNDER "CONTRACTOR MATERIAL"'
where question_name = '[GOVERNMENT SUPPLY STATEMENT]';

--

/*
	These should probably be cleaned up in the spreadsheet but do here for now instead
*/
/*
update temp_questions
set parsed_rule = '[AGREEMENT (INCLUDING BASIC AND LOAN] IS NOT NULL OR "REQUIREMENTS" = [TYPE OF INDEFINITE DELIVERY]'
where question_name = '[ORDER VALUE]';

update temp_questions
set parsed_rule = '[AGREEMENT (INCLUDING BASIC AND LOAN] = "LEASE AGREEMENT"'
where question_name = '[LEASE OPTION]';
*/
update temp_questions
set parsed_rule = '"FULL AND OPEN COMPETITION (FAR PART 6)" <> [COMPETITION TYPE] OR "COMPETITION" <> [PROCEDURES]'
where question_name = '[FULL AND OPEN EXCEPTION]';

update temp_questions
set parsed_rule = '("MAJOR DEFENSE PROGRAM" = [TYPE OF SYSTEM] OR "MAJOR INFORMATION SYSTEMS PROGRAM" = [TYPE OF SYSTEM]) AND "SOLICITATION" = [DOCUMENT TYPE]'
where question_name = '[OSD APPROVAL FOR DATA SYSTEM]';

-- "[QUESTION] RESPONSES ARE COMPLETED" --> [Question %] is not null 
update temp_questions
set parsed_rule = 
	(
		select concat(group_concat(question_name separator ' IS NOT NULL AND '),' IS NOT NULL')
		from ( select question_name from temp_questions ) sub
		where question_name like concat('[%', substring_index(substring_index(temp_questions.rule,'"',2),'"',-1) ,'%]')
	)
where normal_rule like '%RESPONSES ARE COMPLETE%';

update temp_questions
set parsed_rule = 
	case 
		when 
			/* ASK AFTER THE QUESTION "PRICING REQUIREMENTS" IS COMPLETED AND USER HAS NOT SELECTED "COMPETITION" UNDER "PROCEDURES"
            			------>  	
				"PRICING REQUIREMENTS" IS NOT NULL AND "COMPETITION" <> [PROCEDURES] */
			normal_rule REGEXP 'ASK [[:print:]]* "[[:print:]]*" IS COMPLETED (AND|OR) [[:print:]]* NOT SELECTED "[[:print:]]*" (IS SELECTED UNDER|TO|TO THE QUESTION|WITHIN|UNDER|FOR) "[[:print:]]*"'
            then 
				concat('[',
					substring_index(substring_index(normal_rule,'"',2),'"',-1),
					'] IS NOT NULL ',
                    substring_index(substring_index(normal_rule,'" IS COMPLETED ',-1),' ',1),
                    ' "',
					substring_index(substring_index(normal_rule,'"',4),'"',-1),
					'" <> [',
                    substring_index(substring_index(normal_rule,'"',6),'"',-1),
                    ']')
		when 
			/* ASK AFTER THE QUESTION "PRICING REQUIREMENTS" IS COMPLETED AND USER HAS SELECTED "COMPETITION" UNDER "PROCEDURES"
            			------>  	
				"PRICING REQUIREMENTS" IS NOT NULL AND "COMPETITION" = [PROCEDURES] */
			normal_rule REGEXP 'ASK [[:print:]]* "[[:print:]]*" IS COMPLETED (AND|OR) [[:print:]]* (SELECTS|SELECTED) "[[:print:]]*" (IS SELECTED UNDER|TO|TO THE QUESTION|WITHIN|UNDER|FOR) "[[:print:]]*"'
            then 
				concat('[',
					substring_index(substring_index(normal_rule,'"',2),'"',-1),
					'] IS NOT NULL ',
                    substring_index(substring_index(normal_rule,'" IS COMPLETED ',-1),' ',1),
                    ' "',
					substring_index(substring_index(normal_rule,'"',4),'"',-1),
					'" = [',
                    substring_index(substring_index(normal_rule,'"',6),'"',-1),
                    ']')
		when 
			/* ASK THE QUESTION ONLY AFTER THE USER SELECTS EITHER "MAJOR DEFENSE PROGRAM" 
				OR "MAJOR INFORMATION SYSTEMS PROGRAM" 
                ABOVE TO THE QUESTION "TYPE OF SYSTEM?"
						------>  	
				"TYPE OF SYSTEM".ANSWER = "MAJOR DEFENSE PROGRAM" 
                OR
                "TYPE OF SYSTEM".ANSWER = "MAJOR INFORMATION SYSTEMS PROGRAM" 
                
			*/
            normal_rule REGEXP 'ASK [[:print:]]* "[[:print:]]*" OR "[[:print:]]*" OR "[[:print:]]*" (TO|TO THE QUESTION|WITHIN|UNDER|FOR) "[[:print:]]*"\.*'
            then 
				concat(
					'"',
					substring_index(substring_index(normal_rule,'"',2),'"',-1),
					'" = [',
                    substring_index(substring_index(normal_rule,'"',8),'"',-1),
                    '] OR "',
                    substring_index(substring_index(normal_rule,'"',4),'"',-1),
					'" = [',
                    substring_index(substring_index(normal_rule,'"',8),'"',-1),
                    '] OR "',
                    substring_index(substring_index(normal_rule,'"',6),'"',-1),
					'" = [',
                    substring_index(substring_index(normal_rule,'"',8),'"',-1),
                    ']')
			/*
                ASK ONLY IF THE USER SELECTS "RESEARCH AND DEVELOPMENT" UNDER "SERVICES" AND "MAJOR DEFENSE PROGRAM" UNDER "ACQUISITION PLANNING"
                
						----->
                        
				"RESEARCH AND DEVELOPMENT" = [MAJOR DEFENSE PROGRAM]
					AND 
				"SERVICES" = [MAJOR DEFENSE PROGRAM]
			*/
		when normal_rule REGEXP 
				'ASK [[:print:]]* "[[:print:]]*" (IS SELECTED UNDER|TO|TO THE QUESTION|WITHIN|UNDER|FOR) "[[:print:]]*" (AND|OR) "[[:print:]]*" (IS SELECTED UNDER|TO|TO THE QUESTION|WITHIN|UNDER|FOR) "[[:print:]]*"\.*'
            then 
				concat(
					'"',
                    substring_index(substring_index(normal_rule,'"',2),'"',-1),
					'" = [',
                    substring_index(substring_index(normal_rule,'"',4),'"',-1),']',
                    substring_index(substring_index(normal_rule,'"',5),'"',-1),'"',
                    substring_index(substring_index(normal_rule,'"',6),'"',-1),
					'" = [',
                    substring_index(substring_index(normal_rule,'"',8),'"',-1),
                    ']')
		when 
			/* ASK THE QUESTION ONLY AFTER THE USER SELECTS EITHER "MAJOR DEFENSE PROGRAM" 
				OR "MAJOR INFORMATION SYSTEMS PROGRAM" 
                ABOVE TO THE QUESTION "TYPE OF SYSTEM?"
						------>  	
				"TYPE OF SYSTEM" = [MAJOR DEFENSE PROGRAM]
                OR
                "TYPE OF SYSTEM" = [MAJOR INFORMATION SYSTEMS PROGRAM]
                
			*/
            normal_rule REGEXP 'ASK [[:print:]]* "[[:print:]]*" OR "[[:print:]]*" (TO|TO THE QUESTION|WITHIN|UNDER|FOR) "[[:print:]]*"\.*'
            then 
				concat(
					'"',
                    substring_index(substring_index(normal_rule,'"',2),'"',-1),
					'" = [',
                    substring_index(substring_index(normal_rule,'"',6),'"',-1),
                    '] OR "',
                    substring_index(substring_index(normal_rule,'"',4),'"',-1),
					'" = [',
                    substring_index(substring_index(normal_rule,'"',6),'"',-1),
                    ']')
		when normal_rule REGEXP 
				'ASK [[:print:]]* "[[:print:]]*" (TO|TO THE QUESTION|WITHIN|UNDER|FOR) "[[:print:]]*"$'
            then 
				concat('"',
					substring_index(substring_index(normal_rule,'"',2),'"',-1),
					'" = [',
                    substring_index(substring_index(normal_rule,'"',4),'"',-1),
                    ']')
		when 
			(
            normal_rule like 'ASK% AFTER %USER ANSWERS THE QUESTION "%'
			or normal_rule like 'ASK AFTER USER SELECT% THE "%"'
            or normal_rule like 'ASK % "%" %COMPLETED'
            or normal_rule like 'ASK % "%" %COMPLETE'
            or normal_rule like 'ASK % ANSWERED FOR "%"'
            or normal_rule like 'ASK % "%" % SELECTED'
            or normal_rule like 'ASK % "%" %HAS BEEN ANSWERED'
			)
            then 
				concat(
					'[', substring_index(substring_index(rule,'"',2),'"',-1),
                    '] IS NOT NULL'
                    )
		when 
			/* ASK THE FOLLOWING QUESTION ONLY IF "SOLICITATION" IS SELECTED UNDER "DOCUMENT TYPE".
            			------>  	
				"DOCUMENT TYPE" = [SOLICITATION] */
			normal_rule REGEXP 'ASK [[:print:]]* "[[:print:]]*" (IS SELECTED UNDER|TO|TO THE QUESTION|WITHIN|UNDER|FOR) "[[:print:]]*"\.*'
            then 
				concat('"',
					substring_index(substring_index(normal_rule,'"',2),'"',-1),
					'" = [',
                    substring_index(substring_index(normal_rule,'"',4),'"',-1),
                    ']')
		
    end
where parsed_rule is null;

-- refresh question dependencies tables
delete from temp_question_dependencies;
CALL normalize_question_dependencies();

-- Map referenced question names with question ids
update temp_question_dependencies d
set referenced_question_id = ( select id from temp_questions where question_name = d.referenced_question_name );


-- Set parent id to the latest dependent question as it appears in spreadsheet
update temp_questions q
set q.latest_parent_question_id = 
	(
    select sub.latest_parent_question_id
    from 
		(
        select 
			sub.question_id question_id, 
			latest_q.id latest_parent_question_id
        from
			(
			select d.question_id, max(referenced_q.raw_id) latest_parent_question_id
			from temp_question_dependencies d
			join temp_questions referenced_q
				on referenced_q.id = d.referenced_question_id
			group by d.question_id
			) sub
			join temp_questions latest_q
				on sub.latest_parent_question_id = latest_q.raw_id
		) sub
	where sub.question_id = q.id
	);



--
    
update temp_questions set root_question_id = id;
update temp_questions set root_question_id = latest_parent_question_id where latest_parent_question_id is not null;

/* Layer 1 */
update temp_questions
set root_question_id = 
	( 
    select sub.latest_parent_question_id
    from 
		( 
        select * from temp_questions 
        ) sub
    where sub.id = temp_questions.root_question_id 
    )
where exists
	( 
    select sub.latest_parent_question_id
    from 
		( 
        select * from temp_questions where latest_parent_question_id is not null
        ) sub
    where sub.id = temp_questions.root_question_id 
    );


/* Layer 2 */
update temp_questions
set root_question_id = 
	( 
    select sub.latest_parent_question_id
    from 
		( 
        select * from temp_questions 
        ) sub
    where sub.id = temp_questions.root_question_id 
    )
where exists
	( 
    select sub.latest_parent_question_id
    from 
		( 
        select * from temp_questions where latest_parent_question_id is not null
        ) sub
    where sub.id = temp_questions.root_question_id 
    );
    

/* Layer 3 */
update temp_questions
set root_question_id = 
	( 
    select sub.latest_parent_question_id
    from 
		( 
        select * from temp_questions 
        ) sub
    where sub.id = temp_questions.root_question_id 
    )
where exists
	( 
    select sub.latest_parent_question_id
    from 
		( 
        select * from temp_questions where latest_parent_question_id is not null
        ) sub
    where sub.id = temp_questions.root_question_id 
    );



--

/* For child waterfall questions, take root baseline question type as default */
update temp_questions q
join temp_questions parent
	on q.root_question_id = parent.id
set q.question_type = parent.question_type
where q.category = 'Waterfall' and q.question_type is null;


