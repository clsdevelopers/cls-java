
-- Clauses in FAR/DFARs spreadsheet not in ECFR
select clause_number, clause  -- clause_number, r.*
from raw_clauses r
where not exists 
	( 
    select 1 
    from 
    (select clause_number, contentHTML, contentHTMLFillins, contentHTMLProcessed 
	from temp_official_clauses
	union all
	select concat(clause_number, ' ', alt_number) clause_number, contentHTML, contentHTMLFillins, contentHTMLProcessed 
	from temp_official_clauses_alt) as sub
    where sub.clause_number = r.clause_number
    )
    and clause != '' 
    and prescription_text !='';
    /* missing from ECFR 
    and r.clause_number not in
		(
        '252.204-0001','252.204-0002','252.204-0003','252.204-0004','252.204-0005',
        '252.204-0006','252.204-0007','252.204-0008','252.204-0009','252.204-0010',
        '252.204-0011','252.209-7993','252.209-7994','252.209-7995','252.209-7996',
        '252.209-7997','252.209-7998','252.209-7999','252.225-7985','252.225-7991',
		'252.225-7987','252.225-7988','252.225-7989','252.225-7990','252.225-7992',
        '252.225-7993','252.225-7993','252.225-7994','252.225-7996','252.225-7997',
        '252.225-7998','252.225-7999','252.229-7998','252.229-7999'
        );
    */

-- FAR/DFARs prescriptions not linked to questions
select distinct p.prescription
from raw_clauses r
join clause_prescriptions_xref p
	on r.id = p.raw_id
where not exists
	(
    select 1
    from answers_prescriptions_xref
    where trim(prescription) = trim(p.prescription)
    );
    
-- Question prescriptions not in FAR/DFARs spreadsheet
select distinct prescription
from answers_prescriptions_xref a
where not exists
	(
    select 1
    from raw_clauses r
    join clause_prescriptions_xref p
		on r.id = p.raw_id
    where trim(a.prescription) = trim(p.prescription)
    );
    
--


select 
	r.clause, r.CLAUSE_PRESCRIPTION,
	r.conditions,  c.item_id, question_name, value
from temp_clause_conditions c
join raw_clauses r
	on r.id = c.raw_id
where c.real_question_name is null 	
	and c.value is not null
    and not
		(
        c.question_name regexp '([0-9]+)\\.+'
		or c.question_name in ('[CONTRACT VALUE]', '[PERIOD OF PERFORMANCE]')
        );

--

-- Clause "included" conditions
select r.clause, r.CLAUSE_PRESCRIPTION,
	r.conditions,  c.item_id, question_name, value
from temp_clause_conditions c
join raw_clauses r
	on r.id = c.raw_id
where c.real_question_name is null 	
	and c.value is not null
    and 
		(
        c.question_name regexp '([0-9]+)\\.+'
		)
	and not exists
		(
        select 1 from raw_clauses where replace(replace(c.question_name,'[',''),']','') = clause_number
        );
        
-- Questions Validation

select q.question_name, q.id, choice_text
from temp_questions q
join temp_possible_answers a
	on a.question_id = q.id
group by q.question_name, q.id, choice_text
having count(1)>1;

select * from temp_questions where parsed_rule is null and rule is not null;

select * from temp_questions where rule is null and category in ('Waterfall','Follow-on');

select * from temp_questions where question_type is null;

select * from temp_questions where sequence is null;

select * from temp_possible_answers where choice_text is null or choice_text = '';

SELECT 
    question_name,
    parsed_rule
FROM temp_questions q
where parsed_rule is not null
	and root_question_id is null;

select *
from temp_questions q
where not exists ( select 1 from temp_possible_answers where question_id = q.id );
    
--
select 
	distinct r.clause, r.CLAUSE_PRESCRIPTION,
	r.conditions,  c.item_id, question_name, value
from temp_clause_conditions c
join raw_clauses r
	on r.id = c.raw_id
where c.real_question_name is null 	
	and c.value is not null
    and not
		(
        c.question_name regexp '([0-9]+)\\.+'
		or c.question_name in ('[CONTRACT VALUE]', '[PERIOD OF PERFORMANCE]')
        );
--

select 
	question_name, value, count(1)
from temp_clause_conditions c
join raw_clauses r
	on r.id = c.raw_id
where c.real_question_name is null 	
	and c.value is not null
    and not
		(
        c.question_name regexp '([0-9]+)\\.+'
		or c.question_name in ('[CONTRACT VALUE]', '[PERIOD OF PERFORMANCE]')
        )
group by question_name, value;


--


select c.*, a.choice_text, q.question_name
from temp_clause_conditions c
join temp_possible_answers a
	on soundex_hash = soundex(c.value)
join temp_questions q
	on a.question_id = q.id
where c.value is not null 
	and c.real_question_name is null 
    and c.question_name not regexp '^\\[[0-9]+' 
    and c.value not in ('NO', 'YES')
    and 
		(
        select count(1)
        from temp_possible_answers
        where soundex_hash = soundex(c.value)
        ) = 1;
        
        
--

-- Version Diff


create table cls3.Question_Conditions_bk as select * from cls3.Question_Conditions;
create table cls3.Question_Choice_Prescriptions_bk as select *  from cls3.Question_Choice_Prescriptions;
create table cls3.Question_Choices_bk as select *  from cls3.Question_Choices;
create table cls3.Questions_bk as select *  from cls3.Questions;

create table cls3.clause_prescriptions_bk as select *  from cls3.clause_prescriptions;
create table cls3.prescriptions_bk as select *  from cls3.prescriptions;
create table cls3.clause_fill_ins_bk as select *  from cls3.clause_fill_ins;
create table cls3.clauses_bk as select *  from cls3.clauses;


select * 
from cls3.Questions q
where not exists 
	( 
    select 1 
    from cls3.Questions_bk hist
    where q.question_code = hist.question_code
	);



select * 
from cls3.Questions curr 
join cls3.Questions_bk hist
	on curr.question_code = hist.question_code
    and curr.question_text != hist.question_text;
    

select * 
from cls3.Questions q
join cls3.Question_Choices c
	on q.question_id = c.question_id
where not exists
	(
    select 1 
	from cls3.Questions_bk q2
	join cls3.Question_Choices_bk c2
		on q2.question_id = c2.question_id
	where q.question_code = q2.question_code
		and c.choice_text = c2.choice_text
    );
    
    select count(*) from cls3.Questions;

    
select * 
from cls3.Questions_bk q
join cls3.Question_Choices_bk c
	on q.question_id = c.question_id
where not exists
	(
    select 1 
	from cls3.Questions q2
	join cls3.Question_Choices c2
		on q2.question_id = c2.question_id
	where q.question_code = q2.question_code
		and c.choice_text = c2.choice_text
    );
    
--

    