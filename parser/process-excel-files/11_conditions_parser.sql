/*
	This script normalizes the clause conditions for correction/validation
*/

drop procedure if exists normalize_conditions;

DELIMITER $$
 
CREATE PROCEDURE normalize_conditions ()
BEGIN
 
DECLARE v_finished INTEGER DEFAULT 0;
DECLARE v_id INTEGER DEFAULT 0;
DECLARE v_conditions varchar(10000) DEFAULT ""; 
DECLARE INDX INT;
DECLARE SLICE nvarchar(4000); 

DECLARE cond_cursor CURSOR FOR
select id, conditions
from raw_clauses 
where conditions != ''; 
 
-- declare NOT FOUND handler
DECLARE CONTINUE HANDLER
        FOR NOT FOUND SET v_finished = 1;
 
OPEN cond_cursor;
 
get_condition: LOOP
    SET INDX = 1;
	FETCH cond_cursor INTO v_id, v_conditions;
    
	MyLoop: WHILE INDX != 0 DO
		 -- GET THE INDEX OF THE FIRST OCCURENCE OF THE SPLIT CHARACTER
		 SET INDX = LOCATE('\\n',v_conditions);
		 -- NOW PUSH EVERYTHING TO THE LEFT OF IT INTO THE SLICE VARIABLE
		 IF INDX !=0 THEN
		  SET SLICE = LEFT(v_conditions,INDX - 1);
          -- SET SLICE = concat('(',SLICE);
		 ELSE
		  SET SLICE = v_conditions;
		 END IF;
		 -- PUT THE ITEM INTO THE RESULTS SET
		 INSERT INTO temp_clause_conditions(raw_id, condition_text) VALUES(v_id, SLICE);
         
		 -- CHOP THE ITEM REMOVED OFF THE MAIN STRING
		 SET v_conditions = RIGHT(v_conditions,CHAR_LENGTH(v_conditions) - INDX-1);
		 -- BREAK OUT IF WE ARE DONE
		 IF LENGTH(v_conditions) = 0 THEN
		  LEAVE MyLoop;
		 END IF;
	END WHILE;
    
	
	IF v_finished = 1 THEN
		LEAVE get_condition;
	END IF;
    
     
END LOOP get_condition;
 
CLOSE cond_cursor;
 
END$$
 
DELIMITER ;

truncate table temp_clause_conditions;

CALL normalize_conditions();



update temp_clause_conditions 
set condition_text_processed = condition_text;

update temp_clause_conditions 
set condition_text_processed = replace(condition_text_processed, ' )P', ') P');

update temp_clause_conditions 
set condition_text_processed = replace(condition_text_processed, '(C()', '(C)');

update temp_clause_conditions 
set condition_text_processed = replace(condition_text_processed, ' ) ', ') ');

update temp_clause_conditions 
set item_id = concat(substring_index(condition_text_processed,')', 1),')')
where locate(')', condition_text_processed) != 0;

--

update temp_clause_conditions 
set condition_text_processed = replace(condition_text_processed,' IS ',' IS: ')
where condition_text like '% IS %' and condition_text not like '%:%' and condition_text not like '%INCLUDED%';


-- Conditions with clauses

update temp_clause_conditions 
set condition_text_processed = concat(left(condition_text, length(condition_text)-length(' IS INCLUDED')), ' IS: INCLUDED')
where condition_text regexp ' IS INCLUDED$' 
	and not condition_text regexp ': IS INCLUDED$'
    and raw_id not in ( select id from raw_clauses where clause_number in ('252.217-7001') );

update temp_clause_conditions 
set condition_text_processed = concat(left(condition_text, length(condition_text)-length(' : IS INCLUDED')), ' IS: INCLUDED')
where condition_text regexp ' : IS INCLUDED$';

update temp_clause_conditions 
set condition_text_processed = concat(left(condition_text, length(condition_text)-length(': IS INCLUDED')), ' IS: INCLUDED')
where condition_text regexp ': IS INCLUDED$';      

update temp_clause_conditions 
set condition_text_processed = concat(left(condition_text, length(condition_text)-length(' IS NOT INCLUDED')), ' IS: NOT INCLUDED')
where condition_text regexp ' IS NOT INCLUDED$' 
	and not condition_text regexp ': IS NOT INCLUDED$';
    
update temp_clause_conditions 
set condition_text_processed = replace(condition_text,'ALTERNATE',' ALTERNATE')
where condition_text regexp '[0-9]ALTERNATE';
   

update temp_clause_conditions set condition_text_processed = replace(condition_text_processed,'\\n','');

update temp_clause_conditions set condition_text_processed = replace(condition_text_processed,'T YPE','TYPE');
update temp_clause_conditions set condition_text_processed = replace(condition_text_processed,'  ',' ');
update temp_clause_conditions set condition_text_processed = replace(condition_text_processed,'GREATER THAN THE SAT','GREATER THAN SAT');
update temp_clause_conditions set condition_text_processed = replace(condition_text_processed,'GREATER THAT SAT','GREATER THAN SAT');

update temp_clause_conditions set condition_text_processed = replace(condition_text_processed, '::',':');
update temp_clause_conditions set condition_text_processed = replace(condition_text_processed, 'DOCUMENT TYPE IS AWARD','DOCUMENT TYPE IS: AWARD');

update temp_clause_conditions set condition_text_processed = replace(condition_text_processed, 'SOLICIATION','SOLICITATION');
update temp_clause_conditions set condition_text_processed = replace(condition_text_processed, 'SOLISITATION','SOLICITATION');
update temp_clause_conditions set condition_text_processed = replace(condition_text_processed, 'SOLCITATION','SOLICITATION');
update temp_clause_conditions set condition_text_processed = replace(condition_text_processed, 'SOLIITATION','SOLICITATION');


update temp_clause_conditions set condition_text_processed = replace(condition_text_processed, '(E) FUNDING FISCAL YEAR IS: FY2012 FUNDS','(E)FUNDING FISCAL YEAR IS: FY2012 FUNDS');

update temp_clause_conditions set condition_text_processed = replace(condition_text_processed, 'IS: DEFENSE','IS: DEPARTMENT OF DEFENSE') where condition_text_processed not like '%DEFENSE ADVANCED RESEARCH PROJECTS AGENCY';
update temp_clause_conditions set condition_text_processed = replace(condition_text_processed, 'IS: DEFENSE','IS: DEPARTMENT OF DEFENSE') where condition_text_processed not like '%DEFENSE ADVANCED RESEARCH PROJECTS AGENCY';


update temp_clause_conditions set condition_text_processed = replace(condition_text_processed, 'INVITATION OF BIDS','INVITATION FOR BIDS');
update temp_clause_conditions set condition_text_processed = replace(condition_text_processed, 'INVITATION OF BIDS','INVITATION FOR BIDS');

update temp_clause_conditions set condition_text_processed = replace(condition_text_processed, 'SET-ASIDED','SET-ASIDE');
update temp_clause_conditions set condition_text_processed = replace(condition_text_processed, 'NEGOTIATIONS (FAR PART 15)','NEGOTIATION (FAR PART 15)');
update temp_clause_conditions set condition_text_processed = replace(condition_text_processed, 'DEVELOPEMENT','DEVELOPMENT');
update temp_clause_conditions set condition_text_processed = replace(condition_text_processed, 'NORTHERN MARIANA ISLANDS','NORTHERN MARINA ISLANDS');

update temp_clause_conditions set condition_text_processed = replace(condition_text_processed, 'ARCHITECT & ENGINEERING','ARCHITECT-ENGINEERING');


update temp_clause_conditions set condition_text_processed = '(B) DOCUMENT TYPE IS: AWARD' where condition_text = '(B) DOCUMENT TYPE IS: AWAR';
update temp_clause_conditions set condition_text_processed = '(D) PROCEDURES IS: NOT COMMERCIAL PROCEDURES (FAR PART 12)' where condition_text = '(D PROCEDURES IS: NOT COMMERCIAL PROCEDURES (FAR PART 12)';

update temp_clause_conditions set condition_text_processed = replace(condition_text_processed, '(FAR PART 15) (FAR PART 15)','(FAR PART 15)');

update temp_clause_conditions set question_name = '[FUNDING FY]' where question_name = '[FUNDING FISCAL YEAR]';

-- Kyle's Additions

update temp_clause_conditions set condition_text_processed = '(F) BUSINESS TYPE IS: NON-PROFIT ORGANIZATION' where condition_text = '(F) BUSINESS TYPE IS: NONPROFIT ORGANIZATION';
update temp_clause_conditions set condition_text_processed = '(J) BUSINESS TYPE IS: NON-PROFIT ORGANIZATION' where condition_text = '(J) BUSINESS TYPE IS: NONPROFIT ORGANIZATION';
update temp_clause_conditions set condition_text_processed = '(D) BUSINESS TYPE IS: NON-PROFIT ORGANIZATION' where condition_text = '(D) BUSINESS TYPE IS: NONPROFIT ORGANIZATION';
update temp_clause_conditions set condition_text_processed = '(H) BUSINESS TYPE IS: NON-PROFIT ORGANIZATION' where condition_text = '(H) BUSINESS TYPE IS: NONPROFIT ORGANITATION';
update temp_clause_conditions set condition_text_processed = '(E) BUSINESS TYPE IS: HISTORICALLY UNDERUTILIZED BUSINESS ZONE (HUBZone)' where condition_text = '(E) BUSINESS TYPE IS: HISTORICALLY UNDERUTILIZED BUSINESS ZONE ';
update temp_clause_conditions set condition_text_processed = '(C) BUSINESS TYPE IS: FOREIGN SOURCE' where condition_text = '(C) BUSINESS TYPE IS: FOREIGN';
update temp_clause_conditions set condition_text_processed = '(D) CONTRACT TYPE IS: TIME AND MATERIALS' where condition_text = '(D) CONTRACT TYPE IS: TIME AND MATERIAL';
update temp_clause_conditions set condition_text_processed = '(I) CONTRACT TYPE IS: TIME AND MATERIALS' where condition_text = '(I) CONTRACT TYPE IS: TIME AND MATERIAL';
update temp_clause_conditions set condition_text_processed = '(B) DOCUMENT TYPE IS: INVITATION FOR BIDS' where condition_text = '(B) DOCUMENT TYPE IS: INVITATION FOR BID';
update temp_clause_conditions set condition_text_processed = '(J) CONTRACT TYPE IS: FIXED PRICE WITH ECONOMIC PRICE ADJUSTMENT -- ACTUAL COSTS' where condition_text = '(J) CONTRACT TYPE IS: FIXED PRICE WITH ECONOMIC PRICE ADJUSTMENT-ACTUAL COSTS';
update temp_clause_conditions set condition_text_processed = '(F) CONTRACT TYPE IS: INDEFINITE DELIVERY CONTRACT' where condition_text = '(F) CONTRACT TYPE IS: INDEFINITE DELIVERY';
update temp_clause_conditions set condition_text_processed = '(H) MULTIPLE OR MULTI YEAR IS: MULTIPLE YEAR CONTRACT' where condition_text = '(H) MULTIPLE OR MULTI YEAR IS: MULTIPLE YEAR';
update temp_clause_conditions set condition_text_processed = '(I) MULTIPLE OR MULTI YEAR IS: NOT MULTIPLE YEAR CONTRACT' where condition_text = '(I) MULTIPLE OR MULTI YEAR IS: NOT MULTIPLE YEAR';
update temp_clause_conditions set condition_text_processed = '(G) MULTIPLE OR MULTI YEAR IS: MULTIPLE YEAR CONTRACT' where condition_text = '(G) MULTIPLE OR MULTI YEAR: IS MULTIPLE';
update temp_clause_conditions set condition_text_processed = '(C) MULTIPLE OR MULTI YEAR IS: MULTI-YEAR CONTRACT' where condition_text = '(C) MULTIPLE OR MULTI-YEAR IS: MULTI-YEAR';
update temp_clause_conditions set condition_text_processed = '(B) PERFORMANCE REQUIREMENTS IS: NOT LISTED IN THE DOD ACQUISITION STREAMLINING AND STANDARDIZATION INFORMATION SYSTEM (ASSIST) NOT FURNISHED WITH THE SOLICITATION' where condition_text = '(B) PERFORMANCE REQUIREMENTS IS: NOT LISTED IN THE DOD ACQUISITION STREAMLINING AND STANDARDIZATION INFORMATION SYSTEM (ASSIST)';
update temp_clause_conditions set condition_text_processed = '(F) PLACE TYPE IS: GOVERNMENT OWNED OR LEASED FACILITIES/INSTALLATIONS' where condition_text = '(F) PLACE OF PERFORMANCE GOVERNMENT OWNED OR LEASED FACILITIES/INSTALLATIONS';
update temp_clause_conditions set condition_text_processed = '(E) PLACE OF USE IS: FOR USE INSIDE THE UNITED STATES (50 STATES, DISTRICT OF COLUMBIA, OR UNITED STATES OUTLYING AREAS)' where condition_text = '(E) PLACE OF USE IS: FOR USE INSIDE THE UNITED STATES';



update temp_clause_conditions set condition_text_processed = 'CONTRACT TYPE IS: COST NO FEE' where condition_text like '%CONTRACT TYPE IS: COST';
update temp_clause_conditions set condition_text_processed = 'DOCUMENT TYPE IS: AWARD' where condition_text like '%DOCUMENT TYPE IS: AWAR';

update temp_clause_conditions set condition_text_processed = replace(condition_text_processed,'LABOR-HOUR','LABOR HOUR');
update temp_clause_conditions set condition_text_processed = replace(condition_text_processed,'FIXED PRICE INCENTIVE (SUCCESSIVE TARGET)','FIXED PRICE INCENTIVE (SUCCESSIVE TARGETS)');
update temp_clause_conditions set condition_text_processed = replace(condition_text_processed,'FIXED PRICE RE-DERMINATION PROSPECTIVE','FIXED PRICE RE-DETERMINATION PROSPECTIVE');
update temp_clause_conditions set condition_text_processed = replace(condition_text_processed,'FIXED PRICE RE-DERMINATION PROSPECTIVE','FIXED PRICE RE-DETERMINATION PROSPECTIVE');
update temp_clause_conditions set condition_text_processed = replace(condition_text_processed,'(A: DOCUMENT TYPE IS: SOLICITATION','(A) DOCUMENT TYPE IS: SOLICITATION');
update temp_clause_conditions set condition_text_processed = replace(condition_text_processed,'(C)EXCEPTIONS','(C) EXCEPTIONS');
update temp_clause_conditions set condition_text_processed = replace(condition_text_processed,'(C)SERVICES','(C) SERVICES');
update temp_clause_conditions set condition_text_processed = replace(condition_text_processed,'(D)PLACE OF PERFORMANCE','(D) PLACE OF PERFORMANCE');
update temp_clause_conditions set condition_text_processed = replace(condition_text_processed,'(D)SECURITY REQUIREMENTS','(D) SECURITY REQUIREMENTS');
update temp_clause_conditions set condition_text_processed = replace(condition_text_processed,'(E)FUNDING FISCAL YEAR]','(E) FUNDING FISCAL YEAR');
update temp_clause_conditions set condition_text_processed = replace(condition_text_processed,'AWARD SCENARIO\'S','AWARD SCENARIOS');
update temp_clause_conditions set condition_text_processed = replace(condition_text_processed,'AWARD AFTER DISCUSSIONS WITH CONTRACTORS IN THE COMPETITIVE RANGE','AWARD AFTER DISCUSSIONS WITH OFFERORS IN THE COMPETITIVE RANGE');
update temp_clause_conditions set condition_text_processed = replace(condition_text_processed,'BID GUARANTEE OR BOND IS:PAYMENT BONDS APPLY','BID GUARANTEE OR BOND IS: PAYMENT BONDS APPLY');
update temp_clause_conditions set condition_text_processed = replace(condition_text_processed,'BID GUARANTEE OR BOND IS:PERFORMANCE BONDS APPLY','BID GUARANTEE OR BOND IS: PERFORMANCE BONDS APPLY');
update temp_clause_conditions set condition_text_processed = replace(condition_text_processed,'OGVERNMENTAL','GOVERNMENTAL');
update temp_clause_conditions set condition_text_processed = replace(condition_text_processed,'DELIVERD','DELIVERED');
update temp_clause_conditions set condition_text_processed = replace(condition_text_processed,'BUSINESS ORGANIZATION IS: STATE GOVERNMENT','BUSINESS TYPE IS: STATE GOVERNMENT');
update temp_clause_conditions set condition_text_processed = replace(condition_text_processed,'BUSINESS ORGANIZATION IS: LOCAL GOVERNMENT','BUSINESS TYPE IS: LOCAL GOVERNMENT');
update temp_clause_conditions set condition_text_processed = replace(condition_text_processed,'UNDEROMB','UNDER OMB');
update temp_clause_conditions set condition_text_processed = replace(condition_text_processed,'BUSINESS TYPE IS: NONPROFIT ORGANIZATION','BUSINESS TYPE IS: NON-PROFIT ORGANIZATION');
update temp_clause_conditions set condition_text_processed = replace(condition_text_processed,'FOREIGN CORPORATION WHOLLY OWNED BY FOREIGN GOVERNMENTS','FOREIGN CORPORATION WHOLLY OWNED BY A FOREIGN GOVERNMENT');
update temp_clause_conditions set condition_text_processed = replace(condition_text_processed,'REPRESENTATIVE OF FOREIGN GOVERNMENT','REPRESENTATIVE OF A FOREIGN GOVERNMENT');
update temp_clause_conditions set condition_text_processed = replace(condition_text_processed,'COMMERCIAL ITEMS IS:NOT THE SUPPLIES ARE COMMERCIAL ITEMS','COMMERCIAL ITEMS IS: NOT THE SUPPLIES ARE COMMERCIAL ITEMS');
update temp_clause_conditions set condition_text_processed = replace(condition_text_processed,'(U) COMMERCIAL ITEMS IS: THE SUPPLIES ARE COMMERCIAL ITEM','(U) COMMERCIAL ITEMS IS: THE SUPPLIES ARE COMMERCIAL ITEMS');
update temp_clause_conditions set condition_text_processed = replace(condition_text_processed,'COMMERCIAL SERVICES IS:NO','COMMERCIAL SERVICES IS: NO');
update temp_clause_conditions set condition_text_processed = replace(condition_text_processed,'CONSTRACT TYPE','CONTRACT TYPE');
update temp_clause_conditions set condition_text_processed = replace(condition_text_processed,'(E)FUNDING FISCAL YEAR','(E) FUNDING FISCAL YEAR');
update temp_clause_conditions set condition_text_processed = replace(condition_text_processed,'CONTRACT TYPE IS: LETTER CONTRACT','AWARD TYPE IS: LETTER CONTRACT');
update temp_clause_conditions set condition_text_processed = replace(condition_text_processed,'LABOR HOURS','LABOR HOUR');
update temp_clause_conditions set condition_text_processed = replace(condition_text_processed,'CONTRACT VALUE IS:100000000','CONTRACT VALUE IS: 100000000');
update temp_clause_conditions set condition_text_processed = replace(condition_text_processed,'VALUE: IS','VALUE IS:');
update temp_clause_conditions set condition_text_processed = replace(condition_text_processed,'IS:YES','IS: YES');
update temp_clause_conditions set condition_text_processed = replace(condition_text_processed,'FIXED PRICE WITH ECONOMIC PRICE ADJUSTMENT --COST INDICIES','FIXED PRICE WITH ECONOMIC PRICE ADJUSTMENT -- COST INDICIES');
update temp_clause_conditions set condition_text_processed = replace(condition_text_processed,'IS : AWARD','IS: AWARD');
update temp_clause_conditions set condition_text_processed = replace(condition_text_processed,'IS:SOLICITATION','IS: SOLICITATION');
update temp_clause_conditions set condition_text_processed = replace(condition_text_processed,'CONTRACT PRICE IS:','CONTRACT TYPE IS:');
update temp_clause_conditions set condition_text_processed = replace(condition_text_processed,'DEILIVERY BASIS','DELIVERY BASIS');
update temp_clause_conditions set condition_text_processed = replace(condition_text_processed,'INFORMATION IS SUBMITTED TO OTHER THAN THEPAYMENT OFFICE','INFORMATION IS SUBMITTED TO OTHER THAN THE PAYMENT OFFICE');
update temp_clause_conditions set condition_text_processed = replace(condition_text_processed,'TOEVALUATE','TO EVALUATE');
update temp_clause_conditions set condition_text_processed = replace(condition_text_processed,'(R) EXCEPTIONS IS: 5.202 -- EXCEPTION TO THE BUY AMERICAN STATUTE','(R) EXCEPTIONS IS: 25.202 -- EXCEPTION TO THE BUY AMERICAN STATUTE');
update temp_clause_conditions set condition_text_processed = replace(condition_text_processed,'223.7102 -- NOT EXCEPTION TO 223.7102 (STORAGE AND DISPOSAL OF TOXIC AND HAZARDOUS MATERIALS)','NOT 223.7102 -- EXCEPTION TO 223.7102 (STORAGE AND DISPOSAL OF TOXIC AND HAZARDOUS MATERIALS)');
-- update temp_clause_conditions set condition_text_processed = replace(condition_text_processed,'EXCESSIVE PASS-THROUGH CHARGES','EXCESS PASS-TRHOUGH CHARGES');
update temp_clause_conditions set condition_text_processed = replace(condition_text_processed,'22.1003-3/4 -- NOT EXEMPT FROM APPLICATION OF THE SERVICE CONTRACT ACT','NOT 22.1003-3/4 -- EXEMPT FROM APPLICATION OF THE SERVICE CONTRACT ACT');
update temp_clause_conditions set condition_text_processed = replace(condition_text_processed,'22.1203-2 EXEMPT FROM 52.222-17, "NONDISPLACEMENT OF QUALIFIED WORKERS"','22.1203-2 -- EXEMPT FROM 52.222-17, "NONDISPLACEMENT OF QUALIFIED WORKERS"');
update temp_clause_conditions set condition_text_processed = replace(condition_text_processed,'22.1603 -- SECRETARIAL EXEMPTION TO THE  REQUIREMENT TO INCLUDE THE CLAUSE 52.222-40,  "NOTIFICATION OF EMPLOYEE RIGHTS UNDER THE NATIONAL LABOR RELATIONS ACT"', '22.1603 -- SECRETARIAL EXEMPTION TO THE REQUIREMENT TO INCLUDE THE CLAUSE 52.222-40, "NOTIFICATION OF EMPLOYEE RIGHTS UNDER THE NATIONAL LABOR RELATIONS ACT');
update temp_clause_conditions set condition_text_processed = replace(condition_text_processed,'23.204 -- ENERGY STAR® OR FEMP-DESIGNATED PRODUCT EXEMPTION','23.204 -- ENERGY STAR OR FEMP-DESIGNATED PRODUCT EXEMPTION');
update temp_clause_conditions set condition_text_processed = replace(condition_text_processed,'F.O.B. ORIGIN DESIGNATIONS','FOB ORIGIN DESIGNATIONS');
update temp_clause_conditions set condition_text_processed = replace(condition_text_processed,'(C) F.O.B. ORIGIN FREIGHT PREPAID','(C) FOB ORIGIN DESIGNATIONS IS: F.O.B. ORIGIN FREIGHT PREPAID');
update temp_clause_conditions set condition_text_processed = replace(condition_text_processed,'(D) FINANCING ARRANGEMENTS IS: ADVANCED PAYMENTS','(D) FINANCING ARRANGEMENTS IS: ADVANCE PAYMENTS');
update temp_clause_conditions set condition_text_processed = replace(condition_text_processed,'FIRST ARTICLE CONDITIONS IS:contractor IS AUTHORIZED TO PURCHASE MATERIAL OR COMMENCE PRODUCTION BEFORE FIRST ARTICLE APPROVAL','FIRST ARTICLE CONDITIONS IS: CONTRACTOR IS AUTHORIZED TO PURCHASE MATERIAL OR COMMENCE PRODUCTION BEFORE FIRST ARTICLE APPROVAL');
update temp_clause_conditions set condition_text_processed = replace(condition_text_processed,'TESTING IS REQUIRED.','TESTING IS REQUIRED');
update temp_clause_conditions set condition_text_processed = replace(condition_text_processed,'FIXED PRICE INCENTIVE (FIRM TARGETS)','FIXED PRICE INCENTIVE (FIRM TARGET)');
update temp_clause_conditions set condition_text_processed = replace(condition_text_processed,'FIRST ARTICLE TESTER','FIRST ARTICLE CONDITIONS');
update temp_clause_conditions set condition_text_processed = replace(condition_text_processed,'FOB DISIGNATION','FOB DESIGNATION');
update temp_clause_conditions set condition_text_processed = replace(condition_text_processed,'SF1447','SF 1447');
update temp_clause_conditions set condition_text_processed = replace(condition_text_processed,'DOCUMENT VALUE','CONTRACT VALUE');
update temp_clause_conditions set condition_text_processed = replace(condition_text_processed,'6.302-5   AUTHORIZED OR REQUIRED BY STATUTE.','6.302-5   AUTHORIZED OR REQUIRED BY STATUTE');
update temp_clause_conditions set condition_text_processed = replace(condition_text_processed,'6.302-2 UNSUAL AND COMPELLING URGENCY','6.302-2   UNSUAL AND COMPELLING URGENCY');
update temp_clause_conditions set condition_text_processed = replace(condition_text_processed,'FUNDING FISCAL YEAR IS:','FUNDING FY IS:');
update temp_clause_conditions set condition_text_processed = replace(condition_text_processed,'FUNDING FY IS:FY','FUNDING FY IS: FY');
update temp_clause_conditions set condition_text_processed = replace(condition_text_processed,'SUPPLY SOUCE','SUPPLY SOURCE');
update temp_clause_conditions set condition_text_processed = replace(condition_text_processed,'(D) INCLUSION OF SERVICES: YES','(D) INCLUSION OF SERVICES IS: YES');
update temp_clause_conditions set condition_text_processed = replace(condition_text_processed,'INSPECTION AND ACCEPTANCE REQUIREMENTS IS: GOVERNMENT REQUIRES AN EXPLICIT UNDERSTANDING OF THE CONTRACTOR\'S INSPECTION RESPONSIBILITIES','INSPECTION REQUIREMENTS IS: GOVERNMENT REQUIRES AN EXPLICIT UNDERSTANDING OF THE CONTRACTOR\'S INSPECTION RESPONSIBILITIES');
update temp_clause_conditions set condition_text_processed = replace(condition_text_processed,'INSPECTION AND ACCEPTANCE REQUIREMENTS IS: A HIGHER LIEVEL CONTRACT QUALITY REQUIREMENT IS APPROPRIATE','INSPECTION REQUIREMENTS IS: A HIGHER LIEVEL CONTRACT QUALITY REQUIREMENT IS APPROPRIATE');
update temp_clause_conditions set condition_text_processed = replace(condition_text_processed,'INSPECTION AND ACCEPTANCE REQUIREMENTS IS: GOVERNMENT WILL RELY ON CONTRACTOR INSPECTION','INSPECTION REQUIREMENTS IS: GOVERNMENT WILL RELY ON CONTRACTOR INSPECTION');
update temp_clause_conditions set condition_text_processed = replace(condition_text_processed,'INSPECTION AND ACCEPTANCE REQUIREMENTS IS: CERTIFICATE OF CONFORMANCE IS IN THE BEST INTEREST OF THE GOVERNMENT IN LIEU OF SOURCE INSPECTION','ACCEPTANCE REQUIREMENTS IS: CERTIFICATE OF CONFORMANCE IS IN THE BEST INTEREST OF THE GOVERNMENT IN LIEU OF SOURCE INSPECTION');
update temp_clause_conditions set condition_text_processed = replace(condition_text_processed,'INSPECTION AND ACCEPTANCE REQUIREMENTS IS: GOVERNMENT INSPECTION AND ACCEPTANCE ARE TO BE PERFORMED AT THE CONTRACTOR\'S PLANT','ACCEPTANCE REQUIREMENTS IS: GOVERNMENT INSPECTION AND ACCEPTANCE ARE TO BE PERFORMED AT THE CONTRACTOR\'S PLANT');
update temp_clause_conditions set condition_text_processed = replace(condition_text_processed,'INSPECTION AND ACCEPTANCE REQUIREMENTS IS: INSPECTION AND ACCEPTANCE WILL OCCUR AT ORIGIN','ACCEPTANCE REQUIREMENTS IS: INSPECTION AND ACCEPTANCE REQUIREMENTS IS: INSPECTION AND ACCEPTANCE WILL OCCUR AT ORIGIN');
update temp_clause_conditions set condition_text_processed = replace(condition_text_processed,'INSPECTION AND ACCEPTANCE REQUIREMENTS IS: CONTRACTOR WILL PERFORM INFORMATION ASSURANCE FUNCTION','ACCEPTANCE REQUIREMENTS IS: CONTRACTOR WILL PERFORM INFORMATION ASSURANCE FUNCTION');
update temp_clause_conditions set condition_text_processed = replace(condition_text_processed,'INTEREST ON PARTIAL PAYMENTS: YES','INTEREST ON PARTIAL PAYMENTS IS: YES');
update temp_clause_conditions set condition_text_processed = replace(condition_text_processed,'ISSUING OFFICEY','ISSUING OFFICE');
update temp_clause_conditions set condition_text_processed = replace(condition_text_processed,'ISSING OFFICE','ISSUING OFFICE');
update temp_clause_conditions set condition_text_processed = replace(condition_text_processed,'ISSUING OFFICE IS:DEPARTMENT','ISSUING OFFICE IS: DEPARTMENT');
update temp_clause_conditions set condition_text_processed = replace(condition_text_processed,'DEPARTMENT OF DEFENSE THREAT REDUCTION AGENCY','DEFENSE THREAT REDUCTION AGENCY');
update temp_clause_conditions set condition_text_processed = replace(condition_text_processed,'SUBMISSION OF LABOR AGREEMENT FROM SUCCESSFUL CONTRACTOR PRIOR TO AWARD','SUBMISSION OF LABOR AGREEMENT FROM SUCCESSFUL OFFEROR PRIOR TO AWARD');
update temp_clause_conditions set condition_text_processed = replace(condition_text_processed,'RECRUITMENT IS MADE OUTSIDE THE UNITED STATES (50 PLUS DISTRICT OF COLUMBIA)','RECRUITMENT IS MADE OUTSIDE THE UNITED STATES (50 STATES PLUS DISTRICT OF COLUMBIA)');
update temp_clause_conditions set condition_text_processed = replace(condition_text_processed,'RECRUITMENT IS MADE OUTSIDE THE UNITED STATES (50 PLUS DISTRICT OF COLUMBIA), PUERTO RICO, THE NORTHERN MARINA ISLANDS, AMERICAN SAMOA, GUAM, THE U.S VIRGIN ISLANDS AND WAKE ISLAND','RECRUITMENT IS MADE OUTSIDE THE UNITED STATES (50 STATES PLUS DISTRICT OF COLUMBIA), PUERTO RICO, THE NORTHERN MARINA ISLANDS, AMERICAN SAMOA, GUAM, THE U.S VIRGIN ISLANDS AND WAKE ISLAND');
update temp_clause_conditions set condition_text_processed = replace(condition_text_processed,'THE CONTRACTOR INTENDS TO USE MEMBERS OF THE SELECTIVE RESERVE','THE CONTRACTOR INTENDS TO USE MEMBERS OF THE SELECTIVE RESERVE AND THIS WAS AN EVALUATION FACTOR') where condition_text = 'THE CONTRACTOR INTENDS TO USE MEMBERS OF THE SELECTIVE RESERVE';
update temp_clause_conditions set condition_text_processed = replace(condition_text_processed,'(C) LAUNDRY AND DRY CLEANING (BULK WEIGHT BASIS)','(C) LAUNDRY AND DRY CLEANING IS: LAUNDRY AND DRY CLEANING (BULK WEIGHT BASIS)');
update temp_clause_conditions set condition_text_processed = replace(condition_text_processed,'(C) LAUNDRY AND DRY CLEANING (COUNT BASIS)','(C) LAUNDRY AND DRY CLEANING IS: LAUNDRY AND DRY CLEANING (COUNT BASIS)');
update temp_clause_conditions set condition_text_processed = replace(condition_text_processed,'LAUNDRY AND DRY CLEANING IS:LAUNDRY','LAUNDRY AND DRY CLEANING IS: LAUNDRY');
update temp_clause_conditions set condition_text_processed = replace(condition_text_processed,'(D) LEGISLATIVE SOURCE OF FUNDS IS: RECOVERY ACT FUNDS','(D) LEGISLATIVE SOURCE OF FUNDS IS: RECOVERY ACT FUNDS');
update temp_clause_conditions set condition_text_processed = replace(condition_text_processed,'(I) LEGISLATIVE SOURCE OF FUNDS IS: NOT RECOVERY ACT FUNDS','(I) LEGISLATIVE SOURCE OF FUNDS IS: NOT RECOVERY ACT FUNDS');
update temp_clause_conditions set condition_text_processed = replace(condition_text_processed,'FUNDS APPROPRIATED BY THE DEPARTMENT OF DEFENSE APPROPRIATIONS ACT, 2014 (PUBL L. 113-76, DIVISIONS C AND J)','FUNDS APPROPRIATED BY THE DEPARTMENT OF DEFENSE APPROPRIATIONS ACT, 2014 AND BY THE MILITARY CONSTRUCTION AND VETERANS AFFAIRS AND RELATED AGENCIES APPROPRIATIONS ACT, 2014 (PUBL L. 113-76, DIVISIONS C AND J)');
update temp_clause_conditions set condition_text_processed = replace(condition_text_processed,'FUNDS MADE AVAILABLE BY THE CONTINUING APPROPRIATIONS RESOLUTION, 2013 (PUB. L 112-175), DOD APPROPRIATIONS','FUNDS MADE AVAILABLE BY THE CONTINUING APPROPRIATIONS RESOLUTION, 2013  (PUB. L 112-175), DOD MILITARY CONSTRUCTION APPROPRIATIONS');
update temp_clause_conditions set condition_text_processed = replace(condition_text_processed,'MATERIAL CONTENT:','MATERIAL CONTENT IS:');
update temp_clause_conditions set condition_text_processed = replace(condition_text_processed,'MISCELLANEOUS PROGRAM CHARACTERISTICS: THE','MISCELLANEOUS PROGRAM CHARACTERISTICS IS: THE');
update temp_clause_conditions set condition_text_processed = replace(condition_text_processed,'MISCELLANEOUS PROGRAM CHARACTERISTICS: THIS','MISCELLANEOUS PROGRAM CHARACTERISTICS IS: THIS');
update temp_clause_conditions set condition_text_processed = replace(condition_text_processed,'MISCELLANEOUS PROGRAM INFORMATION','MISCELLANEOUS PROGRAM CHARACTERISTICS');
update temp_clause_conditions set condition_text_processed = replace(condition_text_processed,'MISCELLANEOUS PROGRAMS IS:','MISCELLANEOUS PROGRAM CHARACTERISTICS IS:');
update temp_clause_conditions set condition_text_processed = replace(condition_text_processed,'NO TIME TO PROCESS BUY AMERICAN DETERMINATION: IS YES','NO TIME TO PROCESS BUY AMERICAN DETERMINATION IS: YES');
update temp_clause_conditions set condition_text_processed = replace(condition_text_processed,'STANDARD PAYMENT INSTRUCTIONS APPLY (PGI 4204.7108)','STANDARD PAYMENT INSTRUCTIONS APPLY (PGI 204.7108)');
update temp_clause_conditions set condition_text_processed = replace(condition_text_processed,'(C) PERFORMANCE REQUIREMENTS: EARNED VALUE MANAGEMENT SYSTEM','GOVERNMENT REVIEWS IS: EARNED VALUE MANAGEMENT SYSTEM');
update temp_clause_conditions set condition_text_processed = replace(condition_text_processed,'(C) PERFORMANCE REQUIREMENTS: ITEM IDENTIFICATION, VALUATION, OR BOTH ARE REQUIRED','GOVERNMENT REVIEWS IS: "ITEM IDENTIFICATION, VALUATION, OR BOTH ARE  REQUIRED');
update temp_clause_conditions set condition_text_processed = replace(condition_text_processed,'THE ACQUISITION IS SUBJECT TO A QUALIFICATION REQUIREMENT.','THE ACQUISITION IS SUBJECT TO A QUALIFICATION REQUIREMENT');
update temp_clause_conditions set condition_text_processed = replace(condition_text_processed,'PERFORMANCE REQUIREMENTS IS: FIRST ARTICLE APPROVAL IS REQUIRED','GOVERNMENT REVIEWS IS: FIRST ARTICLE APPROVAL IS REQUIRED');
update temp_clause_conditions set condition_text_processed = replace(condition_text_processed,'PERFORMANCE REQUIREMENTS IS: MAKE OR BUY PROGRAM','GOVERNMENT REVIEWS IS: MAKE OR BUY PROGRAM');
update temp_clause_conditions set condition_text_processed = replace(condition_text_processed,'PERFORMANCE REQUIREMENTS IS: INTEGRATED BASELINE REVIEW (PRIOR TO AWARD)','GOVERNMENT REVIEWS IS: INTEGRATED BASELINE REVIEW (PRIOR TO AWARD)');
update temp_clause_conditions set condition_text_processed = replace(condition_text_processed,'PERFORMANCE REQUIREMENTS IS: INTEGRATED BASELINE REVIEW (AFTER AWARD)','GOVERNMENT REVIEWS IS: INTEGRATED BASELINE REVIEW (AFTER AWARD)');
update temp_clause_conditions set condition_text_processed = replace(condition_text_processed,'PERFORMANCE REQUIREMENTS IS: EARNED VALUE MANAGEMENT SYSTEM','GOVERNMENT REVIEWS IS: EARNED VALUE MANAGEMENT SYSTEM');
update temp_clause_conditions set condition_text_processed = replace(condition_text_processed,'PERFORMANCE REQUIREMENTS IS: RADIO FREQUENCY IDENTIFICATION (RFID) IS REQUIRED','GOVERNMENT REVIEWS IS: RADIO FREQUENCY IDENTIFICATION (RFID) IS REQUIRED');
update temp_clause_conditions set condition_text_processed = replace(condition_text_processed,'PERFORMANCE REQUIREMENTS IS: USE OF A LEAD SYSTEMS INTEGRATOR','GOVERNMENT REVIEWS IS: USE OF A LEAD SYSTEMS INTEGRATOR');
update temp_clause_conditions set condition_text_processed = replace(condition_text_processed,'PLACE OF ISSUANCE: ','PLACE OF ISSUANCE IS: ');
update temp_clause_conditions set condition_text_processed = replace(condition_text_processed,'PERFORMACE','PERFORMANCE');
update temp_clause_conditions set condition_text_processed = replace(condition_text_processed,'(E) PLACE OF PERFORMANCE IS : NOT UNITED STATES OUTLYING AREAS','(E) PLACE OF PERFORMANCE IS: NOT UNITED STATES OUTLYING AREAS');
update temp_clause_conditions set condition_text_processed = replace(condition_text_processed,'(I) PLACE OF PERFORMANCE IS :AMERICAN SAMOA','(I) PLACE OF PERFORMANCE IS: AMERICAN SAMOA');
update temp_clause_conditions set condition_text_processed = replace(condition_text_processed,'(L) PLACE OF PERFORMANCE IS:AMERICA SAMOA','(L) PLACE OF PERFORMANCE IS: AMERICA SAMOA');
update temp_clause_conditions set condition_text_processed = replace(condition_text_processed,'(K) PLACE OF PERFORMANCE IS:GUAM','(K) PLACE OF PERFORMANCE IS: GUAM');
update temp_clause_conditions set condition_text_processed = replace(condition_text_processed,'(J) PLACE OF PERFORMANCE IS:NORTHERN MARINA ISLANDS','(J) PLACE OF PERFORMANCE IS: NORTHERN MARINA ISLANDS');
update temp_clause_conditions set condition_text_processed = replace(condition_text_processed,'(E) PLACE OF PERFORMANCE IS:PUERTO RICO','(E) PLACE OF PERFORMANCE IS: PUERTO RICO');
update temp_clause_conditions set condition_text_processed = replace(condition_text_processed,'(K) PLACE OF PERFORMANCE IS:U.S. VIRGIN ISLANDS','(K) PLACE OF PERFORMANCE IS: U.S. VIRGIN ISLANDS');
update temp_clause_conditions set condition_text_processed = replace(condition_text_processed,'(J) PLACE OF PERFORMANCE IS:UNITED STATES VIRGIN ISLANDS','(J) PLACE OF PERFORMANCE IS: UNITED STATES VIRGIN ISLANDS');
update temp_clause_conditions set condition_text_processed = replace(condition_text_processed,'PLACE OF PERFORMANCE IS: UNITED STATES (50 PLUS DISCRICT OF COLUMBIA)','PLACE OF PERFORMANCE IS: UNITED STATES (50 STATES PLUS DISTRICT OF COLUMBIA)');
update temp_clause_conditions set condition_text_processed = replace(condition_text_processed,'PLACE OF PERFORMANCE IS: UNITED STATES( 50 STATES PLUS DISTRICT OF COLUMBIA)','PLACE OF PERFORMANCE IS: UNITED STATES (50 STATES PLUS DISTRICT OF COLUMBIA)');
update temp_clause_conditions set condition_text_processed = replace(condition_text_processed,'PLACE OF PERFORMANCE IS: UNITED STATES(50 STATES PLUS DISTRICT OF COLUMBIA)','PLACE OF PERFORMANCE IS: UNITED STATES (50 STATES PLUS DISTRICT OF COLUMBIA)');
update temp_clause_conditions set condition_text_processed = replace(condition_text_processed,'PLACE OF PERFORMANCE IS: UNITED STATES( 50 PLUS DISTRICT OF COLUMBIA)','PLACE OF PERFORMANCE IS: UNITED STATES (50 STATES PLUS DISTRICT OF COLUMBIA)');
update temp_clause_conditions set condition_text_processed = replace(condition_text_processed,'PLACE OF PERFORMANCE IS: UNITED STATES (50 PLUS DISTRICT OF COLUMBIA)','PLACE OF PERFORMANCE IS: UNITED STATES (50 STATES PLUS DISTRICT OF COLUMBIA)');
update temp_clause_conditions set condition_text_processed = replace(condition_text_processed,'PLACE OF PERFORMANCE IS: UNITED STATES (50 STAPLUS DISTRICT OF COLUMBIA)','PLACE OF PERFORMANCE IS: UNITED STATES (50 STATES PLUS DISTRICT OF COLUMBIA)');
update temp_clause_conditions set condition_text_processed = replace(condition_text_processed,'PLACE OF PERFORMANCE IS: CONTRACTOR OPERATED (GOCO)','PLACE TYPE IS: CONTRACTOR OPERATED (GOCO)');
update temp_clause_conditions set condition_text_processed = replace(condition_text_processed,'PLACE OF PERFORMANCE IS: GOVERNMENT OPERATED','PLACE TYPE IS: GOVERNMENT OPERATED');
update temp_clause_conditions set condition_text_processed = replace(condition_text_processed,'PLACE OF PERFORMANCE IS: GOVERNMENT OWNED OR LEASED FACILITIES/INSTALLATIONS','PLACE TYPE IS: GOVERNMENT OWNED OR LEASED FACILITIES/INSTALLATIONS');
update temp_clause_conditions set condition_text_processed = replace(condition_text_processed,'PLACE OF PERFORMANCE IS: UNITED STATES(50 PLUS DISTRICT OF COLUMBIA)','PLACE OF PERFORMANCE IS: UNITED STATES (50 STATES PLUS DISTRICT OF COLUMBIA)');
update temp_clause_conditions set condition_text_processed = replace(condition_text_processed,'PLACE OF PERFORMANCE IS: UNITED STATES VIRGIN ISLANDS','PLACE OF PERFORMANCE IS: VIRGIN ISLANDS');
update temp_clause_conditions set condition_text_processed = replace(condition_text_processed,'ASSOCIATED TERRITORIAL WATERS AND AIRSPACE.','ASSOCIATED TERRITORIAL WATERS AND AIR SPACE');
update temp_clause_conditions set condition_text_processed = replace(condition_text_processed,'ASSOCIATED TERRITORIAL WATERS AND AIRSPACE','ASSOCIATED TERRITORIAL WATERS AND AIR SPACE');
update temp_clause_conditions set condition_text_processed = replace(condition_text_processed,'PLACE OF PERFORMANCE IS: VEHICLES, GOVERNMENT OWNED OR LEASED','PLACE TYPE IS: VEHICLES, GOVERNMENT OWNED OR LEASED');
update temp_clause_conditions set condition_text_processed = replace(condition_text_processed,'UNITED STATES OULYING AREAS','UNITED STATES OUTLYING AREAS');
update temp_clause_conditions set condition_text_processed = replace(condition_text_processed,'AMERICA SAMOA','AMERICAN SAMOA');
update temp_clause_conditions set condition_text_processed = replace(condition_text_processed,'(C) PLACE OF PERFORMANCE IS: OVERNMENT OWNED OR LEASED FACILITIES/INSTALLATIONS','(C) PLACE TYPE IS: GOVERNMENT OWNED OR LEASED FACILITIES/INSTALLATIONS');
update temp_clause_conditions set condition_text_processed = replace(condition_text_processed,'(G) PLACE OF PERFORMANCE IS: NOT UNITED STATES OUTLYING AREA','(G) PLACE OF PERFORMANCE IS: NOT UNITED STATES OUTLYING AREAS');
update temp_clause_conditions set condition_text_processed = replace(condition_text_processed,'PLACE OF PERFORMANCE IS: SPANISH-AMERICAN INSTALLATIONS AND FACILITIES','PLACE TYPE IS: SPANISH-AMERICAN INSTALLATIONS AND FACILITIES');
update temp_clause_conditions set condition_text_processed = replace(condition_text_processed,'PLACE OF PERFORMANCE IS: NEAR OR ON NAVIGABLE WATERWAYS','PLACE TYPE IS: NEAR OR ON NAVIGABLE WATERWAYS');
update temp_clause_conditions set condition_text_processed = replace(condition_text_processed,'PLACE OF PERFORMANCE IS: AT OR NEAR AIRFIELDS','PLACE TYPE IS: AT OR NEAR AIRFIELDS');
update temp_clause_conditions set condition_text_processed = replace(condition_text_processed,'PLACE OF PERFORMANCE IS: FACILITY HOLDING DETAINEES','PLACE TYPE IS: FACILITY HOLDING DETAINEES');
update temp_clause_conditions set condition_text_processed = replace(condition_text_processed,'PRICING REQUIREMENT IS','PRICING REQUIREMENTS IS');
update temp_clause_conditions set condition_text_processed = replace(condition_text_processed,'(D) PLACE OF USE:','(D) PLACE OF USE IS: ');
update temp_clause_conditions set condition_text_processed = replace(condition_text_processed,'PERFORMANE','PERFORMANCE');
update temp_clause_conditions set condition_text_processed = replace(condition_text_processed,'REQUIRING ACTIVITY IS : ','REQUIRING ACTIVITY IS: ');
update temp_clause_conditions set condition_text_processed = replace(condition_text_processed,'PRIICING','PRICING');
update temp_clause_conditions set condition_text_processed = replace(condition_text_processed,'(B) PRICING REQUIREMENTS IS: PRICING IS: CONSISTANT WITH FAR PART 31.2, "CONTRACTS WITH COMMERCIAL ORGANIZATIONS"','(B) REGULATION PRICING IS: CONSISTANT WITH FAR PART 31.2, "CONTRACTS WITH COMMERCIAL ORGANIZATIONS"');
update temp_clause_conditions set condition_text_processed = replace(condition_text_processed,'PRICING REQUIREMENTS IS: CONSISTANT WITH FAR PART 31.2, "CONTRACTS WITH COMMERCIAL ORGANIZATIONS"','REGULATION PRICING IS: CONSISTANT WITH FAR PART 31.2, "CONTRACTS WITH COMMERCIAL ORGANIZATIONS"');
update temp_clause_conditions set condition_text_processed = replace(condition_text_processed,'PRICING REQUIREMENTS IS: FORMAT IN TABLE 15.2 IS USED','REGULATION PRICING IS: FORMAT IN TABLE 15.2 IS USED');
update temp_clause_conditions set condition_text_processed = replace(condition_text_processed,'PRICING REQUIREMENTS IS: NOT FORMAT IN TABLE 15.2 IS USED','REGULATION PRICING IS: NOT FORMAT IN TABLE 15.2 IS USED');
update temp_clause_conditions set condition_text_processed = replace(condition_text_processed,'PRICING REQUIREMENTS IS: PREDETERMINED INDIRECT COST RATES ARE USED','INDIRECT COST IS: PREDETERMINED INDIRECT COST RATES ARE USED');
update temp_clause_conditions set condition_text_processed = replace(condition_text_processed,'PRICING REQUIREMENTS IS: ESTABLISHED CATALOG OR MARKET PRICE','PRICING LIMITATIONS IS: ESTABLISHED CATALOG OR MARKET PRICE');
update temp_clause_conditions set condition_text_processed = replace(condition_text_processed,'PRICING REQUIREMENTS IS: ONE OR MORE IDENTIFIABLE LABOR OR MATERIAL COST FACTORS ARE SUBJECT TO CHANGE','DIRECT COST IS: ONE OR MORE IDENTIFIABLE LABOR OR MATERIAL COST FACTORS ARE SUBJECT TO CHANGE');
update temp_clause_conditions set condition_text_processed = replace(condition_text_processed,'PRICING REQUIREMENTS IS: FREIGHT IS SHIPPED UNDER RATES SUBJECT TO RELEASED OR DECLARED VALUE','TRANSPORTATION COST IS: FREIGHT IS SHIPPED UNDER RATES SUBJECT TO RELEASED OR DECLARED VALUE');
update temp_clause_conditions set condition_text_processed = replace(condition_text_processed,'PRICING REQUIREMENTS IS: NOT FREIGHT IS SHIPPED UNDER RATES SUBJECT TO RELEASED OR DECLARED VALUE','TRANSPORTATION COST IS: NOT FREIGHT IS SHIPPED UNDER RATES SUBJECT TO RELEASED OR DECLARED VALUE');
update temp_clause_conditions set condition_text_processed = replace(condition_text_processed,'PRICING REQUIREMENTS IS: SUBJECT TO CAS','CAS IS: SUBJECT TO CAS');
update temp_clause_conditions set condition_text_processed = replace(condition_text_processed,'PRICING REQUIREMENTS IS: NOT EXEMPT FROM CAS','CAS IS: NOT EXEMPT FROM CAS');
update temp_clause_conditions set condition_text_processed = replace(condition_text_processed,'PRICING REQUIREMENTS IS: MODIFIED CAS IS AUTHRIZED','CAS IS: MODIFIED CAS IS AUTHORIZED');
update temp_clause_conditions set condition_text_processed = replace(condition_text_processed,'PRICING REQUIREMENTS IS: NOT MODIFIED CAS IS AUTHRIZED','CAS IS: NOT MODIFIED CAS IS AUTHORIZED');
update temp_clause_conditions set condition_text_processed = replace(condition_text_processed,'PRICING REQUIREMENTS IS: SOLICITATION/AWARD CALLS FOR ESTABLISHMENT OF FINAL INDIRECT COST RATES','INDIRECT COST IS: SOLICITATION/AWARD CALLS FOR ESTABLISHMENT OF FINAL INDIRECT COST RATES');
update temp_clause_conditions set condition_text_processed = replace(condition_text_processed,'DOCUMEN TYPE','DOCUMENT TYPE');
update temp_clause_conditions set condition_text_processed = replace(condition_text_processed,'PERIOD OF PERFORMANCE IN DAYS IS:','PERIOD OF PERFORMANCE IS:');
update temp_clause_conditions set condition_text_processed = replace(condition_text_processed,'PRICING REQUIREMENTS IS: PRICES ARE SET BY LAW OR REGULATION','PRICING LIMITATIONS IS: PRICES ARE SET BY LAW OR REGULATION');
update temp_clause_conditions set condition_text_processed = replace(condition_text_processed,'PRICING REQUIREMENTS IS: REGULATED TRANSPORTATION IS INVOLVED','TRANSPORTATION COST IS: REGULATED TRANSPORTATION IS INVOLVED');
update temp_clause_conditions set condition_text_processed = replace(condition_text_processed,'PRICING REQUIREMENTS IS: GOVERNMENT IS RESPONSIBLE FOR GFP TRANSPORTATION ARRANGEMENTS AND COSTS','SHIPPING RESPONSIBILITIES GOVERNMENT IS: GOVERNMENT IS RESPONSIBLE FOR GFP TRANSPORTATION ARRANGEMENTS AND COSTS');
update temp_clause_conditions set condition_text_processed = replace(condition_text_processed,'PROCEDURE IS:','PROCEDURES IS:');
update temp_clause_conditions set condition_text_processed = replace(condition_text_processed,'PRICING REQUIREMENTS IS: CONTRACTORS MAY HAVE POTENTIAL TRANSIT CREDITS AVAIILABLE AND THE GOVERNMENT MAY REDUCE TRANSPORTATION COSTS','TRANSPORTATION COST IS: OFFERORS MAY HAVE POTENTIAL TRANSIT CREDITS AVAILABLE AND THE GOVERNMENT MAY REDUCE TRANSPORTATION COSTS');
update temp_clause_conditions set condition_text_processed = replace(condition_text_processed,'PROCEDURE IS:','PROCEDURES IS:');
update temp_clause_conditions set condition_text_processed = replace(condition_text_processed,'PRICING REQUIREMENTS IS: REIMBURSEMENT OF TRANSPORTATION AS A DIRECT CHARGE IS AUTHORIZED','TRANSPORTATION COST IS: REIMBURSEMENT OF TRANSPORTATION AS A DIRECT CHARGE IS AUTHORIZED');
update temp_clause_conditions set condition_text_processed = replace(condition_text_processed,'PRICING REQUIREMENTS IS: PRICING IS CONSISTANT WITH FAR PART 31.1, "APPLICABILITY"','REGULATION PRICING IS: PRICING IS CONSISTANT WITH FAR PART 31.1, "APPLICABILITY"');
update temp_clause_conditions set condition_text_processed = replace(condition_text_processed,'PRICING REQUIREMENTS IS: PRICING IS CONSISTANT WITH FAR PART 31.2, "CONTRACTS WITH COMMERCIAL ORGANIZATIONS"','REGULATION PRICING IS: PRICING IS CONSISTANT WITH FAR PART 31.2, "CONTRACTS WITH COMMERCIAL ORGANIZATIONS"');
update temp_clause_conditions set condition_text_processed = replace(condition_text_processed,'PRICING REQUIREMENTS IS: PRICING IS CONSISTANT WITH FAR PART 31.6, "CONTRACTS WITH STATE, LOCAL, AND FEDERALLY RECOGNIZED INDIAN TRIBAL GOVERNMENTS"','REGULATION PRICING IS: PRICING IS CONSISTANT WITH FAR PART 31.6, "CONTRACTS WITH STATE, LOCAL, AND FEDERALLY RECOGNIZED INDIAN TRIBAL GOVERNMENTS"');
update temp_clause_conditions set condition_text_processed = replace(condition_text_processed,'PRICING REQUIREMENTS IS: PRICING IS CONSISTANT WITH FAR PART 31.7, "CONTRACTS WITH NONPROFIT ORGANIZATIONS"','REGULATION PRICING IS: PRICING IS CONSISTANT WITH FAR PART 31.7, "CONTRACTS WITH NONPROFIT ORGANIZATIONS"');
update temp_clause_conditions set condition_text_processed = replace(condition_text_processed,'PRICING REQUIREMENTS IS: STATUTORY COST LIMITATIONS APPLY','PRICING LIMITATIONS IS: STATUTORY COST LIMITATIONS APPLY');
update temp_clause_conditions set condition_text_processed = replace(condition_text_processed,'PRICING REQUIREMENTS IS: TRANSPORTATION WITH FUEL-RELATED ADJUSTMENTS','TRANSPORTATION COST IS: TRANSPORTATION WITH FUEL-RELATED ADJUSTMENTS');
update temp_clause_conditions set condition_text_processed = replace(condition_text_processed,'PRICING IS: CONSISTANT WITH FAR PART 31.2, "CONTRACTS WITH COMMERCIAL ORGANIZATIONS",','REGULATION PRICING IS: PRICING IS CONSISTANT WITH FAR PART 31.2, "CONTRACTS WITH COMMERCIAL ORGANIZATIONS"');
update temp_clause_conditions set condition_text_processed = replace(condition_text_processed,'PROCEDURES IS: SEALED BIDDING','PROCEDURES IS: SEALED BIDDING (FAR PART 14)') where condition_text like '%PROCEDURES IS: SEALED BIDDING';
update temp_clause_conditions set condition_text_processed = replace(condition_text_processed,'PROCEDURES IS: NOT FEDERAL SUPPLY SCHEDULE (FAR PART 6)','PROCEDURES IS: NOT FEDERAL SUPPLY SCHEDULE (FAR PART 8.4)');
update temp_clause_conditions set condition_text_processed = replace(condition_text_processed,'PROCEDURES IS: TOTAL SMALL BUSINESS SET ASIDE','PROCEDURES IS: TOTAL SMALL BUSINESS SET-ASIDE');
update temp_clause_conditions set condition_text_processed = replace(condition_text_processed,'PROCEDURES IS: FULL AND OPEN COMPETITION','PROCEDURES IS: FULL AND OPEN COMPETITION (FAR PART 6)');
update temp_clause_conditions set condition_text_processed = replace(condition_text_processed,'PROCEDURES IS: NEGOTIATION','PROCEDURES IS: NEGOTIATION (FAR PART 15)') where condition_text like '%PROCEDURES IS: NEGOTIATION';
update temp_clause_conditions set condition_text_processed = replace(condition_text_processed,'(B) PROCEDURES IS: NOT FULL AND OPEN COMPETITION','(B) PROCEDURES IS: NOT FULL AND OPEN COMPETITION');
update temp_clause_conditions set condition_text_processed = replace(condition_text_processed,'BUSINESS TYPE IS: PROCUREMENT LIST MAINTAINED BY THE COMMITTEE FOR PURCHASE FROM PEOPLE WHO ARE BLIND OR SEVERELY DISABLED','GOVERNMENT SOURCE IS: PROCUREMENT LIST MAINTAINED BY THE COMMITTEE FOR PURCHASE FROM PEOPLE WHO ARE BLIND OR SEVERELY DISABLED');
update temp_clause_conditions set condition_text_processed = replace(condition_text_processed,'(F) BUSINESS TYPE IS: NOT-FOR-PROFIT ORGANIZATION','(F) MISCELLANEOUS SOURCE IS: NOT-FOR-PROFIT ORGANIZATION');
update temp_clause_conditions set condition_text_processed = replace(condition_text_processed,'(H) BUSINESS TYPE IS: NOT NONPROFIT ORGANIZATIONS','(H) BUSINESS TYPE IS: NOT NON-PROFIT ORGANIZATION');
update temp_clause_conditions set condition_text_processed = replace(condition_text_processed,'(D) SERVICES IS: SERVICE CONTRACT REPORTING IS REQUIRED','(D) SERVICE CONTRACT REPORTING IS: YES');
update temp_clause_conditions set condition_text_processed = replace(condition_text_processed,'THEUSE','THE USE');
update temp_clause_conditions set condition_text_processed = replace(condition_text_processed,'','');
update temp_clause_conditions set condition_text_processed = replace(condition_text_processed,'','');
update temp_clause_conditions set condition_text_processed = replace(condition_text_processed,'','');

--

-- Extract question name and answer value for clause conditions into separate columns

update temp_clause_conditions 
set question_name = concat('[',trim(substring_index(substring_index(condition_text_processed, 'IS: ', 1), ') ', -1)),']'),
	value = trim(substring_index(condition_text_processed, 'IS: ', -1));

delete from temp_clause_conditions where condition_text = '' and value = '' and question_name = '[]';
delete from temp_clause_conditions where condition_text = ',' and value = ',' and question_name = '[,]';


-- Set operator to denote whether operation is "equals" or "not equals"
update temp_clause_conditions 
set operator = 
	case 
		when value like 'NOT %' 
			and (raw_id not in ( select id from raw_clauses where clause_number in ('52.211-3','52.211-4'))) -- clause conditions with legitimate "NOT" in answers
			then '!=' 
        else '='
	end;
    

update temp_clause_conditions 
set value = right(value, length(value)-4) 
where value like 'NOT %' and (raw_id not in ( select id from raw_clauses where clause_number in ('52.211-3','52.211-4')));


update temp_clause_conditions
set question_name = '[SUPPLIES OR SERVICES]'
where question_name in ('[SERVICES]', '[SUPPLIES]');

update temp_clause_conditions set real_question_id = null, real_question_name = null;

update temp_clause_conditions
join temp_questions q
	on temp_clause_conditions.question_name = q.question_name
	and q.question_type != 'Numeric'
set real_question_id = 
		(
		select min(q2.id)
		from temp_questions q2
		join temp_possible_answers a2
			on q2.id = a2.question_id
		where q2.root_question_id = q.id
            and temp_clause_conditions.value = a2.choice_text
            and a2.id 
		)
where not exists
	(
    select 1
    from temp_possible_answers a
    where question_id = q.id
		and temp_clause_conditions.value = a.choice_text
    );


update temp_clause_conditions
join temp_questions q
	on temp_clause_conditions.question_name = q.question_name
	and q.question_type != 'Numeric'
set real_question_id = q.id
where exists
	(
    select 1
    from temp_possible_answers a
    where question_id = q.id
		and temp_clause_conditions.value = a.choice_text
    );

update temp_clause_conditions
set real_question_name = ( select question_name from temp_questions where id = temp_clause_conditions.real_question_id);
        


update temp_clause_conditions c
join
	(
    select a.choice_text, q.id, q.question_name
    from temp_possible_answers a
	join temp_questions q
		on q.id = a.question_id
	where q.question_name in
		(
        '[SUPPLIES SHIPPED]',
		'[SHIPPING DESTINATIONS]',
		'[SHIPPING RESPONSIBILITIES CONTRACTOR]',
		'[SHIPPING RESPONSIBILITIES GOVERNMENT]',
		'[MODE OF TRANSPORTATION]',
		'[SHIPPING DETERMINATIONS]',
		'[SHIPPING MULTIPLES]',
		'[SHIPPING METHOD]',
		'[EXPORT REQUIREMENTS]',
		'[IMPORT REQUIREMENTS]',
		'[DUTY DETERMINATIONS]'
        )
	) a
    on a.choice_text = c.value
set real_question_name = a.question_name,
	real_question_id = a.id
where c.value is not null 
	and c.real_question_name is null 
    and c.question_name not regexp '^\\[[0-9]+' 
    and c.question_name in ('[TRANSPORTATION REQUIREMENTS]');


update temp_clause_conditions c
join
	(
    select a.choice_text, q.id, q.question_name
    from temp_possible_answers a
	join temp_questions q
		on q.id = a.question_id
	where q.question_name in
		(
        '[GOVERNMENT REVIEWS]',
		'[RECRUITMENT POLICY]',
		'[LABOR RATES]'
        )
	) a
    on a.choice_text = c.value
set real_question_name = a.question_name,
	real_question_id = a.id
where c.value is not null 
	and c.real_question_name is null 
    and c.question_name not regexp '^\\[[0-9]+' 
    and c.question_name in ('[LABOR REQUIREMENTS]');
    
    

update temp_clause_conditions c
join temp_possible_answers a
	on choice_text = c.value
join temp_questions q
	on a.question_id = q.id
set real_question_name = q.question_name,
	real_question_id = q.id
where c.value is not null 
	and c.real_question_name is null 
    and c.question_name not regexp '^\\[[0-9]+' 
    and c.value not in ('NO', 'YES')
    and 
		(
        select count(1)
        from temp_possible_answers
        where choice_text = c.value
        ) = 1;


update temp_possible_answers set soundex_hash = soundex(choice_text);


update temp_clause_conditions c
join temp_possible_answers a
	on soundex_hash= soundex(c.value)
join temp_questions q
	on a.question_id = q.id
set real_question_name = q.question_name,
	real_question_id = q.id,
    real_value = a.choice_text
where c.value is not null 
	and c.real_question_name is null 
    and c.question_name not regexp '^\\[[0-9]+' 
    and c.value not in ('NO', 'YES')
    and 
		(
        select count(1)
        from temp_possible_answers
        where soundex_hash = soundex(c.value)
        ) = 1;

update temp_clause_conditions set real_value = 'GREATER THAN 150000' where value = 'GREATER THAN SAT' ;
update temp_clause_conditions set real_value = 'GREATER THAN OR EQUAL TO 150000' where value = 'GREATER THAN OR EQUAL TO SAT' ;
update temp_clause_conditions set real_value = 'LESS THAN OR EQUAL TO 150000' where value = 'LESS THAN OR EQUAL TO SAT' ;

update temp_clause_conditions set real_value = 'GREATER THAN 150000' where value = 'GREATER THAN SAP' ;
update temp_clause_conditions set real_value = 'LESS THAN OR EQUAL TO 150000' where value = 'LESS THAN OR EQUAL TO SAP' ;

update temp_clause_conditions 
set real_value = concat('GREATER THAN OR EQUAL TO ', right(value, length(value)-length('GREATER OR EQUAL TO ')))
where value regexp '^GREATER OR EQUAL TO [0-9]+$';

-- Strip out commas 
update temp_clause_conditions 
set real_value = replace(value,',','')
where value regexp '^GREATER THAN OR EQUAL TO ([0-9](,)*)+$' and question_name = '[CONTRACT VALUE]';


-- Enclose anwswer choice in double quotes
update temp_clause_conditions 
set real_value = replace(value,',','')
where value regexp '^GREATER THAN OR EQUAL TO ([0-9](,)*)+$' and question_name = '[CONTRACT VALUE]';

