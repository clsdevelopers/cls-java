/*
	This script extracts the dependencies into a separate table for the display rules of questions
*/

drop procedure if exists normalize_question_dependencies;

DELIMITER $$
 
CREATE PROCEDURE normalize_question_dependencies ()
BEGIN
 
DECLARE v_finished INTEGER DEFAULT 0;
DECLARE v_id INTEGER DEFAULT 0;
DECLARE v_dependencies varchar(10000) DEFAULT ""; 
DECLARE BEGIN_INDX INT;
DECLARE END_INDX INT;
 DECLARE SLICE nvarchar(4000); 

 
DEClARE rules_cursor CURSOR FOR
select id, parsed_rule
from temp_questions 
where parsed_rule != ''; 
 
-- declare NOT FOUND handler
DECLARE CONTINUE HANDLER
        FOR NOT FOUND SET v_finished = 1;
 
OPEN rules_cursor;
 
get_rule: LOOP
  SET BEGIN_INDX = 1;
	FETCH rules_cursor INTO v_id, v_dependencies;
	 
     IF v_finished = 1 THEN
		LEAVE get_rule;
	END IF;
    
	MyLoop: WHILE BEGIN_INDX != 0 DO
		 -- GET THE INDEX OF THE FIRST OCCURENCE OF THE SPLIT CHARACTER
		 SET BEGIN_INDX = LOCATE('[',v_dependencies);
         SET END_INDX = LOCATE(']',v_dependencies);
		 -- NOW PUSH EVERYTHING TO THE LEFT OF IT INTO THE SLICE VARIABLE
		 IF BEGIN_INDX !=0 THEN
		  SET SLICE = substring(v_dependencies,BEGIN_INDX, END_INDX-BEGIN_INDX+1);
		 ELSE
		  LEAVE MyLoop;
		 END IF;

		 -- PUT THE ITEM INTO THE RESULTS SET
		 INSERT INTO TEMP_QUESTION_DEPENDENCIES(question_id, referenced_question_name) VALUES (v_id,SLICE);
		 -- CHOP THE ITEM REMOVED OFF THE MAIN STRING
		 SET v_dependencies = RIGHT(v_dependencies,CHAR_LENGTH(v_dependencies) - END_INDX -1 );
		 -- BREAK OUT IF WE ARE DONE
		 IF LENGTH(v_dependencies) = 0 THEN
			LEAVE MyLoop;
		 END IF;
	END WHILE;
	
END LOOP get_rule;
 
CLOSE rules_cursor;
 
END$$
 
DELIMITER ;





