/*
	
	This script sequences the questions based on the question dependencies

*/

update temp_questions set sequence = null;


update temp_questions
set sequence = 
	(
    select count(*)
	from
		(
		select raw_id, id, g.Question_Group
		from temp_questions q
        join cls3.Question_Group_Ref g
			on q.group_name = g.Question_Group_Name
		) as sub
	where sub.raw_id <= temp_questions.raw_id
    )
where category = 'Baseline';
    
            
update temp_questions
set sequence = 
	(
    select count(*)
	from
		(
		select raw_id, id, Question_Group
		from temp_questions q
        join cls3.Question_Group_Ref g
			on q.group_name = g.Question_Group_Name
		) as sub
	where 
		( select Question_Group from cls3.Question_Group_Ref g where temp_questions.group_name = g.Question_Group_Name ) 
        > sub.Question_Group
    )
    +
    (
    select count(*)
	from
		(
		select sequence, raw_id, id, group_name
		from temp_questions q
		) as sub
	where sub.sequence <= temp_questions.sequence
		and temp_questions.group_name = sub.group_name
    )
where category = 'Baseline';
    

update temp_questions
set sequence = sequence * 100;



update temp_questions
set sequence = 
	(
    select parent.sequence
    from 
		(
        select *
        from temp_questions
        ) parent
	where temp_questions.root_question_id = parent.id
    ) 
    +
	(
    select count(*)
    from 
		(
        select *
        from temp_questions
        where category = 'Waterfall'
        ) sub
	where root_question_id = temp_questions.root_question_id 
		and raw_id <= temp_questions.raw_id
    )
where category = 'Waterfall';


update temp_questions
set sequence = 
	(
    select max(sequence)
    from 
		(
        select *
        from temp_questions
        where sequence is not null
        ) sibling
	where temp_questions.root_question_id = sibling.root_question_id
		and temp_questions.group_name = sibling.group_name
    ) 
    +
	(
    select count(*)
    from 
		(
        select *
        from temp_questions
        where category = 'Follow-on'
        ) sub
	where root_question_id = temp_questions.root_question_id 
		and raw_id <= temp_questions.raw_id
    )
where category = 'Follow-on';

/*
-- Defragment sequence numbers

update temp_questions q
set sequence = 
	(
    select count(*)
    from
		(
        select sequence
		from temp_questions
		) sub
    where sub.sequence <= q.sequence 
    );	
*/

