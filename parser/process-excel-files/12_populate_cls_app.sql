/*

	This script populates the CLS application tables from staging tables
    
*/
set group_concat_max_len = 5000;

ALTER TABLE cls3.Question_Conditions AUTO_INCREMENT = 1;
ALTER TABLE cls3.Question_Choice_Prescriptions AUTO_INCREMENT = 1;
ALTER TABLE cls3.Question_Choices AUTO_INCREMENT = 1;
ALTER TABLE cls3.Questions AUTO_INCREMENT = 1;

ALTER TABLE cls3.prescriptions AUTO_INCREMENT = 1;
ALTER TABLE cls3.clause_prescriptions AUTO_INCREMENT = 1;
ALTER TABLE cls3.clauses AUTO_INCREMENT = 1;
ALTER TABLE cls3.clause_fill_ins AUTO_INCREMENT = 1;
--

delete from cls3.Document_Answers;
delete from cls3.Document_Fill_Ins;

delete from cls3.Question_Conditions;
delete from cls3.Question_Choice_Prescriptions;
delete from cls3.Question_Choices;
delete from cls3.Questions;

delete from cls3.Document_Clauses;
delete from cls3.clause_prescriptions;
delete from cls3.prescriptions;
delete from cls3.clause_fill_ins;
delete from cls3.clauses;
--


/*
insert into cls3.question_group_ref ( question_group, question_group_name )
select 
	char(64 + 
    (
    select count(1)
    from 
		(
        select min(id) group_id, group_name
		from temp_questions
		group by group_name
        ) as sub2
	where sub2.group_id <= sub.group_id
	)),
    group_name
from
	(
    select min(id) group_id, group_name
	from temp_questions
	group by group_name
    ) as sub;
*/



insert into cls3.questions (clause_version_id, question_code, question_type, question_text, question_group)
select 
	1 clause_version_id,
	question_name question_code,
    case 
		when question_type = 'Numeric' then 'N'
        when question_type = 'Single Answer' then '1'
        when question_type = 'Multiple Answer' then 'M'
        when question_type = 'Boolean' then 'B'
	end question_type,
    replace(question_text,'''','''''') question_text,
    g.question_group
from temp_questions q
left join cls3.question_group_ref g
	on g.question_group_name = trim(q.group_name)
    order by 2 desc;




insert into cls3.question_choices (question_id, choice_text)
select distinct app_q.question_id, a.choice_text
from temp_possible_answers a
left join temp_questions q
	on a.question_id = q.id
left join cls3.questions app_q
	on app_q.question_code = q.question_name;

insert into cls3.prescriptions (regulation_id, prescription_name, prescription_url)
select distinct 1, left(replace(prescription,' ',''),50), ''
from answers_prescriptions_xref a
where not exists ( select 1 from cls3.prescriptions where upper(prescription_name) = left(upper(replace(prescription,' ','')),50))
	and prescription not in ('--','n','SEEFOLLOW-ONQUESTIONS')
    and prescription != '';
    
    
insert into cls3.question_choice_prescriptions (question_choce_id, prescription_id)
select 
	distinct c.question_choce_id question_choce_id, p.prescription_id    
from answers_prescriptions_xref a
join cls3.prescriptions p
	on p.prescription_name = a.prescription
join temp_possible_answers r
	on r.raw_id = a.raw_id
join temp_questions q
	on q.id = r.question_id
join cls3.questions app_q
	on app_q.Question_Code = q.question_name
join cls3.question_choices c
	on c.choice_text = r.choice_text
    and c.question_id = app_q.question_id ;
    
insert into cls3.question_conditions (question_condition_sequence, question_id, original_condition_text, condition_to_question)
select q.sequence, app_q.question_id, q.rule, q.parsed_rule
from temp_questions q
left join cls3.questions app_q
	on app_q.question_code = q.question_name;

insert into cls3.clauses (clause_version_id, clause_name, clause_section_id, inclusion_cd, regulation_id, clause_title, clause_data, clause_url, effective_date, clause_conditions, clause_rule)
select 1, o.clause_number, 
	ifnull((select clause_section_id from cls3.Clause_Sections where Clause_Section_Code = upper(trim(r.section))),9), case when IBR_OR_FULL_TEXT = 'FULL TEXT' then 'F' else 'R' end inclusion_cd, 1, o.title, convert(ifNull(contentHTMLProcessed,contentHTML) using latin1), uri, 
	concat(
		substring_index(eff_date,' ',-1), '-',
        case substring_index(eff_date,' ',1)
			when 'JAN' then '01'
            when 'FEB' then '02'
            when 'MAR' then '03'
            when 'APR' then '04'
            when 'MAY' then '05'
            when 'JUN' then '06'
            when 'JUL' then '07'
            when 'JULY' then '07'
            when 'AUG' then '08'
            when 'SEP' then '09'
            when 'OCT' then '10'
            when 'NOV' then '11'
            when 'DEC' then '12'
		end, '-01'
        )
        , ifnull(c.processed_clause, r.conditions) clause_conditions
        , r.rule
from raw_clauses r 
join 
	(
    select 
		case when effective_date regexp '[0-9][0-9]$' then effective_date end eff_date, o.*
    from
		(
        select 
		case 
			when substring_index(substring_index(content, ')', 1),'(',-1) regexp '[0-9][0-9]$'
			then substring_index(substring_index(content, ')', 1),'(',-1)
            else
				substring_index(substring_index(content, ')', 2),'(',-1)
		end effective_date,
		o.* from temp_official_clauses o
		) as o
    ) as o
	on o.clause_number = upper(r.clause_number)
left join
		(
        select 
			raw_id,
			GROUP_CONCAT(
						case 
							when real_question_name is not null 
								then concat(item_id, ' ', real_question_name, 
									case when operator = '=' then ' IS: ' else ' IS: NOT ' end, 
                                    ifnull(real_value,value))
							when value = 'INCLUDED' and question_name regexp '([0-9]+)\\.+'
								then concat(item_id, ' ', question_name, 
									case when operator = '=' then ' IS: ' else ' IS: NOT ' end, 
                                    ifnull(real_value,value))
							when real_value is not null 
								then concat(item_id, ' ', question_name, 
									case when operator = '=' then ' IS: ' else ' IS: NOT ' end, 
                                    ifnull(real_value,value))
							else c.condition_text 
						end order by item_id
                        SEPARATOR ' ') 
			processed_clause
		from 
			(
            select 
				raw_id, item_id, operator, replace(replace(question_name,']',''),'[','') question_name, replace(replace(real_question_name,']',''),'[','') real_question_name, condition_text, value, real_value
            from temp_clause_conditions
            ) c
        group by raw_id
        ) as c
        on r.id = c.raw_id;
    

insert into cls3.clauses (clause_version_id, clause_name, clause_section_id, inclusion_cd, regulation_id, clause_title, clause_data, clause_url, effective_date, clause_conditions, clause_rule, additional_conditions)
select 
	1 clause_version_id, clause_name, 
    ifnull((select clause_section_id from cls3.Clause_Sections where Clause_Section_Code = upper(trim(r.section))),9), case when IBR_OR_FULL_TEXT = 'FULL TEXT' then 'F' else 'R' end inclusion_cd, 1, o.title, convert(concat(instructions, ' ', alt_contenthtml) using latin1), uri, 
	concat(
		substring_index(eff_date,' ',-1), '-',
        case substring_index(eff_date,' ',1)
			when 'JAN' then '01'
            when 'FEB' then '02'
            when 'MAR' then '03'
            when 'APR' then '04'
            when 'MAY' then '05'
            when 'JUN' then '06'
            when 'JUL' then '07'
			when 'JULY' then '07'
            when 'AUG' then '08'
            when 'SEP' then '09'
            when 'OCT' then '10'
            when 'NOV' then '11'
            when 'DEC' then '12'
		end, '-01'
        )
        , ifnull(c.processed_clause, r.conditions) clause_conditions
        , r.rule
        , r.additional_conditions
from raw_clauses r 
join 
	(
    select 
		case when effective_date regexp '[0-9][0-9]$' then effective_date end eff_date, o.*
    from
		(
        select 
		case 
			when substring_index(substring_index(instructions, ')', 1),'(',-1) regexp '[0-9][0-9]$'
			then substring_index(substring_index(instructions, ')', 1),'(',-1)
            else
				substring_index(substring_index(instructions, ')', 2),'(',-1)
		end effective_date,
        concat(o.clause_number, ' ', alt_number) clause_name,
		o.*, a.alt_number, ifNull(a.contentHTMLProcessed, a.contenthtml) alt_contenthtml, a.instructions
        from temp_official_clauses_alt a
        join temp_official_clauses o
			on a.clause_number = o.clause_number
		) as o
    ) as o
	on o.clause_name = r.clause_number
left join
		(
        select 
			raw_id,
			GROUP_CONCAT(
						case 
							when real_question_name is not null 
								then concat(item_id, ' ', real_question_name, 
									case when operator = '=' then ' IS: ' else ' IS: NOT ' end, 
                                    ifnull(real_value,value))
							when value = 'INCLUDED' and question_name regexp '([0-9]+)\\.+'
								then concat(item_id, ' ', question_name, 
									case when operator = '=' then ' IS: ' else ' IS: NOT ' end, 
                                    ifnull(real_value,value))
							when real_value is not null 
								then concat(item_id, ' ', question_name, 
									case when operator = '=' then ' IS: ' else ' IS: NOT ' end, 
                                    ifnull(real_value,value))
							else c.condition_text 
						end order by item_id
                        SEPARATOR ' ') 
			processed_clause
		from 
			(
            select 
				raw_id, item_id, operator, replace(replace(question_name,']',''),'[','') question_name, replace(replace(real_question_name,']',''),'[','') real_question_name, condition_text, value, real_value
            from temp_clause_conditions
            ) c
        group by raw_id
        ) as c
        on r.id = c.raw_id;

UPDATE cls3.clauses
        INNER JOIN raw_clauses
             ON  cls3.clauses.Clause_Name = raw_clauses.CLAUSE_NUMBER
SET cls3.clauses.Additional_Conditions = raw_clauses.ADDITIONAL_CONDITIONS;
    
update cls3.clauses set is_active = '1';

update cls3.clauses
set clause_data = substring(clause_data, locate('</h1>', clause_data)+5)
where locate('</h1>', clause_data)!=0;

insert into cls3.prescriptions (regulation_id, prescription_name, prescription_url)
select distinct 1, left(replace(prescription,' ',''),50), ''
from clause_prescriptions_xref r
where not exists ( select 1 from cls3.prescriptions where upper(trim(prescription_name)) = left(trim(replace(prescription,' ','')),50))
	and prescription not in ('--','n','SEE FOLLOW-ON QUESTIONS')
    and prescription != '';
    

insert into cls3.clause_prescriptions (clause_id, prescription_id)
select distinct c.clause_id, p.prescription_id
from raw_clauses r
join cls3.clauses c
	on upper(r.clause_number) = upper(c.clause_name)
join clause_prescriptions_xref pres_xref
	on pres_xref.raw_id = r.id
join cls3.prescriptions p
	on p.prescription_name = pres_xref.prescription;

update cls3.clauses 
set regulation_id = 1
where clause_name like '52%';

update cls3.clauses 
set regulation_id = 2
where clause_name like '252%';


-- Updates prescription URIs to point to ECFR
update cls3.prescriptions
join
    (
	select 
		prescription_id,
		concat('http://www.ecfr.gov/cgi-bin/text-idx?node=se48.', volume , '.', part, '_1', subpart, replace(suffix, '-', '_6')) uri
	from
		(
		select 
			prescription_id,
			prescription_name,
			subpart,
			part,
			case when suffix like '%(%' then left(suffix,locate('(',suffix)-1) else suffix end suffix,
			case when part regexp '^[0-9]+$'
				then 
					case 
						when cast(part as UNSIGNED) >= '1' and cast(part as UNSIGNED) <= '51' then 1 
						when cast(part as UNSIGNED) >= '52' and cast(part as UNSIGNED) <= '99' then 2
						when cast(part as UNSIGNED) >= '200' and cast(part as UNSIGNED) <= '299' then 3
					end 
            end volume
		from 
			(
			select 
				prescription_id,
				prescription_name,
				substring_index(substring_index(prescription_name,'(', 1),'.',1) part,
				substring_index(substring_index(substring_index(prescription_name,'(', 1),'.',-1),'-',1) subpart,
                case 
					when locate('-',prescription_name) != 0 
						then right(prescription_name, length(prescription_name) - locate('-',prescription_name)+1) 
					else ''
				end suffix
			from cls3.prescriptions
			) prescriptions
		) prescriptions
    ) sub
    on sub.prescription_id = prescriptions.prescription_id
set prescriptions.prescription_url = ifNull(sub.uri,'')
where ifNull(prescription_url,'') = '';
    
    


-- 

-- Update one-off effective dates
update cls3.clauses c
join (
	select 
		clause_id, case when effective_date regexp '[0-9][0-9]$' then effective_date end eff_date
	from
		(
		select 
			clause_id,
            case 
				when substring_index(substring_index(clause_data, ')', 1),'(',-1) regexp '[0-9][0-9]$'
				then substring_index(substring_index(clause_data, ')', 1),'(',-1)
				else
					substring_index(substring_index(clause_data, ')', 2),'(',-1)
			end effective_date 
		from cls3.clauses where effective_date is null
		) as sub
	) as sub
    on sub.clause_id = c.clause_id
set effective_date =
	concat(
			substring_index(eff_date,' ',-1), '-',
			case substring_index(eff_date,' ',1)
				when 'JAN' then '01'
				when 'FEB' then '02'
				when 'MAR' then '03'
				when 'APR' then '04'
				when 'MAY' then '05'
				when 'JUN' then '06'
				when 'JUL' then '07'
				when 'JULY' then '07'
				when 'AUG' then '08'
				when 'SEP' then '09'
				when 'OCT' then '10'
				when 'NOV' then '11'
				when 'DEC' then '12'
			end, '-01'
			)
where effective_date is null;

-- 
    
insert into cls3.clause_fill_ins (clause_id, Fill_In_Code, Fill_In_Type, Fill_In_Max_Size)
select a_c.Clause_Id, Fill_In_Code, Fill_In_Type, Fill_In_Max_Size
from temp_clause_fill_ins f
join temp_official_clauses c
	on f.clause_id = c.id
join cls3.clauses a_c
	on a_c.clause_name = c.clause_number;


insert into cls3.clause_fill_ins (clause_id, Fill_In_Code, Fill_In_Type, Fill_In_Max_Size)
select a_c.Clause_Id, Fill_In_Code, Fill_In_Type, Fill_In_Max_Size
from temp_clause_alt_fill_ins f
join temp_official_clauses_alt c
	on f.clause_id = c.id
join cls3.clauses a_c
	on a_c.clause_name = concat(c.clause_number, ' ', c.alt_number);


--

select * from cls3.Question_Conditions;
select * from cls3.Question_Choice_Prescriptions;
select * from cls3.Question_Choices;
select * from cls3.Questions;

select * from cls3.prescriptions;
select * from cls3.clause_prescriptions;
select * from cls3.clauses;
select * from cls3.clause_fill_ins;

--
