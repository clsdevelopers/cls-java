/*

	This script replaces the fill-ins for clauses pulled form ECFR with the proper format

*/
    
drop procedure if exists replace_fillins;

DELIMITER $$
 
CREATE PROCEDURE replace_fillins (IN paramName VARCHAR(255), IN paramType VARCHAR(255), IN paramSize int)
BEGIN
DECLARE v_clause_number TEXT;
DECLARE v_finished INTEGER DEFAULT 0;
DECLARE v_id INTEGER DEFAULT 0;
DECLARE v_clause TEXT character set UTF8 default ""; 
DECLARE v_processed TEXT character set UTF8 default ""; 
DECLARE INDX INT;
DECLARE v_ind int;
DECLARE v_fill_in_code varchar(200);
 DECLARE SLICE TEXT character set UTF8 default ""; 

 
DEClARE clause_cursor CURSOR FOR
select id, ifNull(contentHTMLProcessed, contentHTMLFillins) contentHTMLFillins, clause_number
from temp_official_clauses 
where contentHTMLFillins like concat('%{{', paramName, '}}%'); 

-- declare NOT FOUND handler
DECLARE CONTINUE HANDLER
        FOR NOT FOUND SET v_finished = 1;
 
OPEN clause_cursor;
 
get_clause: LOOP
  SET INDX = 1;
  set v_ind = 0;
  set v_processed = '';
	FETCH clause_cursor INTO v_id, v_clause, v_clause_number;
	 
	MyLoop: WHILE INDX != 0 DO
		 -- GET THE INDEX OF THE FIRST OCCURENCE OF THE SPLIT CHARACTER
		 SET INDX = LOCATE(concat('{{',paramName,'}}'),v_clause);
		 -- NOW PUSH EVERYTHING TO THE LEFT OF IT INTO THE SLICE VARIABLE
		 IF INDX !=0 THEN
		  SET SLICE = LEFT(v_clause,INDX - 1);
          SET v_fill_in_code = concat(paramName , '_', v_clause_number, '[' , v_ind, ']');
          SET v_processed = concat(v_processed, SLICE, '{{', v_fill_in_code, '}}');
		  insert into temp_clause_fill_ins (clause_id, Fill_In_Code, Fill_In_Type, Fill_In_Max_Size)
			values (v_id, v_fill_in_code, paramType, paramSize);
		 ELSE
		  SET SLICE = v_clause;
		  SET v_processed = concat(v_processed, SLICE);
		 END IF;
		 -- CHOP THE ITEM REMOVED OFF THE MAIN STRING
		 SET v_clause = RIGHT(v_clause,CHAR_LENGTH(v_clause) - INDX-(CHAR_LENGTH(paramName) + 3));
         
     
         
		 -- BREAK OUT IF WE ARE DONE
         SET v_ind = v_ind + 1;
		 IF LENGTH(v_clause) = 0 THEN
		  LEAVE MyLoop;
		 END IF;

	END WHILE;
    
	IF v_finished = 1 THEN
		LEAVE get_clause;
	END IF;
    
    update temp_official_clauses
    set contentHTMLProcessed = v_processed
    where id = v_id;
    
END LOOP get_clause;
 
CLOSE clause_cursor;
 
END$$
 
DELIMITER ;



drop procedure if exists replace_alt_fillins;

DELIMITER $$
 
CREATE PROCEDURE replace_alt_fillins (IN paramName VARCHAR(255), IN paramType VARCHAR(255), IN paramSize int)
BEGIN
DECLARE v_clause_number TEXT;
DECLARE v_finished INTEGER DEFAULT 0;
DECLARE v_id INTEGER DEFAULT 0;
DECLARE v_clause TEXT character set UTF8 default ""; 
DECLARE v_processed TEXT character set UTF8 default ""; 
DECLARE INDX INT;
DECLARE v_ind int;
DECLARE v_fill_in_code varchar(200);
 DECLARE SLICE TEXT character set UTF8 default ""; 

 
DEClARE clause_cursor CURSOR FOR
select id, contentHTMLFillins, clause_number
from temp_official_clauses_alt
where contentHTMLFillins like concat('%{{', paramName, '}}%'); 

-- declare NOT FOUND handler
DECLARE CONTINUE HANDLER
        FOR NOT FOUND SET v_finished = 1;
 
OPEN clause_cursor;
 
get_clause: LOOP
  SET INDX = 1;
  set v_ind = 0;
  set v_processed = '';
	FETCH clause_cursor INTO v_id, v_clause, v_clause_number;
	 
	MyLoop: WHILE INDX != 0 DO
		 -- GET THE INDEX OF THE FIRST OCCURENCE OF THE SPLIT CHARACTER
		 SET INDX = LOCATE(concat('{{',paramName,'}}'),v_clause);
		 -- NOW PUSH EVERYTHING TO THE LEFT OF IT INTO THE SLICE VARIABLE
		 IF INDX !=0 THEN
		  SET SLICE = LEFT(v_clause,INDX - 1);
          SET v_fill_in_code = concat(paramName , '_', v_clause_number, '[' , v_ind, ']');
          SET v_processed = concat(v_processed, SLICE, '{{', v_fill_in_code, '}}');
		  insert into temp_clause_alt_fill_ins (clause_id, Fill_In_Code, Fill_In_Type, Fill_In_Max_Size)
			values (v_id, v_fill_in_code, paramType, paramSize);
		 ELSE
		  SET SLICE = v_clause;
		  SET v_processed = concat(v_processed, SLICE);
		 END IF;
		 -- CHOP THE ITEM REMOVED OFF THE MAIN STRING
		 SET v_clause = RIGHT(v_clause,CHAR_LENGTH(v_clause) - INDX- (CHAR_LENGTH(paramName) + 3));
         
     
         
		 -- BREAK OUT IF WE ARE DONE
         SET v_ind = v_ind + 1;
		 IF LENGTH(v_clause) = 0 THEN
		  LEAVE MyLoop;
		 END IF;

	END WHILE;
    
	IF v_finished = 1 THEN
		LEAVE get_clause;
	END IF;
    
    update temp_official_clauses_alt
    set contentHTMLProcessed = v_processed
    where id = v_id;
    
END LOOP get_clause;
 
CLOSE clause_cursor;
 
END$$
 
DELIMITER ;

--
truncate table temp_clause_fill_ins;
truncate table temp_clause_alt_fill_ins;
update temp_official_clauses set contentHTMLProcessed = null;
update temp_official_clauses_alt set contentHTMLProcessed = null;
--

CALL replace_fillins('textbox','S',100);
CALL replace_alt_fillins('textbox','S',100);
CALL replace_fillins('checkbox','C',null);
CALL replace_alt_fillins('checkbox','C',null);

--

update temp_official_clauses set contentHTMLProcessed = replace(replace(contentHTMLProcessed,'″','"'),'□','');
update temp_official_clauses_alt set contentHTMLProcessed = replace(contentHTMLProcessed,'″','"');
update temp_official_clauses set contentHTMLProcessed = replace(replace(contentHTMLProcessed,'”','"'),'□','');
update temp_official_clauses_alt set contentHTMLProcessed = replace(contentHTMLProcessed,'“','"');

update temp_official_clauses set contentHTMLProcessed = replace(contentHTML,'″','"') where contentHTMLProcessed is null;
update temp_official_clauses_alt set contentHTMLProcessed = replace(contentHTML,'″','"') where contentHTMLProcessed is null;

--





