CREATE TABLE `temp_clause_alt_fill_ins` (
  `id` mediumint(9) NOT NULL AUTO_INCREMENT,
  `clause_id` int(11) DEFAULT NULL,
  `Fill_In_Code` varchar(200) DEFAULT NULL,
  `Fill_In_Type` varchar(100) DEFAULT NULL,
  `Fill_In_Max_Size` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=281 DEFAULT CHARSET=latin1;

CREATE TABLE `temp_clause_conditions` (
  `id` mediumint(9) NOT NULL AUTO_INCREMENT,
  `raw_id` mediumint(9) DEFAULT NULL,
  `condition_text` varchar(1000) DEFAULT NULL,
  `condition_text_processed` text,
  `question_name` varchar(500) DEFAULT NULL,
  `value` varchar(500) DEFAULT NULL,
  `item_id` varchar(100) DEFAULT NULL,
  `real_question_name` varchar(200) DEFAULT NULL,
  `real_question_id` int(11) DEFAULT NULL,
  `operator` varchar(10) DEFAULT NULL,
  `real_value` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ix_clause_cond_qname` (`question_name`)
) ENGINE=InnoDB AUTO_INCREMENT=5860 DEFAULT CHARSET=latin1;

CREATE TABLE `temp_question_dependencies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `question_id` int(11) DEFAULT NULL,
  `referenced_question_id` int(11) DEFAULT NULL,
  `referenced_question_name` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=125831 DEFAULT CHARSET=latin1;

CREATE TABLE `temp_possible_answers` (
  `id` mediumint(9) NOT NULL AUTO_INCREMENT,
  `question_id` mediumint(9) NOT NULL,
  `raw_id` mediumint(9) NOT NULL,
  `choice_text2` varchar(500) DEFAULT NULL,
  `choice_text` varchar(500) DEFAULT NULL,
  `soundex_hash` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ix_choice_text` (`choice_text`),
  KEY `ix_soundex_choice` (`soundex_hash`)
) ENGINE=InnoDB AUTO_INCREMENT=1540 DEFAULT CHARSET=latin1;

CREATE TABLE `temp_questions` (
  `id` mediumint(9) NOT NULL AUTO_INCREMENT,
  `raw_id` mediumint(9) NOT NULL,
  `question_name` varchar(1000) DEFAULT NULL,
  `question_text` varchar(1000) DEFAULT NULL,
  `question_type` varchar(1000) DEFAULT NULL,
  `parsed_rule` varchar(1000) DEFAULT NULL,
  `rule` varchar(1000) DEFAULT NULL,
  `type` varchar(1000) DEFAULT NULL,
  `group_name` varchar(100) DEFAULT NULL,
  `category` varchar(100) DEFAULT NULL,
  `sequence` int(11) DEFAULT NULL,
  `root_question_id` int(11) DEFAULT NULL,
  `normal_rule` varchar(1000) DEFAULT NULL,
  `latest_parent_question_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ix_root_question_id` (`root_question_id`)
) ENGINE=InnoDB AUTO_INCREMENT=321 DEFAULT CHARSET=latin1;

CREATE TABLE `temp_official_clauses` (
  `id` mediumint(9) NOT NULL AUTO_INCREMENT,
  `clause_number` varchar(100) DEFAULT NULL,
  `title` varchar(500) CHARACTER SET utf8 DEFAULT NULL,
  `prescription` varchar(100) DEFAULT NULL,
  `uri` varchar(500) DEFAULT NULL,
  `content` text CHARACTER SET utf8,
  `contentHTML` text CHARACTER SET utf8,
  `contentHTMLFillIns` text CHARACTER SET utf8,
  `contentHTMLProcessed` text CHARACTER SET utf8,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1088 DEFAULT CHARSET=latin1;

CREATE TABLE `temp_official_clauses_alt` (
  `id` mediumint(9) NOT NULL AUTO_INCREMENT,
  `clause_number` varchar(100) DEFAULT NULL,
  `alt_number` varchar(100) DEFAULT NULL,
  `instructions` text CHARACTER SET utf8,
  `content` text CHARACTER SET utf8,
  `contentHTML` text CHARACTER SET utf8,
  `contentHTMLFillIns` text CHARACTER SET utf8,
  `contentHTMLProcessed` text CHARACTER SET utf8,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=257 DEFAULT CHARSET=latin1;

CREATE TABLE `answers_prescriptions_xref` (
  `id` mediumint(9) NOT NULL AUTO_INCREMENT,
  `raw_id` mediumint(9) NOT NULL,
  `prescription` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `raw_clauses` (
  `id` mediumint(9) NOT NULL AUTO_INCREMENT,
  `SECTION` varchar(10) DEFAULT NULL,
  `IBR_OR_FULL_TEXT` varchar(200) DEFAULT NULL,
  `FILL_IN` varchar(200) DEFAULT NULL,
  `CLAUSE` text CHARACTER SET utf8,
  `EFFECTIVE_CLAUSE_DATE` varchar(100) DEFAULT NULL,
  `CLAUSE_PRESCRIPTION` varchar(1000) DEFAULT NULL,
  `PRESCRIPTION_TEXT` text,
  `CONDITIONS` text CHARACTER SET utf8,
  `RULE` varchar(2000) CHARACTER SET utf8 DEFAULT NULL,
  `PRESC_OR_CLAUSE` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `CLAUSE_NUMBER` varchar(100) DEFAULT NULL,
  `ADDITIONAL_CONDITIONS` varchar(2000) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1535 DEFAULT CHARSET=latin1;

CREATE TABLE `raw_questions` (
  `id` mediumint(9) NOT NULL AUTO_INCREMENT,
  `col001` text CHARACTER SET utf8,
  `col002` text CHARACTER SET utf8,
  `tab_name` varchar(100) DEFAULT NULL,
  `category` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8191 DEFAULT CHARSET=latin1;

CREATE TABLE `clause_prescriptions_xref` (
  `id` mediumint(9) NOT NULL AUTO_INCREMENT,
  `raw_id` mediumint(9) NOT NULL,
  `prescription` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ix_clause_prescriptions_xref` (`prescription`)
) ENGINE=InnoDB AUTO_INCREMENT=27216 DEFAULT CHARSET=latin1;

CREATE TABLE `temp_clause_fill_ins` (
  `id` mediumint(9) NOT NULL AUTO_INCREMENT,
  `clause_id` int(11) DEFAULT NULL,
  `Fill_In_Code` varchar(200) DEFAULT NULL,
  `Fill_In_Type` varchar(100) DEFAULT NULL,
  `Fill_In_Max_Size` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1359 DEFAULT CHARSET=latin1;

