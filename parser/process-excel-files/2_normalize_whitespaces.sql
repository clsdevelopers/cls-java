drop procedure if exists normalize_string;

DELIMITER $$
/*
	Replaces consecutive occurences of string with a single occurence
*/
CREATE PROCEDURE normalize_string (table_name varchar(500), column_name varchar(100), string_to_normalize varchar(50))
BEGIN
	DECLARE	update_sql varchar(1000);
	DECLARE row_count int;
	SET @sqlText = concat(
		'update ', table_name, 
        ' set ', column_name, ' = replace(', column_name, ',''', string_to_normalize, string_to_normalize, ''',''', string_to_normalize, ''')');
        
	MyLoop: WHILE TRUE DO
		PREPARE stmt FROM @sqlText;
		EXECUTE stmt;
        set row_count = ROW_COUNT();
        
        IF row_count = 0 THEN
		  LEAVE MyLoop;
		END IF;
	END WHILE;
    
END$$
 
DELIMITER ;
