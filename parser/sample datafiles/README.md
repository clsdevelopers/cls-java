3.3 Questions.xls 	- Spreadsheet as provided by client (with formatting etc)
	questions.csv 	- Generated csv file that will be used for processing


FARDFAR.xls		- Clauses spreadsheet as provided by client (with formatting etc)
	FAR.csv		- Generated csv file that will be used for processing
	DFAR.csv	- Generated csv file that will be used for processing

official_clauses.csv		- Main clauses as scraped from ECFR
official_clauses_alt.csv	- Alternate clauses as scraped from ECFR