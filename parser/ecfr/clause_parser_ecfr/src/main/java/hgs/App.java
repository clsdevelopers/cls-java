package hgs;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.csv.QuoteMode;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.parser.Parser;
import org.jsoup.select.Elements;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;

import com.gargoylesoftware.htmlunit.BrowserVersion;

public class App {
	private static final String NEW_LINE_SEPARATOR 	= "\n";
	private static final String OUTPUT_FILE_MAIN 	= "/Users/kdesautels/desktop/official_clauses.csv";
	private static final String OUTPUT_FILE_ALT 	= "/Users/kdesautels/desktop/official_clauses_alts.csv";
	private static final String [] FILE_HEADER 		= {"clause_number","title","prescription","content","contentHTML","contentHTMLFillin","uri"};
	private static final String [] FILE_HEADER_ALT 	= {"clause_number","instructions","content"};
	
	private static final String[] CLAUSE_INDEX_LINKS = {
		// FARs link - 52
		"http://www.ecfr.gov/cgi-bin/text-idx?SID=f8aabe9535f2d6107402fc92b6b6092c&mc=true&tpl=/ecfrbrowse/Title48/48cfr52_main_02.tpl"
		// DFARs link - 252
		, "http://www.ecfr.gov/cgi-bin/text-idx?SID=cb2ebc4e4c2358875acbdd0b0b468310&tpl=/ecfrbrowse/Title48/48cfr252_main_02.tpl"
	};
	
	public static void main(String[] args) {
		try {
			downloadAll();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// test();
		System.out.println("\nEND OF PARSING"); //added this so it's clear that application isn't hung
	}
	
	public static void downloadAll() throws Exception {

		List<String[]> clauses = getClauseLinks(CLAUSE_INDEX_LINKS);
		System.out.println("clause level: " + clauses.size());
	
		FileWriter fileWriter = null;
		CSVPrinter csvFilePrinter = null;	  
		
		//Create the CSVFormat object with "\n" as a record delimiter
		CSVFormat csvFileFormat = CSVFormat.DEFAULT
				.withRecordSeparator(NEW_LINE_SEPARATOR)
				.withQuoteMode(QuoteMode.ALL)
				.withDelimiter(',')
				.withEscape('\"');
		
		FileWriter fileWriterAlt = null;
		CSVPrinter csvFilePrinterAlt = null;	         
		
		List<String> clauseRec;
		List<String> clauseAltRec;
		String index;
		try {
	    	fileWriter = new FileWriter(OUTPUT_FILE_MAIN);
	    	fileWriterAlt = new FileWriter(OUTPUT_FILE_ALT);
	    	
	    	csvFilePrinter = new CSVPrinter(fileWriter, csvFileFormat);
	    	csvFilePrinterAlt = new CSVPrinter(fileWriterAlt, csvFileFormat);

			for(String[] clause: clauses) {
				System.out.println("pulling clause " + clause[0]);
				Map<String, String> results = pullClause(clause[0], clause[1]);
				
				clauseRec = new ArrayList<String>();
				
				// Basic clauses
				for(String col: FILE_HEADER) {
					clauseRec.add(results.get(col));
				}
				
				csvFilePrinter.printRecord(clauseRec);
				
				// Alternative clauses
				for(String key: results.keySet()) {
					if (key.contains("alternateInstructions_")) {
						clauseAltRec = new ArrayList<String>();
						index = key.substring(key.indexOf("_"));
						clauseAltRec.add(results.get("clause_number"));
						clauseAltRec.add(results.get("alternateNumber"+index));
						clauseAltRec.add(results.get(key));
						clauseAltRec.add(results.get("alternateExtract" + index));
						clauseAltRec.add(results.get("alternateExtractHTML" + index));
						clauseAltRec.add(results.get("alternateExtractHTMLFillin" + index));
						csvFilePrinterAlt.printRecord(clauseAltRec);
					}
				}
				
			}
		} finally {
			try {
				fileWriter.flush();
				fileWriter.close();
				fileWriterAlt.flush();
				fileWriterAlt.close();
			} catch (IOException e) {
				e.printStackTrace();
			}	             
		}
	}

	public static List<String[]> getClauseLinks(String[] parentLinks) {
		WebDriver driver = new HtmlUnitDriver();
		String expression = "//table[@width='120']//td/a";
		List<String[]> nextLinks = new ArrayList<String[]>();
		
		String[] entry;
		for(String parentLink: parentLinks) {
			driver.get(parentLink);
			List<WebElement> nextAnchors = driver.findElements(By.xpath(expression));
		
			for(WebElement el: nextAnchors) {
				entry = new String[2];
				entry[0] = el.getText();
				entry[1] = el.getAttribute("href");
				nextLinks.add(entry);
			}
		}
		driver.quit();
		return nextLinks;
	}
	
	public static void test() {
		pullClause("test","http://www.ecfr.gov/cgi-bin/text-idx?SID=e8278e248a9cfe87dbdf12209cf10f01&mc=true&node=se48.3.252_1232_67010&rgn=div8");
		
	}
	
	public static String getInnerHtml(WebDriver driver, WebElement el) {
		String contents = (String)((JavascriptExecutor)driver).executeScript("return arguments[0].innerHTML;", el);
		// System.out.println("*******" + contents + "*******");
		return contents;
	}
	
	public static String parseFillinsAndNormalize(String html) {
		String contents;
		String replace;
		
		html = html.replaceAll("<div class=\"fpdash\"><span class=\"fpdash\">([^<]*)</span></div>", "$1 {{textbox}}");
		html = html.replaceAll("<p>_+</p>", "<p>{{textbox}}</p>");
		

		
		html = html.replaceAll("Attachment _", "Attachment {{textbox}}");
		
		// one offs where there are no leading underscores
		replace = "\\[<span style=\"font-style:italic\">insert name of SBA's contractor</span>\\]";
		html = html.replaceAll(replace, "{{textbox}} " + replace);
		
		replace = "\\[<span style=\"font-style:italic\">insert name of contracting agency</span>\\]";
		html = html.replaceAll(replace, "{{textbox}} " + replace);
		
		replace = "\\[<span style=\"font-style:italic\">insert telephone number</span>\\]";
		html = html.replaceAll(replace, "{{textbox}} " + replace);
		
		replace = "\\[<span style=\"font-style:italic\">state purpose</span>\\]";
		html = html.replaceAll(replace, "{{textbox}} " + replace);
		
		replace = "\\[<span style=\"font-style:italic\">Insert current budget rate here.</span>\\]";
		html = html.replaceAll(replace, "{{textbox}} " + replace);

		replace = "\\[<span style=\"font-style:italic\">Contracting Officer to insert source of rate</span>\\]";
		html = html.replaceAll(replace, "{{textbox}} " + replace);
		
		replace = "\\[<span style=\"font-style:italic\">Contracting Officer to insert applicable point of contact information cited in PGI 237.171-3(b).</span>\\]";
		html = html.replaceAll(replace, "{{textbox}} " + replace);
		
		replace = "\\[<span style=\"font-style:italic\">Contracting Officer to list applicable excepted materials or indicate “none”</span>\\]";
		html = html.replaceAll(replace, "{{textbox}} " + replace);
		
		replace = "\\[<span style=\"font-style:italic\">Insert portion of labor rate attributable to profit.</span>\\]";
		html = html.replaceAll(replace, "{{textbox}} " + replace);
		
		html = normalizeHtml(html);
		
		html = html.replaceAll("_(_)+", "{{textbox}}");
		html = html.replaceAll("_( )+", "{{textbox}} ");
		html = html.replaceAll("\\(   \\)", "{{checkbox}}");
		html = html.replaceAll("\\[( )+\\]", "{{checkbox}}");
		html = html.replaceAll("□", "{{checkbox}}");
		html = html.replaceAll("☐", "{{checkbox}}");
		html = html.replaceAll("\\[( )+\\]", "{{checkbox}}");
		html = html.replaceAll("[_]", "{{checkbox}}");
		html = html.replaceAll("\\*(_)+", "{{textbox}}");

		System.out.println(html);
		return html;
	}
	
	
	public static Map<String, String> pullClause(String clauseNumber, String clauseLink) {
		HtmlUnitDriver driver = new HtmlUnitDriver(BrowserVersion.FIREFOX_24);
		driver.setJavascriptEnabled(true);
		
		Map<String, String> results = new HashMap<String, String>();

		results.put("clause_number", clauseNumber);
		results.put("uri", clauseLink);
		// Name of the element being parsed
		String elementName;
		// Value of the element being parsed
		StringBuilder elementValue = new StringBuilder();
		// Expression to identity element in HTML
		String expression;

		driver.get(clauseLink);
		
		//----------------------------------------
		
		elementName = "content";
		expression = "//div[@class='extract' and not(preceding-sibling::p[.//span[starts-with(., 'Alternate') or starts-with(., 'Alterate')]][1])"
				+ "and not(preceding-sibling::h3[contains(.,'End of clause')][1])]";
		List<WebElement> text = driver.findElements(By.xpath(expression));
		
		String html = "";
		String parsedFillinsHtml = "";
		for(WebElement el: text) {
			html = getInnerHtml(driver, el);	
			elementValue.append(el.getText().replace("\"", "\"\""));
		}
		
		expression = "//div[not(preceding-sibling::p[.//span[starts-with(., 'Alternate') or starts-with(., 'Alterate')]][1])"
				+ "and not(preceding-sibling::h3[contains(.,'End of clause')][1])"
				+ "and (preceding-sibling::div[@class='extract'][1])]";
		text = driver.findElements(By.xpath(expression));
		
		for(WebElement el: text) {
			html = html + getInnerHtml(driver, el);		
			elementValue.append(el.getText().replace("\"", "\"\""));
		}
		results.put(elementName+"HTML", html);
		
		//do not setup fill-ins for false flag fill-ins (clauses that are not fill-ins with strings
		//that will hit as fill-ins (such as ____________ )
		ArrayList<String> falseFillins = new ArrayList<String>();
		falseFillins.add("252.243-7002");
		if(falseFillins.contains(clauseNumber))	
			results.put(elementName+"HTMLFillin", normalizeHtml(html));
		else
			results.put(elementName+"HTMLFillin", parseFillinsAndNormalize(html)); //this was the original line of code
		//end catching false flag fill-ins
		
		results.put(elementName, elementValue.toString());
		
		//----------------------------------------
		Pattern pattern;
		Matcher matcher;
		elementName = "alternateInstructions";
		expression = "//p[.//span[starts-with(., 'Alternate') or starts-with(., 'Alterate')] and parent::div[not(@class='extract')]]";
		text = driver.findElements(By.xpath(expression));
		
		int alternateIndex = 0;
		WebElement extract;
		for(WebElement el: text) {
			elementName = "alternateInstructions";
			// Save Alternate Instructions
			elementValue.setLength(0);
			if(el.getText().trim().length()!=0) 
				elementValue.append(el.getText().replace("\"", "\"\""));
			results.put(elementName+"_"+alternateIndex, elementValue.toString());
			
			// Alternate Number
			pattern = Pattern.compile("(Alternate|Alterate) ([a-zA-Z]+)");
			matcher = pattern.matcher(elementValue);
			
			if (matcher.find()){
				System.out.println(matcher.group(2));
				results.put("alternateNumber_"+alternateIndex, "Alternate " + matcher.group(2));
			}
			
			// Some Alternate Instructions don't have "extracts", so find immediate sibling and check if it's an extract
			extract = el.findElement(By.xpath("following-sibling::*"));
			if(extract.getTagName().equals("div")) {
				
				// Alternate Extract
				elementName = "alternateExtract";
				elementValue.setLength(0);
				if(el.getText().trim().length()!=0) 
					elementValue.append(el.getText().replace("\"", "\"\""));
				results.put(elementName+"_"+alternateIndex, elementValue.toString());
				
				// Alternate HTML
				html = getInnerHtml(driver, extract);
				results.put(elementName+"HTML_"+alternateIndex, html);
				results.put(elementName+"HTMLFillin_"+alternateIndex, parseFillinsAndNormalize(html));
				System.out.println(html);
			}

			alternateIndex++;
		}
		/*
		elementName = "alternateExtract";
		expression = "//p[.//span[starts-with(., 'Alternate')] and parent::div[not(@class='extract')]]/following-sibling::div[@class='extract'][1]";
		//expression = "//div[@class='extract' and (preceding-sibling::p[.//span[starts-with(., 'Alternate')]][1])]";
		text = driver.findElements(By.xpath(expression));
     
		i = 0;
		for(WebElement el: text) {
			elementValue.setLength(0);
			elementValue.append(el.getText().replace("\"", "\"\""));
			results.put(elementName+"_"+(i++), elementValue.toString());
		}
		*/
		//----------------------------------------
		
		elementName = "title";
		expression = "//h2";
		text = driver.findElements(By.xpath(expression));

		elementValue.setLength(0);
		for(WebElement el: text) {
			if(el.getText().trim().length()!=0)
				elementValue.append(el.getText().replace("\"", "\"\""));
		}
		pattern = Pattern.compile("([a-zA-Z0-9]+)\\s+(.*)");
		matcher = pattern.matcher(elementValue);
		if (matcher.find())
			results.put(elementName, matcher.group(2));
		
		//----------------------------------------
		
		elementName = "prescription";
		expression = "//div[@class='extract']/preceding-sibling::p[starts-with(., 'As prescribed')][1]";
		text = driver.findElements(By.xpath(expression));

		elementValue.setLength(0);
		for(WebElement el: text) {
			if(el.getText().trim().length()!=0)
				elementValue.append(el.getText().replace("\"", "\"\""));
		}
		pattern = Pattern.compile("As prescribed (in|at) (.*?)( |,)");
		matcher = pattern.matcher(elementValue);
		if (matcher.find())
			results.put(elementName, matcher.group(2));
		
		driver.quit();
		return results;
	}
	
	//The actual standardization. Uses jsoup to transform a string of html based on CLS conventions.
	private static String normalizeHtml(String html) {
		Document doc = Jsoup.parse(html, "", Parser.xmlParser());
		doc.select("span[style~=font-weight:\\s*bold]").tagName("b");
		doc.select("span[style~=font-style:\\s*italic]").tagName("i");
		Elements allElements = doc.select("*");
		allElements.removeAttr("class");
		allElements.removeAttr("style");
		allElements.removeAttr("font");
		allElements.removeAttr("span");
		doc.select("span").unwrap();
		doc.select("class").unwrap();
		doc.select("style").unwrap();
		doc.select("font").unwrap();

		doc.outputSettings().prettyPrint(false); //don't want extra linebreaks added.
		return doc.toString();
	}
	
}