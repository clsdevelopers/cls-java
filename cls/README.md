## MySQL Account
Account:	ClsApp
<br />
Password:	Cls-App_804.580.2588

## For password encryption, refer to:
http://www.jasypt.org/index.html

### For jasypt tutorial
http://blog.teamextension.com/quick-jasypt-spring-3-tutorial-626

### For Encrypting from the command line
http://www.jasypt.org/cli.html

Download (http://www.jasypt.org/download.html) and unzip the Jasypt distribution, then open a command line at at the bin directory. Encrypt the password by running

	encrypt input="localhost" password="1qaz@WSX" algorithm="PBEWITHMD5ANDDES"
	

To test its description:

	decrypt input="5HPhAsyQJcfd70dIHnguV8RpKrYOaeY6" password="1qaz@WSX" algorithm="PBEWITHMD5ANDDES"
<br /><br />
encrypt input="ClsApp" password="1qaz@WSX" algorithm="PBEWITHMD5ANDDES"
<br />
decrypt input="TLT9xngORiDxUHMjvROj2w==" password="1qaz@WSX" algorithm="PBEWITHMD5ANDDES"
<br /><br />
encrypt input="Cls-App_804.580.2588" password="1qaz@WSX" algorithm="PBEWITHMD5ANDDES"
<br />
decrypt input="oSNnYWXS189sB7AqFFNnA/j282ZVbkwxR8b2vs3aVjU=" password="1qaz@WSX" algorithm="PBEWITHMD5ANDDES"
<br /><br />
encrypt input="cls3_beta" password="1qaz@WSX" algorithm="PBEWITHMD5ANDDES"
<br />
decrypt input="kB/wEdktMmrjfy17axMD69mZTg/XCGwi" password="1qaz@WSX" algorithm="PBEWITHMD5ANDDES"

## Database Changes
### Add Password field to Users table
ALTER TABLE cls3_beta.users ADD (Password VARCHAR(120) NULL);

## Run Configuration
In Eclipse 'Run Configuration' > Environment, add following

Variable:	PASSWORD_ENV_VARIABLE

Value:	1qaz@WSX

## Log4j configuration

Not required, but to write logs update log4j.properties file location.
Redirect to a local directory structure:

log4j.appender.A1.file.file=${catalina.home}/logs/cls.log

##Test-Branch
