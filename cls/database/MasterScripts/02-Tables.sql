USE cls3;

-- ----------------------------------------------
CREATE TABLE Doc_Type_Ref
(
	Document_Type        VARCHAR(1) NOT NULL,
	Document_Type_Name   VARCHAR(20) NOT NULL,
	Created_At           TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	Updated_At           TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	PRIMARY KEY (Document_Type)
);

CREATE UNIQUE INDEX XAK1Document_Type_Ref_Name ON Doc_Type_Ref
(
	Document_Type_Name
);

-- ----------------------------------------------
CREATE TABLE Fill_In_Type_Ref
(
	Fill_In_Type         VARCHAR(1) NOT NULL,
	Fill_In_Type_Name    VARCHAR(20) NOT NULL,
	Created_At           TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	Updated_At           TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	PRIMARY KEY (Fill_In_Type)
);

CREATE UNIQUE INDEX XAK1Fill_In_Type_Reference ON Fill_In_Type_Ref
(
	Fill_In_Type_Name
);

-- ----------------------------------------------
CREATE TABLE Inclusion_Ref
(
	Inclusion_Cd         VARCHAR(1) NOT NULL,
	Inclusion_Name       VARCHAR(20) NOT NULL,
	Created_At           TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	Updated_At           TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	PRIMARY KEY (Inclusion_Cd)
);

-- ----------------------------------------------
CREATE TABLE Question_Type_Ref
(
	Question_Type        VARCHAR(1) NOT NULL,
	Question_Type_Name   VARCHAR(20) NOT NULL,
	Created_At           TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	Updated_At           TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	PRIMARY KEY (Question_Type)
);

CREATE UNIQUE INDEX XAK1Question_Type_Reference ON Question_Type_Ref
(
	Question_Type_Name
);

-- ----------------------------------------------
CREATE TABLE Question_Group_Ref
(
	Question_Group       VARCHAR(1) NOT NULL,
	Question_Group_Name  VARCHAR(50) NOT NULL,
	Created_At           TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	Updated_At           TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	PRIMARY KEY (Question_Group)
);

CREATE UNIQUE INDEX XAK1Question_Group_Ref ON Question_Group_Ref
(
	Question_Group_Name
);

-- ----------------------------------------------
CREATE TABLE Roles
(
	Role_Id              SMALLINT NOT NULL,
	Role_Name            VARCHAR(20) NOT NULL,
	Created_At           TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	Updated_At           TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	PRIMARY KEY (Role_Id)
);

CREATE UNIQUE INDEX XAK1Roles ON Roles
(
	Role_Name
);

-- ----------------------------------------------
CREATE TABLE Regulations
(
	Regulation_Id        SMALLINT NOT NULL,
	Regulation_Name      VARCHAR(20) NOT NULL,
	Regulation_Title     VARCHAR(100) NOT NULL,
	Regulation_Url       VARCHAR(160) NULL,
	Created_At           TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	Updated_At           TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	PRIMARY KEY (Regulation_Id)
);

CREATE UNIQUE INDEX XAK1Regulations ON Regulations
(
	Regulation_Name
);

-- ----------------------------------------------
CREATE TABLE Clause_Sections
(
	Clause_Section_Id    SMALLINT NOT NULL,
	Clause_Section_Code  VARCHAR(1) NOT NULL,
	Clause_Section_Header VARCHAR(80) NOT NULL,
	Created_At           TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	Updated_At           TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	PRIMARY KEY (Clause_Section_Id)
);

CREATE UNIQUE INDEX XAKClause_Sections_Code ON Clause_Sections
(
	Clause_Section_Code
);

-- ----------------------------------------------
CREATE TABLE Prescriptions
(
	Prescription_Id      SMALLINT AUTO_INCREMENT,
	Regulation_Id        SMALLINT NOT NULL,
	Prescription_Name    VARCHAR(60) NOT NULL,
	Prescription_Url     VARCHAR(120) NULL,
	Prescription_Content TEXT NULL,
	Created_At           TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	Updated_At           TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	PRIMARY KEY (Prescription_Id)
);

CREATE UNIQUE INDEX XAKPrescriptions_Name ON Prescriptions
(
	Prescription_Name
);

CREATE INDEX XIFPrescriptions_Regulation ON Prescriptions
(
	Regulation_Id
);

-- ----------------------------------------------
CREATE TABLE Sys_Last_Table_Change_At
(
	Table_Name           VARCHAR(64) NOT NULL,
	Last_Change_At       TIMESTAMP NULL,
	PRIMARY KEY (Table_Name)
);

-- ----------------------------------------------
CREATE TABLE Depts
(
	Dept_Id              SMALLINT NOT NULL,
	Dept_Name            VARCHAR(120) NOT NULL,
	Created_At           datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
	Updated_At           TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	PRIMARY KEY (Dept_Id)
);

CREATE UNIQUE INDEX XAKDepartments_Name ON Depts
(
	Dept_Name
);

-- ----------------------------------------------
CREATE TABLE Orgs
(
	Org_Id               SMALLINT AUTO_INCREMENT,
	Org_Name             VARCHAR(120) NOT NULL,
	Org_Description      VARCHAR(1000) NULL,
	Org_Source_Id        integer NULL,
	Org_Source_Parent_Id integer NULL,
	Org_Source_Level     NUMERIC(4) NOT NULL,
	Dept_Id              SMALLINT NULL,
	Created_At           TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	Updated_At           TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	PRIMARY KEY (Org_Id)
);

CREATE UNIQUE INDEX XAKOrgs_Name ON Orgs
(
	Org_Name
);

CREATE INDEX XIFOrgs_Department ON Orgs
(
	Dept_Id
);

-- ----------------------------------------------
CREATE TABLE Org_Regulations
(
	Org_Regulation_Id    SMALLINT AUTO_INCREMENT,
	Org_Id               SMALLINT NOT NULL,
	Regulation_Id        SMALLINT NOT NULL,
	Created_At           TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	Updated_At           TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	PRIMARY KEY (Org_Regulation_Id)
);

CREATE UNIQUE INDEX XAK1Org_Regulations ON Org_Regulations
(
	Org_Id,
	Regulation_Id
);

CREATE INDEX XIFOrg_Regulations_Regulation_Id ON Org_Regulations
(
	Regulation_Id
);

-- ----------------------------------------------
CREATE TABLE Clause_Versions
(
	Clause_Version_Id    SMALLINT AUTO_INCREMENT,
	Clause_Version_Name  VARCHAR(20) NOT NULL,
	Clause_Version_Date  TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	Clause_Version_Remarks VARCHAR(255) NULL,
	Is_Active            boolean NOT NULL DEFAULT 0,
	FAC_Number 			VARCHAR(20) NULL,
	DAC_Number			VARCHAR(20) NULL,
	DFARS_Number		VARCHAR(50) NULL,
	FAC_Date 			DATE,
	DAC_Date 			DATE,
	DFARS_Date 			DATE,
	Created_At           datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
	Updated_At           TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	PRIMARY KEY (Clause_Version_Id)
);

CREATE UNIQUE INDEX XAKClause_Versions_Name ON Clause_Versions
(
	Clause_Version_Name
);

-- ----------------------------------------------
CREATE TABLE Clauses
(
	Clause_Id            integer AUTO_INCREMENT,
	Clause_Version_Id    SMALLINT NOT NULL,
	Clause_Name          varchar(50) NULL,
	Inclusion_Cd         VARCHAR(1) NOT NULL,
	Clause_Title         VARCHAR(500) NOT NULL,
	Clause_Data          text NULL,
	Clause_Url           VARCHAR(160) NULL,
	Clause_Section_Id    SMALLINT NOT NULL,
	Effective_Date       date NULL,
	Regulation_Id        SMALLINT NOT NULL,
	Clause_Conditions    VARCHAR(5000) NULL,
	Clause_Rule          VARCHAR(5000) NULL,
	Condition_To_Clause  VARCHAR(255) NULL,
	Additional_Conditions VARCHAR(2000) NULL,
	Is_Active            boolean NOT NULL DEFAULT 0,
	Is_Editable          boolean NOT NULL DEFAULT 1,
	Editable_Remarks     VARCHAR(1500) NULL,
	Is_Optional          boolean NOT NULL DEFAULT 0,
	Provision_Or_Clause  VARCHAR(1) NULL,
	Is_Basic_Clause      boolean NOT NULL DEFAULT 0,
	Commercial_Status    VARCHAR(50) NULL,
	Optional_Status    	 VARCHAR(50) NULL,
	Optional_Conditions  VARCHAR(250) NULL,
	Is_Dfars_Activity    boolean NOT NULL DEFAULT 0,
	Created_At           TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	Updated_At           TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	PRIMARY KEY (Clause_Id)
);

CREATE INDEX XIFClauses_Clause_Section_Id ON Clauses
(
	Clause_Section_Id
);

CREATE INDEX XIFClauses_Version_Id ON Clauses
(
	Clause_Version_Id
);

CREATE INDEX XIFClauses_Regulation_Id ON Clauses
(
	Regulation_Id
);

CREATE INDEX XIFClauses_Inclusion_Code ON Clauses
(
	Inclusion_Cd
);

-- ----------------------------------------------
CREATE TABLE Clause_Prescriptions
(
	Clause_Prescription_Id integer AUTO_INCREMENT,
	Clause_Id            integer NOT NULL,
	Prescription_Id      SMALLINT NOT NULL,
	Created_At           TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	Updated_At           TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	PRIMARY KEY (Clause_Prescription_Id)
);

CREATE UNIQUE INDEX XAK1Clause_Prescriptions ON Clause_Prescriptions
(
	Clause_Id,
	Prescription_Id
);

CREATE INDEX XIFClause_Prescriptions_Prescription_Id ON Clause_Prescriptions
(
	Prescription_Id
);

-- ----------------------------------------------
CREATE TABLE Clause_Fill_Ins
(
	Clause_Fill_In_Id    integer AUTO_INCREMENT,
	Clause_Id            integer NOT NULL,
	Fill_In_Code         varchar(50) NOT NULL,
	Fill_In_Type         VARCHAR(1) NOT NULL,
	Fill_In_Max_Size     SMALLINT NULL,
	Fill_In_Group_Number TINYINT NULL,
	Fill_In_Placeholder  VARCHAR(1000) NULL,
	Fill_In_Default_Data TEXT NULL,
	Fill_In_Display_Rows NUMERIC(2) NULL,
	Fill_In_For_Table    boolean NOT NULL DEFAULT 0,
	Fill_In_Heading      VARCHAR(80) NULL,
	Created_At           TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	Updated_At           TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	PRIMARY KEY (Clause_Fill_In_Id)
);

CREATE INDEX XIE1Clause_Fill_Ins ON Clause_Fill_Ins
(
	Clause_Id,
	Fill_In_Code,
	Fill_In_Group_Number
);

CREATE INDEX XIFClause_Fill_Ins_Fill_In_Type ON Clause_Fill_Ins
(
	Fill_In_Type
);

-- ----------------------------------------------
CREATE TABLE Questions
(
	Question_Id          integer AUTO_INCREMENT,
	Clause_Version_Id    SMALLINT NOT NULL,
	Question_Code        VARCHAR(100) NOT NULL,
	Question_Type        VARCHAR(1) NOT NULL,
	Question_Text        VARCHAR(500) NOT NULL,
	Question_Group       VARCHAR(1) NULL,
	Is_Active            boolean NOT NULL DEFAULT 1,
	Created_At           TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	Updated_At           TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	PRIMARY KEY (Question_Id)
);

CREATE UNIQUE INDEX XAKQuestions_Code ON Questions
(
	Clause_Version_Id,
	Question_Code
);

CREATE INDEX XIFQuestions_Question_Type ON Questions
(
	Question_Type
);

-- ----------------------------------------------
CREATE TABLE Question_Choices
(
	Question_Choce_Id    integer AUTO_INCREMENT,
	Question_Id          integer NOT NULL,
	Choice_Text          VARCHAR(350) NOT NULL,
	Prompt_After_Selection VARCHAR(255) NULL,
	Created_At           TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	Updated_At           TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	PRIMARY KEY (Question_Choce_Id)
);

/* 11/14/2015 Removed due to Error Code of 1071: Specified key was too long; max key length is 767 bytes
CREATE UNIQUE INDEX XAK1Question_Choices ON Question_Choices
(
	Question_Id,
	Choice_Text
);
*/

-- ----------------------------------------------
CREATE TABLE Question_Choice_Prescriptions
(
	Question_Choice_Prescription_Id INTEGER AUTO_INCREMENT,
	Question_Choce_Id    integer NOT NULL,
	Prescription_Id      SMALLINT NOT NULL,
	Created_At           TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	Updated_At           TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	PRIMARY KEY (Question_Choice_Prescription_Id)
);

CREATE UNIQUE INDEX XAKQuestion_Choice_Prescriptions ON Question_Choice_Prescriptions
(
	Question_Choce_Id,
	Prescription_Id
);

CREATE INDEX XIFQuestion_Choice_Prescriptions_Prescription_Id ON Question_Choice_Prescriptions
(
	Prescription_Id
);

-- ----------------------------------------------
CREATE TABLE Question_Conditions
(
	Question_Condition_Id integer AUTO_INCREMENT,
	Question_Condition_Sequence integer NOT NULL,
	Question_Id          integer NOT NULL,
	Parent_Question_Id   integer NULL,
	Default_Baseline boolean NOT NULL DEFAULT 0,
	Original_Condition_Text VARCHAR(1000) NULL,
	Condition_To_Question VARCHAR(400) NULL,
	Created_At           TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	Updated_At           TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	PRIMARY KEY (Question_Condition_Id)
);

CREATE INDEX XIFQuestion_Conditions_Question_Id ON Question_Conditions
(
	Question_Id
);

CREATE INDEX XIF2Question_Conditions ON Question_Conditions
(
	Parent_Question_Id
);

-- ----------------------------------------------
CREATE TABLE Users
(
	User_Id              integer AUTO_INCREMENT,
	Email                varchar(254) NOT NULL,
	First_Name           VARCHAR(60) NULL,
	Last_Name            VARCHAR(60) NULL,
	Password             VARCHAR(120) NULL,
	Password_Updated_At  datetime NULL,
	Logon_Count          integer NULL DEFAULT 0,
	Current_Logon_At     datetime NULL,
	Current_Logon_Ip     VARCHAR(45) NULL,
	Last_Logon_At        datetime NULL,
	last_logon_ip        VARCHAR(45) NULL,
	Reset_Password_Token VARCHAR(80) NULL,
	Reset_Password_Sent_At datetime NULL,
	Cac_Hash             VARCHAR(32) NULL,
	Time_Zone            VARCHAR(32) NULL,
	Org_Id               SMALLINT NULL,
	Is_Active            boolean NOT NULL DEFAULT 1,
	Created_At           TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	Updated_At           TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	PRIMARY KEY (User_Id)
);

CREATE UNIQUE INDEX XAKUsers_Email ON Users
(
	Email
);

CREATE INDEX XIFUsers_Org_Id ON Users
(
	Org_Id
);

CREATE UNIQUE INDEX XAKUsers_Cac_Hash ON Users
(
	Cac_Hash
);

-- ----------------------------------------------
CREATE TABLE User_Roles
(
	User_Role_Id         integer AUTO_INCREMENT,
	User_Id              integer NOT NULL,
	Role_Id              SMALLINT NOT NULL,
	Created_At           TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	Updated_At           TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	PRIMARY KEY (User_Role_Id)
);

CREATE UNIQUE INDEX XAKUser_Roles_User_Role ON User_Roles
(
	User_Id,
	Role_Id
);

CREATE INDEX XIFUser_Roles_User_Role ON User_Roles
(
	Role_Id
);

-- ----------------------------------------------
CREATE TABLE Documents
(
	Document_Id          integer AUTO_INCREMENT,
	Document_Type        VARCHAR(1) NOT NULL,
	Acquisition_Title    varchar(100) NULL,
	Document_Number      VARCHAR(50) NOT NULL,
	User_Id              integer NULL,
	Doc_Status_Cd        CHAR NOT NULL DEFAULT 'I',
	Is_Interview_Completed boolean NOT NULL DEFAULT 0,
	Contract_Number      VARCHAR(17) NULL,
	Order_Number         varchar(255) NULL,
	Solicitation_Id      integer NULL,
	Document_Full_Name   VARCHAR(100) NULL,
	Clause_Version_Id    SMALLINT NOT NULL,
	Application_Id       integer NULL,
	Document_Date        DATE NULL DEFAULT NULL,
	Last_Question_Code   VARCHAR(2000) NULL,
	Created_At           TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	Updated_At           TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	PRIMARY KEY (Document_Id)
);

CREATE INDEX XIFDocuments_User_Id ON Documents
(
	User_Id
);

CREATE INDEX XIFDocuments_Clause_Version_Id ON Documents
(
	Clause_Version_Id
);

CREATE INDEX XIFDocuments_Application_Id ON Documents
(
	Application_Id
);

CREATE INDEX XIFDocuments_Document_Type ON Documents
(
	Document_Type
);

CREATE INDEX XIFDocuments_Soliticitation_Id ON Documents
(
	Solicitation_Id
);

CREATE INDEX XIF6Documents ON Documents
(
	Doc_Status_Cd
);

-- ----------------------------------------------
CREATE TABLE Document_Answers
(
	Document_Answer_Id   integer AUTO_INCREMENT,
	Document_Id          integer NOT NULL,
	Question_Id          integer NOT NULL,
	Question_Answer      VARCHAR(20000) NOT NULL,
	Created_At           TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	Updated_At           TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	PRIMARY KEY (Document_Answer_Id)
);

CREATE UNIQUE INDEX XAK1Document_Answers ON Document_Answers
(
	Document_Id,
	Question_Id
);

CREATE INDEX XIFDocument_Answers_Question_Id ON Document_Answers
(
	Question_Id
);

-- ----------------------------------------------
CREATE TABLE Document_Answer_Clauses
(
	Document_Answer_Id   integer NOT NULL,
	Clause_Id            integer NOT NULL,
	PRIMARY KEY (Document_Answer_Id,Clause_Id)
);

CREATE INDEX XIFDocument_Answer_Clauses_Clause_Id ON Document_Answer_Clauses
(
	Clause_Id
);

-- ----------------------------------------------
CREATE TABLE Document_Clauses
(
	Document_Id          integer NOT NULL,
	Clause_Id            integer NOT NULL,
	Is_Fillin_Completed  boolean NOT NULL DEFAULT 0,
--	Is_Removed_By_User   boolean NOT NULL DEFAULT 0,
	Optional_User_Action_Code VARCHAR(1) NULL CHECK ( Optional_User_Action_Code IN (NULL, 'R', 'A') ),
	Created_At           TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	Updated_At           TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	PRIMARY KEY (Document_Id,Clause_Id)
);

CREATE INDEX XIFDocument_Clauses_Clause_Id ON Document_Clauses
(
	Clause_Id
);

-- ----------------------------------------------
CREATE TABLE Document_Fill_Ins
(
	Document_Fill_In_Id  integer AUTO_INCREMENT,
	Document_Id          integer NOT NULL,
	Clause_Fill_In_Id    integer NOT NULL,
	Document_Fill_In_Answer TEXT NOT NULL,
	Full_Text_Modified   boolean NOT NULL DEFAULT 0,
	Created_At           TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	Updated_At           TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	PRIMARY KEY (Document_Fill_In_Id)
);

CREATE INDEX XIFDocument_Fill_Ins_Document_Id ON Document_Fill_Ins
(
	Document_Id
);

CREATE INDEX XIFDocument_Fill_Ins_Clause_Fill_In_Id ON Document_Fill_Ins
(
	Clause_Fill_In_Id
);

-- ----------------------------------------------
CREATE TABLE OAuth_Applications
(
	Application_Id       integer AUTO_INCREMENT,
	Application_Name     varchar(255) NOT NULL DEFAULT '',
	Application_Uid      varchar(255) NOT NULL DEFAULT '',
	Application_Secret   varchar(255) NOT NULL DEFAULT '',
	Redirect_Uri         VARCHAR(160) NOT NULL DEFAULT '',
	Org_Id               SMALLINT NULL,
	Created_At           TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	Updated_At           TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	PRIMARY KEY (Application_Id)
);

CREATE INDEX XIFOAuth_Applications_Org_Id ON OAuth_Applications
(
	Org_Id
);

CREATE UNIQUE INDEX XAKOAuth_Applications_UID ON OAuth_Applications
(
	Application_Uid
);

/*
-- ----------------------------------------------
CREATE TABLE OAuth_Access_Grants
(
	OAuth_Access_Grant_Id integer AUTO_INCREMENT,
	Resource_Owner_Id    integer NOT NULL DEFAULT 0,
	Application_Id       integer NOT NULL,
	Token                varchar(255) NOT NULL DEFAULT '',
	Redirect_Uri         VARCHAR(160) NOT NULL DEFAULT '',
	Expires_In           integer NOT NULL DEFAULT 0,
	Scopes               varchar(255) NULL,
	Created_At           TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	Updated_At           TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	PRIMARY KEY (OAuth_Access_Grant_Id)
);

CREATE INDEX XIFOAuth_Access_Grants_Application_Id ON OAuth_Access_Grants
(
	Application_Id
);

-- ----------------------------------------------
CREATE TABLE OAuth_Access_Tokens
(
	OAuth_Access_Token_Id integer AUTO_INCREMENT,
	Resource_Owner_Id    integer NULL,
	Application_Id       integer NULL,
	Token                varchar(255) NOT NULL DEFAULT '',
	Refresh_Token        varchar(255) NULL,
	Expires_In           integer NULL,
	Scopes               varchar(255) NULL,
	Created_At           TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	Updated_At           TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	PRIMARY KEY (OAuth_Access_Token_Id)
);

CREATE INDEX XIFOAuth_Access_Tokens_Application_Id ON OAuth_Access_Tokens
(
	Application_Id
);
*/

-- ----------------------------------------------
CREATE TABLE Audit_Events
(
	Audit_Event_Id       integer AUTO_INCREMENT,
	Action               VARCHAR(80) NOT NULL,
	User_Id              integer NULL,
	Initiated_User_Id    integer NULL,
	Ref_User_Name		 VARCHAR(100) NOT NULL,
	Ref_Application_Id 	 integer NULL,
	Ref_Document_Id      integer NULL,
	Ref_Document_Number  VARCHAR(100) NOT NULL,
	Additional_Data      text(65535) NULL,
	Document_Id          integer NULL,
	Clause_Id            integer NULL,
	Question_Id          integer NULL,
	Org_Id               SMALLINT NULL,
	Application_Id       integer NULL,
	Created_At           TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	Updated_At           TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	PRIMARY KEY (Audit_Event_Id)
);

CREATE INDEX XIFAudit_Events_User_Id ON Audit_Events
(
	User_Id
);

CREATE INDEX XIFAudit_Events_Document_Id ON Audit_Events
(
	Document_Id
);

CREATE INDEX XIFAudit_Events_Question_Id ON Audit_Events
(
	Question_Id
);

CREATE INDEX XIFAudit_Events_Clause_Id ON Audit_Events
(
	Clause_Id
);

CREATE INDEX XIFAudit_Events_Org_Id ON Audit_Events
(
	Org_Id
);

CREATE INDEX XIFAudit_Events_Application_Id ON Audit_Events
(
	Application_Id
);

CREATE INDEX XIFAudit_Events_Initiated_User_Id ON Audit_Events
(
	Initiated_User_Id
);

-- ----------------------------------------------
CREATE TABLE OAuth_Access
(
	Oauth_Access_Id      INTEGER AUTO_INCREMENT,
	Application_Id       integer NOT NULL,
	Access_Token         VARCHAR(255) NOT NULL,
	Created_At           TIMESTAMP NOT NULL,
	Expires_At           TIMESTAMP NOT NULL,
	PRIMARY KEY (Oauth_Access_Id)
);

CREATE UNIQUE INDEX XAK1OAuth_Access ON OAuth_Access
(
	Access_Token
);

CREATE INDEX XIFOAuth_Access_Application_Id ON OAuth_Access
(
	Application_Id
);


-- ----------------------------------------------
CREATE TABLE OAuth_Interview
(
	Oauth_Interview_Id   INTEGER NOT NULL AUTO_INCREMENT,
	Application_Id       integer NOT NULL,
	Oauth_Access_Id      INTEGER NULL,
	Document_Id          integer NOT NULL,
	Interview_Token      VARCHAR(255) NULL,
	Expires_At           TIMESTAMP NULL,
	Created_At           TIMESTAMP NOT NULL,
	Launched_At          TIMESTAMP NULL,
	PRIMARY KEY (Oauth_Interview_Id)
);

CREATE INDEX XIEOAuth_Interview_Token ON OAuth_Interview
(
	Interview_Token
);

CREATE INDEX XIFOAuth_Interview_Application_Id ON OAuth_Interview
(
	Application_Id
);

CREATE INDEX XIFOAuth_Interview_Document_Id ON OAuth_Interview
(
	Document_Id
);

CREATE INDEX XIFOAuth_Interview_Oauth_Access_Id ON OAuth_Interview
(
	Oauth_Access_Id
);

/* 11/16/2015 KH: Moved followings to 03-RI.sql
ALTER TABLE OAuth_Interview
ADD FOREIGN KEY OA_Int_Application_Id (Application_Id) REFERENCES OAuth_Applications (Application_Id)
		ON DELETE CASCADE;
		
ALTER TABLE OAuth_Interview
ADD FOREIGN KEY OA_Int_Document_Id (Document_Id) REFERENCES Documents (Document_Id)
		ON DELETE CASCADE;
*/
		
-- ==============================================
CREATE TABLE Clause_Version_Changes
(
	Clause_Ver_Change_Id INTEGER AUTO_INCREMENT,
	Clause_Version_Id    SMALLINT NOT NULL,
	Previous_Clause_Version_Id SMALLINT NULL,
	Is_Question_Or_Clause CHAR NOT NULL CHECK (Is_Question_Or_Clause IN ('C', 'Q')),
	Question_Or_Clause_Code VARCHAR(100) NOT NULL,
	Change_Code          VARCHAR(10) NOT NULL,
	Change_Brief         VARCHAR(500) NOT NULL,
	Created_At           DATETIME NULL DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY (Clause_Ver_Change_Id)
);


CREATE INDEX XIE1Clause_Version_Changes ON Clause_Version_Changes
(
	Clause_Version_Id,
	Is_Question_Or_Clause,
	Question_Or_Clause_Code
);

ALTER TABLE Clause_Version_Changes
ADD FOREIGN KEY R_Clause_Versions_Changes (Clause_Version_Id) REFERENCES Clause_Versions (Clause_Version_Id)
		ON DELETE CASCADE;

ALTER TABLE Clause_Version_Changes
ADD FOREIGN KEY R_Clause_Version_Changes_Previous (Previous_Clause_Version_Id) REFERENCES Clause_Versions (Clause_Version_Id)
		ON DELETE CASCADE;

-- ==============================================
CREATE TABLE Doc_Status_Ref
(
	Doc_Status_Cd        CHAR NOT NULL,
	Doc_Status_Name      VARCHAR(20) NOT NULL,
	Created_At           DATETIME NULL DEFAULT CURRENT_TIMESTAMP,
	Updated_At           DATETIME NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	PRIMARY KEY (Doc_Status_Cd)
);
