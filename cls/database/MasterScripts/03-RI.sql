ALTER TABLE Audit_Events
ADD FOREIGN KEY R_Users_Audit_Events (User_Id) REFERENCES Users (User_Id);

ALTER TABLE Audit_Events
ADD FOREIGN KEY R_Documents_Audit_Events (Document_Id) REFERENCES Documents (Document_Id);

ALTER TABLE Audit_Events
ADD FOREIGN KEY R_Questions_Audit_Events (Question_Id) REFERENCES Questions (Question_Id);

ALTER TABLE Audit_Events
ADD FOREIGN KEY R_Clauses_Audit_Events (Clause_Id) REFERENCES Clauses (Clause_Id);

ALTER TABLE Audit_Events
ADD FOREIGN KEY R_Orgs_Audit_Events (Org_Id) REFERENCES Orgs (Org_Id);

ALTER TABLE Audit_Events
ADD FOREIGN KEY R_OAuth_Applications_Audit_Events (Application_Id) REFERENCES OAuth_Applications (Application_Id);

ALTER TABLE Audit_Events
ADD FOREIGN KEY R_Users_Audit_Event_Initiated (Initiated_User_Id) REFERENCES Users (User_Id);

ALTER TABLE Clause_Fill_Ins
ADD FOREIGN KEY R_Fill_In_Type_Clause_Fill_In (Fill_In_Type) REFERENCES Fill_In_Type_Ref (Fill_In_Type);

ALTER TABLE Clause_Fill_Ins
ADD FOREIGN KEY R_Clauses_Fill_In (Clause_Id) REFERENCES Clauses (Clause_Id)
  ON DELETE CASCADE;

ALTER TABLE Clause_Prescriptions
ADD FOREIGN KEY R_Clauses_Clause_Prescriptions (Clause_Id) REFERENCES Clauses (Clause_Id)
  ON DELETE CASCADE;

ALTER TABLE Clause_Prescriptions
ADD FOREIGN KEY R_Prescriptions_Clause_Prescriptions (Prescription_Id) REFERENCES Prescriptions (Prescription_Id);

ALTER TABLE Clauses
ADD FOREIGN KEY R_Clause_Sections_Clauses (Clause_Section_Id) REFERENCES Clause_Sections (Clause_Section_Id);

ALTER TABLE Clauses
ADD FOREIGN KEY R_Clause_Versions_Clauses (Clause_Version_Id) REFERENCES Clause_Versions (Clause_Version_Id);

ALTER TABLE Clauses
ADD FOREIGN KEY R_Regulations_Clauses (Regulation_Id) REFERENCES Regulations (Regulation_Id);

ALTER TABLE Clauses
ADD FOREIGN KEY R_Inclusion_Ref_Clauses (Inclusion_Cd) REFERENCES Inclusion_Ref (Inclusion_Cd);

/*
ALTER TABLE Document_Answer_Clauses
ADD FOREIGN KEY R_Clauses_Document_Answer_Clauses (Clause_Id) REFERENCES Clauses (Clause_Id);

ALTER TABLE Document_Answer_Clauses
ADD FOREIGN KEY R_Document_Answers_Clauses (Document_Answer_Id) REFERENCES Document_Answers (Document_Answer_Id)
	ON DELETE CASCADE;
*/

ALTER TABLE Document_Clauses
ADD FOREIGN KEY R_Documents_Clauses (Document_Id) REFERENCES Documents (Document_Id)
	ON DELETE CASCADE;

ALTER TABLE Document_Clauses
ADD FOREIGN KEY R_Clauses_Document_Clauses (Clause_Id) REFERENCES Clauses (Clause_Id);

ALTER TABLE Document_Answers
ADD FOREIGN KEY R_Questions_Document_Answers (Question_Id) REFERENCES Questions (Question_Id);

ALTER TABLE Document_Answers
ADD FOREIGN KEY R_Documents_Document_Answers (Document_Id) REFERENCES Documents (Document_Id)
  ON DELETE CASCADE;

ALTER TABLE Document_Fill_Ins
ADD FOREIGN KEY R_Documents_Fill_Ins (Document_Id) REFERENCES Documents (Document_Id)
  ON DELETE CASCADE;

ALTER TABLE Document_Fill_Ins
ADD FOREIGN KEY R_Clause_Fill_In_Document_Fill_In (Clause_Fill_In_Id) REFERENCES Clause_Fill_Ins (Clause_Fill_In_Id);

ALTER TABLE Documents
ADD FOREIGN KEY R_Uses_Documents (User_Id) REFERENCES Users (User_Id);

ALTER TABLE Documents
ADD FOREIGN KEY R_Clause_Versions_Documents (Clause_Version_Id) REFERENCES Clause_Versions (Clause_Version_Id);

ALTER TABLE Documents
ADD FOREIGN KEY R_OAuth_Applications_Documents (Application_Id) REFERENCES OAuth_Applications (Application_Id);

ALTER TABLE Documents
ADD FOREIGN KEY R_Document_Type_Documents (Document_Type) REFERENCES Doc_Type_Ref (Document_Type);

ALTER TABLE Documents
ADD FOREIGN KEY R_Documents_Solicitation_Id (Solicitation_Id) REFERENCES Documents (Document_Id);

-- ALTER TABLE OAuth_Access_Grants
-- ADD FOREIGN KEY R_OAuth_Applications_Access_Grants (Application_Id) REFERENCES OAuth_Applications (Application_Id);

-- ALTER TABLE OAuth_Access_Tokens
-- ADD FOREIGN KEY R_OAuth_Applications_Acess_Tokens (Application_Id) REFERENCES OAuth_Applications (Application_Id);

ALTER TABLE OAuth_Applications
ADD FOREIGN KEY R_Orgs_OAuth_Applications (Org_Id) REFERENCES Orgs (Org_Id);

ALTER TABLE Org_Regulations
ADD FOREIGN KEY R_Orgs_Org_Regulations (Org_Id) REFERENCES Orgs (Org_Id);

ALTER TABLE Org_Regulations
ADD FOREIGN KEY R_Regulations_Org_Regulations (Regulation_Id) REFERENCES Regulations (Regulation_Id);

ALTER TABLE Orgs
ADD FOREIGN KEY R_Departments_Orgs (Dept_Id) REFERENCES Depts (Dept_Id);

ALTER TABLE Prescriptions
ADD FOREIGN KEY R_Regulations_Prescriptions (Regulation_Id) REFERENCES Regulations (Regulation_Id);

ALTER TABLE Question_Choice_Prescriptions
ADD FOREIGN KEY R_Question_Choices_Prescriptions (Question_Choce_Id) REFERENCES Question_Choices (Question_Choce_Id)
	ON DELETE CASCADE;

ALTER TABLE Question_Choice_Prescriptions
ADD FOREIGN KEY R_Prescription_Question_Choice (Prescription_Id) REFERENCES Prescriptions (Prescription_Id);

ALTER TABLE Question_Choices
ADD FOREIGN KEY R_Questions_Choices (Question_Id) REFERENCES Questions (Question_Id)
	ON DELETE CASCADE;

ALTER TABLE Question_Conditions
ADD FOREIGN KEY R_Questions_Conditions (Question_Id) REFERENCES Questions (Question_Id)
  ON DELETE CASCADE;

ALTER TABLE Question_Conditions
ADD FOREIGN KEY R_Question_Condition_Parent (Parent_Question_Id) REFERENCES Questions (Question_Id)
  ON DELETE CASCADE;

ALTER TABLE Questions
ADD FOREIGN KEY R_Clause_Versions_Questions (Clause_Version_Id) REFERENCES Clause_Versions (Clause_Version_Id);

ALTER TABLE Questions
ADD FOREIGN KEY R_Question_Type_Ref_Questions (Question_Type) REFERENCES Question_Type_Ref (Question_Type);

ALTER TABLE User_Roles
ADD FOREIGN KEY R_Users_User_Roles (User_Id) REFERENCES Users (User_Id);

ALTER TABLE User_Roles
ADD FOREIGN KEY R_Roles_User_Roles (Role_Id) REFERENCES Roles (Role_Id);

ALTER TABLE Users
ADD FOREIGN KEY R_Orgs_Users (Org_Id) REFERENCES Orgs (Org_Id);

ALTER TABLE OAuth_Access
ADD FOREIGN KEY R_OAuth_Application_Access (Application_Id) REFERENCES OAuth_Applications (Application_Id)
		ON DELETE CASCADE;

-- 11/16/2015 Added
ALTER TABLE OAuth_Interview
ADD FOREIGN KEY R_OAuth_Application_Interview (Application_Id) REFERENCES OAuth_Applications (Application_Id)
		ON DELETE CASCADE;

-- 11/16/2015 Added
ALTER TABLE OAuth_Interview
ADD FOREIGN KEY R_Documents_OAuth_Interview (Document_Id) REFERENCES Documents (Document_Id)
		ON DELETE CASCADE;

-- 11/16/2015 Added
ALTER TABLE OAuth_Interview
ADD FOREIGN KEY R_Oauth_Access_Interview (Oauth_Access_Id) REFERENCES OAuth_Access (Oauth_Access_Id);


ALTER TABLE Clause_Version_Changes
ADD FOREIGN KEY R_Clause_Versions_Changes (Clause_Version_Id) REFERENCES Clause_Versions (Clause_Version_Id)
		ON DELETE CASCADE;

ALTER TABLE Clause_Version_Changes
ADD FOREIGN KEY R_Clause_Version_Changes_Previous (Previous_Clause_Version_Id) REFERENCES Clause_Versions (Clause_Version_Id)
		ON DELETE CASCADE;

ALTER TABLE Documents
ADD FOREIGN KEY R_Doc_Status_Ref_Documents (Doc_Status_Cd) REFERENCES Doc_Status_Ref (Doc_Status_Cd);
