DELIMITER //

CREATE PROCEDURE etlAddPrescription (
	IN pPrescription_Name VARCHAR(60),
	IN pRegulation_Id  INT,
	IN pPrescription_Url VARCHAR(120),
	IN pPrescription_Content TEXT
)
BEGIN
	DECLARE nPrescription_Id INT DEFAULT NULL;
	
	SELECT Prescription_Id INTO nPrescription_Id
	FROM Prescriptions
	WHERE Prescription_Name = pPrescription_Name;
	
	IF (nPrescription_Id IS NULL) THEN
		INSERT INTO Prescriptions (Prescription_Name, Regulation_Id, Prescription_Url, Prescription_Content)
		VALUES (pPrescription_Name, pRegulation_Id, pPrescription_Url, pPrescription_Content);

		SELECT Prescription_Id INTO nPrescription_Id
		FROM Prescriptions
		WHERE Prescription_Name = pPrescription_Name;
	ELSE
		UPDATE Prescriptions
		SET Regulation_Id = pRegulation_Id
		, Prescription_Url = pPrescription_Url
		, Prescription_Content = pPrescription_Content
		WHERE Prescription_Id = nPrescription_Id;
	END IF;
END //

CREATE PROCEDURE etlAddClausePrescriptionByClauseId (
	IN pClause_Id  INT,
	IN pPrescription_Name VARCHAR(60)
)
BEGIN
	DECLARE nPrescription_Id INT DEFAULT NULL;
	DECLARE nClause_Prescription_Id INT DEFAULT NULL;
	
	SELECT Prescription_Id INTO nPrescription_Id
	FROM Prescriptions
	WHERE Prescription_Name = pPrescription_Name;
	
	IF (nPrescription_Id IS NOT NULL) THEN
		SELECT Clause_Prescription_Id INTO nClause_Prescription_Id
		FROM Clause_Prescriptions
		WHERE Clause_Id = pClause_Id
        AND Prescription_Id = nPrescription_Id;

		IF (nClause_Prescription_Id IS NULL) THEN
			INSERT INTO Clause_Prescriptions (Clause_Id, Prescription_Id)
			VALUES (pClause_Id, nPrescription_Id);
        ELSE
			SELECT CONCAT(pPrescription_Name, ': found');
        END IF;

	ELSE
		SELECT CONCAT(pPrescription_Name, ': not found');
	END IF;
END //

CREATE PROCEDURE etlAddQuestionChoicePrescriptionByChoiceId (
	IN pQuestion_Choce_Id  INT,
	IN pPrescription_Name VARCHAR(60)
)
BEGIN
	DECLARE nPrescription_Id INT DEFAULT NULL;
	DECLARE nQuestion_Choice_Prescription_Id INT DEFAULT NULL;
	
	SELECT Prescription_Id INTO nPrescription_Id
	FROM Prescriptions
	WHERE Prescription_Name = pPrescription_Name;
	
	IF (nPrescription_Id IS NOT NULL) THEN
		SELECT Question_Choice_Prescription_Id INTO nQuestion_Choice_Prescription_Id
		FROM Question_Choice_Prescriptions
		WHERE Question_Choce_Id = pQuestion_Choce_Id
        AND Prescription_Id = nPrescription_Id;

		IF (nQuestion_Choice_Prescription_Id IS NULL) THEN
			INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, Prescription_Id)
			VALUES (pQuestion_Choce_Id, nPrescription_Id);
        ELSE
			SELECT CONCAT(pPrescription_Name, ': found');
        END IF;

	ELSE
		SELECT CONCAT(pPrescription_Name, ': not found');
	END IF;
END //

DELIMITER ;