CREATE DATABASE cls3;

USE cls3;

CREATE USER 'cls-app'@'localhost' IDENTIFIED BY 'Cls.DB-App-User01';
GRANT SELECT ON cls3.* TO 'cls-app'@'localhost';
GRANT INSERT ON cls3.* TO 'cls-app'@'localhost';
GRANT UPDATE ON cls3.* TO 'cls-app'@'localhost';
GRANT DELETE ON cls3.* TO 'cls-app'@'localhost';
GRANT EXECUTE ON cls3.* TO 'cls-app'@'localhost';

FLUSH PRIVILEGES;

-- SHOW GRANTS FOR 'cls-app'@'localhost';
