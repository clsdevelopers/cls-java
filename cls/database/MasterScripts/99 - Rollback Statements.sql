
-- ALERT!!!
-- Remember to globally replace cls3_alt_dataload.

SELECT 'USE cls3_alt_dataload;'

UNION

-- STEP 1. Delete all Foreign Keys.

SELECT Concat ('ALTER TABLE ', Table_Name,' DROP FOREIGN KEY ', Constraint_Name, ';' )
FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS
WHERE CONSTRAINT_SCHEMA = 'cls3_alt_dataload'
AND CONSTRAINT_TYPE = 'FOREIGN KEY'

UNION

-- Step 2. Delete all non-primary indexes.

SELECT DISTINCT Concat ('ALTER TABLE ', Table_Name,' DROP INDEX ', Index_Name, ';' ) 
FROM INFORMATION_SCHEMA.STATISTICS 
WHERE INDEX_SCHEMA = 'cls3_alt_dataload'
AND Index_Name <> 'PRIMARY'

UNION


-- Step 3. Remove the autoincrement property before dropping the primary key

SELECT DISTINCT Concat ('ALTER TABLE ', Table_Name, ' CHANGE COLUMN ', Column_name, ' ', Column_name,  ' INT NOT NULL;' ) 
FROM INFORMATION_SCHEMA.STATISTICS 
WHERE INDEX_SCHEMA = 'cls3_alt_dataload'
AND Index_Name = 'PRIMARY'
AND Table_Name NOT LIKE '%_Ref'
AND Table_Name NOT LIKE '%_At'

UNION 

SELECT DISTINCT Concat ('ALTER TABLE ', Table_Name, ' CHANGE COLUMN ', Column_name, ' ', Column_name,  ' VARCHAR(1) NOT NULL;' ) 
FROM INFORMATION_SCHEMA.STATISTICS 
WHERE INDEX_SCHEMA = 'cls3_alt_dataload'
AND Index_Name = 'PRIMARY'
AND Table_Name LIKE '%_Ref'

UNION 

SELECT DISTINCT Concat ('ALTER TABLE ', Table_Name, ' CHANGE COLUMN ', Column_name, ' ', Column_name,  ' VARCHAR(100) NOT NULL;' ) 
FROM INFORMATION_SCHEMA.STATISTICS 
WHERE INDEX_SCHEMA = 'cls3_alt_dataload'
AND Index_Name = 'PRIMARY'
AND Table_Name LIKE '%_At'

UNION


-- Step 4. Delete all primarys keys.

SELECT DISTINCT Concat ('ALTER TABLE ', Table_Name,' DROP PRIMARY KEY;' ) 
FROM INFORMATION_SCHEMA.STATISTICS 
WHERE INDEX_SCHEMA = 'cls3_alt_dataload'
AND Index_Name = 'PRIMARY'

UNION

-- Step 5. Drop all tables.

SELECT DISTINCT Concat ('DROP TABLE ', Table_Name, ';' ) 
FROM INFORMATION_SCHEMA.STATISTICS 
WHERE INDEX_SCHEMA = 'cls3_alt_dataload'

UNION

-- Step 6. Drop all procedures.

SELECT CONCAT('DROP PROCEDURE ', ROUTINE_NAME, ';')
FROM INFORMATION_SCHEMA.ROUTINES 
WHERE ROUTINE_SCHEMA = 'cls3_alt_dataload';
