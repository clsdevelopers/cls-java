-- 2016-05-25-Alter-Fillin-Placeholder.sql
ALTER TABLE Clause_Fill_Ins
CHANGE COLUMN Fill_In_Placeholder Fill_In_Placeholder VARCHAR(1000) NULL DEFAULT NULL ;

-- 2016-06-14-Document_Date.sql
ALTER TABLE Documents 
ADD COLUMN Document_Date DATE NULL DEFAULT NULL;

-- 2016-3-30-New-Prescriptions.sql
INSERT INTO Prescriptions (Prescription_Name, Prescription_Url, Regulation_Id) 
VALUES ('2014-O0009', 'http://www.acq.osd.mil/dpap/policy/policyvault/USA000945-14-DPAP.pdf', 1) 
ON DUPLICATE KEY UPDATE Prescription_Url='http://www.acq.osd.mil/dpap/policy/policyvault/USA000945-14-DPAP.pdf';

INSERT INTO Prescriptions (Prescription_Name, Prescription_Url, Regulation_Id) 
VALUES ('2014-O0009', 'http://www.acq.osd.mil/dpap/policy/policyvault/USA000945-14-DPAP.pdf', 1)
ON DUPLICATE KEY UPDATE Prescription_Url='http://www.acq.osd.mil/dpap/policy/policyvault/USA000945-14-DPAP.pdf';

INSERT INTO Prescriptions (Prescription_Name, Prescription_Url, Regulation_Id) 
VALUES ('2014-O0010', 'http://www.acq.osd.mil/dpap/policy/policyvault/USA000948-14-DPAP.pdf', 1)
ON DUPLICATE KEY UPDATE Prescription_Url='http://www.acq.osd.mil/dpap/policy/policyvault/USA000948-14-DPAP.pdf';

INSERT INTO Prescriptions (Prescription_Name, Prescription_Url, Regulation_Id) 
VALUES ('2014-O0018', 'http://www.acq.osd.mil/dpap/policy/policyvault/USA003897-14-DPAP.pdf', 1)
ON DUPLICATE KEY UPDATE Prescription_Url='http://www.acq.osd.mil/dpap/policy/policyvault/USA003897-14-DPAP.pdf';

INSERT INTO Prescriptions (Prescription_Name, Prescription_Url, Regulation_Id) 
VALUES ('2014-O0020', 'http://www.acq.osd.mil/dpap/dars/archive/policy/policyvault/USA005533-14-DPAP.pdf', 1)
ON DUPLICATE KEY UPDATE Prescription_Url='http://www.acq.osd.mil/dpap/dars/archive/policy/policyvault/USA005533-14-DPAP.pdf';

INSERT INTO Prescriptions (Prescription_Name, Prescription_Url, Regulation_Id) 
VALUES ('2015-O0007', 'http://www.acq.osd.mil/dpap/policy/policyvault/USA007414-14-DPAP.pdf', 1)
ON DUPLICATE KEY UPDATE Prescription_Url='http://www.acq.osd.mil/dpap/policy/policyvault/USA007414-14-DPAP.pdf';

INSERT INTO Prescriptions (Prescription_Name, Prescription_Url, Regulation_Id) 
VALUES ('2016-00001', 'http://www.acq.osd.mil/dpap/policy/policyvault/USA005505-15-DPAP.pdf', 1)
ON DUPLICATE KEY UPDATE Prescription_Url='http://www.acq.osd.mil/dpap/policy/policyvault/USA005505-15-DPAP.pdf';

INSERT INTO Prescriptions (Prescription_Name, Prescription_Url, Regulation_Id) 
VALUES ('2016-0001', 'http://www.acq.osd.mil/dpap/policy/policyvault/USA005505-15-DPAP.pdf', 1)
ON DUPLICATE KEY UPDATE Prescription_Url='http://www.acq.osd.mil/dpap/policy/policyvault/USA005505-15-DPAP.pdf';

INSERT INTO Prescriptions (Prescription_Name, Prescription_Url, Regulation_Id)
VALUES ('2016-0003', 'http://www.acq.osd.mil/dpap/policy/policyvault/USA005618-15-DPAP.pdf', 1)
ON DUPLICATE KEY UPDATE Prescription_Url='http://www.acq.osd.mil/dpap/policy/policyvault/USA005618-15-DPAP.pdf';

INSERT INTO Prescriptions (Prescription_Name, Prescription_Url, Regulation_Id) 
VALUES ('2016-O0003', 'http://www.acq.osd.mil/dpap/policy/policyvault/USA005618-15-DPAP.pdf', 1)
ON DUPLICATE KEY UPDATE Prescription_Url='http://www.acq.osd.mil/dpap/policy/policyvault/USA005618-15-DPAP.pdf';

INSERT INTO Prescriptions (Prescription_Name, Prescription_Url, Regulation_Id) 
VALUES ('PGI204.7108(d)(1)', 'http://www.acq.osd.mil/dpap/dars/pgi/pgi_htm/current/PGI204_71.htm', 1)
ON DUPLICATE KEY UPDATE Prescription_Url='http://www.acq.osd.mil/dpap/dars/pgi/pgi_htm/current/PGI204_71.htm';

INSERT INTO Prescriptions (Prescription_Name, Prescription_Url, Regulation_Id) 
VALUES ('PGI204.7108(d)(10)', 'http://www.acq.osd.mil/dpap/dars/pgi/pgi_htm/current/PGI204_71.htm', 1)
ON DUPLICATE KEY UPDATE Prescription_Url='http://www.acq.osd.mil/dpap/dars/pgi/pgi_htm/current/PGI204_71.htm';

INSERT INTO Prescriptions (Prescription_Name, Prescription_Url, Regulation_Id) 
VALUES ('PGI204.7108(d)(11)', 'http://www.acq.osd.mil/dpap/dars/pgi/pgi_htm/current/PGI204_71.htm', 1)
ON DUPLICATE KEY UPDATE Prescription_Url='http://www.acq.osd.mil/dpap/dars/pgi/pgi_htm/current/PGI204_71.htm';

INSERT INTO Prescriptions (Prescription_Name, Prescription_Url, Regulation_Id) 
VALUES ('PGI204.7108(d)(2)', 'http://www.acq.osd.mil/dpap/dars/pgi/pgi_htm/current/PGI204_71.htm', 1)
ON DUPLICATE KEY UPDATE Prescription_Url='http://www.acq.osd.mil/dpap/dars/pgi/pgi_htm/current/PGI204_71.htm';

INSERT INTO Prescriptions (Prescription_Name, Prescription_Url, Regulation_Id) 
VALUES ('PGI204.7108(d)(3)', 'http://www.acq.osd.mil/dpap/dars/pgi/pgi_htm/current/PGI204_71.htm', 1)
ON DUPLICATE KEY UPDATE Prescription_Url='http://www.acq.osd.mil/dpap/dars/pgi/pgi_htm/current/PGI204_71.htm';

INSERT INTO Prescriptions (Prescription_Name, Prescription_Url, Regulation_Id) 
VALUES ('PGI204.7108(d)(4)', 'http://www.acq.osd.mil/dpap/dars/pgi/pgi_htm/current/PGI204_71.htm', 1)
ON DUPLICATE KEY UPDATE Prescription_Url='http://www.acq.osd.mil/dpap/dars/pgi/pgi_htm/current/PGI204_71.htm';

INSERT INTO Prescriptions (Prescription_Name, Prescription_Url, Regulation_Id) 
VALUES ('PGI204.7108(d)(5)', 'http://www.acq.osd.mil/dpap/dars/pgi/pgi_htm/current/PGI204_71.htm', 1)
ON DUPLICATE KEY UPDATE Prescription_Url='http://www.acq.osd.mil/dpap/dars/pgi/pgi_htm/current/PGI204_71.htm';

INSERT INTO Prescriptions (Prescription_Name, Prescription_Url, Regulation_Id) 
VALUES ('PGI204.7108(d)(6)', 'http://www.acq.osd.mil/dpap/dars/pgi/pgi_htm/current/PGI204_71.htm', 1)
ON DUPLICATE KEY UPDATE Prescription_Url='http://www.acq.osd.mil/dpap/dars/pgi/pgi_htm/current/PGI204_71.htm';

INSERT INTO Prescriptions (Prescription_Name, Prescription_Url, Regulation_Id) 
VALUES ('PGI204.7108(d)(7)', 'http://www.acq.osd.mil/dpap/dars/pgi/pgi_htm/current/PGI204_71.htm', 1)
ON DUPLICATE KEY UPDATE Prescription_Url='http://www.acq.osd.mil/dpap/dars/pgi/pgi_htm/current/PGI204_71.htm';

INSERT INTO Prescriptions (Prescription_Name, Prescription_Url, Regulation_Id) 
VALUES ('PGI204.7108(d)(8)', 'http://www.acq.osd.mil/dpap/dars/pgi/pgi_htm/current/PGI204_71.htm', 1)
ON DUPLICATE KEY UPDATE Prescription_Url='http://www.acq.osd.mil/dpap/dars/pgi/pgi_htm/current/PGI204_71.htm';

INSERT INTO Prescriptions (Prescription_Name, Prescription_Url, Regulation_Id) 
VALUES ('PGI204.7108(d)(9)', 'http://www.acq.osd.mil/dpap/dars/pgi/pgi_htm/current/PGI204_71.htm', 1)
ON DUPLICATE KEY UPDATE Prescription_Url='http://www.acq.osd.mil/dpap/dars/pgi/pgi_htm/current/PGI204_71.htm';
