ALTER TABLE Users CHANGE Last_Logoin_Ip last_logon_ip VARCHAR(45);
ALTER TABLE Users CHANGE Rest_Password_Token Reset_Password_Token VARCHAR(80);

alter table questions modify column question_text varchar(500) NOT NULL;
alter table clauses modify column clause_title varchar(500) NOT NULL;

INSERT INTO Question_Type_Ref (Question_Type, Question_Type_Name) VALUES ('N', 'Numeric');

DELIMITER //

CREATE PROCEDURE etlAddPrescription (
	IN pPrescription_Name VARCHAR(60),
	IN pRegulation_Id  INT,
	IN pPrescription_Url VARCHAR(120),
	IN pPrescription_Content TEXT
)
BEGIN
	DECLARE nPrescription_Id INT DEFAULT NULL;
	
	SELECT Prescription_Id INTO nPrescription_Id
	FROM Prescriptions
	WHERE Prescription_Name = pPrescription_Name;
	
	IF (nPrescription_Id IS NULL) THEN
		INSERT INTO Prescriptions (Prescription_Name, Regulation_Id, Prescription_Url, Prescription_Content)
		VALUES (pPrescription_Name, pRegulation_Id, pPrescription_Url, pPrescription_Content);

		SELECT Prescription_Id INTO nPrescription_Id
		FROM Prescriptions
		WHERE Prescription_Name = pPrescription_Name;
	ELSE
		UPDATE Prescriptions
		SET Regulation_Id = pRegulation_Id
		, Prescription_Url = pPrescription_Url
		, Prescription_Content = pPrescription_Content
		WHERE Prescription_Id = nPrescription_Id;
	END IF;
END //

CREATE PROCEDURE etlAddClausePrescriptionByClauseId (
	IN pClause_Id  INT,
	IN pPrescription_Name VARCHAR(60)
)
BEGIN
	DECLARE nPrescription_Id INT DEFAULT NULL;
	DECLARE nClause_Prescription_Id INT DEFAULT NULL;
	
	SELECT Prescription_Id INTO nPrescription_Id
	FROM Prescriptions
	WHERE Prescription_Name = pPrescription_Name;
	
	IF (nPrescription_Id IS NOT NULL) THEN
		SELECT Clause_Prescription_Id INTO nClause_Prescription_Id
		FROM Clause_Prescriptions
		WHERE Clause_Id = pClause_Id
        AND Prescription_Id = nPrescription_Id;

		IF (nClause_Prescription_Id IS NULL) THEN
			INSERT INTO Clause_Prescriptions (Clause_Id, Prescription_Id)
			VALUES (pClause_Id, nPrescription_Id);
        ELSE
			SELECT CONCAT(pPrescription_Name, ': found');
        END IF;

	ELSE
		SELECT CONCAT(pPrescription_Name, ': not found');
	END IF;
END //

CREATE PROCEDURE etlAddQuestionChoicePrescriptionByChoiceId (
	IN pQuestion_Choce_Id  INT,
	IN pPrescription_Name VARCHAR(60)
)
BEGIN
	DECLARE nPrescription_Id INT DEFAULT NULL;
	DECLARE nQuestion_Choice_Prescription_Id INT DEFAULT NULL;
	
	SELECT Prescription_Id INTO nPrescription_Id
	FROM Prescriptions
	WHERE Prescription_Name = pPrescription_Name;
	
	IF (nPrescription_Id IS NOT NULL) THEN
		SELECT Question_Choice_Prescription_Id INTO nQuestion_Choice_Prescription_Id
		FROM Question_Choice_Prescriptions
		WHERE Question_Choce_Id = pQuestion_Choce_Id
        AND Prescription_Id = nPrescription_Id;

		IF (nQuestion_Choice_Prescription_Id IS NULL) THEN
			INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, Prescription_Id)
			VALUES (pQuestion_Choce_Id, nPrescription_Id);
        ELSE
			SELECT CONCAT(pPrescription_Name, ': found');
        END IF;

	ELSE
		SELECT CONCAT(pPrescription_Name, ': not found');
	END IF;
END //

DELIMITER ;


CREATE TABLE Question_Group_Ref
(
	Question_Group       VARCHAR(1) NOT NULL,
	Question_Group_Name  VARCHAR(50) NOT NULL,
	Created_At           TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	Updated_At           TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	PRIMARY KEY (Question_Group)
);

CREATE UNIQUE INDEX XAK1Question_Group_Ref ON Question_Group_Ref
(
	Question_Group_Name
);

INSERT INTO Question_Group_Ref (Question_Group, Question_Group_Name) VALUES ('A', 'Acquisition Planning');
INSERT INTO Question_Group_Ref (Question_Group, Question_Group_Name) VALUES ('B', 'Solicitation-Contract');
INSERT INTO Question_Group_Ref (Question_Group, Question_Group_Name) VALUES ('C', 'Supplies or Svcs and Prices');
INSERT INTO Question_Group_Ref (Question_Group, Question_Group_Name) VALUES ('D', 'Description or Specifications');
INSERT INTO Question_Group_Ref (Question_Group, Question_Group_Name) VALUES ('E', 'Packaging and Marking');
INSERT INTO Question_Group_Ref (Question_Group, Question_Group_Name) VALUES ('F', 'Inspection and Acceptance');
INSERT INTO Question_Group_Ref (Question_Group, Question_Group_Name) VALUES ('G', 'Deliveries and Performance');
INSERT INTO Question_Group_Ref (Question_Group, Question_Group_Name) VALUES ('H', 'Transportation');
INSERT INTO Question_Group_Ref (Question_Group, Question_Group_Name) VALUES ('I', 'Contract Admin Data');
INSERT INTO Question_Group_Ref (Question_Group, Question_Group_Name) VALUES ('J', 'Insurance and Claims');
INSERT INTO Question_Group_Ref (Question_Group, Question_Group_Name) VALUES ('K', 'Contract Clauses');
INSERT INTO Question_Group_Ref (Question_Group, Question_Group_Name) VALUES ('L', 'Instructions-Conditions-Notices');
INSERT INTO Question_Group_Ref (Question_Group, Question_Group_Name) VALUES ('M', 'Evaluation Factors for Award');

ALTER TABLE Questions ADD COLUMN Question_Group VARCHAR(1) NULL;
