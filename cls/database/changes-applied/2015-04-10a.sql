ALTER TABLE Documents MODIFY User_Id integer NULL;

CREATE UNIQUE INDEX XAKOAuth_Applications_UID ON OAuth_Applications
(
	Application_Uid
);

ALTER TABLE Document_Answers MODIFY Question_Answer      VARCHAR(2000) NOT NULL;


CREATE TABLE OAuth_Access
(
	Oauth_Access_Id      INTEGER AUTO_INCREMENT,
	Application_Id       integer NOT NULL,
	Access_Token         VARCHAR(255) NOT NULL,
	Created_At           TIMESTAMP NOT NULL,
	Expires_At           TIMESTAMP NOT NULL,
	PRIMARY KEY (Oauth_Access_Id)
);

CREATE UNIQUE INDEX XAK1OAuth_Access ON OAuth_Access
(
	Access_Token
);

CREATE INDEX XIFOAuth_Access_Application_Id ON OAuth_Access
(
	Application_Id
);

ALTER TABLE OAuth_Access
ADD FOREIGN KEY R_OAuth_Application_Access (Application_Id) REFERENCES OAuth_Applications (Application_Id)
		ON DELETE CASCADE;


CREATE TABLE OAuth_Interview
(
	Oauth_Interview_Id   INTEGER NOT NULL AUTO_INCREMENT,
	Application_Id       integer NOT NULL,
	Oauth_Access_Id      INTEGER NOT NULL,
	Document_Id          integer NOT NULL,
	Interview_Token      VARCHAR(255) NOT NULL,
	Expires_At           TIMESTAMP NOT NULL,
	Created_At           TIMESTAMP NOT NULL,
	Launched_At          TIMESTAMP NULL,
	PRIMARY KEY (Oauth_Interview_Id)
);

CREATE UNIQUE INDEX XAK1OAuth_Interview ON OAuth_Interview
(
	Interview_Token
);

CREATE INDEX XIFOAuth_Interview_Application_Id ON OAuth_Interview
(
	Application_Id
);

CREATE INDEX XIFOAuth_Interview_Document_Id ON OAuth_Interview
(
	Document_Id
);

CREATE INDEX XIFOAuth_Interview_Oauth_Access_Id ON OAuth_Interview
(
	Oauth_Access_Id
);

ALTER TABLE OAuth_Interview
ADD FOREIGN KEY R_OAuth_Application_Interview (Application_Id) REFERENCES OAuth_Applications (Application_Id)
		ON DELETE CASCADE;

ALTER TABLE OAuth_Interview
ADD FOREIGN KEY R_Documents_OAuth_Interview (Document_Id) REFERENCES Documents (Document_Id)
		ON DELETE CASCADE;


