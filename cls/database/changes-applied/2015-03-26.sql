UPDATE Clauses
SET Clause_Conditions = '(A) DOCUMENT TYPE IS: SOLICITATION'
WHERE Clause_Id = 12384;

UPDATE Clauses
SET Clause_Conditions = REPLACE(Clause_Conditions, '\\n', '\n')
WHERE Clause_Conditions LIKE '%\\n%';
