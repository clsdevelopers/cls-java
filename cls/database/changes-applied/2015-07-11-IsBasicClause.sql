ALTER TABLE Clauses
ADD COLUMN Is_Basic_Clause      boolean NOT NULL DEFAULT 0;

ALTER TABLE Clauses
CHANGE COLUMN Editable_Remarks Editable_Remarks     VARCHAR(1500) NULL;
