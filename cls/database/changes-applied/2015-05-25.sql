ALTER TABLE Question_Conditions ADD COLUMN 	Parent_Question_Id   integer NULL;

CREATE INDEX XIF2Question_Conditions ON Question_Conditions
(
	Parent_Question_Id
);

ALTER TABLE Question_Conditions
ADD FOREIGN KEY R_Question_Condition_Parent (Parent_Question_Id) REFERENCES Questions (Question_Id);

-- ==============================================
CREATE TABLE Clause_Version_Changes
(
	Clause_Ver_Change_Id INTEGER AUTO_INCREMENT,
	Clause_Version_Id    SMALLINT NOT NULL,
	Previous_Clause_Version_Id SMALLINT NULL,
	Is_Question_Or_Clause CHAR NOT NULL CHECK ( Is_Question_Or_Clause IN ('C', 'Q') ),
	Question_Or_Clause_Code VARCHAR(50) NOT NULL,
	Change_Code          VARCHAR(2) NOT NULL,
	Change_Details       TEXT NULL,
	Created_At           DATETIME NULL DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY (Clause_Ver_Change_Id)
);

CREATE INDEX XIE1Clause_Version_Changes ON Clause_Version_Changes
(
	Clause_Version_Id,
	Is_Question_Or_Clause,
	Question_Or_Clause_Code
);

CREATE INDEX XIF2Clause_Version_Changes ON Clause_Version_Changes
(
	Previous_Clause_Version_Id
);

ALTER TABLE Clause_Version_Changes
ADD FOREIGN KEY R_Clause_Versions_Changes (Clause_Version_Id) REFERENCES Clause_Versions (Clause_Version_Id)
		ON DELETE CASCADE;

ALTER TABLE Clause_Version_Changes
ADD FOREIGN KEY R_Clause_Version_Changes_Previous (Previous_Clause_Version_Id) REFERENCES Clause_Versions (Clause_Version_Id)
		ON DELETE CASCADE;

-- ==========================
ALTER TABLE Prescriptions CHANGE COLUMN Prescription_Url  Prescription_Url VARCHAR(120) NULL;
ALTER TABLE Questions CHANGE COLUMN Question_Code  Question_Code VARCHAR(100) NOT NULL;
