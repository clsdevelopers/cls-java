DROP TABLE Clause_Version_Changes CASCADE;

CREATE TABLE Clause_Version_Changes
(
	Clause_Ver_Change_Id INTEGER AUTO_INCREMENT,
	Clause_Version_Id    SMALLINT NOT NULL,
	Previous_Clause_Version_Id SMALLINT NULL,
	Is_Question_Or_Clause CHAR NOT NULL CHECK (Is_Question_Or_Clause IN ('C', 'Q')),
	Question_Or_Clause_Code VARCHAR(100) NOT NULL,
	Change_Code          VARCHAR(10) NOT NULL,
	Change_Brief         VARCHAR(500) NOT NULL,
	Created_At           DATETIME NULL DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY (Clause_Ver_Change_Id)
);


CREATE INDEX XIE1Clause_Version_Changes ON Clause_Version_Changes
(
	Clause_Version_Id,
	Is_Question_Or_Clause,
	Question_Or_Clause_Code
);

ALTER TABLE Clause_Version_Changes
ADD FOREIGN KEY R_Clause_Versions_Changes (Clause_Version_Id) REFERENCES Clause_Versions (Clause_Version_Id)
		ON DELETE CASCADE;

ALTER TABLE Clause_Version_Changes
ADD FOREIGN KEY R_Clause_Version_Changes_Previous (Previous_Clause_Version_Id) REFERENCES Clause_Versions (Clause_Version_Id)
		ON DELETE CASCADE;
