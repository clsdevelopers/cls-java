ALTER TABLE OAuth_Interview 
CHANGE COLUMN Interview_Token Interview_Token VARCHAR(255) NULL,
CHANGE COLUMN Oauth_Access_Id Oauth_Access_Id INT(11) NULL;

ALTER TABLE OAuth_Interview
DROP INDEX XAK1OAuth_Interview;

CREATE INDEX XIEOAuth_Interview_Token ON OAuth_Interview
(
	Interview_Token
);
