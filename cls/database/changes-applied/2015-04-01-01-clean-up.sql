DELETE FROM Clause_Prescriptions;
DELETE FROM Clause_Fill_Ins;
DELETE FROM Clauses;

DELETE FROM Question_Choice_Prescriptions;
DELETE FROM Question_Choices;
DELETE FROM Question_Conditions;
DELETE FROM Questions;

DELETE FROM Prescriptions WHERE Prescription_Id > 275;
