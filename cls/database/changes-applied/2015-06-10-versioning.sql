CREATE TABLE Doc_Status_Ref
(
	Doc_Status_Cd        CHAR NOT NULL,
	Doc_Status_Name      VARCHAR(20) NOT NULL,
	Created_At           DATETIME NULL DEFAULT CURRENT_TIMESTAMP,
	Updated_At           DATETIME NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	PRIMARY KEY (Doc_Status_Cd)
);

INSERT INTO Doc_Status_Ref ( Doc_Status_Cd, Doc_Status_Name) VALUES ('I', 'In-Progress');
INSERT INTO Doc_Status_Ref ( Doc_Status_Cd, Doc_Status_Name) VALUES ('C', 'Completed');
INSERT INTO Doc_Status_Ref ( Doc_Status_Cd, Doc_Status_Name) VALUES ('F', 'Finalized');

ALTER TABLE Documents ADD COLUMN Doc_Status_Cd CHAR(1) NOT NULL DEFAULT 'I';

CREATE INDEX XIF6Documents ON Documents
(
	Doc_Status_Cd
);

UPDATE Documents
SET Doc_Status_Cd = 'I'; -- IF (Is_Interview_Completed = 1, 'C', 'I');

ALTER TABLE Documents
ADD FOREIGN KEY R_Doc_Status_Ref_Documents (Doc_Status_Cd) REFERENCES Doc_Status_Ref (Doc_Status_Cd);

ALTER TABLE Documents DROP COLUMN Is_Interview_Completed;
