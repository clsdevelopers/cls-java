ALTER TABLE Clause_Fill_Ins ADD COLUMN 	Fill_In_Placeholder  VARCHAR(80) NULL;
ALTER TABLE Clause_Fill_Ins ADD COLUMN 	Fill_In_Default_Data VARCHAR(1000) NULL;
ALTER TABLE Clause_Fill_Ins ADD COLUMN 	Fill_In_Display_Rows NUMERIC(2) NULL;
ALTER TABLE Clause_Fill_Ins ADD COLUMN 	Fill_In_For_Table    boolean NOT NULL DEFAULT 0;
ALTER TABLE Clause_Fill_Ins ADD COLUMN 	Fill_In_Heading      VARCHAR(40) NULL;

ALTER TABLE Document_Fill_Ins ADD COLUMN Full_Text_Modified   boolean NOT NULL DEFAULT 0;
ALTER TABLE Document_Fill_Ins DROP COLUMN Is_Full_Text;

ALTER TABLE Clauses ADD COLUMN Is_Optional          boolean NOT NULL DEFAULT 0;

-- INSERT INTO Fill_In_Type_Ref (Fill_In_Type, Fill_In_Type_Name) VALUES ('D', 'Date');
INSERT INTO Fill_In_Type_Ref (Fill_In_Type, Fill_In_Type_Name) VALUES ('E', 'Email');
INSERT INTO Fill_In_Type_Ref (Fill_In_Type, Fill_In_Type_Name) VALUES ('M', 'Memo');
-- INSERT INTO Fill_In_Type_Ref (Fill_In_Type, Fill_In_Type_Name) VALUES ('N', 'Number');
-- INSERT INTO Fill_In_Type_Ref (Fill_In_Type, Fill_In_Type_Name) VALUES ('S', 'String');
INSERT INTO Fill_In_Type_Ref (Fill_In_Type, Fill_In_Type_Name) VALUES ('U', 'Url');
