-- ======================================
ALTER TABLE Question_Choice_Prescriptions DROP FOREIGN KEY Question_Choice_Prescriptions_ibfk_1;

ALTER TABLE Question_Choice_Prescriptions DROP FOREIGN KEY R_Question_Choices_Prescriptions ;

ALTER TABLE Question_Choice_Prescriptions
ADD FOREIGN KEY R_Question_Choices_Prescriptions (Question_Choce_Id) REFERENCES Question_Choices (Question_Choce_Id)
	ON DELETE CASCADE;

-- ======================================
ALTER TABLE Question_Choices DROP FOREIGN KEY Question_Choices_ibfk_1;

ALTER TABLE Question_Choices DROP FOREIGN KEY R_Questions_Choices;

ALTER TABLE Question_Choices
ADD FOREIGN KEY R_Questions_Choices (Question_Id) REFERENCES Questions (Question_Id)
	ON DELETE CASCADE;

-- ======================================
ALTER TABLE Question_Conditions DROP FOREIGN KEY Question_Conditions_ibfk_1, DROP FOREIGN KEY Question_Conditions_ibfk_2;
ALTER TABLE Question_Conditions DROP FOREIGN KEY R_Questions_Conditions, DROP FOREIGN KEY R_Question_Condition_Parent;

ALTER TABLE Question_Conditions
ADD FOREIGN KEY R_Questions_Conditions (Question_Id) REFERENCES Questions (Question_Id)
  ON DELETE CASCADE;

ALTER TABLE Question_Conditions
ADD FOREIGN KEY R_Question_Condition_Parent (Parent_Question_Id) REFERENCES Questions (Question_Id)
  ON DELETE SET NULL;

-- ======================================
ALTER TABLE Clause_Fill_Ins DROP FOREIGN KEY Clause_Fill_Ins_ibfk_2;

ALTER TABLE Clause_Fill_Ins DROP FOREIGN KEY R_Clauses_Fill_In;

ALTER TABLE Clause_Fill_Ins
ADD FOREIGN KEY R_Clauses_Fill_In (Clause_Id) REFERENCES Clauses (Clause_Id)
  ON DELETE CASCADE;

-- ======================================
ALTER TABLE Clause_Prescriptions DROP FOREIGN KEY Clause_Prescriptions_ibfk_1;

ALTER TABLE Clause_Prescriptions DROP FOREIGN KEY R_Clauses_Clause_Prescriptions;

ALTER TABLE Clause_Prescriptions
ADD FOREIGN KEY R_Clauses_Clause_Prescriptions (Clause_Id) REFERENCES Clauses (Clause_Id)
  ON DELETE CASCADE;

-- ======================================
ALTER TABLE Document_Fill_Ins DROP FOREIGN KEY Document_Fill_Ins_ibfk_1;

ALTER TABLE Document_Fill_Ins DROP FOREIGN KEY R_Documents_Fill_Ins;

ALTER TABLE Document_Fill_Ins
ADD FOREIGN KEY R_Documents_Fill_Ins (Document_Id) REFERENCES Documents (Document_Id)
  ON DELETE CASCADE;

-- ======================================
ALTER TABLE Document_Answers DROP FOREIGN KEY Document_Answers_ibfk_2;

ALTER TABLE Document_Answers DROP FOREIGN KEY R_Documents_Document_Answers;

ALTER TABLE Document_Answers
ADD FOREIGN KEY R_Documents_Document_Answers (Document_Id) REFERENCES Documents (Document_Id)
  ON DELETE CASCADE;

-- ======================================
