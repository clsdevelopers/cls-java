ALTER TABLE Clause_Fill_Ins 
CHANGE COLUMN Fill_In_Default_Data Fill_In_Default_Data TEXT NULL DEFAULT NULL;

ALTER TABLE Document_Fill_Ins 
CHANGE COLUMN Document_Fill_In_Answer Document_Fill_In_Answer TEXT NOT NULL;
