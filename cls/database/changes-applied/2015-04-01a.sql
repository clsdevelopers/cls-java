ALTER TABLE Document_Answers CHANGE Question_Answer Question_Answer      VARCHAR(1000);

DROP TABLE Document_Answer_Clauses CASCADE;

/*
CREATE TABLE Document_Answer_Clauses
(
	Document_Answer_Id   integer NOT NULL,
	Clause_Id            integer NOT NULL,
	Is_Removed_By_User   boolean NOT NULL DEFAULT 0,
	PRIMARY KEY (Document_Answer_Id,Clause_Id)
);

CREATE INDEX XIFDocument_Answer_Clauses_Clause_Id ON Document_Answer_Clauses
(
	Clause_Id
);

ALTER TABLE Document_Answer_Clauses
ADD FOREIGN KEY R_Document_Answers_Clauses (Document_Answer_Id) REFERENCES Document_Answers (Document_Answer_Id)
		ON DELETE CASCADE;

*/


CREATE TABLE Document_Clauses
(
	Document_Id          integer NOT NULL,
	Clause_Id            integer NOT NULL,
	Is_Removed_By_User   boolean NOT NULL DEFAULT 0,
	Created_At           TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	Updated_At           TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	PRIMARY KEY (Document_Id,Clause_Id)
);

CREATE INDEX XIFDocument_Clauses_Clause_Id ON Document_Clauses
(
	Clause_Id
);

ALTER TABLE Document_Clauses
ADD FOREIGN KEY R_Documents_Clauses (Document_Id) REFERENCES Documents (Document_Id)
	ON DELETE CASCADE;

ALTER TABLE Document_Clauses
ADD FOREIGN KEY R_Clauses_Document_Clauses (Clause_Id) REFERENCES Clauses (Clause_Id);

INSERT INTO Sys_Last_Table_Change_At (Table_Name) VALUES ('CLAUSE_FILL_INS');
INSERT INTO Sys_Last_Table_Change_At (Table_Name) VALUES ('CLAUSE_PRESCRIPTIONS');
INSERT INTO Sys_Last_Table_Change_At (Table_Name) VALUES ('CLAUSE_SECTIONS');
INSERT INTO Sys_Last_Table_Change_At (Table_Name) VALUES ('CLAUSE_VERSIONS');
INSERT INTO Sys_Last_Table_Change_At (Table_Name) VALUES ('CLAUSES');
INSERT INTO Sys_Last_Table_Change_At (Table_Name) VALUES ('DEPTS');
INSERT INTO Sys_Last_Table_Change_At (Table_Name) VALUES ('DOC_TYPE_REF');
INSERT INTO Sys_Last_Table_Change_At (Table_Name) VALUES ('DOCUMENT_ANSWERS');
INSERT INTO Sys_Last_Table_Change_At (Table_Name) VALUES ('DOCUMENT_CLAUSES');
INSERT INTO Sys_Last_Table_Change_At (Table_Name) VALUES ('DOCUMENT_FILL_INS');
INSERT INTO Sys_Last_Table_Change_At (Table_Name) VALUES ('DOCUMENTS');
INSERT INTO Sys_Last_Table_Change_At (Table_Name) VALUES ('FILL_IN_TYPE_REF');
INSERT INTO Sys_Last_Table_Change_At (Table_Name) VALUES ('INCLUSION_REF');
INSERT INTO Sys_Last_Table_Change_At (Table_Name) VALUES ('ORG_REGULATIONS');
INSERT INTO Sys_Last_Table_Change_At (Table_Name) VALUES ('ORGS');
INSERT INTO Sys_Last_Table_Change_At (Table_Name) VALUES ('PRESCRIPTIONS');
INSERT INTO Sys_Last_Table_Change_At (Table_Name) VALUES ('QUESTION_CHOICE_PRESCRIPTIONS');
INSERT INTO Sys_Last_Table_Change_At (Table_Name) VALUES ('QUESTION_CHOICES');
INSERT INTO Sys_Last_Table_Change_At (Table_Name) VALUES ('QUESTION_CONDITIONS');
INSERT INTO Sys_Last_Table_Change_At (Table_Name) VALUES ('QUESTION_GROUP_REF');
INSERT INTO Sys_Last_Table_Change_At (Table_Name) VALUES ('QUESTION_TYPE_REF');
INSERT INTO Sys_Last_Table_Change_At (Table_Name) VALUES ('QUESTIONS');
INSERT INTO Sys_Last_Table_Change_At (Table_Name) VALUES ('REGULATIONS');
INSERT INTO Sys_Last_Table_Change_At (Table_Name) VALUES ('ROLES');
INSERT INTO Sys_Last_Table_Change_At (Table_Name) VALUES ('USER_ROLES');

delimiter // 

CREATE TRIGGER TAI_Doc_Type_Ref AFTER INSERT ON Doc_Type_Ref
FOR EACH ROW
BEGIN
	UPDATE Sys_Last_Table_Change_At SET Last_Change_At = CURRENT_TIMESTAMP WHERE Table_Name = 'DOC_TYPE_REF';
END;//

CREATE TRIGGER TAU_Doc_Type_Ref AFTER UPDATE ON Doc_Type_Ref
FOR EACH ROW
BEGIN
	UPDATE Sys_Last_Table_Change_At SET Last_Change_At = CURRENT_TIMESTAMP WHERE Table_Name = 'DOC_TYPE_REF';
END;//

CREATE TRIGGER TAD_Doc_Type_Ref AFTER DELETE ON Doc_Type_Ref
FOR EACH ROW
BEGIN
	UPDATE Sys_Last_Table_Change_At SET Last_Change_At = CURRENT_TIMESTAMP WHERE Table_Name = 'DOC_TYPE_REF';
END;//


CREATE TRIGGER TAI_Fill_In_Type_Ref AFTER INSERT ON Fill_In_Type_Ref
FOR EACH ROW
BEGIN
	UPDATE Sys_Last_Table_Change_At SET Last_Change_At = CURRENT_TIMESTAMP WHERE Table_Name = 'FILL_IN_TYPE_REF';
END;//

CREATE TRIGGER TAU_Fill_In_Type_Ref AFTER UPDATE ON Fill_In_Type_Ref
FOR EACH ROW
BEGIN
	UPDATE Sys_Last_Table_Change_At SET Last_Change_At = CURRENT_TIMESTAMP WHERE Table_Name = 'FILL_IN_TYPE_REF';
END;//

CREATE TRIGGER TAD_Fill_In_Type_Ref AFTER DELETE ON Fill_In_Type_Ref
FOR EACH ROW
BEGIN
	UPDATE Sys_Last_Table_Change_At SET Last_Change_At = CURRENT_TIMESTAMP WHERE Table_Name = 'FILL_IN_TYPE_REF';
END;//


CREATE TRIGGER TAI_Inclusion_Ref AFTER INSERT ON Inclusion_Ref
FOR EACH ROW
BEGIN
	UPDATE Sys_Last_Table_Change_At SET Last_Change_At = CURRENT_TIMESTAMP WHERE Table_Name = 'INCLUSION_REF';
END;//

CREATE TRIGGER TAU_Inclusion_Ref AFTER UPDATE ON Inclusion_Ref
FOR EACH ROW
BEGIN
	UPDATE Sys_Last_Table_Change_At SET Last_Change_At = CURRENT_TIMESTAMP WHERE Table_Name = 'INCLUSION_REF';
END;//

CREATE TRIGGER TAD_Inclusion_Ref AFTER DELETE ON Inclusion_Ref
FOR EACH ROW
BEGIN
	UPDATE Sys_Last_Table_Change_At SET Last_Change_At = CURRENT_TIMESTAMP WHERE Table_Name = 'INCLUSION_REF';
END;//


CREATE TRIGGER TAI_Question_Type_Ref AFTER INSERT ON Question_Type_Ref
FOR EACH ROW
BEGIN
	UPDATE Sys_Last_Table_Change_At SET Last_Change_At = CURRENT_TIMESTAMP WHERE Table_Name = 'QUESTION_TYPE_REF';
END;//

CREATE TRIGGER TAU_Question_Type_Ref AFTER UPDATE ON Question_Type_Ref
FOR EACH ROW
BEGIN
	UPDATE Sys_Last_Table_Change_At SET Last_Change_At = CURRENT_TIMESTAMP WHERE Table_Name = 'QUESTION_TYPE_REF';
END;//

CREATE TRIGGER TAD_Question_Type_Ref AFTER DELETE ON Question_Type_Ref
FOR EACH ROW
BEGIN
	UPDATE Sys_Last_Table_Change_At SET Last_Change_At = CURRENT_TIMESTAMP WHERE Table_Name = 'QUESTION_TYPE_REF';
END;//


CREATE TRIGGER TAI_Question_Group_Ref AFTER INSERT ON Question_Group_Ref
FOR EACH ROW
BEGIN
	UPDATE Sys_Last_Table_Change_At SET Last_Change_At = CURRENT_TIMESTAMP WHERE Table_Name = 'QUESTION_GROUP_REF';
END;//

CREATE TRIGGER TAU_Question_Group_Ref AFTER UPDATE ON Question_Group_Ref
FOR EACH ROW
BEGIN
	UPDATE Sys_Last_Table_Change_At SET Last_Change_At = CURRENT_TIMESTAMP WHERE Table_Name = 'QUESTION_GROUP_REF';
END;//

CREATE TRIGGER TAD_Question_Group_Ref AFTER DELETE ON Question_Group_Ref
FOR EACH ROW
BEGIN
	UPDATE Sys_Last_Table_Change_At SET Last_Change_At = CURRENT_TIMESTAMP WHERE Table_Name = 'QUESTION_GROUP_REF';
END;//


CREATE TRIGGER TAI_Roles AFTER INSERT ON Roles
FOR EACH ROW
BEGIN
	UPDATE Sys_Last_Table_Change_At SET Last_Change_At = CURRENT_TIMESTAMP WHERE Table_Name = 'ROLES';
END;//

CREATE TRIGGER TAU_Roles AFTER UPDATE ON Roles
FOR EACH ROW
BEGIN
	UPDATE Sys_Last_Table_Change_At SET Last_Change_At = CURRENT_TIMESTAMP WHERE Table_Name = 'ROLES';
END;//

CREATE TRIGGER TAD_Roles AFTER DELETE ON Roles
FOR EACH ROW
BEGIN
	UPDATE Sys_Last_Table_Change_At SET Last_Change_At = CURRENT_TIMESTAMP WHERE Table_Name = 'ROLES';
END;//



CREATE TRIGGER TAI_Regulations AFTER INSERT ON Regulations
FOR EACH ROW
BEGIN
	UPDATE Sys_Last_Table_Change_At SET Last_Change_At = CURRENT_TIMESTAMP WHERE Table_Name = 'REGULATIONS';
END;//

CREATE TRIGGER TAU_Regulations AFTER UPDATE ON Regulations
FOR EACH ROW
BEGIN
	UPDATE Sys_Last_Table_Change_At SET Last_Change_At = CURRENT_TIMESTAMP WHERE Table_Name = 'REGULATIONS';
END;//

CREATE TRIGGER TAD_Regulations AFTER DELETE ON Regulations
FOR EACH ROW
BEGIN
	UPDATE Sys_Last_Table_Change_At SET Last_Change_At = CURRENT_TIMESTAMP WHERE Table_Name = 'REGULATIONS';
END;//


CREATE TRIGGER TAI_Clause_Sections AFTER INSERT ON Clause_Sections
FOR EACH ROW
BEGIN
	UPDATE Sys_Last_Table_Change_At SET Last_Change_At = CURRENT_TIMESTAMP WHERE Table_Name = 'CLAUSE_SECTIONS';
END;//

CREATE TRIGGER TAU_Clause_Sections AFTER UPDATE ON Clause_Sections
FOR EACH ROW
BEGIN
	UPDATE Sys_Last_Table_Change_At SET Last_Change_At = CURRENT_TIMESTAMP WHERE Table_Name = 'CLAUSE_SECTIONS';
END;//

CREATE TRIGGER TAD_Clause_Sections AFTER DELETE ON Clause_Sections
FOR EACH ROW
BEGIN
	UPDATE Sys_Last_Table_Change_At SET Last_Change_At = CURRENT_TIMESTAMP WHERE Table_Name = 'CLAUSE_SECTIONS';
END;//


CREATE TRIGGER TAI_Prescriptions AFTER INSERT ON Prescriptions
FOR EACH ROW
BEGIN
	UPDATE Sys_Last_Table_Change_At SET Last_Change_At = CURRENT_TIMESTAMP WHERE Table_Name = 'PRESCRIPTIONS';
END;//

CREATE TRIGGER TAU_Prescriptions AFTER UPDATE ON Prescriptions
FOR EACH ROW
BEGIN
	UPDATE Sys_Last_Table_Change_At SET Last_Change_At = CURRENT_TIMESTAMP WHERE Table_Name = 'PRESCRIPTIONS';
END;//

CREATE TRIGGER TAD_Prescriptions AFTER DELETE ON Prescriptions
FOR EACH ROW
BEGIN
	UPDATE Sys_Last_Table_Change_At SET Last_Change_At = CURRENT_TIMESTAMP WHERE Table_Name = 'PRESCRIPTIONS';
END;//


CREATE TRIGGER TAI_Depts AFTER INSERT ON Depts
FOR EACH ROW
BEGIN
	UPDATE Sys_Last_Table_Change_At SET Last_Change_At = CURRENT_TIMESTAMP WHERE Table_Name = 'DEPTS';
END;//

CREATE TRIGGER TAU_Depts AFTER UPDATE ON Depts
FOR EACH ROW
BEGIN
	UPDATE Sys_Last_Table_Change_At SET Last_Change_At = CURRENT_TIMESTAMP WHERE Table_Name = 'DEPTS';
END;//

CREATE TRIGGER TAD_Depts AFTER DELETE ON Depts
FOR EACH ROW
BEGIN
	UPDATE Sys_Last_Table_Change_At SET Last_Change_At = CURRENT_TIMESTAMP WHERE Table_Name = 'DEPTS';
END;//


CREATE TRIGGER TAI_Orgs AFTER INSERT ON Orgs
FOR EACH ROW
BEGIN
	UPDATE Sys_Last_Table_Change_At SET Last_Change_At = CURRENT_TIMESTAMP WHERE Table_Name = 'ORGS';
END;//

CREATE TRIGGER TAU_Orgs AFTER UPDATE ON Orgs
FOR EACH ROW
BEGIN
	UPDATE Sys_Last_Table_Change_At SET Last_Change_At = CURRENT_TIMESTAMP WHERE Table_Name = 'ORGS';
END;//

CREATE TRIGGER TAD_Orgs AFTER DELETE ON Orgs
FOR EACH ROW
BEGIN
	UPDATE Sys_Last_Table_Change_At SET Last_Change_At = CURRENT_TIMESTAMP WHERE Table_Name = 'ORGS';
END;//


CREATE TRIGGER TAI_Org_Regulations AFTER INSERT ON Org_Regulations
FOR EACH ROW
BEGIN
	UPDATE Sys_Last_Table_Change_At SET Last_Change_At = CURRENT_TIMESTAMP WHERE Table_Name = 'ORG_REGULATIONS';
END;//

CREATE TRIGGER TAU_Org_Regulations AFTER UPDATE ON Org_Regulations
FOR EACH ROW
BEGIN
	UPDATE Sys_Last_Table_Change_At SET Last_Change_At = CURRENT_TIMESTAMP WHERE Table_Name = 'ORG_REGULATIONS';
END;//

CREATE TRIGGER TAD_Org_Regulations AFTER DELETE ON Org_Regulations
FOR EACH ROW
BEGIN
	UPDATE Sys_Last_Table_Change_At SET Last_Change_At = CURRENT_TIMESTAMP WHERE Table_Name = 'ORG_REGULATIONS';
END;//


CREATE TRIGGER TAI_Clause_Versions AFTER INSERT ON Clause_Versions
FOR EACH ROW
BEGIN
	UPDATE Sys_Last_Table_Change_At SET Last_Change_At = CURRENT_TIMESTAMP WHERE Table_Name = 'CLAUSE_VERSIONS';
END;//

CREATE TRIGGER TAU_Clause_Versions AFTER UPDATE ON Clause_Versions
FOR EACH ROW
BEGIN
	UPDATE Sys_Last_Table_Change_At SET Last_Change_At = CURRENT_TIMESTAMP WHERE Table_Name = 'CLAUSE_VERSIONS';
END;//

CREATE TRIGGER TAD_Clause_Versions AFTER DELETE ON Clause_Versions
FOR EACH ROW
BEGIN
	UPDATE Sys_Last_Table_Change_At SET Last_Change_At = CURRENT_TIMESTAMP WHERE Table_Name = 'CLAUSE_VERSIONS';
END;//


CREATE TRIGGER TAI_Clauses AFTER INSERT ON Clauses
FOR EACH ROW
BEGIN
	UPDATE Sys_Last_Table_Change_At SET Last_Change_At = CURRENT_TIMESTAMP WHERE Table_Name = 'CLAUSES';
END;//

CREATE TRIGGER TAU_Clauses AFTER UPDATE ON Clauses
FOR EACH ROW
BEGIN
	UPDATE Sys_Last_Table_Change_At SET Last_Change_At = CURRENT_TIMESTAMP WHERE Table_Name = 'CLAUSES';
END;//

CREATE TRIGGER TAD_Clauses AFTER DELETE ON Clauses
FOR EACH ROW
BEGIN
	UPDATE Sys_Last_Table_Change_At SET Last_Change_At = CURRENT_TIMESTAMP WHERE Table_Name = 'CLAUSES';
END;//


CREATE TRIGGER TAI_Clause_Prescriptions AFTER INSERT ON Clause_Prescriptions
FOR EACH ROW
BEGIN
	UPDATE Sys_Last_Table_Change_At SET Last_Change_At = CURRENT_TIMESTAMP WHERE Table_Name = 'CLAUSE_PRESCRIPTIONS';
END;//

CREATE TRIGGER TAU_Clause_Prescriptions AFTER UPDATE ON Clause_Prescriptions
FOR EACH ROW
BEGIN
	UPDATE Sys_Last_Table_Change_At SET Last_Change_At = CURRENT_TIMESTAMP WHERE Table_Name = 'CLAUSE_PRESCRIPTIONS';
END;//

CREATE TRIGGER TAD_Clause_Prescriptions AFTER DELETE ON Clause_Prescriptions
FOR EACH ROW
BEGIN
	UPDATE Sys_Last_Table_Change_At SET Last_Change_At = CURRENT_TIMESTAMP WHERE Table_Name = 'CLAUSE_PRESCRIPTIONS';
END;//


CREATE TRIGGER TAI_Clause_Fill_Ins AFTER INSERT ON Clause_Fill_Ins
FOR EACH ROW
BEGIN
	UPDATE Sys_Last_Table_Change_At SET Last_Change_At = CURRENT_TIMESTAMP WHERE Table_Name = 'CLAUSE_FILL_INS';
END;//

CREATE TRIGGER TAU_Clause_Fill_Ins AFTER UPDATE ON Clause_Fill_Ins
FOR EACH ROW
BEGIN
	UPDATE Sys_Last_Table_Change_At SET Last_Change_At = CURRENT_TIMESTAMP WHERE Table_Name = 'CLAUSE_FILL_INS';
END;//

CREATE TRIGGER TAD_Clause_Fill_Ins AFTER DELETE ON Clause_Fill_Ins
FOR EACH ROW
BEGIN
	UPDATE Sys_Last_Table_Change_At SET Last_Change_At = CURRENT_TIMESTAMP WHERE Table_Name = 'CLAUSE_FILL_INS';
END;//


CREATE TRIGGER TAI_Questions AFTER INSERT ON Questions
FOR EACH ROW
BEGIN
	UPDATE Sys_Last_Table_Change_At SET Last_Change_At = CURRENT_TIMESTAMP WHERE Table_Name = 'QUESTIONS';
END;//

CREATE TRIGGER TAU_Questions AFTER UPDATE ON Questions
FOR EACH ROW
BEGIN
	UPDATE Sys_Last_Table_Change_At SET Last_Change_At = CURRENT_TIMESTAMP WHERE Table_Name = 'QUESTIONS';
END;//

CREATE TRIGGER TAD_Questions AFTER DELETE ON Questions
FOR EACH ROW
BEGIN
	UPDATE Sys_Last_Table_Change_At SET Last_Change_At = CURRENT_TIMESTAMP WHERE Table_Name = 'QUESTIONS';
END;//


CREATE TRIGGER TAI_Question_Choices AFTER INSERT ON Question_Choices
FOR EACH ROW
BEGIN
	UPDATE Sys_Last_Table_Change_At SET Last_Change_At = CURRENT_TIMESTAMP WHERE Table_Name = 'QUESTION_CHOICES';
END;//

CREATE TRIGGER TAU_Question_Choices AFTER UPDATE ON Question_Choices
FOR EACH ROW
BEGIN
	UPDATE Sys_Last_Table_Change_At SET Last_Change_At = CURRENT_TIMESTAMP WHERE Table_Name = 'QUESTION_CHOICES';
END;//

CREATE TRIGGER TAD_Question_Choices AFTER DELETE ON Question_Choices
FOR EACH ROW
BEGIN
	UPDATE Sys_Last_Table_Change_At SET Last_Change_At = CURRENT_TIMESTAMP WHERE Table_Name = 'QUESTION_CHOICES';
END;//


CREATE TRIGGER TAI_Question_Choice_Prescriptions AFTER INSERT ON Question_Choice_Prescriptions
FOR EACH ROW
BEGIN
	UPDATE Sys_Last_Table_Change_At SET Last_Change_At = CURRENT_TIMESTAMP WHERE Table_Name = 'QUESTION_CHOICE_PRESCRIPTIONS';
END;//

CREATE TRIGGER TAU_Question_Choice_Prescriptions AFTER UPDATE ON Question_Choice_Prescriptions
FOR EACH ROW
BEGIN
	UPDATE Sys_Last_Table_Change_At SET Last_Change_At = CURRENT_TIMESTAMP WHERE Table_Name = 'QUESTION_CHOICE_PRESCRIPTIONS';
END;//

CREATE TRIGGER TAD_Question_Choice_Prescriptions AFTER DELETE ON Question_Choice_Prescriptions
FOR EACH ROW
BEGIN
	UPDATE Sys_Last_Table_Change_At SET Last_Change_At = CURRENT_TIMESTAMP WHERE Table_Name = 'QUESTION_CHOICE_PRESCRIPTIONS';
END;//


CREATE TRIGGER TAI_Question_Conditions AFTER INSERT ON Question_Conditions
FOR EACH ROW
BEGIN
	UPDATE Sys_Last_Table_Change_At SET Last_Change_At = CURRENT_TIMESTAMP WHERE Table_Name = 'QUESTION_CONDITIONS';
END;//

CREATE TRIGGER TAU_Question_Conditions AFTER UPDATE ON Question_Conditions
FOR EACH ROW
BEGIN
	UPDATE Sys_Last_Table_Change_At SET Last_Change_At = CURRENT_TIMESTAMP WHERE Table_Name = 'QUESTION_CONDITIONS';
END;//

CREATE TRIGGER TAD_Question_Conditions AFTER DELETE ON Question_Conditions
FOR EACH ROW
BEGIN
	UPDATE Sys_Last_Table_Change_At SET Last_Change_At = CURRENT_TIMESTAMP WHERE Table_Name = 'QUESTION_CONDITIONS';
END;//


CREATE TRIGGER TAI_User_Roles AFTER INSERT ON User_Roles
FOR EACH ROW
BEGIN
	UPDATE Sys_Last_Table_Change_At SET Last_Change_At = CURRENT_TIMESTAMP WHERE Table_Name = 'USER_ROLES';
END;//

CREATE TRIGGER TAU_User_Roles AFTER UPDATE ON User_Roles
FOR EACH ROW
BEGIN
	UPDATE Sys_Last_Table_Change_At SET Last_Change_At = CURRENT_TIMESTAMP WHERE Table_Name = 'USER_ROLES';
END;//

CREATE TRIGGER TAD_User_Roles AFTER DELETE ON User_Roles
FOR EACH ROW
BEGIN
	UPDATE Sys_Last_Table_Change_At SET Last_Change_At = CURRENT_TIMESTAMP WHERE Table_Name = 'USER_ROLES';
END;//

