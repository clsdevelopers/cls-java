/*
Update '252.219-7003 Alternate I (DEVIATION 2013-00014)' Clause Data for preventing duplicated 'Alternate I (OCT 2014)'.
*/

UPDATE Clauses SET Clause_Data = 'Alternate I (OCT 2014)<p>As prescribed in 219.708(b)(1)(A)(2), substitute the following paragraph (f)(1)(i) for (f)(1)(i) in the basic clause:</p><p>(f)(1)(i) The Standard Form 294 Subcontracting Report for Individual Contracts shall be submitted in accordance with the instructions on that form; paragraph (f)(2)(i) is inapplicable.</p>'
WHERE Clause_Version_Id = 7 and Clause_name = '252.219-7003 Alternate I (DEVIATION 2013-00014)';
