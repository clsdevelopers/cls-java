ALTER TABLE Document_Clauses
ADD COLUMN Optional_User_Action_Code VARCHAR(1) NULL
CHECK ( Optional_User_Action_Code IN (NULL, 'R', 'A') );
-- R: Removed by User
-- A: Added by User

ALTER TABLE Document_Clauses DROP COLUMN Is_Removed_By_User;

ALTER TABLE Clauses
ADD COLUMN Editable_Remarks VARCHAR(1000) NULL;
