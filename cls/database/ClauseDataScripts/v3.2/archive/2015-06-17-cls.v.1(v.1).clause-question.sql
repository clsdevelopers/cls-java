/*
Clause Parser Run at Wed Jun 17 07:36:14 EDT 2015
(Without Correction Information)
*/
/* ===============================================
 * Prescriptions
   =============================================== */
/* ===============================================
 * Questions
   =============================================== */
/*
<h2>52.214-34</h2>
(K) Clause name not found: {52.225-4 ALTERNATE III}
<pre>(K) 52.225-4 ALTERNATE III IS: INCLUDED</pre>
*//*
<h2>52.214-35</h2>
(K) Clause name not found: {52.225-4 ALTERNATE III}
<pre>(K) 52.225-4 ALTERNATE III IS: INCLUDED</pre>
*//*
<h2>52.225-1</h2>
(M) Clause name not found: {52.225-4 ALTERNATE III}
<pre>(M) 52.225-4 ALTERNATE III IS: NOT INCLUDED</pre>
*//*
<h2>52.225-3</h2>
(E) Value not found: "SET-ASIDES" in [PROCEDURES] choices. Available values: <ul><li>"COMPETITION"</li><li>"SIMPLIFIED ACQUISITION PROCEDURES (FAR PART 13)"</li><li>"COMMERCIAL PROCEDURES (FAR PART 12)"</li><li>"PROCEDURES IN SUPPORT OF OPERATIONS IN AFGHANISTAN (DFARS 225.7703-1)"</li><li>"NEGOTIATION (FAR PART 15)"</li><li>"SMALL BUSINESS PROGRAM"</li></ul>
<pre>(E) PROCEDURES IS: NOT SET-ASIDES</pre>
*//*
<h2>52.225-3 Alternate I</h2>
(E) Value not found: "SET-ASIDES" in [PROCEDURES] choices. Available values: <ul><li>"COMPETITION"</li><li>"SIMPLIFIED ACQUISITION PROCEDURES (FAR PART 13)"</li><li>"COMMERCIAL PROCEDURES (FAR PART 12)"</li><li>"PROCEDURES IN SUPPORT OF OPERATIONS IN AFGHANISTAN (DFARS 225.7703-1)"</li><li>"NEGOTIATION (FAR PART 15)"</li><li>"SMALL BUSINESS PROGRAM"</li></ul>
<pre>(E) PROCEDURES IS: NOT SET-ASIDES</pre>
*//*
<h2>52.225-3 Alternate II</h2>
(E2) Value not found: "SET-ASIDES" in [PROCEDURES] choices. Available values: <ul><li>"COMPETITION"</li><li>"SIMPLIFIED ACQUISITION PROCEDURES (FAR PART 13)"</li><li>"COMMERCIAL PROCEDURES (FAR PART 12)"</li><li>"PROCEDURES IN SUPPORT OF OPERATIONS IN AFGHANISTAN (DFARS 225.7703-1)"</li><li>"NEGOTIATION (FAR PART 15)"</li><li>"SMALL BUSINESS PROGRAM"</li></ul>
<pre>(E2) PROCEDURES IS: NOT SET-ASIDES</pre>
*//*
<h2>52.225-3 Alternate II</h2>
(E) Value not found: "SET-ASIDES" in [PROCEDURES] choices. Available values: <ul><li>"COMPETITION"</li><li>"SIMPLIFIED ACQUISITION PROCEDURES (FAR PART 13)"</li><li>"COMMERCIAL PROCEDURES (FAR PART 12)"</li><li>"PROCEDURES IN SUPPORT OF OPERATIONS IN AFGHANISTAN (DFARS 225.7703-1)"</li><li>"NEGOTIATION (FAR PART 15)"</li><li>"SMALL BUSINESS PROGRAM"</li></ul>
<pre>(E) PROCEDURES IS: NOT SET-ASIDES</pre>
*//*
<h2>52.225-3 Alternate III</h2>
(E) Value not found: "SET-ASIDES" in [PROCEDURES] choices. Available values: <ul><li>"COMPETITION"</li><li>"SIMPLIFIED ACQUISITION PROCEDURES (FAR PART 13)"</li><li>"COMMERCIAL PROCEDURES (FAR PART 12)"</li><li>"PROCEDURES IN SUPPORT OF OPERATIONS IN AFGHANISTAN (DFARS 225.7703-1)"</li><li>"NEGOTIATION (FAR PART 15)"</li><li>"SMALL BUSINESS PROGRAM"</li></ul>
<pre>(E) PROCEDURES IS: NOT SET-ASIDES</pre>
*/
-- ProduceClause.resolveIssues() Summary: Total(1211); Merged(9); Corrected(336); Error(9)
/* ===============================================
 * Clause
   =============================================== */

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1201_67000&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1201_67000&rgn=div8'
WHERE clause_id = 72875; -- 252.201-7000

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1203_67000&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1203_67000&rgn=div8'
WHERE clause_id = 72876; -- 252.203-7000

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1203_67001&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1203_67001&rgn=div8'
WHERE clause_id = 72877; -- 252.203-7001

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1203_67002&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1203_67002&rgn=div8'
WHERE clause_id = 72878; -- 252.203-7002

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1203_67003&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1203_67003&rgn=div8'
WHERE clause_id = 72879; -- 252.203-7003

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1203_67004&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1203_67004&rgn=div8'
WHERE clause_id = 72874; -- 252.203-7004

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1203_67005&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1203_67005&rgn=div8'
WHERE clause_id = 72880; -- 252.203-7005

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1204_67000&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1204_67000&rgn=div8'
WHERE clause_id = 72894; -- 252.204-7000

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1204_67002&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1204_67002&rgn=div8'
WHERE clause_id = 72895; -- 252.204-7002

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1204_67003&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1204_67003&rgn=div8'
WHERE clause_id = 72896; -- 252.204-7003

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1204_67004&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1204_67004&rgn=div8'
WHERE clause_id = 72897; -- 252.204-7004

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1204_67005&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1204_67005&rgn=div8'
WHERE clause_id = 72898; -- 252.204-7005

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1204_67006&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1204_67006&rgn=div8'
WHERE clause_id = 72899; -- 252.204-7006

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1204_67007&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1204_67007&rgn=div8'
WHERE clause_id = 72900; -- 252.204-7007

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1204_67010&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1204_67010&rgn=div8'
WHERE clause_id = 72901; -- 252.204-7010

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1204_67011&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1204_67011&rgn=div8'
WHERE clause_id = 72902; -- 252.204-7011

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1204_67012&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1204_67012&rgn=div8'
WHERE clause_id = 72903; -- 252.204-7012

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1204_67013&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1204_67013&rgn=div8'
WHERE clause_id = 72904; -- 252.204-7013

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1204_67014&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1204_67014&rgn=div8'
WHERE clause_id = 72905; -- 252.204-7014

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1204_67015&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1204_67015&rgn=div8'
WHERE clause_id = 72906; -- 252.204-7015

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1205_67000&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1205_67000&rgn=div8'
WHERE clause_id = 72907; -- 252.205-7000

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1206_67000&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1206_67000&rgn=div8'
WHERE clause_id = 72908; -- 252.206-7000

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?node=se48.3.252_1208_67000&amp;rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=033a9dbe0ea0f20cfdd114e4fe8fd8bc&amp;node=se48.3.252_1208_67000&amp;rgn=div8'
WHERE clause_id = 72909; -- 252.208-7000

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?node=se48.3.252_1209_67002&amp;rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=033a9dbe0ea0f20cfdd114e4fe8fd8bc&amp;node=se48.3.252_1209_67002&amp;rgn=div8'
WHERE clause_id = 72910; -- 252.209-7002

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1209_67003&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1209_67003&rgn=div8'
WHERE clause_id = 72911; -- 252.209-7003

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1209_67004&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1209_67004&rgn=div8'
WHERE clause_id = 72912; -- 252.209-7004

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1209_67005&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1209_67005&rgn=div8'
WHERE clause_id = 72913; -- 252.209-7005

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1209_67006&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1209_67006&rgn=div8'
WHERE clause_id = 72914; -- 252.209-7006

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1209_67007&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1209_67007&rgn=div8'
WHERE clause_id = 72915; -- 252.209-7007

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1209_67008&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1209_67008&rgn=div8'
WHERE clause_id = 72916; -- 252.209-7008

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1209_67009&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1209_67009&rgn=div8'
WHERE clause_id = 72917; -- 252.209-7009

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1209_67010&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1209_67010&rgn=div8'
WHERE clause_id = 72918; -- 252.209-7010

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1211_67000&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1211_67000&rgn=div8'
WHERE clause_id = 72927; -- 252.211-7000

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1211_67001&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1211_67001&rgn=div8'
WHERE clause_id = 72928; -- 252.211-7001

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1211_67002&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1211_67002&rgn=div8'
WHERE clause_id = 72929; -- 252.211-7002

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?node=se48.3.252_1211_67003&amp;rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=033a9dbe0ea0f20cfdd114e4fe8fd8bc&amp;node=se48.3.252_1211_67003&amp;rgn=div8'
WHERE clause_id = 72930; -- 252.211-7003

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1211_67004&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1211_67004&rgn=div8'
WHERE clause_id = 72931; -- 252.211-7004

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1211_67005&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1211_67005&rgn=div8'
WHERE clause_id = 72932; -- 252.211-7005

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?node=se48.3.252_1211_67006&amp;rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=033a9dbe0ea0f20cfdd114e4fe8fd8bc&amp;node=se48.3.252_1211_67006&amp;rgn=div8'
WHERE clause_id = 72933; -- 252.211-7006

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1211_67007&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1211_67007&rgn=div8'
WHERE clause_id = 72934; -- 252.211-7007

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1211_67008&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1211_67008&rgn=div8'
WHERE clause_id = 72935; -- 252.211-7008

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1212_67002&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1212_67002&rgn=div8'
WHERE clause_id = 72936; -- 252.212-7002

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1213_67000&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1213_67000&rgn=div8'
WHERE clause_id = 72937; -- 252.213-7000

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1215_67000&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1215_67000&rgn=div8'
WHERE clause_id = 72938; -- 252.215-7000

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1215_67002&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1215_67002&rgn=div8'
WHERE clause_id = 72939; -- 252.215-7002

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1215_67003&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1215_67003&rgn=div8'
WHERE clause_id = 72940; -- 252.215-7003

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1215_67004&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1215_67004&rgn=div8'
WHERE clause_id = 72941; -- 252.215-7004

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1215_67005&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1215_67005&rgn=div8'
WHERE clause_id = 72942; -- 252.215-7005

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1215_67006&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1215_67006&rgn=div8'
WHERE clause_id = 72943; -- 252.215-7006

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1215_67007&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1215_67007&rgn=div8'
WHERE clause_id = 72945; -- 252.215-7007

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1215_67008&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1215_67008&rgn=div8'
WHERE clause_id = 72946; -- 252.215-7008

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?node=se48.3.252_1215_67009&amp;rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=28cf82b7494efcd9810df820f9359a44&amp;node=se48.3.252_1215_67009&amp;rgn=div8'
, clause_data = '<p>The offeror shall complete the following checklist, providing location of requested information, or an explanation of why the requested information is not provided. In preparation of the offeror''s checklist, offerors may elect to have their prospective subcontractors use the same or similar checklist as appropriate.</p><div><p>Proposal Adequacy Checklist</p></div><div><table border="&quot;1&quot;" cellpadding="&quot;1&quot;" cellspacing="&quot;1&quot;" frame="&quot;void&quot;" width="&quot;100%&quot;"><tbody><tr><th scope="&quot;col&quot;">References</th><th scope="&quot;col&quot;">Submission item</th><th scope="&quot;col&quot;">Proposal page No.</th><th scope="&quot;col&quot;">If not provided<br>EXPLAIN<br>(may use<br>continuation<br>pages)</br></br></br></br></th></tr><tr><td align="&quot;center&quot;" colspan="&quot;4&quot;" scope="&quot;row&quot;"><i>GENERAL INSTRUCTIONS</i></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">1. FAR 15.408, Table 15-2, Section I Paragraph A</td><td align="&quot;left&quot;">Is there a properly completed first page of the proposal per FAR 15.408 Table 15-2 I.A or as specified in the solicitation?</td><td align="&quot;right&quot;"></td><td align="&quot;left&quot;"></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">2. FAR 15.408, Table 15-2, Section I Paragraph A(7)</td><td align="&quot;left&quot;">Does the proposal identify the need for Government-furnished material/tooling/test equipment? Include the accountable contract number and contracting officer contact information if known.</td><td align="&quot;right&quot;"></td><td align="&quot;left&quot;"></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">3. FAR 15.408, Table 15-2, Section I Paragraph A(8)</td><td align="&quot;left&quot;">Does the proposal identify and explain notifications of noncompliance with Cost Accounting Standards Board or Cost Accounting Standards (CAS); any proposal inconsistencies with your disclosed practices or applicable CAS; and inconsistencies with your established estimating and accounting principles and procedures?</td><td align="&quot;right&quot;"></td><td align="&quot;left&quot;"></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">4. FAR 15.408, Table 15-2, Section I, Paragraph C(1)</td><td align="&quot;left&quot;">Does the proposal disclose any other known activity that could materially impact the costs?</td><td align="&quot;right&quot;"></td><td align="&quot;left&quot;"></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">FAR 2.101, &ldquo;??Cost or pricing data&ldquo;?</td><td align="&quot;left&quot;">This may include, but is not limited to, such factors as&ldquo;??<br>(1) Vendor quotations;<br>(2) Nonrecurring costs;<br>(3) Information on changes in production methods and in production or purchasing volume;<br>(4) Data supporting projections of business prospects and objectives and related operations costs;</br></br></br></br></td><td align="&quot;right&quot;"></td><td align="&quot;left&quot;"></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">&nbsp;&nbsp;&nbsp;</td><td align="&quot;left&quot;">(5) Unit-cost trends such as those associated with labor efficiency;<br>(6) Make-or-buy decisions;</br></td><td align="&quot;right&quot;"></td><td align="&quot;left&quot;"></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">&nbsp;&nbsp;&nbsp;</td><td align="&quot;left&quot;">(7) Estimated resources to attain business goals; and</td><td align="&quot;right&quot;"></td><td align="&quot;left&quot;"></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">&nbsp;&nbsp;&nbsp;</td><td align="&quot;left&quot;">(8) Information on management decisions that could have a significant bearing on costs.</td><td align="&quot;right&quot;"></td><td align="&quot;left&quot;"></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">5. FAR 15.408, Table 15-2, Section I Paragraph B</td><td align="&quot;left&quot;">Is an Index of all certified cost or pricing data and information accompanying or identified in the proposal provided and appropriately referenced?</td><td align="&quot;right&quot;"></td><td align="&quot;left&quot;"></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">6. FAR 15.403-1(b)</td><td align="&quot;left&quot;">Are there any exceptions to submission of certified cost or pricing data pursuant to FAR 15.403-1(b)? If so, is supporting documentation included in the proposal? (Note questions 18-20.)</td><td align="&quot;right&quot;"></td><td align="&quot;left&quot;"></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">7. FAR 15.408, Table 15-2, Section I Paragraph C(2)(i)</td><td align="&quot;left&quot;">Does the proposal disclose the judgmental factors applied and the mathematical or other methods used in the estimate, including those used in projecting from known data?</td><td align="&quot;right&quot;"></td><td align="&quot;left&quot;"></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">8. FAR 15.408, Table 15-2, Section I Paragraph C(2)(ii)</td><td align="&quot;left&quot;">Does the proposal disclose the nature and amount of any contingencies included in the proposed price?</td><td align="&quot;right&quot;"></td><td align="&quot;left&quot;"></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">9. FAR 15.408 Table 15-2, Section II, Paragraph A or B</td><td align="&quot;left&quot;">Does the proposal explain the basis of all cost estimating relationships (labor hours or material) proposed on other than a discrete basis?</td><td align="&quot;right&quot;"></td><td align="&quot;left&quot;"></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">10. FAR 15.408, Table 15-2, Section I Paragraphs D and E</td><td align="&quot;left&quot;">Is there a summary of total cost by element of cost and are the elements of cost cross-referenced to the supporting cost or pricing data? (Breakdowns for each cost element must be consistent with your cost accounting system, including breakdown by year.)</td><td align="&quot;right&quot;"></td><td align="&quot;left&quot;"></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">11. FAR 15.408, Table 15-2, Section I Paragraphs D and E</td><td align="&quot;left&quot;">If more than one Contract Line Item Number (CLIN) or sub Contract Line Item Number (sub-CLIN) is proposed as required by the RFP, are there summary total amounts covering all line items for each element of cost and is it cross-referenced to the supporting cost or pricing data?</td><td align="&quot;right&quot;"></td><td align="&quot;left&quot;"></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">12. FAR 15.408, Table 15-2, Section I Paragraph F</td><td align="&quot;left&quot;">Does the proposal identify any incurred costs for work performed before the submission of the proposal?</td><td align="&quot;right&quot;"></td><td align="&quot;left&quot;"></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">13. FAR 15.408, Table 15-2, Section I Paragraph G</td><td align="&quot;left&quot;">Is there a Government forward pricing rate agreement (FPRA)? If so, the offeror shall identify the official submittal of such rate and factor data. If not, does the proposal include all rates and factors by year that are utilized in the development of the proposal and the basis for those rates and factors?</td><td align="&quot;right&quot;"></td><td align="&quot;left&quot;"></td></tr><tr><td align="&quot;center&quot;" colspan="&quot;4&quot;" scope="&quot;row&quot;"><i>COST ELEMENTS</i></td></tr><tr><td align="&quot;center&quot;" colspan="&quot;4&quot;" scope="&quot;row&quot;">MATERIALS AND SERVICES</td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">14. FAR 15.408, Table 15-2, Section II Paragraph A</td><td align="&quot;left&quot;">Does the proposal include a consolidated summary of individual material and services, frequently referred to as a Consolidated Bill of Material (CBOM), to include the basis for pricing? The offeror''s consolidated summary shall include raw materials, parts, components, assemblies, subcontracts and services to be produced or performed by others, identifying as a minimum the item, source, quantity, and price.</td><td align="&quot;right&quot;"></td><td align="&quot;left&quot;"></td></tr><tr><td align="&quot;center&quot;" colspan="&quot;4&quot;" scope="&quot;row&quot;">SUBCONTRACTS (Purchased materials or services)</td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">15. DFARS 215.404-3</td><td align="&quot;left&quot;">Has the offeror identified in the proposal those subcontractor proposals, for which the contracting officer has initiated or may need to request field pricing analysis?</td><td align="&quot;right&quot;"></td><td align="&quot;left&quot;"></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">16. FAR 15.404-3(c)<br>FAR 52.244-2</br></td><td align="&quot;left&quot;">Per the thresholds of FAR 15.404-3(c), Subcontract Pricing Considerations, does the proposal include a copy of the applicable subcontractor''s certified cost or pricing data?</td><td align="&quot;right&quot;"></td><td align="&quot;left&quot;"></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">17. FAR 15.408, Table 15-2, Note 1; Section II Paragraph A</td><td align="&quot;left&quot;">Is there a price/cost analysis establishing the reasonableness of each of the proposed subcontracts included with the proposal? If the offeror''s price/cost analyses are not provided with the proposal, does the proposal include a matrix identifying dates for receipt of subcontractor proposal, completion of fact finding for purposes of price/cost analysis, and submission of the price/cost analysis?</td><td align="&quot;right&quot;"></td><td align="&quot;left&quot;"></td></tr><tr><td align="&quot;center&quot;" colspan="&quot;4&quot;" scope="&quot;row&quot;"><i>EXCEPTIONS TO CERTIFIED COST OR PRICING DATA</i></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">18. FAR 52.215-20<br>FAR 2.101, &ldquo;??commercial item&ldquo;?</br></td><td align="&quot;left&quot;">Has the offeror submitted an exception to the submission of certified cost or pricing data for commercial items proposed either at the prime or subcontractor level, in accordance with provision 52.215-20?</td><td align="&quot;right&quot;"></td><td align="&quot;left&quot;"></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">&nbsp;&nbsp;&nbsp;</td><td align="&quot;left&quot;">a. Has the offeror specifically identified the type of commercial item claim (FAR 2.101 commercial item definition, paragraphs (1) through (8)), and the basis on which the item meets the definition?</td><td align="&quot;right&quot;"></td><td align="&quot;left&quot;"></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">&nbsp;&nbsp;&nbsp;</td><td align="&quot;left&quot;">b. For modified commercial items (FAR 2.101 commercial item definition paragraph (3)); did the offeror classify the modification(s) as either&ldquo;??</td><td align="&quot;right&quot;"></td><td align="&quot;left&quot;"></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">&nbsp;&nbsp;&nbsp;</td><td align="&quot;left&quot;">i. A modification of a type customarily available in the commercial marketplace (paragraph (3)(i)); or</td><td align="&quot;right&quot;"></td><td align="&quot;left&quot;"></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">&nbsp;&nbsp;&nbsp;</td><td align="&quot;left&quot;">ii. A minor modification (paragraph (3)(ii)) of a type not customarily available in the commercial marketplace made to meet Federal Government requirements not exceeding the thresholds in FAR 15.403-1(c)(3)(iii)(B)?</td><td align="&quot;right&quot;"></td><td align="&quot;left&quot;"></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">&nbsp;&nbsp;&nbsp;</td><td align="&quot;left&quot;">c. For proposed commercial items &ldquo;??of a type&ldquo;?, or &ldquo;??evolved&ldquo;? or modified (FAR 2.101 commercial item definition paragraphs (1) through (3)), did the contractor provide a technical description of the differences between the proposed item and the comparison item(s)?</td><td align="&quot;right&quot;"></td><td align="&quot;left&quot;"></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">19.</td><td align="&quot;left&quot;">[Reserved]</td><td align="&quot;right&quot;"></td><td align="&quot;left&quot;"></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">20. FAR 15.408, Table 15-2, Section II Paragraph A(1)</td><td align="&quot;left&quot;">Does the proposal support the degree of competition and the basis for establishing the source and reasonableness of price for each subcontract or purchase order priced on a competitive basis exceeding the threshold for certified cost or pricing data?</td><td align="&quot;right&quot;"></td><td align="&quot;left&quot;"></td></tr><tr><td align="&quot;center&quot;" colspan="&quot;4&quot;" scope="&quot;row&quot;">INTERORGANIZATIONAL TRANSFERS</td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">21. FAR 15.408, Table 15-2, Section II Paragraph A.(2)</td><td align="&quot;left&quot;">For inter-organizational transfers proposed at cost, does the proposal include a complete cost proposal in compliance with Table 15-2?</td><td align="&quot;right&quot;"></td><td align="&quot;left&quot;"></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">22. FAR 15.408, Table 15-2, Section II Paragraph A(1)</td><td align="&quot;left&quot;">For inter-organizational transfers proposed at price in accordance with FAR 31.205-26(e), does the proposal provide an analysis by the prime that supports the exception from certified cost or pricing data in accordance with FAR 15.403-1?</td><td align="&quot;right&quot;"></td><td align="&quot;left&quot;"></td></tr><tr><td align="&quot;center&quot;" colspan="&quot;4&quot;" scope="&quot;row&quot;">DIRECT LABOR</td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">23. FAR 15.408, Table 15-2, Section II Paragraph B</td><td align="&quot;left&quot;">Does the proposal include a time phased (<i>i.e.</i>; monthly, quarterly) breakdown of labor hours, rates and costs by category or skill level? If labor is the allocation base for indirect costs, the labor cost must be summarized in order that the applicable overhead rate can be applied.</td><td align="&quot;right&quot;"></td><td align="&quot;left&quot;"></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">24. FAR 15.408, Table 15-2, Section II Paragraph B</td><td align="&quot;left&quot;">For labor Basis of Estimates (BOEs), does the proposal include labor categories, labor hours, and task descriptions&ldquo;??(e.g.; Statement of Work reference, applicable CLIN, Work Breakdown Structure, rationale for estimate, applicable history, and time-phasing)?</td><td align="&quot;right&quot;"></td><td align="&quot;left&quot;"></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">25. FAR subpart 22.10</td><td align="&quot;left&quot;">If covered by the Service Contract Labor Standards statute (41 U.S.C. chapter 67), are the rates in the proposal in compliance with the minimum rates specified in the statute?</td><td align="&quot;right&quot;"></td><td align="&quot;left&quot;"></td></tr><tr><td align="&quot;center&quot;" colspan="&quot;4&quot;" scope="&quot;row&quot;"><i>INDIRECT COSTS</i></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">26. FAR 15.408, Table 15-2, Section II Paragraph C</td><td align="&quot;left&quot;">Does the proposal indicate the basis of estimate for proposed indirect costs and how they are applied? (Support for the indirect rates could consist of cost breakdowns, trends, and budgetary data.)</td><td align="&quot;right&quot;"></td><td align="&quot;left&quot;"></td></tr><tr><td align="&quot;center&quot;" colspan="&quot;4&quot;" scope="&quot;row&quot;"><i>OTHER COSTS</i></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">27. FAR 15.408, Table 15-2, Section II Paragraph D</td><td align="&quot;left&quot;">Does the proposal include other direct costs and the basis for pricing? If travel is included does the proposal include number of trips, number of people, number of days per trip, locations, and rates (e.g. airfare, per diem, hotel, car rental, etc)?</td><td align="&quot;right&quot;"></td><td align="&quot;left&quot;"></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">28. FAR 15.408, Table 15-2, Section II Paragraph E</td><td align="&quot;left&quot;">If royalties exceed $1,500 does the proposal provide the information/data identified by Table 15-2?</td><td align="&quot;right&quot;"></td><td align="&quot;left&quot;"></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">29. FAR 15.408, Table 15-2, Section II Paragraph F</td><td align="&quot;left&quot;">When facilities capital cost of money is proposed, does the proposal include submission of Form CASB-CMF or reference to an FPRA/FPRP and show the calculation of the proposed amount?</td><td align="&quot;right&quot;"></td><td align="&quot;left&quot;"></td></tr><tr><td align="&quot;center&quot;" colspan="&quot;4&quot;" scope="&quot;row&quot;"><i>FORMATS FOR SUBMISSION OF LINE ITEM SUMMARIES</i></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">30. FAR 15.408, Table 15-2, Section III</td><td align="&quot;left&quot;">Are all cost element breakdowns provided using the applicable format prescribed in FAR 15.408, Table 15-2 III? (or alternative format if specified in the request for proposal)</td><td align="&quot;right&quot;"></td><td align="&quot;left&quot;"></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">31. FAR 15.408, Table 15-2, Section III Paragraph B</td><td align="&quot;left&quot;">If the proposal is for a modification or change order, have cost of work deleted (credits) and cost of work added (debits) been provided in the format described in FAR 15.408, Table 15-2.III.B?</td><td align="&quot;right&quot;"></td><td align="&quot;left&quot;"></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">32. FAR 15.408, Table 15-2, Section III Paragraph C</td><td align="&quot;left&quot;">For price revisions/redeterminations, does the proposal follow the format in FAR 15.408, Table 15-2.III.C?</td><td align="&quot;right&quot;"></td><td align="&quot;left&quot;"></td></tr><tr><td align="&quot;center&quot;" colspan="&quot;4&quot;" scope="&quot;row&quot;"><i>OTHER</i></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">33. FAR 16.4</td><td align="&quot;left&quot;">If an incentive contract type, does the proposal include offeror proposed target cost, target profit or fee, share ratio, and, when applicable, minimum/maximum fee, ceiling price?</td><td align="&quot;right&quot;"></td><td align="&quot;left&quot;"></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">34. FAR 16.203-4 and FAR 15.408 Table 15-2, Section II, Paragraphs A, B, C, and D</td><td align="&quot;left&quot;">If Economic Price Adjustments are being proposed, does the proposal show the rationale and application for the economic price adjustment?</td><td align="&quot;right&quot;"></td><td align="&quot;left&quot;"></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">35. FAR 52.232-28</td><td align="&quot;left&quot;">If the offeror is proposing Performance-Based Payments&ldquo;??did the offeror comply with FAR 52.232-28?</td><td align="&quot;right&quot;"></td><td align="&quot;left&quot;"></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">36. FAR 15.408(n)<br>FAR 52.215-22<br>FAR 52.215-23</br></br></td><td align="&quot;left&quot;">Excessive Pass-through Charges&ldquo;??Identification of Subcontract Effort: If the offeror intends to subcontract more than 70% of the total cost of work to be performed, does the proposal identify:<br>(i) the amount of the offeror''s indirect costs and profit applicable to the work to be performed by the proposed subcontractor(s); and (ii) a description of the added value provided by the offeror as related to the work to be performed by the proposed subcontractor(s)?</br></td><td align="&quot;right&quot;"></td><td align="&quot;left&quot;"></td></tr></tbody></table></div>'
--               '<p>The offeror shall complete the following checklist, providing location of requested information, or an explanation of why the requested information is not provided. In preparation of the offeror''s checklist, offerors may elect to have their prospective subcontractors use the same or similar checklist as appropriate.</p><div><p>Proposal Adequacy Checklist</p></div><div><table border="&quot;1&quot;" cellpadding="&quot;1&quot;" cellspacing="&quot;1&quot;" frame="&quot;void&quot;" width="&quot;100%&quot;"><tbody><tr><th scope="&quot;col&quot;">References</th><th scope="&quot;col&quot;">Submission item</th><th scope="&quot;col&quot;">Proposal page No.</th><th scope="&quot;col&quot;">If not provided<br>EXPLAIN<br>(may use<br>continuation<br>pages)</br></br></br></br></th></tr><tr><td align="&quot;center&quot;" colspan="&quot;4&quot;" scope="&quot;row&quot;"><i>GENERAL INSTRUCTIONS</i></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">1. FAR 15.408, Table 15-2, Section I Paragraph A</td><td align="&quot;left&quot;">Is there a properly completed first page of the proposal per FAR 15.408 Table 15-2 I.A or as specified in the solicitation?</td><td align="&quot;right&quot;"></td><td align="&quot;left&quot;"></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">2. FAR 15.408, Table 15-2, Section I Paragraph A(7)</td><td align="&quot;left&quot;">Does the proposal identify the need for Government-furnished material/tooling/test equipment? Include the accountable contract number and contracting officer contact information if known.</td><td align="&quot;right&quot;"></td><td align="&quot;left&quot;"></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">3. FAR 15.408, Table 15-2, Section I Paragraph A(8)</td><td align="&quot;left&quot;">Does the proposal identify and explain notifications of noncompliance with Cost Accounting Standards Board or Cost Accounting Standards (CAS); any proposal inconsistencies with your disclosed practices or applicable CAS; and inconsistencies with your established estimating and accounting principles and procedures?</td><td align="&quot;right&quot;"></td><td align="&quot;left&quot;"></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">4. FAR 15.408, Table 15-2, Section I, Paragraph C(1)</td><td align="&quot;left&quot;">Does the proposal disclose any other known activity that could materially impact the costs?</td><td align="&quot;right&quot;"></td><td align="&quot;left&quot;"></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">FAR 2.101, &ldquo;??Cost or pricing data&ldquo;?</td><td align="&quot;left&quot;">This may include, but is not limited to, such factors as&ldquo;??<br>(1) Vendor quotations;<br>(2) Nonrecurring costs;<br>(3) Information on changes in production methods and in production or purchasing volume;<br>(4) Data supporting projections of business prospects and objectives and related operations costs;</br></br></br></br></td><td align="&quot;right&quot;"></td><td align="&quot;left&quot;"></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">Â&nbsp;Â&nbsp;Â&nbsp;</td><td align="&quot;left&quot;">(5) Unit-cost trends such as those associated with labor efficiency;<br>(6) Make-or-buy decisions;</br></td><td align="&quot;right&quot;"></td><td align="&quot;left&quot;"></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">Â&nbsp;Â&nbsp;Â&nbsp;</td><td align="&quot;left&quot;">(7) Estimated resources to attain business goals; and</td><td align="&quot;right&quot;"></td><td align="&quot;left&quot;"></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">Â&nbsp;Â&nbsp;Â&nbsp;</td><td align="&quot;left&quot;">(8) Information on management decisions that could have a significant bearing on costs.</td><td align="&quot;right&quot;"></td><td align="&quot;left&quot;"></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">5. FAR 15.408, Table 15-2, Section I Paragraph B</td><td align="&quot;left&quot;">Is an Index of all certified cost or pricing data and information accompanying or identified in the proposal provided and appropriately referenced?</td><td align="&quot;right&quot;"></td><td align="&quot;left&quot;"></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">6. FAR 15.403-1(b)</td><td align="&quot;left&quot;">Are there any exceptions to submission of certified cost or pricing data pursuant to FAR 15.403-1(b)? If so, is supporting documentation included in the proposal? (Note questions 18-20.)</td><td align="&quot;right&quot;"></td><td align="&quot;left&quot;"></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">7. FAR 15.408, Table 15-2, Section I Paragraph C(2)(i)</td><td align="&quot;left&quot;">Does the proposal disclose the judgmental factors applied and the mathematical or other methods used in the estimate, including those used in projecting from known data?</td><td align="&quot;right&quot;"></td><td align="&quot;left&quot;"></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">8. FAR 15.408, Table 15-2, Section I Paragraph C(2)(ii)</td><td align="&quot;left&quot;">Does the proposal disclose the nature and amount of any contingencies included in the proposed price?</td><td align="&quot;right&quot;"></td><td align="&quot;left&quot;"></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">9. FAR 15.408 Table 15-2, Section II, Paragraph A or B</td><td align="&quot;left&quot;">Does the proposal explain the basis of all cost estimating relationships (labor hours or material) proposed on other than a discrete basis?</td><td align="&quot;right&quot;"></td><td align="&quot;left&quot;"></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">10. FAR 15.408, Table 15-2, Section I Paragraphs D and E</td><td align="&quot;left&quot;">Is there a summary of total cost by element of cost and are the elements of cost cross-referenced to the supporting cost or pricing data? (Breakdowns for each cost element must be consistent with your cost accounting system, including breakdown by year.)</td><td align="&quot;right&quot;"></td><td align="&quot;left&quot;"></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">11. FAR 15.408, Table 15-2, Section I Paragraphs D and E</td><td align="&quot;left&quot;">If more than one Contract Line Item Number (CLIN) or sub Contract Line Item Number (sub-CLIN) is proposed as required by the RFP, are there summary total amounts covering all line items for each element of cost and is it cross-referenced to the supporting cost or pricing data?</td><td align="&quot;right&quot;"></td><td align="&quot;left&quot;"></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">12. FAR 15.408, Table 15-2, Section I Paragraph F</td><td align="&quot;left&quot;">Does the proposal identify any incurred costs for work performed before the submission of the proposal?</td><td align="&quot;right&quot;"></td><td align="&quot;left&quot;"></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">13. FAR 15.408, Table 15-2, Section I Paragraph G</td><td align="&quot;left&quot;">Is there a Government forward pricing rate agreement (FPRA)? If so, the offeror shall identify the official submittal of such rate and factor data. If not, does the proposal include all rates and factors by year that are utilized in the development of the proposal and the basis for those rates and factors?</td><td align="&quot;right&quot;"></td><td align="&quot;left&quot;"></td></tr><tr><td align="&quot;center&quot;" colspan="&quot;4&quot;" scope="&quot;row&quot;"><i>COST ELEMENTS</i></td></tr><tr><td align="&quot;center&quot;" colspan="&quot;4&quot;" scope="&quot;row&quot;">MATERIALS AND SERVICES</td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">14. FAR 15.408, Table 15-2, Section II Paragraph A</td><td align="&quot;left&quot;">Does the proposal include a consolidated summary of individual material and services, frequently referred to as a Consolidated Bill of Material (CBOM), to include the basis for pricing? The offeror''s consolidated summary shall include raw materials, parts, components, assemblies, subcontracts and services to be produced or performed by others, identifying as a minimum the item, source, quantity, and price.</td><td align="&quot;right&quot;"></td><td align="&quot;left&quot;"></td></tr><tr><td align="&quot;center&quot;" colspan="&quot;4&quot;" scope="&quot;row&quot;">SUBCONTRACTS (Purchased materials or services)</td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">15. DFARS 215.404-3</td><td align="&quot;left&quot;">Has the offeror identified in the proposal those subcontractor proposals, for which the contracting officer has initiated or may need to request field pricing analysis?</td><td align="&quot;right&quot;"></td><td align="&quot;left&quot;"></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">16. FAR 15.404-3(c)<br>FAR 52.244-2</br></td><td align="&quot;left&quot;">Per the thresholds of FAR 15.404-3(c), Subcontract Pricing Considerations, does the proposal include a copy of the applicable subcontractor''s certified cost or pricing data?</td><td align="&quot;right&quot;"></td><td align="&quot;left&quot;"></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">17. FAR 15.408, Table 15-2, Note 1; Section II Paragraph A</td><td align="&quot;left&quot;">Is there a price/cost analysis establishing the reasonableness of each of the proposed subcontracts included with the proposal? If the offeror''s price/cost analyses are not provided with the proposal, does the proposal include a matrix identifying dates for receipt of subcontractor proposal, completion of fact finding for purposes of price/cost analysis, and submission of the price/cost analysis?</td><td align="&quot;right&quot;"></td><td align="&quot;left&quot;"></td></tr><tr><td align="&quot;center&quot;" colspan="&quot;4&quot;" scope="&quot;row&quot;"><i>EXCEPTIONS TO CERTIFIED COST OR PRICING DATA</i></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">18. FAR 52.215-20<br>FAR 2.101, &ldquo;??commercial item&ldquo;?</br></td><td align="&quot;left&quot;">Has the offeror submitted an exception to the submission of certified cost or pricing data for commercial items proposed either at the prime or subcontractor level, in accordance with provision 52.215-20?</td><td align="&quot;right&quot;"></td><td align="&quot;left&quot;"></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">Â&nbsp;Â&nbsp;Â&nbsp;</td><td align="&quot;left&quot;">a. Has the offeror specifically identified the type of commercial item claim (FAR 2.101 commercial item definition, paragraphs (1) through (8)), and the basis on which the item meets the definition?</td><td align="&quot;right&quot;"></td><td align="&quot;left&quot;"></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">Â&nbsp;Â&nbsp;Â&nbsp;</td><td align="&quot;left&quot;">b. For modified commercial items (FAR 2.101 commercial item definition paragraph (3)); did the offeror classify the modification(s) as either&ldquo;??</td><td align="&quot;right&quot;"></td><td align="&quot;left&quot;"></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">Â&nbsp;Â&nbsp;Â&nbsp;</td><td align="&quot;left&quot;">i. A modification of a type customarily available in the commercial marketplace (paragraph (3)(i)); or</td><td align="&quot;right&quot;"></td><td align="&quot;left&quot;"></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">Â&nbsp;Â&nbsp;Â&nbsp;</td><td align="&quot;left&quot;">ii. A minor modification (paragraph (3)(ii)) of a type not customarily available in the commercial marketplace made to meet Federal Government requirements not exceeding the thresholds in FAR 15.403-1(c)(3)(iii)(B)?</td><td align="&quot;right&quot;"></td><td align="&quot;left&quot;"></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">Â&nbsp;Â&nbsp;Â&nbsp;</td><td align="&quot;left&quot;">c. For proposed commercial items &ldquo;??of a type&ldquo;?, or &ldquo;??evolved&ldquo;? or modified (FAR 2.101 commercial item definition paragraphs (1) through (3)), did the contractor provide a technical description of the differences between the proposed item and the comparison item(s)?</td><td align="&quot;right&quot;"></td><td align="&quot;left&quot;"></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">19.</td><td align="&quot;left&quot;">[Reserved]</td><td align="&quot;right&quot;"></td><td align="&quot;left&quot;"></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">20. FAR 15.408, Table 15-2, Section II Paragraph A(1)</td><td align="&quot;left&quot;">Does the proposal support the degree of competition and the basis for establishing the source and reasonableness of price for each subcontract or purchase order priced on a competitive basis exceeding the threshold for certified cost or pricing data?</td><td align="&quot;right&quot;"></td><td align="&quot;left&quot;"></td></tr><tr><td align="&quot;center&quot;" colspan="&quot;4&quot;" scope="&quot;row&quot;">INTERORGANIZATIONAL TRANSFERS</td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">21. FAR 15.408, Table 15-2, Section II Paragraph A.(2)</td><td align="&quot;left&quot;">For inter-organizational transfers proposed at cost, does the proposal include a complete cost proposal in compliance with Table 15-2?</td><td align="&quot;right&quot;"></td><td align="&quot;left&quot;"></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">22. FAR 15.408, Table 15-2, Section II Paragraph A(1)</td><td align="&quot;left&quot;">For inter-organizational transfers proposed at price in accordance with FAR 31.205-26(e), does the proposal provide an analysis by the prime that supports the exception from certified cost or pricing data in accordance with FAR 15.403-1?</td><td align="&quot;right&quot;"></td><td align="&quot;left&quot;"></td></tr><tr><td align="&quot;center&quot;" colspan="&quot;4&quot;" scope="&quot;row&quot;">DIRECT LABOR</td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">23. FAR 15.408, Table 15-2, Section II Paragraph B</td><td align="&quot;left&quot;">Does the proposal include a time phased (<i>i.e.</i>; monthly, quarterly) breakdown of labor hours, rates and costs by category or skill level? If labor is the allocation base for indirect costs, the labor cost must be summarized in order that the applicable overhead rate can be applied.</td><td align="&quot;right&quot;"></td><td align="&quot;left&quot;"></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">24. FAR 15.408, Table 15-2, Section II Paragraph B</td><td align="&quot;left&quot;">For labor Basis of Estimates (BOEs), does the proposal include labor categories, labor hours, and task descriptions&ldquo;??(e.g.; Statement of Work reference, applicable CLIN, Work Breakdown Structure, rationale for estimate, applicable history, and time-phasing)?</td><td align="&quot;right&quot;"></td><td align="&quot;left&quot;"></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">25. FAR subpart 22.10</td><td align="&quot;left&quot;">If covered by the Service Contract Labor Standards statute (41 U.S.C. chapter 67), are the rates in the proposal in compliance with the minimum rates specified in the statute?</td><td align="&quot;right&quot;"></td><td align="&quot;left&quot;"></td></tr><tr><td align="&quot;center&quot;" colspan="&quot;4&quot;" scope="&quot;row&quot;"><i>INDIRECT COSTS</i></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">26. FAR 15.408, Table 15-2, Section II Paragraph C</td><td align="&quot;left&quot;">Does the proposal indicate the basis of estimate for proposed indirect costs and how they are applied? (Support for the indirect rates could consist of cost breakdowns, trends, and budgetary data.)</td><td align="&quot;right&quot;"></td><td align="&quot;left&quot;"></td></tr><tr><td align="&quot;center&quot;" colspan="&quot;4&quot;" scope="&quot;row&quot;"><i>OTHER COSTS</i></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">27. FAR 15.408, Table 15-2, Section II Paragraph D</td><td align="&quot;left&quot;">Does the proposal include other direct costs and the basis for pricing? If travel is included does the proposal include number of trips, number of people, number of days per trip, locations, and rates (e.g. airfare, per diem, hotel, car rental, etc)?</td><td align="&quot;right&quot;"></td><td align="&quot;left&quot;"></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">28. FAR 15.408, Table 15-2, Section II Paragraph E</td><td align="&quot;left&quot;">If royalties exceed $1,500 does the proposal provide the information/data identified by Table 15-2?</td><td align="&quot;right&quot;"></td><td align="&quot;left&quot;"></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">29. FAR 15.408, Table 15-2, Section II Paragraph F</td><td align="&quot;left&quot;">When facilities capital cost of money is proposed, does the proposal include submission of Form CASB-CMF or reference to an FPRA/FPRP and show the calculation of the proposed amount?</td><td align="&quot;right&quot;"></td><td align="&quot;left&quot;"></td></tr><tr><td align="&quot;center&quot;" colspan="&quot;4&quot;" scope="&quot;row&quot;"><i>FORMATS FOR SUBMISSION OF LINE ITEM SUMMARIES</i></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">30. FAR 15.408, Table 15-2, Section III</td><td align="&quot;left&quot;">Are all cost element breakdowns provided using the applicable format prescribed in FAR 15.408, Table 15-2 III? (or alternative format if specified in the request for proposal)</td><td align="&quot;right&quot;"></td><td align="&quot;left&quot;"></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">31. FAR 15.408, Table 15-2, Section III Paragraph B</td><td align="&quot;left&quot;">If the proposal is for a modification or change order, have cost of work deleted (credits) and cost of work added (debits) been provided in the format described in FAR 15.408, Table 15-2.III.B?</td><td align="&quot;right&quot;"></td><td align="&quot;left&quot;"></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">32. FAR 15.408, Table 15-2, Section III Paragraph C</td><td align="&quot;left&quot;">For price revisions/redeterminations, does the proposal follow the format in FAR 15.408, Table 15-2.III.C?</td><td align="&quot;right&quot;"></td><td align="&quot;left&quot;"></td></tr><tr><td align="&quot;center&quot;" colspan="&quot;4&quot;" scope="&quot;row&quot;"><i>OTHER</i></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">33. FAR 16.4</td><td align="&quot;left&quot;">If an incentive contract type, does the proposal include offeror proposed target cost, target profit or fee, share ratio, and, when applicable, minimum/maximum fee, ceiling price?</td><td align="&quot;right&quot;"></td><td align="&quot;left&quot;"></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">34. FAR 16.203-4 and FAR 15.408 Table 15-2, Section II, Paragraphs A, B, C, and D</td><td align="&quot;left&quot;">If Economic Price Adjustments are being proposed, does the proposal show the rationale and application for the economic price adjustment?</td><td align="&quot;right&quot;"></td><td align="&quot;left&quot;"></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">35. FAR 52.232-28</td><td align="&quot;left&quot;">If the offeror is proposing Performance-Based Payments&ldquo;??did the offeror comply with FAR 52.232-28?</td><td align="&quot;right&quot;"></td><td align="&quot;left&quot;"></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">36. FAR 15.408(n)<br>FAR 52.215-22<br>FAR 52.215-23</br></br></td><td align="&quot;left&quot;">Excessive Pass-through Charges&ldquo;??Identification of Subcontract Effort: If the offeror intends to subcontract more than 70% of the total cost of work to be performed, does the proposal identify:<br>(i) the amount of the offeror''s indirect costs and profit applicable to the work to be performed by the proposed subcontractor(s); and (ii) a description of the added value provided by the offeror as related to the work to be performed by the proposed subcontractor(s)?</br></td><td align="&quot;right&quot;"></td><td align="&quot;left&quot;"></td></tr></tbody></table></div>'
WHERE clause_id = 72947; -- 252.215-7009

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1216_67000&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1216_67000&rgn=div8'
WHERE clause_id = 72948; -- 252.216-7000

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1216_67001&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1216_67001&rgn=div8'
WHERE clause_id = 72949; -- 252.216-7001

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1216_67002&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1216_67002&rgn=div8'
WHERE clause_id = 72950; -- 252.216-7002

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1216_67003&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1216_67003&rgn=div8'
WHERE clause_id = 72951; -- 252.216-7003

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1216_67004&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1216_67004&rgn=div8'
WHERE clause_id = 72952; -- 252.216-7004

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1216_67005&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1216_67005&rgn=div8'
WHERE clause_id = 72953; -- 252.216-7005

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1216_67006&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1216_67006&rgn=div8'
WHERE clause_id = 72954; -- 252.216-7006

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1216_67007&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1216_67007&rgn=div8'
WHERE clause_id = 72955; -- 252.216-7007

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1216_67008&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1216_67008&rgn=div8'
WHERE clause_id = 72956; -- 252.216-7008

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1216_67009&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1216_67009&rgn=div8'
WHERE clause_id = 72957; -- 252.216-7009

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1216_67010&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1216_67010&rgn=div8'
WHERE clause_id = 72959; -- 252.216-7010

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1217_67000&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1217_67000&rgn=div8'
WHERE clause_id = 72961; -- 252.217-7000

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1217_67001&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1217_67001&rgn=div8'
WHERE clause_id = 72962; -- 252.217-7001

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1217_67002&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1217_67002&rgn=div8'
WHERE clause_id = 72963; -- 252.217-7002

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1217_67003&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1217_67003&rgn=div8'
WHERE clause_id = 72964; -- 252.217-7003

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1217_67004&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1217_67004&rgn=div8'
WHERE clause_id = 72965; -- 252.217-7004

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1217_67005&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1217_67005&rgn=div8'
WHERE clause_id = 72966; -- 252.217-7005

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1217_67006&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1217_67006&rgn=div8'
WHERE clause_id = 72967; -- 252.217-7006

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1217_67007&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1217_67007&rgn=div8'
WHERE clause_id = 72968; -- 252.217-7007

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1217_67008&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1217_67008&rgn=div8'
WHERE clause_id = 72969; -- 252.217-7008

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1217_67009&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1217_67009&rgn=div8'
WHERE clause_id = 72970; -- 252.217-7009

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1217_67010&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1217_67010&rgn=div8'
WHERE clause_id = 72971; -- 252.217-7010

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1217_67011&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1217_67011&rgn=div8'
WHERE clause_id = 72972; -- 252.217-7011

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1217_67012&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1217_67012&rgn=div8'
WHERE clause_id = 72973; -- 252.217-7012

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1217_67013&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1217_67013&rgn=div8'
WHERE clause_id = 72974; -- 252.217-7013

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1217_67014&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1217_67014&rgn=div8'
WHERE clause_id = 72975; -- 252.217-7014

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1217_67015&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1217_67015&rgn=div8'
WHERE clause_id = 72976; -- 252.217-7015

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1217_67016&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1217_67016&rgn=div8'
WHERE clause_id = 72977; -- 252.217-7016

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1217_67026&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1217_67026&rgn=div8'
WHERE clause_id = 72978; -- 252.217-7026

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1217_67027&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1217_67027&rgn=div8'
WHERE clause_id = 72979; -- 252.217-7027

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1217_67028&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1217_67028&rgn=div8'
WHERE clause_id = 72980; -- 252.217-7028

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1219_67000&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1219_67000&rgn=div8'
WHERE clause_id = 72981; -- 252.219-7000

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1219_67003&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1219_67003&rgn=div8'
WHERE clause_id = 72985; -- 252.219-7003

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1219_67004&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1219_67004&rgn=div8'
WHERE clause_id = 72986; -- 252.219-7004

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1219_67009&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1219_67009&rgn=div8'
WHERE clause_id = 72987; -- 252.219-7009

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1219_67010&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1219_67010&rgn=div8'
WHERE clause_id = 72988; -- 252.219-7010

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1219_67011&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1219_67011&rgn=div8'
WHERE clause_id = 72989; -- 252.219-7011

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1222_67000&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1222_67000&rgn=div8'
WHERE clause_id = 72990; -- 252.222-7000

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1222_67001&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1222_67001&rgn=div8'
WHERE clause_id = 72991; -- 252.222-7001

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1222_67002&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1222_67002&rgn=div8'
WHERE clause_id = 72992; -- 252.222-7002

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1222_67003&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1222_67003&rgn=div8'
WHERE clause_id = 72993; -- 252.222-7003

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1222_67004&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1222_67004&rgn=div8'
WHERE clause_id = 72994; -- 252.222-7004

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1222_67005&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1222_67005&rgn=div8'
WHERE clause_id = 72995; -- 252.222-7005

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1222_67006&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1222_67006&rgn=div8'
WHERE clause_id = 72996; -- 252.222-7006

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1222_67007&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1222_67007&rgn=div8'
WHERE clause_id = 72997; -- 252.222-7007

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?node=se48.3.252_1223_67001&amp;rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=033a9dbe0ea0f20cfdd114e4fe8fd8bc&amp;node=se48.3.252_1223_67001&amp;rgn=div8'
WHERE clause_id = 72998; -- 252.223-7001

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1223_67002&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1223_67002&rgn=div8'
WHERE clause_id = 72999; -- 252.223-7002

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1223_67003&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1223_67003&rgn=div8'
WHERE clause_id = 73000; -- 252.223-7003

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1223_67004&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1223_67004&rgn=div8'
WHERE clause_id = 73001; -- 252.223-7004

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1223_67006&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1223_67006&rgn=div8'
WHERE clause_id = 73003; -- 252.223-7006

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?node=se48.3.252_1223_67007&amp;rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=033a9dbe0ea0f20cfdd114e4fe8fd8bc&amp;node=se48.3.252_1223_67007&amp;rgn=div8'
WHERE clause_id = 73004; -- 252.223-7007

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1223_67008&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1223_67008&rgn=div8'
WHERE clause_id = 73005; -- 252.223-7008

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1225_67000&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1225_67000&rgn=div8'
WHERE clause_id = 73007; -- 252.225-7000

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?node=se48.3.252_1225_67001&amp;rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=033a9dbe0ea0f20cfdd114e4fe8fd8bc&amp;node=se48.3.252_1225_67001&amp;rgn=div8'
, clause_data = '<p>(a) <i>Definitions.</i> As used in this clause&mdash;</p><p><i>Commercially available off-the-shelf (COTS) item</i>&mdash;</p><p>(i) Means any item of supply (including construction material) that is&mdash;</p><p>(A) A commercial item (as defined in paragraph (1) of the definition of &ldquo;commercial item&ldquo; in section 2.101 of the Federal Acquisition Regulation);</p><p>(B) Sold in substantial quantities in the commercial marketplace; and</p><p>(C) Offered to the Government, under a contract or subcontract at any tier, without modification, in the same form in which it is sold in the commercial marketplace; and</p><p>(ii) Does not include bulk cargo, as defined in 46 U.S.C. 40102(4), such as agricultural products and petroleum products.</p><p><i>Component</i> means an article, material, or supply incorporated directly into an end product. </p><p><i>Domestic end product means</i>&mdash;</p><p>(i) An unmanufactured end product that has been mined or produced in the United States; or </p><p>(ii) An end product manufactured in the United States if&mdash;</p><p>(A) The cost of its qualifying country components and its components that are mined, produced, or manufactured in the United States exceeds 50 percent of the cost of all its components. The cost of components includes transportation costs to the place of incorporation into the end product and U.S. duty (whether or not a duty-free entry certificate is issued). Scrap generated, collected, and prepared for processing in the United States is considered domestic. A component is considered to have been mined, produced, or manufactured in the United States (regardless of its source in fact) if the end product in which it is incorporated is manufactured in the United States and the component is of a class or kind for which the Government has determined that&mdash;</p><p>(<i>1</i>) Sufficient and reasonably available commercial quantities of a satisfactory quality are not mined, produced, or manufactured in the United States; or</p><p>(<i>2</i>) It is inconsistent with the public interest to apply the restrictions of the Buy American statute; or</p><p>(B) The end product is a COTS item. </p><p><i>End product</i> means those articles, materials, and supplies to be acquired under this contract for public use. </p><p><i>Foreign end product</i> means an end product other than a domestic end product.</p><p><i>Qualifying country</i> means a country with a reciprocal defense procurement memorandum of understanding or international agreement with the United States in which both countries agree to remove barriers to purchases of supplies produced in the other country or services performed by sources of the other country, and the memorandum or agreement complies, where applicable, with the requirements of section 36 of the Arms Export Control Act (22 U.S.C. 2776) and with 10 U.S.C. 2457. Accordingly, the following are qualifying countries:</p><table auto;"=""><tbody><tr><td>Australia</td><td>&nbsp;&nbsp;&nbsp;Italy</td></tr><tr><td>Austria</td><td>&nbsp;&nbsp;&nbsp;Luxembourg</td></tr><tr><td>Belgium</td><td>&nbsp;&nbsp;&nbsp;Netherlands</td></tr><tr><td>Canada</td><td>&nbsp;&nbsp;&nbsp;Norway</td></tr><tr><td>Czech Republic</td><td>&nbsp;&nbsp;&nbsp;Poland</td></tr><tr><td>Denmark</td><td>&nbsp;&nbsp;&nbsp;Portugal</td></tr><tr><td>Egypt</td><td>&nbsp;&nbsp;&nbsp;Spain</td></tr><tr><td>Finland</td><td>&nbsp;&nbsp;&nbsp;Sweden</td></tr><tr><td>France</td><td>&nbsp;&nbsp;&nbsp;Switzerland</td></tr><tr><td>Germany</td><td>&nbsp;&nbsp;&nbsp;Turkey</td></tr><tr><td>Greece</td><td>&nbsp;&nbsp;&nbsp;United Kingdom of Great Britain and Northern Ireland.</td></tr><tr><td>Israel</td><td>&nbsp;</td></tr></tbody></table><p><i>Qualifying country component</i> means a component mined, produced, or manufactured in a qualifying country. </p><p><i>Qualifying country end product</i> means&mdash;</p><p>(i) An unmanufactured end product mined or produced in a qualifying country; or</p><p>(ii) An end product manufactured in a qualifying country if&mdash;</p><p>(A) The cost of the following types of components exceeds 50 percent of the cost of all its components:</p><p>(<i>1</i>) Components mined, produced, or manufactured in a qualifying country.</p><p>(<i>2</i>) Components mined, produced, or manufactured in the United States.</p><p>(<i>3</i>) Components of foreign origin of a class or kind for which the Government has determined that sufficient and reasonably available commercial quantities of a satisfactory quality are not mined, produced, or manufactured in the United States; or</p><p>(B) The end product is a COTS item. </p><p><i>United States</i> means the 50 States, the District of Columbia, and outlying areas.</p><p>(b) This clause implements, Buy American. In accordance with 41 U.S.C. 1907, the component test of the Buy American statute is waived for an end product that is a COTS item (see section 12.505(a)(1) of the Federal Acquisition Regulation). Unless otherwise specified, this clause applies to all line items in the contract.</p><p>(c) The Contractor shall deliver only domestic end products unless, in its offer, it specified delivery of other end products in the Buy American &mdash;Balance of Payments Program Certificate provision of the solicitation. If the Contractor certified in its offer that it will deliver a qualifying country end product, the Contractor shall deliver a qualifying country end product or, at the Contractor''s option, a domestic end product. </p><p>(d) The contract price does not include duty for end products or components for which the Contractor will claim duty-free entry.</p>'
--               '<p>(a) <i>Definitions.</i> As used in this clause&mdash;</p><p><i>Commercially available off-the-shelf (COTS) item</i>&mdash;</p><p>(i) Means any item of supply (including construction material) that is&mdash;</p><p>(A) A commercial item (as defined in paragraph (1) of the definition of &ldquo;commercial item&ldquo; in section 2.101 of the Federal Acquisition Regulation);</p><p>(B) Sold in substantial quantities in the commercial marketplace; and</p><p>(C) Offered to the Government, under a contract or subcontract at any tier, without modification, in the same form in which it is sold in the commercial marketplace; and</p><p>(ii) Does not include bulk cargo, as defined in 46 U.S.C. 40102(4), such as agricultural products and petroleum products.</p><p><i>Component</i> means an article, material, or supply incorporated directly into an end product. </p><p><i>Domestic end product means</i>&mdash;</p><p>(i) An unmanufactured end product that has been mined or produced in the United States; or </p><p>(ii) An end product manufactured in the United States if&mdash;</p><p>(A) The cost of its qualifying country components and its components that are mined, produced, or manufactured in the United States exceeds 50 percent of the cost of all its components. The cost of components includes transportation costs to the place of incorporation into the end product and U.S. duty (whether or not a duty-free entry certificate is issued). Scrap generated, collected, and prepared for processing in the United States is considered domestic. A component is considered to have been mined, produced, or manufactured in the United States (regardless of its source in fact) if the end product in which it is incorporated is manufactured in the United States and the component is of a class or kind for which the Government has determined that&mdash;</p><p>(<i>1</i>) Sufficient and reasonably available commercial quantities of a satisfactory quality are not mined, produced, or manufactured in the United States; or</p><p>(<i>2</i>) It is inconsistent with the public interest to apply the restrictions of the Buy American statute; or</p><p>(B) The end product is a COTS item. </p><p><i>End product</i> means those articles, materials, and supplies to be acquired under this contract for public use. </p><p><i>Foreign end product</i> means an end product other than a domestic end product.</p><p><i>Qualifying country</i> means a country with a reciprocal defense procurement memorandum of understanding or international agreement with the United States in which both countries agree to remove barriers to purchases of supplies produced in the other country or services performed by sources of the other country, and the memorandum or agreement complies, where applicable, with the requirements of section 36 of the Arms Export Control Act (22 U.S.C. 2776) and with 10 U.S.C. 2457. Accordingly, the following are qualifying countries:</p><table auto;"=""><tbody><tr><td>Australia</td><td>Â&nbsp;Â&nbsp;Â&nbsp;Italy</td></tr><tr><td>Austria</td><td>Â&nbsp;Â&nbsp;Â&nbsp;Luxembourg</td></tr><tr><td>Belgium</td><td>Â&nbsp;Â&nbsp;Â&nbsp;Netherlands</td></tr><tr><td>Canada</td><td>Â&nbsp;Â&nbsp;Â&nbsp;Norway</td></tr><tr><td>Czech Republic</td><td>Â&nbsp;Â&nbsp;Â&nbsp;Poland</td></tr><tr><td>Denmark</td><td>Â&nbsp;Â&nbsp;Â&nbsp;Portugal</td></tr><tr><td>Egypt</td><td>Â&nbsp;Â&nbsp;Â&nbsp;Spain</td></tr><tr><td>Finland</td><td>Â&nbsp;Â&nbsp;Â&nbsp;Sweden</td></tr><tr><td>France</td><td>Â&nbsp;Â&nbsp;Â&nbsp;Switzerland</td></tr><tr><td>Germany</td><td>Â&nbsp;Â&nbsp;Â&nbsp;Turkey</td></tr><tr><td>Greece</td><td>Â&nbsp;Â&nbsp;Â&nbsp;United Kingdom of Great Britain and Northern Ireland.</td></tr><tr><td>Israel</td><td>Â&nbsp;</td></tr></tbody></table><p><i>Qualifying country component</i> means a component mined, produced, or manufactured in a qualifying country. </p><p><i>Qualifying country end product</i> means&mdash;</p><p>(i) An unmanufactured end product mined or produced in a qualifying country; or</p><p>(ii) An end product manufactured in a qualifying country if&mdash;</p><p>(A) The cost of the following types of components exceeds 50 percent of the cost of all its components:</p><p>(<i>1</i>) Components mined, produced, or manufactured in a qualifying country.</p><p>(<i>2</i>) Components mined, produced, or manufactured in the United States.</p><p>(<i>3</i>) Components of foreign origin of a class or kind for which the Government has determined that sufficient and reasonably available commercial quantities of a satisfactory quality are not mined, produced, or manufactured in the United States; or</p><p>(B) The end product is a COTS item. </p><p><i>United States</i> means the 50 States, the District of Columbia, and outlying areas.</p><p>(b) This clause implements, Buy American. In accordance with 41 U.S.C. 1907, the component test of the Buy American statute is waived for an end product that is a COTS item (see section 12.505(a)(1) of the Federal Acquisition Regulation). Unless otherwise specified, this clause applies to all line items in the contract.</p><p>(c) The Contractor shall deliver only domestic end products unless, in its offer, it specified delivery of other end products in the Buy American &mdash;Balance of Payments Program Certificate provision of the solicitation. If the Contractor certified in its offer that it will deliver a qualifying country end product, the Contractor shall deliver a qualifying country end product or, at the Contractor''s option, a domestic end product. </p><p>(d) The contract price does not include duty for end products or components for which the Contractor will claim duty-free entry.</p>'
WHERE clause_id = 73009; -- 252.225-7001

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1225_67002&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1225_67002&rgn=div8'
WHERE clause_id = 73010; -- 252.225-7002

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1225_67003&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1225_67003&rgn=div8'
WHERE clause_id = 73011; -- 252.225-7003

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1225_67004&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1225_67004&rgn=div8'
WHERE clause_id = 73012; -- 252.225-7004

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1225_67005&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1225_67005&rgn=div8'
WHERE clause_id = 73013; -- 252.225-7005

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1225_67007&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1225_67007&rgn=div8'
WHERE clause_id = 73014; -- 252.225-7007

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1225_67008&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1225_67008&rgn=div8'
WHERE clause_id = 73015; -- 252.225-7008

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1225_67009&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1225_67009&rgn=div8'
WHERE clause_id = 73016; -- 252.225-7009

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1225_67010&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1225_67010&rgn=div8'
WHERE clause_id = 73017; -- 252.225-7010

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1225_67011&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1225_67011&rgn=div8'
WHERE clause_id = 73018; -- 252.225-7011

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1225_67012&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1225_67012&rgn=div8'
WHERE clause_id = 73019; -- 252.225-7012

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1225_67013&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1225_67013&rgn=div8'
WHERE clause_id = 73020; -- 252.225-7013

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1225_67015&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1225_67015&rgn=div8'
WHERE clause_id = 73021; -- 252.225-7015

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1225_67016&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1225_67016&rgn=div8'
WHERE clause_id = 73022; -- 252.225-7016

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1225_67017&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1225_67017&rgn=div8'
WHERE clause_id = 73023; -- 252.225-7017

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1225_67018&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1225_67018&rgn=div8'
WHERE clause_id = 73024; -- 252.225-7018

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1225_67019&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1225_67019&rgn=div8'
WHERE clause_id = 73025; -- 252.225-7019

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1225_67020&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1225_67020&rgn=div8'
WHERE clause_id = 73027; -- 252.225-7020

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1225_67021&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1225_67021&rgn=div8'
WHERE clause_id = 73029; -- 252.225-7021

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1225_67023&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1225_67023&rgn=div8'
WHERE clause_id = 73030; -- 252.225-7023

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1225_67024&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1225_67024&rgn=div8'
WHERE clause_id = 73031; -- 252.225-7024

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1225_67025&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1225_67025&rgn=div8'
WHERE clause_id = 73032; -- 252.225-7025

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1225_67026&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1225_67026&rgn=div8'
WHERE clause_id = 73033; -- 252.225-7026

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1225_67027&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1225_67027&rgn=div8'
WHERE clause_id = 73034; -- 252.225-7027

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1225_67028&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1225_67028&rgn=div8'
WHERE clause_id = 73035; -- 252.225-7028

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1225_67029&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1225_67029&rgn=div8'
WHERE clause_id = 73036; -- 252.225-7029

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1225_67030&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1225_67030&rgn=div8'
WHERE clause_id = 73037; -- 252.225-7030

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1225_67031&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1225_67031&rgn=div8'
WHERE clause_id = 73038; -- 252.225-7031

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1225_67032&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1225_67032&rgn=div8'
WHERE clause_id = 73039; -- 252.225-7032

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1225_67033&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1225_67033&rgn=div8'
WHERE clause_id = 73040; -- 252.225-7033

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1225_67035&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1225_67035&rgn=div8'
WHERE clause_id = 73046; -- 252.225-7035

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1225_67036&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1225_67036&rgn=div8'
WHERE clause_id = 73052; -- 252.225-7036

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1225_67037&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1225_67037&rgn=div8'
WHERE clause_id = 73053; -- 252.225-7037

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1225_67038&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1225_67038&rgn=div8'
WHERE clause_id = 73054; -- 252.225-7038

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1225_67039&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1225_67039&rgn=div8'
WHERE clause_id = 73055; -- 252.225-7039

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1225_67040&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1225_67040&rgn=div8'
WHERE clause_id = 73056; -- 252.225-7040

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1225_67041&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1225_67041&rgn=div8'
WHERE clause_id = 73057; -- 252.225-7041

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1225_67042&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1225_67042&rgn=div8'
WHERE clause_id = 73058; -- 252.225-7042

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1225_67043&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1225_67043&rgn=div8'
WHERE clause_id = 73059; -- 252.225-7043

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1225_67044&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1225_67044&rgn=div8'
WHERE clause_id = 73061; -- 252.225-7044

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1225_67045&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1225_67045&rgn=div8'
WHERE clause_id = 73065; -- 252.225-7045

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1225_67046&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1225_67046&rgn=div8'
WHERE clause_id = 73066; -- 252.225-7046

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1225_67047&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1225_67047&rgn=div8'
WHERE clause_id = 73067; -- 252.225-7047

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1225_67048&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1225_67048&rgn=div8'
WHERE clause_id = 73068; -- 252.225-7048

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1225_67049&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1225_67049&rgn=div8'
WHERE clause_id = 73069; -- 252.225-7049

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1225_67050&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1225_67050&rgn=div8'
WHERE clause_id = 73070; -- 252.225-7050

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1226_67001&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1226_67001&rgn=div8'
WHERE clause_id = 73089; -- 252.226-7001

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1227_67000&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1227_67000&rgn=div8'
WHERE clause_id = 73090; -- 252.227-7000

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1227_67001&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1227_67001&rgn=div8'
WHERE clause_id = 73091; -- 252.227-7001

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1227_67002&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1227_67002&rgn=div8'
WHERE clause_id = 73092; -- 252.227-7002

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1227_67003&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1227_67003&rgn=div8'
WHERE clause_id = 73093; -- 252.227-7003

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1227_67004&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1227_67004&rgn=div8'
WHERE clause_id = 73094; -- 252.227-7004

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1227_67006&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1227_67006&rgn=div8'
WHERE clause_id = 73097; -- 252.227-7006

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1227_67007&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1227_67007&rgn=div8'
WHERE clause_id = 73098; -- 252.227-7007

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1227_67008&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1227_67008&rgn=div8'
WHERE clause_id = 73099; -- 252.227-7008

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1227_67009&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1227_67009&rgn=div8'
WHERE clause_id = 73100; -- 252.227-7009

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1227_67010&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1227_67010&rgn=div8'
WHERE clause_id = 73101; -- 252.227-7010

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1227_67011&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1227_67011&rgn=div8'
WHERE clause_id = 73102; -- 252.227-7011

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1227_67012&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1227_67012&rgn=div8'
WHERE clause_id = 73103; -- 252.227-7012

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1227_67013&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1227_67013&rgn=div8'
WHERE clause_id = 73106; -- 252.227-7013

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1227_67014&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1227_67014&rgn=div8'
WHERE clause_id = 73108; -- 252.227-7014

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1227_67015&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1227_67015&rgn=div8'
WHERE clause_id = 73110; -- 252.227-7015

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1227_67016&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1227_67016&rgn=div8'
WHERE clause_id = 73111; -- 252.227-7016

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1227_67017&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1227_67017&rgn=div8'
WHERE clause_id = 73112; -- 252.227-7017

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1227_67018&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1227_67018&rgn=div8'
WHERE clause_id = 73114; -- 252.227-7018

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1227_67019&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1227_67019&rgn=div8'
WHERE clause_id = 73115; -- 252.227-7019

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1227_67020&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1227_67020&rgn=div8'
WHERE clause_id = 73116; -- 252.227-7020

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1227_67021&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1227_67021&rgn=div8'
WHERE clause_id = 73117; -- 252.227-7021

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1227_67022&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1227_67022&rgn=div8'
WHERE clause_id = 73118; -- 252.227-7022

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1227_67023&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1227_67023&rgn=div8'
WHERE clause_id = 73119; -- 252.227-7023

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1227_67024&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1227_67024&rgn=div8'
WHERE clause_id = 73120; -- 252.227-7024

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1227_67025&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1227_67025&rgn=div8'
WHERE clause_id = 73121; -- 252.227-7025

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1227_67026&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1227_67026&rgn=div8'
WHERE clause_id = 73122; -- 252.227-7026

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1227_67027&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1227_67027&rgn=div8'
WHERE clause_id = 73123; -- 252.227-7027

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1227_67028&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1227_67028&rgn=div8'
WHERE clause_id = 73124; -- 252.227-7028

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1227_67030&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1227_67030&rgn=div8'
WHERE clause_id = 73125; -- 252.227-7030

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1227_67032&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1227_67032&rgn=div8'
WHERE clause_id = 73126; -- 252.227-7032

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1227_67033&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1227_67033&rgn=div8'
WHERE clause_id = 73127; -- 252.227-7033

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1227_67037&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1227_67037&rgn=div8'
WHERE clause_id = 73128; -- 252.227-7037

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1227_67038&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1227_67038&rgn=div8'
WHERE clause_id = 73129; -- 252.227-7038

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1227_67039&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1227_67039&rgn=div8'
WHERE clause_id = 73130; -- 252.227-7039

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1228_67000&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1228_67000&rgn=div8'
WHERE clause_id = 73131; -- 252.228-7000

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1228_67001&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1228_67001&rgn=div8'
WHERE clause_id = 73132; -- 252.228-7001

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1228_67003&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1228_67003&rgn=div8'
WHERE clause_id = 73133; -- 252.228-7003

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1228_67004&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1228_67004&rgn=div8'
WHERE clause_id = 73134; -- 252.228-7004

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1228_67005&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1228_67005&rgn=div8'
WHERE clause_id = 73135; -- 252.228-7005

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?node=se48.3.252_1228_67006&amp;rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=28cf82b7494efcd9810df820f9359a44&amp;node=se48.3.252_1228_67006&amp;rgn=div8'
WHERE clause_id = 73136; -- 252.228-7006

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1229_67000&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1229_67000&rgn=div8'
WHERE clause_id = 73137; -- 252.229-7000

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1229_67001&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1229_67001&rgn=div8'
WHERE clause_id = 73139; -- 252.229-7001

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1229_67002&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1229_67002&rgn=div8'
WHERE clause_id = 73140; -- 252.229-7002

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1229_67003&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1229_67003&rgn=div8'
WHERE clause_id = 73141; -- 252.229-7003

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1229_67004&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1229_67004&rgn=div8'
WHERE clause_id = 73142; -- 252.229-7004

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1229_67005&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1229_67005&rgn=div8'
WHERE clause_id = 73143; -- 252.229-7005

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1229_67006&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1229_67006&rgn=div8'
WHERE clause_id = 73144; -- 252.229-7006

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1229_67007&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1229_67007&rgn=div8'
WHERE clause_id = 73145; -- 252.229-7007

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1229_67008&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1229_67008&rgn=div8'
WHERE clause_id = 73146; -- 252.229-7008

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1229_67009&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1229_67009&rgn=div8'
WHERE clause_id = 73147; -- 252.229-7009

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1229_67010&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1229_67010&rgn=div8'
WHERE clause_id = 73148; -- 252.229-7010

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1229_67011&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1229_67011&rgn=div8'
WHERE clause_id = 73149; -- 252.229-7011

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1229_67012&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1229_67012&rgn=div8'
WHERE clause_id = 73150; -- 252.229-7012

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1229_67013&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1229_67013&rgn=div8'
WHERE clause_id = 73151; -- 252.229-7013

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1231_67000&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1231_67000&rgn=div8'
WHERE clause_id = 73154; -- 252.231-7000

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1232_67000&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1232_67000&rgn=div8'
WHERE clause_id = 73155; -- 252.232-7000

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1232_67001&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1232_67001&rgn=div8'
WHERE clause_id = 73156; -- 252.232-7001

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1232_67002&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1232_67002&rgn=div8'
WHERE clause_id = 73157; -- 252.232-7002

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1232_67003&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1232_67003&rgn=div8'
WHERE clause_id = 73158; -- 252.232-7003

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1232_67004&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1232_67004&rgn=div8'
WHERE clause_id = 73159; -- 252.232-7004

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1232_67005&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1232_67005&rgn=div8'
WHERE clause_id = 73160; -- 252.232-7005

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1232_67006&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1232_67006&rgn=div8'
WHERE clause_id = 73161; -- 252.232-7006

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1232_67007&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1232_67007&rgn=div8'
WHERE clause_id = 73162; -- 252.232-7007

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1232_67008&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1232_67008&rgn=div8'
WHERE clause_id = 73163; -- 252.232-7008

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1232_67009&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1232_67009&rgn=div8'
WHERE clause_id = 73164; -- 252.232-7009

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1232_67010&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1232_67010&rgn=div8'
WHERE clause_id = 73165; -- 252.232-7010

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1232_67011&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1232_67011&rgn=div8'
WHERE clause_id = 73166; -- 252.232-7011

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1232_67012&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1232_67012&rgn=div8'
WHERE clause_id = 73167; -- 252.232-7012

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1232_67013&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1232_67013&rgn=div8'
WHERE clause_id = 73168; -- 252.232-7013

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1232_67014&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1232_67014&rgn=div8'
WHERE clause_id = 73169; -- 252.232-7014

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1233_67001&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1233_67001&rgn=div8'
WHERE clause_id = 73170; -- 252.233-7001

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1234_67001&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1234_67001&rgn=div8'
WHERE clause_id = 73171; -- 252.234-7001

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1234_67002&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1234_67002&rgn=div8'
WHERE clause_id = 73172; -- 252.234-7002

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1234_67003&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1234_67003&rgn=div8'
WHERE clause_id = 73174; -- 252.234-7003

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1234_67004&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1234_67004&rgn=div8'
WHERE clause_id = 73176; -- 252.234-7004

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1235_67000&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1235_67000&rgn=div8'
WHERE clause_id = 73177; -- 252.235-7000

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1235_67001&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1235_67001&rgn=div8'
WHERE clause_id = 73178; -- 252.235-7001

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1235_67002&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1235_67002&rgn=div8'
WHERE clause_id = 73179; -- 252.235-7002

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1235_67003&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1235_67003&rgn=div8'
WHERE clause_id = 73181; -- 252.235-7003

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1235_67004&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1235_67004&rgn=div8'
WHERE clause_id = 73182; -- 252.235-7004

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1235_67010&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1235_67010&rgn=div8'
WHERE clause_id = 73183; -- 252.235-7010

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1235_67011&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1235_67011&rgn=div8'
WHERE clause_id = 73184; -- 252.235-7011

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1236_67000&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1236_67000&rgn=div8'
WHERE clause_id = 73185; -- 252.236-7000

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1236_67001&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1236_67001&rgn=div8'
WHERE clause_id = 73186; -- 252.236-7001

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1236_67002&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1236_67002&rgn=div8'
WHERE clause_id = 73187; -- 252.236-7002

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1236_67003&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1236_67003&rgn=div8'
WHERE clause_id = 73188; -- 252.236-7003

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1236_67004&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1236_67004&rgn=div8'
WHERE clause_id = 73189; -- 252.236-7004

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1236_67005&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1236_67005&rgn=div8'
WHERE clause_id = 73190; -- 252.236-7005

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1236_67006&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1236_67006&rgn=div8'
WHERE clause_id = 73191; -- 252.236-7006

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1236_67007&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1236_67007&rgn=div8'
WHERE clause_id = 73192; -- 252.236-7007

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1236_67008&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1236_67008&rgn=div8'
WHERE clause_id = 73193; -- 252.236-7008

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1236_67009&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1236_67009&rgn=div8'
WHERE clause_id = 73194; -- 252.236-7009

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1236_67010&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1236_67010&rgn=div8'
WHERE clause_id = 73195; -- 252.236-7010

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1236_67011&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1236_67011&rgn=div8'
WHERE clause_id = 73196; -- 252.236-7011

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1236_67012&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1236_67012&rgn=div8'
WHERE clause_id = 73197; -- 252.236-7012

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1236_67013&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1236_67013&rgn=div8'
WHERE clause_id = 73198; -- 252.236-7013

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1237_67000&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1237_67000&rgn=div8'
WHERE clause_id = 73199; -- 252.237-7000

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1237_67001&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1237_67001&rgn=div8'
WHERE clause_id = 73200; -- 252.237-7001

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1237_67002&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1237_67002&rgn=div8'
WHERE clause_id = 73202; -- 252.237-7002

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1237_67003&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1237_67003&rgn=div8'
WHERE clause_id = 73203; -- 252.237-7003

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1237_67004&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1237_67004&rgn=div8'
WHERE clause_id = 73204; -- 252.237-7004

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1237_67005&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1237_67005&rgn=div8'
WHERE clause_id = 73205; -- 252.237-7005

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1237_67006&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1237_67006&rgn=div8'
WHERE clause_id = 73206; -- 252.237-7006

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1237_67007&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1237_67007&rgn=div8'
WHERE clause_id = 73207; -- 252.237-7007

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1237_67008&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1237_67008&rgn=div8'
WHERE clause_id = 73208; -- 252.237-7008

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1237_67009&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1237_67009&rgn=div8'
WHERE clause_id = 73209; -- 252.237-7009

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1237_67010&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1237_67010&rgn=div8'
WHERE clause_id = 73210; -- 252.237-7010

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1237_67011&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1237_67011&rgn=div8'
WHERE clause_id = 73211; -- 252.237-7011

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1237_67012&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1237_67012&rgn=div8'
WHERE clause_id = 73212; -- 252.237-7012

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1237_67013&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1237_67013&rgn=div8'
WHERE clause_id = 73213; -- 252.237-7013

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1237_67014&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1237_67014&rgn=div8'
WHERE clause_id = 73214; -- 252.237-7014

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1237_67015&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1237_67015&rgn=div8'
WHERE clause_id = 73215; -- 252.237-7015

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1237_67016&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1237_67016&rgn=div8'
WHERE clause_id = 73218; -- 252.237-7016

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1237_67017&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1237_67017&rgn=div8'
WHERE clause_id = 73219; -- 252.237-7017

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1237_67018&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1237_67018&rgn=div8'
WHERE clause_id = 73220; -- 252.237-7018

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1237_67019&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1237_67019&rgn=div8'
WHERE clause_id = 73221; -- 252.237-7019

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1237_67022&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1237_67022&rgn=div8'
WHERE clause_id = 73222; -- 252.237-7022

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1237_67023&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1237_67023&rgn=div8'
WHERE clause_id = 73223; -- 252.237-7023

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1237_67024&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1237_67024&rgn=div8'
WHERE clause_id = 73224; -- 252.237-7024

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1239_67000&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1239_67000&rgn=div8'
WHERE clause_id = 73225; -- 252.239-7000

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1239_67001&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1239_67001&rgn=div8'
WHERE clause_id = 73226; -- 252.239-7001

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1239_67002&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1239_67002&rgn=div8'
WHERE clause_id = 73227; -- 252.239-7002

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1239_67004&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1239_67004&rgn=div8'
WHERE clause_id = 73228; -- 252.239-7004

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1239_67005&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1239_67005&rgn=div8'
WHERE clause_id = 73229; -- 252.239-7005

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1239_67006&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1239_67006&rgn=div8'
WHERE clause_id = 73230; -- 252.239-7006

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1239_67007&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1239_67007&rgn=div8'
WHERE clause_id = 73231; -- 252.239-7007

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1239_67008&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1239_67008&rgn=div8'
WHERE clause_id = 73232; -- 252.239-7008

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1239_67011&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1239_67011&rgn=div8'
WHERE clause_id = 73233; -- 252.239-7011

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1239_67012&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1239_67012&rgn=div8'
WHERE clause_id = 73234; -- 252.239-7012

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1239_67013&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1239_67013&rgn=div8'
WHERE clause_id = 73235; -- 252.239-7013

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1239_67014&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1239_67014&rgn=div8'
WHERE clause_id = 73236; -- 252.239-7014

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1239_67015&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1239_67015&rgn=div8'
WHERE clause_id = 73237; -- 252.239-7015

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1239_67016&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1239_67016&rgn=div8'
WHERE clause_id = 73238; -- 252.239-7016

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1239_67017&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1239_67017&rgn=div8'
WHERE clause_id = 73239; -- 252.239-7017

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1239_67018&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1239_67018&rgn=div8'
WHERE clause_id = 73240; -- 252.239-7018

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1241_67000&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1241_67000&rgn=div8'
WHERE clause_id = 73242; -- 252.241-7000

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1241_67001&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1241_67001&rgn=div8'
WHERE clause_id = 73243; -- 252.241-7001

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1242_67004&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1242_67004&rgn=div8'
WHERE clause_id = 73244; -- 252.242-7004

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1242_67005&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1242_67005&rgn=div8'
WHERE clause_id = 73245; -- 252.242-7005

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1242_67006&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1242_67006&rgn=div8'
WHERE clause_id = 73246; -- 252.242-7006

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1243_67001&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1243_67001&rgn=div8'
WHERE clause_id = 73247; -- 252.243-7001

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1243_67002&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1243_67002&rgn=div8'
WHERE clause_id = 73248; -- 252.243-7002

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1244_67000&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1244_67000&rgn=div8'
WHERE clause_id = 73249; -- 252.244-7000

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1244_67001&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1244_67001&rgn=div8'
WHERE clause_id = 73251; -- 252.244-7001

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1245_67000&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1245_67000&rgn=div8'
WHERE clause_id = 73252; -- 252.245-7000

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1245_67001&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1245_67001&rgn=div8'
WHERE clause_id = 73253; -- 252.245-7001

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1245_67002&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1245_67002&rgn=div8'
WHERE clause_id = 73254; -- 252.245-7002

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1245_67003&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1245_67003&rgn=div8'
WHERE clause_id = 73255; -- 252.245-7003

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1245_67004&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1245_67004&rgn=div8'
WHERE clause_id = 73256; -- 252.245-7004

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1246_67000&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1246_67000&rgn=div8'
WHERE clause_id = 73257; -- 252.246-7000

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1246_67001&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1246_67001&rgn=div8'
WHERE clause_id = 73260; -- 252.246-7001

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1246_67002&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1246_67002&rgn=div8'
WHERE clause_id = 73261; -- 252.246-7002

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1246_67003&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1246_67003&rgn=div8'
WHERE clause_id = 73262; -- 252.246-7003

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1246_67004&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1246_67004&rgn=div8'
WHERE clause_id = 73263; -- 252.246-7004

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1246_67005&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1246_67005&rgn=div8'
WHERE clause_id = 73264; -- 252.246-7005

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1246_67006&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1246_67006&rgn=div8'
WHERE clause_id = 73265; -- 252.246-7006

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1246_67007&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1246_67007&rgn=div8'
WHERE clause_id = 73266; -- 252.246-7007

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1247_67000&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1247_67000&rgn=div8'
WHERE clause_id = 73267; -- 252.247-7000

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1247_67001&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1247_67001&rgn=div8'
WHERE clause_id = 73268; -- 252.247-7001

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1247_67002&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1247_67002&rgn=div8'
WHERE clause_id = 73269; -- 252.247-7002

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1247_67003&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1247_67003&rgn=div8'
WHERE clause_id = 73270; -- 252.247-7003

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1247_67004&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1247_67004&rgn=div8'
WHERE clause_id = 73271; -- 252.247-7004

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1247_67005&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1247_67005&rgn=div8'
WHERE clause_id = 73272; -- 252.247-7005

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1247_67006&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1247_67006&rgn=div8'
WHERE clause_id = 73273; -- 252.247-7006

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1247_67007&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1247_67007&rgn=div8'
WHERE clause_id = 73274; -- 252.247-7007

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1247_67008&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1247_67008&rgn=div8'
WHERE clause_id = 73276; -- 252.247-7008

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1247_67009&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1247_67009&rgn=div8'
WHERE clause_id = 73277; -- 252.247-7009

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1247_67010&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1247_67010&rgn=div8'
WHERE clause_id = 73278; -- 252.247-7010

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1247_67011&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1247_67011&rgn=div8'
WHERE clause_id = 73279; -- 252.247-7011

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1247_67012&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1247_67012&rgn=div8'
WHERE clause_id = 73280; -- 252.247-7012

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1247_67013&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1247_67013&rgn=div8'
WHERE clause_id = 73281; -- 252.247-7013

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1247_67014&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1247_67014&rgn=div8'
WHERE clause_id = 73282; -- 252.247-7014

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1247_67016&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1247_67016&rgn=div8'
WHERE clause_id = 73283; -- 252.247-7016

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1247_67017&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1247_67017&rgn=div8'
WHERE clause_id = 73284; -- 252.247-7017

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1247_67018&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1247_67018&rgn=div8'
WHERE clause_id = 73285; -- 252.247-7018

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1247_67019&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1247_67019&rgn=div8'
WHERE clause_id = 73286; -- 252.247-7019

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1247_67020&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1247_67020&rgn=div8'
WHERE clause_id = 73287; -- 252.247-7020

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1247_67021&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1247_67021&rgn=div8'
WHERE clause_id = 73288; -- 252.247-7021

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1247_67022&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1247_67022&rgn=div8'
WHERE clause_id = 73289; -- 252.247-7022

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1247_67023&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1247_67023&rgn=div8'
WHERE clause_id = 73292; -- 252.247-7023

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1247_67024&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1247_67024&rgn=div8'
WHERE clause_id = 73293; -- 252.247-7024

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1247_67025&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1247_67025&rgn=div8'
WHERE clause_id = 73294; -- 252.247-7025

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1247_67026&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1247_67026&rgn=div8'
WHERE clause_id = 73295; -- 252.247-7026

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1247_67027&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1247_67027&rgn=div8'
WHERE clause_id = 73296; -- 252.247-7027

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1247_67028&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1247_67028&rgn=div8'
WHERE clause_id = 73297; -- 252.247-7028

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1249_67000&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1249_67000&rgn=div8'
WHERE clause_id = 73298; -- 252.249-7000

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1249_67002&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1249_67002&rgn=div8'
WHERE clause_id = 73299; -- 252.249-7002

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1251_67000&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1251_67000&rgn=div8'
WHERE clause_id = 73300; -- 252.251-7000

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1251_67001&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1251_67001&rgn=div8'
WHERE clause_id = 73301; -- 252.251-7001

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1202_61&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1202_61&rgn=div8'
WHERE clause_id = 72095; -- 52.202-1

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1203_610&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1203_610&rgn=div8'
WHERE clause_id = 72096; -- 52.203-10

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1203_611&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1203_611&rgn=div8'
WHERE clause_id = 72097; -- 52.203-11

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1203_612&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1203_612&rgn=div8'
WHERE clause_id = 72098; -- 52.203-12

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1203_613&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1203_613&rgn=div8'
WHERE clause_id = 72099; -- 52.203-13

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1203_614&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1203_614&rgn=div8'
WHERE clause_id = 72100; -- 52.203-14

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1203_615&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1203_615&rgn=div8'
WHERE clause_id = 72101; -- 52.203-15

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1203_616&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1203_616&rgn=div8'
WHERE clause_id = 72102; -- 52.203-16

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1203_617&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1203_617&rgn=div8'
WHERE clause_id = 72103; -- 52.203-17

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1203_62&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1203_62&rgn=div8'
WHERE clause_id = 72104; -- 52.203-2

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1203_63&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1203_63&rgn=div8'
WHERE clause_id = 72105; -- 52.203-3

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1203_65&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1203_65&rgn=div8'
WHERE clause_id = 72106; -- 52.203-5

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1203_66&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1203_66&rgn=div8'
WHERE clause_id = 72108; -- 52.203-6

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1203_67&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1203_67&rgn=div8'
WHERE clause_id = 72109; -- 52.203-7

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1203_68&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1203_68&rgn=div8'
WHERE clause_id = 72110; -- 52.203-8

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1204_610&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1204_610&rgn=div8'
WHERE clause_id = 72111; -- 52.204-10

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1204_612&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1204_612&rgn=div8'
WHERE clause_id = 72112; -- 52.204-12

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1204_613&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1204_613&rgn=div8'
WHERE clause_id = 72113; -- 52.204-13

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1204_614&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1204_614&rgn=div8'
WHERE clause_id = 72114; -- 52.204-14

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1204_615&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1204_615&rgn=div8'
WHERE clause_id = 72115; -- 52.204-15

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1204_616&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1204_616&rgn=div8'
WHERE clause_id = 72116; -- 52.204-16

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1204_617&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1204_617&rgn=div8'
WHERE clause_id = 72117; -- 52.204-17

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1204_618&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1204_618&rgn=div8'
WHERE clause_id = 72118; -- 52.204-18

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1204_619&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1204_619&rgn=div8'
WHERE clause_id = 72119; -- 52.204-19

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1204_62&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1204_62&rgn=div8'
WHERE clause_id = 72122; -- 52.204-2

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1204_63&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1204_63&rgn=div8'
WHERE clause_id = 72123; -- 52.204-3

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1204_64&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1204_64&rgn=div8'
WHERE clause_id = 72124; -- 52.204-4

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1204_65&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1204_65&rgn=div8'
WHERE clause_id = 72125; -- 52.204-5

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1204_66&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1204_66&rgn=div8'
WHERE clause_id = 72126; -- 52.204-6

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1204_67&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1204_67&rgn=div8'
WHERE clause_id = 72128; -- 52.204-7

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?node=se48.2.52_1204_68&amp;rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=1e45c9c95cc0fa5020d0a83c2da53fb4&amp;node=se48.2.52_1204_68&amp;rgn=div8'
, clause_data = '<p>(a)(1) The North American Industry Classification System (NAICS) code for this acquisition is {{textbox}} [<i>insert NAICS code</i>].</p><p>(2) The small business size standard is {{textbox}} [<i>insert size standard</i>].</p><p>(3) The small business size standard for a concern which submits an offer in its own name, other than on a construction or service contract, but which proposes to furnish a product which it did not itself manufacture, is 500 employees.3</p><p>(b)(1) If the provision at 52.204-7, System for Award Management, is included in this solicitation, paragraph (d) of this provision applies.</p><p>(2) If the provision at 52.204-7 is not included in this solicitation, and the offeror is currently registered in the System for Award Management (SAM), and has completed the Representations and Certifications section of SAM electronically, the offeror may choose to use paragraph (d) of this provision instead of completing the corresponding individual representations and certifications in the solicitation. The offeror shall indicate which option applies by checking one of the following boxes:</p><p>{{checkbox_52.204-8[0]}}} (i) Paragraph (d) applies.</p><p>{{checkbox_52.204-8[1]}}} (ii) Paragraph (d) does not apply and the offeror has completed the individual representations and certifications in the solicitation.</p><p>(c)(1) The following representations or certifications in SAM are applicable to this solicitation as indicated:</p><p>(i) 52.203-2, Certificate of Independent Price Determination. This provision applies to solicitations when a firm-fixed-price contract or fixed-price contract with economic price adjustment is contemplated, unlessâ</p><p>(A) The acquisition is to be made under the simplified acquisition procedures in Part 13;</p><p>(B) The solicitation is a request for technical proposals under two-step sealed bidding procedures; or</p><p>(C) The solicitation is for utility services for which rates are set by law or regulation.</p><p>(ii) 52.203-11, Certification and Disclosure Regarding Payments to Influence Certain Federal Transactions. This provision applies to solicitations expected to exceed $150,000.</p><p>(iii) 52.204-3, Taxpayer Identification. This provision applies to solicitations that do not include provision at 52.204-7, System for Award Management.</p><p>(iv) 52.204-5, Women-Owned Business (Other Than Small Business). This provision applies to solicitations thatâ</p><p>(A) Are not set aside for small business concerns;</p><p>(B) Exceed the simplified acquisition threshold; and</p><p>(C) Are for contracts that will be performed in the United States or its outlying areas.</p><p>(v) 52.209-2, Prohibition on Contracting with Inverted Domestic CorporationsâRepresentation.</p><p>(vi) 52.209-5, Certification Regarding Responsibility Matters. This provision applies to solicitations where the contract value is expected to exceed the simplified acquisition threshold.</p><p>(vii) 52.214-14, Place of PerformanceâSealed Bidding. This provision applies to invitations for bids except those in which the place of performance is specified by the Government.</p><p>(viii) 52.215-6, Place of Performance. This provision applies to solicitations unless the place of performance is specified by the Government.</p><p>(ix) 52.219-1, Small Business Program Representations (Basic &amp; Alternate I). This provision applies to solicitations when the contract will be performed in the United States or its outlying areas.</p><p>(A) The basic provision applies when the solicitations are issued by other than DoD, NASA, and the Coast Guard.</p><p>(B) The provision with its Alternate I applies to solicitations issued by DoD, NASA, or the Coast Guard.</p><p>(x) 52.219-2, Equal Low Bids. This provision applies to solicitations when contracting by sealed bidding and the contract will be performed in the United States or its outlying areas.</p><p>(xi) 52.222-22, Previous Contracts and Compliance Reports. This provision applies to solicitations that include the clause at 52.222-26, Equal Opportunity.</p><p>(xii) 52.222-25, Affirmative Action Compliance. This provision applies to solicitations, other than those for construction, when the solicitation includes the clause at 52.222-26, Equal Opportunity.</p><p>(xiii) 52.222-38, Compliance with Veterans'' Employment Reporting Requirements. This provision applies to solicitations when it is anticipated the contract award will exceed the simplified acquisition threshold and the contract is not for acquisition of commercial items.</p><p>(xiv) 52.223-1, Biobased Product Certification. This provision applies to solicitations that require the delivery or specify the use of USDA-designated items; or include the clause at 52.223-2, Affirmative Procurement of Biobased Products Under Service and Construction Contracts.</p><p>(xv) 52.223-4, Recovered Material Certification. This provision applies to solicitations that are for, or specify the use of, EPA-designated items.</p><p>(xvi) 52.225-2, Buy American Certificate. This provision applies to solicitations containing the clause at 52.225-1.</p><p>(xvii) 52.225-4, Buy AmericanâFree Trade AgreementsâIsraeli Trade Act Certificate. (Basic, Alternates I, II, and III.) This provision applies to solicitations containing the clause at 52.225-3.</p><p>(A) If the acquisition value is less than $25,000, the basic provision applies.</p><p>(B) If the acquisition value is $25,000 or more but is less than $50,000, the provision with its Alternate I applies.</p><p>(C) If the acquisition value is $50,000 or more but is less than $79,507, the provision with its Alternate II applies.</p><p>(D) If the acquisition value is $79,507 or more but is less than $100,000, the provision with its Alternate III applies.</p><p>(xviii) 52.225-6, Trade Agreements Certificate. This provision applies to solicitations containing the clause at 52.225-5.</p><p>(xix) 52.225-20, Prohibition on Conducting Restricted Business Operations in SudanâCertification. This provision applies to all solicitations.</p><p>(xx) 52.225-25, Prohibition on Contracting with Entities Engaging in Certain Activities or Transactions Relating to IranâRepresentation and Certifications. This provision applies to all solicitations.</p><p>(xxi) 52.226-2, Historically Black College or University and Minority Institution Representation. This provision applies to solicitations for research, studies, supplies, or services of the type normally acquired from higher educational institutions.</p><p>(2) The following certifications are applicable as indicated by the Contracting Officer:</p>\n<p>[<i>Contracting Officer check as appropriate.</i>]</p><p>{{textbox}} (i) 52.204-17, Ownership or Control of Offeror.</p><p>{{textbox}} (ii) 52.222-18, Certification Regarding Knowledge of Child Labor for Listed End Products.</p><p>{{textbox}} (iii) 52.222-48, Exemption from Application of the Service Contract Labor Standards to Contracts for Maintenance, Calibration, or Repair of Certain EquipmentâCertification.</p><p>{{textbox}} (iv) 52.222-52, Exemption from Application of the Service Contract Labor Standards to Contracts for Certain ServicesâCertification.</p><p>{{textbox}} (v) 52.223-9, with its Alternate I, Estimate of Percentage of Recovered Material Content for EPA-Designated Products (Alternate I only).</p><p>{{textbox}} (vi) 52.227-6, Royalty Information.</p><p>{{textbox}} (A) Basic.</p><p>{{textbox}} (B) Alternate I.</p><p>{{textbox}} (vii) 52.227-15, Representation of Limited Rights Data and Restricted Computer Software.</p><p>(d) The offeror has completed the annual representations and certifications electronically via the SAM Web site accessed through <i>https://www.acquisition.gov.</i> After reviewing the SAM database information, the offeror verifies by submission of the offer that the representations and certifications currently posted electronically that apply to this solicitation as indicated in paragraph (c) of this provision have been entered or updated within the last 12 months, are current, accurate, complete, and applicable to this solicitation (including the business size standard applicable to the NAICS code referenced for this solicitation), as of the date of this offer and are incorporated in this offer by reference (see FAR 4.1201); except for the changes identified below [<i>offeror to insert changes, identifying change by clause number, title, date</i>]. These amended representation(s) and/or certification(s) are also incorporated in this offer and are current, accurate, and complete as of the date of this offer.</p>\n<div><div><table border="&quot;1&quot;" cellpadding="&quot;1&quot;" cellspacing="&quot;1&quot;" frame="&quot;void&quot;" width="&quot;100%&quot;"><tbody><tr><th scope="&quot;col&quot;">FAR Clause No.</th><th scope="&quot;col&quot;">Title</th><th scope="&quot;col&quot;">Date</th><th scope="&quot;col&quot;">Change</th></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">&nbsp;&nbsp;&nbsp;</td><td align="&quot;left&quot;"></td><td align="&quot;left&quot;"></td><td align="&quot;left&quot;"></td></tr></tbody></table></div></div>\n<p>Any changes provided by the offeror are applicable to this solicitation only, and do not result in an update to the representations and certifications posted on SAM.</p>'
--               '<p>(a)(1) The North American Industry Classification System (NAICS) code for this acquisition is {{textbox}} [<i>insert NAICS code</i>].</p><p>(2) The small business size standard is {{textbox}} [<i>insert size standard</i>].</p><p>(3) The small business size standard for a concern which submits an offer in its own name, other than on a construction or service contract, but which proposes to furnish a product which it did not itself manufacture, is 500 employees.3</p><p>(b)(1) If the provision at 52.204-7, System for Award Management, is included in this solicitation, paragraph (d) of this provision applies.</p><p>(2) If the provision at 52.204-7 is not included in this solicitation, and the offeror is currently registered in the System for Award Management (SAM), and has completed the Representations and Certifications section of SAM electronically, the offeror may choose to use paragraph (d) of this provision instead of completing the corresponding individual representations and certifications in the solicitation. The offeror shall indicate which option applies by checking one of the following boxes:</p><p>{{checkbox_52.204-8[0]}}} (i) Paragraph (d) applies.</p><p>{{checkbox_52.204-8[1]}}} (ii) Paragraph (d) does not apply and the offeror has completed the individual representations and certifications in the solicitation.</p><p>(c)(1) The following representations or certifications in SAM are applicable to this solicitation as indicated:</p><p>(i) 52.203-2, Certificate of Independent Price Determination. This provision applies to solicitations when a firm-fixed-price contract or fixed-price contract with economic price adjustment is contemplated, unlessâ</p><p>(A) The acquisition is to be made under the simplified acquisition procedures in Part 13;</p><p>(B) The solicitation is a request for technical proposals under two-step sealed bidding procedures; or</p><p>(C) The solicitation is for utility services for which rates are set by law or regulation.</p><p>(ii) 52.203-11, Certification and Disclosure Regarding Payments to Influence Certain Federal Transactions. This provision applies to solicitations expected to exceed $150,000.</p><p>(iii) 52.204-3, Taxpayer Identification. This provision applies to solicitations that do not include provision at 52.204-7, System for Award Management.</p><p>(iv) 52.204-5, Women-Owned Business (Other Than Small Business). This provision applies to solicitations thatâ</p><p>(A) Are not set aside for small business concerns;</p><p>(B) Exceed the simplified acquisition threshold; and</p><p>(C) Are for contracts that will be performed in the United States or its outlying areas.</p><p>(v) 52.209-2, Prohibition on Contracting with Inverted Domestic CorporationsâRepresentation.</p><p>(vi) 52.209-5, Certification Regarding Responsibility Matters. This provision applies to solicitations where the contract value is expected to exceed the simplified acquisition threshold.</p><p>(vii) 52.214-14, Place of PerformanceâSealed Bidding. This provision applies to invitations for bids except those in which the place of performance is specified by the Government.</p><p>(viii) 52.215-6, Place of Performance. This provision applies to solicitations unless the place of performance is specified by the Government.</p><p>(ix) 52.219-1, Small Business Program Representations (Basic &amp; Alternate I). This provision applies to solicitations when the contract will be performed in the United States or its outlying areas.</p><p>(A) The basic provision applies when the solicitations are issued by other than DoD, NASA, and the Coast Guard.</p><p>(B) The provision with its Alternate I applies to solicitations issued by DoD, NASA, or the Coast Guard.</p><p>(x) 52.219-2, Equal Low Bids. This provision applies to solicitations when contracting by sealed bidding and the contract will be performed in the United States or its outlying areas.</p><p>(xi) 52.222-22, Previous Contracts and Compliance Reports. This provision applies to solicitations that include the clause at 52.222-26, Equal Opportunity.</p><p>(xii) 52.222-25, Affirmative Action Compliance. This provision applies to solicitations, other than those for construction, when the solicitation includes the clause at 52.222-26, Equal Opportunity.</p><p>(xiii) 52.222-38, Compliance with Veterans'' Employment Reporting Requirements. This provision applies to solicitations when it is anticipated the contract award will exceed the simplified acquisition threshold and the contract is not for acquisition of commercial items.</p><p>(xiv) 52.223-1, Biobased Product Certification. This provision applies to solicitations that require the delivery or specify the use of USDA-designated items; or include the clause at 52.223-2, Affirmative Procurement of Biobased Products Under Service and Construction Contracts.</p><p>(xv) 52.223-4, Recovered Material Certification. This provision applies to solicitations that are for, or specify the use of, EPA-designated items.</p><p>(xvi) 52.225-2, Buy American Certificate. This provision applies to solicitations containing the clause at 52.225-1.</p><p>(xvii) 52.225-4, Buy AmericanâFree Trade AgreementsâIsraeli Trade Act Certificate. (Basic, Alternates I, II, and III.) This provision applies to solicitations containing the clause at 52.225-3.</p><p>(A) If the acquisition value is less than $25,000, the basic provision applies.</p><p>(B) If the acquisition value is $25,000 or more but is less than $50,000, the provision with its Alternate I applies.</p><p>(C) If the acquisition value is $50,000 or more but is less than $79,507, the provision with its Alternate II applies.</p><p>(D) If the acquisition value is $79,507 or more but is less than $100,000, the provision with its Alternate III applies.</p><p>(xviii) 52.225-6, Trade Agreements Certificate. This provision applies to solicitations containing the clause at 52.225-5.</p><p>(xix) 52.225-20, Prohibition on Conducting Restricted Business Operations in SudanâCertification. This provision applies to all solicitations.</p><p>(xx) 52.225-25, Prohibition on Contracting with Entities Engaging in Certain Activities or Transactions Relating to IranâRepresentation and Certifications. This provision applies to all solicitations.</p><p>(xxi) 52.226-2, Historically Black College or University and Minority Institution Representation. This provision applies to solicitations for research, studies, supplies, or services of the type normally acquired from higher educational institutions.</p><p>(2) The following certifications are applicable as indicated by the Contracting Officer:</p>\n<p>[<i>Contracting Officer check as appropriate.</i>]</p><p>{{textbox}} (i) 52.204-17, Ownership or Control of Offeror.</p><p>{{textbox}} (ii) 52.222-18, Certification Regarding Knowledge of Child Labor for Listed End Products.</p><p>{{textbox}} (iii) 52.222-48, Exemption from Application of the Service Contract Labor Standards to Contracts for Maintenance, Calibration, or Repair of Certain EquipmentâCertification.</p><p>{{textbox}} (iv) 52.222-52, Exemption from Application of the Service Contract Labor Standards to Contracts for Certain ServicesâCertification.</p><p>{{textbox}} (v) 52.223-9, with its Alternate I, Estimate of Percentage of Recovered Material Content for EPA-Designated Products (Alternate I only).</p><p>{{textbox}} (vi) 52.227-6, Royalty Information.</p><p>{{textbox}} (A) Basic.</p><p>{{textbox}} (B) Alternate I.</p><p>{{textbox}} (vii) 52.227-15, Representation of Limited Rights Data and Restricted Computer Software.</p><p>(d) The offeror has completed the annual representations and certifications electronically via the SAM Web site accessed through <i>https://www.acquisition.gov.</i> After reviewing the SAM database information, the offeror verifies by submission of the offer that the representations and certifications currently posted electronically that apply to this solicitation as indicated in paragraph (c) of this provision have been entered or updated within the last 12 months, are current, accurate, complete, and applicable to this solicitation (including the business size standard applicable to the NAICS code referenced for this solicitation), as of the date of this offer and are incorporated in this offer by reference (see FAR 4.1201); except for the changes identified below [<i>offeror to insert changes, identifying change by clause number, title, date</i>]. These amended representation(s) and/or certification(s) are also incorporated in this offer and are current, accurate, and complete as of the date of this offer.</p>\n<div><div><table border="&quot;1&quot;" cellpadding="&quot;1&quot;" cellspacing="&quot;1&quot;" frame="&quot;void&quot;" width="&quot;100%&quot;"><tbody><tr><th scope="&quot;col&quot;">FAR Clause No.</th><th scope="&quot;col&quot;">Title</th><th scope="&quot;col&quot;">Date</th><th scope="&quot;col&quot;">Change</th></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">Â&nbsp;Â&nbsp;Â&nbsp;</td><td align="&quot;left&quot;"></td><td align="&quot;left&quot;"></td><td align="&quot;left&quot;"></td></tr></tbody></table></div></div>\n<p>Any changes provided by the offeror are applicable to this solicitation only, and do not result in an update to the representations and certifications posted on SAM.</p>'
WHERE clause_id = 72129; -- 52.204-8

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1204_69&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1204_69&rgn=div8'
WHERE clause_id = 72130; -- 52.204-9

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1207_61&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1207_61&rgn=div8'
WHERE clause_id = 72131; -- 52.207-1

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1207_62&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1207_62&rgn=div8'
WHERE clause_id = 72132; -- 52.207-2

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1207_63&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1207_63&rgn=div8'
WHERE clause_id = 72133; -- 52.207-3

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?node=se48.2.52_1207_64&amp;rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=1e45c9c95cc0fa5020d0a83c2da53fb4&amp;node=se48.2.52_1207_64&amp;rgn=div8'
, clause_data = '<p>(a) Offerors are invited to state an opinion on whether the quantity(ies) of supplies on which bids, proposals or quotes are requested in this solicitation is (are) economically advantageous to the Government.</p>&nbsp; {{textbox_52.207-4[0]}}&nbsp; {{textbox_52.207-4[1]}}&nbsp; {{textbox_52.207-4[2]}}<p>(b) Each offeror who believes that acquisitions in different quantities would be more advantageous is invited to recommend an economic purchase quantity. If different quantities are recommended, a total and a unit price must be quoted for applicable items. An economic purchase quantity is that quantity at which a significant price break occurs. If there are significant price breaks at different quantity points, this information is desired as well.</p>\n<div><div><p>Offeror Recommendations</p></div><div><table border="&quot;1&quot;" cellpadding="&quot;1&quot;" cellspacing="&quot;1&quot;" frame="&quot;void&quot;" width="&quot;100%&quot;"><tbody><tr><th scope="&quot;col&quot;">Item</th><th scope="&quot;col&quot;">Quantity</th><th scope="&quot;col&quot;">Price quotation</th><th scope="&quot;col&quot;">Total</th></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;"></td><td align="&quot;right&quot;"></td><td align="&quot;right&quot;"></td><td align="&quot;right&quot;"></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;"></td><td align="&quot;right&quot;"></td><td align="&quot;right&quot;"></td><td align="&quot;right&quot;"></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;"></td><td align="&quot;right&quot;"></td><td align="&quot;right&quot;"></td><td align="&quot;right&quot;"></td></tr></tbody></table></div></div><p>(c) The information requested in this provision is being solicited to avoid acquisitions in disadvantageous quantities and to assist the Government in developing a data base for future acquisitions of these items. However, the Government reserves the right to amend or cancel the solicitation and resolicit with respect to any individual item in the event quotations received and the Government''s requirements indicate that different quantities should be acquired.</p>'
--               '<p>(a) Offerors are invited to state an opinion on whether the quantity(ies) of supplies on which bids, proposals or quotes are requested in this solicitation is (are) economically advantageous to the Government.</p>Â&nbsp; {{textbox_52.207-4[0]}}Â&nbsp; {{textbox_52.207-4[1]}}Â&nbsp; {{textbox_52.207-4[2]}}<p>(b) Each offeror who believes that acquisitions in different quantities would be more advantageous is invited to recommend an economic purchase quantity. If different quantities are recommended, a total and a unit price must be quoted for applicable items. An economic purchase quantity is that quantity at which a significant price break occurs. If there are significant price breaks at different quantity points, this information is desired as well.</p>\n<div><div><p>Offeror Recommendations</p></div><div><table border="&quot;1&quot;" cellpadding="&quot;1&quot;" cellspacing="&quot;1&quot;" frame="&quot;void&quot;" width="&quot;100%&quot;"><tbody><tr><th scope="&quot;col&quot;">Item</th><th scope="&quot;col&quot;">Quantity</th><th scope="&quot;col&quot;">Price quotation</th><th scope="&quot;col&quot;">Total</th></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;"></td><td align="&quot;right&quot;"></td><td align="&quot;right&quot;"></td><td align="&quot;right&quot;"></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;"></td><td align="&quot;right&quot;"></td><td align="&quot;right&quot;"></td><td align="&quot;right&quot;"></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;"></td><td align="&quot;right&quot;"></td><td align="&quot;right&quot;"></td><td align="&quot;right&quot;"></td></tr></tbody></table></div></div><p>(c) The information requested in this provision is being solicited to avoid acquisitions in disadvantageous quantities and to assist the Government in developing a data base for future acquisitions of these items. However, the Government reserves the right to amend or cancel the solicitation and resolicit with respect to any individual item in the event quotations received and the Government''s requirements indicate that different quantities should be acquired.</p>'
WHERE clause_id = 72134; -- 52.207-4

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1207_65&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1207_65&rgn=div8'
WHERE clause_id = 72135; -- 52.207-5

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1208_64&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1208_64&rgn=div8'
WHERE clause_id = 72136; -- 52.208-4

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1208_65&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1208_65&rgn=div8'
WHERE clause_id = 72137; -- 52.208-5

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1208_66&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1208_66&rgn=div8'
WHERE clause_id = 72138; -- 52.208-6

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1208_67&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1208_67&rgn=div8'
WHERE clause_id = 72139; -- 52.208-7

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1208_68&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1208_68&rgn=div8'
WHERE clause_id = 72140; -- 52.208-8

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1208_69&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1208_69&rgn=div8'
WHERE clause_id = 72141; -- 52.208-9

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1209_61&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1209_61&rgn=div8'
WHERE clause_id = 72142; -- 52.209-1

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1209_610&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1209_610&rgn=div8'
WHERE clause_id = 72143; -- 52.209-10

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1209_62&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1209_62&rgn=div8'
WHERE clause_id = 72144; -- 52.209-2

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1209_63&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1209_63&rgn=div8'
WHERE clause_id = 72147; -- 52.209-3

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1209_64&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1209_64&rgn=div8'
WHERE clause_id = 72150; -- 52.209-4

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1209_65&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1209_65&rgn=div8'
WHERE clause_id = 72151; -- 52.209-5

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1209_66&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1209_66&rgn=div8'
WHERE clause_id = 72152; -- 52.209-6

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1209_67&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1209_67&rgn=div8'
WHERE clause_id = 72153; -- 52.209-7

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1209_69&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1209_69&rgn=div8'
WHERE clause_id = 72154; -- 52.209-9

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1210_61&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1210_61&rgn=div8'
WHERE clause_id = 72155; -- 52.210-1

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1211_61&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1211_61&rgn=div8'
WHERE clause_id = 72156; -- 52.211-1

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1211_610&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1211_610&rgn=div8'
WHERE clause_id = 72158; -- 52.211-10

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1211_611&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1211_611&rgn=div8'
WHERE clause_id = 72159; -- 52.211-11

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1211_612&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1211_612&rgn=div8'
WHERE clause_id = 72160; -- 52.211-12

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1211_613&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1211_613&rgn=div8'
WHERE clause_id = 72161; -- 52.211-13

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1211_614&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1211_614&rgn=div8'
WHERE clause_id = 72162; -- 52.211-14

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1211_615&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1211_615&rgn=div8'
WHERE clause_id = 72163; -- 52.211-15

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1211_616&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1211_616&rgn=div8'
WHERE clause_id = 72164; -- 52.211-16

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1211_617&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1211_617&rgn=div8'
WHERE clause_id = 72165; -- 52.211-17

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1211_618&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1211_618&rgn=div8'
WHERE clause_id = 72166; -- 52.211-18

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1211_62&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1211_62&rgn=div8'
WHERE clause_id = 72167; -- 52.211-2

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1211_63&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1211_63&rgn=div8'
WHERE clause_id = 72168; -- 52.211-3

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1211_64&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1211_64&rgn=div8'
WHERE clause_id = 72169; -- 52.211-4

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1211_65&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1211_65&rgn=div8'
WHERE clause_id = 72170; -- 52.211-5

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1211_66&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1211_66&rgn=div8'
WHERE clause_id = 72171; -- 52.211-6

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1211_67&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1211_67&rgn=div8'
WHERE clause_id = 72172; -- 52.211-7

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?node=se48.2.52_1211_68&amp;rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=1e45c9c95cc0fa5020d0a83c2da53fb4&amp;node=se48.2.52_1211_68&amp;rgn=div8'
WHERE clause_id = 72176; -- 52.211-8

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?node=se48.2.52_1211_69&amp;rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=1e45c9c95cc0fa5020d0a83c2da53fb4&amp;node=se48.2.52_1211_69&amp;rgn=div8'
WHERE clause_id = 72180; -- 52.211-9

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1212_61&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1212_61&rgn=div8'
WHERE clause_id = 72181; -- 52.212-1

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1212_62&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1212_62&rgn=div8'
WHERE clause_id = 72182; -- 52.212-2

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?node=se48.2.52_1212_63&amp;rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=1e45c9c95cc0fa5020d0a83c2da53fb4&amp;node=se48.2.52_1212_63&amp;rgn=div8'
, clause_data = '<p>The Offeror shall complete only paragraph (b) of this provision if the Offeror has completed the annual representations and certification electronically via the System for Award Management (SAM) Web site accessed through <i>http://www.acquisition.gov.</i> If the Offeror has not completed the annual representations and certifications electronically, the Offeror shall complete only paragraphs (c) through (p) of this provision.</p><p>(a) <i>Definitions.</i> As used in this provision&mdash;</p><p><i>Economically disadvantaged women-owned small business (EDWOSB) concern</i> means a small business concern that is at least 51 percent directly and unconditionally owned by, and the management and daily business operations of which are controlled by, one or more women who are citizens of the United States and who are economically disadvantaged in accordance with 13 CFR part 127. It automatically qualifies as a women-owned small business eligible under the WOSB Program.</p><p><i>Forced or indentured child labor</i> means all work or service&mdash; </p><p>(1) Exacted from any person under the age of 18 under the menace of any penalty for its nonperformance and for which the worker does not offer himself voluntarily; or </p><p>(2) Performed by any person under the age of 18 pursuant to a contract the enforcement of which can be accomplished by process or penalties.</p><p><i>Highest-level owner</i> means the entity that owns or controls an immediate owner of the offeror, or that owns or controls one or more entities that control an immediate owner of the offeror. No entity owns or exercises control of the highest level owner.</p><p><i>Immediate owner</i> means an entity, other than the offeror, that has direct control of the offeror. Indicators of control include, but are not limited to, one or more of the following: Ownership or interlocking management, identity of interests among family members, shared facilities and equipment, and the common use of employees.</p><p><i>Inverted domestic corporation</i> means a foreign incorporated entity that meets the definition of an inverted domestic corporation under 6 U.S.C. 395(b), applied in accordance with the rules and definitions of 6 U.S.C. 395(c).</p><p><i>Manufactured end product</i> means any end product in Federal Supply Classes (FSC) 1000-9999, except&mdash;</p><p>(1) FSC 5510, Lumber and Related Basic Wood Materials;</p><p>(2) Federal Supply Group (FSG) 87, Agricultural Supplies;</p><p>(3) FSG 88, Live Animals;</p><p>(4) FSG 89, Food and Related Consumables;</p><p>(5) FSC 9410, Crude Grades of Plant Materials;</p><p>(6) FSC 9430, Miscellaneous Crude Animal Products, Inedible;</p><p>(7) FSC 9440, Miscellaneous Crude Agricultural and Forestry Products;</p><p>(8) FSC 9610, Ores;</p><p>(9) FSC 9620, Minerals, Natural and Synthetic; and</p><p>(10) FSC 9630, Additive Metal Materials.</p><p><i>Manufactured end product</i> means any end product in product and service codes (PSCs) 1000-9999, except&mdash;</p><p>(1) PSC 5510, Lumber and Related Basic Wood Materials;</p><p>(2) Product or Service Group (PSG) 87, Agricultural Supplies;</p><p>(3) PSG 88, Live Animals;</p><p>(4) PSG 89, Subsistence;</p><p>(5) PSC 9410, Crude Grades of Plant Materials;</p><p>(6) PSC 9430, Miscellaneous Crude Animal Products, Inedible;</p><p>(7) PSC 9440, Miscellaneous Crude Agricultural and Forestry Products;</p><p>(8) PSC 9610, Ores;</p><p>(9) PSC 9620, Minerals, Natural and Synthetic; and</p><p>(10) PSC 9630, Additive Metal Materials.</p><p><i>Place of manufacture</i> means the place where an end product is assembled out of components, or otherwise made or processed from raw materials into the finished product that is to be provided to the Government. If a product is disassembled and reassembled, the place of reassembly is not the place of manufacture.</p><p><i>Restricted business operations</i> means business operations in Sudan that include power production activities, mineral extraction activities, oil-related activities, or the production of military equipment, as those terms are defined in the Sudan Accountability and Divestment Act of 2007 (Pub. L. 110-174). Restricted business operations do not include business operations that the person (as that term is defined in Section 2 of the Sudan Accountability and Divestment Act of 2007) conducting the business can demonstrate&mdash;</p><p>(1) Are conducted under contract directly and exclusively with the regional government of southern Sudan;</p><p>(2) Are conducted pursuant to specific authorization from the Office of Foreign Assets Control in the Department of the Treasury, or are expressly exempted under Federal law from the requirement to be conducted under such authorization;</p><p>(3) Consist of providing goods or services to marginalized populations of Sudan;</p><p>(4) Consist of providing goods or services to an internationally recognized peacekeeping force or humanitarian organization;</p><p>(5) Consist of providing goods or services that are used only to promote health or education; or</p><p>(6) Have been voluntarily suspended.</p><p><i>Sensitive technology</i>&mdash;</p><p>(1) Means hardware, software, telecommunications equipment, or any other technology that is to be used specifically&mdash;</p><p>(i) To restrict the free flow of unbiased information in Iran; or</p><p>(ii) To disrupt, monitor, or otherwise restrict speech of the people of Iran; and</p><p>(2) Does not include information or informational materials the export of which the President does not have the authority to regulate or prohibit pursuant to section 203(b)(3) of the International Emergency Economic Powers Act (50 U.S.C. 1702(b)(3)).</p><p><i>Service-disabled veteran-owned small business concern</i>&mdash; </p><p>(1) Means a small business concern&mdash; </p><p>(i) Not less than 51 percent of which is owned by one or more service&mdash;disabled veterans or, in the case of any publicly owned business, not less than 51 percent of the stock of which is owned by one or more service-disabled veterans; and </p><p>(ii) The management and daily business operations of which are controlled by one or more service-disabled veterans or, in the case of a service-disabled veteran with permanent and severe disability, the spouse or permanent caregiver of such veteran. </p><p>(2) <i>Service-disabled veteran</i> means a veteran, as defined in 38 U.S.C. 101(2), with a disability that is service-connected, as defined in 38 U.S.C. 101(16).</p><p><i>Small business concern</i> means a concern, including its affiliates, that is independently owned and operated, not dominant in the field of operation in which it is bidding on Government contracts, and qualified as a small business under the criteria in 13 CFR Part 121 and size standards in this solicitation.</p><p><i>Small disadvantaged business concern,</i> consistent with 13 CFR 124.1002, means a small business concern under the size standard applicable to the acquisition, that&mdash;</p><p>(1) Is at least 51 percent unconditionally and directly owned (as defined at 13 CFR 124.105) by&mdash;</p><p>(i) One or more socially disadvantaged (as defined at 13 CFR 124.103) and economically disadvantaged (as defined at 13 CFR 124.104) individuals who are citizens of the United States; and</p><p>(ii) Each individual claiming economic disadvantage has a net worth not exceeding $750,000 after taking into account the applicable exclusions set forth at 13 CFR 124.104(c)(2); and</p><p>(2) The management and daily business operations of which are controlled (as defined at 13.CFR 124.106) by individuals, who meet the criteria in paragraphs (1)(i) and (ii) of this definition.</p><p><i>Subsidiary</i> means an entity in which more than 50 percent of the entity is owned&mdash;</p><p>(1) Directly by a parent corporation; or</p><p>(2) Through another subsidiary of a parent corporation.</p><p><i>Veteran-owned small business concern</i> means a small business concern&mdash; </p><p>(1) Not less than 51 percent of which is owned by one or more veterans (as defined at 38 U.S.C. 101(2)) or, in the case of any publicly owned business, not less than 51 percent of the stock of which is owned by one or more veterans; and</p><p>(2) The management and daily business operations of which are controlled by one or more veterans.</p><p><i>Women-owned business concern</i> means a concern which is at least 51 percent owned by one or more women; or in the case of any publicly owned business, at least 51 percent of its stock is owned by one or more women; and whose management and daily business operations are controlled by one or more women.</p><p><i>Women-owned small business concern</i> means a small business concern&mdash;</p><p>(1) That is at least 51 percent owned by one or more women; or, in the case of any publicly owned business, at least 51 percent of the stock of which is owned by one or more women; and</p><p>(2) Whose management and daily business operations are controlled by one or more women.</p><p><i>Women-owned small business (WOSB) concern eligible under the WOSB Program</i> (in accordance with 13 CFR part 127), means a small business concern that is at least 51 percent directly and unconditionally owned by, and the management and daily business operations of which are controlled by, one or more women who are citizens of the United States.</p><p>(b)(1) <i>Annual Representations and Certifications.</i> Any changes provided by the offeror in paragraph (b)(2) of this provision do not automatically change the representations and certifications posted on the SAM website.</p><p>(2) The offeror has completed the annual representations and certifications electronically via the SAM website accessed through <i>http://www.acquisition.gov.</i> After reviewing the SAM database information, the offeror verifies by submission of this offer that the representations and certifications currently posted electronically at FAR 52.212-3, Offeror Representations and Certifications&mdash;Commercial Items, have been entered or updated in the last 12 months, are current, accurate, complete, and applicable to this solicitation (including the business size standard applicable to the NAICS code referenced for this solicitation), as of the date of this offer and are incorporated in this offer by reference (see FAR 4.1201), except for paragraphs {{textbox}}.</p><p>[<i>Offeror to identify the applicable paragraphs at (c) through (p) of this provision that the offeror has completed for the purposes of this solicitation only, if any.</i></p><p><i>These amended representation(s) and/or certification(s) are also incorporated in this offer and are current, accurate, and complete as of the date of this offer.</i></p><p><i>Any changes provided by the offeror are applicable to this solicitation only, and do not result in an update to the representations and certifications posted electronically on SAM.</i>]</p><p>(c) Offerors must complete the following representations when the resulting contract will be performed in the United States or its outlying areas. Check all that apply.</p><p>(1) <i>Small business concern.</i> The offeror represents as part of its offer that it {{checkbox_52.212-3[0]}}} is, {{checkbox_52.212-3[1]}}}&nbsp;&nbsp;is not a small business concern.</p><p>(2) <i>Veteran-owned small business concern.</i> [<i>Complete only if the offeror represented itself as a small business concern in paragraph (c)(1) of this provision.</i>] The offeror represents as part of its offer that it {{checkbox_52.212-3[2]}}} is, {{checkbox_52.212-3[3]}}} is not a veteran-owned small business concern. </p><p>(3) <i>Service-disabled veteran-owned small business concern.</i> [<i>Complete only if the offeror represented itself as a veteran-owned small business concern in paragraph (c)(2) of this provision.</i>] The offeror represents as part of its offer that it {{checkbox_52.212-3[4]}}} is, {{checkbox_52.212-3[5]}}} is not a service-disabled veteran-owned small business concern.</p><p>(4) <i>Small disadvantaged business concern. [Complete only if the offeror represented itself as a small business concern in paragraph (c)(1) of this provision.]</i> The offeror represents  that it {{checkbox_52.212-3[6]}}} is, {{checkbox_52.212-3[7]}}} is not a small disadvantaged business concern as defined in 13 CFR 124.1002.</p><p>(5) <i>Women-owned small business concern. [Complete only if the offeror represented itself as a small business concern in paragraph (c)(1) of this provision.]</i> The offeror represents that it {{checkbox_52.212-3[8]}}} is, {{checkbox_52.212-3[9]}}} is not a women-owned small business concern.</p><p>(6) WOSB concern eligible under the WOSB Program. [<i>Complete only if the offeror represented itself as a women-owned small business concern in paragraph (c)(5) of this provision.</i>] The offeror represents that&mdash;</p><p>(i) It {{checkbox_52.212-3[10]}}} is, {{checkbox_52.212-3[11]}}} is not a WOSB concern eligible under the WOSB Program, has provided all the required documents to the WOSB Repository, and no change in circumstances or adverse decisions have been issued that affects its eligibility; and</p><p>(ii) It {{checkbox_52.212-3[12]}}} is, {{checkbox_52.212-3[13]}}} is not a joint venture that complies with the requirements of 13 CFR part 127, and the representation in paragraph (c)(6)(i) of this provision is accurate for each WOSB concern eligible under the WOSB Program participating in the joint venture. [<i>The offeror shall enter the name or names of the WOSB concern eligible under the WOSB Program and other small businesses that are participating in the joint venture:</i> {{textbox}}.] Each WOSB concern eligible under the WOSB Program participating in the joint venture shall submit a separate signed copy of the WOSB representation.</p><p>(7) Economically disadvantaged women-owned small business (EDWOSB) concern. [<i>Complete only if the offeror represented itself as a WOSB concern eligible under the WOSB Program in (c)(6) of this provision.</i>] The offeror represents that&mdash;</p><p>(i) It {{checkbox_52.212-3[14]}}} is, {{checkbox_52.212-3[15]}}} is not an EDWOSB concern, has provided all the required documents to the WOSB Repository, and no change in circumstances or adverse decisions have been issued that affects its eligibility; and</p><p>(ii) It {{checkbox_52.212-3[16]}}} is, {{checkbox_52.212-3[17]}}} is not a joint venture that complies with the requirements of 13 CFR part 127, and the representation in paragraph (c)(7)(i) of this provision is accurate for each EDWOSB concern participating in the joint venture. [<i>The offeror shall enter the name or names of the EDWOSB concern and other small businesses that are participating in the joint venture:</i> {{textbox}}.] Each EDWOSB concern participating in the joint venture shall submit a separate signed copy of the EDWOSB representation.</p>\n<p>Note to paragraphs (c)(8) and (9): Complete paragraphs (c)(8) and (9) only if this solicitation is expected to exceed the simplified acquisition threshold.</p> <p>(8) <i>Women-owned business concern (other than small business concern). [Complete only if the offeror is a women-owned business concern and did not represent itself as a small business concern in paragraph (c)(1) of this provision.]</i> The offeror represents that it {{checkbox_52.212-3[18]}}} is, a women-owned business concern.</p><p>(9) <i>Tie bid priority for labor surplus area concerns.</i> If this is an invitation for bid, small business offerors may identify the labor surplus areas in which costs to be incurred on account of manufacturing or production (by offeror or first-tier subcontractors) amount to more than 50 percent of the contract price:</p>&nbsp; {{textbox}}<p>(10) <i>HUBZone small business concern.</i> [Complete only if the offeror represented itself as a small business concern in paragraph (c)(1) of this provision.] The offeror represents, as part of its offer, that&mdash;</p><p>(i) It {{checkbox_52.212-3[19]}}} is, {{checkbox_52.212-3[20]}}} is not a HUBZone small business concern listed, on the date of this representation, on the List of Qualified HUBZone Small Business Concerns maintained by the Small Business Administration, and no material changes in ownership and control, principal office, or HUBZone employee percentage have occurred since it was certified in accordance with 13 CFR Part 126; and</p><p>(ii) It {{checkbox_52.212-3[21]}}} is, {{checkbox_52.212-3[22]}}} is not a HUBZone joint venture that complies with the requirements of 13 CFR Part 126, and the representation in paragraph (c)(10)(i) of this provision is accurate for each HUBZone small business concern participating in the HUBZone joint venture. [<i>The offeror shall enter the names of each of the HUBZone small business concerns participating in the HUBZone joint venture: {{textbox}}.</i>] Each HUBZone small business concern participating in the HUBZone joint venture shall submit a separate signed copy of the HUBZone representation.</p><p>(d) Representations required to implement provisions of Executive Order 11246&mdash;</p><p>(1) Previous contracts and compliance. The offeror represents that&mdash;</p><p>(i) It {{checkbox_52.212-3[23]}}} has, {{checkbox_52.212-3[24]}}} has not participated in a previous contract or subcontract subject to the Equal Opportunity clause of this solicitation; and</p><p>(ii) It {{checkbox_52.212-3[25]}}} has, {{checkbox_52.212-3[26]}}} has not filed all required compliance reports.</p><p>(2) Affirmative Action Compliance. The offeror represents that&mdash;</p><p>(i) It {{checkbox_52.212-3[27]}}} has developed and has on file, {{checkbox_52.212-3[28]}}}&nbsp;&nbsp;&nbsp;has not developed and does not have on file, at each establishment, affirmative action programs required by rules and regulations of the Secretary of Labor (41 CFR parts 60-1 and 60-2), or</p><p>(ii) It {{checkbox_52.212-3[29]}}}&nbsp;&nbsp;has not previously had contracts subject to the written affirmative action programs requirement of the rules and regulations of the Secretary of Labor.</p><p>(e) <i>Certification Regarding Payments to Influence Federal Transactions (31 U.S.C. 1352).</i> (Applies only if the contract is expected to exceed $150,000.) By submission of its offer, the offeror certifies to the best of its knowledge and belief that no Federal appropriated funds have been paid or will be paid to any person for influencing or attempting to influence an officer or employee of any agency, a Member of Congress, an officer or employee of Congress or an employee of a Member of Congress on his or her behalf in connection with the award of any resultant contract. If any registrants under the Lobbying Disclosure Act of 1995 have made a lobbying contact on behalf of the offeror with respect to this contract, the offeror shall complete and submit, with its offer, OMB Standard Form LLL, Disclosure of Lobbying Activities, to provide the name of the registrants. The offeror need not report regularly employed officers or employees of the offeror to whom payments of reasonable compensation were made.</p><p>(f) <i>Buy American Certificate.</i> (Applies only if the clause at Federal Acquisition Regulation (FAR) 52.225-1, Buy American&mdash;Supplies, is included in this solicitation.)</p><p>(1) The offeror certifies that each end product, except those listed in paragraph (f)(2) of this provision, is a domestic end product and that for other than COTS items, the offeror has considered components of unknown origin to have been mined, produced, or manufactured outside the United States. The offeror shall list as foreign end products those end products manufactured in the United States that do not qualify as domestic end products, <i>i.e.</i>, an end product that is not a COTS item and does not meet the component test in paragraph (2) of the definition of âdomestic end product.â The terms âcommercially available off-the-shelf (COTS) item,â âcomponent,â âdomestic end product,â âend product,â âforeign end product,â and âUnited Statesâ are defined in the clause of this solicitation entitled âBuy American&mdash;Supplies.â</p><p>(2) Foreign End Products:</p>Line Item No.: {{textbox}}Country of Origin: {{textbox}}<p>(List as necessary)</p><p>(3) The Government will evaluate offers in accordance with the policies and procedures of FAR Part 25.</p><p>(g)(1) <i>Buy American&mdash;Free Trade Agreements&mdash;Israeli Trade Act Certificate.</i> (Applies only if the clause at FAR 52.225-3, Buy American&mdash;Free Trade Agreements&mdash;Israeli Trade Act, is included in this solicitation.)</p><p>(i) The offeror certifies that each end product, except those listed in paragraph (g)(1)(ii) or (g)(1)(iii) of this provision, is a domestic end product and that for other than COTS items, the offeror has considered components of unknown origin to have been mined, produced, or manufactured outside the United States. The terms âBahrainian, Moroccan, Omani, Panamanian, or Peruvian end product,â âcommercially available off-the-shelf (COTS) item,â âcomponent,â âdomestic end product,â âend product,â âforeign end product,â âFree Trade Agreement country,â âFree Trade Agreement country end product,â âIsraeli end product,â and âUnited Statesâ are defined in the clause of this solicitation entitled âBuy American&mdash;Free Trade Agreements&mdash;Israeli Trade Act.â</p><p>(ii) The offeror certifies that the following supplies are Free Trade Agreement country end products (other than Bahrainian, Moroccan, Omani, Panamanian, or Peruvian end products) or Israeli end products as defined in the clause of this solicitation entitled âBuy American&mdash;Free Trade Agreements&mdash;Israeli Trade Actâ:</p><p>Free Trade Agreement Country End Products (Other than Bahrainian, Moroccan, Omani, Panamanian, or Peruvian End Products) or Israeli End Products:</p>\n<div><div><table border="&quot;1&quot;" cellpadding="&quot;1&quot;" cellspacing="&quot;1&quot;" frame="&quot;void&quot;" width="&quot;100%&quot;"><tbody><tr><th scope="&quot;col&quot;">Line Item No.</th><th scope="&quot;col&quot;">&nbsp;&nbsp;</th><th scope="&quot;col&quot;">Country of Origin</th></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">&nbsp;&nbsp;</td><td align="&quot;right&quot;"></td><td align="&quot;left&quot;"></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">&nbsp;&nbsp;</td><td align="&quot;right&quot;"></td><td align="&quot;left&quot;"></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">&nbsp;&nbsp;</td><td align="&quot;right&quot;"></td><td align="&quot;left&quot;"></td></tr><tr><td align="&quot;center&quot;" colspan="&quot;3&quot;" scope="&quot;row&quot;">[List as necessary]</td></tr></tbody></table></div></div><p>(iii) The offeror shall list those supplies that are foreign end products (other than those listed in paragraph (g)(1)(ii) of this provision) as defined in the clause of this solicitation entitled âBuy American&mdash;Free Trade Agreements&mdash;Israeli Trade Act.â The offeror shall list as other foreign end products those end products manufactured in the United States that do not qualify as domestic end products, <i>i.e.</i>, an end product that is not a COTS item and does not meet the component test in paragraph (2) of the definition of âdomestic end product.â</p>\n<h2>Other Foreign End Products</h2>\nLine Item No.: {{textbox}}Country of Origin: {{textbox}}<p>(List as necessary)</p><p>(iv) The Government will evaluate offers in accordance with the policies and procedures of FAR Part 25.</p><p>(2) <i>Buy American&mdash;Free Trade Agreements&mdash;Israeli Trade Act Certificate, Alternate I.</i> If <i>Alternate I</i> to the clause at FAR 52.225-3 is included in this solicitation, substitute the following paragraph (g)(1)(ii) for paragraph (g)(1)(ii) of the basic provision: </p><p>(g)(1)(ii) The offeror certifies that the following supplies are Canadian end products as defined in the clause of this solicitation entitled âBuy American&mdash;Free Trade Agreements&mdash;Israeli Trade Actâ: </p>\n<h3>Canadian End Products: </h3>\n<h3>Line Item No. </h3>\n&nbsp; {{textbox}}&nbsp; {{textbox}}&nbsp; {{textbox}}<p>$(<i>List as necessary</i>) </p><p>(3) <i>Buy American&mdash;Free Trade Agreements&mdash;Israeli Trade Act Certificate, Alternate II.</i> If <i>Alternate II</i> to the clause at FAR 52.225-3 is included in this solicitation, substitute the following paragraph (g)(1)(ii) for paragraph (g)(1)(ii) of the basic provision: </p><p>(g)(1)(ii) The offeror certifies that the following supplies are Canadian end products or Israeli end products as defined in the clause of this solicitation entitled âBuy American&mdash;Free Trade Agreements&mdash;Israeli Trade Actâ: </p>\n<h3>Canadian or Israeli End Products: </h3>\n<p>Line Item No. </p>&nbsp; {{textbox}}&nbsp; {{textbox}}&nbsp; {{textbox}}\n<p>Country of Origin </p>&nbsp; {{textbox}}&nbsp; {{textbox}}&nbsp; {{textbox}}<p>$(<i>List as necessary</i>)</p><p>(g)(4) <i>Buy American&mdash;Free Trade Agreements&mdash;Israeli Trade Act Certificate, Alternate III.</i> If Alternate III to the clause at FAR 52.225-3 is included in this solicitation, substitute the following paragraph (g)(1)(ii) for paragraph (g)(1)(ii) of the basic provision:</p><p>(g)(1)(ii) The offeror certifies that the following supplies are Free Trade Agreement country end products (other than Bahrainian, Korean, Moroccan, Omani, Panamanian, or Peruvian end products) or Israeli end products as defined in the clause of this solicitation entitled âBuy American&mdash;Free Trade Agreements&mdash;Israeli Trade Actâ:</p><p>Free Trade Agreement Country End Products (Other than Bahrainian, Korean, Moroccan, Omani, Panamanian, or Peruvian End Products) or Israeli End Products:</p>\n<div><div><table border="&quot;1&quot;" cellpadding="&quot;1&quot;" cellspacing="&quot;1&quot;" frame="&quot;void&quot;" width="&quot;100%&quot;"><tbody><tr><th scope="&quot;col&quot;">Line Item No.</th><th scope="&quot;col&quot;">&nbsp;&nbsp;</th><th scope="&quot;col&quot;">Country of Origin</th></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">&nbsp;&nbsp;</td><td align="&quot;right&quot;"></td><td align="&quot;left&quot;"></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">&nbsp;&nbsp;</td><td align="&quot;right&quot;"></td><td align="&quot;left&quot;"></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">&nbsp;&nbsp;</td><td align="&quot;right&quot;"></td><td align="&quot;left&quot;"></td></tr><tr><td align="&quot;center&quot;" colspan="&quot;3&quot;" scope="&quot;row&quot;">[List as necessary]</td></tr></tbody></table></div></div><p>(5) <i>Trade Agreements Certificate.</i> (Applies only if the clause at FAR 52.225-5, Trade Agreements, is included in this solicitation.)</p><p>(i) The offeror certifies that each end product, except those listed in paragraph (g)(5)(ii) of this provision, is a U.S.-made or designated country end product, as defined in the clause of this solicitation entitled âTrade Agreementsâ.</p><p>(ii) The offeror shall list as other end products those end products that are not U.S.-made or designated country end products.</p><p>Other End Products:</p>\n<div><div><table border="&quot;1&quot;" cellpadding="&quot;1&quot;" cellspacing="&quot;1&quot;" frame="&quot;void&quot;" width="&quot;100%&quot;"><tbody><tr><th scope="&quot;col&quot;">Line item No.</th><th scope="&quot;col&quot;">&nbsp;&nbsp;</th><th scope="&quot;col&quot;">Country of origin</th></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">&nbsp;&nbsp;</td><td align="&quot;right&quot;"></td><td align="&quot;left&quot;"></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">&nbsp;&nbsp;</td><td align="&quot;right&quot;"></td><td align="&quot;left&quot;"></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">&nbsp;&nbsp;</td><td align="&quot;right&quot;"></td><td align="&quot;left&quot;"></td></tr><tr><td align="&quot;center&quot;" colspan="&quot;3&quot;" scope="&quot;row&quot;">[List as necessary]</td></tr></tbody></table></div></div><p>(iii) The Government will evaluate offers in accordance with the policies and procedures of FAR Part 25. For line items covered by the WTO GPA, the Government will evaluate offers of U.S.-made or designated country end products without regard to the restrictions of the Buy American statute. The Government will consider for award only offers of U.S.-made or designated country end products unless the Contracting Officer determines that there are no offers for such products or that the offers for such products are insufficient to fulfill the requirements of the solicitation.</p><p>(h) <i>Certification Regarding Responsibility Matters (Executive Order 12689).</i> (Applies only if the contract value is expected to exceed the simplified acquisition threshold.) The offeror certifies, to the best of its knowledge and belief, that the offeror and/or any of its principals&mdash; </p><p>(1) {{checkbox_52.212-3[30]}}} Are, {{checkbox_52.212-3[31]}}} are not presently debarred, suspended, proposed for debarment, or declared ineligible for the award of contracts by any Federal agency;</p><p>(2) {{checkbox_52.212-3[32]}}} Have, {{checkbox_52.212-3[33]}}} have not, within a three-year period preceding this offer, been convicted of or had a civil judgment rendered against them for: Commission of fraud or a criminal offense in connection with obtaining, attempting to obtain, or performing a Federal, state or local government contract or subcontract; violation of Federal or state antitrust statutes relating to the submission of offers; or Commission of embezzlement, theft, forgery, bribery, falsification or destruction of records, making false statements, tax evasion, violating Federal criminal tax laws, or receiving stolen property,</p><p>(3) {{checkbox_52.212-3[34]}}} Are, {{checkbox_52.212-3[35]}}} are not presently indicted for, or otherwise criminally or civilly charged by a Government entity with, commission of any of these offenses enumerated in paragraph (h)(2) of this clause; and</p><p>(i) <i>Certification Regarding Knowledge of Child Labor for Listed End Products (Executive Order 13126). [The Contracting Officer must list in paragraph (i)(1) any end products being acquired under this solicitation that are included in the List of Products Requiring Contractor Certification as to Forced or Indentured Child Labor, unless excluded at 22.1503(b).</i>] </p><p>(1) <i>Listed end products.</i> </p>\n<h3>Listed End Product</h3>\n&nbsp; {{textbox}}&nbsp;  {{textbox}}\n<h3>Listed Countries of Origin </h3>\n&nbsp; {{textbox}}&nbsp; {{textbox}}<p>(2) <i>Certification.</i> [<i>If the Contracting Officer has identified end products and countries of origin in paragraph (i)(1) of this provision, then the offeror must certify to either (i)(2)(i) or (i)(2)(ii) by checking the appropriate block.</i>]</p><p>{{checkbox_52.212-3[36]}}}&nbsp;&nbsp;(i) The offeror will not supply any end product listed in paragraph (i)(1) of this provision that was mined, produced, or manufactured in the corresponding country as listed for that product.</p><p>{{checkbox_52.212-3[37]}}}&nbsp;&nbsp;(ii) The offeror may supply an end product listed in paragraph (i)(1) of this provision that was mined, produced, or manufactured in the corresponding country as listed for that product. The offeror certifies that it has made a good faith effort to determine whether forced or indentured child labor was used to mine, produce, or manufacture any such end product furnished under this contract. On the basis of those efforts, the offeror certifies that it is not aware of any such use of child labor.</p><p>(4) Have,{{checkbox_52.212-3[38]}}}&nbsp;&nbsp; have not, within a three-year period preceding this offer, been notified of any delinquent Federal taxes in an amount that exceeds $3,000 for which the liability remains unsatisfied.</p><p>(i) Taxes are considered delinquent if both of the following criteria apply:</p><p>(A) <i>The tax liability is finally determined.</i> The liability is finally determined if it has been assessed. A liability is not finally determined if there is a pending administrative or judicial challenge. In the case of a judicial challenge to the liability, the liability is not finally determined until all judicial appeal rights have been exhausted.</p><p>(B) <i>The taxpayer is delinquent in making payment.</i> A taxpayer is delinquent if the taxpayer has failed to pay the tax liability when full payment was due and required. A taxpayer is not delinquent in cases where enforced collection action is precluded.</p><p>(ii) <i>Examples.</i> (A) The taxpayer has received a statutory notice of deficiency, under I.R.C. §6212, which entitles the taxpayer to seek Tax Court review of a proposed tax deficiency. This is not a delinquent tax because it is not a final tax liability. Should the taxpayer seek Tax Court review, this will not be a final tax liability until the taxpayer has exercised all judicial appeal rights.</p><p>(B) The IRS has filed a notice of Federal tax lien with respect to an assessed tax liability, and the taxpayer has been issued a notice under I.R.C. §6320 entitling the taxpayer to request a hearing with the IRS Office of Appeals contesting the lien filing, and to further appeal to the Tax Court if the IRS determines to sustain the lien filing. In the course of the hearing, the taxpayer is entitled to contest the underlying tax liability because the taxpayer has had no prior opportunity to contest the liability. This is not a delinquent tax because it is not a final tax liability. Should the taxpayer seek tax court review, this will not be a final tax liability until the taxpayer has exercised all judicial appeal rights.</p><p>(C) The taxpayer has entered into an installment agreement pursuant to I.R.C. §6159. The taxpayer is making timely payments and is in full compliance with the agreement terms. The taxpayer is not delinquent because the taxpayer is not currently required to make full payment.</p><p>(D) The taxpayer has filed for bankruptcy protection. The taxpayer is not delinquent because enforced collection action is stayed under 11 U.S.C. 362 (the Bankruptcy Code).</p><p>(i) <i>Certification Regarding Knowledge of Child Labor for Listed End Products (Executive Order 13126). [The Contracting Officer must list in paragraph (i)(1) any end products being acquired under this solicitation that are included in the List of Products Requiring Contractor Certification as to Forced or Indentured Child Labor, unless excluded at 22.1503(b).</i>] </p><p>(1) <i>Listed end products.</i> </p>\n<h3>Listed End Product</h3>\n&nbsp; {{textbox}}&nbsp;  {{textbox}}\n<h3>Listed Countries of Origin </h3>\n&nbsp; {{textbox}}&nbsp; {{textbox}}<p>(2) <i>Certification.</i> [<i>If the Contracting Officer has identified end products and countries of origin in paragraph (i)(1) of this provision, then the offeror must certify to either (i)(2)(i) or (i)(2)(ii) by checking the appropriate block.</i>]</p><p>{{checkbox_52.212-3[39]}}}&nbsp;&nbsp;(i) The offeror will not supply any end product listed in paragraph (i)(1) of this provision that was mined, produced, or manufactured in the corresponding country as listed for that product.</p><p>{{checkbox_52.212-3[40]}}}&nbsp;&nbsp;(ii) The offeror may supply an end product listed in paragraph (i)(1) of this provision that was mined, produced, or manufactured in the corresponding country as listed for that product. The offeror certifies that it has made a good faith effort to determine whether forced or indentured child labor was used to mine, produce, or manufacture any such end product furnished under this contract. On the basis of those efforts, the offeror certifies that it is not aware of any such use of child labor.</p><p>(j) <i>Place of manufacture.</i> (Does not apply unless the solicitation is predominantly for the acquisition of manufactured end products.) For statistical purposes only, the offeror shall indicate whether the place of manufacture of the end products it expects to provide in response to this solicitation is predominantly&mdash;</p><p>(1) {{checkbox_52.212-3[41]}}} In the United States (Check this box if the total anticipated price of offered end products manufactured in the United States exceeds the total anticipated price of offered end products manufactured outside the United States); or</p><p>(2) {{checkbox_52.212-3[42]}}} Outside the United States.</p><p>(k) <i>Certificates regarding exemptions from the application of the Service Contract Labor Standards.</i> (Certification by the offeror as to its compliance with respect to the contract also constitutes its certification as to compliance by its subcontractor if it subcontracts out the exempt services.) [<i>The contracting officer is to check a box to indicate if paragraph (k)(1) or (k)(2) applies.</i>]</p><p>(1){{checkbox_52.212-3[43]}}}&nbsp;&nbsp; Maintenance, calibration, or repair of certain equipment as described in FAR 22.1003-4(c)(1). The offeror {{checkbox_52.212-3[44]}}}&nbsp;&nbsp;does {{checkbox_52.212-3[45]}}}&nbsp;&nbsp; does not certify that&mdash;</p><p>(i) The items of equipment to be serviced under this contract are used regularly for other than Governmental purposes and are sold or traded by the offeror (or subcontractor in the case of an exempt subcontract) in substantial quantities to the general public in the course of normal business operations;</p><p>(ii) The services will be furnished at prices which are, or are based on, established catalog or market prices (see FAR 22.1003-4(c)(2)(ii)) for the maintenance, calibration, or repair of such equipment; and</p><p>(iii) The compensation (wage and fringe benefits) plan for all service employees performing work under the contract will be the same as that used for these employees and equivalent employees servicing the same equipment of commercial customers.</p><p>(2){{checkbox_52.212-3[46]}}}&nbsp;&nbsp;Certain services as described in FAR 22.1003-4(d)(1). The offeror {{checkbox_52.212-3[47]}}}&nbsp;&nbsp;does {{checkbox_52.212-3[48]}}}&nbsp;&nbsp;does not certify that&mdash;</p><p>(i) The services under the contract are offered and sold regularly to non-Governmental customers, and are provided by the offeror (or subcontractor in the case of an exempt subcontract) to the general public in substantial quantities in the course of normal business operations;</p><p>(ii) The contract services will be furnished at prices that are, or are based on, established catalog or market prices (see FAR 22.1003-4(d)(2)(iii));</p><p>(iii) Each service employee who will perform the services under the contract will spend only a small portion of his or her time (a monthly average of less than 20 percent of the available hours on an annualized basis, or less than 20 percent of available hours during the contract period if the contract period is less than a month) servicing the Government contract; and</p><p>(iv) The compensation (wage and fringe benefits) plan for all service employees performing work under the contract is the same as that used for these employees and equivalent employees servicing commercial customers.</p><p>(3) If paragraph (k)(1) or (k)(2) of this clause applies&mdash;</p><p>(i) If the offeror does not certify to the conditions in paragraph (k)(1) or (k)(2) and the Contracting Officer did not attach a Service Contract Labor Standards wage determination to the solicitation, the offeror shall notify the Contracting Officer as soon as possible; and</p><p>(ii) The Contracting Officer may not make an award to the offeror if the offeror fails to execute the certification in paragraph (k)(1) or (k)(2) of this clause or to contact the Contracting Officer as required in paragraph (k)(3)(i) of this clause.</p><p>(l) <i>Taxpayer Identification Number (TIN) (26 U.S.C. 6109, 31 U.S.C. 7701).</i> (Not applicable if the offeror is required to provide this information to the SAM database to be eligible for award.)</p><p>(1) All offerors must submit the information required in paragraphs (l)(3) through (l)(5) of this provision to comply with debt collection requirements of 31 U.S.C. 7701(c) and 3325(d), reporting requirements of 26 U.S.C. 6041, 6041A, and 6050M, and implementing regulations issued by the Internal Revenue Service (IRS).</p><p>(2) The TIN may be used by the Government to collect and report on any delinquent amounts arising out of the offeror''s relationship with the Government (31 U.S.C. 7701(c)(3)). If the resulting contract is subject to the payment reporting requirements described in FAR 4.904, the TIN provided hereunder may be matched with IRS records to verify the accuracy of the offeror''s TIN.</p><p>(3) <i>Taxpayer Identification Number (TIN).</i></p><p>{{checkbox_52.212-3[49]}}}&nbsp;&nbsp;TIN: {{textbox}}.</p><p>{{checkbox_52.212-3[50]}}}&nbsp;&nbsp;TIN has been applied for.</p><p>{{checkbox_52.212-3[51]}}}&nbsp;&nbsp;TIN is not required because:</p><p>{{checkbox_52.212-3[52]}}}&nbsp;&nbsp;Offeror is a nonresident alien, foreign corporation, or foreign partnership that does not have income effectively connected with the conduct of a trade or business in the United States and does not have an office or place of business or a fiscal paying agent in the United States;</p><p>{{checkbox_52.212-3[53]}}}&nbsp;&nbsp;Offeror is an agency or instrumentality of a foreign government;</p><p>{{checkbox_52.212-3[54]}}}&nbsp;&nbsp;Offeror is an agency or instrumentality of the Federal Government.</p><p>(4) <i>Type of organization.</i></p><p>{{checkbox_52.212-3[55]}}}&nbsp;&nbsp;Sole proprietorship;</p><p>{{checkbox_52.212-3[56]}}}&nbsp;&nbsp;Partnership;</p><p>{{checkbox_52.212-3[57]}}}&nbsp;&nbsp;Corporate entity (not tax-exempt);</p><p>{{checkbox_52.212-3[58]}}}&nbsp;&nbsp;Corporate entity (tax-exempt);</p><p>{{checkbox_52.212-3[59]}}}&nbsp;&nbsp;Government entity (Federal, State, or local);</p><p>{{checkbox_52.212-3[60]}}}&nbsp;&nbsp;Foreign government;</p><p>{{checkbox_52.212-3[61]}}}&nbsp;&nbsp;International organization per 26 CFR 1.6049-4;</p><p>{{checkbox_52.212-3[62]}}}&nbsp;&nbsp;Other {{textbox}}.</p><p>(5) <i>Common parent.</i></p><p>{{checkbox_52.212-3[63]}}}&nbsp;&nbsp;Offeror is not owned or controlled by a common parent;</p><p>{{checkbox_52.212-3[64]}}}&nbsp;&nbsp;Name and TIN of common parent:</p><p>Name {{textbox}}.</p><p>TIN {{textbox}}.</p><p>(m) <i>Restricted business operations in Sudan.</i> By submission of its offer, the offeror certifies that the offeror does not conduct any restricted business operations in Sudan.</p><p>(n) <i>Prohibition on Contracting with Inverted Domestic Corporations.</i> (1) Government agencies are not permitted to use appropriated (or otherwise made available) funds for contracts with either an inverted domestic corporation, or a subsidiary of an inverted domestic corporation, unless the exception at 9.108-2(b) applies or the requirement is waived in accordance with the procedures at 9.108-4.</p><p>(2) <i>Representation.</i> By submission of its offer, the offeror represents that&mdash;</p><p>(i) It is not an inverted domestic corporation; and</p><p>(ii) It is not a subsidiary of an inverted domestic corporation.</p><p>(o) <i>Prohibition on contracting with entities engaging in certain activities or transactions relating to Iran.</i> (1) The offeror shall email questions concerning sensitive technology to the Department of State at CISADA106@state.gov.</p><p>(2) <i>Representation and certifications.</i> Unless a waiver is granted or an exception applies as provided in paragraph (o)(3) of this provision, by submission of its offer, the offeror&mdash;</p><p>(i) Represents, to the best of its knowledge and belief, that the offeror does not export any sensitive technology to the government of Iran or any entities or individuals owned or controlled by, or acting on behalf or at the direction of, the government of Iran;</p><p>(ii) Certifies that the offeror, or any person owned or controlled by the offeror, does not engage in any activities for which sanctions may be imposed under section 5 of the Iran Sanctions Act; and</p><p>(iii) Certifies that the offeror, and any person owned or controlled by the offeror, does not knowingly engage in any transaction that exceeds $3,000 with Iran''s Revolutionary Guard Corps or any of its officials, agents, or affiliates, the property and interests in property of which are blocked pursuant to the International Emergency Economic Powers Act (50 U.S.C. 1701 <i>et seq.</i>) (see OFAC''s Specially Designated Nationals and Blocked Persons List at <i>http://www.treasury.gov/ofac/downloads/t11sdn.pdf</i>).</p><p>(3) The representation and certification requirements of paragraph (o)(2) of this provision do not apply if&mdash;</p><p>(i) This solicitation includes a trade agreements certification (<i>e.g.,</i> 52.212-3(g) or a comparable agency provision); and</p><p>(ii) The offeror has certified that all the offered products to be supplied are designated country end products.</p>'
--               '<p>The Offeror shall complete only paragraph (b) of this provision if the Offeror has completed the annual representations and certification electronically via the System for Award Management (SAM) Web site accessed through <i>http://www.acquisition.gov.</i> If the Offeror has not completed the annual representations and certifications electronically, the Offeror shall complete only paragraphs (c) through (p) of this provision.</p><p>(a) <i>Definitions.</i> As used in this provision&mdash;</p><p><i>Economically disadvantaged women-owned small business (EDWOSB) concern</i> means a small business concern that is at least 51 percent directly and unconditionally owned by, and the management and daily business operations of which are controlled by, one or more women who are citizens of the United States and who are economically disadvantaged in accordance with 13 CFR part 127. It automatically qualifies as a women-owned small business eligible under the WOSB Program.</p><p><i>Forced or indentured child labor</i> means all work or service&mdash; </p><p>(1) Exacted from any person under the age of 18 under the menace of any penalty for its nonperformance and for which the worker does not offer himself voluntarily; or </p><p>(2) Performed by any person under the age of 18 pursuant to a contract the enforcement of which can be accomplished by process or penalties.</p><p><i>Highest-level owner</i> means the entity that owns or controls an immediate owner of the offeror, or that owns or controls one or more entities that control an immediate owner of the offeror. No entity owns or exercises control of the highest level owner.</p><p><i>Immediate owner</i> means an entity, other than the offeror, that has direct control of the offeror. Indicators of control include, but are not limited to, one or more of the following: Ownership or interlocking management, identity of interests among family members, shared facilities and equipment, and the common use of employees.</p><p><i>Inverted domestic corporation</i> means a foreign incorporated entity that meets the definition of an inverted domestic corporation under 6 U.S.C. 395(b), applied in accordance with the rules and definitions of 6 U.S.C. 395(c).</p><p><i>Manufactured end product</i> means any end product in Federal Supply Classes (FSC) 1000-9999, except&mdash;</p><p>(1) FSC 5510, Lumber and Related Basic Wood Materials;</p><p>(2) Federal Supply Group (FSG) 87, Agricultural Supplies;</p><p>(3) FSG 88, Live Animals;</p><p>(4) FSG 89, Food and Related Consumables;</p><p>(5) FSC 9410, Crude Grades of Plant Materials;</p><p>(6) FSC 9430, Miscellaneous Crude Animal Products, Inedible;</p><p>(7) FSC 9440, Miscellaneous Crude Agricultural and Forestry Products;</p><p>(8) FSC 9610, Ores;</p><p>(9) FSC 9620, Minerals, Natural and Synthetic; and</p><p>(10) FSC 9630, Additive Metal Materials.</p><p><i>Manufactured end product</i> means any end product in product and service codes (PSCs) 1000-9999, except&mdash;</p><p>(1) PSC 5510, Lumber and Related Basic Wood Materials;</p><p>(2) Product or Service Group (PSG) 87, Agricultural Supplies;</p><p>(3) PSG 88, Live Animals;</p><p>(4) PSG 89, Subsistence;</p><p>(5) PSC 9410, Crude Grades of Plant Materials;</p><p>(6) PSC 9430, Miscellaneous Crude Animal Products, Inedible;</p><p>(7) PSC 9440, Miscellaneous Crude Agricultural and Forestry Products;</p><p>(8) PSC 9610, Ores;</p><p>(9) PSC 9620, Minerals, Natural and Synthetic; and</p><p>(10) PSC 9630, Additive Metal Materials.</p><p><i>Place of manufacture</i> means the place where an end product is assembled out of components, or otherwise made or processed from raw materials into the finished product that is to be provided to the Government. If a product is disassembled and reassembled, the place of reassembly is not the place of manufacture.</p><p><i>Restricted business operations</i> means business operations in Sudan that include power production activities, mineral extraction activities, oil-related activities, or the production of military equipment, as those terms are defined in the Sudan Accountability and Divestment Act of 2007 (Pub. L. 110-174). Restricted business operations do not include business operations that the person (as that term is defined in Section 2 of the Sudan Accountability and Divestment Act of 2007) conducting the business can demonstrate&mdash;</p><p>(1) Are conducted under contract directly and exclusively with the regional government of southern Sudan;</p><p>(2) Are conducted pursuant to specific authorization from the Office of Foreign Assets Control in the Department of the Treasury, or are expressly exempted under Federal law from the requirement to be conducted under such authorization;</p><p>(3) Consist of providing goods or services to marginalized populations of Sudan;</p><p>(4) Consist of providing goods or services to an internationally recognized peacekeeping force or humanitarian organization;</p><p>(5) Consist of providing goods or services that are used only to promote health or education; or</p><p>(6) Have been voluntarily suspended.</p><p><i>Sensitive technology</i>&mdash;</p><p>(1) Means hardware, software, telecommunications equipment, or any other technology that is to be used specifically&mdash;</p><p>(i) To restrict the free flow of unbiased information in Iran; or</p><p>(ii) To disrupt, monitor, or otherwise restrict speech of the people of Iran; and</p><p>(2) Does not include information or informational materials the export of which the President does not have the authority to regulate or prohibit pursuant to section 203(b)(3) of the International Emergency Economic Powers Act (50 U.S.C. 1702(b)(3)).</p><p><i>Service-disabled veteran-owned small business concern</i>&mdash; </p><p>(1) Means a small business concern&mdash; </p><p>(i) Not less than 51 percent of which is owned by one or more service&mdash;disabled veterans or, in the case of any publicly owned business, not less than 51 percent of the stock of which is owned by one or more service-disabled veterans; and </p><p>(ii) The management and daily business operations of which are controlled by one or more service-disabled veterans or, in the case of a service-disabled veteran with permanent and severe disability, the spouse or permanent caregiver of such veteran. </p><p>(2) <i>Service-disabled veteran</i> means a veteran, as defined in 38 U.S.C. 101(2), with a disability that is service-connected, as defined in 38 U.S.C. 101(16).</p><p><i>Small business concern</i> means a concern, including its affiliates, that is independently owned and operated, not dominant in the field of operation in which it is bidding on Government contracts, and qualified as a small business under the criteria in 13 CFR Part 121 and size standards in this solicitation.</p><p><i>Small disadvantaged business concern,</i> consistent with 13 CFR 124.1002, means a small business concern under the size standard applicable to the acquisition, that&mdash;</p><p>(1) Is at least 51 percent unconditionally and directly owned (as defined at 13 CFR 124.105) by&mdash;</p><p>(i) One or more socially disadvantaged (as defined at 13 CFR 124.103) and economically disadvantaged (as defined at 13 CFR 124.104) individuals who are citizens of the United States; and</p><p>(ii) Each individual claiming economic disadvantage has a net worth not exceeding $750,000 after taking into account the applicable exclusions set forth at 13 CFR 124.104(c)(2); and</p><p>(2) The management and daily business operations of which are controlled (as defined at 13.CFR 124.106) by individuals, who meet the criteria in paragraphs (1)(i) and (ii) of this definition.</p><p><i>Subsidiary</i> means an entity in which more than 50 percent of the entity is owned&mdash;</p><p>(1) Directly by a parent corporation; or</p><p>(2) Through another subsidiary of a parent corporation.</p><p><i>Veteran-owned small business concern</i> means a small business concern&mdash; </p><p>(1) Not less than 51 percent of which is owned by one or more veterans (as defined at 38 U.S.C. 101(2)) or, in the case of any publicly owned business, not less than 51 percent of the stock of which is owned by one or more veterans; and</p><p>(2) The management and daily business operations of which are controlled by one or more veterans.</p><p><i>Women-owned business concern</i> means a concern which is at least 51 percent owned by one or more women; or in the case of any publicly owned business, at least 51 percent of its stock is owned by one or more women; and whose management and daily business operations are controlled by one or more women.</p><p><i>Women-owned small business concern</i> means a small business concern&mdash;</p><p>(1) That is at least 51 percent owned by one or more women; or, in the case of any publicly owned business, at least 51 percent of the stock of which is owned by one or more women; and</p><p>(2) Whose management and daily business operations are controlled by one or more women.</p><p><i>Women-owned small business (WOSB) concern eligible under the WOSB Program</i> (in accordance with 13 CFR part 127), means a small business concern that is at least 51 percent directly and unconditionally owned by, and the management and daily business operations of which are controlled by, one or more women who are citizens of the United States.</p><p>(b)(1) <i>Annual Representations and Certifications.</i> Any changes provided by the offeror in paragraph (b)(2) of this provision do not automatically change the representations and certifications posted on the SAM website.</p><p>(2) The offeror has completed the annual representations and certifications electronically via the SAM website accessed through <i>http://www.acquisition.gov.</i> After reviewing the SAM database information, the offeror verifies by submission of this offer that the representations and certifications currently posted electronically at FAR 52.212-3, Offeror Representations and Certifications&mdash;Commercial Items, have been entered or updated in the last 12 months, are current, accurate, complete, and applicable to this solicitation (including the business size standard applicable to the NAICS code referenced for this solicitation), as of the date of this offer and are incorporated in this offer by reference (see FAR 4.1201), except for paragraphs {{textbox}}.</p><p>[<i>Offeror to identify the applicable paragraphs at (c) through (p) of this provision that the offeror has completed for the purposes of this solicitation only, if any.</i></p><p><i>These amended representation(s) and/or certification(s) are also incorporated in this offer and are current, accurate, and complete as of the date of this offer.</i></p><p><i>Any changes provided by the offeror are applicable to this solicitation only, and do not result in an update to the representations and certifications posted electronically on SAM.</i>]</p><p>(c) Offerors must complete the following representations when the resulting contract will be performed in the United States or its outlying areas. Check all that apply.</p><p>(1) <i>Small business concern.</i> The offeror represents as part of its offer that it {{checkbox_52.212-3[0]}}} is, {{checkbox_52.212-3[1]}}}Â&nbsp;Â&nbsp;is not a small business concern.</p><p>(2) <i>Veteran-owned small business concern.</i> [<i>Complete only if the offeror represented itself as a small business concern in paragraph (c)(1) of this provision.</i>] The offeror represents as part of its offer that it {{checkbox_52.212-3[2]}}} is, {{checkbox_52.212-3[3]}}} is not a veteran-owned small business concern. </p><p>(3) <i>Service-disabled veteran-owned small business concern.</i> [<i>Complete only if the offeror represented itself as a veteran-owned small business concern in paragraph (c)(2) of this provision.</i>] The offeror represents as part of its offer that it {{checkbox_52.212-3[4]}}} is, {{checkbox_52.212-3[5]}}} is not a service-disabled veteran-owned small business concern.</p><p>(4) <i>Small disadvantaged business concern. [Complete only if the offeror represented itself as a small business concern in paragraph (c)(1) of this provision.]</i> The offeror represents  that it {{checkbox_52.212-3[6]}}} is, {{checkbox_52.212-3[7]}}} is not a small disadvantaged business concern as defined in 13 CFR 124.1002.</p><p>(5) <i>Women-owned small business concern. [Complete only if the offeror represented itself as a small business concern in paragraph (c)(1) of this provision.]</i> The offeror represents that it {{checkbox_52.212-3[8]}}} is, {{checkbox_52.212-3[9]}}} is not a women-owned small business concern.</p><p>(6) WOSB concern eligible under the WOSB Program. [<i>Complete only if the offeror represented itself as a women-owned small business concern in paragraph (c)(5) of this provision.</i>] The offeror represents that&mdash;</p><p>(i) It {{checkbox_52.212-3[10]}}} is, {{checkbox_52.212-3[11]}}} is not a WOSB concern eligible under the WOSB Program, has provided all the required documents to the WOSB Repository, and no change in circumstances or adverse decisions have been issued that affects its eligibility; and</p><p>(ii) It {{checkbox_52.212-3[12]}}} is, {{checkbox_52.212-3[13]}}} is not a joint venture that complies with the requirements of 13 CFR part 127, and the representation in paragraph (c)(6)(i) of this provision is accurate for each WOSB concern eligible under the WOSB Program participating in the joint venture. [<i>The offeror shall enter the name or names of the WOSB concern eligible under the WOSB Program and other small businesses that are participating in the joint venture:</i> {{textbox}}.] Each WOSB concern eligible under the WOSB Program participating in the joint venture shall submit a separate signed copy of the WOSB representation.</p><p>(7) Economically disadvantaged women-owned small business (EDWOSB) concern. [<i>Complete only if the offeror represented itself as a WOSB concern eligible under the WOSB Program in (c)(6) of this provision.</i>] The offeror represents that&mdash;</p><p>(i) It {{checkbox_52.212-3[14]}}} is, {{checkbox_52.212-3[15]}}} is not an EDWOSB concern, has provided all the required documents to the WOSB Repository, and no change in circumstances or adverse decisions have been issued that affects its eligibility; and</p><p>(ii) It {{checkbox_52.212-3[16]}}} is, {{checkbox_52.212-3[17]}}} is not a joint venture that complies with the requirements of 13 CFR part 127, and the representation in paragraph (c)(7)(i) of this provision is accurate for each EDWOSB concern participating in the joint venture. [<i>The offeror shall enter the name or names of the EDWOSB concern and other small businesses that are participating in the joint venture:</i> {{textbox}}.] Each EDWOSB concern participating in the joint venture shall submit a separate signed copy of the EDWOSB representation.</p>\n<p>Note to paragraphs (c)(8) and (9): Complete paragraphs (c)(8) and (9) only if this solicitation is expected to exceed the simplified acquisition threshold.</p> <p>(8) <i>Women-owned business concern (other than small business concern). [Complete only if the offeror is a women-owned business concern and did not represent itself as a small business concern in paragraph (c)(1) of this provision.]</i> The offeror represents that it {{checkbox_52.212-3[18]}}} is, a women-owned business concern.</p><p>(9) <i>Tie bid priority for labor surplus area concerns.</i> If this is an invitation for bid, small business offerors may identify the labor surplus areas in which costs to be incurred on account of manufacturing or production (by offeror or first-tier subcontractors) amount to more than 50 percent of the contract price:</p>Â&nbsp; {{textbox}}<p>(10) <i>HUBZone small business concern.</i> [Complete only if the offeror represented itself as a small business concern in paragraph (c)(1) of this provision.] The offeror represents, as part of its offer, that&mdash;</p><p>(i) It {{checkbox_52.212-3[19]}}} is, {{checkbox_52.212-3[20]}}} is not a HUBZone small business concern listed, on the date of this representation, on the List of Qualified HUBZone Small Business Concerns maintained by the Small Business Administration, and no material changes in ownership and control, principal office, or HUBZone employee percentage have occurred since it was certified in accordance with 13 CFR Part 126; and</p><p>(ii) It {{checkbox_52.212-3[21]}}} is, {{checkbox_52.212-3[22]}}} is not a HUBZone joint venture that complies with the requirements of 13 CFR Part 126, and the representation in paragraph (c)(10)(i) of this provision is accurate for each HUBZone small business concern participating in the HUBZone joint venture. [<i>The offeror shall enter the names of each of the HUBZone small business concerns participating in the HUBZone joint venture: {{textbox}}.</i>] Each HUBZone small business concern participating in the HUBZone joint venture shall submit a separate signed copy of the HUBZone representation.</p><p>(d) Representations required to implement provisions of Executive Order 11246&mdash;</p><p>(1) Previous contracts and compliance. The offeror represents that&mdash;</p><p>(i) It {{checkbox_52.212-3[23]}}} has, {{checkbox_52.212-3[24]}}} has not participated in a previous contract or subcontract subject to the Equal Opportunity clause of this solicitation; and</p><p>(ii) It {{checkbox_52.212-3[25]}}} has, {{checkbox_52.212-3[26]}}} has not filed all required compliance reports.</p><p>(2) Affirmative Action Compliance. The offeror represents that&mdash;</p><p>(i) It {{checkbox_52.212-3[27]}}} has developed and has on file, {{checkbox_52.212-3[28]}}}Â&nbsp;Â&nbsp;Â&nbsp;has not developed and does not have on file, at each establishment, affirmative action programs required by rules and regulations of the Secretary of Labor (41 CFR parts 60-1 and 60-2), or</p><p>(ii) It {{checkbox_52.212-3[29]}}}Â&nbsp;Â&nbsp;has not previously had contracts subject to the written affirmative action programs requirement of the rules and regulations of the Secretary of Labor.</p><p>(e) <i>Certification Regarding Payments to Influence Federal Transactions (31 U.S.C. 1352).</i> (Applies only if the contract is expected to exceed $150,000.) By submission of its offer, the offeror certifies to the best of its knowledge and belief that no Federal appropriated funds have been paid or will be paid to any person for influencing or attempting to influence an officer or employee of any agency, a Member of Congress, an officer or employee of Congress or an employee of a Member of Congress on his or her behalf in connection with the award of any resultant contract. If any registrants under the Lobbying Disclosure Act of 1995 have made a lobbying contact on behalf of the offeror with respect to this contract, the offeror shall complete and submit, with its offer, OMB Standard Form LLL, Disclosure of Lobbying Activities, to provide the name of the registrants. The offeror need not report regularly employed officers or employees of the offeror to whom payments of reasonable compensation were made.</p><p>(f) <i>Buy American Certificate.</i> (Applies only if the clause at Federal Acquisition Regulation (FAR) 52.225-1, Buy American&mdash;Supplies, is included in this solicitation.)</p><p>(1) The offeror certifies that each end product, except those listed in paragraph (f)(2) of this provision, is a domestic end product and that for other than COTS items, the offeror has considered components of unknown origin to have been mined, produced, or manufactured outside the United States. The offeror shall list as foreign end products those end products manufactured in the United States that do not qualify as domestic end products, <i>i.e.</i>, an end product that is not a COTS item and does not meet the component test in paragraph (2) of the definition of âdomestic end product.â The terms âcommercially available off-the-shelf (COTS) item,â âcomponent,â âdomestic end product,â âend product,â âforeign end product,â and âUnited Statesâ are defined in the clause of this solicitation entitled âBuy American&mdash;Supplies.â</p><p>(2) Foreign End Products:</p>Line Item No.: {{textbox}}Country of Origin: {{textbox}}<p>(List as necessary)</p><p>(3) The Government will evaluate offers in accordance with the policies and procedures of FAR Part 25.</p><p>(g)(1) <i>Buy American&mdash;Free Trade Agreements&mdash;Israeli Trade Act Certificate.</i> (Applies only if the clause at FAR 52.225-3, Buy American&mdash;Free Trade Agreements&mdash;Israeli Trade Act, is included in this solicitation.)</p><p>(i) The offeror certifies that each end product, except those listed in paragraph (g)(1)(ii) or (g)(1)(iii) of this provision, is a domestic end product and that for other than COTS items, the offeror has considered components of unknown origin to have been mined, produced, or manufactured outside the United States. The terms âBahrainian, Moroccan, Omani, Panamanian, or Peruvian end product,â âcommercially available off-the-shelf (COTS) item,â âcomponent,â âdomestic end product,â âend product,â âforeign end product,â âFree Trade Agreement country,â âFree Trade Agreement country end product,â âIsraeli end product,â and âUnited Statesâ are defined in the clause of this solicitation entitled âBuy American&mdash;Free Trade Agreements&mdash;Israeli Trade Act.â</p><p>(ii) The offeror certifies that the following supplies are Free Trade Agreement country end products (other than Bahrainian, Moroccan, Omani, Panamanian, or Peruvian end products) or Israeli end products as defined in the clause of this solicitation entitled âBuy American&mdash;Free Trade Agreements&mdash;Israeli Trade Actâ:</p><p>Free Trade Agreement Country End Products (Other than Bahrainian, Moroccan, Omani, Panamanian, or Peruvian End Products) or Israeli End Products:</p>\n<div><div><table border="&quot;1&quot;" cellpadding="&quot;1&quot;" cellspacing="&quot;1&quot;" frame="&quot;void&quot;" width="&quot;100%&quot;"><tbody><tr><th scope="&quot;col&quot;">Line Item No.</th><th scope="&quot;col&quot;">Â&nbsp;Â&nbsp;</th><th scope="&quot;col&quot;">Country of Origin</th></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">Â&nbsp;Â&nbsp;</td><td align="&quot;right&quot;"></td><td align="&quot;left&quot;"></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">Â&nbsp;Â&nbsp;</td><td align="&quot;right&quot;"></td><td align="&quot;left&quot;"></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">Â&nbsp;Â&nbsp;</td><td align="&quot;right&quot;"></td><td align="&quot;left&quot;"></td></tr><tr><td align="&quot;center&quot;" colspan="&quot;3&quot;" scope="&quot;row&quot;">[List as necessary]</td></tr></tbody></table></div></div><p>(iii) The offeror shall list those supplies that are foreign end products (other than those listed in paragraph (g)(1)(ii) of this provision) as defined in the clause of this solicitation entitled âBuy American&mdash;Free Trade Agreements&mdash;Israeli Trade Act.â The offeror shall list as other foreign end products those end products manufactured in the United States that do not qualify as domestic end products, <i>i.e.</i>, an end product that is not a COTS item and does not meet the component test in paragraph (2) of the definition of âdomestic end product.â</p>\n<h2>Other Foreign End Products</h2>\nLine Item No.: {{textbox}}Country of Origin: {{textbox}}<p>(List as necessary)</p><p>(iv) The Government will evaluate offers in accordance with the policies and procedures of FAR Part 25.</p><p>(2) <i>Buy American&mdash;Free Trade Agreements&mdash;Israeli Trade Act Certificate, Alternate I.</i> If <i>Alternate I</i> to the clause at FAR 52.225-3 is included in this solicitation, substitute the following paragraph (g)(1)(ii) for paragraph (g)(1)(ii) of the basic provision: </p><p>(g)(1)(ii) The offeror certifies that the following supplies are Canadian end products as defined in the clause of this solicitation entitled âBuy American&mdash;Free Trade Agreements&mdash;Israeli Trade Actâ: </p>\n<h3>Canadian End Products: </h3>\n<h3>Line Item No. </h3>\nÂ&nbsp; {{textbox}}Â&nbsp; {{textbox}}Â&nbsp; {{textbox}}<p>$(<i>List as necessary</i>) </p><p>(3) <i>Buy American&mdash;Free Trade Agreements&mdash;Israeli Trade Act Certificate, Alternate II.</i> If <i>Alternate II</i> to the clause at FAR 52.225-3 is included in this solicitation, substitute the following paragraph (g)(1)(ii) for paragraph (g)(1)(ii) of the basic provision: </p><p>(g)(1)(ii) The offeror certifies that the following supplies are Canadian end products or Israeli end products as defined in the clause of this solicitation entitled âBuy American&mdash;Free Trade Agreements&mdash;Israeli Trade Actâ: </p>\n<h3>Canadian or Israeli End Products: </h3>\n<p>Line Item No. </p>Â&nbsp; {{textbox}}Â&nbsp; {{textbox}}Â&nbsp; {{textbox}}\n<p>Country of Origin </p>Â&nbsp; {{textbox}}Â&nbsp; {{textbox}}Â&nbsp; {{textbox}}<p>$(<i>List as necessary</i>)</p><p>(g)(4) <i>Buy American&mdash;Free Trade Agreements&mdash;Israeli Trade Act Certificate, Alternate III.</i> If Alternate III to the clause at FAR 52.225-3 is included in this solicitation, substitute the following paragraph (g)(1)(ii) for paragraph (g)(1)(ii) of the basic provision:</p><p>(g)(1)(ii) The offeror certifies that the following supplies are Free Trade Agreement country end products (other than Bahrainian, Korean, Moroccan, Omani, Panamanian, or Peruvian end products) or Israeli end products as defined in the clause of this solicitation entitled âBuy American&mdash;Free Trade Agreements&mdash;Israeli Trade Actâ:</p><p>Free Trade Agreement Country End Products (Other than Bahrainian, Korean, Moroccan, Omani, Panamanian, or Peruvian End Products) or Israeli End Products:</p>\n<div><div><table border="&quot;1&quot;" cellpadding="&quot;1&quot;" cellspacing="&quot;1&quot;" frame="&quot;void&quot;" width="&quot;100%&quot;"><tbody><tr><th scope="&quot;col&quot;">Line Item No.</th><th scope="&quot;col&quot;">Â&nbsp;Â&nbsp;</th><th scope="&quot;col&quot;">Country of Origin</th></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">Â&nbsp;Â&nbsp;</td><td align="&quot;right&quot;"></td><td align="&quot;left&quot;"></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">Â&nbsp;Â&nbsp;</td><td align="&quot;right&quot;"></td><td align="&quot;left&quot;"></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">Â&nbsp;Â&nbsp;</td><td align="&quot;right&quot;"></td><td align="&quot;left&quot;"></td></tr><tr><td align="&quot;center&quot;" colspan="&quot;3&quot;" scope="&quot;row&quot;">[List as necessary]</td></tr></tbody></table></div></div><p>(5) <i>Trade Agreements Certificate.</i> (Applies only if the clause at FAR 52.225-5, Trade Agreements, is included in this solicitation.)</p><p>(i) The offeror certifies that each end product, except those listed in paragraph (g)(5)(ii) of this provision, is a U.S.-made or designated country end product, as defined in the clause of this solicitation entitled âTrade Agreementsâ.</p><p>(ii) The offeror shall list as other end products those end products that are not U.S.-made or designated country end products.</p><p>Other End Products:</p>\n<div><div><table border="&quot;1&quot;" cellpadding="&quot;1&quot;" cellspacing="&quot;1&quot;" frame="&quot;void&quot;" width="&quot;100%&quot;"><tbody><tr><th scope="&quot;col&quot;">Line item No.</th><th scope="&quot;col&quot;">Â&nbsp;Â&nbsp;</th><th scope="&quot;col&quot;">Country of origin</th></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">Â&nbsp;Â&nbsp;</td><td align="&quot;right&quot;"></td><td align="&quot;left&quot;"></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">Â&nbsp;Â&nbsp;</td><td align="&quot;right&quot;"></td><td align="&quot;left&quot;"></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">Â&nbsp;Â&nbsp;</td><td align="&quot;right&quot;"></td><td align="&quot;left&quot;"></td></tr><tr><td align="&quot;center&quot;" colspan="&quot;3&quot;" scope="&quot;row&quot;">[List as necessary]</td></tr></tbody></table></div></div><p>(iii) The Government will evaluate offers in accordance with the policies and procedures of FAR Part 25. For line items covered by the WTO GPA, the Government will evaluate offers of U.S.-made or designated country end products without regard to the restrictions of the Buy American statute. The Government will consider for award only offers of U.S.-made or designated country end products unless the Contracting Officer determines that there are no offers for such products or that the offers for such products are insufficient to fulfill the requirements of the solicitation.</p><p>(h) <i>Certification Regarding Responsibility Matters (Executive Order 12689).</i> (Applies only if the contract value is expected to exceed the simplified acquisition threshold.) The offeror certifies, to the best of its knowledge and belief, that the offeror and/or any of its principals&mdash; </p><p>(1) {{checkbox_52.212-3[30]}}} Are, {{checkbox_52.212-3[31]}}} are not presently debarred, suspended, proposed for debarment, or declared ineligible for the award of contracts by any Federal agency;</p><p>(2) {{checkbox_52.212-3[32]}}} Have, {{checkbox_52.212-3[33]}}} have not, within a three-year period preceding this offer, been convicted of or had a civil judgment rendered against them for: Commission of fraud or a criminal offense in connection with obtaining, attempting to obtain, or performing a Federal, state or local government contract or subcontract; violation of Federal or state antitrust statutes relating to the submission of offers; or Commission of embezzlement, theft, forgery, bribery, falsification or destruction of records, making false statements, tax evasion, violating Federal criminal tax laws, or receiving stolen property,</p><p>(3) {{checkbox_52.212-3[34]}}} Are, {{checkbox_52.212-3[35]}}} are not presently indicted for, or otherwise criminally or civilly charged by a Government entity with, commission of any of these offenses enumerated in paragraph (h)(2) of this clause; and</p><p>(i) <i>Certification Regarding Knowledge of Child Labor for Listed End Products (Executive Order 13126). [The Contracting Officer must list in paragraph (i)(1) any end products being acquired under this solicitation that are included in the List of Products Requiring Contractor Certification as to Forced or Indentured Child Labor, unless excluded at 22.1503(b).</i>] </p><p>(1) <i>Listed end products.</i> </p>\n<h3>Listed End Product</h3>\nÂ&nbsp; {{textbox}}Â&nbsp;  {{textbox}}\n<h3>Listed Countries of Origin </h3>\nÂ&nbsp; {{textbox}}Â&nbsp; {{textbox}}<p>(2) <i>Certification.</i> [<i>If the Contracting Officer has identified end products and countries of origin in paragraph (i)(1) of this provision, then the offeror must certify to either (i)(2)(i) or (i)(2)(ii) by checking the appropriate block.</i>]</p><p>{{checkbox_52.212-3[36]}}}Â&nbsp;Â&nbsp;(i) The offeror will not supply any end product listed in paragraph (i)(1) of this provision that was mined, produced, or manufactured in the corresponding country as listed for that product.</p><p>{{checkbox_52.212-3[37]}}}Â&nbsp;Â&nbsp;(ii) The offeror may supply an end product listed in paragraph (i)(1) of this provision that was mined, produced, or manufactured in the corresponding country as listed for that product. The offeror certifies that it has made a good faith effort to determine whether forced or indentured child labor was used to mine, produce, or manufacture any such end product furnished under this contract. On the basis of those efforts, the offeror certifies that it is not aware of any such use of child labor.</p><p>(4) Have,{{checkbox_52.212-3[38]}}}Â&nbsp;Â&nbsp; have not, within a three-year period preceding this offer, been notified of any delinquent Federal taxes in an amount that exceeds $3,000 for which the liability remains unsatisfied.</p><p>(i) Taxes are considered delinquent if both of the following criteria apply:</p><p>(A) <i>The tax liability is finally determined.</i> The liability is finally determined if it has been assessed. A liability is not finally determined if there is a pending administrative or judicial challenge. In the case of a judicial challenge to the liability, the liability is not finally determined until all judicial appeal rights have been exhausted.</p><p>(B) <i>The taxpayer is delinquent in making payment.</i> A taxpayer is delinquent if the taxpayer has failed to pay the tax liability when full payment was due and required. A taxpayer is not delinquent in cases where enforced collection action is precluded.</p><p>(ii) <i>Examples.</i> (A) The taxpayer has received a statutory notice of deficiency, under I.R.C. Â§6212, which entitles the taxpayer to seek Tax Court review of a proposed tax deficiency. This is not a delinquent tax because it is not a final tax liability. Should the taxpayer seek Tax Court review, this will not be a final tax liability until the taxpayer has exercised all judicial appeal rights.</p><p>(B) The IRS has filed a notice of Federal tax lien with respect to an assessed tax liability, and the taxpayer has been issued a notice under I.R.C. Â§6320 entitling the taxpayer to request a hearing with the IRS Office of Appeals contesting the lien filing, and to further appeal to the Tax Court if the IRS determines to sustain the lien filing. In the course of the hearing, the taxpayer is entitled to contest the underlying tax liability because the taxpayer has had no prior opportunity to contest the liability. This is not a delinquent tax because it is not a final tax liability. Should the taxpayer seek tax court review, this will not be a final tax liability until the taxpayer has exercised all judicial appeal rights.</p><p>(C) The taxpayer has entered into an installment agreement pursuant to I.R.C. Â§6159. The taxpayer is making timely payments and is in full compliance with the agreement terms. The taxpayer is not delinquent because the taxpayer is not currently required to make full payment.</p><p>(D) The taxpayer has filed for bankruptcy protection. The taxpayer is not delinquent because enforced collection action is stayed under 11 U.S.C. 362 (the Bankruptcy Code).</p><p>(i) <i>Certification Regarding Knowledge of Child Labor for Listed End Products (Executive Order 13126). [The Contracting Officer must list in paragraph (i)(1) any end products being acquired under this solicitation that are included in the List of Products Requiring Contractor Certification as to Forced or Indentured Child Labor, unless excluded at 22.1503(b).</i>] </p><p>(1) <i>Listed end products.</i> </p>\n<h3>Listed End Product</h3>\nÂ&nbsp; {{textbox}}Â&nbsp;  {{textbox}}\n<h3>Listed Countries of Origin </h3>\nÂ&nbsp; {{textbox}}Â&nbsp; {{textbox}}<p>(2) <i>Certification.</i> [<i>If the Contracting Officer has identified end products and countries of origin in paragraph (i)(1) of this provision, then the offeror must certify to either (i)(2)(i) or (i)(2)(ii) by checking the appropriate block.</i>]</p><p>{{checkbox_52.212-3[39]}}}Â&nbsp;Â&nbsp;(i) The offeror will not supply any end product listed in paragraph (i)(1) of this provision that was mined, produced, or manufactured in the corresponding country as listed for that product.</p><p>{{checkbox_52.212-3[40]}}}Â&nbsp;Â&nbsp;(ii) The offeror may supply an end product listed in paragraph (i)(1) of this provision that was mined, produced, or manufactured in the corresponding country as listed for that product. The offeror certifies that it has made a good faith effort to determine whether forced or indentured child labor was used to mine, produce, or manufacture any such end product furnished under this contract. On the basis of those efforts, the offeror certifies that it is not aware of any such use of child labor.</p><p>(j) <i>Place of manufacture.</i> (Does not apply unless the solicitation is predominantly for the acquisition of manufactured end products.) For statistical purposes only, the offeror shall indicate whether the place of manufacture of the end products it expects to provide in response to this solicitation is predominantly&mdash;</p><p>(1) {{checkbox_52.212-3[41]}}} In the United States (Check this box if the total anticipated price of offered end products manufactured in the United States exceeds the total anticipated price of offered end products manufactured outside the United States); or</p><p>(2) {{checkbox_52.212-3[42]}}} Outside the United States.</p><p>(k) <i>Certificates regarding exemptions from the application of the Service Contract Labor Standards.</i> (Certification by the offeror as to its compliance with respect to the contract also constitutes its certification as to compliance by its subcontractor if it subcontracts out the exempt services.) [<i>The contracting officer is to check a box to indicate if paragraph (k)(1) or (k)(2) applies.</i>]</p><p>(1){{checkbox_52.212-3[43]}}}Â&nbsp;Â&nbsp; Maintenance, calibration, or repair of certain equipment as described in FAR 22.1003-4(c)(1). The offeror {{checkbox_52.212-3[44]}}}Â&nbsp;Â&nbsp;does {{checkbox_52.212-3[45]}}}Â&nbsp;Â&nbsp; does not certify that&mdash;</p><p>(i) The items of equipment to be serviced under this contract are used regularly for other than Governmental purposes and are sold or traded by the offeror (or subcontractor in the case of an exempt subcontract) in substantial quantities to the general public in the course of normal business operations;</p><p>(ii) The services will be furnished at prices which are, or are based on, established catalog or market prices (see FAR 22.1003-4(c)(2)(ii)) for the maintenance, calibration, or repair of such equipment; and</p><p>(iii) The compensation (wage and fringe benefits) plan for all service employees performing work under the contract will be the same as that used for these employees and equivalent employees servicing the same equipment of commercial customers.</p><p>(2){{checkbox_52.212-3[46]}}}Â&nbsp;Â&nbsp;Certain services as described in FAR 22.1003-4(d)(1). The offeror {{checkbox_52.212-3[47]}}}Â&nbsp;Â&nbsp;does {{checkbox_52.212-3[48]}}}Â&nbsp;Â&nbsp;does not certify that&mdash;</p><p>(i) The services under the contract are offered and sold regularly to non-Governmental customers, and are provided by the offeror (or subcontractor in the case of an exempt subcontract) to the general public in substantial quantities in the course of normal business operations;</p><p>(ii) The contract services will be furnished at prices that are, or are based on, established catalog or market prices (see FAR 22.1003-4(d)(2)(iii));</p><p>(iii) Each service employee who will perform the services under the contract will spend only a small portion of his or her time (a monthly average of less than 20 percent of the available hours on an annualized basis, or less than 20 percent of available hours during the contract period if the contract period is less than a month) servicing the Government contract; and</p><p>(iv) The compensation (wage and fringe benefits) plan for all service employees performing work under the contract is the same as that used for these employees and equivalent employees servicing commercial customers.</p><p>(3) If paragraph (k)(1) or (k)(2) of this clause applies&mdash;</p><p>(i) If the offeror does not certify to the conditions in paragraph (k)(1) or (k)(2) and the Contracting Officer did not attach a Service Contract Labor Standards wage determination to the solicitation, the offeror shall notify the Contracting Officer as soon as possible; and</p><p>(ii) The Contracting Officer may not make an award to the offeror if the offeror fails to execute the certification in paragraph (k)(1) or (k)(2) of this clause or to contact the Contracting Officer as required in paragraph (k)(3)(i) of this clause.</p><p>(l) <i>Taxpayer Identification Number (TIN) (26 U.S.C. 6109, 31 U.S.C. 7701).</i> (Not applicable if the offeror is required to provide this information to the SAM database to be eligible for award.)</p><p>(1) All offerors must submit the information required in paragraphs (l)(3) through (l)(5) of this provision to comply with debt collection requirements of 31 U.S.C. 7701(c) and 3325(d), reporting requirements of 26 U.S.C. 6041, 6041A, and 6050M, and implementing regulations issued by the Internal Revenue Service (IRS).</p><p>(2) The TIN may be used by the Government to collect and report on any delinquent amounts arising out of the offeror''s relationship with the Government (31 U.S.C. 7701(c)(3)). If the resulting contract is subject to the payment reporting requirements described in FAR 4.904, the TIN provided hereunder may be matched with IRS records to verify the accuracy of the offeror''s TIN.</p><p>(3) <i>Taxpayer Identification Number (TIN).</i></p><p>{{checkbox_52.212-3[49]}}}Â&nbsp;Â&nbsp;TIN: {{textbox}}.</p><p>{{checkbox_52.212-3[50]}}}Â&nbsp;Â&nbsp;TIN has been applied for.</p><p>{{checkbox_52.212-3[51]}}}Â&nbsp;Â&nbsp;TIN is not required because:</p><p>{{checkbox_52.212-3[52]}}}Â&nbsp;Â&nbsp;Offeror is a nonresident alien, foreign corporation, or foreign partnership that does not have income effectively connected with the conduct of a trade or business in the United States and does not have an office or place of business or a fiscal paying agent in the United States;</p><p>{{checkbox_52.212-3[53]}}}Â&nbsp;Â&nbsp;Offeror is an agency or instrumentality of a foreign government;</p><p>{{checkbox_52.212-3[54]}}}Â&nbsp;Â&nbsp;Offeror is an agency or instrumentality of the Federal Government.</p><p>(4) <i>Type of organization.</i></p><p>{{checkbox_52.212-3[55]}}}Â&nbsp;Â&nbsp;Sole proprietorship;</p><p>{{checkbox_52.212-3[56]}}}Â&nbsp;Â&nbsp;Partnership;</p><p>{{checkbox_52.212-3[57]}}}Â&nbsp;Â&nbsp;Corporate entity (not tax-exempt);</p><p>{{checkbox_52.212-3[58]}}}Â&nbsp;Â&nbsp;Corporate entity (tax-exempt);</p><p>{{checkbox_52.212-3[59]}}}Â&nbsp;Â&nbsp;Government entity (Federal, State, or local);</p><p>{{checkbox_52.212-3[60]}}}Â&nbsp;Â&nbsp;Foreign government;</p><p>{{checkbox_52.212-3[61]}}}Â&nbsp;Â&nbsp;International organization per 26 CFR 1.6049-4;</p><p>{{checkbox_52.212-3[62]}}}Â&nbsp;Â&nbsp;Other {{textbox}}.</p><p>(5) <i>Common parent.</i></p><p>{{checkbox_52.212-3[63]}}}Â&nbsp;Â&nbsp;Offeror is not owned or controlled by a common parent;</p><p>{{checkbox_52.212-3[64]}}}Â&nbsp;Â&nbsp;Name and TIN of common parent:</p><p>Name {{textbox}}.</p><p>TIN {{textbox}}.</p><p>(m) <i>Restricted business operations in Sudan.</i> By submission of its offer, the offeror certifies that the offeror does not conduct any restricted business operations in Sudan.</p><p>(n) <i>Prohibition on Contracting with Inverted Domestic Corporations.</i> (1) Government agencies are not permitted to use appropriated (or otherwise made available) funds for contracts with either an inverted domestic corporation, or a subsidiary of an inverted domestic corporation, unless the exception at 9.108-2(b) applies or the requirement is waived in accordance with the procedures at 9.108-4.</p><p>(2) <i>Representation.</i> By submission of its offer, the offeror represents that&mdash;</p><p>(i) It is not an inverted domestic corporation; and</p><p>(ii) It is not a subsidiary of an inverted domestic corporation.</p><p>(o) <i>Prohibition on contracting with entities engaging in certain activities or transactions relating to Iran.</i> (1) The offeror shall email questions concerning sensitive technology to the Department of State at CISADA106@state.gov.</p><p>(2) <i>Representation and certifications.</i> Unless a waiver is granted or an exception applies as provided in paragraph (o)(3) of this provision, by submission of its offer, the offeror&mdash;</p><p>(i) Represents, to the best of its knowledge and belief, that the offeror does not export any sensitive technology to the government of Iran or any entities or individuals owned or controlled by, or acting on behalf or at the direction of, the government of Iran;</p><p>(ii) Certifies that the offeror, or any person owned or controlled by the offeror, does not engage in any activities for which sanctions may be imposed under section 5 of the Iran Sanctions Act; and</p><p>(iii) Certifies that the offeror, and any person owned or controlled by the offeror, does not knowingly engage in any transaction that exceeds $3,000 with Iran''s Revolutionary Guard Corps or any of its officials, agents, or affiliates, the property and interests in property of which are blocked pursuant to the International Emergency Economic Powers Act (50 U.S.C. 1701 <i>et seq.</i>) (see OFAC''s Specially Designated Nationals and Blocked Persons List at <i>http://www.treasury.gov/ofac/downloads/t11sdn.pdf</i>).</p><p>(3) The representation and certification requirements of paragraph (o)(2) of this provision do not apply if&mdash;</p><p>(i) This solicitation includes a trade agreements certification (<i>e.g.,</i> 52.212-3(g) or a comparable agency provision); and</p><p>(ii) The offeror has certified that all the offered products to be supplied are designated country end products.</p>'
WHERE clause_id = 72183; -- 52.212-3

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1212_64&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1212_64&rgn=div8'
WHERE clause_id = 72186; -- 52.212-4

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1212_65&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1212_65&rgn=div8'
WHERE clause_id = 72189; -- 52.212-5

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1213_61&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1213_61&rgn=div8'
WHERE clause_id = 72190; -- 52.213-1

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1213_62&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1213_62&rgn=div8'
WHERE clause_id = 72191; -- 52.213-2

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1213_63&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1213_63&rgn=div8'
WHERE clause_id = 72192; -- 52.213-3

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1213_64&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1213_64&rgn=div8'
WHERE clause_id = 72193; -- 52.213-4

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1214_610&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1214_610&rgn=div8'
WHERE clause_id = 72194; -- 52.214-10

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1214_612&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1214_612&rgn=div8'
WHERE clause_id = 72195; -- 52.214-12

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1214_613&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1214_613&rgn=div8'
WHERE clause_id = 72197; -- 52.214-13

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?node=se48.2.52_1214_614&amp;rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=31961bd8c221b0a8c46e7a6af888422a&amp;node=se48.2.52_1214_614&amp;rgn=div8'
, clause_data = '<p>(a) The bidder, in the performance of any contract resulting from this solicitation, {{checkbox_52.214-14[0]}}} intends, {{checkbox_52.214-14[1]}}} does not intend [<i>check applicable box</i>] to use one or more plants or facilities located at a different address from the address of the bidder as indicated in this bid.</p><p>(b) If the bidder checks <i>intends</i> in paragraph (a) above, it shall insert in the spaces provided below the required information:</p><div><table border="&quot;1&quot;" cellpadding="&quot;1&quot;" cellspacing="&quot;1&quot;" frame="&quot;void&quot;" width="&quot;100%&quot;"><tbody><tr><th scope="&quot;col&quot;">Place of Performance (Street Address, City, County, State, Zip Code)</th><th scope="&quot;col&quot;">Name and Address of Owner and Operator of the Plant or Facility if Other than Bidder</th><th scope="&quot;col&quot;">&nbsp;&nbsp;</th></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">&nbsp;&nbsp;&nbsp;</td><td align="&quot;left&quot;"></td><td align="&quot;left&quot;"></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">&nbsp;&nbsp;&nbsp;</td><td align="&quot;left&quot;"></td><td align="&quot;left&quot;"></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">&nbsp;&nbsp;&nbsp;</td><td align="&quot;left&quot;"></td><td align="&quot;left&quot;"></td></tr></tbody></table></div>'
--               '<p>(a) The bidder, in the performance of any contract resulting from this solicitation, {{checkbox_52.214-14[0]}}} intends, {{checkbox_52.214-14[1]}}} does not intend [<i>check applicable box</i>] to use one or more plants or facilities located at a different address from the address of the bidder as indicated in this bid.</p><p>(b) If the bidder checks <i>intends</i> in paragraph (a) above, it shall insert in the spaces provided below the required information:</p><div><table border="&quot;1&quot;" cellpadding="&quot;1&quot;" cellspacing="&quot;1&quot;" frame="&quot;void&quot;" width="&quot;100%&quot;"><tbody><tr><th scope="&quot;col&quot;">Place of Performance (Street Address, City, County, State, Zip Code)</th><th scope="&quot;col&quot;">Name and Address of Owner and Operator of the Plant or Facility if Other than Bidder</th><th scope="&quot;col&quot;">Â&nbsp;Â&nbsp;</th></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">Â&nbsp;Â&nbsp;Â&nbsp;</td><td align="&quot;left&quot;"></td><td align="&quot;left&quot;"></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">Â&nbsp;Â&nbsp;Â&nbsp;</td><td align="&quot;left&quot;"></td><td align="&quot;left&quot;"></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">Â&nbsp;Â&nbsp;Â&nbsp;</td><td align="&quot;left&quot;"></td><td align="&quot;left&quot;"></td></tr></tbody></table></div>'
WHERE clause_id = 72198; -- 52.214-14

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1214_615&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1214_615&rgn=div8'
WHERE clause_id = 72199; -- 52.214-15

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1214_616&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1214_616&rgn=div8'
WHERE clause_id = 72200; -- 52.214-16

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1214_618&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1214_618&rgn=div8'
WHERE clause_id = 72201; -- 52.214-18

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1214_619&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1214_619&rgn=div8'
WHERE clause_id = 72202; -- 52.214-19

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1214_620&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1214_620&rgn=div8'
WHERE clause_id = 72205; -- 52.214-20

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1214_621&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1214_621&rgn=div8'
WHERE clause_id = 72207; -- 52.214-21

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1214_622&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1214_622&rgn=div8'
WHERE clause_id = 72208; -- 52.214-22

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1214_623&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1214_623&rgn=div8'
WHERE clause_id = 72209; -- 52.214-23

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1214_624&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1214_624&rgn=div8'
WHERE clause_id = 72210; -- 52.214-24

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1214_625&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1214_625&rgn=div8'
WHERE clause_id = 72211; -- 52.214-25

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1214_626&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1214_626&rgn=div8'
WHERE clause_id = 72213; -- 52.214-26

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1214_627&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1214_627&rgn=div8'
WHERE clause_id = 72214; -- 52.214-27

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1214_628&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1214_628&rgn=div8'
WHERE clause_id = 72215; -- 52.214-28

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1214_629&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1214_629&rgn=div8'
WHERE clause_id = 72216; -- 52.214-29

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1214_63&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1214_63&rgn=div8'
WHERE clause_id = 72217; -- 52.214-3

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1214_631&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1214_631&rgn=div8'
WHERE clause_id = 72218; -- 52.214-31

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1214_634&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1214_634&rgn=div8'
WHERE clause_id = 72219; -- 52.214-34

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1214_635&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1214_635&rgn=div8'
WHERE clause_id = 72220; -- 52.214-35

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1214_64&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1214_64&rgn=div8'
WHERE clause_id = 72221; -- 52.214-4

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1214_65&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1214_65&rgn=div8'
WHERE clause_id = 72222; -- 52.214-5

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1214_66&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1214_66&rgn=div8'
WHERE clause_id = 72223; -- 52.214-6

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1214_67&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1214_67&rgn=div8'
WHERE clause_id = 72224; -- 52.214-7

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1215_61&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1215_61&rgn=div8'
WHERE clause_id = 72227; -- 52.215-1

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1215_610&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1215_610&rgn=div8'
WHERE clause_id = 72228; -- 52.215-10

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1215_611&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1215_611&rgn=div8'
WHERE clause_id = 72229; -- 52.215-11

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1215_612&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1215_612&rgn=div8'
WHERE clause_id = 72230; -- 52.215-12

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1215_613&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1215_613&rgn=div8'
WHERE clause_id = 72231; -- 52.215-13

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1215_614&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1215_614&rgn=div8'
WHERE clause_id = 72233; -- 52.215-14

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1215_615&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1215_615&rgn=div8'
WHERE clause_id = 72234; -- 52.215-15

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1215_616&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1215_616&rgn=div8'
WHERE clause_id = 72235; -- 52.215-16

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1215_617&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1215_617&rgn=div8'
WHERE clause_id = 72236; -- 52.215-17

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1215_618&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1215_618&rgn=div8'
WHERE clause_id = 72237; -- 52.215-18

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1215_619&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1215_619&rgn=div8'
WHERE clause_id = 72238; -- 52.215-19

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1215_62&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1215_62&rgn=div8'
WHERE clause_id = 72242; -- 52.215-2

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1215_620&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1215_620&rgn=div8'
WHERE clause_id = 72247; -- 52.215-20

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1215_621&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1215_621&rgn=div8'
WHERE clause_id = 72252; -- 52.215-21

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1215_622&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1215_622&rgn=div8'
WHERE clause_id = 72253; -- 52.215-22

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1215_623&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1215_623&rgn=div8'
WHERE clause_id = 72255; -- 52.215-23

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1215_63&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1215_63&rgn=div8'
WHERE clause_id = 72256; -- 52.215-3

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1215_65&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1215_65&rgn=div8'
WHERE clause_id = 72257; -- 52.215-5

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?node=se48.2.52_1215_66&amp;rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=31961bd8c221b0a8c46e7a6af888422a&amp;node=se48.2.52_1215_66&amp;rgn=div8'
, clause_data = '<p>(a) The offeror or respondent, in the performance of any contract resulting from this solicitation, {{checkbox_52.215-6[0]}}}&nbsp;&nbsp;intends, {{checkbox_52.215-6[1]}}}&nbsp;&nbsp;does not intend [<i>check applicable block</i>] to use one or more plants or facilities located at a different address from the address of the offeror or respondent as indicated in this proposal or response to request for information.</p><p>(b) If the offeror or respondent checks &ldquo;??intends&ldquo;? in paragraph (a) of this provision, it shall insert in the following spaces the required information:</p><div><table border="&quot;1&quot;" cellpadding="&quot;1&quot;" cellspacing="&quot;1&quot;" frame="&quot;void&quot;" width="&quot;100%&quot;"><tbody><tr><th scope="&quot;col&quot;">Place of performance (street address, city, state, county, zip code)</th><th scope="&quot;col&quot;">Name and address of owner and operator of the plant or facility if other than offeror or respondent</th></tr><tr><td align="&quot;right&quot;" scope="&quot;row&quot;">&nbsp;&nbsp;&nbsp;</td><td align="&quot;right&quot;"></td></tr><tr><td align="&quot;right&quot;" scope="&quot;row&quot;">&nbsp;&nbsp;&nbsp;</td><td align="&quot;right&quot;"></td></tr></tbody></table></div>'
--               '<p>(a) The offeror or respondent, in the performance of any contract resulting from this solicitation, {{checkbox_52.215-6[0]}}}Â&nbsp;Â&nbsp;intends, {{checkbox_52.215-6[1]}}}Â&nbsp;Â&nbsp;does not intend [<i>check applicable block</i>] to use one or more plants or facilities located at a different address from the address of the offeror or respondent as indicated in this proposal or response to request for information.</p><p>(b) If the offeror or respondent checks &ldquo;??intends&ldquo;? in paragraph (a) of this provision, it shall insert in the following spaces the required information:</p><div><table border="&quot;1&quot;" cellpadding="&quot;1&quot;" cellspacing="&quot;1&quot;" frame="&quot;void&quot;" width="&quot;100%&quot;"><tbody><tr><th scope="&quot;col&quot;">Place of performance (street address, city, state, county, zip code)</th><th scope="&quot;col&quot;">Name and address of owner and operator of the plant or facility if other than offeror or respondent</th></tr><tr><td align="&quot;right&quot;" scope="&quot;row&quot;">Â&nbsp;Â&nbsp;Â&nbsp;</td><td align="&quot;right&quot;"></td></tr><tr><td align="&quot;right&quot;" scope="&quot;row&quot;">Â&nbsp;Â&nbsp;Â&nbsp;</td><td align="&quot;right&quot;"></td></tr></tbody></table></div>'
WHERE clause_id = 72258; -- 52.215-6

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1215_68&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1215_68&rgn=div8'
WHERE clause_id = 72259; -- 52.215-8

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1215_69&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1215_69&rgn=div8'
WHERE clause_id = 72262; -- 52.215-9

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1216_61&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1216_61&rgn=div8'
WHERE clause_id = 72263; -- 52.216-1

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1216_610&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1216_610&rgn=div8'
WHERE clause_id = 72264; -- 52.216-10

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1216_611&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1216_611&rgn=div8'
WHERE clause_id = 72266; -- 52.216-11

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1216_612&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1216_612&rgn=div8'
WHERE clause_id = 72268; -- 52.216-12

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1216_615&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1216_615&rgn=div8'
WHERE clause_id = 72269; -- 52.216-15

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1216_616&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1216_616&rgn=div8'
WHERE clause_id = 72271; -- 52.216-16

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1216_617&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1216_617&rgn=div8'
WHERE clause_id = 72273; -- 52.216-17

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1216_618&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1216_618&rgn=div8'
WHERE clause_id = 72274; -- 52.216-18

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1216_619&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1216_619&rgn=div8'
WHERE clause_id = 72275; -- 52.216-19

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1216_62&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1216_62&rgn=div8'
WHERE clause_id = 72276; -- 52.216-2

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1216_620&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1216_620&rgn=div8'
WHERE clause_id = 72277; -- 52.216-20

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1216_621&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1216_621&rgn=div8'
WHERE clause_id = 72282; -- 52.216-21

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1216_622&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1216_622&rgn=div8'
WHERE clause_id = 72283; -- 52.216-22

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1216_623&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1216_623&rgn=div8'
WHERE clause_id = 72284; -- 52.216-23

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1216_624&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1216_624&rgn=div8'
WHERE clause_id = 72285; -- 52.216-24

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1216_625&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1216_625&rgn=div8'
WHERE clause_id = 72287; -- 52.216-25

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1216_626&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1216_626&rgn=div8'
WHERE clause_id = 72288; -- 52.216-26

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1216_627&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1216_627&rgn=div8'
WHERE clause_id = 72289; -- 52.216-27

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1216_628&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1216_628&rgn=div8'
WHERE clause_id = 72290; -- 52.216-28

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1216_629&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1216_629&rgn=div8'
WHERE clause_id = 72291; -- 52.216-29

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1216_63&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1216_63&rgn=div8'
WHERE clause_id = 72292; -- 52.216-3

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1216_630&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1216_630&rgn=div8'
WHERE clause_id = 72293; -- 52.216-30

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1216_631&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1216_631&rgn=div8'
WHERE clause_id = 72294; -- 52.216-31

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1216_64&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1216_64&rgn=div8'
WHERE clause_id = 72295; -- 52.216-4

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1216_65&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1216_65&rgn=div8'
WHERE clause_id = 72296; -- 52.216-5

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1216_66&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1216_66&rgn=div8'
WHERE clause_id = 72297; -- 52.216-6

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1216_67&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1216_67&rgn=div8'
WHERE clause_id = 72298; -- 52.216-7

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1216_68&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1216_68&rgn=div8'
WHERE clause_id = 72303; -- 52.216-8

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1216_69&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1216_69&rgn=div8'
WHERE clause_id = 72304; -- 52.216-9

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1217_62&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1217_62&rgn=div8'
WHERE clause_id = 72305; -- 52.217-2

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1217_63&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1217_63&rgn=div8'
WHERE clause_id = 72306; -- 52.217-3

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1217_64&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1217_64&rgn=div8'
WHERE clause_id = 72307; -- 52.217-4

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1217_65&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1217_65&rgn=div8'
WHERE clause_id = 72308; -- 52.217-5

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1217_66&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1217_66&rgn=div8'
WHERE clause_id = 72309; -- 52.217-6

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1217_67&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1217_67&rgn=div8'
WHERE clause_id = 72310; -- 52.217-7

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1217_68&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1217_68&rgn=div8'
WHERE clause_id = 72311; -- 52.217-8

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1217_69&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1217_69&rgn=div8'
WHERE clause_id = 72312; -- 52.217-9

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1219_61&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1219_61&rgn=div8'
WHERE clause_id = 72314; -- 52.219-1

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1219_610&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1219_610&rgn=div8'
WHERE clause_id = 72315; -- 52.219-10

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1219_611&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1219_611&rgn=div8'
WHERE clause_id = 72316; -- 52.219-11

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1219_612&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1219_612&rgn=div8'
WHERE clause_id = 72317; -- 52.219-12

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1219_613&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1219_613&rgn=div8'
WHERE clause_id = 72318; -- 52.219-13

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1219_614&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1219_614&rgn=div8'
WHERE clause_id = 72319; -- 52.219-14

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1219_616&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1219_616&rgn=div8'
WHERE clause_id = 72320; -- 52.219-16

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1219_617&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1219_617&rgn=div8'
WHERE clause_id = 72321; -- 52.219-17

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1219_618&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1219_618&rgn=div8'
WHERE clause_id = 72324; -- 52.219-18

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1219_62&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1219_62&rgn=div8'
WHERE clause_id = 72325; -- 52.219-2

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1219_627&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1219_627&rgn=div8'
WHERE clause_id = 72326; -- 52.219-27

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1219_628&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1219_628&rgn=div8'
WHERE clause_id = 72327; -- 52.219-28

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1219_629&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1219_629&rgn=div8'
WHERE clause_id = 72328; -- 52.219-29

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1219_63&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1219_63&rgn=div8'
WHERE clause_id = 72330; -- 52.219-3

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1219_630&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1219_630&rgn=div8'
WHERE clause_id = 72331; -- 52.219-30

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1219_64&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1219_64&rgn=div8'
WHERE clause_id = 72333; -- 52.219-4

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1219_66&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1219_66&rgn=div8'
WHERE clause_id = 72334; -- 52.219-6

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1219_67&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1219_67&rgn=div8'
WHERE clause_id = 72339; -- 52.219-7

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1219_68&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1219_68&rgn=div8'
WHERE clause_id = 72340; -- 52.219-8

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1219_69&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1219_69&rgn=div8'
WHERE clause_id = 72344; -- 52.219-9

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1222_61&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1222_61&rgn=div8'
WHERE clause_id = 72345; -- 52.222-1

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1222_610&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1222_610&rgn=div8'
WHERE clause_id = 72346; -- 52.222-10

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1222_611&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1222_611&rgn=div8'
WHERE clause_id = 72347; -- 52.222-11

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1222_612&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1222_612&rgn=div8'
WHERE clause_id = 72348; -- 52.222-12

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1222_613&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1222_613&rgn=div8'
WHERE clause_id = 72349; -- 52.222-13

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1222_614&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1222_614&rgn=div8'
WHERE clause_id = 72350; -- 52.222-14

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1222_615&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1222_615&rgn=div8'
WHERE clause_id = 72351; -- 52.222-15

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1222_616&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1222_616&rgn=div8'
WHERE clause_id = 72352; -- 52.222-16

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1222_617&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1222_617&rgn=div8'
WHERE clause_id = 72353; -- 52.222-17

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1222_618&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1222_618&rgn=div8'
WHERE clause_id = 72354; -- 52.222-18

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1222_619&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1222_619&rgn=div8'
WHERE clause_id = 72355; -- 52.222-19

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1222_62&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1222_62&rgn=div8'
WHERE clause_id = 72356; -- 52.222-2

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1222_620&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1222_620&rgn=div8'
WHERE clause_id = 72357; -- 52.222-20

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1222_621&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1222_621&rgn=div8'
WHERE clause_id = 72358; -- 52.222-21

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1222_622&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1222_622&rgn=div8'
WHERE clause_id = 72359; -- 52.222-22

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?node=se48.2.52_1222_623&amp;rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=1e45c9c95cc0fa5020d0a83c2da53fb4&amp;node=se48.2.52_1222_623&amp;rgn=div8'
WHERE clause_id = 72360; -- 52.222-23

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1222_624&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1222_624&rgn=div8'
WHERE clause_id = 72361; -- 52.222-24

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1222_625&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1222_625&rgn=div8'
WHERE clause_id = 72362; -- 52.222-25

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1222_626&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1222_626&rgn=div8'
WHERE clause_id = 72364; -- 52.222-26

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1222_627&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1222_627&rgn=div8'
WHERE clause_id = 72365; -- 52.222-27

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1222_629&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1222_629&rgn=div8'
WHERE clause_id = 72366; -- 52.222-29

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1222_63&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1222_63&rgn=div8'
WHERE clause_id = 72367; -- 52.222-3

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1222_630&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1222_630&rgn=div8'
WHERE clause_id = 72368; -- 52.222-30

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1222_631&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1222_631&rgn=div8'
WHERE clause_id = 72369; -- 52.222-31

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1222_632&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1222_632&rgn=div8'
WHERE clause_id = 72370; -- 52.222-32

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1222_633&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1222_633&rgn=div8'
WHERE clause_id = 72374; -- 52.222-33

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1222_634&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1222_634&rgn=div8'
WHERE clause_id = 72375; -- 52.222-34

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1222_635&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1222_635&rgn=div8'
WHERE clause_id = 72378; -- 52.222-35

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1222_636&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1222_636&rgn=div8'
WHERE clause_id = 72380; -- 52.222-36

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1222_637&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1222_637&rgn=div8'
WHERE clause_id = 72381; -- 52.222-37

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1222_638&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1222_638&rgn=div8'
WHERE clause_id = 72382; -- 52.222-38

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1222_64&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1222_64&rgn=div8'
WHERE clause_id = 72383; -- 52.222-4

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1222_640&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1222_640&rgn=div8'
WHERE clause_id = 72384; -- 52.222-40

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1222_641&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1222_641&rgn=div8'
WHERE clause_id = 72385; -- 52.222-41

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?node=se48.2.52_1222_642&amp;rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=31961bd8c221b0a8c46e7a6af888422a&amp;node=se48.2.52_1222_642&amp;rgn=div8'
WHERE clause_id = 72386; -- 52.222-42

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1222_643&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1222_643&rgn=div8'
WHERE clause_id = 72387; -- 52.222-43

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1222_644&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1222_644&rgn=div8'
WHERE clause_id = 72388; -- 52.222-44

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1222_646&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1222_646&rgn=div8'
WHERE clause_id = 72389; -- 52.222-46

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1222_648&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1222_648&rgn=div8'
WHERE clause_id = 72390; -- 52.222-48

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1222_649&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1222_649&rgn=div8'
WHERE clause_id = 72391; -- 52.222-49

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1222_65&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1222_65&rgn=div8'
WHERE clause_id = 72392; -- 52.222-5

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1222_650&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1222_650&rgn=div8'
WHERE clause_id = 72394; -- 52.222-50

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1222_651&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1222_651&rgn=div8'
WHERE clause_id = 72395; -- 52.222-51

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1222_652&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1222_652&rgn=div8'
WHERE clause_id = 72396; -- 52.222-52

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1222_653&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1222_653&rgn=div8'
WHERE clause_id = 72397; -- 52.222-53

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1222_654&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1222_654&rgn=div8'
WHERE clause_id = 72398; -- 52.222-54

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1222_656&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1222_656&rgn=div8'
WHERE clause_id = 72400; -- 52.222-56

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1222_66&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1222_66&rgn=div8'
WHERE clause_id = 72401; -- 52.222-6

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1222_67&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1222_67&rgn=div8'
WHERE clause_id = 72402; -- 52.222-7

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1222_68&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1222_68&rgn=div8'
WHERE clause_id = 72403; -- 52.222-8

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1222_69&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1222_69&rgn=div8'
WHERE clause_id = 72404; -- 52.222-9

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1223_61&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1223_61&rgn=div8'
WHERE clause_id = 72405; -- 52.223-1

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1223_610&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1223_610&rgn=div8'
WHERE clause_id = 72406; -- 52.223-10

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1223_611&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1223_611&rgn=div8'
WHERE clause_id = 72407; -- 52.223-11

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1223_612&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1223_612&rgn=div8'
WHERE clause_id = 72408; -- 52.223-12

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1223_613&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1223_613&rgn=div8'
WHERE clause_id = 72409; -- 52.223-13

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1223_614&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1223_614&rgn=div8'
WHERE clause_id = 72411; -- 52.223-14

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1223_615&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1223_615&rgn=div8'
WHERE clause_id = 72413; -- 52.223-15

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1223_616&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1223_616&rgn=div8'
WHERE clause_id = 72415; -- 52.223-16

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1223_617&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1223_617&rgn=div8'
WHERE clause_id = 72416; -- 52.223-17

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1223_618&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1223_618&rgn=div8'
WHERE clause_id = 72417; -- 52.223-18

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1223_619&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1223_619&rgn=div8'
WHERE clause_id = 72418; -- 52.223-19

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1223_62&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1223_62&rgn=div8'
WHERE clause_id = 72419; -- 52.223-2

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1223_63&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1223_63&rgn=div8'
WHERE clause_id = 72421; -- 52.223-3

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1223_64&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1223_64&rgn=div8'
WHERE clause_id = 72422; -- 52.223-4

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1223_65&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1223_65&rgn=div8'
WHERE clause_id = 72425; -- 52.223-5

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1223_66&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1223_66&rgn=div8'
WHERE clause_id = 72426; -- 52.223-6

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1223_67&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1223_67&rgn=div8'
WHERE clause_id = 72427; -- 52.223-7

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1223_69&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1223_69&rgn=div8'
WHERE clause_id = 72428; -- 52.223-9

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1224_61&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1224_61&rgn=div8'
WHERE clause_id = 72429; -- 52.224-1

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1224_62&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1224_62&rgn=div8'
WHERE clause_id = 72430; -- 52.224-2

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1225_61&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1225_61&rgn=div8'
WHERE clause_id = 72431; -- 52.225-1

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1225_610&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1225_610&rgn=div8'
WHERE clause_id = 72433; -- 52.225-10

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?node=se48.2.52_1225_611&amp;rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=31961bd8c221b0a8c46e7a6af888422a&amp;node=se48.2.52_1225_611&amp;rgn=div8'
WHERE clause_id = 72435; -- 52.225-11

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1225_612&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1225_612&rgn=div8'
WHERE clause_id = 72438; -- 52.225-12

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1225_613&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1225_613&rgn=div8'
WHERE clause_id = 72439; -- 52.225-13

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1225_614&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1225_614&rgn=div8'
WHERE clause_id = 72440; -- 52.225-14

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1225_617&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1225_617&rgn=div8'
WHERE clause_id = 72441; -- 52.225-17

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1225_618&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1225_618&rgn=div8'
WHERE clause_id = 72442; -- 52.225-18

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1225_619&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1225_619&rgn=div8'
WHERE clause_id = 72443; -- 52.225-19

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1225_62&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1225_62&rgn=div8'
WHERE clause_id = 72444; -- 52.225-2

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1225_620&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1225_620&rgn=div8'
WHERE clause_id = 72445; -- 52.225-20

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?node=se48.2.52_1225_621&amp;rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=1e45c9c95cc0fa5020d0a83c2da53fb4&amp;node=se48.2.52_1225_621&amp;rgn=div8'
, clause_data = '<p>(a) <i>Definitions.</i> As used in this clause&mdash;</p><p><i>Component</i> means an article, material, or supply incorporated directly into a construction material.</p><p><i>Construction material</i> means an article, material, or supply brought to the construction site by the Contractor or a subcontractor for incorporation into the building or work. The term also includes an item brought to the site preassembled from articles, materials, or supplies. However, emergency life safety systems, such as emergency lighting, fire alarm, and audio evacuation systems, that are discrete systems incorporated into a public building or work and that are produced as complete systems, are evaluated as a single and distinct construction material regardless of when or how the individual parts or components of those systems are delivered to the construction site. </p><p><i>Domestic construction material</i> means the following&mdash;</p><p>(1) An unmanufactured construction material mined or produced in the United States. (The Buy American statute applies.)</p><p>(2) A manufactured construction material that is manufactured in the United States and, if the construction material consists wholly or predominantly of iron or steel, the iron or steel was produced in the United States. (Section 1605 of the Recovery Act applies.)</p><p><i>Foreign construction material</i> means a construction material other than a domestic construction material.</p><p><i>Manufactured construction material</i> means any construction material that is not unmanufactured construction material.</p><p><i>Steel</i> means an alloy that includes at least 50 percent iron, between .02 and 2 percent carbon, and may include other elements.</p><p><i>United States</i> means the 50 States, the District of Columbia, and outlying areas.</p><p><i>Unmanufactured construction material</i> means raw material brought to the construction site for incorporation into the building or work that has not been&mdash;</p><p>(1) Processed into a specific form and shape; or</p><p>(2) Combined with other raw material to create a material that has different properties than the properties of the individual raw materials.</p><p>(b) <i>Domestic preference.</i> (1) This clause implements&mdash;</p><p>(i) Section 1605 of the American Recovery and Reinvestment Act of 2009 (Recovery Act) (Pub. L. 111-5), by requiring, unless an exception applies, that all manufactured construction material in the project is manufactured in the United States and, if the construction material consists wholly or predominantly of iron or steel, the iron or steel was produced in the United States (produced in the United States means that all manufacturing processes of the iron or steel must take place in the United States, except metallurgical processes involving refinement of steel additives); and</p><p>(ii) 41 U.S.C. chapter 83, Buy American, by providing a preference for unmanufactured construction material mined or produced in the United States over unmanufactured construction material mined or produced in a foreign country.</p><p>(2) The Contractor shall use only domestic construction material in performing this contract, except as provided in paragraph (b)(3) and (b)(4) of this clause.</p><p>(3) This requirement does not apply to the construction material or components listed by the Government as follows:</p>&nbsp; {{textbox_52.225-21[0]}}\n<p><i>[Contracting Officer to list applicable excepted materials or indicate ânoneâ]</i></p><p>(4) The Contracting Officer may add other foreign construction material to the list in paragraph (b)(3) of this clause if the Government determines that&mdash;</p><p>(i) The cost of domestic construction material would be unreasonable;</p><p>(A) The cost of domestic manufactured construction material, when compared to the cost of comparable foreign manufactured construction material, is unreasonable when the cumulative cost of such material will increase the cost of the contract by more than 25 percent;</p><p>(B) The cost of domestic unmanufactured construction material is unreasonable when the cost of such material exceeds the cost of comparable foreign unmanufactured construction material by more than 6 percent;</p><p>(ii) The construction material is not mined, produced, or manufactured in the United States in sufficient and reasonably available quantities and of a satisfactory quality;</p><p>(iii) The application of the restriction of section 1605 of the Recovery Act to a particular manufactured construction material would be inconsistent with the public interest or the application of the Buy American statute to a particular unmanufactured construction material would be impracticable or inconsistent with the public interest.</p><p>(c) <i>Request for determination of inapplicability of Section 1605 of the Recovery Act or the Buy American statute.</i> (1)(i) Any Contractor request to use foreign construction material in accordance with paragraph (b)(4) of this clause shall include adequate information for Government evaluation of the request, including&mdash;</p><p>(A) A description of the foreign and domestic construction materials;</p><p>(B) Unit of measure;</p><p>(C) Quantity;</p><p>(D) Cost;</p><p>(E) Time of delivery or availability;</p><p>(F) Location of the construction project;</p><p>(G) Name and address of the proposed supplier; and</p><p>(H) A detailed justification of the reason for use of foreign construction materials cited in accordance with paragraph (b)(4) of this clause.</p><p>(ii) A request based on unreasonable cost shall include a reasonable survey of the market and a completed cost comparison table in the format in paragraph (d) of this clause.</p><p>(iii) The cost of construction material shall include all delivery costs to the construction site and any applicable duty.</p><p>(iv) Any Contractor request for a determination submitted after contract award shall explain why the Contractor could not reasonably foresee the need for such determination and could not have requested the determination before contract award. If the Contractor does not submit a satisfactory explanation, the Contracting Officer need not make a determination.</p><p>(2) If the Government determines after contract award that an exception to section 1605 of the Recovery Act or the Buy American statute applies and the Contracting Officer and the Contractor negotiate adequate consideration, the Contracting Officer will modify the contract to allow use of the foreign construction material. However, when the basis for the exception is the unreasonable cost of a domestic construction material, adequate consideration is not less than the differential established in paragraph (b)(4)(i) of this clause.</p><p>(3) Unless the Government determines that an exception to section 1605 of the Recovery Act or the Buy American statute applies, use of foreign construction material is noncompliant with section 1605 of the American Recovery and Reinvestment Act or the Buy American statute.</p><p>(d) <i>Data.</i> To permit evaluation of requests under paragraph (c) of this clause based on unreasonable cost, the Contractor shall include the following information and any applicable supporting data based on the survey of suppliers:</p>\n<div><div><p>Foreign and Domestic Construction Materials Cost Comparison</p></div><div><table border="&quot;1&quot;" cellpadding="&quot;1&quot;" cellspacing="&quot;1&quot;" frame="&quot;void&quot;" width="&quot;100%&quot;"><tbody><tr><th scope="&quot;col&quot;">Construction material description</th><th scope="&quot;col&quot;">Unit of measure</th><th scope="&quot;col&quot;">Quantity</th><th scope="&quot;col&quot;">Cost<br>(dollars)*</br></th></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">Item 1:</td><td align="&quot;center&quot;"></td><td align="&quot;center&quot;"></td><td align="&quot;center&quot;"></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;" 4em"="">Foreign construction material</td><td align="&quot;center&quot;">{{textbox_52.225-21[1]}}</td><td align="&quot;center&quot;">{{textbox_52.225-21[2]}}</td><td align="&quot;center&quot;">{{textbox_52.225-21[3]}}</td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;" 4em"="">Domestic construction material</td><td align="&quot;center&quot;">{{textbox_52.225-21[4]}}</td><td align="&quot;center&quot;">{{textbox_52.225-21[5]}}</td><td align="&quot;center&quot;">{{textbox_52.225-21[6]}}</td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">Item 2</td><td align="&quot;center&quot;"></td><td align="&quot;center&quot;"></td><td align="&quot;center&quot;"></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;" 4em"="">Foreign construction material</td><td align="&quot;center&quot;">{{textbox_52.225-21[7]}}</td><td align="&quot;center&quot;">{{textbox_52.225-21[8]}}</td><td align="&quot;center&quot;">{{textbox_52.225-21[9]}}</td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;" 4em"="">Domestic construction material</td><td align="&quot;center&quot;">{{textbox_52.225-21[10]}}</td><td align="&quot;center&quot;">{{textbox_52.225-21[11]}}</td><td align="&quot;center&quot;">{{textbox_52.225-21[12]}}</td></tr></tbody></table></div><div><p><i>[List name, address, telephone number, and contact for suppliers surveyed. Attach copy of reponse; if oral, attach summary.] [Include other applicable supporting information.]</i></p><p>*<i>Include all delivery costs to the construction site.]</i></p></div></div>'
--               '<p>(a) <i>Definitions.</i> As used in this clause&mdash;</p><p><i>Component</i> means an article, material, or supply incorporated directly into a construction material.</p><p><i>Construction material</i> means an article, material, or supply brought to the construction site by the Contractor or a subcontractor for incorporation into the building or work. The term also includes an item brought to the site preassembled from articles, materials, or supplies. However, emergency life safety systems, such as emergency lighting, fire alarm, and audio evacuation systems, that are discrete systems incorporated into a public building or work and that are produced as complete systems, are evaluated as a single and distinct construction material regardless of when or how the individual parts or components of those systems are delivered to the construction site. </p><p><i>Domestic construction material</i> means the following&mdash;</p><p>(1) An unmanufactured construction material mined or produced in the United States. (The Buy American statute applies.)</p><p>(2) A manufactured construction material that is manufactured in the United States and, if the construction material consists wholly or predominantly of iron or steel, the iron or steel was produced in the United States. (Section 1605 of the Recovery Act applies.)</p><p><i>Foreign construction material</i> means a construction material other than a domestic construction material.</p><p><i>Manufactured construction material</i> means any construction material that is not unmanufactured construction material.</p><p><i>Steel</i> means an alloy that includes at least 50 percent iron, between .02 and 2 percent carbon, and may include other elements.</p><p><i>United States</i> means the 50 States, the District of Columbia, and outlying areas.</p><p><i>Unmanufactured construction material</i> means raw material brought to the construction site for incorporation into the building or work that has not been&mdash;</p><p>(1) Processed into a specific form and shape; or</p><p>(2) Combined with other raw material to create a material that has different properties than the properties of the individual raw materials.</p><p>(b) <i>Domestic preference.</i> (1) This clause implements&mdash;</p><p>(i) Section 1605 of the American Recovery and Reinvestment Act of 2009 (Recovery Act) (Pub. L. 111-5), by requiring, unless an exception applies, that all manufactured construction material in the project is manufactured in the United States and, if the construction material consists wholly or predominantly of iron or steel, the iron or steel was produced in the United States (produced in the United States means that all manufacturing processes of the iron or steel must take place in the United States, except metallurgical processes involving refinement of steel additives); and</p><p>(ii) 41 U.S.C. chapter 83, Buy American, by providing a preference for unmanufactured construction material mined or produced in the United States over unmanufactured construction material mined or produced in a foreign country.</p><p>(2) The Contractor shall use only domestic construction material in performing this contract, except as provided in paragraph (b)(3) and (b)(4) of this clause.</p><p>(3) This requirement does not apply to the construction material or components listed by the Government as follows:</p>Â&nbsp; {{textbox_52.225-21[0]}}\n<p><i>[Contracting Officer to list applicable excepted materials or indicate ânoneâ]</i></p><p>(4) The Contracting Officer may add other foreign construction material to the list in paragraph (b)(3) of this clause if the Government determines that&mdash;</p><p>(i) The cost of domestic construction material would be unreasonable;</p><p>(A) The cost of domestic manufactured construction material, when compared to the cost of comparable foreign manufactured construction material, is unreasonable when the cumulative cost of such material will increase the cost of the contract by more than 25 percent;</p><p>(B) The cost of domestic unmanufactured construction material is unreasonable when the cost of such material exceeds the cost of comparable foreign unmanufactured construction material by more than 6 percent;</p><p>(ii) The construction material is not mined, produced, or manufactured in the United States in sufficient and reasonably available quantities and of a satisfactory quality;</p><p>(iii) The application of the restriction of section 1605 of the Recovery Act to a particular manufactured construction material would be inconsistent with the public interest or the application of the Buy American statute to a particular unmanufactured construction material would be impracticable or inconsistent with the public interest.</p><p>(c) <i>Request for determination of inapplicability of Section 1605 of the Recovery Act or the Buy American statute.</i> (1)(i) Any Contractor request to use foreign construction material in accordance with paragraph (b)(4) of this clause shall include adequate information for Government evaluation of the request, including&mdash;</p><p>(A) A description of the foreign and domestic construction materials;</p><p>(B) Unit of measure;</p><p>(C) Quantity;</p><p>(D) Cost;</p><p>(E) Time of delivery or availability;</p><p>(F) Location of the construction project;</p><p>(G) Name and address of the proposed supplier; and</p><p>(H) A detailed justification of the reason for use of foreign construction materials cited in accordance with paragraph (b)(4) of this clause.</p><p>(ii) A request based on unreasonable cost shall include a reasonable survey of the market and a completed cost comparison table in the format in paragraph (d) of this clause.</p><p>(iii) The cost of construction material shall include all delivery costs to the construction site and any applicable duty.</p><p>(iv) Any Contractor request for a determination submitted after contract award shall explain why the Contractor could not reasonably foresee the need for such determination and could not have requested the determination before contract award. If the Contractor does not submit a satisfactory explanation, the Contracting Officer need not make a determination.</p><p>(2) If the Government determines after contract award that an exception to section 1605 of the Recovery Act or the Buy American statute applies and the Contracting Officer and the Contractor negotiate adequate consideration, the Contracting Officer will modify the contract to allow use of the foreign construction material. However, when the basis for the exception is the unreasonable cost of a domestic construction material, adequate consideration is not less than the differential established in paragraph (b)(4)(i) of this clause.</p><p>(3) Unless the Government determines that an exception to section 1605 of the Recovery Act or the Buy American statute applies, use of foreign construction material is noncompliant with section 1605 of the American Recovery and Reinvestment Act or the Buy American statute.</p><p>(d) <i>Data.</i> To permit evaluation of requests under paragraph (c) of this clause based on unreasonable cost, the Contractor shall include the following information and any applicable supporting data based on the survey of suppliers:</p>\n<div><div><p>Foreign and Domestic Construction Materials Cost Comparison</p></div><div><table border="&quot;1&quot;" cellpadding="&quot;1&quot;" cellspacing="&quot;1&quot;" frame="&quot;void&quot;" width="&quot;100%&quot;"><tbody><tr><th scope="&quot;col&quot;">Construction material description</th><th scope="&quot;col&quot;">Unit of measure</th><th scope="&quot;col&quot;">Quantity</th><th scope="&quot;col&quot;">Cost<br>(dollars)*</br></th></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">Item 1:</td><td align="&quot;center&quot;"></td><td align="&quot;center&quot;"></td><td align="&quot;center&quot;"></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;" 4em"="">Foreign construction material</td><td align="&quot;center&quot;">{{textbox_52.225-21[1]}}</td><td align="&quot;center&quot;">{{textbox_52.225-21[2]}}</td><td align="&quot;center&quot;">{{textbox_52.225-21[3]}}</td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;" 4em"="">Domestic construction material</td><td align="&quot;center&quot;">{{textbox_52.225-21[4]}}</td><td align="&quot;center&quot;">{{textbox_52.225-21[5]}}</td><td align="&quot;center&quot;">{{textbox_52.225-21[6]}}</td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">Item 2</td><td align="&quot;center&quot;"></td><td align="&quot;center&quot;"></td><td align="&quot;center&quot;"></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;" 4em"="">Foreign construction material</td><td align="&quot;center&quot;">{{textbox_52.225-21[7]}}</td><td align="&quot;center&quot;">{{textbox_52.225-21[8]}}</td><td align="&quot;center&quot;">{{textbox_52.225-21[9]}}</td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;" 4em"="">Domestic construction material</td><td align="&quot;center&quot;">{{textbox_52.225-21[10]}}</td><td align="&quot;center&quot;">{{textbox_52.225-21[11]}}</td><td align="&quot;center&quot;">{{textbox_52.225-21[12]}}</td></tr></tbody></table></div><div><p><i>[List name, address, telephone number, and contact for suppliers surveyed. Attach copy of reponse; if oral, attach summary.] [Include other applicable supporting information.]</i></p><p>*<i>Include all delivery costs to the construction site.]</i></p></div></div>'
WHERE clause_id = 72446; -- 52.225-21

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1225_622&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1225_622&rgn=div8'
WHERE clause_id = 72448; -- 52.225-22

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?node=se48.2.52_1225_623&amp;rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=31961bd8c221b0a8c46e7a6af888422a&amp;node=se48.2.52_1225_623&amp;rgn=div8'
WHERE clause_id = 72450; -- 52.225-23

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1225_624&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1225_624&rgn=div8'
WHERE clause_id = 72453; -- 52.225-24

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1225_625&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1225_625&rgn=div8'
WHERE clause_id = 72454; -- 52.225-25

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1225_626&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1225_626&rgn=div8'
WHERE clause_id = 72455; -- 52.225-26

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1225_63&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1225_63&rgn=div8'
WHERE clause_id = 72459; -- 52.225-3

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1225_63&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1225_63&rgn=div8'
WHERE clause_id = 72463; -- 52.225-3

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?node=se48.2.52_1225_64&amp;rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=1e45c9c95cc0fa5020d0a83c2da53fb4&amp;node=se48.2.52_1225_64&amp;rgn=div8'
, clause_data = '<p>(a) The offeror certifies that each end product, except those listed in paragraph (b) or (c) of this provision, is a domestic end product and that for other than COTS items, the offeror has considered components of unknown origin to have been mined, produced, or manufactured outside the United States. The terms âBahrainian, Moroccan, Omani, Panamanian, or Peruvian end product,â âcommercially available off-the-shelf (COTS) item,â âcomponent,â âdomestic end product,â âend product,â âforeign end product,â âFree Trade Agreement country,â âFree Trade Agreement country end product,â âIsraeli end product,â and âUnited Statesâ are defined in the clause of this solicitation entitled âBuy American&mdash;Free Trade Agreements&mdash;Israeli Trade Act.â</p><p>(b) The offeror certifies that the following supplies are Free Trade Agreement country end products (other than Bahrainian, Moroccan, Omani, Panamanian, or Peruvian end products) or Israeli end products as defined in the clause of this solicitation entitled âBuy American&mdash;Free Trade Agreements&mdash;Israeli Trade Actâ:</p>\n<div><div><p>Free Trade Agreement Country End Products (Other than Bahrainian, Moroccan, Omani, Panamanian, or Peruvian End Products) or Israeli End Products:</p></div><div><table border="&quot;1&quot;" cellpadding="&quot;1&quot;" cellspacing="&quot;1&quot;" frame="&quot;void&quot;" width="&quot;100%&quot;"><tbody><tr><th scope="&quot;col&quot;">Line Item No.</th><th scope="&quot;col&quot;">&nbsp;&nbsp;</th><th scope="&quot;col&quot;">Country of<br>Origin</br></th></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">&nbsp;&nbsp;&nbsp;</td><td align="&quot;left&quot;">&nbsp;&nbsp;&nbsp;</td><td align="&quot;left&quot;"></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">&nbsp;&nbsp;&nbsp;</td><td align="&quot;left&quot;">&nbsp;&nbsp;&nbsp;</td><td align="&quot;left&quot;"></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">&nbsp;&nbsp;&nbsp;</td><td align="&quot;left&quot;">&nbsp;&nbsp;&nbsp;</td><td align="&quot;left&quot;"></td></tr><tr><td align="&quot;center&quot;" colspan="&quot;3&quot;" scope="&quot;row&quot;">[List as necessary]</td></tr></tbody></table></div></div><p>(c) The offeror shall list those supplies that are foreign end products (other than those listed in paragraph (b) of this provision) as defined in the clause of this solicitation entitled âBuy American&mdash;Free Trade Agreements&mdash;Israeli Trade Act.â The offeror shall list as other foreign end products those end products manufactured in the United States that do not qualify as domestic end products, <i>i.e.</i>, an end product that is not a COTS item and does not meet the component test in paragraph (2) of the definition of âdomestic end product.â</p><p>Other Foreign End Products:</p><p>LINE ITEM NO. COUNTRY OF ORIGIN</p><p>{{textbox_52.225-4[0]}} {{textbox_52.225-4[1]}}</p><p>{{textbox_52.225-4[2]}} {{textbox_52.225-4[3]}}</p><p>{{textbox_52.225-4[4]}} {{textbox_52.225-4[5]}}</p><p>[List as necessary]</p><p>(d) The Government will evaluate offers in accordance with the policies and procedures of Part 25 of the Federal Acquisition Regulation.</p>'
--               '<p>(a) The offeror certifies that each end product, except those listed in paragraph (b) or (c) of this provision, is a domestic end product and that for other than COTS items, the offeror has considered components of unknown origin to have been mined, produced, or manufactured outside the United States. The terms âBahrainian, Moroccan, Omani, Panamanian, or Peruvian end product,â âcommercially available off-the-shelf (COTS) item,â âcomponent,â âdomestic end product,â âend product,â âforeign end product,â âFree Trade Agreement country,â âFree Trade Agreement country end product,â âIsraeli end product,â and âUnited Statesâ are defined in the clause of this solicitation entitled âBuy American&mdash;Free Trade Agreements&mdash;Israeli Trade Act.â</p><p>(b) The offeror certifies that the following supplies are Free Trade Agreement country end products (other than Bahrainian, Moroccan, Omani, Panamanian, or Peruvian end products) or Israeli end products as defined in the clause of this solicitation entitled âBuy American&mdash;Free Trade Agreements&mdash;Israeli Trade Actâ:</p>\n<div><div><p>Free Trade Agreement Country End Products (Other than Bahrainian, Moroccan, Omani, Panamanian, or Peruvian End Products) or Israeli End Products:</p></div><div><table border="&quot;1&quot;" cellpadding="&quot;1&quot;" cellspacing="&quot;1&quot;" frame="&quot;void&quot;" width="&quot;100%&quot;"><tbody><tr><th scope="&quot;col&quot;">Line Item No.</th><th scope="&quot;col&quot;">Â&nbsp;Â&nbsp;</th><th scope="&quot;col&quot;">Country of<br>Origin</br></th></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">Â&nbsp;Â&nbsp;Â&nbsp;</td><td align="&quot;left&quot;">Â&nbsp;Â&nbsp;Â&nbsp;</td><td align="&quot;left&quot;"></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">Â&nbsp;Â&nbsp;Â&nbsp;</td><td align="&quot;left&quot;">Â&nbsp;Â&nbsp;Â&nbsp;</td><td align="&quot;left&quot;"></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">Â&nbsp;Â&nbsp;Â&nbsp;</td><td align="&quot;left&quot;">Â&nbsp;Â&nbsp;Â&nbsp;</td><td align="&quot;left&quot;"></td></tr><tr><td align="&quot;center&quot;" colspan="&quot;3&quot;" scope="&quot;row&quot;">[List as necessary]</td></tr></tbody></table></div></div><p>(c) The offeror shall list those supplies that are foreign end products (other than those listed in paragraph (b) of this provision) as defined in the clause of this solicitation entitled âBuy American&mdash;Free Trade Agreements&mdash;Israeli Trade Act.â The offeror shall list as other foreign end products those end products manufactured in the United States that do not qualify as domestic end products, <i>i.e.</i>, an end product that is not a COTS item and does not meet the component test in paragraph (2) of the definition of âdomestic end product.â</p><p>Other Foreign End Products:</p><p>LINE ITEM NO. COUNTRY OF ORIGIN</p><p>{{textbox_52.225-4[0]}} {{textbox_52.225-4[1]}}</p><p>{{textbox_52.225-4[2]}} {{textbox_52.225-4[3]}}</p><p>{{textbox_52.225-4[4]}} {{textbox_52.225-4[5]}}</p><p>[List as necessary]</p><p>(d) The Government will evaluate offers in accordance with the policies and procedures of Part 25 of the Federal Acquisition Regulation.</p>'
WHERE clause_id = 72466; -- 52.225-4

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1225_65&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1225_65&rgn=div8'
WHERE clause_id = 72467; -- 52.225-5

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1225_66&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1225_66&rgn=div8'
WHERE clause_id = 72468; -- 52.225-6

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1225_67&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1225_67&rgn=div8'
WHERE clause_id = 72469; -- 52.225-7

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1225_68&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1225_68&rgn=div8'
WHERE clause_id = 72470; -- 52.225-8

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?node=se48.2.52_1225_69&amp;rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=31961bd8c221b0a8c46e7a6af888422a&amp;node=se48.2.52_1225_69&amp;rgn=div8'
WHERE clause_id = 72471; -- 52.225-9

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1226_61&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1226_61&rgn=div8'
WHERE clause_id = 72472; -- 52.226-1

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1226_62&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1226_62&rgn=div8'
WHERE clause_id = 72473; -- 52.226-2

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1226_63&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1226_63&rgn=div8'
WHERE clause_id = 72474; -- 52.226-3

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1226_64&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1226_64&rgn=div8'
WHERE clause_id = 72475; -- 52.226-4

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1226_65&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1226_65&rgn=div8'
WHERE clause_id = 72476; -- 52.226-5

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1226_66&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1226_66&rgn=div8'
WHERE clause_id = 72477; -- 52.226-6

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1227_61&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1227_61&rgn=div8'
WHERE clause_id = 72480; -- 52.227-1

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1227_610&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1227_610&rgn=div8'
WHERE clause_id = 72481; -- 52.227-10

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1227_611&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1227_611&rgn=div8'
WHERE clause_id = 72487; -- 52.227-11

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1227_613&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1227_613&rgn=div8'
WHERE clause_id = 72490; -- 52.227-13

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1227_614&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1227_614&rgn=div8'
WHERE clause_id = 72496; -- 52.227-14

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1227_615&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1227_615&rgn=div8'
WHERE clause_id = 72497; -- 52.227-15

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1227_616&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1227_616&rgn=div8'
WHERE clause_id = 72498; -- 52.227-16

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1227_617&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1227_617&rgn=div8'
WHERE clause_id = 72499; -- 52.227-17

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1227_618&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1227_618&rgn=div8'
WHERE clause_id = 72500; -- 52.227-18

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1227_619&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1227_619&rgn=div8'
WHERE clause_id = 72501; -- 52.227-19

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1227_62&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1227_62&rgn=div8'
WHERE clause_id = 72502; -- 52.227-2

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1227_620&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1227_620&rgn=div8'
WHERE clause_id = 72503; -- 52.227-20

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1227_621&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1227_621&rgn=div8'
WHERE clause_id = 72504; -- 52.227-21

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1227_622&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1227_622&rgn=div8'
WHERE clause_id = 72505; -- 52.227-22

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1227_623&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1227_623&rgn=div8'
WHERE clause_id = 72506; -- 52.227-23

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1227_63&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1227_63&rgn=div8'
WHERE clause_id = 72510; -- 52.227-3

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1227_64&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1227_64&rgn=div8'
WHERE clause_id = 72512; -- 52.227-4

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1227_65&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1227_65&rgn=div8'
WHERE clause_id = 72513; -- 52.227-5

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1227_66&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1227_66&rgn=div8'
WHERE clause_id = 72515; -- 52.227-6

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1227_67&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1227_67&rgn=div8'
WHERE clause_id = 72516; -- 52.227-7

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1227_69&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1227_69&rgn=div8'
WHERE clause_id = 72517; -- 52.227-9

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1228_61&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1228_61&rgn=div8'
WHERE clause_id = 72518; -- 52.228-1

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1228_610&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1228_610&rgn=div8'
WHERE clause_id = 72519; -- 52.228-10

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1228_611&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1228_611&rgn=div8'
WHERE clause_id = 72520; -- 52.228-11

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1228_612&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1228_612&rgn=div8'
WHERE clause_id = 72521; -- 52.228-12

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1228_613&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1228_613&rgn=div8'
WHERE clause_id = 72522; -- 52.228-13

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1228_614&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1228_614&rgn=div8'
WHERE clause_id = 72523; -- 52.228-14

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1228_615&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1228_615&rgn=div8'
WHERE clause_id = 72524; -- 52.228-15

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1228_616&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1228_616&rgn=div8'
WHERE clause_id = 72526; -- 52.228-16

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1228_62&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1228_62&rgn=div8'
WHERE clause_id = 72527; -- 52.228-2

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1228_63&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1228_63&rgn=div8'
WHERE clause_id = 72528; -- 52.228-3

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1228_64&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1228_64&rgn=div8'
WHERE clause_id = 72529; -- 52.228-4

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1228_65&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1228_65&rgn=div8'
WHERE clause_id = 72530; -- 52.228-5

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1228_67&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1228_67&rgn=div8'
WHERE clause_id = 72531; -- 52.228-7

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1228_68&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1228_68&rgn=div8'
WHERE clause_id = 72532; -- 52.228-8

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1228_69&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1228_69&rgn=div8'
WHERE clause_id = 72533; -- 52.228-9

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1229_61&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1229_61&rgn=div8'
WHERE clause_id = 72534; -- 52.229-1

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1229_610&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1229_610&rgn=div8'
WHERE clause_id = 72535; -- 52.229-10

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1229_62&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1229_62&rgn=div8'
WHERE clause_id = 72537; -- 52.229-2

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1229_63&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1229_63&rgn=div8'
WHERE clause_id = 72538; -- 52.229-3

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1229_64&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1229_64&rgn=div8'
WHERE clause_id = 72539; -- 52.229-4

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1229_66&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1229_66&rgn=div8'
WHERE clause_id = 72541; -- 52.229-6

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1229_67&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1229_67&rgn=div8'
WHERE clause_id = 72542; -- 52.229-7

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1229_68&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1229_68&rgn=div8'
WHERE clause_id = 72543; -- 52.229-8

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1229_69&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1229_69&rgn=div8'
WHERE clause_id = 72544; -- 52.229-9

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1230_61&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1230_61&rgn=div8'
WHERE clause_id = 72546; -- 52.230-1

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1230_62&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1230_62&rgn=div8'
WHERE clause_id = 72547; -- 52.230-2

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1230_63&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1230_63&rgn=div8'
WHERE clause_id = 72548; -- 52.230-3

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1230_64&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1230_64&rgn=div8'
WHERE clause_id = 72549; -- 52.230-4

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1230_65&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1230_65&rgn=div8'
WHERE clause_id = 72550; -- 52.230-5

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1230_66&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1230_66&rgn=div8'
WHERE clause_id = 72551; -- 52.230-6

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1230_67&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1230_67&rgn=div8'
WHERE clause_id = 72552; -- 52.230-7

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1232_61&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1232_61&rgn=div8'
WHERE clause_id = 72553; -- 52.232-1

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1232_610&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1232_610&rgn=div8'
WHERE clause_id = 72554; -- 52.232-10

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1232_611&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1232_611&rgn=div8'
WHERE clause_id = 72555; -- 52.232-11

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1232_612&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1232_612&rgn=div8'
WHERE clause_id = 72556; -- 52.232-12

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1232_613&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1232_613&rgn=div8'
WHERE clause_id = 72562; -- 52.232-13

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1232_614&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1232_614&rgn=div8'
WHERE clause_id = 72563; -- 52.232-14

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1232_615&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1232_615&rgn=div8'
WHERE clause_id = 72564; -- 52.232-15

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1232_616&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1232_616&rgn=div8'
WHERE clause_id = 72568; -- 52.232-16

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1232_617&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1232_617&rgn=div8'
WHERE clause_id = 72569; -- 52.232-17

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1232_618&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1232_618&rgn=div8'
WHERE clause_id = 72570; -- 52.232-18

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1232_619&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1232_619&rgn=div8'
WHERE clause_id = 72571; -- 52.232-19

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1232_62&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1232_62&rgn=div8'
WHERE clause_id = 72572; -- 52.232-2

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1232_620&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1232_620&rgn=div8'
WHERE clause_id = 72573; -- 52.232-20

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1232_622&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1232_622&rgn=div8'
WHERE clause_id = 72574; -- 52.232-22

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1232_623&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1232_623&rgn=div8'
WHERE clause_id = 72576; -- 52.232-23

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1232_624&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1232_624&rgn=div8'
WHERE clause_id = 72577; -- 52.232-24

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1232_625&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1232_625&rgn=div8'
WHERE clause_id = 72579; -- 52.232-25

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1232_626&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1232_626&rgn=div8'
WHERE clause_id = 72580; -- 52.232-26

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1232_627&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1232_627&rgn=div8'
WHERE clause_id = 72581; -- 52.232-27

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1232_628&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1232_628&rgn=div8'
WHERE clause_id = 72583; -- 52.232-28

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1232_629&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1232_629&rgn=div8'
WHERE clause_id = 72584; -- 52.232-29

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1232_63&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1232_63&rgn=div8'
WHERE clause_id = 72585; -- 52.232-3

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1232_630&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1232_630&rgn=div8'
WHERE clause_id = 72586; -- 52.232-30

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1232_631&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1232_631&rgn=div8'
WHERE clause_id = 72587; -- 52.232-31

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1232_632&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1232_632&rgn=div8'
WHERE clause_id = 72588; -- 52.232-32

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1232_633&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1232_633&rgn=div8'
WHERE clause_id = 72589; -- 52.232-33

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1232_634&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1232_634&rgn=div8'
WHERE clause_id = 72590; -- 52.232-34

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1232_635&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1232_635&rgn=div8'
WHERE clause_id = 72591; -- 52.232-35

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1232_636&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1232_636&rgn=div8'
WHERE clause_id = 72592; -- 52.232-36

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1232_637&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1232_637&rgn=div8'
WHERE clause_id = 72593; -- 52.232-37

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1232_638&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1232_638&rgn=div8'
WHERE clause_id = 72594; -- 52.232-38

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1232_639&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1232_639&rgn=div8'
WHERE clause_id = 72595; -- 52.232-39

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1232_64&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1232_64&rgn=div8'
WHERE clause_id = 72596; -- 52.232-4

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1232_640&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1232_640&rgn=div8'
WHERE clause_id = 72597; -- 52.232-40

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1232_65&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1232_65&rgn=div8'
WHERE clause_id = 72598; -- 52.232-5

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1232_66&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1232_66&rgn=div8'
WHERE clause_id = 72599; -- 52.232-6

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1232_67&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1232_67&rgn=div8'
WHERE clause_id = 72600; -- 52.232-7

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1232_68&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1232_68&rgn=div8'
WHERE clause_id = 72601; -- 52.232-8

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1232_69&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1232_69&rgn=div8'
WHERE clause_id = 72602; -- 52.232-9

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1233_61&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1233_61&rgn=div8'
WHERE clause_id = 72604; -- 52.233-1

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1233_62&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1233_62&rgn=div8'
WHERE clause_id = 72605; -- 52.233-2

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1233_63&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1233_63&rgn=div8'
WHERE clause_id = 72607; -- 52.233-3

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1233_64&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1233_64&rgn=div8'
WHERE clause_id = 72608; -- 52.233-4

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1234_61&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1234_61&rgn=div8'
WHERE clause_id = 72609; -- 52.234-1

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1234_62&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1234_62&rgn=div8'
WHERE clause_id = 72610; -- 52.234-2

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1234_63&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1234_63&rgn=div8'
WHERE clause_id = 72611; -- 52.234-3

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1234_64&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1234_64&rgn=div8'
WHERE clause_id = 72612; -- 52.234-4

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1236_61&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1236_61&rgn=div8'
WHERE clause_id = 72613; -- 52.236-1

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1236_610&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1236_610&rgn=div8'
WHERE clause_id = 72614; -- 52.236-10

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1236_611&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1236_611&rgn=div8'
WHERE clause_id = 72615; -- 52.236-11

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1236_612&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1236_612&rgn=div8'
WHERE clause_id = 72616; -- 52.236-12

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1236_613&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1236_613&rgn=div8'
WHERE clause_id = 72617; -- 52.236-13

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1236_614&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1236_614&rgn=div8'
WHERE clause_id = 72619; -- 52.236-14

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1236_615&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1236_615&rgn=div8'
WHERE clause_id = 72620; -- 52.236-15

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1236_616&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1236_616&rgn=div8'
WHERE clause_id = 72622; -- 52.236-16

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1236_617&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1236_617&rgn=div8'
WHERE clause_id = 72623; -- 52.236-17

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1236_618&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1236_618&rgn=div8'
WHERE clause_id = 72624; -- 52.236-18

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1236_619&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1236_619&rgn=div8'
WHERE clause_id = 72625; -- 52.236-19

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1236_62&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1236_62&rgn=div8'
WHERE clause_id = 72626; -- 52.236-2

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1236_621&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1236_621&rgn=div8'
WHERE clause_id = 72629; -- 52.236-21

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1236_622&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1236_622&rgn=div8'
WHERE clause_id = 72630; -- 52.236-22

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1236_623&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1236_623&rgn=div8'
WHERE clause_id = 72631; -- 52.236-23

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1236_624&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1236_624&rgn=div8'
WHERE clause_id = 72632; -- 52.236-24

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1236_625&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1236_625&rgn=div8'
WHERE clause_id = 72633; -- 52.236-25

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1236_626&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1236_626&rgn=div8'
WHERE clause_id = 72634; -- 52.236-26

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1236_627&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1236_627&rgn=div8'
WHERE clause_id = 72636; -- 52.236-27

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1236_628&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1236_628&rgn=div8'
WHERE clause_id = 72637; -- 52.236-28

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1236_63&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1236_63&rgn=div8'
WHERE clause_id = 72638; -- 52.236-3

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1236_64&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1236_64&rgn=div8'
WHERE clause_id = 72639; -- 52.236-4

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1236_65&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1236_65&rgn=div8'
WHERE clause_id = 72640; -- 52.236-5

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1236_66&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1236_66&rgn=div8'
WHERE clause_id = 72641; -- 52.236-6

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1236_67&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1236_67&rgn=div8'
WHERE clause_id = 72642; -- 52.236-7

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1236_68&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1236_68&rgn=div8'
WHERE clause_id = 72643; -- 52.236-8

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1236_69&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1236_69&rgn=div8'
WHERE clause_id = 72644; -- 52.236-9

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1237_61&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1237_61&rgn=div8'
WHERE clause_id = 72645; -- 52.237-1

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1237_610&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1237_610&rgn=div8'
WHERE clause_id = 72646; -- 52.237-10

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1237_611&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1237_611&rgn=div8'
WHERE clause_id = 72647; -- 52.237-11

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1237_62&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1237_62&rgn=div8'
WHERE clause_id = 72648; -- 52.237-2

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1237_63&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1237_63&rgn=div8'
WHERE clause_id = 72649; -- 52.237-3

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1237_64&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1237_64&rgn=div8'
WHERE clause_id = 72651; -- 52.237-4

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1237_65&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1237_65&rgn=div8'
WHERE clause_id = 72652; -- 52.237-5

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1237_66&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1237_66&rgn=div8'
WHERE clause_id = 72653; -- 52.237-6

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1237_67&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1237_67&rgn=div8'
WHERE clause_id = 72654; -- 52.237-7

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1237_68&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1237_68&rgn=div8'
WHERE clause_id = 72655; -- 52.237-8

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1237_69&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1237_69&rgn=div8'
WHERE clause_id = 72656; -- 52.237-9

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1239_61&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1239_61&rgn=div8'
WHERE clause_id = 72657; -- 52.239-1

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1241_61&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1241_61&rgn=div8'
WHERE clause_id = 72658; -- 52.241-1

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1241_610&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1241_610&rgn=div8'
WHERE clause_id = 72659; -- 52.241-10

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1241_611&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1241_611&rgn=div8'
WHERE clause_id = 72660; -- 52.241-11

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1241_612&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1241_612&rgn=div8'
WHERE clause_id = 72661; -- 52.241-12

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1241_613&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1241_613&rgn=div8'
WHERE clause_id = 72662; -- 52.241-13

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1241_62&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1241_62&rgn=div8'
WHERE clause_id = 72663; -- 52.241-2

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1241_63&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1241_63&rgn=div8'
WHERE clause_id = 72664; -- 52.241-3

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1241_64&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1241_64&rgn=div8'
WHERE clause_id = 72665; -- 52.241-4

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1241_65&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1241_65&rgn=div8'
WHERE clause_id = 72666; -- 52.241-5

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1241_66&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1241_66&rgn=div8'
WHERE clause_id = 72667; -- 52.241-6

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1241_67&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1241_67&rgn=div8'
WHERE clause_id = 72668; -- 52.241-7

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1241_68&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1241_68&rgn=div8'
WHERE clause_id = 72669; -- 52.241-8

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1241_69&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1241_69&rgn=div8'
WHERE clause_id = 72671; -- 52.241-9

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1242_61&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1242_61&rgn=div8'
WHERE clause_id = 72672; -- 52.242-1

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1242_613&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1242_613&rgn=div8'
WHERE clause_id = 72673; -- 52.242-13

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1242_614&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1242_614&rgn=div8'
WHERE clause_id = 72674; -- 52.242-14

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1242_615&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1242_615&rgn=div8'
WHERE clause_id = 72676; -- 52.242-15

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1242_617&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1242_617&rgn=div8'
WHERE clause_id = 72677; -- 52.242-17

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1242_62&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1242_62&rgn=div8'
WHERE clause_id = 72678; -- 52.242-2

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1242_63&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1242_63&rgn=div8'
WHERE clause_id = 72679; -- 52.242-3

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1242_64&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1242_64&rgn=div8'
WHERE clause_id = 72680; -- 52.242-4

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1243_61&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1243_61&rgn=div8'
WHERE clause_id = 72686; -- 52.243-1

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1243_62&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1243_62&rgn=div8'
WHERE clause_id = 72691; -- 52.243-2

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1243_63&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1243_63&rgn=div8'
WHERE clause_id = 72692; -- 52.243-3

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1243_64&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1243_64&rgn=div8'
WHERE clause_id = 72693; -- 52.243-4

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1243_65&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1243_65&rgn=div8'
WHERE clause_id = 72694; -- 52.243-5

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1243_66&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1243_66&rgn=div8'
WHERE clause_id = 72695; -- 52.243-6

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1243_67&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1243_67&rgn=div8'
WHERE clause_id = 72696; -- 52.243-7

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1244_62&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1244_62&rgn=div8'
WHERE clause_id = 72697; -- 52.244-2

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1244_64&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1244_64&rgn=div8'
WHERE clause_id = 72699; -- 52.244-4

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1244_65&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1244_65&rgn=div8'
WHERE clause_id = 72700; -- 52.244-5

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1244_66&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1244_66&rgn=div8'
WHERE clause_id = 72701; -- 52.244-6

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1245_61&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1245_61&rgn=div8'
WHERE clause_id = 72704; -- 52.245-1

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1245_62&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1245_62&rgn=div8'
WHERE clause_id = 72705; -- 52.245-2

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1245_69&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1245_69&rgn=div8'
WHERE clause_id = 72706; -- 52.245-9

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1246_61&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1246_61&rgn=div8'
WHERE clause_id = 72707; -- 52.246-1

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1246_611&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1246_611&rgn=div8'
WHERE clause_id = 72708; -- 52.246-11

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1246_612&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1246_612&rgn=div8'
WHERE clause_id = 72709; -- 52.246-12

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1246_613&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1246_613&rgn=div8'
WHERE clause_id = 72710; -- 52.246-13

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1246_614&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1246_614&rgn=div8'
WHERE clause_id = 72711; -- 52.246-14

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1246_615&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1246_615&rgn=div8'
WHERE clause_id = 72712; -- 52.246-15

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1246_616&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1246_616&rgn=div8'
WHERE clause_id = 72713; -- 52.246-16

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1246_617&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1246_617&rgn=div8'
WHERE clause_id = 72718; -- 52.246-17

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1246_618&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1246_618&rgn=div8'
WHERE clause_id = 72722; -- 52.246-18

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1246_619&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1246_619&rgn=div8'
WHERE clause_id = 72726; -- 52.246-19

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1246_62&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1246_62&rgn=div8'
WHERE clause_id = 72729; -- 52.246-2

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1246_620&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1246_620&rgn=div8'
WHERE clause_id = 72730; -- 52.246-20

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1246_621&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1246_621&rgn=div8'
WHERE clause_id = 72732; -- 52.246-21

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1246_623&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1246_623&rgn=div8'
WHERE clause_id = 72733; -- 52.246-23

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1246_624&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1246_624&rgn=div8'
WHERE clause_id = 72735; -- 52.246-24

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1246_625&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1246_625&rgn=div8'
WHERE clause_id = 72736; -- 52.246-25

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1246_63&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1246_63&rgn=div8'
WHERE clause_id = 72737; -- 52.246-3

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1246_64&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1246_64&rgn=div8'
WHERE clause_id = 72738; -- 52.246-4

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1246_65&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1246_65&rgn=div8'
WHERE clause_id = 72739; -- 52.246-5

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1246_66&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1246_66&rgn=div8'
WHERE clause_id = 72741; -- 52.246-6

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1246_67&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1246_67&rgn=div8'
WHERE clause_id = 72742; -- 52.246-7

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1246_68&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1246_68&rgn=div8'
WHERE clause_id = 72744; -- 52.246-8

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1246_69&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1246_69&rgn=div8'
WHERE clause_id = 72745; -- 52.246-9

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1247_61&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1247_61&rgn=div8'
WHERE clause_id = 72746; -- 52.247-1

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1247_610&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1247_610&rgn=div8'
WHERE clause_id = 72747; -- 52.247-10

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1247_611&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1247_611&rgn=div8'
WHERE clause_id = 72748; -- 52.247-11

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1247_612&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1247_612&rgn=div8'
WHERE clause_id = 72749; -- 52.247-12

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1247_613&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1247_613&rgn=div8'
WHERE clause_id = 72750; -- 52.247-13

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1247_614&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1247_614&rgn=div8'
WHERE clause_id = 72751; -- 52.247-14

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1247_615&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1247_615&rgn=div8'
WHERE clause_id = 72752; -- 52.247-15

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1247_616&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1247_616&rgn=div8'
WHERE clause_id = 72753; -- 52.247-16

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1247_617&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1247_617&rgn=div8'
WHERE clause_id = 72754; -- 52.247-17

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1247_618&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1247_618&rgn=div8'
WHERE clause_id = 72755; -- 52.247-18

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1247_619&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1247_619&rgn=div8'
WHERE clause_id = 72756; -- 52.247-19

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1247_62&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1247_62&rgn=div8'
WHERE clause_id = 72757; -- 52.247-2

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?node=se48.2.52_1247_620&amp;rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=31961bd8c221b0a8c46e7a6af888422a&amp;node=se48.2.52_1247_620&amp;rgn=div8'
, clause_data = '<p>For the purpose of evaluating offers, and for no other purpose, the following estimated quantities or weights will be considered as the quantities or weights to be shipped between each origin and destination listed:</p><div><table border="&quot;1&quot;" cellpadding="&quot;1&quot;" cellspacing="&quot;1&quot;" frame="&quot;void&quot;" width="&quot;100%&quot;"><tbody><tr><th scope="&quot;col&quot;">Origin</th><th scope="&quot;col&quot;">Destination</th><th scope="&quot;col&quot;">Estimated quantity or weight</th><th scope="&quot;col&quot;">&nbsp;&nbsp;</th></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">&nbsp;&nbsp;&nbsp;</td><td align="&quot;left&quot;"></td><td align="&quot;left&quot;"></td><td align="&quot;left&quot;"></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">&nbsp;&nbsp;&nbsp;</td><td align="&quot;left&quot;"></td><td align="&quot;left&quot;"></td><td align="&quot;left&quot;"></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">&nbsp;&nbsp;&nbsp;</td><td align="&quot;left&quot;"></td><td align="&quot;left&quot;"></td><td align="&quot;left&quot;"></td></tr></tbody></table></div>'
--               '<p>For the purpose of evaluating offers, and for no other purpose, the following estimated quantities or weights will be considered as the quantities or weights to be shipped between each origin and destination listed:</p><div><table border="&quot;1&quot;" cellpadding="&quot;1&quot;" cellspacing="&quot;1&quot;" frame="&quot;void&quot;" width="&quot;100%&quot;"><tbody><tr><th scope="&quot;col&quot;">Origin</th><th scope="&quot;col&quot;">Destination</th><th scope="&quot;col&quot;">Estimated quantity or weight</th><th scope="&quot;col&quot;">Â&nbsp;Â&nbsp;</th></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">Â&nbsp;Â&nbsp;Â&nbsp;</td><td align="&quot;left&quot;"></td><td align="&quot;left&quot;"></td><td align="&quot;left&quot;"></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">Â&nbsp;Â&nbsp;Â&nbsp;</td><td align="&quot;left&quot;"></td><td align="&quot;left&quot;"></td><td align="&quot;left&quot;"></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">Â&nbsp;Â&nbsp;Â&nbsp;</td><td align="&quot;left&quot;"></td><td align="&quot;left&quot;"></td><td align="&quot;left&quot;"></td></tr></tbody></table></div>'
WHERE clause_id = 72758; -- 52.247-20

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1247_621&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1247_621&rgn=div8'
WHERE clause_id = 72759; -- 52.247-21

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1247_622&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1247_622&rgn=div8'
WHERE clause_id = 72760; -- 52.247-22

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1247_623&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1247_623&rgn=div8'
WHERE clause_id = 72761; -- 52.247-23

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1247_624&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1247_624&rgn=div8'
WHERE clause_id = 72762; -- 52.247-24

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1247_625&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1247_625&rgn=div8'
WHERE clause_id = 72763; -- 52.247-25

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1247_626&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1247_626&rgn=div8'
WHERE clause_id = 72764; -- 52.247-26

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1247_627&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1247_627&rgn=div8'
WHERE clause_id = 72765; -- 52.247-27

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1247_628&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1247_628&rgn=div8'
WHERE clause_id = 72766; -- 52.247-28

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1247_629&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1247_629&rgn=div8'
WHERE clause_id = 72767; -- 52.247-29

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1247_63&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1247_63&rgn=div8'
WHERE clause_id = 72769; -- 52.247-3

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1247_630&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1247_630&rgn=div8'
WHERE clause_id = 72770; -- 52.247-30

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1247_631&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1247_631&rgn=div8'
WHERE clause_id = 72771; -- 52.247-31

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1247_632&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1247_632&rgn=div8'
WHERE clause_id = 72772; -- 52.247-32

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1247_633&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1247_633&rgn=div8'
WHERE clause_id = 72773; -- 52.247-33

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1247_634&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1247_634&rgn=div8'
WHERE clause_id = 72774; -- 52.247-34

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1247_635&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1247_635&rgn=div8'
WHERE clause_id = 72775; -- 52.247-35

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1247_636&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1247_636&rgn=div8'
WHERE clause_id = 72776; -- 52.247-36

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1247_637&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1247_637&rgn=div8'
WHERE clause_id = 72777; -- 52.247-37

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1247_638&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1247_638&rgn=div8'
WHERE clause_id = 72778; -- 52.247-38

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1247_639&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1247_639&rgn=div8'
WHERE clause_id = 72779; -- 52.247-39

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1247_64&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1247_64&rgn=div8'
WHERE clause_id = 72780; -- 52.247-4

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1247_640&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1247_640&rgn=div8'
WHERE clause_id = 72781; -- 52.247-40

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1247_641&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1247_641&rgn=div8'
WHERE clause_id = 72782; -- 52.247-41

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1247_642&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1247_642&rgn=div8'
WHERE clause_id = 72783; -- 52.247-42

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1247_643&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1247_643&rgn=div8'
WHERE clause_id = 72784; -- 52.247-43

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1247_644&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1247_644&rgn=div8'
WHERE clause_id = 72785; -- 52.247-44

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1247_645&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1247_645&rgn=div8'
WHERE clause_id = 72786; -- 52.247-45

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1247_646&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1247_646&rgn=div8'
WHERE clause_id = 72787; -- 52.247-46

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1247_647&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1247_647&rgn=div8'
WHERE clause_id = 72788; -- 52.247-47

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1247_648&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1247_648&rgn=div8'
WHERE clause_id = 72789; -- 52.247-48

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1247_649&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1247_649&rgn=div8'
WHERE clause_id = 72790; -- 52.247-49

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1247_65&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1247_65&rgn=div8'
WHERE clause_id = 72791; -- 52.247-5

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1247_650&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1247_650&rgn=div8'
WHERE clause_id = 72792; -- 52.247-50

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?node=se48.2.52_1247_651&amp;rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=31961bd8c221b0a8c46e7a6af888422a&amp;node=se48.2.52_1247_651&amp;rgn=div8'
, clause_data = '<p>(e) <i>Ports of loading nominated by offeror.</i> The ports of loading named in paragraph (d) above are considered by the Government to be appropriate for this solicitation due to their compatibility with methods and facilities required to handle the cargo and types of vessels and to meet the required overseas delivery dates. Notwithstanding the foregoing, offerors may nominate additional ports of loading that the offeror considers to be more favorable to the Government. The Government may disregard such nominated ports if, after considering the quantity and nature of the supplies concerned, the requisite cargo handling capability, the available sailings on U.S.-flag vessels, and other pertinent transportation factors, it determines that use of the nominated ports is not compatible with the required overseas delivery date. United States Great Lakes ports of loading may be considered in the evaluation of offers only for those items scheduled in this provision for delivery during the ice-free or navigable period as proclaimed by the authorities of the St. Lawrence Seaway (normal period is between April 15 and November 30 annually). All ports named, including those nominated by offerors and determined to be eligible as provided in this provision, shall be considered in evaluating all offers received in order to establish the lowest laid down cost to the Government at the overseas port of discharge. All determinations shall be based on availability of ocean services by U.S.-flag vessels only. Additional U.S. port(s) of loading nominated by offeror, if any: {{textbox_52.247-51[0]}}</p><p>(f) <i>Price basis:</i> Offeror shall indicate whether prices are based on&ldquo;??</p><p>(&nbsp;&nbsp;&nbsp;) Paragraph (b), f.o.b. origin, transportation by GBL to port listed in paragraph (d);</p><p>(&nbsp;&nbsp;&nbsp;) Paragraph (c), f.o.b. destination (i.e., a port listed in paragraph (d));</p><p>(&nbsp;&nbsp;&nbsp;) Paragraph (e), f.o.b. origin, transportation by GBL to port nominated in paragraph (e); and/or</p><p>(&nbsp;&nbsp;&nbsp;) Paragraph (e), f.o.b. destination (i.e., a port nominated in paragraph (e)).</p><div><table border="&quot;1&quot;" cellpadding="&quot;1&quot;" cellspacing="&quot;1&quot;" frame="&quot;void&quot;" width="&quot;100%&quot;"><tbody><tr><th scope="&quot;col&quot;"><i>Ports/Terminals of Loading</i></th><th scope="&quot;col&quot;"><i>Combined Ocean and Port Handling Charges to (Indicate Country)</i></th><th scope="&quot;col&quot;"><i>Unit of Measure: i.e., metric ton, measurement ton, cubic foot, etc.</i></th></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">{{textbox_52.247-51[1]}}</td><td align="&quot;left&quot;">{{textbox_52.247-51[2]}}</td><td align="&quot;left&quot;">{{textbox_52.247-51[3]}}</td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">{{textbox_52.247-51[4]}}</td><td align="&quot;left&quot;">{{textbox_52.247-51[5]}}</td><td align="&quot;left&quot;">{{textbox_52.247-51[6]}}</td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">{{textbox_52.247-51[7]}}</td><td align="&quot;left&quot;">{{textbox_52.247-51[8]}}</td><td align="&quot;left&quot;">{{textbox_52.247-51[9]}}</td></tr></tbody></table></div><p>(e) <i>Ports of loading nominated by offeror.</i> The ports of loading named in paragraph (d) above are considered by the Government to be appropriate for this solicitation due to their compatibility with methods and facilities required to handle the cargo and types of vessels and to meet the required overseas delivery dates. Notwithstanding the foregoing, offerors may nominate additional ports of loading that the offeror considers to be more favorable to the Government. The Government may disregard such nominated ports if, after considering the quantity and nature of the supplies concerned, the requisite cargo handling capability, the available sailings on U.S.-flag vessels, and other pertinent transportation factors, it determines that use of the nominated ports is not compatible with the required overseas delivery date. United States Great Lakes ports of loading may be considered in the evaluation of offers only for those items scheduled in this provision for delivery during the ice-free or navigable period as proclaimed by the authorities of the St. Lawrence Seaway (normal period is between April 15 and November 30 annually). All ports named, including those nominated by offerors and determined to be eligible as provided in this provision, shall be considered in evaluating all offers received in order to establish the lowest laid down cost to the Government at the overseas port of discharge. All determinations shall be based on availability of ocean services by U.S.-flag vessels only. Additional U.S. port(s) of loading nominated by offeror, if any: {{textbox_52.247-51[10]}}</p><p>(f) <i>Price basis:</i> Offeror shall indicate whether prices are based on&ldquo;??</p><p>(&nbsp;&nbsp;&nbsp;) Paragraph (b), f.o.b. origin, transportation by GBL to port listed in paragraph (d);</p><p>(&nbsp;&nbsp;&nbsp;) Paragraph (c), f.o.b. destination (i.e., a port listed in paragraph (d));</p><p>(&nbsp;&nbsp;&nbsp;) Paragraph (e), f.o.b. origin, transportation by GBL to port nominated in paragraph (e); and/or</p><p>(&nbsp;&nbsp;&nbsp;) Paragraph (e), f.o.b. destination (i.e., a port nominated in paragraph (e)).</p>'
--               '<p>(e) <i>Ports of loading nominated by offeror.</i> The ports of loading named in paragraph (d) above are considered by the Government to be appropriate for this solicitation due to their compatibility with methods and facilities required to handle the cargo and types of vessels and to meet the required overseas delivery dates. Notwithstanding the foregoing, offerors may nominate additional ports of loading that the offeror considers to be more favorable to the Government. The Government may disregard such nominated ports if, after considering the quantity and nature of the supplies concerned, the requisite cargo handling capability, the available sailings on U.S.-flag vessels, and other pertinent transportation factors, it determines that use of the nominated ports is not compatible with the required overseas delivery date. United States Great Lakes ports of loading may be considered in the evaluation of offers only for those items scheduled in this provision for delivery during the ice-free or navigable period as proclaimed by the authorities of the St. Lawrence Seaway (normal period is between April 15 and November 30 annually). All ports named, including those nominated by offerors and determined to be eligible as provided in this provision, shall be considered in evaluating all offers received in order to establish the lowest laid down cost to the Government at the overseas port of discharge. All determinations shall be based on availability of ocean services by U.S.-flag vessels only. Additional U.S. port(s) of loading nominated by offeror, if any: {{textbox_52.247-51[0]}}</p><p>(f) <i>Price basis:</i> Offeror shall indicate whether prices are based on&ldquo;??</p><p>(Â&nbsp;Â&nbsp;Â&nbsp;) Paragraph (b), f.o.b. origin, transportation by GBL to port listed in paragraph (d);</p><p>(Â&nbsp;Â&nbsp;Â&nbsp;) Paragraph (c), f.o.b. destination (i.e., a port listed in paragraph (d));</p><p>(Â&nbsp;Â&nbsp;Â&nbsp;) Paragraph (e), f.o.b. origin, transportation by GBL to port nominated in paragraph (e); and/or</p><p>(Â&nbsp;Â&nbsp;Â&nbsp;) Paragraph (e), f.o.b. destination (i.e., a port nominated in paragraph (e)).</p><div><table border="&quot;1&quot;" cellpadding="&quot;1&quot;" cellspacing="&quot;1&quot;" frame="&quot;void&quot;" width="&quot;100%&quot;"><tbody><tr><th scope="&quot;col&quot;"><i>Ports/Terminals of Loading</i></th><th scope="&quot;col&quot;"><i>Combined Ocean and Port Handling Charges to (Indicate Country)</i></th><th scope="&quot;col&quot;"><i>Unit of Measure: i.e., metric ton, measurement ton, cubic foot, etc.</i></th></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">{{textbox_52.247-51[1]}}</td><td align="&quot;left&quot;">{{textbox_52.247-51[2]}}</td><td align="&quot;left&quot;">{{textbox_52.247-51[3]}}</td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">{{textbox_52.247-51[4]}}</td><td align="&quot;left&quot;">{{textbox_52.247-51[5]}}</td><td align="&quot;left&quot;">{{textbox_52.247-51[6]}}</td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">{{textbox_52.247-51[7]}}</td><td align="&quot;left&quot;">{{textbox_52.247-51[8]}}</td><td align="&quot;left&quot;">{{textbox_52.247-51[9]}}</td></tr></tbody></table></div><p>(e) <i>Ports of loading nominated by offeror.</i> The ports of loading named in paragraph (d) above are considered by the Government to be appropriate for this solicitation due to their compatibility with methods and facilities required to handle the cargo and types of vessels and to meet the required overseas delivery dates. Notwithstanding the foregoing, offerors may nominate additional ports of loading that the offeror considers to be more favorable to the Government. The Government may disregard such nominated ports if, after considering the quantity and nature of the supplies concerned, the requisite cargo handling capability, the available sailings on U.S.-flag vessels, and other pertinent transportation factors, it determines that use of the nominated ports is not compatible with the required overseas delivery date. United States Great Lakes ports of loading may be considered in the evaluation of offers only for those items scheduled in this provision for delivery during the ice-free or navigable period as proclaimed by the authorities of the St. Lawrence Seaway (normal period is between April 15 and November 30 annually). All ports named, including those nominated by offerors and determined to be eligible as provided in this provision, shall be considered in evaluating all offers received in order to establish the lowest laid down cost to the Government at the overseas port of discharge. All determinations shall be based on availability of ocean services by U.S.-flag vessels only. Additional U.S. port(s) of loading nominated by offeror, if any: {{textbox_52.247-51[10]}}</p><p>(f) <i>Price basis:</i> Offeror shall indicate whether prices are based on&ldquo;??</p><p>(Â&nbsp;Â&nbsp;Â&nbsp;) Paragraph (b), f.o.b. origin, transportation by GBL to port listed in paragraph (d);</p><p>(Â&nbsp;Â&nbsp;Â&nbsp;) Paragraph (c), f.o.b. destination (i.e., a port listed in paragraph (d));</p><p>(Â&nbsp;Â&nbsp;Â&nbsp;) Paragraph (e), f.o.b. origin, transportation by GBL to port nominated in paragraph (e); and/or</p><p>(Â&nbsp;Â&nbsp;Â&nbsp;) Paragraph (e), f.o.b. destination (i.e., a port nominated in paragraph (e)).</p>'
WHERE clause_id = 72796; -- 52.247-51

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1247_652&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1247_652&rgn=div8'
WHERE clause_id = 72797; -- 52.247-52

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1247_653&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1247_653&rgn=div8'
WHERE clause_id = 72798; -- 52.247-53

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1247_655&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1247_655&rgn=div8'
WHERE clause_id = 72799; -- 52.247-55

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?node=se48.2.52_1247_656&amp;rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=31961bd8c221b0a8c46e7a6af888422a&amp;node=se48.2.52_1247_656&amp;rgn=div8'
, clause_data = '<p>The lowest appropriate common carrier transportation costs, including offeror''s through transit rates and charges when applicable, from offeror''s shipping points, via the transit point, to the ultimate destination will be used in evaluating offers.</p><div><table border="&quot;1&quot;" cellpadding="&quot;1&quot;" cellspacing="&quot;1&quot;" frame="&quot;void&quot;" width="&quot;100%&quot;"><tbody><tr><th scope="&quot;col&quot;">Transit point(s)</th><th scope="&quot;col&quot;">Destination(s)</th><th scope="&quot;col&quot;">&nbsp;&nbsp;</th></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">&nbsp;&nbsp;&nbsp;</td><td align="&quot;left&quot;"></td><td align="&quot;left&quot;"></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">&nbsp;&nbsp;&nbsp;</td><td align="&quot;left&quot;"></td><td align="&quot;left&quot;"></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">&nbsp;&nbsp;&nbsp;</td><td align="&quot;left&quot;"></td><td align="&quot;left&quot;"></td></tr></tbody></table></div>'
--               '<p>The lowest appropriate common carrier transportation costs, including offeror''s through transit rates and charges when applicable, from offeror''s shipping points, via the transit point, to the ultimate destination will be used in evaluating offers.</p><div><table border="&quot;1&quot;" cellpadding="&quot;1&quot;" cellspacing="&quot;1&quot;" frame="&quot;void&quot;" width="&quot;100%&quot;"><tbody><tr><th scope="&quot;col&quot;">Transit point(s)</th><th scope="&quot;col&quot;">Destination(s)</th><th scope="&quot;col&quot;">Â&nbsp;Â&nbsp;</th></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">Â&nbsp;Â&nbsp;Â&nbsp;</td><td align="&quot;left&quot;"></td><td align="&quot;left&quot;"></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">Â&nbsp;Â&nbsp;Â&nbsp;</td><td align="&quot;left&quot;"></td><td align="&quot;left&quot;"></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">Â&nbsp;Â&nbsp;Â&nbsp;</td><td align="&quot;left&quot;"></td><td align="&quot;left&quot;"></td></tr></tbody></table></div>'
WHERE clause_id = 72800; -- 52.247-56

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1247_657&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1247_657&rgn=div8'
WHERE clause_id = 72801; -- 52.247-57

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1247_658&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1247_658&rgn=div8'
WHERE clause_id = 72802; -- 52.247-58

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1247_659&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1247_659&rgn=div8'
WHERE clause_id = 72803; -- 52.247-59

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1247_66&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1247_66&rgn=div8'
WHERE clause_id = 72804; -- 52.247-6

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1247_660&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1247_660&rgn=div8'
WHERE clause_id = 72805; -- 52.247-60

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1247_661&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1247_661&rgn=div8'
WHERE clause_id = 72807; -- 52.247-61

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?node=se48.2.52_1247_662&amp;rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=1e45c9c95cc0fa5020d0a83c2da53fb4&amp;node=se48.2.52_1247_662&amp;rgn=div8'
WHERE clause_id = 72808; -- 52.247-62

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1247_663&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1247_663&rgn=div8'
WHERE clause_id = 72809; -- 52.247-63

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1247_664&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1247_664&rgn=div8'
WHERE clause_id = 72812; -- 52.247-64

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1247_665&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1247_665&rgn=div8'
WHERE clause_id = 72813; -- 52.247-65

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1247_666&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1247_666&rgn=div8'
WHERE clause_id = 72814; -- 52.247-66

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1247_667&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1247_667&rgn=div8'
WHERE clause_id = 72815; -- 52.247-67

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1247_668&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1247_668&rgn=div8'
WHERE clause_id = 72816; -- 52.247-68

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1247_67&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1247_67&rgn=div8'
WHERE clause_id = 72817; -- 52.247-7

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1247_68&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1247_68&rgn=div8'
WHERE clause_id = 72818; -- 52.247-8

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1247_69&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1247_69&rgn=div8'
WHERE clause_id = 72819; -- 52.247-9

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?node=se48.2.52_1248_61&amp;rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=1e45c9c95cc0fa5020d0a83c2da53fb4&amp;node=se48.2.52_1248_61&amp;rgn=div8'
WHERE clause_id = 72823; -- 52.248-1

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1248_62&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1248_62&rgn=div8'
WHERE clause_id = 72824; -- 52.248-2

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1248_63&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1248_63&rgn=div8'
WHERE clause_id = 72826; -- 52.248-3

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1249_61&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1249_61&rgn=div8'
WHERE clause_id = 72828; -- 52.249-1

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1249_610&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1249_610&rgn=div8'
WHERE clause_id = 72832; -- 52.249-10

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1249_612&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1249_612&rgn=div8'
WHERE clause_id = 72833; -- 52.249-12

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1249_614&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1249_614&rgn=div8'
WHERE clause_id = 72834; -- 52.249-14

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1249_62&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1249_62&rgn=div8'
WHERE clause_id = 72838; -- 52.249-2

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1249_63&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1249_63&rgn=div8'
WHERE clause_id = 72840; -- 52.249-3

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1249_64&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1249_64&rgn=div8'
WHERE clause_id = 72841; -- 52.249-4

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1249_65&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1249_65&rgn=div8'
WHERE clause_id = 72842; -- 52.249-5

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1249_66&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1249_66&rgn=div8'
WHERE clause_id = 72848; -- 52.249-6

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1249_67&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1249_67&rgn=div8'
WHERE clause_id = 72849; -- 52.249-7

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1249_68&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1249_68&rgn=div8'
WHERE clause_id = 72851; -- 52.249-8

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1249_69&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1249_69&rgn=div8'
WHERE clause_id = 72852; -- 52.249-9

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1250_61&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1250_61&rgn=div8'
WHERE clause_id = 72854; -- 52.250-1

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1250_62&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1250_62&rgn=div8'
WHERE clause_id = 72855; -- 52.250-2

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1250_63&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1250_63&rgn=div8'
WHERE clause_id = 72858; -- 52.250-3

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1250_64&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1250_64&rgn=div8'
WHERE clause_id = 72861; -- 52.250-4

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1250_65&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1250_65&rgn=div8'
WHERE clause_id = 72862; -- 52.250-5

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1251_61&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1251_61&rgn=div8'
WHERE clause_id = 72863; -- 52.251-1

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1251_62&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1251_62&rgn=div8'
WHERE clause_id = 72864; -- 52.251-2

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1252_61&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1252_61&rgn=div8'
WHERE clause_id = 72865; -- 52.252-1

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1252_62&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1252_62&rgn=div8'
WHERE clause_id = 72866; -- 52.252-2

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1252_63&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1252_63&rgn=div8'
WHERE clause_id = 72867; -- 52.252-3

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1252_64&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1252_64&rgn=div8'
WHERE clause_id = 72868; -- 52.252-4

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1252_65&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1252_65&rgn=div8'
WHERE clause_id = 72869; -- 52.252-5

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1252_66&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1252_66&rgn=div8'
WHERE clause_id = 72870; -- 52.252-6

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1253_61&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1253_61&rgn=div8'
WHERE clause_id = 72871; -- 52.253-1

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1216_67010&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1216_67010&rgn=div8'
WHERE clause_id = 72958; -- 252.216-7010 Alternate I

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1217_67000&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1217_67000&rgn=div8'
WHERE clause_id = 72960; -- 252.217-7000 Alternate I

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1219_67003&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1219_67003&rgn=div8'
WHERE clause_id = 72983; -- 252.219-7003 Alternate I

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1223_67006&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1223_67006&rgn=div8'
WHERE clause_id = 73002; -- 252.223-7006 Alternate I

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1225_67000&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1225_67000&rgn=div8'
WHERE clause_id = 73006; -- 252.225-7000 Alternate I

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?node=se48.3.252_1225_67001&amp;rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=033a9dbe0ea0f20cfdd114e4fe8fd8bc&amp;node=se48.3.252_1225_67001&amp;rgn=div8'
WHERE clause_id = 73008; -- 252.225-7001 Alternate I

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1225_67020&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1225_67020&rgn=div8'
WHERE clause_id = 73026; -- 252.225-7020 Alternate I

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1225_67021&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1225_67021&rgn=div8'
WHERE clause_id = 73028; -- 252.225-7021 Alternate II

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1225_67035&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1225_67035&rgn=div8'
WHERE clause_id = 73041; -- 252.225-7035 Alternate I

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1225_67035&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1225_67035&rgn=div8'
WHERE clause_id = 73042; -- 252.225-7035 Alternate II

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1225_67035&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1225_67035&rgn=div8'
WHERE clause_id = 73043; -- 252.225-7035 Alternate III

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1225_67035&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1225_67035&rgn=div8'
WHERE clause_id = 73044; -- 252.225-7035 Alternate IV

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1225_67035&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1225_67035&rgn=div8'
WHERE clause_id = 73045; -- 252.225-7035 Alternate V

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1225_67036&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1225_67036&rgn=div8'
WHERE clause_id = 73047; -- 252.225-7036 Alternate I

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1225_67036&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1225_67036&rgn=div8'
WHERE clause_id = 73048; -- 252.225-7036 Alternate II

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1225_67036&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1225_67036&rgn=div8'
WHERE clause_id = 73049; -- 252.225-7036 Alternate III

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1225_67036&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1225_67036&rgn=div8'
WHERE clause_id = 73050; -- 252.225-7036 Alternate IV

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1225_67036&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1225_67036&rgn=div8'
WHERE clause_id = 73051; -- 252.225-7036 Alternate V

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1225_67044&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1225_67044&rgn=div8'
WHERE clause_id = 73060; -- 252.225-7044 Alternate I

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1225_67045&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1225_67045&rgn=div8'
WHERE clause_id = 73062; -- 252.225-7045 Alternate I

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1225_67045&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1225_67045&rgn=div8'
WHERE clause_id = 73063; -- 252.225-7045 Alternate II

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1225_67045&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1225_67045&rgn=div8'
WHERE clause_id = 73064; -- 252.225-7045 Alternate III

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1227_67005&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1227_67005&rgn=div8'
WHERE clause_id = 73095; -- 252.227-7005 Alternate I

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1227_67005&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1227_67005&rgn=div8'
WHERE clause_id = 73096; -- 252.227-7005 Alternate II

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1227_67013&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1227_67013&rgn=div8'
WHERE clause_id = 73104; -- 252.227-7013 Alternate I

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1227_67013&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1227_67013&rgn=div8'
WHERE clause_id = 73105; -- 252.227-7013 Alternate II

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1227_67014&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1227_67014&rgn=div8'
WHERE clause_id = 73107; -- 252.227-7014 Alternate I

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1227_67015&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1227_67015&rgn=div8'
WHERE clause_id = 73109; -- 252.227-7015 Alternate I

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1227_67018&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1227_67018&rgn=div8'
WHERE clause_id = 73113; -- 252.227-7018 Alternate I

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1229_67001&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1229_67001&rgn=div8'
WHERE clause_id = 73138; -- 252.229-7001 Alternate I

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1234_67003&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1234_67003&rgn=div8'
WHERE clause_id = 73173; -- 252.234-7003 Alternate I

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1234_67004&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1234_67004&rgn=div8'
WHERE clause_id = 73175; -- 252.234-7004 Alternate I

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1235_67003&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1235_67003&rgn=div8'
WHERE clause_id = 73180; -- 252.235-7003 Alternate I

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1237_67002&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1237_67002&rgn=div8'
WHERE clause_id = 73201; -- 252.237-7002 Alternate I

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1237_67016&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1237_67016&rgn=div8'
WHERE clause_id = 73216; -- 252.237-7016 Alternate I

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1237_67016&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1237_67016&rgn=div8'
WHERE clause_id = 73217; -- 252.237-7016 Alternate II

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1244_67001&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1244_67001&rgn=div8'
WHERE clause_id = 73250; -- 252.244-7001 Alternate I

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1246_67001&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1246_67001&rgn=div8'
WHERE clause_id = 73258; -- 252.246-7001 Alternate I

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1246_67001&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1246_67001&rgn=div8'
WHERE clause_id = 73259; -- 252.246-7001 Alternate II

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1247_67008&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1247_67008&rgn=div8'
WHERE clause_id = 73275; -- 252.247-7008 Alternate I

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1247_67023&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1247_67023&rgn=div8'
WHERE clause_id = 73290; -- 252.247-7023 Alternate I

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1247_67023&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1247_67023&rgn=div8'
WHERE clause_id = 73291; -- 252.247-7023 Alternate II

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1203_66&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1203_66&rgn=div8'
WHERE clause_id = 72107; -- 52.203-6 Alternate I

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1204_62&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1204_62&rgn=div8'
WHERE clause_id = 72120; -- 52.204-2 Alternate I

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1204_62&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1204_62&rgn=div8'
WHERE clause_id = 72121; -- 52.204-2 Alternate II

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1204_67&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1204_67&rgn=div8'
WHERE clause_id = 72127; -- 52.204-7 Alternate I

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1209_63&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1209_63&rgn=div8'
WHERE clause_id = 72145; -- 52.209-3 Alternate I

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1209_63&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1209_63&rgn=div8'
WHERE clause_id = 72146; -- 52.209-3 Alternate II

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1209_64&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1209_64&rgn=div8'
WHERE clause_id = 72148; -- 52.209-4 Alternate I

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1209_64&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1209_64&rgn=div8'
WHERE clause_id = 72149; -- 52.209-4 Alternate II

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1211_610&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1211_610&rgn=div8'
WHERE clause_id = 72157; -- 52.211-10 Alternate I

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?node=se48.2.52_1211_68&amp;rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=1e45c9c95cc0fa5020d0a83c2da53fb4&amp;node=se48.2.52_1211_68&amp;rgn=div8'
WHERE clause_id = 72173; -- 52.211-8 Alternate I

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?node=se48.2.52_1211_68&amp;rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=1e45c9c95cc0fa5020d0a83c2da53fb4&amp;node=se48.2.52_1211_68&amp;rgn=div8'
WHERE clause_id = 72174; -- 52.211-8 Alternate II

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?node=se48.2.52_1211_68&amp;rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=1e45c9c95cc0fa5020d0a83c2da53fb4&amp;node=se48.2.52_1211_68&amp;rgn=div8'
WHERE clause_id = 72175; -- 52.211-8 Alternate III

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?node=se48.2.52_1211_69&amp;rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=1e45c9c95cc0fa5020d0a83c2da53fb4&amp;node=se48.2.52_1211_69&amp;rgn=div8'
WHERE clause_id = 72177; -- 52.211-9 Alternate I

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?node=se48.2.52_1211_69&amp;rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=1e45c9c95cc0fa5020d0a83c2da53fb4&amp;node=se48.2.52_1211_69&amp;rgn=div8'
WHERE clause_id = 72178; -- 52.211-9 Alternate II

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?node=se48.2.52_1211_69&amp;rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=1e45c9c95cc0fa5020d0a83c2da53fb4&amp;node=se48.2.52_1211_69&amp;rgn=div8'
WHERE clause_id = 72179; -- 52.211-9 Alternate III

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?node=se48.2.52_1212_63&amp;rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=1e45c9c95cc0fa5020d0a83c2da53fb4&amp;node=se48.2.52_1212_63&amp;rgn=div8'
WHERE clause_id = 72184; -- 52.212-3 Alternate I

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1212_64&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1212_64&rgn=div8'
WHERE clause_id = 72185; -- 52.212-4 Alternate I

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1212_65&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1212_65&rgn=div8'
WHERE clause_id = 72187; -- 52.212-5 Alternate I

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1212_65&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1212_65&rgn=div8'
WHERE clause_id = 72188; -- 52.212-5 Alternate II

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1214_613&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1214_613&rgn=div8'
WHERE clause_id = 72196; -- 52.214-13 Alternate I

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1214_620&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1214_620&rgn=div8'
WHERE clause_id = 72203; -- 52.214-20 Alternate I

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1214_620&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1214_620&rgn=div8'
WHERE clause_id = 72204; -- 52.214-20 Alternate II

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1214_621&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1214_621&rgn=div8'
WHERE clause_id = 72206; -- 52.214-21 Alternate I

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1214_626&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1214_626&rgn=div8'
WHERE clause_id = 72212; -- 52.214-26 Alternate I

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1215_61&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1215_61&rgn=div8'
WHERE clause_id = 72225; -- 52.215-1 Alternate I

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1215_61&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1215_61&rgn=div8'
WHERE clause_id = 72226; -- 52.215-1 Alternate II

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1215_614&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1215_614&rgn=div8'
WHERE clause_id = 72232; -- 52.215-14 Alternate I

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1215_62&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1215_62&rgn=div8'
WHERE clause_id = 72239; -- 52.215-2 Alternate I

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1215_62&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1215_62&rgn=div8'
WHERE clause_id = 72240; -- 52.215-2 Alternate II

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1215_62&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1215_62&rgn=div8'
WHERE clause_id = 72241; -- 52.215-2 Alternate III

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1215_620&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1215_620&rgn=div8'
WHERE clause_id = 72243; -- 52.215-20 Alternate I

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1215_620&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1215_620&rgn=div8'
WHERE clause_id = 72244; -- 52.215-20 Alternate II

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1215_620&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1215_620&rgn=div8'
WHERE clause_id = 72245; -- 52.215-20 Alternate III

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1215_620&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1215_620&rgn=div8'
WHERE clause_id = 72246; -- 52.215-20 Alternate IV

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1215_621&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1215_621&rgn=div8'
WHERE clause_id = 72248; -- 52.215-21 Alternate I

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1215_621&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1215_621&rgn=div8'
WHERE clause_id = 72249; -- 52.215-21 Alternate II

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1215_621&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1215_621&rgn=div8'
WHERE clause_id = 72250; -- 52.215-21 Alternate III

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1215_621&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1215_621&rgn=div8'
WHERE clause_id = 72251; -- 52.215-21 Alternate IV

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1215_623&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1215_623&rgn=div8'
WHERE clause_id = 72254; -- 52.215-23 Alternate I

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1215_69&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1215_69&rgn=div8'
WHERE clause_id = 72260; -- 52.215-9 Alternate I

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1215_69&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1215_69&rgn=div8'
WHERE clause_id = 72261; -- 52.215-9 Alternate II

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1216_611&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1216_611&rgn=div8'
WHERE clause_id = 72265; -- 52.216-11 Alternate I

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1216_612&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1216_612&rgn=div8'
WHERE clause_id = 72267; -- 52.216-12 Alternate I

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1216_616&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1216_616&rgn=div8'
WHERE clause_id = 72270; -- 52.216-16 Alternate I

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1216_617&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1216_617&rgn=div8'
WHERE clause_id = 72272; -- 52.216-17 Alternate I

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1216_621&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1216_621&rgn=div8'
WHERE clause_id = 72278; -- 52.216-21 Alternate I

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1216_621&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1216_621&rgn=div8'
WHERE clause_id = 72279; -- 52.216-21 Alternate II

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1216_621&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1216_621&rgn=div8'
WHERE clause_id = 72280; -- 52.216-21 Alternate III

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1216_621&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1216_621&rgn=div8'
WHERE clause_id = 72281; -- 52.216-21 Alternate IV

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1216_625&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1216_625&rgn=div8'
WHERE clause_id = 72286; -- 52.216-25 Alternate I

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1216_67&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1216_67&rgn=div8'
WHERE clause_id = 72299; -- 52.216-7 Alternate I

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1216_67&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1216_67&rgn=div8'
WHERE clause_id = 72300; -- 52.216-7 Alternate II

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1216_67&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1216_67&rgn=div8'
WHERE clause_id = 72301; -- 52.216-7 Alternate III

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1216_67&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1216_67&rgn=div8'
WHERE clause_id = 72302; -- 52.216-7 Alternate IV

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1219_61&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1219_61&rgn=div8'
WHERE clause_id = 72313; -- 52.219-1 Alternate I

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1219_618&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1219_618&rgn=div8'
WHERE clause_id = 72322; -- 52.219-18 Alternate I

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1219_618&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1219_618&rgn=div8'
WHERE clause_id = 72323; -- 52.219-18 Alternate II

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1219_63&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1219_63&rgn=div8'
WHERE clause_id = 72329; -- 52.219-3 Alternate I

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1219_64&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1219_64&rgn=div8'
WHERE clause_id = 72332; -- 52.219-4 Alternate I

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1219_66&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1219_66&rgn=div8'
WHERE clause_id = 72335; -- 52.219-6 Alternate I

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1219_66&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1219_66&rgn=div8'
WHERE clause_id = 72336; -- 52.219-6 Alternate II

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1219_67&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1219_67&rgn=div8'
WHERE clause_id = 72337; -- 52.219-7 Alternate I

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1219_67&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1219_67&rgn=div8'
WHERE clause_id = 72338; -- 52.219-7 Alternate II

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1219_69&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1219_69&rgn=div8'
WHERE clause_id = 72341; -- 52.219-9 Alternate I

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1219_69&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1219_69&rgn=div8'
WHERE clause_id = 72343; -- 52.219-9 Alternate II

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1219_69&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1219_69&rgn=div8'
WHERE clause_id = 72342; -- 52.219-9 Alternate III

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1222_626&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1222_626&rgn=div8'
WHERE clause_id = 72363; -- 52.222-26 Alternate I

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1222_633&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1222_633&rgn=div8'
WHERE clause_id = 72372; -- 52.222-33 Alternate I

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1222_633&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1222_633&rgn=div8'
WHERE clause_id = 72373; -- 52.222-33 Alternate II

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1222_634&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1222_634&rgn=div8'
WHERE clause_id = 72376; -- 52.222-34 Alternate I

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1222_635&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1222_635&rgn=div8'
WHERE clause_id = 72377; -- 52.222-35 Alternate I

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1222_636&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1222_636&rgn=div8'
WHERE clause_id = 72379; -- 52.222-36 Alternate I

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1222_650&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1222_650&rgn=div8'
WHERE clause_id = 72393; -- 52.222-50 Alternate I

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1223_613&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1223_613&rgn=div8'
WHERE clause_id = 72410; -- 52.223-13 Alternate I

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1223_614&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1223_614&rgn=div8'
WHERE clause_id = 72412; -- 52.223-14 Alternate I

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1223_616&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1223_616&rgn=div8'
WHERE clause_id = 72414; -- 52.223-16 Alternate I

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1223_63&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1223_63&rgn=div8'
WHERE clause_id = 72420; -- 52.223-3 Alternate I

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1223_65&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1223_65&rgn=div8'
WHERE clause_id = 72423; -- 52.223-5 Alternate I

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1223_65&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1223_65&rgn=div8'
WHERE clause_id = 72424; -- 52.223-5 Alternate II

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1223_69&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1223_69&rgn=div8'
WHERE clause_id = 72872; -- 52.223-9 Alternate I

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1225_610&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1225_610&rgn=div8'
WHERE clause_id = 72432; -- 52.225-10 Alternate I

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?node=se48.2.52_1225_611&amp;rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=31961bd8c221b0a8c46e7a6af888422a&amp;node=se48.2.52_1225_611&amp;rgn=div8'
WHERE clause_id = 72434; -- 52.225-11 Alternate I

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1225_612&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1225_612&rgn=div8'
WHERE clause_id = 72436; -- 52.225-12 Alternate I

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1225_612&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1225_612&rgn=div8'
WHERE clause_id = 72437; -- 52.225-12 Alternate II

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1225_622&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1225_622&rgn=div8'
WHERE clause_id = 72447; -- 52.225-22 Alternate I

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?node=se48.2.52_1225_623&amp;rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=31961bd8c221b0a8c46e7a6af888422a&amp;node=se48.2.52_1225_623&amp;rgn=div8'
WHERE clause_id = 72449; -- 52.225-23 Alternate I

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1225_624&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1225_624&rgn=div8'
WHERE clause_id = 72451; -- 52.225-24 Alternate I

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1225_624&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1225_624&rgn=div8'
WHERE clause_id = 72452; -- 52.225-24 Alternate II

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1225_63&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1225_63&rgn=div8'
WHERE clause_id = 72456; -- 52.225-3 Alternate I

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1225_63&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1225_63&rgn=div8'
WHERE clause_id = 72460; -- 52.225-3 Alternate I

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1225_63&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1225_63&rgn=div8'
WHERE clause_id = 72457; -- 52.225-3 Alternate II

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1225_63&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1225_63&rgn=div8'
WHERE clause_id = 72461; -- 52.225-3 Alternate II

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1225_63&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1225_63&rgn=div8'
WHERE clause_id = 72458; -- 52.225-3 Alternate III

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1225_63&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1225_63&rgn=div8'
WHERE clause_id = 72462; -- 52.225-3 Alternate III

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?node=se48.2.52_1225_64&amp;rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=1e45c9c95cc0fa5020d0a83c2da53fb4&amp;node=se48.2.52_1225_64&amp;rgn=div8'
WHERE clause_id = 72464; -- 52.225-4 Alternate I

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?node=se48.2.52_1225_64&amp;rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=1e45c9c95cc0fa5020d0a83c2da53fb4&amp;node=se48.2.52_1225_64&amp;rgn=div8'
WHERE clause_id = 72465; -- 52.225-4 Alternate II

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1227_61&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1227_61&rgn=div8'
WHERE clause_id = 72478; -- 52.227-1 Alternate I

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1227_61&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1227_61&rgn=div8'
WHERE clause_id = 72479; -- 52.227-1 Alternate II

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1227_611&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1227_611&rgn=div8'
WHERE clause_id = 72482; -- 52.227-11 Alternate I

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1227_611&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1227_611&rgn=div8'
WHERE clause_id = 72483; -- 52.227-11 Alternate II

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1227_611&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1227_611&rgn=div8'
WHERE clause_id = 72484; -- 52.227-11 Alternate III

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1227_611&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1227_611&rgn=div8'
WHERE clause_id = 72485; -- 52.227-11 Alternate IV

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1227_611&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1227_611&rgn=div8'
WHERE clause_id = 72486; -- 52.227-11 Alternate V

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1227_613&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1227_613&rgn=div8'
WHERE clause_id = 72488; -- 52.227-13 Alternate I

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1227_613&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1227_613&rgn=div8'
WHERE clause_id = 72489; -- 52.227-13 Alternate II

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1227_614&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1227_614&rgn=div8'
WHERE clause_id = 72491; -- 52.227-14 Alternate I

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1227_614&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1227_614&rgn=div8'
WHERE clause_id = 72492; -- 52.227-14 Alternate II

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1227_614&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1227_614&rgn=div8'
WHERE clause_id = 72493; -- 52.227-14 Alternate III

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1227_614&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1227_614&rgn=div8'
WHERE clause_id = 72494; -- 52.227-14 Alternate IV

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1227_614&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1227_614&rgn=div8'
WHERE clause_id = 72495; -- 52.227-14 Alternate V

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1227_63&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1227_63&rgn=div8'
WHERE clause_id = 72507; -- 52.227-3 Alternate I

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1227_63&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1227_63&rgn=div8'
WHERE clause_id = 72508; -- 52.227-3 Alternate II

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1227_63&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1227_63&rgn=div8'
WHERE clause_id = 72509; -- 52.227-3 Alternate III

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1227_64&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1227_64&rgn=div8'
WHERE clause_id = 72511; -- 52.227-4 Alternate I

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1227_66&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1227_66&rgn=div8'
WHERE clause_id = 72514; -- 52.227-6 Alternate I

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1228_616&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1228_616&rgn=div8'
WHERE clause_id = 72525; -- 52.228-16 Alternate I

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1229_62&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1229_62&rgn=div8'
WHERE clause_id = 72536; -- 52.229-2 Alternate I

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1232_612&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1232_612&rgn=div8'
WHERE clause_id = 72557; -- 52.232-12 Alternate I

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1232_612&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1232_612&rgn=div8'
WHERE clause_id = 72558; -- 52.232-12 Alternate II

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1232_612&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1232_612&rgn=div8'
WHERE clause_id = 72559; -- 52.232-12 Alternate III

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1232_612&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1232_612&rgn=div8'
WHERE clause_id = 72560; -- 52.232-12 Alternate IV

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1232_612&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1232_612&rgn=div8'
WHERE clause_id = 72561; -- 52.232-12 Alternate V

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1232_616&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1232_616&rgn=div8'
WHERE clause_id = 72565; -- 52.232-16 Alternate I

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1232_616&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1232_616&rgn=div8'
WHERE clause_id = 72566; -- 52.232-16 Alternate II

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1232_616&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1232_616&rgn=div8'
WHERE clause_id = 72567; -- 52.232-16 Alternate III

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1232_623&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1232_623&rgn=div8'
WHERE clause_id = 72575; -- 52.232-23 Alternate I

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1232_625&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1232_625&rgn=div8'
WHERE clause_id = 72578; -- 52.232-25 Alternate I

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1232_628&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1232_628&rgn=div8'
WHERE clause_id = 72582; -- 52.232-28 Alternate I

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1233_61&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1233_61&rgn=div8'
WHERE clause_id = 72603; -- 52.233-1 Alternate I

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1233_63&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1233_63&rgn=div8'
WHERE clause_id = 72606; -- 52.233-3 Alternate I

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1236_613&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1236_613&rgn=div8'
WHERE clause_id = 72618; -- 52.236-13 Alternate I

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1236_616&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1236_616&rgn=div8'
WHERE clause_id = 72621; -- 52.236-16 Alternate I

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1236_621&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1236_621&rgn=div8'
WHERE clause_id = 72627; -- 52.236-21 Alternate I

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1236_621&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1236_621&rgn=div8'
WHERE clause_id = 72628; -- 52.236-21 Alternate II

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1236_627&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1236_627&rgn=div8'
WHERE clause_id = 72635; -- 52.236-27 Alternate I

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1237_64&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1237_64&rgn=div8'
WHERE clause_id = 72650; -- 52.237-4 Alternate I

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1241_69&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1241_69&rgn=div8'
WHERE clause_id = 72670; -- 52.241-9 Alternate I

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1242_615&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1242_615&rgn=div8'
WHERE clause_id = 72675; -- 52.242-15 Alternate I

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1243_61&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1243_61&rgn=div8'
WHERE clause_id = 72681; -- 52.243-1 Alternate I

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1243_61&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1243_61&rgn=div8'
WHERE clause_id = 72682; -- 52.243-1 Alternate II

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1243_61&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1243_61&rgn=div8'
WHERE clause_id = 72683; -- 52.243-1 Alternate III

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1243_61&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1243_61&rgn=div8'
WHERE clause_id = 72684; -- 52.243-1 Alternate IV

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1243_61&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1243_61&rgn=div8'
WHERE clause_id = 72685; -- 52.243-1 Alternate V

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1243_62&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1243_62&rgn=div8'
WHERE clause_id = 72687; -- 52.243-2 Alternate I

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1243_62&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1243_62&rgn=div8'
WHERE clause_id = 72688; -- 52.243-2 Alternate II

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1243_62&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1243_62&rgn=div8'
WHERE clause_id = 72689; -- 52.243-2 Alternate III

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1243_62&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1243_62&rgn=div8'
WHERE clause_id = 72690; -- 52.243-2 Alternate V

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1244_62&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1244_62&rgn=div8'
WHERE clause_id = 72698; -- 52.244-2 Alternate I

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1245_61&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1245_61&rgn=div8'
WHERE clause_id = 72702; -- 52.245-1 Alternate I

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1245_61&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1245_61&rgn=div8'
WHERE clause_id = 72703; -- 52.245-1 Alternate II

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1246_617&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1246_617&rgn=div8'
WHERE clause_id = 72715; -- 52.246-17 Alternate II

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1246_617&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1246_617&rgn=div8'
WHERE clause_id = 72716; -- 52.246-17 Alternate III

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1246_617&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1246_617&rgn=div8'
WHERE clause_id = 72717; -- 52.246-17 Alternate IV

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1246_617&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1246_617&rgn=div8'
WHERE clause_id = 72714; -- 52.246-17 Alternate V

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1246_618&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1246_618&rgn=div8'
WHERE clause_id = 72719; -- 52.246-18 Alternate II

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1246_618&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1246_618&rgn=div8'
WHERE clause_id = 72720; -- 52.246-18 Alternate III

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1246_618&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1246_618&rgn=div8'
WHERE clause_id = 72721; -- 52.246-18 Alternate IV

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1246_619&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1246_619&rgn=div8'
WHERE clause_id = 72723; -- 52.246-19 Alternate I

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1246_619&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1246_619&rgn=div8'
WHERE clause_id = 72724; -- 52.246-19 Alternate II

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1246_619&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1246_619&rgn=div8'
WHERE clause_id = 72725; -- 52.246-19 Alternate III

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1246_62&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1246_62&rgn=div8'
WHERE clause_id = 72727; -- 52.246-2 Alternate I

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1246_62&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1246_62&rgn=div8'
WHERE clause_id = 72728; -- 52.246-2 Alternate II

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1246_621&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1246_621&rgn=div8'
WHERE clause_id = 72731; -- 52.246-21 Alternate I

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1246_624&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1246_624&rgn=div8'
WHERE clause_id = 72734; -- 52.246-24 Alternate I

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1246_66&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1246_66&rgn=div8'
WHERE clause_id = 72740; -- 52.246-6 Alternate I

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1246_68&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1246_68&rgn=div8'
WHERE clause_id = 72743; -- 52.246-8 Alternate I

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1247_63&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1247_63&rgn=div8'
WHERE clause_id = 72768; -- 52.247-3 Alternate I

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?node=se48.2.52_1247_651&amp;rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=31961bd8c221b0a8c46e7a6af888422a&amp;node=se48.2.52_1247_651&amp;rgn=div8'
WHERE clause_id = 72793; -- 52.247-51 Alternate I

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?node=se48.2.52_1247_651&amp;rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=31961bd8c221b0a8c46e7a6af888422a&amp;node=se48.2.52_1247_651&amp;rgn=div8'
WHERE clause_id = 72794; -- 52.247-51 Alternate II

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?node=se48.2.52_1247_651&amp;rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=31961bd8c221b0a8c46e7a6af888422a&amp;node=se48.2.52_1247_651&amp;rgn=div8'
WHERE clause_id = 72795; -- 52.247-51 Alternate III

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1247_664&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1247_664&rgn=div8'
WHERE clause_id = 72810; -- 52.247-64 Alternate I

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1247_664&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1247_664&rgn=div8'
WHERE clause_id = 72811; -- 52.247-64 Alternate II

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?node=se48.2.52_1248_61&amp;rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=1e45c9c95cc0fa5020d0a83c2da53fb4&amp;node=se48.2.52_1248_61&amp;rgn=div8'
WHERE clause_id = 72820; -- 52.248-1 Alternate I

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?node=se48.2.52_1248_61&amp;rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=1e45c9c95cc0fa5020d0a83c2da53fb4&amp;node=se48.2.52_1248_61&amp;rgn=div8'
WHERE clause_id = 72821; -- 52.248-1 Alternate II

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?node=se48.2.52_1248_61&amp;rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=1e45c9c95cc0fa5020d0a83c2da53fb4&amp;node=se48.2.52_1248_61&amp;rgn=div8'
WHERE clause_id = 72822; -- 52.248-1 Alternate III

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1248_63&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1248_63&rgn=div8'
WHERE clause_id = 72825; -- 52.248-3 Alternate I

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1249_61&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1249_61&rgn=div8'
WHERE clause_id = 72827; -- 52.249-1 Alternate I

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1249_610&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1249_610&rgn=div8'
WHERE clause_id = 72830; -- 52.249-10 Alternate I

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1249_610&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1249_610&rgn=div8'
WHERE clause_id = 72831; -- 52.249-10 Alternate II

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1249_610&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1249_610&rgn=div8'
WHERE clause_id = 72829; -- 52.249-10 Alternate III

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1249_62&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1249_62&rgn=div8'
WHERE clause_id = 72835; -- 52.249-2 Alternate I

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1249_62&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1249_62&rgn=div8'
WHERE clause_id = 72836; -- 52.249-2 Alternate II

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1249_62&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1249_62&rgn=div8'
WHERE clause_id = 72837; -- 52.249-2 Alternate III

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1249_63&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1249_63&rgn=div8'
WHERE clause_id = 72839; -- 52.249-3 Alternate I

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1249_66&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1249_66&rgn=div8'
WHERE clause_id = 72843; -- 52.249-6 Alternate I

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1249_66&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1249_66&rgn=div8'
WHERE clause_id = 72844; -- 52.249-6 Alternate II

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1249_66&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1249_66&rgn=div8'
WHERE clause_id = 72845; -- 52.249-6 Alternate III

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1249_66&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1249_66&rgn=div8'
WHERE clause_id = 72846; -- 52.249-6 Alternate IV

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1249_66&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1249_66&rgn=div8'
WHERE clause_id = 72847; -- 52.249-6 Alternate V

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1249_68&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1249_68&rgn=div8'
WHERE clause_id = 72850; -- 52.249-8 Alternate I

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1250_61&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1250_61&rgn=div8'
WHERE clause_id = 72853; -- 52.250-1 Alternate I

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1250_63&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1250_63&rgn=div8'
WHERE clause_id = 72856; -- 52.250-3 Alternate I

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1250_63&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1250_63&rgn=div8'
WHERE clause_id = 72857; -- 52.250-3 Alternate II

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1250_64&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1250_64&rgn=div8'
WHERE clause_id = 72860; -- 52.250-4 Alternate I

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1250_64&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?SID=b18e8a9f34ab3c4ced18008db1af7ab5&mc=true&node=se48.2.52_1250_64&rgn=div8'
WHERE clause_id = 72859; -- 52.250-4 Alternate II

/*
*** End of SQL Scripts at Wed Jun 17 07:36:17 EDT 2015 ***
*/
