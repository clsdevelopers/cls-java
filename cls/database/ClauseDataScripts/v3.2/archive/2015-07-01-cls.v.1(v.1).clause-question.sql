/*
Clause Parser Run at Wed Jul 01 10:36:01 EDT 2015
(Without Correction Information)
*/
/* ===============================================
 * Prescriptions
   =============================================== */
/* ===============================================
 * Questions
   =============================================== */

-- ProduceClause.resolveIssues() Summary: Total(1212); Merged(13); Corrected(336); Error(1)
/* ===============================================
 * Clause
   =============================================== */

UPDATE Clauses
SET clause_data = '<p>Substitute the following paragraphs (d) and (e) for paragraph (d) of the provision at FAR 52.204-8:</p><p>(d)(1) The following representations or certifications in the System for Award Management (SAM) database are applicable to this solicitation as indicated:</p><p>(i) 252.209-7003, Reserve Officer Training Corps and Military Recruiting on Campus-Representation. Applies to all solicitations with institutions of higher education.</p><p>(ii) 252.216-7008, Economic Price Adjustment-Wage Rates or Material Prices Controlled by a Foreign Government. Applies to solicitations for fixed-price supply and service contracts when the contract is to be performed wholly or in part in a foreign country, and a foreign government controls wage rates or material prices and may during contract performance impose a mandatory change in wages or prices of materials.</p><p>(iii) 252.222-7007, Representation Regarding Combating Trafficking in Persons, as prescribed in 222.1771. Applies to solicitations with a value expected to exceed the simplified acquisition threshold.</p><p>(iv) 252.225-7042, Authorization to Perform. Applies to all solicitations when performance will be wholly or in part in a foreign country.</p><p>(v) 252.225-7049, Prohibition on Acquisition of Commercial Satellite Services from Certain Foreign Entities-Representations. Applies to solicitations for the acquisition of commercial satellite services.</p><p>(vi) 252.225-7050, Disclosure of Ownership or Control by the Government of a Country that is a State Sponsor of Terrorism. Applies to all solicitations expected to result in contracts of $150,000 or more.</p><p>(vii) 252.229-7012, Tax Exemptions (Italy)-Representation. Applies to solicitations when contract performance will be in Italy.</p><p>(viii) 252.229-7013, Tax Exemptions (Spain)-Representation. Applies to solicitations when contract performance will be in Spain.</p><p>(ix) 252.247-7022, Representation of Extent of Transportation by Sea. Applies to all solicitations except those for direct purchase of ocean transportation services or those with an anticipated value at or below the simplified acquisition threshold.</p><p>(2) The following representations or certifications in SAM are applicable to this solicitation as indicated by the Contracting Officer: [<i>Contracting Officer check as appropriate.</i>]</p><p>{{textbox_252.204-7007[0]}}(i) 252.209-7002, Disclosure of Ownership or Control by a Foreign Government.</p><p>{{textbox_252.204-7007[1]}}(ii) 252.225-7000, Buy American-Balance of Payments Program Certificate.</p><p>{{textbox_252.204-7007[2]}}(iii) 252.225-7020, Trade Agreements Certificate.</p><p>{{textbox_252.204-7007[3]}}Use with Alternate I.</p><p>{{textbox_252.204-7007[4]}}(iv) 252.225-7031, Secondary Arab Boycott of Israel.</p><p>{{textbox_252.204-7007[5]}}(v) 252.225-7035, Buy American-Free Trade Agreements-Balance of Payments Program Certificate.</p><p>{{textbox_252.204-7007[6]}}Use with Alternate I.</p><p>{{textbox_252.204-7007[7]}}Use with Alternate II.</p><p>{{textbox_252.204-7007[8]}}Use with Alternate III.</p><p>{{textbox_252.204-7007[9]}}Use with Alternate IV.</p><p>{{textbox_252.204-7007[10]}}Use with Alternate V.</p><p>(e) The offeror has completed the annual representations and certifications electronically via the SAM Web site at <i>https://www.acquisition.gov/.</i> After reviewing the ORCA database information, the offeror verifies by submission of the offer that the representations and certifications currently posted electronically that apply to this solicitation as indicated in FAR 52.204-8(c) and paragraph (d) of this provision have been entered or updated within the last 12 months, are current, accurate, complete, and applicable to this solicitation (including the business size standard applicable to the NAICS code referenced for this solicitation), as of the date of this offer, and are incorporated in this offer by reference (see FAR 4.1201); except for the changes identified below [<i>offeror to insert changes, identifying change by provision number, title, date</i>]. Any changes provided by the offeror are applicable to this solicitation only, and do not result in an update to the representations and certifications located in the SAM database.</p><div><div><table border="1" cellpadding="1" cellspacing="1" frame="void" width="100%"><tbody><tr><th scope="col">FAR/DFARS provision No.</th><th scope="col">Title</th><th scope="col">Date</th><th scope="col">Change</th></tr><tr><td>{{textbox_252.204-7007[11]}}</td><td>{{textbox_252.204-7007[12]}}</td><td>{{textbox_252.204-7007[13]}}</td><td>{{textbox_252.204-7007[14]}}</td></tr></tbody></table></div></div><p>Any changes provided by the offeror are applicable to this solicitation only, and do not result in an update to the representations and certifications posted on ORCA.</p>'
--               '<p>Substitute the following paragraphs (d) and (e) for paragraph (d) of the provision at FAR 52.204-8:</p><p>(d)(1) The following representations or certifications in the System for Award Management (SAM) database are applicable to this solicitation as indicated:</p><p>(i) 252.209-7003, Reserve Officer Training Corps and Military Recruiting on Campus-Representation. Applies to all solicitations with institutions of higher education.</p><p>(ii) 252.216-7008, Economic Price Adjustment-Wage Rates or Material Prices Controlled by a Foreign Government. Applies to solicitations for fixed-price supply and service contracts when the contract is to be performed wholly or in part in a foreign country, and a foreign government controls wage rates or material prices and may during contract performance impose a mandatory change in wages or prices of materials.</p><p>(iii) 252.222-7007, Representation Regarding Combating Trafficking in Persons, as prescribed in 222.1771. Applies to solicitations with a value expected to exceed the simplified acquisition threshold.</p><p>(iv) 252.225-7042, Authorization to Perform. Applies to all solicitations when performance will be wholly or in part in a foreign country.</p><p>(v) 252.225-7049, Prohibition on Acquisition of Commercial Satellite Services from Certain Foreign Entities-Representations. Applies to solicitations for the acquisition of commercial satellite services.</p><p>(vi) 252.225-7050, Disclosure of Ownership or Control by the Government of a Country that is a State Sponsor of Terrorism. Applies to all solicitations expected to result in contracts of $150,000 or more.</p><p>(vii) 252.229-7012, Tax Exemptions (Italy)-Representation. Applies to solicitations when contract performance will be in Italy.</p><p>(viii) 252.229-7013, Tax Exemptions (Spain)-Representation. Applies to solicitations when contract performance will be in Spain.</p><p>(ix) 252.247-7022, Representation of Extent of Transportation by Sea. Applies to all solicitations except those for direct purchase of ocean transportation services or those with an anticipated value at or below the simplified acquisition threshold.</p><p>(2) The following representations or certifications in SAM are applicable to this solicitation as indicated by the Contracting Officer: [<i>Contracting Officer check as appropriate.</i>]</p><p>{{textbox_252.204-7007[0]}}(i) 252.209-7002, Disclosure of Ownership or Control by a Foreign Government.</p><p>{{textbox_252.204-7007[1]}}(ii) 252.225-7000, Buy American-Balance of Payments Program Certificate.</p><p>{{textbox_252.204-7007[2]}}(iii) 252.225-7020, Trade Agreements Certificate.</p><p>{{textbox_252.204-7007[3]}}Use with Alternate I.</p><p>{{textbox_252.204-7007[4]}}(iv) 252.225-7031, Secondary Arab Boycott of Israel.</p><p>{{textbox_252.204-7007[5]}}(v) 252.225-7035, Buy American-Free Trade Agreements-Balance of Payments Program Certificate.</p><p>{{textbox_252.204-7007[6]}}Use with Alternate I.</p><p>{{textbox_252.204-7007[7]}}Use with Alternate II.</p><p>{{textbox_252.204-7007[8]}}Use with Alternate III.</p><p>{{textbox_252.204-7007[9]}}Use with Alternate IV.</p><p>{{textbox_252.204-7007[10]}}Use with Alternate V.</p><p>(e) The offeror has completed the annual representations and certifications electronically via the SAM Web site at <i>https://www.acquisition.gov/.</i> After reviewing the ORCA database information, the offeror verifies by submission of the offer that the representations and certifications currently posted electronically that apply to this solicitation as indicated in FAR 52.204-8(c) and paragraph (d) of this provision have been entered or updated within the last 12 months, are current, accurate, complete, and applicable to this solicitation (including the business size standard applicable to the NAICS code referenced for this solicitation), as of the date of this offer, and are incorporated in this offer by reference (see FAR 4.1201); except for the changes identified below [<i>offeror to insert changes, identifying change by provision number, title, date</i>]. Any changes provided by the offeror are applicable to this solicitation only, and do not result in an update to the representations and certifications located in the SAM database.</p><div><div><table border="1" cellpadding="1" cellspacing="1" frame="void" width="100%"><tbody><tr><th scope="col">FAR/DFARS provision No.</th><th scope="col">Title</th><th scope="col">Date</th><th scope="col">Change</th></tr><tr><td align="left" scope="row">&nbsp;&nbsp;&nbsp;</td><td>{{textbox_252.204-7007[11]}}</td><td>{{textbox_252.204-7007[12]}}</td><td>{{textbox_252.204-7007[13]}}</td></tr></tbody></table></div></div><p>Any changes provided by the offeror are applicable to this solicitation only, and do not result in an update to the representations and certifications posted on ORCA.</p>'
WHERE clause_id = 93268; -- 252.204-7007
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (93268, 'textbox_252.204-7007[14]', 'S', 100);

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1208_67000&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?node=se48.3.252_1208_67000&amp;rgn=div8'
, clause_data = '<p>(a) The Government intends to furnish precious metals required in the manufacture of items to be delivered under the contract if the Contracting Officer determines it to be in the Government''s best interest. The use of Government-furnished silver is mandatory when the quantity required is one hundred troy ounces or more. The precious metal(s) will be furnished pursuant to the Government Furnished Property clause of the contract.</p><p>(b) The Offeror shall cite the type (silver, gold, platinum, palladium, iridium, rhodium, and ruthenium) and quantity in whole troy ounces of precious metals required in the performance of this contract (including precious metals required for any first article or production sample), and shall specify the national stock number (NSN) and nomenclature, if known, of the deliverable item requiring precious metals.</p><div><div><table border="1" cellpadding="1" cellspacing="1" frame="void" width="100%"><tbody><tr><th scope="col">Precious metal*</th><th scope="col">Quantity</th><th scope="col">Deliverable item (NSN and nomenclature)!!rs</th></tr><tr><td>{{textbox_252.208-7000[0]}}</td></tr></tbody></table></div><div><p>*If platinum or palladium, specify whether sponge or granules are required.</p></div></div><p>(c) Offerors shall submit two prices for each deliverable item which contains precious metals-one based on the Government furnishing precious metals, and one based on the Contractor furnishing precious metals. Award will be made on the basis which is in the best interest of the Government.</p><p>(d) The Contractor agrees to insert this clause, including this paragraph (d), in solicitations for subcontracts and purchase orders issued in performance of this contract, unless the Contractor knows that the item being purchased contains no precious metals.</p>'
--               '<p>(a) The Government intends to furnish precious metals required in the manufacture of items to be delivered under the contract if the Contracting Officer determines it to be in the Government''s best interest. The use of Government-furnished silver is mandatory when the quantity required is one hundred troy ounces or more. The precious metal(s) will be furnished pursuant to the Government Furnished Property clause of the contract.</p><p>(b) The Offeror shall cite the type (silver, gold, platinum, palladium, iridium, rhodium, and ruthenium) and quantity in whole troy ounces of precious metals required in the performance of this contract (including precious metals required for any first article or production sample), and shall specify the national stock number (NSN) and nomenclature, if known, of the deliverable item requiring precious metals.</p>\n<div><div>\n<table border="&quot;1&quot;" cellpadding="&quot;1&quot;" cellspacing="&quot;1&quot;" frame="&quot;void&quot;" width="&quot;100%&quot;"><tbody><tr><th scope="&quot;col&quot;">Precious metal*</th><th scope="&quot;col&quot;">Quantity</th><th scope="&quot;col&quot;">Deliverable item (NSN and nomenclature)!!rs</th></tr>\n<tr><td>{{textbox_252.208-7000[0]}}</td><td>{{textbox_252.208-7000[1]}}</td><td>{{textbox_252.208-7000[2]}}</td></tr>\n<tr><td>{{textbox_252.208-7000[3]}}</td><td>{{textbox_252.208-7000[4]}}</td><td>{{textbox_252.208-7000[5]}}</td></tr>\n<tr><td>{{textbox_252.208-7000[6]}}</td><td>{{textbox_252.208-7000[7]}}</td><td>{{textbox_252.208-7000[8]}}</td></tr>\n<tr><td>{{textbox_252.208-7000[9]}}</td><td>{{textbox_252.208-7000[10]}}</td><td>{{textbox_252.208-7000[11]}}</td></tr>\n<tr><td>{{textbox_252.208-7000[12]}}</td><td>{{textbox_252.208-7000[13]}}</td><td>{{textbox_252.208-7000[14]}}</td></tr>\n</tbody></table>\n</div><div><p>*If platinum or palladium, specify whether sponge or granules are required.</p></div></div><p>(c) Offerors shall submit two prices for each deliverable item which contains precious metals-one based on the Government furnishing precious metals, and one based on the Contractor furnishing precious metals. Award will be made on the basis which is in the best interest of the Government.</p><p>(d) The Contractor agrees to insert this clause, including this paragraph (d), in solicitations for subcontracts and purchase orders issued in performance of this contract, unless the Contractor knows that the item being purchased contains no precious metals.</p>'
WHERE clause_id = 93277; -- 252.208-7000
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 28216; -- textbox_252.208-7000[10]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 28217; -- textbox_252.208-7000[11]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 28218; -- textbox_252.208-7000[12]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 28219; -- textbox_252.208-7000[13]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 28220; -- textbox_252.208-7000[14]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 28207; -- textbox_252.208-7000[1]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 28208; -- textbox_252.208-7000[2]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 28209; -- textbox_252.208-7000[3]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 28210; -- textbox_252.208-7000[4]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 28211; -- textbox_252.208-7000[5]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 28212; -- textbox_252.208-7000[6]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 28213; -- textbox_252.208-7000[7]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 28214; -- textbox_252.208-7000[8]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 28215; -- textbox_252.208-7000[9]

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1209_67002&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?node=se48.3.252_1209_67002&amp;rgn=div8'
, clause_data = '<p>(a) <i>Definitions.</i> As used in this provision-</p><p>(1) <i>Effectively owned or controlled</i> means that a foreign government or any entity controlled by a foreign government has the power, either directly or indirectly, whether exercised or exercisable, to control the election, appointment, or tenure of the Offeror''s officers or a majority of the Offeror''s board of directors by any means, e.g., ownership, contract, or operation of law (or equivalent power for unincorporated organizations).</p><p>(2) <i>Entity controlled by a foreign government</i>-</p><p>(i) Means-</p><p>(A) Any domestic or foreign organization or corporation that is effectively owned or controlled by a foreign government; or</p><p>(B) Any individual acting on behalf of a foreign government.</p><p>(ii) Does not include an organization or corporation that is owned, but is not controlled, either directly or indirectly, by a foreign government if the ownership of that organization or corporation by that foreign government was effective before October 23, 1992.</p><p>(3) <i>Foreign government</i> includes the state and the government of any country (other than the United States and its outlying areas) as well as any political subdivision, agency, or instrumentality thereof.</p><p>(4) <i>Proscribed information</i> means-</p><p>(i) Top Secret information;</p><p>(ii) Communications security (COMSEC) material, excluding controlled cryptographic items when unkeyed or utilized with unclassified keys;</p><p>(iii) Restricted Data as defined in the U.S. Atomic Energy Act of 1954, as amended;</p><p>(iv) Special Access Program (SAP) information; or</p><p>(v) Sensitive Compartmented Information (SCI).</p><p>(b) <i>Prohibition on award.</i> No contract under a national security program may be awarded to an entity controlled by a foreign government if that entity requires access to proscribed information to perform the contract, unless the Secretary of Defense or a designee has waived application of 10 U.S.C. 2536(a).</p><p>(c) <i>Disclosure.</i> The Offeror shall disclose any interest a foreign government has in the Offeror when that interest constitutes control by a foreign government as defined in this provision. If the Offeror is a subsidiary, it shall also disclose any reportable interest a foreign government has in any entity that owns or controls the subsidiary, including reportable interest concerning the Offeror''s immediate parent, intermediate parents, and the ultimate parent. Use separate paper as needed, and provide the information in the following format: Offeror''s Point of Contact for Questions about Disclosure (Name and Phone Number with Country Code, City Code and Area Code, as applicable)</p><div><div><table border="1" cellpadding="1" cellspacing="1" frame="void" width="100%"><tbody><tr><td align="left" scope="row">Name and Address of Offeror</td><td align="left"></td></tr><tr><td align="left" scope="row">Name and Address of Entity Controlled by a Foreign Government</td><td align="left">Description of Interest, Ownership Percentage, and Identification of Foreign Government</td></tr></tbody></table></div></div>'
--               '<p>(a) <i>Definitions.</i> As used in this provision-</p><p>(1) <i>Effectively owned or controlled</i> means that a foreign government or any entity controlled by a foreign government has the power, either directly or indirectly, whether exercised or exercisable, to control the election, appointment, or tenure of the Offeror''s officers or a majority of the Offeror''s board of directors by any means, e.g., ownership, contract, or operation of law (or equivalent power for unincorporated organizations).</p><p>(2) <i>Entity controlled by a foreign government</i>-</p><p>(i) Means-</p><p>(A) Any domestic or foreign organization or corporation that is effectively owned or controlled by a foreign government; or</p><p>(B) Any individual acting on behalf of a foreign government.</p><p>(ii) Does not include an organization or corporation that is owned, but is not controlled, either directly or indirectly, by a foreign government if the ownership of that organization or corporation by that foreign government was effective before October 23, 1992.</p><p>(3) <i>Foreign government</i> includes the state and the government of any country (other than the United States and its outlying areas) as well as any political subdivision, agency, or instrumentality thereof.</p><p>(4) <i>Proscribed information</i> means-</p><p>(i) Top Secret information;</p><p>(ii) Communications security (COMSEC) material, excluding controlled cryptographic items when unkeyed or utilized with unclassified keys;</p><p>(iii) Restricted Data as defined in the U.S. Atomic Energy Act of 1954, as amended;</p><p>(iv) Special Access Program (SAP) information; or</p><p>(v) Sensitive Compartmented Information (SCI).</p><p>(b) <i>Prohibition on award.</i> No contract under a national security program may be awarded to an entity controlled by a foreign government if that entity requires access to proscribed information to perform the contract, unless the Secretary of Defense or a designee has waived application of 10 U.S.C. 2536(a).</p><p>(c) <i>Disclosure.</i> The Offeror shall disclose any interest a foreign government has in the Offeror when that interest constitutes control by a foreign government as defined in this provision. If the Offeror is a subsidiary, it shall also disclose any reportable interest a foreign government has in any entity that owns or controls the subsidiary, including reportable interest concerning the Offeror''s immediate parent, intermediate parents, and the ultimate parent. Use separate paper as needed, and provide the information in the following format: Offeror''s Point of Contact for Questions about Disclosure (Name and Phone Number with Country Code, City Code and Area Code, as applicable)</p>\n<div><div>\n<table border="&quot;1&quot;" cellpadding="&quot;1&quot;" cellspacing="&quot;1&quot;" frame="&quot;void&quot;" width="&quot;100%&quot;"><tbody><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">Name and Address of Offeror</td><td align="&quot;left&quot;">{{textbox_252.209-7002[0]}}</td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">Name and Address of Entity Controlled by a Foreign Government</td><td align="&quot;left&quot;">Description of Interest, Ownership Percentage, and Identification of Foreign Government</td></tr>\n<tr><td>{{textbox_252.209-7002[1]}}</td><td>{{textbox_252.209-7002[2]}}</td></tr>\n<tr><td>{{textbox_252.209-7002[3]}}</td><td>{{textbox_252.209-7002[4]}}</td></tr>\n<tr><td>{{textbox_252.209-7002[5]}}</td><td>{{textbox_252.209-7002[6]}}</td></tr>\n<tr><td>{{textbox_252.209-7002[7]}}</td><td>{{textbox_252.209-7002[8]}}</td></tr>\n<tr><td>{{textbox_252.209-7002[9]}}</td><td>{{textbox_252.209-7002[10]}}</td></tr>\n</tbody></table></div></div>'
WHERE clause_id = 93278; -- 252.209-7002
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 28221; -- textbox_252.209-7002[0]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 28231; -- textbox_252.209-7002[10]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 28222; -- textbox_252.209-7002[1]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 28223; -- textbox_252.209-7002[2]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 28224; -- textbox_252.209-7002[3]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 28225; -- textbox_252.209-7002[4]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 28226; -- textbox_252.209-7002[5]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 28227; -- textbox_252.209-7002[6]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 28228; -- textbox_252.209-7002[7]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 28229; -- textbox_252.209-7002[8]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 28230; -- textbox_252.209-7002[9]

UPDATE Clauses
SET effective_date = '2013-10-01' -- '2013-01-01'
WHERE clause_id = 93289; -- 252.209-7994 (DEVIATION 2014-00004)

UPDATE Clauses
SET effective_date = '2013-04-01' -- '2013-01-01'
WHERE clause_id = 93290; -- 252.209-7995 (DEVIATION 2013-00010)

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1211_67003&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?node=se48.3.252_1211_67003&amp;rgn=div8'
, clause_data = '<p>(a) <i>Definitions.</i> As used in this clause-</p><p><i>Automatic identification device</i> means a device, such as a reader or interrogator, used to retrieve data encoded on machine-readable media.</p><p><i>Concatenated unique item identifier</i> means-</p><p>(1) For items that are serialized within the enterprise identifier, the linking together of the unique identifier data elements in order of the issuing agency code, enterprise identifier, and unique serial number within the enterprise identifier; or</p><p>(2) For items that are serialized within the original part, lot, or batch number, the linking together of the unique identifier data elements in order of the issuing agency code; enterprise identifier; original part, lot, or batch number; and serial number within the original part, lot, or batch number.</p><p><i>Data matrix</i> means a two-dimensional matrix symbology, which is made up of square or, in some cases, round modules arranged within a perimeter finder pattern and uses the Error Checking and Correction 200 (ECC200) specification found within International Standards Organization (ISO)/International Electrotechnical Commission (IEC) 16022.</p><p><i>Data qualifier</i> means a specified character (or string of characters) that immediately precedes a data field that defines the general category or intended use of the data that follows.</p><p><i>DoD recognized unique identification equivalent</i> means a unique identification method that is in commercial use and has been recognized by DoD. All DoD recognized unique identification equivalents are listed at <i>http://www.acq.osd.mil/dpap/pdi/uid/iuid_equivalents.html.</i> </p><p><i>DoD item unique identification</i> means a system of marking items delivered to DoD with unique item identifiers that have machine-readable data elements to distinguish an item from all other like and unlike items. For items that are serialized within the enterprise identifier, the unique item identifier shall include the data elements of the enterprise identifier and a unique serial number. For items that are serialized within the part, lot, or batch number within the enterprise identifier, the unique item identifier shall include the data elements of the enterprise identifier; the original part, lot, or batch number; and the serial number.</p><p><i>Enterprise</i> means the entity (<i>e.g.</i>, a manufacturer or vendor) responsible for assigning unique item identifiers to items.</p><p><i>Enterprise identifier</i> means a code that is uniquely assigned to an enterprise by an issuing agency.</p><p><i>Government''s unit acquisition cost</i> means-</p><p>(1) For fixed-price type line, subline, or exhibit line items, the unit price identified in the contract at the time of delivery;</p><p>(2) For cost-type or undefinitized line, subline, or exhibit line items, the Contractor''s estimated fully burdened unit cost to the Government at the time of delivery; and</p><p>(3) For items produced under a time-and-materials contract, the Contractor''s estimated fully burdened unit cost to the Government at the time of delivery.</p><p><i>Issuing agency</i> means an organization responsible for assigning a globally unique identifier to an enterprise (e.g., Dun & Bradstreet''s Data Universal Numbering System (DUNS) Number, GS1 Company Prefix, Allied Committee 135 NATO Commercial and Government Entity (NCAGE)/Commercial and Government Entity (CAGE) Code, or the Coded Representation of the North American Telecommunications Industry Manufacturers, Suppliers, and Related Service Companies (ATIS-0322000) Number), European Health Industry Business Communication Council (EHIBCC) and Health Industry Business Communication Council (HIBCC)), as indicated in the Register of Issuing Agency Codes for ISO/IEC 15459, located at <i>http://www.nen.nl/Normontwikkeling/Certificatieschemas-en-keurmerken/Schemabeheer/ISOIEC-15459.htm</i></p><p><i>Issuing agency code</i> means a code that designates the registration (or controlling) authority for the enterprise identifier.</p><p><i>Item</i> means a single hardware article or a single unit formed by a grouping of subassemblies, components, or constituent parts.</p><p><i>Lot or batch number</i> means an identifying number assigned by the enterprise to a designated group of items, usually referred to as either a lot or a batch, all of which were manufactured under identical conditions.</p><p><i>Machine-readable</i> means an automatic identification technology media, such as bar codes, contact memory buttons, radio frequency identification, or optical memory cards.</p><p><i>Original part number</i> means a combination of numbers or letters assigned by the enterprise at item creation to a class of items with the same form, fit, function, and interface.</p><p><i>Parent item</i> means the item assembly, intermediate component, or subassembly that has an embedded item with a unique item identifier or DoD recognized unique identification equivalent.</p><p><i>Serial number within the enterprise identifier</i> means a combination of numbers, letters, or symbols assigned by the enterprise to an item that provides for the differentiation of that item from any other like and unlike item and is never used again within the enterprise.</p><p><i>Serial number within the part, lot, or batch number</i> means a combination of numbers or letters assigned by the enterprise to an item that provides for the differentiation of that item from any other like item within a part, lot, or batch number assignment.</p><p><i>Serialization within the enterprise identifier</i> means each item produced is assigned a serial number that is unique among all the tangible items produced by the enterprise and is never used again. The enterprise is responsible for ensuring unique serialization within the enterprise identifier.</p><p><i>Serialization within the part, lot, or batch number</i> means each item of a particular part, lot, or batch number is assigned a unique serial number within that part, lot, or batch number assignment. The enterprise is responsible for ensuring unique serialization within the part, lot, or batch number within the enterprise identifier.</p><p><i>Type designation</i> means a combination of letters and numerals assigned by the Government to a major end item, assembly or subassembly, as appropriate, to provide a convenient means of differentiating between items having the same basic name and to indicate modifications and changes thereto.</p><p><i>Unique item identifier</i> means a set of data elements marked on items that is globally unique and unambiguous. The term includes a concatenated unique item identifier or a DoD recognized unique identification equivalent.</p><p><i>Unique item identifier type</i> means a designator to indicate which method of uniquely identifying a part has been used. The current list of accepted unique item identifier types is maintained at <i>http://www.acq.osd.mil/dpap/pdi/uid/uii_types.html.</i> </p><p>(b) The Contractor shall deliver all items under a contract line, subline, or exhibit line item.</p><p>(c) <i>Unique item identifier.</i> (1) The Contractor shall provide a unique item identifier for the following:</p><p>(i) Delivered items for which the Government''s unit acquisition cost is $5,000 or more, except for the following line items:</p><div><div><table border="1" cellpadding="1" cellspacing="1" frame="void" width="100%"><tbody><tr><th scope="col">Contract line, subline, or exhibit line item No.</th><th scope="col">Item description</th></tr><tr><td>{{textbox_252.211-7003[0]}}</td><td>{{textbox_252.211-7003[1]}}</td></tr></tbody></table></div></div><p>(ii) Items for which the Government''s unit acquisition cost is less than $5,000 that are identified in the Schedule or the following table:</p><div><div><table border="1" cellpadding="1" cellspacing="1" frame="void" width="100%"><tbody><tr><th scope="col">Contract line, subline, or exhibit line item No.</th><th scope="col">Item description</th></tr><tr><td>{{textbox_252.211-7003[2]}}</td><td>{{textbox_252.211-7003[3]}}</td></tr></tbody></table></div></div><p>(<i>If items are identified in the Schedule, insert ''See Schedule'' in this table.</i>)</p><p>(iii) Subassemblies, components, and parts embedded within delivered items, items with warranty requirements, DoD serially managed reparables and DoD serially managed nonreparables as specified in Attachment Number {{textbox_252.211-7003[4]}}.</p><p>(iv) Any item of special tooling or special test equipment as defined in FAR 2.101 that have been designated for preservation and storage for a Major Defense Acquisition Program as specified in Attachment Number {{textbox_252.211-7003[5]}}.</p><p>(v) Any item not included in paragraphs (c)(1)(i), (ii), (iii), or (iv) of this clause for which the contractor creates and marks a unique item identifier for traceability.</p><p>(2) The unique item identifier assignment and its component data element combination shall not be duplicated on any other item marked or registered in the DoD Item Unique Identification Registry by the contractor.</p><p>(3) The unique item identifier component data elements shall be marked on an item using two dimensional data matrix symbology that complies with ISO/IEC International Standard 16022, Information technology-International symbology specification-Data matrix; ECC200 data matrix specification.</p><p>(4) <i>Data syntax and semantics of unique item identifiers.</i> The Contractor shall ensure that-</p><p>(i) The data elements (except issuing agency code) of the unique item identifier are encoded within the data matrix symbol that is marked on the item using one of the following three types of data qualifiers, as determined by the Contractor:</p><p>(A) Application Identifiers (AIs) (Format Indicator 05 of ISO/IEC International Standard 15434), in accordance with ISO/IEC International Standard 15418, Information Technology-EAN/UCC Application Identifiers and Fact Data Identifiers and Maintenance and ANSI MH 10.8.2 Data Identifier and Application Identifier Standard.</p><p>(B) Data Identifiers (DIs) (Format Indicator 06 of ISO/IEC International Standard 15434), in accordance with ISO/IEC International Standard 15418, Information Technology-EAN/UCC Application Identifiers and Fact Data Identifiers and Maintenance and ANSI MH 10.8.2 Data Identifier and Application Identifier Standard.</p><p>(C) Text Element Identifiers (TEIs) (Format Indicator 12 of ISO/IEC International Standard 15434), in accordance with the Air Transport Association Common Support Data Dictionary; and</p><p>(ii) The encoded data elements of the unique item identifier conform to the transfer structure, syntax, and coding of messages and data formats specified for Format Indicators 05, 06, and 12 in ISO/IEC International Standard 15434, Information Technology-Transfer Syntax for High Capacity Automatic Data Capture Media.</p><p>(5) <i>Unique item identifier.</i></p><p>(i) The Contractor shall-</p><p>(A) Determine whether to-</p><p>(<i>1</i>) Serialize within the enterprise identifier;</p><p>(<i>2</i>) Serialize within the part, lot, or batch number; or</p><p>(<i>3</i>) Use a DoD recognized unique identification equivalent (e.g. Vehicle Identification Number); and</p><p>(B) Place the data elements of the unique item identifier (enterprise identifier; serial number; DoD recognized unique identification equivalent; and for serialization within the part, lot, or batch number only: Original part, lot, or batch number) on items requiring marking by paragraph (c)(1) of this clause, based on the criteria provided in MIL-STD-130, Identification Marking of U.S. Military Property, latest version;</p><p>(C) Label shipments, storage containers and packages that contain uniquely identified items in accordance with the requirements of MIL-STD-129, Military Marking for Shipment and Storage, latest version; and</p><p>(D) Verify that the marks on items and labels on shipments, storage containers, and packages are machine readable and conform to the applicable standards. The contractor shall use an automatic identification technology device for this verification that has been programmed to the requirements of Appendix A, MIL-STD-130, latest version.</p><p>(ii) The issuing agency code-</p><p>(A) Shall not be placed on the item; and</p><p>(B) Shall be derived from the data qualifier for the enterprise identifier.</p><p>(d) For each item that requires item unique identification under paragraph (c)(1)(i), (ii), or (iv) of this clause or when item unique identification is provided under paragraph (c)(1)(v), in addition to the information provided as part of the Material Inspection and Receiving Report specified elsewhere in this contract, the Contractor shall report at the time of delivery, as part of the Material Inspection and Receiving Report, the following information:</p><p>(1) Unique item identifier.</p><p>(2) Unique item identifier type.</p><p>(3) Issuing agency code (if concatenated unique item identifier is used).</p><p>(4) Enterprise identifier (if concatenated unique item identifier is used).</p><p>(5) Original part number (if there is serialization within the original part number).</p><p>(6) Lot or batch number (if there is serialization within the lot or batch number).</p><p>(7) Current part number (optional and only if not the same as the original part number).</p><p>(8) Current part number effective date (optional and only if current part number is used).</p><p>(9) Serial number (if concatenated unique item identifier is used).</p><p>(10) Government''s unit acquisition cost.</p><p>(11) Unit of measure.</p><p>(12) Type designation of the item as specified in the contract schedule, if any.</p><p>(13) Whether the item is an item of Special Tooling or Special Test Equipment.</p><p>(14) Whether the item is covered by a warranty.</p><p>(e) For embedded subassemblies, components, and parts that require DoD item unique identification under paragraph (c)(1)(iii) of this clause or when item unique identification is provided under paragraph (c)(1)(v), the Contractor shall report as part of the Material Inspection and Receiving Report specified elsewhere in this contract, the following information:</p><p>(1) Unique item identifier of the parent item under paragraph (c)(1) of this clause that contains the embedded subassembly, component, or part.</p><p>(2) Unique item identifier of the embedded subassembly, component, or part.</p><p>(3) Unique item identifier type.**</p><p>(4) Issuing agency code (if concatenated unique item identifier is used).**</p><p>(5) Enterprise identifier (if concatenated unique item identifier is used).**</p><p>(6) Original part number (if there is serialization within the original part number).**</p><p>(7) Lot or batch number (if there is serialization within the lot or batch number).**</p><p>(8) Current part number (optional and only if not the same as the original part number).**</p><p>(9) Current part number effective date (optional and only if current part number is used).**</p><p>(10) Serial number (if concatenated unique item identifier is used).**</p><p>(11) Description.</p><p>** Once per item.</p><p>(f) The Contractor shall submit the information required by paragraphs (d) and (e) of this clause as follows:</p><p>(1) End items shall be reported using the receiving report capability in Wide Area WorkFlow (WAWF) in accordance with the clause at 252.232-7003. If WAWF is not required by this contract, and the contractor is not using WAWF, follow the procedures at <i>http://dodprocurementtoolbox.com/site/uidregistry/.</i></p><p>(2) Embedded items shall be reported by one of the following methods-</p><p>(i) Use of the embedded items capability in WAWF;</p><p>(ii) Direct data submission to the IUID Registry following the procedures and formats at <i>http://dodprocurementtoolbox.com/site/uidregistry/;</i> or</p><p>(iii) Via WAWF as a deliverable attachment for exhibit line item number (<i>fill in</i>) {{textbox_252.211-7003[6]}}, Unique Item Identifier Report for Embedded Items, Contract Data Requirements List, DD Form 1423.</p><p>(g) <i>Subcontracts.</i> If the Contractor acquires by contract any items for which item unique identification is required in accordance with paragraph (c)(1) of this clause, the Contractor shall include this clause, including this paragraph (g), in the applicable subcontract(s), including subcontracts for commercial items.</p>'
--               '<p>(a) <i>Definitions.</i> As used in this clause-</p><p><i>Automatic identification device</i> means a device, such as a reader or interrogator, used to retrieve data encoded on machine-readable media.</p><p><i>Concatenated unique item identifier</i> means-</p><p>(1) For items that are serialized within the enterprise identifier, the linking together of the unique identifier data elements in order of the issuing agency code, enterprise identifier, and unique serial number within the enterprise identifier; or</p><p>(2) For items that are serialized within the original part, lot, or batch number, the linking together of the unique identifier data elements in order of the issuing agency code; enterprise identifier; original part, lot, or batch number; and serial number within the original part, lot, or batch number.</p><p><i>Data matrix</i> means a two-dimensional matrix symbology, which is made up of square or, in some cases, round modules arranged within a perimeter finder pattern and uses the Error Checking and Correction 200 (ECC200) specification found within International Standards Organization (ISO)/International Electrotechnical Commission (IEC) 16022.</p><p><i>Data qualifier</i> means a specified character (or string of characters) that immediately precedes a data field that defines the general category or intended use of the data that follows.</p><p><i>DoD recognized unique identification equivalent</i> means a unique identification method that is in commercial use and has been recognized by DoD. All DoD recognized unique identification equivalents are listed at <i>http://www.acq.osd.mil/dpap/pdi/uid/iuid_equivalents.html.</i> </p><p><i>DoD item unique identification</i> means a system of marking items delivered to DoD with unique item identifiers that have machine-readable data elements to distinguish an item from all other like and unlike items. For items that are serialized within the enterprise identifier, the unique item identifier shall include the data elements of the enterprise identifier and a unique serial number. For items that are serialized within the part, lot, or batch number within the enterprise identifier, the unique item identifier shall include the data elements of the enterprise identifier; the original part, lot, or batch number; and the serial number.</p><p><i>Enterprise</i> means the entity (<i>e.g.</i>, a manufacturer or vendor) responsible for assigning unique item identifiers to items.</p><p><i>Enterprise identifier</i> means a code that is uniquely assigned to an enterprise by an issuing agency.</p><p><i>Government''s unit acquisition cost</i> means-</p><p>(1) For fixed-price type line, subline, or exhibit line items, the unit price identified in the contract at the time of delivery;</p><p>(2) For cost-type or undefinitized line, subline, or exhibit line items, the Contractor''s estimated fully burdened unit cost to the Government at the time of delivery; and</p><p>(3) For items produced under a time-and-materials contract, the Contractor''s estimated fully burdened unit cost to the Government at the time of delivery.</p><p><i>Issuing agency</i> means an organization responsible for assigning a globally unique identifier to an enterprise (e.g., Dun & Bradstreet''s Data Universal Numbering System (DUNS) Number, GS1 Company Prefix, Allied Committee 135 NATO Commercial and Government Entity (NCAGE)/Commercial and Government Entity (CAGE) Code, or the Coded Representation of the North American Telecommunications Industry Manufacturers, Suppliers, and Related Service Companies (ATIS-0322000) Number), European Health Industry Business Communication Council (EHIBCC) and Health Industry Business Communication Council (HIBCC)), as indicated in the Register of Issuing Agency Codes for ISO/IEC 15459, located at <i>http://www.nen.nl/Normontwikkeling/Certificatieschemas-en-keurmerken/Schemabeheer/ISOIEC-15459.htm</i></p><p><i>Issuing agency code</i> means a code that designates the registration (or controlling) authority for the enterprise identifier.</p><p><i>Item</i> means a single hardware article or a single unit formed by a grouping of subassemblies, components, or constituent parts.</p><p><i>Lot or batch number</i> means an identifying number assigned by the enterprise to a designated group of items, usually referred to as either a lot or a batch, all of which were manufactured under identical conditions.</p><p><i>Machine-readable</i> means an automatic identification technology media, such as bar codes, contact memory buttons, radio frequency identification, or optical memory cards.</p><p><i>Original part number</i> means a combination of numbers or letters assigned by the enterprise at item creation to a class of items with the same form, fit, function, and interface.</p><p><i>Parent item</i> means the item assembly, intermediate component, or subassembly that has an embedded item with a unique item identifier or DoD recognized unique identification equivalent.</p><p><i>Serial number within the enterprise identifier</i> means a combination of numbers, letters, or symbols assigned by the enterprise to an item that provides for the differentiation of that item from any other like and unlike item and is never used again within the enterprise.</p><p><i>Serial number within the part, lot, or batch number</i> means a combination of numbers or letters assigned by the enterprise to an item that provides for the differentiation of that item from any other like item within a part, lot, or batch number assignment.</p><p><i>Serialization within the enterprise identifier</i> means each item produced is assigned a serial number that is unique among all the tangible items produced by the enterprise and is never used again. The enterprise is responsible for ensuring unique serialization within the enterprise identifier.</p><p><i>Serialization within the part, lot, or batch number</i> means each item of a particular part, lot, or batch number is assigned a unique serial number within that part, lot, or batch number assignment. The enterprise is responsible for ensuring unique serialization within the part, lot, or batch number within the enterprise identifier.</p><p><i>Type designation</i> means a combination of letters and numerals assigned by the Government to a major end item, assembly or subassembly, as appropriate, to provide a convenient means of differentiating between items having the same basic name and to indicate modifications and changes thereto.</p><p><i>Unique item identifier</i> means a set of data elements marked on items that is globally unique and unambiguous. The term includes a concatenated unique item identifier or a DoD recognized unique identification equivalent.</p><p><i>Unique item identifier type</i> means a designator to indicate which method of uniquely identifying a part has been used. The current list of accepted unique item identifier types is maintained at <i>http://www.acq.osd.mil/dpap/pdi/uid/uii_types.html.</i> </p><p>(b) The Contractor shall deliver all items under a contract line, subline, or exhibit line item.</p><p>(c) <i>Unique item identifier.</i> (1) The Contractor shall provide a unique item identifier for the following:</p><p>(i) Delivered items for which the Government''s unit acquisition cost is $5,000 or more, except for the following line items:</p>\n<div><div><table border="&quot;1&quot;" cellpadding="&quot;1&quot;" cellspacing="&quot;1&quot;" frame="&quot;void&quot;" width="&quot;100%&quot;"><tbody><tr><th scope="&quot;col&quot;">Contract line, subline, or exhibit line item No.</th><th scope="&quot;col&quot;">Item description</th></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;"></td><td align="&quot;left&quot;"></td></tr></tbody></table></div></div><p>(ii) Items for which the Government''s unit acquisition cost is less than $5,000 that are identified in the Schedule or the following table:</p>\n<div><div><table border="&quot;1&quot;" cellpadding="&quot;1&quot;" cellspacing="&quot;1&quot;" frame="&quot;void&quot;" width="&quot;100%&quot;"><tbody><tr><th scope="&quot;col&quot;">Contract line, subline, or exhibit line item No.</th><th scope="&quot;col&quot;">Item description</th></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;"></td><td align="&quot;left&quot;"></td></tr></tbody></table></div></div>\n<p>(<i>If items are identified in the Schedule, insert âSee Scheduleâ in this table.</i>)</p><p>(iii) Subassemblies, components, and parts embedded within delivered items, items with warranty requirements, DoD serially managed reparables and DoD serially managed nonreparables as specified in Attachment Number {{textbox_252.211-7003[0]}}.</p><p>(iv) Any item of special tooling or special test equipment as defined in FAR 2.101 that have been designated for preservation and storage for a Major Defense Acquisition Program as specified in Attachment Number {{textbox_252.211-7003[1]}}.</p><p>(v) Any item not included in paragraphs (c)(1)(i), (ii), (iii), or (iv) of this clause for which the contractor creates and marks a unique item identifier for traceability.</p><p>(2) The unique item identifier assignment and its component data element combination shall not be duplicated on any other item marked or registered in the DoD Item Unique Identification Registry by the contractor.</p><p>(3) The unique item identifier component data elements shall be marked on an item using two dimensional data matrix symbology that complies with ISO/IEC International Standard 16022, Information technology-International symbology specification-Data matrix; ECC200 data matrix specification.</p><p>(4) <i>Data syntax and semantics of unique item identifiers.</i> The Contractor shall ensure that-</p><p>(i) The data elements (except issuing agency code) of the unique item identifier are encoded within the data matrix symbol that is marked on the item using one of the following three types of data qualifiers, as determined by the Contractor:</p><p>(A) Application Identifiers (AIs) (Format Indicator 05 of ISO/IEC International Standard 15434), in accordance with ISO/IEC International Standard 15418, Information Technology-EAN/UCC Application Identifiers and Fact Data Identifiers and Maintenance and ANSI MH 10.8.2 Data Identifier and Application Identifier Standard.</p><p>(B) Data Identifiers (DIs) (Format Indicator 06 of ISO/IEC International Standard 15434), in accordance with ISO/IEC International Standard 15418, Information Technology-EAN/UCC Application Identifiers and Fact Data Identifiers and Maintenance and ANSI MH 10.8.2 Data Identifier and Application Identifier Standard.</p><p>(C) Text Element Identifiers (TEIs) (Format Indicator 12 of ISO/IEC International Standard 15434), in accordance with the Air Transport Association Common Support Data Dictionary; and</p><p>(ii) The encoded data elements of the unique item identifier conform to the transfer structure, syntax, and coding of messages and data formats specified for Format Indicators 05, 06, and 12 in ISO/IEC International Standard 15434, Information Technology-Transfer Syntax for High Capacity Automatic Data Capture Media.</p><p>(5) <i>Unique item identifier.</i></p><p>(i) The Contractor shall-</p><p>(A) Determine whether to-</p><p>(<i>1</i>) Serialize within the enterprise identifier;</p><p>(<i>2</i>) Serialize within the part, lot, or batch number; or</p><p>(<i>3</i>) Use a DoD recognized unique identification equivalent (e.g. Vehicle Identification Number); and</p><p>(B) Place the data elements of the unique item identifier (enterprise identifier; serial number; DoD recognized unique identification equivalent; and for serialization within the part, lot, or batch number only: Original part, lot, or batch number) on items requiring marking by paragraph (c)(1) of this clause, based on the criteria provided in MIL-STD-130, Identification Marking of U.S. Military Property, latest version;</p><p>(C) Label shipments, storage containers and packages that contain uniquely identified items in accordance with the requirements of MIL-STD-129, Military Marking for Shipment and Storage, latest version; and</p><p>(D) Verify that the marks on items and labels on shipments, storage containers, and packages are machine readable and conform to the applicable standards. The contractor shall use an automatic identification technology device for this verification that has been programmed to the requirements of Appendix A, MIL-STD-130, latest version.</p><p>(ii) The issuing agency code-</p><p>(A) Shall not be placed on the item; and</p><p>(B) Shall be derived from the data qualifier for the enterprise identifier.</p><p>(d) For each item that requires item unique identification under paragraph (c)(1)(i), (ii), or (iv) of this clause or when item unique identification is provided under paragraph (c)(1)(v), in addition to the information provided as part of the Material Inspection and Receiving Report specified elsewhere in this contract, the Contractor shall report at the time of delivery, as part of the Material Inspection and Receiving Report, the following information:</p><p>(1) Unique item identifier.</p><p>(2) Unique item identifier type.</p><p>(3) Issuing agency code (if concatenated unique item identifier is used).</p><p>(4) Enterprise identifier (if concatenated unique item identifier is used).</p><p>(5) Original part number (if there is serialization within the original part number).</p><p>(6) Lot or batch number (if there is serialization within the lot or batch number).</p><p>(7) Current part number (optional and only if not the same as the original part number).</p><p>(8) Current part number effective date (optional and only if current part number is used).</p><p>(9) Serial number (if concatenated unique item identifier is used).</p><p>(10) Government''s unit acquisition cost.</p><p>(11) Unit of measure.</p><p>(12) Type designation of the item as specified in the contract schedule, if any.</p><p>(13) Whether the item is an item of Special Tooling or Special Test Equipment.</p><p>(14) Whether the item is covered by a warranty.</p><p>(e) For embedded subassemblies, components, and parts that require DoD item unique identification under paragraph (c)(1)(iii) of this clause or when item unique identification is provided under paragraph (c)(1)(v), the Contractor shall report as part of the Material Inspection and Receiving Report specified elsewhere in this contract, the following information:</p><p>(1) Unique item identifier of the parent item under paragraph (c)(1) of this clause that contains the embedded subassembly, component, or part.</p><p>(2) Unique item identifier of the embedded subassembly, component, or part.</p><p>(3) Unique item identifier type.**</p><p>(4) Issuing agency code (if concatenated unique item identifier is used).**</p><p>(5) Enterprise identifier (if concatenated unique item identifier is used).**</p><p>(6) Original part number (if there is serialization within the original part number).**</p><p>(7) Lot or batch number (if there is serialization within the lot or batch number).**</p><p>(8) Current part number (optional and only if not the same as the original part number).**</p><p>(9) Current part number effective date (optional and only if current part number is used).**</p><p>(10) Serial number (if concatenated unique item identifier is used).**</p><p>(11) Description.</p>\n<p>** Once per item.</p><p>(f) The Contractor shall submit the information required by paragraphs (d) and (e) of this clause as follows:</p><p>(1) End items shall be reported using the receiving report capability in Wide Area WorkFlow (WAWF) in accordance with the clause at 252.232-7003. If WAWF is not required by this contract, and the contractor is not using WAWF, follow the procedures at <i>http://dodprocurementtoolbox.com/site/uidregistry/.</i></p><p>(2) Embedded items shall be reported by one of the following methods-</p><p>(i) Use of the embedded items capability in WAWF;</p><p>(ii) Direct data submission to the IUID Registry following the procedures and formats at <i>http://dodprocurementtoolbox.com/site/uidregistry/;</i> or</p><p>(iii) Via WAWF as a deliverable attachment for exhibit line item number (<i>fill in</i>) {{textbox_252.211-7003[2]}}, Unique Item Identifier Report for Embedded Items, Contract Data Requirements List, DD Form 1423.</p><p>(g) <i>Subcontracts.</i> If the Contractor acquires by contract any items for which item unique identification is required in accordance with paragraph (c)(1) of this clause, the Contractor shall include this clause, including this paragraph (g), in the applicable subcontract(s), including subcontracts for commercial items.</p>'
WHERE clause_id = 93298; -- 252.211-7003
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (93298, 'textbox_252.211-7003[0]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (93298, 'textbox_252.211-7003[1]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (93298, 'textbox_252.211-7003[2]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (93298, 'textbox_252.211-7003[3]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (93298, 'textbox_252.211-7003[4]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (93298, 'textbox_252.211-7003[5]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (93298, 'textbox_252.211-7003[6]', 'S', 100);

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1211_67006&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?node=se48.3.252_1211_67006&amp;rgn=div8'
, clause_data = '<p>(a) <i>Definitions.</i> As used in this clause-</p><p><i>Advance shipment notice</i> means an electronic notification used to list the contents of a shipment of goods as well as additional information relating to the shipment, such as passive radio frequency identification (RFID) or item unique identification (IUID) information, order information, product description, physical characteristics, type of packaging, marking, carrier information, and configuration of goods within the transportation equipment.</p><p><i>Bulk commodities</i> means the following commodities, when shipped in rail tank cars, tanker trucks, trailers, other bulk wheeled conveyances, or pipelines:</p><p>(1) Sand.</p><p>(2) Gravel.</p><p>(3) Bulk liquids (water, chemicals, or petroleum products).</p><p>(4) Ready-mix concrete or similar construction materials.</p><p>(5) Coal or combustibles such as firewood.</p><p>(6) Agricultural products such as seeds, grains, or animal feed.</p><p><i>Case</i> means either a MIL-STD-129 defined exterior container within a palletized unit load or a MIL-STD-129 defined individual shipping container.</p><p><i>Electronic Product CodeTM (EPC&reg;)</i> means an identification scheme for universally identifying physical objects via RFID tags and other means. The standardized EPCTM data consists of an EPCTM (or EPCTM identifier) that uniquely identifies an individual object, as well as an optional filter value when judged to be necessary to enable effective and efficient reading of the EPCTM tags. In addition to this standardized data, certain classes of EPCTM tags will allow user-defined data. The EPCTM Tag Data Standards will define the length and position of this data, without defining its content.</p><p><i>EPCglobal&reg;</i> means a subscriber-driven organization comprised of industry leaders and organizations focused on creating global standards for the adoption of passive RFID technology.</p><p><i>Exterior container</i> means a MIL-STD-129 defined container, bundle, or assembly that is sufficient by reason of material, design, and construction to protect unit packs and intermediate containers and their contents during shipment and storage. It can be a unit pack or a container with a combination of unit packs or intermediate containers. An exterior container may or may not be used as a shipping container.</p><p><i>Palletized unit load</i> means a MIL-STD-129 defined quantity of items, packed or unpacked, arranged on a pallet in a specified manner and secured, strapped, or fastened on the pallet so that the whole palletized load is handled as a single unit. A palletized or skidded load is not considered to be a shipping container. A loaded 463L System pallet is not considered to be a palletized unit load. Refer to the Defense Transportation Regulation, DoD 4500.9-R, Part II, Chapter 203, for marking of 463L System pallets.</p><p><i>Passive RFID tag</i> means a tag that reflects energy from the reader/interrogator or that receives and temporarily stores a small amount of energy from the reader/interrogator signal in order to generate the tag response. The only acceptable tags are EPC Class 1 passive RFID tags that meet the EPCglobalTM Class 1 Generation 2 standard.</p><p><i>Radio frequency identification (RFID)</i> means an automatic identification and data capture technology comprising one or more reader/interrogators and one or more radio frequency transponders in which data transfer is achieved by means of suitably modulated inductive or radiating electromagnetic carriers.</p><p><i>Shipping container</i> means a MIL-STD-129 defined exterior container that meets carrier regulations and is of sufficient strength, by reason of material, design, and construction, to be shipped safely without further packing (<i>e.g.,</i> wooden boxes or crates, fiber and metal drums, and corrugated and solid fiberboard boxes).</p><p>(b)(1) Except as provided in paragraph (b)(2) of this clause, the Contractor shall affix passive RFID tags, at the case- and palletized- unit-load packaging levels, for shipments of items that-</p><p>(i) Are in any of the following classes of supply, as defined in DoD 4140.1-R, DoD Supply Chain Materiel Management Regulation, AP1.1.11:</p><p>(A) Subclass of Class I-Packaged operational rations.</p><p>(B) Class II-Clothing, individual equipment, tentage, organizational tool kits, hand tools, and administrative and housekeeping supplies and equipment.</p><p>(C) Class IIIP-Packaged petroleum, lubricants, oils, preservatives, chemicals, and additives.</p><p>(D) Class IV-Construction and barrier materials.</p><p>(E) Class VI-Personal demand items (non-military sales items).</p><p>(F) Subclass of Class VIII-Medical materials (excluding pharmaceuticals, biologicals, and reagents-suppliers should limit the mixing of excluded and non-excluded materials).</p><p>(G) Class IX-Repair parts and components including kits, assemblies and subassemblies, reparable and consumable items required for maintenance support of all equipment, excluding medical-peculiar repair parts; and</p><p>(ii) Are being shipped to one of the locations listed at <i>http://www.acq.osd.mil/log/rfid/</i> or to-</p><p>(A) A location outside the contiguous United States when the shipment has been assigned Transportation Priority 1, or to-</p><p>(B) The following location(s) deemed necessary by the requiring activity:</p><div><div><table border="1" cellpadding="1" cellspacing="1" frame="void" width="100%"><tbody><tr><th scope="col">Contract line, subline, or exhibit line item number</th><th scope="col">Location name</th><th scope="col">City</th><th scope="col">State</th><th scope="col">DoDAAC</th></tr><tr><td>{{textbox_252.211-7006[0]}}</td><td>{{textbox_252.211-7006[1]}}</td><td>{{textbox_252.211-7006[2]}}</td><td>{{textbox_252.211-7006[3]}}</td><td>{{textbox_252.211-7006[4]}}</td></tr><tr><td>{{textbox_252.211-7006[5]}}</td><td>{{textbox_252.211-7006[6]}}</td><td>{{textbox_252.211-7006[7]}}</td><td>{{textbox_252.211-7006[8]}}</td><td>{{textbox_252.211-7006[9]}}</td></tr></tbody></table></div></div><p>(2) The following are excluded from the requirements of paragraph (b)(1) of this clause:</p><p>(i) Shipments of bulk commodities.</p><p>(ii) Shipments to locations other than Defense Distribution Depots when the contract includes the clause at FAR 52.213-1, Fast Payment Procedures.</p><p>(c) The Contractor shall-</p><p>(1) Ensure that the data encoded on each passive RFID tag are globally unique (<i>i.e.,</i> the tag ID is never repeated across two or more RFID tags) and conforms to the requirements in paragraph (d) of this clause;</p><p>(2) Use passive tags that are readable; and</p><p>(3) Ensure that the passive tag is affixed at the appropriate location on the specific level of packaging, in accordance with MIL-STD-129 (Section 4.9.2) tag placement specifications.</p><p>(d) <i>Data syntax and standards.</i> The Contractor shall encode an approved RFID tag using the instructions provided in the EPCTM Tag Data Standards in effect at the time of contract award. The EPCTM Tag Data Standards are available at <i>http://www.epcglobalinc.org/standards/.</i></p><p>(1) If the Contractor is an EPCglobalTM subscriber and possesses a unique EPCTM company prefix, the Contractor may use any of the identifiers and encoding instructions described in the most recent EPCTM Tag Data Standards document to encode tags.</p><p>(2) If the Contractor chooses to employ the DoD identifier, the Contractor shall use its previously assigned Commercial and Government Entity (CAGE) code and shall encode the tags in accordance with the tag identifier details located at <i>http://www.acq.osd.mil/log/rfid/tag_data.htm.</i> If the Contractor uses a third-party packaging house to encode its tags, the CAGE code of the third-party packaging house is acceptable.</p><p>(3) Regardless of the selected encoding scheme, the Contractor with which the Department holds the contract is responsible for ensuring that the tag ID encoded on each passive RFID tag is globally unique, per the requirements in paragraph (c)(1) of this clause.</p><p>(e) <i>Advance shipment notice.</i> The Contractor shall use Wide Area WorkFlow (WAWF), as required by DFARS 252.232-7003, Electronic Submission of Payment Requests, to electronically submit advance shipment notice(s) with the RFID tag ID(s) (specified in paragraph (d) of this clause) in advance of the shipment in accordance with the procedures at <i>https://wawf.eb.mil/.</i></p>'
--               '<p>(a) <i>Definitions.</i> As used in this clause-</p><p><i>Advance shipment notice</i> means an electronic notification used to list the contents of a shipment of goods as well as additional information relating to the shipment, such as passive radio frequency identification (RFID) or item unique identification (IUID) information, order information, product description, physical characteristics, type of packaging, marking, carrier information, and configuration of goods within the transportation equipment.</p><p><i>Bulk commodities</i> means the following commodities, when shipped in rail tank cars, tanker trucks, trailers, other bulk wheeled conveyances, or pipelines:</p><p>(1) Sand.</p><p>(2) Gravel.</p><p>(3) Bulk liquids (water, chemicals, or petroleum products).</p><p>(4) Ready-mix concrete or similar construction materials.</p><p>(5) Coal or combustibles such as firewood.</p><p>(6) Agricultural products such as seeds, grains, or animal feed.</p><p><i>Case</i> means either a MIL-STD-129 defined exterior container within a palletized unit load or a MIL-STD-129 defined individual shipping container.</p><p><i>Electronic Product CodeTM (EPCÂ&reg;)</i> means an identification scheme for universally identifying physical objects via RFID tags and other means. The standardized EPCTM data consists of an EPCTM (or EPCTM identifier) that uniquely identifies an individual object, as well as an optional filter value when judged to be necessary to enable effective and efficient reading of the EPCTM tags. In addition to this standardized data, certain classes of EPCTM tags will allow user-defined data. The EPCTM Tag Data Standards will define the length and position of this data, without defining its content.</p><p><i>EPCglobalÂ&reg;</i> means a subscriber-driven organization comprised of industry leaders and organizations focused on creating global standards for the adoption of passive RFID technology.</p><p><i>Exterior container</i> means a MIL-STD-129 defined container, bundle, or assembly that is sufficient by reason of material, design, and construction to protect unit packs and intermediate containers and their contents during shipment and storage. It can be a unit pack or a container with a combination of unit packs or intermediate containers. An exterior container may or may not be used as a shipping container.</p><p><i>Palletized unit load</i> means a MIL-STD-129 defined quantity of items, packed or unpacked, arranged on a pallet in a specified manner and secured, strapped, or fastened on the pallet so that the whole palletized load is handled as a single unit. A palletized or skidded load is not considered to be a shipping container. A loaded 463L System pallet is not considered to be a palletized unit load. Refer to the Defense Transportation Regulation, DoD 4500.9-R, Part II, Chapter 203, for marking of 463L System pallets.</p><p><i>Passive RFID tag</i> means a tag that reflects energy from the reader/interrogator or that receives and temporarily stores a small amount of energy from the reader/interrogator signal in order to generate the tag response. The only acceptable tags are EPC Class 1 passive RFID tags that meet the EPCglobalTM Class 1 Generation 2 standard.</p><p><i>Radio frequency identification (RFID)</i> means an automatic identification and data capture technology comprising one or more reader/interrogators and one or more radio frequency transponders in which data transfer is achieved by means of suitably modulated inductive or radiating electromagnetic carriers.</p><p><i>Shipping container</i> means a MIL-STD-129 defined exterior container that meets carrier regulations and is of sufficient strength, by reason of material, design, and construction, to be shipped safely without further packing (<i>e.g.,</i> wooden boxes or crates, fiber and metal drums, and corrugated and solid fiberboard boxes).</p><p>(b)(1) Except as provided in paragraph (b)(2) of this clause, the Contractor shall affix passive RFID tags, at the case- and palletized- unit-load packaging levels, for shipments of items that-</p><p>(i) Are in any of the following classes of supply, as defined in DoD 4140.1-R, DoD Supply Chain Materiel Management Regulation, AP1.1.11:</p><p>(A) Subclass of Class I-Packaged operational rations.</p><p>(B) Class II-Clothing, individual equipment, tentage, organizational tool kits, hand tools, and administrative and housekeeping supplies and equipment.</p><p>(C) Class IIIP-Packaged petroleum, lubricants, oils, preservatives, chemicals, and additives.</p><p>(D) Class IV-Construction and barrier materials.</p><p>(E) Class VI-Personal demand items (non-military sales items).</p><p>(F) Subclass of Class VIII-Medical materials (excluding pharmaceuticals, biologicals, and reagents-suppliers should limit the mixing of excluded and non-excluded materials).</p><p>(G) Class IX-Repair parts and components including kits, assemblies and subassemblies, reparable and consumable items required for maintenance support of all equipment, excluding medical-peculiar repair parts; and</p><p>(ii) Are being shipped to one of the locations listed at <i>http://www.acq.osd.mil/log/rfid/</i> or to-</p><p>(A) A location outside the contiguous United States when the shipment has been assigned Transportation Priority 1, or to-</p><p>(B) The following location(s) deemed necessary by the requiring activity:</p>\n<div><div><table border="&quot;1&quot;" cellpadding="&quot;1&quot;" cellspacing="&quot;1&quot;" frame="&quot;void&quot;" width="&quot;100%&quot;"><tbody><tr><th scope="&quot;col&quot;">Contract line, subline, or exhibit line item number</th><th scope="&quot;col&quot;">Location name</th><th scope="&quot;col&quot;">City</th><th scope="&quot;col&quot;">State</th><th scope="&quot;col&quot;">DoDAAC</th></tr>\n<tr><td align="&quot;left&quot;" scope="&quot;row&quot;">{{textbox_252.211-7006[0]}}</td><td align="&quot;left&quot;">{{textbox_252.211-7006[1]}}</td><td align="&quot;left&quot;">{{textbox_252.211-7006[2]}}</td><td align="&quot;left&quot;">{{textbox_252.211-7006[3]}}</td><td align="&quot;left&quot;">{{textbox_252.211-7006[4]}}</td></tr>\n<tr><td align="&quot;left&quot;" scope="&quot;row&quot;">{{textbox_252.211-7006[5]}}</td><td align="&quot;left&quot;">{{textbox_252.211-7006[6]}}</td><td align="&quot;left&quot;">{{textbox_252.211-7006[7]}}</td><td align="&quot;left&quot;">{{textbox_252.211-7006[8]}}</td><td align="&quot;left&quot;">{{textbox_252.211-7006[9]}}</td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">{{textbox_252.211-7006[10]}}</td><td align="&quot;left&quot;">{{textbox_252.211-7006[11]}}</td><td align="&quot;left&quot;">{{textbox_252.211-7006[12]}}</td><td align="&quot;left&quot;">{{textbox_252.211-7006[13]}}</td><td align="&quot;left&quot;">{{textbox_252.211-7006[14]}}</td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">{{textbox_252.211-7006[15]}}</td><td align="&quot;left&quot;">{{textbox_252.211-7006[16]}}</td><td align="&quot;left&quot;">{{textbox_252.211-7006[17]}}</td><td align="&quot;left&quot;">{{textbox_252.211-7006[18]}}</td><td align="&quot;left&quot;">{{textbox_252.211-7006[19]}}</td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">{{textbox_252.211-7006[20]}}</td><td align="&quot;left&quot;">{{textbox_252.211-7006[21]}}</td><td align="&quot;left&quot;">{{textbox_252.211-7006[22]}}</td><td align="&quot;left&quot;">{{textbox_252.211-7006[23]}}</td><td align="&quot;left&quot;">{{textbox_252.211-7006[24]}}</td></tr></tbody></table></div></div><p>(2) The following are excluded from the requirements of paragraph (b)(1) of this clause:</p><p>(i) Shipments of bulk commodities.</p><p>(ii) Shipments to locations other than Defense Distribution Depots when the contract includes the clause at FAR 52.213-1, Fast Payment Procedures.</p><p>(c) The Contractor shall-</p><p>(1) Ensure that the data encoded on each passive RFID tag are globally unique (<i>i.e.,</i> the tag ID is never repeated across two or more RFID tags) and conforms to the requirements in paragraph (d) of this clause;</p><p>(2) Use passive tags that are readable; and</p><p>(3) Ensure that the passive tag is affixed at the appropriate location on the specific level of packaging, in accordance with MIL-STD-129 (Section 4.9.2) tag placement specifications.</p><p>(d) <i>Data syntax and standards.</i> The Contractor shall encode an approved RFID tag using the instructions provided in the EPCTM Tag Data Standards in effect at the time of contract award. The EPCTM Tag Data Standards are available at <i>http://www.epcglobalinc.org/standards/.</i></p><p>(1) If the Contractor is an EPCglobalTM subscriber and possesses a unique EPCTM company prefix, the Contractor may use any of the identifiers and encoding instructions described in the most recent EPCTM Tag Data Standards document to encode tags.</p><p>(2) If the Contractor chooses to employ the DoD identifier, the Contractor shall use its previously assigned Commercial and Government Entity (CAGE) code and shall encode the tags in accordance with the tag identifier details located at <i>http://www.acq.osd.mil/log/rfid/tag_data.htm.</i> If the Contractor uses a third-party packaging house to encode its tags, the CAGE code of the third-party packaging house is acceptable.</p><p>(3) Regardless of the selected encoding scheme, the Contractor with which the Department holds the contract is responsible for ensuring that the tag ID encoded on each passive RFID tag is globally unique, per the requirements in paragraph (c)(1) of this clause.</p><p>(e) <i>Advance shipment notice.</i> The Contractor shall use Wide Area WorkFlow (WAWF), as required by DFARS 252.232-7003, Electronic Submission of Payment Requests, to electronically submit advance shipment notice(s) with the RFID tag ID(s) (specified in paragraph (d) of this clause) in advance of the shipment in accordance with the procedures at <i>https://wawf.eb.mil/.</i></p>'
WHERE clause_id = 93301; -- 252.211-7006
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 28273; -- textbox_252.211-7006[10]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 28274; -- textbox_252.211-7006[11]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 28275; -- textbox_252.211-7006[12]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 28276; -- textbox_252.211-7006[13]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 28277; -- textbox_252.211-7006[14]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 28278; -- textbox_252.211-7006[15]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 28279; -- textbox_252.211-7006[16]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 28280; -- textbox_252.211-7006[17]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 28281; -- textbox_252.211-7006[18]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 28282; -- textbox_252.211-7006[19]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 28283; -- textbox_252.211-7006[20]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 28284; -- textbox_252.211-7006[21]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 28285; -- textbox_252.211-7006[22]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 28286; -- textbox_252.211-7006[23]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 28287; -- textbox_252.211-7006[24]

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1215_67009&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?node=se48.3.252_1215_67009&amp;rgn=div8'
, clause_data = '<p>The offeror shall complete the following checklist, providing location of requested information, or an explanation of why the requested information is not provided. In preparation of the offeror''s checklist, offerors may elect to have their prospective subcontractors use the same or similar checklist as appropriate.</p><div><p>Proposal Adequacy Checklist</p></div><div><table border="1" cellpadding="1" cellspacing="1" frame="void" width="100%"><tbody><tr><th scope="col">References</th><th scope="col">Submission item</th><th scope="col">Proposal page No.</th><th scope="col">If not provided<br>EXPLAIN<br>(may use<br>continuation<br>pages)</br></br></br></br></th></tr><tr><td align="center" colspan="4" scope="row"><i>GENERAL INSTRUCTIONS</i></td></tr><tr><td align="left" scope="row">1. FAR 15.408, Table 15-2, Section I Paragraph A</td><td align="left">Is there a properly completed first page of the proposal per FAR 15.408 Table 15-2 I.A or as specified in the solicitation?</td><td align="right"></td><td align="left"></td></tr><tr><td align="left" scope="row">2. FAR 15.408, Table 15-2, Section I Paragraph A(7)</td><td align="left">Does the proposal identify the need for Government-furnished material/tooling/test equipment? Include the accountable contract number and contracting officer contact information if known.</td><td align="right"></td><td align="left"></td></tr><tr><td align="left" scope="row">3. FAR 15.408, Table 15-2, Section I Paragraph A(8)</td><td align="left">Does the proposal identify and explain notifications of noncompliance with Cost Accounting Standards Board or Cost Accounting Standards (CAS); any proposal inconsistencies with your disclosed practices or applicable CAS; and inconsistencies with your established estimating and accounting principles and procedures?</td><td align="right"></td><td align="left"></td></tr><tr><td align="left" scope="row">4. FAR 15.408, Table 15-2, Section I, Paragraph C(1)</td><td align="left">Does the proposal disclose any other known activity that could materially impact the costs?</td><td align="right"></td><td align="left"></td></tr><tr><td align="left" scope="row">FAR 2.101, ''Cost or pricing data''</td><td align="left">This may include, but is not limited to, such factors as-<br>(1) Vendor quotations;<br>(2) Nonrecurring costs;<br>(3) Information on changes in production methods and in production or purchasing volume;<br>(4) Data supporting projections of business prospects and objectives and related operations costs;</br></br></br></br></td><td align="right"></td><td align="left"></td></tr><tr><td align="left" scope="row">&nbsp;&nbsp;&nbsp;</td><td align="left">(5) Unit-cost trends such as those associated with labor efficiency;<br>(6) Make-or-buy decisions;</br></td><td align="right"></td><td align="left"></td></tr><tr><td align="left" scope="row">&nbsp;&nbsp;&nbsp;</td><td align="left">(7) Estimated resources to attain business goals; and</td><td align="right"></td><td align="left"></td></tr><tr><td align="left" scope="row">&nbsp;&nbsp;&nbsp;</td><td align="left">(8) Information on management decisions that could have a significant bearing on costs.</td><td align="right"></td><td align="left"></td></tr><tr><td align="left" scope="row">5. FAR 15.408, Table 15-2, Section I Paragraph B</td><td align="left">Is an Index of all certified cost or pricing data and information accompanying or identified in the proposal provided and appropriately referenced?</td><td align="right"></td><td align="left"></td></tr><tr><td align="left" scope="row">6. FAR 15.403-1(b)</td><td align="left">Are there any exceptions to submission of certified cost or pricing data pursuant to FAR 15.403-1(b)? If so, is supporting documentation included in the proposal? (Note questions 18-20.)</td><td align="right"></td><td align="left"></td></tr><tr><td align="left" scope="row">7. FAR 15.408, Table 15-2, Section I Paragraph C(2)(i)</td><td align="left">Does the proposal disclose the judgmental factors applied and the mathematical or other methods used in the estimate, including those used in projecting from known data?</td><td align="right"></td><td align="left"></td></tr><tr><td align="left" scope="row">8. FAR 15.408, Table 15-2, Section I Paragraph C(2)(ii)</td><td align="left">Does the proposal disclose the nature and amount of any contingencies included in the proposed price?</td><td align="right"></td><td align="left"></td></tr><tr><td align="left" scope="row">9. FAR 15.408 Table 15-2, Section II, Paragraph A or B</td><td align="left">Does the proposal explain the basis of all cost estimating relationships (labor hours or material) proposed on other than a discrete basis?</td><td align="right"></td><td align="left"></td></tr><tr><td align="left" scope="row">10. FAR 15.408, Table 15-2, Section I Paragraphs D and E</td><td align="left">Is there a summary of total cost by element of cost and are the elements of cost cross-referenced to the supporting cost or pricing data? (Breakdowns for each cost element must be consistent with your cost accounting system, including breakdown by year.)</td><td align="right"></td><td align="left"></td></tr><tr><td align="left" scope="row">11. FAR 15.408, Table 15-2, Section I Paragraphs D and E</td><td align="left">If more than one Contract Line Item Number (CLIN) or sub Contract Line Item Number (sub-CLIN) is proposed as required by the RFP, are there summary total amounts covering all line items for each element of cost and is it cross-referenced to the supporting cost or pricing data?</td><td align="right"></td><td align="left"></td></tr><tr><td align="left" scope="row">12. FAR 15.408, Table 15-2, Section I Paragraph F</td><td align="left">Does the proposal identify any incurred costs for work performed before the submission of the proposal?</td><td align="right"></td><td align="left"></td></tr><tr><td align="left" scope="row">13. FAR 15.408, Table 15-2, Section I Paragraph G</td><td align="left">Is there a Government forward pricing rate agreement (FPRA)? If so, the offeror shall identify the official submittal of such rate and factor data. If not, does the proposal include all rates and factors by year that are utilized in the development of the proposal and the basis for those rates and factors?</td><td align="right"></td><td align="left"></td></tr><tr><td align="center" colspan="4" scope="row"><i>COST ELEMENTS</i></td></tr><tr><td align="center" colspan="4" scope="row">MATERIALS AND SERVICES</td></tr><tr><td align="left" scope="row">14. FAR 15.408, Table 15-2, Section II Paragraph A</td><td align="left">Does the proposal include a consolidated summary of individual material and services, frequently referred to as a Consolidated Bill of Material (CBOM), to include the basis for pricing? The offeror''s consolidated summary shall include raw materials, parts, components, assemblies, subcontracts and services to be produced or performed by others, identifying as a minimum the item, source, quantity, and price.</td><td align="right"></td><td align="left"></td></tr><tr><td align="center" colspan="4" scope="row">SUBCONTRACTS (Purchased materials or services)</td></tr><tr><td align="left" scope="row">15. DFARS 215.404-3</td><td align="left">Has the offeror identified in the proposal those subcontractor proposals, for which the contracting officer has initiated or may need to request field pricing analysis?</td><td align="right"></td><td align="left"></td></tr><tr><td align="left" scope="row">16. FAR 15.404-3(c)<br>FAR 52.244-2</br></td><td align="left">Per the thresholds of FAR 15.404-3(c), Subcontract Pricing Considerations, does the proposal include a copy of the applicable subcontractor''s certified cost or pricing data?</td><td align="right"></td><td align="left"></td></tr><tr><td align="left" scope="row">17. FAR 15.408, Table 15-2, Note 1; Section II Paragraph A</td><td align="left">Is there a price/cost analysis establishing the reasonableness of each of the proposed subcontracts included with the proposal? If the offeror''s price/cost analyses are not provided with the proposal, does the proposal include a matrix identifying dates for receipt of subcontractor proposal, completion of fact finding for purposes of price/cost analysis, and submission of the price/cost analysis?</td><td align="right"></td><td align="left"></td></tr><tr><td align="center" colspan="4" scope="row"><i>EXCEPTIONS TO CERTIFIED COST OR PRICING DATA</i></td></tr><tr><td align="left" scope="row">18. FAR 52.215-20<br>FAR 2.101, ''commercial item''</br></td><td align="left">Has the offeror submitted an exception to the submission of certified cost or pricing data for commercial items proposed either at the prime or subcontractor level, in accordance with provision 52.215-20?</td><td align="right"></td><td align="left"></td></tr><tr><td align="left" scope="row">&nbsp;&nbsp;&nbsp;</td><td align="left">a. Has the offeror specifically identified the type of commercial item claim (FAR 2.101 commercial item definition, paragraphs (1) through (8)), and the basis on which the item meets the definition?</td><td align="right"></td><td align="left"></td></tr><tr><td align="left" scope="row">&nbsp;&nbsp;&nbsp;</td><td align="left">b. For modified commercial items (FAR 2.101 commercial item definition paragraph (3)); did the offeror classify the modification(s) as either-</td><td align="right"></td><td align="left"></td></tr><tr><td align="left" scope="row">&nbsp;&nbsp;&nbsp;</td><td align="left">i. A modification of a type customarily available in the commercial marketplace (paragraph (3)(i)); or</td><td align="right"></td><td align="left"></td></tr><tr><td align="left" scope="row">&nbsp;&nbsp;&nbsp;</td><td align="left">ii. A minor modification (paragraph (3)(ii)) of a type not customarily available in the commercial marketplace made to meet Federal Government requirements not exceeding the thresholds in FAR 15.403-1(c)(3)(iii)(B)?</td><td align="right"></td><td align="left"></td></tr><tr><td align="left" scope="row">&nbsp;&nbsp;&nbsp;</td><td align="left">c. For proposed commercial items ''of a type'', or ''evolved'' or modified (FAR 2.101 commercial item definition paragraphs (1) through (3)), did the contractor provide a technical description of the differences between the proposed item and the comparison item(s)?</td><td align="right"></td><td align="left"></td></tr><tr><td align="left" scope="row">19.</td><td align="left">[Reserved]</td><td align="right"></td><td align="left"></td></tr><tr><td align="left" scope="row">20. FAR 15.408, Table 15-2, Section II Paragraph A(1)</td><td align="left">Does the proposal support the degree of competition and the basis for establishing the source and reasonableness of price for each subcontract or purchase order priced on a competitive basis exceeding the threshold for certified cost or pricing data?</td><td align="right"></td><td align="left"></td></tr><tr><td align="center" colspan="4" scope="row">INTERORGANIZATIONAL TRANSFERS</td></tr><tr><td align="left" scope="row">21. FAR 15.408, Table 15-2, Section II Paragraph A.(2)</td><td align="left">For inter-organizational transfers proposed at cost, does the proposal include a complete cost proposal in compliance with Table 15-2?</td><td align="right"></td><td align="left"></td></tr><tr><td align="left" scope="row">22. FAR 15.408, Table 15-2, Section II Paragraph A(1)</td><td align="left">For inter-organizational transfers proposed at price in accordance with FAR 31.205-26(e), does the proposal provide an analysis by the prime that supports the exception from certified cost or pricing data in accordance with FAR 15.403-1?</td><td align="right"></td><td align="left"></td></tr><tr><td align="center" colspan="4" scope="row">DIRECT LABOR</td></tr><tr><td align="left" scope="row">23. FAR 15.408, Table 15-2, Section II Paragraph B</td><td align="left">Does the proposal include a time phased (<i>i.e.</i>; monthly, quarterly) breakdown of labor hours, rates and costs by category or skill level? If labor is the allocation base for indirect costs, the labor cost must be summarized in order that the applicable overhead rate can be applied.</td><td align="right"></td><td align="left"></td></tr><tr><td align="left" scope="row">24. FAR 15.408, Table 15-2, Section II Paragraph B</td><td align="left">For labor Basis of Estimates (BOEs), does the proposal include labor categories, labor hours, and task descriptions-(e.g.; Statement of Work reference, applicable CLIN, Work Breakdown Structure, rationale for estimate, applicable history, and time-phasing)?</td><td align="right"></td><td align="left"></td></tr><tr><td align="left" scope="row">25. FAR subpart 22.10</td><td align="left">If covered by the Service Contract Labor Standards statute (41 U.S.C. chapter 67), are the rates in the proposal in compliance with the minimum rates specified in the statute?</td><td align="right"></td><td align="left"></td></tr><tr><td align="center" colspan="4" scope="row"><i>INDIRECT COSTS</i></td></tr><tr><td align="left" scope="row">26. FAR 15.408, Table 15-2, Section II Paragraph C</td><td align="left">Does the proposal indicate the basis of estimate for proposed indirect costs and how they are applied? (Support for the indirect rates could consist of cost breakdowns, trends, and budgetary data.)</td><td align="right"></td><td align="left"></td></tr><tr><td align="center" colspan="4" scope="row"><i>OTHER COSTS</i></td></tr><tr><td align="left" scope="row">27. FAR 15.408, Table 15-2, Section II Paragraph D</td><td align="left">Does the proposal include other direct costs and the basis for pricing? If travel is included does the proposal include number of trips, number of people, number of days per trip, locations, and rates (e.g. airfare, per diem, hotel, car rental, etc)?</td><td align="right"></td><td align="left"></td></tr><tr><td align="left" scope="row">28. FAR 15.408, Table 15-2, Section II Paragraph E</td><td align="left">If royalties exceed $1,500 does the proposal provide the information/data identified by Table 15-2?</td><td align="right"></td><td align="left"></td></tr><tr><td align="left" scope="row">29. FAR 15.408, Table 15-2, Section II Paragraph F</td><td align="left">When facilities capital cost of money is proposed, does the proposal include submission of Form CASB-CMF or reference to an FPRA/FPRP and show the calculation of the proposed amount?</td><td align="right"></td><td align="left"></td></tr><tr><td align="center" colspan="4" scope="row"><i>FORMATS FOR SUBMISSION OF LINE ITEM SUMMARIES</i></td></tr><tr><td align="left" scope="row">30. FAR 15.408, Table 15-2, Section III</td><td align="left">Are all cost element breakdowns provided using the applicable format prescribed in FAR 15.408, Table 15-2 III? (or alternative format if specified in the request for proposal)</td><td align="right"></td><td align="left"></td></tr><tr><td align="left" scope="row">31. FAR 15.408, Table 15-2, Section III Paragraph B</td><td align="left">If the proposal is for a modification or change order, have cost of work deleted (credits) and cost of work added (debits) been provided in the format described in FAR 15.408, Table 15-2.III.B?</td><td align="right"></td><td align="left"></td></tr><tr><td align="left" scope="row">32. FAR 15.408, Table 15-2, Section III Paragraph C</td><td align="left">For price revisions/redeterminations, does the proposal follow the format in FAR 15.408, Table 15-2.III.C?</td><td align="right"></td><td align="left"></td></tr><tr><td align="center" colspan="4" scope="row"><i>OTHER</i></td></tr><tr><td align="left" scope="row">33. FAR 16.4</td><td align="left">If an incentive contract type, does the proposal include offeror proposed target cost, target profit or fee, share ratio, and, when applicable, minimum/maximum fee, ceiling price?</td><td align="right"></td><td align="left"></td></tr><tr><td align="left" scope="row">34. FAR 16.203-4 and FAR 15.408 Table 15-2, Section II, Paragraphs A, B, C, and D</td><td align="left">If Economic Price Adjustments are being proposed, does the proposal show the rationale and application for the economic price adjustment?</td><td align="right"></td><td align="left"></td></tr><tr><td align="left" scope="row">35. FAR 52.232-28</td><td align="left">If the offeror is proposing Performance-Based Payments-did the offeror comply with FAR 52.232-28?</td><td align="right"></td><td align="left"></td></tr><tr><td align="left" scope="row">36. FAR 15.408(n)<br>FAR 52.215-22<br>FAR 52.215-23</br></br></td><td align="left">Excessive Pass-through Charges-Identification of Subcontract Effort: If the offeror intends to subcontract more than 70% of the total cost of work to be performed, does the proposal identify:<br>(i) the amount of the offeror''s indirect costs and profit applicable to the work to be performed by the proposed subcontractor(s); and (ii) a description of the added value provided by the offeror as related to the work to be performed by the proposed subcontractor(s)?</br></td><td align="right"></td><td align="left"></td></tr></tbody></table></div>'
--               '<p>The offeror shall complete the following checklist, providing location of requested information, or an explanation of why the requested information is not provided. In preparation of the offeror''s checklist, offerors may elect to have their prospective subcontractors use the same or similar checklist as appropriate.</p><div><p>Proposal Adequacy Checklist</p></div><div><table border="&quot;1&quot;" cellpadding="&quot;1&quot;" cellspacing="&quot;1&quot;" frame="&quot;void&quot;" width="&quot;100%&quot;"><tbody><tr><th scope="&quot;col&quot;">References</th><th scope="&quot;col&quot;">Submission item</th><th scope="&quot;col&quot;">Proposal page No.</th><th scope="&quot;col&quot;">If not provided<br>EXPLAIN<br>(may use<br>continuation<br>pages)</br></br></br></br></th></tr><tr><td align="&quot;center&quot;" colspan="&quot;4&quot;" scope="&quot;row&quot;"><i>GENERAL INSTRUCTIONS</i></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">1. FAR 15.408, Table 15-2, Section I Paragraph A</td><td align="&quot;left&quot;">Is there a properly completed first page of the proposal per FAR 15.408 Table 15-2 I.A or as specified in the solicitation?</td><td align="&quot;right&quot;"></td><td align="&quot;left&quot;"></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">2. FAR 15.408, Table 15-2, Section I Paragraph A(7)</td><td align="&quot;left&quot;">Does the proposal identify the need for Government-furnished material/tooling/test equipment? Include the accountable contract number and contracting officer contact information if known.</td><td align="&quot;right&quot;"></td><td align="&quot;left&quot;"></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">3. FAR 15.408, Table 15-2, Section I Paragraph A(8)</td><td align="&quot;left&quot;">Does the proposal identify and explain notifications of noncompliance with Cost Accounting Standards Board or Cost Accounting Standards (CAS); any proposal inconsistencies with your disclosed practices or applicable CAS; and inconsistencies with your established estimating and accounting principles and procedures?</td><td align="&quot;right&quot;"></td><td align="&quot;left&quot;"></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">4. FAR 15.408, Table 15-2, Section I, Paragraph C(1)</td><td align="&quot;left&quot;">Does the proposal disclose any other known activity that could materially impact the costs?</td><td align="&quot;right&quot;"></td><td align="&quot;left&quot;"></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">FAR 2.101, ''??Cost or pricing data''?</td><td align="&quot;left&quot;">This may include, but is not limited to, such factors as''??<br>(1) Vendor quotations;<br>(2) Nonrecurring costs;<br>(3) Information on changes in production methods and in production or purchasing volume;<br>(4) Data supporting projections of business prospects and objectives and related operations costs;</br></br></br></br></td><td align="&quot;right&quot;"></td><td align="&quot;left&quot;"></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">&nbsp;&nbsp;&nbsp;</td><td align="&quot;left&quot;">(5) Unit-cost trends such as those associated with labor efficiency;<br>(6) Make-or-buy decisions;</br></td><td align="&quot;right&quot;"></td><td align="&quot;left&quot;"></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">&nbsp;&nbsp;&nbsp;</td><td align="&quot;left&quot;">(7) Estimated resources to attain business goals; and</td><td align="&quot;right&quot;"></td><td align="&quot;left&quot;"></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">&nbsp;&nbsp;&nbsp;</td><td align="&quot;left&quot;">(8) Information on management decisions that could have a significant bearing on costs.</td><td align="&quot;right&quot;"></td><td align="&quot;left&quot;"></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">5. FAR 15.408, Table 15-2, Section I Paragraph B</td><td align="&quot;left&quot;">Is an Index of all certified cost or pricing data and information accompanying or identified in the proposal provided and appropriately referenced?</td><td align="&quot;right&quot;"></td><td align="&quot;left&quot;"></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">6. FAR 15.403-1(b)</td><td align="&quot;left&quot;">Are there any exceptions to submission of certified cost or pricing data pursuant to FAR 15.403-1(b)? If so, is supporting documentation included in the proposal? (Note questions 18-20.)</td><td align="&quot;right&quot;"></td><td align="&quot;left&quot;"></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">7. FAR 15.408, Table 15-2, Section I Paragraph C(2)(i)</td><td align="&quot;left&quot;">Does the proposal disclose the judgmental factors applied and the mathematical or other methods used in the estimate, including those used in projecting from known data?</td><td align="&quot;right&quot;"></td><td align="&quot;left&quot;"></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">8. FAR 15.408, Table 15-2, Section I Paragraph C(2)(ii)</td><td align="&quot;left&quot;">Does the proposal disclose the nature and amount of any contingencies included in the proposed price?</td><td align="&quot;right&quot;"></td><td align="&quot;left&quot;"></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">9. FAR 15.408 Table 15-2, Section II, Paragraph A or B</td><td align="&quot;left&quot;">Does the proposal explain the basis of all cost estimating relationships (labor hours or material) proposed on other than a discrete basis?</td><td align="&quot;right&quot;"></td><td align="&quot;left&quot;"></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">10. FAR 15.408, Table 15-2, Section I Paragraphs D and E</td><td align="&quot;left&quot;">Is there a summary of total cost by element of cost and are the elements of cost cross-referenced to the supporting cost or pricing data? (Breakdowns for each cost element must be consistent with your cost accounting system, including breakdown by year.)</td><td align="&quot;right&quot;"></td><td align="&quot;left&quot;"></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">11. FAR 15.408, Table 15-2, Section I Paragraphs D and E</td><td align="&quot;left&quot;">If more than one Contract Line Item Number (CLIN) or sub Contract Line Item Number (sub-CLIN) is proposed as required by the RFP, are there summary total amounts covering all line items for each element of cost and is it cross-referenced to the supporting cost or pricing data?</td><td align="&quot;right&quot;"></td><td align="&quot;left&quot;"></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">12. FAR 15.408, Table 15-2, Section I Paragraph F</td><td align="&quot;left&quot;">Does the proposal identify any incurred costs for work performed before the submission of the proposal?</td><td align="&quot;right&quot;"></td><td align="&quot;left&quot;"></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">13. FAR 15.408, Table 15-2, Section I Paragraph G</td><td align="&quot;left&quot;">Is there a Government forward pricing rate agreement (FPRA)? If so, the offeror shall identify the official submittal of such rate and factor data. If not, does the proposal include all rates and factors by year that are utilized in the development of the proposal and the basis for those rates and factors?</td><td align="&quot;right&quot;"></td><td align="&quot;left&quot;"></td></tr><tr><td align="&quot;center&quot;" colspan="&quot;4&quot;" scope="&quot;row&quot;"><i>COST ELEMENTS</i></td></tr><tr><td align="&quot;center&quot;" colspan="&quot;4&quot;" scope="&quot;row&quot;">MATERIALS AND SERVICES</td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">14. FAR 15.408, Table 15-2, Section II Paragraph A</td><td align="&quot;left&quot;">Does the proposal include a consolidated summary of individual material and services, frequently referred to as a Consolidated Bill of Material (CBOM), to include the basis for pricing? The offeror''s consolidated summary shall include raw materials, parts, components, assemblies, subcontracts and services to be produced or performed by others, identifying as a minimum the item, source, quantity, and price.</td><td align="&quot;right&quot;"></td><td align="&quot;left&quot;"></td></tr><tr><td align="&quot;center&quot;" colspan="&quot;4&quot;" scope="&quot;row&quot;">SUBCONTRACTS (Purchased materials or services)</td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">15. DFARS 215.404-3</td><td align="&quot;left&quot;">Has the offeror identified in the proposal those subcontractor proposals, for which the contracting officer has initiated or may need to request field pricing analysis?</td><td align="&quot;right&quot;"></td><td align="&quot;left&quot;"></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">16. FAR 15.404-3(c)<br>FAR 52.244-2</br></td><td align="&quot;left&quot;">Per the thresholds of FAR 15.404-3(c), Subcontract Pricing Considerations, does the proposal include a copy of the applicable subcontractor''s certified cost or pricing data?</td><td align="&quot;right&quot;"></td><td align="&quot;left&quot;"></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">17. FAR 15.408, Table 15-2, Note 1; Section II Paragraph A</td><td align="&quot;left&quot;">Is there a price/cost analysis establishing the reasonableness of each of the proposed subcontracts included with the proposal? If the offeror''s price/cost analyses are not provided with the proposal, does the proposal include a matrix identifying dates for receipt of subcontractor proposal, completion of fact finding for purposes of price/cost analysis, and submission of the price/cost analysis?</td><td align="&quot;right&quot;"></td><td align="&quot;left&quot;"></td></tr><tr><td align="&quot;center&quot;" colspan="&quot;4&quot;" scope="&quot;row&quot;"><i>EXCEPTIONS TO CERTIFIED COST OR PRICING DATA</i></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">18. FAR 52.215-20<br>FAR 2.101, ''??commercial item''?</br></td><td align="&quot;left&quot;">Has the offeror submitted an exception to the submission of certified cost or pricing data for commercial items proposed either at the prime or subcontractor level, in accordance with provision 52.215-20?</td><td align="&quot;right&quot;"></td><td align="&quot;left&quot;"></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">&nbsp;&nbsp;&nbsp;</td><td align="&quot;left&quot;">a. Has the offeror specifically identified the type of commercial item claim (FAR 2.101 commercial item definition, paragraphs (1) through (8)), and the basis on which the item meets the definition?</td><td align="&quot;right&quot;"></td><td align="&quot;left&quot;"></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">&nbsp;&nbsp;&nbsp;</td><td align="&quot;left&quot;">b. For modified commercial items (FAR 2.101 commercial item definition paragraph (3)); did the offeror classify the modification(s) as either''??</td><td align="&quot;right&quot;"></td><td align="&quot;left&quot;"></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">&nbsp;&nbsp;&nbsp;</td><td align="&quot;left&quot;">i. A modification of a type customarily available in the commercial marketplace (paragraph (3)(i)); or</td><td align="&quot;right&quot;"></td><td align="&quot;left&quot;"></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">&nbsp;&nbsp;&nbsp;</td><td align="&quot;left&quot;">ii. A minor modification (paragraph (3)(ii)) of a type not customarily available in the commercial marketplace made to meet Federal Government requirements not exceeding the thresholds in FAR 15.403-1(c)(3)(iii)(B)?</td><td align="&quot;right&quot;"></td><td align="&quot;left&quot;"></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">&nbsp;&nbsp;&nbsp;</td><td align="&quot;left&quot;">c. For proposed commercial items ''??of a type''?, or ''??evolved''? or modified (FAR 2.101 commercial item definition paragraphs (1) through (3)), did the contractor provide a technical description of the differences between the proposed item and the comparison item(s)?</td><td align="&quot;right&quot;"></td><td align="&quot;left&quot;"></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">19.</td><td align="&quot;left&quot;">[Reserved]</td><td align="&quot;right&quot;"></td><td align="&quot;left&quot;"></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">20. FAR 15.408, Table 15-2, Section II Paragraph A(1)</td><td align="&quot;left&quot;">Does the proposal support the degree of competition and the basis for establishing the source and reasonableness of price for each subcontract or purchase order priced on a competitive basis exceeding the threshold for certified cost or pricing data?</td><td align="&quot;right&quot;"></td><td align="&quot;left&quot;"></td></tr><tr><td align="&quot;center&quot;" colspan="&quot;4&quot;" scope="&quot;row&quot;">INTERORGANIZATIONAL TRANSFERS</td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">21. FAR 15.408, Table 15-2, Section II Paragraph A.(2)</td><td align="&quot;left&quot;">For inter-organizational transfers proposed at cost, does the proposal include a complete cost proposal in compliance with Table 15-2?</td><td align="&quot;right&quot;"></td><td align="&quot;left&quot;"></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">22. FAR 15.408, Table 15-2, Section II Paragraph A(1)</td><td align="&quot;left&quot;">For inter-organizational transfers proposed at price in accordance with FAR 31.205-26(e), does the proposal provide an analysis by the prime that supports the exception from certified cost or pricing data in accordance with FAR 15.403-1?</td><td align="&quot;right&quot;"></td><td align="&quot;left&quot;"></td></tr><tr><td align="&quot;center&quot;" colspan="&quot;4&quot;" scope="&quot;row&quot;">DIRECT LABOR</td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">23. FAR 15.408, Table 15-2, Section II Paragraph B</td><td align="&quot;left&quot;">Does the proposal include a time phased (<i>i.e.</i>; monthly, quarterly) breakdown of labor hours, rates and costs by category or skill level? If labor is the allocation base for indirect costs, the labor cost must be summarized in order that the applicable overhead rate can be applied.</td><td align="&quot;right&quot;"></td><td align="&quot;left&quot;"></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">24. FAR 15.408, Table 15-2, Section II Paragraph B</td><td align="&quot;left&quot;">For labor Basis of Estimates (BOEs), does the proposal include labor categories, labor hours, and task descriptions''??(e.g.; Statement of Work reference, applicable CLIN, Work Breakdown Structure, rationale for estimate, applicable history, and time-phasing)?</td><td align="&quot;right&quot;"></td><td align="&quot;left&quot;"></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">25. FAR subpart 22.10</td><td align="&quot;left&quot;">If covered by the Service Contract Labor Standards statute (41 U.S.C. chapter 67), are the rates in the proposal in compliance with the minimum rates specified in the statute?</td><td align="&quot;right&quot;"></td><td align="&quot;left&quot;"></td></tr><tr><td align="&quot;center&quot;" colspan="&quot;4&quot;" scope="&quot;row&quot;"><i>INDIRECT COSTS</i></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">26. FAR 15.408, Table 15-2, Section II Paragraph C</td><td align="&quot;left&quot;">Does the proposal indicate the basis of estimate for proposed indirect costs and how they are applied? (Support for the indirect rates could consist of cost breakdowns, trends, and budgetary data.)</td><td align="&quot;right&quot;"></td><td align="&quot;left&quot;"></td></tr><tr><td align="&quot;center&quot;" colspan="&quot;4&quot;" scope="&quot;row&quot;"><i>OTHER COSTS</i></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">27. FAR 15.408, Table 15-2, Section II Paragraph D</td><td align="&quot;left&quot;">Does the proposal include other direct costs and the basis for pricing? If travel is included does the proposal include number of trips, number of people, number of days per trip, locations, and rates (e.g. airfare, per diem, hotel, car rental, etc)?</td><td align="&quot;right&quot;"></td><td align="&quot;left&quot;"></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">28. FAR 15.408, Table 15-2, Section II Paragraph E</td><td align="&quot;left&quot;">If royalties exceed $1,500 does the proposal provide the information/data identified by Table 15-2?</td><td align="&quot;right&quot;"></td><td align="&quot;left&quot;"></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">29. FAR 15.408, Table 15-2, Section II Paragraph F</td><td align="&quot;left&quot;">When facilities capital cost of money is proposed, does the proposal include submission of Form CASB-CMF or reference to an FPRA/FPRP and show the calculation of the proposed amount?</td><td align="&quot;right&quot;"></td><td align="&quot;left&quot;"></td></tr><tr><td align="&quot;center&quot;" colspan="&quot;4&quot;" scope="&quot;row&quot;"><i>FORMATS FOR SUBMISSION OF LINE ITEM SUMMARIES</i></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">30. FAR 15.408, Table 15-2, Section III</td><td align="&quot;left&quot;">Are all cost element breakdowns provided using the applicable format prescribed in FAR 15.408, Table 15-2 III? (or alternative format if specified in the request for proposal)</td><td align="&quot;right&quot;"></td><td align="&quot;left&quot;"></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">31. FAR 15.408, Table 15-2, Section III Paragraph B</td><td align="&quot;left&quot;">If the proposal is for a modification or change order, have cost of work deleted (credits) and cost of work added (debits) been provided in the format described in FAR 15.408, Table 15-2.III.B?</td><td align="&quot;right&quot;"></td><td align="&quot;left&quot;"></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">32. FAR 15.408, Table 15-2, Section III Paragraph C</td><td align="&quot;left&quot;">For price revisions/redeterminations, does the proposal follow the format in FAR 15.408, Table 15-2.III.C?</td><td align="&quot;right&quot;"></td><td align="&quot;left&quot;"></td></tr><tr><td align="&quot;center&quot;" colspan="&quot;4&quot;" scope="&quot;row&quot;"><i>OTHER</i></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">33. FAR 16.4</td><td align="&quot;left&quot;">If an incentive contract type, does the proposal include offeror proposed target cost, target profit or fee, share ratio, and, when applicable, minimum/maximum fee, ceiling price?</td><td align="&quot;right&quot;"></td><td align="&quot;left&quot;"></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">34. FAR 16.203-4 and FAR 15.408 Table 15-2, Section II, Paragraphs A, B, C, and D</td><td align="&quot;left&quot;">If Economic Price Adjustments are being proposed, does the proposal show the rationale and application for the economic price adjustment?</td><td align="&quot;right&quot;"></td><td align="&quot;left&quot;"></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">35. FAR 52.232-28</td><td align="&quot;left&quot;">If the offeror is proposing Performance-Based Payments''??did the offeror comply with FAR 52.232-28?</td><td align="&quot;right&quot;"></td><td align="&quot;left&quot;"></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">36. FAR 15.408(n)<br>FAR 52.215-22<br>FAR 52.215-23</br></br></td><td align="&quot;left&quot;">Excessive Pass-through Charges''??Identification of Subcontract Effort: If the offeror intends to subcontract more than 70% of the total cost of work to be performed, does the proposal identify:<br>(i) the amount of the offeror''s indirect costs and profit applicable to the work to be performed by the proposed subcontractor(s); and (ii) a description of the added value provided by the offeror as related to the work to be performed by the proposed subcontractor(s)?</br></td><td align="&quot;right&quot;"></td><td align="&quot;left&quot;"></td></tr></tbody></table></div>'
WHERE clause_id = 93315; -- 252.215-7009

UPDATE Clauses
SET effective_date = '2011-02-01' -- NULL
WHERE clause_id = 93321; -- 252.216-7005

UPDATE Clauses
SET effective_date = '2014-04-01' -- NULL
WHERE clause_id = 93327; -- 252.216-7010

UPDATE Clauses
SET effective_date = '1991-12-01' -- NULL
WHERE clause_id = 93348; -- 252.217-7028

UPDATE Clauses
SET effective_date = '2007-09-01' -- NULL
WHERE clause_id = 93355; -- 252.219-7009

UPDATE Clauses
SET effective_date = '1998-06-01' -- NULL
WHERE clause_id = 93356; -- 252.219-7010

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1223_67001&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?node=se48.3.252_1223_67001&amp;rgn=div8'
, clause_data = '<p>(a) ''Hazardous material,'' as used in this clause, is defined in the Hazardous Material Identification and Material Safety Data clause of this contract.</p><p>(b) The Contractor shall label the item package (unit container) of any hazardous material to be delivered under this contract in accordance with the Hazard Communication Standard (29 CFR 1910.1200 <i>et seq</i>). The Standard requires that the hazard warning label conform to the requirements of the standard unless the material is otherwise subject to the labelling requirements of one of the following statutes:</p><p>(1) Federal Insecticide, Fungicide and Rodenticide Act;</p><p>(2) Federal Food, Drug and Cosmetics Act;</p><p>(3) Consumer Product Safety Act;</p><p>(4) Federal Hazardous Substances Act; or</p><p>(5) Federal Alcohol Administration Act.</p><p>(c) The Offeror shall list which hazardous material listed in the Hazardous Material Identification and Material Safety Data clause of this contract will be labelled in accordance with one of the Acts in paragraphs (b) (1) through (5) of this clause instead of the Hazard Communication Standard. Any hazardous material not listed will be interpreted to mean that a label is required in accordance with the Hazard Communication Standard.</p><div><div><table border="1" cellpadding="1" cellspacing="1" frame="void" width="100%"><tbody><tr><th scope="col">Material (if none, insert ''none.'')</th><th scope="col">Act</th></tr><tr><td align="left" scope="row">{{textbox_252.223-7001[0]}}</td><td align="left">{{textbox_252.223-7001[1]}}</td></tr><tr><td align="left" scope="row">{{textbox_252.223-7001[2]}}</td><td align="left">{{textbox_252.223-7001[3]}}</td></tr></tbody></table></div></div><p>(d) The apparently successful Offeror agrees to submit, before award, a copy of the hazard warning label for all hazardous materials not listed in paragraph (c) of this clause. The Offeror shall submit the label with the Material Safety Data Sheet being furnished under the Hazardous Material Identification and Material Safety Data clause of this contract.</p><p>(e) The Contractor shall also comply with MIL-STD-129, Marking for Shipment and Storage (including revisions adopted during the term of this contract).</p>'
--               '<p>(a) ''Hazardous material,'' as used in this clause, is defined in the Hazardous Material Identification and Material Safety Data clause of this contract.</p><p>(b) The Contractor shall label the item package (unit container) of any hazardous material to be delivered under this contract in accordance with the Hazard Communication Standard (29 CFR 1910.1200 <i>et seq</i>). The Standard requires that the hazard warning label conform to the requirements of the standard unless the material is otherwise subject to the labelling requirements of one of the following statutes:</p><p>(1) Federal Insecticide, Fungicide and Rodenticide Act;</p><p>(2) Federal Food, Drug and Cosmetics Act;</p><p>(3) Consumer Product Safety Act;</p><p>(4) Federal Hazardous Substances Act; or</p><p>(5) Federal Alcohol Administration Act.</p><p>(c) The Offeror shall list which hazardous material listed in the Hazardous Material Identification and Material Safety Data clause of this contract will be labelled in accordance with one of the Acts in paragraphs (b) (1) through (5) of this clause instead of the Hazard Communication Standard. Any hazardous material not listed will be interpreted to mean that a label is required in accordance with the Hazard Communication Standard.</p>\n<div><div><table border="&quot;1&quot;" cellpadding="&quot;1&quot;" cellspacing="&quot;1&quot;" frame="&quot;void&quot;" width="&quot;100%&quot;"><tbody><tr><th scope="&quot;col&quot;">Material (if none, insert ''none.'')</th><th scope="&quot;col&quot;">Act</th></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">{{textbox_252.223-7001[0]}}</td><td align="&quot;left&quot;">{{textbox_252.223-7001[1]}}</td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">{{textbox_252.223-7001[2]}}</td><td align="&quot;left&quot;">{{textbox_252.223-7001[3]}}</td></tr></tbody></table></div></div><p>(d) The apparently successful Offeror agrees to submit, before award, a copy of the hazard warning label for all hazardous materials not listed in paragraph (c) of this clause. The Offeror shall submit the label with the Material Safety Data Sheet being furnished under the Hazardous Material Identification and Material Safety Data clause of this contract.</p><p>(e) The Contractor shall also comply with MIL-STD-129, Marking for Shipment and Storage (including revisions adopted during the term of this contract).</p>'
WHERE clause_id = 93366; -- 252.223-7001
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (93366, 'textbox_252.223-7001[0]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (93366, 'textbox_252.223-7001[1]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (93366, 'textbox_252.223-7001[2]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (93366, 'textbox_252.223-7001[3]', 'S', 100);

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1223_67007&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?node=se48.3.252_1223_67007&amp;rgn=div8'
, clause_data = '<p>(a) <i>Definition.</i> ''Arms, ammunition, and explosives (AA&E),'' as used in this clause, means those items within the scope (chapter 1, paragraph B) of DoD 5100.76-M, Physical Security of Sensitive Conventional Arms, Ammunition, and Explosives.</p><p>(b) The requirements of DoD 5100.76-M apply to the following items of AA&E being developed, produced, manufactured, or purchased for the Government, or provided to the Contractor as Government-furnished property under this contract:</p><div><div><table border="1" cellpadding="1" cellspacing="1" frame="void" width="100%"><tbody><tr><th scope="col">Nomenclature</th><th scope="col">National stock<br>number</br></th><th scope="col">Sensitivity category</th></tr><tr><td>{{textbox_252.223-7007[0]}}</td><td>{{textbox_252.223-7007[1]}}</td><td>{{textbox_252.223-7007[2]}}</td></tr><tr><td>{{textbox_252.223-7007[3]}}</td><td>{{textbox_252.223-7007[4]}}</td><td>{{textbox_252.223-7007[5]}}</td></tr></tbody></table></div></div><p>(c) The Contractor shall comply with the requirements of DoD 5100.76-M, as specified in the statement of work. The edition of DoD 5100.76-M in effect on the date of issuance of the solicitation for this contract shall apply.</p><p>(d) The Contractor shall allow representatives of the Defense Security Service (DSS), and representatives of other appropriate offices of the Government, access at all reasonable times into its facilities and those of its subcontractors, for the purpose of performing surveys, inspections, and investigations necessary to review compliance with the physical security standards applicable to this contract.</p><p>(e) The Contractor shall notify the cognizant DSS field office of any subcontract involving AA&E within 10 days after award of the subcontract.</p><p>(f) The Contractor shall ensure that the requirements of this clause are included in all subcontracts, at every tier-</p><p>(1) For the development, production, manufacture, or purchase of AA&E; or</p><p>(2) When AA&E will be provided to the subcontractor as Government-furnished property.</p><p>(g) Nothing in this clause shall relieve the Contractor of its responsibility for complying with applicable Federal, state, and local laws, ordinances, codes, and regulations (including requirements for obtaining licenses and permits) in connection with the performance of this contract.</p>'
--               '<p>(a) <i>Definition.</i> ''Arms, ammunition, and explosives (AA&E),'' as used in this clause, means those items within the scope (chapter 1, paragraph B) of DoD 5100.76-M, Physical Security of Sensitive Conventional Arms, Ammunition, and Explosives.</p><p>(b) The requirements of DoD 5100.76-M apply to the following items of AA&E being developed, produced, manufactured, or purchased for the Government, or provided to the Contractor as Government-furnished property under this contract:</p>\n<div><div><table border="&quot;1&quot;" cellpadding="&quot;1&quot;" cellspacing="&quot;1&quot;" frame="&quot;void&quot;" width="&quot;100%&quot;"><tbody><tr><th scope="&quot;col&quot;">Nomenclature</th><th scope="&quot;col&quot;">National stock<br>number</br></th><th scope="&quot;col&quot;">Sensitivity category</th></tr>\n<tr><td align="&quot;left&quot;" scope="&quot;row&quot;">{{textbox_252.223-7007[0]}}</td><td align="&quot;right&quot;">{{textbox_252.223-7007[1]}}</td><td align="&quot;right&quot;">{{textbox_252.223-7007[2]}}</td></tr>\n<tr><td align="&quot;left&quot;" scope="&quot;row&quot;">{{textbox_252.223-7007[3]}}</td><td align="&quot;right&quot;">{{textbox_252.223-7007[4]}}</td><td align="&quot;right&quot;">{{textbox_252.223-7007[5]}}</td></tr>\n<tr><td align="&quot;left&quot;" scope="&quot;row&quot;">{{textbox_252.223-7007[6]}}</td><td align="&quot;right&quot;">{{textbox_252.223-7007[7]}}</td><td align="&quot;right&quot;">{{textbox_252.223-7007[8]}}</td></tr>\n<tr><td align="&quot;left&quot;" scope="&quot;row&quot;">{{textbox_252.223-7007[9]}}</td><td align="&quot;right&quot;">{{textbox_252.223-7007[10]}}</td><td align="&quot;right&quot;">{{textbox_252.223-7007[11]}}</td></tr>\n<tr><td align="&quot;left&quot;" scope="&quot;row&quot;">{{textbox_252.223-7007[12]}}</td><td align="&quot;right&quot;">{{textbox_252.223-7007[13]}}</td><td align="&quot;right&quot;">{{textbox_252.223-7007[14]}}</td></tr>\n</tbody></table></div></div><p>(c) The Contractor shall comply with the requirements of DoD 5100.76-M, as specified in the statement of work. The edition of DoD 5100.76-M in effect on the date of issuance of the solicitation for this contract shall apply.</p><p>(d) The Contractor shall allow representatives of the Defense Security Service (DSS), and representatives of other appropriate offices of the Government, access at all reasonable times into its facilities and those of its subcontractors, for the purpose of performing surveys, inspections, and investigations necessary to review compliance with the physical security standards applicable to this contract.</p><p>(e) The Contractor shall notify the cognizant DSS field office of any subcontract involving AA&E within 10 days after award of the subcontract.</p><p>(f) The Contractor shall ensure that the requirements of this clause are included in all subcontracts, at every tier-</p><p>(1) For the development, production, manufacture, or purchase of AA&E; or</p><p>(2) When AA&E will be provided to the subcontractor as Government-furnished property.</p><p>(g) Nothing in this clause shall relieve the Contractor of its responsibility for complying with applicable Federal, state, and local laws, ordinances, codes, and regulations (including requirements for obtaining licenses and permits) in connection with the performance of this contract.</p>'
WHERE clause_id = 93372; -- 252.223-7007
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 28329; -- textbox_252.223-7007[10]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 28330; -- textbox_252.223-7007[11]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 28331; -- textbox_252.223-7007[12]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 28332; -- textbox_252.223-7007[13]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 28333; -- textbox_252.223-7007[14]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 28325; -- textbox_252.223-7007[6]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 28326; -- textbox_252.223-7007[7]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 28327; -- textbox_252.223-7007[8]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 28328; -- textbox_252.223-7007[9]

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1225_67001&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?node=se48.3.252_1225_67001&amp;rgn=div8'
, clause_data = '<p>(a) <i>Definitions.</i> As used in this clause-</p><p><i>Commercially available off-the-shelf (COTS) item</i>-</p><p>(i) Means any item of supply (including construction material) that is-</p><p>(A) A commercial item (as defined in paragraph (1) of the definition of ''commercial item'' in section 2.101 of the Federal Acquisition Regulation);</p><p>(B) Sold in substantial quantities in the commercial marketplace; and</p><p>(C) Offered to the Government, under a contract or subcontract at any tier, without modification, in the same form in which it is sold in the commercial marketplace; and</p><p>(ii) Does not include bulk cargo, as defined in 46 U.S.C. 40102(4), such as agricultural products and petroleum products.</p><p><i>Component</i> means an article, material, or supply incorporated directly into an end product. </p><p><i>Domestic end product means</i>-</p><p>(i) An unmanufactured end product that has been mined or produced in the United States; or </p><p>(ii) An end product manufactured in the United States if-</p><p>(A) The cost of its qualifying country components and its components that are mined, produced, or manufactured in the United States exceeds 50 percent of the cost of all its components. The cost of components includes transportation costs to the place of incorporation into the end product and U.S. duty (whether or not a duty-free entry certificate is issued). Scrap generated, collected, and prepared for processing in the United States is considered domestic. A component is considered to have been mined, produced, or manufactured in the United States (regardless of its source in fact) if the end product in which it is incorporated is manufactured in the United States and the component is of a class or kind for which the Government has determined that-</p><p>(<i>1</i>) Sufficient and reasonably available commercial quantities of a satisfactory quality are not mined, produced, or manufactured in the United States; or</p><p>(<i>2</i>) It is inconsistent with the public interest to apply the restrictions of the Buy American statute; or</p><p>(B) The end product is a COTS item. </p><p><i>End product</i> means those articles, materials, and supplies to be acquired under this contract for public use. </p><p><i>Foreign end product</i> means an end product other than a domestic end product.</p><p><i>Qualifying country</i> means a country with a reciprocal defense procurement memorandum of understanding or international agreement with the United States in which both countries agree to remove barriers to purchases of supplies produced in the other country or services performed by sources of the other country, and the memorandum or agreement complies, where applicable, with the requirements of section 36 of the Arms Export Control Act (22 U.S.C. 2776) and with 10 U.S.C. 2457. Accordingly, the following are qualifying countries:</p><table><tbody><tr><td>Australia</td><td>&nbsp;&nbsp;&nbsp;Italy</td></tr><tr><td>Austria</td><td>&nbsp;&nbsp;&nbsp;Luxembourg</td></tr><tr><td>Belgium</td><td>&nbsp;&nbsp;&nbsp;Netherlands</td></tr><tr><td>Canada</td><td>&nbsp;&nbsp;&nbsp;Norway</td></tr><tr><td>Czech Republic</td><td>&nbsp;&nbsp;&nbsp;Poland</td></tr><tr><td>Denmark</td><td>&nbsp;&nbsp;&nbsp;Portugal</td></tr><tr><td>Egypt</td><td>&nbsp;&nbsp;&nbsp;Spain</td></tr><tr><td>Finland</td><td>&nbsp;&nbsp;&nbsp;Sweden</td></tr><tr><td>France</td><td>&nbsp;&nbsp;&nbsp;Switzerland</td></tr><tr><td>Germany</td><td>&nbsp;&nbsp;&nbsp;Turkey</td></tr><tr><td>Greece</td><td>&nbsp;&nbsp;&nbsp;United Kingdom of Great Britain and Northern Ireland.</td></tr><tr><td>Israel</td><td>&nbsp;</td></tr></tbody></table><p><i>Qualifying country component</i> means a component mined, produced, or manufactured in a qualifying country. </p><p><i>Qualifying country end product</i> means-</p><p>(i) An unmanufactured end product mined or produced in a qualifying country; or</p><p>(ii) An end product manufactured in a qualifying country if-</p><p>(A) The cost of the following types of components exceeds 50 percent of the cost of all its components:</p><p>(<i>1</i>) Components mined, produced, or manufactured in a qualifying country.</p><p>(<i>2</i>) Components mined, produced, or manufactured in the United States.</p><p>(<i>3</i>) Components of foreign origin of a class or kind for which the Government has determined that sufficient and reasonably available commercial quantities of a satisfactory quality are not mined, produced, or manufactured in the United States; or</p><p>(B) The end product is a COTS item. </p><p><i>United States</i> means the 50 States, the District of Columbia, and outlying areas.</p><p>(b) This clause implements, Buy American. In accordance with 41 U.S.C. 1907, the component test of the Buy American statute is waived for an end product that is a COTS item (see section 12.505(a)(1) of the Federal Acquisition Regulation). Unless otherwise specified, this clause applies to all line items in the contract.</p><p>(c) The Contractor shall deliver only domestic end products unless, in its offer, it specified delivery of other end products in the Buy American -Balance of Payments Program Certificate provision of the solicitation. If the Contractor certified in its offer that it will deliver a qualifying country end product, the Contractor shall deliver a qualifying country end product or, at the Contractor''s option, a domestic end product. </p><p>(d) The contract price does not include duty for end products or components for which the Contractor will claim duty-free entry.</p>'
--               '<p>(a) <i>Definitions.</i> As used in this clause-</p><p><i>Commercially available off-the-shelf (COTS) item</i>-</p><p>(i) Means any item of supply (including construction material) that is-</p><p>(A) A commercial item (as defined in paragraph (1) of the definition of ''commercial item'' in section 2.101 of the Federal Acquisition Regulation);</p><p>(B) Sold in substantial quantities in the commercial marketplace; and</p><p>(C) Offered to the Government, under a contract or subcontract at any tier, without modification, in the same form in which it is sold in the commercial marketplace; and</p><p>(ii) Does not include bulk cargo, as defined in 46 U.S.C. 40102(4), such as agricultural products and petroleum products.</p><p><i>Component</i> means an article, material, or supply incorporated directly into an end product. </p><p><i>Domestic end product means</i>-</p><p>(i) An unmanufactured end product that has been mined or produced in the United States; or </p><p>(ii) An end product manufactured in the United States if-</p><p>(A) The cost of its qualifying country components and its components that are mined, produced, or manufactured in the United States exceeds 50 percent of the cost of all its components. The cost of components includes transportation costs to the place of incorporation into the end product and U.S. duty (whether or not a duty-free entry certificate is issued). Scrap generated, collected, and prepared for processing in the United States is considered domestic. A component is considered to have been mined, produced, or manufactured in the United States (regardless of its source in fact) if the end product in which it is incorporated is manufactured in the United States and the component is of a class or kind for which the Government has determined that-</p><p>(<i>1</i>) Sufficient and reasonably available commercial quantities of a satisfactory quality are not mined, produced, or manufactured in the United States; or</p><p>(<i>2</i>) It is inconsistent with the public interest to apply the restrictions of the Buy American statute; or</p><p>(B) The end product is a COTS item. </p><p><i>End product</i> means those articles, materials, and supplies to be acquired under this contract for public use. </p><p><i>Foreign end product</i> means an end product other than a domestic end product.</p><p><i>Qualifying country</i> means a country with a reciprocal defense procurement memorandum of understanding or international agreement with the United States in which both countries agree to remove barriers to purchases of supplies produced in the other country or services performed by sources of the other country, and the memorandum or agreement complies, where applicable, with the requirements of section 36 of the Arms Export Control Act (22 U.S.C. 2776) and with 10 U.S.C. 2457. Accordingly, the following are qualifying countries:</p><table auto;"=""><tbody><tr><td>Australia</td><td>&nbsp;&nbsp;&nbsp;Italy</td></tr><tr><td>Austria</td><td>&nbsp;&nbsp;&nbsp;Luxembourg</td></tr><tr><td>Belgium</td><td>&nbsp;&nbsp;&nbsp;Netherlands</td></tr><tr><td>Canada</td><td>&nbsp;&nbsp;&nbsp;Norway</td></tr><tr><td>Czech Republic</td><td>&nbsp;&nbsp;&nbsp;Poland</td></tr><tr><td>Denmark</td><td>&nbsp;&nbsp;&nbsp;Portugal</td></tr><tr><td>Egypt</td><td>&nbsp;&nbsp;&nbsp;Spain</td></tr><tr><td>Finland</td><td>&nbsp;&nbsp;&nbsp;Sweden</td></tr><tr><td>France</td><td>&nbsp;&nbsp;&nbsp;Switzerland</td></tr><tr><td>Germany</td><td>&nbsp;&nbsp;&nbsp;Turkey</td></tr><tr><td>Greece</td><td>&nbsp;&nbsp;&nbsp;United Kingdom of Great Britain and Northern Ireland.</td></tr><tr><td>Israel</td><td>&nbsp;</td></tr></tbody></table><p><i>Qualifying country component</i> means a component mined, produced, or manufactured in a qualifying country. </p><p><i>Qualifying country end product</i> means-</p><p>(i) An unmanufactured end product mined or produced in a qualifying country; or</p><p>(ii) An end product manufactured in a qualifying country if-</p><p>(A) The cost of the following types of components exceeds 50 percent of the cost of all its components:</p><p>(<i>1</i>) Components mined, produced, or manufactured in a qualifying country.</p><p>(<i>2</i>) Components mined, produced, or manufactured in the United States.</p><p>(<i>3</i>) Components of foreign origin of a class or kind for which the Government has determined that sufficient and reasonably available commercial quantities of a satisfactory quality are not mined, produced, or manufactured in the United States; or</p><p>(B) The end product is a COTS item. </p><p><i>United States</i> means the 50 States, the District of Columbia, and outlying areas.</p><p>(b) This clause implements, Buy American. In accordance with 41 U.S.C. 1907, the component test of the Buy American statute is waived for an end product that is a COTS item (see section 12.505(a)(1) of the Federal Acquisition Regulation). Unless otherwise specified, this clause applies to all line items in the contract.</p><p>(c) The Contractor shall deliver only domestic end products unless, in its offer, it specified delivery of other end products in the Buy American -Balance of Payments Program Certificate provision of the solicitation. If the Contractor certified in its offer that it will deliver a qualifying country end product, the Contractor shall deliver a qualifying country end product or, at the Contractor''s option, a domestic end product. </p><p>(d) The contract price does not include duty for end products or components for which the Contractor will claim duty-free entry.</p>'
WHERE clause_id = 93377; -- 252.225-7001

UPDATE Clauses
SET effective_date = '2013-06-01' -- '2013-01-01'
WHERE clause_id = 93434; -- 252.225-7046

UPDATE Clauses
SET effective_date = '2013-06-01' -- '2013-01-01'
WHERE clause_id = 93435; -- 252.225-7047

UPDATE Clauses
SET effective_date = '2013-06-01' -- '2013-01-01'
WHERE clause_id = 93436; -- 252.225-7048

UPDATE Clauses
SET effective_date = '2014-12-01' -- NULL
WHERE clause_id = 93438; -- 252.225-7050

UPDATE Clauses
SET effective_date = '2015-02-01' -- '2015-01-01'
WHERE clause_id = 93439; -- 252.225-7982 (DEVIATION 2015-00012)

UPDATE Clauses
SET effective_date = '2015-02-01' -- '2015-01-01'
WHERE clause_id = 93440; -- 252.225-7983 (DEVIATION 2015-00012)

UPDATE Clauses
SET effective_date = '2015-02-01' -- '2015-01-01'
WHERE clause_id = 93441; -- 252.225-7984 (DEVIATION 2015-00012)

UPDATE Clauses
SET effective_date = '2014-12-01' -- '2014-01-01'
WHERE clause_id = 93442; -- 252.225-7985 (DEVIATION 2015-00003)

UPDATE Clauses
SET effective_date = '2013-08-01' -- '2013-01-01'
WHERE clause_id = 93454; -- 252.225-7997 (DEVIATION 2013-00017)

UPDATE Clauses
SET effective_date = '1984-08-01' -- NULL
WHERE clause_id = 93468; -- 252.227-7009

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1228_67006&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?node=se48.3.252_1228_67006&amp;rgn=div8'
, clause_data = '<p>(e) The Contractor shall provide the Contracting Officer with a similar representation for all subcontracts with non-Spanish concerns that will perform work in Spain under this contract.</p><p>(f) Insurance policies required herein shall be purchased from Spanish insurance companies or other insurance companies legally authorized to conduct business in Spain. Such policies shall conform to Spanish laws and regulations and shall-</p><p>(1) Contain provisions requiring submission to Spanish law and jurisdiction of any problem that may arise with regard to the interpretation or application of the clauses and conditions of the insurance policy;</p><p>(2) Contain a provision authorizing the insurance company, as subrogee of the insured entity, to assume and attend to directly, with respect to any person damaged, the legal consequences arising from the occurrence of such damages;</p><p>(3) Contain a provision worded as follows: ''The insurance company waives any right of subrogation against the United States of America that may arise by reason of any payment under this policy.'';</p><p>(4) Not contain any deductible amount or similar limitation; and</p><p>(5) Not contain any provisions requiring submission to any type of arbitration.</p><div><table border="1" cellpadding="1" cellspacing="1" frame="void" width="100%"><tbody><tr><th scope="col">Type of insurance</th><th scope="col">Coverage per person</th><th scope="col">Coverage per accident</th><th scope="col">Property damage</th></tr><tr><td align="left" scope="row">Comprehensive General Liability</td><td align="right">$300,000</td><td align="right">$1,000,000</td><td align="right">$100,000</td></tr></tbody></table></div><p>(e) The Contractor shall provide the Contracting Officer with a similar representation for all subcontracts with non-Spanish concerns that will perform work in Spain under this contract.</p><p>(f) Insurance policies required herein shall be purchased from Spanish insurance companies or other insurance companies legally authorized to conduct business in Spain. Such policies shall conform to Spanish laws and regulations and shall-</p><p>(1) Contain provisions requiring submission to Spanish law and jurisdiction of any problem that may arise with regard to the interpretation or application of the clauses and conditions of the insurance policy;</p><p>(2) Contain a provision authorizing the insurance company, as subrogee of the insured entity, to assume and attend to directly, with respect to any person damaged, the legal consequences arising from the occurrence of such damages;</p><p>(3) Contain a provision worded as follows: ''The insurance company waives any right of subrogation against the United States of America that may arise by reason of any payment under this policy.'';</p><p>(4) Not contain any deductible amount or similar limitation; and</p><p>(5) Not contain any provisions requiring submission to any type of arbitration.</p>'
--               '<p>(e) The Contractor shall provide the Contracting Officer with a similar representation for all subcontracts with non-Spanish concerns that will perform work in Spain under this contract.</p><p>(f) Insurance policies required herein shall be purchased from Spanish insurance companies or other insurance companies legally authorized to conduct business in Spain. Such policies shall conform to Spanish laws and regulations and shall''??</p><p>(1) Contain provisions requiring submission to Spanish law and jurisdiction of any problem that may arise with regard to the interpretation or application of the clauses and conditions of the insurance policy;</p><p>(2) Contain a provision authorizing the insurance company, as subrogee of the insured entity, to assume and attend to directly, with respect to any person damaged, the legal consequences arising from the occurrence of such damages;</p><p>(3) Contain a provision worded as follows: ''??The insurance company waives any right of subrogation against the United States of America that may arise by reason of any payment under this policy.''?;</p><p>(4) Not contain any deductible amount or similar limitation; and</p><p>(5) Not contain any provisions requiring submission to any type of arbitration.</p><div><table border="&quot;1&quot;" cellpadding="&quot;1&quot;" cellspacing="&quot;1&quot;" frame="&quot;void&quot;" width="&quot;100%&quot;"><tbody><tr><th scope="&quot;col&quot;">Type of insurance</th><th scope="&quot;col&quot;">Coverage per person</th><th scope="&quot;col&quot;">Coverage per accident</th><th scope="&quot;col&quot;">Property damage</th></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">Comprehensive General Liability</td><td align="&quot;right&quot;">$300,000</td><td align="&quot;right&quot;">$1,000,000</td><td align="&quot;right&quot;">$100,000</td></tr></tbody></table></div><p>(e) The Contractor shall provide the Contracting Officer with a similar representation for all subcontracts with non-Spanish concerns that will perform work in Spain under this contract.</p><p>(f) Insurance policies required herein shall be purchased from Spanish insurance companies or other insurance companies legally authorized to conduct business in Spain. Such policies shall conform to Spanish laws and regulations and shall''??</p><p>(1) Contain provisions requiring submission to Spanish law and jurisdiction of any problem that may arise with regard to the interpretation or application of the clauses and conditions of the insurance policy;</p><p>(2) Contain a provision authorizing the insurance company, as subrogee of the insured entity, to assume and attend to directly, with respect to any person damaged, the legal consequences arising from the occurrence of such damages;</p><p>(3) Contain a provision worded as follows: ''??The insurance company waives any right of subrogation against the United States of America that may arise by reason of any payment under this policy.''?;</p><p>(4) Not contain any deductible amount or similar limitation; and</p><p>(5) Not contain any provisions requiring submission to any type of arbitration.</p>'
WHERE clause_id = 93504; -- 252.228-7006

UPDATE Clauses
SET effective_date = '1997-01-01' -- NULL
WHERE clause_id = 93556; -- 252.236-7003

UPDATE Clauses
SET effective_date = '1991-12-01' -- NULL
WHERE clause_id = 93558; -- 252.236-7005

UPDATE Clauses
SET effective_date = '2013-06-01' -- NULL
WHERE clause_id = 93578; -- 252.237-7010

UPDATE Clauses
SET effective_date = '1991-12-01' -- NULL
WHERE clause_id = 93604; -- 252.239-7014

UPDATE Clauses
SET effective_date = '1995-05-01' -- NULL
WHERE clause_id = 93656; -- 252.247-7021

UPDATE Clauses
SET effective_date = '1992-08-01' -- NULL
WHERE clause_id = 93657; -- 252.247-7022

UPDATE Clauses
SET effective_date = '2013-11-01' -- '2013-01-01'
WHERE clause_id = 92462; -- 52.202-1

UPDATE Clauses
SET effective_date = '2014-05-01' -- '2014-01-01'
WHERE clause_id = 92463; -- 52.203-10

UPDATE Clauses
SET effective_date = '2007-09-01' -- '2007-01-01'
WHERE clause_id = 92464; -- 52.203-11

UPDATE Clauses
SET effective_date = '2010-10-01' -- '2010-01-01'
WHERE clause_id = 92465; -- 52.203-12

UPDATE Clauses
SET effective_date = '2010-04-01' -- '2010-01-01'
WHERE clause_id = 92466; -- 52.203-13

UPDATE Clauses
SET effective_date = '2007-12-01' -- '2007-01-01'
WHERE clause_id = 92467; -- 52.203-14

UPDATE Clauses
SET effective_date = '2010-06-01' -- '2010-01-01'
WHERE clause_id = 92468; -- 52.203-15

UPDATE Clauses
SET effective_date = '2011-12-01' -- '2011-01-01'
WHERE clause_id = 92469; -- 52.203-16

UPDATE Clauses
SET effective_date = '2014-04-01' -- '2014-01-01'
WHERE clause_id = 92470; -- 52.203-17

UPDATE Clauses
SET effective_date = '1985-04-01' -- '1985-01-01'
WHERE clause_id = 92471; -- 52.203-2

UPDATE Clauses
SET effective_date = '1984-04-01' -- '1984-01-01'
WHERE clause_id = 92472; -- 52.203-3

UPDATE Clauses
SET effective_date = '2014-05-01' -- '2014-01-01'
WHERE clause_id = 92473; -- 52.203-5

UPDATE Clauses
SET effective_date = '2014-05-01' -- '2014-01-01'
WHERE clause_id = 92475; -- 52.203-6

UPDATE Clauses
SET effective_date = '2014-05-01' -- '2014-01-01'
WHERE clause_id = 92476; -- 52.203-7

UPDATE Clauses
SET effective_date = '2014-05-01' -- '2014-01-01'
WHERE clause_id = 92477; -- 52.203-8

UPDATE Clauses
SET effective_date = '2013-07-01' -- '2013-01-01'
WHERE clause_id = 92478; -- 52.204-10

UPDATE Clauses
SET effective_date = '2012-12-01' -- '2012-01-01'
WHERE clause_id = 92479; -- 52.204-12

UPDATE Clauses
SET effective_date = '2013-07-01' -- '2013-01-01'
WHERE clause_id = 92480; -- 52.204-13

UPDATE Clauses
SET effective_date = '2014-11-01' -- '2014-01-01'
WHERE clause_id = 92483; -- 52.204-16

UPDATE Clauses
SET effective_date = '2014-11-01' -- '2014-01-01'
WHERE clause_id = 92484; -- 52.204-17

UPDATE Clauses
SET effective_date = '2014-11-01' -- '2014-01-01'
WHERE clause_id = 92485; -- 52.204-18

UPDATE Clauses
SET effective_date = '1996-08-01' -- '1996-01-01'
WHERE clause_id = 92489; -- 52.204-2

UPDATE Clauses
SET effective_date = '1998-10-01' -- '1998-01-01'
WHERE clause_id = 92490; -- 52.204-3

UPDATE Clauses
SET effective_date = '2011-05-01' -- '2011-01-01'
WHERE clause_id = 92491; -- 52.204-4

UPDATE Clauses
SET effective_date = '2014-10-01' -- '2014-01-01'
WHERE clause_id = 92492; -- 52.204-5

UPDATE Clauses
SET effective_date = '2013-07-01' -- '2013-01-01'
WHERE clause_id = 92493; -- 52.204-6

UPDATE Clauses
SET effective_date = '2013-07-01' -- '2013-01-01'
WHERE clause_id = 92495; -- 52.204-7

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1204_68&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?node=se48.2.52_1204_68&amp;rgn=div8'
, clause_data = '<p>(a)(1) The North American Industry Classification System (NAICS) code for this acquisition is {{textbox_52.204-8[0]}} [<i>insert NAICS code</i>].</p><p>(2) The small business size standard is {{textbox_52.204-8[1]}} [<i>insert size standard</i>].</p><p>(3) The small business size standard for a concern which submits an offer in its own name, other than on a construction or service contract, but which proposes to furnish a product which it did not itself manufacture, is 500 employees.3</p><p>(b)(1) If the provision at 52.204-7, System for Award Management, is included in this solicitation, paragraph (d) of this provision applies.</p><p>(2) If the provision at 52.204-7 is not included in this solicitation, and the offeror is currently registered in the System for Award Management (SAM), and has completed the Representations and Certifications section of SAM electronically, the offeror may choose to use paragraph (d) of this provision instead of completing the corresponding individual representations and certifications in the solicitation. The offeror shall indicate which option applies by checking one of the following boxes:</p><p>[{{checkbox_52.204-8[0]}}] (i) Paragraph (d) applies.</p><p>[{{checkbox_52.204-8[1]}}] (ii) Paragraph (d) does not apply and the offeror has completed the individual representations and certifications in the solicitation.</p><p>(c)(1) The following representations or certifications in SAM are applicable to this solicitation as indicated:</p><p>(i) 52.203-2, Certificate of Independent Price Determination. This provision applies to solicitations when a firm-fixed-price contract or fixed-price contract with economic price adjustment is contemplated, unless-</p><p>(A) The acquisition is to be made under the simplified acquisition procedures in Part 13;</p><p>(B) The solicitation is a request for technical proposals under two-step sealed bidding procedures; or</p><p>(C) The solicitation is for utility services for which rates are set by law or regulation.</p><p>(ii) 52.203-11, Certification and Disclosure Regarding Payments to Influence Certain Federal Transactions. This provision applies to solicitations expected to exceed $150,000.</p><p>(iii) 52.204-3, Taxpayer Identification. This provision applies to solicitations that do not include provision at 52.204-7, System for Award Management.</p><p>(iv) 52.204-5, Women-Owned Business (Other Than Small Business). This provision applies to solicitations that-</p><p>(A) Are not set aside for small business concerns;</p><p>(B) Exceed the simplified acquisition threshold; and</p><p>(C) Are for contracts that will be performed in the United States or its outlying areas.</p><p>(v) 52.209-2, Prohibition on Contracting with Inverted Domestic Corporations-Representation.</p><p>(vi) 52.209-5, Certification Regarding Responsibility Matters. This provision applies to solicitations where the contract value is expected to exceed the simplified acquisition threshold.</p><p>(vii) 52.214-14, Place of Performance-Sealed Bidding. This provision applies to invitations for bids except those in which the place of performance is specified by the Government.</p><p>(viii) 52.215-6, Place of Performance. This provision applies to solicitations unless the place of performance is specified by the Government.</p><p>(ix) 52.219-1, Small Business Program Representations (Basic & Alternate I). This provision applies to solicitations when the contract will be performed in the United States or its outlying areas.</p><p>(A) The basic provision applies when the solicitations are issued by other than DoD, NASA, and the Coast Guard.</p><p>(B) The provision with its Alternate I applies to solicitations issued by DoD, NASA, or the Coast Guard.</p><p>(x) 52.219-2, Equal Low Bids. This provision applies to solicitations when contracting by sealed bidding and the contract will be performed in the United States or its outlying areas.</p><p>(xi) 52.222-22, Previous Contracts and Compliance Reports. This provision applies to solicitations that include the clause at 52.222-26, Equal Opportunity.</p><p>(xii) 52.222-25, Affirmative Action Compliance. This provision applies to solicitations, other than those for construction, when the solicitation includes the clause at 52.222-26, Equal Opportunity.</p><p>(xiii) 52.222-38, Compliance with Veterans'' Employment Reporting Requirements. This provision applies to solicitations when it is anticipated the contract award will exceed the simplified acquisition threshold and the contract is not for acquisition of commercial items.</p><p>(xiv) 52.223-1, Biobased Product Certification. This provision applies to solicitations that require the delivery or specify the use of USDA-designated items; or include the clause at 52.223-2, Affirmative Procurement of Biobased Products Under Service and Construction Contracts.</p><p>(xv) 52.223-4, Recovered Material Certification. This provision applies to solicitations that are for, or specify the use of, EPA-designated items.</p><p>(xvi) 52.225-2, Buy American Certificate. This provision applies to solicitations containing the clause at 52.225-1.</p><p>(xvii) 52.225-4, Buy American-Free Trade Agreements-Israeli Trade Act Certificate. (Basic, Alternates I, II, and III.) This provision applies to solicitations containing the clause at 52.225-3.</p><p>(A) If the acquisition value is less than $25,000, the basic provision applies.</p><p>(B) If the acquisition value is $25,000 or more but is less than $50,000, the provision with its Alternate I applies.</p><p>(C) If the acquisition value is $50,000 or more but is less than $79,507, the provision with its Alternate II applies.</p><p>(D) If the acquisition value is $79,507 or more but is less than $100,000, the provision with its Alternate III applies.</p><p>(xviii) 52.225-6, Trade Agreements Certificate. This provision applies to solicitations containing the clause at 52.225-5.</p><p>(xix) 52.225-20, Prohibition on Conducting Restricted Business Operations in Sudan-Certification. This provision applies to all solicitations.</p><p>(xx) 52.225-25, Prohibition on Contracting with Entities Engaging in Certain Activities or Transactions Relating to Iran-Representation and Certifications. This provision applies to all solicitations.</p><p>(xxi) 52.226-2, Historically Black College or University and Minority Institution Representation. This provision applies to solicitations for research, studies, supplies, or services of the type normally acquired from higher educational institutions.</p><p>(2) The following certifications are applicable as indicated by the Contracting Officer:</p><p>[<i>Contracting Officer check as appropriate.</i>]</p><p>{{textbox_52.204-8[2]}} (i) 52.204-17, Ownership or Control of Offeror.</p><p>{{textbox_52.204-8[3]}} (ii) 52.222-18, Certification Regarding Knowledge of Child Labor for Listed End Products.</p><p>{{textbox_52.204-8[4]}} (iii) 52.222-48, Exemption from Application of the Service Contract Labor Standards to Contracts for Maintenance, Calibration, or Repair of Certain Equipment-Certification.</p><p>{{textbox_52.204-8[5]}} (iv) 52.222-52, Exemption from Application of the Service Contract Labor Standards to Contracts for Certain Services-Certification.</p><p>{{textbox_52.204-8[6]}} (v) 52.223-9, with its Alternate I, Estimate of Percentage of Recovered Material Content for EPA-Designated Products (Alternate I only).</p><p>{{textbox_52.204-8[7]}} (vi) 52.227-6, Royalty Information.</p><p>{{textbox_52.204-8[8]}} (A) Basic.</p><p>{{textbox_52.204-8[9]}} (B) Alternate I.</p><p>{{textbox_52.204-8[10]}} (vii) 52.227-15, Representation of Limited Rights Data and Restricted Computer Software.</p><p>(d) The offeror has completed the annual representations and certifications electronically via the SAM Web site accessed through <i>https://www.acquisition.gov.</i> After reviewing the SAM database information, the offeror verifies by submission of the offer that the representations and certifications currently posted electronically that apply to this solicitation as indicated in paragraph (c) of this provision have been entered or updated within the last 12 months, are current, accurate, complete, and applicable to this solicitation (including the business size standard applicable to the NAICS code referenced for this solicitation), as of the date of this offer and are incorporated in this offer by reference (see FAR 4.1201); except for the changes identified below [<i>offeror to insert changes, identifying change by clause number, title, date</i>]. These amended representation(s) and/or certification(s) are also incorporated in this offer and are current, accurate, and complete as of the date of this offer.</p><div><div><table border="1" cellpadding="1" cellspacing="1" frame="void" width="100%"><tbody><tr><th scope="col">FAR Clause No.</th><th scope="col">Title</th><th scope="col">Date</th><th scope="col">Change</th></tr><tr><td>{{textbox_52.204-8[11]}}</td><td>{{textbox_52.204-8[12]}}</td><td>{{textbox_52.204-8[13]}}</td><td>{{textbox_52.204-8[14]}}</td></tr></tbody></table></div></div><p>Any changes provided by the offeror are applicable to this solicitation only, and do not result in an update to the representations and certifications posted on SAM.</p>'
--               '<p>(a)(1) The North American Industry Classification System (NAICS) code for this acquisition is {{textbox_52.204-8[0]}} [<i>insert NAICS code</i>].</p><p>(2) The small business size standard is {{textbox_52.204-8[1]}} [<i>insert size standard</i>].</p><p>(3) The small business size standard for a concern which submits an offer in its own name, other than on a construction or service contract, but which proposes to furnish a product which it did not itself manufacture, is 500 employees.3</p><p>(b)(1) If the provision at 52.204-7, System for Award Management, is included in this solicitation, paragraph (d) of this provision applies.</p><p>(2) If the provision at 52.204-7 is not included in this solicitation, and the offeror is currently registered in the System for Award Management (SAM), and has completed the Representations and Certifications section of SAM electronically, the offeror may choose to use paragraph (d) of this provision instead of completing the corresponding individual representations and certifications in the solicitation. The offeror shall indicate which option applies by checking one of the following boxes:</p><p>{{checkbox_52.204-8[0]}}} (i) Paragraph (d) applies.</p><p>{{checkbox_52.204-8[1]}}} (ii) Paragraph (d) does not apply and the offeror has completed the individual representations and certifications in the solicitation.</p><p>(c)(1) The following representations or certifications in SAM are applicable to this solicitation as indicated:</p><p>(i) 52.203-2, Certificate of Independent Price Determination. This provision applies to solicitations when a firm-fixed-price contract or fixed-price contract with economic price adjustment is contemplated, unlessâ</p><p>(A) The acquisition is to be made under the simplified acquisition procedures in Part 13;</p><p>(B) The solicitation is a request for technical proposals under two-step sealed bidding procedures; or</p><p>(C) The solicitation is for utility services for which rates are set by law or regulation.</p><p>(ii) 52.203-11, Certification and Disclosure Regarding Payments to Influence Certain Federal Transactions. This provision applies to solicitations expected to exceed $150,000.</p><p>(iii) 52.204-3, Taxpayer Identification. This provision applies to solicitations that do not include provision at 52.204-7, System for Award Management.</p><p>(iv) 52.204-5, Women-Owned Business (Other Than Small Business). This provision applies to solicitations thatâ</p><p>(A) Are not set aside for small business concerns;</p><p>(B) Exceed the simplified acquisition threshold; and</p><p>(C) Are for contracts that will be performed in the United States or its outlying areas.</p><p>(v) 52.209-2, Prohibition on Contracting with Inverted Domestic CorporationsâRepresentation.</p><p>(vi) 52.209-5, Certification Regarding Responsibility Matters. This provision applies to solicitations where the contract value is expected to exceed the simplified acquisition threshold.</p><p>(vii) 52.214-14, Place of PerformanceâSealed Bidding. This provision applies to invitations for bids except those in which the place of performance is specified by the Government.</p><p>(viii) 52.215-6, Place of Performance. This provision applies to solicitations unless the place of performance is specified by the Government.</p><p>(ix) 52.219-1, Small Business Program Representations (Basic & Alternate I). This provision applies to solicitations when the contract will be performed in the United States or its outlying areas.</p><p>(A) The basic provision applies when the solicitations are issued by other than DoD, NASA, and the Coast Guard.</p><p>(B) The provision with its Alternate I applies to solicitations issued by DoD, NASA, or the Coast Guard.</p><p>(x) 52.219-2, Equal Low Bids. This provision applies to solicitations when contracting by sealed bidding and the contract will be performed in the United States or its outlying areas.</p><p>(xi) 52.222-22, Previous Contracts and Compliance Reports. This provision applies to solicitations that include the clause at 52.222-26, Equal Opportunity.</p><p>(xii) 52.222-25, Affirmative Action Compliance. This provision applies to solicitations, other than those for construction, when the solicitation includes the clause at 52.222-26, Equal Opportunity.</p><p>(xiii) 52.222-38, Compliance with Veterans'' Employment Reporting Requirements. This provision applies to solicitations when it is anticipated the contract award will exceed the simplified acquisition threshold and the contract is not for acquisition of commercial items.</p><p>(xiv) 52.223-1, Biobased Product Certification. This provision applies to solicitations that require the delivery or specify the use of USDA-designated items; or include the clause at 52.223-2, Affirmative Procurement of Biobased Products Under Service and Construction Contracts.</p><p>(xv) 52.223-4, Recovered Material Certification. This provision applies to solicitations that are for, or specify the use of, EPA-designated items.</p><p>(xvi) 52.225-2, Buy American Certificate. This provision applies to solicitations containing the clause at 52.225-1.</p><p>(xvii) 52.225-4, Buy AmericanâFree Trade AgreementsâIsraeli Trade Act Certificate. (Basic, Alternates I, II, and III.) This provision applies to solicitations containing the clause at 52.225-3.</p><p>(A) If the acquisition value is less than $25,000, the basic provision applies.</p><p>(B) If the acquisition value is $25,000 or more but is less than $50,000, the provision with its Alternate I applies.</p><p>(C) If the acquisition value is $50,000 or more but is less than $79,507, the provision with its Alternate II applies.</p><p>(D) If the acquisition value is $79,507 or more but is less than $100,000, the provision with its Alternate III applies.</p><p>(xviii) 52.225-6, Trade Agreements Certificate. This provision applies to solicitations containing the clause at 52.225-5.</p><p>(xix) 52.225-20, Prohibition on Conducting Restricted Business Operations in SudanâCertification. This provision applies to all solicitations.</p><p>(xx) 52.225-25, Prohibition on Contracting with Entities Engaging in Certain Activities or Transactions Relating to IranâRepresentation and Certifications. This provision applies to all solicitations.</p><p>(xxi) 52.226-2, Historically Black College or University and Minority Institution Representation. This provision applies to solicitations for research, studies, supplies, or services of the type normally acquired from higher educational institutions.</p><p>(2) The following certifications are applicable as indicated by the Contracting Officer:</p>\n<p>[<i>Contracting Officer check as appropriate.</i>]</p><p>{{textbox_52.204-8[2]}} (i) 52.204-17, Ownership or Control of Offeror.</p><p>{{textbox_52.204-8[3]}} (ii) 52.222-18, Certification Regarding Knowledge of Child Labor for Listed End Products.</p><p>{{textbox_52.204-8[4]}} (iii) 52.222-48, Exemption from Application of the Service Contract Labor Standards to Contracts for Maintenance, Calibration, or Repair of Certain EquipmentâCertification.</p><p>{{textbox_52.204-8[5]}} (iv) 52.222-52, Exemption from Application of the Service Contract Labor Standards to Contracts for Certain ServicesâCertification.</p><p>{{textbox_52.204-8[6]}} (v) 52.223-9, with its Alternate I, Estimate of Percentage of Recovered Material Content for EPA-Designated Products (Alternate I only).</p><p>{{textbox_52.204-8[7]}} (vi) 52.227-6, Royalty Information.</p><p>{{textbox_52.204-8[8]}} (A) Basic.</p><p>{{textbox_52.204-8[9]}} (B) Alternate I.</p><p>{{textbox_52.204-8[10]}} (vii) 52.227-15, Representation of Limited Rights Data and Restricted Computer Software.</p><p>(d) The offeror has completed the annual representations and certifications electronically via the SAM Web site accessed through <i>https://www.acquisition.gov.</i> After reviewing the SAM database information, the offeror verifies by submission of the offer that the representations and certifications currently posted electronically that apply to this solicitation as indicated in paragraph (c) of this provision have been entered or updated within the last 12 months, are current, accurate, complete, and applicable to this solicitation (including the business size standard applicable to the NAICS code referenced for this solicitation), as of the date of this offer and are incorporated in this offer by reference (see FAR 4.1201); except for the changes identified below [<i>offeror to insert changes, identifying change by clause number, title, date</i>]. These amended representation(s) and/or certification(s) are also incorporated in this offer and are current, accurate, and complete as of the date of this offer.</p>\n<div><div><table border="&quot;1&quot;" cellpadding="&quot;1&quot;" cellspacing="&quot;1&quot;" frame="&quot;void&quot;" width="&quot;100%&quot;"><tbody><tr><th scope="&quot;col&quot;">FAR Clause No.</th><th scope="&quot;col&quot;">Title</th><th scope="&quot;col&quot;">Date</th><th scope="&quot;col&quot;">Change</th></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">&nbsp;&nbsp;&nbsp;</td><td align="&quot;left&quot;"></td><td align="&quot;left&quot;"></td><td align="&quot;left&quot;"></td></tr></tbody></table></div></div>\n<p>Any changes provided by the offeror are applicable to this solicitation only, and do not result in an update to the representations and certifications posted on SAM.</p>'
WHERE clause_id = 92496; -- 52.204-8
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (92496, 'textbox_52.204-8[11]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (92496, 'textbox_52.204-8[12]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (92496, 'textbox_52.204-8[13]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (92496, 'textbox_52.204-8[14]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (92496, 'checkbox_52.204-8[0]', 'C', null);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (92496, 'checkbox_52.204-8[1]', 'C', null);

UPDATE Clauses
SET effective_date = '2006-05-01' -- '2006-01-01'
WHERE clause_id = 92498; -- 52.207-1

UPDATE Clauses
SET effective_date = '2006-05-01' -- '2006-01-01'
WHERE clause_id = 92499; -- 52.207-2

UPDATE Clauses
SET effective_date = '2006-05-01' -- '2006-01-01'
WHERE clause_id = 92500; -- 52.207-3

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1207_64&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?node=se48.2.52_1207_64&amp;rgn=div8'
, effective_date = '1987-08-01' -- '1987-01-01'
, clause_data = '<p>(a) Offerors are invited to state an opinion on whether the quantity(ies) of supplies on which bids, proposals or quotes are requested in this solicitation is (are) economically advantageous to the Government.</p><div>&nbsp;</div><div>&nbsp;</div><div>&nbsp;</div><p>(b) Each offeror who believes that acquisitions in different quantities would be more advantageous is invited to recommend an economic purchase quantity. If different quantities are recommended, a total and a unit price must be quoted for applicable items. An economic purchase quantity is that quantity at which a significant price break occurs. If there are significant price breaks at different quantity points, this information is desired as well.</p><div><div><p>Offeror Recommendations</p></div><div><table border="1" cellpadding="1" cellspacing="1" frame="void" width="100%"><tbody><tr><th scope="col">Item</th><th scope="col">Quantity</th><th scope="col">Price quotation</th><th scope="col">Total</th></tr><tr><td align="left" scope="row"></td><td align="right"></td><td align="right"></td><td align="right"></td></tr><tr><td align="left" scope="row"></td><td align="right"></td><td align="right"></td><td align="right"></td></tr><tr><td align="left" scope="row"></td><td align="right"></td><td align="right"></td><td align="right"></td></tr></tbody></table></div></div><p>(c) The information requested in this provision is being solicited to avoid acquisitions in disadvantageous quantities and to assist the Government in developing a data base for future acquisitions of these items. However, the Government reserves the right to amend or cancel the solicitation and resolicit with respect to any individual item in the event quotations received and the Government''s requirements indicate that different quantities should be acquired.</p>'
--               '<p>(a) Offerors are invited to state an opinion on whether the quantity(ies) of supplies on which bids, proposals or quotes are requested in this solicitation is (are) economically advantageous to the Government.</p>&nbsp; {{textbox_52.207-4[0]}}&nbsp; {{textbox_52.207-4[1]}}&nbsp; {{textbox_52.207-4[2]}}<p>(b) Each offeror who believes that acquisitions in different quantities would be more advantageous is invited to recommend an economic purchase quantity. If different quantities are recommended, a total and a unit price must be quoted for applicable items. An economic purchase quantity is that quantity at which a significant price break occurs. If there are significant price breaks at different quantity points, this information is desired as well.</p>\n<div><div><p>Offeror Recommendations</p></div><div><table border="&quot;1&quot;" cellpadding="&quot;1&quot;" cellspacing="&quot;1&quot;" frame="&quot;void&quot;" width="&quot;100%&quot;"><tbody><tr><th scope="&quot;col&quot;">Item</th><th scope="&quot;col&quot;">Quantity</th><th scope="&quot;col&quot;">Price quotation</th><th scope="&quot;col&quot;">Total</th></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;"></td><td align="&quot;right&quot;"></td><td align="&quot;right&quot;"></td><td align="&quot;right&quot;"></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;"></td><td align="&quot;right&quot;"></td><td align="&quot;right&quot;"></td><td align="&quot;right&quot;"></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;"></td><td align="&quot;right&quot;"></td><td align="&quot;right&quot;"></td><td align="&quot;right&quot;"></td></tr></tbody></table></div></div><p>(c) The information requested in this provision is being solicited to avoid acquisitions in disadvantageous quantities and to assist the Government in developing a data base for future acquisitions of these items. However, the Government reserves the right to amend or cancel the solicitation and resolicit with respect to any individual item in the event quotations received and the Government''s requirements indicate that different quantities should be acquired.</p>'
WHERE clause_id = 92501; -- 52.207-4

UPDATE Clauses
SET effective_date = '1995-02-01' -- '1995-01-01'
WHERE clause_id = 92502; -- 52.207-5

UPDATE Clauses
SET effective_date = '1984-04-01' -- '1984-01-01'
WHERE clause_id = 92503; -- 52.208-4

UPDATE Clauses
SET effective_date = '1984-04-01' -- '1984-01-01'
WHERE clause_id = 92504; -- 52.208-5

UPDATE Clauses
SET effective_date = '1984-04-01' -- '1984-01-01'
WHERE clause_id = 92505; -- 52.208-6

UPDATE Clauses
SET effective_date = '1986-05-01' -- '1986-01-01'
WHERE clause_id = 92506; -- 52.208-7

UPDATE Clauses
SET effective_date = '2014-04-01' -- '2014-01-01'
WHERE clause_id = 92507; -- 52.208-8

UPDATE Clauses
SET effective_date = '2014-05-01' -- '2014-01-01'
WHERE clause_id = 92508; -- 52.208-9

UPDATE Clauses
SET effective_date = '1995-02-01' -- '1995-01-01'
WHERE clause_id = 92509; -- 52.209-1

UPDATE Clauses
SET effective_date = '1989-09-01' -- '1989-01-01'
WHERE clause_id = 92514; -- 52.209-3

UPDATE Clauses
SET effective_date = '1989-09-01' -- '1989-01-01'
WHERE clause_id = 92517; -- 52.209-4

UPDATE Clauses
SET effective_date = '2010-04-01' -- '2010-01-01'
WHERE clause_id = 92518; -- 52.209-5

UPDATE Clauses
SET effective_date = '2013-08-01' -- '2013-01-01'
WHERE clause_id = 92519; -- 52.209-6

UPDATE Clauses
SET effective_date = '2013-07-01' -- '2013-01-01'
WHERE clause_id = 92520; -- 52.209-7

UPDATE Clauses
SET effective_date = '2013-07-01' -- '2013-01-01'
WHERE clause_id = 92521; -- 52.209-9

UPDATE Clauses
SET effective_date = '2011-04-01' -- '2011-01-01'
WHERE clause_id = 92522; -- 52.210-1

UPDATE Clauses
SET effective_date = '1998-08-01' -- '1998-01-01'
WHERE clause_id = 92523; -- 52.211-1

UPDATE Clauses
SET effective_date = '1984-04-01' -- NULL
WHERE clause_id = 92525; -- 52.211-10

UPDATE Clauses
SET effective_date = '2000-09-01' -- '2000-01-01'
WHERE clause_id = 92526; -- 52.211-11

UPDATE Clauses
SET effective_date = '2000-09-01' -- '2000-01-01'
WHERE clause_id = 92527; -- 52.211-12

UPDATE Clauses
SET effective_date = '2000-09-01' -- '2000-01-01'
WHERE clause_id = 92528; -- 52.211-13

UPDATE Clauses
SET effective_date = '2008-04-01' -- '2008-01-01'
WHERE clause_id = 92529; -- 52.211-14

UPDATE Clauses
SET effective_date = '2008-04-01' -- '2008-01-01'
WHERE clause_id = 92530; -- 52.211-15

UPDATE Clauses
SET effective_date = '1984-04-01' -- NULL
WHERE clause_id = 92531; -- 52.211-16

UPDATE Clauses
SET effective_date = '1989-09-01' -- '1989-01-01'
WHERE clause_id = 92532; -- 52.211-17

UPDATE Clauses
SET effective_date = '1984-04-01' -- '1984-01-01'
WHERE clause_id = 92533; -- 52.211-18

UPDATE Clauses
SET effective_date = '2014-04-01' -- '2014-01-01'
WHERE clause_id = 92534; -- 52.211-2

UPDATE Clauses
SET effective_date = '1988-06-01' -- '1988-01-01'
WHERE clause_id = 92535; -- 52.211-3

UPDATE Clauses
SET effective_date = '1988-06-01' -- '1988-01-01'
WHERE clause_id = 92536; -- 52.211-4

UPDATE Clauses
SET effective_date = '2000-08-01' -- '2000-01-01'
WHERE clause_id = 92537; -- 52.211-5

UPDATE Clauses
SET effective_date = '1999-08-01' -- '1999-01-01'
WHERE clause_id = 92538; -- 52.211-6

UPDATE Clauses
SET effective_date = '1999-11-01' -- '1999-01-01'
WHERE clause_id = 92539; -- 52.211-7

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1211_68&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?node=se48.2.52_1211_68&amp;rgn=div8'
, effective_date = '1997-06-01' -- '1997-01-01'
, clause_data = '<p>(a) The Government requires delivery to be made according to the following schedule:</p><div><div><p>REQUIRED DELIVERY SCHEDULE</p><p>[Contracting Officer insert specific details]</p></div><div><table border="1" cellpadding="1" cellspacing="1" frame="void" width="100%"><tbody><tr><th scope="col">ITEM NO.</th><th scope="col">QUANTITY</th><th scope="col">WITHIN DAYS AFTER DATE OF CONTRACT</th></tr><tr><td align="left" scope="row">{{textbox_52.211-8[0]}}</td><td align="left">{{textbox_52.211-8[1]}}</td><td align="left">{{textbox_52.211-8[2]}}</td></tr><tr><td align="left" scope="row">{{textbox_52.211-8[3]}}</td><td align="left">{{textbox_52.211-8[4]}}</td><td align="left">{{textbox_52.211-8[5]}}</td></tr><tr><td align="left" scope="row">{{textbox_52.211-8[6]}}</td><td align="left">{{textbox_52.211-8[7]}}</td><td align="left">{{textbox_52.211-8[8]}}</td></tr></tbody></table></div></div><p>The Government will evaluate equally, as regards time of delivery, offers that propose delivery of each quantity within the applicable delivery period specified above. Offers that propose delivery that will not clearly fall within the applicable required delivery period specified above, will be considered nonresponsive and rejected. The Government reserves the right to award under either the required delivery schedule or the proposed delivery schedule, when an offeror offers an earlier delivery schedule than required above. If the offeror proposes no other delivery schedule, the required delivery schedule above will apply.</p><div><div><p>OFFEROR''S PROPOSED DELIVERY SCHEDULE</p></div><div><table border="1" cellpadding="1" cellspacing="1" frame="void" width="100%"><tbody><tr><th scope="col">ITEM NO.</th><th scope="col">QUANTITY</th><th scope="col">WITHIN DAYS AFTER DATE OF CONTRACT</th></tr><tr><td align="left" scope="row">{{textbox_52.211-8[9]}}</td><td align="left">{{textbox_52.211-8[10]}}</td><td align="left">{{textbox_52.211-8[11]}}</td></tr><tr><td align="left" scope="row">{{textbox_52.211-8[12]}}</td><td align="left">{{textbox_52.211-8[13]}}</td><td align="left">{{textbox_52.211-8[14]}}</td></tr><tr><td align="left" scope="row">{{textbox_52.211-8[15]}}</td><td align="left">{{textbox_52.211-8[16]}}</td><td align="left">{{textbox_52.211-8[17]}}</td></tr></tbody></table></div></div><p>(b) Attention is directed to the Contract Award provision of the solicitation that provides that a written award or acceptance of offer mailed, or otherwise furnished to the successful offeror, results in a binding contract. The Government will mail or otherwise furnish to the offeror an award or notice of award not later than the day award is dated. Therefore, the offeror should compute the time available for performance beginning with the actual date of award, rather than the date the written notice of award is received from the Contracting Officer through the ordinary mails. However, the Government will evaluate an offer that proposes delivery based on the Contractor''s date of receipt of the contract or notice of award by adding (1) five calendar days for delivery of the award through the ordinary mails, or (2) one working day if the solicitation states that the contract or notice of award will be transmitted electronically. (The term <i>working day</i> excludes weekends and U.S. Federal holidays.) If, as so computed, the offered delivery date is later than the required delivery date, the offer will be considered nonresponsive and rejected.</p>'
--               '<p>(a) The Government requires delivery to be made according to the following schedule:</p>\n<div><div><p>REQUIRED DELIVERY SCHEDULE</p><p>[Contracting Officer insert specific details]</p></div><div><table border="&quot;1&quot;" cellpadding="&quot;1&quot;" cellspacing="&quot;1&quot;" frame="&quot;void&quot;" width="&quot;100%&quot;"><tbody><tr><th scope="&quot;col&quot;">ITEM NO.</th><th scope="&quot;col&quot;">QUANTITY</th><th scope="&quot;col&quot;">WITHIN DAYS AFTER DATE OF CONTRACT</th></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">{{textbox_52.211-8[0]}}</td><td align="&quot;left&quot;">{{textbox_52.211-8[1]}}</td><td align="&quot;left&quot;">{{textbox_52.211-8[2]}}</td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">{{textbox_52.211-8[3]}}</td><td align="&quot;left&quot;">{{textbox_52.211-8[4]}}</td><td align="&quot;left&quot;">{{textbox_52.211-8[5]}}</td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">{{textbox_52.211-8[6]}}</td><td align="&quot;left&quot;">{{textbox_52.211-8[7]}}</td><td align="&quot;left&quot;">{{textbox_52.211-8[8]}}</td></tr></tbody></table></div></div><p>The Government will evaluate equally, as regards time of delivery, offers that propose delivery of each quantity within the applicable delivery period specified above. Offers that propose delivery that will not clearly fall within the applicable required delivery period specified above, will be considered nonresponsive and rejected. The Government reserves the right to award under either the required delivery schedule or the proposed delivery schedule, when an offeror offers an earlier delivery schedule than required above. If the offeror proposes no other delivery schedule, the required delivery schedule above will apply.</p>\n<div><div><p>OFFEROR''S PROPOSED DELIVERY SCHEDULE</p></div><div><table border="&quot;1&quot;" cellpadding="&quot;1&quot;" cellspacing="&quot;1&quot;" frame="&quot;void&quot;" width="&quot;100%&quot;"><tbody><tr><th scope="&quot;col&quot;">ITEM NO.</th><th scope="&quot;col&quot;">QUANTITY</th><th scope="&quot;col&quot;">WITHIN DAYS AFTER DATE OF CONTRACT</th></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">{{textbox_52.211-8[9]}}</td><td align="&quot;left&quot;">{{textbox_52.211-8[10]}}</td><td align="&quot;left&quot;">{{textbox_52.211-8[11]}}</td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">{{textbox_52.211-8[12]}}</td><td align="&quot;left&quot;">{{textbox_52.211-8[13]}}</td><td align="&quot;left&quot;">{{textbox_52.211-8[14]}}</td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">{{textbox_52.211-8[15]}}</td><td align="&quot;left&quot;">{{textbox_52.211-8[16]}}</td><td align="&quot;left&quot;">{{textbox_52.211-8[17]}}</td></tr></tbody></table></div></div><p>(b) Attention is directed to the Contract Award provision of the solicitation that provides that a written award or acceptance of offer mailed, or otherwise furnished to the successful offeror, results in a binding contract. The Government will mail or otherwise furnish to the offeror an award or notice of award not later than the day award is dated. Therefore, the offeror should compute the time available for performance beginning with the actual date of award, rather than the date the written notice of award is received from the Contracting Officer through the ordinary mails. However, the Government will evaluate an offer that proposes delivery based on the Contractor''s date of receipt of the contract or notice of award by adding (1) five calendar days for delivery of the award through the ordinary mails, or (2) one working day if the solicitation states that the contract or notice of award will be transmitted electronically. (The term <i>working day</i> excludes weekends and U.S. Federal holidays.) If, as so computed, the offered delivery date is later than the required delivery date, the offer will be considered nonresponsive and rejected.</p>'
WHERE clause_id = 92543; -- 52.211-8
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (92543, 'textbox_52.211-8[0]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (92543, 'textbox_52.211-8[1]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (92543, 'textbox_52.211-8[2]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (92543, 'textbox_52.211-8[3]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (92543, 'textbox_52.211-8[4]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (92543, 'textbox_52.211-8[5]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (92543, 'textbox_52.211-8[6]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (92543, 'textbox_52.211-8[7]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (92543, 'textbox_52.211-8[8]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (92543, 'textbox_52.211-8[9]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (92543, 'textbox_52.211-8[10]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (92543, 'textbox_52.211-8[11]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (92543, 'textbox_52.211-8[12]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (92543, 'textbox_52.211-8[13]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (92543, 'textbox_52.211-8[14]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (92543, 'textbox_52.211-8[15]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (92543, 'textbox_52.211-8[16]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (92543, 'textbox_52.211-8[17]', 'S', 100);

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1211_69&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?node=se48.2.52_1211_69&amp;rgn=div8'
, effective_date = '1997-06-01' -- '1997-01-01'
, clause_data = '<p>(a) The Government desires delivery to be made according to the following schedule:</p><div><div><p>DESIRED DELIVERY SCHEDULE</p><p>[Contracting Officer insert specific details]</p></div><div><table border="1" cellpadding="1" cellspacing="1" frame="void" width="100%"><tbody><tr><th scope="col">ITEM NO.</th><th scope="col">QUANTITY</th><th scope="col">WITHIN DAYS AFTER DATE OF CONTRACT</th></tr><tr><td align="left" scope="row">{{textbox_52.211-9[0]}}</td><td align="left">{{textbox_52.211-9[1]}}</td><td align="left">{{textbox_52.211-9[2]}}</td></tr><tr><td align="left" scope="row">{{textbox_52.211-9[3]}}</td><td align="left">{{textbox_52.211-9[4]}}</td><td align="left">{{textbox_52.211-9[5]}}</td></tr><tr><td align="left" scope="row">{{textbox_52.211-9[6]}}</td><td align="left">{{textbox_52.211-9[7]}}</td><td align="left">{{textbox_52.211-9[8]}}</td></tr></tbody></table></div></div><p>If the offeror is unable to meet the desired delivery schedule, it may, without prejudicing evaluation of its offer, propose a delivery schedule below. However, the offeror''s proposed delivery schedule must not extend the delivery period beyond the time for delivery in the Government''s required delivery schedule as follows:</p><div><div><p>REQUIRED DELIVERY SCHEDULE</p><p>[Contracting Officer insert specific details]</p></div><div><table border="1" cellpadding="1" cellspacing="1" frame="void" width="100%"><tbody><tr><th scope="col">ITEM NO.</th><th scope="col">QUANTITY</th><th scope="col">WITHIN DAYS AFTER DATE OF CONTRACT</th></tr><tr><td align="left" scope="row">{{textbox_52.211-9[9]}}</td><td align="left">{{textbox_52.211-9[10]}}</td><td align="left">{{textbox_52.211-9[11]}}</td></tr><tr><td align="left" scope="row">{{textbox_52.211-9[12]}}</td><td align="left">{{textbox_52.211-9[13]}}</td><td align="left">{{textbox_52.211-9[14]}}</td></tr><tr><td align="left" scope="row">{{textbox_52.211-9[15]}}</td><td align="left">{{textbox_52.211-9[16]}}</td><td align="left">{{textbox_52.211-9[17]}}</td></tr></tbody></table></div></div><p>Offers that propose delivery of a quantity under such terms or conditions that delivery will not clearly fall within the applicable required delivery period specified above, will be considered nonresponsive and rejected. If the offeror proposes no other delivery schedule, the desired delivery schedule above will apply.</p><div><div><p>OFFEROR''S PROPOSED DELIVERY SCHEDULE</p></div><div><table border="1" cellpadding="1" cellspacing="1" frame="void" width="100%"><tbody><tr><th scope="col">ITEM NO.</th><th scope="col">QUANTITY</th><th scope="col">WITHIN DAYS AFTER DATE OF CONTRACT</th></tr><tr><td align="left" scope="row">{{textbox_52.211-9[18]}}</td><td align="left">{{textbox_52.211-9[19]}}</td><td align="left">{{textbox_52.211-9[20]}}</td></tr><tr><td align="left" scope="row">{{textbox_52.211-9[21]}}</td><td align="left">{{textbox_52.211-9[22]}}</td><td align="left">{{textbox_52.211-9[23]}}</td></tr><tr><td align="left" scope="row">{{textbox_52.211-9[24]}}</td><td align="left">{{textbox_52.211-9[25]}}</td><td align="left">{{textbox_52.211-9[26]}}</td></tr></tbody></table></div></div><p>(b) Attention is directed to the Contract Award provision of the solicitation that provides that a written award or acceptance of offer mailed or otherwise furnished to the successful offeror results in a binding contract. The Government will mail or otherwise furnish to the offeror an award or notice of award not later than the day the award is dated. Therefore, the offeror shall compute the time available for performance beginning with the actual date of award, rather than the date the written notice of award is received from the Contracting Officer through the ordinary mails. However, the Government will evaluate an offer that proposes delivery based on the Contractor''s date of receipt of the contract or notice of award by adding (1) five calendar days for delivery of the award through the ordinary mails, or (2) one working day if the solicitation states that the contract or notice of award will be transmitted electronically. (The term <i>working day</i> excludes weekends and U.S. Federal holidays.) If, as so computed, the offered delivery date is later than the required delivery date, the offer will be considered nonresponsive and rejected.</p>'
--               '<p>(a) The Government desires delivery to be made according to the following schedule:</p>\n<div><div><p>DESIRED DELIVERY SCHEDULE</p><p>[Contracting Officer insert specific details]</p></div><div><table border="&quot;1&quot;" cellpadding="&quot;1&quot;" cellspacing="&quot;1&quot;" frame="&quot;void&quot;" width="&quot;100%&quot;"><tbody><tr><th scope="&quot;col&quot;">ITEM NO.</th><th scope="&quot;col&quot;">QUANTITY</th><th scope="&quot;col&quot;">WITHIN DAYS AFTER DATE OF CONTRACT</th></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">{{textbox_52.211-9[0]}}</td><td align="&quot;left&quot;">{{textbox_52.211-9[1]}}</td><td align="&quot;left&quot;">{{textbox_52.211-9[2]}}</td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">{{textbox_52.211-9[3]}}</td><td align="&quot;left&quot;">{{textbox_52.211-9[4]}}</td><td align="&quot;left&quot;">{{textbox_52.211-9[5]}}</td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">{{textbox_52.211-9[6]}}</td><td align="&quot;left&quot;">{{textbox_52.211-9[7]}}</td><td align="&quot;left&quot;">{{textbox_52.211-9[8]}}</td></tr></tbody></table></div></div><p>If the offeror is unable to meet the desired delivery schedule, it may, without prejudicing evaluation of its offer, propose a delivery schedule below. However, the offeror''s proposed delivery schedule must not extend the delivery period beyond the time for delivery in the Government''s required delivery schedule as follows:</p>\n<div><div><p>REQUIRED DELIVERY SCHEDULE</p><p>[Contracting Officer insert specific details]</p></div><div><table border="&quot;1&quot;" cellpadding="&quot;1&quot;" cellspacing="&quot;1&quot;" frame="&quot;void&quot;" width="&quot;100%&quot;"><tbody><tr><th scope="&quot;col&quot;">ITEM NO.</th><th scope="&quot;col&quot;">QUANTITY</th><th scope="&quot;col&quot;">WITHIN DAYS AFTER DATE OF CONTRACT</th></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">{{textbox_52.211-9[9]}}</td><td align="&quot;left&quot;">{{textbox_52.211-9[10]}}</td><td align="&quot;left&quot;">{{textbox_52.211-9[11]}}</td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">{{textbox_52.211-9[12]}}</td><td align="&quot;left&quot;">{{textbox_52.211-9[13]}}</td><td align="&quot;left&quot;">{{textbox_52.211-9[14]}}</td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">{{textbox_52.211-9[15]}}</td><td align="&quot;left&quot;">{{textbox_52.211-9[16]}}</td><td align="&quot;left&quot;">{{textbox_52.211-9[17]}}</td></tr></tbody></table></div></div><p>Offers that propose delivery of a quantity under such terms or conditions that delivery will not clearly fall within the applicable required delivery period specified above, will be considered nonresponsive and rejected. If the offeror proposes no other delivery schedule, the desired delivery schedule above will apply.</p>\n<div><div><p>OFFEROR''S PROPOSED DELIVERY SCHEDULE</p></div><div><table border="&quot;1&quot;" cellpadding="&quot;1&quot;" cellspacing="&quot;1&quot;" frame="&quot;void&quot;" width="&quot;100%&quot;"><tbody><tr><th scope="&quot;col&quot;">ITEM NO.</th><th scope="&quot;col&quot;">QUANTITY</th><th scope="&quot;col&quot;">WITHIN DAYS AFTER DATE OF CONTRACT</th></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">{{textbox_52.211-9[18]}}</td><td align="&quot;left&quot;">{{textbox_52.211-9[19]}}</td><td align="&quot;left&quot;">{{textbox_52.211-9[20]}}</td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">{{textbox_52.211-9[21]}}</td><td align="&quot;left&quot;">{{textbox_52.211-9[22]}}</td><td align="&quot;left&quot;">{{textbox_52.211-9[23]}}</td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">{{textbox_52.211-9[24]}}</td><td align="&quot;left&quot;">{{textbox_52.211-9[25]}}</td><td align="&quot;left&quot;">{{textbox_52.211-9[26]}}</td></tr></tbody></table></div></div><p>(b) Attention is directed to the Contract Award provision of the solicitation that provides that a written award or acceptance of offer mailed or otherwise furnished to the successful offeror results in a binding contract. The Government will mail or otherwise furnish to the offeror an award or notice of award not later than the day the award is dated. Therefore, the offeror shall compute the time available for performance beginning with the actual date of award, rather than the date the written notice of award is received from the Contracting Officer through the ordinary mails. However, the Government will evaluate an offer that proposes delivery based on the Contractor''s date of receipt of the contract or notice of award by adding (1) five calendar days for delivery of the award through the ordinary mails, or (2) one working day if the solicitation states that the contract or notice of award will be transmitted electronically. (The term <i>working day</i> excludes weekends and U.S. Federal holidays.) If, as so computed, the offered delivery date is later than the required delivery date, the offer will be considered nonresponsive and rejected.</p>'
WHERE clause_id = 92547; -- 52.211-9
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (92547, 'textbox_52.211-9[0]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (92547, 'textbox_52.211-9[1]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (92547, 'textbox_52.211-9[2]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (92547, 'textbox_52.211-9[3]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (92547, 'textbox_52.211-9[4]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (92547, 'textbox_52.211-9[5]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (92547, 'textbox_52.211-9[6]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (92547, 'textbox_52.211-9[7]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (92547, 'textbox_52.211-9[8]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (92547, 'textbox_52.211-9[9]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (92547, 'textbox_52.211-9[10]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (92547, 'textbox_52.211-9[11]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (92547, 'textbox_52.211-9[12]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (92547, 'textbox_52.211-9[13]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (92547, 'textbox_52.211-9[14]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (92547, 'textbox_52.211-9[15]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (92547, 'textbox_52.211-9[16]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (92547, 'textbox_52.211-9[17]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (92547, 'textbox_52.211-9[18]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (92547, 'textbox_52.211-9[19]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (92547, 'textbox_52.211-9[20]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (92547, 'textbox_52.211-9[21]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (92547, 'textbox_52.211-9[22]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (92547, 'textbox_52.211-9[23]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (92547, 'textbox_52.211-9[24]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (92547, 'textbox_52.211-9[25]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (92547, 'textbox_52.211-9[26]', 'S', 100);

UPDATE Clauses
SET effective_date = '2014-04-01' -- '2014-01-01'
WHERE clause_id = 92548; -- 52.212-1

UPDATE Clauses
SET effective_date = '2014-10-01' -- '2014-01-01'
WHERE clause_id = 92549; -- 52.212-2

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1212_63&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?node=se48.2.52_1212_63&amp;rgn=div8'
, clause_data = '<p>The Offeror shall complete only paragraph (b) of this provision if the Offeror has completed the annual representations and certification electronically via the System for Award Management (SAM) Web site accessed through <i>http://www.acquisition.gov.</i> If the Offeror has not completed the annual representations and certifications electronically, the Offeror shall complete only paragraphs (c) through (p) of this provision.</p><p>(a) <i>Definitions.</i> As used in this provision-</p><p><i>Economically disadvantaged women-owned small business (EDWOSB) concern</i> means a small business concern that is at least 51 percent directly and unconditionally owned by, and the management and daily business operations of which are controlled by, one or more women who are citizens of the United States and who are economically disadvantaged in accordance with 13 CFR part 127. It automatically qualifies as a women-owned small business eligible under the WOSB Program.</p><p><i>Forced or indentured child labor</i> means all work or service- </p><p>(1) Exacted from any person under the age of 18 under the menace of any penalty for its nonperformance and for which the worker does not offer himself voluntarily; or </p><p>(2) Performed by any person under the age of 18 pursuant to a contract the enforcement of which can be accomplished by process or penalties.</p><p><i>Highest-level owner</i> means the entity that owns or controls an immediate owner of the offeror, or that owns or controls one or more entities that control an immediate owner of the offeror. No entity owns or exercises control of the highest level owner.</p><p><i>Immediate owner</i> means an entity, other than the offeror, that has direct control of the offeror. Indicators of control include, but are not limited to, one or more of the following: Ownership or interlocking management, identity of interests among family members, shared facilities and equipment, and the common use of employees.</p><p><i>Inverted domestic corporation</i> means a foreign incorporated entity that meets the definition of an inverted domestic corporation under 6 U.S.C. 395(b), applied in accordance with the rules and definitions of 6 U.S.C. 395(c).</p><p><i>Manufactured end product</i> means any end product in Federal Supply Classes (FSC) 1000-9999, except-</p><p>(1) FSC 5510, Lumber and Related Basic Wood Materials;</p><p>(2) Federal Supply Group (FSG) 87, Agricultural Supplies;</p><p>(3) FSG 88, Live Animals;</p><p>(4) FSG 89, Food and Related Consumables;</p><p>(5) FSC 9410, Crude Grades of Plant Materials;</p><p>(6) FSC 9430, Miscellaneous Crude Animal Products, Inedible;</p><p>(7) FSC 9440, Miscellaneous Crude Agricultural and Forestry Products;</p><p>(8) FSC 9610, Ores;</p><p>(9) FSC 9620, Minerals, Natural and Synthetic; and</p><p>(10) FSC 9630, Additive Metal Materials.</p><p><i>Manufactured end product</i> means any end product in product and service codes (PSCs) 1000-9999, except-</p><p>(1) PSC 5510, Lumber and Related Basic Wood Materials;</p><p>(2) Product or Service Group (PSG) 87, Agricultural Supplies;</p><p>(3) PSG 88, Live Animals;</p><p>(4) PSG 89, Subsistence;</p><p>(5) PSC 9410, Crude Grades of Plant Materials;</p><p>(6) PSC 9430, Miscellaneous Crude Animal Products, Inedible;</p><p>(7) PSC 9440, Miscellaneous Crude Agricultural and Forestry Products;</p><p>(8) PSC 9610, Ores;</p><p>(9) PSC 9620, Minerals, Natural and Synthetic; and</p><p>(10) PSC 9630, Additive Metal Materials.</p><p><i>Place of manufacture</i> means the place where an end product is assembled out of components, or otherwise made or processed from raw materials into the finished product that is to be provided to the Government. If a product is disassembled and reassembled, the place of reassembly is not the place of manufacture.</p><p><i>Restricted business operations</i> means business operations in Sudan that include power production activities, mineral extraction activities, oil-related activities, or the production of military equipment, as those terms are defined in the Sudan Accountability and Divestment Act of 2007 (Pub. L. 110-174). Restricted business operations do not include business operations that the person (as that term is defined in Section 2 of the Sudan Accountability and Divestment Act of 2007) conducting the business can demonstrate-</p><p>(1) Are conducted under contract directly and exclusively with the regional government of southern Sudan;</p><p>(2) Are conducted pursuant to specific authorization from the Office of Foreign Assets Control in the Department of the Treasury, or are expressly exempted under Federal law from the requirement to be conducted under such authorization;</p><p>(3) Consist of providing goods or services to marginalized populations of Sudan;</p><p>(4) Consist of providing goods or services to an internationally recognized peacekeeping force or humanitarian organization;</p><p>(5) Consist of providing goods or services that are used only to promote health or education; or</p><p>(6) Have been voluntarily suspended.</p><p><i>Sensitive technology</i>-</p><p>(1) Means hardware, software, telecommunications equipment, or any other technology that is to be used specifically-</p><p>(i) To restrict the free flow of unbiased information in Iran; or</p><p>(ii) To disrupt, monitor, or otherwise restrict speech of the people of Iran; and</p><p>(2) Does not include information or informational materials the export of which the President does not have the authority to regulate or prohibit pursuant to section 203(b)(3) of the International Emergency Economic Powers Act (50 U.S.C. 1702(b)(3)).</p><p><i>Service-disabled veteran-owned small business concern</i>- </p><p>(1) Means a small business concern- </p><p>(i) Not less than 51 percent of which is owned by one or more service-disabled veterans or, in the case of any publicly owned business, not less than 51 percent of the stock of which is owned by one or more service-disabled veterans; and </p><p>(ii) The management and daily business operations of which are controlled by one or more service-disabled veterans or, in the case of a service-disabled veteran with permanent and severe disability, the spouse or permanent caregiver of such veteran. </p><p>(2) <i>Service-disabled veteran</i> means a veteran, as defined in 38 U.S.C. 101(2), with a disability that is service-connected, as defined in 38 U.S.C. 101(16).</p><p><i>Small business concern</i> means a concern, including its affiliates, that is independently owned and operated, not dominant in the field of operation in which it is bidding on Government contracts, and qualified as a small business under the criteria in 13 CFR Part 121 and size standards in this solicitation.</p><p><i>Small disadvantaged business concern,</i> consistent with 13 CFR 124.1002, means a small business concern under the size standard applicable to the acquisition, that-</p><p>(1) Is at least 51 percent unconditionally and directly owned (as defined at 13 CFR 124.105) by-</p><p>(i) One or more socially disadvantaged (as defined at 13 CFR 124.103) and economically disadvantaged (as defined at 13 CFR 124.104) individuals who are citizens of the United States; and</p><p>(ii) Each individual claiming economic disadvantage has a net worth not exceeding $750,000 after taking into account the applicable exclusions set forth at 13 CFR 124.104(c)(2); and</p><p>(2) The management and daily business operations of which are controlled (as defined at 13.CFR 124.106) by individuals, who meet the criteria in paragraphs (1)(i) and (ii) of this definition.</p><p><i>Subsidiary</i> means an entity in which more than 50 percent of the entity is owned-</p><p>(1) Directly by a parent corporation; or</p><p>(2) Through another subsidiary of a parent corporation.</p><p><i>Veteran-owned small business concern</i> means a small business concern- </p><p>(1) Not less than 51 percent of which is owned by one or more veterans (as defined at 38 U.S.C. 101(2)) or, in the case of any publicly owned business, not less than 51 percent of the stock of which is owned by one or more veterans; and</p><p>(2) The management and daily business operations of which are controlled by one or more veterans.</p><p><i>Women-owned business concern</i> means a concern which is at least 51 percent owned by one or more women; or in the case of any publicly owned business, at least 51 percent of its stock is owned by one or more women; and whose management and daily business operations are controlled by one or more women.</p><p><i>Women-owned small business concern</i> means a small business concern-</p><p>(1) That is at least 51 percent owned by one or more women; or, in the case of any publicly owned business, at least 51 percent of the stock of which is owned by one or more women; and</p><p>(2) Whose management and daily business operations are controlled by one or more women.</p><p><i>Women-owned small business (WOSB) concern eligible under the WOSB Program</i> (in accordance with 13 CFR part 127), means a small business concern that is at least 51 percent directly and unconditionally owned by, and the management and daily business operations of which are controlled by, one or more women who are citizens of the United States.</p><p>(b)(1) <i>Annual Representations and Certifications.</i> Any changes provided by the offeror in paragraph (b)(2) of this provision do not automatically change the representations and certifications posted on the SAM website.</p><p>(2) The offeror has completed the annual representations and certifications electronically via the SAM website accessed through <i>http://www.acquisition.gov.</i> After reviewing the SAM database information, the offeror verifies by submission of this offer that the representations and certifications currently posted electronically at FAR 52.212-3, Offeror Representations and Certifications-Commercial Items, have been entered or updated in the last 12 months, are current, accurate, complete, and applicable to this solicitation (including the business size standard applicable to the NAICS code referenced for this solicitation), as of the date of this offer and are incorporated in this offer by reference (see FAR 4.1201), except for paragraphs {{textbox_52.212-3[0]}}.</p><p>[<i>Offeror to identify the applicable paragraphs at (c) through (p) of this provision that the offeror has completed for the purposes of this solicitation only, if any.</i></p><p><i>These amended representation(s) and/or certification(s) are also incorporated in this offer and are current, accurate, and complete as of the date of this offer.</i></p><p><i>Any changes provided by the offeror are applicable to this solicitation only, and do not result in an update to the representations and certifications posted electronically on SAM.</i>]</p><p>(c) Offerors must complete the following representations when the resulting contract will be performed in the United States or its outlying areas. Check all that apply.</p><p>(1) <i>Small business concern.</i> The offeror represents as part of its offer that it &#x25a1; is, &#x25a1;&nbsp;&nbsp;is not a small business concern.</p><p>(2) <i>Veteran-owned small business concern.</i> [<i>Complete only if the offeror represented itself as a small business concern in paragraph (c)(1) of this provision.</i>] The offeror represents as part of its offer that it &#x25a1; is, &#x25a1; is not a veteran-owned small business concern. </p><p>(3) <i>Service-disabled veteran-owned small business concern.</i> [<i>Complete only if the offeror represented itself as a veteran-owned small business concern in paragraph (c)(2) of this provision.</i>] The offeror represents as part of its offer that it &#x25a1; is, &#x25a1; is not a service-disabled veteran-owned small business concern.</p><p>(4) <i>Small disadvantaged business concern. [Complete only if the offeror represented itself as a small business concern in paragraph (c)(1) of this provision.]</i> The offeror represents that it &#x25a1; is, &#x25a1; is not a small disadvantaged business concern as defined in 13 CFR 124.1002.</p><p>(5) <i>Women-owned small business concern. [Complete only if the offeror represented itself as a small business concern in paragraph (c)(1) of this provision.]</i> The offeror represents that it &#x25a1; is, &#x25a1; is not a women-owned small business concern.</p><p>(6) WOSB concern eligible under the WOSB Program. [<i>Complete only if the offeror represented itself as a women-owned small business concern in paragraph (c)(5) of this provision.</i>] The offeror represents that-</p><p>(i) It &#x25a1; is, &#x25a1; is not a WOSB concern eligible under the WOSB Program, has provided all the required documents to the WOSB Repository, and no change in circumstances or adverse decisions have been issued that affects its eligibility; and</p><p>(ii) It &#x25a1; is, &#x25a1; is not a joint venture that complies with the requirements of 13 CFR part 127, and the representation in paragraph (c)(6)(i) of this provision is accurate for each WOSB concern eligible under the WOSB Program participating in the joint venture. [<i>The offeror shall enter the name or names of the WOSB concern eligible under the WOSB Program and other small businesses that are participating in the joint venture:</i> {{textbox_52.212-3[1]}}.] Each WOSB concern eligible under the WOSB Program participating in the joint venture shall submit a separate signed copy of the WOSB representation.</p><p>(7) Economically disadvantaged women-owned small business (EDWOSB) concern. [<i>Complete only if the offeror represented itself as a WOSB concern eligible under the WOSB Program in (c)(6) of this provision.</i>] The offeror represents that-</p><p>(i) It &#x25a1; is, &#x25a1; is not an EDWOSB concern, has provided all the required documents to the WOSB Repository, and no change in circumstances or adverse decisions have been issued that affects its eligibility; and</p><p>(ii) It &#x25a1; is, &#x25a1; is not a joint venture that complies with the requirements of 13 CFR part 127, and the representation in paragraph (c)(7)(i) of this provision is accurate for each EDWOSB concern participating in the joint venture. [<i>The offeror shall enter the name or names of the EDWOSB concern and other small businesses that are participating in the joint venture:</i> {{textbox_52.212-3[2]}}.] Each EDWOSB concern participating in the joint venture shall submit a separate signed copy of the EDWOSB representation.</p><p>Note to paragraphs (c)(8) and (9): Complete paragraphs (c)(8) and (9) only if this solicitation is expected to exceed the simplified acquisition threshold.</p><p>(8) <i>Women-owned business concern (other than small business concern). [Complete only if the offeror is a women-owned business concern and did not represent itself as a small business concern in paragraph (c)(1) of this provision.]</i> The offeror represents that it &#x25a1; is, a women-owned business concern.</p><p>(9) <i>Tie bid priority for labor surplus area concerns.</i> If this is an invitation for bid, small business offerors may identify the labor surplus areas in which costs to be incurred on account of manufacturing or production (by offeror or first-tier subcontractors) amount to more than 50 percent of the contract price:</p>&nbsp; {{textbox_52.212-3[3]}}<p>(10) <i>HUBZone small business concern.</i> [Complete only if the offeror represented itself as a small business concern in paragraph (c)(1) of this provision.] The offeror represents, as part of its offer, that-</p><p>(i) It &#x25a1; is, &#x25a1; is not a HUBZone small business concern listed, on the date of this representation, on the List of Qualified HUBZone Small Business Concerns maintained by the Small Business Administration, and no material changes in ownership and control, principal office, or HUBZone employee percentage have occurred since it was certified in accordance with 13 CFR Part 126; and</p><p>(ii) It &#x25a1; is, &#x25a1; is not a HUBZone joint venture that complies with the requirements of 13 CFR Part 126, and the representation in paragraph (c)(10)(i) of this provision is accurate for each HUBZone small business concern participating in the HUBZone joint venture. [<i>The offeror shall enter the names of each of the HUBZone small business concerns participating in the HUBZone joint venture: {{textbox_52.212-3[4]}}.</i>] Each HUBZone small business concern participating in the HUBZone joint venture shall submit a separate signed copy of the HUBZone representation.</p><p>(d) Representations required to implement provisions of Executive Order 11246-</p><p>(1) Previous contracts and compliance. The offeror represents that-</p><p>(i) It &#x25a1; has, &#x25a1; has not participated in a previous contract or subcontract subject to the Equal Opportunity clause of this solicitation; and</p><p>(ii) It &#x25a1; has, &#x25a1; has not filed all required compliance reports.</p><p>(2) Affirmative Action Compliance. The offeror represents that-</p><p>(i) It &#x25a1; has developed and has on file, &#x25a1;&nbsp;&nbsp;&nbsp;has not developed and does not have on file, at each establishment, affirmative action programs required by rules and regulations of the Secretary of Labor (41 CFR parts 60-1 and 60-2), or</p><p>(ii) It &#x25a1;&nbsp;&nbsp;has not previously had contracts subject to the written affirmative action programs requirement of the rules and regulations of the Secretary of Labor.</p><p>(e) <i>Certification Regarding Payments to Influence Federal Transactions (31 U.S.C. 1352).</i> (Applies only if the contract is expected to exceed $150,000.) By submission of its offer, the offeror certifies to the best of its knowledge and belief that no Federal appropriated funds have been paid or will be paid to any person for influencing or attempting to influence an officer or employee of any agency, a Member of Congress, an officer or employee of Congress or an employee of a Member of Congress on his or her behalf in connection with the award of any resultant contract. If any registrants under the Lobbying Disclosure Act of 1995 have made a lobbying contact on behalf of the offeror with respect to this contract, the offeror shall complete and submit, with its offer, OMB Standard Form LLL, Disclosure of Lobbying Activities, to provide the name of the registrants. The offeror need not report regularly employed officers or employees of the offeror to whom payments of reasonable compensation were made.</p><p>(f) <i>Buy American Certificate.</i> (Applies only if the clause at Federal Acquisition Regulation (FAR) 52.225-1, Buy American-Supplies, is included in this solicitation.)</p><p>(1) The offeror certifies that each end product, except those listed in paragraph (f)(2) of this provision, is a domestic end product and that for other than COTS items, the offeror has considered components of unknown origin to have been mined, produced, or manufactured outside the United States. The offeror shall list as foreign end products those end products manufactured in the United States that do not qualify as domestic end products, <i>i.e.</i>, an end product that is not a COTS item and does not meet the component test in paragraph (2) of the definition of ''domestic end product.'' The terms ''commercially available off-the-shelf (COTS) item,'' ''component,'' ''domestic end product,'' ''end product,'' ''foreign end product,'' and ''United States'' are defined in the clause of this solicitation entitled ''Buy American-Supplies.''</p><p>(2) Foreign End Products:</p>Line Item No.: {{textbox_52.212-3[5]}}Country of Origin: {{textbox_52.212-3[6]}}<p>(List as necessary)</p><p>(3) The Government will evaluate offers in accordance with the policies and procedures of FAR Part 25.</p><p>(g)(1) <i>Buy American-Free Trade Agreements-Israeli Trade Act Certificate.</i> (Applies only if the clause at FAR 52.225-3, Buy American-Free Trade Agreements-Israeli Trade Act, is included in this solicitation.)</p><p>(i) The offeror certifies that each end product, except those listed in paragraph (g)(1)(ii) or (g)(1)(iii) of this provision, is a domestic end product and that for other than COTS items, the offeror has considered components of unknown origin to have been mined, produced, or manufactured outside the United States. The terms ''Bahrainian, Moroccan, Omani, Panamanian, or Peruvian end product,'' ''commercially available off-the-shelf (COTS) item,'' ''component,'' ''domestic end product,'' ''end product,'' ''foreign end product,'' ''Free Trade Agreement country,'' ''Free Trade Agreement country end product,'' ''Israeli end product,'' and ''United States'' are defined in the clause of this solicitation entitled ''Buy American-Free Trade Agreements-Israeli Trade Act.''</p><p>(ii) The offeror certifies that the following supplies are Free Trade Agreement country end products (other than Bahrainian, Moroccan, Omani, Panamanian, or Peruvian end products) or Israeli end products as defined in the clause of this solicitation entitled ''Buy American-Free Trade Agreements-Israeli Trade Act''</p><p>Free Trade Agreement Country End Products (Other than Bahrainian, Moroccan, Omani, Panamanian, or Peruvian End Products) or Israeli End Products:</p><div><div><table border="1" cellpadding="1" cellspacing="1" frame="void" width="100%"><tbody><tr><th scope="col">Line Item No.</th><th scope="col">&nbsp;&nbsp;</th><th scope="col">Country of Origin</th></tr><tr><td>{{textbox_52.212-3[7]}}</td><td>{{textbox_52.212-3[8]}}</td><td>{{textbox_52.212-3[9]}}</td></tr><tr><td>{{textbox_52.212-3[10]}}</td><td>{{textbox_52.212-3[11]}}</td><td>{{textbox_52.212-3[12]}}</td></tr><tr><td>{{textbox_52.212-3[13]}}</td><td>{{textbox_52.212-3[14]}}</td><td>{{textbox_52.212-3[15]}}</td></tr><tr><td align="center" colspan="3" scope="row">[List as necessary]</td></tr></tbody></table></div></div><p>(iii) The offeror shall list those supplies that are foreign end products (other than those listed in paragraph (g)(1)(ii) of this provision) as defined in the clause of this solicitation entitled ''Buy American-Free Trade Agreements-Israeli Trade Act.'' The offeror shall list as other foreign end products those end products manufactured in the United States that do not qualify as domestic end products, <i>i.e.</i>, an end product that is not a COTS item and does not meet the component test in paragraph (2) of the definition of ''domestic end product.''</p><h2>Other Foreign End Products</h2>Line Item No.: {{textbox_52.212-3[16]}}Country of Origin: {{textbox_52.212-3[17]}}<p>(List as necessary)</p><p>(iv) The Government will evaluate offers in accordance with the policies and procedures of FAR Part 25.</p><p>(2) <i>Buy American-Free Trade Agreements-Israeli Trade Act Certificate, Alternate I.</i> If <i>Alternate I</i> to the clause at FAR 52.225-3 is included in this solicitation, substitute the following paragraph (g)(1)(ii) for paragraph (g)(1)(ii) of the basic provision: </p><p>(g)(1)(ii) The offeror certifies that the following supplies are Canadian end products as defined in the clause of this solicitation entitled ''Buy American-Free Trade Agreements-Israeli Trade Act'': </p><h3>Canadian End Products: </h3><h3>Line Item No. </h3>&nbsp; {{textbox_52.212-3[18]}}&nbsp; {{textbox_52.212-3[19]}}&nbsp; {{textbox_52.212-3[20]}}<p>$(<i>List as necessary</i>) </p><p>(3) <i>Buy American-Free Trade Agreements-Israeli Trade Act Certificate, Alternate II.</i> If <i>Alternate II</i> to the clause at FAR 52.225-3 is included in this solicitation, substitute the following paragraph (g)(1)(ii) for paragraph (g)(1)(ii) of the basic provision: </p><p>(g)(1)(ii) The offeror certifies that the following supplies are Canadian end products or Israeli end products as defined in the clause of this solicitation entitled ''Buy American-Free Trade Agreements-Israeli Trade Act'': </p><h3>Canadian or Israeli End Products: </h3><p>Line Item No. </p>&nbsp; {{textbox_52.212-3[21]}}&nbsp; {{textbox_52.212-3[22]}}&nbsp; {{textbox_52.212-3[23]}}<p>Country of Origin </p>&nbsp; {{textbox_52.212-3[24]}}&nbsp; {{textbox_52.212-3[25]}}&nbsp; {{textbox_52.212-3[26]}}<p>$(<i>List as necessary</i>)</p><p>(g)(4) <i>Buy American-Free Trade Agreements-Israeli Trade Act Certificate, Alternate III.</i> If Alternate III to the clause at FAR 52.225-3 is included in this solicitation, substitute the following paragraph (g)(1)(ii) for paragraph (g)(1)(ii) of the basic provision:</p><p>(g)(1)(ii) The offeror certifies that the following supplies are Free Trade Agreement country end products (other than Bahrainian, Korean, Moroccan, Omani, Panamanian, or Peruvian end products) or Israeli end products as defined in the clause of this solicitation entitled ''Buy American-Free Trade Agreements-Israeli Trade Act'':</p><p>Free Trade Agreement Country End Products (Other than Bahrainian, Korean, Moroccan, Omani, Panamanian, or Peruvian End Products) or Israeli End Products:</p><div><div><table border="1" cellpadding="1" cellspacing="1" frame="void" width="100%"><tbody><tr><th scope="col">Line Item No.</th><th scope="col">&nbsp;&nbsp;</th><th scope="col">Country of Origin</th></tr><tr><td>{{textbox_52.212-3[27]}}</td><td>{{textbox_52.212-3[28]}}</td><td>{{textbox_52.212-3[29]}}</td></tr><tr><td>{{textbox_52.212-3[30]}}</td><td>{{textbox_52.212-3[31]}}</td><td>{{textbox_52.212-3[32]}}</td></tr><tr><td>{{textbox_52.212-3[33]}}</td><td>{{textbox_52.212-3[34]}}</td><td>{{textbox_52.212-3[35]}}</td></tr><tr><td align="center" colspan="3" scope="row">[List as necessary]</td></tr></tbody></table></div></div><p>(5) <i>Trade Agreements Certificate.</i> (Applies only if the clause at FAR 52.225-5, Trade Agreements, is included in this solicitation.)</p><p>(i) The offeror certifies that each end product, except those listed in paragraph (g)(5)(ii) of this provision, is a U.S.-made or designated country end product, as defined in the clause of this solicitation entitled ''Trade Agreements''.</p><p>(ii) The offeror shall list as other end products those end products that are not U.S.-made or designated country end products.</p><p>Other End Products:</p><div><div><table border="1" cellpadding="1" cellspacing="1" frame="void" width="100%"><tbody><tr><th scope="col">Line item No.</th><th scope="col">&nbsp;&nbsp;</th><th scope="col">Country of origin</th></tr><tr><td>{{textbox_52.212-3[36]}}</td><td>{{textbox_52.212-3[37]}}</td><td>{{textbox_52.212-3[38]}}</td></tr><tr><td>{{textbox_52.212-3[39]}}</td><td>{{textbox_52.212-3[40]}}</td><td>{{textbox_52.212-3[41]}}</td></tr><tr><td>{{textbox_52.212-3[42]}}</td><td>{{textbox_52.212-3[43]}}</td><td>{{textbox_52.212-3[44]}}</td></tr><tr><td align="center" colspan="3" scope="row">[List as necessary]</td></tr></tbody></table></div></div><p>(iii) The Government will evaluate offers in accordance with the policies and procedures of FAR Part 25. For line items covered by the WTO GPA, the Government will evaluate offers of U.S.-made or designated country end products without regard to the restrictions of the Buy American statute. The Government will consider for award only offers of U.S.-made or designated country end products unless the Contracting Officer determines that there are no offers for such products or that the offers for such products are insufficient to fulfill the requirements of the solicitation.</p><p>(h) <i>Certification Regarding Responsibility Matters (Executive Order 12689).</i> (Applies only if the contract value is expected to exceed the simplified acquisition threshold.) The offeror certifies, to the best of its knowledge and belief, that the offeror and/or any of its principals- </p><p>(1) &#x25a1; Are, &#x25a1; are not presently debarred, suspended, proposed for debarment, or declared ineligible for the award of contracts by any Federal agency;</p><p>(2) &#x25a1; Have, &#x25a1; have not, within a three-year period preceding this offer, been convicted of or had a civil judgment rendered against them for: Commission of fraud or a criminal offense in connection with obtaining, attempting to obtain, or performing a Federal, state or local government contract or subcontract; violation of Federal or state antitrust statutes relating to the submission of offers; or Commission of embezzlement, theft, forgery, bribery, falsification or destruction of records, making false statements, tax evasion, violating Federal criminal tax laws, or receiving stolen property,</p><p>(3) &#x25a1; Are, &#x25a1; are not presently indicted for, or otherwise criminally or civilly charged by a Government entity with, commission of any of these offenses enumerated in paragraph (h)(2) of this clause; and</p><p>(i) <i>Certification Regarding Knowledge of Child Labor for Listed End Products (Executive Order 13126). [The Contracting Officer must list in paragraph (i)(1) any end products being acquired under this solicitation that are included in the List of Products Requiring Contractor Certification as to Forced or Indentured Child Labor, unless excluded at 22.1503(b).</i>] </p><p>(1) <i>Listed end products.</i> </p><h3>Listed End Product</h3>&nbsp; {{textbox_52.212-3[45]}}&nbsp;  {{textbox_52.212-3[46]}}<h3>Listed Countries of Origin </h3>&nbsp; {{textbox_52.212-3[47]}}&nbsp; {{textbox_52.212-3[48]}}<p>(2) <i>Certification.</i> [<i>If the Contracting Officer has identified end products and countries of origin in paragraph (i)(1) of this provision, then the offeror must certify to either (i)(2)(i) or (i)(2)(ii) by checking the appropriate block.</i>]</p><p>&#x25a1;&nbsp;&nbsp;(i) The offeror will not supply any end product listed in paragraph (i)(1) of this provision that was mined, produced, or manufactured in the corresponding country as listed for that product.</p><p>&#x25a1;&nbsp;&nbsp;(ii) The offeror may supply an end product listed in paragraph (i)(1) of this provision that was mined, produced, or manufactured in the corresponding country as listed for that product. The offeror certifies that it has made a good faith effort to determine whether forced or indentured child labor was used to mine, produce, or manufacture any such end product furnished under this contract. On the basis of those efforts, the offeror certifies that it is not aware of any such use of child labor.</p><p>(4) Have,&#x25a1;&nbsp;&nbsp; have not, within a three-year period preceding this offer, been notified of any delinquent Federal taxes in an amount that exceeds $3,000 for which the liability remains unsatisfied.</p><p>(i) Taxes are considered delinquent if both of the following criteria apply:</p><p>(A) <i>The tax liability is finally determined.</i> The liability is finally determined if it has been assessed. A liability is not finally determined if there is a pending administrative or judicial challenge. In the case of a judicial challenge to the liability, the liability is not finally determined until all judicial appeal rights have been exhausted.</p><p>(B) <i>The taxpayer is delinquent in making payment.</i> A taxpayer is delinquent if the taxpayer has failed to pay the tax liability when full payment was due and required. A taxpayer is not delinquent in cases where enforced collection action is precluded.</p><p>(ii) <i>Examples.</i> (A) The taxpayer has received a statutory notice of deficiency, under I.R.C. §6212, which entitles the taxpayer to seek Tax Court review of a proposed tax deficiency. This is not a delinquent tax because it is not a final tax liability. Should the taxpayer seek Tax Court review, this will not be a final tax liability until the taxpayer has exercised all judicial appeal rights.</p><p>(B) The IRS has filed a notice of Federal tax lien with respect to an assessed tax liability, and the taxpayer has been issued a notice under I.R.C. §6320 entitling the taxpayer to request a hearing with the IRS Office of Appeals contesting the lien filing, and to further appeal to the Tax Court if the IRS determines to sustain the lien filing. In the course of the hearing, the taxpayer is entitled to contest the underlying tax liability because the taxpayer has had no prior opportunity to contest the liability. This is not a delinquent tax because it is not a final tax liability. Should the taxpayer seek tax court review, this will not be a final tax liability until the taxpayer has exercised all judicial appeal rights.</p><p>(C) The taxpayer has entered into an installment agreement pursuant to I.R.C. §6159. The taxpayer is making timely payments and is in full compliance with the agreement terms. The taxpayer is not delinquent because the taxpayer is not currently required to make full payment.</p><p>(D) The taxpayer has filed for bankruptcy protection. The taxpayer is not delinquent because enforced collection action is stayed under 11 U.S.C. 362 (the Bankruptcy Code).</p><p>(i) <i>Certification Regarding Knowledge of Child Labor for Listed End Products (Executive Order 13126). [The Contracting Officer must list in paragraph (i)(1) any end products being acquired under this solicitation that are included in the List of Products Requiring Contractor Certification as to Forced or Indentured Child Labor, unless excluded at 22.1503(b).</i>] </p><p>(1) <i>Listed end products.</i> </p><h3>Listed End Product</h3>&nbsp; {{textbox_52.212-3[49]}}&nbsp;  {{textbox_52.212-3[50]}}<h3>Listed Countries of Origin </h3>&nbsp; {{textbox_52.212-3[51]}}&nbsp; {{textbox_52.212-3[52]}}<p>(2) <i>Certification.</i> [<i>If the Contracting Officer has identified end products and countries of origin in paragraph (i)(1) of this provision, then the offeror must certify to either (i)(2)(i) or (i)(2)(ii) by checking the appropriate block.</i>]</p><p>&#x25a1;&nbsp;&nbsp;(i) The offeror will not supply any end product listed in paragraph (i)(1) of this provision that was mined, produced, or manufactured in the corresponding country as listed for that product.</p><p>&#x25a1;&nbsp;&nbsp;(ii) The offeror may supply an end product listed in paragraph (i)(1) of this provision that was mined, produced, or manufactured in the corresponding country as listed for that product. The offeror certifies that it has made a good faith effort to determine whether forced or indentured child labor was used to mine, produce, or manufacture any such end product furnished under this contract. On the basis of those efforts, the offeror certifies that it is not aware of any such use of child labor.</p><p>(j) <i>Place of manufacture.</i> (Does not apply unless the solicitation is predominantly for the acquisition of manufactured end products.) For statistical purposes only, the offeror shall indicate whether the place of manufacture of the end products it expects to provide in response to this solicitation is predominantly-</p><p>(1) &#x25a1; In the United States (Check this box if the total anticipated price of offered end products manufactured in the United States exceeds the total anticipated price of offered end products manufactured outside the United States); or</p><p>(2) &#x25a1; Outside the United States.</p><p>(k) <i>Certificates regarding exemptions from the application of the Service Contract Labor Standards.</i> (Certification by the offeror as to its compliance with respect to the contract also constitutes its certification as to compliance by its subcontractor if it subcontracts out the exempt services.) [<i>The contracting officer is to check a box to indicate if paragraph (k)(1) or (k)(2) applies.</i>]</p><p>(1)&#x25a1;&nbsp;&nbsp; Maintenance, calibration, or repair of certain equipment as described in FAR 22.1003-4(c)(1). The offeror &#x25a1;&nbsp;&nbsp;does &#x25a1;&nbsp;&nbsp; does not certify that-</p><p>(i) The items of equipment to be serviced under this contract are used regularly for other than Governmental purposes and are sold or traded by the offeror (or subcontractor in the case of an exempt subcontract) in substantial quantities to the general public in the course of normal business operations;</p><p>(ii) The services will be furnished at prices which are, or are based on, established catalog or market prices (see FAR 22.1003-4(c)(2)(ii)) for the maintenance, calibration, or repair of such equipment; and</p><p>(iii) The compensation (wage and fringe benefits) plan for all service employees performing work under the contract will be the same as that used for these employees and equivalent employees servicing the same equipment of commercial customers.</p><p>(2)&#x25a1;&nbsp;&nbsp;Certain services as described in FAR 22.1003-4(d)(1). The offeror &#x25a1;&nbsp;&nbsp;does &#x25a1;&nbsp;&nbsp;does not certify that-</p><p>(i) The services under the contract are offered and sold regularly to non-Governmental customers, and are provided by the offeror (or subcontractor in the case of an exempt subcontract) to the general public in substantial quantities in the course of normal business operations;</p><p>(ii) The contract services will be furnished at prices that are, or are based on, established catalog or market prices (see FAR 22.1003-4(d)(2)(iii));</p><p>(iii) Each service employee who will perform the services under the contract will spend only a small portion of his or her time (a monthly average of less than 20 percent of the available hours on an annualized basis, or less than 20 percent of available hours during the contract period if the contract period is less than a month) servicing the Government contract; and</p><p>(iv) The compensation (wage and fringe benefits) plan for all service employees performing work under the contract is the same as that used for these employees and equivalent employees servicing commercial customers.</p><p>(3) If paragraph (k)(1) or (k)(2) of this clause applies-</p><p>(i) If the offeror does not certify to the conditions in paragraph (k)(1) or (k)(2) and the Contracting Officer did not attach a Service Contract Labor Standards wage determination to the solicitation, the offeror shall notify the Contracting Officer as soon as possible; and</p><p>(ii) The Contracting Officer may not make an award to the offeror if the offeror fails to execute the certification in paragraph (k)(1) or (k)(2) of this clause or to contact the Contracting Officer as required in paragraph (k)(3)(i) of this clause.</p><p>(l) <i>Taxpayer Identification Number (TIN) (26 U.S.C. 6109, 31 U.S.C. 7701).</i> (Not applicable if the offeror is required to provide this information to the SAM database to be eligible for award.)</p><p>(1) All offerors must submit the information required in paragraphs (l)(3) through (l)(5) of this provision to comply with debt collection requirements of 31 U.S.C. 7701(c) and 3325(d), reporting requirements of 26 U.S.C. 6041, 6041A, and 6050M, and implementing regulations issued by the Internal Revenue Service (IRS).</p><p>(2) The TIN may be used by the Government to collect and report on any delinquent amounts arising out of the offeror''s relationship with the Government (31 U.S.C. 7701(c)(3)). If the resulting contract is subject to the payment reporting requirements described in FAR 4.904, the TIN provided hereunder may be matched with IRS records to verify the accuracy of the offeror''s TIN.</p><p>(3) <i>Taxpayer Identification Number (TIN).</i></p><p>&#x25a1;&nbsp;&nbsp;TIN: {{textbox_52.212-3[53]}}.</p><p>&#x25a1;&nbsp;&nbsp;TIN has been applied for.</p><p>&#x25a1;&nbsp;&nbsp;TIN is not required because:</p><p>&#x25a1;&nbsp;&nbsp;Offeror is a nonresident alien, foreign corporation, or foreign partnership that does not have income effectively connected with the conduct of a trade or business in the United States and does not have an office or place of business or a fiscal paying agent in the United States;</p><p>&#x25a1;&nbsp;&nbsp;Offeror is an agency or instrumentality of a foreign government;</p><p>&#x25a1;&nbsp;&nbsp;Offeror is an agency or instrumentality of the Federal Government.</p><p>(4) <i>Type of organization.</i></p><p>&#x25a1;&nbsp;&nbsp;Sole proprietorship;</p><p>&#x25a1;&nbsp;&nbsp;Partnership;</p><p>&#x25a1;&nbsp;&nbsp;Corporate entity (not tax-exempt);</p><p>&#x25a1;&nbsp;&nbsp;Corporate entity (tax-exempt);</p><p>&#x25a1;&nbsp;&nbsp;Government entity (Federal, State, or local);</p><p>&#x25a1;&nbsp;&nbsp;Foreign government;</p><p>&#x25a1;&nbsp;&nbsp;International organization per 26 CFR 1.6049-4;</p><p>&#x25a1;&nbsp;&nbsp;Other {{textbox_52.212-3[54]}}.</p><p>(5) <i>Common parent.</i></p><p>&#x25a1;&nbsp;&nbsp;Offeror is not owned or controlled by a common parent;</p><p>&#x25a1;&nbsp;&nbsp;Name and TIN of common parent:</p><p>Name {{textbox_52.212-3[55]}}.</p><p>TIN {{textbox_52.212-3[56]}}.</p><p>(m) <i>Restricted business operations in Sudan.</i> By submission of its offer, the offeror certifies that the offeror does not conduct any restricted business operations in Sudan.</p><p>(n) <i>Prohibition on Contracting with Inverted Domestic Corporations.</i> (1) Government agencies are not permitted to use appropriated (or otherwise made available) funds for contracts with either an inverted domestic corporation, or a subsidiary of an inverted domestic corporation, unless the exception at 9.108-2(b) applies or the requirement is waived in accordance with the procedures at 9.108-4.</p><p>(2) <i>Representation.</i> By submission of its offer, the offeror represents that-</p><p>(i) It is not an inverted domestic corporation; and</p><p>(ii) It is not a subsidiary of an inverted domestic corporation.</p><p>(o) <i>Prohibition on contracting with entities engaging in certain activities or transactions relating to Iran.</i> (1) The offeror shall email questions concerning sensitive technology to the Department of State at CISADA106@state.gov.</p><p>(2) <i>Representation and certifications.</i> Unless a waiver is granted or an exception applies as provided in paragraph (o)(3) of this provision, by submission of its offer, the offeror-</p><p>(i) Represents, to the best of its knowledge and belief, that the offeror does not export any sensitive technology to the government of Iran or any entities or individuals owned or controlled by, or acting on behalf or at the direction of, the government of Iran;</p><p>(ii) Certifies that the offeror, or any person owned or controlled by the offeror, does not engage in any activities for which sanctions may be imposed under section 5 of the Iran Sanctions Act; and</p><p>(iii) Certifies that the offeror, and any person owned or controlled by the offeror, does not knowingly engage in any transaction that exceeds $3,000 with Iran''s Revolutionary Guard Corps or any of its officials, agents, or affiliates, the property and interests in property of which are blocked pursuant to the International Emergency Economic Powers Act (50 U.S.C. 1701 <i>et seq.</i>) (see OFAC''s Specially Designated Nationals and Blocked Persons List at <i>http://www.treasury.gov/ofac/downloads/t11sdn.pdf</i>).</p><p>(3) The representation and certification requirements of paragraph (o)(2) of this provision do not apply if-</p><p>(i) This solicitation includes a trade agreements certification (<i>e.g.,</i> 52.212-3(g) or a comparable agency provision); and</p><p>(ii) The offeror has certified that all the offered products to be supplied are designated country end products.</p>'
--               '<p>The Offeror shall complete only paragraph (b) of this provision if the Offeror has completed the annual representations and certification electronically via the System for Award Management (SAM) Web site accessed through <i>http://www.acquisition.gov.</i> If the Offeror has not completed the annual representations and certifications electronically, the Offeror shall complete only paragraphs (c) through (p) of this provision.</p><p>(a) <i>Definitions.</i> As used in this provision-</p><p><i>Economically disadvantaged women-owned small business (EDWOSB) concern</i> means a small business concern that is at least 51 percent directly and unconditionally owned by, and the management and daily business operations of which are controlled by, one or more women who are citizens of the United States and who are economically disadvantaged in accordance with 13 CFR part 127. It automatically qualifies as a women-owned small business eligible under the WOSB Program.</p><p><i>Forced or indentured child labor</i> means all work or service- </p><p>(1) Exacted from any person under the age of 18 under the menace of any penalty for its nonperformance and for which the worker does not offer himself voluntarily; or </p><p>(2) Performed by any person under the age of 18 pursuant to a contract the enforcement of which can be accomplished by process or penalties.</p><p><i>Highest-level owner</i> means the entity that owns or controls an immediate owner of the offeror, or that owns or controls one or more entities that control an immediate owner of the offeror. No entity owns or exercises control of the highest level owner.</p><p><i>Immediate owner</i> means an entity, other than the offeror, that has direct control of the offeror. Indicators of control include, but are not limited to, one or more of the following: Ownership or interlocking management, identity of interests among family members, shared facilities and equipment, and the common use of employees.</p><p><i>Inverted domestic corporation</i> means a foreign incorporated entity that meets the definition of an inverted domestic corporation under 6 U.S.C. 395(b), applied in accordance with the rules and definitions of 6 U.S.C. 395(c).</p><p><i>Manufactured end product</i> means any end product in Federal Supply Classes (FSC) 1000-9999, except-</p><p>(1) FSC 5510, Lumber and Related Basic Wood Materials;</p><p>(2) Federal Supply Group (FSG) 87, Agricultural Supplies;</p><p>(3) FSG 88, Live Animals;</p><p>(4) FSG 89, Food and Related Consumables;</p><p>(5) FSC 9410, Crude Grades of Plant Materials;</p><p>(6) FSC 9430, Miscellaneous Crude Animal Products, Inedible;</p><p>(7) FSC 9440, Miscellaneous Crude Agricultural and Forestry Products;</p><p>(8) FSC 9610, Ores;</p><p>(9) FSC 9620, Minerals, Natural and Synthetic; and</p><p>(10) FSC 9630, Additive Metal Materials.</p><p><i>Manufactured end product</i> means any end product in product and service codes (PSCs) 1000-9999, except-</p><p>(1) PSC 5510, Lumber and Related Basic Wood Materials;</p><p>(2) Product or Service Group (PSG) 87, Agricultural Supplies;</p><p>(3) PSG 88, Live Animals;</p><p>(4) PSG 89, Subsistence;</p><p>(5) PSC 9410, Crude Grades of Plant Materials;</p><p>(6) PSC 9430, Miscellaneous Crude Animal Products, Inedible;</p><p>(7) PSC 9440, Miscellaneous Crude Agricultural and Forestry Products;</p><p>(8) PSC 9610, Ores;</p><p>(9) PSC 9620, Minerals, Natural and Synthetic; and</p><p>(10) PSC 9630, Additive Metal Materials.</p><p><i>Place of manufacture</i> means the place where an end product is assembled out of components, or otherwise made or processed from raw materials into the finished product that is to be provided to the Government. If a product is disassembled and reassembled, the place of reassembly is not the place of manufacture.</p><p><i>Restricted business operations</i> means business operations in Sudan that include power production activities, mineral extraction activities, oil-related activities, or the production of military equipment, as those terms are defined in the Sudan Accountability and Divestment Act of 2007 (Pub. L. 110-174). Restricted business operations do not include business operations that the person (as that term is defined in Section 2 of the Sudan Accountability and Divestment Act of 2007) conducting the business can demonstrate-</p><p>(1) Are conducted under contract directly and exclusively with the regional government of southern Sudan;</p><p>(2) Are conducted pursuant to specific authorization from the Office of Foreign Assets Control in the Department of the Treasury, or are expressly exempted under Federal law from the requirement to be conducted under such authorization;</p><p>(3) Consist of providing goods or services to marginalized populations of Sudan;</p><p>(4) Consist of providing goods or services to an internationally recognized peacekeeping force or humanitarian organization;</p><p>(5) Consist of providing goods or services that are used only to promote health or education; or</p><p>(6) Have been voluntarily suspended.</p><p><i>Sensitive technology</i>-</p><p>(1) Means hardware, software, telecommunications equipment, or any other technology that is to be used specifically-</p><p>(i) To restrict the free flow of unbiased information in Iran; or</p><p>(ii) To disrupt, monitor, or otherwise restrict speech of the people of Iran; and</p><p>(2) Does not include information or informational materials the export of which the President does not have the authority to regulate or prohibit pursuant to section 203(b)(3) of the International Emergency Economic Powers Act (50 U.S.C. 1702(b)(3)).</p><p><i>Service-disabled veteran-owned small business concern</i>- </p><p>(1) Means a small business concern- </p><p>(i) Not less than 51 percent of which is owned by one or more service-disabled veterans or, in the case of any publicly owned business, not less than 51 percent of the stock of which is owned by one or more service-disabled veterans; and </p><p>(ii) The management and daily business operations of which are controlled by one or more service-disabled veterans or, in the case of a service-disabled veteran with permanent and severe disability, the spouse or permanent caregiver of such veteran. </p><p>(2) <i>Service-disabled veteran</i> means a veteran, as defined in 38 U.S.C. 101(2), with a disability that is service-connected, as defined in 38 U.S.C. 101(16).</p><p><i>Small business concern</i> means a concern, including its affiliates, that is independently owned and operated, not dominant in the field of operation in which it is bidding on Government contracts, and qualified as a small business under the criteria in 13 CFR Part 121 and size standards in this solicitation.</p><p><i>Small disadvantaged business concern,</i> consistent with 13 CFR 124.1002, means a small business concern under the size standard applicable to the acquisition, that-</p><p>(1) Is at least 51 percent unconditionally and directly owned (as defined at 13 CFR 124.105) by-</p><p>(i) One or more socially disadvantaged (as defined at 13 CFR 124.103) and economically disadvantaged (as defined at 13 CFR 124.104) individuals who are citizens of the United States; and</p><p>(ii) Each individual claiming economic disadvantage has a net worth not exceeding $750,000 after taking into account the applicable exclusions set forth at 13 CFR 124.104(c)(2); and</p><p>(2) The management and daily business operations of which are controlled (as defined at 13.CFR 124.106) by individuals, who meet the criteria in paragraphs (1)(i) and (ii) of this definition.</p><p><i>Subsidiary</i> means an entity in which more than 50 percent of the entity is owned-</p><p>(1) Directly by a parent corporation; or</p><p>(2) Through another subsidiary of a parent corporation.</p><p><i>Veteran-owned small business concern</i> means a small business concern- </p><p>(1) Not less than 51 percent of which is owned by one or more veterans (as defined at 38 U.S.C. 101(2)) or, in the case of any publicly owned business, not less than 51 percent of the stock of which is owned by one or more veterans; and</p><p>(2) The management and daily business operations of which are controlled by one or more veterans.</p><p><i>Women-owned business concern</i> means a concern which is at least 51 percent owned by one or more women; or in the case of any publicly owned business, at least 51 percent of its stock is owned by one or more women; and whose management and daily business operations are controlled by one or more women.</p><p><i>Women-owned small business concern</i> means a small business concern-</p><p>(1) That is at least 51 percent owned by one or more women; or, in the case of any publicly owned business, at least 51 percent of the stock of which is owned by one or more women; and</p><p>(2) Whose management and daily business operations are controlled by one or more women.</p><p><i>Women-owned small business (WOSB) concern eligible under the WOSB Program</i> (in accordance with 13 CFR part 127), means a small business concern that is at least 51 percent directly and unconditionally owned by, and the management and daily business operations of which are controlled by, one or more women who are citizens of the United States.</p><p>(b)(1) <i>Annual Representations and Certifications.</i> Any changes provided by the offeror in paragraph (b)(2) of this provision do not automatically change the representations and certifications posted on the SAM website.</p><p>(2) The offeror has completed the annual representations and certifications electronically via the SAM website accessed through <i>http://www.acquisition.gov.</i> After reviewing the SAM database information, the offeror verifies by submission of this offer that the representations and certifications currently posted electronically at FAR 52.212-3, Offeror Representations and Certifications-Commercial Items, have been entered or updated in the last 12 months, are current, accurate, complete, and applicable to this solicitation (including the business size standard applicable to the NAICS code referenced for this solicitation), as of the date of this offer and are incorporated in this offer by reference (see FAR 4.1201), except for paragraphs {{textbox_52.212-3[0]}}.</p><p>[<i>Offeror to identify the applicable paragraphs at (c) through (p) of this provision that the offeror has completed for the purposes of this solicitation only, if any.</i></p><p><i>These amended representation(s) and/or certification(s) are also incorporated in this offer and are current, accurate, and complete as of the date of this offer.</i></p><p><i>Any changes provided by the offeror are applicable to this solicitation only, and do not result in an update to the representations and certifications posted electronically on SAM.</i>]</p><p>(c) Offerors must complete the following representations when the resulting contract will be performed in the United States or its outlying areas. Check all that apply.</p><p>(1) <i>Small business concern.</i> The offeror represents as part of its offer that it {{checkbox_52.212-3[0]}}} is, {{checkbox_52.212-3[1]}}}&nbsp;&nbsp;is not a small business concern.</p><p>(2) <i>Veteran-owned small business concern.</i> [<i>Complete only if the offeror represented itself as a small business concern in paragraph (c)(1) of this provision.</i>] The offeror represents as part of its offer that it {{checkbox_52.212-3[2]}}} is, {{checkbox_52.212-3[3]}}} is not a veteran-owned small business concern. </p><p>(3) <i>Service-disabled veteran-owned small business concern.</i> [<i>Complete only if the offeror represented itself as a veteran-owned small business concern in paragraph (c)(2) of this provision.</i>] The offeror represents as part of its offer that it {{checkbox_52.212-3[4]}}} is, {{checkbox_52.212-3[5]}}} is not a service-disabled veteran-owned small business concern.</p><p>(4) <i>Small disadvantaged business concern. [Complete only if the offeror represented itself as a small business concern in paragraph (c)(1) of this provision.]</i> The offeror represents  that it {{checkbox_52.212-3[6]}}} is, {{checkbox_52.212-3[7]}}} is not a small disadvantaged business concern as defined in 13 CFR 124.1002.</p><p>(5) <i>Women-owned small business concern. [Complete only if the offeror represented itself as a small business concern in paragraph (c)(1) of this provision.]</i> The offeror represents that it {{checkbox_52.212-3[8]}}} is, {{checkbox_52.212-3[9]}}} is not a women-owned small business concern.</p><p>(6) WOSB concern eligible under the WOSB Program. [<i>Complete only if the offeror represented itself as a women-owned small business concern in paragraph (c)(5) of this provision.</i>] The offeror represents that-</p><p>(i) It {{checkbox_52.212-3[10]}}} is, {{checkbox_52.212-3[11]}}} is not a WOSB concern eligible under the WOSB Program, has provided all the required documents to the WOSB Repository, and no change in circumstances or adverse decisions have been issued that affects its eligibility; and</p><p>(ii) It {{checkbox_52.212-3[12]}}} is, {{checkbox_52.212-3[13]}}} is not a joint venture that complies with the requirements of 13 CFR part 127, and the representation in paragraph (c)(6)(i) of this provision is accurate for each WOSB concern eligible under the WOSB Program participating in the joint venture. [<i>The offeror shall enter the name or names of the WOSB concern eligible under the WOSB Program and other small businesses that are participating in the joint venture:</i> {{textbox_52.212-3[1]}}.] Each WOSB concern eligible under the WOSB Program participating in the joint venture shall submit a separate signed copy of the WOSB representation.</p><p>(7) Economically disadvantaged women-owned small business (EDWOSB) concern. [<i>Complete only if the offeror represented itself as a WOSB concern eligible under the WOSB Program in (c)(6) of this provision.</i>] The offeror represents that-</p><p>(i) It {{checkbox_52.212-3[14]}}} is, {{checkbox_52.212-3[15]}}} is not an EDWOSB concern, has provided all the required documents to the WOSB Repository, and no change in circumstances or adverse decisions have been issued that affects its eligibility; and</p><p>(ii) It {{checkbox_52.212-3[16]}}} is, {{checkbox_52.212-3[17]}}} is not a joint venture that complies with the requirements of 13 CFR part 127, and the representation in paragraph (c)(7)(i) of this provision is accurate for each EDWOSB concern participating in the joint venture. [<i>The offeror shall enter the name or names of the EDWOSB concern and other small businesses that are participating in the joint venture:</i> {{textbox_52.212-3[2]}}.] Each EDWOSB concern participating in the joint venture shall submit a separate signed copy of the EDWOSB representation.</p>\n<p>Note to paragraphs (c)(8) and (9): Complete paragraphs (c)(8) and (9) only if this solicitation is expected to exceed the simplified acquisition threshold.</p> <p>(8) <i>Women-owned business concern (other than small business concern). [Complete only if the offeror is a women-owned business concern and did not represent itself as a small business concern in paragraph (c)(1) of this provision.]</i> The offeror represents that it {{checkbox_52.212-3[18]}}} is, a women-owned business concern.</p><p>(9) <i>Tie bid priority for labor surplus area concerns.</i> If this is an invitation for bid, small business offerors may identify the labor surplus areas in which costs to be incurred on account of manufacturing or production (by offeror or first-tier subcontractors) amount to more than 50 percent of the contract price:</p>&nbsp; {{textbox_52.212-3[3]}}<p>(10) <i>HUBZone small business concern.</i> [Complete only if the offeror represented itself as a small business concern in paragraph (c)(1) of this provision.] The offeror represents, as part of its offer, that-</p><p>(i) It {{checkbox_52.212-3[19]}}} is, {{checkbox_52.212-3[20]}}} is not a HUBZone small business concern listed, on the date of this representation, on the List of Qualified HUBZone Small Business Concerns maintained by the Small Business Administration, and no material changes in ownership and control, principal office, or HUBZone employee percentage have occurred since it was certified in accordance with 13 CFR Part 126; and</p><p>(ii) It {{checkbox_52.212-3[21]}}} is, {{checkbox_52.212-3[22]}}} is not a HUBZone joint venture that complies with the requirements of 13 CFR Part 126, and the representation in paragraph (c)(10)(i) of this provision is accurate for each HUBZone small business concern participating in the HUBZone joint venture. [<i>The offeror shall enter the names of each of the HUBZone small business concerns participating in the HUBZone joint venture: {{textbox_52.212-3[4]}}.</i>] Each HUBZone small business concern participating in the HUBZone joint venture shall submit a separate signed copy of the HUBZone representation.</p><p>(d) Representations required to implement provisions of Executive Order 11246-</p><p>(1) Previous contracts and compliance. The offeror represents that-</p><p>(i) It {{checkbox_52.212-3[23]}}} has, {{checkbox_52.212-3[24]}}} has not participated in a previous contract or subcontract subject to the Equal Opportunity clause of this solicitation; and</p><p>(ii) It {{checkbox_52.212-3[25]}}} has, {{checkbox_52.212-3[26]}}} has not filed all required compliance reports.</p><p>(2) Affirmative Action Compliance. The offeror represents that-</p><p>(i) It {{checkbox_52.212-3[27]}}} has developed and has on file, {{checkbox_52.212-3[28]}}}&nbsp;&nbsp;&nbsp;has not developed and does not have on file, at each establishment, affirmative action programs required by rules and regulations of the Secretary of Labor (41 CFR parts 60-1 and 60-2), or</p><p>(ii) It {{checkbox_52.212-3[29]}}}&nbsp;&nbsp;has not previously had contracts subject to the written affirmative action programs requirement of the rules and regulations of the Secretary of Labor.</p><p>(e) <i>Certification Regarding Payments to Influence Federal Transactions (31 U.S.C. 1352).</i> (Applies only if the contract is expected to exceed $150,000.) By submission of its offer, the offeror certifies to the best of its knowledge and belief that no Federal appropriated funds have been paid or will be paid to any person for influencing or attempting to influence an officer or employee of any agency, a Member of Congress, an officer or employee of Congress or an employee of a Member of Congress on his or her behalf in connection with the award of any resultant contract. If any registrants under the Lobbying Disclosure Act of 1995 have made a lobbying contact on behalf of the offeror with respect to this contract, the offeror shall complete and submit, with its offer, OMB Standard Form LLL, Disclosure of Lobbying Activities, to provide the name of the registrants. The offeror need not report regularly employed officers or employees of the offeror to whom payments of reasonable compensation were made.</p><p>(f) <i>Buy American Certificate.</i> (Applies only if the clause at Federal Acquisition Regulation (FAR) 52.225-1, Buy American-Supplies, is included in this solicitation.)</p><p>(1) The offeror certifies that each end product, except those listed in paragraph (f)(2) of this provision, is a domestic end product and that for other than COTS items, the offeror has considered components of unknown origin to have been mined, produced, or manufactured outside the United States. The offeror shall list as foreign end products those end products manufactured in the United States that do not qualify as domestic end products, <i>i.e.</i>, an end product that is not a COTS item and does not meet the component test in paragraph (2) of the definition of âdomestic end product.â The terms âcommercially available off-the-shelf (COTS) item,â âcomponent,â âdomestic end product,â âend product,â âforeign end product,â and âUnited Statesâ are defined in the clause of this solicitation entitled âBuy American-Supplies.â</p><p>(2) Foreign End Products:</p>Line Item No.: {{textbox_52.212-3[5]}}Country of Origin: {{textbox_52.212-3[6]}}<p>(List as necessary)</p><p>(3) The Government will evaluate offers in accordance with the policies and procedures of FAR Part 25.</p><p>(g)(1) <i>Buy American-Free Trade Agreements-Israeli Trade Act Certificate.</i> (Applies only if the clause at FAR 52.225-3, Buy American-Free Trade Agreements-Israeli Trade Act, is included in this solicitation.)</p><p>(i) The offeror certifies that each end product, except those listed in paragraph (g)(1)(ii) or (g)(1)(iii) of this provision, is a domestic end product and that for other than COTS items, the offeror has considered components of unknown origin to have been mined, produced, or manufactured outside the United States. The terms âBahrainian, Moroccan, Omani, Panamanian, or Peruvian end product,â âcommercially available off-the-shelf (COTS) item,â âcomponent,â âdomestic end product,â âend product,â âforeign end product,â âFree Trade Agreement country,â âFree Trade Agreement country end product,â âIsraeli end product,â and âUnited Statesâ are defined in the clause of this solicitation entitled âBuy American-Free Trade Agreements-Israeli Trade Act.â</p><p>(ii) The offeror certifies that the following supplies are Free Trade Agreement country end products (other than Bahrainian, Moroccan, Omani, Panamanian, or Peruvian end products) or Israeli end products as defined in the clause of this solicitation entitled âBuy American-Free Trade Agreements-Israeli Trade Actâ:</p><p>Free Trade Agreement Country End Products (Other than Bahrainian, Moroccan, Omani, Panamanian, or Peruvian End Products) or Israeli End Products:</p>\n<div><div><table border="&quot;1&quot;" cellpadding="&quot;1&quot;" cellspacing="&quot;1&quot;" frame="&quot;void&quot;" width="&quot;100%&quot;"><tbody><tr><th scope="&quot;col&quot;">Line Item No.</th><th scope="&quot;col&quot;">&nbsp;&nbsp;</th><th scope="&quot;col&quot;">Country of Origin</th></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">&nbsp;&nbsp;</td><td align="&quot;right&quot;"></td><td align="&quot;left&quot;"></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">&nbsp;&nbsp;</td><td align="&quot;right&quot;"></td><td align="&quot;left&quot;"></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">&nbsp;&nbsp;</td><td align="&quot;right&quot;"></td><td align="&quot;left&quot;"></td></tr><tr><td align="&quot;center&quot;" colspan="&quot;3&quot;" scope="&quot;row&quot;">[List as necessary]</td></tr></tbody></table></div></div><p>(iii) The offeror shall list those supplies that are foreign end products (other than those listed in paragraph (g)(1)(ii) of this provision) as defined in the clause of this solicitation entitled âBuy American-Free Trade Agreements-Israeli Trade Act.â The offeror shall list as other foreign end products those end products manufactured in the United States that do not qualify as domestic end products, <i>i.e.</i>, an end product that is not a COTS item and does not meet the component test in paragraph (2) of the definition of âdomestic end product.â</p>\n<h2>Other Foreign End Products</h2>\nLine Item No.: {{textbox_52.212-3[7]}}Country of Origin: {{textbox_52.212-3[8]}}<p>(List as necessary)</p><p>(iv) The Government will evaluate offers in accordance with the policies and procedures of FAR Part 25.</p><p>(2) <i>Buy American-Free Trade Agreements-Israeli Trade Act Certificate, Alternate I.</i> If <i>Alternate I</i> to the clause at FAR 52.225-3 is included in this solicitation, substitute the following paragraph (g)(1)(ii) for paragraph (g)(1)(ii) of the basic provision: </p><p>(g)(1)(ii) The offeror certifies that the following supplies are Canadian end products as defined in the clause of this solicitation entitled âBuy American-Free Trade Agreements-Israeli Trade Actâ: </p>\n<h3>Canadian End Products: </h3>\n<h3>Line Item No. </h3>\n&nbsp; {{textbox_52.212-3[9]}}&nbsp; {{textbox_52.212-3[10]}}&nbsp; {{textbox_52.212-3[11]}}<p>$(<i>List as necessary</i>) </p><p>(3) <i>Buy American-Free Trade Agreements-Israeli Trade Act Certificate, Alternate II.</i> If <i>Alternate II</i> to the clause at FAR 52.225-3 is included in this solicitation, substitute the following paragraph (g)(1)(ii) for paragraph (g)(1)(ii) of the basic provision: </p><p>(g)(1)(ii) The offeror certifies that the following supplies are Canadian end products or Israeli end products as defined in the clause of this solicitation entitled âBuy American-Free Trade Agreements-Israeli Trade Actâ: </p>\n<h3>Canadian or Israeli End Products: </h3>\n<p>Line Item No. </p>&nbsp; {{textbox_52.212-3[12]}}&nbsp; {{textbox_52.212-3[13]}}&nbsp; {{textbox_52.212-3[14]}}\n<p>Country of Origin </p>&nbsp; {{textbox_52.212-3[15]}}&nbsp; {{textbox_52.212-3[16]}}&nbsp; {{textbox_52.212-3[17]}}<p>$(<i>List as necessary</i>)</p><p>(g)(4) <i>Buy American-Free Trade Agreements-Israeli Trade Act Certificate, Alternate III.</i> If Alternate III to the clause at FAR 52.225-3 is included in this solicitation, substitute the following paragraph (g)(1)(ii) for paragraph (g)(1)(ii) of the basic provision:</p><p>(g)(1)(ii) The offeror certifies that the following supplies are Free Trade Agreement country end products (other than Bahrainian, Korean, Moroccan, Omani, Panamanian, or Peruvian end products) or Israeli end products as defined in the clause of this solicitation entitled âBuy American-Free Trade Agreements-Israeli Trade Actâ:</p><p>Free Trade Agreement Country End Products (Other than Bahrainian, Korean, Moroccan, Omani, Panamanian, or Peruvian End Products) or Israeli End Products:</p>\n<div><div><table border="&quot;1&quot;" cellpadding="&quot;1&quot;" cellspacing="&quot;1&quot;" frame="&quot;void&quot;" width="&quot;100%&quot;"><tbody><tr><th scope="&quot;col&quot;">Line Item No.</th><th scope="&quot;col&quot;">&nbsp;&nbsp;</th><th scope="&quot;col&quot;">Country of Origin</th></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">&nbsp;&nbsp;</td><td align="&quot;right&quot;"></td><td align="&quot;left&quot;"></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">&nbsp;&nbsp;</td><td align="&quot;right&quot;"></td><td align="&quot;left&quot;"></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">&nbsp;&nbsp;</td><td align="&quot;right&quot;"></td><td align="&quot;left&quot;"></td></tr><tr><td align="&quot;center&quot;" colspan="&quot;3&quot;" scope="&quot;row&quot;">[List as necessary]</td></tr></tbody></table></div></div><p>(5) <i>Trade Agreements Certificate.</i> (Applies only if the clause at FAR 52.225-5, Trade Agreements, is included in this solicitation.)</p><p>(i) The offeror certifies that each end product, except those listed in paragraph (g)(5)(ii) of this provision, is a U.S.-made or designated country end product, as defined in the clause of this solicitation entitled âTrade Agreementsâ.</p><p>(ii) The offeror shall list as other end products those end products that are not U.S.-made or designated country end products.</p><p>Other End Products:</p>\n<div><div><table border="&quot;1&quot;" cellpadding="&quot;1&quot;" cellspacing="&quot;1&quot;" frame="&quot;void&quot;" width="&quot;100%&quot;"><tbody><tr><th scope="&quot;col&quot;">Line item No.</th><th scope="&quot;col&quot;">&nbsp;&nbsp;</th><th scope="&quot;col&quot;">Country of origin</th></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">&nbsp;&nbsp;</td><td align="&quot;right&quot;"></td><td align="&quot;left&quot;"></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">&nbsp;&nbsp;</td><td align="&quot;right&quot;"></td><td align="&quot;left&quot;"></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">&nbsp;&nbsp;</td><td align="&quot;right&quot;"></td><td align="&quot;left&quot;"></td></tr><tr><td align="&quot;center&quot;" colspan="&quot;3&quot;" scope="&quot;row&quot;">[List as necessary]</td></tr></tbody></table></div></div><p>(iii) The Government will evaluate offers in accordance with the policies and procedures of FAR Part 25. For line items covered by the WTO GPA, the Government will evaluate offers of U.S.-made or designated country end products without regard to the restrictions of the Buy American statute. The Government will consider for award only offers of U.S.-made or designated country end products unless the Contracting Officer determines that there are no offers for such products or that the offers for such products are insufficient to fulfill the requirements of the solicitation.</p><p>(h) <i>Certification Regarding Responsibility Matters (Executive Order 12689).</i> (Applies only if the contract value is expected to exceed the simplified acquisition threshold.) The offeror certifies, to the best of its knowledge and belief, that the offeror and/or any of its principals- </p><p>(1) {{checkbox_52.212-3[30]}}} Are, {{checkbox_52.212-3[31]}}} are not presently debarred, suspended, proposed for debarment, or declared ineligible for the award of contracts by any Federal agency;</p><p>(2) {{checkbox_52.212-3[32]}}} Have, {{checkbox_52.212-3[33]}}} have not, within a three-year period preceding this offer, been convicted of or had a civil judgment rendered against them for: Commission of fraud or a criminal offense in connection with obtaining, attempting to obtain, or performing a Federal, state or local government contract or subcontract; violation of Federal or state antitrust statutes relating to the submission of offers; or Commission of embezzlement, theft, forgery, bribery, falsification or destruction of records, making false statements, tax evasion, violating Federal criminal tax laws, or receiving stolen property,</p><p>(3) {{checkbox_52.212-3[34]}}} Are, {{checkbox_52.212-3[35]}}} are not presently indicted for, or otherwise criminally or civilly charged by a Government entity with, commission of any of these offenses enumerated in paragraph (h)(2) of this clause; and</p><p>(i) <i>Certification Regarding Knowledge of Child Labor for Listed End Products (Executive Order 13126). [The Contracting Officer must list in paragraph (i)(1) any end products being acquired under this solicitation that are included in the List of Products Requiring Contractor Certification as to Forced or Indentured Child Labor, unless excluded at 22.1503(b).</i>] </p><p>(1) <i>Listed end products.</i> </p>\n<h3>Listed End Product</h3>\n&nbsp; {{textbox_52.212-3[18]}}&nbsp;  {{textbox_52.212-3[19]}}\n<h3>Listed Countries of Origin </h3>\n&nbsp; {{textbox_52.212-3[20]}}&nbsp; {{textbox_52.212-3[21]}}<p>(2) <i>Certification.</i> [<i>If the Contracting Officer has identified end products and countries of origin in paragraph (i)(1) of this provision, then the offeror must certify to either (i)(2)(i) or (i)(2)(ii) by checking the appropriate block.</i>]</p><p>{{checkbox_52.212-3[36]}}}&nbsp;&nbsp;(i) The offeror will not supply any end product listed in paragraph (i)(1) of this provision that was mined, produced, or manufactured in the corresponding country as listed for that product.</p><p>{{checkbox_52.212-3[37]}}}&nbsp;&nbsp;(ii) The offeror may supply an end product listed in paragraph (i)(1) of this provision that was mined, produced, or manufactured in the corresponding country as listed for that product. The offeror certifies that it has made a good faith effort to determine whether forced or indentured child labor was used to mine, produce, or manufacture any such end product furnished under this contract. On the basis of those efforts, the offeror certifies that it is not aware of any such use of child labor.</p><p>(4) Have,{{checkbox_52.212-3[38]}}}&nbsp;&nbsp; have not, within a three-year period preceding this offer, been notified of any delinquent Federal taxes in an amount that exceeds $3,000 for which the liability remains unsatisfied.</p><p>(i) Taxes are considered delinquent if both of the following criteria apply:</p><p>(A) <i>The tax liability is finally determined.</i> The liability is finally determined if it has been assessed. A liability is not finally determined if there is a pending administrative or judicial challenge. In the case of a judicial challenge to the liability, the liability is not finally determined until all judicial appeal rights have been exhausted.</p><p>(B) <i>The taxpayer is delinquent in making payment.</i> A taxpayer is delinquent if the taxpayer has failed to pay the tax liability when full payment was due and required. A taxpayer is not delinquent in cases where enforced collection action is precluded.</p><p>(ii) <i>Examples.</i> (A) The taxpayer has received a statutory notice of deficiency, under I.R.C. §6212, which entitles the taxpayer to seek Tax Court review of a proposed tax deficiency. This is not a delinquent tax because it is not a final tax liability. Should the taxpayer seek Tax Court review, this will not be a final tax liability until the taxpayer has exercised all judicial appeal rights.</p><p>(B) The IRS has filed a notice of Federal tax lien with respect to an assessed tax liability, and the taxpayer has been issued a notice under I.R.C. §6320 entitling the taxpayer to request a hearing with the IRS Office of Appeals contesting the lien filing, and to further appeal to the Tax Court if the IRS determines to sustain the lien filing. In the course of the hearing, the taxpayer is entitled to contest the underlying tax liability because the taxpayer has had no prior opportunity to contest the liability. This is not a delinquent tax because it is not a final tax liability. Should the taxpayer seek tax court review, this will not be a final tax liability until the taxpayer has exercised all judicial appeal rights.</p><p>(C) The taxpayer has entered into an installment agreement pursuant to I.R.C. §6159. The taxpayer is making timely payments and is in full compliance with the agreement terms. The taxpayer is not delinquent because the taxpayer is not currently required to make full payment.</p><p>(D) The taxpayer has filed for bankruptcy protection. The taxpayer is not delinquent because enforced collection action is stayed under 11 U.S.C. 362 (the Bankruptcy Code).</p><p>(i) <i>Certification Regarding Knowledge of Child Labor for Listed End Products (Executive Order 13126). [The Contracting Officer must list in paragraph (i)(1) any end products being acquired under this solicitation that are included in the List of Products Requiring Contractor Certification as to Forced or Indentured Child Labor, unless excluded at 22.1503(b).</i>] </p><p>(1) <i>Listed end products.</i> </p>\n<h3>Listed End Product</h3>\n&nbsp; {{textbox_52.212-3[22]}}&nbsp;  {{textbox_52.212-3[23]}}\n<h3>Listed Countries of Origin </h3>\n&nbsp; {{textbox_52.212-3[24]}}&nbsp; {{textbox_52.212-3[25]}}<p>(2) <i>Certification.</i> [<i>If the Contracting Officer has identified end products and countries of origin in paragraph (i)(1) of this provision, then the offeror must certify to either (i)(2)(i) or (i)(2)(ii) by checking the appropriate block.</i>]</p><p>{{checkbox_52.212-3[39]}}}&nbsp;&nbsp;(i) The offeror will not supply any end product listed in paragraph (i)(1) of this provision that was mined, produced, or manufactured in the corresponding country as listed for that product.</p><p>{{checkbox_52.212-3[40]}}}&nbsp;&nbsp;(ii) The offeror may supply an end product listed in paragraph (i)(1) of this provision that was mined, produced, or manufactured in the corresponding country as listed for that product. The offeror certifies that it has made a good faith effort to determine whether forced or indentured child labor was used to mine, produce, or manufacture any such end product furnished under this contract. On the basis of those efforts, the offeror certifies that it is not aware of any such use of child labor.</p><p>(j) <i>Place of manufacture.</i> (Does not apply unless the solicitation is predominantly for the acquisition of manufactured end products.) For statistical purposes only, the offeror shall indicate whether the place of manufacture of the end products it expects to provide in response to this solicitation is predominantly-</p><p>(1) {{checkbox_52.212-3[41]}}} In the United States (Check this box if the total anticipated price of offered end products manufactured in the United States exceeds the total anticipated price of offered end products manufactured outside the United States); or</p><p>(2) {{checkbox_52.212-3[42]}}} Outside the United States.</p><p>(k) <i>Certificates regarding exemptions from the application of the Service Contract Labor Standards.</i> (Certification by the offeror as to its compliance with respect to the contract also constitutes its certification as to compliance by its subcontractor if it subcontracts out the exempt services.) [<i>The contracting officer is to check a box to indicate if paragraph (k)(1) or (k)(2) applies.</i>]</p><p>(1){{checkbox_52.212-3[43]}}}&nbsp;&nbsp; Maintenance, calibration, or repair of certain equipment as described in FAR 22.1003-4(c)(1). The offeror {{checkbox_52.212-3[44]}}}&nbsp;&nbsp;does {{checkbox_52.212-3[45]}}}&nbsp;&nbsp; does not certify that-</p><p>(i) The items of equipment to be serviced under this contract are used regularly for other than Governmental purposes and are sold or traded by the offeror (or subcontractor in the case of an exempt subcontract) in substantial quantities to the general public in the course of normal business operations;</p><p>(ii) The services will be furnished at prices which are, or are based on, established catalog or market prices (see FAR 22.1003-4(c)(2)(ii)) for the maintenance, calibration, or repair of such equipment; and</p><p>(iii) The compensation (wage and fringe benefits) plan for all service employees performing work under the contract will be the same as that used for these employees and equivalent employees servicing the same equipment of commercial customers.</p><p>(2){{checkbox_52.212-3[46]}}}&nbsp;&nbsp;Certain services as described in FAR 22.1003-4(d)(1). The offeror {{checkbox_52.212-3[47]}}}&nbsp;&nbsp;does {{checkbox_52.212-3[48]}}}&nbsp;&nbsp;does not certify that-</p><p>(i) The services under the contract are offered and sold regularly to non-Governmental customers, and are provided by the offeror (or subcontractor in the case of an exempt subcontract) to the general public in substantial quantities in the course of normal business operations;</p><p>(ii) The contract services will be furnished at prices that are, or are based on, established catalog or market prices (see FAR 22.1003-4(d)(2)(iii));</p><p>(iii) Each service employee who will perform the services under the contract will spend only a small portion of his or her time (a monthly average of less than 20 percent of the available hours on an annualized basis, or less than 20 percent of available hours during the contract period if the contract period is less than a month) servicing the Government contract; and</p><p>(iv) The compensation (wage and fringe benefits) plan for all service employees performing work under the contract is the same as that used for these employees and equivalent employees servicing commercial customers.</p><p>(3) If paragraph (k)(1) or (k)(2) of this clause applies-</p><p>(i) If the offeror does not certify to the conditions in paragraph (k)(1) or (k)(2) and the Contracting Officer did not attach a Service Contract Labor Standards wage determination to the solicitation, the offeror shall notify the Contracting Officer as soon as possible; and</p><p>(ii) The Contracting Officer may not make an award to the offeror if the offeror fails to execute the certification in paragraph (k)(1) or (k)(2) of this clause or to contact the Contracting Officer as required in paragraph (k)(3)(i) of this clause.</p><p>(l) <i>Taxpayer Identification Number (TIN) (26 U.S.C. 6109, 31 U.S.C. 7701).</i> (Not applicable if the offeror is required to provide this information to the SAM database to be eligible for award.)</p><p>(1) All offerors must submit the information required in paragraphs (l)(3) through (l)(5) of this provision to comply with debt collection requirements of 31 U.S.C. 7701(c) and 3325(d), reporting requirements of 26 U.S.C. 6041, 6041A, and 6050M, and implementing regulations issued by the Internal Revenue Service (IRS).</p><p>(2) The TIN may be used by the Government to collect and report on any delinquent amounts arising out of the offeror''s relationship with the Government (31 U.S.C. 7701(c)(3)). If the resulting contract is subject to the payment reporting requirements described in FAR 4.904, the TIN provided hereunder may be matched with IRS records to verify the accuracy of the offeror''s TIN.</p><p>(3) <i>Taxpayer Identification Number (TIN).</i></p><p>{{checkbox_52.212-3[49]}}}&nbsp;&nbsp;TIN: {{textbox_52.212-3[26]}}.</p><p>{{checkbox_52.212-3[50]}}}&nbsp;&nbsp;TIN has been applied for.</p><p>{{checkbox_52.212-3[51]}}}&nbsp;&nbsp;TIN is not required because:</p><p>{{checkbox_52.212-3[52]}}}&nbsp;&nbsp;Offeror is a nonresident alien, foreign corporation, or foreign partnership that does not have income effectively connected with the conduct of a trade or business in the United States and does not have an office or place of business or a fiscal paying agent in the United States;</p><p>{{checkbox_52.212-3[53]}}}&nbsp;&nbsp;Offeror is an agency or instrumentality of a foreign government;</p><p>{{checkbox_52.212-3[54]}}}&nbsp;&nbsp;Offeror is an agency or instrumentality of the Federal Government.</p><p>(4) <i>Type of organization.</i></p><p>{{checkbox_52.212-3[55]}}}&nbsp;&nbsp;Sole proprietorship;</p><p>{{checkbox_52.212-3[56]}}}&nbsp;&nbsp;Partnership;</p><p>{{checkbox_52.212-3[57]}}}&nbsp;&nbsp;Corporate entity (not tax-exempt);</p><p>{{checkbox_52.212-3[58]}}}&nbsp;&nbsp;Corporate entity (tax-exempt);</p><p>{{checkbox_52.212-3[59]}}}&nbsp;&nbsp;Government entity (Federal, State, or local);</p><p>{{checkbox_52.212-3[60]}}}&nbsp;&nbsp;Foreign government;</p><p>{{checkbox_52.212-3[61]}}}&nbsp;&nbsp;International organization per 26 CFR 1.6049-4;</p><p>{{checkbox_52.212-3[62]}}}&nbsp;&nbsp;Other {{textbox_52.212-3[27]}}.</p><p>(5) <i>Common parent.</i></p><p>{{checkbox_52.212-3[63]}}}&nbsp;&nbsp;Offeror is not owned or controlled by a common parent;</p><p>{{checkbox_52.212-3[64]}}}&nbsp;&nbsp;Name and TIN of common parent:</p><p>Name {{textbox_52.212-3[28]}}.</p><p>TIN {{textbox_52.212-3[29]}}.</p><p>(m) <i>Restricted business operations in Sudan.</i> By submission of its offer, the offeror certifies that the offeror does not conduct any restricted business operations in Sudan.</p><p>(n) <i>Prohibition on Contracting with Inverted Domestic Corporations.</i> (1) Government agencies are not permitted to use appropriated (or otherwise made available) funds for contracts with either an inverted domestic corporation, or a subsidiary of an inverted domestic corporation, unless the exception at 9.108-2(b) applies or the requirement is waived in accordance with the procedures at 9.108-4.</p><p>(2) <i>Representation.</i> By submission of its offer, the offeror represents that-</p><p>(i) It is not an inverted domestic corporation; and</p><p>(ii) It is not a subsidiary of an inverted domestic corporation.</p><p>(o) <i>Prohibition on contracting with entities engaging in certain activities or transactions relating to Iran.</i> (1) The offeror shall email questions concerning sensitive technology to the Department of State at CISADA106@state.gov.</p><p>(2) <i>Representation and certifications.</i> Unless a waiver is granted or an exception applies as provided in paragraph (o)(3) of this provision, by submission of its offer, the offeror-</p><p>(i) Represents, to the best of its knowledge and belief, that the offeror does not export any sensitive technology to the government of Iran or any entities or individuals owned or controlled by, or acting on behalf or at the direction of, the government of Iran;</p><p>(ii) Certifies that the offeror, or any person owned or controlled by the offeror, does not engage in any activities for which sanctions may be imposed under section 5 of the Iran Sanctions Act; and</p><p>(iii) Certifies that the offeror, and any person owned or controlled by the offeror, does not knowingly engage in any transaction that exceeds $3,000 with Iran''s Revolutionary Guard Corps or any of its officials, agents, or affiliates, the property and interests in property of which are blocked pursuant to the International Emergency Economic Powers Act (50 U.S.C. 1701 <i>et seq.</i>) (see OFAC''s Specially Designated Nationals and Blocked Persons List at <i>http://www.treasury.gov/ofac/downloads/t11sdn.pdf</i>).</p><p>(3) The representation and certification requirements of paragraph (o)(2) of this provision do not apply if-</p><p>(i) This solicitation includes a trade agreements certification (<i>e.g.,</i> 52.212-3(g) or a comparable agency provision); and</p><p>(ii) The offeror has certified that all the offered products to be supplied are designated country end products.</p>'
WHERE clause_id = 92551; -- 52.212-3
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (92551, 'textbox_52.212-3[30]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (92551, 'textbox_52.212-3[31]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (92551, 'textbox_52.212-3[32]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (92551, 'textbox_52.212-3[33]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (92551, 'textbox_52.212-3[34]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (92551, 'textbox_52.212-3[35]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (92551, 'textbox_52.212-3[36]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (92551, 'textbox_52.212-3[37]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (92551, 'textbox_52.212-3[38]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (92551, 'textbox_52.212-3[39]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (92551, 'textbox_52.212-3[40]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (92551, 'textbox_52.212-3[41]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (92551, 'textbox_52.212-3[42]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (92551, 'textbox_52.212-3[43]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (92551, 'textbox_52.212-3[44]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (92551, 'textbox_52.212-3[45]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (92551, 'textbox_52.212-3[46]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (92551, 'textbox_52.212-3[47]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (92551, 'textbox_52.212-3[48]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (92551, 'textbox_52.212-3[49]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (92551, 'textbox_52.212-3[50]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (92551, 'textbox_52.212-3[51]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (92551, 'textbox_52.212-3[52]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (92551, 'textbox_52.212-3[53]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (92551, 'textbox_52.212-3[54]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (92551, 'textbox_52.212-3[55]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (92551, 'textbox_52.212-3[56]', 'S', 100);

UPDATE Clauses
SET effective_date = '2006-05-01' -- '2006-01-01'
WHERE clause_id = 92557; -- 52.213-1

UPDATE Clauses
SET effective_date = '1984-04-01' -- '1984-01-01'
WHERE clause_id = 92558; -- 52.213-2

UPDATE Clauses
SET effective_date = '1984-04-01' -- '1984-01-01'
WHERE clause_id = 92559; -- 52.213-3

UPDATE Clauses
SET effective_date = '1990-07-01' -- '1990-01-01'
WHERE clause_id = 92561; -- 52.214-10

UPDATE Clauses
SET effective_date = '1984-04-01' -- '1984-01-01'
WHERE clause_id = 92562; -- 52.214-12

UPDATE Clauses
SET effective_date = '1984-04-01' -- '1984-01-01'
WHERE clause_id = 92564; -- 52.214-13

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1214_614&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?node=se48.2.52_1214_614&amp;rgn=div8'
, effective_date = '1985-04-01' -- '1985-01-01'
, clause_data = '<p>(a) The bidder, in the performance of any contract resulting from this solicitation, &#x25a1; intends, &#x25a1; does not intend [<i>check applicable box</i>] to use one or more plants or facilities located at a different address from the address of the bidder as indicated in this bid.</p><p>(b) If the bidder checks <i>intends</i> in paragraph (a) above, it shall insert in the spaces provided below the required information:</p><div><table border="1" cellpadding="1" cellspacing="1" frame="void" width="100%"><tbody><tr><th scope="col">Place of Performance (Street Address, City, County, State, Zip Code)</th><th scope="col">Name and Address of Owner and Operator of the Plant or Facility if Other than Bidder</th><th scope="col">&nbsp;&nbsp;</th></tr><tr><td align="left" scope="row">&nbsp;&nbsp;&nbsp;</td><td align="left"></td><td align="left"></td></tr><tr><td align="left" scope="row">&nbsp;&nbsp;&nbsp;</td><td align="left"></td><td align="left"></td></tr><tr><td align="left" scope="row">&nbsp;&nbsp;&nbsp;</td><td align="left"></td><td align="left"></td></tr></tbody></table></div>'
--               '<p>(a) The bidder, in the performance of any contract resulting from this solicitation, {{checkbox_52.214-14[0]}}} intends, {{checkbox_52.214-14[1]}}} does not intend [<i>check applicable box</i>] to use one or more plants or facilities located at a different address from the address of the bidder as indicated in this bid.</p><p>(b) If the bidder checks <i>intends</i> in paragraph (a) above, it shall insert in the spaces provided below the required information:</p><div><table border="&quot;1&quot;" cellpadding="&quot;1&quot;" cellspacing="&quot;1&quot;" frame="&quot;void&quot;" width="&quot;100%&quot;"><tbody><tr><th scope="&quot;col&quot;">Place of Performance (Street Address, City, County, State, Zip Code)</th><th scope="&quot;col&quot;">Name and Address of Owner and Operator of the Plant or Facility if Other than Bidder</th><th scope="&quot;col&quot;">&nbsp;&nbsp;</th></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">&nbsp;&nbsp;&nbsp;</td><td align="&quot;left&quot;"></td><td align="&quot;left&quot;"></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">&nbsp;&nbsp;&nbsp;</td><td align="&quot;left&quot;"></td><td align="&quot;left&quot;"></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">&nbsp;&nbsp;&nbsp;</td><td align="&quot;left&quot;"></td><td align="&quot;left&quot;"></td></tr></tbody></table></div>'
WHERE clause_id = 92565; -- 52.214-14

UPDATE Clauses
SET effective_date = '1984-04-01' -- '1984-01-01'
WHERE clause_id = 92566; -- 52.214-15

UPDATE Clauses
SET effective_date = '1984-04-01' -- '1984-01-01'
WHERE clause_id = 92567; -- 52.214-16

UPDATE Clauses
SET effective_date = '1984-04-01' -- '1984-01-01'
WHERE clause_id = 92568; -- 52.214-18

UPDATE Clauses
SET effective_date = '1996-08-01' -- '1996-01-01'
WHERE clause_id = 92569; -- 52.214-19

UPDATE Clauses
SET effective_date = '2002-04-01' -- '2002-01-01'
WHERE clause_id = 92572; -- 52.214-20

UPDATE Clauses
SET effective_date = '2002-04-01' -- '2002-01-01'
WHERE clause_id = 92574; -- 52.214-21

UPDATE Clauses
SET effective_date = '1990-03-01' -- '1990-01-01'
WHERE clause_id = 92575; -- 52.214-22

UPDATE Clauses
SET effective_date = '1999-11-01' -- '1999-01-01'
WHERE clause_id = 92576; -- 52.214-23

UPDATE Clauses
SET effective_date = '1984-04-01' -- '1984-01-01'
WHERE clause_id = 92577; -- 52.214-24

UPDATE Clauses
SET effective_date = '1985-04-01' -- '1985-01-01'
WHERE clause_id = 92578; -- 52.214-25

UPDATE Clauses
SET effective_date = '2010-10-01' -- '2010-01-01'
WHERE clause_id = 92580; -- 52.214-26

UPDATE Clauses
SET effective_date = '2011-08-01' -- '2011-01-01'
WHERE clause_id = 92581; -- 52.214-27

UPDATE Clauses
SET effective_date = '2010-10-01' -- '2010-01-01'
WHERE clause_id = 92582; -- 52.214-28

UPDATE Clauses
SET effective_date = '1989-12-01' -- '1989-01-01'
WHERE clause_id = 92584; -- 52.214-3

UPDATE Clauses
SET effective_date = '1989-12-01' -- '1989-01-01'
WHERE clause_id = 92585; -- 52.214-31

UPDATE Clauses
SET effective_date = '1991-04-01' -- '1991-01-01'
WHERE clause_id = 92586; -- 52.214-34

UPDATE Clauses
SET effective_date = '1991-04-01' -- '1991-01-01'
WHERE clause_id = 92587; -- 52.214-35

UPDATE Clauses
SET effective_date = '1984-04-01' -- '1984-01-01'
WHERE clause_id = 92588; -- 52.214-4

UPDATE Clauses
SET effective_date = '1997-03-01' -- '1997-01-01'
WHERE clause_id = 92589; -- 52.214-5

UPDATE Clauses
SET effective_date = '1984-04-01' -- '1984-01-01'
WHERE clause_id = 92590; -- 52.214-6

UPDATE Clauses
SET effective_date = '1999-11-01' -- '1999-01-01'
WHERE clause_id = 92591; -- 52.214-7

UPDATE Clauses
SET effective_date = '2011-08-01' -- '2011-01-01'
WHERE clause_id = 92595; -- 52.215-10

UPDATE Clauses
SET effective_date = '2011-08-01' -- '2011-01-01'
WHERE clause_id = 92596; -- 52.215-11

UPDATE Clauses
SET effective_date = '2010-10-01' -- '2010-01-01'
WHERE clause_id = 92597; -- 52.215-12

UPDATE Clauses
SET effective_date = '2010-10-01' -- '2010-01-01'
WHERE clause_id = 92598; -- 52.215-13

UPDATE Clauses
SET effective_date = '2010-10-01' -- '2010-01-01'
WHERE clause_id = 92600; -- 52.215-14

UPDATE Clauses
SET effective_date = '2010-10-01' -- '2010-01-01'
WHERE clause_id = 92601; -- 52.215-15

UPDATE Clauses
SET effective_date = '2003-06-01' -- '2003-01-01'
WHERE clause_id = 92602; -- 52.215-16

UPDATE Clauses
SET effective_date = '1997-10-01' -- '1997-01-01'
WHERE clause_id = 92603; -- 52.215-17

UPDATE Clauses
SET effective_date = '2005-07-01' -- '2005-01-01'
WHERE clause_id = 92604; -- 52.215-18

UPDATE Clauses
SET effective_date = '1997-10-01' -- '1997-01-01'
WHERE clause_id = 92605; -- 52.215-19

UPDATE Clauses
SET effective_date = '2010-10-01' -- '2010-01-01'
WHERE clause_id = 92609; -- 52.215-2

UPDATE Clauses
SET effective_date = '2010-10-01' -- '2010-01-01'
WHERE clause_id = 92614; -- 52.215-20

UPDATE Clauses
SET effective_date = '2010-10-01' -- '2010-01-01'
WHERE clause_id = 92619; -- 52.215-21

UPDATE Clauses
SET effective_date = '2009-10-01' -- '2009-01-01'
WHERE clause_id = 92620; -- 52.215-22

UPDATE Clauses
SET effective_date = '2009-10-01' -- '2009-01-01'
WHERE clause_id = 92622; -- 52.215-23

UPDATE Clauses
SET effective_date = '1997-10-01' -- '1997-01-01'
WHERE clause_id = 92623; -- 52.215-3

UPDATE Clauses
SET effective_date = '1997-10-01' -- '1997-01-01'
WHERE clause_id = 92624; -- 52.215-5

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1215_66&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?node=se48.2.52_1215_66&amp;rgn=div8'
, effective_date = '1997-10-01' -- '1997-01-01'
, clause_data = '<p>(a) The offeror or respondent, in the performance of any contract resulting from this solicitation, &#x25a1;&nbsp;&nbsp;intends, &#x25a1;&nbsp;&nbsp;does not intend [<i>check applicable block</i>] to use one or more plants or facilities located at a different address from the address of the offeror or respondent as indicated in this proposal or response to request for information.</p><p>(b) If the offeror or respondent checks ''intends'' in paragraph (a) of this provision, it shall insert in the following spaces the required information:</p><div><table border="1" cellpadding="1" cellspacing="1" frame="void" width="100%"><tbody><tr><th scope="col">Place of performance (street address, city, state, county, zip code)</th><th scope="col">Name and address of owner and operator of the plant or facility if other than offeror or respondent</th></tr><tr><td align="right" scope="row">&nbsp;&nbsp;&nbsp;</td><td align="right"></td></tr><tr><td align="right" scope="row">&nbsp;&nbsp;&nbsp;</td><td align="right"></td></tr></tbody></table></div>'
--               '<p>(a) The offeror or respondent, in the performance of any contract resulting from this solicitation, {{checkbox_52.215-6[0]}}}&nbsp;&nbsp;intends, {{checkbox_52.215-6[1]}}}&nbsp;&nbsp;does not intend [<i>check applicable block</i>] to use one or more plants or facilities located at a different address from the address of the offeror or respondent as indicated in this proposal or response to request for information.</p><p>(b) If the offeror or respondent checks ''??intends''? in paragraph (a) of this provision, it shall insert in the following spaces the required information:</p><div><table border="&quot;1&quot;" cellpadding="&quot;1&quot;" cellspacing="&quot;1&quot;" frame="&quot;void&quot;" width="&quot;100%&quot;"><tbody><tr><th scope="&quot;col&quot;">Place of performance (street address, city, state, county, zip code)</th><th scope="&quot;col&quot;">Name and address of owner and operator of the plant or facility if other than offeror or respondent</th></tr><tr><td align="&quot;right&quot;" scope="&quot;row&quot;">&nbsp;&nbsp;&nbsp;</td><td align="&quot;right&quot;"></td></tr><tr><td align="&quot;right&quot;" scope="&quot;row&quot;">&nbsp;&nbsp;&nbsp;</td><td align="&quot;right&quot;"></td></tr></tbody></table></div>'
WHERE clause_id = 92625; -- 52.215-6

UPDATE Clauses
SET effective_date = '1997-10-01' -- '1997-01-01'
WHERE clause_id = 92626; -- 52.215-8

UPDATE Clauses
SET effective_date = '1997-10-01' -- '1997-01-01'
WHERE clause_id = 92629; -- 52.215-9

UPDATE Clauses
SET effective_date = '1984-04-01' -- NULL
WHERE clause_id = 92630; -- 52.216-1

UPDATE Clauses
SET effective_date = '2011-06-01' -- '2011-01-01'
WHERE clause_id = 92631; -- 52.216-10

UPDATE Clauses
SET effective_date = '1984-04-01' -- '1984-01-01'
WHERE clause_id = 92633; -- 52.216-11

UPDATE Clauses
SET effective_date = '1984-04-01' -- '1984-01-01'
WHERE clause_id = 92635; -- 52.216-12

UPDATE Clauses
SET effective_date = '1998-04-01' -- '1998-01-01'
WHERE clause_id = 92636; -- 52.216-15

UPDATE Clauses
SET effective_date = '1997-10-01' -- '1997-01-01'
WHERE clause_id = 92638; -- 52.216-16

UPDATE Clauses
SET effective_date = '1997-10-01' -- '1997-01-01'
WHERE clause_id = 92640; -- 52.216-17

UPDATE Clauses
SET effective_date = '1995-10-01' -- '1995-01-01'
WHERE clause_id = 92641; -- 52.216-18

UPDATE Clauses
SET effective_date = '1995-10-01' -- '1995-01-01'
WHERE clause_id = 92642; -- 52.216-19

UPDATE Clauses
SET effective_date = '1995-10-01' -- '1995-01-01'
WHERE clause_id = 92644; -- 52.216-20

UPDATE Clauses
SET effective_date = '1995-10-01' -- '1995-01-01'
WHERE clause_id = 92649; -- 52.216-21

UPDATE Clauses
SET effective_date = '1995-10-01' -- '1995-01-01'
WHERE clause_id = 92650; -- 52.216-22

UPDATE Clauses
SET effective_date = '1984-04-01' -- '1984-01-01'
WHERE clause_id = 92651; -- 52.216-23

UPDATE Clauses
SET effective_date = '1984-04-01' -- '1984-01-01'
WHERE clause_id = 92652; -- 52.216-24

UPDATE Clauses
SET effective_date = '2010-10-01' -- '2010-01-01'
WHERE clause_id = 92654; -- 52.216-25

UPDATE Clauses
SET effective_date = '2002-12-01' -- '2002-01-01'
WHERE clause_id = 92655; -- 52.216-26

UPDATE Clauses
SET effective_date = '1995-10-01' -- '1995-01-01'
WHERE clause_id = 92656; -- 52.216-27

UPDATE Clauses
SET effective_date = '1995-10-01' -- '1995-01-01'
WHERE clause_id = 92657; -- 52.216-28

UPDATE Clauses
SET effective_date = '2007-02-01' -- '2007-01-01'
WHERE clause_id = 92658; -- 52.216-29

UPDATE Clauses
SET effective_date = '2007-02-01' -- '2007-01-01'
WHERE clause_id = 92660; -- 52.216-30

UPDATE Clauses
SET effective_date = '2007-02-01' -- '2007-01-01'
WHERE clause_id = 92661; -- 52.216-31

UPDATE Clauses
SET effective_date = '1997-10-01' -- '1997-01-01'
WHERE clause_id = 92663; -- 52.216-5

UPDATE Clauses
SET effective_date = '1997-10-01' -- '1997-01-01'
WHERE clause_id = 92664; -- 52.216-6

UPDATE Clauses
SET effective_date = '2013-06-01' -- '2013-01-01'
WHERE clause_id = 92665; -- 52.216-7

UPDATE Clauses
SET effective_date = '2011-06-01' -- '2011-01-01'
WHERE clause_id = 92670; -- 52.216-8

UPDATE Clauses
SET effective_date = '2011-06-01' -- '2011-01-01'
WHERE clause_id = 92671; -- 52.216-9

UPDATE Clauses
SET effective_date = '1997-10-01' -- '1997-01-01'
WHERE clause_id = 92672; -- 52.217-2

UPDATE Clauses
SET effective_date = '1984-04-01' -- '1984-01-01'
WHERE clause_id = 92673; -- 52.217-3

UPDATE Clauses
SET effective_date = '1988-06-01' -- '1988-01-01'
WHERE clause_id = 92674; -- 52.217-4

UPDATE Clauses
SET effective_date = '1990-07-01' -- '1990-01-01'
WHERE clause_id = 92675; -- 52.217-5

UPDATE Clauses
SET effective_date = '1989-03-01' -- '1989-01-01'
WHERE clause_id = 92676; -- 52.217-6

UPDATE Clauses
SET effective_date = '1989-03-01' -- '1989-01-01'
WHERE clause_id = 92677; -- 52.217-7

UPDATE Clauses
SET effective_date = '1999-11-01' -- '1999-01-01'
WHERE clause_id = 92678; -- 52.217-8

UPDATE Clauses
SET effective_date = '2000-03-01' -- '2000-01-01'
WHERE clause_id = 92679; -- 52.217-9

UPDATE Clauses
SET effective_date = '2014-10-01' -- '2014-01-01'
WHERE clause_id = 92681; -- 52.219-1

UPDATE Clauses
SET effective_date = '2014-10-01' -- '2014-01-01'
WHERE clause_id = 92682; -- 52.219-10

UPDATE Clauses
SET effective_date = '1990-02-01' -- '1990-01-01'
WHERE clause_id = 92683; -- 52.219-11

UPDATE Clauses
SET effective_date = '1990-02-01' -- '1990-01-01'
WHERE clause_id = 92684; -- 52.219-12

UPDATE Clauses
SET effective_date = '2011-11-01' -- '2011-01-01'
WHERE clause_id = 92685; -- 52.219-13

UPDATE Clauses
SET effective_date = '2011-11-01' -- '2011-01-01'
WHERE clause_id = 92686; -- 52.219-14

UPDATE Clauses
SET effective_date = '1996-12-01' -- '1996-01-01'
WHERE clause_id = 92688; -- 52.219-17

UPDATE Clauses
SET effective_date = '2003-06-01' -- '2003-01-01'
WHERE clause_id = 92691; -- 52.219-18

UPDATE Clauses
SET effective_date = '1995-10-01' -- '1995-01-01'
WHERE clause_id = 92692; -- 52.219-2

UPDATE Clauses
SET effective_date = '2011-11-01' -- '2011-01-01'
WHERE clause_id = 92693; -- 52.219-27

UPDATE Clauses
SET effective_date = '2013-07-01' -- '2013-01-01'
WHERE clause_id = 92694; -- 52.219-28

UPDATE Clauses
SET effective_date = '2013-07-01' -- '2013-01-01'
WHERE clause_id = 92695; -- 52.219-29

UPDATE Clauses
SET effective_date = '2011-11-01' -- '2011-01-01'
WHERE clause_id = 92697; -- 52.219-3

UPDATE Clauses
SET effective_date = '2013-07-01' -- '2013-01-01'
WHERE clause_id = 92698; -- 52.219-30

UPDATE Clauses
SET effective_date = '2014-10-01' -- '2014-01-01'
WHERE clause_id = 92700; -- 52.219-4

UPDATE Clauses
SET effective_date = '2011-11-01' -- '2011-01-01'
WHERE clause_id = 92701; -- 52.219-6

UPDATE Clauses
SET effective_date = '2003-06-01' -- '2003-01-01'
WHERE clause_id = 92706; -- 52.219-7

UPDATE Clauses
SET effective_date = '2014-10-01' -- '2014-01-01'
WHERE clause_id = 92707; -- 52.219-8

UPDATE Clauses
SET effective_date = '2014-10-01' -- '2014-01-01'
WHERE clause_id = 92711; -- 52.219-9

UPDATE Clauses
SET effective_date = '1997-02-01' -- '1997-01-01'
WHERE clause_id = 92712; -- 52.222-1

UPDATE Clauses
SET effective_date = '1988-02-01' -- '1988-01-01'
WHERE clause_id = 92713; -- 52.222-10

UPDATE Clauses
SET effective_date = '2014-05-01' -- '2014-01-01'
WHERE clause_id = 92714; -- 52.222-11

UPDATE Clauses
SET effective_date = '2014-05-01' -- '2014-01-01'
WHERE clause_id = 92715; -- 52.222-12

UPDATE Clauses
SET effective_date = '2014-05-01' -- '2014-01-01'
WHERE clause_id = 92716; -- 52.222-13

UPDATE Clauses
SET effective_date = '1988-02-01' -- '1988-01-01'
WHERE clause_id = 92717; -- 52.222-14

UPDATE Clauses
SET effective_date = '2014-05-01' -- '2014-01-01'
WHERE clause_id = 92718; -- 52.222-15

UPDATE Clauses
SET effective_date = '2014-05-01' -- '2014-01-01'
WHERE clause_id = 92719; -- 52.222-16

UPDATE Clauses
SET effective_date = '2014-05-01' -- '2014-01-01'
WHERE clause_id = 92720; -- 52.222-17

UPDATE Clauses
SET effective_date = '2001-02-01' -- '2001-01-01'
WHERE clause_id = 92721; -- 52.222-18

UPDATE Clauses
SET effective_date = '1990-07-01' -- '1990-01-01'
WHERE clause_id = 92723; -- 52.222-2

UPDATE Clauses
SET effective_date = '2014-05-01' -- '2014-01-01'
WHERE clause_id = 92724; -- 52.222-20

UPDATE Clauses
SET effective_date = '1999-02-01' -- '1999-01-01'
WHERE clause_id = 92726; -- 52.222-22

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1222_623&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?node=se48.2.52_1222_623&amp;rgn=div8'
, effective_date = '1999-02-01' -- '1999-01-01'
, clause_data = '<p>(a) The offeror''s attention is called to the Equal Opportunity clause and the Affirmative Action Compliance Requirements for Construction clause of this solicitation.</p><p>(b) The goals for minority and female participation, expressed in percentage terms for the Contractor''s aggregate workforce in each trade on all construction work in the covered area, are as follows:</p><div><div><table border="1" cellpadding="1" cellspacing="1" frame="void" width="100%"><tbody><tr><th scope="col">Goals for minority participation for each trade</th><th scope="col">Goals for female participation for each trade</th></tr><tr><td align="left" scope="row">{{textbox_52.222-23[0]}}</td><td align="left">{{textbox_52.222-23[1]}}</td></tr><tr><td align="left" scope="row">[<i>Contracting Officer shall insert goals</i>]</td><td align="left">[<i>Contracting Officer shall insert goals</i>]</td></tr></tbody></table></div></div><p>These goals are applicable to all the Contractor''s construction work performed in the covered area. If the Contractor performs construction work in a geographical area located outside of the covered area, the Contractor shall apply the goals established for the geographical area where the work is actually performed. Goals are published periodically in the Federal Register in notice form, and these notices may be obtained from any Office of Federal Contract Compliance Programs office.</p><p>(c) The Contractor''s compliance with Executive Order 11246, as amended, and the regulations in 41 CFR 60-4 shall be based on (1) its implementation of the Equal Opportunity clause, (2) specific affirmative action obligations required by the clause entitled <i>Affirmative Action Compliance Requirements for Construction,</i> and (3) its efforts to meet the goals. The hours of minority and female employment and training must be substantially uniform throughout the length of the contract, and in each trade. The Contractor shall make a good faith effort to employ minorities and women evenly on each of its projects. The transfer of minority or female employees or trainees from Contractor to Contractor, or from project to project, for the sole purpose of meeting the Contractor''s goals shall be a violation of the contract, Executive Order 11246, as amended, and the regulations in 41 CFR 60-4. Compliance with the goals will be measured against the total work hours performed.</p><p>(d) The Contractor shall provide written notification to the Deputy Assistant Secretary for Federal Contract Compliance, U.S. Department of Labor, within 10 working days following award of any construction subcontract in excess of $10,000 at any tier for construction work under the contract resulting from this solicitation. The notification shall list the-</p><p>(2) Name, address, and telephone number of the subcontractor;</p><p>(i) Employer''s identification number of the subcontractor;</p><p>(3) Estimated dollar amount of the subcontract;</p><p>(4) Estimated starting and completion dates of the subcontract; and</p><p>(5) Geographical area in which the subcontract is to be performed.</p><p>(e) As used in this Notice, and in any contract resulting from this solicitation, the <i>covered area</i> is {{textbox_52.222-23[2]}} [<i>Contracting Officer shall insert description of the geographical areas where the contract is to be performed, giving the State, county, and city</i>].</p>'
--               '<p>(a) The offeror''s attention is called to the Equal Opportunity clause and the Affirmative Action Compliance Requirements for Construction clause of this solicitation.</p><p>(b) The goals for minority and female participation, expressed in percentage terms for the Contractor''s aggregate workforce in each trade on all construction work in the covered area, are as follows:</p>\n<div><div><table border="&quot;1&quot;" cellpadding="&quot;1&quot;" cellspacing="&quot;1&quot;" frame="&quot;void&quot;" width="&quot;100%&quot;"><tbody><tr><th scope="&quot;col&quot;">Goals for minority participation for each trade</th><th scope="&quot;col&quot;">Goals for female participation for each trade</th></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">{{textbox_52.222-23[0]}}</td><td align="&quot;left&quot;">{{textbox_52.222-23[1]}}</td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">[<i>Contracting Officer shall insert goals</i>]</td><td align="&quot;left&quot;">[<i>Contracting Officer shall insert goals</i>]</td></tr></tbody></table></div></div><p>These goals are applicable to all the Contractor''s construction work performed in the covered area. If the Contractor performs construction work in a geographical area located outside of the covered area, the Contractor shall apply the goals established for the geographical area where the work is actually performed. Goals are published periodically in the Federal Register in notice form, and these notices may be obtained from any Office of Federal Contract Compliance Programs office.</p><p>(c) The Contractor''s compliance with Executive Order 11246, as amended, and the regulations in 41 CFR 60-4 shall be based on (1) its implementation of the Equal Opportunity clause, (2) specific affirmative action obligations required by the clause entitled <i>Affirmative Action Compliance Requirements for Construction,</i> and (3) its efforts to meet the goals. The hours of minority and female employment and training must be substantially uniform throughout the length of the contract, and in each trade. The Contractor shall make a good faith effort to employ minorities and women evenly on each of its projects. The transfer of minority or female employees or trainees from Contractor to Contractor, or from project to project, for the sole purpose of meeting the Contractor''s goals shall be a violation of the contract, Executive Order 11246, as amended, and the regulations in 41 CFR 60-4. Compliance with the goals will be measured against the total work hours performed.</p><p>(d) The Contractor shall provide written notification to the Deputy Assistant Secretary for Federal Contract Compliance, U.S. Department of Labor, within 10 working days following award of any construction subcontract in excess of $10,000 at any tier for construction work under the contract resulting from this solicitation. The notification shall list the-</p><p>(2) Name, address, and telephone number of the subcontractor;</p><p>(i) Employer''s identification number of the subcontractor;</p><p>(3) Estimated dollar amount of the subcontract;</p><p>(4) Estimated starting and completion dates of the subcontract; and</p><p>(5) Geographical area in which the subcontract is to be performed.</p><p>(e) As used in this Notice, and in any contract resulting from this solicitation, the <i>covered area</i> is {{textbox_52.222-23[2]}} [<i>Contracting Officer shall insert description of the geographical areas where the contract is to be performed, giving the State, county, and city</i>].</p>'
WHERE clause_id = 92727; -- 52.222-23
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (92727, 'textbox_52.222-23[0]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (92727, 'textbox_52.222-23[1]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (92727, 'textbox_52.222-23[2]', 'S', 100);

UPDATE Clauses
SET effective_date = '1999-02-01' -- '1999-01-01'
WHERE clause_id = 92728; -- 52.222-24

UPDATE Clauses
SET effective_date = '1984-04-01' -- '1984-01-01'
WHERE clause_id = 92729; -- 52.222-25

UPDATE Clauses
SET effective_date = '2003-06-01' -- '2003-01-01'
WHERE clause_id = 92734; -- 52.222-3

UPDATE Clauses
SET effective_date = '2014-05-01' -- '2014-01-01'
WHERE clause_id = 92736; -- 52.222-30

UPDATE Clauses
SET effective_date = '2014-05-01' -- '2014-01-01'
WHERE clause_id = 92737; -- 52.222-31

UPDATE Clauses
SET effective_date = '2014-05-01' -- '2014-01-01'
WHERE clause_id = 92738; -- 52.222-32

UPDATE Clauses
SET effective_date = '2010-05-01' -- '2010-01-01'
WHERE clause_id = 92741; -- 52.222-33

UPDATE Clauses
SET effective_date = '2010-05-01' -- '2010-01-01'
WHERE clause_id = 92743; -- 52.222-34

UPDATE Clauses
SET effective_date = '2014-07-01' -- '2014-01-01'
WHERE clause_id = 92745; -- 52.222-35

UPDATE Clauses
SET effective_date = '2014-07-01' -- '2014-01-01'
WHERE clause_id = 92747; -- 52.222-36

UPDATE Clauses
SET effective_date = '2014-07-01' -- '2014-01-01'
WHERE clause_id = 92748; -- 52.222-37

UPDATE Clauses
SET effective_date = '2010-09-01' -- '2010-01-01'
WHERE clause_id = 92749; -- 52.222-38

UPDATE Clauses
SET effective_date = '2014-05-01' -- '2014-01-01'
WHERE clause_id = 92750; -- 52.222-4

UPDATE Clauses
SET effective_date = '2010-12-01' -- '2010-01-01'
WHERE clause_id = 92751; -- 52.222-40

UPDATE Clauses
SET effective_date = '2014-05-01' -- '2014-01-01'
WHERE clause_id = 92752; -- 52.222-41

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1222_642&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?node=se48.2.52_1222_642&amp;rgn=div8'
, effective_date = '2014-05-01' -- '2014-01-01'
, clause_data = '<p>In compliance with the Service Contract Labor Standards statute and the regulations of the Secretary of Labor (29 CFR part 4), this clause identifies the classes of service employees expected to be employed under the contract and states the wages and fringe benefits payable to each if they were employed by the contracting agency subject to the provisions of 5 U.S.C. 5341 or 5332.</p><h3>This Statement is for Information Only: It Is Not a Wage Determination</h3><div><table border="1" cellpadding="1" cellspacing="1" frame="void" width="100%"><tbody><tr><th scope="col">Employee class</th><th scope="col">Monetary wage-Fringe benefits</th></tr><tr><td>{{textbox_52.222-42[0]}}</td><td>{{textbox_52.222-42[1]}}</td></tr><tr><td>{{textbox_52.222-42[2]}}</td><td>{{textbox_52.222-42[3]}}</td></tr><tr><td>{{textbox_52.222-42[4]}}</td><td>{{textbox_52.222-42[5]}}</td></tr><tr><td>{{textbox_52.222-42[6]}}</td><td>{{textbox_52.222-42[7]}}</td></tr><tr><td>{{textbox_52.222-42[8]}}</td><td>{{textbox_52.222-42[9]}}</td></tr><tr><td>{{textbox_52.222-42[10]}}</td><td>{{textbox_52.222-42[11]}}</td></tr></tbody></table></div>'
--               '<p>In compliance with the Service Contract Labor Standards statute and the regulations of the Secretary of Labor (29 CFR part 4), this clause identifies the classes of service employees expected to be employed under the contract and states the wages and fringe benefits payable to each if they were employed by the contracting agency subject to the provisions of 5 U.S.C. 5341 or 5332.</p>\n<h3>This Statement is for Information Only: It Is Not a Wage Determination</h3>\n<div><table border="&quot;1&quot;" cellpadding="&quot;1&quot;" cellspacing="&quot;1&quot;" frame="&quot;void&quot;" width="&quot;100%&quot;"><tbody><tr><th scope="&quot;col&quot;">Employee class</th><th scope="&quot;col&quot;">Monetary wage''??Fringe benefits</th></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;"></td><td align="&quot;right&quot;"></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;"></td><td align="&quot;right&quot;"></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;"></td><td align="&quot;right&quot;"></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;"></td><td align="&quot;right&quot;"></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;"></td><td align="&quot;right&quot;"></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;"></td><td align="&quot;right&quot;"></td></tr></tbody></table></div>'
WHERE clause_id = 92753; -- 52.222-42
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (92753, 'textbox_52.222-42[0]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (92753, 'textbox_52.222-42[1]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (92753, 'textbox_52.222-42[2]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (92753, 'textbox_52.222-42[3]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (92753, 'textbox_52.222-42[4]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (92753, 'textbox_52.222-42[5]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (92753, 'textbox_52.222-42[6]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (92753, 'textbox_52.222-42[7]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (92753, 'textbox_52.222-42[8]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (92753, 'textbox_52.222-42[9]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (92753, 'textbox_52.222-42[10]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (92753, 'textbox_52.222-42[11]', 'S', 100);

UPDATE Clauses
SET effective_date = '2014-05-01' -- '2014-01-01'
WHERE clause_id = 92754; -- 52.222-43

UPDATE Clauses
SET effective_date = '2014-05-01' -- '2014-01-01'
WHERE clause_id = 92755; -- 52.222-44

UPDATE Clauses
SET effective_date = '1993-02-01' -- '1993-01-01'
WHERE clause_id = 92756; -- 52.222-46

UPDATE Clauses
SET effective_date = '2014-05-01' -- '2014-01-01'
WHERE clause_id = 92757; -- 52.222-48

UPDATE Clauses
SET effective_date = '2014-05-01' -- '2014-01-01'
WHERE clause_id = 92758; -- 52.222-49

UPDATE Clauses
SET effective_date = '2014-05-01' -- '2014-01-01'
WHERE clause_id = 92759; -- 52.222-5

UPDATE Clauses
SET effective_date = '2014-05-01' -- '2014-01-01'
WHERE clause_id = 92763; -- 52.222-52

UPDATE Clauses
SET effective_date = '2014-05-01' -- '2014-01-01'
WHERE clause_id = 92764; -- 52.222-53

UPDATE Clauses
SET effective_date = '2013-08-01' -- '2013-01-01'
WHERE clause_id = 92765; -- 52.222-54

UPDATE Clauses
SET effective_date = '2014-05-01' -- '2014-01-01'
WHERE clause_id = 92768; -- 52.222-6

UPDATE Clauses
SET effective_date = '2014-05-01' -- '2014-01-01'
WHERE clause_id = 92769; -- 52.222-7

UPDATE Clauses
SET effective_date = '2014-05-01' -- '2014-01-01'
WHERE clause_id = 92770; -- 52.222-8

UPDATE Clauses
SET effective_date = '2005-07-01' -- '2005-01-01'
WHERE clause_id = 92771; -- 52.222-9

UPDATE Clauses
SET effective_date = '2012-05-01' -- '2012-01-01'
WHERE clause_id = 92772; -- 52.223-1

UPDATE Clauses
SET effective_date = '2008-05-01' -- '2008-01-01'
WHERE clause_id = 92773; -- 52.223-10

UPDATE Clauses
SET effective_date = '2001-05-01' -- '2001-01-01'
WHERE clause_id = 92774; -- 52.223-11

UPDATE Clauses
SET effective_date = '1995-05-01' -- '1995-01-01'
WHERE clause_id = 92775; -- 52.223-12

UPDATE Clauses
SET effective_date = '1995-05-01' -- '1995-01-01'
WHERE clause_id = 92776; -- 52.223-13

UPDATE Clauses
SET effective_date = '2014-06-01' -- '2014-01-01'
WHERE clause_id = 92778; -- 52.223-14

UPDATE Clauses
SET effective_date = '2007-12-01' -- '2007-01-01'
WHERE clause_id = 92780; -- 52.223-15

UPDATE Clauses
SET effective_date = '2014-06-01' -- '2014-01-01'
WHERE clause_id = 92782; -- 52.223-16

UPDATE Clauses
SET effective_date = '2008-05-01' -- '2008-01-01'
WHERE clause_id = 92783; -- 52.223-17

UPDATE Clauses
SET effective_date = '2011-08-01' -- '2011-01-01'
WHERE clause_id = 92784; -- 52.223-18

UPDATE Clauses
SET effective_date = '2011-05-01' -- '2011-01-01'
WHERE clause_id = 92785; -- 52.223-19

UPDATE Clauses
SET effective_date = '2013-09-01' -- '2013-01-01'
WHERE clause_id = 92786; -- 52.223-2

UPDATE Clauses
SET effective_date = '2008-05-01' -- '2008-01-01'
WHERE clause_id = 92789; -- 52.223-4

UPDATE Clauses
SET effective_date = '2008-05-01' -- '2008-01-01'
WHERE clause_id = 92792; -- 52.223-5

UPDATE Clauses
SET effective_date = '2001-05-01' -- '2001-01-01'
WHERE clause_id = 92793; -- 52.223-6

UPDATE Clauses
SET effective_date = '2008-05-01' -- '2008-01-01'
WHERE clause_id = 92796; -- 52.223-9

UPDATE Clauses
SET effective_date = '1984-04-01' -- '1984-01-01'
WHERE clause_id = 92797; -- 52.224-1

UPDATE Clauses
SET effective_date = '1984-04-01' -- '1984-01-01'
WHERE clause_id = 92798; -- 52.224-2

UPDATE Clauses
SET effective_date = '2014-05-01' -- '2014-01-01'
WHERE clause_id = 92801; -- 52.225-10

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1225_611&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?node=se48.2.52_1225_611&amp;rgn=div8'
, effective_date = '2014-05-01' -- '2014-01-01'
, clause_data = '<p>(a) <i>Definitions.</i> As used in this clause-</p><p><i>Caribbean Basin country construction material</i> means a construction material that-</p><p>(1) Is wholly the growth, product, or manufacture of a Caribbean Basin country; or</p><p>(2) In the case of a construction material that consists in whole or in part of materials from another country, has been substantially transformed in a Caribbean Basin country into a new and different construction material distinct from the materials from which it was transformed.</p><p><i>Commercially available off-the-shelf (COTS) item</i>-(1) Means any item of supply (including construction material) that is-</p><p>(i) A commercial item (as defined in paragraph (1) of the definition at FAR 2.101);</p><p>(ii) Sold in substantial quantities in the commercial marketplace; and</p><p>(iii) Offered to the Government, under a contract or subcontract at any tier, without modification, in the same form in which it is sold in the commercial marketplace; and</p><p>(2) Does not include bulk cargo, as defined in 46 U.S.C. 40102(4), such as agricultural products and petroleum products.</p><p><i>Component</i> means an article, material, or supply incorporated directly into a construction material.</p><p><i>Construction material</i> means an article, material, or supply brought to the construction site by the Contractor or subcontractor for incorporation into the building or work. The term also includes an item brought to the site preassembled from articles, materials, or supplies. However, emergency life safety systems, such as emergency lighting, fire alarm, and audio evacuation systems, that are discrete systems incorporated into a public building or work and that are produced as complete systems, are evaluated as a single and distinct construction material regardless of when or how the individual parts or components of those systems are delivered to the construction site. Materials purchased directly by the Government are supplies, not construction material.</p><p><i>Cost of components</i> means-</p><p>(1) For components purchased by the Contractor, the acquisition cost, including transportation costs to the place of incorporation into the construction material (whether or not such costs are paid to a domestic firm), and any applicable duty (whether or not a duty-free entry certificate is issued); or</p><p>(2) For components manufactured by the Contractor, all costs associated with the manufacture of the component, including transportation costs as described in paragraph (1) of this definition, plus allocable overhead costs, but excluding profit. Cost of components does not include any costs associated with the manufacture of the construction material.</p><p><i>Designated country</i> means any of the following countries:</p><p>(1) A World Trade Organization Government Procurement Agreement country (Armenia, Aruba, Austria, Belgium, Bulgaria, Canada, Croatia, Cyprus, Czech Republic, Denmark, Estonia, Finland, France, Germany, Greece, Hong Kong, Hungary, Iceland, Ireland, Israel, Italy, Japan, Korea (Republic of), Latvia, Liechtenstein, Lithuania, Luxembourg, Malta, Netherlands, Norway, Poland, Portugal, Romania, Singapore, Slovak Republic, Slovenia, Spain, Sweden, Switzerland, Taiwan, or United Kingdom);</p><p>(2) A Free Trade Agreement country (Australia, Bahrain, Canada, Chile, Colombia, Costa Rica, Dominican Republic, El Salvador, Guatemala, Honduras, Korea (Republic of), Mexico, Morocco, Nicaragua, Oman, Panama, Peru, or Singapore);</p><p>(3) A least developed country (Afghanistan, Angola, Bangladesh, Benin, Bhutan, Burkina Faso, Burundi, Cambodia, Central African Republic, Chad, Comoros, Democratic Republic of Congo, Djibouti, Equatorial Guinea, Eritrea, Ethiopia, Gambia, Guinea, Guinea-Bissau, Haiti, Kiribati, Laos, Lesotho, Liberia, Madagascar, Malawi, Mali, Mauritania, Mozambique, Nepal, Niger, Rwanda, Samoa, Sao Tome and Principe, Senegal, Sierra Leone, Solomon Islands, Somalia, South Sudan,Tanzania, Timor-Leste, Togo, Tuvalu, Uganda, Vanuatu, Yemen, or Zambia); or</p><p>(4) A Caribbean Basin country ((Antigua and Barbuda, Aruba, Bahamas, Barbados, Belize, Bonaire, British Virgin Islands, Curacao, Dominica, Grenada, Guyana, Haiti, Jamaica, Montserrat, Saba, St. Kitts and Nevis, St. Lucia, St. Vincent and the Grenadines, Sint Eustatius, Sint Maarten, or Trinidad and Tobago).</p><p><i>Designated country construction material</i> means a construction material that is a WTO GPA country construction material, an FTA country construction material, a least developed country construction material, or a Caribbean Basin country construction material.</p><p><i>Domestic construction material means</i>-</p><p>(1) An unmanufactured construction material mined or produced in the United States;</p><p>(2) A construction material manufactured in the United States, if-</p><p>(i) The cost of its components mined, produced, or manufactured in the United States exceeds 50 percent of the cost of all its components. Components of foreign origin of the same class or kind for which nonavailability determinations have been made are treated as domestic; or</p><p>(ii) The construction material is a COTS item.</p><p><i>Foreign construction material</i> means a construction material other than a domestic construction material.</p><p><i>Free Trade Agreement country construction material</i> means a construction material that-</p><p>(1) Is wholly the growth, product, or manufacture of a Free Trade Agreement (FTA) country; or </p><p>(2) In the case of a construction material that consists in whole or in part of materials from another country, has been substantially transformed in a FTA country into a new and different construction material distinct from the materials from which it was transformed.</p><p><i>Least developed country construction material</i> means a construction material that-</p><p>(1) Is wholly the growth, product, or manufacture of a least developed country; or</p><p>(2) In the case of a construction material that consists in whole or in part of materials from another country, has been substantially transformed in a least developed country into a new and different construction material distinct from the materials from which it was transformed.</p><p><i>United States</i> means the 50 States, the District of Columbia, and outlying areas.</p><p><i>WTO GPA country construction material</i> means a construction material that-</p><p>(1) Is wholly the growth, product, or manufacture of a WTO GPA country; or</p><p>(2) In the case of a construction material that consists in whole or in part of materials from another country, has been substantially transformed in a WTO GPA country into a new and different construction material distinct from the materials from which it was transformed.</p><p>(b) <i>Construction materials.</i> (1) This clause implements 41 U.S.C. chapter 83, Buy American, by providing a preference for domestic construction material. In accordance with 41 U.S.C. 1907, the component test of the Buy American statute is waived for construction material that is a COTS item. (See FAR 12.505(a)(2)). In addition, the Contracting Officer has determined that the WTO GPA and Free Trade Agreements (FTAs) apply to this acquisition. Therefore, the Buy American restrictions are waived for designated country construction materials.</p><p>(2) The Contractor shall use only domestic or designated country construction material in performing this contract, except as provided in paragraphs (b)(3) and (b)(4) of this clause.</p><p>(3) The requirement in paragraph (b)(2) of this clause does not apply to information technology that is a commercial item or to the construction materials or components listed by the Government as follows:</p><p>[Contracting Officer is to list applicable excepted materials or indicate ''none'']</p><p>(4) The Contracting Officer may add other foreign construction material to the list in paragraph (b)(3) of this clause if the Government determines that-</p><p>(i) The cost of domestic construction material would be unreasonable. The cost of a particular domestic construction material subject to the restrictions of the Buy American statute is unreasonable when the cost of such material exceeds the cost of foreign material by more than 6 percent;</p><p>(ii) The application of the restriction of the Buy American statute to a particular construction material would be impracticable or inconsistent with the public interest; or</p><p>(iii) The construction material is not mined, produced, or manufactured in the United States in sufficient and reasonably available commercial quantities of a satisfactory quality.</p><p>(c) <i>Request for determination of inapplicability of the Buy American statute.</i> (1)(i) Any Contractor request to use foreign construction material in accordance with paragraph (b)(4) of this clause shall include adequate information for Government evaluation of the request, including-</p><p>(A) A description of the foreign and domestic construction materials;</p><p>(B) Unit of measure;</p><p>(C) Quantity;</p><p>(D) Price;</p><p>(E) Time of delivery or availability;</p><p>(F) Location of the construction project;</p><p>(G) Name and address of the proposed supplier; and</p><p>(H) A detailed justification of the reason for use of foreign construction materials cited in accordance with paragraph (b)(3) of this clause.</p><p>(ii) A request based on unreasonable cost shall include a reasonable survey of the market and a completed price comparison table in the format in paragraph (d) of this clause.</p><p>(iii) The price of construction material shall include all delivery costs to the construction site and any applicable duty (whether or not a duty-free certificate may be issued).</p><p>(iv) Any Contractor request for a determination submitted after contract award shall explain why the Contractor could not reasonably foresee the need for such determination and could not have requested the determination before contract award. If the Contractor does not submit a satisfactory explanation, the Contracting Officer need not make a determination.</p><p>(2) If the Government determines after contract award that an exception to the Buy American statute applies and the Contracting Officer and the Contractor negotiate adequate consideration, the Contracting Officer will modify the contract to allow use of the foreign construction material. However, when the basis for the exception is the unreasonable price of a domestic construction material, adequate consideration is not less than the differential established in paragraph (b)(4)(i) of this clause.</p><p>(3) Unless the Government determines that an exception to the Buy American statute applies, use of foreign construction material is noncompliant with the Buy Americanstatute.</p><p>(d) <i>Data.</i> To permit evaluation of requests under paragraph (c) of this clause based on unreasonable cost, the Contractor shall include the following information and any applicable supporting data based on the survey of suppliers:</p><div><p>Foreign and Domestic Construction Materials Price Comparison</p></div><div><table border="1" cellpadding="1" cellspacing="1" frame="void" width="100%"><tbody><tr><th scope="col">Construction material description</th><th scope="col">Unit of measure</th><th scope="col">Quantity</th><th scope="col">Price (dollars)<sup>1</sup></th></tr><tr><td align="left" scope="row">Item 1:</td><td>{{textbox_52.225-11[0]}}</td><td>{{textbox_52.225-11[1]}}</td><td>{{textbox_52.225-11[2]}}</td></tr><tr><td align="left" scope="row">Foreign construction material</td><td>{{textbox_52.225-11[3]}}</td><td>{{textbox_52.225-11[4]}}</td><td>{{textbox_52.225-11[5]}}</td></tr><tr><td align="left" scope="row">Domestic construction material</td><td>{{textbox_52.225-11[6]}}</td><td>{{textbox_52.225-11[7]}}</td><td>{{textbox_52.225-11[8]}}</td></tr><tr><td align="left" scope="row">Item 2:</td><td>{{textbox_52.225-11[9]}}</td><td>{{textbox_52.225-11[10]}}</td><td>{{textbox_52.225-11[11]}}</td></tr><tr><td align="left" scope="row">Foreign construction material</td><td>{{textbox_52.225-11[12]}}</td><td>{{textbox_52.225-11[13]}}</td><td>{{textbox_52.225-11[14]}}</td></tr><tr><td align="left" scope="row">Domestic construction material</td><td>{{textbox_52.225-11[15]}}</td><td>{{textbox_52.225-11[16]}}</td><td>{{textbox_52.225-11[17]}}</td></tr></tbody></table></div><div><p><sup>1</sup>Include all delivery costs to the construction site and any applicable duty (whether or not a duty-free entry certificate is issued).</p><p>List name, address, telephone number, and contact for suppliers surveyed. Attach copy of response; if oral, attach summary.</p><p>Include other applicable supporting information.</p></div>'
--               '<p>(a) <i>Definitions.</i> As used in this clause''??</p><p><i>Caribbean Basin country construction material</i> means a construction material that''??</p><p>(1) Is wholly the growth, product, or manufacture of a Caribbean Basin country; or</p><p>(2) In the case of a construction material that consists in whole or in part of materials from another country, has been substantially transformed in a Caribbean Basin country into a new and different construction material distinct from the materials from which it was transformed.</p><p><i>Commercially available off-the-shelf (COTS) item</i>''??(1) Means any item of supply (including construction material) that is''??</p><p>(i) A commercial item (as defined in paragraph (1) of the definition at FAR 2.101);</p><p>(ii) Sold in substantial quantities in the commercial marketplace; and</p><p>(iii) Offered to the Government, under a contract or subcontract at any tier, without modification, in the same form in which it is sold in the commercial marketplace; and</p><p>(2) Does not include bulk cargo, as defined in 46 U.S.C. 40102(4), such as agricultural products and petroleum products.</p><p><i>Component</i> means an article, material, or supply incorporated directly into a construction material.</p><p><i>Construction material</i> means an article, material, or supply brought to the construction site by the Contractor or subcontractor for incorporation into the building or work. The term also includes an item brought to the site preassembled from articles, materials, or supplies. However, emergency life safety systems, such as emergency lighting, fire alarm, and audio evacuation systems, that are discrete systems incorporated into a public building or work and that are produced as complete systems, are evaluated as a single and distinct construction material regardless of when or how the individual parts or components of those systems are delivered to the construction site. Materials purchased directly by the Government are supplies, not construction material.</p><p><i>Cost of components</i> means''??</p><p>(1) For components purchased by the Contractor, the acquisition cost, including transportation costs to the place of incorporation into the construction material (whether or not such costs are paid to a domestic firm), and any applicable duty (whether or not a duty-free entry certificate is issued); or</p><p>(2) For components manufactured by the Contractor, all costs associated with the manufacture of the component, including transportation costs as described in paragraph (1) of this definition, plus allocable overhead costs, but excluding profit. Cost of components does not include any costs associated with the manufacture of the construction material.</p><p><i>Designated country</i> means any of the following countries:</p><p>(1) A World Trade Organization Government Procurement Agreement country (Armenia, Aruba, Austria, Belgium, Bulgaria, Canada, Croatia, Cyprus, Czech Republic, Denmark, Estonia, Finland, France, Germany, Greece, Hong Kong, Hungary, Iceland, Ireland, Israel, Italy, Japan, Korea (Republic of), Latvia, Liechtenstein, Lithuania, Luxembourg, Malta, Netherlands, Norway, Poland, Portugal, Romania, Singapore, Slovak Republic, Slovenia, Spain, Sweden, Switzerland, Taiwan, or United Kingdom);</p><p>(2) A Free Trade Agreement country (Australia, Bahrain, Canada, Chile, Colombia, Costa Rica, Dominican Republic, El Salvador, Guatemala, Honduras, Korea (Republic of), Mexico, Morocco, Nicaragua, Oman, Panama, Peru, or Singapore);</p><p>(3) A least developed country (Afghanistan, Angola, Bangladesh, Benin, Bhutan, Burkina Faso, Burundi, Cambodia, Central African Republic, Chad, Comoros, Democratic Republic of Congo, Djibouti,  Equatorial Guinea, Eritrea, Ethiopia, Gambia, Guinea, Guinea-Bissau, Haiti, Kiribati, Laos, Lesotho, Liberia, Madagascar, Malawi,  Mali, Mauritania, Mozambique, Nepal, Niger, Rwanda, Samoa, Sao Tome and Principe, Senegal, Sierra Leone, Solomon Islands, Somalia, South Sudan,Tanzania, Timor-Leste, Togo, Tuvalu, Uganda, Vanuatu, Yemen, or Zambia); or</p><p>(4) A Caribbean Basin country ((Antigua and Barbuda, Aruba, Bahamas, Barbados, Belize, Bonaire, British Virgin Islands, Curacao, Dominica, Grenada, Guyana, Haiti, Jamaica, Montserrat, Saba, St. Kitts and Nevis, St. Lucia, St. Vincent and the Grenadines, Sint Eustatius, Sint Maarten, or Trinidad and Tobago).</p><p><i>Designated country construction material</i> means a construction material that is a WTO GPA country construction material, an FTA country construction material, a least developed country construction material, or a Caribbean Basin country construction material.</p><p><i>Domestic construction material means</i>''??</p><p>(1) An unmanufactured construction material mined or produced in the United States;</p><p>(2) A construction material manufactured in the United States, if''??</p><p>(i) The cost of its components mined, produced, or manufactured in the United States exceeds 50 percent of the cost of all its components. Components of foreign origin of the same class or kind for which nonavailability determinations have been made are treated as domestic; or</p><p>(ii) The construction material is a COTS item.</p><p><i>Foreign construction material</i> means a construction material other than a domestic construction material.</p><p><i>Free Trade Agreement country construction material</i> means a construction material that''??</p><p>(1) Is wholly the growth, product, or manufacture of a Free Trade Agreement (FTA) country; or </p><p>(2) In the case of a construction material that consists in whole or in part of materials from another country, has been substantially transformed in a FTA country into a new and different construction material distinct from the materials from which it was transformed.</p><p><i>Least developed country construction material</i> means a construction material that''??</p><p>(1) Is wholly the growth, product, or manufacture of a least developed country; or</p><p>(2) In the case of a construction material that consists in whole or in part of materials from another country, has been substantially transformed in a least developed country into a new and different construction material distinct from the materials from which it was transformed.</p><p><i>United States</i> means the 50 States, the District of Columbia, and outlying areas.</p><p><i>WTO GPA country construction material</i> means a construction material that''??</p><p>(1) Is wholly the growth, product, or manufacture of a WTO GPA country; or</p><p>(2) In the case of a construction material that consists in whole or in part of materials from another country, has been substantially transformed in a WTO GPA country into a new and different construction material distinct from the materials from which it was transformed.</p><p>(b) <i>Construction materials.</i> (1) This clause implements 41 U.S.C. chapter 83, Buy American, by providing a preference for domestic construction material. In accordance with 41 U.S.C. 1907, the component test of the Buy American statute is waived for construction material that is a COTS item. (See FAR 12.505(a)(2)). In addition, the Contracting Officer has determined that the WTO GPA and Free Trade Agreements (FTAs) apply to this acquisition. Therefore, the Buy American restrictions are waived for designated country construction materials.</p><p>(2) The Contractor shall use only domestic or designated country construction material in performing this contract, except as provided in paragraphs (b)(3) and (b)(4) of this clause.</p><p>(3) The requirement in paragraph (b)(2) of this clause does not apply to information technology that is a commercial item or to the construction materials or components listed by the Government as follows:</p><p>[Contracting Officer is to list applicable excepted materials or indicate ''??none''?]</p><p>(4) The Contracting Officer may add other foreign construction material to the list in paragraph (b)(3) of this clause if the Government determines that''??</p><p>(i) The cost of domestic construction material would be unreasonable. The cost of a particular domestic construction material subject to the restrictions of the Buy American statute is unreasonable when the cost of such material exceeds the cost of foreign material by more than 6 percent;</p><p>(ii) The application of the restriction of the Buy American statute to a particular construction material would be impracticable or inconsistent with the public interest; or</p><p>(iii) The construction material is not mined, produced, or manufactured in the United States in sufficient and reasonably available commercial quantities of a satisfactory quality.</p><p>(c) <i>Request for determination of inapplicability of the Buy American statute.</i> (1)(i) Any Contractor request to use foreign construction material in accordance with paragraph (b)(4) of this clause shall include adequate information for Government evaluation of the request, including''??</p><p>(A) A description of the foreign and domestic construction materials;</p><p>(B) Unit of measure;</p><p>(C) Quantity;</p><p>(D) Price;</p><p>(E) Time of delivery or availability;</p><p>(F) Location of the construction project;</p><p>(G) Name and address of the proposed supplier; and</p><p>(H) A detailed justification of the reason for use of foreign construction materials cited in accordance with paragraph (b)(3) of this clause.</p><p>(ii) A request based on unreasonable cost shall include a reasonable survey of the market and a completed price comparison table in the format in paragraph (d) of this clause.</p><p>(iii) The price of construction material shall include all delivery costs to the construction site and any applicable duty (whether or not a duty-free certificate may be issued).</p><p>(iv) Any Contractor request for a determination submitted after contract award shall explain why the Contractor could not reasonably foresee the need for such determination and could not have requested the determination before contract award. If the Contractor does not submit a satisfactory explanation, the Contracting Officer need not make a determination.</p><p>(2) If the Government determines after contract award that an exception to the Buy American statute applies and the Contracting Officer and the Contractor negotiate adequate consideration, the Contracting Officer will modify the contract to allow use of the foreign construction material. However, when the basis for the exception is the unreasonable price of a domestic construction material, adequate consideration is not less than the differential established in paragraph (b)(4)(i) of this clause.</p><p>(3) Unless the Government determines that an exception to the Buy American statute applies, use of foreign construction material is noncompliant with the Buy Americanstatute.</p><p>(d) <i>Data.</i> To permit evaluation of requests under paragraph (c) of this clause based on unreasonable cost, the Contractor shall include the following information and any applicable supporting data based on the survey of suppliers:</p><div><p>Foreign and Domestic Construction Materials Price Comparison</p></div><div><table border="&quot;1&quot;" cellpadding="&quot;1&quot;" cellspacing="&quot;1&quot;" frame="&quot;void&quot;" width="&quot;100%&quot;"><tbody><tr><th scope="&quot;col&quot;">Construction material description</th><th scope="&quot;col&quot;">Unit of measure</th><th scope="&quot;col&quot;">Quantity</th><th scope="&quot;col&quot;">Price (dollars)<sup>1</sup></th></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">Item 1:</td><td align="&quot;right&quot;"></td><td align="&quot;right&quot;"></td><td align="&quot;right&quot;"></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;" 4em"="">Foreign construction material</td><td align="&quot;right&quot;"></td><td align="&quot;right&quot;"></td><td align="&quot;right&quot;"></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;" 4em"="">Domestic construction material</td><td align="&quot;right&quot;"></td><td align="&quot;right&quot;"></td><td align="&quot;right&quot;"></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">Item 2:</td><td align="&quot;right&quot;"></td><td align="&quot;right&quot;"></td><td align="&quot;right&quot;"></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;" 4em"="">Foreign construction material</td><td align="&quot;right&quot;"></td><td align="&quot;right&quot;"></td><td align="&quot;right&quot;"></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;" 4em"="">Domestic construction material</td><td align="&quot;right&quot;"></td><td align="&quot;right&quot;"></td><td align="&quot;right&quot;"></td></tr></tbody></table></div><div><p><sup>1</sup>Include all delivery costs to the construction site and any applicable duty (whether or not a duty-free entry certificate is issued).</p><p>List name, address, telephone number, and contact for suppliers surveyed. Attach copy of response; if oral, attach summary.</p><p>Include other applicable supporting information.</p></div>'
WHERE clause_id = 92803; -- 52.225-11
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (92803, 'textbox_52.225-11[0]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (92803, 'textbox_52.225-11[1]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (92803, 'textbox_52.225-11[2]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (92803, 'textbox_52.225-11[3]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (92803, 'textbox_52.225-11[4]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (92803, 'textbox_52.225-11[5]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (92803, 'textbox_52.225-11[6]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (92803, 'textbox_52.225-11[7]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (92803, 'textbox_52.225-11[8]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (92803, 'textbox_52.225-11[9]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (92803, 'textbox_52.225-11[10]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (92803, 'textbox_52.225-11[11]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (92803, 'textbox_52.225-11[12]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (92803, 'textbox_52.225-11[13]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (92803, 'textbox_52.225-11[14]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (92803, 'textbox_52.225-11[15]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (92803, 'textbox_52.225-11[16]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (92803, 'textbox_52.225-11[17]', 'S', 100);

UPDATE Clauses
SET effective_date = '2014-05-01' -- '2014-01-01'
WHERE clause_id = 92806; -- 52.225-12

UPDATE Clauses
SET effective_date = '2008-06-01' -- '2008-01-01'
WHERE clause_id = 92807; -- 52.225-13

UPDATE Clauses
SET effective_date = '2000-02-01' -- '2000-01-01'
WHERE clause_id = 92808; -- 52.225-14

UPDATE Clauses
SET effective_date = '2000-02-01' -- '2000-01-01'
WHERE clause_id = 92809; -- 52.225-17

UPDATE Clauses
SET effective_date = '2008-03-01' -- '2008-01-01'
WHERE clause_id = 92811; -- 52.225-19

UPDATE Clauses
SET effective_date = '2014-05-01' -- '2014-01-01'
WHERE clause_id = 92812; -- 52.225-2

UPDATE Clauses
SET effective_date = '2009-08-01' -- '2009-01-01'
WHERE clause_id = 92813; -- 52.225-20

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1225_621&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?node=se48.2.52_1225_621&amp;rgn=div8'
, effective_date = '2014-05-01' -- '2014-01-01'
, clause_data = '<p>(a) <i>Definitions.</i> As used in this clause-</p><p><i>Component</i> means an article, material, or supply incorporated directly into a construction material.</p><p><i>Construction material</i> means an article, material, or supply brought to the construction site by the Contractor or a subcontractor for incorporation into the building or work. The term also includes an item brought to the site preassembled from articles, materials, or supplies. However, emergency life safety systems, such as emergency lighting, fire alarm, and audio evacuation systems, that are discrete systems incorporated into a public building or work and that are produced as complete systems, are evaluated as a single and distinct construction material regardless of when or how the individual parts or components of those systems are delivered to the construction site. </p><p><i>Domestic construction material</i> means the following-</p><p>(1) An unmanufactured construction material mined or produced in the United States. (The Buy American statute applies.)</p><p>(2) A manufactured construction material that is manufactured in the United States and, if the construction material consists wholly or predominantly of iron or steel, the iron or steel was produced in the United States. (Section 1605 of the Recovery Act applies.)</p><p><i>Foreign construction material</i> means a construction material other than a domestic construction material.</p><p><i>Manufactured construction material</i> means any construction material that is not unmanufactured construction material.</p><p><i>Steel</i> means an alloy that includes at least 50 percent iron, between .02 and 2 percent carbon, and may include other elements.</p><p><i>United States</i> means the 50 States, the District of Columbia, and outlying areas.</p><p><i>Unmanufactured construction material</i> means raw material brought to the construction site for incorporation into the building or work that has not been-</p><p>(1) Processed into a specific form and shape; or</p><p>(2) Combined with other raw material to create a material that has different properties than the properties of the individual raw materials.</p><p>(b) <i>Domestic preference.</i> (1) This clause implements-</p><p>(i) Section 1605 of the American Recovery and Reinvestment Act of 2009 (Recovery Act) (Pub. L. 111-5), by requiring, unless an exception applies, that all manufactured construction material in the project is manufactured in the United States and, if the construction material consists wholly or predominantly of iron or steel, the iron or steel was produced in the United States (produced in the United States means that all manufacturing processes of the iron or steel must take place in the United States, except metallurgical processes involving refinement of steel additives); and</p><p>(ii) 41 U.S.C. chapter 83, Buy American, by providing a preference for unmanufactured construction material mined or produced in the United States over unmanufactured construction material mined or produced in a foreign country.</p><p>(2) The Contractor shall use only domestic construction material in performing this contract, except as provided in paragraph (b)(3) and (b)(4) of this clause.</p><p>(3) This requirement does not apply to the construction material or components listed by the Government as follows:</p>&nbsp; {{textbox_52.225-21[0]}}<p><i>[Contracting Officer to list applicable excepted materials or indicate ''none'']</i></p><p>(4) The Contracting Officer may add other foreign construction material to the list in paragraph (b)(3) of this clause if the Government determines that-</p><p>(i) The cost of domestic construction material would be unreasonable;</p><p>(A) The cost of domestic manufactured construction material, when compared to the cost of comparable foreign manufactured construction material, is unreasonable when the cumulative cost of such material will increase the cost of the contract by more than 25 percent;</p><p>(B) The cost of domestic unmanufactured construction material is unreasonable when the cost of such material exceeds the cost of comparable foreign unmanufactured construction material by more than 6 percent;</p><p>(ii) The construction material is not mined, produced, or manufactured in the United States in sufficient and reasonably available quantities and of a satisfactory quality;</p><p>(iii) The application of the restriction of section 1605 of the Recovery Act to a particular manufactured construction material would be inconsistent with the public interest or the application of the Buy American statute to a particular unmanufactured construction material would be impracticable or inconsistent with the public interest.</p><p>(c) <i>Request for determination of inapplicability of Section 1605 of the Recovery Act or the Buy American statute.</i> (1)(i) Any Contractor request to use foreign construction material in accordance with paragraph (b)(4) of this clause shall include adequate information for Government evaluation of the request, including-</p><p>(A) A description of the foreign and domestic construction materials;</p><p>(B) Unit of measure;</p><p>(C) Quantity;</p><p>(D) Cost;</p><p>(E) Time of delivery or availability;</p><p>(F) Location of the construction project;</p><p>(G) Name and address of the proposed supplier; and</p><p>(H) A detailed justification of the reason for use of foreign construction materials cited in accordance with paragraph (b)(4) of this clause.</p><p>(ii) A request based on unreasonable cost shall include a reasonable survey of the market and a completed cost comparison table in the format in paragraph (d) of this clause.</p><p>(iii) The cost of construction material shall include all delivery costs to the construction site and any applicable duty.</p><p>(iv) Any Contractor request for a determination submitted after contract award shall explain why the Contractor could not reasonably foresee the need for such determination and could not have requested the determination before contract award. If the Contractor does not submit a satisfactory explanation, the Contracting Officer need not make a determination.</p><p>(2) If the Government determines after contract award that an exception to section 1605 of the Recovery Act or the Buy American statute applies and the Contracting Officer and the Contractor negotiate adequate consideration, the Contracting Officer will modify the contract to allow use of the foreign construction material. However, when the basis for the exception is the unreasonable cost of a domestic construction material, adequate consideration is not less than the differential established in paragraph (b)(4)(i) of this clause.</p><p>(3) Unless the Government determines that an exception to section 1605 of the Recovery Act or the Buy American statute applies, use of foreign construction material is noncompliant with section 1605 of the American Recovery and Reinvestment Act or the Buy American statute.</p><p>(d) <i>Data.</i> To permit evaluation of requests under paragraph (c) of this clause based on unreasonable cost, the Contractor shall include the following information and any applicable supporting data based on the survey of suppliers:</p><div><div><p>Foreign and Domestic Construction Materials Cost Comparison</p></div><div><table border="1" cellpadding="1" cellspacing="1" frame="void" width="100%"><tbody><tr><th scope="col">Construction material description</th><th scope="col">Unit of measure</th><th scope="col">Quantity</th><th scope="col">Cost<br>(dollars)*</br></th></tr><tr><td align="left" scope="row">Item 1:</td><td>{{textbox_52.225-21[1]}}</td><td>{{textbox_52.225-21[2]}}</td><td>{{textbox_52.225-21[3]}}</td></tr><tr><td align="left" scope="row">Foreign construction material</td><td align="center">{{textbox_52.225-21[4]}}</td><td align="center">{{textbox_52.225-21[5]}}</td><td align="center">{{textbox_52.225-21[6]}}</td></tr><tr><td align="left" scope="row">Domestic construction material</td><td align="center">{{textbox_52.225-21[7]}}</td><td align="center">{{textbox_52.225-21[8]}}</td><td align="center">{{textbox_52.225-21[9]}}</td></tr><tr><td align="left" scope="row">Item 2</td><td>{{textbox_52.225-21[10]}}</td><td>{{textbox_52.225-21[11]}}</td><td>{{textbox_52.225-21[12]}}</td></tr><tr><td align="left" scope="row">Foreign construction material</td><td align="center">{{textbox_52.225-21[13]}}</td><td align="center">{{textbox_52.225-21[14]}}</td><td align="center">{{textbox_52.225-21[15]}}</td></tr><tr><td align="left" scope="row">Domestic construction material</td><td align="center">{{textbox_52.225-21[16]}}</td><td align="center">{{textbox_52.225-21[17]}}</td><td align="center">{{textbox_52.225-21[18]}}</td></tr></tbody></table></div><div><p><i>[List name, address, telephone number, and contact for suppliers surveyed. Attach copy of reponse; if oral, attach summary.] [Include other applicable supporting information.]</i></p><p>*<i>Include all delivery costs to the construction site.]</i></p></div></div>'
--               '<p>(a) <i>Definitions.</i> As used in this clause-</p><p><i>Component</i> means an article, material, or supply incorporated directly into a construction material.</p><p><i>Construction material</i> means an article, material, or supply brought to the construction site by the Contractor or a subcontractor for incorporation into the building or work. The term also includes an item brought to the site preassembled from articles, materials, or supplies. However, emergency life safety systems, such as emergency lighting, fire alarm, and audio evacuation systems, that are discrete systems incorporated into a public building or work and that are produced as complete systems, are evaluated as a single and distinct construction material regardless of when or how the individual parts or components of those systems are delivered to the construction site. </p><p><i>Domestic construction material</i> means the following-</p><p>(1) An unmanufactured construction material mined or produced in the United States. (The Buy American statute applies.)</p><p>(2) A manufactured construction material that is manufactured in the United States and, if the construction material consists wholly or predominantly of iron or steel, the iron or steel was produced in the United States. (Section 1605 of the Recovery Act applies.)</p><p><i>Foreign construction material</i> means a construction material other than a domestic construction material.</p><p><i>Manufactured construction material</i> means any construction material that is not unmanufactured construction material.</p><p><i>Steel</i> means an alloy that includes at least 50 percent iron, between .02 and 2 percent carbon, and may include other elements.</p><p><i>United States</i> means the 50 States, the District of Columbia, and outlying areas.</p><p><i>Unmanufactured construction material</i> means raw material brought to the construction site for incorporation into the building or work that has not been-</p><p>(1) Processed into a specific form and shape; or</p><p>(2) Combined with other raw material to create a material that has different properties than the properties of the individual raw materials.</p><p>(b) <i>Domestic preference.</i> (1) This clause implements-</p><p>(i) Section 1605 of the American Recovery and Reinvestment Act of 2009 (Recovery Act) (Pub. L. 111-5), by requiring, unless an exception applies, that all manufactured construction material in the project is manufactured in the United States and, if the construction material consists wholly or predominantly of iron or steel, the iron or steel was produced in the United States (produced in the United States means that all manufacturing processes of the iron or steel must take place in the United States, except metallurgical processes involving refinement of steel additives); and</p><p>(ii) 41 U.S.C. chapter 83, Buy American, by providing a preference for unmanufactured construction material mined or produced in the United States over unmanufactured construction material mined or produced in a foreign country.</p><p>(2) The Contractor shall use only domestic construction material in performing this contract, except as provided in paragraph (b)(3) and (b)(4) of this clause.</p><p>(3) This requirement does not apply to the construction material or components listed by the Government as follows:</p>&nbsp; {{textbox_52.225-21[0]}}\n<p><i>[Contracting Officer to list applicable excepted materials or indicate ânoneâ]</i></p><p>(4) The Contracting Officer may add other foreign construction material to the list in paragraph (b)(3) of this clause if the Government determines that-</p><p>(i) The cost of domestic construction material would be unreasonable;</p><p>(A) The cost of domestic manufactured construction material, when compared to the cost of comparable foreign manufactured construction material, is unreasonable when the cumulative cost of such material will increase the cost of the contract by more than 25 percent;</p><p>(B) The cost of domestic unmanufactured construction material is unreasonable when the cost of such material exceeds the cost of comparable foreign unmanufactured construction material by more than 6 percent;</p><p>(ii) The construction material is not mined, produced, or manufactured in the United States in sufficient and reasonably available quantities and of a satisfactory quality;</p><p>(iii) The application of the restriction of section 1605 of the Recovery Act to a particular manufactured construction material would be inconsistent with the public interest or the application of the Buy American statute to a particular unmanufactured construction material would be impracticable or inconsistent with the public interest.</p><p>(c) <i>Request for determination of inapplicability of Section 1605 of the Recovery Act or the Buy American statute.</i> (1)(i) Any Contractor request to use foreign construction material in accordance with paragraph (b)(4) of this clause shall include adequate information for Government evaluation of the request, including-</p><p>(A) A description of the foreign and domestic construction materials;</p><p>(B) Unit of measure;</p><p>(C) Quantity;</p><p>(D) Cost;</p><p>(E) Time of delivery or availability;</p><p>(F) Location of the construction project;</p><p>(G) Name and address of the proposed supplier; and</p><p>(H) A detailed justification of the reason for use of foreign construction materials cited in accordance with paragraph (b)(4) of this clause.</p><p>(ii) A request based on unreasonable cost shall include a reasonable survey of the market and a completed cost comparison table in the format in paragraph (d) of this clause.</p><p>(iii) The cost of construction material shall include all delivery costs to the construction site and any applicable duty.</p><p>(iv) Any Contractor request for a determination submitted after contract award shall explain why the Contractor could not reasonably foresee the need for such determination and could not have requested the determination before contract award. If the Contractor does not submit a satisfactory explanation, the Contracting Officer need not make a determination.</p><p>(2) If the Government determines after contract award that an exception to section 1605 of the Recovery Act or the Buy American statute applies and the Contracting Officer and the Contractor negotiate adequate consideration, the Contracting Officer will modify the contract to allow use of the foreign construction material. However, when the basis for the exception is the unreasonable cost of a domestic construction material, adequate consideration is not less than the differential established in paragraph (b)(4)(i) of this clause.</p><p>(3) Unless the Government determines that an exception to section 1605 of the Recovery Act or the Buy American statute applies, use of foreign construction material is noncompliant with section 1605 of the American Recovery and Reinvestment Act or the Buy American statute.</p><p>(d) <i>Data.</i> To permit evaluation of requests under paragraph (c) of this clause based on unreasonable cost, the Contractor shall include the following information and any applicable supporting data based on the survey of suppliers:</p>\n<div><div><p>Foreign and Domestic Construction Materials Cost Comparison</p></div><div><table border="&quot;1&quot;" cellpadding="&quot;1&quot;" cellspacing="&quot;1&quot;" frame="&quot;void&quot;" width="&quot;100%&quot;"><tbody><tr><th scope="&quot;col&quot;">Construction material description</th><th scope="&quot;col&quot;">Unit of measure</th><th scope="&quot;col&quot;">Quantity</th><th scope="&quot;col&quot;">Cost<br>(dollars)*</br></th></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">Item 1:</td><td align="&quot;center&quot;"></td><td align="&quot;center&quot;"></td><td align="&quot;center&quot;"></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;" 4em"="">Foreign construction material</td><td align="&quot;center&quot;">{{textbox_52.225-21[1]}}</td><td align="&quot;center&quot;">{{textbox_52.225-21[2]}}</td><td align="&quot;center&quot;">{{textbox_52.225-21[3]}}</td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;" 4em"="">Domestic construction material</td><td align="&quot;center&quot;">{{textbox_52.225-21[4]}}</td><td align="&quot;center&quot;">{{textbox_52.225-21[5]}}</td><td align="&quot;center&quot;">{{textbox_52.225-21[6]}}</td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">Item 2</td><td align="&quot;center&quot;"></td><td align="&quot;center&quot;"></td><td align="&quot;center&quot;"></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;" 4em"="">Foreign construction material</td><td align="&quot;center&quot;">{{textbox_52.225-21[7]}}</td><td align="&quot;center&quot;">{{textbox_52.225-21[8]}}</td><td align="&quot;center&quot;">{{textbox_52.225-21[9]}}</td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;" 4em"="">Domestic construction material</td><td align="&quot;center&quot;">{{textbox_52.225-21[10]}}</td><td align="&quot;center&quot;">{{textbox_52.225-21[11]}}</td><td align="&quot;center&quot;">{{textbox_52.225-21[12]}}</td></tr></tbody></table></div><div><p><i>[List name, address, telephone number, and contact for suppliers surveyed. Attach copy of reponse; if oral, attach summary.] [Include other applicable supporting information.]</i></p><p>*<i>Include all delivery costs to the construction site.]</i></p></div></div>'
WHERE clause_id = 92814; -- 52.225-21
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (92814, 'textbox_52.225-21[0]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (92814, 'textbox_52.225-21[1]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (92814, 'textbox_52.225-21[2]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (92814, 'textbox_52.225-21[3]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (92814, 'textbox_52.225-21[4]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (92814, 'textbox_52.225-21[5]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (92814, 'textbox_52.225-21[6]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (92814, 'textbox_52.225-21[7]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (92814, 'textbox_52.225-21[8]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (92814, 'textbox_52.225-21[9]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (92814, 'textbox_52.225-21[10]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (92814, 'textbox_52.225-21[11]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (92814, 'textbox_52.225-21[12]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (92814, 'textbox_52.225-21[13]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (92814, 'textbox_52.225-21[14]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (92814, 'textbox_52.225-21[15]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (92814, 'textbox_52.225-21[16]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (92814, 'textbox_52.225-21[17]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (92814, 'textbox_52.225-21[18]', 'S', 100);

UPDATE Clauses
SET effective_date = '2014-05-01' -- '2014-01-01'
WHERE clause_id = 92816; -- 52.225-22

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1225_623&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?node=se48.2.52_1225_623&amp;rgn=div8'
, effective_date = '2014-05-01' -- '2014-01-01'
, clause_data = '<p>(a) <i>Definitions.</i> As used in this clause-</p><p><i>Component</i> means an article, material, or supply incorporated directly into a construction material.</p><p><i>Construction material</i> means an article, material, or supply brought to the construction site by the Contractor or subcontractor for incorporation into the building or work. The term also includes an item brought to the site preassembled from articles, materials, or supplies. However, emergency life safety systems, such as emergency lighting, fire alarm, and audio evacuation systems, that are discrete systems incorporated into a public building or work and that are produced as complete systems, are evaluated as a single and distinct construction material regardless of when or how the individual parts or components of those systems are delivered to the construction site. </p><p><i>Designated country</i> means any of the following countries:</p><p>(1) A World Trade Organization Government Procurement Agreement (WTO GPA) country (Armenia, Aruba, Austria, Belgium, Bulgaria, Canada, Croatia, Cyprus, Czech Republic, Denmark, Estonia, Finland, France, Germany, Greece, Hong Kong, Hungary, Iceland, Ireland, Israel, Italy, Japan, Korea (Republic of), Latvia, Liechtenstein, Lithuania, Luxembourg, Malta, Netherlands, Norway, Poland, Portugal, Romania, Singapore, Slovak Republic, Slovenia, Spain, Sweden, Switzerland, Taiwan, or United Kingdom);</p><p>(2) A Free Trade Agreement (FTA) country (Australia, Bahrain, Canada, Chile, Colombia, Costa Rica, Dominican Republic, El Salvador, Guatemala, Honduras, Korea (Republic of), Mexico, Morocco, Nicaragua, Oman, Panama, Peru, or Singapore);</p><p>(3) A least developed country (Afghanistan, Angola, Bangladesh, Benin, Bhutan, Burkina Faso, Burundi, Cambodia, Central African Republic, Chad, Comoros, Democratic Republic of Congo, Djibouti, Equatorial Guinea, Eritrea, Ethiopia, Gambia, Guinea, Guinea-Bissau, Haiti, Kiribati, Laos, Lesotho, Liberia, Madagascar, Malawi, Mali, Mauritania, Mozambique, Nepal, Niger, Rwanda, Samoa, Sao Tome and Principe, Senegal, Sierra Leone, Solomon Islands, Somalia, South Sudan, Tanzania, Timor-Leste, Togo, Tuvalu, Uganda, Vanuatu, Yemen, or Zambia); or</p><p>(4) A Caribbean Basin country (Antigua and Barbuda, Aruba, Bahamas, Barbados, Belize, Bonaire, British Virgin Islands, Curacao, Dominica, Grenada, Guyana, Haiti, Jamaica, Montserrat, Saba, St. Kitts and Nevis, St. Lucia, St. Vincent and the Grenadines, Sint Eustatius, Sint Maarten, or Trinidad and Tobago).</p><p><i>Designated country construction material</i> means a construction material that is a WTO GPA country construction material, an FTA country construction material, a least developed country construction material, or a Caribbean Basin country construction material.</p><p><i>Domestic construction material</i> means the following:</p><p>(1) An unmanufactured construction material mined or produced in the United States. (The Buy American statute applies.)</p><p>(2) A manufactured construction material that is manufactured in the United States and, if the construction material consists wholly or predominantly of iron or steel, the iron or steel was produced in the United States. (Section 1605 of the Recovery Act applies.)</p><p><i>Foreign construction material</i> means a construction material other than a domestic construction material.</p><p><i>Free trade agreement (FTA) country construction material</i> means a construction material that-</p><p>(1) Is wholly the growth, product, or manufacture of an FTA country; or</p><p>(2) In the case of a construction material that consists in whole or in part of materials from another country, has been substantially transformed in an FTA country into a new and different construction material distinct from the materials from which it was transformed.</p><p><i>Least developed country construction material</i> means a construction material that-</p><p>(1) Is wholly the growth, product, or manufacture of a least developed country; or</p><p>(2) In the case of a construction material that consists in whole or in part of materials from another country, has been substantially transformed in a least developed country into a new and different construction material distinct from the materials from which it was transformed.</p><p><i>Manufactured construction material</i> means any construction material that is not unmanufactured construction material.</p><p><i>Nondesignated country</i> means a country other than the United States or a designated country.</p><p><i>Recovery Act designated country</i> means any of the following countries:</p><p>(1) A World Trade Organization Government Procurement Agreement (WTO GPA) country (Armenia, Aruba, Austria, Belgium, Bulgaria, Canada, Croatia, Cyprus, Czech Republic, Denmark, Estonia, Finland, France, Germany, Greece, Hong Kong, Hungary, Iceland, Ireland, Israel, Italy, Japan, Korea (Republic of), Latvia, Liechtenstein, Lithuania, Luxembourg, Malta, Netherlands, Norway, Poland, Portugal, Romania, Singapore, Slovak Republic, Slovenia, Spain, Sweden, Switzerland, Taiwan, or United Kingdom);</p><p>(2) A Free Trade Agreement country (FTA)(Australia, Bahrain, Canada, Chile, Colombia, Costa Rica, Dominican Republic, El Salvador, Guatemala, Honduras, Korea (Republic of), Mexico, Morocco, Nicaragua, Oman, Panama, Peru, or Singapore); or</p><p>(3) A least developed country (Afghanistan, Angola, Bangladesh, Benin, Bhutan, Burkina Faso, Burundi, Cambodia, Central African Republic, Chad, Comoros, Democratic Republic of Congo, Djibouti, Equatorial Guinea, Eritrea, Ethiopia, Gambia, Guinea, Guinea-Bissau, Haiti, Kiribati, Laos, Lesotho, Liberia, Madagascar, Malawi, Mali, Mauritania, Mozambique, Nepal, Niger, Rwanda, Samoa, Sao Tome and Principe, Senegal, Sierra Leone, Solomon Islands, Somalia, South Sudan, Tanzania, Timor-Leste, Togo, Tuvalu, Uganda, Vanuatu, Yemen, or Zambia).</p><p><i>Recovery Act designated country construction material</i> means a construction material that is a WTO GPA country construction material, an FTA country construction material, or a least developed country construction material.</p><p><i>Steel</i> means an alloy that includes at least 50 percent iron, between .02 and 2 percent carbon, and may include other elements.</p><p><i>United States</i> means the 50 States, the District of Columbia, and outlying areas.</p><p><i>Unmanufactured construction material</i> means raw material brought to the construction site for incorporation into the building or work that has not been-</p><p>(1) Processed into a specific form and shape; or</p><p>(2) Combined with other raw material to create a material that has different properties than the properties of the individual raw materials.</p><p><i>WTO GPA country construction material</i> means a construction material that-</p><p>(1) Is wholly the growth, product, or manufacture of a WTO GPA country; or</p><p>(2) In the case of a construction material that consists in whole or in part of materials from another country, has been substantially transformed in a WTO GPA country into a new and different construction material distinct from the materials from which it was transformed.</p><p>(b) <i>Construction materials.</i> (1) The restrictions of section 1605 of the American Recovery and Reinvestment Act of 2009 (Pub. L. 111-5) (Recovery Act) do not apply to Recovery Act designated country manufactured construction material. The restrictions of the Buy American statute do not apply to designated country unmanufactured construction material. Consistent with U.S. obligations under international agreements, this clause implements-</p><p>(i) Section 1605 of the Recovery Act by requiring, unless an exception applies, that all manufactured construction material in the project is manufactured in the United States and, if the construction material consists wholly or predominantly of iron or steel, the iron or steel was produced in the United States (produced in the United States means that all manufacturing processes of the iron or steel must take place in the United States, except metallurgical processes involving refinement of steel additives); and</p><p>(ii) The Buy American statute by providing a preference for unmanufactured construction material mined or produced in the United States over unmanufactured construction material mined or produced in a nondesignated country.</p><p>(2) The Contractor shall use only domestic construction material, Recovery Act designated country manufactured construction material, or designated country unmanufactured construction material in performing this contract, except as provided in paragraphs (b)(3) and (b)(4) of this clause.</p><p>(3) The requirement in paragraph (b)(2) of this clause does not apply to the construction materials or components listed by the Government as follows:</p><p>{{textbox_52.225-23[0]}}<br>[<i>Contracting Officer to list applicable excepted materials or indicate ''none''.</i>]</br></p><p>(4) The Contracting Officer may add other construction material to the list in paragraph (b)(3) of this clause if the Government determines that-</p><p>(i) The cost of domestic construction material would be unreasonable;</p><p>(A) The cost of domestic manufactured construction material is unreasonable when the cumulative cost of such material, when compared to the cost of comparable foreign manufactured construction material, other than Recovery Act designated country construction material, will increase the overall cost of the contract by more than 25 percent;</p><p>(B) The cost of domestic unmanufactured construction material is unreasonable when the cost of such material exceeds the cost of comparable foreign unmanufactured construction material, other than designated country construction material, by more than 6 percent;</p><p>(ii) The construction material is not mined, produced, or manufactured in the United States in sufficient and reasonably available commercial quantities of a satisfactory quality; or</p><p>(iii) The application of the restriction of section 1605 of the Recovery Act to a particular manufactured construction material would be inconsistent with the public interest or the application of the Buy American statute to a particular unmanufactured construction material would be impracticable or inconsistent with the public interest.</p><p>(c) <i>Request for determination of inapplicability of section 1605 of the Recovery Act or the Buy American statute.</i></p><p>(1)(i) Any Contractor request to use foreign construction material in accordance with paragraph (b)(4) of this clause shall include adequate information for Government evaluation of the request, including-</p><p>(A) A description of the foreign and domestic construction materials;</p><p>(B) Unit of measure;</p><p>(C) Quantity;</p><p>(D) Cost;</p><p>(E) Time of delivery or availability;</p><p>(F) Location of the construction project;</p><p>(G) Name and address of the proposed supplier; and</p><p>(H) A detailed justification of the reason for use of foreign construction materials cited in accordance with paragraph (b)(4) of this clause.</p><p>(ii) A request based on unreasonable cost shall include a reasonable survey of the market and a completed cost comparison table in the format in paragraph (d) of this clause.</p><p>(iii) The cost of construction material shall include all delivery costs to the construction site and any applicable duty.</p><p>(iv) Any Contractor request for a determination submitted after contract award shall explain why the Contractor could not reasonably foresee the need for such determination and could not have requested the determination before contract award. If the Contractor does not submit a satisfactory explanation, the Contracting Officer need not make a determination.</p><p>(2) If the Government determines after contract award that an exception to section 1605 of the Recovery Act or the Buy American statute applies and the Contracting Officer and the Contractor negotiate adequate consideration, the Contracting Officer will modify the contract to allow use of the foreign construction material. However, when the basis for the exception is the unreasonable cost of a domestic construction material, adequate consideration is not less than the differential established in paragraph (b)(4)(i) of this clause.</p><p>(3) Unless the Government determines that an exception to section 1605 of the Recovery Act or the Buy American statute applies, use of foreign construction material other than manufactured construction material from a Recovery Act designated country or unmanufactured construction material from a designated country is noncompliant with the applicable statute.</p><p>(d) <i>Data.</i> To permit evaluation of requests under paragraph (c) of this clause based on unreasonable cost, the Contractor shall include the following information and any applicable supporting data based on the survey of suppliers:</p><div><p>Foreign (Nondesignated Country) and Domestic Construction Materials Cost Comparison</p></div><div><table border="1" cellpadding="1" cellspacing="1" frame="void" width="100%"><tbody><tr><th scope="col">Construction material description</th><th scope="col">Unit of measure</th><th scope="col">Quantity</th><th scope="col">Cost (dollars)*</th></tr><tr><td align="left" scope="row"><i>Item 1:</i></td><td>{{textbox_52.225-23[1]}}</td><td>{{textbox_52.225-23[2]}}</td><td>{{textbox_52.225-23[3]}}</td></tr><tr><td align="left" scope="row">Foreign construction material</td><td align="center">{{textbox_52.225-23[4]}}</td><td align="center">{{textbox_52.225-23[5]}}</td><td align="center">{{textbox_52.225-23[6]}}</td></tr><tr><td align="left" scope="row">Domestic construction material</td><td align="center">{{textbox_52.225-23[7]}}</td><td align="center">{{textbox_52.225-23[8]}}</td><td align="center">{{textbox_52.225-23[9]}}</td></tr><tr><td align="left" scope="row"><i>Item 2:</i></td><td>{{textbox_52.225-23[10]}}</td><td>{{textbox_52.225-23[11]}}</td><td>{{textbox_52.225-23[12]}}</td></tr><tr><td align="left" scope="row">Foreign construction material</td><td align="center">{{textbox_52.225-23[13]}}</td><td align="center">{{textbox_52.225-23[14]}}</td><td align="center">{{textbox_52.225-23[15]}}</td></tr><tr><td align="left" scope="row">Domestic construction material</td><td align="center">{{textbox_52.225-23[16]}}</td><td align="center">{{textbox_52.225-23[17]}}</td><td align="center">{{textbox_52.225-23[18]}}</td></tr></tbody></table></div><div><p><i>[List name, address, telephone number, and contact for suppliers surveyed. Attach copy of response; if oral, attach summary.][Include other applicable supporting information.]</i></p><p><i>[*Include all delivery costs to the construction site.]</i></p></div>'
--               '<p>(a) <i>Definitions.</i> As used in this clause''??</p><p><i>Component</i> means an article, material, or supply incorporated directly into a construction material.</p><p><i>Construction material</i> means an article, material, or supply brought to the construction site by the Contractor or subcontractor for incorporation into the building or work. The term also includes an item brought to the site preassembled from articles, materials, or supplies. However, emergency life safety systems, such as emergency lighting, fire alarm, and audio evacuation systems, that are discrete systems incorporated into a public building or work and that are produced as complete systems, are evaluated as a single and distinct construction material regardless of when or how the individual parts or components of those systems are delivered to the construction site. </p><p><i>Designated country</i> means any of the following countries:</p><p>(1) A World Trade Organization Government Procurement Agreement (WTO GPA) country (Armenia, Aruba, Austria, Belgium, Bulgaria, Canada, Croatia, Cyprus, Czech Republic, Denmark, Estonia, Finland, France, Germany, Greece, Hong Kong, Hungary, Iceland, Ireland, Israel, Italy, Japan, Korea (Republic of), Latvia, Liechtenstein, Lithuania, Luxembourg, Malta, Netherlands, Norway, Poland, Portugal, Romania, Singapore, Slovak Republic, Slovenia, Spain, Sweden, Switzerland, Taiwan, or United Kingdom);</p><p>(2) A Free Trade Agreement (FTA) country (Australia, Bahrain, Canada, Chile, Colombia, Costa Rica, Dominican Republic, El Salvador, Guatemala, Honduras, Korea (Republic of), Mexico, Morocco, Nicaragua, Oman, Panama, Peru, or Singapore);</p><p>(3) A least developed country (Afghanistan, Angola, Bangladesh, Benin, Bhutan, Burkina Faso, Burundi, Cambodia, Central African Republic, Chad, Comoros, Democratic Republic of Congo, Djibouti,  Equatorial Guinea, Eritrea, Ethiopia, Gambia, Guinea, Guinea-Bissau, Haiti, Kiribati, Laos, Lesotho, Liberia, Madagascar, Malawi,  Mali, Mauritania, Mozambique, Nepal, Niger, Rwanda, Samoa, Sao Tome and Principe, Senegal, Sierra Leone, Solomon Islands, Somalia, South Sudan, Tanzania, Timor-Leste, Togo, Tuvalu, Uganda, Vanuatu, Yemen, or Zambia); or</p><p>(4) A Caribbean Basin country (Antigua and Barbuda, Aruba, Bahamas, Barbados, Belize, Bonaire, British Virgin Islands, Curacao, Dominica, Grenada, Guyana, Haiti, Jamaica, Montserrat, Saba, St. Kitts and Nevis, St. Lucia, St. Vincent and the Grenadines, Sint Eustatius, Sint Maarten, or Trinidad and Tobago).</p><p><i>Designated country construction material</i> means a construction material that is a WTO GPA country construction material, an FTA country construction material, a least developed country construction material, or a Caribbean Basin country construction material.</p><p><i>Domestic construction material</i> means the following:</p><p>(1) An unmanufactured construction material mined or produced in the United States. (The Buy American statute applies.)</p><p>(2) A manufactured construction material that is manufactured in the United States and, if the construction material consists wholly or predominantly of iron or steel, the iron or steel was produced in the United States. (Section 1605 of the Recovery Act applies.)</p><p><i>Foreign construction material</i> means a construction material other than a domestic construction material.</p><p><i>Free trade agreement (FTA) country construction material</i> means a construction material that''??</p><p>(1) Is wholly the growth, product, or manufacture of an FTA country; or</p><p>(2) In the case of a construction material that consists in whole or in part of materials from another country, has been substantially transformed in an FTA country into a new and different construction material distinct from the materials from which it was transformed.</p><p><i>Least developed country construction material</i> means a construction material that''??</p><p>(1) Is wholly the growth, product, or manufacture of a least developed country; or</p><p>(2) In the case of a construction material that consists in whole or in part of materials from another country, has been substantially transformed in a least developed country into a new and different construction material distinct from the materials from which it was transformed.</p><p><i>Manufactured construction material</i> means any construction material that is not unmanufactured construction material.</p><p><i>Nondesignated country</i> means a country other than the United States or a designated country.</p><p><i>Recovery Act designated country</i> means any of the following countries:</p><p>(1) A World Trade Organization Government Procurement Agreement (WTO GPA) country (Armenia, Aruba, Austria, Belgium, Bulgaria, Canada, Croatia, Cyprus, Czech Republic, Denmark, Estonia, Finland, France, Germany, Greece, Hong Kong, Hungary, Iceland, Ireland, Israel, Italy, Japan, Korea (Republic of), Latvia, Liechtenstein, Lithuania, Luxembourg, Malta, Netherlands, Norway, Poland, Portugal, Romania, Singapore, Slovak Republic, Slovenia, Spain, Sweden, Switzerland, Taiwan, or United Kingdom);</p><p>(2) A Free Trade Agreement country (FTA)(Australia, Bahrain, Canada, Chile, Colombia, Costa Rica, Dominican Republic, El Salvador, Guatemala, Honduras, Korea (Republic of), Mexico, Morocco, Nicaragua, Oman, Panama, Peru, or Singapore); or</p><p>(3) A least developed country (Afghanistan, Angola, Bangladesh, Benin, Bhutan, Burkina Faso, Burundi, Cambodia, Central African Republic, Chad, Comoros, Democratic Republic of Congo, Djibouti,  Equatorial Guinea, Eritrea, Ethiopia, Gambia, Guinea, Guinea-Bissau, Haiti, Kiribati, Laos, Lesotho, Liberia, Madagascar, Malawi,  Mali, Mauritania, Mozambique, Nepal, Niger, Rwanda, Samoa, Sao Tome and Principe, Senegal, Sierra Leone, Solomon Islands, Somalia, South Sudan, Tanzania, Timor-Leste, Togo, Tuvalu, Uganda, Vanuatu, Yemen, or Zambia).</p><p><i>Recovery Act designated country construction material</i> means a construction material that is a WTO GPA country construction material, an FTA country construction material, or a least developed country construction material.</p><p><i>Steel</i> means an alloy that includes at least 50 percent iron, between .02 and 2 percent carbon, and may include other elements.</p><p><i>United States</i> means the 50 States, the District of Columbia, and outlying areas.</p><p><i>Unmanufactured construction material</i> means raw material brought to the construction site for incorporation into the building or work that has not been''??</p><p>(1) Processed into a specific form and shape; or</p><p>(2) Combined with other raw material to create a material that has different properties than the properties of the individual raw materials.</p><p><i>WTO GPA country construction material</i> means a construction material that''??</p><p>(1) Is wholly the growth, product, or manufacture of a WTO GPA country; or</p><p>(2) In the case of a construction material that consists in whole or in part of materials from another country, has been substantially transformed in a WTO GPA country into a new and different construction material distinct from the materials from which it was transformed.</p><p>(b) <i>Construction materials.</i> (1) The restrictions of section 1605 of the American Recovery and Reinvestment Act of 2009 (Pub. L. 111-5) (Recovery Act) do not apply to Recovery Act designated country manufactured construction material. The restrictions of the Buy American statute do not apply to designated country unmanufactured construction material. Consistent with U.S. obligations under international agreements, this clause implements''??</p><p>(i) Section 1605 of the Recovery Act by requiring, unless an exception applies, that all manufactured construction material in the project is manufactured in the United States and, if the construction material consists wholly or predominantly of iron or steel, the iron or steel was produced in the United States (produced in the United States means that all manufacturing processes of the iron or steel must take place in the United States, except metallurgical processes involving refinement of steel additives); and</p><p>(ii) The Buy American statute by providing a preference for unmanufactured construction material mined or produced in the United States over unmanufactured construction material mined or produced in a nondesignated country.</p><p>(2) The Contractor shall use only domestic construction material, Recovery Act designated country manufactured construction material, or designated country unmanufactured construction material in performing this contract, except as provided in paragraphs (b)(3) and (b)(4) of this clause.</p><p>(3) The requirement in paragraph (b)(2) of this clause does not apply to the construction materials or components listed by the Government as follows:</p><p>[<i>Contracting Officer to list applicable excepted materials or indicate ''??none''?.</i>]</p><p>(4) The Contracting Officer may add other construction material to the list in paragraph (b)(3) of this clause if the Government determines that''??</p><p>(i) The cost of domestic construction material would be unreasonable;</p><p>(A) The cost of domestic manufactured construction material is unreasonable when the cumulative cost of such material, when compared to the cost of comparable foreign manufactured construction material, other than Recovery Act designated country construction material, will increase the overall cost of the contract by more than 25 percent;</p><p>(B) The cost of domestic unmanufactured construction material is unreasonable when the cost of such material exceeds the cost of comparable foreign unmanufactured construction material, other than designated country construction material, by more than 6 percent;</p><p>(ii) The construction material is not mined, produced, or manufactured in the United States in sufficient and reasonably available commercial quantities of a satisfactory quality; or</p><p>(iii) The application of the restriction of section 1605 of the Recovery Act to a particular manufactured construction material would be inconsistent with the public interest or the application of the Buy American statute to a particular unmanufactured construction material would be impracticable or inconsistent with the public interest.</p><p>(c) <i>Request for determination of inapplicability of section 1605 of the Recovery Act or the Buy American statute.</i></p><p>(1)(i) Any Contractor request to use foreign construction material in accordance with paragraph (b)(4) of this clause shall include adequate information for Government evaluation of the request, including''??</p><p>(A) A description of the foreign and domestic construction materials;</p><p>(B) Unit of measure;</p><p>(C) Quantity;</p><p>(D) Cost;</p><p>(E) Time of delivery or availability;</p><p>(F) Location of the construction project;</p><p>(G) Name and address of the proposed supplier; and</p><p>(H) A detailed justification of the reason for use of foreign construction materials cited in accordance with paragraph (b)(4) of this clause.</p><p>(ii) A request based on unreasonable cost shall include a reasonable survey of the market and a completed cost comparison table in the format in paragraph (d) of this clause.</p><p>(iii) The cost of construction material shall include all delivery costs to the construction site and any applicable duty.</p><p>(iv) Any Contractor request for a determination submitted after contract award shall explain why the Contractor could not reasonably foresee the need for such determination and could not have requested the determination before contract award. If the Contractor does not submit a satisfactory explanation, the Contracting Officer need not make a determination.</p><p>(2) If the Government determines after contract award that an exception to section 1605 of the Recovery Act or the Buy American statute applies and the Contracting Officer and the Contractor negotiate adequate consideration, the Contracting Officer will modify the contract to allow use of the foreign construction material. However, when the basis for the exception is the unreasonable cost of a domestic construction material, adequate consideration is not less than the differential established in paragraph (b)(4)(i) of this clause.</p><p>(3) Unless the Government determines that an exception to section 1605 of the Recovery Act or the Buy American statute applies, use of foreign construction material other than manufactured construction material from a Recovery Act designated country or unmanufactured construction material from a designated country is noncompliant with the applicable statute.</p><p>(d) <i>Data.</i> To permit evaluation of requests under paragraph (c) of this clause based on unreasonable cost, the Contractor shall include the following information and any applicable supporting data based on the survey of suppliers:</p><div><p>Foreign (Nondesignated Country) and Domestic Construction Materials Cost Comparison</p></div><div><table border="&quot;1&quot;" cellpadding="&quot;1&quot;" cellspacing="&quot;1&quot;" frame="&quot;void&quot;" width="&quot;100%&quot;"><tbody><tr><th scope="&quot;col&quot;">Construction material description</th><th scope="&quot;col&quot;">Unit of measure</th><th scope="&quot;col&quot;">Quantity</th><th scope="&quot;col&quot;">Cost (dollars)*</th></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;"><i>Item 1:</i></td><td align="&quot;center&quot;"></td><td align="&quot;center&quot;"></td><td align="&quot;center&quot;"></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;" 4em"="">Foreign construction material</td><td align="&quot;center&quot;">{{textbox_52.225-23[0]}}</td><td align="&quot;center&quot;">{{textbox_52.225-23[1]}}</td><td align="&quot;center&quot;">{{textbox_52.225-23[2]}}</td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;" 4em"="">Domestic construction material</td><td align="&quot;center&quot;">{{textbox_52.225-23[3]}}</td><td align="&quot;center&quot;">{{textbox_52.225-23[4]}}</td><td align="&quot;center&quot;">{{textbox_52.225-23[5]}}</td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;"><i>Item 2:</i></td><td align="&quot;center&quot;"></td><td align="&quot;center&quot;"></td><td align="&quot;center&quot;"></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;" 4em"="">Foreign construction material</td><td align="&quot;center&quot;">{{textbox_52.225-23[6]}}</td><td align="&quot;center&quot;">{{textbox_52.225-23[7]}}</td><td align="&quot;center&quot;">{{textbox_52.225-23[8]}}</td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;" 4em"="">Domestic construction material</td><td align="&quot;center&quot;">{{textbox_52.225-23[9]}}</td><td align="&quot;center&quot;">{{textbox_52.225-23[10]}}</td><td align="&quot;center&quot;">{{textbox_52.225-23[11]}}</td></tr></tbody></table></div><div><p><i>[List name, address, telephone number, and contact for suppliers surveyed. Attach copy of response; if oral, attach summary.][Include other applicable supporting information.]</i></p><p><i>[*Include all delivery costs to the construction site.]</i></p></div>'
WHERE clause_id = 92818; -- 52.225-23
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (92818, 'textbox_52.225-23[0]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (92818, 'textbox_52.225-23[1]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (92818, 'textbox_52.225-23[2]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (92818, 'textbox_52.225-23[3]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (92818, 'textbox_52.225-23[4]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (92818, 'textbox_52.225-23[5]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (92818, 'textbox_52.225-23[6]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (92818, 'textbox_52.225-23[7]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (92818, 'textbox_52.225-23[8]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (92818, 'textbox_52.225-23[9]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (92818, 'textbox_52.225-23[10]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (92818, 'textbox_52.225-23[11]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (92818, 'textbox_52.225-23[12]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (92818, 'textbox_52.225-23[13]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (92818, 'textbox_52.225-23[14]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (92818, 'textbox_52.225-23[15]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (92818, 'textbox_52.225-23[16]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (92818, 'textbox_52.225-23[17]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (92818, 'textbox_52.225-23[18]', 'S', 100);

UPDATE Clauses
SET effective_date = '2014-05-01' -- '2014-01-01'
WHERE clause_id = 92821; -- 52.225-24

UPDATE Clauses
SET effective_date = '2012-12-01' -- '2012-01-01'
WHERE clause_id = 92822; -- 52.225-25

UPDATE Clauses
SET effective_date = '2013-07-01' -- '2013-01-01'
WHERE clause_id = 92823; -- 52.225-26

UPDATE Clauses
SET effective_date = '2014-05-01' -- '2014-01-01'
WHERE clause_id = 92830; -- 52.225-3

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1225_64&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?node=se48.2.52_1225_64&amp;rgn=div8'
, effective_date = '2014-05-01' -- '2014-01-01'
, clause_data = '<p>(a) The offeror certifies that each end product, except those listed in paragraph (b) or (c) of this provision, is a domestic end product and that for other than COTS items, the offeror has considered components of unknown origin to have been mined, produced, or manufactured outside the United States. The terms ''Bahrainian, Moroccan, Omani, Panamanian, or Peruvian end product,'' ''commercially available off-the-shelf (COTS) item,'' ''component,'' ''domestic end product,'' ''end product,'' ''foreign end product,'' ''Free Trade Agreement country,'' ''Free Trade Agreement country end product,'' ''Israeli end product,'' and ''United States'' are defined in the clause of this solicitation entitled ''Buy American-Free Trade Agreements-Israeli Trade Act.''</p><p>(b) The offeror certifies that the following supplies are Free Trade Agreement country end products (other than Bahrainian, Moroccan, Omani, Panamanian, or Peruvian end products) or Israeli end products as defined in the clause of this solicitation entitled ''Buy American-Free Trade Agreements-Israeli Trade Act'':</p><div><div><p>Free Trade Agreement Country End Products (Other than Bahrainian, Moroccan, Omani, Panamanian, or Peruvian End Products) or Israeli End Products:</p></div><div><table border="1" cellpadding="1" cellspacing="1" frame="void" width="100%"><tbody><tr><th scope="col">Line Item No.</th><th scope="col">&nbsp;&nbsp;</th><th scope="col">Country of<br>Origin</br></th></tr><tr><td align="left" scope="row">&nbsp;&nbsp;&nbsp;</td><td align="left">&nbsp;&nbsp;&nbsp;</td><td align="left"></td></tr><tr><td align="left" scope="row">&nbsp;&nbsp;&nbsp;</td><td align="left">&nbsp;&nbsp;&nbsp;</td><td align="left"></td></tr><tr><td align="left" scope="row">&nbsp;&nbsp;&nbsp;</td><td align="left">&nbsp;&nbsp;&nbsp;</td><td align="left"></td></tr><tr><td align="center" colspan="3" scope="row">[List as necessary]</td></tr></tbody></table></div></div><p>(c) The offeror shall list those supplies that are foreign end products (other than those listed in paragraph (b) of this provision) as defined in the clause of this solicitation entitled ''Buy American-Free Trade Agreements-Israeli Trade Act.'' The offeror shall list as other foreign end products those end products manufactured in the United States that do not qualify as domestic end products, <i>i.e.</i>, an end product that is not a COTS item and does not meet the component test in paragraph (2) of the definition of ''domestic end product.''</p><p>Other Foreign End Products:</p><p>LINE ITEM NO. COUNTRY OF ORIGIN</p><p>________ ________</p><p>________ ________</p><p>________ ________</p><p>[List as necessary]</p><p>(d) The Government will evaluate offers in accordance with the policies and procedures of Part 25 of the Federal Acquisition Regulation.</p>'
--               '<p>(a) The offeror certifies that each end product, except those listed in paragraph (b) or (c) of this provision, is a domestic end product and that for other than COTS items, the offeror has considered components of unknown origin to have been mined, produced, or manufactured outside the United States. The terms âBahrainian, Moroccan, Omani, Panamanian, or Peruvian end product,â âcommercially available off-the-shelf (COTS) item,â âcomponent,â âdomestic end product,â âend product,â âforeign end product,â âFree Trade Agreement country,â âFree Trade Agreement country end product,â âIsraeli end product,â and âUnited Statesâ are defined in the clause of this solicitation entitled âBuy American-Free Trade Agreements-Israeli Trade Act.â</p><p>(b) The offeror certifies that the following supplies are Free Trade Agreement country end products (other than Bahrainian, Moroccan, Omani, Panamanian, or Peruvian end products) or Israeli end products as defined in the clause of this solicitation entitled âBuy American-Free Trade Agreements-Israeli Trade Actâ:</p>\n<div><div><p>Free Trade Agreement Country End Products (Other than Bahrainian, Moroccan, Omani, Panamanian, or Peruvian End Products) or Israeli End Products:</p></div><div><table border="&quot;1&quot;" cellpadding="&quot;1&quot;" cellspacing="&quot;1&quot;" frame="&quot;void&quot;" width="&quot;100%&quot;"><tbody><tr><th scope="&quot;col&quot;">Line Item No.</th><th scope="&quot;col&quot;">&nbsp;&nbsp;</th><th scope="&quot;col&quot;">Country of<br>Origin</br></th></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">&nbsp;&nbsp;&nbsp;</td><td align="&quot;left&quot;">&nbsp;&nbsp;&nbsp;</td><td align="&quot;left&quot;"></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">&nbsp;&nbsp;&nbsp;</td><td align="&quot;left&quot;">&nbsp;&nbsp;&nbsp;</td><td align="&quot;left&quot;"></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">&nbsp;&nbsp;&nbsp;</td><td align="&quot;left&quot;">&nbsp;&nbsp;&nbsp;</td><td align="&quot;left&quot;"></td></tr><tr><td align="&quot;center&quot;" colspan="&quot;3&quot;" scope="&quot;row&quot;">[List as necessary]</td></tr></tbody></table></div></div><p>(c) The offeror shall list those supplies that are foreign end products (other than those listed in paragraph (b) of this provision) as defined in the clause of this solicitation entitled âBuy American-Free Trade Agreements-Israeli Trade Act.â The offeror shall list as other foreign end products those end products manufactured in the United States that do not qualify as domestic end products, <i>i.e.</i>, an end product that is not a COTS item and does not meet the component test in paragraph (2) of the definition of âdomestic end product.â</p><p>Other Foreign End Products:</p><p>LINE ITEM NO. COUNTRY OF ORIGIN</p><p>{{textbox_52.225-4[0]}} {{textbox_52.225-4[1]}}</p><p>{{textbox_52.225-4[2]}} {{textbox_52.225-4[3]}}</p><p>{{textbox_52.225-4[4]}} {{textbox_52.225-4[5]}}</p><p>[List as necessary]</p><p>(d) The Government will evaluate offers in accordance with the policies and procedures of Part 25 of the Federal Acquisition Regulation.</p>'
WHERE clause_id = 92835; -- 52.225-4

UPDATE Clauses
SET effective_date = '2013-11-01' -- '2013-01-01'
WHERE clause_id = 92836; -- 52.225-5

UPDATE Clauses
SET effective_date = '2014-05-01' -- '2014-01-01'
WHERE clause_id = 92837; -- 52.225-6

UPDATE Clauses
SET effective_date = '2014-05-01' -- '2014-01-01'
WHERE clause_id = 92838; -- 52.225-7

UPDATE Clauses
SET effective_date = '2010-10-01' -- '2010-01-01'
WHERE clause_id = 92839; -- 52.225-8

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1225_69&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?node=se48.2.52_1225_69&amp;rgn=div8'
, effective_date = '2014-05-01' -- '2014-01-01'
, clause_data = '<p>(a) <i>Definitions.</i> As used in this clause-</p><p><i>Commercially available off-the-shelf (COTS) item</i>-(1) Means any item of supply (including construction material) that is-</p><p>(i) A commercial item (as defined in paragraph (1) of the definition at FAR 2.101);</p><p>(ii) Sold in substantial quantities in the commercial marketplace; and</p><p>(iii) Offered to the Government, under a contract or subcontract at any tier, without modification, in the same form in which it is sold in the commercial marketplace; and</p><p>(2) Does not include bulk cargo, as defined in 46 U.S.C. 40102(4), such as agricultural products and petroleum products.</p><p><i>Construction material</i> means an article, material, or supply brought to the construction site by the Contractor or a subcontractor for incorporation into the building or work. The term also includes an item brought to the site preassembled from articles, materials, or supplies. However, emergency life safety systems, such as emergency lighting, fire alarm, and audio evacuation systems, that are discrete systems incorporated into a public building or work and that are produced as complete systems, are evaluated as a single and distinct construction material regardless of when or how the individual parts or components of those systems are delivered to the construction site. Materials purchased directly by the Government are supplies, not construction material.</p><p><i>Cost of components</i> means-</p><p>(1) For components purchased by the Contractor, the acquisition cost, including transportation costs to the place of incorporation into the construction material (whether or not such costs are paid to a domestic firm), and any applicable duty (whether or not a duty-free entry certificate is issued); or</p><p>(2) For components manufactured by the Contractor, all costs associated with the manufacture of the component, including transportation costs as described in paragraph (1) of this definition, plus allocable overhead costs, but excluding profit. Cost of components does not include any costs associated with the manufacture of the construction material.</p><p><i>Domestic construction material</i> means-</p><p>(1) An unmanufactured construction material mined or produced in the United States;</p><p>(2) A construction material manufactured in the United States, if-</p><p>(i) The cost of its components mined, produced, or manufactured in the United States exceeds 50 percent of the cost of all its components. Components of foreign origin of the same class or kind for which nonavailability determinations have been made are treated as domestic; or</p><p>(ii) The construction material is a COTS item.</p><p><i>Foreign construction material</i> means a construction material other than a domestic construction material.</p><p><i>United States</i> means the 50 States, the District of Columbia, and outlying areas.</p><p>(b) <i>Domestic preference.</i> (1) This clause implements 41 U.S.C. chapter 83, Buy American, by providing a preference for domestic construction material. In accordance with 41 U.S.C. 1907, the component test of the Buy American statute is waived for construction material that is a COTS item. (See FAR 12.505(a)(2)). The Contractor shall use only domestic construction material in performing this contract, except as provided in paragraphs (b)(2) and (b)(3) of this clause.</p><p>(2) This requirement does not apply to information technology that is a commercial item or to the construction materials or components listed by the Government as follows:</p><p>(3) The Contracting Officer may add other foreign construction material to the list in paragraph (b)(2) of this clause if the Government determines that-</p><p>(i) The cost of domestic construction material would be unreasonable. The cost of a particular domestic construction material subject to the requirements of the Buy American statute is unreasonable when the cost of such material exceeds the cost of foreign material by more than 6 percent; </p><p>(ii) The application of the restriction of the Buy American statute to a particular construction material would be impracticable or inconsistent with the public interest; or</p><p>(iii) The construction material is not mined, produced, or manufactured in the United States in sufficient and reasonably available commercial quantities of a satisfactory quality.</p><p>(c) <i>Request for determination of inapplicability of the Buy American statute.</i> (1)(i) Any Contractor request to use foreign construction material in accordance with paragraph (b)(3) of this clause shall include adequate information for Government evaluation of the request, including-</p><p>(A) A description of the foreign and domestic construction materials;</p><p>(B) Unit of measure;</p><p>(C) Quantity;</p><p>(D) Price;</p><p>(E) Time of delivery or availability;</p><p>(F) Location of the construction project;</p><p>(G) Name and address of the proposed supplier; and</p><p>(H) A detailed justification of the reason for use of foreign construction materials cited in accordance with paragraph (b)(3) of this clause.</p><p>(ii) A request based on unreasonable cost shall include a reasonable survey of the market and a completed price comparison table in the format in paragraph (d) of this clause.</p><p>(iii) The price of construction material shall include all delivery costs to the construction site and any applicable duty (whether or not a duty-free certificate may be issued).</p><p>(iv) Any Contractor request for a determination submitted after contract award shall explain why the Contractor could not reasonably foresee the need for such determination and could not have requested the determination before contract award. If the Contractor does not submit a satisfactory explanation, the Contracting Officer need not make a determination.</p><p>(2) If the Government determines after contract award that an exception to the Buy American statute applies and the Contracting Officer and the Contractor negotiate adequate consideration, the Contracting Officer will modify the contract to allow use of the foreign construction material. However, when the basis for the exception is the unreasonable price of a domestic construction material, adequate consideration is not less than the differential established in paragraph (b)(3)(i) of this clause.</p><p>(3) Unless the Government determines that an exception to the Buy American statute applies, use of foreign construction material is noncompliant with the Buy American statute or Balance of Payments Program.</p><p>(d) <i>Data.</i> To permit evaluation of requests under paragraph (c) of this clause based on unreasonable cost, the Contractor shall include the following information and any applicable supporting data based on the survey of suppliers:</p><div><p>Foreign and Domestic Construction Materials Price Comparison</p></div><div><table border="1" cellpadding="1" cellspacing="1" frame="void" width="100%"><tbody><tr><th scope="col">Construction material description</th><th scope="col">Unit of measure</th><th scope="col">Quantity</th><th scope="col">Price (dollars)<sup>1</sup></th></tr><tr><td align="left" scope="row">Item 1</td><td>{{textbox_52.225-9[0]}}</td><td>{{textbox_52.225-9[1]}}</td><td>{{textbox_52.225-9[2]}}</td></tr><tr><td align="left" scope="row">Foreign construction material</td><td>{{textbox_52.225-9[3]}}</td><td>{{textbox_52.225-9[4]}}</td><td>{{textbox_52.225-9[5]}}</td></tr><tr><td align="left" scope="row">Domestic construction material</td><td>{{textbox_52.225-9[6]}}</td><td>{{textbox_52.225-9[7]}}</td><td>{{textbox_52.225-9[8]}}</td></tr><tr><td align="left" scope="row">Item 2</td><td>{{textbox_52.225-9[9]}}</td><td>{{textbox_52.225-9[10]}}</td><td>{{textbox_52.225-9[11]}}</td></tr><tr><td align="left" scope="row">Foreign construction material</td><td>{{textbox_52.225-9[12]}}</td><td>{{textbox_52.225-9[13]}}</td><td>{{textbox_52.225-9[14]}}</td></tr><tr><td align="left" scope="row">Domestic construction material</td><td>{{textbox_52.225-9[15]}}</td><td>{{textbox_52.225-9[16]}}</td><td>{{textbox_52.225-9[17]}}</td></tr></tbody></table></div><div><p>Include all delivery costs to the construction site and any applicable duty (whether or not a duty-free entry certificate is issued).</p><p>List name, address, telephone number, and contact for suppliers surveyed. Attach copy of response; if oral, attach summary.</p><p>Include other applicable supporting information.</p></div>'
--               '<p>(a) <i>Definitions.</i> As used in this clause''??</p><p><i>Commercially available off-the-shelf (COTS) item</i>''??(1) Means any item of supply (including construction material) that is''??</p><p>(i) A commercial item (as defined in paragraph (1) of the definition at FAR 2.101);</p><p>(ii) Sold in substantial quantities in the commercial marketplace; and</p><p>(iii) Offered to the Government, under a contract or subcontract at any tier, without modification, in the same form in which it is sold in the commercial marketplace; and</p><p>(2) Does not include bulk cargo, as defined in 46 U.S.C. 40102(4), such as agricultural products and petroleum products.</p><p><i>Construction material</i> means an article, material, or supply brought to the construction site by the Contractor or a subcontractor for incorporation into the building or work. The term also includes an item brought to the site preassembled from articles, materials, or supplies. However, emergency life safety systems, such as emergency lighting, fire alarm, and audio evacuation systems, that are discrete systems incorporated into a public building or work and that are produced as complete systems, are evaluated as a single and distinct construction material regardless of when or how the individual parts or components of those systems are delivered to the construction site. Materials purchased directly by the Government are supplies, not construction material.</p><p><i>Cost of components</i> means''??</p><p>(1) For components purchased by the Contractor, the acquisition cost, including transportation costs to the place of incorporation into the construction material (whether or not such costs are paid to a domestic firm), and any applicable duty (whether or not a duty-free entry certificate is issued); or</p><p>(2) For components manufactured by the Contractor, all costs associated with the manufacture of the component, including transportation costs as described in paragraph (1) of this definition, plus allocable overhead costs, but excluding profit. Cost of components does not include any costs associated with the manufacture of the construction material.</p><p><i>Domestic construction material</i> means''??</p><p>(1) An unmanufactured construction material mined or produced in the United States;</p><p>(2) A construction material manufactured in the United States, if''??</p><p>(i) The cost of its components mined, produced, or manufactured in the United States exceeds 50 percent of the cost of all its components. Components of foreign origin of the same class or kind for which nonavailability determinations have been made are treated as domestic; or</p><p>(ii) The construction material is a COTS item.</p><p><i>Foreign construction material</i> means a construction material other than a domestic construction material.</p><p><i>United States</i> means the 50 States, the District of Columbia, and outlying areas.</p><p>(b) <i>Domestic preference.</i> (1) This clause implements 41 U.S.C. chapter 83, Buy American, by providing a preference for domestic construction material. In accordance with 41 U.S.C. 1907, the component test of the Buy American statute is waived for construction material that is a COTS item. (See FAR 12.505(a)(2)). The Contractor shall use only domestic construction material in performing this contract, except as provided in paragraphs (b)(2) and (b)(3) of this clause.</p><p>(2) This requirement does not apply to information technology that is a commercial item or to the construction materials or components listed by the Government as follows:</p><p>(3) The Contracting Officer may add other foreign construction material to the list in paragraph (b)(2) of this clause if the Government determines that''??</p><p>(i) The cost of domestic construction material would be unreasonable. The cost of a particular domestic construction material subject to the requirements of the Buy American statute is unreasonable when the cost of such material exceeds the cost of foreign material by more than 6 percent; </p><p>(ii) The application of the restriction of the Buy American statute to a particular construction material would be impracticable or inconsistent with the public interest; or</p><p>(iii) The construction material is not mined, produced, or manufactured in the United States in sufficient and reasonably available commercial quantities of a satisfactory quality.</p><p>(c) <i>Request for determination of inapplicability of the Buy American statute.</i> (1)(i) Any Contractor request to use foreign construction material in accordance with paragraph (b)(3) of this clause shall include adequate information for Government evaluation of the request, including''??</p><p>(A) A description of the foreign and domestic construction materials;</p><p>(B) Unit of measure;</p><p>(C) Quantity;</p><p>(D) Price;</p><p>(E) Time of delivery or availability;</p><p>(F) Location of the construction project;</p><p>(G) Name and address of the proposed supplier; and</p><p>(H) A detailed justification of the reason for use of foreign construction materials cited in accordance with paragraph (b)(3) of this clause.</p><p>(ii) A request based on unreasonable cost shall include a reasonable survey of the market and a completed price comparison table in the format in paragraph (d) of this clause.</p><p>(iii) The price of construction material shall include all delivery costs to the construction site and any applicable duty (whether or not a duty-free certificate may be issued).</p><p>(iv) Any Contractor request for a determination submitted after contract award shall explain why the Contractor could not reasonably foresee the need for such determination and could not have requested the determination before contract award. If the Contractor does not submit a satisfactory explanation, the Contracting Officer need not make a determination.</p><p>(2) If the Government determines after contract award that an exception to the Buy American statute applies and the Contracting Officer and the Contractor negotiate adequate consideration, the Contracting Officer will modify the contract to allow use of the foreign construction material. However, when the basis for the exception is the unreasonable price of a domestic construction material, adequate consideration is not less than the differential established in paragraph (b)(3)(i) of this clause.</p><p>(3) Unless the Government determines that an exception to the Buy American statute applies, use of foreign construction material is noncompliant with the Buy American statute or Balance of Payments Program.</p><p>(d) <i>Data.</i> To permit evaluation of requests under paragraph (c) of this clause based on unreasonable cost, the Contractor shall include the following information and any applicable supporting data based on the survey of suppliers:</p><div><p>Foreign and Domestic Construction Materials Price Comparison</p></div><div><table border="&quot;1&quot;" cellpadding="&quot;1&quot;" cellspacing="&quot;1&quot;" frame="&quot;void&quot;" width="&quot;100%&quot;"><tbody><tr><th scope="&quot;col&quot;">Construction material description</th><th scope="&quot;col&quot;">Unit of measure</th><th scope="&quot;col&quot;">Quantity</th><th scope="&quot;col&quot;">Price (dollars)<sup>1</sup></th></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">Item 1</td><td align="&quot;right&quot;"></td><td align="&quot;right&quot;"></td><td align="&quot;right&quot;"></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;" 4em"="">Foreign construction material</td><td align="&quot;right&quot;"></td><td align="&quot;right&quot;"></td><td align="&quot;right&quot;"></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;" 4em"="">Domestic construction material</td><td align="&quot;right&quot;"></td><td align="&quot;right&quot;"></td><td align="&quot;right&quot;"></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">Item 2</td><td align="&quot;right&quot;"></td><td align="&quot;right&quot;"></td><td align="&quot;right&quot;"></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;" 4em"="">Foreign construction material</td><td align="&quot;right&quot;"></td><td align="&quot;right&quot;"></td><td align="&quot;right&quot;"></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;" 4em"="">Domestic construction material</td><td align="&quot;right&quot;"></td><td align="&quot;right&quot;"></td><td align="&quot;right&quot;"></td></tr></tbody></table></div><div><p>Include all delivery costs to the construction site and any applicable duty (whether or not a duty-free entry certificate is issued).</p><p>List name, address, telephone number, and contact for suppliers surveyed. Attach copy of response; if oral, attach summary.</p><p>Include other applicable supporting information.</p></div>'
WHERE clause_id = 92840; -- 52.225-9
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (92840, 'textbox_52.225-9[0]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (92840, 'textbox_52.225-9[1]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (92840, 'textbox_52.225-9[2]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (92840, 'textbox_52.225-9[3]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (92840, 'textbox_52.225-9[4]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (92840, 'textbox_52.225-9[5]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (92840, 'textbox_52.225-9[6]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (92840, 'textbox_52.225-9[7]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (92840, 'textbox_52.225-9[8]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (92840, 'textbox_52.225-9[9]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (92840, 'textbox_52.225-9[10]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (92840, 'textbox_52.225-9[11]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (92840, 'textbox_52.225-9[12]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (92840, 'textbox_52.225-9[13]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (92840, 'textbox_52.225-9[14]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (92840, 'textbox_52.225-9[15]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (92840, 'textbox_52.225-9[16]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (92840, 'textbox_52.225-9[17]', 'S', 100);

UPDATE Clauses
SET effective_date = '2000-06-01' -- '2000-01-01'
WHERE clause_id = 92841; -- 52.226-1

UPDATE Clauses
SET effective_date = '2014-10-01' -- '2014-01-01'
WHERE clause_id = 92842; -- 52.226-2

UPDATE Clauses
SET effective_date = '2007-11-01' -- '2007-01-01'
WHERE clause_id = 92843; -- 52.226-3

UPDATE Clauses
SET effective_date = '2007-11-01' -- '2007-01-01'
WHERE clause_id = 92844; -- 52.226-4

UPDATE Clauses
SET effective_date = '2007-11-01' -- '2007-01-01'
WHERE clause_id = 92845; -- 52.226-5

UPDATE Clauses
SET effective_date = '2014-05-01' -- '2014-01-01'
WHERE clause_id = 92846; -- 52.226-6

UPDATE Clauses
SET effective_date = '2007-12-01' -- '2007-01-01'
WHERE clause_id = 92849; -- 52.227-1

UPDATE Clauses
SET effective_date = '2007-12-01' -- '2007-01-01'
WHERE clause_id = 92850; -- 52.227-10

UPDATE Clauses
SET effective_date = '2014-05-01' -- '2014-01-01'
WHERE clause_id = 92856; -- 52.227-11

UPDATE Clauses
SET effective_date = '2007-12-01' -- '2007-01-01'
WHERE clause_id = 92859; -- 52.227-13

UPDATE Clauses
SET effective_date = '2014-05-01' -- '2014-01-01'
WHERE clause_id = 92865; -- 52.227-14

UPDATE Clauses
SET effective_date = '2007-12-01' -- '2007-01-01'
WHERE clause_id = 92866; -- 52.227-15

UPDATE Clauses
SET effective_date = '1987-06-01' -- '1987-01-01'
WHERE clause_id = 92867; -- 52.227-16

UPDATE Clauses
SET effective_date = '2007-12-01' -- '2007-01-01'
WHERE clause_id = 92868; -- 52.227-17

UPDATE Clauses
SET effective_date = '2007-12-01' -- '2007-01-01'
WHERE clause_id = 92869; -- 52.227-18

UPDATE Clauses
SET effective_date = '2007-12-01' -- '2007-01-01'
WHERE clause_id = 92870; -- 52.227-19

UPDATE Clauses
SET effective_date = '2007-12-01' -- '2007-01-01'
WHERE clause_id = 92871; -- 52.227-2

UPDATE Clauses
SET effective_date = '2014-05-01' -- '2014-01-01'
WHERE clause_id = 92872; -- 52.227-20

UPDATE Clauses
SET effective_date = '2014-05-01' -- '2014-01-01'
WHERE clause_id = 92873; -- 52.227-21

UPDATE Clauses
SET effective_date = '1987-06-01' -- '1987-01-01'
WHERE clause_id = 92874; -- 52.227-22

UPDATE Clauses
SET effective_date = '1987-06-01' -- '1987-01-01'
WHERE clause_id = 92875; -- 52.227-23

UPDATE Clauses
SET effective_date = '1984-04-01' -- '1984-01-01'
WHERE clause_id = 92879; -- 52.227-3

UPDATE Clauses
SET effective_date = '2007-12-01' -- '2007-01-01'
WHERE clause_id = 92881; -- 52.227-4

UPDATE Clauses
SET effective_date = '1984-04-01' -- '1984-01-01'
WHERE clause_id = 92882; -- 52.227-5

UPDATE Clauses
SET effective_date = '1984-04-01' -- '1984-01-01'
WHERE clause_id = 92884; -- 52.227-6

UPDATE Clauses
SET effective_date = '1984-04-01' -- '1984-01-01'
WHERE clause_id = 92885; -- 52.227-7

UPDATE Clauses
SET effective_date = '1984-04-01' -- '1984-01-01'
WHERE clause_id = 92886; -- 52.227-9

UPDATE Clauses
SET effective_date = '1996-09-01' -- '1996-01-01'
WHERE clause_id = 92887; -- 52.228-1

UPDATE Clauses
SET effective_date = '1984-04-01' -- '1984-01-01'
WHERE clause_id = 92888; -- 52.228-10

UPDATE Clauses
SET effective_date = '2014-05-01' -- '2014-01-01'
WHERE clause_id = 92890; -- 52.228-12

UPDATE Clauses
SET effective_date = '2000-07-01' -- '2000-01-01'
WHERE clause_id = 92891; -- 52.228-13

UPDATE Clauses
SET effective_date = '2014-11-01' -- '2014-01-01'
WHERE clause_id = 92892; -- 52.228-14

UPDATE Clauses
SET effective_date = '2010-10-01' -- '2010-01-01'
WHERE clause_id = 92893; -- 52.228-15

UPDATE Clauses
SET effective_date = '2006-11-01' -- '2006-01-01'
WHERE clause_id = 92895; -- 52.228-16

UPDATE Clauses
SET effective_date = '1997-10-01' -- '1997-01-01'
WHERE clause_id = 92896; -- 52.228-2

UPDATE Clauses
SET effective_date = '2014-07-01' -- '2014-01-01'
WHERE clause_id = 92897; -- 52.228-3

UPDATE Clauses
SET effective_date = '1984-04-01' -- '1984-01-01'
WHERE clause_id = 92898; -- 52.228-4

UPDATE Clauses
SET effective_date = '1996-03-01' -- '1996-01-01'
WHERE clause_id = 92900; -- 52.228-7

UPDATE Clauses
SET effective_date = '1999-05-01' -- '1999-01-01'
WHERE clause_id = 92901; -- 52.228-8

UPDATE Clauses
SET effective_date = '1999-05-01' -- '1999-01-01'
WHERE clause_id = 92902; -- 52.228-9

UPDATE Clauses
SET effective_date = '1984-04-01' -- '1984-01-01'
WHERE clause_id = 92903; -- 52.229-1

UPDATE Clauses
SET effective_date = '2003-04-01' -- '2003-01-01'
WHERE clause_id = 92904; -- 52.229-10

UPDATE Clauses
SET effective_date = '1984-04-01' -- '1984-01-01'
WHERE clause_id = 92906; -- 52.229-2

UPDATE Clauses
SET effective_date = '2013-02-01' -- '2013-01-01'
WHERE clause_id = 92907; -- 52.229-3

UPDATE Clauses
SET effective_date = '2013-02-01' -- '2013-01-01'
WHERE clause_id = 92908; -- 52.229-4

UPDATE Clauses
SET effective_date = '2013-02-01' -- '2013-01-01'
WHERE clause_id = 92910; -- 52.229-6

UPDATE Clauses
SET effective_date = '2013-02-01' -- '2013-01-01'
WHERE clause_id = 92911; -- 52.229-7

UPDATE Clauses
SET effective_date = '1990-03-01' -- '1990-01-01'
WHERE clause_id = 92912; -- 52.229-8

UPDATE Clauses
SET effective_date = '1990-03-01' -- '1990-01-01'
WHERE clause_id = 92913; -- 52.229-9

UPDATE Clauses
SET effective_date = '2012-05-01' -- '2012-01-01'
WHERE clause_id = 92915; -- 52.230-1

UPDATE Clauses
SET effective_date = '2014-05-01' -- '2014-01-01'
WHERE clause_id = 92916; -- 52.230-2

UPDATE Clauses
SET effective_date = '2014-05-01' -- '2014-01-01'
WHERE clause_id = 92917; -- 52.230-3

UPDATE Clauses
SET effective_date = '2014-05-01' -- '2014-01-01'
WHERE clause_id = 92918; -- 52.230-4

UPDATE Clauses
SET effective_date = '2014-05-01' -- '2014-01-01'
WHERE clause_id = 92919; -- 52.230-5

UPDATE Clauses
SET effective_date = '2010-06-01' -- '2010-01-01'
WHERE clause_id = 92920; -- 52.230-6

UPDATE Clauses
SET effective_date = '2005-04-01' -- '2005-01-01'
WHERE clause_id = 92921; -- 52.230-7

UPDATE Clauses
SET effective_date = '2005-04-01' -- '2005-01-01'
WHERE clause_id = 92922; -- 52.232-1

UPDATE Clauses
SET effective_date = '2010-04-01' -- '2010-01-01'
WHERE clause_id = 92923; -- 52.232-10

UPDATE Clauses
SET effective_date = '1984-04-01' -- '1984-01-01'
WHERE clause_id = 92924; -- 52.232-11

UPDATE Clauses
SET effective_date = '2001-05-01' -- '2001-01-01'
WHERE clause_id = 92925; -- 52.232-12

UPDATE Clauses
SET effective_date = '1984-04-01' -- '1984-01-01'
WHERE clause_id = 92931; -- 52.232-13

UPDATE Clauses
SET effective_date = '1984-04-01' -- '1984-01-01'
WHERE clause_id = 92932; -- 52.232-14

UPDATE Clauses
SET effective_date = '1984-04-01' -- '1984-01-01'
WHERE clause_id = 92933; -- 52.232-15

UPDATE Clauses
SET effective_date = '2012-04-01' -- NULL
WHERE clause_id = 92937; -- 52.232-16

UPDATE Clauses
SET effective_date = '2014-05-01' -- '2014-01-01'
WHERE clause_id = 92938; -- 52.232-17

UPDATE Clauses
SET effective_date = '1984-04-01' -- '1984-01-01'
WHERE clause_id = 92939; -- 52.232-18

UPDATE Clauses
SET effective_date = '1984-04-01' -- NULL
WHERE clause_id = 92940; -- 52.232-19

UPDATE Clauses
SET effective_date = '1984-04-01' -- '1984-01-01'
WHERE clause_id = 92941; -- 52.232-2

UPDATE Clauses
SET effective_date = '1984-04-01' -- '1984-01-01'
WHERE clause_id = 92942; -- 52.232-20

UPDATE Clauses
SET effective_date = '1984-04-01' -- '1984-01-01'
WHERE clause_id = 92943; -- 52.232-22

UPDATE Clauses
SET effective_date = '2014-05-01' -- '2014-01-01'
WHERE clause_id = 92945; -- 52.232-23

UPDATE Clauses
SET effective_date = '2014-05-01' -- '2014-01-01'
WHERE clause_id = 92946; -- 52.232-24

UPDATE Clauses
SET effective_date = '2013-07-01' -- '2013-01-01'
WHERE clause_id = 92948; -- 52.232-25

UPDATE Clauses
SET effective_date = '2013-07-01' -- '2013-01-01'
WHERE clause_id = 92949; -- 52.232-26

UPDATE Clauses
SET effective_date = '2014-05-01' -- '2014-01-01'
WHERE clause_id = 92950; -- 52.232-27

UPDATE Clauses
SET effective_date = '2000-03-01' -- '2000-01-01'
WHERE clause_id = 92952; -- 52.232-28

UPDATE Clauses
SET effective_date = '2002-02-01' -- '2002-01-01'
WHERE clause_id = 92953; -- 52.232-29

UPDATE Clauses
SET effective_date = '1984-04-01' -- '1984-01-01'
WHERE clause_id = 92954; -- 52.232-3

UPDATE Clauses
SET effective_date = '1995-10-01' -- '1995-01-01'
WHERE clause_id = 92955; -- 52.232-30

UPDATE Clauses
SET effective_date = '2014-05-01' -- '2014-01-01'
WHERE clause_id = 92956; -- 52.232-31

UPDATE Clauses
SET effective_date = '2012-04-01' -- '2012-01-01'
WHERE clause_id = 92957; -- 52.232-32

UPDATE Clauses
SET effective_date = '2013-07-01' -- '2013-01-01'
WHERE clause_id = 92958; -- 52.232-33

UPDATE Clauses
SET effective_date = '2013-07-01' -- '2013-01-01'
WHERE clause_id = 92959; -- 52.232-34

UPDATE Clauses
SET effective_date = '2013-07-01' -- '2013-01-01'
WHERE clause_id = 92960; -- 52.232-35

UPDATE Clauses
SET effective_date = '2014-05-01' -- '2014-01-01'
WHERE clause_id = 92961; -- 52.232-36

UPDATE Clauses
SET effective_date = '1999-05-01' -- '1999-01-01'
WHERE clause_id = 92962; -- 52.232-37

UPDATE Clauses
SET effective_date = '2013-07-01' -- '2013-01-01'
WHERE clause_id = 92963; -- 52.232-38

UPDATE Clauses
SET effective_date = '2013-06-01' -- '2013-01-01'
WHERE clause_id = 92964; -- 52.232-39

UPDATE Clauses
SET effective_date = '1984-04-01' -- '1984-01-01'
WHERE clause_id = 92965; -- 52.232-4

UPDATE Clauses
SET effective_date = '2013-12-01' -- '2013-01-01'
WHERE clause_id = 92966; -- 52.232-40

UPDATE Clauses
SET effective_date = '2014-05-01' -- '2014-01-01'
WHERE clause_id = 92967; -- 52.232-5

UPDATE Clauses
SET effective_date = '1984-04-01' -- '1984-01-01'
WHERE clause_id = 92968; -- 52.232-6

UPDATE Clauses
SET effective_date = '2012-08-01' -- '2012-01-01'
WHERE clause_id = 92969; -- 52.232-7

UPDATE Clauses
SET effective_date = '2002-02-01' -- '2002-01-01'
WHERE clause_id = 92970; -- 52.232-8

UPDATE Clauses
SET effective_date = '1984-04-01' -- '1984-01-01'
WHERE clause_id = 92971; -- 52.232-9

UPDATE Clauses
SET effective_date = '2014-05-01' -- '2014-01-01'
WHERE clause_id = 92973; -- 52.233-1

UPDATE Clauses
SET effective_date = '2006-09-01' -- '2006-01-01'
WHERE clause_id = 92974; -- 52.233-2

UPDATE Clauses
SET effective_date = '1996-08-01' -- '1996-01-01'
WHERE clause_id = 92976; -- 52.233-3

UPDATE Clauses
SET effective_date = '1994-12-01' -- '1994-01-01'
WHERE clause_id = 92978; -- 52.234-1

UPDATE Clauses
SET effective_date = '2006-07-01' -- '2006-01-01'
WHERE clause_id = 92979; -- 52.234-2

UPDATE Clauses
SET effective_date = '2006-07-01' -- '2006-01-01'
WHERE clause_id = 92980; -- 52.234-3

UPDATE Clauses
SET effective_date = '2014-05-01' -- '2014-01-01'
WHERE clause_id = 92981; -- 52.234-4

UPDATE Clauses
SET effective_date = '1984-04-01' -- NULL
WHERE clause_id = 92982; -- 52.236-1

UPDATE Clauses
SET effective_date = '1984-04-01' -- '1984-01-01'
WHERE clause_id = 92983; -- 52.236-10

UPDATE Clauses
SET effective_date = '1984-04-01' -- '1984-01-01'
WHERE clause_id = 92984; -- 52.236-11

UPDATE Clauses
SET effective_date = '1984-04-01' -- '1984-01-01'
WHERE clause_id = 92985; -- 52.236-12

UPDATE Clauses
SET effective_date = '1991-11-01' -- '1991-01-01'
WHERE clause_id = 92986; -- 52.236-13

UPDATE Clauses
SET effective_date = '1984-04-01' -- '1984-01-01'
WHERE clause_id = 92988; -- 52.236-14

UPDATE Clauses
SET effective_date = '1984-04-01' -- '1984-01-01'
WHERE clause_id = 92989; -- 52.236-15

UPDATE Clauses
SET effective_date = '1984-04-01' -- '1984-01-01'
WHERE clause_id = 92991; -- 52.236-16

UPDATE Clauses
SET effective_date = '1984-04-01' -- '1984-01-01'
WHERE clause_id = 92992; -- 52.236-17

UPDATE Clauses
SET effective_date = '1984-04-01' -- '1984-01-01'
WHERE clause_id = 92993; -- 52.236-18

UPDATE Clauses
SET effective_date = '1984-04-01' -- '1984-01-01'
WHERE clause_id = 92994; -- 52.236-19

UPDATE Clauses
SET effective_date = '1984-04-01' -- '1984-01-01'
WHERE clause_id = 92995; -- 52.236-2

UPDATE Clauses
SET effective_date = '1997-02-01' -- '1997-01-01'
WHERE clause_id = 92998; -- 52.236-21

UPDATE Clauses
SET effective_date = '1984-04-01' -- '1984-01-01'
WHERE clause_id = 92999; -- 52.236-22

UPDATE Clauses
SET effective_date = '1984-04-01' -- '1984-01-01'
WHERE clause_id = 93000; -- 52.236-23

UPDATE Clauses
SET effective_date = '1984-04-01' -- '1984-01-01'
WHERE clause_id = 93001; -- 52.236-24

UPDATE Clauses
SET effective_date = '2003-06-01' -- '2003-01-01'
WHERE clause_id = 93002; -- 52.236-25

UPDATE Clauses
SET effective_date = '1995-02-01' -- '1995-01-01'
WHERE clause_id = 93003; -- 52.236-26

UPDATE Clauses
SET effective_date = '1995-02-01' -- '1995-01-01'
WHERE clause_id = 93005; -- 52.236-27

UPDATE Clauses
SET effective_date = '1997-10-01' -- '1997-01-01'
WHERE clause_id = 93006; -- 52.236-28

UPDATE Clauses
SET effective_date = '1984-04-01' -- '1984-01-01'
WHERE clause_id = 93007; -- 52.236-3

UPDATE Clauses
SET effective_date = '1984-04-01' -- '1984-01-01'
WHERE clause_id = 93008; -- 52.236-4

UPDATE Clauses
SET effective_date = '1984-04-01' -- '1984-01-01'
WHERE clause_id = 93009; -- 52.236-5

UPDATE Clauses
SET effective_date = '1984-04-01' -- '1984-01-01'
WHERE clause_id = 93010; -- 52.236-6

UPDATE Clauses
SET effective_date = '1991-11-01' -- '1991-01-01'
WHERE clause_id = 93011; -- 52.236-7

UPDATE Clauses
SET effective_date = '1984-04-01' -- '1984-01-01'
WHERE clause_id = 93012; -- 52.236-8

UPDATE Clauses
SET effective_date = '1984-04-01' -- '1984-01-01'
WHERE clause_id = 93013; -- 52.236-9

UPDATE Clauses
SET effective_date = '1984-04-01' -- '1984-01-01'
WHERE clause_id = 93014; -- 52.237-1

UPDATE Clauses
SET effective_date = '2008-09-01' -- '2008-01-01'
WHERE clause_id = 93016; -- 52.237-11

UPDATE Clauses
SET effective_date = '1984-04-01' -- '1984-01-01'
WHERE clause_id = 93017; -- 52.237-2

UPDATE Clauses
SET effective_date = '1984-04-01' -- '1984-01-01'
WHERE clause_id = 93020; -- 52.237-4

UPDATE Clauses
SET effective_date = '1984-04-01' -- '1984-01-01'
WHERE clause_id = 93021; -- 52.237-5

UPDATE Clauses
SET effective_date = '1984-04-01' -- '1984-01-01'
WHERE clause_id = 93022; -- 52.237-6

UPDATE Clauses
SET effective_date = '2003-08-01' -- '2003-01-01'
WHERE clause_id = 93024; -- 52.237-8

UPDATE Clauses
SET effective_date = '2014-05-01' -- '2014-01-01'
WHERE clause_id = 93025; -- 52.237-9

UPDATE Clauses
SET effective_date = '1996-08-01' -- '1996-01-01'
WHERE clause_id = 93026; -- 52.239-1

UPDATE Clauses
SET effective_date = '1999-05-01' -- '1999-01-01'
WHERE clause_id = 93027; -- 52.241-1

UPDATE Clauses
SET effective_date = '1995-02-01' -- '1995-01-01'
WHERE clause_id = 93028; -- 52.241-10

UPDATE Clauses
SET effective_date = '1995-02-01' -- '1995-01-01'
WHERE clause_id = 93029; -- 52.241-11

UPDATE Clauses
SET effective_date = '1995-02-01' -- '1995-01-01'
WHERE clause_id = 93030; -- 52.241-12

UPDATE Clauses
SET effective_date = '1995-02-01' -- '1995-01-01'
WHERE clause_id = 93031; -- 52.241-13

UPDATE Clauses
SET effective_date = '1995-02-01' -- '1995-01-01'
WHERE clause_id = 93032; -- 52.241-2

UPDATE Clauses
SET effective_date = '1995-02-01' -- '1995-01-01'
WHERE clause_id = 93033; -- 52.241-3

UPDATE Clauses
SET effective_date = '1995-02-01' -- '1995-01-01'
WHERE clause_id = 93034; -- 52.241-4

UPDATE Clauses
SET effective_date = '1995-02-01' -- '1995-01-01'
WHERE clause_id = 93035; -- 52.241-5

UPDATE Clauses
SET effective_date = '1995-02-01' -- '1995-01-01'
WHERE clause_id = 93036; -- 52.241-6

UPDATE Clauses
SET effective_date = '1995-02-01' -- '1995-01-01'
WHERE clause_id = 93037; -- 52.241-7

UPDATE Clauses
SET effective_date = '1995-02-01' -- '1995-01-01'
WHERE clause_id = 93038; -- 52.241-8

UPDATE Clauses
SET effective_date = '1995-02-01' -- '1995-01-01'
WHERE clause_id = 93040; -- 52.241-9

UPDATE Clauses
SET effective_date = '1984-04-01' -- '1984-01-01'
WHERE clause_id = 93041; -- 52.242-1

UPDATE Clauses
SET effective_date = '1995-07-01' -- '1995-01-01'
WHERE clause_id = 93042; -- 52.242-13

UPDATE Clauses
SET effective_date = '1984-04-01' -- '1984-01-01'
WHERE clause_id = 93043; -- 52.242-14

UPDATE Clauses
SET effective_date = '1989-08-01' -- '1989-01-01'
WHERE clause_id = 93045; -- 52.242-15

UPDATE Clauses
SET effective_date = '1984-04-01' -- '1984-01-01'
WHERE clause_id = 93046; -- 52.242-17

UPDATE Clauses
SET effective_date = '1991-04-01' -- '1991-01-01'
WHERE clause_id = 93047; -- 52.242-2

UPDATE Clauses
SET effective_date = '2014-05-01' -- '2014-01-01'
WHERE clause_id = 93048; -- 52.242-3

UPDATE Clauses
SET effective_date = '1987-08-01' -- '1987-01-01'
WHERE clause_id = 93055; -- 52.243-1

UPDATE Clauses
SET effective_date = '1987-08-01' -- '1987-01-01'
WHERE clause_id = 93060; -- 52.243-2

UPDATE Clauses
SET effective_date = '2000-09-01' -- '2000-01-01'
WHERE clause_id = 93061; -- 52.243-3

UPDATE Clauses
SET effective_date = '2007-06-01' -- '2007-01-01'
WHERE clause_id = 93062; -- 52.243-4

UPDATE Clauses
SET effective_date = '1984-04-01' -- '1984-01-01'
WHERE clause_id = 93063; -- 52.243-5

UPDATE Clauses
SET effective_date = '1984-04-01' -- '1984-01-01'
WHERE clause_id = 93064; -- 52.243-6

UPDATE Clauses
SET effective_date = '1984-04-01' -- '1984-01-01'
WHERE clause_id = 93065; -- 52.243-7

UPDATE Clauses
SET effective_date = '2010-10-01' -- '2010-01-01'
WHERE clause_id = 93067; -- 52.244-2

UPDATE Clauses
SET effective_date = '1998-08-01' -- '1998-01-01'
WHERE clause_id = 93068; -- 52.244-4

UPDATE Clauses
SET effective_date = '1996-12-01' -- '1996-01-01'
WHERE clause_id = 93069; -- 52.244-5

UPDATE Clauses
SET effective_date = '2012-04-01' -- '2012-01-01'
WHERE clause_id = 93073; -- 52.245-1

UPDATE Clauses
SET effective_date = '2012-04-01' -- '2012-01-01'
WHERE clause_id = 93074; -- 52.245-2

UPDATE Clauses
SET effective_date = '2012-04-01' -- '2012-01-01'
WHERE clause_id = 93075; -- 52.245-9

UPDATE Clauses
SET effective_date = '1984-04-01' -- '1984-01-01'
WHERE clause_id = 93076; -- 52.246-1

UPDATE Clauses
SET effective_date = '1996-08-01' -- '1996-01-01'
WHERE clause_id = 93078; -- 52.246-12

UPDATE Clauses
SET effective_date = '1996-08-01' -- '1996-01-01'
WHERE clause_id = 93079; -- 52.246-13

UPDATE Clauses
SET effective_date = '1984-04-01' -- '1984-01-01'
WHERE clause_id = 93080; -- 52.246-14

UPDATE Clauses
SET effective_date = '1984-04-01' -- '1984-01-01'
WHERE clause_id = 93082; -- 52.246-16

UPDATE Clauses
SET effective_date = '2003-06-01' -- '2003-01-01'
WHERE clause_id = 93087; -- 52.246-17

UPDATE Clauses
SET effective_date = '2001-05-01' -- '2001-01-01'
WHERE clause_id = 93091; -- 52.246-18

UPDATE Clauses
SET effective_date = '2001-05-01' -- '2001-01-01'
WHERE clause_id = 93095; -- 52.246-19

UPDATE Clauses
SET effective_date = '1996-08-01' -- '1996-01-01'
WHERE clause_id = 93098; -- 52.246-2

UPDATE Clauses
SET effective_date = '2001-05-01' -- '2001-01-01'
WHERE clause_id = 93099; -- 52.246-20

UPDATE Clauses
SET effective_date = '1994-03-01' -- '1994-01-01'
WHERE clause_id = 93101; -- 52.246-21

UPDATE Clauses
SET effective_date = '1997-02-01' -- '1997-01-01'
WHERE clause_id = 93102; -- 52.246-23

UPDATE Clauses
SET effective_date = '1997-02-01' -- '1997-01-01'
WHERE clause_id = 93104; -- 52.246-24

UPDATE Clauses
SET effective_date = '1997-02-01' -- '1997-01-01'
WHERE clause_id = 93105; -- 52.246-25

UPDATE Clauses
SET effective_date = '2001-05-01' -- '2001-01-01'
WHERE clause_id = 93106; -- 52.246-3

UPDATE Clauses
SET effective_date = '1996-08-01' -- '1996-01-01'
WHERE clause_id = 93107; -- 52.246-4

UPDATE Clauses
SET effective_date = '1984-04-01' -- '1984-01-01'
WHERE clause_id = 93108; -- 52.246-5

UPDATE Clauses
SET effective_date = '2001-05-01' -- '2001-01-01'
WHERE clause_id = 93110; -- 52.246-6

UPDATE Clauses
SET effective_date = '1996-08-01' -- '1996-01-01'
WHERE clause_id = 93111; -- 52.246-7

UPDATE Clauses
SET effective_date = '2001-05-01' -- '2001-01-01'
WHERE clause_id = 93113; -- 52.246-8

UPDATE Clauses
SET effective_date = '1984-04-01' -- '1984-01-01'
WHERE clause_id = 93114; -- 52.246-9

UPDATE Clauses
SET effective_date = '2006-02-01' -- '2006-01-01'
WHERE clause_id = 93115; -- 52.247-1

UPDATE Clauses
SET effective_date = '1984-04-01' -- '1984-01-01'
WHERE clause_id = 93116; -- 52.247-10

UPDATE Clauses
SET effective_date = '1984-04-01' -- '1984-01-01'
WHERE clause_id = 93117; -- 52.247-11

UPDATE Clauses
SET effective_date = '1984-04-01' -- '1984-01-01'
WHERE clause_id = 93118; -- 52.247-12

UPDATE Clauses
SET effective_date = '1984-04-01' -- '1984-01-01'
WHERE clause_id = 93119; -- 52.247-13

UPDATE Clauses
SET effective_date = '1984-04-01' -- '1984-01-01'
WHERE clause_id = 93120; -- 52.247-14

UPDATE Clauses
SET effective_date = '1984-04-01' -- '1984-01-01'
WHERE clause_id = 93121; -- 52.247-15

UPDATE Clauses
SET effective_date = '1984-04-01' -- '1984-01-01'
WHERE clause_id = 93122; -- 52.247-16

UPDATE Clauses
SET effective_date = '1984-04-01' -- '1984-01-01'
WHERE clause_id = 93123; -- 52.247-17

UPDATE Clauses
SET effective_date = '1984-04-01' -- '1984-01-01'
WHERE clause_id = 93124; -- 52.247-18

UPDATE Clauses
SET effective_date = '1984-04-01' -- '1984-01-01'
WHERE clause_id = 93125; -- 52.247-19

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1247_620&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?node=se48.2.52_1247_620&amp;rgn=div8'
, effective_date = '1984-04-01' -- '1984-01-01'
, clause_data = '<p>For the purpose of evaluating offers, and for no other purpose, the following estimated quantities or weights will be considered as the quantities or weights to be shipped between each origin and destination listed:</p><div><table border="1" cellpadding="1" cellspacing="1" frame="void" width="100%"><tbody><tr><th scope="col">Origin</th><th scope="col">Destination</th><th scope="col">Estimated quantity or weight</th><th scope="col">&nbsp;&nbsp;</th></tr><tr><td>{{textbox_52.247-20[0]}}</td><td>{{textbox_52.247-20[1]}}</td><td>{{textbox_52.247-20[2]}}</td><td>{{textbox_52.247-20[3]}}</td></tr><tr><td>{{textbox_52.247-20[4]}}</td><td>{{textbox_52.247-20[5]}}</td><td>{{textbox_52.247-20[6]}}</td><td>{{textbox_52.247-20[7]}}</td></tr><tr><td>{{textbox_52.247-20[8]}}</td><td>{{textbox_52.247-20[9]}}</td><td>{{textbox_52.247-20[10]}}</td><td>{{textbox_52.247-20[11]}}</td></tr></tbody></table></div>'
--               '<p>For the purpose of evaluating offers, and for no other purpose, the following estimated quantities or weights will be considered as the quantities or weights to be shipped between each origin and destination listed:</p><div><table border="&quot;1&quot;" cellpadding="&quot;1&quot;" cellspacing="&quot;1&quot;" frame="&quot;void&quot;" width="&quot;100%&quot;"><tbody><tr><th scope="&quot;col&quot;">Origin</th><th scope="&quot;col&quot;">Destination</th><th scope="&quot;col&quot;">Estimated quantity or weight</th><th scope="&quot;col&quot;">&nbsp;&nbsp;</th></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">&nbsp;&nbsp;&nbsp;</td><td align="&quot;left&quot;"></td><td align="&quot;left&quot;"></td><td align="&quot;left&quot;"></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">&nbsp;&nbsp;&nbsp;</td><td align="&quot;left&quot;"></td><td align="&quot;left&quot;"></td><td align="&quot;left&quot;"></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">&nbsp;&nbsp;&nbsp;</td><td align="&quot;left&quot;"></td><td align="&quot;left&quot;"></td><td align="&quot;left&quot;"></td></tr></tbody></table></div>'
WHERE clause_id = 93127; -- 52.247-20
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (93127, 'textbox_52.247-20[0]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (93127, 'textbox_52.247-20[1]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (93127, 'textbox_52.247-20[2]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (93127, 'textbox_52.247-20[3]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (93127, 'textbox_52.247-20[4]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (93127, 'textbox_52.247-20[5]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (93127, 'textbox_52.247-20[6]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (93127, 'textbox_52.247-20[7]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (93127, 'textbox_52.247-20[8]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (93127, 'textbox_52.247-20[9]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (93127, 'textbox_52.247-20[10]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (93127, 'textbox_52.247-20[11]', 'S', 100);

UPDATE Clauses
SET effective_date = '1984-04-01' -- '1984-01-01'
WHERE clause_id = 93128; -- 52.247-21

UPDATE Clauses
SET effective_date = '1984-04-01' -- '1984-01-01'
WHERE clause_id = 93129; -- 52.247-22

UPDATE Clauses
SET effective_date = '1984-04-01' -- '1984-01-01'
WHERE clause_id = 93131; -- 52.247-24

UPDATE Clauses
SET effective_date = '1984-04-01' -- '1984-01-01'
WHERE clause_id = 93132; -- 52.247-25

UPDATE Clauses
SET effective_date = '1984-04-01' -- '1984-01-01'
WHERE clause_id = 93133; -- 52.247-26

UPDATE Clauses
SET effective_date = '1984-04-01' -- '1984-01-01'
WHERE clause_id = 93134; -- 52.247-27

UPDATE Clauses
SET effective_date = '1984-04-01' -- '1984-01-01'
WHERE clause_id = 93135; -- 52.247-28

UPDATE Clauses
SET effective_date = '2006-02-01' -- '2006-01-01'
WHERE clause_id = 93136; -- 52.247-29

UPDATE Clauses
SET effective_date = '2006-02-01' -- '2006-01-01'
WHERE clause_id = 93138; -- 52.247-3

UPDATE Clauses
SET effective_date = '2006-02-01' -- '2006-01-01'
WHERE clause_id = 93139; -- 52.247-30

UPDATE Clauses
SET effective_date = '2006-02-01' -- '2006-01-01'
WHERE clause_id = 93140; -- 52.247-31

UPDATE Clauses
SET effective_date = '2006-02-01' -- '2006-01-01'
WHERE clause_id = 93141; -- 52.247-32

UPDATE Clauses
SET effective_date = '2006-02-01' -- '2006-01-01'
WHERE clause_id = 93142; -- 52.247-33

UPDATE Clauses
SET effective_date = '1991-11-01' -- '1991-01-01'
WHERE clause_id = 93143; -- 52.247-34

UPDATE Clauses
SET effective_date = '1984-04-01' -- '1984-01-01'
WHERE clause_id = 93144; -- 52.247-35

UPDATE Clauses
SET effective_date = '1984-04-01' -- '1984-01-01'
WHERE clause_id = 93145; -- 52.247-36

UPDATE Clauses
SET effective_date = '1984-04-01' -- '1984-01-01'
WHERE clause_id = 93146; -- 52.247-37

UPDATE Clauses
SET effective_date = '2006-02-01' -- '2006-01-01'
WHERE clause_id = 93147; -- 52.247-38

UPDATE Clauses
SET effective_date = '1984-04-01' -- '1984-01-01'
WHERE clause_id = 93148; -- 52.247-39

UPDATE Clauses
SET effective_date = '1984-04-01' -- NULL
WHERE clause_id = 93149; -- 52.247-4

UPDATE Clauses
SET effective_date = '1984-04-01' -- '1984-01-01'
WHERE clause_id = 93150; -- 52.247-40

UPDATE Clauses
SET effective_date = '1984-04-01' -- '1984-01-01'
WHERE clause_id = 93151; -- 52.247-41

UPDATE Clauses
SET effective_date = '1984-04-01' -- '1984-01-01'
WHERE clause_id = 93152; -- 52.247-42

UPDATE Clauses
SET effective_date = '2006-02-01' -- '2006-01-01'
WHERE clause_id = 93153; -- 52.247-43

UPDATE Clauses
SET effective_date = '1984-04-01' -- '1984-01-01'
WHERE clause_id = 93154; -- 52.247-44

UPDATE Clauses
SET effective_date = '1984-04-01' -- '1984-01-01'
WHERE clause_id = 93155; -- 52.247-45

UPDATE Clauses
SET effective_date = '1984-04-01' -- '1984-01-01'
WHERE clause_id = 93156; -- 52.247-46

UPDATE Clauses
SET effective_date = '2003-06-01' -- '2003-01-01'
WHERE clause_id = 93157; -- 52.247-47

UPDATE Clauses
SET effective_date = '1999-02-01' -- '1999-01-01'
WHERE clause_id = 93158; -- 52.247-48

UPDATE Clauses
SET effective_date = '1984-04-01' -- '1984-01-01'
WHERE clause_id = 93159; -- 52.247-49

UPDATE Clauses
SET effective_date = '1984-04-01' -- '1984-01-01'
WHERE clause_id = 93160; -- 52.247-5

UPDATE Clauses
SET effective_date = '1984-04-01' -- '1984-01-01'
WHERE clause_id = 93161; -- 52.247-50

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1247_651&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?node=se48.2.52_1247_651&amp;rgn=div8'
, clause_data = '<p>(e) <i>Ports of loading nominated by offeror.</i> The ports of loading named in paragraph (d) above are considered by the Government to be appropriate for this solicitation due to their compatibility with methods and facilities required to handle the cargo and types of vessels and to meet the required overseas delivery dates. Notwithstanding the foregoing, offerors may nominate additional ports of loading that the offeror considers to be more favorable to the Government. The Government may disregard such nominated ports if, after considering the quantity and nature of the supplies concerned, the requisite cargo handling capability, the available sailings on U.S.-flag vessels, and other pertinent transportation factors, it determines that use of the nominated ports is not compatible with the required overseas delivery date. United States Great Lakes ports of loading may be considered in the evaluation of offers only for those items scheduled in this provision for delivery during the ice-free or navigable period as proclaimed by the authorities of the St. Lawrence Seaway (normal period is between April 15 and November 30 annually). All ports named, including those nominated by offerors and determined to be eligible as provided in this provision, shall be considered in evaluating all offers received in order to establish the lowest laid down cost to the Government at the overseas port of discharge. All determinations shall be based on availability of ocean services by U.S.-flag vessels only. Additional U.S. port(s) of loading nominated by offeror, if any: {{textbox_52.247-51[0]}}</p><p>(f) <i>Price basis:</i> Offeror shall indicate whether prices are based on-</p><p>(&nbsp;&nbsp;&nbsp;) Paragraph (b), f.o.b. origin, transportation by GBL to port listed in paragraph (d);</p><p>(&nbsp;&nbsp;&nbsp;) Paragraph (c), f.o.b. destination (i.e., a port listed in paragraph (d));</p><p>(&nbsp;&nbsp;&nbsp;) Paragraph (e), f.o.b. origin, transportation by GBL to port nominated in paragraph (e); and/or</p><p>(&nbsp;&nbsp;&nbsp;) Paragraph (e), f.o.b. destination (i.e., a port nominated in paragraph (e)).</p><div><table border="1" cellpadding="1" cellspacing="1" frame="void" width="100%"><tbody><tr><th scope="col"><i>Ports/Terminals of Loading</i></th><th scope="col"><i>Combined Ocean and Port Handling Charges to (Indicate Country)</i></th><th scope="col"><i>Unit of Measure: i.e., metric ton, measurement ton, cubic foot, etc.</i></th></tr><tr><td align="left" scope="row">{{textbox_52.247-51[1]}}</td><td align="left">{{textbox_52.247-51[2]}}</td><td align="left">{{textbox_52.247-51[3]}}</td></tr><tr><td align="left" scope="row">{{textbox_52.247-51[4]}}</td><td align="left">{{textbox_52.247-51[5]}}</td><td align="left">{{textbox_52.247-51[6]}}</td></tr><tr><td align="left" scope="row">{{textbox_52.247-51[7]}}</td><td align="left">{{textbox_52.247-51[8]}}</td><td align="left">{{textbox_52.247-51[9]}}</td></tr></tbody></table></div><p>(e) <i>Ports of loading nominated by offeror.</i> The ports of loading named in paragraph (d) above are considered by the Government to be appropriate for this solicitation due to their compatibility with methods and facilities required to handle the cargo and types of vessels and to meet the required overseas delivery dates. Notwithstanding the foregoing, offerors may nominate additional ports of loading that the offeror considers to be more favorable to the Government. The Government may disregard such nominated ports if, after considering the quantity and nature of the supplies concerned, the requisite cargo handling capability, the available sailings on U.S.-flag vessels, and other pertinent transportation factors, it determines that use of the nominated ports is not compatible with the required overseas delivery date. United States Great Lakes ports of loading may be considered in the evaluation of offers only for those items scheduled in this provision for delivery during the ice-free or navigable period as proclaimed by the authorities of the St. Lawrence Seaway (normal period is between April 15 and November 30 annually). All ports named, including those nominated by offerors and determined to be eligible as provided in this provision, shall be considered in evaluating all offers received in order to establish the lowest laid down cost to the Government at the overseas port of discharge. All determinations shall be based on availability of ocean services by U.S.-flag vessels only. Additional U.S. port(s) of loading nominated by offeror, if any: {{textbox_52.247-51[10]}}</p><p>(f) <i>Price basis:</i> Offeror shall indicate whether prices are based on-</p><p>(&nbsp;&nbsp;&nbsp;) Paragraph (b), f.o.b. origin, transportation by GBL to port listed in paragraph (d);</p><p>(&nbsp;&nbsp;&nbsp;) Paragraph (c), f.o.b. destination (i.e., a port listed in paragraph (d));</p><p>(&nbsp;&nbsp;&nbsp;) Paragraph (e), f.o.b. origin, transportation by GBL to port nominated in paragraph (e); and/or</p><p>(&nbsp;&nbsp;&nbsp;) Paragraph (e), f.o.b. destination (i.e., a port nominated in paragraph (e)).</p>'
--               '<p>(e) <i>Ports of loading nominated by offeror.</i> The ports of loading named in paragraph (d) above are considered by the Government to be appropriate for this solicitation due to their compatibility with methods and facilities required to handle the cargo and types of vessels and to meet the required overseas delivery dates. Notwithstanding the foregoing, offerors may nominate additional ports of loading that the offeror considers to be more favorable to the Government. The Government may disregard such nominated ports if, after considering the quantity and nature of the supplies concerned, the requisite cargo handling capability, the available sailings on U.S.-flag vessels, and other pertinent transportation factors, it determines that use of the nominated ports is not compatible with the required overseas delivery date. United States Great Lakes ports of loading may be considered in the evaluation of offers only for those items scheduled in this provision for delivery during the ice-free or navigable period as proclaimed by the authorities of the St. Lawrence Seaway (normal period is between April 15 and November 30 annually). All ports named, including those nominated by offerors and determined to be eligible as provided in this provision, shall be considered in evaluating all offers received in order to establish the lowest laid down cost to the Government at the overseas port of discharge. All determinations shall be based on availability of ocean services by U.S.-flag vessels only. Additional U.S. port(s) of loading nominated by offeror, if any: {{textbox_52.247-51[0]}}</p><p>(f) <i>Price basis:</i> Offeror shall indicate whether prices are based on''??</p><p>(&nbsp;&nbsp;&nbsp;) Paragraph (b), f.o.b. origin, transportation by GBL to port listed in paragraph (d);</p><p>(&nbsp;&nbsp;&nbsp;) Paragraph (c), f.o.b. destination (i.e., a port listed in paragraph (d));</p><p>(&nbsp;&nbsp;&nbsp;) Paragraph (e), f.o.b. origin, transportation by GBL to port nominated in paragraph (e); and/or</p><p>(&nbsp;&nbsp;&nbsp;) Paragraph (e), f.o.b. destination (i.e., a port nominated in paragraph (e)).</p><div><table border="&quot;1&quot;" cellpadding="&quot;1&quot;" cellspacing="&quot;1&quot;" frame="&quot;void&quot;" width="&quot;100%&quot;"><tbody><tr><th scope="&quot;col&quot;"><i>Ports/Terminals of Loading</i></th><th scope="&quot;col&quot;"><i>Combined Ocean and Port Handling Charges to (Indicate Country)</i></th><th scope="&quot;col&quot;"><i>Unit of Measure: i.e., metric ton, measurement ton, cubic foot, etc.</i></th></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">{{textbox_52.247-51[1]}}</td><td align="&quot;left&quot;">{{textbox_52.247-51[2]}}</td><td align="&quot;left&quot;">{{textbox_52.247-51[3]}}</td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">{{textbox_52.247-51[4]}}</td><td align="&quot;left&quot;">{{textbox_52.247-51[5]}}</td><td align="&quot;left&quot;">{{textbox_52.247-51[6]}}</td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">{{textbox_52.247-51[7]}}</td><td align="&quot;left&quot;">{{textbox_52.247-51[8]}}</td><td align="&quot;left&quot;">{{textbox_52.247-51[9]}}</td></tr></tbody></table></div><p>(e) <i>Ports of loading nominated by offeror.</i> The ports of loading named in paragraph (d) above are considered by the Government to be appropriate for this solicitation due to their compatibility with methods and facilities required to handle the cargo and types of vessels and to meet the required overseas delivery dates. Notwithstanding the foregoing, offerors may nominate additional ports of loading that the offeror considers to be more favorable to the Government. The Government may disregard such nominated ports if, after considering the quantity and nature of the supplies concerned, the requisite cargo handling capability, the available sailings on U.S.-flag vessels, and other pertinent transportation factors, it determines that use of the nominated ports is not compatible with the required overseas delivery date. United States Great Lakes ports of loading may be considered in the evaluation of offers only for those items scheduled in this provision for delivery during the ice-free or navigable period as proclaimed by the authorities of the St. Lawrence Seaway (normal period is between April 15 and November 30 annually). All ports named, including those nominated by offerors and determined to be eligible as provided in this provision, shall be considered in evaluating all offers received in order to establish the lowest laid down cost to the Government at the overseas port of discharge. All determinations shall be based on availability of ocean services by U.S.-flag vessels only. Additional U.S. port(s) of loading nominated by offeror, if any: {{textbox_52.247-51[10]}}</p><p>(f) <i>Price basis:</i> Offeror shall indicate whether prices are based on''??</p><p>(&nbsp;&nbsp;&nbsp;) Paragraph (b), f.o.b. origin, transportation by GBL to port listed in paragraph (d);</p><p>(&nbsp;&nbsp;&nbsp;) Paragraph (c), f.o.b. destination (i.e., a port listed in paragraph (d));</p><p>(&nbsp;&nbsp;&nbsp;) Paragraph (e), f.o.b. origin, transportation by GBL to port nominated in paragraph (e); and/or</p><p>(&nbsp;&nbsp;&nbsp;) Paragraph (e), f.o.b. destination (i.e., a port nominated in paragraph (e)).</p>'
WHERE clause_id = 93165; -- 52.247-51
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (93165, 'textbox_52.247-51[0]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (93165, 'textbox_52.247-51[1]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (93165, 'textbox_52.247-51[2]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (93165, 'textbox_52.247-51[3]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (93165, 'textbox_52.247-51[4]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (93165, 'textbox_52.247-51[5]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (93165, 'textbox_52.247-51[6]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (93165, 'textbox_52.247-51[7]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (93165, 'textbox_52.247-51[8]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (93165, 'textbox_52.247-51[9]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (93165, 'textbox_52.247-51[10]', 'S', 100);

UPDATE Clauses
SET effective_date = '2006-02-01' -- '2006-01-01'
WHERE clause_id = 93166; -- 52.247-52

UPDATE Clauses
SET effective_date = '1984-04-01' -- '1984-01-01'
WHERE clause_id = 93167; -- 52.247-53

UPDATE Clauses
SET effective_date = '2003-06-01' -- '2003-01-01'
WHERE clause_id = 93168; -- 52.247-55

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1247_656&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?node=se48.2.52_1247_656&amp;rgn=div8'
, effective_date = '1984-04-01' -- '1984-01-01'
, clause_data = '<p>The lowest appropriate common carrier transportation costs, including offeror''s through transit rates and charges when applicable, from offeror''s shipping points, via the transit point, to the ultimate destination will be used in evaluating offers.</p><div><table border="1" cellpadding="1" cellspacing="1" frame="void" width="100%"><tbody><tr><th scope="col">Transit point(s)</th><th scope="col">Destination(s)</th><th scope="col">&nbsp;&nbsp;</th></tr><tr><td>{{textbox_52.247-56[0]}}</td><td>{{textbox_52.247-56[1]}}</td><td>{{textbox_52.247-56[2]}}</td></tr><tr><td>{{textbox_52.247-56[3]}}</td><td>{{textbox_52.247-56[4]}}</td><td>{{textbox_52.247-56[5]}}</td></tr><tr><td>{{textbox_52.247-56[6]}}</td><td>{{textbox_52.247-56[7]}}</td><td>{{textbox_52.247-56[8]}}</td></tr></tbody></table></div>'
--               '<p>The lowest appropriate common carrier transportation costs, including offeror''s through transit rates and charges when applicable, from offeror''s shipping points, via the transit point, to the ultimate destination will be used in evaluating offers.</p><div><table border="&quot;1&quot;" cellpadding="&quot;1&quot;" cellspacing="&quot;1&quot;" frame="&quot;void&quot;" width="&quot;100%&quot;"><tbody><tr><th scope="&quot;col&quot;">Transit point(s)</th><th scope="&quot;col&quot;">Destination(s)</th><th scope="&quot;col&quot;">&nbsp;&nbsp;</th></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">&nbsp;&nbsp;&nbsp;</td><td align="&quot;left&quot;"></td><td align="&quot;left&quot;"></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">&nbsp;&nbsp;&nbsp;</td><td align="&quot;left&quot;"></td><td align="&quot;left&quot;"></td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">&nbsp;&nbsp;&nbsp;</td><td align="&quot;left&quot;"></td><td align="&quot;left&quot;"></td></tr></tbody></table></div>'
WHERE clause_id = 93169; -- 52.247-56
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (93169, 'textbox_52.247-56[0]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (93169, 'textbox_52.247-56[1]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (93169, 'textbox_52.247-56[2]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (93169, 'textbox_52.247-56[3]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (93169, 'textbox_52.247-56[4]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (93169, 'textbox_52.247-56[5]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (93169, 'textbox_52.247-56[6]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (93169, 'textbox_52.247-56[7]', 'S', 100);
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (93169, 'textbox_52.247-56[8]', 'S', 100);

UPDATE Clauses
SET effective_date = '1984-04-01' -- '1984-01-01'
WHERE clause_id = 93170; -- 52.247-57

UPDATE Clauses
SET effective_date = '1984-04-01' -- '1984-01-01'
WHERE clause_id = 93171; -- 52.247-58

UPDATE Clauses
SET effective_date = '1984-04-01' -- '1984-01-01'
WHERE clause_id = 93172; -- 52.247-59

UPDATE Clauses
SET effective_date = '1984-04-01' -- '1984-01-01'
WHERE clause_id = 93173; -- 52.247-6

UPDATE Clauses
SET effective_date = '1989-12-01' -- '1989-01-01'
WHERE clause_id = 93174; -- 52.247-60

UPDATE Clauses
SET effective_date = '1984-04-01' -- '1984-01-01'
WHERE clause_id = 93176; -- 52.247-61

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1247_662&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?node=se48.2.52_1247_662&amp;rgn=div8'
, effective_date = '1984-04-01' -- '1984-01-01'
, clause_data = '<p>(a) For the purpose of evaluating <i>f.o.b. destination</i> offers, the Government estimates that the quantity specified will be shipped to the destinations indicated:</p><div><div><table border="1" cellpadding="1" cellspacing="1" frame="void" width="100%"><tbody><tr><th scope="col">Estimated quantity</th><th scope="col">Destination</th><th scope="col">&nbsp;&nbsp;</th></tr><tr><td>{{textbox_52.247-62[0]}}</td><td>{{textbox_52.247-62[1]}}</td><td>{{textbox_52.247-62[2]}}</td></tr><tr><td>{{textbox_52.247-62[3]}}</td><td>{{textbox_52.247-62[4]}}</td><td>{{textbox_52.247-62[5]}}</td></tr><tr><td>{{textbox_52.247-62[6]}}</td><td>{{textbox_52.247-62[7]}}</td><td>{{textbox_52.247-62[8]}}</td></tr></tbody></table></div></div><p>(b) If the quantity shipped to each destination varies from the quantity estimated, and if the variation results in a change in the transportation costs, appropriate adjustment shall be made.</p>'
--               '<p>(a) For the purpose of evaluating <i>f.o.b. destination</i> offers, the Government estimates that the quantity specified will be shipped to the destinations indicated:</p>\n<div><div><table border="&quot;1&quot;" cellpadding="&quot;1&quot;" cellspacing="&quot;1&quot;" frame="&quot;void&quot;" width="&quot;100%&quot;"><tbody>\n<tr><th scope="&quot;col&quot;">Estimated quantity</th><th scope="&quot;col&quot;">Destination</th></tr>\n<tr><td align="&quot;left&quot;" scope="&quot;row&quot;">{{textbox_52.247-62[0]}}</td><td align="&quot;left&quot;">{{textbox_52.247-62[1]}}</td></tr>\n<tr><td align="&quot;left&quot;" scope="&quot;row&quot;">{{textbox_52.247-62[2]}}</td><td align="&quot;left&quot;">{{textbox_52.247-62[3]}}</td></tr>\n<tr><td align="&quot;left&quot;" scope="&quot;row&quot;">{{textbox_52.247-62[4]}}</td><td align="&quot;left&quot;">{{textbox_52.247-62[5]}}</td></tr>\n<tr><td align="&quot;left&quot;" scope="&quot;row&quot;">{{textbox_52.247-62[6]}}</td><td align="&quot;left&quot;">{{textbox_52.247-62[7]}}</td></tr>\n<tr><td align="&quot;left&quot;" scope="&quot;row&quot;">{{textbox_52.247-62[8]}}</td><td align="&quot;left&quot;">{{textbox_52.247-62[9]}}</td></tr>\n</tbody></table></div></div><p>(b) If the quantity shipped to each destination varies from the quantity estimated, and if the variation results in a change in the transportation costs, appropriate adjustment shall be made.</p>'
WHERE clause_id = 93177; -- 52.247-62
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 28974; -- textbox_52.247-62[9]

UPDATE Clauses
SET effective_date = '2003-06-01' -- '2003-01-01'
WHERE clause_id = 93178; -- 52.247-63

UPDATE Clauses
SET effective_date = '2006-02-01' -- '2006-01-01'
WHERE clause_id = 93181; -- 52.247-64

UPDATE Clauses
SET effective_date = '1994-05-01' -- '1994-01-01'
WHERE clause_id = 93183; -- 52.247-66

UPDATE Clauses
SET effective_date = '2006-02-01' -- '2006-01-01'
WHERE clause_id = 93184; -- 52.247-67

UPDATE Clauses
SET effective_date = '1984-04-01' -- '1984-01-01'
WHERE clause_id = 93186; -- 52.247-7

UPDATE Clauses
SET effective_date = '1984-04-01' -- '1984-01-01'
WHERE clause_id = 93187; -- 52.247-8

UPDATE Clauses
SET effective_date = '1984-04-01' -- '1984-01-01'
WHERE clause_id = 93188; -- 52.247-9

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1248_61&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?node=se48.2.52_1248_61&amp;rgn=div8'
, effective_date = '2010-10-01' -- '2010-01-01'
, clause_data = '<p>(a) <i>General.</i> The Contractor is encouraged to develop, prepare, and submit value engineering change proposals (VECP''s) voluntarily. The Contractor shall share in any net acquisition savings realized from accepted VECP''s, in accordance with the incentive sharing rates in paragraph (f) below.</p><p>(b) <i>Definitions. Acquisition savings,</i> as used in this clause, means savings resulting from the application of a VECP to contracts awarded by the same contracting office or its successor for essentially the same unit. Acquisition savings include-</p><p>(1) Instant contract savings, which are the net cost reductions on this, the instant contract, and which are equal to the instant unit cost reduction multiplied by the number of instant contract units affected by the VECP, less the Contractor''s allowable development and implementation costs;</p><p>(2) Concurrent contract savings, which are net reductions in the prices of other contracts that are definitized and ongoing at the time the VECP is accepted; and</p><p>(3) Future contract savings, which are the product of the future unit cost reduction multiplied by the number of future contract units in the sharing base. On an instant contract, future contract savings include savings on increases in quantities after VECP acceptance that are due to contract modifications, exercise of options, additional orders, and funding of subsequent year requirements on a multiyear contract.</p><p><i>Collateral costs,</i> as used in this clause, means agency cost of operation, maintenance, logistic support, or Government-furnished property.</p><p><i>Collateral savings,</i> as used in this clause, means those measurable net reductions resulting from a VECP in the agency''s overall projected collateral costs, exclusive of acquisition savings, whether or not the acquisition cost changes.</p><p><i>Contracting office</i> includes any contracting office that the acquisition is transferred to, such as another branch of the agency or another agency''s office that is performing a joint acquisition action.</p><p><i>Contractor''s development and implementation costs,</i> as used in this clause, means those costs the Contractor incurs on a VECP specifically in developing, testing, preparing, and submitting the VECP, as well as those costs the Contractor incurs to make the contractual changes required by Government acceptance of a VECP.</p><p><i>Future unit cost reduction,</i> as used in this clause, means the instant unit cost reduction adjusted as the Contracting Officer considers necessary for projected learning or changes in quantity during the sharing period. It is calculated at the time the VECP is accepted and applies either (1) throughout the sharing period, unless the Contracting Officer decides that recalculation is necessary because conditions are significantly different from those previously anticipated or (2) to the calculation of a lump-sum payment, which cannot later be revised.</p><p><i>Government costs,</i> as used in this clause, means those agency costs that result directly from developing and implementing the VECP, such as any net increases in the cost of testing, operations, maintenance, and logistics support. The term does not include the normal administrative costs of processing the VECP or any increase in this contract''s cost or price resulting from negative instant contract savings.</p><p><i>Instant contract,</i> as used in this clause, means this contract, under which the VECP is submitted. It does not include increases in quantities after acceptance of the VECP that are due to contract modifications, exercise of options, or additional orders. If this is a multiyear contract, the term does not include quantities funded after VECP acceptance. If this contract is a fixed-price contract with prospective price redetermination, the term refers to the period for which firm prices have been established.</p><p><i>Instant unit cost reduction</i> means the amount of the decrease in unit cost of performance (without deducting any Contractor''s development or implementation costs) resulting from using the VECP on this, the instant contract. If this is a service contract, the instant unit cost reduction is normally equal to the number of hours per line-item task saved by using the VECP on this contract, multiplied by the appropriate contract labor rate.</p><p><i>Negative instant contract savings</i> means the increase in the cost or price of this contract when the acceptance of a VECP results in an excess of the Contractor''s allowable development and implementation costs over the product of the instant unit cost reduction multiplied by the number of instant contract units affected.</p><p><i>Net acquisition savings</i> means total acquisition savings, including instant, concurrent, and future contract savings, less Government costs.</p><p><i>Sharing base,</i> as used in this clause, means the number of affected end items on contracts of the contracting office accepting the VECP.</p><p><i>Sharing period,</i> as used in this clause, means the period beginning with acceptance of the first unit incorporating the VECP and ending at a calendar date or event determined by the contracting officer for each VECP.</p><p><i>Unit,</i> as used in this clause, means the item or task to which the Contracting Officer and the Contractor agree the VECP applies.</p><p><i>Value engineering change proposal (VECP)</i> means a proposal that-</p><p>(1) Requires a change to this, the instant contract, to implement; and</p><p>(2) Results in reducing the overall projected cost to the agency without impairing essential functions or characteristics; <i>provided,</i> that it does not involve a change-</p><p>(i) In deliverable end item quantities only;</p><p>(ii) In research and development (R&D) end items or R&D test quantities that is due solely to results of previous testing under this contract; or</p><p>(iii) To the contract type only.</p><p>(c) <i>VECP preparation.</i> As a minimum, the Contractor shall include in each VECP the information described in subparagraphs (1) through (8) below. If the proposed change is affected by contractually required configuration management or similar procedures, the instructions in those procedures relating to format, identification, and priority assignment shall govern VECP preparation. The VECP shall include the following:</p><p>(1) A description of the difference between the existing contract requirement and the proposed requirement, the comparative advantages and disadvantages of each, a justification when an item''s function or characteristics are being altered, the effect of the change on the end item''s performance, and any pertinent objective test data.</p><p>(2) A list and analysis of the contract requirements that must be changed if the VECP is accepted, including any suggested specification revisions.</p><p>(3) Identification of the unit to which the VECP applies.</p><p>(4) A separate, detailed cost estimate for (i) the affected portions of the existing contract requirement and (ii) the VECP. The cost reduction associated with the VECP shall take into account the Contractor''s allowable development and implementation costs, including any amount attributable to subcontracts under the Subcontracts paragraph of this clause, below.</p><p>(5) A description and estimate of costs the Government may incur in implementing the VECP, such as test and evaluation and operating and support costs.</p><p>(6) A prediction of any effects the proposed change would have on collateral costs to the agency.</p><p>(7) A statement of the time by which a contract modification accepting the VECP must be issued in order to achieve the maximum cost reduction, noting any effect on the contract completion time or delivery schedule.</p><p>(8) Identification of any previous submissions of the VECP, including the dates submitted, the agencies and contract numbers involved, and previous Government actions, if known.</p><p>(d) <i>Submission.</i> The Contractor shall submit VECP''s to the Contracting Officer, unless this contract states otherwise. If this contract is administered by other than the contracting office, the Contractor shall submit a copy of the VECP simultaneously to the Contracting Officer and to the Administrative Contracting Officer.</p><p>(e) <i>Government action.</i> (1) The Contracting Officer will notify the Contractor of the status of the VECP within 45 calendar days after the contracting office receives it. If additional time is required, the Contracting Officer will notify the Contractor within the 45-day period and provide the reason for the delay and the expected date of the decision. The Government will process VECP''s expeditiously; however, it will not be liable for any delay in acting upon a VECP.</p><p>(2) If the VECP is not accepted, the Contracting Officer will notify the Contractor in writing, explaining the reasons for rejection. The Contractor may withdraw any VECP, in whole or in part, at any time before it is accepted by the Government. The Contracting Officer may require that the Contractor provide written notification before undertaking significant expenditures for VECP effort.</p><p>(3) Any VECP may be accepted, in whole or in part, by the Contracting Officer''s award of a modification to this contract citing this clause and made either before or within a reasonable time after contract performance is completed. Until such a contract modification applies a VECP to this contract, the Contractor shall perform in accordance with the existing contract. The decision to accept or reject all or part of any VECP is a unilateral decision made solely at the discretion of the Contracting Officer.</p><p>(f) <i>Sharing rates.</i> If a VECP is accepted, the Contractor shall share in net acquisition savings according to the percentages shown in the table below. The percentage paid the Contractor depends upon (1) this contract''s type (fixed-price, incentive, or cost-reimbursement), (2) the sharing arrangement specified in paragraph (a) above (incentive, program requirement, or a combination as delineated in the Schedule), and (3) the source of the savings (the instant contract, or concurrent and future contracts), as follows:</p><div><div><p>Contractor''s Share of Net Acquisition Savings</p><p>[Figures in Percent]</p></div><div><table border="1" cellpadding="1" cellspacing="1" frame="void" width="100%"><tbody><tr><th rowspan="3" scope="col">Contract type</th><th colspan="4" scope="col">Sharing arrangement</th></tr><tr><th colspan="2" scope="col">Incentive (voluntary)</th><th colspan="2" scope="col">Program requirement (mandatory)</th></tr><tr><th scope="col">Instant contract rate</th><th scope="col">Con-current and future contract rate</th><th scope="col">Instant contract rate</th><th scope="col">Con-current and future contract rate</th></tr><tr><td align="left" scope="row">Fixed-price (includes fixed-price-award-fee; excludes other fixed-price incentive contracts)</td><td align="center"><sup>1</sup>50</td><td align="center"><sup>1</sup>50</td><td align="center">25</td><td align="center">25</td></tr><tr><td align="left" scope="row">Incentive (fixed-price or cost) (other than award fee)</td><td align="center">(<sup>2</sup>)</td><td align="center"><sup>1</sup>50</td><td align="center">(<sup>2</sup>)</td><td align="center">25</td></tr><tr><td align="left" scope="row">Cost-reimbursement (includes cost-plus-award-fee; excludes other cost-type incentive contracts)</td><td align="center"><sup>3</sup>25</td><td align="center"><sup>3</sup>25</td><td align="center">15</td><td align="center">15</td></tr></tbody></table></div><div><p><sup>1</sup>The Contracting Officer may increase the Contractor''s sharing rate to as high as 75 percent for each VECP.</p><p><sup>2</sup>Same sharing arrangement as the contract''s profit or fee adjustment formula.</p><p><sup>3</sup>The Contracting Officer may increase the Contractor''s sharing rate to as high as 50 percent for each VECP.</p></div></div><p>(g) <i>Calculating net acquisition savings.</i> (1) Acquisition savings are realized when (i) the cost or price is reduced on the instant contract, (ii) reductions are negotiated in concurrent contracts, (iii) future contracts are awarded, or (iv) agreement is reached on a lump-sum payment for future contract savings (see subparagraph (i)(4) below). Net acquisition savings are first realized, and the Contractor shall be paid a share, when Government costs and any negative instant contract savings have been fully offset against acquisition savings.</p><p>(2) Except in incentive contracts, Government costs and any price or cost increases resulting from negative instant contract savings shall be offset against acquisition savings each time such savings are realized until they are fully offset. Then, the Contractor''s share is calculated by multiplying net acquisition savings by the appropriate Contractor''s percentage sharing rate (see paragraph (f) above). Additional Contractor shares of net acquisition savings shall be paid to the Contractor at the time realized.</p><p>(3) If this is an incentive contract, recovery of Government costs on the instant contract shall be deferred and offset against concurrent and future contract savings. The Contractor shall share through the contract incentive structure in savings on the instant contract items affected. Any negative instant contract savings shall be added to the target cost or to the target price and ceiling price, and the amount shall be offset against concurrent and future contract savings.</p><p>(4) If the Government does not receive and accept all items on which it paid the Contractor''s share, the Contractor shall reimburse the Government for the proportionate share of these payments.</p><p>(h) <i>Contract adjustment.</i> The modification accepting the VECP (or a subsequent modification issued as soon as possible after any negotiations are completed) shall-</p><p>(1) Reduce the contract price or estimated cost by the amount of instant contract savings, unless this is an incentive contract;</p><p>(2) When the amount of instant contract savings is negative, increase the contract price, target price and ceiling price, target cost, or estimated cost by that amount;</p><p>(3) Specify the Contractor''s dollar share per unit on future contracts, or provide the lump-sum payment;</p><p>(4) Specify the amount of any Government costs or negative instant contract savings to be offset in determining net acquisition savings realized from concurrent or future contract savings; and</p><p>(5) Provide the Contractor''s share of any net acquisition savings under the instant contract in accordance with the following:</p><p>(i) Fixed-price contracts-add to contract price.</p><p>(ii) Cost-reimbursement contracts-add to contract fee.</p><p>(i) <i>Concurrent and future contract savings.</i> (1) Payments of the Contractor''s share of concurrent and future contract savings shall be made by a modification to the instant contract in accordance with subparagraph (h)(5) above. For incentive contracts, shares shall be added as a separate firm-fixed-price line item on the instant contract. The Contractor shall maintain records adequate to identify the first delivered unit for 3 years after final payment under this contract.</p><p>(2) The Contracting Officer shall calculate the Contractor''s share of concurrent contract savings by (i) subtracting from the reduction in price negotiated on the concurrent contract any Government costs or negative instant contract savings not yet offset and (ii) multiplying the result by the Contractor''s sharing rate.</p><p>(3) The Contracting Officer shall calculate the Contractor''s share of future contract savings by (i) multiplying the future unit cost reduction by the number of future contract units scheduled for delivery during the sharing period, (ii) subtracting any Government costs or negative instant contract savings not yet offset, and (iii) multiplying the result by the Contractor''s sharing rate.</p><p>(4) When the Government wishes and the Contractor agrees, the Contractor''s share of future contract savings may be paid in a single lump sum rather than in a series of payments over time as future contracts are awarded. Under this alternate procedure, the future contract savings may be calculated when the VECP is accepted, on the basis of the Contracting Officer''s forecast of the number of units that will be delivered during the sharing period. The Contractor''s share shall be included in a modification to this contract (see subparagraph (h)(3) above) and shall not be subject to subsequent adjustment.</p><p>(5) Alternate no-cost settlement method. When, in accordance with subsection 48.104-4 of the Federal Acquisition Regulation, the Government and the Contractor mutually agree to use the no-cost settlement method, the following applies:</p><p>(i) The Contractor will keep all the savings on the instant contract and on its concurrent contracts only.</p><p>(ii) The Government will keep all the savings resulting from concurrent contracts placed on other sources, savings from all future contracts, and all collateral savings.</p><p>(j) <i>Collateral savings.</i> If a VECP is accepted, the Contracting Officer will increase the instant contract amount, as specified in paragraph (h)(5) of this clause, by a rate from 20 to 100 percent, as determined by the Contracting Officer, of any projected collateral savings determined to be realized in a typical year of use after subtracting any Government costs not previously offset. However, the Contractor''s share of collateral savings will not exceed the contract''s firm-fixed-price, target price, target cost, or estimated cost, at the time the VECP is accepted, or $100,000, whichever is greater. The Contracting Officer will be the sole determiner of the amount of collateral savings.</p><p>(k) <i>Relationship to other incentives.</i> Only those benefits of an accepted VECP not rewardable under performance, design-to-cost (production unit cost, operating and support costs, reliability and maintainability), or similar incentives shall be rewarded under this clause. However, the targets of such incentives affected by the VECP shall not be adjusted because of VECP acceptance. If this contract specifies targets but provides no incentive to surpass them, the value engineering sharing shall apply only to the amount of achievement better than target.</p><p>(l) <i>Subcontracts.</i> The Contractor shall include an appropriate value engineering clause in any subcontract of $150,000 or more and may include one in subcontracts of lesser value. In calculating any adjustment in this contract''s price for instant contract savings (or negative instant contract savings), the Contractor''s allowable development and implementation costs shall include any subcontractor''s allowable development and implementation costs, and any value engineering incentive payments to a subcontractor, clearly resulting from a VECP accepted by the Government under this contract. The Contractor may choose any arrangement for subcontractor value engineering incentive payments; <i>provided,</i> that the payments shall not reduce the Government''s share of concurrent or future contract savings or collateral savings.</p><p>(m) <i>Data.</i> The Contractor may restrict the Government''s right to use any part of a VECP or the supporting data by marking the following legend on the affected parts:</p><p>''These data, furnished under the Value Engineering clause of contract {{textbox_52.248-1[0]}}, shall not be disclosed outside the Government or duplicated, used, or disclosed, in whole or in part, for any purpose other than to evaluate a value engineering change proposal submitted under the clause. This restriction does not limit the Government''s right to use information contained in these data if it has been obtained or is otherwise available from the Contractor or from another source without limitations.''</p><p>If a VECP is accepted, the Contractor hereby grants the Government unlimited rights in the VECP and supporting data, except that, with respect to data qualifying and submitted as limited rights technical data, the Government shall have the rights specified in the contract modification implementing the VECP and shall appropriately mark the data. (The terms <i>unlimited rights</i> and <i>limited rights</i> are defined in part 27 of the Federal Acquisition Regulation.)</p>'
--               '<p>(a) <i>General.</i> The Contractor is encouraged to develop, prepare, and submit value engineering change proposals (VECP''s) voluntarily. The Contractor shall share in any net acquisition savings realized from accepted VECP''s, in accordance with the incentive sharing rates in paragraph (f) below.</p><p>(b) <i>Definitions. Acquisition savings,</i> as used in this clause, means savings resulting from the application of a VECP to contracts awarded by the same contracting office or its successor for essentially the same unit. Acquisition savings include-</p><p>(1) Instant contract savings, which are the net cost reductions on this, the instant contract, and which are equal to the instant unit cost reduction multiplied by the number of instant contract units affected by the VECP, less the Contractor''s allowable development and implementation costs;</p><p>(2) Concurrent contract savings, which are net reductions in the prices of other contracts that are definitized and ongoing at the time the VECP is accepted; and</p><p>(3) Future contract savings, which are the product of the future unit cost reduction multiplied by the number of future contract units in the sharing base. On an instant contract, future contract savings include savings on increases in quantities after VECP acceptance that are due to contract modifications, exercise of options, additional orders, and funding of subsequent year requirements on a multiyear contract.</p><p><i>Collateral costs,</i> as used in this clause, means agency cost of operation, maintenance, logistic support, or Government-furnished property.</p><p><i>Collateral savings,</i> as used in this clause, means those measurable net reductions resulting from a VECP in the agency''s overall projected collateral costs, exclusive of acquisition savings, whether or not the acquisition cost changes.</p><p><i>Contracting office</i> includes any contracting office that the acquisition is transferred to, such as another branch of the agency or another agency''s office that is performing a joint acquisition action.</p><p><i>Contractor''s development and implementation costs,</i> as used in this clause, means those costs the Contractor incurs on a VECP specifically in developing, testing, preparing, and submitting the VECP, as well as those costs the Contractor incurs to make the contractual changes required by Government acceptance of a VECP.</p><p><i>Future unit cost reduction,</i> as used in this clause, means the instant unit cost reduction adjusted as the Contracting Officer considers necessary for projected learning or changes in quantity during the sharing period. It is calculated at the time the VECP is accepted and applies either (1) throughout the sharing period, unless the Contracting Officer decides that recalculation is necessary because conditions are significantly different from those previously anticipated or (2) to the calculation of a lump-sum payment, which cannot later be revised.</p><p><i>Government costs,</i> as used in this clause, means those agency costs that result directly from developing and implementing the VECP, such as any net increases in the cost of testing, operations, maintenance, and logistics support. The term does not include the normal administrative costs of processing the VECP or any increase in this contract''s cost or price resulting from negative instant contract savings.</p><p><i>Instant contract,</i> as used in this clause, means this contract, under which the VECP is submitted. It does not include increases in quantities after acceptance of the VECP that are due to contract modifications, exercise of options, or additional orders. If this is a multiyear contract, the term does not include quantities funded after VECP acceptance. If this contract is a fixed-price contract with prospective price redetermination, the term refers to the period for which firm prices have been established.</p><p><i>Instant unit cost reduction</i> means the amount of the decrease in unit cost of performance (without deducting any Contractor''s development or implementation costs) resulting from using the VECP on this, the instant contract. If this is a service contract, the instant unit cost reduction is normally equal to the number of hours per line-item task saved by using the VECP on this contract, multiplied by the appropriate contract labor rate.</p><p><i>Negative instant contract savings</i> means the increase in the cost or price of this contract when the acceptance of a VECP results in an excess of the Contractor''s allowable development and implementation costs over the product of the instant unit cost reduction multiplied by the number of instant contract units affected.</p><p><i>Net acquisition savings</i> means total acquisition savings, including instant, concurrent, and future contract savings, less Government costs.</p><p><i>Sharing base,</i> as used in this clause, means the number of affected end items on contracts of the contracting office accepting the VECP.</p><p><i>Sharing period,</i> as used in this clause, means the period beginning with acceptance of the first unit incorporating the VECP and ending at a calendar date or event determined by the contracting officer for each VECP.</p><p><i>Unit,</i> as used in this clause, means the item or task to which the Contracting Officer and the Contractor agree the VECP applies.</p><p><i>Value engineering change proposal (VECP)</i> means a proposal that-</p><p>(1) Requires a change to this, the instant contract, to implement; and</p><p>(2) Results in reducing the overall projected cost to the agency without impairing essential functions or characteristics; <i>provided,</i> that it does not involve a change-</p><p>(i) In deliverable end item quantities only;</p><p>(ii) In research and development (R&D) end items or R&D test quantities that is due solely to results of previous testing under this contract; or</p><p>(iii) To the contract type only.</p><p>(c) <i>VECP preparation.</i> As a minimum, the Contractor shall include in each VECP the information described in subparagraphs (1) through (8) below. If the proposed change is affected by contractually required configuration management or similar procedures, the instructions in those procedures relating to format, identification, and priority assignment shall govern VECP preparation. The VECP shall include the following:</p><p>(1) A description of the difference between the existing contract requirement and the proposed requirement, the comparative advantages and disadvantages of each, a justification when an item''s function or characteristics are being altered, the effect of the change on the end item''s performance, and any pertinent objective test data.</p><p>(2) A list and analysis of the contract requirements that must be changed if the VECP is accepted, including any suggested specification revisions.</p><p>(3) Identification of the unit to which the VECP applies.</p><p>(4) A separate, detailed cost estimate for (i) the affected portions of the existing contract requirement and (ii) the VECP. The cost reduction associated with the VECP shall take into account the Contractor''s allowable development and implementation costs, including any amount attributable to subcontracts under the Subcontracts paragraph of this clause, below.</p><p>(5) A description and estimate of costs the Government may incur in implementing the VECP, such as test and evaluation and operating and support costs.</p><p>(6) A prediction of any effects the proposed change would have on collateral costs to the agency.</p><p>(7) A statement of the time by which a contract modification accepting the VECP must be issued in order to achieve the maximum cost reduction, noting any effect on the contract completion time or delivery schedule.</p><p>(8) Identification of any previous submissions of the VECP, including the dates submitted, the agencies and contract numbers involved, and previous Government actions, if known.</p><p>(d) <i>Submission.</i> The Contractor shall submit VECP''s to the Contracting Officer, unless this contract states otherwise. If this contract is administered by other than the contracting office, the Contractor shall submit a copy of the VECP simultaneously to the Contracting Officer and to the Administrative Contracting Officer.</p><p>(e) <i>Government action.</i> (1) The Contracting Officer will notify the Contractor of the status of the VECP within 45 calendar days after the contracting office receives it. If additional time is required, the Contracting Officer will notify the Contractor within the 45-day period and provide the reason for the delay and the expected date of the decision. The Government will process VECP''s expeditiously; however, it will not be liable for any delay in acting upon a VECP.</p><p>(2) If the VECP is not accepted, the Contracting Officer will notify the Contractor in writing, explaining the reasons for rejection. The Contractor may withdraw any VECP, in whole or in part, at any time before it is accepted by the Government. The Contracting Officer may require that the Contractor provide written notification before undertaking significant expenditures for VECP effort.</p><p>(3) Any VECP may be accepted, in whole or in part, by the Contracting Officer''s award of a modification to this contract citing this clause and made either before or within a reasonable time after contract performance is completed. Until such a contract modification applies a VECP to this contract, the Contractor shall perform in accordance with the existing contract. The decision to accept or reject all or part of any VECP is a unilateral decision made solely at the discretion of the Contracting Officer.</p><p>(f) <i>Sharing rates.</i> If a VECP is accepted, the Contractor shall share in net acquisition savings according to the percentages shown in the table below. The percentage paid the Contractor depends upon (1) this contract''s type (fixed-price, incentive, or cost-reimbursement), (2) the sharing arrangement specified in paragraph (a) above (incentive, program requirement, or a combination as delineated in the Schedule), and (3) the source of the savings (the instant contract, or concurrent and future contracts), as follows:</p>\n<div><div><p>Contractor''s Share of Net Acquisition Savings</p><p>[Figures in Percent]</p></div><div><table border="&quot;1&quot;" cellpadding="&quot;1&quot;" cellspacing="&quot;1&quot;" frame="&quot;void&quot;" width="&quot;100%&quot;"><tbody><tr><th rowspan="&quot;3&quot;" scope="&quot;col&quot;">Contract type</th><th colspan="&quot;4&quot;" scope="&quot;col&quot;">Sharing arrangement</th></tr><tr><th colspan="&quot;2&quot;" scope="&quot;col&quot;">Incentive (voluntary)</th><th colspan="&quot;2&quot;" scope="&quot;col&quot;">Program requirement (mandatory)</th></tr><tr><th scope="&quot;col&quot;">Instant contract rate</th><th scope="&quot;col&quot;">Con-current and future contract rate</th><th scope="&quot;col&quot;">Instant contract rate</th><th scope="&quot;col&quot;">Con-current and future contract rate</th></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">Fixed-price (includes fixed-price-award-fee; excludes other fixed-price incentive contracts)</td><td align="&quot;center&quot;"><sup>1</sup>50</td><td align="&quot;center&quot;"><sup>1</sup>50</td><td align="&quot;center&quot;">25</td><td align="&quot;center&quot;">25</td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">Incentive (fixed-price or cost) (other than award fee)</td><td align="&quot;center&quot;">(<sup>2</sup>)</td><td align="&quot;center&quot;"><sup>1</sup>50</td><td align="&quot;center&quot;">(<sup>2</sup>)</td><td align="&quot;center&quot;">25</td></tr><tr><td align="&quot;left&quot;" scope="&quot;row&quot;">Cost-reimbursement (includes cost-plus-award-fee; excludes other cost-type incentive contracts)</td><td align="&quot;center&quot;"><sup>3</sup>25</td><td align="&quot;center&quot;"><sup>3</sup>25</td><td align="&quot;center&quot;">15</td><td align="&quot;center&quot;">15</td></tr></tbody></table></div><div><p><sup>1</sup>The Contracting Officer may increase the Contractor''s sharing rate to as high as 75 percent for each VECP.</p><p><sup>2</sup>Same sharing arrangement as the contract''s profit or fee adjustment formula.</p><p><sup>3</sup>The Contracting Officer may increase the Contractor''s sharing rate to as high as 50 percent for each VECP.</p></div></div><p>(g) <i>Calculating net acquisition savings.</i> (1) Acquisition savings are realized when (i) the cost or price is reduced on the instant contract, (ii) reductions are negotiated in concurrent contracts, (iii) future contracts are awarded, or (iv) agreement is reached on a lump-sum payment for future contract savings (see subparagraph (i)(4) below). Net acquisition savings are first realized, and the Contractor shall be paid a share, when Government costs and any negative instant contract savings have been fully offset against acquisition savings.</p><p>(2) Except in incentive contracts, Government costs and any price or cost increases resulting from negative instant contract savings shall be offset against acquisition savings each time such savings are realized until they are fully offset. Then, the Contractor''s share is calculated by multiplying net acquisition savings by the appropriate Contractor''s percentage sharing rate (see paragraph (f) above). Additional Contractor shares of net acquisition savings shall be paid to the Contractor at the time realized.</p><p>(3) If this is an incentive contract, recovery of Government costs on the instant contract shall be deferred and offset against concurrent and future contract savings. The Contractor shall share through the contract incentive structure in savings on the instant contract items affected. Any negative instant contract savings shall be added to the target cost or to the target price and ceiling price, and the amount shall be offset against concurrent and future contract savings.</p><p>(4) If the Government does not receive and accept all items on which it paid the Contractor''s share, the Contractor shall reimburse the Government for the proportionate share of these payments.</p><p>(h) <i>Contract adjustment.</i> The modification accepting the VECP (or a subsequent modification issued as soon as possible after any negotiations are completed) shall-</p><p>(1) Reduce the contract price or estimated cost by the amount of instant contract savings, unless this is an incentive contract;</p><p>(2) When the amount of instant contract savings is negative, increase the contract price, target price and ceiling price, target cost, or estimated cost by that amount;</p><p>(3) Specify the Contractor''s dollar share per unit on future contracts, or provide the lump-sum payment;</p><p>(4) Specify the amount of any Government costs or negative instant contract savings to be offset in determining net acquisition savings realized from concurrent or future contract savings; and</p><p>(5) Provide the Contractor''s share of any net acquisition savings under the instant contract in accordance with the following:</p><p>(i) Fixed-price contracts-add to contract price.</p><p>(ii) Cost-reimbursement contracts-add to contract fee.</p><p>(i) <i>Concurrent and future contract savings.</i> (1) Payments of the Contractor''s share of concurrent and future contract savings shall be made by a modification to the instant contract in accordance with subparagraph (h)(5) above. For incentive contracts, shares shall be added as a separate firm-fixed-price line item on the instant contract. The Contractor shall maintain records adequate to identify the first delivered unit for 3 years after final payment under this contract.</p><p>(2) The Contracting Officer shall calculate the Contractor''s share of concurrent contract savings by (i) subtracting from the reduction in price negotiated on the concurrent contract any Government costs or negative instant contract savings not yet offset and (ii) multiplying the result by the Contractor''s sharing rate.</p><p>(3) The Contracting Officer shall calculate the Contractor''s share of future contract savings by (i) multiplying the future unit cost reduction by the number of future contract units scheduled for delivery during the sharing period, (ii) subtracting any Government costs or negative instant contract savings not yet offset, and (iii) multiplying the result by the Contractor''s sharing rate.</p><p>(4) When the Government wishes and the Contractor agrees, the Contractor''s share of future contract savings may be paid in a single lump sum rather than in a series of payments over time as future contracts are awarded. Under this alternate procedure, the future contract savings may be calculated when the VECP is accepted, on the basis of the Contracting Officer''s forecast of the number of units that will be delivered during the sharing period. The Contractor''s share shall be included in a modification to this contract (see subparagraph (h)(3) above) and shall not be subject to subsequent adjustment.</p><p>(5) Alternate no-cost settlement method. When, in accordance with subsection 48.104-4 of the Federal Acquisition Regulation, the Government and the Contractor mutually agree to use the no-cost settlement method, the following applies:</p><p>(i) The Contractor will keep all the savings on the instant contract and on its concurrent contracts only.</p><p>(ii) The Government will keep all the savings resulting from concurrent contracts placed on other sources, savings from all future contracts, and all collateral savings.</p><p>(j) <i>Collateral savings.</i> If a VECP is accepted, the Contracting Officer will increase the instant contract amount, as specified in paragraph (h)(5) of this clause, by a rate from 20 to 100 percent, as determined by the Contracting Officer, of any projected collateral savings determined to be realized in a typical year of use after subtracting any Government costs not previously offset. However, the Contractor''s share of collateral savings will not exceed the contract''s firm-fixed-price, target price, target cost, or estimated cost, at the time the VECP is accepted, or $100,000, whichever is greater. The Contracting Officer will be the sole determiner of the amount of collateral savings.</p><p>(k) <i>Relationship to other incentives.</i> Only those benefits of an accepted VECP not rewardable under performance, design-to-cost (production unit cost, operating and support costs, reliability and maintainability), or similar incentives shall be rewarded under this clause. However, the targets of such incentives affected by the VECP shall not be adjusted because of VECP acceptance. If this contract specifies targets but provides no incentive to surpass them, the value engineering sharing shall apply only to the amount of achievement better than target.</p><p>(l) <i>Subcontracts.</i> The Contractor shall include an appropriate value engineering clause in any subcontract of $150,000 or more and may include one in subcontracts of lesser value. In calculating any adjustment in this contract''s price for instant contract savings (or negative instant contract savings), the Contractor''s allowable development and implementation costs shall include any subcontractor''s allowable development and implementation costs, and any value engineering incentive payments to a subcontractor, clearly resulting from a VECP accepted by the Government under this contract. The Contractor may choose any arrangement for subcontractor value engineering incentive payments; <i>provided,</i> that the payments shall not reduce the Government''s share of concurrent or future contract savings or collateral savings.</p><p>(m) <i>Data.</i> The Contractor may restrict the Government''s right to use any part of a VECP or the supporting data by marking the following legend on the affected parts:</p>\n<p>âThese data, furnished under the Value Engineering clause of contract {{textbox_52.248-1[0]}}, shall not be disclosed outside the Government or duplicated, used, or disclosed, in whole or in part, for any purpose other than to evaluate a value engineering change proposal submitted under the clause. This restriction does not limit the Government''s right to use information contained in these data if it has been obtained or is otherwise available from the Contractor or from another source without limitations.â</p><p>If a VECP is accepted, the Contractor hereby grants the Government unlimited rights in the VECP and supporting data, except that, with respect to data qualifying and submitted as limited rights technical data, the Government shall have the rights specified in the contract modification implementing the VECP and shall appropriately mark the data. (The terms <i>unlimited rights</i> and <i>limited rights</i> are defined in part 27 of the Federal Acquisition Regulation.)</p>'
WHERE clause_id = 93192; -- 52.248-1
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size) VALUES (93192, 'textbox_52.248-1[0]', 'S', 100);

UPDATE Clauses
SET effective_date = '1990-03-01' -- '1990-01-01'
WHERE clause_id = 93193; -- 52.248-2

UPDATE Clauses
SET effective_date = '2010-10-01' -- '2010-01-01'
WHERE clause_id = 93195; -- 52.248-3

UPDATE Clauses
SET effective_date = '1984-04-01' -- '1984-01-01'
WHERE clause_id = 93197; -- 52.249-1

UPDATE Clauses
SET effective_date = '1984-04-01' -- '1984-01-01'
WHERE clause_id = 93201; -- 52.249-10

UPDATE Clauses
SET effective_date = '1984-04-01' -- '1984-01-01'
WHERE clause_id = 93202; -- 52.249-12

UPDATE Clauses
SET effective_date = '1984-04-01' -- '1984-01-01'
WHERE clause_id = 93203; -- 52.249-14

UPDATE Clauses
SET effective_date = '2012-04-01' -- '2012-01-01'
WHERE clause_id = 93207; -- 52.249-2

UPDATE Clauses
SET effective_date = '2012-04-01' -- '2012-01-01'
WHERE clause_id = 93209; -- 52.249-3

UPDATE Clauses
SET effective_date = '1984-04-01' -- '1984-01-01'
WHERE clause_id = 93210; -- 52.249-4

UPDATE Clauses
SET effective_date = '1996-09-01' -- '1996-01-01'
WHERE clause_id = 93211; -- 52.249-5

UPDATE Clauses
SET effective_date = '2004-05-01' -- '2004-01-01'
WHERE clause_id = 93217; -- 52.249-6

UPDATE Clauses
SET effective_date = '1984-04-01' -- '1984-01-01'
WHERE clause_id = 93218; -- 52.249-7

UPDATE Clauses
SET effective_date = '1984-04-01' -- '1984-01-01'
WHERE clause_id = 93220; -- 52.249-8

UPDATE Clauses
SET effective_date = '1984-04-01' -- '1984-01-01'
WHERE clause_id = 93221; -- 52.249-9

UPDATE Clauses
SET effective_date = '1984-04-01' -- '1984-01-01'
WHERE clause_id = 93223; -- 52.250-1

UPDATE Clauses
SET effective_date = '2009-02-01' -- '2009-01-01'
WHERE clause_id = 93224; -- 52.250-2

UPDATE Clauses
SET effective_date = '2009-02-01' -- '2009-01-01'
WHERE clause_id = 93227; -- 52.250-3

UPDATE Clauses
SET effective_date = '2009-02-01' -- '2009-01-01'
WHERE clause_id = 93230; -- 52.250-4

UPDATE Clauses
SET effective_date = '2009-02-01' -- '2009-01-01'
WHERE clause_id = 93231; -- 52.250-5

UPDATE Clauses
SET effective_date = '2012-04-01' -- '2012-01-01'
WHERE clause_id = 93232; -- 52.251-1

UPDATE Clauses
SET effective_date = '1998-02-01' -- '1998-01-01'
WHERE clause_id = 93234; -- 52.252-1

UPDATE Clauses
SET effective_date = '1998-02-01' -- '1998-01-01'
WHERE clause_id = 93235; -- 52.252-2

UPDATE Clauses
SET effective_date = '1984-04-01' -- '1984-01-01'
WHERE clause_id = 93236; -- 52.252-3

UPDATE Clauses
SET effective_date = '1984-04-01' -- '1984-01-01'
WHERE clause_id = 93237; -- 52.252-4

UPDATE Clauses
SET effective_date = '1984-04-01' -- '1984-01-01'
WHERE clause_id = 93238; -- 52.252-5

UPDATE Clauses
SET effective_date = '1984-04-01' -- '1984-01-01'
WHERE clause_id = 93239; -- 52.252-6

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1225_67001&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?node=se48.3.252_1225_67001&amp;rgn=div8'
WHERE clause_id = 93376; -- 252.225-7001 Alternate I

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1211_68&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?node=se48.2.52_1211_68&amp;rgn=div8'
WHERE clause_id = 92540; -- 52.211-8 Alternate I

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1211_68&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?node=se48.2.52_1211_68&amp;rgn=div8'
WHERE clause_id = 92541; -- 52.211-8 Alternate II

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1211_68&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?node=se48.2.52_1211_68&amp;rgn=div8'
WHERE clause_id = 92542; -- 52.211-8 Alternate III

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1211_69&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?node=se48.2.52_1211_69&amp;rgn=div8'
WHERE clause_id = 92544; -- 52.211-9 Alternate I

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1211_69&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?node=se48.2.52_1211_69&amp;rgn=div8'
WHERE clause_id = 92545; -- 52.211-9 Alternate II

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1211_69&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?node=se48.2.52_1211_69&amp;rgn=div8'
WHERE clause_id = 92546; -- 52.211-9 Alternate III

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1212_63&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?node=se48.2.52_1212_63&amp;rgn=div8'
WHERE clause_id = 92550; -- 52.212-3 Alternate I

UPDATE Clauses
SET effective_date = '2014-07-01' -- '2014-01-01'
WHERE clause_id = 92744; -- 52.222-35 Alternate I

UPDATE Clauses
SET effective_date = '2014-07-01' -- '2014-01-01'
WHERE clause_id = 92746; -- 52.222-36 Alternate I

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1225_611&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?node=se48.2.52_1225_611&amp;rgn=div8'
WHERE clause_id = 92802; -- 52.225-11 Alternate I

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1225_623&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?node=se48.2.52_1225_623&amp;rgn=div8'
WHERE clause_id = 92817; -- 52.225-23 Alternate I

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1225_64&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?node=se48.2.52_1225_64&amp;rgn=div8'
WHERE clause_id = 92832; -- 52.225-4 Alternate I

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1225_64&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?node=se48.2.52_1225_64&amp;rgn=div8'
WHERE clause_id = 92833; -- 52.225-4 Alternate II

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1225_64&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?node=se48.2.52_1225_64&amp;rgn=div8'
WHERE clause_id = 92834; -- 52.225-4 Alternate III

UPDATE Clauses
SET effective_date = '2007-06-01' -- '2007-01-01'
WHERE clause_id = 93066; -- 52.244-2 Alternate I

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1247_651&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?node=se48.2.52_1247_651&amp;rgn=div8'
WHERE clause_id = 93162; -- 52.247-51 Alternate I

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1247_651&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?node=se48.2.52_1247_651&amp;rgn=div8'
WHERE clause_id = 93163; -- 52.247-51 Alternate II

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1247_651&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?node=se48.2.52_1247_651&amp;rgn=div8'
WHERE clause_id = 93164; -- 52.247-51 Alternate III

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1248_61&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?node=se48.2.52_1248_61&amp;rgn=div8'
WHERE clause_id = 93189; -- 52.248-1 Alternate I

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1248_61&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?node=se48.2.52_1248_61&amp;rgn=div8'
WHERE clause_id = 93190; -- 52.248-1 Alternate II

UPDATE Clauses
SET clause_url = 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1248_61&rgn=div8' -- 'http://www.ecfr.gov/cgi-bin/text-idx?node=se48.2.52_1248_61&amp;rgn=div8'
WHERE clause_id = 93191; -- 52.248-1 Alternate III

/*
*** End of SQL Scripts at Wed Jul 01 10:36:04 EDT 2015 ***
*/
