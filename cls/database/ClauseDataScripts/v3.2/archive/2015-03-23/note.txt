CREATE TABLE etl_Prescriptions
(
	Prescription_Id      SMALLINT NOT NULL,
	Regulation_Id        SMALLINT NOT NULL,
	Prescription_Name    VARCHAR(60) NOT NULL,
	Prescription_Url     VARCHAR(120) NOT NULL,
	Prescription_Content TEXT NULL,
	Created_At           TIMESTAMP NULL,
	Updated_At           TIMESTAMP NULL,
	PRIMARY KEY (Prescription_Id)
);


SELECT CONCAT('CALL etlAddPrescription('
, '\'', Prescription_Name, '\', '
, Regulation_Id, ', '
, '\'', Prescription_Url, '\', '
, IF(Prescription_Content IS NULL, 'NULL', CONCAT('\'', Prescription_Content, '\''))
, ');') AS TEST
FROM etl_Prescriptions
ORDER BY Prescription_Id;


CREATE TABLE etl_Clause_Prescriptions
(
	Clause_Prescription_Id integer NOT NULL,
	Clause_Id            integer NOT NULL,
	Prescription_Id      SMALLINT NOT NULL,
	Created_At           TIMESTAMP,
	Updated_At           TIMESTAMP,
	PRIMARY KEY (Clause_Prescription_Id)
);

SELECT CONCAT('CALL etlAddClausePrescriptionByClauseId('
, A.Clause_Id, ', \''
, B.Prescription_Name
, '\');') TEST
FROM etl_clause_prescriptions A
INNER JOIN etl_prescriptions B ON (B.Prescription_Id = A.Prescription_Id)
ORDER BY A.Clause_Prescription_Id;


CREATE TABLE etl_Question_Choice_Prescriptions
(
	Question_Choice_Prescription_Id INTEGER NOT NULL,
	Question_Choce_Id    integer NOT NULL,
	Prescription_Id      SMALLINT NOT NULL,
	Created_At           TIMESTAMP NULL,
	Updated_At           TIMESTAMP NULL,
	PRIMARY KEY (Question_Choice_Prescription_Id)
);


SELECT CONCAT('CALL etlAddQuestionChoicePrescriptionByChoiceId('
, A.Question_Choce_Id, ', \''
, B.Prescription_Name
, '\');') TEST
FROM etl_Question_Choice_Prescriptions A
INNER JOIN etl_prescriptions B ON (B.Prescription_Id = A.Prescription_Id)

