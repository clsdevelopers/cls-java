/*
-- Query: select * from question_conditions
-- Date: 2015-03-26 19:21
*/
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`) 
VALUES (2806,101,4484,'ASK ONLY IF USER SELECTS "ARCHITECT-ENGINEERING" FOR "SERVICES TYPE"', 'ARCHITECT-ENGINEERING SERVICES = [SERVICES TYPE]', '2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (2807,148,4483,NULL,NULL,'2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (2808,179,4482,NULL,NULL,'2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (2809,186,4481,NULL,NULL,'2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (2810,188,4480,'ASK ONLY AFTER THE USER HAS SELECTED "BY LINE ITEM" TO THE QUESTION "ACRN ASSIGNMENT"', 'BY LINE ITEM = [ACRN ASSIGNMENT]', '2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (2811,187,4479,'ASK ONLY AFTER THE USER HAS SELECTED "BY CONTRACT WIDE" TO THE QUESTION "ACRN ASSIGNMENT"', 'BY CONTRACT WIDE = [ACRN ASSIGNMENT]', '2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (2812,59,4478,'ASK ONLY AFTER CONTRACTOR "BUSINESS TYPE" IS SELECTED', '[COMPETITION]E] IS NOT NULL', '2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (2813,36,4477,'ASK ONLY IF  ONE OF THE "COMPETITION" ANSWERS IS SELECTED', '[COMPETITION] IS NOT NULL', '2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (2814,191,4476,'ASK THIS QUESTION ONLY IF THE USER SELECTS "ADVANCE PAYMENTS" UNDER "FINANCING ARRANGEMENTS"', 'ADVANCE PAYMENTS = [FINANCING ARRANGEMENTS]', '2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (2815,5,4475,'a. AGREEMENT (INCLUDING BASIC AND LOAN)  \n                             [AGREEMENT TYPE]\n                            IF SELECTED USER CAN SELECT ONE ONLY', 'AGREEMENT (INCLUDING BASIC AND LOAN) = [AWARD TYPE]', '2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (2816,67,4474,'a. AIRCRAFT\nIF SELECTED, THE USER MUST SELECT ONE OR MORE', 'AIRCRAFT = [SUPPLIES TYPE]', '2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (2817,68,4473,'c. AMUNITION AND EXPLOSIVES\nIF SELECTED, THE USER MUST SELECT ONE OR MORE', 'AMUNITION AND EXPLOSIVES = [SUPPLIES TYPE]', '2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (2818,184,4472,'ASK ONLY AFTER ALL THE BASELINE AND FOLLOW-ON QUESTIONS HAVE BEEN ANSWERED FOR "LEGISLATIVE SOURCE OF FUNDS"', '[LEGISLATIVE SOURCE OF FUNDS] IS NOT NULL', '2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (2819,80,4471,'a. ARCHITECT-ENGINEERING SERVICES\nIF SELECTED, THE USER MUST SELECT\n               ONE OR MORE', 'ARCHITECT-ENGINEERING SERVICES = [SERVICES TYPE]', '2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (2820,219,4470,'ASK ONLY IF THE USER SELECTED "SOLICITATION" UNDER "DOCUMENT TYPE"', 'SOLICITATION = DOCUMENT TYPE', '2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (2821,4,4469,'2.  AWARD \n[AWARD TYPE]\n IF SELECTED USER CAN SELECT ONE ONLY', 'AWARD = DOCUMENT TYPE', '2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (2822,180,4468,'ASK AFTER USER SELECTS "CONTRACT ADMINISTRATION IS RETAINED BY THE CONTRACTING OFFICE" UNDER "ACO RESPONSIBILITIES".', 'CONTRACT ADMINISTRATION IS RETAINED BY THE CONTRACTING OFFICE = [ACO RESPONSIBILITIES]', '2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (2823,193,4467,'ASK THIS QUESTION ONLY IF THE USER SELECTS "BASED ON PERFORMANCE" UNDER "PROGRESS PAYMENT CONDITIONS"', 'BASED ON PERFORMANCE = [PROGRESS PAYMENT CONDITIONS]', '2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (2824,200,4466,'ASK ONLY IF THE USER SELECTS "A BID GUARANTEE OR BOND APPLIES" UNDER "INSURANCE CONDITIONS"', 'A BID GUARANTEE OR BOND APPLIES = [INSURANCE CONDITIONS]', '2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (2825,194,4465,NULL,NULL,'2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (2826,111,4464,'ASK ONLY IF USER SELECTS "FREIGHT TRANSPORTATION SERVICES (INCLUDING LOCAL DRAYAGE)" UNDER "TRANSPORTATION/STORAGE SERVICES"', 'FREIGHT TRANSPORTATION SERVICES (INCLUDING LOCAL DRAYAGE) = [TRANSPORTATION/STORAGE SERVICES]', '2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (2827,96,4463,'ASK THE QUESTION ONLY AFTER THE USER SELECTS EITHER "SUBSISTENCE" OR "PERISHABLE SUBSISTENCE" OR "SUBSISTENCE FOR BOTH GOVERNMENT USE AND RESALE IN THE SAME SCHEDULE" FOR "FOOD/TEXTILES"', 'SUBSISTENCE = [FOOD/TEXTILES] OR PERISHABLE SUBSISTENCE = [FOOD/TEXTILES] OR SUBSISTENCE FOR BOTH GOVERNMENT USE AND RESALE IN THE SAME SCHEDULE = [FOOD/TEXTILES]', '2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (2828,53,4462,NULL,NULL,'2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (2829,168,4461,'ASK ONLY IF "INTERNATIONAL OCEAN CARRIERS" IS SELECTED UNDER "MODE OF TRANSPORTATION"', 'INTERNATIONAL OCEAN CARRIERS = [MODE OF TRANSPORTATION]', '2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (2830,48,4460,NULL,NULL,'2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (2831,212,4459,NULL,NULL,'2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (2832,72,4458,'(iii) CLOTHING OR TEXTILES', 'CLOTHING OR TEXTILES = [FOOD/TEXTILES]', '2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (2833,85,4457,'ASK THE QUESTION ONLY AFTER THE USER SELECTS "SUPPLIES" UNDER "SUPPLIES OR SERVICES"', 'SUPPLIES = [SUPPLIES OR SERVICES]', '2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (2834,99,4456,'ASK THE QUESTION ONLY IS THE USER SELECTS "SERVICES" UNDER "SUPPLIES OR SERVICES"', 'SERVICES = [SUPPLIES OR SERVICES]', '2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (2835,102,4455,'ASK ONLY IF USER SELECTS "COMMUNICATION SERVICES (AND FACILITIES)" FOR "SERVICES TYPE"', 'COMMUNICATION SERVICES (AND FACILITIES) = [SERVICES TYPE]', '2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (2836,32,4454,'ASK ONLY AFTER USER ANSWERS THE QUESTION "COMPETITION"', '[COMPETITION] IS NOT NULL', '2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (2837,27,4453,'1.  COMPETITION\nUSER MAY SELECT ONE COMPETITION TYPE', 'COMPETITION = [PROCEDURES]', '2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (2838,69,4452,'d. COMPUTERS/INFORMATION TECHNOLOGY\nIF SELECTED, THE USER MUST \n             SELECT ONE OR MORE', 'COMPUTERS/INFORMATION TECHNOLOGY = [SUPPLIES TYPE]', '2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (2839,103,4451,'ASK ONLY IF USER SELECTS "CONSTRUCTION" FOR "SERVICES TYPE"', 'CONSTRUCTION = [SERVICES TYPE]', '2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (2840,106,4450,'ASK ONLY IF USER SELECTS "CONSTRUCTION" FOR "SERVICES TYPE"', 'CONSTRUCTION = [SERVICES TYPE]', '2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (2841,160,4449,NULL,NULL,'2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (2842,198,4448,NULL,NULL,'2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (2843,115,4447,NULL,NULL,'2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (2844,18,4446,NULL,NULL,'2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (2845,21,4445,'2. CIVILIAN\n[CONTRACTING CIVILIAN]', 'CIVILIAN = [ISSUING OFFICE]', '2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (2846,20,4444,'1. DEPARTMENT OF DEFENSE\n[CONTRACTING DEFENSE]', 'DEPARTMENT OF DEFENSE = [ISSUING OFFICE]', '2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (2847,137,4443,NULL,NULL,'2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (2848,60,4442,'ASK ONLY AFTER CONTRACTOR "BUSINESS TYPE" IS SELECTED', '[BUSINESS TYPE] IS NOT NULL', '2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (2849,116,4441,'1.  COST REIMBURSEMENT\nIF SELECTED, THE USER MUST SELECT ONE OR \n                 MORE', 'COST REIMBURSEMENT = [CONTRACT TYPE]', '2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (2850,211,4440,'ASK AFTER THE QUESTION "PRICING REQUIREMENTS" IS COMPLETED', '[PRICING REQUIREMENTS] IS NOT NULL', '2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (2851,130,4439,'ASK ONLY IF THE USER SELECTS "RADIO FREQUENCY IDENTIFICATION (RFID) IS REQUIRED" UNDER "GOVERNMENT REVIEWS".', 'RADIO FREQUENCY IDENTIFICATION (RFID) IS REQUIRED = [GOVERNMENT REVIEWS]', '2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (2852,159,4438,NULL,NULL,'2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (2853,35,4437,'ASK ONLY IF  ONE OF THE "COMPETITION" ANSWERS IS SELECTED', '[COMPETITION] IS NOT NULL', '2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (2854,214,4436,NULL,NULL,'2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (2855,107,4435,'ASK ONLY IF THE USER SELECTS "DISMANTLING, DEMOLITION, OR REMOVAL OF  IMPROVEMENTS" UNDER "SERVICES TYPE"', 'DISMANTLING, DEMOLITION, OR REMOVAL OF  IMPROVEMENTS = [SERVICES TYPE]', '2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (2856,202,4434,NULL,NULL,'2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (2857,42,4433,NULL,NULL,'2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (2858,1,4432,NULL,NULL,'2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (2859,135,4431,'ASK ONLY AFTER ALL OF THE "LABOR REQUIREMENTS" RESPONSES ARE COMPLETED', '[LABOR REQUIREMENTS] IS NOT NULL', '2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (2860,174,4430,NULL,NULL,'2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (2861,57,4429,'4. EDUCATIONAL SOURCE\nIF SELECTED, USER MUST SELECT ONE ONLY', 'EDUCATIONAL SOURCE = [BUSINESS TYPE]', '2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (2862,195,4428,'ASK THIS QUESTION ONLY IF THE USER SELECTS "ELECTRONIC FUNDS TRANSFER EXCEPTION" UNDER "BILLING ARRANGEMENTS"', 'ELECTRONIC FUNDS TRANSFER EXCEPTION = [BILLING ARRANGEMENTS]', '2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (2863,145,4427,NULL,NULL,'2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (2864,92,4426,'ASK THE QUESTION ONLY AFTER THE USER SELECTS "SUPPLIES" UNDER "SUPPLIES OR SERVICES"', 'SUPPLIES = [SUPPLIES OR SERVICES]', '2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (2865,142,4425,NULL,NULL,'2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (2866,94,4424,'ASK THE FOLLOWING QUESTIONS ONLY IF THE USER SELECTS "PERSONAL COMPUTER PRODUCTS" UNDER "COMPUTERS/INFORMATION TECHNOLOGY"', 'PERSONAL COMPUTER PRODUCTS = COMPUTERS/INFORMATION TECHNOLOGY', '2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (2867,220,4423,NULL,NULL,'2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (2868,11,4422,'ASK THE FOLLOWING QUESTION ONLY AFTER  "REQUIREMENTS" IS SELECTED UNDER "TYPE OF INDEFINITE DELIVERY"', 'REQUIREMENTS = TYPE OF INDEFINITE DELIVERY', '2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (2869,205,4421,NULL,NULL,'2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (2870,208,4420,'ASK AFTER THE QUESTION "PRICING REQUIREMENTS" IS COMPLETED', '[PRICING REQUIREMENTS] IS NOT NULL', '2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (2871,204,4419,NULL,NULL,'2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (2872,172,4418,NULL,NULL,'2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (2873,33,4417,'ASK ONLY IF  ONE OF THE "COMPETITION" ANSWERS IS SELECTED', '[COMPETITION] IS NOT NULL', '2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (2874,190,4416,NULL,NULL,'2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (2875,129,4415,'ASK ONLY IF THE USER HAS SELECTED "FIRST ARTICLE APPROVAL IS REQUIRED" UNDER "GOVERNMENT REVIEWS".', 'FIRST ARTICLE APPROVAL IS REQUIRED = [GOVERNMENT REVIEWS]', '2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (2876,117,4414,'2.  FIXED PRICE\n[FIXED PRICE]\nIF SELECTED, THE USER MUST SELECT ONE OR MORE', 'FIXED PRICE = [CONTRACT TYPE]', '2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (2877,178,4413,NULL,NULL,'2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (2878,176,4412,NULL,NULL,'2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (2879,70,4411,'i. FOOD/TEXTILES\nIF SELECTED, THE USER MUST SELECT ONE OR MORE', 'FOOD/TEXTILES = [SUPPLIES TYPE]', '2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (2880,71,4410,'(i) FOOD', 'FOOD = [FOOD/TEXTILES]', '2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (2881,154,4409,'ASK ONLY IF USER SELECTS "FOREIGN COUNTRY" UNDER "PLACE OF PERFORMANCE"', 'FOREIGN COUNTRY = PLACE OF PERFORMANCE', '2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (2882,56,4408,'3. FOREIGN SOURCE\nIF SELECTED, USER MUST SELECT ONE ONLY', 'FOREIGN SOURCE = [BUSINESS TYPE]', '2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (2883,63,4407,NULL,NULL,'2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (2884,73,4406,'k. FUELS AND LUBRICANTS\nIF SELECTED, THE USER MUST SELECT ONE OR \n                 MORE', 'FUELS AND LUBRICANTS = [SUPPLIES TYPE]', '2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (2885,40,4405,'ASK AFTER USER ANSWERS THE QUESTION "PROCEDURES" AND THE ANSWER IS NOT "FULL AND OPEN COMPETITION"', '[PROCEDURES] IS NOT NULL', '2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (2886,189,4404,NULL,NULL,'2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (2887,181,4403,NULL,NULL,'2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (2888,64,4402,NULL,NULL,'2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (2889,138,4401,'ASK ONLY IF THE USER HAS SELECTED "GOVERNMENT FURNISHED PROPERTY (GFP) IS PROVIDED" UNDER "CONTRACTOR MATERIAL"', 'GOVERNMENT FURNISHED PROPERTY (GFP) IS PROVIDED = CONTRACTOR MATERIAL', '2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (2890,158,4400,'1. GOVERNMENT OWNED OR LEASED FACILITIES/INSTALLATIONS\n[GOVERNMENT OWNED]', 'GOVERNMENT OWNED OR LEASED FACILITIES/INSTALLATIONS = PLACE TYPE', '2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (2891,127,4399,NULL,NULL,'2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (2892,55,4398,'2. GOVERNMENT SOURCE\nIF SELECTED, USER MUST SELECT ONE ONLY', 'GOVERNMENT SOURCE = [BUSINESS TYPE]', '2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (2893,139,4397,'ASK ONLY IF USER SELECTS "GOVERNMENT SUPPLY SOURCE INFORMATION/AUTHORIZATION APPLIES" UNDER "CONTRACTOR MATERIAL"', 'GOVERNMENT SUPPLY SOURCE INFORMATION/AUTHORIZATION APPLIES = CONTRACTOR MATERIAL', '2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (2894,95,4396,'ASK THE QUESTION ONLY IF THE USER SELECTS "HAZARDOUS MATERIAL" UNDER "SUPPLIES TYPE"', 'HAZARDOUS MATERIAL = [SUPPLIES TYPE]', '2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (2895,61,4395,'ASK ONLY IF THE USER SELECTS "HISTORICALLY UNDERUTILIZED BUSINESS ZONE" UNDER "BUSINESS TYPE"', 'HISTORICALLY UNDERUTILIZED BUSINESS ZONE = [BUSINESS TYPE]', '2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (2896,173,4394,NULL,NULL,'2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (2897,210,4393,'ASK AFTER THE QUESTION "PRICING REQUIREMENTS" IS COMPLETED', '[PRICING REQUIREMENTS] IS NOT NULL', '2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (2898,84,4392,'ASK THE QUESTION ONLY AFTER THE USER SELECTS "SUPPLIES" UNDER "SUPPLIES OR SERVICES"', 'SUPPLIES = [SUPPLIES OR SERVICES]', '2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (2899,10,4391,'ASK ONLY IF THE USER HAS SELECTED AN "INDEFINITE DELIVERY CONTRACT" UNDER "AWARD TYPE".', 'INDEFINITE DELIVERY CONTRACT = [AWARD TYPE]', '2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (2900,215,4390,NULL,NULL,'2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (2901,147,4389,NULL,NULL,'2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (2902,199,4388,NULL,NULL,'2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (2903,143,4387,NULL,NULL,'2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (2904,112,4386,'ASK ONLY IF "RELOCATION OF FEDERAL OFFICES" IS SELECTED UNDER "TRANSPORTATION/STORAGE SERVICES"', 'RELOCATION OF FEDERAL OFFICES = [TRANSPORTATION/STORAGE SERVICES]', '2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (2905,19,4385,NULL,NULL,'2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (2906,133,4384,'ASK ONLY AFTER ALL OF THE "LABOR REQUIREMENTS" RESPONSES ARE COMPLETED', '[LABOR REQUIREMENTS] IS NOT NULL', '2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (2907,136,4383,NULL,NULL,'2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (2908,132,4382,NULL,NULL,'2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (2909,113,4381,'ASK ONLY IF THE USER SELECTS "LAUNDRY AND DRY CLEANING SERVICES" UNDER "SERVICES TYPE"', 'LAUNDRY AND DRY CLEANING SERVICES = [SERVICES TYPE]', '2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (2910,16,4380,'ASK ONLY IF THE USER SELECTS "LEASE AGREEMENT" UNDER "AWARD TYPE"', 'LEASE AGREEMENT = [AWARD TYPE]', '2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (2911,183,4379,'ASK ONLY AFTER THE "FUNDING FY" HAS BEEN SELECTED', 'FUNDING FY IS NOT NULL', '2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (2912,185,4378,'ASK ONLY AFTER ALL THE BASELINE AND FOLLOW-ON QUESTIONS HAVE BEEN ANSWERED FOR "LEGISLATIVE SOURCE OF FUNDS"', '[LEGISLATIVE SOURCE OF FUNDS] IS NOT NULL', '2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (2913,161,4377,NULL,NULL,'2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (2914,81,4376,'j. MAINTENANCE, REPAIR, INSTALLATION AND OVERHAUL SERVICES  \n               [MAINTENANCE SERVICES]\n               IF SELECTED, THE USER MUST SELECT ONE OR MORE', 'MAINTENANCE, REPAIR, INSTALLATION AND OVERHAUL SERVICES = [SERVICES TYPE]', '2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (2915,118,4375,'ASK ONLY IF THE USER SELECTS "FIXED PRICE WITH ECONOMIC PRICE  ADJUSTMENT -- ACTUAL COSTS" OR "FIXED PRICE WITH ECONOMIC PRICE  ADJUSTMENT -- COST INDICIES" OR "FIXED PRICE WITH ECONOMIC PRICE ADJUSTMENT - ESTABLISHED PRICES" UNDER "FIXED PRICE"', 'FIXED PRICE WITH ECONOMIC PRICE  ADJUSTMENT -- ACTUAL COSTS = FIXED PRICE OR FIXED PRICE WITH ECONOMIC PRICE  ADJUSTMENT -- COST INDICIES = FIXED PRICE OR FIXED PRICE WITH ECONOMIC PRICE ADJUSTMENT - ESTABLISHED PRICES = FIXED PRICE', '2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (2916,86,4374,'ASK THE QUESTION ONLY AFTER THE USER SELECTS "SUPPLIES" UNDER "SUPPLIES OR SERVICES"', 'SUPPLIES = [SUPPLIES OR SERVICES]', '2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (2917,74,4373,'l.  METALS\nIF SELECTED, THE USER MUST SELECT ONE OR MORE', 'METALS = [SUPPLIES TYPE]', '2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (2918,156,4372,'ASK ONLY IF USER SELECTS "AFGHANISTAN" UNDER "FOREIGN COUNTRY"', 'AFGHANISTAN = FOREIGN COUNTRY', '2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (2919,75,4371,'n. MISCELLANEOUS END ITEMS\nIF SELECTED, THE USER MUST SELECT ONE OR\n             MORE', 'MISCELLANEOUS END ITEMS = [SUPPLIES TYPE]', '2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (2920,58,4370,'5. MISCELLANEOUS SOURCE\nIF SELECTED, USER MUST SELECT ONE ONLY', 'MISCELLANEOUS SOURCE = [BUSINESS TYPE]', '2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (2921,45,4369,NULL,NULL,'2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (2922,167,4368,NULL,NULL,'2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (2923,7,4367,'ASK ONLY AFTER  "AWARD TYPE" SELECTION IS COMPLETED', '[AWARD TYPE] IS NOT NULL', '2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (2924,29,4366,'5.  NEGOTIATION (FAR PART 15)', 'NEGOTIATION (FAR PART 15) = [PROCEDURES]', '2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (2925,201,4365,NULL,NULL,'2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (2926,124,4364,'ASK ONLY IF THE USER SELECTS "YES" TO THE QUESTION "OPTIONS"', 'YES = OPTIONS', '2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (2927,122,4363,NULL,NULL,'2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (2928,13,4362,'ASK ONLY IF THE USER SELECTS "PURCHASE ORDER" UNDER "AWARD TYPE"  OR "ORDER" UNDER "AWARD TYPE"', 'PURCHASE ORDER = [AWARD TYPE]', '2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (2929,17,4361,'ASK ONLY IF THE USER HAS SELECTED AN "AGREEMENT" UNDER "AWARD TYPE" OR A "REQUIREMENTS CONTRACT" UNDER "TYPE OF INDEFINITE DELIVERY"', 'AGREEMENT = [AWARD TYPE]', '2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (2930,52,4360,'ASK THE QUESTION ONLY AFTER THE USER SELECTS EITHER "MAJOR DEFENSE PROGRAM" OR "MAJOR INFORMATION SYSTEMS PROGRAM" TO THE QUESTION "TYPE OF SYSTEM"', 'MAJOR DEFENSE PROGRAM = TYPE OF SYSTEM OR MAJOR INFORMATION SYSTEMS PROGRAM = TYPE OF SYSTEM', '2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (2931,206,4359,NULL,NULL,'2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (2932,196,4358,NULL,NULL,'2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (2933,9,4357,'ASK THE FOLLOWING QUESTION ONLY IF THE USER HAS SELECTED AN "INDEFINITE DELIVERY CONTRACT" UNDER "AWARD TYPE"', 'INDEFINITE DELIVERY CONTRACT = [AWARD TYPE]', '2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (2934,119,4356,'ASK ONLY IF THE USER SELECTS "FIXED PRICE WITH ECONOMIC PRICE  ADJUSTMENT -- ACTUAL COSTS" OR "FIXED PRICE WITH ECONOMIC PRICE  ADJUSTMENT -- COST INDICIES" OR "FIXED PRICE WITH ECONOMIC PRICE ADJUSTMENT - ESTABLISHED PRICES" UNDER "FIXED PRICE"', 'FIXED PRICE WITH ECONOMIC PRICE  ADJUSTMENT -- ACTUAL COSTS = FIXED PRICE OR FIXED PRICE WITH ECONOMIC PRICE  ADJUSTMENT -- COST INDICIES = FIXED PRICE OR FIXED PRICE WITH ECONOMIC PRICE ADJUSTMENT - ESTABLISHED PRICES = FIXED PRICE', '2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (2935,125,4355,NULL,NULL,'2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (2936,152,4354,NULL,NULL,'2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (2937,22,4353,'ASK AFTER USER SELECTS THE "ISSUING OFFICE"', 'ISSUING OFFICE IS NOT NULL', '2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (2938,153,4352,NULL,NULL,'2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (2939,44,4351,NULL,NULL,'2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (2940,157,4350,NULL,NULL,'2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (2941,114,4349,'ASK ONLY IF THE USER SELECTS "MORTUARY SERVICES" UNDER "SERVICES TYPE"', 'MORTUARY SERVICES = [SERVICES TYPE]', '2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (2942,104,4348,'ASK THE QUESTION ONLY IF THE USER SELECTS "CONSTRUCTION" FOR "SERVICES TYPE"', 'CONSTRUCTION = [SERVICES TYPE]', '2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (2943,146,4347,'ASK ONLY IF THE USER SELECTED "SOLICITATION" UNDER "DOCUMENT TYPE"', 'SOLICITATION = DOCUMENT TYPE', '2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (2944,221,4346,'ASK ONLY IF "PRICE IS A SELECTION FACTOR" IS SELECTED UNDER "EVALUATION FACTORS" AND  "SOLICITATION" UNDER "DOCUMENT TYPE".', 'PRICE IS A SELECTION FACTOR = EVALUATION FACTORS', '2015-03-26 19:16:49', '2015-03-26 19:20:30');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (2945,216,4345,NULL,NULL,'2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (2946,207,4344,NULL,NULL,'2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (2947,26,4343,NULL,NULL,'2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (2948,192,4342,'ASK THIS QUESTION ONLY IF THE USER SELECTS "PROGRESS PAYMENTS" UNDER "FINANCING ARRANGEMENTS"', 'PROGRESS PAYMENTS = [FINANCING ARRANGEMENTS]', '2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (2949,90,4341,'ASK THE QUESTION ONLY AFTER THE USER SELECTS "SUPPLIES" UNDER "SUPPLIES OR SERVICES"', 'SUPPLIES = [SUPPLIES OR SERVICES]', '2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (2950,218,4340,NULL,NULL,'2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (2951,3,4339,'c.  REQUEST FOR PROPOSAL\n                      [PROPOSAL TYPE]\n                      IF "REQUESST FOR PROPOSAL" IS SELECTED, USER MUST SELECT ONE \n               OR BOTH', 'REQUEST FOR PROPOSAL = SOLICITATION TYPE', '2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (2952,15,4338,'ASK ONLY IF  ANY "ORDER" IS SELECTED UNDER "AWARD TYPE"', 'ORDER = [AWARD TYPE]', '2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (2953,43,4337,NULL,NULL,'2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (2954,121,4336,NULL,NULL,'2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (2955,109,4335,'ASK ONLY IF THE USER SELECTS "RESEARCH AND DEVELOPMENT" UNDER "SERVICES TYPE" AND "MAJOR DEFENSE PROGRAM" UNDER "ACQUISITION PLANNING"', 'RESEARCH AND DEVELOPMENT = [SERVICES TYPE] AND MAJOR DEFENSE PROGRAM = ACQUISITION PLANNING', '2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (2956,197,4334,NULL,NULL,'2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (2957,88,4333,'ASK ONLY IF THE USER SELECTS "EPA-DESIGNATED ITEM(S) CONTAINING RECOVERED MATERIALS" UNDER "SUPPLY CLASSIFICATION"', 'EPA-DESIGNATED ITEM(S) CONTAINING RECOVERED MATERIALS = SUPPLY CLASSIFICATION', '2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (2958,131,4332,NULL,NULL,'2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (2959,213,4331,NULL,NULL,'2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (2960,93,4330,'ASK THE QUESTION ONLY AFTER THE USER SELECTS "DATA, DRAWINGS AND REPORTS" UNDER "SUPPLIES TYPE"', 'DATA, DRAWINGS AND REPORTS = [SUPPLIES TYPE]', '2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (2961,23,4329,NULL,NULL,'2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (2962,25,4328,'2. CIVILIAN\n[REQUIRING CIVILIAN]', 'CIVILIAN = REQUIRING ACTIVITY', '2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (2963,24,4327,'1. DEPARTMENT OF DEFENSE\n[REQUIRING DEFENSE]', 'DEPARTMENT OF DEFENSE = REQUIRING ACTIVITY', '2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (2964,82,4326,'n. RESEARCH, DEVELOPMENT AND EXPERIMENTAL SERVICES  \n               IF SELECTED, THE USER MUST SELECT ONE OR MORE', 'RESEARCH, DEVELOPMENT AND EXPERIMENTAL SERVICES = [SERVICES TYPE]', '2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (2965,76,4325,'o.  RING FORGINGS\nIF SELECTED, THE USER MUST SELECT ONE OR MORE', 'RING FORGINGS = [SUPPLIES TYPE]', '2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (2966,46,4324,NULL,NULL,'2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (2967,47,4323,'ASK ONLY IF THE USER HAS SELECTED "YES" TO THE QUESTION "SAFETY ACT APPLICABILITY"', 'YES = SAFETY ACT APPLICABILITY', '2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (2968,49,4322,NULL,NULL,'2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (2969,39,4321,'ASK ONLY IF "COMMERICAL PROCEDURES (FAR PART 12)" IS SELECTED UNDER "PROCEDURES"', 'COMMERICAL PROCEDURES (FAR PART 12) = [PROCEDURES]', '2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (2970,37,4320,'ASK ONLY IF "SEALED BIDDING" IS SELECTED UNDER "PROCEDURES" AND  "SOLICITATION" UNDER "IS THIS A SOLICITATION OR AWARD?"', 'SEALED BIDDING = [PROCEDURES]', '2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (2971,41,4319,NULL,NULL,'2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (2972,162,4318,NULL,NULL,'2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (2973,97,4317,'ASK THE QUESTION ONLY IS THE USER SELECTS "SERVICES" UNDER "SUPPLIES OR SERVICES"', 'SERVICES = [SUPPLIES OR SERVICES]', '2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (2974,98,4316,'ASK THE QUESTION ONLY IS THE USER SELECTS "SERVICES" UNDER "SUPPLIES OR SERVICES"', 'SERVICES = [SUPPLIES OR SERVICES]', '2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (2975,100,4315,'ASK THE QUESTION ONLY IS THE USER SELECTS "SERVICES" UNDER "SUPPLIES OR SERVICES"', 'SERVICES = [SUPPLIES OR SERVICES]', '2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (2976,79,4314,'2. SERVICES  \n[SERVICES TYPE]', 'SERVICES = [SUPPLIES OR SERVICES]', '2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (2977,28,4313,'h. SET-ASIDE\n(SELECT ONE ONLY)', 'SET-ASIDE = [COMPETITION]', '2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (2978,77,4312,'p. SHIPBUILDING\nIF SELECTED, THE USER MUST SELECT ONE OR MORE', 'SHIPBUILDING = [SUPPLIES TYPE]', '2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (2979,164,4311,NULL,NULL,'2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (2980,169,4310,NULL,NULL,'2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (2981,171,4309,NULL,NULL,'2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (2982,170,4308,NULL,NULL,'2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (2983,165,4307,NULL,NULL,'2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (2984,166,4306,NULL,NULL,'2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (2985,175,4305,NULL,NULL,'2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (2986,105,4304,'ASK THE QUESTION ONLY IF THE USER SELECTS "CONSTRUCTION" FOR "SERVICES TYPE"', 'CONSTRUCTION = [SERVICES TYPE]', '2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (2987,123,4303,'ASK ONLY IF USER SELECTS "COST PLUS AWARD FEE" UNDER "CONTRACT TYPE" OR "FIXED PRICE AWARD FEE" UNDER "FIXED PRICE"', 'COST PLUS AWARD FEE = FIXED PRICE AWARD FEE OR CONTRACT TYPE = FIXED PRICE AWARD FEE', '2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (2988,31,4302,'6.  SMALL BUSINESS PROGRAM\nuser must select one', 'SMALL BUSINESS PROGRAM = [PROCEDURES]', '2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (2989,54,4301,'1. SMALL BUSINESS/ SOCIOECONOMIC SOURCE\nIF SELECTED, USER HAS TO SELECT ONE OR MORE OF THE FOLLOWING:', 'SMALL BUSINESS/ SOCIOECONOMIC SOURCE = [BUSINESS TYPE]', '2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (2990,30,4300,'a.   SOLE SOURCE\n                   if selected, user must select one', 'SOLE SOURCE = NEGOTIATION (FAR PART 15)', '2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (2991,6,4299,'ASK THE FOLLOWING QUESTION ONLY IF "SOLICITATION" IS SELECTED UNDER "DOCUMENT TYPE".', 'SOLICITATION = DOCUMENT TYPE', '2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (2992,2,4298,'1.  SOLICITATION\n[SOLICITATION TYPE]\n(IF SOLICITATION IS SELECTED THEN AWARD SHOULD ALSO BE PRESENTED TO THE USER) -  IF SELECTED USER CAN SELECT ONE ONLY', 'SOLICITATION = DOCUMENT TYPE', '2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (2993,50,4297,NULL,NULL,'2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (2994,155,4296,'ASK ONLY IF USER SELECTS "UNITED STATES  (50 PLUS DISTRICT OF COLUMBIA)" UNDER "PLACE OF PERFORMANCE"', 'UNITED STATES  (50 PLUS DISTRICT OF COLUMBIA) = PLACE OF PERFORMANCE', '2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (2995,141,4295,'ASK ONLY IF THE USER SELECTS "SUBCONTRACTING POSSIBILITIES EXIST" UNDER "SUBCONTRACTING"', 'SUBCONTRACTING POSSIBILITIES EXIST = SUBCONTRACTING', '2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (2996,140,4294,NULL,NULL,'2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (2997,65,4293,NULL,NULL,'2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (2998,163,4292,NULL,NULL,'2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (2999,66,4291,'1. SUPPLIES\n[SUPPLIES TYPE]', 'SUPPLIES = [SUPPLIES OR SERVICES]', '2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (3000,87,4290,'ASK THE QUESTION ONLY AFTER THE USER SELECTS "SUPPLIES" UNDER "SUPPLIES OR SERVICES"', 'SUPPLIES = [SUPPLIES OR SERVICES]', '2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (3001,120,4289,'ASK ONLY IF THE USER SELECTS "COST PLUS INCENTIVE FEE (COST BASED)" UNDER "FIXED PRICE"', 'COST PLUS INCENTIVE FEE (COST BASED) = FIXED PRICE', '2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (3002,14,4288,'ASK ONLY IF  "TASK ORDER" IS SELECTED UNDER "TYPE OF ORDER"', 'TASK ORDER = TYPE OF ORDER', '2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (3003,126,4287,'ASK ONLY AFTER "PERFORMANCE REQUIREMENTS" HAS BEEN ANSWERED', 'PERFORMANCE REQUIREMENTS IS NOT NULL', '2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (3004,144,4286,NULL,NULL,'2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (3005,91,4285,'ASK THE QUESTION ONLY AFTER THE USER SELECTS "SUPPLIES" UNDER "SUPPLIES OR SERVICES"', 'SUPPLIES = [SUPPLIES OR SERVICES]', '2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (3006,62,4284,'ASK ONLY AFTER  ANY SELECTION WITHIN "FOREIGN SOURCE" UNDER "BUSINESS TYPE" IS SELECTED.', 'FOREIGN SOURCE = [BUSINESS TYPE]', '2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (3007,217,4283,NULL,NULL,'2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (3008,83,4282,'q. TRANSPORTATION/STORAGE SERVICES\n           IF SELECTED, THE USER MUST SELECT ONE OR MORE', 'TRANSPORTATION/STORAGE SERVICES = [SERVICES TYPE]', '2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (3009,89,4281,'ASK THE QUESTION ONLY AFTER THE USER SELECTS "SUPPLIES" UNDER "SUPPLIES OR SERVICES"', 'SUPPLIES = [SUPPLIES OR SERVICES]', '2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (3010,8,4280,'ASK THE FOLLOWING QUESTION ONLY AFTER  "INDEFINITE DELIVERY CONTRACT" IS SELECTED UNDER "AWARD TYPE"', 'INDEFINITE DELIVERY CONTRACT = [AWARD TYPE]', '2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (3011,12,4279,'ASK ONLY IF THE USER SELECTS "PURCHASE ORDER" OR "CHANGE ORDER" OR "ORDER" UNDER "AWARD TYPE"', 'PURCHASE ORDER = [AWARD TYPE] OR CHANGE ORDER = [AWARD TYPE] OR ORDER = [AWARD TYPE]', '2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (3012,51,4278,NULL,NULL,'2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (3013,182,4277,'ASK ONLY AFTER THE  "FUNDING FY" HAS BEEN SELECTED', 'FUNDING FY IS NOT NULL', '2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (3014,209,4276,'ASK AFTER THE QUESTION "PRICING REQUIREMENTS" IS COMPLETED', '[PRICING REQUIREMENTS] IS NOT NULL', '2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (3015,34,4275,'ASK ONLY IF  ONE OF THE "COMPETITION" ANSWERS IS SELECTED', '[COMPETITION] IS NOT NULL', '2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (3016,134,4274,'ASK ONLY AFTER ALL OF THE "LABOR REQUIREMENTS" RESPONSES ARE COMPLETED', '[LABOR REQUIREMENTS] IS NOT NULL', '2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (3017,108,4273,'ASK ONLY IF THE USER SELECTS "UTILITY SERVICES" UNDER "SERVICES TYPE"', 'UTILITY SERVICES = [SERVICES TYPE]', '2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (3018,128,4272,'ASK ONLY IF THE USER HAS SELECTED "VALUE ENGINEERING" UNDER "GOVERNMENT REVIEWS"', 'VALUE ENGINEERING = [GOVERNMENT REVIEWS]', '2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (3019,78,4271,'r. VEHICLES\nIF SELECTED, THE USER MUST SELECT ONE OR MORE', 'VEHICLES = [SUPPLIES TYPE]', '2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (3020,38,4270,'ASK ONLY IF THE USER SELECTS "SET-ASIDE" UNDER "COMPETITION"', 'SET-ASIDE = [COMPETITION]', '2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (3021,203,4269,NULL,NULL,'2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (3022,149,4268,NULL,NULL,'2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (3023,151,4267,NULL,NULL,'2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (3024,150,4266,NULL,NULL,'2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (3025,110,4265,'ASK ONLY IF THE USER SELECTS "RESEARCH AND DEVELOPMENT" UNDER "SERVICES TYPE"', 'RESEARCH AND DEVELOPMENT = [SERVICES TYPE]', '2015-03-26 19:16:49', '2015-03-26 19:16:49');
INSERT INTO Question_Conditions (`Question_Condition_Id`,`Question_Condition_Sequence`,`Question_Id`,`Original_Condition_Text`,`Condition_To_Question`,`Created_At`,`Updated_At`)
VALUES (3026,177,4264,NULL,NULL,'2015-03-26 19:16:49', '2015-03-26 19:16:49');
