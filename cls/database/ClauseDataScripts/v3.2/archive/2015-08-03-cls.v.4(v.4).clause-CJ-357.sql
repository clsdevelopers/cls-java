/*
Clause Parser Run at Mon Aug 03 16:38:19 EDT 2015
(Without Correction Information)
*/
/*
<h2>52.204-1</h2>
No identifier
<pre>(If applicable, the rule for the prescription for using this clause will be in the component clause</pre><br />Empty Rule
*/
/* ===============================================
 * Clause
   =============================================== */

UPDATE Clauses
SET clause_data = '<p>This contract supersedes contract No. {{textbox_252.241-7000[0]}}, dated {{textbox_252.241-7000[1]}} which provided similar services. Any capital credits accrued to the Government, any remaining credits due to the Government under the connection charge, or any termination liability are transferred to this contract, as follows:</p><h1>Capital Credits</h1><p>(List years and accrued credits by year and separate delivery points.)<br />{{capital_252.241-7000[0]}}{{capital_252.241-7000[1]}}{{capital_252.241-7000[2]}}</p><h1>Outstanding Connection Charge Credits</h1><p>(List by month and year the amount credited and show the remaining amount of outstanding credits due the Government.)<br />{{outstanding_252.241-7000[0]}}{{outstanding_252.241-7000[1]}}{{outstanding_252.241-7000[2]}}{{outstanding_252.241-7000[3]}}</p><h1>Termination Liability Charges</h1><p>(List by month and year the amount of monthly facility cost recovered and show the remaining amount of facility cost to be recovered.)<br />{{termination_252.241-7000[0]}}{{termination_252.241-7000[1]}}{{termination_252.241-7000[2]}}{{termination_252.241-7000[3]}}</p>'
--               '<p>This contract supersedes contract No. {{textbox_252.241-7000[0]}}, dated {{textbox_252.241-7000[1]}} which provided similar services. Any capital credits accrued to the Government, any remaining credits due to the Government under the connection charge, or any termination liability are transferred to this contract, as follows:</p><h1>Capital Credits</h1><p>(List years and accrued credits by year and separate delivery points.)<br />{{memo_252.241-7000[0]}}</p><h1>Outstanding Connection Charge Credits</h1><p>(List by month and year the amount credited and show the remaining amount of outstanding credits due the Government.)<br />{{memo_252.241-7000[1]}}</p><h1>Termination Liability Charges</h1><p>(List by month and year the amount of monthly facility cost recovered and show the remaining amount of facility cost to be recovered.)<br />{{memo_252.241-7000[2]}}</p>'
WHERE clause_version_id = 4 AND clause_name = '252.241-7000';

INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size, fill_in_group_number, fill_in_placeholder, fill_in_default_data, fill_in_display_rows, fill_in_for_table, fill_in_heading) VALUES (122145, 'capital_252.241-7000[0]', 'N', 4, 0, 'Year', NULL, 5, 1, 'Year');
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size, fill_in_group_number, fill_in_placeholder, fill_in_default_data, fill_in_display_rows, fill_in_for_table, fill_in_heading) VALUES (122145, 'capital_252.241-7000[1]', '$', 12, 0, 'Accrued credits', NULL, 5, 1, 'Accrued credits');
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size, fill_in_group_number, fill_in_placeholder, fill_in_default_data, fill_in_display_rows, fill_in_for_table, fill_in_heading) VALUES (122145, 'capital_252.241-7000[2]', 'S', 60, 0, 'Delivery points', NULL, 5, 1, 'Delivery points');
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size, fill_in_group_number, fill_in_placeholder, fill_in_default_data, fill_in_display_rows, fill_in_for_table, fill_in_heading) VALUES (122145, 'outstanding_252.241-7000[0]', 'S', 3, 1, 'Month', NULL, 5, 1, 'Month');
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size, fill_in_group_number, fill_in_placeholder, fill_in_default_data, fill_in_display_rows, fill_in_for_table, fill_in_heading) VALUES (122145, 'outstanding_252.241-7000[1]', 'N', 4, 1, 'Year', NULL, 5, 1, 'Year');
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size, fill_in_group_number, fill_in_placeholder, fill_in_default_data, fill_in_display_rows, fill_in_for_table, fill_in_heading) VALUES (122145, 'outstanding_252.241-7000[2]', '$', 12, 1, 'Amount credited', NULL, 5, 1, 'Amount credited');
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size, fill_in_group_number, fill_in_placeholder, fill_in_default_data, fill_in_display_rows, fill_in_for_table, fill_in_heading) VALUES (122145, 'outstanding_252.241-7000[3]', '$', 12, 1, 'Remaining amount of outstanding credits due the Government', NULL, 5, 1, 'Remaining amount');
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size, fill_in_group_number, fill_in_placeholder, fill_in_default_data, fill_in_display_rows, fill_in_for_table, fill_in_heading) VALUES (122145, 'termination_252.241-7000[0]', 'S', 3, 2, 'Month', NULL, 5, 1, 'Month');
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size, fill_in_group_number, fill_in_placeholder, fill_in_default_data, fill_in_display_rows, fill_in_for_table, fill_in_heading) VALUES (122145, 'termination_252.241-7000[1]', 'N', 4, 2, 'Year', NULL, 5, 1, 'Year');
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size, fill_in_group_number, fill_in_placeholder, fill_in_default_data, fill_in_display_rows, fill_in_for_table, fill_in_heading) VALUES (122145, 'termination_252.241-7000[2]', '$', 12, 2, 'Monthly facility cost recovered', NULL, 5, 1, 'Cost recovered');
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size, fill_in_group_number, fill_in_placeholder, fill_in_default_data, fill_in_display_rows, fill_in_for_table, fill_in_heading) VALUES (122145, 'termination_252.241-7000[3]', '$', 12, 2, 'Remaining amount of facility cost to be recovered', NULL, 5, 1, 'Remaining amount');
DELETE FROM Document_Fill_Ins WHERE Clause_Fill_In_Id
IN (SELECT Clause_Fill_In_Id FROM Clause_Fill_Ins WHERE clause_id = 122145 AND fill_in_code='memo_252.241-7000[0]');
DELETE FROM Clause_Fill_Ins WHERE clause_id = 122145 AND fill_in_code='memo_252.241-7000[0]';
DELETE FROM Document_Fill_Ins WHERE Clause_Fill_In_Id
IN (SELECT Clause_Fill_In_Id FROM Clause_Fill_Ins WHERE clause_id = 122145 AND fill_in_code='memo_252.241-7000[1]');
DELETE FROM Clause_Fill_Ins WHERE clause_id = 122145 AND fill_in_code='memo_252.241-7000[1]';
DELETE FROM Document_Fill_Ins WHERE Clause_Fill_In_Id
IN (SELECT Clause_Fill_In_Id FROM Clause_Fill_Ins WHERE clause_id = 122145 AND fill_in_code='memo_252.241-7000[2]');
DELETE FROM Clause_Fill_Ins WHERE clause_id = 122145 AND fill_in_code='memo_252.241-7000[2]';

/*
*** End of SQL Scripts at Mon Aug 03 16:38:23 EDT 2015 ***
*/
