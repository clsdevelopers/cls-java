-- [DPAP APPROVAL FOR USE OF 252.229-7015 ]
DELETE FROM Document_Answers WHERE Question_Id = 38481;

DELETE FROM Question_Choice_Prescriptions
WHERE Question_Choce_Id in
(SELECT Question_Choce_Id FROM Question_Choices WHERE question_id = 38481);

DELETE FROM Question_Choices WHERE question_id = 38481;

DELETE FROM Question_Conditions WHERE question_id = 38481;

DELETE FROM Questions WHERE question_id = 38481;
