/*
Clause Parser Run at Fri Mar 11 14:51:12 EST 2016
(Without Correction Information)
*/

-- Clause Version Change Scripts Start
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (16, 15, 'Q', '[DOCUMENT TYPE]', 'Changed', 'Prescriptions');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (16, 15, 'Q', '[NEGOTIATION (FAR PART 15)]', 'Changed', 'Type');

INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (16, 15, 'C', '252.225-7982 (DEVIATION 2016-00005)', 'Added', 'Added');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (16, 15, 'C', '252.225-7983 (DEVIATION 2016-00005)', 'Added', 'Added');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (16, 15, 'C', '252.225-7984 (DEVIATION 2016-00005)', 'Added', 'Added');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (16, 15, 'C', '52.203-3', 'Changed', 'Condition');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (16, 15, 'C', '52.215-20', 'Changed', 'Rule');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (16, 15, 'C', '52.215-21', 'Changed', 'Condition');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (16, 15, 'C', '52.215-23', 'Changed', 'Rule');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (16, 15, 'C', '52.247-5', 'Changed', 'Condition');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (16, 15, 'C', '52.215-20 Alternate II', 'Changed', 'Condition, Rule');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (16, 15, 'C', '52.215-23 Alternate I', 'Changed', 'Additional Instruction');
-- Clause Version Change Scripts End


UPDATE Clause_Versions SET Is_Active = IF (Clause_Version_Id = 16, 1, 0);

/*
*** End of SQL Scripts at Fri Mar 11 14:51:31 EST 2016 ***
*/
