/*
Clause Parser Run at Sat Dec 05 02:20:51 EST 2015
(Without Correction Information)
*/
/* ===============================================
 * Clause
   =============================================== */
UPDATE Clauses
SET is_basic_clause = 1
WHERE clause_version_id = 14 AND clause_name = '252.216-7010';

UPDATE Clauses
SET is_basic_clause = 1
WHERE clause_version_id = 14 AND clause_name = '252.217-7000';

UPDATE Clauses
SET is_basic_clause = 1
WHERE clause_version_id = 14 AND clause_name = '252.223-7006';

UPDATE Clauses
SET is_basic_clause = 1
WHERE clause_version_id = 14 AND clause_name = '252.225-7000';

UPDATE Clauses
SET is_basic_clause = 1
WHERE clause_version_id = 14 AND clause_name = '252.225-7001';

UPDATE Clauses
SET is_basic_clause = 1
WHERE clause_version_id = 14 AND clause_name = '252.225-7020';

UPDATE Clauses
SET is_basic_clause = 1
WHERE clause_version_id = 14 AND clause_name = '252.225-7021';

UPDATE Clauses
SET is_basic_clause = 1
WHERE clause_version_id = 14 AND clause_name = '252.225-7035';

UPDATE Clauses
SET is_basic_clause = 1
WHERE clause_version_id = 14 AND clause_name = '252.225-7036';

UPDATE Clauses
SET is_basic_clause = 1
WHERE clause_version_id = 14 AND clause_name = '252.225-7044';

UPDATE Clauses
SET is_basic_clause = 1
WHERE clause_version_id = 14 AND clause_name = '252.225-7045';

UPDATE Clauses
SET is_basic_clause = 1
WHERE clause_version_id = 14 AND clause_name = '252.229-7001';

UPDATE Clauses
SET is_basic_clause = 1
WHERE clause_version_id = 14 AND clause_name = '252.234-7003';

UPDATE Clauses
SET is_basic_clause = 1
WHERE clause_version_id = 14 AND clause_name = '252.234-7004';

UPDATE Clauses
SET is_basic_clause = 1
WHERE clause_version_id = 14 AND clause_name = '252.235-7003';

UPDATE Clauses
SET is_basic_clause = 1
WHERE clause_version_id = 14 AND clause_name = '252.237-7002';

UPDATE Clauses
SET is_basic_clause = 1
WHERE clause_version_id = 14 AND clause_name = '252.237-7016';

UPDATE Clauses
SET is_basic_clause = 1
WHERE clause_version_id = 14 AND clause_name = '252.244-7001';

UPDATE Clauses
SET is_basic_clause = 1
WHERE clause_version_id = 14 AND clause_name = '252.246-7001';

UPDATE Clauses
SET is_basic_clause = 1
WHERE clause_version_id = 14 AND clause_name = '252.247-7008';

UPDATE Clauses
SET is_basic_clause = 1
WHERE clause_version_id = 14 AND clause_name = '252.247-7023';

/*
*** End of SQL Scripts at Sat Dec 05 02:20:55 EST 2015 ***
*/
