/*
Clause Parser Run at Fri Nov 06 10:57:19 EST 2015
(Without Correction Information)
*/
/* ===============================================
 * Prescriptions
   =============================================== */
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '10.003') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '11.107(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '11.107(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '11.204(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '11.204(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '11.204(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '11.204(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '11.304') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '11.404(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '11.404(a)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '11.404(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '11.503') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '11.503(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '11.503(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '11.503(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '11.604(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '11.604(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '11.703(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '11.703(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '11.703(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '12.301(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '12.301(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '12.301(b)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '12.301(b)(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '12.301(b)(4)(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '12.301(b)(4)(ii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '12.301(c)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '13.302-5(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '13.302-5(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '13.302-5(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '13.302-5(d)(3)(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '13.404') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(c)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(c)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(c)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(f)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(g)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(g)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(h)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(j)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(l)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(m)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(o)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(o)(2)(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(o)(2)(ii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(p)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(p)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(q)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(r)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(s)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(t)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(v)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(w)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(x)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-7(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-7(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-7(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-7(c)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-7(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.209(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.209(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.209(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.209(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.209(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.209(b)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.209(b)(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.209(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.209(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.209(f)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.209(h)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.408(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.408(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.408(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.408(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.408(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.408(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.408(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.408(f)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.408(f)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.408(g)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.408(h)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.408(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.408(j)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.408(k)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.408(l)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.408(m)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.408(n)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.408(n)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.408(n)(2)(iii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.105') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.203-4(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.203-4(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.203-4(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.205-4') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.206-4') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.307(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.307(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.307(a)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.307(a)(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.307(a)(5)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.307(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.307(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.307(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.307(e)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.307(e)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.307(f)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.307(f)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.307(g)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.406') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.406(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.406(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.506') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.506(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.506(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.506(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.506(d)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.506(d)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.506(d)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.506(d)(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.506(d)(5)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.506(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.506(f)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.506(g)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.601(f)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.601(f)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.601(f)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.603-4(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.603-4(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.603-4(b)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.603-4(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '17.109') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '17.109(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '17.208(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '17.208(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '17.208(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '17.208(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '17.208(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '17.208(f)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '17.208(g)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.1309') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.1309(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.1309(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.1309(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.1309(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.1407') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.1506(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.1506(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.309(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.309(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.309(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.309(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.508((c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.508(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.508(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.508(d)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.508(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.508(f)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.708(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.708(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.708(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.708(b)(1)(iii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.708(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.708(c)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.811-3') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.811-3(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.811-3(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.811-3(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.811-3(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.811-3(d)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.811-3(d)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.811-3(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2.201') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '201.602-70') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2012-O0004') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2012-O0007') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2013-00017') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2013-00019') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2013-O0006') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2013-O0010') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2013-O0014') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2013-O0014)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2013-O0016') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2013-O0017') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2013-O0019') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2014-00014') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2014-O0004') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2014-O0005') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2014-O0010') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2014-O0016') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2014-O0018') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2014-O0020') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2014-OO0009') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2015-00003') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2015-00009') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2015-00016') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2015-00017') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2015-O0007') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2015-O0010') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2015-O0012') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2015-O0013') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2015-O0016') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2015-OO005') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2016-00001') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2016-00002') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2016-0001') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2016-0002') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '203.1004(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '203.1004(b)(2)(ii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '203.171-4') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '203.171-4(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '203.570-3') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '203.97') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '204.1105') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '204.1202') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '204.404-70(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '204.404-70(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '204.404-70(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '204.470-3') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '204.7104-1(b)(3)(iv)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '204.7109(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '204.7109(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '204.7304(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '204.7304(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '204.7304(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '204.7304[c]') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '204.7403(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '204.7403(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '204.7403(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '205.47') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '206.302-3-70') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '208.7305(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '209.104-70') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '209.270-5') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '209.409') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '209.470-4(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '209.470-4(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '209.570-4(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '209.570-4(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '209.571-8(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '209.571-8(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '211.002-70') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '211.204(c)(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '211.204(c)(ii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '211.272') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '211.273-4') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '211.274-6(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '211.274-6(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '211.274-6(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '211.275-3') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '211.503') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '211.503(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '212.7103') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '213.106-2-70') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '215.370-3(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '215.370-3(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '215.371-6') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '215.408') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '215.408(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '215.408(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '215.408(3)(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '215.408(3)(ii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '215.408(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '215.408(4)(ii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '215.408(5)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '216.203-4-70(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '216.203-4-70(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '216.203-4-70(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '216.203-4-70(c)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '216.203-4-70(c)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '216.307(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '216.406') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '216.406(e)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '216.506(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '216.506(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '216.506(d)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '216.506(d)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '216.601(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '217.208-70(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '217.208-70(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '217.208-70(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '217.208-70(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '217.7005') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '217.7104(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '217.7303') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '217.7406(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '217.7702') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '219.309(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '219.708(b)(1)(A)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '219.708(b)(1)(B)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '219.811-3') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '219.811-3(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '219.811-3(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1006') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1006(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1006(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1006(c)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1006(c)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1006(e)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1006(e)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1006(e)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1006(e)(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1006(f)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.103-5(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.103-5(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1103') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1207') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1310(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1310(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1310(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1310(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1408(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1408(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1505(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1505(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1605') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1705(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1705(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1705(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1803') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1906') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.202') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.305') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.407(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.407(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.407(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.407(f)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.407(g)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.407(h)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.505(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.505(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.505(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.505(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.505-(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.505-(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.610') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.810(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.810(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.810(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.810(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.810(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.810(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.810(f)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.810(g)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '222.1771') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '222.7004') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '222.7102') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '222.7201(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '222.7201(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '222.7201(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '222.7302') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '222.7405') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '223.303') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '223.370-5') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '223.570-2') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '223.7106') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '223.7106(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '223.7106(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '223.7203') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '223.7306') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225-7011-3') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225-7306') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225-7799-4') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225-7799-4(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225-7799-4(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225-7799-4(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225-7799-4(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225-7799-4(f)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225-7799-4(h)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225..1101(10)(i)(C)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(1)(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(1)(ii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(10)(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(10)(i)(A)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(10)(i)(B)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(10)(i)(C)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(10)(i)(D)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(10)(i)(E)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(10)(i)(F)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(2)(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(2)(ii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(2)(iii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(5)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(5)(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(5)(ii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(6)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(6)(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(6)(ii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(7)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(8)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(9)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(9)(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(9)(ii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(9)(iii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(9)(iv)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(9)(v)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(9)(vi)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1103(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1103(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1103(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1103(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.302-6') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.371-5') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.371-5(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.372-2') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7002-2(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7002-3(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7002-3(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7003-5(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7003-5(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7003-5(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7006-4(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7006-4(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7007-3') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7009-5') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7011-3') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7012-3') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7017-4(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7017-4(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7102-4') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7204(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7204(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7307(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7307(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7402-5(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7503(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7503(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7503(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7503(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7503(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7503(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7503(b)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7503(b)(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7605') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7703-4(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7703-4(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7703-4(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7703-4(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.771-5') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.772-5') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7798-6(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7798-6(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7798-6(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7901-4') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7902-5(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7902-5(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '226.104') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.303(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.303(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7009- 4(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7009-1') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7009-2') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7009-2(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7009-2(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7009-3(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7009-3(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7009-4(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7009-4(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7009-4(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7009-4(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7010') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7012') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7102-4(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7102-4(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7103-17') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7103-3(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7103-6(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7103-6(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7103-6(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7103-6(e)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7103-6(e)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7103-6(e)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7103-8(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7103-8(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7104(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7104(e)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7104(e)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7104(e)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7104(e)(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7104(e)(5)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7104(f)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7104(f)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7105-2(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7105-3') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7106(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7107-1(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7107-1(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7107-1(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7107-3') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7203-3(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7203-6(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7203-6(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7203-6(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7203-6(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7203-6(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7203-6(f)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7205(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '228.170') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '228.370(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '228.370(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '228.370(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '228.370(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '228.370(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '229.170-4') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '229.402-1') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '229.402-70(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '229.402-70(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '229.402-70(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '229.402-70(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '229.402-70(c)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '229.402-70(c)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '229.402-70(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '229.402-70(e)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '229.402-70(e)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '229.402-70(f)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '229.402-70(g)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '229.402-70(h)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '229.402-70(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '229.402-70(j)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '23.1005') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '23.1005(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '23.1005(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '23.1105') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '23.206') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '23.303') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '23.303(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '23.406(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '23.406(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '23.406(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '23.406(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '23.406(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '23.505') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '23.602') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '23.705(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '23.705(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '23.705(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '23.705(c)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '23.705(d)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '23.705(d)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '23.804(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '23.804(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '23.903') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '231.100-70') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '232.1005-70(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '232.1005-70(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '232.1110') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '232.412-70(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '232.412-70(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '232.412-70(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '232.502-4-70(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '232.502-4-70(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '232.7004(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '232.7004(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '232.705-70') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '232.7102') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '232.7202') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '232.806(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '232.908') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '233.215-70') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '234.203(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '234.203(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '234.7101(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '234.7101(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '234.7101(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '234.7101(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '234.7101(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '235.070-3') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '235.072') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '235.072(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '235.072(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '235.072(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '235.072(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '235.072(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '235.072(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '235.072©') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '236.570(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '236.570(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '236.570(b)(2)(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '236.570(b)(2)(ii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '236.570(b)(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '236.570(b)(5)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '236.570(b)(6)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '236.570(c)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '236.570(c)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '236.570(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '236.609-70') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '236.609-70(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '237-7003(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '237.171-4') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '237.173-5') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '237.270(d)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '237.270(d)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '237.7003(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '237.7003(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '237.7003(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '237.7003(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '237.7101(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '237.7101(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '237.7101(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '237.7101(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '237.7101(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '237.7101(e)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '237.7101(e)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '237.7101(e)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '237.7101(f)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '237.7101(g)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '237.7402') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '237.7603(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '239.7103(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '239.7103(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '239.7306(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '239.7306(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '239.7411(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '239.7411(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '239.7411(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '239.7411(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '239.7411©') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '239.7603(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '239.7603(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '24.104(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '24.104(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '241.501-70(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '241.501-70(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '242.7001') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '242.7204') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '242.7503') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '243.205-70') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '243.205-71') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '244.305-71') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '244.305-71(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '244.305-71(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '244.403') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '245.107(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '245.107(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '245.107(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '245.107(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '245.107(5)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '246.270-4') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '246.370') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '246.371(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '246.710(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '246.710(1)(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '246.710(1)(ii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '246.710(1)(iii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '246.710(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '246.710(3)(i)(A)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '246.710(3)(i)(B)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '246.710(3)(i)(B)(iii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '246.870-3') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.207') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.207(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.270-4') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.270-4(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.270-4(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.270-4(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.270-4(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.270-4(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.270-4(f)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.270-4(g)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.271-3') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.271-3(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.271-3(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.271-3(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.271-3(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.271-3(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.271-3(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.271-3(g)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.271-3(h)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.271-3(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.271-3(j)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.271-3(k)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.271-3(l)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.271-3(m)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.271-3(n)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.305-70') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.574(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.574(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.574(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.574(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.574(b)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.574(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.574(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.574(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.574(f)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '249.501-70') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '249.7003(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.110') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1101(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1101(1)(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1101(1)(ii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1101(6)(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1101(6)(ii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1101(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1101(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1101(b)(1)(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1101(b)(1)(ii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1101(b)(1)(iii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1101(b)(1)(iv)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1101(b)(2)(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1101(b)(2)(ii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1101(b)(2)(iii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1101(b)(2)(iv)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1101(c)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1101(c)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1101(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1101(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1101(f)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1102(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1102(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1102(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1102(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1102(c)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1102(d)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1102(d)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1102(d)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1102(e)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1103') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1103(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1103(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1103(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1103(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.301-4') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.302-6') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '251.107') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '251.205') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '26.104') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '26.206(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '26.206(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '26.206(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '26.304') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '26.404') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.201-2') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.201-2(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.201-2(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.201-2(a)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.201-2(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.201-2(c)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.201-2(c)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.201-2(c)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.201-2(d)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.201-2(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.202-5(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.202-5(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.202-5(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.202-5(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.203-2') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.303(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.303(b)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.303(b)(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.303(b)(5)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.303(b)(6)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.303(b)(7)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.303(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.303(e)(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.303(e)(5)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.409(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.409(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.409(b)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.409(b)(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.409(b)(5)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.409(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.409(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.409(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.409(f)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.409(g)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.409(h)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.409(j)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.409(k)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.409(l)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '28.101-2') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '28.103-4') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '28.106-4(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '28.106-4(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '28.1102-3(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '28.1102-3(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '28.203-6') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '28.204-4') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '28.309(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '28.309(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '28.310') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '28.311-1') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '28.312') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '28.313(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '28.313(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '29.401-1') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '29.401-2') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '29.401-3') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '29.401-3(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '29.401-3(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '29.401-4(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '29.402-1(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '29.402-1(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '29.402-2(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '29.402-2(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '3.1004(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '3.1004(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '3.103-1') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '3.104-9(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '3.104-9(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '3.1106') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '3.202') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '3.301-1') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '3.404') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '3.502-3') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '3.503-2') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '3.808(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '3.808(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '3.907-7') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '3.908-9') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '30.201-3') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '30.201-3(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '30.201-3(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '30.201-4(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '30.201-4(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '30.201-4(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '30.201-4(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '30.201-4(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '30.202') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.009-2') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.1005') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.1005(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.1005(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.111(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.111(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.111(a)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.111(a)(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.111(a)(5)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.111(a)(6)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.111(a)(7)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.111(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.111(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.111(c)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.111(c)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.1110(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.1110(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.1110(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.1110(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.1110(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.1110(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.1110(g)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.205(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.206') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.206(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.206(g)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.206(g) ***') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.412(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.412(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.412(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.412(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.412(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.412(f)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.502-3(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.502-3(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.502-3(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.502-4(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.502-4(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.502-4(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.502-4(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.611(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.611(a) &(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.611(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.706-1(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.706-1(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.706-2(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.706-2(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.706-3') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.806(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.806(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.806(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.908(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.908(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.908(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.908(c)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '33.106(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '33.106(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '33.215(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '33.215(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '34.104') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '34.203(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '34.203(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '34.203(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.501(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.502') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.503') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.504') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.505') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.506') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.507') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.508') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.509') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.510') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.511') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.512') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.513') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.514') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.515') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.516') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.517') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.518') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.519') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.520') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.521') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.522') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.523') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.609-1(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.609-2(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.609-3') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.609-4') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '37.110(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '37.110(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '37.110(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '37.113-2(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '37.113-2(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '37.115-3') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '37.116-2') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '37.304(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '37.304(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '37.304(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '37.403') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '39.106') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '4.103') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '4.1105(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '4.1105(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '4.1202(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '4.1202(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '4.1303') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '4.1403(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '4.1705(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '4.1705(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '4.1804(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '4.1804(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '4.1804(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '4.303') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '4.404(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '4.607(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '4.607(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '4.905') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '41.501(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '41.501(c)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '41.501(c)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '41.501(c)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '41.501(c)(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '41.501(c)(5)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '41.501(d)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '41.501(d)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '41.501(d)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '41.501(d)(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '41.501(d)(5)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '41.501(d)(6)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '41.501(d)(7)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '42.1107(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '42.1305(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '42.1305(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '42.1305(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '42.1305(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '42.703-2(f)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '42.709-6') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '42.802') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '42.903') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '43.107') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '43.205(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '43.205(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '43.205(a)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '43.205(a)(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '43.205(a)(5)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '43.205(a)(6)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '43.205(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '43.205(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '43.205(b)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '43.205(b)(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '43.205(b)(6)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '43.205(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '43.205(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '43.205(f)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '44.204(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '44.204(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '44.204(a)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '44.204(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '44.204(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '44.403') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '45.107(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '45.107(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '45.107(a)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '45.107(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '45.107(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.301') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.302') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.303') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.304') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.305') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.306') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.307(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.308') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.309') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.311') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.312') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.313') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.314') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.315') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.316') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.710(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.710(a)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.710(a)(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.710(a)(5)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.710(a)(6)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.710(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.710(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.710(b)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.710(b)(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.710(c)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.710(c)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.710(c)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.710(c)(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.710(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.710(e)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.710(e)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.805') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.805(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.805(a)(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.103-2') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.104-4') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-1(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-1(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-1(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-1(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-1(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-1(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-3(d)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-3(e)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-4(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-4(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-4(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-5( c )') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-5(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-5(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-5(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-5(f)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-6(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-6(c)(5)(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-6(c)(5)(ii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-6(c)(6)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-7(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-7(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-7(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-8(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-8(a)(2)(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-8(a)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-8(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-9(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.208-2') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.303- 14(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.303-1(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.303-10(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.303-11(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.303-12(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.303-13(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.303-14(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.303-15(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.303-16(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.303-17(f)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.303-2(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.303-3(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.303-4(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.303-5(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.303-6(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.303-7(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.303-8(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.303-9(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.304-7(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.305-12(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.305-13(a)(3)(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.305-14(b)(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.305-15(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.305-16(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.305-16(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.305-16(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.305-16(d)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.305-17') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.305-2(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.305-3(b)(4)(ii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.305-3(f)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.305-4(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.305-5(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.305-5(c)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.305-6(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.305-6(e)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.305-6(e)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.305-6(e)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.305-6(f)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.305-9(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.405') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.507(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.507(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.507(a)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '48.201') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '48.201(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '48.201(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '48.201(e)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '48.201(f)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '48.202') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '49.502(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '49.502(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '49.502(b)(1)(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '49.502(b)(1)(ii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '49.502(b)(1)(iii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '49.502(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '49.502(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '49.502(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '49.503(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '49.503(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '49.503(a)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '49.503(a)(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '49.503(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '49.504(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '49.504(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '49.504(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '49.504(c)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '49.504(c)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '49.504(c)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '49.505(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '49.505(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '50.104-4') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '50.206(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '50.206(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '50.206(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '50.206(b)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '50.206(c)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '50.206(c)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '50.206(c)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '50.206(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '50.403-3') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '51.107') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '51.205') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '52.107(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '52.107(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '52.107(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '52.107(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '52.107(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '52.107(f)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '53.111') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '7.203') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '7.305(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '7.305(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '7.305(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '7.404') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '8.005') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '8.1104(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '8.1104(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '8.1104(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '8.1104(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '8.505') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '9.104-7(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '9.104-7(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '9.104-7(c)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '9.108-5(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '9.108-5(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '9.206-2') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '9.308-1(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '9.308-1(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '9.308-1(a)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '9.308-1(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '9.308-1(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '9.308-1(b)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '9.308-2(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '9.308-2(a)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '9.308-2(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '9.308-2(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '9.308-2(b)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '9.409') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, 'PGI 204.7108(d)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, 'PGI 204.7108(d)(10)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, 'PGI 204.7108(d)(11)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, 'PGI 204.7108(d)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, 'PGI 204.7108(d)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, 'PGI 204.7108(d)(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, 'PGI 204.7108(d)(5)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, 'PGI 204.7108(d)(6)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, 'PGI 204.7108(d)(7)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, 'PGI 204.7108(d)(8)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, 'PGI 204.7108(d)(9)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, 'Preamble to clause 252.209-7991 as written in 2016-00002') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;

UPDATE Prescriptions
JOIN (
	SELECT
		prescription_id,
		concat('http://www.ecfr.gov/cgi-bin/text-idx?node=se48.', volume , '.', part, '_1', subpart, replace(suffix, '-', '_6')) uri
	FROM (
		SELECT
			prescription_id,
			prescription_name,
			subpart,
			part,
			case when suffix like '%(%' then left(suffix,locate('(',suffix)-1) else suffix end suffix,
			case when part regexp '^[0-9]+$'
				then
					case
						when cast(part as UNSIGNED) >= '1' and cast(part as UNSIGNED) <= '51' then 1
						when cast(part as UNSIGNED) >= '52' and cast(part as UNSIGNED) <= '99' then 2
						when cast(part as UNSIGNED) >= '200' and cast(part as UNSIGNED) <= '299' then 3
					end
			end volume
		FROM (
			SELECT
				prescription_id,
				prescription_name,
				substring_index(substring_index(prescription_name,'(', 1),'.',1) part,
				substring_index(substring_index(substring_index(prescription_name,'(', 1),'.',-1),'-',1) subpart,
				case
					when locate('-',prescription_name) != 0
						then right(prescription_name, length(prescription_name) - locate('-',prescription_name)+1)
					else ''
				end suffix
			FROM Prescriptions
		) Prescriptions
	) Prescriptions
) sub ON (sub.prescription_id = Prescriptions.prescription_id)
SET Prescriptions.prescription_url = ifNull(sub.uri,'')
WHERE ifNull(prescription_url, '') = '';

/* ===============================================
 * Questions
   =============================================== */


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198389, prescription_id FROM Prescriptions WHERE prescription_name IN ('30.202'); -- [ISSUING OFFICE] 'DEPARTMENT OF DEFENSE'

UPDATE Question_Conditions
SET question_condition_sequence = 330 -- 320
WHERE question_condition_id = 12806; -- [PAYMENT OF FIXED CHARGES]

UPDATE Question_Conditions
SET question_condition_sequence = 320 -- 330
WHERE question_condition_id = 12807; -- [ORDER VALUE]

UPDATE Question_Conditions
SET question_condition_sequence = 470 -- 490
, default_baseline = 1
WHERE question_condition_id = 12814; -- [SECURITY REQUIREMENTS]

UPDATE Question_Conditions
SET default_baseline = 1
WHERE question_condition_id = 12815; -- [DOCUMENT SECURITY LEVEL]

UPDATE Question_Conditions
SET question_condition_sequence = 490 -- 470
WHERE question_condition_id = 12816; -- [PURPOSE OF PROCUREMENT]
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198516, prescription_id FROM Prescriptions WHERE prescription_name IN ('247.574(b)(3)'); -- [PURPOSE OF PROCUREMENT] 'FOR USE IN CONTINGENCY OPERATIONS'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198523, prescription_id FROM Prescriptions WHERE prescription_name IN ('247.574(b)(3)'); -- [PURPOSE OF PROCUREMENT] 'FOR USE IN HUMANITARIAN OR PEACEKEEPING OPERATIONS'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198528, prescription_id FROM Prescriptions WHERE prescription_name IN ('247.574(b)(1)','247.574(b)(2)','247.574(b)(3)'); -- [PURPOSE OF PROCUREMENT] 'FOR USE IN MILITARY OPERATIONS OR MILITARY EXERCISES DESIGNATED BY THE COMBATANT COMMANDER'

UPDATE Question_Conditions
SET default_baseline = 1
WHERE question_condition_id = 12819; -- [SAFETY ACT APPLICABILITY]

UPDATE Question_Conditions
SET default_baseline = 1
WHERE question_condition_id = 12821; -- [SAM EXEMPTION]

UPDATE Question_Conditions
SET default_baseline = 1
WHERE question_condition_id = 12829; -- [FORMS]


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198589, prescription_id FROM Prescriptions WHERE prescription_name IN ('247.574(b)(1)','247.574(b)(2)'); -- [SUPPLIES OR SERVICES] 'SUPPLIES'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198590, prescription_id FROM Prescriptions WHERE prescription_name IN ('25.110'); -- [SUPPLIES OR SERVICES] 'SERVICES'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198622, prescription_id FROM Prescriptions WHERE prescription_name IN ('247.574(b)(2)','247.574(b)(3)'); -- [COMMERCIAL ITEMS] 'THE SUPPLIES ARE COMMERCIAL ITEMS'

UPDATE Question_Conditions
SET question_condition_sequence = 1200 -- 1210
WHERE question_condition_id = 12855; -- [R&D TECHNOLOGIES]


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198728, prescription_id FROM Prescriptions WHERE prescription_name IN ('32.206(g) ***'); -- [COMMERCIAL SERVICES] 'YES'

UPDATE Question_Conditions
SET question_condition_sequence = 1210 -- 1200
WHERE question_condition_id = 12866; -- [OSD APPROVAL FOR DATA SYSTEM]

UPDATE Question_Conditions
SET question_condition_sequence = 1320 -- 1370
, default_baseline = 1
WHERE question_condition_id = 12872; -- [PERFORMANCE REQUIREMENTS 1]

UPDATE Question_Conditions
SET question_condition_sequence = 1340 -- 1390
, default_baseline = 1
WHERE question_condition_id = 12873; -- [PERFORMANCE REQUIREMENTS 2]

UPDATE Question_Conditions
SET question_condition_sequence = 1380 -- 1320
WHERE question_condition_id = 12874; -- [LABOR REQUIREMENTS 1]

UPDATE Question_Conditions
SET question_condition_sequence = 1390 -- 1330
WHERE question_condition_id = 12875; -- [LABOR REQUIREMENTS 2]

UPDATE Question_Conditions
SET question_condition_sequence = 1410 -- 1440
, default_baseline = 1
WHERE question_condition_id = 12876; -- [LABOR REQUIREMENTS 3]

UPDATE Question_Conditions
SET question_condition_sequence = 1430 -- 1340
WHERE question_condition_id = 12877; -- [CONTRACTOR MATERIAL]

UPDATE Question_Conditions
SET question_condition_sequence = 1460 -- 1350
WHERE question_condition_id = 12878; -- [SUBCONTRACTING]

UPDATE Question_Conditions
SET default_baseline = 1
WHERE question_condition_id = 12879; -- [ENVIRONMENT]

UPDATE Question_Conditions
SET default_baseline = 1
WHERE question_condition_id = 12880; -- [INTEREST ON PARTIAL PAYMENTS]

UPDATE Question_Conditions
SET default_baseline = 1
WHERE question_condition_id = 12881; -- [TERMINATION]

UPDATE Question_Conditions
SET question_condition_sequence = 1330 -- 1380
WHERE question_condition_id = 12882; -- [TECHNICAL CHANGES]

UPDATE Question_Conditions
SET question_condition_sequence = 1350 -- 1400
WHERE question_condition_id = 12883; -- [VALUE ENGINEERING CONDITIONS]

UPDATE Question_Conditions
SET question_condition_sequence = 1360 -- 1410
WHERE question_condition_id = 12884; -- [FIRST ARTICLE CONDITIONS]

UPDATE Question_Conditions
SET question_condition_sequence = 1370 -- 1420
WHERE question_condition_id = 12885; -- [DD FORM 1494]
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198833, prescription_id FROM Prescriptions WHERE prescription_name IN ('235.072(b)(2)'); -- [DD FORM 1494] 'YES'

UPDATE Question_Conditions
SET question_condition_sequence = 1400 -- 1430
WHERE question_condition_id = 12886; -- [LABOR ACTS]

UPDATE Question_Conditions
SET question_condition_sequence = 1420 -- 1450
WHERE question_condition_id = 12887; -- [DRUG FREE WORKFORCE REQUIRED]

UPDATE Question_Conditions
SET question_condition_sequence = 1440 -- 1460
WHERE question_condition_id = 12888; -- [GOVERNMENT FURNISHED PROPERTY]

UPDATE Question_Conditions
SET question_condition_sequence = 1450 -- 1470
WHERE question_condition_id = 12889; -- [GOVERNMENT SUPPLY STATEMENT]

UPDATE Question_Conditions
SET question_condition_sequence = 1470 -- 1360
WHERE question_condition_id = 12890; -- [SUBCONTRACTING PLAN CONDITIONS]

UPDATE Question_Conditions
SET question_condition_sequence = 1670 -- 1660
WHERE question_condition_id = 12901; -- [PLACE TYPE]

UPDATE Question_Conditions
SET question_condition_sequence = 1690 -- 1680
, default_baseline = 1
WHERE question_condition_id = 12902; -- [DELIVERY BASIS]

UPDATE Question_Conditions
SET question_condition_sequence = 1700 -- 1690
, default_baseline = 1
WHERE question_condition_id = 12903; -- [CONTINUED PERFORMANCE]

UPDATE Question_Conditions
SET question_condition_sequence = 1710 -- 1700
, default_baseline = 1
WHERE question_condition_id = 12904; -- [LIQUIDATED DAMAGES]

UPDATE Question_Conditions
SET question_condition_sequence = 1720 -- 1710
WHERE question_condition_id = 12905; -- [SEPARATE DELIVERABLES]

UPDATE Question_Conditions
SET question_condition_sequence = 1730 -- 1660
WHERE question_condition_id = 12906; -- [PACE OF WORK]
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198931, prescription_id FROM Prescriptions WHERE prescription_name IN ('211.503'); -- [PACE OF WORK] 'NO'

UPDATE Question_Conditions
SET question_condition_sequence = 1620 -- 1630
WHERE question_condition_id = 12908; -- [MILITARY TECHNICAL AGREEMENT]

UPDATE Question_Conditions
SET question_condition_sequence = 1660 -- 1650
, default_baseline = 1
WHERE question_condition_id = 12910; -- [OUTSIDE U.S.]

UPDATE Question_Conditions
SET question_condition_sequence = 1650 -- 1720
WHERE question_condition_id = 12911; -- [UNEMPLOYMENT RATE]

UPDATE Question_Conditions
SET question_condition_sequence = 1740 -- 1730
, default_baseline = 1
WHERE question_condition_id = 12912; -- [TRANSPORTATION REQUIREMENTS 1]

UPDATE Question_Conditions
SET question_condition_sequence = 1780 -- 1770
, default_baseline = 1
WHERE question_condition_id = 12913; -- [TRANSPORTATION REQUIREMENTS 2]

UPDATE Question_Conditions
SET question_condition_sequence = 1790 -- 1780
, default_baseline = 1
WHERE question_condition_id = 12914; -- [TRANSPORTATION REQUIREMENTS 3]

UPDATE Question_Conditions
SET question_condition_sequence = 1800 -- 1790
, default_baseline = 1
WHERE question_condition_id = 12915; -- [TRANSPORTATION REQUIREMENTS 4]

UPDATE Question_Conditions
SET question_condition_sequence = 1810 -- 1860
, default_baseline = 1
WHERE question_condition_id = 12916; -- [TRANSPORTATION REQUIREMENTS 5]
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198981, prescription_id FROM Prescriptions WHERE prescription_name IN ('247.574(b)(1)','247.574(b)(2)','247.574(b)(3)'); -- [TRANSPORTATION REQUIREMENTS 5] 'INTERNATIONAL OCEAN CARRIERS'

UPDATE Question_Conditions
SET question_condition_sequence = 1820 -- 1800
, default_baseline = 1
WHERE question_condition_id = 12917; -- [TRANSPORTATION REQUIREMENTS 6]

UPDATE Question_Conditions
SET question_condition_sequence = 1830 -- 1810
, default_baseline = 1
WHERE question_condition_id = 12918; -- [TRANSPORTATION REQUIREMENTS 7]

UPDATE Question_Conditions
SET question_condition_sequence = 1840 -- 1820
, default_baseline = 1
WHERE question_condition_id = 12919; -- [TRANSPORTATION REQUIREMENT 8]

UPDATE Question_Conditions
SET question_condition_sequence = 1850 -- 1830
, default_baseline = 1
WHERE question_condition_id = 12920; -- [TRANSPORTATION REQUIREMENTS 9]

UPDATE Question_Conditions
SET question_condition_sequence = 1860 -- 1840
, default_baseline = 1
WHERE question_condition_id = 12921; -- [TRANSPORTATION REQUIREMENTS 10]

UPDATE Question_Conditions
SET question_condition_sequence = 1870 -- 1850
, default_baseline = 1
WHERE question_condition_id = 12922; -- [TRANSPORTATION REQUIREMENTS 11]

UPDATE Question_Conditions
SET question_condition_sequence = 1890 -- 1880
, default_baseline = 1
WHERE question_condition_id = 12923; -- [SHIPPING STATEMENTS]

UPDATE Question_Conditions
SET question_condition_sequence = 1750 -- 1740
WHERE question_condition_id = 12924; -- [FOB ORIGIN DESIGNATIONS]

UPDATE Question_Conditions
SET question_condition_sequence = 1760 -- 1750
WHERE question_condition_id = 12925; -- [WITHIN CONSIGNEE'S PREMISES]

UPDATE Question_Conditions
SET question_condition_sequence = 1770 -- 1760
WHERE question_condition_id = 12926; -- [FOB DESIGNATION]

UPDATE Question_Conditions
SET question_condition_sequence = 1880 -- 1870
WHERE question_condition_id = 12927; -- [CARGO PREFERENCE ACT]

UPDATE Question_Conditions
SET question_condition_sequence = 1900 -- 1890
WHERE question_condition_id = 12928; -- [ACO RESPONSIBILITIES]

UPDATE Question_Conditions
SET question_condition_sequence = 1920 -- 1910
WHERE question_condition_id = 12929; -- [FUNDING FY]
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199054, prescription_id FROM Prescriptions WHERE prescription_name IN ('2016-00002'); -- [FUNDING FY] 'FY 2016 FUNDS'

UPDATE Question_Conditions
SET question_condition_sequence = 1970 -- 1960
WHERE question_condition_id = 12930; -- [ACRN ASSIGNMENT]

UPDATE Question_Conditions
SET question_condition_sequence = 2000 -- 1990
WHERE question_condition_id = 12931; -- [FULLY OR INCREMENTAL FUNDING]

UPDATE Question_Conditions
SET question_condition_sequence = 2010 -- 2000
WHERE question_condition_id = 12932; -- [FINANCING ARRANGEMENTS]

UPDATE Question_Conditions
SET question_condition_sequence = 2050 -- 2040
WHERE question_condition_id = 12933; -- [BILLING ARRANGEMENTS]

UPDATE Question_Conditions
SET question_condition_sequence = 2080 -- 2070
WHERE question_condition_id = 12934; -- [PAYMENT INSTRUCTIONS]

UPDATE Question_Conditions
SET question_condition_sequence = 2090 -- 2080
, default_baseline = 1
WHERE question_condition_id = 12935; -- [RAPID LIQUIDATION]

UPDATE Question_Conditions
SET question_condition_sequence = 2100 -- 2090
, default_baseline = 1
WHERE question_condition_id = 12936; -- [CONTRACT PRIOR TO FUNDS]

UPDATE Question_Conditions
SET question_condition_sequence = 1910 -- 1900
WHERE question_condition_id = 12937; -- [BASE, POST, CAMP OR STATION]

UPDATE Question_Conditions
SET question_condition_sequence = 1930 -- 1920
WHERE question_condition_id = 12938; -- [TYPE OF FUNDS]

UPDATE Question_Conditions
SET question_condition_sequence = 1940 -- 1930
WHERE question_condition_id = 12939; -- [LEGISLATIVE SOURCE OF FUNDS]
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199105, prescription_id FROM Prescriptions WHERE prescription_name IN ('2016-0002'); -- [LEGISLATIVE SOURCE OF FUNDS] 'FUNDS MADE AVAILABLE BY THE CONTINUING APPROPRIATIONS ACT, 2016 (Pub. L. 114-53) OR ANY OTHER 2016 APPROPRIATIONS ACT THAT EXTENDS TO FY 2016 FUNDS THE SAME PROHIBITIONS AS CONTAINED IN SECTIONS 744 AND 745 OF DIVISION E, TITLE VII, OF THE CONSOLIDATED AND FURTHER CONTINUING APPROPRIATONS ACT, 2015 (Pub.L. 113-235).'

UPDATE Question_Conditions
SET question_condition_sequence = 1950 -- 1940
WHERE question_condition_id = 12940; -- [ANNUAL FUNDS EXTENDED]

UPDATE Question_Conditions
SET question_condition_sequence = 1960 -- 1950
WHERE question_condition_id = 12941; -- [LIABILITY FOR SPECIAL COSTS]

UPDATE Question_Conditions
SET question_condition_sequence = 1980 -- 1970
WHERE question_condition_id = 12942; -- [ACRN CONTRACT WIDE]

UPDATE Question_Conditions
SET question_condition_sequence = 1990 -- 1980
WHERE question_condition_id = 12943; -- [ACRN BY LINE ITEM]

UPDATE Question_Conditions
SET question_condition_sequence = 2020 -- 2010
WHERE question_condition_id = 12944; -- [ADVANCED PAYMENT CONDITIONS]

UPDATE Question_Conditions
SET question_condition_sequence = 2030 -- 2020
WHERE question_condition_id = 12945; -- [PROGRESS PAYMENT CONDITIONS]

UPDATE Question_Conditions
SET question_condition_sequence = 2040 -- 2030
WHERE question_condition_id = 12946; -- [BASIS OF PERFORMANCE BASED PAYMENTS]

UPDATE Question_Conditions
SET question_condition_sequence = 2060 -- 2050
WHERE question_condition_id = 12947; -- [EFT EXCEPTIONS]

UPDATE Question_Conditions
SET question_condition_sequence = 2070 -- 2060
WHERE question_condition_id = 12948; -- [PURCHASE CARD]

UPDATE Question_Conditions
SET question_condition_sequence = 2110 -- 2100
, default_baseline = 1
WHERE question_condition_id = 12949; -- [INSURANCE CONDITIONS]

UPDATE Question_Conditions
SET question_condition_sequence = 2120 -- 2110
WHERE question_condition_id = 12950; -- [BID GUARANTEE OR BOND]

UPDATE Question_Conditions
SET question_condition_sequence = 2130 -- 2120
WHERE question_condition_id = 12951; -- [NO TIME TO PROCESS BUY AMERICAN DETERMINATION]

UPDATE Question_Conditions
SET question_condition_sequence = 2140 -- 2130
WHERE question_condition_id = 12952; -- [DISPUTES ACT]

UPDATE Question_Conditions
SET question_condition_sequence = 2150 -- 2140
WHERE question_condition_id = 12953; -- [WAIVERS]

UPDATE Question_Conditions
SET question_condition_sequence = 2160 -- 2150
WHERE question_condition_id = 12954; -- [EXEMPTIONS]

UPDATE Question_Conditions
SET question_condition_sequence = 2170 -- 2160
WHERE question_condition_id = 12955; -- [EXCEPTIONS]

UPDATE Question_Conditions
SET question_condition_sequence = 2180 -- 2170
WHERE question_condition_id = 12956; -- [PATENT COUNCIL RECOMMENDED]

UPDATE Question_Conditions
SET question_condition_sequence = 2190 -- 2180
WHERE question_condition_id = 12957; -- [PRICING REQUIREMENTS]

UPDATE Question_Conditions
SET question_condition_sequence = 2240 -- 2230
WHERE question_condition_id = 12958; -- [CAS]

UPDATE Question_Conditions
SET question_condition_sequence = 2250 -- 2240
WHERE question_condition_id = 12959; -- [REGULATION PRICING]

UPDATE Question_Conditions
SET question_condition_sequence = 2260 -- 2250
, default_baseline = 1
WHERE question_condition_id = 12960; -- [DIRECT COST]

UPDATE Question_Conditions
SET question_condition_sequence = 2270 -- 2260
, default_baseline = 1
WHERE question_condition_id = 12961; -- [INDIRECT COST]

UPDATE Question_Conditions
SET question_condition_sequence = 2280 -- 2270
WHERE question_condition_id = 12962; -- [PRICING LIMITATIONS]

UPDATE Question_Conditions
SET question_condition_sequence = 2290 -- 2280
WHERE question_condition_id = 12963; -- [TRANSPORTATION COST]

UPDATE Question_Conditions
SET question_condition_sequence = 2300 -- 2290
, default_baseline = 1
WHERE question_condition_id = 12964; -- [PROPOSAL REQUIREMENTS]

UPDATE Question_Conditions
SET question_condition_sequence = 2310 -- 2300
, default_baseline = 1
WHERE question_condition_id = 12965; -- [AWARD SCENARIOS]

UPDATE Question_Conditions
SET question_condition_sequence = 2200 -- 2190
WHERE question_condition_id = 12966; -- [EXCESS PASS-THROUGH CHARGES]

UPDATE Question_Conditions
SET question_condition_sequence = 2210 -- 2200
WHERE question_condition_id = 12967; -- [UCA]

UPDATE Question_Conditions
SET question_condition_sequence = 2220 -- 2210
WHERE question_condition_id = 12968; -- [INAPPROPRIATE CONTINGENCY]

UPDATE Question_Conditions
SET question_condition_sequence = 2230 -- 2220
WHERE question_condition_id = 12969; -- [CURRENCY]


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199343, prescription_id FROM Prescriptions WHERE prescription_name IN ('237.7003(a)(1)'); -- [COMPETITION TYPE] 'SEALED BIDDING (FAR PART 14)'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199462, prescription_id FROM Prescriptions WHERE prescription_name IN ('247.574(b)(3)'); -- [SERVICES] 'CONSTRUCTION'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199481, prescription_id FROM Prescriptions WHERE prescription_name IN ('239.7603(a)'); -- [COMPUTERS/INFORMATION TECHNOLOGY] 'INFORMATION TECHNOLOGY'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199557, prescription_id FROM Prescriptions WHERE prescription_name IN ('237.7003(a)(1)','237.7003(a)(2)'); -- [SUPPORT SERVICES] 'MORTUARY SERVICES'

UPDATE Question_Conditions
SET question_condition_sequence = 1630 -- 1620
WHERE question_condition_id = 13011; -- [UNITED STATES OUTLYING AREAS]

UPDATE Question_Conditions
SET question_condition_sequence = 1680 -- 1670
WHERE question_condition_id = 13012; -- [GOVERNMENT OWNED OR LEASED FACILITIES/INSTALLATIONS]
/*
<h2>252.204-7008</h2>
(B) Clause name not found: {252.204-7008 (DEVIATION 2016-00001)}
<pre>(B) 252.204-7008 (DEVIATION 2016-00001) IS: NOT INCLUDED</pre>
*//*
<h2>252.204-7012</h2>
(C) Clause name not found: {252.204-7012 (DEVIATION 2016-00001)}
<pre>(C) 252.204-7012 (DEVIATION 2016-00001) IS: NOT INCLUDED</pre>
*//*
<h2>52.204-1</h2>
No identifier
<pre>(If applicable, the rule for the prescription for using this clause will be in the component clause</pre><br />Empty Rule
*//*
<h2>52.208-9</h2>
(C) Not " IS:" syntax
<pre>(C) CONTRACTOR MATERIAL: SUBCONTRACTS FOR SUPPLIES OR SERVICES FOR GOVERNMENT USE ARE REQUIRED TO BE PROCURED FROM THOSE LISTED ON THE PROCUREMENT LIST MAINTAINED BY THE COMMITTEE FOR PURCHASE FROM PEOPLE WHO ARE BLIND OR SEVERELY DISABLE</pre><br>

*//*
<h2>52.226-6</h2>
(F) No " IS:" found
<pre>(F) FOR USE IN UNITED STATES (50 STATES PLUS DISTRICT OF COLUMBIA)</pre>
*/
-- ProduceClause.resolveIssues() Summary: Total(1225); Merged(15); Corrected(347); Error(4)
/* ===============================================
 * Clause
   =============================================== */


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158422, prescription_id FROM Prescriptions
WHERE prescription_name IN ('2016-00001'); -- 252.204-7008


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158718, prescription_id FROM Prescriptions
WHERE prescription_name IN ('239.7603(a)'); -- 252.239-7009


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158719, prescription_id FROM Prescriptions
WHERE prescription_name IN ('239.7603(b)'); -- 252.239-7010


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158839, prescription_id FROM Prescriptions
WHERE prescription_name IN ('211.503(b)'); -- 52.211-12

DELETE FROM Document_Fill_Ins WHERE Clause_Fill_In_Id
IN (SELECT Clause_Fill_In_Id FROM Clause_Fill_Ins WHERE clause_id = 158839 AND fill_in_code='paragraph_a_memo_52.211-12');
DELETE FROM Clause_Fill_Ins WHERE clause_id = 158839 AND fill_in_code='paragraph_a_memo_52.211-12';
DELETE FROM Document_Fill_Ins WHERE Clause_Fill_In_Id
IN (SELECT Clause_Fill_In_Id FROM Clause_Fill_Ins WHERE clause_id = 158839 AND fill_in_code='paragraph_a_text_52.211-12');
DELETE FROM Clause_Fill_Ins WHERE clause_id = 158839 AND fill_in_code='paragraph_a_text_52.211-12';

/*
*** End of SQL Scripts at Fri Nov 06 10:57:23 EST 2015 ***
*/
