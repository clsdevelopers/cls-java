UPDATE Question_Conditions
SET condition_to_question = '("COMPETITION" <> [PROCEDURES] OR "FULL AND OPEN COMPETITION (FAR PART 6)" <> [COMPETITION TYPE]) AND "COMMERCIAL PROCEDURES (FAR PART 12)" <> [PROCEDURES]'
WHERE question_id = (
	SELECT question_id FROM Questions WHERE clause_version_id = 7 AND Question_Code = '[FULL AND OPEN EXCEPTION]'
);
