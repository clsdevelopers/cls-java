/*
Clause Parser Run at Thu Jul 02 09:07:05 EDT 2015
(Without Correction Information)
*/

-- ProduceClause.resolveIssues() Summary: Total(1212); Merged(13); Corrected(336); Error(1)
/* ===============================================
 * Prescriptions
   =============================================== */
/* ===============================================
 * Clause
   =============================================== */

UPDATE Clauses
SET effective_date = '2004-01-01' -- NULL
WHERE clause_id = 92594; -- 52.215-1

UPDATE Clauses
SET effective_date = '2014-05-01' -- NULL
WHERE clause_id = 92799; -- 52.225-1

UPDATE Clauses
SET effective_date = '1984-04-01' -- NULL
WHERE clause_id = 93081; -- 52.246-15

UPDATE Clauses
SET effective_date = '2014-04-01' -- NULL
WHERE clause_id = 93326; -- 252.216-7010 Alternate I

UPDATE Clauses
SET effective_date = '2014-11-01' -- NULL
WHERE clause_id = 93328; -- 252.217-7000 Alternate I

UPDATE Clauses
SET effective_date = '2014-09-01' -- NULL
WHERE clause_id = 93370; -- 252.223-7006 Alternate I

UPDATE Clauses
SET effective_date = '2014-11-01' -- NULL
WHERE clause_id = 93374; -- 252.225-7000 Alternate I

UPDATE Clauses
SET effective_date = '2014-11-01' -- NULL
WHERE clause_id = 93376; -- 252.225-7001 Alternate I

UPDATE Clauses
SET effective_date = '2014-11-01' -- NULL
WHERE clause_id = 93394; -- 252.225-7020 Alternate I

UPDATE Clauses
SET effective_date = '2014-11-01' -- NULL
WHERE clause_id = 93396; -- 252.225-7021 Alternate II

UPDATE Clauses
SET effective_date = '2014-11-01' -- NULL
WHERE clause_id = 93409; -- 252.225-7035 Alternate I

UPDATE Clauses
SET effective_date = '2014-11-01' -- NULL
WHERE clause_id = 93410; -- 252.225-7035 Alternate II

UPDATE Clauses
SET effective_date = '2014-11-01' -- NULL
WHERE clause_id = 93411; -- 252.225-7035 Alternate III

UPDATE Clauses
SET effective_date = '2014-11-01' -- NULL
WHERE clause_id = 93412; -- 252.225-7035 Alternate IV

UPDATE Clauses
SET effective_date = '2014-11-01' -- NULL
WHERE clause_id = 93413; -- 252.225-7035 Alternate V

UPDATE Clauses
SET effective_date = '2014-11-01' -- NULL
WHERE clause_id = 93415; -- 252.225-7036 Alternate I

UPDATE Clauses
SET effective_date = '2014-11-01' -- NULL
WHERE clause_id = 93416; -- 252.225-7036 Alternate II

UPDATE Clauses
SET effective_date = '2014-11-01' -- NULL
WHERE clause_id = 93417; -- 252.225-7036 Alternate III

UPDATE Clauses
SET effective_date = '2014-11-01' -- NULL
WHERE clause_id = 93418; -- 252.225-7036 Alternate IV

UPDATE Clauses
SET effective_date = '2014-11-01' -- NULL
WHERE clause_id = 93419; -- 252.225-7036 Alternate V

UPDATE Clauses
SET effective_date = '2014-11-01' -- NULL
WHERE clause_id = 93428; -- 252.225-7044 Alternate I

UPDATE Clauses
SET effective_date = '2014-11-01' -- NULL
WHERE clause_id = 93430; -- 252.225-7045 Alternate I

UPDATE Clauses
SET effective_date = '2014-11-01' -- NULL
WHERE clause_id = 93431; -- 252.225-7045 Alternate II

UPDATE Clauses
SET effective_date = '2014-11-01' -- NULL
WHERE clause_id = 93432; -- 252.225-7045 Alternate III

UPDATE Clauses
SET effective_date = '2001-10-01' -- '1984-08-01'
WHERE clause_id = 93463; -- 252.227-7005 Alternate I

UPDATE Clauses
SET effective_date = '2014-02-01' -- '1995-06-01'
WHERE clause_id = 93472; -- 252.227-7013 Alternate I

UPDATE Clauses
SET effective_date = '2014-02-01' -- '2009-11-01'
WHERE clause_id = 93473; -- 252.227-7013 Alternate II

UPDATE Clauses
SET effective_date = '2014-02-01' -- '1995-06-01'
WHERE clause_id = 93475; -- 252.227-7014 Alternate I

UPDATE Clauses
SET effective_date = '2014-02-01' -- '2011-12-01'
WHERE clause_id = 93477; -- 252.227-7015 Alternate I

UPDATE Clauses
SET effective_date = '2014-02-01' -- '1995-06-01'
WHERE clause_id = 93481; -- 252.227-7018 Alternate I

UPDATE Clauses
SET effective_date = '2014-09-01' -- NULL
WHERE clause_id = 93506; -- 252.229-7001 Alternate I

UPDATE Clauses
SET effective_date = '2014-11-01' -- NULL
WHERE clause_id = 93541; -- 252.234-7003 Alternate I

UPDATE Clauses
SET effective_date = '2014-11-01' -- NULL
WHERE clause_id = 93543; -- 252.234-7004 Alternate I

UPDATE Clauses
SET effective_date = '2014-03-01' -- NULL
WHERE clause_id = 93548; -- 252.235-7003 Alternate I

UPDATE Clauses
SET effective_date = '2014-11-01' -- NULL
WHERE clause_id = 93569; -- 252.237-7002 Alternate I

UPDATE Clauses
SET effective_date = '2014-11-01' -- NULL
WHERE clause_id = 93584; -- 252.237-7016 Alternate I

UPDATE Clauses
SET effective_date = '2014-11-01' -- NULL
WHERE clause_id = 93585; -- 252.237-7016 Alternate II

UPDATE Clauses
SET effective_date = '2014-03-01' -- NULL
WHERE clause_id = 93626; -- 252.246-7001 Alternate I

UPDATE Clauses
SET effective_date = '2014-03-01' -- NULL
WHERE clause_id = 93627; -- 252.246-7001 Alternate II

UPDATE Clauses
SET effective_date = '2014-04-01' -- NULL
WHERE clause_id = 93643; -- 252.247-7008 Alternate I

UPDATE Clauses
SET effective_date = '2014-04-01' -- NULL
WHERE clause_id = 93658; -- 252.247-7023 Alternate I

UPDATE Clauses
SET effective_date = '2014-04-01' -- NULL
WHERE clause_id = 93659; -- 252.247-7023 Alternate II

UPDATE Clauses
SET effective_date = '1989-09-01' -- '1997-01-01'
WHERE clause_id = 92512; -- 52.209-3 Alternate I

UPDATE Clauses
SET effective_date = '2014-11-01' -- '2014-10-01'
WHERE clause_id = 92550; -- 52.212-3 Alternate I

UPDATE Clauses
SET effective_date = '2000-02-01' -- '2014-07-01'
WHERE clause_id = 92554; -- 52.212-5 Alternate I

UPDATE Clauses
SET effective_date = NULL -- '2009-03-01'
WHERE clause_id = 92579; -- 52.214-26 Alternate I

UPDATE Clauses
SET effective_date = NULL -- '2010-10-01'
WHERE clause_id = 92610; -- 52.215-20 Alternate I

UPDATE Clauses
SET effective_date = '1995-02-01' -- '1994-11-01'
WHERE clause_id = 93039; -- 52.241-9 Alternate I

UPDATE Clauses
SET effective_date = '1987-08-01' -- '1984-04-01'
WHERE clause_id = 93050; -- 52.243-1 Alternate I

UPDATE Clauses
SET effective_date = '1987-08-01' -- '1984-04-01'
WHERE clause_id = 93051; -- 52.243-1 Alternate II

UPDATE Clauses
SET effective_date = '1987-08-01' -- '1984-04-01'
WHERE clause_id = 93052; -- 52.243-1 Alternate III

UPDATE Clauses
SET effective_date = '1987-08-01' -- '1984-04-01'
WHERE clause_id = 93053; -- 52.243-1 Alternate IV

UPDATE Clauses
SET effective_date = '1987-08-01' -- '1984-04-01'
WHERE clause_id = 93054; -- 52.243-1 Alternate V

/*
*** End of SQL Scripts at Thu Jul 02 09:07:07 EDT 2015 ***
*/
