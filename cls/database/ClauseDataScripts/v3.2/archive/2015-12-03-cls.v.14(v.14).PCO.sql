/*
Clause Parser Run at Thu Dec 03 13:56:51 EST 2015
(Without Correction Information)
*/
/* ===============================================
 * Prescriptions
   =============================================== */
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '10.003') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '11.107(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '11.107(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '11.204(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '11.204(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '11.204(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '11.204(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '11.304') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '11.404(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '11.404(a)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '11.404(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '11.503') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '11.503(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '11.503(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '11.503(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '11.604(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '11.604(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '11.703(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '11.703(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '11.703(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '12.301(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '12.301(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '12.301(b)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '12.301(b)(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '12.301(b)(4)(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '12.301(b)(4)(ii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '12.301(c)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '13.302-5(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '13.302-5(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '13.302-5(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '13.302-5(d)(3)(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '13.404') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(c)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(c)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(c)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(f)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(g)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(g)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(h)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(j)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(l)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(m)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(o)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(o)(2)(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(o)(2)(ii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(p)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(p)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(q)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(r)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(s)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(t)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(v)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(w)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(x)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-7(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-7(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-7(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-7(c)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-7(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.209(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.209(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.209(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.209(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.209(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.209(b)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.209(b)(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.209(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.209(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.209(f)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.209(h)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.408(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.408(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.408(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.408(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.408(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.408(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.408(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.408(f)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.408(f)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.408(g)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.408(h)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.408(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.408(j)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.408(k)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.408(l)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.408(m)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.408(n)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.408(n)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.408(n)(2)(iii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.105') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.203-4(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.203-4(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.203-4(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.205-4') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.206-4') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.30(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.307(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.307(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.307(a)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.307(a)(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.307(a)(5)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.307(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.307(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.307(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.307(e)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.307(e)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.307(f)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.307(f)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.307(g)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.406') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.406(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.406(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.506') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.506(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.506(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.506(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.506(d)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.506(d)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.506(d)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.506(d)(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.506(d)(5)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.506(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.506(f)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.506(g)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.601(f)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.601(f)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.601(f)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.603-4(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.603-4(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.603-4(b)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.603-4(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '17.109') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '17.109(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '17.208(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '17.208(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '17.208(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '17.208(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '17.208(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '17.208(f)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '17.208(g)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.1309(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.1309(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.1309(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.1309(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.1407') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.1506(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.1506(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.309(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.309(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.309(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.309(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.508(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.508(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.508(d)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.508(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.508(f)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.708(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.708(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.708(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.708(b)(1)(iii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.708(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.708(c)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.811-3') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.811-3(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.811-3(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.811-3(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.811-3(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.811-3(d)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.811-3(d)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.811-3(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2.201') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '201.602-70') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2012-O0004') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2012-O0007') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2013-00017') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2013-00019') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2013-O0006') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2013-O0010') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2013-O0014') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2013-O0014)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2013-O0016') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2013-O0017') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2013-O0019') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2014-00014') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2014-O0004') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2014-O0005') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2014-O0009') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2014-O0010') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2014-O0016') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2014-O0018') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2014-O0020') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2014-OO0009') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2015-00003') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2015-00009') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2015-00016') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2015-00017') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2015-O0007') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2015-O0010') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2015-O0012') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2015-O0013') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2015-O0016') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2015-OO005') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2016-00001') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2016-00002') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2016-0001') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2016-0002') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2016-0003') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2016-O0003') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '203.1004(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '203.1004(b)(2)(ii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '203.171-4') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '203.171-4(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '203.570-3') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '203.97') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '204.1105') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '204.1202') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '204.404-70(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '204.404-70(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '204.404-70(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '204.470-3') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '204.7104-1(b)(3)(iv)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '204.7109(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '204.7109(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '204.7304(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '204.7304(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '204.7304(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '204.7304[c]') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '204.7403(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '204.7403(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '204.7403(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '205.47') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '206.302-3-70') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '208.7305(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '209.104-70') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '209.270-5') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '209.409') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '209.470-4(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '209.470-4(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '209.570-4(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '209.570-4(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '209.571-8(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '209.571-8(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '211.002-70') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '211.107') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '211.204(c)(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '211.204(c)(ii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '211.272') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '211.273-4') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '211.274-6(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '211.274-6(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '211.274-6(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '211.275-3') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '211.503') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '211.503(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '212.7103') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '213.106-2-70') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '215.370-3(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '215.370-3(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '215.371-6') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '215.408') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '215.408(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '215.408(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '215.408(3)(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '215.408(3)(ii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '215.408(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '215.408(4)(ii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '215.408(5)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '216.203-4-70(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '216.203-4-70(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '216.203-4-70(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '216.203-4-70(c)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '216.203-4-70(c)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '216.307(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '216.406') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '216.406(e)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '216.506(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '216.506(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '216.506(d)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '216.506(d)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '216.601(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '217.208-70(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '217.208-70(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '217.208-70(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '217.208-70(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '217.7005') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '217.7104(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '217.7303') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '217.7406(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '217.7702') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '219.309(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '219.708(b)(1)(A)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '219.708(b)(1)(B)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '219.811-3') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '219.811-3(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '219.811-3(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1006') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1006(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1006(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1006(c)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1006(c)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1006(e)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1006(e)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1006(e)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1006(e)(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1006(f)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.103-5(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.103-5(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1103') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1207') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1310(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1310(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1310(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1310(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1408(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1408(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1505(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1505(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1605') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1705(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1705(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1705(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1803') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1906') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.202') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.305') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.407(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.407(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.407(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.407(f)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.407(g)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.407(h)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.505(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.505(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.505(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.505(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.505-(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.505-(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.610') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.810(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.810(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.810(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.810(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.810(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.810(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.810(f)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.810(g)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '222.1771') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '222.7004') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '222.7102') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '222.7201(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '222.7201(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '222.7201(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '222.7302') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '222.7405') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '223.303') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '223.370-5') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '223.570-2') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '223.7106') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '223.7106(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '223.7106(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '223.7203') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '223.7306') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225-7011-3') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225-7799-4') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225-7799-4(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225-7799-4(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225-7799-4(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225-7799-4(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225-7799-4(f)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225..1101(10)(i)(C)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(1)(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(1)(ii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(10)(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(10)(i)(A)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(10)(i)(B)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(10)(i)(C)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(10)(i)(D)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(10)(i)(E)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(10)(i)(F)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(2)(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(2)(ii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(2)(iii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(5)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(5)(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(5)(ii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(6)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(6)(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(6)(ii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(7)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(8)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(9)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(9)(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(9)(ii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(9)(iii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(9)(iv)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(9)(v)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(9)(vi)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1103(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1103(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1103(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1103(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.302-6') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.371-5') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.371-5(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.372-2') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7002-2(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7002-3(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7002-3(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7003-5(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7003-5(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7003-5(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7006-4(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7006-4(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7007-3') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7009-5') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7011-3') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7012-3') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7017-4(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7017-4(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7102-4') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7204(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7204(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7307(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7307(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7402-5(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7503(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7503(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7503(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7503(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7503(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7503(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7503(b)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7503(b)(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7605') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7703-4(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7703-4(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7703-4(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7703-4(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.771-5') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.772-5') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7798-6(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7798-6(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7798-6(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7901-4') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7902-5(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7902-5(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '226.104') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.303(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.303(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7009-1') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7009-2') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7009-2(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7009-2(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7009-3(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7009-3(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7009-4(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7009-4(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7009-4(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7009-4(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7009-4(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7010') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7012') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7102-4(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7102-4(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7103-17') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7103-3(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7103-6(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7103-6(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7103-6(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7103-6(e)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7103-6(e)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7103-6(e)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7103-8(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7103-8(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7104(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7104(e)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7104(e)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7104(e)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7104(e)(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7104(e)(5)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7104(f)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7104(f)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7105-2(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7105-3') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7106(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7107-1(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7107-1(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7107-1(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7107-3') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7203-3(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7203-6(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7203-6(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7203-6(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7203-6(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7203-6(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7203-6(f)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7205(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '228.170') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '228.370(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '228.370(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '228.370(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '228.370(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '228.370(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '229.170-4') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '229.402-1') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '229.402-70(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '229.402-70(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '229.402-70(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '229.402-70(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '229.402-70(c)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '229.402-70(c)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '229.402-70(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '229.402-70(e)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '229.402-70(e)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '229.402-70(f)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '229.402-70(g)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '229.402-70(h)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '229.402-70(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '229.402-70(j)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '23.1005') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '23.1005(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '23.1005(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '23.1105') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '23.206') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '23.303') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '23.303(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '23.406(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '23.406(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '23.406(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '23.406(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '23.406(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '23.505') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '23.602') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '23.705(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '23.705(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '23.705(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '23.705(c)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '23.705(d)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '23.705(d)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '23.804(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '23.804(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '23.903') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '231.100-70') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '232.1005-70(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '232.1005-70(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '232.1110') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '232.412-70(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '232.412-70(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '232.412-70(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '232.502-4(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '232.502-4-70(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '232.7004(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '232.7004(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '232.705-70') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '232.7102') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '232.7202') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '232.806(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '232.908') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '233.215-70') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '234.203(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '234.203(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '234.7101(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '234.7101(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '234.7101(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '234.7101(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '234.7101(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '235.070-3') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '235.072') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '235.072(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '235.072(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '235.072(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '235.072(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '235.072(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '235.072(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '236.570(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '236.570(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '236.570(b)(2)(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '236.570(b)(2)(ii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '236.570(b)(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '236.570(b)(5)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '236.570(b)(6)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '236.570(c)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '236.570(c)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '236.570(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '236.609-70') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '236.609-70(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '237-7003(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '237.171-4') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '237.173-5') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '237.270(d)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '237.270(d)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '237.7003(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '237.7003(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '237.7003(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '237.7003(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '237.7101(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '237.7101(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '237.7101(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '237.7101(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '237.7101(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '237.7101(e)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '237.7101(e)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '237.7101(e)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '237.7101(f)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '237.7101(g)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '237.7402') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '237.7603(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '239.7103(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '239.7103(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '239.7306(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '239.7306(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '239.7411(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '239.7411(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '239.7411(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '239.7411(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '239.7603(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '239.7603(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '24.104(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '24.104(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '241.501-70(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '241.501-70(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '242.7001') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '242.7204') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '242.7503') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '243.205-70') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '243.205-71') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '244.305-71') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '244.305-71(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '244.305-71(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '244.403') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '245.107(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '245.107(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '245.107(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '245.107(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '245.107(5)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '246.270-4') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '246.370') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '246.371(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '246.710(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '246.710(1)(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '246.710(1)(ii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '246.710(1)(iii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '246.710(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '246.710(3)(i)(A)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '246.710(3)(i)(B)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '246.710(3)(i)(B)(iii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '246.870-3') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.207') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.207(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.270-4') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.270-4(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.270-4(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.270-4(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.270-4(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.270-4(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.270-4(f)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.270-4(g)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.271-3') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.271-3(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.271-3(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.271-3(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.271-3(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.271-3(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.271-3(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.271-3(g)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.271-3(h)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.271-3(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.271-3(j)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.271-3(k)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.271-3(l)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.271-3(m)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.271-3(n)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.305-70') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.574(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.574(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.574(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.574(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.574(b)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.574(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.574(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.574(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.574(f)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '249.501-70') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '249.7003(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.110') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1101(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1101(1)(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1101(1)(ii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1101(6)(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1101(6)(ii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1101(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1101(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1101(b)(1)(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1101(b)(1)(ii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1101(b)(1)(iii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1101(b)(1)(iv)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1101(b)(2)(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1101(b)(2)(ii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1101(b)(2)(iii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1101(b)(2)(iv)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1101(c)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1101(c)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1101(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1101(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1101(f)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1102(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1102(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1102(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1102(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1102(c)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1102(d)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1102(d)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1102(d)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1102(e)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1103') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1103(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1103(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1103(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1103(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.301-4') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.302-6') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '251.107') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '251.205') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '26.104') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '26.206(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '26.206(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '26.206(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '26.304') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '26.404') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.201-2') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.201-2(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.201-2(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.201-2(a)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.201-2(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.201-2(c)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.201-2(c)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.201-2(c)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.201-2(d)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.201-2(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.202-5(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.202-5(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.202-5(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.202-5(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.203-2') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.303(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.303(b)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.303(b)(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.303(b)(5)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.303(b)(6)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.303(b)(7)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.303(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.303(e)(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.303(e)(5)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.409(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.409(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.409(b)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.409(b)(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.409(b)(5)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.409(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.409(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.409(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.409(f)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.409(g)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.409(h)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.409(j)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.409(k)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.409(l)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '28.101-2') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '28.103-4') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '28.106-4(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '28.106-4(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '28.1102-3(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '28.1102-3(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '28.203-6') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '28.204-4') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '28.309(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '28.309(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '28.310') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '28.311-1') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '28.312') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '28.313(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '28.313(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '29.401-1') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '29.401-2') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '29.401-3') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '29.401-3(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '29.401-3(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '29.401-4(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '29.402-1(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '29.402-1(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '29.402-2(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '29.402-2(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '3.1004(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '3.1004(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '3.103-1') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '3.104-9(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '3.104-9(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '3.1106') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '3.202') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '3.301-1') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '3.404') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '3.502-3') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '3.503-2') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '3.808(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '3.808(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '3.907-7') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '3.908-9') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '30.201-3') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '30.201-3(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '30.201-3(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '30.201-4(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '30.201-4(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '30.201-4(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '30.201-4(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '30.201-4(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '30.202') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.009-2') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.1005') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.1005(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.1005(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.111(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.111(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.111(a)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.111(a)(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.111(a)(5)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.111(a)(6)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.111(a)(7)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.111(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.111(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.111(c)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.111(c)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.1110(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.1110(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.1110(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.1110(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.1110(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.1110(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.1110(g)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.205(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.206') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.206(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.206(g)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.412(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.412(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.412(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.412(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.412(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.412(f)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.502-3(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.502-3(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.502-3(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.502-4(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.502-4(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.502-4(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.502-4(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.611(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.611(a)&(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.611(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.706-1(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.706-1(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.706-2(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.706-2(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.706-3') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.806(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.806(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.806(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.908(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.908(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.908(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.908(c)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '33.106(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '33.106(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '33.215(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '33.215(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '34.104') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '34.203(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '34.203(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '34.203(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.501(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.502') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.503') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.504') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.505') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.506') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.507') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.508') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.509') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.510') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.511') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.512') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.513') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.514') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.515') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.516') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.517') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.518') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.519') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.520') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.521') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.522') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.523') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.609-1(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.609-2(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.609-3') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.609-4') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '37.110(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '37.110(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '37.110(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '37.113-2(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '37.113-2(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '37.115-3') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '37.116-2') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '37.304(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '37.304(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '37.304(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '37.403') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '39.106') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '4.103') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '4.1105(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '4.1105(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '4.1202(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '4.1202(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '4.1303') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '4.1403(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '4.1705(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '4.1705(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '4.1804(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '4.1804(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '4.1804(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '4.303') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '4.404(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '4.607(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '4.607(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '4.905') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '41.501(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '41.501(c)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '41.501(c)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '41.501(c)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '41.501(c)(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '41.501(c)(5)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '41.501(d)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '41.501(d)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '41.501(d)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '41.501(d)(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '41.501(d)(5)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '41.501(d)(6)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '41.501(d)(7)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '42.1107(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '42.1305(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '42.1305(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '42.1305(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '42.1305(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '42.703-2(f)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '42.709-6') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '42.802') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '42.903') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '43.107') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '43.205(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '43.205(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '43.205(a)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '43.205(a)(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '43.205(a)(5)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '43.205(a)(6)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '43.205(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '43.205(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '43.205(b)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '43.205(b)(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '43.205(b)(6)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '43.205(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '43.205(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '43.205(f)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '44.204(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '44.204(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '44.204(a)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '44.204(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '44.204(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '44.403') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '45.107(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '45.107(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '45.107(a)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '45.107(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '45.107(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.301') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.302') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.303') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.304') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.305') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.306') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.307(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.308') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.309') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.311') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.312') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.313') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.314') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.315') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.316') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.710(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.710(a)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.710(a)(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.710(a)(5)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.710(a)(6)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.710(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.710(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.710(b)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.710(b)(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.710(c)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.710(c)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.710(c)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.710(c)(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.710(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.710(e)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.710(e)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.805') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.805(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.805(a)(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.103-2') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.104-4') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-1(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-1(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-1(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-1(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-1(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-1(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-3(d)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-3(e)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-4(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-4(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-4(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-5(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-5(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-5(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-5(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-5(f)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-6(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-6(c)(5)(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-6(c)(5)(ii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-6(c)(6)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-7(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-7(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-7(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-8(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-8(a)(2)(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-8(a)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-8(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-9(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.208-2') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.303-1(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.303-10(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.303-11(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.303-12(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.303-13(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.303-14(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.303-15(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.303-16(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.303-17(f)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.303-2(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.303-3(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.303-4(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.303-5(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.303-6(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.303-7(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.303-8(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.303-9(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.304-7(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.305-12(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.305-13(a)(3)(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.305-14(b)(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.305-15(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.305-16(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.305-16(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.305-16(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.305-16(d)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.305-17') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.305-2(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.305-3(b)(4)(ii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.305-3(f)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.305-4(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.305-5(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.305-5(c)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.305-6(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.305-6(e)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.305-6(e)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.305-6(e)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.305-6(f)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.305-9(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.405') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.507(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.507(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.507(a)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '48.201') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '48.201(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '48.201(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '48.201(e)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '48.201(f)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '48.202') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '49.502(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '49.502(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '49.502(b)(1)(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '49.502(b)(1)(ii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '49.502(b)(1)(iii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '49.502(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '49.502(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '49.502(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '49.503(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '49.503(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '49.503(a)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '49.503(a)(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '49.503(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '49.504(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '49.504(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '49.504(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '49.504(c)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '49.504(c)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '49.504(c)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '49.505(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '49.505(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '50.104-4') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '50.206(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '50.206(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '50.206(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '50.206(b)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '50.206(c)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '50.206(c)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '50.206(c)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '50.206(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '50.403-3') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '51.107') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '51.205') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '52.107(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '52.107(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '52.107(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '52.107(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '52.107(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '52.107(f)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '53.111') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '7.203') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '7.305(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '7.305(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '7.305(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '7.404') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '8.005') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '8.1104(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '8.1104(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '8.1104(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '8.1104(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '8.505') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '9.104-7(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '9.104-7(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '9.104-7(c)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '9.108-5(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '9.108-5(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '9.206-2') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '9.308-1(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '9.308-1(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '9.308-1(a)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '9.308-1(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '9.308-1(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '9.308-1(b)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '9.308-2(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '9.308-2(a)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '9.308-2(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '9.308-2(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '9.308-2(b)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '9.409') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, 'NOTAPPLICABLE') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, 'PGI 204.7108(d)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, 'PGI 204.7108(d)(10)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, 'PGI 204.7108(d)(11)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, 'PGI 204.7108(d)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, 'PGI 204.7108(d)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, 'PGI 204.7108(d)(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, 'PGI 204.7108(d)(5)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, 'PGI 204.7108(d)(6)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, 'PGI 204.7108(d)(7)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, 'PGI 204.7108(d)(8)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, 'PGI 204.7108(d)(9)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
/* ===============================================
 * Questions
   =============================================== */

UPDATE Question_Conditions
SET original_condition_text = 'ASK ONLY AFTER USER SELECTS "YES" OR "NO" UNDER "SAFETY ACT APPLICABILITY" AND DO NOT ASK IF THE USER SELECTED "COMMERCIAL PROCEDURES (FAR PART 12)" UNDER "PROCEDURES"' -- 'ASK ONLY AFTER USER SELECTS "YES" OR "NO" UNDER "SAFETY ACT APPLICABILITY'' AND DO NOT ASK IF THE USER SELECTED "COMMERCIAL PROCEDURES (FAR PART 12)" UNDER "PROCEDURES"'
, condition_to_question = '("YES" = [SAFETY ACT APPLICABILITY] OR "NO" = [SAFETY ACT APPLICABILITY]) AND "COMMERCIAL PROCEDURES (FAR PART 12)" <> [PROCEDURES]' -- '("YES" = [SAFETY ACT APPLICABILITY''] OR "NO" = [SAFETY ACT APPLICABILITY'']) AND "COMMERCIAL PROCEDURES (FAR PART 12)" <> [PROCEDURES]'
WHERE question_id IN 
  (SELECT question_id FROM Questions WHERE question_code = '[SAFETY ACT STATEMENTS]' AND clause_version_id = 14); -- [SAFETY ACT STATEMENTS]
DELETE FROM Question_Choices WHERE question_id = 38061 AND choice_text = 'DHS HAS NOT ISSUED A SAFETY ACT DESIGNATION OR CERTIFICATION TO SUCCESSFUL OFFEROR BEFORE CONTRACT AWARD'; -- [SAFETY ACT STATEMENTS]
INSERT INTO Question_Choices( question_id, choice_text, prompt_after_selection)
SELECT question_id, 'DHS HAS NOT ISSUED A SAFETY ACT DESIGNATION OR CERTIFICATION TO SUCCESSFUL CONTRACTOR BEFORE CONTRACT AWARD', NULL FROM Questions WHERE clause_version_id = 14 AND question_code = '[SAFETY ACT STATEMENTS]'; -- [SAFETY ACT STATEMENTS]
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT Question_Choce_Id, prescription_id FROM Question_Choices, Prescriptions
  WHERE prescription_name IN ('50.206(d)') 
  AND choice_text = 'DHS HAS NOT ISSUED A SAFETY ACT DESIGNATION OR CERTIFICATION TO SUCCESSFUL CONTRACTOR BEFORE CONTRACT AWARD' AND  question_id IN (SELECT question_id FROM Questions WHERE clause_version_id = 14 AND question_code = '[SAFETY ACT STATEMENTS]'); -- [SAFETY ACT STATEMENTS] 'DHS HAS NOT ISSUED A SAFETY ACT DESIGNATION OR CERTIFICATION TO SUCCESSFUL CONTRACTOR BEFORE CONTRACT AWARD'
/*
<h2>52.204-7</h2>
(O) Value not found: "UNITED STATES OUTLYING AREA" in [PLACE OF PERFORMANCE] choices. Available values: <ul><li>"ASSOCIATED TERRITORIAL WATERS AND AIR SPACE"</li><li>"OUTER CONTINENTAL SHELF"</li><li>"DESIGNATED OPERATIONAL AREA"</li><li>"FOREIGN COUNTRY"</li><li>"UNITED STATES OUTLYING AREAS"</li><li>"UNITED STATES (50 STATES PLUS DISTRICT OF COLUMBIA)"</li><li>"UNITED STATES CENTRAL COMMAND (USCENTCOM) AREA OF RESPONSIBILITY"</li><li>"UNITED STATES EUROPEAN COMMAND (USEUCOM) THEATER OF OPERATION"</li><li>"UNITED STATES AFRICA COMMAND (USAFRICOM) THEATER OF OPERATION"</li><li>"UNITED STATES SOUTHERN COMMAND (USSOUTHCOM) THEATER OF OPERATION"</li><li>"UNITED STATES PACIFIC COMMAND (USPACOM) THEATER OF OPERATION"</li><li>"WAKE ISLAND"</li><li>"SPECIFIED BY THE GOVERNMENT"</li><li>"UNKNOWN"</li><li>"NONE OF THE ABOVE LISTED PLACES OF PERFORMANCE"</li></ul>
<pre>(O) PLACE OF PERFORMANCE IS: UNITED STATES OUTLYING AREA</pre><br />
(R) Value not found: "FOREIGN" in [BUSINESS TYPE] choices. Available values: <ul><li>"SMALL BUSINESS/ SOCIOECONOMIC SOURCE"</li><li>"GOVERNMENT SOURCE"</li><li>"FOREIGN SOURCE"</li><li>"EDUCATIONAL SOURCE"</li><li>"MISCELLANEOUS SOURCE"</li><li>"UNKNOWN - COMPETITION"</li><li>"NONE OF THE LISTED SOURCES"</li></ul>
<pre>(R) BUSINESS TYPE IS: FOREIGN</pre>
*//*
<h2>52.211-7</h2>
No identifier
<pre>(ADDITIONAL DIRECTIONS: IF THE REQUIRING ACTIVITY IS: DEPARTMENT OF DEFENSE DO NOT USE THIS CLAUSE</pre>
*//*
<h2>52.226-6</h2>
(F) No " IS:" found
<pre>(F) FOR USE IN UNITED STATES (50 STATES PLUS DISTRICT OF COLUMBIA)</pre>
*//*
<h2>52.247-5</h2>
(C) [TRANSPORTATION REQUIREMENTS] Question Name not found
<pre>(C) TRANSPORTATION REQUIREMENTS IS: CONTRACTOR RESPONSIBLE FOR FAMILIARIZATION WITH CONDITIONS UNDER WHICH AND WHERE THE SERVICES ARE TO BE PERFORMED</pre>
*//*
<h2>52.215-23 Alternate I</h2>
(Q) [EXCESSIVE PASS-THROUGH CHARGES]Found multiple questions for choice value of "NO":<br>
 [WAIVER OF NON MANUFACTURING RULE] [FEDERAL PRISON INDUSTRY] [UK OFFER] [DERIVATIVES ANTICIPATED] [ADEQUATE PRICE COMPETITION] [SIMPLIFIED ACQUISITION TEST PROGRAM] [SDB MECHANISMS ON A REGIONAL BASIS] [SOLICITATION TIME] [LEASE OPTION] [INDEFINITE QUANTITY TOTAL ORDER VALUE] [EXCEEDS GOVERNMENT CAPABILITY] [PAYMENT OF FIXED CHARGES] [ORDER VALUE] [TASK ORDER] [ORDER RATING] [PROVISIONING ORDER] [SAFETY ACT APPLICABILITY] [CAR] [SAM EXEMPTION] [GENERIC DUNS] [HUBZONE 50% WAIVER] [TRANSLATION TO ANOTHER LANGUAGE] [ACTIVE FEDERAL CONTRACTS] [CONTRACTOR PERFORM ON BEHALF OF FOREIGN COUNTRY] [OPTIONS] [EPEAT SILVER/GOLD] [BRAND NAME BASIS] [HAZARDOUS DATA SHEETS] [INCLUSION OF SERVICES] [RECOVERED MATERIAL ESTIMATES] [TREATY ELIGIBLE SUPPLY] [PROPERTY FOR EXCHANGE] [TRADE AGREEMENT APPLICABILITY] [PRE-CONSTRUCTION CONFERENCE] [SITE VISIT] [R&D TECHNOLOGIES] [WITHHOLDING ALLOWABLE COST] [COVERED SYSTEM] [SEPARATELY PRICED ITEMS] [PORT OF ENTRY REQUIREMENTS] [INTRASTATE MOVEMENT] [BILLS OF LADING] [SERVICE CONFIGURED FOR OCCUPANCY] [COMMERCIAL SERVICES] [SERVICE CONTRACT REPORTING] [OSD APPROVAL FOR DATA SYSTEM] [SWING FROM TARGET FEE] [SMALL BUSINESS AWARD FEE] [MAJOR DESIGN OR DEVELOPMENT] [PCO EPA DETERMINATION] [INTEREST ON PARTIAL PAYMENTS] [TERMINATION] [TECHNICAL CHANGES] [DD FORM 1494] [DRUG FREE WORKFORCE REQUIRED] [END ITEM MARKING] [PRESERVATION AND PACKING SPECIFICATIONS] [USE OF WARRANTY] [OUTSIDE U.S.] [CONTINUED PERFORMANCE] [LIQUIDATED DAMAGES] [SEPARATE DELIVERABLES] [PACE OF WORK] [MILITARY TECHNICAL AGREEMENT] [UNEMPLOYMENT RATE] [WITHIN CONSIGNEE'S PREMISES] [CARGO PREFERENCE ACT] [RAPID LIQUIDATION] [CONTRACT PRIOR TO FUNDS] [BASE, POST, CAMP OR STATION] [ANNUAL FUNDS EXTENDED] [LIABILITY FOR SPECIAL COSTS] [PURCHASE CARD] [NO TIME TO PROCESS BUY AMERICAN DETERMINATION] [DISPUTES ACT] [EXCESS PASS-THROUGH CHARGES] [UCA] [INAPPROPRIATE CONTINGENCY] [CURRENCY]
<pre>(Q) EXCESSIVE PASS-THROUGH CHARGES IS: NO</pre>
*/
-- ProduceClause.resolveIssues() Summary: Total(1231); Merged(17); Corrected(352); Error(6)
/* ===============================================
 * Clause
   =============================================== */

UPDATE Clauses
SET effective_date = '2013-01-01' -- NULL
WHERE clause_version_id = 14 AND clause_name = '252.209-7996 (DEVIATION 2013-00006)';

UPDATE Clauses
SET effective_date = '2013-01-01' -- NULL
WHERE clause_version_id = 14 AND clause_name = '252.209-7997 (DEVIATION 2013-00006)';

UPDATE Clauses
SET clause_rule = '(A OR B) AND C AND L AND ((D AND E) OR (F AND G)) OR ((!(D AND E) OR !(F AND G)) AND M) OR (H AND I AND ((D AND E) OR (F AND G)))' -- '(A OR B) AND (C AND L AND ((D AND E) OR (F AND G)) OR ((!(D AND E) OR !(F AND G)) AND M) OR (H AND I AND ((D AND E) OR (F AND G)))'
WHERE clause_version_id = 14 AND clause_name = '252.215-7004';

UPDATE Clauses
SET effective_date = '2014-04-01' -- NULL
WHERE clause_version_id = 14 AND clause_name = '252.216-7010';

UPDATE Clauses
SET clause_conditions = '(A) DOCUMENT TYPE IS: SOLICITATION (B) 252.225-7999 (DEVIATION 2014-00014) IS: INCLUDED' -- '(A) DOCUMENT TYPE IS: SOLICITATION (B) 252.225-7999 IS: INCLUDED'
WHERE clause_version_id = 14 AND clause_name = '252.225-7998 (DEVIATION 2014-00014)';



UPDATE Clause_Fill_Ins SET fill_in_default_data = '<p>The Offeror shall complete only paragraph (b) of this provision if the Offeror has completed the annual representations and certification electronically via the System for Award Management (SAM) Web site accessed through <i>http://www.acquisition.gov.</i> If the Offeror has not completed the annual representations and certifications electronically, the Offeror shall complete only paragraphs (c) through (p) of this provision.</p><p>(a) <i>Definitions.</i> As used in this provision&mdash;</p><p><i>Economically disadvantaged women-owned small business (EDWOSB) concern</i> means a small business concern that is at least 51 percent directly and unconditionally owned by, and the management and daily business operations of which are controlled by, one or more women who are citizens of the United States and who are economically disadvantaged in accordance with 13 CFR part 127. It automatically qualifies as a women-owned small business eligible under the WOSB Program.</p><p><i>Forced or indentured child labor</i> means all work or service&mdash; </p><p>(1) Exacted from any person under the age of 18 under the menace of any penalty for its nonperformance and for which the worker does not offer himself voluntarily; or </p><p>(2) Performed by any person under the age of 18 pursuant to a contract the enforcement of which can be accomplished by process or penalties.</p><p><i>Highest-level owner</i> means the entity that owns or controls an immediate owner of the offeror, or that owns or controls one or more entities that control an immediate owner of the offeror. No entity owns or exercises control of the highest level owner.</p><p><i>Immediate owner</i> means an entity, other than the offeror, that has direct control of the offeror. Indicators of control include, but are not limited to, one or more of the following: Ownership or interlocking management, identity of interests among family members, shared facilities and equipment, and the common use of employees.</p><p><i>Inverted domestic corporation</i> means a foreign incorporated entity that meets the definition of an inverted domestic corporation under 6 U.S.C. 395(b), applied in accordance with the rules and definitions of 6 U.S.C. 395(c).</p><p><i>Manufactured end product</i> means any end product in Federal Supply Classes (FSC) 1000-9999, except&mdash;</p><p>(1) FSC 5510, Lumber and Related Basic Wood Materials;</p><p>(2) Federal Supply Group (FSG) 87, Agricultural Supplies;</p><p>(3) FSG 88, Live Animals;</p><p>(4) FSG 89, Food and Related Consumables;</p><p>(5) FSC 9410, Crude Grades of Plant Materials;</p><p>(6) FSC 9430, Miscellaneous Crude Animal Products, Inedible;</p><p>(7) FSC 9440, Miscellaneous Crude Agricultural and Forestry Products;</p><p>(8) FSC 9610, Ores;</p><p>(9) FSC 9620, Minerals, Natural and Synthetic; and</p><p>(10) FSC 9630, Additive Metal Materials.</p><p><i>Manufactured end product</i> means any end product in product and service codes (PSCs) 1000-9999, except&mdash;</p><p>(1) PSC 5510, Lumber and Related Basic Wood Materials;</p><p>(2) Product or Service Group (PSG) 87, Agricultural Supplies;</p><p>(3) PSG 88, Live Animals;</p><p>(4) PSG 89, Subsistence;</p><p>(5) PSC 9410, Crude Grades of Plant Materials;</p><p>(6) PSC 9430, Miscellaneous Crude Animal Products, Inedible;</p><p>(7) PSC 9440, Miscellaneous Crude Agricultural and Forestry Products;</p><p>(8) PSC 9610, Ores;</p><p>(9) PSC 9620, Minerals, Natural and Synthetic; and</p><p>(10) PSC 9630, Additive Metal Materials.</p><p><i>Place of manufacture</i> means the place where an end product is assembled out of components, or otherwise made or processed from raw materials into the finished product that is to be provided to the Government. If a product is disassembled and reassembled, the place of reassembly is not the place of manufacture.</p><p><i>Restricted business operations</i> means business operations in Sudan that include power production activities, mineral extraction activities, oil-related activities, or the production of military equipment, as those terms are defined in the Sudan Accountability and Divestment Act of 2007 (Pub. L. 110-174). Restricted business operations do not include business operations that the person (as that term is defined in Section 2 of the Sudan Accountability and Divestment Act of 2007) conducting the business can demonstrate&mdash;</p><p>(1) Are conducted under contract directly and exclusively with the regional government of southern Sudan;</p><p>(2) Are conducted pursuant to specific authorization from the Office of Foreign Assets Control in the Department of the Treasury, or are expressly exempted under Federal law from the requirement to be conducted under such authorization;</p><p>(3) Consist of providing goods or services to marginalized populations of Sudan;</p><p>(4) Consist of providing goods or services to an internationally recognized peacekeeping force or humanitarian organization;</p><p>(5) Consist of providing goods or services that are used only to promote health or education; or</p><p>(6) Have been voluntarily suspended.</p><p><i>Sensitive technology</i>&mdash;</p><p>(1) Means hardware, software, telecommunications equipment, or any other technology that is to be used specifically&mdash;</p><p>(i) To restrict the free flow of unbiased information in Iran; or</p><p>(ii) To disrupt, monitor, or otherwise restrict speech of the people of Iran; and</p><p>(2) Does not include information or informational materials the export of which the President does not have the authority to regulate or prohibit pursuant to section 203(b)(3) of the International Emergency Economic Powers Act (50 U.S.C. 1702(b)(3)).</p><p><i>Service-disabled veteran-owned small business concern</i>&mdash; </p><p>(1) Means a small business concern&mdash; </p><p>(i) Not less than 51 percent of which is owned by one or more service&mdash;disabled veterans or, in the case of any publicly owned business, not less than 51 percent of the stock of which is owned by one or more service-disabled veterans; and </p><p>(ii) The management and daily business operations of which are controlled by one or more service-disabled veterans or, in the case of a service-disabled veteran with permanent and severe disability, the spouse or permanent caregiver of such veteran. </p><p>(2) <i>Service-disabled veteran</i> means a veteran, as defined in 38 U.S.C. 101(2), with a disability that is service-connected, as defined in 38 U.S.C. 101(16).</p><p><i>Small business concern</i> means a concern, including its affiliates, that is independently owned and operated, not dominant in the field of operation in which it is bidding on Government contracts, and qualified as a small business under the criteria in 13 CFR Part 121 and size standards in this solicitation.</p><p><i>Small disadvantaged business concern,</i> consistent with 13 CFR 124.1002, means a small business concern under the size standard applicable to the acquisition, that&mdash;</p><p>(1) Is at least 51 percent unconditionally and directly owned (as defined at 13 CFR 124.105) by&mdash;</p><p>(i) One or more socially disadvantaged (as defined at 13 CFR 124.103) and economically disadvantaged (as defined at 13 CFR 124.104) individuals who are citizens of the United States; and</p><p>(ii) Each individual claiming economic disadvantage has a net worth not exceeding $750,000 after taking into account the applicable exclusions set forth at 13 CFR 124.104(c)(2); and</p><p>(2) The management and daily business operations of which are controlled (as defined at 13.CFR 124.106) by individuals, who meet the criteria in paragraphs (1)(i) and (ii) of this definition.</p><p><i>Subsidiary</i> means an entity in which more than 50 percent of the entity is owned&mdash;</p><p>(1) Directly by a parent corporation; or</p><p>(2) Through another subsidiary of a parent corporation.</p><p><i>Veteran-owned small business concern</i> means a small business concern&mdash; </p><p>(1) Not less than 51 percent of which is owned by one or more veterans (as defined at 38 U.S.C. 101(2)) or, in the case of any publicly owned business, not less than 51 percent of the stock of which is owned by one or more veterans; and</p><p>(2) The management and daily business operations of which are controlled by one or more veterans.</p><p><i>Women-owned business concern</i> means a concern which is at least 51 percent owned by one or more women; or in the case of any publicly owned business, at least 51 percent of its stock is owned by one or more women; and whose management and daily business operations are controlled by one or more women.</p><p><i>Women-owned small business concern</i> means a small business concern&mdash;</p><p>(1) That is at least 51 percent owned by one or more women; or, in the case of any publicly owned business, at least 51 percent of the stock of which is owned by one or more women; and</p><p>(2) Whose management and daily business operations are controlled by one or more women.</p><p><i>Women-owned small business (WOSB) concern eligible under the WOSB Program</i> (in accordance with 13 CFR part 127), means a small business concern that is at least 51 percent directly and unconditionally owned by, and the management and daily business operations of which are controlled by, one or more women who are citizens of the United States.</p><p>(b)(1) <i>Annual Representations and Certifications.</i> Any changes provided by the offeror in paragraph (b)(2) of this provision do not automatically change the representations and certifications posted on the SAM website.</p><p>(2) The offeror has completed the annual representations and certifications electronically via the SAM website accessed through <i>http://www.acquisition.gov.</i> After reviewing the SAM database information, the offeror verifies by submission of this offer that the representations and certifications currently posted electronically at FAR 52.212-3, Offeror Representations and Certifications&mdash;Commercial Items, have been entered or updated in the last 12 months, are current, accurate, complete, and applicable to this solicitation (including the business size standard applicable to the NAICS code referenced for this solicitation), as of the date of this offer and are incorporated in this offer by reference (see FAR 4.1201), except for paragraphs {{textbox_52.212-3[0]}}.</p><p>[<i>Offeror to identify the applicable paragraphs at (c) through (p) of this provision that the offeror has completed for the purposes of this solicitation only, if any.</i></p><p><i>These amended representation(s) and/or certification(s) are also incorporated in this offer and are current, accurate, and complete as of the date of this offer.</i></p><p><i>Any changes provided by the offeror are applicable to this solicitation only, and do not result in an update to the representations and certifications posted electronically on SAM.</i>]</p><p>(c) Offerors must complete the following representations when the resulting contract will be performed in the United States or its outlying areas. Check all that apply.</p><p>(1) <i>Small business concern.</i> The offeror represents as part of its offer that it ____ is, ____&nbsp;&nbsp;is not a small business concern.</p><p>(2) <i>Veteran-owned small business concern.</i> [<i>Complete only if the offeror represented itself as a small business concern in paragraph (c)(1) of this provision.</i>] The offeror represents as part of its offer that it ____ is, ____ is not a veteran-owned small business concern. </p><p>(3) <i>Service-disabled veteran-owned small business concern.</i> [<i>Complete only if the offeror represented itself as a veteran-owned small business concern in paragraph (c)(2) of this provision.</i>] The offeror represents as part of its offer that it ____ is, ____ is not a service-disabled veteran-owned small business concern.</p><p>(4) <i>Small disadvantaged business concern. [Complete only if the offeror represented itself as a small business concern in paragraph (c)(1) of this provision.]</i> The offeror represents that it ____ is, ____ is not a small disadvantaged business concern as defined in 13 CFR 124.1002.</p><p>(5) <i>Women-owned small business concern. [Complete only if the offeror represented itself as a small business concern in paragraph (c)(1) of this provision.]</i> The offeror represents that it ____ is, ____ is not a women-owned small business concern.</p><p>(6) WOSB concern eligible under the WOSB Program. [<i>Complete only if the offeror represented itself as a women-owned small business concern in paragraph (c)(5) of this provision.</i>] The offeror represents that&mdash;</p><p>(i) It ____ is, ____ is not a WOSB concern eligible under the WOSB Program, has provided all the required documents to the WOSB Repository, and no change in circumstances or adverse decisions have been issued that affects its eligibility; and</p><p>(ii) It ____ is, ____ is not a joint venture that complies with the requirements of 13 CFR part 127, and the representation in paragraph (c)(6)(i) of this provision is accurate for each WOSB concern eligible under the WOSB Program participating in the joint venture. [<i>The offeror shall enter the name or names of the WOSB concern eligible under the WOSB Program and other small businesses that are participating in the joint venture:</i> ________.] Each WOSB concern eligible under the WOSB Program participating in the joint venture shall submit a separate signed copy of the WOSB representation.</p><p>(7) Economically disadvantaged women-owned small business (EDWOSB) concern. [<i>Complete only if the offeror represented itself as a WOSB concern eligible under the WOSB Program in (c)(6) of this provision.</i>] The offeror represents that&mdash;</p><p>(i) It ____ is, ____ is not an EDWOSB concern, has provided all the required documents to the WOSB Repository, and no change in circumstances or adverse decisions have been issued that affects its eligibility; and</p><p>(ii) It ____ is, ____ is not a joint venture that complies with the requirements of 13 CFR part 127, and the representation in paragraph (c)(7)(i) of this provision is accurate for each EDWOSB concern participating in the joint venture. [<i>The offeror shall enter the name or names of the EDWOSB concern and other small businesses that are participating in the joint venture:</i> ________.] Each EDWOSB concern participating in the joint venture shall submit a separate signed copy of the EDWOSB representation.</p><p>Note to paragraphs (c)(8) and (9): Complete paragraphs (c)(8) and (9) only if this solicitation is expected to exceed the simplified acquisition threshold.</p><p>(8) <i>Women-owned business concern (other than small business concern). [Complete only if the offeror is a women-owned business concern and did not represent itself as a small business concern in paragraph (c)(1) of this provision.]</i> The offeror represents that it ____ is, a women-owned business concern.</p><p>(9) <i>Tie bid priority for labor surplus area concerns.</i> If this is an invitation for bid, small business offerors may identify the labor surplus areas in which costs to be incurred on account of manufacturing or production (by offeror or first-tier subcontractors) amount to more than 50 percent of the contract price:</p>&nbsp; _____<p>(10) <i>HUBZone small business concern.</i> [Complete only if the offeror represented itself as a small business concern in paragraph (c)(1) of this provision.] The offeror represents, as part of its offer, that&mdash;</p><p>(i) It ____ is, ____ is not a HUBZone small business concern listed, on the date of this representation, on the List of Qualified HUBZone Small Business Concerns maintained by the Small Business Administration, and no material changes in ownership and control, principal office, or HUBZone employee percentage have occurred since it was certified in accordance with 13 CFR Part 126; and</p><p>(ii) It ____ is, ____ is not a HUBZone joint venture that complies with the requirements of 13 CFR Part 126, and the representation in paragraph (c)(10)(i) of this provision is accurate for each HUBZone small business concern participating in the HUBZone joint venture. [<i>The offeror shall enter the names of each of the HUBZone small business concerns participating in the HUBZone joint venture: ____.</i>] Each HUBZone small business concern participating in the HUBZone joint venture shall submit a separate signed copy of the HUBZone representation.</p><p>(d) Representations required to implement provisions of Executive Order 11246&mdash;</p><p>(1) Previous contracts and compliance. The offeror represents that&mdash;</p><p>(i) It ____ has, ____ has not participated in a previous contract or subcontract subject to the Equal Opportunity clause of this solicitation; and</p><p>(ii) It ____ has, ____ has not filed all required compliance reports.</p><p>(2) Affirmative Action Compliance. The offeror represents that&mdash;</p><p>(i) It ____ has developed and has on file, ____&nbsp;&nbsp;&nbsp;has not developed and does not have on file, at each establishment, affirmative action programs required by rules and regulations of the Secretary of Labor (41 CFR parts 60-1 and 60-2), or</p><p>(ii) It ____&nbsp;&nbsp;has not previously had contracts subject to the written affirmative action programs requirement of the rules and regulations of the Secretary of Labor.</p><p>(e) <i>Certification Regarding Payments to Influence Federal Transactions (31 U.S.C. 1352).</i> (Applies only if the contract is expected to exceed $150,000.) By submission of its offer, the offeror certifies to the best of its knowledge and belief that no Federal appropriated funds have been paid or will be paid to any person for influencing or attempting to influence an officer or employee of any agency, a Member of Congress, an officer or employee of Congress or an employee of a Member of Congress on his or her behalf in connection with the award of any resultant contract. If any registrants under the Lobbying Disclosure Act of 1995 have made a lobbying contact on behalf of the offeror with respect to this contract, the offeror shall complete and submit, with its offer, OMB Standard Form LLL, Disclosure of Lobbying Activities, to provide the name of the registrants. The offeror need not report regularly employed officers or employees of the offeror to whom payments of reasonable compensation were made.</p><p>(f) <i>Buy American Certificate.</i> (Applies only if the clause at Federal Acquisition Regulation (FAR) 52.225-1, Buy American&mdash;Supplies, is included in this solicitation.)</p><p>(1) The offeror certifies that each end product, except those listed in paragraph (f)(2) of this provision, is a domestic end product and that for other than COTS items, the offeror has considered components of unknown origin to have been mined, produced, or manufactured outside the United States. The offeror shall list as foreign end products those end products manufactured in the United States that do not qualify as domestic end products, <i>i.e.</i>, an end product that is not a COTS item and does not meet the component test in paragraph (2) of the definition of &ldquo;domestic end product." The terms &ldquo;commercially available off-the-shelf (COTS) item," &ldquo;component," &ldquo;domestic end product," &ldquo;end product," &ldquo;foreign end product," and &ldquo;United States" are defined in the clause of this solicitation entitled &ldquo;Buy American&mdash;Supplies."</p><p>(2) Foreign End Products:</p>Line Item No.: _____Country of Origin: _____<p>(List as necessary)</p><p>(3) The Government will evaluate offers in accordance with the policies and procedures of FAR Part 25.</p><p>(g)(1) <i>Buy American&mdash;Free Trade Agreements&mdash;Israeli Trade Act Certificate.</i> (Applies only if the clause at FAR 52.225-3, Buy American&mdash;Free Trade Agreements&mdash;Israeli Trade Act, is included in this solicitation.)</p><p>(i) The offeror certifies that each end product, except those listed in paragraph (g)(1)(ii) or (g)(1)(iii) of this provision, is a domestic end product and that for other than COTS items, the offeror has considered components of unknown origin to have been mined, produced, or manufactured outside the United States. The terms &ldquo;Bahrainian, Moroccan, Omani, Panamanian, or Peruvian end product," &ldquo;commercially available off-the-shelf (COTS) item," &ldquo;component," &ldquo;domestic end product," &ldquo;end product," &ldquo;foreign end product," &ldquo;Free Trade Agreement country," &ldquo;Free Trade Agreement country end product," &ldquo;Israeli end product," and &ldquo;United States" are defined in the clause of this solicitation entitled &ldquo;Buy American&mdash;Free Trade Agreements&mdash;Israeli Trade Act."</p><p>(ii) The offeror certifies that the following supplies are Free Trade Agreement country end products (other than Bahrainian, Moroccan, Omani, Panamanian, or Peruvian end products) or Israeli end products as defined in the clause of this solicitation entitled &ldquo;Buy American&mdash;Free Trade Agreements&mdash;Israeli Trade Act"</p><p>Free Trade Agreement Country End Products (Other than Bahrainian, Moroccan, Omani, Panamanian, or Peruvian End Products) or Israeli End Products:</p><div><div><table border="1" cellpadding="1" cellspacing="1" frame="void" width="100%"><tbody><tr><th scope="col">Line Item No.</th><th scope="col">&nbsp;&nbsp;</th><th scope="col">Country of Origin</th></tr><tr><td align="left" scope="row">&nbsp;&nbsp;</td><td align="right"></td><td align="left"></td></tr><tr><td align="left" scope="row">&nbsp;&nbsp;</td><td align="right"></td><td align="left"></td></tr><tr><td align="left" scope="row">&nbsp;&nbsp;</td><td align="right"></td><td align="left"></td></tr><tr><td align="center" colspan="3" scope="row">[List as necessary]</td></tr></tbody></table></div></div><p>(iii) The offeror shall list those supplies that are foreign end products (other than those listed in paragraph (g)(1)(ii) of this provision) as defined in the clause of this solicitation entitled &ldquo;Buy American&mdash;Free Trade Agreements&mdash;Israeli Trade Act." The offeror shall list as other foreign end products those end products manufactured in the United States that do not qualify as domestic end products, <i>i.e.</i>, an end product that is not a COTS item and does not meet the component test in paragraph (2) of the definition of &ldquo;domestic end product."</p><h2>Other Foreign End Products</h2>Line Item No.: _____Country of Origin: _____<p>(List as necessary)</p><p>(iv) The Government will evaluate offers in accordance with the policies and procedures of FAR Part 25.</p><p>(2) <i>Buy American&mdash;Free Trade Agreements&mdash;Israeli Trade Act Certificate, Alternate I.</i> If <i>Alternate I</i> to the clause at FAR 52.225-3 is included in this solicitation, substitute the following paragraph (g)(1)(ii) for paragraph (g)(1)(ii) of the basic provision: </p><p>(g)(1)(ii) The offeror certifies that the following supplies are Canadian end products as defined in the clause of this solicitation entitled &ldquo;Buy American&mdash;Free Trade Agreements&mdash;Israeli Trade Act": </p><h3>Canadian End Products: </h3><h3>Line Item No. </h3>&nbsp; _____&nbsp; _____&nbsp; _____<p>$(<i>List as necessary</i>) </p><p>(3) <i>Buy American&mdash;Free Trade Agreements&mdash;Israeli Trade Act Certificate, Alternate II.</i> If <i>Alternate II</i> to the clause at FAR 52.225-3 is included in this solicitation, substitute the following paragraph (g)(1)(ii) for paragraph (g)(1)(ii) of the basic provision: </p><p>(g)(1)(ii) The offeror certifies that the following supplies are Canadian end products or Israeli end products as defined in the clause of this solicitation entitled &ldquo;Buy American&mdash;Free Trade Agreements&mdash;Israeli Trade Act": </p><h3>Canadian or Israeli End Products: </h3><p>Line Item No. </p>&nbsp; _____&nbsp; _____&nbsp; _____<p>Country of Origin </p>&nbsp; _____&nbsp; _____&nbsp; _____<p>$(<i>List as necessary</i>)</p><p>(g)(4) <i>Buy American&mdash;Free Trade Agreements&mdash;Israeli Trade Act Certificate, Alternate III.</i> If Alternate III to the clause at FAR 52.225-3 is included in this solicitation, substitute the following paragraph (g)(1)(ii) for paragraph (g)(1)(ii) of the basic provision:</p><p>(g)(1)(ii) The offeror certifies that the following supplies are Free Trade Agreement country end products (other than Bahrainian, Korean, Moroccan, Omani, Panamanian, or Peruvian end products) or Israeli end products as defined in the clause of this solicitation entitled &ldquo;Buy American&mdash;Free Trade Agreements&mdash;Israeli Trade Act":</p><p>Free Trade Agreement Country End Products (Other than Bahrainian, Korean, Moroccan, Omani, Panamanian, or Peruvian End Products) or Israeli End Products:</p><div><div><table border="1" cellpadding="1" cellspacing="1" frame="void" width="100%"><tbody><tr><th scope="col">Line Item No.</th><th scope="col">&nbsp;&nbsp;</th><th scope="col">Country of Origin</th></tr><tr><td align="left" scope="row">&nbsp;&nbsp;</td><td align="right"></td><td align="left"></td></tr><tr><td align="left" scope="row">&nbsp;&nbsp;</td><td align="right"></td><td align="left"></td></tr><tr><td align="left" scope="row">&nbsp;&nbsp;</td><td align="right"></td><td align="left"></td></tr><tr><td align="center" colspan="3" scope="row">[List as necessary]</td></tr></tbody></table></div></div><p>(5) <i>Trade Agreements Certificate.</i> (Applies only if the clause at FAR 52.225-5, Trade Agreements, is included in this solicitation.)</p><p>(i) The offeror certifies that each end product, except those listed in paragraph (g)(5)(ii) of this provision, is a U.S.-made or designated country end product, as defined in the clause of this solicitation entitled &ldquo;Trade Agreements".</p><p>(ii) The offeror shall list as other end products those end products that are not U.S.-made or designated country end products.</p><p>Other End Products:</p><div><div><table border="1" cellpadding="1" cellspacing="1" frame="void" width="100%"><tbody><tr><th scope="col">Line item No.</th><th scope="col">&nbsp;&nbsp;</th><th scope="col">Country of origin</th></tr><tr><td align="left" scope="row">&nbsp;&nbsp;</td><td align="right"></td><td align="left"></td></tr><tr><td align="left" scope="row">&nbsp;&nbsp;</td><td align="right"></td><td align="left"></td></tr><tr><td align="left" scope="row">&nbsp;&nbsp;</td><td align="right"></td><td align="left"></td></tr><tr><td align="center" colspan="3" scope="row">[List as necessary]</td></tr></tbody></table></div></div><p>(iii) The Government will evaluate offers in accordance with the policies and procedures of FAR Part 25. For line items covered by the WTO GPA, the Government will evaluate offers of U.S.-made or designated country end products without regard to the restrictions of the Buy American statute. The Government will consider for award only offers of U.S.-made or designated country end products unless the Contracting Officer determines that there are no offers for such products or that the offers for such products are insufficient to fulfill the requirements of the solicitation.</p><p>(h) <i>Certification Regarding Responsibility Matters (Executive Order 12689).</i> (Applies only if the contract value is expected to exceed the simplified acquisition threshold.) The offeror certifies, to the best of its knowledge and belief, that the offeror and/or any of its principals&mdash; </p><p>(1) ____ Are, ____ are not presently debarred, suspended, proposed for debarment, or declared ineligible for the award of contracts by any Federal agency;</p><p>(2) ____ Have, ____ have not, within a three-year period preceding this offer, been convicted of or had a civil judgment rendered against them for: Commission of fraud or a criminal offense in connection with obtaining, attempting to obtain, or performing a Federal, state or local government contract or subcontract; violation of Federal or state antitrust statutes relating to the submission of offers; or Commission of embezzlement, theft, forgery, bribery, falsification or destruction of records, making false statements, tax evasion, violating Federal criminal tax laws, or receiving stolen property,</p><p>(3) ____ Are, ____ are not presently indicted for, or otherwise criminally or civilly charged by a Government entity with, commission of any of these offenses enumerated in paragraph (h)(2) of this clause; and</p><p>(i) <i>Certification Regarding Knowledge of Child Labor for Listed End Products (Executive Order 13126). [The Contracting Officer must list in paragraph (i)(1) any end products being acquired under this solicitation that are included in the List of Products Requiring Contractor Certification as to Forced or Indentured Child Labor, unless excluded at 22.1503(b).</i>] </p><p>(1) <i>Listed end products.</i> </p><h3>Listed End Product</h3>&nbsp; {{textbox_52.212-3[1]}}&nbsp;  {{textbox_52.212-3[2]}}<p>Listed Countries of Origin </p>&nbsp; {{textbox_52.212-3[3]}}&nbsp; {{textbox_52.212-3[4]}}<p>(2) <i>Certification.</i> [<i>If the Contracting Officer has identified end products and countries of origin in paragraph (i)(1) of this provision, then the offeror must certify to either (i)(2)(i) or (i)(2)(ii) by checking the appropriate block.</i>]</p><p>____&nbsp;&nbsp;(i) The offeror will not supply any end product listed in paragraph (i)(1) of this provision that was mined, produced, or manufactured in the corresponding country as listed for that product.</p><p>____&nbsp;&nbsp;(ii) The offeror may supply an end product listed in paragraph (i)(1) of this provision that was mined, produced, or manufactured in the corresponding country as listed for that product. The offeror certifies that it has made a good faith effort to determine whether forced or indentured child labor was used to mine, produce, or manufacture any such end product furnished under this contract. On the basis of those efforts, the offeror certifies that it is not aware of any such use of child labor.</p><p>(4) Have,____&nbsp;&nbsp; have not, within a three-year period preceding this offer, been notified of any delinquent Federal taxes in an amount that exceeds $3,500 for which the liability remains unsatisfied.</p><p>(i) Taxes are considered delinquent if both of the following criteria apply:</p><p>(A) <i>The tax liability is finally determined.</i> The liability is finally determined if it has been assessed. A liability is not finally determined if there is a pending administrative or judicial challenge. In the case of a judicial challenge to the liability, the liability is not finally determined until all judicial appeal rights have been exhausted.</p><p>(B) <i>The taxpayer is delinquent in making payment.</i> A taxpayer is delinquent if the taxpayer has failed to pay the tax liability when full payment was due and required. A taxpayer is not delinquent in cases where enforced collection action is precluded.</p><p>(ii) <i>Examples.</i> (A) The taxpayer has received a statutory notice of deficiency, under I.R.C. §6212, which entitles the taxpayer to seek Tax Court review of a proposed tax deficiency. This is not a delinquent tax because it is not a final tax liability. Should the taxpayer seek Tax Court review, this will not be a final tax liability until the taxpayer has exercised all judicial appeal rights.</p><p>(B) The IRS has filed a notice of Federal tax lien with respect to an assessed tax liability, and the taxpayer has been issued a notice under I.R.C. §6320 entitling the taxpayer to request a hearing with the IRS Office of Appeals contesting the lien filing, and to further appeal to the Tax Court if the IRS determines to sustain the lien filing. In the course of the hearing, the taxpayer is entitled to contest the underlying tax liability because the taxpayer has had no prior opportunity to contest the liability. This is not a delinquent tax because it is not a final tax liability. Should the taxpayer seek tax court review, this will not be a final tax liability until the taxpayer has exercised all judicial appeal rights.</p><p>(C) The taxpayer has entered into an installment agreement pursuant to I.R.C. §6159. The taxpayer is making timely payments and is in full compliance with the agreement terms. The taxpayer is not delinquent because the taxpayer is not currently required to make full payment.</p><p>(D) The taxpayer has filed for bankruptcy protection. The taxpayer is not delinquent because enforced collection action is stayed under 11 U.S.C. 362 (the Bankruptcy Code).</p><p>(i) <i>Certification Regarding Knowledge of Child Labor for Listed End Products (Executive Order 13126). [The Contracting Officer must list in paragraph (i)(1) any end products being acquired under this solicitation that are included in the List of Products Requiring Contractor Certification as to Forced or Indentured Child Labor, unless excluded at 22.1503(b).</i>] </p><p>(1) <i>Listed end products.</i> </p><h3>Listed End Product</h3>&nbsp; {{textbox_52.212-3[5]}}&nbsp;  {{textbox_52.212-3[6]}}<p>Listed Countries of Origin </p>&nbsp; {{textbox_52.212-3[7]}}&nbsp; {{textbox_52.212-3[8]}}<p>(2) <i>Certification.</i> [<i>If the Contracting Officer has identified end products and countries of origin in paragraph (i)(1) of this provision, then the offeror must certify to either (i)(2)(i) or (i)(2)(ii) by checking the appropriate block.</i>]</p><p>____&nbsp;&nbsp;(i) The offeror will not supply any end product listed in paragraph (i)(1) of this provision that was mined, produced, or manufactured in the corresponding country as listed for that product.</p><p>____&nbsp;&nbsp;(ii) The offeror may supply an end product listed in paragraph (i)(1) of this provision that was mined, produced, or manufactured in the corresponding country as listed for that product. The offeror certifies that it has made a good faith effort to determine whether forced or indentured child labor was used to mine, produce, or manufacture any such end product furnished under this contract. On the basis of those efforts, the offeror certifies that it is not aware of any such use of child labor.</p><p>(j) <i>Place of manufacture.</i> (Does not apply unless the solicitation is predominantly for the acquisition of manufactured end products.) For statistical purposes only, the offeror shall indicate whether the place of manufacture of the end products it expects to provide in response to this solicitation is predominantly&mdash;</p><p>(1) ____ In the United States (Check this box if the total anticipated price of offered end products manufactured in the United States exceeds the total anticipated price of offered end products manufactured outside the United States); or</p><p>(2) ____ Outside the United States.</p><p>(k) <i>Certificates regarding exemptions from the application of the Service Contract Labor Standards.</i> (Certification by the offeror as to its compliance with respect to the contract also constitutes its certification as to compliance by its subcontractor if it subcontracts out the exempt services.) [<i>The contracting officer is to check a box to indicate if paragraph {{checkbox_52.212-3[0]}} (k)(1) or {{checkbox_52.212-3[1]}} (k)(2) applies.</i>]</p><p>(1)____&nbsp;&nbsp; Maintenance, calibration, or repair of certain equipment as described in FAR 22.1003-4(c)(1). The offeror ____&nbsp;&nbsp;does ____&nbsp;&nbsp; does not certify that&mdash;</p><p>(i) The items of equipment to be serviced under this contract are used regularly for other than Governmental purposes and are sold or traded by the offeror (or subcontractor in the case of an exempt subcontract) in substantial quantities to the general public in the course of normal business operations;</p><p>(ii) The services will be furnished at prices which are, or are based on, established catalog or market prices (see FAR 22.1003-4(c)(2)(ii)) for the maintenance, calibration, or repair of such equipment; and</p><p>(iii) The compensation (wage and fringe benefits) plan for all service employees performing work under the contract will be the same as that used for these employees and equivalent employees servicing the same equipment of commercial customers.</p><p>(2)____&nbsp;&nbsp;Certain services as described in FAR 22.1003-4(d)(1). The offeror ____&nbsp;&nbsp;does ____&nbsp;&nbsp;does not certify that&mdash;</p><p>(i) The services under the contract are offered and sold regularly to non-Governmental customers, and are provided by the offeror (or subcontractor in the case of an exempt subcontract) to the general public in substantial quantities in the course of normal business operations;</p><p>(ii) The contract services will be furnished at prices that are, or are based on, established catalog or market prices (see FAR 22.1003-4(d)(2)(iii));</p><p>(iii) Each service employee who will perform the services under the contract will spend only a small portion of his or her time (a monthly average of less than 20 percent of the available hours on an annualized basis, or less than 20 percent of available hours during the contract period if the contract period is less than a month) servicing the Government contract; and</p><p>(iv) The compensation (wage and fringe benefits) plan for all service employees performing work under the contract is the same as that used for these employees and equivalent employees servicing commercial customers.</p><p>(3) If paragraph (k)(1) or (k)(2) of this clause applies&mdash;</p><p>(i) If the offeror does not certify to the conditions in paragraph (k)(1) or (k)(2) and the Contracting Officer did not attach a Service Contract Labor Standards wage determination to the solicitation, the offeror shall notify the Contracting Officer as soon as possible; and</p><p>(ii) The Contracting Officer may not make an award to the offeror if the offeror fails to execute the certification in paragraph (k)(1) or (k)(2) of this clause or to contact the Contracting Officer as required in paragraph (k)(3)(i) of this clause.</p><p>(l) <i>Taxpayer Identification Number (TIN) (26 U.S.C. 6109, 31 U.S.C. 7701).</i> (Not applicable if the offeror is required to provide this information to the SAM database to be eligible for award.)</p><p>(1) All offerors must submit the information required in paragraphs (l)(3) through (l)(5) of this provision to comply with debt collection requirements of 31 U.S.C. 7701(c) and 3325(d), reporting requirements of 26 U.S.C. 6041, 6041A, and 6050M, and implementing regulations issued by the Internal Revenue Service (IRS).</p><p>(2) The TIN may be used by the Government to collect and report on any delinquent amounts arising out of the offeror''s relationship with the Government (31 U.S.C. 7701(c)(3)). If the resulting contract is subject to the payment reporting requirements described in FAR 4.904, the TIN provided hereunder may be matched with IRS records to verify the accuracy of the offeror''s TIN.</p><p>(3) <i>Taxpayer Identification Number (TIN).</i></p><p>____&nbsp;&nbsp;TIN: __________.</p><p>____&nbsp;&nbsp;TIN has been applied for.</p><p>____&nbsp;&nbsp;TIN is not required because:</p><p>____&nbsp;&nbsp;Offeror is a nonresident alien, foreign corporation, or foreign partnership that does not have income effectively connected with the conduct of a trade or business in the United States and does not have an office or place of business or a fiscal paying agent in the United States;</p><p>____&nbsp;&nbsp;Offeror is an agency or instrumentality of a foreign government;</p><p>____&nbsp;&nbsp;Offeror is an agency or instrumentality of the Federal Government.</p><p>(4) <i>Type of organization.</i></p><p>____&nbsp;&nbsp;Sole proprietorship;</p><p>____&nbsp;&nbsp;Partnership;</p><p>____&nbsp;&nbsp;Corporate entity (not tax-exempt);</p><p>____&nbsp;&nbsp;Corporate entity (tax-exempt);</p><p>____&nbsp;&nbsp;Government entity (Federal, State, or local);</p><p>____&nbsp;&nbsp;Foreign government;</p><p>____&nbsp;&nbsp;International organization per 26 CFR 1.6049-4;</p><p>____&nbsp;&nbsp;Other _____.</p><p>(5) <i>Common parent.</i></p><p>____&nbsp;&nbsp;Offeror is not owned or controlled by a common parent;</p><p>____&nbsp;&nbsp;Name and TIN of common parent:</p><p>Name __________.</p><p>TIN __________.</p><p>(m) <i>Restricted business operations in Sudan.</i> By submission of its offer, the offeror certifies that the offeror does not conduct any restricted business operations in Sudan.</p><p>(n) <i>Prohibition on Contracting with Inverted Domestic Corporations.</i> (1) Government agencies are not permitted to use appropriated (or otherwise made available) funds for contracts with either an inverted domestic corporation, or a subsidiary of an inverted domestic corporation, unless the exception at 9.108-2(b) applies or the requirement is waived in accordance with the procedures at 9.108-4.</p><p>(2) <i>Representation.</i> The Offeror represents that&mdash;</p><p>(i) It ____ is, ____ is not an inverted domestic corporation; and</p><p>(ii) It ____ is, ____ is not a subsidiary of an inverted domestic corporation.</p><p>(o) <i>Prohibition on contracting with entities engaging in certain activities or transactions relating to Iran.</i> (1) The offeror shall email questions concerning sensitive technology to the Department of State at CISADA106@state.gov.</p><p>(2) <i>Representation and certifications.</i> Unless a waiver is granted or an exception applies as provided in paragraph (o)(3) of this provision, by submission of its offer, the offeror&mdash;</p><p>(i) Represents, to the best of its knowledge and belief, that the offeror does not export any sensitive technology to the government of Iran or any entities or individuals owned or controlled by, or acting on behalf or at the direction of, the government of Iran;</p><p>(ii) Certifies that the offeror, or any person owned or controlled by the offeror, does not engage in any activities for which sanctions may be imposed under section 5 of the Iran Sanctions Act; and</p><p>(iii) Certifies that the offeror, and any person owned or controlled by the offeror, does not knowingly engage in any transaction that exceeds $3,500 with Iran''s Revolutionary Guard Corps or any of its officials, agents, or affiliates, the property and interests in property of which are blocked pursuant to the International Emergency Economic Powers Act (50 U.S.C. 1701 <i>et seq.</i>) (see OFAC''s Specially Designated Nationals and Blocked Persons List at <i>http://www.treasury.gov/ofac/downloads/t11sdn.pdf</i>).</p><p>(3) The representation and certification requirements of paragraph (o)(2) of this provision do not apply if&mdash;</p><p>(i) This solicitation includes a trade agreements certification (<i>e.g.,</i> 52.212-3(g) or a comparable agency provision); and</p><p>(ii) The offeror has certified that all the offered products to be supplied are designated country end products.</p>' WHERE fill_in_code = 'memobox_fe_52.212-3[0]'
AND clause_id IN (SELECT clause_id FROM Clauses 
WHERE clause_name = '52.212-3' AND clause_version_id = 14) ;
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size, fill_in_group_number, fill_in_placeholder, fill_in_default_data, fill_in_display_rows, fill_in_for_table, fill_in_heading) 
SELECT clause_id, 'checkbox_52.212-3[0]', 'C', NULL, NULL, NULL, NULL, NULL, 0, NULL FROM Clauses WHERE clause_name = '52.212-3' AND clause_version_id = 14; 
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size, fill_in_group_number, fill_in_placeholder, fill_in_default_data, fill_in_display_rows, fill_in_for_table, fill_in_heading) 
SELECT clause_id, 'checkbox_52.212-3[1]', 'C', NULL, NULL, NULL, NULL, NULL, 0, NULL FROM Clauses WHERE clause_name = '52.212-3' AND clause_version_id = 14; 

UPDATE Clauses
SET clause_data = '<p>(a) <i>Acceptance period,</i> as used in this provision, means the number of calendar days available to the Government for awarding a contract from the date specified in this solicitation for receipt of bids.</p><p>(b) This provision supersedes any language pertaining to the acceptance period that may appear elsewhere in this solicitation.</p><p>(c) The Government requires a minimum acceptance period of {{textbox_52.214-16[0]}} calendar days [<i>the Contracting Officer shall insert the number of days</i>].</p><p>(d) In the space provided immediately below, bidders may specify a longer acceptance period than the Government''s minimum requirement.</p><p>The bidder allows the following acceptance period: {{textbox_52.214-16[1]}} calendar days.</p><p>(e) A bid allowing less than the Government''s minimum acceptance period will be rejected.</p><p>(f) The bidder agrees to execute all that it has undertaken to do, in compliance with its bid, if that bid is accepted in writing within (1) the acceptance period stated in paragraph (c) above or (2) any longer acceptance period stated in paragraph (d) above.</p>'
--               '<p>(a) <i>Acceptance period,</i> as used in this provision, means the number of calendar days available to the Government for awarding a contract from the date specified in this solicitation for receipt of bids.</p><p>(b) This provision supersedes any language pertaining to the acceptance period that may appear elsewhere in this solicitation.</p><p>(c) The Government requires a minimum acceptance period of _ calendar days [<i>the Contracting Officer shall insert the number of days</i>].</p><p>(d) In the space provided immediately below, bidders may specify a longer acceptance period than the Government''s minimum requirement.</p><p>The bidder allows the following acceptance period: _ calendar days.</p><p>(e) A bid allowing less than the Government''s minimum acceptance period will be rejected.</p><p>(f) The bidder agrees to execute all that it has undertaken to do, in compliance with its bid, if that bid is accepted in writing within (1) the acceptance period stated in paragraph (c) above or (2) any longer acceptance period stated in paragraph (d) above.</p>'
WHERE clause_version_id = 14 AND clause_name = '52.214-16';

INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size, fill_in_group_number, fill_in_placeholder, fill_in_default_data, fill_in_display_rows, fill_in_for_table, fill_in_heading) 
SELECT clause_id, 'textbox_52.214-16[0]', 'S', 100, NULL, NULL, NULL, NULL, 0, NULL FROM Clauses WHERE clause_name = '52.214-16' AND clause_version_id = 14; 
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size, fill_in_group_number, fill_in_placeholder, fill_in_default_data, fill_in_display_rows, fill_in_for_table, fill_in_heading) 
SELECT clause_id, 'textbox_52.214-16[1]', 'S', 100, NULL, NULL, NULL, NULL, 0, NULL FROM Clauses WHERE clause_name = '52.214-16' AND clause_version_id = 14; 

UPDATE Clauses
SET clause_conditions = '(A) DOCUMENT TYPE IS: SOLICITATION (B) DOCUMENT TYPE IS: AWARD (C) PRICING REQUIREMENTS IS: CERTIFIED COST AND PRICING DATA ARE REQUIRED FOR MODIFICATIONS (D) PRICING REQUIREMENTS IS: CONTRACT REQUIRES OTHER THAN CERTIFIED COST AND PRICING DATA FOR MODIFICATIONS (E) PROCEDURES IS: NEGOTIATION (FAR PART 15) (F) PROCEDURES IS: NOT COMMERCIAL PROCEDURES (FAR PART 12) (G) BUSINESS TYPE IS: NOT CANADIAN COMMERCIAL CORPORATION' -- '(A) DOCUMENT TYPE IS: SOLICITATION (B) DOCUMENT TYPE IS: AWARD (C) PRICING REQUIREMENTS IS: CERTIFIED COST AND PRICING DATA ARE REQUIRED FOR MODIFICATIONS (D) PRICING REQUIREMENTS IS: CONTRACT REQUIRES OTHER THAN CERTIFIED COST AND PRICING DATA FOR MODIFICATIONS (E) PROCEDURES IS: NEGOTIATION (FAR PART 15) (F) PROCEDURES IS: NOT COMMERCIAL PROCEDURES (FAR PART 12)'
, clause_rule = '(A OR B) AND (C OR D) AND E AND F AND G' -- '(A OR B) AND (C OR D) AND E AND F'
WHERE clause_version_id = 14 AND clause_name = '52.215-21';

UPDATE Clauses
SET is_editable = 0
, editable_remarks = NULL -- 'ALL'
, clause_data = '<p>(a) Any supplies and services to be furnished under this contract shall be ordered by issuance of delivery orders or task orders by the individuals or activities designated in the Schedule. Such orders may be issued from {{textbox_52.216-18[0]}} through {{textbox_52.216-18[1]}} [<i>insert dates</i>].</p><p>(b) All delivery orders or task orders are subject to the terms and conditions of this contract. In the event of conflict between a delivery order or task order and this contract, the contract shall control.</p><p>(c) If mailed, a delivery order or task order is considered ''issued" when the Government deposits the order in the mail. Orders may be issued orally, by facsimile, or by electronic commerce methods only if authorized in the Schedule.</p>'
--               '{{memobox_fe_52.216-18[0]}}'
WHERE clause_version_id = 14 AND clause_name = '52.216-18';

DELETE FROM Document_Fill_Ins WHERE clause_fill_in_id IN (SELECT clause_fill_in_id FROM Clause_Fill_Ins CF INNER JOIN Clauses C  ON C.clause_id= CF.clause_id
WHERE clause_name = '52.216-18' AND clause_version_id = 14 AND fill_in_code='memobox_fe_52.216-18[0]');
DELETE FROM Clause_Fill_Ins WHERE clause_id IN (SELECT clause_id FROM Clauses 
WHERE clause_name = '52.216-18' AND clause_version_id = 14)  AND fill_in_code='memobox_fe_52.216-18[0]';

UPDATE Clauses
SET clause_data = '<p>(a) <i>Definitions.</i> As used in this clause-</p><p><i>Long-term contract</i> means a contract of more than five years in duration, including options. However, the term does not include contracts that exceed five years in duration because the period of performance has been extended for a cumulative period not to exceed six months under the clause at 52.217-8, Option to Extend Services, or other appropriate authority.</p><p><i>Small business concern</i> means a concern, including its affiliates, that is independently owned and operated, not dominant in the field of operation in which it is bidding on Government contracts, and qualified as a small business under the criteria in 13 CFR part 121 and the size standard in paragraph (c) of this clause. Such a concern is ''not dominant in its field of operation" when it does not exercise a controlling or major influence on a national basis in a kind of business activity in which a number of business concerns are primarily engaged. In determining whether dominance exists, consideration shall be given to all appropriate factors, including volume of business, number of employees, financial resources, competitive status or position, ownership or control of materials, processes, patents, license agreements, facilities, sales territory, and nature of business activity.</p><p>(b) If the Contractor represented that it was a small business concern prior to award of this contract, the Contractor shall rerepresent its size status according to paragraph (e) of this clause or, if applicable, paragraph (g) of this clause, upon the occurrence of any of the following:</p><p>(1) Within 30 days after execution of a novation agreement or within 30 days after modification of the contract to include this clause, if the novation agreement was executed prior to inclusion of this clause in the contract.</p><p>(2) Within 30 days after a merger or acquisition that does not require a novation or within 30 days after modification of the contract to include this clause, if the merger or acquisition occurred prior to inclusion of this clause in the contract.</p><p>(3) For long-term contracts-</p><p>(i) Within 60 to 120 days prior to the end of the fifth year of the contract; and</p><p>(ii) Within 60 to 120 days prior to the date specified in the contract for exercising any option thereafter.</p><p>(c) The Contractor shall rerepresent its size status in accordance with the size standard in effect at the time of this rerepresentation that corresponds to the North American Industry Classification System (NAICS) code assigned to this contract. The small business size standard corresponding to this NAICS code can be found at <i>http://www.sba.gov/content/table-small-business-size-standards.</i></p><p>(d) The small business size standard for a Contractor providing a product which it does not manufacture itself, for a contract other than a construction or service contract, is 500 employees.</p><p>(e) Except as provided in paragraph (g) of this clause, the Contractor shall make the representation required by paragraph (b) of this clause by validating or updating all its representations in the Representations and Certifications section of the System for Award Management (SAM) and its other data in SAM, as necessary, to ensure that they reflect the Contractor''s current status. The Contractor shall notify the contracting office in writing within the timeframes specified in paragraph (b) of this clause that the data have been validated or updated, and provide the date of the validation or update.</p><p>(f) If the Contractor represented that it was other than a small business concern prior to award of this contract, the Contractor may, but is not required to, take the actions required by paragraphs (e) or (g) of this clause.</p><p>(g) If the Contractor does not have representations and certifications in SAM, or does not have a representation in SAM for the NAICS code applicable to this contract, the Contractor is required to complete the following rerepresentation and submit it to the contracting office, along with the contract number and the date on which the rerepresentation was completed:</p><p>The Contractor represents that it is, is not a small business concern under NAICS Code {{textbox_52.219-28[0]}} assigned to contract number {{textbox_52.219-28[1]}}.</p><p>Contractor Name: {{textbox_52.219-28[2]}} Date: {{textbox_52.219-28[3]}} Signer''s Name: {{textbox_52.219-28[4]}} Title: {{textbox_52.219-28[5]}} <br>[<i>Contractor to sign and date and insert authorized signer''s name and title</i>].</br></p>'
--               '<p>(a) <i>Definitions.</i> As used in this clause-</p><p><i>Long-term contract</i> means a contract of more than five years in duration, including options. However, the term does not include contracts that exceed five years in duration because the period of performance has been extended for a cumulative period not to exceed six months under the clause at 52.217-8, Option to Extend Services, or other appropriate authority.</p><p><i>Small business concern</i> means a concern, including its affiliates, that is independently owned and operated, not dominant in the field of operation in which it is bidding on Government contracts, and qualified as a small business under the criteria in 13 CFR part 121 and the size standard in paragraph (c) of this clause. Such a concern is ''not dominant in its field of operation" when it does not exercise a controlling or major influence on a national basis in a kind of business activity in which a number of business concerns are primarily engaged. In determining whether dominance exists, consideration shall be given to all appropriate factors, including volume of business, number of employees, financial resources, competitive status or position, ownership or control of materials, processes, patents, license agreements, facilities, sales territory, and nature of business activity.</p><p>(b) If the Contractor represented that it was a small business concern prior to award of this contract, the Contractor shall rerepresent its size status according to paragraph (e) of this clause or, if applicable, paragraph (g) of this clause, upon the occurrence of any of the following:</p><p>(1) Within 30 days after execution of a novation agreement or within 30 days after modification of the contract to include this clause, if the novation agreement was executed prior to inclusion of this clause in the contract.</p><p>(2) Within 30 days after a merger or acquisition that does not require a novation or within 30 days after modification of the contract to include this clause, if the merger or acquisition occurred prior to inclusion of this clause in the contract.</p><p>(3) For long-term contracts-</p><p>(i) Within 60 to 120 days prior to the end of the fifth year of the contract; and</p><p>(ii) Within 60 to 120 days prior to the date specified in the contract for exercising any option thereafter.</p><p>(c) The Contractor shall rerepresent its size status in accordance with the size standard in effect at the time of this rerepresentation that corresponds to the North American Industry Classification System (NAICS) code assigned to this contract. The small business size standard corresponding to this NAICS code can be found at <i>http://www.sba.gov/content/table-small-business-size-standards.</i></p><p>(d) The small business size standard for a Contractor providing a product which it does not manufacture itself, for a contract other than a construction or service contract, is 500 employees.</p><p>(e) Except as provided in paragraph (g) of this clause, the Contractor shall make the representation required by paragraph (b) of this clause by validating or updating all its representations in the Representations and Certifications section of the System for Award Management (SAM) and its other data in SAM, as necessary, to ensure that they reflect the Contractor''s current status. The Contractor shall notify the contracting office in writing within the timeframes specified in paragraph (b) of this clause that the data have been validated or updated, and provide the date of the validation or update.</p><p>(f) If the Contractor represented that it was other than a small business concern prior to award of this contract, the Contractor may, but is not required to, take the actions required by paragraphs (e) or (g) of this clause.</p><p>(g) If the Contractor does not have representations and certifications in SAM, or does not have a representation in SAM for the NAICS code applicable to this contract, the Contractor is required to complete the following rerepresentation and submit it to the contracting office, along with the contract number and the date on which the rerepresentation was completed:</p><p>The Contractor represents that it is, is not a small business concern under NAICS Code {{textbox_52.219-28[0]}}{{textbox_52.219-28[1]}} assigned to contract number {{textbox_52.219-28[2]}}{{textbox_52.219-28[3]}}.</p><p>Contractor Name: {{textbox_52.219-28[4]}} Date: {{textbox_52.219-28[5]}} Signer''s Name: {{textbox_52.219-28[6]}} Title: {{textbox_52.219-28[7]}} <br>[<i>Contractor to sign and date and insert authorized signer''s name and title</i>].</br></p>'
WHERE clause_version_id = 14 AND clause_name = '52.219-28';

DELETE FROM Document_Fill_Ins WHERE clause_fill_in_id IN (SELECT clause_fill_in_id FROM Clause_Fill_Ins CF INNER JOIN Clauses C  ON C.clause_id= CF.clause_id
WHERE clause_name = '52.219-28' AND clause_version_id = 14 AND fill_in_code='textbox_52.219-28[6]');
DELETE FROM Clause_Fill_Ins WHERE clause_id IN (SELECT clause_id FROM Clauses 
WHERE clause_name = '52.219-28' AND clause_version_id = 14)  AND fill_in_code='textbox_52.219-28[6]';
DELETE FROM Document_Fill_Ins WHERE clause_fill_in_id IN (SELECT clause_fill_in_id FROM Clause_Fill_Ins CF INNER JOIN Clauses C  ON C.clause_id= CF.clause_id
WHERE clause_name = '52.219-28' AND clause_version_id = 14 AND fill_in_code='textbox_52.219-28[7]');
DELETE FROM Clause_Fill_Ins WHERE clause_id IN (SELECT clause_id FROM Clauses 
WHERE clause_name = '52.219-28' AND clause_version_id = 14)  AND fill_in_code='textbox_52.219-28[7]';

UPDATE Clauses
SET clause_data = '<p>(a) <i>Definitions. See</i> 13 CFR 125.6(e) for definitions of terms used in paragraph (d).</p><p>(b) <i>Evaluation preference.</i> (1) Offers will be evaluated by adding a factor of 10 percent to the price of all offers, except-</p><p>(i) Offers from HUBZone small business concerns that have not waived the evaluation preference; and</p><p>(ii) Otherwise successful offers from small business concerns.</p><p>(2) The factor of 10 percent shall be applied on a line item basis or to any group of items on which award may be made. Other evaluation factors described in the solicitation shall be applied before application of the factor.</p><p>(3) When the two highest rated offerors are a HUBZone small business concern and a large business, and the evaluated offer of the HUBZone small business concern is equal to the evaluated offer of the large business after considering the price evaluation preference, award will be made to the HUBZone small business concern.</p><p>(c) <i>Waiver of evaluation preference.</i> A HUBZone small business concern may elect to waive the evaluation preference, in which case the factor will be added to its offer for evaluation purposes. The agreements in paragraphs (d) and (e) of this clause do not apply if the offeror has waived the evaluation preference.</p><p>{{checkbox_52.219-4[0]}} &nbsp;&nbsp;Offeror elects to waive the evaluation preference.</p><p>(d) <i>Agreement.</i> A HUBZone small business concern agrees that in the performance of the contract, in the case of a contract for</p><p>(1) Services (except construction), at least 50 percent of the cost of personnel for contract performance will be spent for employees of the concern or employees of other HUBZone small business concerns;</p><p>(2) Supplies (other than procurement from a nonmanufacturer of such supplies), at least 50 percent of the cost of manufacturing, excluding the cost of materials, will be performed by the concern or other HUBZone small business concerns;</p><p>(3) <i>General construction.</i> (i) At least 15 percent of the cost of contract performance to be incurred for personnel will be spent on the prime contractor''s employees;</p><p>(ii) At least 50 percent of the cost of the contract performance to be incurred for personnel will be spent on the prime contractor''s employees or on a combination of the prime contractor''s employees and employees of HUBZone small business concern subcontractors;</p><p>(iii) No more than 50 percent of the cost of contract performance to be incurred for personnel will be subcontracted to concerns that are not HUBZone small business concerns; or</p><p>(4) <i>Construction by special trade contractors.</i> (i) At least 25 percent of the cost of contract performance to be incurred for personnel will be spent on the prime contractor''s employees;</p><p>(ii) At least 50 percent of the cost of the contract performance to be incurred for personnel will be spent on the prime contractor''s employees or on a combination of the prime contractor''s employees and employees of HUBZone small business concern subcontractors;</p><p>(iii) No more than 50 percent of the cost of contract performance to be incurred for personnel will be subcontracted to concerns that are not HUBZone small business concerns.</p><p>(e) A HUBZone joint venture agrees that the aggregate of the HUBZone small business concerns to the joint venture, not each concern separately, will perform the applicable percentage of work requirements.</p><p>(f)(1) When the total value of the contract exceeds $25,000, a HUBZone small business concern nonmanufacturer agrees to furnish in performing this contract only end items manufactured or produced by HUBZone small business concern manufacturers.</p><p>(2) When the total value of the contract is equal to or less than $25,000, a HUBZone small business concern nonmanufacturer may provide end items manufactured by other than a HUBZone small business concern manufacturer provided the end items are produced or manufactured in the United States.</p><p>(3) Paragraphs (f)(1) and (f)(2) of this section do not apply in connection with construction or service contracts.</p><p>(g) <i>Notice.</i> The HUBZone small business offeror acknowledges that a prospective HUBZone awardee must be a HUBZone small business concern at the time of award of this contract. The HUBZone offeror shall provide the Contracting Officer a copy of the notice required by 13 CFR 126.501 if material changes occur before contract award that could affect its HUBZone eligibility. If the apparently successful HUBZone offeror is not a HUBZone small business concern at the time of award of this contract, the Contracting Officer will proceed to award to the next otherwise successful HUBZone small business concern or other offeror.</p>'
--               '<p>(a) <i>Definitions. See</i> 13 CFR 125.6(e) for definitions of terms used in paragraph (d).</p><p>(b) <i>Evaluation preference.</i> (1) Offers will be evaluated by adding a factor of 10 percent to the price of all offers, except-</p><p>(i) Offers from HUBZone small business concerns that have not waived the evaluation preference; and</p><p>(ii) Otherwise successful offers from small business concerns.</p><p>(2) The factor of 10 percent shall be applied on a line item basis or to any group of items on which award may be made. Other evaluation factors described in the solicitation shall be applied before application of the factor.</p><p>(3) When the two highest rated offerors are a HUBZone small business concern and a large business, and the evaluated offer of the HUBZone small business concern is equal to the evaluated offer of the large business after considering the price evaluation preference, award will be made to the HUBZone small business concern.</p><p>(c) <i>Waiver of evaluation preference.</i> A HUBZone small business concern may elect to waive the evaluation preference, in which case the factor will be added to its offer for evaluation purposes. The agreements in paragraphs (d) and (e) of this clause do not apply if the offeror has waived the evaluation preference.</p><p>{{textbox_52.219-4[0]}}&nbsp;&nbsp;Offeror elects to waive the evaluation preference.</p><p>(d) <i>Agreement.</i> A HUBZone small business concern agrees that in the performance of the contract, in the case of a contract for</p><p>(1) Services (except construction), at least 50 percent of the cost of personnel for contract performance will be spent for employees of the concern or employees of other HUBZone small business concerns;</p><p>(2) Supplies (other than procurement from a nonmanufacturer of such supplies), at least 50 percent of the cost of manufacturing, excluding the cost of materials, will be performed by the concern or other HUBZone small business concerns;</p><p>(3) <i>General construction.</i> (i) At least 15 percent of the cost of contract performance to be incurred for personnel will be spent on the prime contractor''s employees;</p><p>(ii) At least 50 percent of the cost of the contract performance to be incurred for personnel will be spent on the prime contractor''s employees or on a combination of the prime contractor''s employees and employees of HUBZone small business concern subcontractors;</p><p>(iii) No more than 50 percent of the cost of contract performance to be incurred for personnel will be subcontracted to concerns that are not HUBZone small business concerns; or</p><p>(4) <i>Construction by special trade contractors.</i> (i) At least 25 percent of the cost of contract performance to be incurred for personnel will be spent on the prime contractor''s employees;</p><p>(ii) At least 50 percent of the cost of the contract performance to be incurred for personnel will be spent on the prime contractor''s employees or on a combination of the prime contractor''s employees and employees of HUBZone small business concern subcontractors;</p><p>(iii) No more than 50 percent of the cost of contract performance to be incurred for personnel will be subcontracted to concerns that are not HUBZone small business concerns.</p><p>(e) A HUBZone joint venture agrees that the aggregate of the HUBZone small business concerns to the joint venture, not each concern separately, will perform the applicable percentage of work requirements.</p><p>(f)(1) When the total value of the contract exceeds $25,000, a HUBZone small business concern nonmanufacturer agrees to furnish in performing this contract only end items manufactured or produced by HUBZone small business concern manufacturers.</p><p>(2) When the total value of the contract is equal to or less than $25,000, a HUBZone small business concern nonmanufacturer may provide end items manufactured by other than a HUBZone small business concern manufacturer provided the end items are produced or manufactured in the United States.</p><p>(3) Paragraphs (f)(1) and (f)(2) of this section do not apply in connection with construction or service contracts.</p><p>(g) <i>Notice.</i> The HUBZone small business offeror acknowledges that a prospective HUBZone awardee must be a HUBZone small business concern at the time of award of this contract. The HUBZone offeror shall provide the Contracting Officer a copy of the notice required by 13 CFR 126.501 if material changes occur before contract award that could affect its HUBZone eligibility. If the apparently successful HUBZone offeror is not a HUBZone small business concern at the time of award of this contract, the Contracting Officer will proceed to award to the next otherwise successful HUBZone small business concern or other offeror.</p>'
WHERE clause_version_id = 14 AND clause_name = '52.219-4';

INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size, fill_in_group_number, fill_in_placeholder, fill_in_default_data, fill_in_display_rows, fill_in_for_table, fill_in_heading) 
SELECT clause_id, 'checkbox_52.219-4[0]', 'C', NULL, NULL, NULL, NULL, NULL, 0, NULL FROM Clauses WHERE clause_name = '52.219-4' AND clause_version_id = 14; 
DELETE FROM Document_Fill_Ins WHERE clause_fill_in_id IN (SELECT clause_fill_in_id FROM Clause_Fill_Ins CF INNER JOIN Clauses C  ON C.clause_id= CF.clause_id
WHERE clause_name = '52.219-4' AND clause_version_id = 14 AND fill_in_code='textbox_52.219-4[0]');
DELETE FROM Clause_Fill_Ins WHERE clause_id IN (SELECT clause_id FROM Clauses 
WHERE clause_name = '52.219-4' AND clause_version_id = 14)  AND fill_in_code='textbox_52.219-4[0]';

UPDATE Clauses
SET effective_date = '2014-05-01' -- NULL
WHERE clause_version_id = 14 AND clause_name = '52.225-3';

UPDATE Clauses
SET clause_data = '<p>(a) <i>Set-aside area.</i> The area covered in this contract is: {{textbox_52.226-3[0]}}__ [<i>Contracting Officer to fill in with definite geographic boundaries.</i>]</p><p>(b) <i>Representations.</i> The offeror represents that it ___does {{checkbox_52.226-3[0]}} does not reside or primarily do business in the set-aside area.</p><p>(c) An offeror is considered to be residing or primarily doing business in the set-aside area if, during the last twelve months-</p><p>(1) The offeror had its main operating office in the area; and</p><p>(2) That office generated at least half of the offeror''s gross revenues and employed at least half of the offeror''s permanent employees.</p><p>(d) If the offeror does not meet the criteria in paragraph (c) of this provision, factors to be considered in determining whether an offeror resides or primarily does business in the set-aside area include-</p><p>(1) Physical location(s) of the offeror''s permanent office(s) and date any office in the set-aside area(s) was established;</p><p>(2) Current state licenses;</p><p>(3) Record of past work in the set-aside area(s) (<i>e.g.</i>, how much and for how long);</p><p>(4) Contractual history the offeror has had with subcontractors and/or suppliers in the set-aside area;</p><p>(5) Percentage of the offeror''s gross revenues attributable to work performed in the set-aside area;</p><p>(6) Number of permanent employees the offeror employs in the set-aside area;</p><p>(7) Membership in local and state organizations in the set-aside area; and</p><p>(8) Other evidence that establishes the offeror resides or primarily does business in the set-aside area. For example, sole proprietorships may submit utility bills and bank statements.</p><p>(e) If the offeror represents it resides or primarily does business in the set-aside area, the offeror shall furnish documentation to support its representation if requested by the Contracting Officer. The solicitation may require the offeror to submit with its offer documentation to support the representation.</p>'
--               '<p>(a) <i>Set-aside area.</i> The area covered in this contract is: {{textbox_52.226-3[0]}}{{textbox_52.226-3[1]}}_ [<i>Contracting Officer to fill in with definite geographic boundaries.</i>]</p><p>(b) <i>Representations.</i> The offeror represents that it ___does {{checkbox_52.226-3[0]}} does not reside or primarily do business in the set-aside area.</p><p>(c) An offeror is considered to be residing or primarily doing business in the set-aside area if, during the last twelve months-</p><p>(1) The offeror had its main operating office in the area; and</p><p>(2) That office generated at least half of the offeror''s gross revenues and employed at least half of the offeror''s permanent employees.</p><p>(d) If the offeror does not meet the criteria in paragraph (c) of this provision, factors to be considered in determining whether an offeror resides or primarily does business in the set-aside area include-</p><p>(1) Physical location(s) of the offeror''s permanent office(s) and date any office in the set-aside area(s) was established;</p><p>(2) Current state licenses;</p><p>(3) Record of past work in the set-aside area(s) (<i>e.g.</i>, how much and for how long);</p><p>(4) Contractual history the offeror has had with subcontractors and/or suppliers in the set-aside area;</p><p>(5) Percentage of the offeror''s gross revenues attributable to work performed in the set-aside area;</p><p>(6) Number of permanent employees the offeror employs in the set-aside area;</p><p>(7) Membership in local and state organizations in the set-aside area; and</p><p>(8) Other evidence that establishes the offeror resides or primarily does business in the set-aside area. For example, sole proprietorships may submit utility bills and bank statements.</p><p>(e) If the offeror represents it resides or primarily does business in the set-aside area, the offeror shall furnish documentation to support its representation if requested by the Contracting Officer. The solicitation may require the offeror to submit with its offer documentation to support the representation.</p>'
WHERE clause_version_id = 14 AND clause_name = '52.226-3';

DELETE FROM Document_Fill_Ins WHERE clause_fill_in_id IN (SELECT clause_fill_in_id FROM Clause_Fill_Ins CF INNER JOIN Clauses C  ON C.clause_id= CF.clause_id
WHERE clause_name = '52.226-3' AND clause_version_id = 14 AND fill_in_code='textbox_52.226-3[1]');
DELETE FROM Clause_Fill_Ins WHERE clause_id IN (SELECT clause_id FROM Clauses 
WHERE clause_name = '52.226-3' AND clause_version_id = 14)  AND fill_in_code='textbox_52.226-3[1]';

UPDATE Clauses
SET effective_date = '2012-04-01' -- NULL
WHERE clause_version_id = 14 AND clause_name = '52.232-16';

UPDATE Clauses
SET clause_rule = 'A AND B AND C AND D AND E' -- 'A AND B AND AND D AND E'
WHERE clause_version_id = 14 AND clause_name = '52.247-45';

UPDATE Clauses
SET effective_date = '2014-04-01' -- NULL
WHERE clause_version_id = 14 AND clause_name = '252.216-7010 Alternate I';

UPDATE Clauses
SET clause_conditions = '(A) DOCUMENT TYPE IS: SOLICITATION\n(B) DOCUMENT TYPE IS: AWARD\n(C) PRICING REQUIREMENTS IS: CERTIFIED COST AND PRICING DATA ARE REQUIRED FOR MODIFICATIONS\n(D) PRICING REQUIREMENTS IS: CONTRACT REQUIRES OTHER THAN CERTIFIED COST AND PRICING DATA FOR MODIFICATIONS\n(E) PROCEDURES IS: NEGOTIATION (FAR PART 15)\n(F) REGULATION PRICING IS: NOT FORMAT IN TABLE 15.2 IS USED\n(G) PROCEDURES IS: NOT COMMERCIAL PROCEDURES (FAR PART 12)\n(H) BUSINESS TYPE IS: NOT CANADIAN COMMERCIAL CORPORATION' -- '(A) DOCUMENT TYPE IS: SOLICITATION\n(B) DOCUMENT TYPE IS: AWARD\n(C) PRICING REQUIREMENTS IS: CERTIFIED COST AND PRICING DATA ARE REQUIRED FOR MODIFICATIONS\n(D) PRICING REQUIREMENTS IS: CONTRACT REQUIRES OTHER THAN CERTIFIED COST AND PRICING DATA FOR MODIFICATIONS\n(E) PROCEDURES IS: NEGOTIATION (FAR PART 15)\n(F) REGULATION PRICING IS: NOT FORMAT IN TABLE 15.2 IS USED\n(G) PROCEDURES IS: NOT COMMERCIAL PROCEDURES (FAR PART 12)'
, clause_rule = '(A OR B) AND (C OR D) AND E AND F AND G AND H' -- '(A OR B) AND (C OR D) AND E AND F AND G'
WHERE clause_version_id = 14 AND clause_name = '52.215-21 Alternate I';

UPDATE Clauses
SET clause_conditions = '(A) DOCUMENT TYPE IS: SOLICITATION (B) DOCUMENT TYPE IS: AWARD (C) PRICING REQUIREMENTS IS: CERTIFIED COST AND PRICING DATA ARE REQUIRED FOR MODIFICATIONS (D) PRICING REQUIREMENTS IS: CONTRACT REQUIRES OTHER THAN CERTIFIED COST AND PRICING DATA FOR MODIFICATIONS (E) PROCEDURES IS: NEGOTIATION (FAR PART 15) (F) PROPOSAL REQUIREMENTS IS: PROPOSAL COPIES ARE TO BE SENT TO THE ADMINISTRATIVE CONTRACTING OFFICER (ACO) AND CONTRACT AUDITOR (BASIC AWARD AND FUTURE MODIFICATIONS) (G) PROCEDURES IS: NOT COMMERCIAL PROCEDURES (FAR PART 12) (H) BUSINESS TYPE IS: NOT CANADIAN COMMERCIAL CORPORATION' -- '(A) DOCUMENT TYPE IS: SOLICITATION (B) DOCUMENT TYPE IS: AWARD (C) PRICING REQUIREMENTS IS: CERTIFIED COST AND PRICING DATA ARE REQUIRED FOR MODIFICATIONS (D) PRICING REQUIREMENTS IS: CONTRACT REQUIRES OTHER THAN CERTIFIED COST AND PRICING DATA FOR MODIFICATIONS (E) PROCEDURES IS: NEGOTIATION (FAR PART 15) (F) PROPOSAL REQUIREMENTS IS: PROPOSAL COPIES ARE TO BE SENT TO THE ADMINISTRATIVE CONTRACTING OFFICER (ACO) AND CONTRACT AUDITOR (BASIC AWARD AND FUTURE MODIFICATIONS) (G) PROCEDURES IS: NOT COMMERCIAL PROCEDURES (FAR PART 12)'
, clause_rule = '(A OR B) AND (C OR D) AND E AND F AND G AND H' -- '(A OR B) AND (C OR D) AND E AND F AND G'
WHERE clause_version_id = 14 AND clause_name = '52.215-21 Alternate II';

UPDATE Clauses
SET clause_conditions = '(A) DOCUMENT TYPE IS: SOLICITATION (B) DOCUMENT TYPE IS: AWARD (C) PRICING REQUIREMENTS IS: CERTIFIED COST AND PRICING DATA ARE REQUIRED FOR MODIFICATIONS (D) PRICING REQUIREMENTS IS: CONTRACT REQUIRES OTHER THAN CERTIFIED COST AND PRICING DATA FOR MODIFICATIONS (E) PROCEDURES IS: NEGOTIATION (FAR PART 15) (F) PROPOSAL REQUIREMENTS IS: SUBMISSION VIA ELECTRONIC MEDIA IS REQUIRED (BASIC AWARD AND FUTURE MODIFICATIONS) (G) PROCEDURES IS: NOT COMMERCIAL PROCEDURES (FAR PART 12) (H) BUSINESS TYPE IS: NOT CANADIAN COMMERCIAL CORPORATION' -- '(A) DOCUMENT TYPE IS: SOLICITATION (B) DOCUMENT TYPE IS: AWARD (C) PRICING REQUIREMENTS IS: CERTIFIED COST AND PRICING DATA ARE REQUIRED FOR MODIFICATIONS (D) PRICING REQUIREMENTS IS: CONTRACT REQUIRES OTHER THAN CERTIFIED COST AND PRICING DATA FOR MODIFICATIONS (E) PROCEDURES IS: NEGOTIATION (FAR PART 15) (F) PROPOSAL REQUIREMENTS IS: SUBMISSION VIA ELECTRONIC MEDIA IS REQUIRED (BASIC AWARD AND FUTURE MODIFICATIONS) (G) PROCEDURES IS: NOT COMMERCIAL PROCEDURES (FAR PART 12)'
, clause_rule = '(A OR B) AND (C OR D) AND E AND F AND G AND H' -- '(A OR B) AND (C OR D) AND E AND F AND G'
WHERE clause_version_id = 14 AND clause_name = '52.215-21 Alternate III';

UPDATE Clauses
SET clause_conditions = '(A) DOCUMENT TYPE IS: SOLICITATION (B) DOCUMENT TYPE IS: AWARD (C) PRICING REQUIREMENTS IS: NOT CERTIFIED COST AND PRICING DATA ARE REQUIRED FOR MODIFICATIONS (D) PRICING REQUIREMENTS IS: CONTRACT REQUIRES OTHER THAN CERTIFIED COST AND PRICING DATA FOR MODIFICATIONS (E) PROCEDURES IS: NEGOTIATION (FAR PART 15) (F) PROCEDURES IS: NOT COMMERCIAL PROCEDURES (FAR PART 12) (G) BUSINESS TYPE IS: NOT CANADIAN COMMERCIAL CORPORATION' -- '(A) DOCUMENT TYPE IS: SOLICITATION (B) DOCUMENT TYPE IS: AWARD (C) PRICING REQUIREMENTS IS: NOT CERTIFIED COST AND PRICING DATA ARE REQUIRED FOR MODIFICATIONS (D) PRICING REQUIREMENTS IS: CONTRACT REQUIRES OTHER THAN CERTIFIED COST AND PRICING DATA FOR MODIFICATIONS (E) PROCEDURES IS: NEGOTIATION (FAR PART 15) (F) PROCEDURES IS: NOT COMMERCIAL PROCEDURES (FAR PART 12)'
, clause_rule = '(A OR B) AND C AND D AND E AND F AND G' -- '(A OR B) AND C AND D AND E AND F'
WHERE clause_version_id = 14 AND clause_name = '52.215-21 Alternate IV';

/*
*** End of SQL Scripts at Thu Dec 03 13:56:58 EST 2015 ***
*/
