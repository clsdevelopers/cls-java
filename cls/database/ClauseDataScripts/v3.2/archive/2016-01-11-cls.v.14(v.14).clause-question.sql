/*
Clause Parser Run at Mon Jan 11 01:32:52 EST 2016
(Without Correction Information)
*/
/* ===============================================
 * Prescriptions
   =============================================== */
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '10.003') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '11.107(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '11.107(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '11.204(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '11.204(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '11.204(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '11.204(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '11.304') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '11.404(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '11.404(a)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '11.404(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '11.503') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '11.503(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '11.503(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '11.503(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '11.604(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '11.604(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '11.703(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '11.703(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '11.703(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '12.301(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '12.301(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '12.301(b)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '12.301(b)(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '12.301(b)(4)(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '12.301(b)(4)(ii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '12.301(c)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '13.302-5(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '13.302-5(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '13.302-5(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '13.302-5(d)(3)(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '13.404') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(c)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(c)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(c)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(f)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(g)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(g)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(h)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(j)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(l)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(m)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(o)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(o)(2)(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(o)(2)(ii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(p)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(p)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(q)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(r)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(s)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(t)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(v)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(w)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(x)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-7(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-7(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-7(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-7(c)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-7(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.209(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.209(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.209(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.209(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.209(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.209(b)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.209(b)(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.209(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.209(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.209(f)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.209(h)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.408(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.408(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.408(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.408(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.408(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.408(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.408(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.408(f)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.408(f)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.408(g)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.408(h)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.408(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.408(j)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.408(k)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.408(l)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.408(m)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.408(n)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.408(n)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.408(n)(2)(iii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.105') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.203-4(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.203-4(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.203-4(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.205-4') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.206-4') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.30(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.307(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.307(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.307(a)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.307(a)(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.307(a)(5)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.307(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.307(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.307(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.307(e)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.307(e)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.307(f)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.307(f)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.307(g)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.406') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.406(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.406(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.506') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.506(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.506(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.506(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.506(d)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.506(d)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.506(d)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.506(d)(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.506(d)(5)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.506(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.506(f)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.506(g)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.601(f)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.601(f)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.601(f)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.603-4(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.603-4(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.603-4(b)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.603-4(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '17.109') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '17.109(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '17.208(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '17.208(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '17.208(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '17.208(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '17.208(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '17.208(f)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '17.208(g)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.1309(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.1309(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.1309(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.1309(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.1407') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.1506(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.1506(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.309(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.309(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.309(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.309(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.508(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.508(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.508(d)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.508(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.508(f)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.708(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.708(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.708(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.708(b)(1)(iii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.708(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.708(c)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.811-3') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.811-3(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.811-3(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.811-3(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.811-3(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.811-3(d)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.811-3(d)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.811-3(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2.101') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2.201') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '201.602-70') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2012-O0004') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2012-O0007') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2013-00017') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2013-00019') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2013-O0006') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2013-O0010') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2013-O0014') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2013-O0014)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2013-O0016') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2013-O0017') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2013-O0019') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2014-00014') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2014-O0004') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2014-O0005') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2014-O0009') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2014-O0010') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2014-O0016') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2014-O0018') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2014-O0020') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2014-OO0009') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2015-00003') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2015-00009') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2015-00016') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2015-00017') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2015-O0007') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2015-O0010') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2015-O0012') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2015-O0013') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2015-O0016') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2015-OO005') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2016-00001') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2016-00002') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2016-0001') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2016-0002') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2016-0003') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2016-O0003') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '203.1004(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '203.1004(b)(2)(ii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '203.171-4') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '203.171-4(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '203.570-3') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '203.97') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '204.1105') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '204.1202') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '204.404-70(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '204.404-70(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '204.404-70(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '204.470-3') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '204.7104-1(b)(3)(iv)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '204.7109(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '204.7109(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '204.7304(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '204.7304(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '204.7304(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '204.7304[c]') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '204.7403(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '204.7403(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '204.7403(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '205.47') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '206.302-3-70') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '208.7305(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '209.104-70') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '209.270-5') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '209.409') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '209.470-4(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '209.470-4(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '209.570-4(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '209.570-4(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '209.571-8(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '209.571-8(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '211.002-70') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '211.107') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '211.204(c)(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '211.204(c)(ii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '211.272') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '211.273-4') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '211.274-6(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '211.274-6(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '211.274-6(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '211.275-3') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '211.503') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '211.503(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '212.7103') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '213.106-2-70') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '215.370-3(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '215.370-3(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '215.371-6') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '215.408') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '215.408(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '215.408(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '215.408(3)(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '215.408(3)(ii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '215.408(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '215.408(4)(ii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '215.408(5)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '216.203-4-70(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '216.203-4-70(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '216.203-4-70(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '216.203-4-70(c)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '216.203-4-70(c)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '216.307(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '216.406') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '216.406(e)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '216.506(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '216.506(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '216.506(d)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '216.506(d)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '216.601(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '217.208-70(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '217.208-70(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '217.208-70(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '217.208-70(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '217.7005') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '217.7104(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '217.7303') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '217.7406(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '217.7702') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '219.309(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '219.708(b)(1)(A)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '219.708(b)(1)(B)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '219.811-3') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '219.811-3(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '219.811-3(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1006') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1006(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1006(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1006(c)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1006(c)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1006(e)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1006(e)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1006(e)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1006(e)(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1006(f)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.103-5(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.103-5(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1103') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1207') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1310(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1310(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1310(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1310(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1408(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1408(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1505(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1505(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1605') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1705(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1705(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1705(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1803') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1906') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.202') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.305') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.407(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.407(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.407(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.407(f)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.407(g)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.407(h)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.505(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.505(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.505(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.505(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.505-(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.505-(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.610') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.810(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.810(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.810(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.810(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.810(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.810(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.810(f)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.810(g)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '222.1771') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '222.7004') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '222.7102') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '222.7201(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '222.7201(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '222.7201(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '222.7302') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '222.7405') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '223.303') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '223.370-5') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '223.570-2') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '223.7106') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '223.7106(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '223.7106(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '223.7203') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '223.7306') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225-7011-3') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225-7799-4') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225-7799-4(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225-7799-4(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225-7799-4(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225-7799-4(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225-7799-4(f)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225..1101(10)(i)(C)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(1)(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(1)(ii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(10)(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(10)(i)(A)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(10)(i)(B)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(10)(i)(C)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(10)(i)(D)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(10)(i)(E)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(10)(i)(F)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(2)(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(2)(ii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(2)(iii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(5)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(5)(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(5)(ii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(6)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(6)(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(6)(ii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(7)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(8)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(9)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(9)(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(9)(ii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(9)(iii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(9)(iv)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(9)(v)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(9)(vi)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1103(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1103(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1103(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1103(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.302-6') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.371-5') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.371-5(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.372-2') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7002-2(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7002-3(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7002-3(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7003-5(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7003-5(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7003-5(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7006-4(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7006-4(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7007-3') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7009-5') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7011-3') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7012-3') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7017-4(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7017-5(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7017-5(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7102-4') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7204(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7204(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7307(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7307(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7402-5(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7503(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7503(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7503(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7503(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7503(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7503(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7503(b)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7503(b)(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7605') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7703-4(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7703-4(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7703-4(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7703-4(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.771-5') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.772-5') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7798-6(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7798-6(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7798-6(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7901-4') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7902-5(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7902-5(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '226.104') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.303(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.303(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7009-1') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7009-2') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7009-2(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7009-2(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7009-3(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7009-3(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7009-4(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7009-4(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7009-4(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7009-4(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7009-4(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7010') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7012') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7102-4(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7102-4(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7103-17') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7103-3(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7103-6(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7103-6(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7103-6(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7103-6(e)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7103-6(e)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7103-6(e)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7103-8(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7103-8(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7104(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7104(e)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7104(e)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7104(e)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7104(e)(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7104(e)(5)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7104(f)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7104(f)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7105-2(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7105-3') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7106(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7107-1(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7107-1(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7107-1(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7107-3') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7203-3(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7203-6(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7203-6(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7203-6(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7203-6(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7203-6(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7203-6(f)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7205(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '228.170') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '228.370(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '228.370(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '228.370(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '228.370(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '228.370(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '229.170-4') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '229.402-1') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '229.402-70(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '229.402-70(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '229.402-70(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '229.402-70(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '229.402-70(c)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '229.402-70(c)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '229.402-70(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '229.402-70(e)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '229.402-70(e)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '229.402-70(f)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '229.402-70(g)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '229.402-70(h)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '229.402-70(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '229.402-70(j)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '23.1005') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '23.1005(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '23.1005(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '23.1105') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '23.206') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '23.303') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '23.303(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '23.406(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '23.406(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '23.406(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '23.406(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '23.406(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '23.505') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '23.602') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '23.705(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '23.705(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '23.705(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '23.705(c)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '23.705(d)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '23.705(d)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '23.804(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '23.804(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '23.903') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '231.100-70') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '232.1005-70(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '232.1005-70(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '232.1110') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '232.412-70(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '232.412-70(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '232.412-70(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '232.502-4(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '232.502-4-70(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '232.7004(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '232.7004(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '232.705-70') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '232.7102') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '232.7202') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '232.806(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '232.908') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '233.215-70') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '234.203(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '234.203(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '234.7101(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '234.7101(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '234.7101(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '234.7101(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '234.7101(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '235.070-3') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '235.072') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '235.072(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '235.072(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '235.072(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '235.072(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '235.072(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '235.072(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '236.570(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '236.570(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '236.570(b)(2)(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '236.570(b)(2)(ii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '236.570(b)(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '236.570(b)(5)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '236.570(b)(6)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '236.570(c)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '236.570(c)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '236.570(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '236.609-70') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '236.609-70(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '237-7003(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '237.171-4') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '237.173-5') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '237.270(d)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '237.270(d)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '237.7003(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '237.7003(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '237.7003(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '237.7003(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '237.7101(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '237.7101(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '237.7101(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '237.7101(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '237.7101(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '237.7101(e)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '237.7101(e)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '237.7101(e)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '237.7101(f)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '237.7101(g)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '237.7402') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '237.7603(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '239.7103(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '239.7103(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '239.7306(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '239.7306(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '239.7411(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '239.7411(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '239.7411(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '239.7411(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '239.7603(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '239.7603(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '239.7604(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '239.7604(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '24.104(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '24.104(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '241.501-70(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '241.501-70(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '242.7001') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '242.7204') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '242.7503') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '243.205-70') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '243.205-71') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '244.305-71') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '244.305-71(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '244.305-71(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '244.403') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '245.107(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '245.107(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '245.107(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '245.107(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '245.107(5)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '246.270-4') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '246.370') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '246.371(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '246.710(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '246.710(1)(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '246.710(1)(ii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '246.710(1)(iii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '246.710(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '246.710(3)(i)(A)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '246.710(3)(i)(B)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '246.710(3)(i)(B)(iii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '246.870-3') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.207') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.207(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.270-4') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.270-4(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.270-4(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.270-4(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.270-4(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.270-4(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.270-4(f)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.270-4(g)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.271-3') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.271-3(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.271-3(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.271-3(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.271-3(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.271-3(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.271-3(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.271-3(g)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.271-3(h)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.271-3(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.271-3(j)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.271-3(k)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.271-3(l)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.271-3(m)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.271-3(n)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.305-70') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.574(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.574(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.574(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.574(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.574(b)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.574(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.574(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.574(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.574(f)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '249.501-70') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '249.7003(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.110') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1101(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1101(1)(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1101(1)(ii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1101(6)(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1101(6)(ii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1101(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1101(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1101(b)(1)(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1101(b)(1)(ii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1101(b)(1)(iii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1101(b)(1)(iv)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1101(b)(2)(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1101(b)(2)(ii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1101(b)(2)(iii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1101(b)(2)(iv)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1101(c)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1101(c)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1101(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1101(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1101(f)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1102(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1102(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1102(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1102(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1102(c)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1102(d)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1102(d)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1102(d)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1102(e)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1103') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1103(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1103(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1103(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1103(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.301-4') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.302-6') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '251.107') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '251.205') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '26.104') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '26.206(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '26.206(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '26.206(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '26.304') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '26.404') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.201-2') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.201-2(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.201-2(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.201-2(a)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.201-2(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.201-2(c)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.201-2(c)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.201-2(c)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.201-2(d)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.201-2(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.202-5(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.202-5(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.202-5(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.202-5(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.203-2') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.303(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.303(b)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.303(b)(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.303(b)(5)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.303(b)(6)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.303(b)(7)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.303(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.303(e)(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.303(e)(5)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.409(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.409(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.409(b)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.409(b)(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.409(b)(5)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.409(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.409(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.409(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.409(f)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.409(g)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.409(h)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.409(j)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.409(k)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.409(l)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '28.101-2') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '28.103-4') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '28.106-4(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '28.106-4(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '28.1102-3(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '28.1102-3(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '28.203-6') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '28.204-4') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '28.309(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '28.309(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '28.310') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '28.311-1') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '28.312') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '28.313(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '28.313(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '29.401-1') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '29.401-2') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '29.401-3') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '29.401-3(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '29.401-3(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '29.401-4(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '29.402-1(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '29.402-1(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '29.402-2(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '29.402-2(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '3.1004(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '3.1004(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '3.103-1') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '3.104-9(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '3.104-9(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '3.1106') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '3.202') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '3.301-1') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '3.404') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '3.502-3') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '3.503-2') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '3.808(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '3.808(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '3.907-7') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '3.908-9') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '30.201-3') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '30.201-3(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '30.201-3(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '30.201-4(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '30.201-4(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '30.201-4(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '30.201-4(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '30.201-4(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '30.202') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.009-2') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.1005') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.1005(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.1005(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.111(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.111(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.111(a)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.111(a)(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.111(a)(5)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.111(a)(6)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.111(a)(7)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.111(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.111(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.111(c)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.111(c)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.1110(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.1110(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.1110(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.1110(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.1110(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.1110(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.1110(g)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.205(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.206') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.206(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.206(g)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.412(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.412(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.412(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.412(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.412(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.412(f)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.502-3(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.502-3(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.502-3(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.502-4(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.502-4(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.502-4(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.502-4(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.611(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.611(a)&(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.611(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.706-1(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.706-1(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.706-2(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.706-2(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.706-3') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.806(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.806(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.806(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.908(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.908(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.908(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.908(c)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '33.106(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '33.106(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '33.215(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '33.215(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '34.104') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '34.203(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '34.203(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '34.203(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.501(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.502') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.503') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.504') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.505') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.506') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.507') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.508') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.509') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.510') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.511') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.512') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.513') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.514') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.515') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.516') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.517') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.518') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.519') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.520') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.521') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.522') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.523') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.609-1(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.609-2(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.609-3') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.609-4') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '37.110(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '37.110(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '37.110(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '37.113-2(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '37.113-2(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '37.115-3') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '37.116-2') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '37.304(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '37.304(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '37.304(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '37.403') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '39.106') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '4.103') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '4.1105(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '4.1105(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '4.1202(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '4.1202(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '4.1303') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '4.1403(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '4.1705(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '4.1705(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '4.1804(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '4.1804(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '4.1804(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '4.303') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '4.404(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '4.607(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '4.607(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '4.905') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '41.501(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '41.501(c)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '41.501(c)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '41.501(c)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '41.501(c)(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '41.501(c)(5)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '41.501(d)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '41.501(d)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '41.501(d)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '41.501(d)(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '41.501(d)(5)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '41.501(d)(6)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '41.501(d)(7)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '42.1107(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '42.1305(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '42.1305(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '42.1305(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '42.1305(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '42.703-2(f)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '42.709-6') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '42.802') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '42.903') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '43.107') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '43.205(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '43.205(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '43.205(a)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '43.205(a)(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '43.205(a)(5)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '43.205(a)(6)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '43.205(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '43.205(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '43.205(b)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '43.205(b)(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '43.205(b)(6)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '43.205(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '43.205(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '43.205(f)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '44.204(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '44.204(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '44.204(a)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '44.204(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '44.204(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '44.403') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '45.107(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '45.107(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '45.107(a)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '45.107(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '45.107(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.301') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.302') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.303') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.304') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.305') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.306') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.307(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.308') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.309') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.311') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.312') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.313') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.314') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.315') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.316') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.710(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.710(a)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.710(a)(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.710(a)(5)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.710(a)(6)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.710(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.710(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.710(b)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.710(b)(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.710(c)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.710(c)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.710(c)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.710(c)(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.710(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.710(e)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.710(e)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.805') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.805(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.805(a)(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.103-2') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.104-4') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-1(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-1(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-1(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-1(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-1(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-1(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-3(d)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-3(e)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-4(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-4(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-4(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-5(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-5(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-5(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-5(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-5(f)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-6(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-6(c)(5)(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-6(c)(5)(ii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-6(c)(6)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-7(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-7(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-7(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-8(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-8(a)(2)(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-8(a)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-8(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-9(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.208-2') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.303-1(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.303-10(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.303-11(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.303-12(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.303-13(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.303-14(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.303-15(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.303-16(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.303-17(f)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.303-2(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.303-3(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.303-4(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.303-5(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.303-6(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.303-7(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.303-8(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.303-9(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.304-7(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.305-12(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.305-13(a)(3)(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.305-14(b)(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.305-15(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.305-16(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.305-16(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.305-16(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.305-16(d)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.305-17') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.305-2(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.305-3(b)(4)(ii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.305-3(f)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.305-4(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.305-5(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.305-5(c)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.305-6(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.305-6(e)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.305-6(e)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.305-6(e)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.305-6(f)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.305-9(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.405') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.507(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.507(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.507(a)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '48.201') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '48.201(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '48.201(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '48.201(e)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '48.201(f)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '48.202') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '49.502(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '49.502(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '49.502(b)(1)(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '49.502(b)(1)(ii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '49.502(b)(1)(iii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '49.502(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '49.502(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '49.502(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '49.503(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '49.503(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '49.503(a)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '49.503(a)(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '49.503(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '49.504(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '49.504(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '49.504(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '49.504(c)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '49.504(c)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '49.504(c)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '49.505(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '49.505(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '50.104-4') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '50.206(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '50.206(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '50.206(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '50.206(b)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '50.206(c)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '50.206(c)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '50.206(c)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '50.206(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '50.403-3') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '51.107') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '51.205') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '52.107(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '52.107(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '52.107(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '52.107(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '52.107(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '52.107(f)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '53.111') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '7.203') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '7.305(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '7.305(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '7.305(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '7.404') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '8.005') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '8.1104(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '8.1104(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '8.1104(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '8.1104(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '8.505') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '9.104-7(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '9.104-7(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '9.104-7(c)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '9.108-5(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '9.108-5(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '9.206-2') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '9.308-1(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '9.308-1(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '9.308-1(a)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '9.308-1(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '9.308-1(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '9.308-1(b)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '9.308-2(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '9.308-2(a)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '9.308-2(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '9.308-2(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '9.308-2(b)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '9.409') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, 'NOTAPPLICABLE') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, 'PGI 204.7108(d)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, 'PGI 204.7108(d)(10)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, 'PGI 204.7108(d)(11)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, 'PGI 204.7108(d)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, 'PGI 204.7108(d)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, 'PGI 204.7108(d)(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, 'PGI 204.7108(d)(5)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, 'PGI 204.7108(d)(6)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, 'PGI 204.7108(d)(7)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, 'PGI 204.7108(d)(8)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, 'PGI 204.7108(d)(9)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;

UPDATE Prescriptions
JOIN (
	SELECT
		prescription_id,
		concat('http://www.ecfr.gov/cgi-bin/text-idx?node=se48.', volume , '.', part, '_1', subpart, replace(suffix, '-', '_6')) uri
	FROM (
		SELECT
			prescription_id,
			prescription_name,
			subpart,
			part,
			case when suffix like '%(%' then left(suffix,locate('(',suffix)-1) else suffix end suffix,
			case when part regexp '^[0-9]+$'
				then
					case
						when cast(part as UNSIGNED) >= '1' and cast(part as UNSIGNED) <= '51' then 1
						when cast(part as UNSIGNED) >= '52' and cast(part as UNSIGNED) <= '99' then 2
						when cast(part as UNSIGNED) >= '200' and cast(part as UNSIGNED) <= '299' then 3
					end
			end volume
		FROM (
			SELECT
				prescription_id,
				prescription_name,
				substring_index(substring_index(prescription_name,'(', 1),'.',1) part,
				substring_index(substring_index(substring_index(prescription_name,'(', 1),'.',-1),'-',1) subpart,
				case
					when locate('-',prescription_name) != 0
						then right(prescription_name, length(prescription_name) - locate('-',prescription_name)+1)
					else ''
				end suffix
			FROM Prescriptions
		) Prescriptions
	) Prescriptions
) sub ON (sub.prescription_id = Prescriptions.prescription_id)
SET Prescriptions.prescription_url = ifNull(sub.uri,'')
WHERE ifNull(prescription_url, '') = '';

/* ===============================================
 * Questions
   =============================================== */


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT Question_Choce_Id, prescription_id FROM Question_Choices, Prescriptions
  WHERE prescription_name IN ('2.101') 
  AND choice_text = 'FOR USE IN DEFENDING AGAINST OR RECOVERY FROM NUCLEAR, BIOLOGICAL, CHEMICAL, OR RADIOLOGICAL ATTACK' AND  question_id IN (SELECT question_id FROM Questions WHERE clause_version_id = 14 AND question_code = '[PURPOSE OF PROCUREMENT]'); -- [PURPOSE OF PROCUREMENT] 'FOR USE IN DEFENDING AGAINST OR RECOVERY FROM NUCLEAR, BIOLOGICAL, CHEMICAL, OR RADIOLOGICAL ATTACK'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT Question_Choce_Id, prescription_id FROM Question_Choices, Prescriptions
  WHERE prescription_name IN ('225.7017-5(a)(1)') 
  AND choice_text = 'CONTRACTOR REQUIRED TO INSTALL A PHOTVOLTAIC DEVICE IN THE UNITED STATES OR IN A FACILITY OWNED BY DOD' AND  question_id IN (SELECT question_id FROM Questions WHERE clause_version_id = 14 AND question_code = '[ENVIRONMENT]'); -- [ENVIRONMENT] 'CONTRACTOR REQUIRED TO INSTALL A PHOTVOLTAIC DEVICE IN THE UNITED STATES OR IN A FACILITY OWNED BY DOD'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT Question_Choce_Id, prescription_id FROM Question_Choices, Prescriptions
  WHERE prescription_name IN ('225.7017-5(a)(1)') 
  AND choice_text = 'CONTRACTOR REQUIRED TO RESERVE A PHOTOVOLTAIC DEVICE FOR THE EXCLUSIVE USE OF DOD IN THE UNITED STATES FOR THE FULL ECONOMIC LIFE OF THE DEVICE' AND  question_id IN (SELECT question_id FROM Questions WHERE clause_version_id = 14 AND question_code = '[ENVIRONMENT]'); -- [ENVIRONMENT] 'CONTRACTOR REQUIRED TO RESERVE A PHOTOVOLTAIC DEVICE FOR THE EXCLUSIVE USE OF DOD IN THE UNITED STATES FOR THE FULL ECONOMIC LIFE OF THE DEVICE'

UPDATE Questions
SET question_type = 'M' -- 1
WHERE question_code = '[REQUIRING ACTIVITY DOD]' AND clause_version_id = 14; -- [REQUIRING ACTIVITY DOD]

UPDATE Questions
SET question_type = 'M' -- 1
WHERE question_code = '[REQUIRING ACTIVITY CIVILIAN]' AND clause_version_id = 14; -- [REQUIRING ACTIVITY CIVILIAN]
/*
<h2>52.211-7</h2>
No identifier
<pre>(ADDITIONAL DIRECTIONS: IF THE REQUIRING ACTIVITY IS: DEPARTMENT OF DEFENSE DO NOT USE THIS CLAUSE</pre>
*//*
<h2>52.212-5</h2>
(K) Clause name not found: {52.212-5 ALTERNATE III}
<pre>(K) 52.212-5 ALTERNATE III IS: INCLUDED</pre>
*//*
<h2>52.226-6</h2>
(F) No " IS:" found
<pre>(F) FOR USE IN UNITED STATES (50 STATES PLUS DISTRICT OF COLUMBIA)</pre>
*//*
<h2>52.237-6</h2>
(D) [DISMANTALLING PAYMENT ARRANGEMENT] Question Name not found
<pre>(D) DISMANTALLING PAYMENT ARRANGEMENT IS: CONTRACTOR IS TO RECEIVE TITLE TO DISMANTLED OR DEMOLISHED PROPERTY AND A NET AMOUNT OF COMENSATION IS DUE TO THE GOVERNMENT</pre><br />
(G) Clause name not found: {52.237-6 ALTERNATE I}
<pre>(G) 52.237-6 ALTERNATE I IS: INCLUDED</pre>
*//*
<h2>52.243-2</h2>
(I) Clause name not found: {52.243-2 ALTERNATE IV}
<pre>(I) 52.243-2 ALTERNATE IV IS: INCLUDED</pre>
*//*
<h2>52.247-5</h2>
(C) [TRANSPORTATION REQUIREMENTS] Question Name not found
<pre>(C) TRANSPORTATION REQUIREMENTS IS: CONTRACTOR RESPONSIBLE FOR FAMILIARIZATION WITH CONDITIONS UNDER WHICH AND WHERE THE SERVICES ARE TO BE PERFORMED</pre>
*/
-- ProduceClause.resolveIssues() Summary: Total(1234); Merged(20); Corrected(351); Error(7)
/* ===============================================
 * Clause
   =============================================== */

UPDATE Clauses
SET clause_title = 'Contracting Officer''s Representative.' -- 'Contracting officer''s representative.'
WHERE clause_version_id = 14 AND clause_name = '252.201-7000';

UPDATE Clauses
SET clause_title = 'Requirements Relating to Compensation of Former DOD Officials.' -- 'Requirements Relating to Compensation of Former DoD Officials.'
WHERE clause_version_id = 14 AND clause_name = '252.203-7000';

UPDATE Clauses
SET clause_title = 'Prohibition on Persons Convicted of Fraud or Other Defense Contract-Related Felonies.' -- 'Prohibition on persons convicted of fraud or other defense-contract-related felonies.'
WHERE clause_version_id = 14 AND clause_name = '252.203-7001';

UPDATE Clauses
SET clause_title = 'Display of Hotline Poster.' -- 'Display of Hotline Posters.'
WHERE clause_version_id = 14 AND clause_name = '252.203-7004';

UPDATE Clauses
SET clause_title = 'Representation Relating to Compensation of Former DOD Officials.' -- 'Representation Relating to Compensation of Former DoD Officials.'
WHERE clause_version_id = 14 AND clause_name = '252.203-7005';

UPDATE Clauses
SET clause_title = 'Prohibition on Contracting With Entities That Require Certain Internal Confidentiality Agreements-Representation.' -- 'Prohibition On Contracting With Entities That Require Certain Internal Confidentiality Agreements-Representation'
WHERE clause_version_id = 14 AND clause_name = '252.203-7996 (DEVIATION 2016-00003)';

UPDATE Clauses
SET clause_title = 'Prohibition on Contracting With Entities That Require Certain Internal Confidentiality Agreements.' -- 'Prohibition On Contracting With Entities That Require Certain Internal Confidentiality Agreements'
WHERE clause_version_id = 14 AND clause_name = '252.203-7997 (DEVIATION 2016-00003)';

UPDATE Clauses
SET clause_title = 'Prohibition on Contracting With Entities That Require Certain Internal Confidentiality Agreements - Representation.' -- 'Prohibition on Contracting with Entities that Require Certain Internal Confidentiality Agreements- Representation.'
WHERE clause_version_id = 14 AND clause_name = '252.203-7998 (DEVIATION 2015-00010)';

UPDATE Clauses
SET clause_title = 'Prohibition on Contracting With Entities That Require Certain Internal Confidentiality Agreements.' -- 'Prohibition on Contracting with Entities that Require Certain Internal Confidentiality Agreements.'
WHERE clause_version_id = 14 AND clause_name = '252.203-7999 (DEVIATION 2015-00010)';

UPDATE Clauses
SET clause_title = 'Line Item Specific: Sequential Acrn Order.' -- 'Line Item Specific: Sequential ACRN Order.'
WHERE clause_version_id = 14 AND clause_name = '252.204-0002';

UPDATE Clauses
SET clause_title = 'Line Item Specific: Contracting Officer Specified Acrn Order.' -- 'Line Item Specific: Contracting Officer Specified ACRN Order.'
WHERE clause_version_id = 14 AND clause_name = '252.204-0003';

UPDATE Clauses
SET clause_title = 'Line Item Specific: by Cancellation Date.' -- 'Line Item Specific: by Cancellation Date'
WHERE clause_version_id = 14 AND clause_name = '252.204-0005';

UPDATE Clauses
SET clause_title = 'Contract-Wide: Sequential Acrn Order.' -- 'Contract-wide: Sequential ACRN Order.'
WHERE clause_version_id = 14 AND clause_name = '252.204-0007';

UPDATE Clauses
SET clause_title = 'Contract-Wide: Contracting Officer Specified Acrn Order.' -- 'Contract-wide: Contracting Officer Specified ACRN Order.'
WHERE clause_version_id = 14 AND clause_name = '252.204-0008';

UPDATE Clauses
SET clause_title = 'Contract-Wide: by Fiscal Year.' -- 'Contract-wide: by Fiscal Year.'
WHERE clause_version_id = 14 AND clause_name = '252.204-0009';

UPDATE Clauses
SET clause_title = 'Contract-Wide: by Cancellation Date.' -- 'Contract-wide: by Cancellation Date'
WHERE clause_version_id = 14 AND clause_name = '252.204-0010';

UPDATE Clauses
SET clause_title = 'Contract-Wide: Proration.' -- 'Contract-wide: Proration.'
WHERE clause_version_id = 14 AND clause_name = '252.204-0011';

UPDATE Clauses
SET clause_title = 'Disclosure of Information.' -- 'Disclosure of information.'
WHERE clause_version_id = 14 AND clause_name = '252.204-7000';

UPDATE Clauses
SET clause_title = 'Payment for Subline Items Not Separately Priced.' -- 'Payment for subline items not separately priced.'
WHERE clause_version_id = 14 AND clause_name = '252.204-7002';

UPDATE Clauses
SET clause_title = 'Control of Government Personnel Work Product.' -- 'Control of government personnel work product.'
WHERE clause_version_id = 14 AND clause_name = '252.204-7003';

UPDATE Clauses
SET clause_title = 'Alter A, System for Award Management.' -- 'Alternate A, System for Award Management.'
WHERE clause_version_id = 14 AND clause_name = '252.204-7004';

UPDATE Clauses
SET clause_title = 'Oral Attestation of Security Responsibilities.' -- 'Oral attestation of security responsibilities.'
WHERE clause_version_id = 14 AND clause_name = '252.204-7005';

UPDATE Clauses
SET clause_title = 'Billing Instructions.' -- 'Billing instructions.'
WHERE clause_version_id = 14 AND clause_name = '252.204-7006';

UPDATE Clauses
SET clause_title = 'Compliance With Safeguarding Covered Defense Information Controls.' -- 'Compliance with safeguarding covered defense information controls.'
WHERE clause_version_id = 14 AND clause_name = '252.204-7008';

UPDATE Clauses
SET clause_title = 'Compliance With Safeguarding Covered Defense Information Controls.' -- 'Compliance with Safeguarding Covered Defense Information Controls'
WHERE clause_version_id = 14 AND clause_name = '252.204-7008 (DEVIATION 2016-00001)';

UPDATE Clauses
SET clause_title = 'Limitations on the Use or Disclosure of Third-Party Contractor Information.' -- 'Limitations on the use or disclosure of third-party contractor reported cyber incident information.'
WHERE clause_version_id = 14 AND clause_name = '252.204-7009';

UPDATE Clauses
SET clause_title = 'Requirement for Contractor to Notify DOD If the ContractorS Activities Are Subject to Reporting Under the U.S.-International Atomic Energy Agency Additional Protocol.' -- 'Requirement for Contractor To Notify DoD if the Contractor''s Activities are Subject to Reporting Under the U.S.-International Atomic Energy Agency Additional Protocol.'
WHERE clause_version_id = 14 AND clause_name = '252.204-7010';

UPDATE Clauses
SET clause_title = 'Safeguarding Covered Defense Information and Cyber Incident Reporting.' -- 'Safeguarding covered defense information and cyber incident reporting.'
WHERE clause_version_id = 14 AND clause_name = '252.204-7012';

UPDATE Clauses
SET clause_title = 'Safeguarding Covered Defense Information and Cyber Incident Reporting.' -- 'Safeguarding Covered Defense Information and Cyber Incident Reporting'
WHERE clause_version_id = 14 AND clause_name = '252.204-7012 (DEVIATION 2016-00001)';

UPDATE Clauses
SET clause_title = 'Provision of Information to Cooperative Agreement Holders.' -- 'Provision of information to cooperative agreement holders.'
WHERE clause_version_id = 14 AND clause_name = '252.205-7000';

UPDATE Clauses
SET clause_title = 'Domestic Source Restriction.' -- 'Domestic source restriction.'
WHERE clause_version_id = 14 AND clause_name = '252.206-7000';

UPDATE Clauses
SET clause_title = 'Intent to Furnish Precious Metals As Government-Furnished Material.' -- 'Intent to furnish precious metals as Government-furnished material.'
WHERE clause_version_id = 14 AND clause_name = '252.208-7000';

UPDATE Clauses
SET clause_title = 'Disclosure of Ownership or Control by a Foreign Government.' -- 'Disclosure of ownership or control by a foreign government.'
WHERE clause_version_id = 14 AND clause_name = '252.209-7002';

UPDATE Clauses
SET clause_title = 'Subcontracting With Firms That Are Owned or Controlled by the Government of a Country That Is a State Sponsor of Terrorism.' -- 'Subcontracting with Firms that are Owned or Controlled by the Government of a Country that is a State Sponsor of Terrorism.'
WHERE clause_version_id = 14 AND clause_name = '252.209-7004';

UPDATE Clauses
SET clause_title = 'Reserve Officer Training Corps and Military Recruiting on Campus.' -- 'Reserve Officer Training Corps and military recruiting on campus.'
WHERE clause_version_id = 14 AND clause_name = '252.209-7005';

UPDATE Clauses
SET clause_title = 'Limitations on Offerors Acting As Lead System Integrators.' -- 'Limitations on Contractors Acting as Lead System Integrators.'
WHERE clause_version_id = 14 AND clause_name = '252.209-7006';

UPDATE Clauses
SET clause_title = 'Organizational Conflict of Interest-Major Defense Acquisition Program.' -- 'Critical Safety Items.'
WHERE clause_version_id = 14 AND clause_name = '252.209-7010';

UPDATE Clauses
SET clause_title = 'Representation by Corporations Regarding An Unpaid Delinquent Tax Liability or a Felony Conviction Under Any Federal Law-Fiscal Year 2016 Appropriations.' -- 'Representation by Corporations Regarding an Unpaid Delinquent Tax Liability or a Felony Conviction under any Federal Law- Fiscal Year 2016 Appropriations'
WHERE clause_version_id = 14 AND clause_name = '252.209-7991 (DEVIATION 2016-00002)';

UPDATE Clauses
SET clause_title = 'Representation by Corporations Regarding An Unpaid Delinquent Tax Liability or a Felony Conviction Under Any Federal Law-Fiscal Year 2015 Appropriations.' -- 'Representation by Corporations Regarding an Unpaid Delinquent Tax Liability or a Felony Conviction under any Federal Law-Fiscal Year 2015 Appropriations'
WHERE clause_version_id = 14 AND clause_name = '252.209-7992 (DEVIATION 2015-00005)';

UPDATE Clauses
SET clause_title = 'Representation by Corporations Regarding An Unpaid Delinquent Tax Liability or a Felony Conviction Under Any Federal Law-Fiscal Year 2014 Appropriations.' -- 'Representation by Corporations Regarding an Unpaid Delinquent Tax Liability or a Felony Conviction under any Federal Law - Fiscal Year 2014 Appropriations.'
WHERE clause_version_id = 14 AND clause_name = '252.209-7993 (DEVIATION 2014-00009)';

UPDATE Clauses
SET clause_title = 'Representation by Corporations Regarding An Unpaid Delinquent Tax Liability or a Felony Conviction Under Any Federal Law-Fiscal Year 2014 Appropriations.' -- 'Representation by Corporations Regarding an Unpaid Delinquent Tax Liability or a Felony Conviction under any Federal Law-Fiscal Year 2014 Appropriations'
WHERE clause_version_id = 14 AND clause_name = '252.209-7994 (DEVIATION 2014-00004)';

UPDATE Clauses
SET clause_title = 'Representation by Corporations Regarding An Unpaid Delinquent Tax Liability or a Felony Conviction Under Any Federal Law-DOD Military Construction Appropriations.' -- 'Representation by Corporations Regarding an Unpaid Delinquent Tax Liability or a Felony Conviction under any Federal Law-DoD Military Construction Appropriations'
WHERE clause_version_id = 14 AND clause_name = '252.209-7996 (DEVIATION 2013-00006)';

UPDATE Clauses
SET clause_title = 'Representation by Corporations Regarding An Unpaid Delinquent Tax Liability or a Felony Conviction Under Any Federal Law-DOD Appropriations.' -- 'Representation by Corporations Regarding an Unpaid Delinquent Tax Liability or a Felony Conviction under any Federal Law-DoD Appropriations'
WHERE clause_version_id = 14 AND clause_name = '252.209-7997 (DEVIATION 2013-00006)';

UPDATE Clauses
SET clause_title = 'Representation Regarding Conviction of a Felony Criminal.' -- 'Representation Regarding Conviction of a Felony Criminal Violation under any Federal or State Law'
WHERE clause_version_id = 14 AND clause_name = '252.209-7998 (DEVIATION 2012-00007)';

UPDATE Clauses
SET clause_title = 'Representation by Corporations Regarding An Unpaid Delinquent Tax Liability or a Felony Conviction Under Any Federal Law.' -- 'Representation by Corporations Regarding an Unpaid Delinquent Tax Liability or a Felony Conviction under any Federal Law'
WHERE clause_version_id = 14 AND clause_name = '252.209-7999 (DEVIATION 2012-00004)';

UPDATE Clauses
SET clause_title = 'Acquisition Streamlining.' -- 'Acquisition streamlining.'
WHERE clause_version_id = 14 AND clause_name = '252.211-7000';

UPDATE Clauses
SET clause_title = 'Availability of Specifications, Standards, and Data Item Descriptions Not Listed in the Acquisition Streamlining and Standardization Information System (ASSIST), and Plans, Drawings, and Other Pertinent Documents.' -- 'Availability of Specifications, Standards, and Data Item Descriptions Not Listed in the Acquisition Streamlining and Standardization Information System (ASSIST), and Plans, Drawings, and Other Pertinent Documents. Availability of specifications and standards Not listed in DoDISS, data item descriptions Not listed in DoD 5010.12-L, and plans, drawings, and other pertinent documents.'
WHERE clause_version_id = 14 AND clause_name = '252.211-7001';

UPDATE Clauses
SET clause_title = 'Availability for Examination of Specifications, Standards, Plans, Drawings, Data Item Descriptions, and Other Pertinent Documents.' -- 'Availability for examination of specifications, standards, plans, drawings, data item descriptions, and other pertinent documents.'
WHERE clause_version_id = 14 AND clause_name = '252.211-7002';

UPDATE Clauses
SET clause_title = 'Item Unique Identification and Valuation.' -- 'Item unique identification and valuation.'
WHERE clause_version_id = 14 AND clause_name = '252.211-7003';

UPDATE Clauses
SET clause_title = 'Alter Preservation, Packaging, and Packing.' -- 'Alternate preservation, packaging, and packing.'
WHERE clause_version_id = 14 AND clause_name = '252.211-7004';

UPDATE Clauses
SET clause_title = 'Substitutions for Military or Federal Specifications and Standards.' -- 'Substitutions for military or Federal specifications and standards.'
WHERE clause_version_id = 14 AND clause_name = '252.211-7005';

UPDATE Clauses
SET clause_title = 'Use of Government-Assigned Serial Numbers.' -- 'Use of Government-assigned Serial Numbers'
WHERE clause_version_id = 14 AND clause_name = '252.211-7008';

UPDATE Clauses
SET clause_title = 'Pilot Program for Acquisition of Military-Purpose Nondevelopmental Items.' -- 'Pilot Program for Acquisition of Military-purpose Nondevelopmental Items.'
WHERE clause_version_id = 14 AND clause_name = '252.212-7002';

UPDATE Clauses
SET clause_title = 'Pricing Adjustments.' -- 'Pricing adjustments.'
WHERE clause_version_id = 14 AND clause_name = '252.215-7000';

UPDATE Clauses
SET clause_title = 'Cost Estimating System Requirements.' -- 'Cost estimating system requirements.'
WHERE clause_version_id = 14 AND clause_name = '252.215-7002';

UPDATE Clauses
SET clause_title = 'Proposal Adequacy Checklist.' -- 'Proposal adequacy checklist.'
WHERE clause_version_id = 14 AND clause_name = '252.215-7009';

UPDATE Clauses
SET clause_title = 'Economic Price Adjustment-Basic Steel, Aluminum, Brass, Bronze, or Copper Mill Products.' -- 'Economic price adjustment-basic steel, aluminum, brass, bronze, or copper mill products.'
WHERE clause_version_id = 14 AND clause_name = '252.216-7000';

UPDATE Clauses
SET clause_title = 'Economic Price AdjustmentNonstandard Steel Items.' -- 'Economic price adjustment-nonstandard steel items.'
WHERE clause_version_id = 14 AND clause_name = '252.216-7001';

UPDATE Clauses
SET clause_title = 'Alter A, Time-And-Materials/Labor-Hour Proposal Requirements Non-Commercial Item Acquisition With Adequate Price Competition.' -- 'Alternate A, Time-and-Materials/Labor-Hour Proposal Requirements-Non-Commercial Item Acquisition With Adequate Price Competition.'
WHERE clause_version_id = 14 AND clause_name = '252.216-7002';

UPDATE Clauses
SET clause_title = 'Economic Price Adjustment-Wage Rates or Material Prices Controlled by a Foreign Government.' -- 'Economic price adjustment-wage rates or material prices controlled by a foreign government.'
WHERE clause_version_id = 14 AND clause_name = '252.216-7003';

UPDATE Clauses
SET clause_title = 'Economic Price Adjustment-Basic Steel, Aluminum, Brass, Bronze, or Copper Mill Products-Representation.' -- 'Economic price adjustment-basic steel, aluminum, brass, bronze, or copper mill products-representation.'
WHERE clause_version_id = 14 AND clause_name = '252.216-7007';

UPDATE Clauses
SET clause_title = 'Economic Price Adjustment-Wage Rates or Material Prices Controlled by a Foreign Government-Representation.' -- 'Economic price adjustment-wage rates or material prices controlled by a foreign government-representation.'
WHERE clause_version_id = 14 AND clause_name = '252.216-7008';

UPDATE Clauses
SET clause_title = 'Allowability of Legal Costs Incurred in Connection With a Whistleblower Proceeding.' -- 'Allowability of legal costs incurred in connection with a whistleblower proceeding.'
WHERE clause_version_id = 14 AND clause_name = '252.216-7009';

UPDATE Clauses
SET clause_title = 'Exercise of Option to Fulfill Foreign Military Sales Commitments.' -- 'Exercise of option to fulfill foreign military sales commitments.'
WHERE clause_version_id = 14 AND clause_name = '252.217-7000';

UPDATE Clauses
SET clause_title = 'Surge Option.' -- 'Surge option.'
WHERE clause_version_id = 14 AND clause_name = '252.217-7001';

UPDATE Clauses
SET clause_title = 'Offering Property for Exchange.' -- 'Offering property for exchange.'
WHERE clause_version_id = 14 AND clause_name = '252.217-7002';

UPDATE Clauses
SET clause_title = 'Job Orders and Compensation.' -- 'Job orders and compensation.'
WHERE clause_version_id = 14 AND clause_name = '252.217-7004';

UPDATE Clauses
SET clause_title = 'Inspection and Manner of Doing Work.' -- 'Inspection and manner of doing work.'
WHERE clause_version_id = 14 AND clause_name = '252.217-7005';

UPDATE Clauses
SET clause_title = 'Access to Vessel.' -- 'Access to vessel.'
WHERE clause_version_id = 14 AND clause_name = '252.217-7011';

UPDATE Clauses
SET clause_title = 'Liability and Insurance.' -- 'Liability and insurance.'
WHERE clause_version_id = 14 AND clause_name = '252.217-7012';

UPDATE Clauses
SET clause_title = 'Discharge of Liens.' -- 'Discharge of liens.'
WHERE clause_version_id = 14 AND clause_name = '252.217-7014';

UPDATE Clauses
SET clause_title = 'Safety and Health.' -- 'Safety and health.'
WHERE clause_version_id = 14 AND clause_name = '252.217-7015';

UPDATE Clauses
SET clause_title = 'Plant Protection.' -- 'Plant protection.'
WHERE clause_version_id = 14 AND clause_name = '252.217-7016';

UPDATE Clauses
SET clause_title = 'Identification of Sources of Supply.' -- 'Identification of sources of supply.'
WHERE clause_version_id = 14 AND clause_name = '252.217-7026';

UPDATE Clauses
SET clause_title = 'Contract Definitization.' -- 'Contract definitization.'
WHERE clause_version_id = 14 AND clause_name = '252.217-7027';

UPDATE Clauses
SET clause_title = 'Over and Above Work.' -- 'Over and above work.'
WHERE clause_version_id = 14 AND clause_name = '252.217-7028';

UPDATE Clauses
SET clause_title = 'Small Business Subcontracting Plan (DOD Contracts).' -- 'Small business subcontracting plan (DoD contracts).'
WHERE clause_version_id = 14 AND clause_name = '252.219-7003';

UPDATE Clauses
SET clause_title = 'Small Business Subcontracting Plan (DOD Contracts).' -- 'SMALL BUSINESS SUBCONTRACTING PLAN (DOD CONTRACTS) (DEVIATION 2013-00014)'
WHERE clause_version_id = 14 AND clause_name = '252.219-7003 (DEVIATION 2013-00014)';

UPDATE Clauses
SET clause_title = 'Small Business Subcontracting Plan (Test Program).' -- 'Small business subcontracting plan (test program).'
WHERE clause_version_id = 14 AND clause_name = '252.219-7004';

UPDATE Clauses
SET clause_title = 'Section 8(a) Direct Award.' -- 'Section 8(a) direct award.'
WHERE clause_version_id = 14 AND clause_name = '252.219-7009';

UPDATE Clauses
SET clause_title = 'Notification to Delay Performance.' -- 'Notification to delay performance.'
WHERE clause_version_id = 14 AND clause_name = '252.219-7011';

UPDATE Clauses
SET clause_title = 'Restrictions on Employment of Personnel.' -- 'Restrictions on employment of personnel.'
WHERE clause_version_id = 14 AND clause_name = '252.222-7000';

UPDATE Clauses
SET clause_title = 'Right of First Refusal of Employment--Closure of Military Installations.' -- 'Right of first refusal of employment-Closure of military installations.'
WHERE clause_version_id = 14 AND clause_name = '252.222-7001';

UPDATE Clauses
SET clause_title = 'Compliance With Local Labor Laws (Overseas).' -- 'Compliance with local labor laws (overseas).'
WHERE clause_version_id = 14 AND clause_name = '252.222-7002';

UPDATE Clauses
SET clause_title = 'Permit From Italian Inspectorate of Labor.' -- 'Permit from Italian Inspectorate of Labor.'
WHERE clause_version_id = 14 AND clause_name = '252.222-7003';

UPDATE Clauses
SET clause_title = 'Compliance With Spanish Social Security Laws and Regulations.' -- 'Compliance with Spanish social security laws and regulations.'
WHERE clause_version_id = 14 AND clause_name = '252.222-7004';

UPDATE Clauses
SET clause_title = 'Prohibition on Use of Nonimmigrant Aliens--Guam.' -- 'Prohibition on use of nonimmigrant aliens-Guam.'
WHERE clause_version_id = 14 AND clause_name = '252.222-7005';

UPDATE Clauses
SET clause_title = 'Hazard Warning Labels.' -- 'Hazard warning labels.'
WHERE clause_version_id = 14 AND clause_name = '252.223-7001';

UPDATE Clauses
SET clause_title = 'Safety Precautions for Ammunition and Explosives.' -- 'Safety precautions for ammunition and explosives.'
WHERE clause_version_id = 14 AND clause_name = '252.223-7002';

UPDATE Clauses
SET clause_title = 'Change in Place of Performance--Ammunition and Explosives.' -- 'Change in place of performance-ammunition and explosives.'
WHERE clause_version_id = 14 AND clause_name = '252.223-7003';

UPDATE Clauses
SET clause_title = 'Drug-Free Work Force.' -- 'Drug-free work force.'
WHERE clause_version_id = 14 AND clause_name = '252.223-7004';

UPDATE Clauses
SET clause_title = 'Safeguarding Sensitive Conventional Arms, Ammunition, and Explosives.' -- 'Safeguarding sensitive conventional arms, ammunition, and explosives.'
WHERE clause_version_id = 14 AND clause_name = '252.223-7007';

UPDATE Clauses
SET clause_title = 'Buy American-Balance of Payments Program Certificate-Basic.' -- 'Buy American-Balance of Payments Program Certificate.'
WHERE clause_version_id = 14 AND clause_name = '252.225-7000';

UPDATE Clauses
SET clause_title = 'Buy American and Balance of Payments Program-Basic (Nov 2014).' -- 'Buy American and Balance of Payments Program.'
WHERE clause_version_id = 14 AND clause_name = '252.225-7001';

UPDATE Clauses
SET clause_title = 'Qualifying Country Sources As Subcontractors.' -- 'Qualifying country sources as subcontractors.'
WHERE clause_version_id = 14 AND clause_name = '252.225-7002';

UPDATE Clauses
SET clause_title = 'Report of Intended Performance Outside the United States and Canada-Submission With Offer.' -- 'Report of intended performance outside the United States and Canada-Submission with offer.'
WHERE clause_version_id = 14 AND clause_name = '252.225-7003';

UPDATE Clauses
SET clause_title = 'Report of Intended Performance Outside the United States and Canada-Submission After Award.' -- 'Report of Intended Performance Outside the United States and Canada-Submission after Award.'
WHERE clause_version_id = 14 AND clause_name = '252.225-7004';

UPDATE Clauses
SET clause_title = 'Acquisition of the American Flag.' -- 'Acquisition of the American flag.'
WHERE clause_version_id = 14 AND clause_name = '252.225-7006';

UPDATE Clauses
SET clause_title = 'Prohibition on Acquisition of United States Munitions List Items From Communist Chinese Military Companies.' -- 'Prohibition on Acquisition of United States Munitions List Items from Communist Chinese Military Companies.'
WHERE clause_version_id = 14 AND clause_name = '252.225-7007';

UPDATE Clauses
SET clause_title = 'Restriction on Acquisition of Supercomputers.' -- 'Restriction on acquisition of supercomputers.'
WHERE clause_version_id = 14 AND clause_name = '252.225-7011';

UPDATE Clauses
SET clause_title = 'Preference for Certain Domestic Commodities.' -- 'Preference for certain domestic commodities.'
WHERE clause_version_id = 14 AND clause_name = '252.225-7012';

UPDATE Clauses
SET clause_title = 'Duty-Free Entry.' -- 'Duty-free entry.'
WHERE clause_version_id = 14 AND clause_name = '252.225-7013';

UPDATE Clauses
SET clause_title = 'Restriction on Acquisition of Hand or Measuring Tools.' -- 'Restriction on acquisition of hand or measuring tools.'
WHERE clause_version_id = 14 AND clause_name = '252.225-7015';

UPDATE Clauses
SET clause_title = 'Restriction on Acquisition of Ball and Roller Bearings.' -- 'Restriction on acquisition of ball and roller bearings.'
WHERE clause_version_id = 14 AND clause_name = '252.225-7016';

UPDATE Clauses
SET clause_data = '<p>(a) <i>Definitions.</i> As used in this clause-</p><p><i>Bahrainian photovoltaic device</i> means a photovoltaic device that-</p><p>(i) Is wholly manufactured in Bahrain; or</p><p>(ii) In the case of a photovoltaic device that consists in whole or in part of materials from another country, has been substantially transformed in Bahrain into a new and different article of commerce with a name, character, or use distinct from that of the article or articles from which it was transformed, provided that the photovoltaic device is not subsequently substantially transformed outside of Bahrain.</p><p><i>Canadian photovoltaic device</i> means a photovoltaic device that has been substantially transformed in Canada into a new and different article of commerce with a name, character, or use distinct from that of the article or articles from which it was transformed, provided that the photovoltaic device is not subsequently substantially transformed outside of Canada.</p><p><i>Caribbean Basin country photovoltaic device</i> means a photovoltaic device that-</p><p>(i) Is wholly manufactured in a Caribbean Basin country; or</p><p>(ii) In the case of a photovoltaic device that consists in whole or in part of materials from another country, has been substantially transformed in a Caribbean Basin country into a new and different article of commerce with a name, character, or use distinct from that of the article or articles from which it was transformed, provided that the photovoltaic device is not subsequently substantially transformed outside of a Caribbean Basin country.</p><p><i>Designated country</i> means-</p><p>(i) A World Trade Organization Government Procurement Agreement (WTO GPA) country (Armenia, Aruba, Austria, Belgium, Bulgaria, Canada, Croatia, Cyprus, Czech Republic, Denmark, Estonia, Finland, France, Germany, Greece, Hong Kong, Hungary, Iceland, Ireland, Israel, Italy, Japan, Korea (Republic of), Latvia, Liechtenstein, Lithuania, Luxembourg, Malta, Montenegro, Netherlands, New Zealand, Norway, Poland, Portugal, Romania, Singapore, Slovak Republic, Slovenia, Spain, Sweden, Switzerland, Taiwan (known in the World Trade Organization as ''the Separate Customs Territory of Taiwan, Penghu, Kinmen, and Matsu" (Chinese Taipei)), or the United Kingdom);</p><p>(ii) A Free Trade Agreement country (Australia, Bahrain, Canada, Chile, Colombia, Costa Rica, Dominican Republic, El Salvador, Guatemala, Honduras, Korea (Republic of), Mexico, Morocco, Nicaragua, Panama, Peru, or Singapore);</p><p>(iii) A least developed country (Afghanistan, Angola, Bangladesh, Benin, Bhutan, Burkina Faso, Burundi, Cambodia, Central African Republic, Chad, Comoros, Democratic Republic of Congo, Djibouti, Equatorial Guinea, Eritrea, Ethiopia, Gambia, Guinea, Guinea-Bissau, Haiti, Kiribati, Laos, Lesotho, Liberia, Madagascar, Malawi, Mali, Mauritania, Mozambique, Nepal, Niger, Rwanda, Samoa, Sao Tome and Principe, Senegal, Sierra Leone, Solomon Islands, Somalia, South Sudan, Tanzania, Timor-Leste, Togo, Tuvalu, Uganda, Vanuatu, Yemen, or Zambia); or</p><p>(iv) A Caribbean Basin country (Antigua and Barbuda, Aruba, Bahamas, Barbados, Belize, Bonaire, British Virgin Islands, Curacao, Dominica, Grenada, Guyana, Haiti, Jamaica, Montserrat, Saba, St. Kitts and Nevis, St. Lucia, St. Vincent and the Grenadines, Sint Eustatius, Sint Maarten, or Trinidad and Tobago).</p><p><i>Designated country photovoltaic device</i> means a WTO GPA country photovoltaic device, a Free Trade Agreement country photovoltaic device, a least developed country photovoltaic device, or a Caribbean Basin country photovoltaic device.</p><p><i>Domestic photovoltaic device</i> means a photovoltaic device-</p><p>(i) Manufactured in the United States; and</p><p>(ii) The cost of its components that are mined, produced, or manufactured in the United States exceeds 50 percent of the cost of all components. The cost of components includes transportation costs to the place of incorporation into the end product and U.S. duty (whether or not a duty-free entry certificate is issued). Scrap generated, collected, and prepared for processing in the United States is considered domestic.</p><p><i>Foreign photovoltaic device</i> means a photovoltaic device other than a domestic photovoltaic device.</p><p><i>Free Trade Agreement country</i> means Australia, Bahrain, Canada, Chile, Colombia, Costa Rica, Dominican Republic, El Salvador, Guatemala, Honduras, Korea (Republic of), Mexico, Morocco, Nicaragua, Panama, Peru, or Singapore.</p><p><i>Free Trade Agreement country photovoltaic device</i> means a photovoltaic device that-</p><p>(i) Is wholly manufactured in a Free Trade Agreement country; or</p><p>(ii) In the case of a photovoltaic device that consists in whole or in part of materials from another country, has been substantially transformed in a Free Trade Agreement country into a new and different article of commerce with a name, character, or use distinct from that of the article or articles from which it was transformed, provided that the photovoltaic device is not subsequently substantially transformed outside of a Free Trade Agreement country.</p><p><i>Korean photovoltaic device</i> means a photovoltaic device that-</p><p>(i) Is wholly manufactured in Korea; or</p><p>(ii) In the case of a photovoltaic device that consists in whole or in part of materials from another country, has been substantially transformed in Korea (Republic of) into a new and different article of commerce with a name, character, or use distinct from that of the article or articles from which it was transformed, provided that the photovoltaic device is not subsequently substantially transformed outside of Korea (Republic of).</p><p><i>Least developed country photovoltaic device</i> means a photovoltaic device that-</p><p>(i) Is wholly manufactured in a least developed country; or</p><p>(ii) In the case of a photovoltaic device thatconsists in whole or in part of materials from another country, has been substantially transformed in a least developed country into a new and different article of commerce with a name, character, or use distinct from that of the article or articles from which it was transformed, provided that the photovoltaic device is not subsequently substantially transformed outside of a least developed country.</p><p><i>Moroccan photovoltaic device</i> means a photovoltaic device that-</p><p>(i) Is wholly manufactured in Morocco; or	</p><p>(ii) In the case of a photovoltaic device that consists in whole or in part of materials from another country, has been substantially transformed in Morocco into a new and different article of commerce with a name, character, or use distinct from that of the article or articles from which it was transformed, provided that the photovoltaic device is not subsequently substantially transformed outside of Morocco.</p><p><i>Panamanian photovoltaic device</i> means a photovoltaic device that-</p><p>(i) Is wholly manufactured in Panama; or</p><p>(ii) In the case of a photovoltaic device that consists in whole or in part of materials from another country, has been substantially transformed in Panama into a new and different article of commerce with a name, character, or use distinct from that of the article or articles from which it was transformed, provided that the photovoltaic device is not subsequently substantially transformed outside of Panama.</p><p><i>Peruvian photovoltaic device</i> means a photovoltaic device that-</p><p>(i) Is wholly manufactured in Peru; or</p><p>(ii) In the case of a photovoltaic device that consists in whole or in part of materials from another country, has been substantially transformed in Peru into a new and different article of commerce with a name, character, or use distinct from that of the article or articles from which it was transformed, provided that the photovoltaic device is not subsequently substantially transformed outside of Peru.</p><p><i>Photovoltaic device</i> means a device that converts light directly into electricity through a solid-state, semiconductor process.</p><p><i>Qualifying country</i> means a country with a reciprocal defense procurement memorandum of understanding or international agreement with the United States in which both countries agree to remove barriers to purchases of supplies produced in the other country or services performed by sources of the other country, and the memorandum or agreement complies, where applicable, with the requirements of section 36 of the Arms Export Control Act (22 U.S.C. 2776) and with 10 U.S.C. 2457. Accordingly, the following are qualifying countries:</p><table><tbody><tr><td>Australia</td><td>&nbsp;&nbsp;&nbsp;Italy</td></tr><tr><td>Austria</td><td>&nbsp;&nbsp;&nbsp;Luxembourg</td></tr><tr><td>Belgium</td><td>&nbsp;&nbsp;&nbsp;Netherlands</td></tr><tr><td>Canada</td><td>&nbsp;&nbsp;&nbsp;Norway</td></tr><tr><td>Czech Republic</td><td>&nbsp;&nbsp;&nbsp;Poland</td></tr><tr><td>Denmark</td><td>&nbsp;&nbsp;&nbsp;Portugal</td></tr><tr><td>Egypt</td><td>&nbsp;&nbsp;&nbsp;Spain</td></tr><tr><td>Finland</td><td>&nbsp;&nbsp;&nbsp;Sweden</td></tr><tr><td>France</td><td>&nbsp;&nbsp;&nbsp;Switzerland</td></tr><tr><td>Germany</td><td>&nbsp;&nbsp;&nbsp;Turkey</td></tr><tr><td>Greece</td><td>&nbsp;&nbsp;&nbsp;United Kingdom of Great Britain and Northern Ireland</td></tr><tr><td>Israel</td><td>&nbsp;</td></tr></tbody></table><p><i>Qualifying country photovoltaic device</i> means a photovoltaic device manufactured in a qualifying country.</p><p><i>United States</i> means the 50 States, the District of Columbia, and outlying areas.</p><p><i>U.S.-made photovoltaic device</i> means a photovoltaic device that-</p><p>(i) Is manufactured in the United States; or</p><p>(ii) Is substantially transformed in the United States into a new and different article of commerce with a name, character, or use distinct from that of the article or articles from which it was transformed, provided that the photovoltaic device is not subsequently substantially transformed outside of the United States.</p><p><i>WTO GPA country photovoltaic device</i> means a photovoltaic device that-</p><p>(i) Is wholly manufactured in a WTO GPA country; or</p><p>(ii) In the case of a photovoltaic device that consists in whole or in part of materials from another country, has been substantially transformed in a WTO GPA country into a new and different article of commerce with a name, character, or use distinct from that of the article or articles from which it was transformed, provided that the photovoltaic device is not subsequently substantially transformed outside of a WTO GPA country.</p><p>(b) This clause implements section 858 of the National Defense Authorization Act for Fiscal Year 2015 (Pub. L. 113-291).</p><p>(c) <i>Restriction.</i> If the Contractor specified in its offer in the Photovoltaic Devices-Certificate provision of the solicitation that the estimated value of the photovoltaic devices to be utilized in performance of this contract would be-</p><p>(1) Less than $25,000, then the Contractor shall utilize only domestic photovoltaic devices unless, in its offer, it specified utilization of qualifying country or other foreign photovoltaic devices in paragraph (d)(2) of the Photovoltaic Devices-Certificate provision of the solicitation. If the Contractor certified in its offer that it will utilize a qualifying country photovoltaic device, then the Contractor shall utilize a qualifying country photovoltaic device as specified, or, at the Contractor''s option, a domestic photovoltaic device;</p><p>(2) $25,000 or more but less than $77,533, then the Contractor shall utilize in the performance of this contract only domestic photovoltaic devices unless, in its offer, it specified utilization of Canadian, qualifying country, or other foreign photovoltaic devices in paragraph (d)(3) of the Photovoltaic Devices-Certificate provision of the solicitation. If the Contractor certified in its offer that it will utilize a qualifying country photovoltaic device or a Canadian photovoltaic device, then the Contractor shall utilize a qualifying country photovoltaic device or a Canadian photovoltaic device as specified, or, at the Contractor''s option, a domestic photovoltaic device;</p><p>(3) $77,533or more but less than $100,000, then the Contractor shall utilize under this contract only domestic photovoltaic devices or Free Trade Agreement country photovoltaic devices (other than Bahrainian, Korean, Moroccan, Panamanian, or Peruvian photovoltaic devices), unless, in its offer, it specified utilization of qualifying country or other foreign photovoltaic devices in paragraph (d)(4) of the Photovoltaic Devices-Certificate provision of the solicitation. If the Contractor certified in its offer that it will utilize a qualifying country photovoltaic device or a Free Trade Agreement country photovoltaic device (other than a Bahrainian, Korean, Moroccan, Panamanian, or Peruvian photovoltaic device), then the Contractor shall utilize a qualifying country photovoltaic device; a Free Trade Agreement country photovoltaic device (other than a Bahrainian, Korean, Moroccan, Panamanian, or Peruvian photovoltaic device) as specified; or, at the Contractor''s option, a domestic photovoltaic device;</p><p>(4) $100,000 or more but less than;$191,000, then the Contractor shall utilize under this contract only domestic photovoltaic devices or Free Trade Agreement country photovoltaic devices (other than Bahrainian, Moroccan, Panamanian, or Peruvian photovoltaic devices), unless, in its offer, it specified utilization of qualifying country or other foreign photovoltaic devices in paragraph (d)(5) of the Photovoltaic Devices-Certificate provision of the solicitation. If the Contractor certified in its offer that it will utilize a qualifying country photovoltaic device or a Free Trade Agreement country photovoltaic device (other than a Bahrainian, Moroccan, Panamanian, or Peruvian photovoltaic device), then the Contractor shall utilize a qualifying country photovoltaic device; a Free Trade Agreement country photovoltaic device (other than a Bahrainian, Moroccan, Panamanian, or Peruvian photovoltaic device) as specified; or, at the Contractor''s option, a domestic photovoltaic device; or</p><p>(5) $191,000or more, then the Contractor shall utilize under this contract only domestic or designated country photovoltaic devices unless, in its offer, it specified utilization of U.S.-made or qualifying country photovoltaic devices in paragraph (d)(6)(ii) or (iii) respectively of the Photovoltaic Devices-Certificate provision of the solicitation. If the Contractor certified in its offer that it will utilize a designated country, U.S.-made, or qualifying country photovoltaic device, then the Contractor shall utilize a designated country, U.S.-made, or qualifying country photovoltaic device as specified, or, at the Contractor''s option, a domestic photovoltaic device.</p>'
--               '<p>(a) <i>Definitions.</i> As used in this clause-</p><p><i>Bahrainian photovoltaic device</i> means a photovoltaic device that-</p><p>(i) Is wholly manufactured in Bahrain; or</p><p>(ii) In the case of a photovoltaic device that consists in whole or in part of materials from another country, has been substantially transformed in Bahrain into a new and different article of commerce with a name, character, or use distinct from that of the article or articles from which it was transformed, provided that the photovoltaic device is not subsequently substantially transformed outside of Bahrain.</p><p><i>Canadian photovoltaic device</i> means a photovoltaic device that has been substantially transformed in Canada into a new and different article of commerce with a name, character, or use distinct from that of the article or articles from which it was transformed, provided that the photovoltaic device is not subsequently substantially transformed outside of Canada.</p><p><i>Caribbean Basin country photovoltaic device</i> means a photovoltaic device that-</p><p>(i) Is wholly manufactured in a Caribbean Basin country; or</p><p>(ii) In the case of a photovoltaic device that consists in whole or in part of materials from another country, has been substantially transformed in a Caribbean Basin country into a new and different article of commerce with a name, character, or use distinct from that of the article or articles from which it was transformed, provided that the photovoltaic device is not subsequently substantially transformed outside of a Caribbean Basin country.</p><p><i>Designated country</i> means-</p><p>(i) A World Trade Organization Government Procurement Agreement (WTO GPA) country (Armenia, Aruba, Austria, Belgium, Bulgaria, Canada, Croatia, Cyprus, Czech Republic, Denmark, Estonia, Finland, France, Germany, Greece, Hong Kong, Hungary, Iceland, Ireland, Israel, Italy, Japan, Korea (Republic of), Latvia, Liechtenstein, Lithuania, Luxembourg, Malta, Montenegro, Netherlands, New Zealand, Norway, Poland, Portugal, Romania, Singapore, Slovak Republic, Slovenia, Spain, Sweden, Switzerland, Taiwan (known in the World Trade Organization as ''the Separate Customs Territory of Taiwan, Penghu, Kinmen, and Matsu" (Chinese Taipei)), or the United Kingdom);</p><p>(ii) A Free Trade Agreement country (Australia, Bahrain, Canada, Chile, Colombia, Costa Rica, Dominican Republic, El Salvador, Guatemala, Honduras, Korea (Republic of), Mexico, Morocco, Nicaragua, Panama, Peru, or Singapore);</p><p>(iii) A least developed country (Afghanistan, Angola, Bangladesh, Benin, Bhutan, Burkina Faso, Burundi, Cambodia, Central African Republic, Chad, Comoros, Democratic Republic of Congo, Djibouti, Equatorial Guinea, Eritrea, Ethiopia, Gambia, Guinea, Guinea-Bissau, Haiti, Kiribati, Laos, Lesotho, Liberia, Madagascar, Malawi, Mali, Mauritania, Mozambique, Nepal, Niger, Rwanda, Samoa, Sao Tome and Principe, Senegal, Sierra Leone, Solomon Islands, Somalia, South Sudan, Tanzania, Timor-Leste, Togo, Tuvalu, Uganda, Vanuatu, Yemen, or Zambia); or</p><p>(iv) A Caribbean Basin country (Antigua and Barbuda, Aruba, Bahamas, Barbados, Belize, Bonaire, British Virgin Islands, Curacao, Dominica, Grenada, Guyana, Haiti, Jamaica, Montserrat, Saba, St. Kitts and Nevis, St. Lucia, St. Vincent and the Grenadines, Sint Eustatius, Sint Maarten, or Trinidad and Tobago).</p><p><i>Designated country photovoltaic device</i> means a WTO GPA country photovoltaic device, a Free Trade Agreement country photovoltaic device, a least developed country photovoltaic device, or a Caribbean Basin country photovoltaic device.</p><p><i>Domestic photovoltaic device</i> means a photovoltaic device-</p><p>(i) Manufactured in the United States; and</p><p>(ii) The cost of its components that are mined, produced, or manufactured in the United States exceeds 50 percent of the cost of all components. The cost of components includes transportation costs to the place of incorporation into the end product and U.S. duty (whether or not a duty-free entry certificate is issued). Scrap generated, collected, and prepared for processing in the United States is considered domestic.</p><p><i>Foreign photovoltaic device</i> means a photovoltaic device other than a domestic photovoltaic device.</p><p><i>Free Trade Agreement country</i> means Australia, Bahrain, Canada, Chile, Colombia, Costa Rica, Dominican Republic, El Salvador, Guatemala, Honduras, Korea (Republic of), Mexico, Morocco, Nicaragua, Panama, Peru, or Singapore.</p><p><i>Free Trade Agreement country photovoltaic device</i> means a photovoltaic device that-</p><p>(i) Is wholly manufactured in a Free Trade Agreement country; or</p><p>(ii) In the case of a photovoltaic device that consists in whole or in part of materials from another country, has been substantially transformed in a Free Trade Agreement country into a new and different article of commerce with a name, character, or use distinct from that of the article or articles from which it was transformed, provided that the photovoltaic device is not subsequently substantially transformed outside of a Free Trade Agreement country.</p><p><i>Korean photovoltaic device</i> means a photovoltaic device that-</p><p>(i) Is wholly manufactured in Korea; or</p><p>(ii) In the case of a photovoltaic device that consists in whole or in part of materials from another country, has been substantially transformed in Korea (Republic of) into a new and different article of commerce with a name, character, or use distinct from that of the article or articles from which it was transformed, provided that the photovoltaic device is not subsequently substantially transformed outside of Korea (Republic of).</p><p><i>Least developed country photovoltaic device</i> means a photovoltaic device that-</p><p>(i) Is wholly manufactured in a least developed country; or</p><p>(ii) In the case of a photovoltaic device thatconsists in whole or in part of materials from another country, has been substantially transformed in a least developed country into a new and different article of commerce with a name, character, or use distinct from that of the article or articles from which it was transformed, provided that the photovoltaic device is not subsequently substantially transformed outside of a least developed country.</p><p><i>Moroccan photovoltaic device</i> means a photovoltaic device that-</p><p>(i) Is wholly manufactured in Morocco; or	</p><p>(ii) In the case of a photovoltaic device that consists in whole or in part of materials from another country, has been substantially transformed in Morocco into a new and different article of commerce with a name, character, or use distinct from that of the article or articles from which it was transformed, provided that the photovoltaic device is not subsequently substantially transformed outside of Morocco.</p><p><i>Panamanian photovoltaic device</i> means a photovoltaic device that-</p><p>(i) Is wholly manufactured in Panama; or</p><p>(ii) In the case of a photovoltaic device that consists in whole or in part of materials from another country, has been substantially transformed in Panama into a new and different article of commerce with a name, character, or use distinct from that of the article or articles from which it was transformed, provided that the photovoltaic device is not subsequently substantially transformed outside of Panama.</p><p><i>Peruvian photovoltaic device</i> means a photovoltaic device that-</p><p>(i) Is wholly manufactured in Peru; or</p><p>(ii) In the case of a photovoltaic device that consists in whole or in part of materials from another country, has been substantially transformed in Peru into a new and different article of commerce with a name, character, or use distinct from that of the article or articles from which it was transformed, provided that the photovoltaic device is not subsequently substantially transformed outside of Peru.</p><p><i>Photovoltaic device</i> means a device that converts light directly into electricity through a solid-state, semiconductor process.</p><p><i>Qualifying country</i> means a country with a reciprocal defense procurement memorandum of understanding or international agreement with the United States in which both countries agree to remove barriers to purchases of supplies produced in the other country or services performed by sources of the other country, and the memorandum or agreement complies, where applicable, with the requirements of section 36 of the Arms Export Control Act (22 U.S.C. 2776) and with 10 U.S.C. 2457. Accordingly, the following are qualifying countries:</p><table><tbody><tr><td>Australia</td><td>&nbsp;&nbsp;&nbsp;Italy</td></tr><tr><td>Austria</td><td>&nbsp;&nbsp;&nbsp;Luxembourg</td></tr><tr><td>Belgium</td><td>&nbsp;&nbsp;&nbsp;Netherlands</td></tr><tr><td>Canada</td><td>&nbsp;&nbsp;&nbsp;Norway</td></tr><tr><td>Czech Republic</td><td>&nbsp;&nbsp;&nbsp;Poland</td></tr><tr><td>Denmark</td><td>&nbsp;&nbsp;&nbsp;Portugal</td></tr><tr><td>Egypt</td><td>&nbsp;&nbsp;&nbsp;Spain</td></tr><tr><td>Finland</td><td>&nbsp;&nbsp;&nbsp;Sweden</td></tr><tr><td>France</td><td>&nbsp;&nbsp;&nbsp;Switzerland</td></tr><tr><td>Germany</td><td>&nbsp;&nbsp;&nbsp;Turkey</td></tr><tr><td>Greece</td><td>&nbsp;&nbsp;&nbsp;United Kingdom of Great Britain and Northern Ireland</td></tr><tr><td>Israel</td><td>&nbsp;</td></tr></tbody></table><p><i>Qualifying country photovoltaic device</i> means a photovoltaic device manufactured in a qualifying country.</p><p><i>United States</i> means the 50 States, the District of Columbia, and outlying areas.</p><p><i>U.S.-made photovoltaic device</i> means a photovoltaic device that-</p><p>(i) Is manufactured in the United States; or</p><p>(ii) Is substantially transformed in the United States into a new and different article of commerce with a name, character, or use distinct from that of the article or articles from which it was transformed, provided that the photovoltaic device is not subsequently substantially transformed outside of the United States.</p><p><i>WTO GPA country photovoltaic device</i> means a photovoltaic device that-</p><p>(i) Is wholly manufactured in a WTO GPA country; or</p><p>(ii) In the case of a photovoltaic device that consists in whole or in part of materials from another country, has been substantially transformed in a WTO GPA country into a new and different article of commerce with a name, character, or use distinct from that of the article or articles from which it was transformed, provided that the photovoltaic device is not subsequently substantially transformed outside of a WTO GPA country.</p><p>(b) This clause implements section 858 of the National Defense Authorization Act for Fiscal Year 2015 (Pub. L. 113-291).</p><p>(c) <i>Restriction.</i> If the Contractor specified in its offer in the Photovoltaic Devices-Certificate provision of the solicitation that the estimated value of the photovoltaic devices to be utilized in performance of this contract would be-</p><p>(1) Less than $25,000, then the Contractor shall utilize only domestic photovoltaic devices unless, in its offer, it specified utilization of qualifying country or other foreign photovoltaic devices in paragraph (d)(2) of the Photovoltaic Devices-Certificate provision of the solicitation. If the Contractor certified in its offer that it will utilize a qualifying country photovoltaic device, then the Contractor shall utilize a qualifying country photovoltaic device as specified, or, at the Contractor''s option, a domestic photovoltaic device;</p><p>(2) $25,000 or more but less than $79,507, then the Contractor shall utilize in the performance of this contract only domestic photovoltaic devices unless, in its offer, it specified utilization of Canadian, qualifying country, or other foreign photovoltaic devices in paragraph (d)(3) of the Photovoltaic Devices-Certificate provision of the solicitation. If the Contractor certified in its offer that it will utilize a qualifying country photovoltaic device or a Canadian photovoltaic device, then the Contractor shall utilize a qualifying country photovoltaic device or a Canadian photovoltaic device as specified, or, at the Contractor''s option, a domestic photovoltaic device;</p><p>(3) $79,507 or more but less than $100,000, then the Contractor shall utilize under this contract only domestic photovoltaic devices or Free Trade Agreement country photovoltaic devices (other than Bahrainian, Korean, Moroccan, Panamanian, or Peruvian photovoltaic devices), unless, in its offer, it specified utilization of qualifying country or other foreign photovoltaic devices in paragraph (d)(4) of the Photovoltaic Devices-Certificate provision of the solicitation. If the Contractor certified in its offer that it will utilize a qualifying country photovoltaic device or a Free Trade Agreement country photovoltaic device (other than a Bahrainian, Korean, Moroccan, Panamanian, or Peruvian photovoltaic device), then the Contractor shall utilize a qualifying country photovoltaic device; a Free Trade Agreement country photovoltaic device (other than a Bahrainian, Korean, Moroccan, Panamanian, or Peruvian photovoltaic device) as specified; or, at the Contractor''s option, a domestic photovoltaic device;</p><p>(4) $100,000 or more but less than $204,000, then the Contractor shall utilize under this contract only domestic photovoltaic devices or Free Trade Agreement country photovoltaic devices (other than Bahrainian, Moroccan, Panamanian, or Peruvian photovoltaic devices), unless, in its offer, it specified utilization of qualifying country or other foreign photovoltaic devices in paragraph (d)(5) of the Photovoltaic Devices-Certificate provision of the solicitation. If the Contractor certified in its offer that it will utilize a qualifying country photovoltaic device or a Free Trade Agreement country photovoltaic device (other than a Bahrainian, Moroccan, Panamanian, or Peruvian photovoltaic device), then the Contractor shall utilize a qualifying country photovoltaic device; a Free Trade Agreement country photovoltaic device (other than a Bahrainian, Moroccan, Panamanian, or Peruvian photovoltaic device) as specified; or, at the Contractor''s option, a domestic photovoltaic device; or</p><p>(5) $204,000 or more, then the Contractor shall utilize under this contract only domestic or designated country photovoltaic devices unless, in its offer, it specified utilization of U.S.-made or qualifying country photovoltaic devices in paragraph (d)(6)(ii) or (iii) respectively of the Photovoltaic Devices-Certificate provision of the solicitation. If the Contractor certified in its offer that it will utilize a designated country, U.S.-made, or qualifying country photovoltaic device, then the Contractor shall utilize a designated country, U.S.-made, or qualifying country photovoltaic device as specified, or, at the Contractor''s option, a domestic photovoltaic device.</p>'
WHERE clause_version_id = 14 AND clause_name = '252.225-7017';
INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT clause_id, prescription_id FROM Prescriptions, Clauses
WHERE prescription_name IN ('225.7017-5(a)(1)') 
AND clause_id IN (SELECT clause_id FROM Clauses WHERE clause_name = '252.225-7017' AND clause_version_id = 14); 

UPDATE Clauses
SET clause_data = '<p>(a) <i>Definitions.</i> ''Bahrainian photovoltaic device," ''Canadian photovoltaic device," ''Caribbean Basin photovoltaic device," ''designated country," ''designated country photovoltaic device," ''domestic photovoltaic device," ''foreign photovoltaic device," ''Free Trade Agreement country," ''Free Trade Agreement photovoltaic device," ''Korean photovoltaic device," ''least developed country photovoltaic device," ''Moroccan photovoltaic device," ''Panamanian photovoltaic device," ''Peruvian photovoltaic device," ''photovoltaic device," ''qualifying country," ''qualifying country photovoltaic device," ''United States," ''U.S.-made photovoltaic device," and ''WTO GPA country photovoltaic device" have the meanings given in the Photovoltaic Devices clause of this solicitation.</p><p>(b) <i>Restrictions.</i> The following restrictions apply, depending on the estimated aggregate value of photovoltaic devices to be utilized under a resultant contract:</p><p>(1) If less than $191,000, then the Government will not accept an offer specifying the use of-</p><p>(i) Other foreign photovoltaic devices in paragraph (d)(2)(iii), (d)(3)(iii), (d)(4)(iii), or (d)(5)(iii) of this provision, unless the offeror documents to the satisfaction of the Contracting Officer that the price of the foreign photovoltaic device plus 50 percent is less than the price of a comparable domestic photovoltaic device and the Government determines in accordance with DFARS 225.7017-4(b) that the price of a comparable domestic photovoltaic device would be unreasonable; and</p><p>(ii) A qualifying country photovoltaic device unless the Government determines in accordance with DFARS 225.7017-4(a) that it is in the public interest to allow use of a qualifying country photovoltaic device.</p><p>(2) If $191,000or more, then the Government will consider only offers that utilize photovoltaic devices that are domestic or designated country photovoltaic devices, unless the Government determines in accordance with DFARS 225.7017-4(a) that it is in the public interest to allow use of a qualifying country photovoltaic device from Egypt or Turkey, or a U.S.-made photovoltaic device.</p><p>(c) <i>Country in which a designated country photovoltaic device was wholly manufactured or was substantially transformed.</i> If the estimated value of the photovoltaic devices to be utilized under a resultant contract exceeds $25,000, the Offeror''s certification that such photovoltaic device (e.g., solar panel) is a designated country photovoltaic device shall be consistent with country of origin determinations by the U.S. Customs and Border Protection with regard to importation of the same or similar photovoltaic devices into the United States. If the Offeror is uncertain as to what the country of origin would be determined to be by the U.S. Customs and Border Protection, the Offeror shall request a determination from U.S. Customs and Border Protection. (See <i>http://www.cbp.gov/trade/rulings</i>.)</p><p>(d) <i>Certification and identification of country of origin. [The offeror shall check the block and fill in the blank for one of the following paragraphs, based on the estimated value and the country of origin of photovoltaic devices to be utilized in performance of the contract:]</i></p><p>__(1) No photovoltaic devices will be utilized in performance of the contract.</p><p>(2) If less than $25,000-</p><p>__(i) The offeror certifies that each photovoltaic device to be utilized in performance of the contract is a domestic photovoltaic device;</p><p>__(ii) The offeror certifies that each photovoltaic device to be utilized in performance of the contract is a qualifying country photovoltaic device <i>[Offeror to specify country of origin__];</i> or</p><p>__(iii) The foreign (other than qualifying country) photovoltaic devices to be utilized in performance of the contract are the product of ___. <i>[Offeror to specify country of origin, if known, and provide documentation that the cost of a domestic photovoltaic device would be unreasonable in comparison to the cost of the proposed foreign photovoltaic device, i.e.</i><i>, that the price of the foreign photovoltaic device plus 50 percent is less than the price of a comparable domestic photovoltaic device.]</i></p><p>(3) If $25,000 or more but less than $77,533-</p><p>__(i) The offeror certifies that each photovoltaic device to be utilized in performance of the contract is a domestic photovoltaic device or a Canadian photovoltaic device <i>[Offeror to specify country of origin__];</i></p><p>__(ii) The offeror certifies that each photovoltaic device to be utilized in performance of the contract is a qualifying country photovoltaic device <i>[Offeror to specify country of origin__];</i> or</p><p>__(iii) The foreign (other than qualifying country or Canadian) photovoltaic devices to be utilized in performance of the contract are the product of ___. <i>[Offeror to specify country of origin, if known, and provide documentation that the cost of a domestic photovoltaic device would be unreasonable in comparison to the cost of the proposed foreign photovoltaic device, i.e.</i><i>, that the price of the foreign photovoltaic device plus 50 percent is less than the price of a comparable domestic photovoltaic device.]</i></p><p>(4) If $77,533or more but less than $100,000-</p><p>__(i) The offeror certifies that each photovoltaic device to be utilized in performance of the contract is a domestic photovoltaic device or a Free Trade Agreement country photovoltaic device (other than a Bahrainian, Korean, Moroccan, Panamanian, or Peruvian photovoltaic device) <i>[Offeror to specify country of origin__];</i></p><p>__(ii) The offeror certifies that each photovoltaic device to be utilized in performance of the contract is a qualifying country photovoltaic device (except an Australian or Canadian photovoltaic device, to be listed in paragraph (d)(4)(i) of this provision as a Free Trade Agreement country photovoltaic device) <i>[Offeror to specify country of origin__];</i> or</p><p>__(iii) The offered foreign photovoltaic devices (other than those from countries listed in paragraph (d)(4)(i) or (d)(4)(ii) of this provision) are the product of ___. <i>[Offeror to specify country of origin, if known, and provide documentation that the cost of a domestic photovoltaic device would be unreasonable in comparison to the cost of the proposed foreign photovoltaic device, i.e.</i><i>, that the price of the foreign photovoltaic device plus 50 percent is less than the price of a comparable domestic photovoltaic device.]</i></p><p>(5) If $100,000 or more but less than $191,000-</p><p>__(i) The offeror certifies that each photovoltaic device to be utilized in performance of the contract is a domestic photovoltaic device or a Free Trade Agreement country photovoltaic device (other than a Bahrainian, Moroccan, Panamanian, or Peruvian photovoltaic device) <i>[Offeror to specify country of origin__];</i></p><p>__(ii) The offeror certifies that each photovoltaic device to be utilized in performance of the contract is a qualifying country photovoltaic device (except an Australian or Canadian photovoltaic device, to be listed in paragraph (d)(5)(i) of this provision as a Free Trade Agreement country photovoltaic device) <i>[Offeror to specify country of origin__];</i> or</p><p>__(iii) The offered foreign photovoltaic devices (other than those from countries listed in paragraph (d)(5)(i) or (d)(5)(ii) of this provision) are the product of ___. <i>[Offeror to specify country of origin, if known, and provide documentation that the cost of a domestic photovoltaic device would be unreasonable in comparison to the cost of the proposed foreign photovoltaic device, i.e.</i><i>, that the price of the foreign photovoltaic device plus 50 percent is less than the price of a comparable domestic photovoltaic device.]</i></p><p>(6) If $191,000or more, the Offeror certifies that each photovoltaic device to be used in performance of the contract is-</p><p>__(i) A domestic or designated country photovoltaic device <i>[Offeror to specify country of origin__];</i></p><p>__(ii) A U.S.-made photovoltaic device; or</p><p>__(iii) A qualifying country photovoltaic device from Egypt of Turkey (photovoltaic devices from other qualifying countries to be listed in paragraph (d)(6)(i) of this provision as designated country photovoltaic devices). <i>[Offeror to specify country of origin__.]</i></p>'
--               '<p>(a) <i>Definitions.</i> ''Bahrainian photovoltaic device," ''Canadian photovoltaic device," ''Caribbean Basin photovoltaic device," ''designated country," ''designated country photovoltaic device," ''domestic photovoltaic device," ''foreign photovoltaic device," ''Free Trade Agreement country," ''Free Trade Agreement photovoltaic device," ''Korean photovoltaic device," ''least developed country photovoltaic device," ''Moroccan photovoltaic device," ''Panamanian photovoltaic device," ''Peruvian photovoltaic device," ''photovoltaic device," ''qualifying country," ''qualifying country photovoltaic device," ''United States," ''U.S.-made photovoltaic device," and ''WTO GPA country photovoltaic device" have the meanings given in the Photovoltaic Devices clause of this solicitation.</p><p>(b) <i>Restrictions.</i> The following restrictions apply, depending on the estimated aggregate value of photovoltaic devices to be utilized under a resultant contract:</p><p>(1) If less than $204,000, then the Government will not accept an offer specifying the use of-</p><p>(i) Other foreign photovoltaic devices in paragraph (d)(2)(iii), (d)(3)(iii), (d)(4)(iii), or (d)(5)(iii) of this provision, unless the offeror documents to the satisfaction of the Contracting Officer that the price of the foreign photovoltaic device plus 50 percent is less than the price of a comparable domestic photovoltaic device and the Government determines in accordance with DFARS 225.217-4(b) that the price of a comparable domestic photovoltaic device would be unreasonable; and</p><p>(ii) A qualifying country photovoltaic device unless the Government determines in accordance with DFARS 225.217-4(a) that it is in the public interest to allow use of a qualifying country photovoltaic device.</p><p>(2) If $204,000 or more, then the Government will consider only offers that utilize photovoltaic devices that are domestic or designated country photovoltaic devices, unless the Government determines in accordance with DFARS 225.7017-4(a) that it is in the public interest to allow use of a qualifying country photovoltaic device from Egypt or Turkey, or a U.S.-made photovoltaic device.</p><p>(c) <i>Country in which a designated country photovoltaic device was wholly manufactured or was substantially transformed.</i> If the estimated value of the photovoltaic devices to be utilized under a resultant contract exceeds $25,000, the Offeror''s certification that such photovoltaic device (e.g., solar panel) is a designated country photovoltaic device shall be consistent with country of origin determinations by the U.S. Customs and Border Protection with regard to importation of the same or similar photovoltaic devices into the United States. If the Offeror is uncertain as to what the country of origin would be determined to be by the U.S. Customs and Border Protection, the Offeror shall request a determination from U.S. Customs and Border Protection. (See <i>http://www.cbp.gov/trade/rulings</i>.)</p><p>(d) <i>Certification and identification of country of origin. [The offeror shall check the block and fill in the blank for one of the following paragraphs, based on the estimated value and the country of origin of photovoltaic devices to be utilized in performance of the contract:]</i></p><p>__(1) No photovoltaic devices will be utilized in performance of the contract.</p><p>(2) If less than $25,000-</p><p>__(i) The offeror certifies that each photovoltaic device to be utilized in performance of the contract is a domestic photovoltaic device;</p><p>__(ii) The offeror certifies that each photovoltaic device to be utilized in performance of the contract is a qualifying country photovoltaic device <i>[Offeror to specify country of origin__];</i> or</p><p>__(iii) The foreign (other than qualifying country) photovoltaic devices to be utilized in performance of the contract are the product of ___. <i>[Offeror to specify country of origin, if known, and provide documentation that the cost of a domestic photovoltaic device would be unreasonable in comparison to the cost of the proposed foreign photovoltaic device, i.e.</i><i>, that the price of the foreign photovoltaic device plus 50 percent is less than the price of a comparable domestic photovoltaic device.]</i></p><p>(3) If $25,000 or more but less than $79,507-</p><p>__(i) The offeror certifies that each photovoltaic device to be utilized in performance of the contract is a domestic photovoltaic device or a Canadian photovoltaic device <i>[Offeror to specify country of origin__];</i></p><p>__(ii) The offeror certifies that each photovoltaic device to be utilized in performance of the contract is a qualifying country photovoltaic device <i>[Offeror to specify country of origin__];</i> or</p><p>__(iii) The foreign (other than qualifying country or Canadian) photovoltaic devices to be utilized in performance of the contract are the product of ___. <i>[Offeror to specify country of origin, if known, and provide documentation that the cost of a domestic photovoltaic device would be unreasonable in comparison to the cost of the proposed foreign photovoltaic device, i.e.</i><i>, that the price of the foreign photovoltaic device plus 50 percent is less than the price of a comparable domestic photovoltaic device.]</i></p><p>(4) If $79,507 or more but less than $100,000-</p><p>__(i) The offeror certifies that each photovoltaic device to be utilized in performance of the contract is a domestic photovoltaic device or a Free Trade Agreement country photovoltaic device (other than a Bahrainian, Korean, Moroccan, Panamanian, or Peruvian photovoltaic device) <i>[Offeror to specify country of origin__];</i></p><p>__(ii) The offeror certifies that each photovoltaic device to be utilized in performance of the contract is a qualifying country photovoltaic device (except an Australian or Canadian photovoltaic device, to be listed in paragraph (d)(4)(i) of this provision as a Free Trade Agreement country photovoltaic device) <i>[Offeror to specify country of origin__];</i> or</p><p>__(iii) The offered foreign photovoltaic devices (other than those from countries listed in paragraph (d)(4)(i) or (d)(4)(ii) of this provision) are the product of ___. <i>[Offeror to specify country of origin, if known, and provide documentation that the cost of a domestic photovoltaic device would be unreasonable in comparison to the cost of the proposed foreign photovoltaic device, i.e.</i><i>, that the price of the foreign photovoltaic device plus 50 percent is less than the price of a comparable domestic photovoltaic device.]</i></p><p>(5) If $100,000 or more but less than $204,000-</p><p>__(i) The offeror certifies that each photovoltaic device to be utilized in performance of the contract is a domestic photovoltaic device or a Free Trade Agreement country photovoltaic device (other than a Bahrainian, Moroccan, Panamanian, or Peruvian photovoltaic device) <i>[Offeror to specify country of origin__];</i></p><p>__(ii) The offeror certifies that each photovoltaic device to be utilized in performance of the contract is a qualifying country photovoltaic device (except an Australian or Canadian photovoltaic device, to be listed in paragraph (d)(5)(i) of this provision as a Free Trade Agreement country photovoltaic device) <i>[Offeror to specify country of origin__];</i> or</p><p>__(iii) The offered foreign photovoltaic devices (other than those from countries listed in paragraph (d)(5)(i) or (d)(5)(ii) of this provision) are the product of ___. <i>[Offeror to specify country of origin, if known, and provide documentation that the cost of a domestic photovoltaic device would be unreasonable in comparison to the cost of the proposed foreign photovoltaic device, i.e.</i><i>, that the price of the foreign photovoltaic device plus 50 percent is less than the price of a comparable domestic photovoltaic device.]</i></p><p>(6) If $204,000 or more, the Offeror certifies that each photovoltaic device to be used in performance of the contract is-</p><p>__(i) A domestic or designated country photovoltaic device <i>[Offeror to specify country of origin__];</i></p><p>__(ii) A U.S.-made photovoltaic device; or</p><p>__(iii) A qualifying country photovoltaic device from Egypt of Turkey (photovoltaic devices from other qualifying countries to be listed in paragraph (d)(6)(i) of this provision as designated country photovoltaic devices). <i>[Offeror to specify country of origin__.]</i></p>'
WHERE clause_version_id = 14 AND clause_name = '252.225-7018';
INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT clause_id, prescription_id FROM Prescriptions, Clauses
WHERE prescription_name IN ('225.7017-5(b)') 
AND clause_id IN (SELECT clause_id FROM Clauses WHERE clause_name = '252.225-7018' AND clause_version_id = 14); 

UPDATE Clauses
SET clause_title = 'Restriction on Acquisition of Anchor and Mooring Chain.' -- 'Restriction on acquisition of anchor and mooring chain.'
WHERE clause_version_id = 14 AND clause_name = '252.225-7019';

UPDATE Clauses
SET clause_title = 'Trade Agreements Certificate-Basic.' -- 'Trade Agreements Certificate.'
WHERE clause_version_id = 14 AND clause_name = '252.225-7020';

UPDATE Clauses
SET clause_title = 'Trade Agreements.' -- 'Trade agreements.'
WHERE clause_version_id = 14 AND clause_name = '252.225-7021';

UPDATE Clauses
SET clause_title = 'Preference for Products or Services From Afghanistan.' -- 'Preference for Products or Services from Afghanistan.'
WHERE clause_version_id = 14 AND clause_name = '252.225-7023';

UPDATE Clauses
SET clause_title = 'Requirement for Products or Services From Afghanistan.' -- 'Requirement for Products or Services from Afghanistan.'
WHERE clause_version_id = 14 AND clause_name = '252.225-7024';

UPDATE Clauses
SET clause_title = 'Restriction on Acquisition of Forgings.' -- 'Restriction on acquisition of forgings.'
WHERE clause_version_id = 14 AND clause_name = '252.225-7025';

UPDATE Clauses
SET clause_title = 'Acquisition Restricted to Products or Services From Afghanistan.' -- 'Acquisition Restricted to Products or Services from Afghanistan.'
WHERE clause_version_id = 14 AND clause_name = '252.225-7026';

UPDATE Clauses
SET clause_title = 'Restriction on Contingent Fees for Foreign Military Sales.' -- 'Restriction on contingent fees for foreign military sales.'
WHERE clause_version_id = 14 AND clause_name = '252.225-7027';

UPDATE Clauses
SET clause_title = 'Exclusionary Policies and Practices of Foreign Governments.' -- 'Exclusionary policies and practices of foreign governments.'
WHERE clause_version_id = 14 AND clause_name = '252.225-7028';

UPDATE Clauses
SET clause_title = 'Secondary Arab Boycott of Israel.' -- 'Secondary Arab boycott of Israel.'
WHERE clause_version_id = 14 AND clause_name = '252.225-7031';

UPDATE Clauses
SET clause_title = 'Waiver of United Kingdom Levies-Evaluation of Offers.' -- 'Waiver of United Kingdom Levies-Evaluation of offers.'
WHERE clause_version_id = 14 AND clause_name = '252.225-7032';

UPDATE Clauses
SET clause_title = 'Waiver of United Kingdom Levies.' -- 'Waiver of United Kingdom levies.'
WHERE clause_version_id = 14 AND clause_name = '252.225-7033';

UPDATE Clauses
SET clause_title = 'Buy American-Free Trade Agreements-Balance of Payments Program Certificate-Basic.' -- 'Buy American-Free Trade Agreements-Balance of Payments Program Certificate.'
WHERE clause_version_id = 14 AND clause_name = '252.225-7035';

UPDATE Clauses
SET clause_title = 'Authorization to Perform.' -- 'Authorization to perform.'
WHERE clause_version_id = 14 AND clause_name = '252.225-7042';

UPDATE Clauses
SET clause_title = 'Antiterrorism/Force Protection for Defense Contractors Outside the United States.' -- 'Antiterrorism/force protection policy for defense contractors outside the United States.'
WHERE clause_version_id = 14 AND clause_name = '252.225-7043';

UPDATE Clauses
SET clause_title = 'Balance of Payments Program-Construction Material-Basic.' -- 'Balance of Payments Program-Construction Material.'
WHERE clause_version_id = 14 AND clause_name = '252.225-7044';

UPDATE Clauses
SET clause_title = 'Balance of Payments Program - Construction Material Under Trade Agreements-Basic.' -- 'Balance of Payments Program-Construction Material Under Trade Agreements.'
WHERE clause_version_id = 14 AND clause_name = '252.225-7045';

UPDATE Clauses
SET clause_title = 'Disclosure of Ownership or Control by the Government of a Country That Is a State Sponsor of Terrorism.' -- 'Disclosure of Ownership or Control by the Government of a Country that is a State Sponsor of Terrorism.'
WHERE clause_version_id = 14 AND clause_name = '252.225-7050';

UPDATE Clauses
SET clause_title = 'Additional Access to Contractor and Subcontractor Records (Other Than USCENTCOM).' -- 'Additional Access to Contractor and Subcontractor Records (Other than USCENTCOM)'
WHERE clause_version_id = 14 AND clause_name = '252.225-7981 (DEVIATION 2015-00016)';

UPDATE Clauses
SET clause_title = 'Preference for Products or Services of Djibouti.' -- 'Preference for Products or Services of Djibouti'
WHERE clause_version_id = 14 AND clause_name = '252.225-7982 (DEVIATION 2015-00012)';

UPDATE Clauses
SET clause_title = 'Requirement for Products or Services of Djibouti.' -- 'Requirement for Products or Services of Djibouti'
WHERE clause_version_id = 14 AND clause_name = '252.225-7983 (DEVIATION 2015-00012)';

UPDATE Clauses
SET clause_title = 'Acquisition Restricted to Products or Services of Djibouti.' -- 'Acquisition Restricted to Products or Services of Djibouti'
WHERE clause_version_id = 14 AND clause_name = '252.225-7984 (DEVIATION 2015-00012)';

UPDATE Clauses
SET clause_title = 'Contractor Personnel Performing in Support of Operation United Assistance (OUA) in the United States Africa Command (AFRICOM) Theater of Operations.' -- 'Contractor Personnel Performing in Support of Operation United Assistance (OUA) in the United States Mrica Command (USAFRICOM) Theater of Operations'
WHERE clause_version_id = 14 AND clause_name = '252.225-7985 (DEVIATION 2015-00003)';

UPDATE Clauses
SET clause_title = 'Requirements for Contractor Personnel Performing in Ussouthcom Area of Responsibility.' -- 'REQUIREMENTS FOR CONTRACTOR PERSONNEL PERFORMING IN USSOUTHCOM AREA OF RESPONSIBILITY'
WHERE clause_version_id = 14 AND clause_name = '252.225-7987 (DEVIATION 2014-00016)';

UPDATE Clauses
SET clause_title = 'Requirements for Contractor Personnel Performing in Djibouti.' -- 'Requirements for Contractor Personnel Performing in Djibouti'
WHERE clause_version_id = 14 AND clause_name = '252.225-7989 (DEVIATION 2014-00005)';

UPDATE Clauses
SET clause_title = 'Preference for Products or Services From a Central Asian State or Afghanistan.' -- 'Preference for Products or Services from a Central Asian State or Afghanistan.'
WHERE clause_version_id = 14 AND clause_name = '252.225-7990 (DEVIATION 2014-00014)';

UPDATE Clauses
SET clause_title = 'Requirement for Products or Services From a Central Asian State or Afghanistan.' -- 'Requirement for Products or Services from a Central Asian State or Afghanistan'
WHERE clause_version_id = 14 AND clause_name = '252.225-7991 (DEVIATION 2014-00014)';

UPDATE Clauses
SET clause_title = 'Acquisition Restricted to Products or Services From a Central Asian State or Afghanistan.' -- 'Acquisition Restricted to Products or Services from a Central Asian State or Afghanistan'
WHERE clause_version_id = 14 AND clause_name = '252.225-7992 (DEVIATION 2014-00014)';

UPDATE Clauses
SET clause_title = 'Prohibition on Providing Funds to the Enemy.' -- 'Prohibition on Providing Funds to the Enemy'
WHERE clause_version_id = 14 AND clause_name = '252.225-7993 (DEVIATION 2015-00016)';

UPDATE Clauses
SET clause_title = 'Additional Access to Contractor and Subcontractor Records in the United States Central Command Theater of Operations.' -- 'Additional Access to Contractor and Subcontractor Records in the United States Central Command Theater of Operations'
WHERE clause_version_id = 14 AND clause_name = '252.225-7994 (DEVIATION 2015-00013)';

UPDATE Clauses
SET clause_title = 'Acquisition Restricted to Products or Services From a Central Asia, Pakistan, the South Caucasus, or Afghanistan.' -- 'Acquisition Restricted to Products or Services from a Central Asia, Pakistan, the South Caucasus, or Afghanistan'
WHERE clause_version_id = 14 AND clause_name = '252.225-7996 (DEVIATION 2014-00014)';

UPDATE Clauses
SET clause_title = 'Contractor Demobilization.' -- 'Contractor Demobilization. (DEVIATION 2013-00017)'
WHERE clause_version_id = 14 AND clause_name = '252.225-7997 (DEVIATION 2013-00017)';

UPDATE Clauses
SET clause_title = 'Preference for Products or Services From Central Asia, Pakistan, the South Caucasus or Afghanistan.' -- 'Preference for Products or Services from Central Asia, Pakistan, the South Caucasus or Afghanistan'
WHERE clause_version_id = 14 AND clause_name = '252.225-7998 (DEVIATION 2014-00014)';

UPDATE Clauses
SET clause_title = 'Requirement for Products or Services From Central Asia, Pakistan, the South Caucasus, or Afghanistan.' -- 'Requirement for Products or Services from Central Asia, Pakistan, the South Caucasus, or Afghanistan.'
WHERE clause_version_id = 14 AND clause_name = '252.225-7999 (DEVIATION 2014-00014)';

UPDATE Clauses
SET clause_title = 'Utilization of Indian Organizations, Indian-Owned Economic Enterprises, and Native Hawaiian Small Business Concerns.' -- 'Utilization of Indian organizations, Indian-owned economic enterprises, and native Hawaiian small business concerns.'
WHERE clause_version_id = 14 AND clause_name = '252.226-7001';

UPDATE Clauses
SET clause_title = 'Non-Estoppel.' -- 'Non-estoppel.'
WHERE clause_version_id = 14 AND clause_name = '252.227-7000';

UPDATE Clauses
SET clause_title = 'Release of Past Infringement.' -- 'Release of past infringement.'
WHERE clause_version_id = 14 AND clause_name = '252.227-7001';

UPDATE Clauses
SET clause_title = 'Readjustment of Payments.' -- 'Readjustment of payments.'
WHERE clause_version_id = 14 AND clause_name = '252.227-7002';

UPDATE Clauses
SET clause_title = 'License Grant.' -- 'License grant.'
WHERE clause_version_id = 14 AND clause_name = '252.227-7004';

UPDATE Clauses
SET clause_title = 'License Grant---Running Royalty.' -- 'License grant-running royalty.'
WHERE clause_version_id = 14 AND clause_name = '252.227-7006';

UPDATE Clauses
SET clause_title = 'License Term--Running Royalty.' -- 'License term-running royalty.'
WHERE clause_version_id = 14 AND clause_name = '252.227-7007';

UPDATE Clauses
SET clause_title = 'Computation of Royalties.' -- 'Computation of royalties.'
WHERE clause_version_id = 14 AND clause_name = '252.227-7008';

UPDATE Clauses
SET clause_title = 'Reporting and Payment of Royalties.' -- 'Reporting and payment of royalties.'
WHERE clause_version_id = 14 AND clause_name = '252.227-7009';

UPDATE Clauses
SET clause_title = 'License to Other Government Agencies.' -- 'License to other Government agencies.'
WHERE clause_version_id = 14 AND clause_name = '252.227-7010';

UPDATE Clauses
SET clause_title = 'Patent License and Release Contract.' -- 'Patent license and release contract.'
WHERE clause_version_id = 14 AND clause_name = '252.227-7012';

UPDATE Clauses
SET clause_title = 'Rights in Technical Data--Noncommercial Items.' -- 'Rights in technical data-Noncommercial items.'
WHERE clause_version_id = 14 AND clause_name = '252.227-7013';

UPDATE Clauses
SET clause_title = 'Rights in Noncommercial Computer Software and Noncommercial Computer Software Documentation.' -- 'Rights in noncommercial computer software and noncommercial computer software documentation.'
WHERE clause_version_id = 14 AND clause_name = '252.227-7014';

UPDATE Clauses
SET clause_title = 'Technical DataCommercial Items.' -- 'Technical data-Commercial items.'
WHERE clause_version_id = 14 AND clause_name = '252.227-7015';

UPDATE Clauses
SET clause_title = 'Rights in Bid or Proposal Information.' -- 'Rights in bid or proposal information.'
WHERE clause_version_id = 14 AND clause_name = '252.227-7016';

UPDATE Clauses
SET clause_title = 'Identification and Assertion of Use, Release, or Disclosure Restrictions.' -- 'Identification and assertion of use, release, or disclosure restrictions.'
WHERE clause_version_id = 14 AND clause_name = '252.227-7017';

UPDATE Clauses
SET clause_title = 'Rights in Noncommercial Technical Data and Computer Software--Small Business Innovation Research (SBIR) Program.' -- 'Rights in noncommercial technical data and computer software-Small Business Innovation Research (SBIR) Program.'
WHERE clause_version_id = 14 AND clause_name = '252.227-7018';

UPDATE Clauses
SET clause_title = 'Validation of Asserted Restrictions--Computer Software.' -- 'Validation of asserted restrictions-Computer software.'
WHERE clause_version_id = 14 AND clause_name = '252.227-7019';

UPDATE Clauses
SET clause_title = 'Rights in Special Works.' -- 'Rights in special works.'
WHERE clause_version_id = 14 AND clause_name = '252.227-7020';

UPDATE Clauses
SET clause_title = 'Rights in Data--Existing Works.' -- 'Rights in data-existing works.'
WHERE clause_version_id = 14 AND clause_name = '252.227-7021';

UPDATE Clauses
SET clause_title = 'Government Rights (Unlimited).' -- 'Government rights (unlimited).'
WHERE clause_version_id = 14 AND clause_name = '252.227-7022';

UPDATE Clauses
SET clause_title = 'Drawings and Other Data to Become Property of Government.' -- 'Drawings and other data to become property of Government.'
WHERE clause_version_id = 14 AND clause_name = '252.227-7023';

UPDATE Clauses
SET clause_title = 'Notice and Approval of Restricted Designs.' -- 'Notice and approval of restricted designs.'
WHERE clause_version_id = 14 AND clause_name = '252.227-7024';

UPDATE Clauses
SET clause_title = 'Limitations on the Use or Disclosure of Government-Furnished Information Marked With Restrictive Legends.' -- 'Limitations on the Use or Disclosure of Government-Furnished Information Marked with Restrictive Legends.'
WHERE clause_version_id = 14 AND clause_name = '252.227-7025';

UPDATE Clauses
SET clause_title = 'Deferred Delivery of Technical Data or Computer Software.' -- 'Deferred delivery of technical data or computer software.'
WHERE clause_version_id = 14 AND clause_name = '252.227-7026';

UPDATE Clauses
SET clause_title = 'Deferred Ordering of Technical Data or Computer Software.' -- 'Deferred ordering of technical data or computer software.'
WHERE clause_version_id = 14 AND clause_name = '252.227-7027';

UPDATE Clauses
SET clause_title = 'Technical Data or Computer Software Previously Delivered to the Government.' -- 'Technical data or computer software previously delivered to the government.'
WHERE clause_version_id = 14 AND clause_name = '252.227-7028';

UPDATE Clauses
SET clause_title = 'Technical Data--Withholding of Payment.' -- 'Technical data-withholding of payment.'
WHERE clause_version_id = 14 AND clause_name = '252.227-7030';

UPDATE Clauses
SET clause_title = 'Rights in Technical Data and Computer Software (Foreign).' -- 'Rights in technical data and computer software (foreign).'
WHERE clause_version_id = 14 AND clause_name = '252.227-7032';

UPDATE Clauses
SET clause_title = 'Rights in Shop Drawings.' -- 'Rights in shop drawings.'
WHERE clause_version_id = 14 AND clause_name = '252.227-7033';

UPDATE Clauses
SET clause_title = 'Validation of Restrictive Markings on Technical Data.' -- 'Validation of restrictive markings on technical data.'
WHERE clause_version_id = 14 AND clause_name = '252.227-7037';

UPDATE Clauses
SET clause_title = 'Patents--Reporting of Subject Inventions.' -- 'Patents-reporting of subject inventions.'
WHERE clause_version_id = 14 AND clause_name = '252.227-7039';

UPDATE Clauses
SET clause_title = 'Reimbursement for War-Hazard Losses.' -- 'Reimbursement for war-hazard losses.'
WHERE clause_version_id = 14 AND clause_name = '252.228-7000';

UPDATE Clauses
SET clause_title = 'Ground and Flight Risk.' -- 'Ground and flight risk.'
WHERE clause_version_id = 14 AND clause_name = '252.228-7001';

UPDATE Clauses
SET clause_title = 'Capture and Detention.' -- 'Capture and detention.'
WHERE clause_version_id = 14 AND clause_name = '252.228-7003';

UPDATE Clauses
SET clause_title = 'Bonds or Other Security.' -- 'Bonds or other security.'
WHERE clause_version_id = 14 AND clause_name = '252.228-7004';

UPDATE Clauses
SET clause_title = 'Accident Reporting and Investigation Involving Aircraft, Missiles, and Space Launch Vehicles.' -- 'Accident reporting and investigation involving aircraft, missiles, and space launch vehicles.'
WHERE clause_version_id = 14 AND clause_name = '252.228-7005';

UPDATE Clauses
SET clause_title = 'Compliance With Spanish Laws and Insurance.' -- 'Compliance with Spanish laws and insurance.'
WHERE clause_version_id = 14 AND clause_name = '252.228-7006';

UPDATE Clauses
SET clause_title = 'Invoices Exclusive of Taxes or Duties.' -- 'Invoices exclusive of taxes or duties.'
WHERE clause_version_id = 14 AND clause_name = '252.229-7000';

UPDATE Clauses
SET clause_title = 'Customs Exemptions (Germany).' -- 'Customs exemptions (Germany).'
WHERE clause_version_id = 14 AND clause_name = '252.229-7002';

UPDATE Clauses
SET clause_title = 'Status of Contractor As a Direct Contractor (Spain).' -- 'Status of contractors as a direct contractor (Spain).'
WHERE clause_version_id = 14 AND clause_name = '252.229-7004';

UPDATE Clauses
SET clause_title = 'Tax Exemptions (Spain).' -- 'Tax exemptions (Spain).'
WHERE clause_version_id = 14 AND clause_name = '252.229-7005';

UPDATE Clauses
SET clause_title = 'Value Added Tax Exclusion (United Kingdom).' -- 'Value Added Tax Exclusion (United Kingdom)'
WHERE clause_version_id = 14 AND clause_name = '252.229-7006';

UPDATE Clauses
SET clause_title = 'Verification of United States Receipt of Goods.' -- 'Verification of United States receipt of goods.'
WHERE clause_version_id = 14 AND clause_name = '252.229-7007';

UPDATE Clauses
SET clause_title = 'Relief From Import Duty (United Kingdom).' -- 'Relief from Import Duty (United Kingdom)'
WHERE clause_version_id = 14 AND clause_name = '252.229-7008';

UPDATE Clauses
SET clause_title = 'Relief From Customs Duty and Value Added Tax on Fuel (Passenger Vehicles) (United Kingdom).' -- 'Relief from customs duty and value added tax on fuel (passenger vehicles) (United Kingdom).'
WHERE clause_version_id = 14 AND clause_name = '252.229-7009';

UPDATE Clauses
SET clause_title = 'Relief From Customs Duty on Fuel (United Kingdom).' -- 'Relief from customs duty on fuel (United Kingdom).'
WHERE clause_version_id = 14 AND clause_name = '252.229-7010';

UPDATE Clauses
SET clause_title = 'Reporting of Foreign Taxes U.S. Assistance Programs.' -- 'Reporting of Foreign Taxes-U.S. Assistance Programs.'
WHERE clause_version_id = 14 AND clause_name = '252.229-7011';

UPDATE Clauses
SET clause_title = 'Tax Exemptions (Italy)-Representation.' -- 'Tax exemptions (Italy)-representation.'
WHERE clause_version_id = 14 AND clause_name = '252.229-7012';

UPDATE Clauses
SET clause_title = 'Tax Exemptions (Spain)-Representation.' -- 'Tax exemptions (Spain)-representation.'
WHERE clause_version_id = 14 AND clause_name = '252.229-7013';

UPDATE Clauses
SET clause_title = 'Taxes Foreign Contracts in Afghanistan (Military Technical Agreement).' -- 'Taxes - Foreign Contracts in Afghanistan (Military Technical Agreement).'
WHERE clause_version_id = 14 AND clause_name = '252.229-7998 (DEVIATION 2013-00016)';

UPDATE Clauses
SET clause_title = 'Taxes Foreign Contracts in Afghanistan.' -- 'Taxes - Foreign Contracts in Afghanistan.'
WHERE clause_version_id = 14 AND clause_name = '252.229-7999 (DEVIATION 2013-00016)';

UPDATE Clauses
SET clause_title = 'Supplemental Cost Principles.' -- 'Supplemental cost principles.'
WHERE clause_version_id = 14 AND clause_name = '252.231-7000';

UPDATE Clauses
SET clause_title = 'Advance Payment Pool.' -- 'Advance payment pool.'
WHERE clause_version_id = 14 AND clause_name = '252.232-7000';

UPDATE Clauses
SET clause_title = 'Disposition of Payments.' -- 'Disposition of payments.'
WHERE clause_version_id = 14 AND clause_name = '252.232-7001';

UPDATE Clauses
SET clause_title = 'Progress Payments for Foreign Military Sales Acquisitions.' -- 'Progress payments for foreign military sales acquisitions.'
WHERE clause_version_id = 14 AND clause_name = '252.232-7002';

UPDATE Clauses
SET clause_title = 'Electronic Submission of Payment Requests and Receiving Reports.' -- 'Electronic submission of payment requests and receiving reports.'
WHERE clause_version_id = 14 AND clause_name = '252.232-7003';

UPDATE Clauses
SET clause_title = 'DOD Progress Payment Rates.' -- 'DoD Progress Payment Rates.'
WHERE clause_version_id = 14 AND clause_name = '252.232-7004';

UPDATE Clauses
SET clause_title = 'Reimbursement of Subcontractor Advance Payments--DOD Pilot Mentor-Protege Program.' -- 'Reimbursement of subcontractor advance payments-DoD pilot mentor-protege program.'
WHERE clause_version_id = 14 AND clause_name = '252.232-7005';

UPDATE Clauses
SET clause_title = 'Wide Area Workflow Payment Instructions.' -- 'Wide Area WorkFlow Payment Instructions.'
WHERE clause_version_id = 14 AND clause_name = '252.232-7006';

UPDATE Clauses
SET clause_title = 'Limitation of GovernmentS Obligation.' -- 'Limitation of Government''s obligation.'
WHERE clause_version_id = 14 AND clause_name = '252.232-7007';

UPDATE Clauses
SET clause_title = 'Assignment of Claims (Overseas).' -- 'Assignment of claims (overseas).'
WHERE clause_version_id = 14 AND clause_name = '252.232-7008';

UPDATE Clauses
SET clause_title = 'Mandatory Payment by Governmentwide Commercial Purchase Card.' -- 'Mandatory payment by Governmentwide commercial purchase card.'
WHERE clause_version_id = 14 AND clause_name = '252.232-7009';

UPDATE Clauses
SET clause_title = 'Performance-Based PaymentsWhole-Contract Basis.' -- 'Performance-Based Payments-Whole-Contract Basis.'
WHERE clause_version_id = 14 AND clause_name = '252.232-7012';

UPDATE Clauses
SET clause_title = 'Choice of Law (Overseas).' -- 'Choice of law (overseas).'
WHERE clause_version_id = 14 AND clause_name = '252.233-7001';

UPDATE Clauses
SET clause_title = 'Notice of Earned Value Management System.' -- 'Notice Of Earned Value Management System'
WHERE clause_version_id = 14 AND clause_name = '252.234-7001 (DEVIATION 2015-00017)';

UPDATE Clauses
SET clause_title = 'Earned Value Management System.' -- 'Earned Value Management System'
WHERE clause_version_id = 14 AND clause_name = '252.234-7002 (DEVIATION 2015-00017)';

UPDATE Clauses
SET clause_title = 'Indemnification Under 10 U.S.C. 2354--Fixed Price.' -- 'Indemnification under 10 U.S.C. 2354-fixed price.'
WHERE clause_version_id = 14 AND clause_name = '252.235-7000';

UPDATE Clauses
SET clause_title = 'Indemnification Under 10 U.S.C. 2354--Cost Reimbursement.' -- 'Indemnification under 10 U.S.C. 2354-cost reimbursement.'
WHERE clause_version_id = 14 AND clause_name = '252.235-7001';

UPDATE Clauses
SET clause_title = 'Animal Welfare.' -- 'Animal welfare.'
WHERE clause_version_id = 14 AND clause_name = '252.235-7002';

UPDATE Clauses
SET clause_title = 'Frequency Authorization.' -- 'Frequency authorization.'
WHERE clause_version_id = 14 AND clause_name = '252.235-7003';

UPDATE Clauses
SET clause_title = 'Acknowledgment of Support and Disclaimer.' -- 'Acknowledgment of support and disclaimer.'
WHERE clause_version_id = 14 AND clause_name = '252.235-7010';

UPDATE Clauses
SET clause_title = 'Final Scientific or Technical Report.' -- 'Final scientific or technical report.'
WHERE clause_version_id = 14 AND clause_name = '252.235-7011';

UPDATE Clauses
SET clause_title = 'Modification Proposals--Price Breakdown.' -- 'Modification proposals-price breakdown.'
WHERE clause_version_id = 14 AND clause_name = '252.236-7000';

UPDATE Clauses
SET clause_title = 'Contract Drawings and Specifications.' -- 'Contract drawings and specifications.'
WHERE clause_version_id = 14 AND clause_name = '252.236-7001';

UPDATE Clauses
SET clause_title = 'Obstruction of Navigable Waterways.' -- 'Obstruction of navigable waterways.'
WHERE clause_version_id = 14 AND clause_name = '252.236-7002';

UPDATE Clauses
SET clause_title = 'Payment for Mobilization and Preparatory Work.' -- 'Payment for mobilization and preparatory work.'
WHERE clause_version_id = 14 AND clause_name = '252.236-7003';

UPDATE Clauses
SET clause_title = 'Payment for Mobilization and Demobilization.' -- 'Payment for mobilization and demobilization.'
WHERE clause_version_id = 14 AND clause_name = '252.236-7004';

UPDATE Clauses
SET clause_title = 'Airfield Safety Precautions.' -- 'Airfield safety precautions.'
WHERE clause_version_id = 14 AND clause_name = '252.236-7005';

UPDATE Clauses
SET clause_title = 'Cost Limitation.' -- 'Cost limitation.'
WHERE clause_version_id = 14 AND clause_name = '252.236-7006';

UPDATE Clauses
SET clause_title = 'Additive or Deductive Items.' -- 'Additive or deductive items.'
WHERE clause_version_id = 14 AND clause_name = '252.236-7007';

UPDATE Clauses
SET clause_title = 'Contract Prices--Bidding Schedules.' -- 'Contract prices-bidding schedules.'
WHERE clause_version_id = 14 AND clause_name = '252.236-7008';

UPDATE Clauses
SET clause_title = 'Option for Supervision and Inspection Services.' -- 'Option for supervision and inspection services.'
WHERE clause_version_id = 14 AND clause_name = '252.236-7009';

UPDATE Clauses
SET clause_title = 'Overseas Military Construction--Preference for United States Firms.' -- 'Overseas military construction-Preference for United States firms.'
WHERE clause_version_id = 14 AND clause_name = '252.236-7010';

UPDATE Clauses
SET clause_title = 'Overseas Architect-Engineer Services--Restriction to United States Firms.' -- 'Overseas architect-engineer services-Restriction to United States firms.'
WHERE clause_version_id = 14 AND clause_name = '252.236-7011';

UPDATE Clauses
SET clause_title = 'Military Construction on Kwajalein Atoll--Evaluation Preference.' -- 'Military construction on Kwajalein Atoll-evaluation preference.'
WHERE clause_version_id = 14 AND clause_name = '252.236-7012';

UPDATE Clauses
SET clause_title = 'Requirement for Competition Opportunity for American Steel Producers, Fabricators, and Manufacturers.' -- 'Requirement for competition opportunity for American steel producers, fabricators, and manufacturers.'
WHERE clause_version_id = 14 AND clause_name = '252.236-7013';

UPDATE Clauses
SET clause_title = 'Notice of Special Standards of Responsibility.' -- 'Notice of special standards of responsibility.'
WHERE clause_version_id = 14 AND clause_name = '252.237-7000';

UPDATE Clauses
SET clause_title = 'Compliance With Audit Standards.' -- 'Compliance with audit standards.'
WHERE clause_version_id = 14 AND clause_name = '252.237-7001';

UPDATE Clauses
SET clause_title = 'Award to Single Offeror.' -- 'Award to single offeror.'
WHERE clause_version_id = 14 AND clause_name = '252.237-7002';

UPDATE Clauses
SET clause_title = 'Area of Performance.' -- 'Area of performance.'
WHERE clause_version_id = 14 AND clause_name = '252.237-7004';

UPDATE Clauses
SET clause_title = 'Performance and Delivery.' -- 'Performance and delivery.'
WHERE clause_version_id = 14 AND clause_name = '252.237-7005';

UPDATE Clauses
SET clause_title = 'Termination for Default.' -- 'Termination for default.'
WHERE clause_version_id = 14 AND clause_name = '252.237-7007';

UPDATE Clauses
SET clause_title = 'Group Interment.' -- 'Group interment.'
WHERE clause_version_id = 14 AND clause_name = '252.237-7008';

UPDATE Clauses
SET clause_title = 'Prohibition on Interrogation of Detainees by Contractor Personnel.' -- 'Prohibition on interrogation of detainees by contractor personnel.'
WHERE clause_version_id = 14 AND clause_name = '252.237-7010';

UPDATE Clauses
SET clause_title = 'Preparation History.' -- 'Preparation history.'
WHERE clause_version_id = 14 AND clause_name = '252.237-7011';

UPDATE Clauses
SET clause_title = 'Instruction to Offerors (Count-of-Articles).' -- 'Instruction to offerors (count-of-articles).'
WHERE clause_version_id = 14 AND clause_name = '252.237-7012';

UPDATE Clauses
SET clause_title = 'Instruction to Offerors (Bulk Weight).' -- 'Instruction to offerors (bulk weight).'
WHERE clause_version_id = 14 AND clause_name = '252.237-7013';

UPDATE Clauses
SET clause_title = 'Loss or Damage (Count-of-Articles).' -- 'Loss or damage (count-of-articles).'
WHERE clause_version_id = 14 AND clause_name = '252.237-7014';

UPDATE Clauses
SET clause_title = 'Loss or Damage (Weight of Articles).' -- 'Loss or damage (weight of articles).'
WHERE clause_version_id = 14 AND clause_name = '252.237-7015';

UPDATE Clauses
SET clause_title = 'Delivery Tickets.' -- 'Delivery tickets.'
WHERE clause_version_id = 14 AND clause_name = '252.237-7016';

UPDATE Clauses
SET clause_title = 'Individual Laundry.' -- 'Individual laundry.'
WHERE clause_version_id = 14 AND clause_name = '252.237-7017';

UPDATE Clauses
SET clause_title = 'Special Definitions of Government Property.' -- 'Special definitions of Government property.'
WHERE clause_version_id = 14 AND clause_name = '252.237-7018';

UPDATE Clauses
SET clause_title = 'Training for Contractor Personnel Interacting With Detainees.' -- 'Training for Contractor Personnel Interacting with Detainees.'
WHERE clause_version_id = 14 AND clause_name = '252.237-7019';

UPDATE Clauses
SET clause_title = 'Services At Installations Being Closed.' -- 'Services at installations being closed.'
WHERE clause_version_id = 14 AND clause_name = '252.237-7022';

UPDATE Clauses
SET clause_title = 'Protection Against Compromising Emanations.' -- 'Protection against compromising emanations.'
WHERE clause_version_id = 14 AND clause_name = '252.239-7000';

UPDATE Clauses
SET clause_title = 'Orders for Facilities and Services.' -- 'Orders for facilities and services.'
WHERE clause_version_id = 14 AND clause_name = '252.239-7004';

UPDATE Clauses
SET clause_title = 'Rates, Charges, and Services.' -- 'Rates, charges, and services.'
WHERE clause_version_id = 14 AND clause_name = '252.239-7005';

UPDATE Clauses
SET clause_title = 'Tariff Information.' -- 'Tariff information.'
WHERE clause_version_id = 14 AND clause_name = '252.239-7006';

UPDATE Clauses
SET clause_title = 'Cancellation or Termination of Orders.' -- 'Cancellation or termination of orders.'
WHERE clause_version_id = 14 AND clause_name = '252.239-7007';

UPDATE Clauses
SET clause_title = 'Reuse Arrangements.' -- 'Reuse arrangements.'
WHERE clause_version_id = 14 AND clause_name = '252.239-7008';

UPDATE Clauses
SET clause_title = 'Representation of Use of Cloud Computing.' -- 'Representation of use of cloud computing.'
WHERE clause_version_id = 14 AND clause_name = '252.239-7009';
INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT clause_id, prescription_id FROM Prescriptions, Clauses
WHERE prescription_name IN ('239.7604(a)') 
AND clause_id IN (SELECT clause_id FROM Clauses WHERE clause_name = '252.239-7009' AND clause_version_id = 14); 

UPDATE Clauses
SET clause_title = 'Cloud Computing Services.' -- 'Cloud computing services.'
WHERE clause_version_id = 14 AND clause_name = '252.239-7010';
INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT clause_id, prescription_id FROM Prescriptions, Clauses
WHERE prescription_name IN ('239.7604(b)') 
AND clause_id IN (SELECT clause_id FROM Clauses WHERE clause_name = '252.239-7010' AND clause_version_id = 14); 

UPDATE Clauses
SET clause_title = 'Special Construction and Equipment Charges.' -- 'Special construction and equipment charges.'
WHERE clause_version_id = 14 AND clause_name = '252.239-7011';

UPDATE Clauses
SET clause_title = 'Title to Telecommunication Facilities and Equipment.' -- 'Title to telecommunication facilities and equipment.'
WHERE clause_version_id = 14 AND clause_name = '252.239-7012';

UPDATE Clauses
SET clause_title = 'Term of Agreement.' -- 'Term of agreement.'
WHERE clause_version_id = 14 AND clause_name = '252.239-7014';

UPDATE Clauses
SET clause_title = 'Continuation of Communication Service Authorizations.' -- 'Continuation of communication service authorizations.'
WHERE clause_version_id = 14 AND clause_name = '252.239-7015';

UPDATE Clauses
SET clause_title = 'Telecommunications Security Equipment, Devices, Techniques, and Services.' -- 'Telecommunications security equipment, devices, techniques, and services.'
WHERE clause_version_id = 14 AND clause_name = '252.239-7016';

UPDATE Clauses
SET clause_title = 'Notice of Supply Chain Risk.' -- 'Notice of supply chain risk.'
WHERE clause_version_id = 14 AND clause_name = '252.239-7017';

UPDATE Clauses
SET clause_title = 'Supply Chain Risk.' -- 'Supply chain risk.'
WHERE clause_version_id = 14 AND clause_name = '252.239-7018';

UPDATE Clauses
SET clause_title = 'Superseding Contract.' -- 'Superseding contract.'
WHERE clause_version_id = 14 AND clause_name = '252.241-7000';

UPDATE Clauses
SET clause_title = 'Government Access.' -- 'Government access.'
WHERE clause_version_id = 14 AND clause_name = '252.241-7001';

UPDATE Clauses
SET clause_title = 'Contractor Business Systems.' -- 'Contractor business systems.'
WHERE clause_version_id = 14 AND clause_name = '252.242-7005';

UPDATE Clauses
SET clause_title = 'Accounting System Administration.' -- 'Accounting system administration.'
WHERE clause_version_id = 14 AND clause_name = '252.242-7006';

UPDATE Clauses
SET clause_title = 'Pricing of Contract Modifications.' -- 'Pricing of contract modifications.'
WHERE clause_version_id = 14 AND clause_name = '252.243-7001';

UPDATE Clauses
SET clause_title = 'Requests for Equitable Adjustment.' -- 'Requests for equitable adjustment.'
WHERE clause_version_id = 14 AND clause_name = '252.243-7002';

UPDATE Clauses
SET clause_title = 'Contractor Purchasing System Administration-Basic.' -- 'Contractor purchasing system administration.'
WHERE clause_version_id = 14 AND clause_name = '252.244-7001';

UPDATE Clauses
SET clause_title = 'Government-Furnished Mapping, Charting, and Geodesy Property.' -- 'Government-furnished mapping, charting, and geodesy property.'
WHERE clause_version_id = 14 AND clause_name = '252.245-7000';

UPDATE Clauses
SET clause_title = 'Tagging, Labeling, and Marking of Government-Furnished Property.' -- 'Tagging, labeling, and marking of government-furnished property'
WHERE clause_version_id = 14 AND clause_name = '252.245-7001';

UPDATE Clauses
SET clause_title = 'Material Inspection and Receiving Report.' -- 'Material inspection and receiving report.'
WHERE clause_version_id = 14 AND clause_name = '252.246-7000';

UPDATE Clauses
SET clause_title = 'Warranty of Data-Basic.' -- 'Warranty of data.'
WHERE clause_version_id = 14 AND clause_name = '252.246-7001';

UPDATE Clauses
SET clause_title = 'Warranty of Construction (Germany).' -- 'Warranty of construction (Germany).'
WHERE clause_version_id = 14 AND clause_name = '252.246-7002';

UPDATE Clauses
SET clause_title = 'Hardship Conditions.' -- 'Hardship conditions.'
WHERE clause_version_id = 14 AND clause_name = '252.247-7000';

UPDATE Clauses
SET clause_title = 'Price Adjustment.' -- 'Price adjustment.'
WHERE clause_version_id = 14 AND clause_name = '252.247-7001';

UPDATE Clauses
SET clause_title = 'Revision of Prices.' -- 'Revision of prices.'
WHERE clause_version_id = 14 AND clause_name = '252.247-7002';

UPDATE Clauses
SET clause_title = 'Pass-Through of Motor Carrier Fuel Surcharge Adjustment to the Cost Bearer.' -- 'Pass-Through of Motor Carrier Fuel Surcharge Adjustment To The Cost Bearer.'
WHERE clause_version_id = 14 AND clause_name = '252.247-7003';

UPDATE Clauses
SET clause_title = 'Indefinite Quantities--Fixed Charges.' -- 'Indefinite quantities-fixed charges.'
WHERE clause_version_id = 14 AND clause_name = '252.247-7004';

UPDATE Clauses
SET clause_title = 'Indefinite Quantities--No Fixed Charges.' -- 'Indefinite quantities-no fixed charges.'
WHERE clause_version_id = 14 AND clause_name = '252.247-7005';

UPDATE Clauses
SET clause_title = 'Removal of Contractor''s Employees.' -- 'Removal of contractor''s employees.'
WHERE clause_version_id = 14 AND clause_name = '252.247-7006';

UPDATE Clauses
SET clause_title = 'Liability and Insurance.' -- 'Liability and insurance.'
WHERE clause_version_id = 14 AND clause_name = '252.247-7007';

UPDATE Clauses
SET clause_title = 'Evaluation of Bids-Basic.' -- 'Evaluation of bids.'
WHERE clause_version_id = 14 AND clause_name = '252.247-7008';

UPDATE Clauses
SET clause_title = 'Scope of Contract.' -- 'Scope of contract.'
WHERE clause_version_id = 14 AND clause_name = '252.247-7010';

UPDATE Clauses
SET clause_title = 'Period of Contract.' -- 'Period of contract.'
WHERE clause_version_id = 14 AND clause_name = '252.247-7011';

UPDATE Clauses
SET clause_title = 'Ordering Limitation.' -- 'Ordering limitation.'
WHERE clause_version_id = 14 AND clause_name = '252.247-7012';

UPDATE Clauses
SET clause_title = 'Contract Areas of Performance.' -- 'Contract areas of performance.'
WHERE clause_version_id = 14 AND clause_name = '252.247-7013';

UPDATE Clauses
SET clause_title = 'Contractor Liability for Loss or Damage.' -- 'Contractor liability for loss or damage.'
WHERE clause_version_id = 14 AND clause_name = '252.247-7016';

UPDATE Clauses
SET clause_title = 'Erroneous Shipments.' -- 'Erroneous shipments.'
WHERE clause_version_id = 14 AND clause_name = '252.247-7017';

UPDATE Clauses
SET clause_title = 'Additional Services.' -- 'Additional services.'
WHERE clause_version_id = 14 AND clause_name = '252.247-7020';

UPDATE Clauses
SET clause_title = 'Returnable Containers Other Than Cylinders.' -- 'Returnable containers other than cylinders.'
WHERE clause_version_id = 14 AND clause_name = '252.247-7021';

UPDATE Clauses
SET clause_title = 'Representation of Extent of Transportation by Sea.' -- 'Representation of extent of transportation by sea.'
WHERE clause_version_id = 14 AND clause_name = '252.247-7022';

UPDATE Clauses
SET clause_title = 'Transportation of Supplies by Sea-Basic.' -- 'Transportation of supplies by sea.'
WHERE clause_version_id = 14 AND clause_name = '252.247-7023';

UPDATE Clauses
SET clause_title = 'Notification of Transportation of Supplies by Sea.' -- 'Notification of transportation of supplies by sea.'
WHERE clause_version_id = 14 AND clause_name = '252.247-7024';

UPDATE Clauses
SET clause_title = 'Reflagging or Repair Work.' -- 'Reflagging or repair work.'
WHERE clause_version_id = 14 AND clause_name = '252.247-7025';

UPDATE Clauses
SET clause_title = 'Evaluation Preference for Use of Domestic Shipyards - Applicable to Acquisition of Carriage by Vessel for DOD Cargo in the Coastwise or Noncontiguous Trade.' -- 'Evaluation Preference for Use of Domestic Shipyards-Applicable to Acquisition of Carriage by Vessel for DoD Cargo in the Coastwise or Noncontiguous Trade.'
WHERE clause_version_id = 14 AND clause_name = '252.247-7026';

UPDATE Clauses
SET clause_title = 'Riding Gang Member Requirements.' -- 'Riding gang member requirements.'
WHERE clause_version_id = 14 AND clause_name = '252.247-7027';

UPDATE Clauses
SET clause_title = 'Special Termination Costs.' -- 'Special termination costs.'
WHERE clause_version_id = 14 AND clause_name = '252.249-7000';

UPDATE Clauses
SET clause_title = 'Notification of Anticipated Contract Termination or Reduction.' -- 'Notification of anticipated contract termination or reduction.'
WHERE clause_version_id = 14 AND clause_name = '252.249-7002';

UPDATE Clauses
SET clause_title = 'Ordering From Government Supply Sources.' -- 'Ordering from Government supply sources.'
WHERE clause_version_id = 14 AND clause_name = '252.251-7000';

UPDATE Clauses
SET clause_title = 'Use of Interagency Fleet Management System (IFMS) Vehicles and Related Services.' -- 'Use of Interagency Fleet Management System (IFMS) vehicles and related services.'
WHERE clause_version_id = 14 AND clause_name = '252.251-7001';

UPDATE Clauses
SET clause_title = 'Whistleblower Protections Under The American Recovery and Reinvestment Act of 2009.' -- 'Whistleblower Protections Under the American Recovery and Reinvestment Act of 2009'
WHERE clause_version_id = 14 AND clause_name = '52.203-15';

UPDATE Clauses
SET clause_title = 'Contractor Employee Whistleblower Rights and Requirement to Inform Employees of Whistleblower Rights.' -- 'Contractor Employee Whistleblower Rights and Requirement To Inform Employees of Whistleblower Rights.'
WHERE clause_version_id = 14 AND clause_name = '52.203-17';

UPDATE Clauses
SET clause_title = 'Cancellation, Recession, and Recovery of Funds for Illegal or Improper Activity.' -- 'Cancellation, Rescission, and Recovery of Funds for Illegal or Improper Activity.'
WHERE clause_version_id = 14 AND clause_name = '52.203-8';

UPDATE Clauses
SET clause_title = 'Ownership or Control of Contractor.' -- 'Ownership or Control of Offeror.'
WHERE clause_version_id = 14 AND clause_name = '52.204-17';

UPDATE Clauses
SET clause_title = 'Incorporation by Reference of Representations And Certifications.' -- 'Incorporation by Reference of Representations and Certifications.'
WHERE clause_version_id = 14 AND clause_name = '52.204-19';

UPDATE Clauses
SET clause_title = 'Taxpayer Identification.' -- 'Taxpayer identification.'
WHERE clause_version_id = 14 AND clause_name = '52.204-3';



UPDATE Clause_Fill_Ins SET fill_in_default_data = '<p>(a)(1) The North American Industry Classification System (NAICS) code for this acquisition is {{textbox_52.204-8[0]}} [<i>insert NAICS code</i>].</p><p>(2) The small business size standard is {{textbox_52.204-8[1]}} [<i>insert size standard</i>].</p><p>(3) The small business size standard for a concern which submits an offer in its own name, other than on a construction or service contract, but which proposes to furnish a product which it did not itself manufacture, is 500 employees.3</p><p>(b)(1) If the provision at 52.204-7, System for Award Management, is included in this solicitation, paragraph (d) of this provision applies.</p><p>(2) If the provision at 52.204-7 is not included in this solicitation, and the offeror is currently registered in the System for Award Management (SAM), and has completed the Representations and Certifications section of SAM electronically, the offeror may choose to use paragraph (d) of this provision instead of completing the corresponding individual representations and certifications in the solicitation. The offeror shall indicate which option applies by checking one of the following boxes:</p><p>[&nbsp;&nbsp;] (i) Paragraph (d) applies.</p><p>[&nbsp;&nbsp;] (ii) Paragraph (d) does not apply and the offeror has completed the individual representations and certifications in the solicitation.</p><p>(c)(1) The following representations or certifications in SAM are applicable to this solicitation as indicated:</p><p>(i) 52.203-2, Certificate of Independent Price Determination. This provision applies to solicitations when a firm-fixed-price contract or fixed-price contract with economic price adjustment is contemplated, unless&mdash;</p><p>(A) The acquisition is to be made under the simplified acquisition procedures in Part 13;</p><p>(B) The solicitation is a request for technical proposals under two-step sealed bidding procedures; or</p><p>(C) The solicitation is for utility services for which rates are set by law or regulation.</p><p>(ii) 52.203-11, Certification and Disclosure Regarding Payments to Influence Certain Federal Transactions. This provision applies to solicitations expected to exceed $150,000.</p><p>(iii) 52.204-3, Taxpayer Identification. This provision applies to solicitations that do not include provision at 52.204-7, System for Award Management.</p><p>(iv) 52.204-5, Women-Owned Business (Other Than Small Business). This provision applies to solicitations that&mdash;</p><p>(A) Are not set aside for small business concerns;</p><p>(B) Exceed the simplified acquisition threshold; and</p><p>(C) Are for contracts that will be performed in the United States or its outlying areas.</p><p>(v) 52.209-2, Prohibition on Contracting with Inverted Domestic Corporations&mdash;Representation.</p><p>(vi) 52.209-5, Certification Regarding Responsibility Matters. This provision applies to solicitations where the contract value is expected to exceed the simplified acquisition threshold.</p><p>(vii) 52.214-14, Place of Performance&mdash;Sealed Bidding. This provision applies to invitations for bids except those in which the place of performance is specified by the Government.</p><p>(viii) 52.215-6, Place of Performance. This provision applies to solicitations unless the place of performance is specified by the Government.</p><p>(ix) 52.219-1, Small Business Program Representations (Basic &amp; Alternate I). This provision applies to solicitations when the contract will be performed in the United States or its outlying areas.</p><p>(A) The basic provision applies when the solicitations are issued by other than DoD, NASA, and the Coast Guard.</p><p>(B) The provision with its Alternate I applies to solicitations issued by DoD, NASA, or the Coast Guard.</p><p>(x) 52.219-2, Equal Low Bids. This provision applies to solicitations when contracting by sealed bidding and the contract will be performed in the United States or its outlying areas.</p><p>(xi) 52.222-22, Previous Contracts and Compliance Reports. This provision applies to solicitations that include the clause at 52.222-26, Equal Opportunity.</p><p>(xii) 52.222-25, Affirmative Action Compliance. This provision applies to solicitations, other than those for construction, when the solicitation includes the clause at 52.222-26, Equal Opportunity.</p><p>(xiii) 52.222-38, Compliance with Veterans'' Employment Reporting Requirements. This provision applies to solicitations when it is anticipated the contract award will exceed the simplified acquisition threshold and the contract is not for acquisition of commercial items.</p><p>(xiv) 52.223-1, Biobased Product Certification. This provision applies to solicitations that require the delivery or specify the use of USDA-designated items; or include the clause at 52.223-2, Affirmative Procurement of Biobased Products Under Service and Construction Contracts.</p><p>(xv) 52.223-4, Recovered Material Certification. This provision applies to solicitations that are for, or specify the use of, EPA-designated items.</p><p>(xvi) 52.225-2, Buy American Certificate. This provision applies to solicitations containing the clause at 52.225-1.</p><p>(xvii) 52.225-4, Buy American&mdash;Free Trade Agreements&mdash;Israeli Trade Act Certificate. (Basic, Alternates I, II, and III.) This provision applies to solicitations containing the clause at 52.225-3.</p><p>(A) If the acquisition value is less than $25,000, the basic provision applies.</p><p>(B) If the acquisition value is $25,000 or more but is less than $50,000, the provision with its Alternate I applies.</p><p>(C) If the acquisition value is $50,000 or more but is less than $77,533, the provision with its Alternate II applies.</p><p>(D) If the acquisition value is $77,533 or more but is less than $100,000, the provision with its Alternate III applies.</p><p>(xviii) 52.225-6, Trade Agreements Certificate. This provision applies to solicitations containing the clause at 52.225-5.</p><p>(xix) 52.225-20, Prohibition on Conducting Restricted Business Operations in Sudan&mdash;Certification. This provision applies to all solicitations.</p><p>(xx) 52.225-25, Prohibition on Contracting with Entities Engaging in Certain Activities or Transactions Relating to Iran&mdash;Representation and Certifications. This provision applies to all solicitations.</p><p>(xxi) 52.226-2, Historically Black College or University and Minority Institution Representation. This provision applies to solicitations for research, studies, supplies, or services of the type normally acquired from higher educational institutions.</p><p>(2) The following representations or certifications are applicable as indicated by the Contracting Officer:</p><p>[<i>Contracting Officer check as appropriate.</i>]</p><p>{{checkbox_52.204-8[0]}} (i) 52.204-17, Ownership or Control of Offeror.</p><p>{{checkbox_52.204-8[1]}} (ii) 52.222-18, Certification Regarding Knowledge of Child Labor for Listed End Products.</p><p>{{checkbox_52.204-8[2]}} (iii) 52.222-48, Exemption from Application of the Service Contract Labor Standards to Contracts for Maintenance, Calibration, or Repair of Certain Equipment&mdash;Certification.</p><p>{{checkbox_52.204-8[3]}} (iv) 52.222-52, Exemption from Application of the Service Contract Labor Standards to Contracts for Certain Services&mdash;Certification.</p><p>{{checkbox_52.204-8[4]}} (v) 52.223-9, with its Alternate I, Estimate of Percentage of Recovered Material Content for EPA-Designated Products (Alternate I only).</p><p>{{checkbox_52.204-8[5]}} (vi) 52.227-6, Royalty Information.</p><p>{{checkbox_52.204-8[6]}} (A) Basic.</p><p>{{checkbox_52.204-8[7]}} (B) Alternate I.</p><p>{{checkbox_52.204-8[8]}} (vii) 52.227-15, Representation of Limited Rights Data and Restricted Computer Software.</p><p>(d) The offeror has completed the annual representations and certifications electronically via the SAM Web site accessed through <i>https://www.acquisition.gov.</i> After reviewing the SAM database information, the offeror verifies by submission of the offer that the representations and certifications currently posted electronically that apply to this solicitation as indicated in paragraph (c) of this provision have been entered or updated within the last 12 months, are current, accurate, complete, and applicable to this solicitation (including the business size standard applicable to the NAICS code referenced for this solicitation), as of the date of this offer and are incorporated in this offer by reference (see FAR 4.1201); except for the changes identified below [<i>offeror to insert changes, identifying change by clause number, title, date</i>]. These amended representation(s) and/or certification(s) are also incorporated in this offer and are current, accurate, and complete as of the date of this offer.</p><div><div><table border="1" cellpadding="1" cellspacing="1" frame="void" width="100%"><tbody><tr><th scope="col">FAR Clause No.</th><th scope="col">Title</th><th scope="col">Date</th><th scope="col">Change</th></tr><tr><td align="left" scope="row">&nbsp;&nbsp;&nbsp;</td><td align="left"></td><td align="left"></td><td align="left"></td></tr></tbody></table></div></div><p>Any changes provided by the offeror are applicable to this solicitation only, and do not result in an update to the representations and certifications posted on SAM.</p>' WHERE fill_in_code = 'memobox_fe_52.204-8[0]'
AND clause_id IN (SELECT clause_id FROM Clauses 
WHERE clause_name = '52.204-8' AND clause_version_id = 14) ;

UPDATE Clauses
SET clause_title = 'Option to Purchase Equipment.' -- 'Option To Purchase Equipment.'
WHERE clause_version_id = 14 AND clause_name = '52.207-5';

UPDATE Clauses
SET clause_title = 'First Article Approval- Contractor Testing.' -- 'First Article Approval-Contractor Testing.'
WHERE clause_version_id = 14 AND clause_name = '52.209-3';

UPDATE Clauses
SET clause_title = 'Protecting the Governments Interest When Subcontracting With Contractor''s Debarred, Suspended, or Proposed for Debarment.' -- 'Protecting the Government''s Interest When Subcontracting With Contractors Debarred, Suspended, or Proposed for Debarment.'
WHERE clause_version_id = 14 AND clause_name = '52.209-6';

UPDATE Clauses
SET clause_title = 'Liquidated Damages--Supplies, Services, or Research and Development.' -- 'Liquidated Damages-Supplies, Services, or Research and Development.'
WHERE clause_version_id = 14 AND clause_name = '52.211-11';

UPDATE Clauses
SET clause_title = 'Brand Name or Equal.' -- 'Brand name or equal.'
WHERE clause_version_id = 14 AND clause_name = '52.211-6';

UPDATE Clauses
SET clause_title = 'Alternatives to Government-Unique Standards.' -- 'Alternatives to Government-unique standards.'
WHERE clause_version_id = 14 AND clause_name = '52.211-7';

UPDATE Clauses
SET clause_title = 'Instructions to Contractors- Commercial Items.' -- 'Instructions to Offerors-Commercial Items.'
WHERE clause_version_id = 14 AND clause_name = '52.212-1';

UPDATE Clauses
SET clause_title = 'Offeror Representations and Certifications- Commercial Items.' -- 'Offeror Representations and Certifications-Commercial Items.'
WHERE clause_version_id = 14 AND clause_name = '52.212-3';

UPDATE Clauses
SET clause_title = 'Contract Terms and Conditions--Commercial Items.' -- 'Contract Terms and Conditions-Commercial Items.'
WHERE clause_version_id = 14 AND clause_name = '52.212-4';

UPDATE Clauses
SET clause_title = 'Contract Terms and Conditions Required to Implement Statutes or Executive Orders- Commercial Items.' -- 'Contract Terms and Conditions Required To Implement Statutes or Executive Orders-Commercial Items.'
, clause_data = '<p>(a) The Contractor shall comply with the following Federal Acquisition Regulation (FAR) clauses, which are incorporated in this contract by reference, to implement provisions of law or Executive orders applicable to acquisitions of commercial items:</p><p>(1) 52.209-10, Prohibition on Contracting with Inverted Domestic Corporations (NOV 2015).</p><p>(2) 52.233-3, Protest After Award (AUG 1996) (31 U.S.C. 3553).</p><p>(3) 52.233-4, Applicable Law for Breach of Contract Claim (OCT 2004) (Public Laws 108-77 and 108-78 (19 U.S.C. 3805 note)).</p><p>(b) The Contractor shall comply with the FAR clauses in this paragraph (b) that the Contracting Officer has indicated as being incorporated in this contract by reference to implement provisions of law or Executive orders applicable to acquisitions of commercial items: [Contracting Officer check as appropriate.]</p><p>{{checkbox_52.212-5[0]}} (1) 52.203-6, Restrictions on Subcontractor Sales to the Government (SEP 2006), with <i>Alternate I</i> (OCT 1995) (41 U.S.C. 4704 and 10 U.S.C. 2402).</p><p>{{checkbox_52.212-5[1]}} (2) 52.203-13, Contractor Code of Business Ethics and Conduct (Oct 2015) (41 U.S.C. 3509).</p><p>{{checkbox_52.212-5[2]}} (3) 52.203-15, Whistleblower Protections under the American Recovery and Reinvestment Act of 2009 (JUN 2010) (Section 1553 of Pub. L. 111-5). (Applies to contracts funded by the American Recovery and Reinvestment Act of 2009.)</p><p>{{checkbox_52.212-5[3]}} (4) 52.204-10, Reporting Executive Compensation and First-Tier Subcontract Awards (Oct 2015) (Pub. L. 109-282) (31 U.S.C. 6101 note).</p><p>{{checkbox_52.212-5[4]}} (5) [Reserved]</p><p>{{checkbox_52.212-5[5]}} (6) 52.204-14, Service Contract Reporting Requirements (JAN 2014) (Pub. L. 111-117, section 743 of Div. C).</p><p>{{checkbox_52.212-5[6]}} (7) 52.204-15, Service Contract Reporting Requirements for Indefinite-Delivery Contracts (JAN 2014) (Pub. L. 111-117, section 743 of Div. C).</p><p>{{checkbox_52.212-5[7]}} (8) 52.209-6, Protecting the Government''s Interest When Subcontracting with Contractors Debarred, Suspended, or Proposed for Debarment. (Oct 2015) (31 U.S.C. 6101 note).</p><p>{{checkbox_52.212-5[8]}} (9) 52.209-9, Updates of Publicly Available Information Regarding Responsibility Matters (JUL 2013) (41 U.S.C. 2313).</p><p>{{checkbox_52.212-5[9]}} (10) [Reserved]</p><p>{{checkbox_52.212-5[10]}} (11)(i) 52.219-3, Notice of HUBZone Set-Aside or Sole-Source Award (NOV 2011) (15 U.S.C. 657a).</p><p>{{checkbox_52.212-5[11]}} (ii) Alternate I (NOV 2011) of 52.219-3.</p><p>{{checkbox_52.212-5[12]}} (12)(i) 52.219-4, Notice of Price Evaluation Preference for HUBZone Small Business Concerns (OCT 2014) (if the offeror elects to waive the preference, it shall so indicate in its offer) (15 U.S.C. 657a).</p><p>{{checkbox_52.212-5[13]}} (ii) Alternate I (JAN 2011) of 52.219-4.</p><p>{{checkbox_52.212-5[14]}} (13) [Reserved]</p><p>{{checkbox_52.212-5[15]}} (14)(i) 52.219-6, Notice of Total Small Business Set-Aside (NOV 2011) (15 U.S.C. 644).</p><p>{{checkbox_52.212-5[16]}} (ii) Alternate I (NOV 2011).</p><p>{{checkbox_52.212-5[17]}} (iii) Alternate II (NOV 2011).</p><p>{{checkbox_52.212-5[18]}} (15)(i) 52.219-7, Notice of Partial Small Business Set-Aside (JUN 2003) (15 U.S.C. 644). </p><p>{{checkbox_52.212-5[19]}} (ii) <i>Alternate I</i> (OCT 1995) of 52.219-7. </p><p>{{checkbox_52.212-5[20]}} (iii) <i>Alternate II</i> (MAR 2004) of 52.219-7. </p><p>{{checkbox_52.212-5[21]}} (16) 52.219-8, Utilization of Small Business Concerns (OCT 2014) (15 U.S.C. 637(d)(2) and (3)).</p><p>{{checkbox_52.212-5[22]}} (17)(i) 52.219-9, Small Business Subcontracting Plan (Oct 2015) (15 U.S.C. 637(d)(4)).</p><p>{{checkbox_52.212-5[23]}} (ii) <i>Alternate I</i> (OCT 2001) of 52.219-9. </p><p>{{checkbox_52.212-5[24]}} (iii) <i>Alternate II</i> (OCT 2001) of 52.219-9. </p><p>{{checkbox_52.212-5[25]}} (iv) Alternate III (Oct 2015) of 52.219-9.</p><p>{{checkbox_52.212-5[26]}} (18) 52.219-13, Notice of Set-Aside of Orders (NOV 2011) (15 U.S.C. 644(r)).</p><p>{{checkbox_52.212-5[27]}} (19) <i>52.219-14,</i> Limitations on Subcontracting (NOV 2011) (15 U.S.C. 637(a)(14)).</p><p>{{checkbox_52.212-5[28]}} (20) 52.219-16, Liquidated Damages-Subcontracting Plan (JAN 1999) (15 U.S.C. 637(d)(4)(F)(i)).</p><p>{{checkbox_52.212-5[29]}} (21) <i>52.219-27,</i> Notice of Service-Disabled Veteran-Owned Small Business Set-Aside (NOV 2011) (15 U.S.C. 657f).</p><p>{{checkbox_52.212-5[30]}} (22) 52.219-28, Post Award Small Business Program Rerepresentation (JUL 2013) (15 U.S.C. 632(a)(2)).</p><p>{{checkbox_52.212-5[31]}} (23) 52.219-29, Notice of Set-Aside for, or Sole Source Award to, Economically Disadvantaged Women-Owned Small Business Concerns (Dec 2015) (15 U.S.C. 637(m)).</p><p>{{checkbox_52.212-5[32]}} (24) 52.219-30, Notice of Set-Aside for, or Sole Source Award to, Women-Owned Small Business Concerns Eligible Under the Women-Owned Small Business Program (Dec 2015) (15 U.S.C. 637(m)).</p><p>{{checkbox_52.212-5[33]}} (25) 52.222-3, Convict Labor (JUN 2003) (E.O. 11755).</p><p>{{checkbox_52.212-5[34]}} (26) 52.222-19, Child Labor-Cooperation with Authorities and Remedies (JAN 2016) (E.O. 13126).</p><p>{{checkbox_52.212-5[35]}} (27) 52.222-21, Prohibition of Segregated Facilities (APR 2015).</p><p>{{checkbox_52.212-5[36]}} (28) 52.222-26, Equal Opportunity (APR 2015) (E.O. 11246).</p><p>{{checkbox_52.212-5[37]}} (29) 52.222-35, Equal Opportunity for Veterans (Oct 2015) (38 U.S.C. 4212).</p><p>{{checkbox_52.212-5[38]}} (30) 52.222-36, Equal Opportunity for Workers with Disabilities (July 2014) (29 U.S.C. 793).</p><p>{{checkbox_52.212-5[39]}} (31) 52.222-37, Employment Reports on Veterans (Oct 2015) (38 U.S.C. 4212).</p><p>{{checkbox_52.212-5[40]}} (32) 52.222-40, Notification of Employee Rights Under the National Labor Relations Act (DEC 2010) (E.O. 13496).</p><p> {{checkbox_52.212-5[41]}} (33)(i) 52.222-50, Combating Trafficking in Persons (Mar 2015) (22 U.S.C. chapter 78 and E.O. 13627).</p><p>{{checkbox_52.212-5[42]}} (ii) <i>Alternate I</i> (Mar 2015) of 52.222-50 (22 U.S.C. chapter 78 and E.O. 13627).</p><p>{{checkbox_52.212-5[43]}} (34) 52.222-54, Employment Eligibility Verification (Oct 2015). (E. O. 12989). (Not applicable to the acquisition of commercially available off-the-shelf items or certain other types of commercial items as prescribed in 22.1803.)</p><p>{{checkbox_52.212-5[44]}} (35)(i) 52.223-9, Estimate of Percentage of Recovered Material Content for EPA-Designated Items (MAY 2008) (42 U.S.C. 6962(c)(3)(A)(ii)). (Not applicable to the acquisition of commercially available off-the-shelf items.)</p><p>{{checkbox_52.212-5[45]}} (ii) Alternate I (MAY 2008) of 52.223-9 (42 U.S.C. 6962(i)(2)(C)). (Not applicable to the acquisition of commercially available off-the-shelf items.)</p><p>{{checkbox_52.212-5[46]}} (36)(i) 52.223-13, Acquisition of EPEAT&reg;-Registered Imaging Equipment (JUN 2014) (E.O.s 13423 and 13514).</p><p>{{checkbox_52.212-5[47]}} (ii) Alternate I (OCT 2015) of 52.223-13.</p><p>{{checkbox_52.212-5[48]}} (37)(i) 52.223-14, Acquisition of EPEAT&reg;-Registered Televisions (Jun 2014) (E.O.s 13423 and 13514).</p><p>(ii) Alternate I (Jun 2014) of 52.223-14.</p><p>{{checkbox_52.212-5[49]}} (38) 52.223-15, Energy Efficiency in Energy-Consuming Products (DEC 2007) (42 U.S.C. 8259b).</p><p>{{checkbox_52.212-5[50]}} (39)(i) 52.223-16, Acquisition of EPEAT&reg;-Registered Personal Computer Products (OCT 2015) (E.O.s 13423 and 13514).</p><p>{{checkbox_52.212-5[51]}} (ii) Alternate I (Jun 2014) of 52.223-16.</p><p>{{checkbox_52.212-5[52]}} (40) 52.223-18, Encouraging Contractor Policies to Ban Text Messaging While Driving (AUG 2011)</p><p>{{checkbox_52.212-5[53]}} (41) 52.225-1, Buy American-Supplies (<b>MAY 2014</b>) (41 U.S.C. chapter 83).</p><p>{{checkbox_52.212-5[54]}} (42)(i) 52.225-3, Buy American-Free Trade Agreements-Israeli Trade Act (<b>MAY 2014</b>) (41 U.S.C. chapter 83, 19 U.S.C. 3301 note, 19 U.S.C. 2112 note, 19 U.S.C. 3805 note, 19 U.S.C. 4001 note, Pub. L. 103-182, 108-77, 108-78, 108-286, 108-302, 109-53, 109-169, 109-283, 110-138, 112-41, 112-42, and 112-43.</p><p>{{checkbox_52.212-5[55]}} (ii) Alternate I (<b>MAY 2014</b>) of 52.225-3.</p><p>{{checkbox_52.212-5[56]}} (iii) Alternate II (<b>MAY 2014</b>) of 52.225-3.</p><p>{{checkbox_52.212-5[57]}} (iv) Alternate III (<b>MAY 2014</b>) of 52.225-3.</p><p>{{checkbox_52.212-5[58]}} (43) 52.225-5, Trade Agreements (NOV 2013) (19 U.S.C. 2501, <i>et seq.</i>, 19 U.S.C. 3301 note).</p><p>{{checkbox_52.212-5[59]}} (44) 52.225-13, Restrictions on Certain Foreign Purchases (JUN 2008) (E.O.''s, proclamations, and statutes administered by the Office of Foreign Assets Control of the Department of the Treasury).</p><p>{{checkbox_52.212-5[60]}} (45) 52.225-26, Contractors Performing Private Security Functions Outside the United States (JUL 2013) (Section 862, as amended, of the National Defense Authorization Act for Fiscal Year 2008; 10 U.S.C. 2302 Note).</p><p>{{checkbox_52.212-5[61]}} (46) 52.226-4, Notice of Disaster or Emergency Area Set-Aside (NOV 2007) (42 U.S.C. 5150).</p><p>{{checkbox_52.212-5[62]}} (47) 52.226-5, Restrictions on Subcontracting Outside Disaster or Emergency Area (NOV 2007) (42 U.S.C. 5150).</p><p>{{checkbox_52.212-5[63]}} (48) 52.232-29, Terms for Financing of Purchases of Commercial Items (FEB 2002) (41 U.S.C.4505, 10 U.S.C. 2307(f)).</p><p>{{checkbox_52.212-5[64]}} (49) 52.232-30, Installment Payments for Commercial Items (OCT 1995) (41 U.S.C.4505, 10 U.S.C. 2307(f)).</p><p>{{checkbox_52.212-5[65]}} (50) 52.232-33, Payment by Electronic Funds Transfer-System for Award Management (JUL 2013) (31 U.S.C. 3332).</p><p>{{checkbox_52.212-5[66]}} (51) 52.232-34, Payment by Electronic Funds Transfer-Other than System for Award Management (JUL 2013) (31 U.S.C. 3332).</p><p>{{checkbox_52.212-5[67]}} (52) 52.232-36, Payment by Third Party (MAY 2014) (31 U.S.C. 3332).</p><p>{{checkbox_52.212-5[68]}} (53) 52.239-1, Privacy or Security Safeguards (AUG 1996) (5 U.S.C. 552a).</p><p>{{checkbox_52.212-5[69]}} (54)(i) 52.247-64, Preference for Privately Owned U.S.-Flag Commercial Vessels (FEB 2006) (46 U.S.C. Appx. 1241(b) and 10 U.S.C. 2631).</p><p>{{checkbox_52.212-5[70]}} (ii) <i>Alternate I</i> (APR 2003) of 52.247-64.</p><p>(c) The Contractor shall comply with the FAR clauses in this paragraph (c), applicable to commercial services, that the Contracting Officer has indicated as being incorporated in this contract by reference to implement provisions of law or Executive orders applicable to acquisitions of commercial items: [Contracting Officer check as appropriate.]</p><p>{{checkbox_52.212-5[71]}} (1) 52.222-17, Nondisplacement of Qualified Workers (May 2014) (E.O. 13495).</p><p>{{checkbox_52.212-5[72]}} (2) 52.222-41, Service Contract Labor Standards (MAY 2014) (41 U.S.C. chapter 67).</p><p>{{checkbox_52.212-5[73]}} (3) 52.222-42, Statement of Equivalent Rates for Federal Hires (MAY 2014) (29 U.S.C. 206 and 41 U.S.C. chapter 67).</p><p>{{checkbox_52.212-5[74]}} (4) 52.222-43, Fair Labor Standards Act and Service Contract Labor Standards-Price Adjustment (Multiple Year and Option Contracts) (MAY 2014) (29 U.S.C. 206 and 41 U.S.C. chapter 67).</p><p>{{checkbox_52.212-5[75]}} (5) 52.222-44, Fair Labor Standards Act and Service Contract Labor Standards-Price Adjustment (MAY 2014) (29 U.S.C 206 and 41 U.S.C. chapter 67).</p><p>{{checkbox_52.212-5[76]}} (6) 52.222-51, Exemption from Application of the Service Contract Labor Standards to Contracts for Maintenance, Calibration, or Repair of Certain Equipment-Requirements (MAY 2014) (41 U.S.C. chapter 67).</p><p>{{checkbox_52.212-5[77]}} (7) 52.222-53, Exemption from Application of the Service Contract Labor Standards to Contracts for Certain Services-Requirements (MAY 2014) (41 U.S.C. chapter 67).</p><p> {{checkbox_52.212-5[78]}} (8) 52.222-55, Minimum Wages Under Executive Order 13658 (DEC 2015) (E.O. 13658).</p><p>{{checkbox_52.212-5[79]}} (9) 52.226-6, Promoting Excess Food Donation to Nonprofit Organizations (MAY 2014) (42 U.S.C. 1792).</p><p>{{checkbox_52.212-5[80]}} (10) 52.237-11, Accepting and Dispensing of $1 Coin (SEP 2008) (31 U.S.C. 5112(p)(1)).</p><p>(d) <i>Comptroller General Examination of Record.</i> The Contractor shall comply with the provisions of this paragraph (d) if this contract was awarded using other than sealed bid, is in excess of the simplified acquisition threshold, and does not contain the clause at 52.215-2, Audit and Records-Negotiation.</p><p>(1) The Comptroller General of the United States, or an authorized representative of the Comptroller General, shall have access to and right to examine any of the Contractor''s directly pertinent records involving transactions related to this contract.</p><p>(2) The Contractor shall make available at its offices at all reasonable times the records, materials, and other evidence for examination, audit, or reproduction, until 3 years after final payment under this contract or for any shorter period specified in FAR Subpart 4.7, Contractor Records Retention, of the other clauses of this contract. If this contract is completely or partially terminated, the records relating to the work terminated shall be made available for 3 years after any resulting final termination settlement. Records relating to appeals under the disputes clause or to litigation or the settlement of claims arising under or relating to this contract shall be made available until such appeals, litigation, or claims are finally resolved.</p><p>(3) As used in this clause, records include books, documents, accounting procedures and practices, and other data, regardless of type and regardless of form. This does not require the Contractor to create or maintain any record that the Contractor does not maintain in the ordinary course of business or pursuant to a provision of law.</p><p>(e)(1) Notwithstanding the requirements of the clauses in paragraphs (a), (b), (c), and (d) of this clause, the Contractor is not required to flow down any FAR clause, other than those in this paragraph (e)(1) of this paragraph in a subcontract for commercial items. Unless otherwise indicated below, the extent of the flow down shall be as required by the clause-</p><p>(i) 52.203-13, Contractor Code of Business Ethics and Conduct (Oct 2015) (41 U.S.C. 3509).</p><p>(ii) 52.219-8, Utilization of Small Business Concerns (OCT 2014) (15 U.S.C. 637(d)(2) and (3)), in all subcontracts that offer further subcontracting opportunities. If the subcontract (except subcontracts to small business concerns) exceeds $700,000 ($1.5 million for construction of any public facility), the subcontractor must include 52.219-8 in lower tier subcontracts that offer subcontracting opportunities.</p><p>(iii) 52.222-17, Nondisplacement of Qualified Workers (MAY 2014) (E.O. 13495). Flow down required in accordance with paragraph (l) of FAR clause 52.222-17.</p><p>(iv) 52.222-21, Prohibition of Segregated Facilities (APR 2015).</p><p>(v) 52.222-26, Equal Opportunity (APR 2015) (E.O. 11246).</p><p>(vi) 52.222-35, Equal Opportunity for Veterans (Oct 2015) (38 U.S.C. 4212).</p><p>(vii) 52.222-36, Equal Opportunity for Workers with Disabilities (July 2014) (29 U.S.C. 793).</p><p>(viii) 52.222-37, Employment Reports on Veterans (Oct 2015) (38 U.S.C. 4212).</p><p>(ix) 52.222-40, Notification of Employee Rights Under the National Labor Relations Act (DEC 2010) (E.O. 13496). Flow down required in accordance with paragraph (f) of FAR clause 52.222-40.</p><p>(x) 52.222-41, Service Contract Labor Standards (MAY 2014) (41 U.S.C. chapter 67).</p><p>(xi) {{checkbox_52.212-5[81]}} (A) 52.222-50, Combating Trafficking in Persons (Mar 2015) (22 U.S.C. chapter 78 and E.O. 13627).</p><p>{{checkbox_52.212-5[82]}} (B) Alternate I (Mar 2015) of 52.222-50 (22 U.S.C. chapter 78 and E.O. 13627).</p><p>(xii) 52.222-51, Exemption from Application of the Service Contract Labor Standards to Contracts for Maintenance, Calibration, or Repair of Certain Equipment-Requirements (MAY 2014) (41 U.S.C. chapter 67).</p><p>(xiii) 52.222-53, Exemption from Application of the Service Contract Labor Standards to Contracts for Certain Services-Requirements (MAY 2014) (41 U.S.C. chapter 67).</p><p>(xiv) 52.222-54, Employment Eligibility Verification (Oct 2015) (E. O. 12989).</p><p>(xv) 52.222-55, Minimum Wages Under Executive Order 13658 (DEC 2015) (E.O. 13658).</p><p>(xvi) 52.225-26, Contractors Performing Private Security Functions Outside the United States (JUL 2013) (Section 862, as amended, of the National Defense Authorization Act for Fiscal Year 2008; 10 U.S.C. 2302 Note).</p><p>(xvii) 52.226-6, Promoting Excess Food Donation to Nonprofit Organizations (MAY 2014) (42 U.S.C. 1792). Flow down required in accordance with paragraph (e) of FAR clause 52.226-6.</p><p>(xviii) 52.247-64, Preference for Privately Owned U.S.-Flag Commercial Vessels (FEB 2006) (46 U.S.C. Appx. 1241(b) and 10 U.S.C. 2631). Flow down required in accordance with paragraph (d) of FAR clause 52.247-64.</p><p>(2) While not required, the Contractor May include in its subcontracts for commercial items a minimal number of additional clauses necessary to satisfy its contractual obligations.</p>'
--               '<p>(a) The Contractor shall comply with the following Federal Acquisition Regulation (FAR) clauses, which are incorporated in this contract by reference, to implement provisions of law or Executive orders applicable to acquisitions of commercial items:</p><p>(1) 52.209-10, Prohibition on Contracting with Inverted Domestic Corporations (NOV 2015).</p><p>(2) 52.233-3, Protest After Award (AUG 1996) (31 U.S.C. 3553).</p><p>(3) 52.233-4, Applicable Law for Breach of Contract Claim (OCT 2004) (Public Laws 108-77 and 108-78 (19 U.S.C. 3805 note)).</p><p>(b) The Contractor shall comply with the FAR clauses in this paragraph (b) that the Contracting Officer has indicated as being incorporated in this contract by reference to implement provisions of law or Executive orders applicable to acquisitions of commercial items: [Contracting Officer check as appropriate.]</p><p>{{checkbox_52.212-5[0]}} (1) 52.203-6, Restrictions on Subcontractor Sales to the Government (SEP 2006), with <i>Alternate I</i> (OCT 1995) (41 U.S.C. 4704 and 10 U.S.C. 2402).</p><p>{{checkbox_52.212-5[1]}} (2) 52.203-13, Contractor Code of Business Ethics and Conduct (Oct 2015) (41 U.S.C. 3509).</p><p>{{checkbox_52.212-5[2]}} (3) 52.203-15, Whistleblower Protections under the American Recovery and Reinvestment Act of 2009 (JUN 2010) (Section 1553 of Pub. L. 111-5). (Applies to contracts funded by the American Recovery and Reinvestment Act of 2009.)</p><p>{{checkbox_52.212-5[3]}} (4) 52.204-10, Reporting Executive Compensation and First-Tier Subcontract Awards (Oct 2015) (Pub. L. 109-282) (31 U.S.C. 6101 note).</p><p>{{checkbox_52.212-5[4]}} (5) [Reserved]</p><p>{{checkbox_52.212-5[5]}} (6) 52.204-14, Service Contract Reporting Requirements (JAN 2014) (Pub. L. 111-117, section 743 of Div. C).</p><p>{{checkbox_52.212-5[6]}} (7) 52.204-15, Service Contract Reporting Requirements for Indefinite-Delivery Contracts (JAN 2014) (Pub. L. 111-117, section 743 of Div. C).</p><p>{{checkbox_52.212-5[7]}} (8) 52.209-6, Protecting the Government''s Interest When Subcontracting with Contractors Debarred, Suspended, or Proposed for Debarment. (Oct 2015) (31 U.S.C. 6101 note).</p><p>{{checkbox_52.212-5[8]}} (9) 52.209-9, Updates of Publicly Available Information Regarding Responsibility Matters (JUL 2013) (41 U.S.C. 2313).</p><p>{{checkbox_52.212-5[9]}} (10) [Reserved]</p><p>{{checkbox_52.212-5[10]}} (11)(i) 52.219-3, Notice of HUBZone Set-Aside or Sole-Source Award (NOV 2011) (15 U.S.C. 657a).</p><p>{{checkbox_52.212-5[11]}} (ii) Alternate I (NOV 2011) of 52.219-3.</p><p>{{checkbox_52.212-5[12]}} (12)(i) 52.219-4, Notice of Price Evaluation Preference for HUBZone Small Business Concerns (OCT 2014) (if the offeror elects to waive the preference, it shall so indicate in its offer) (15 U.S.C. 657a).</p><p>{{checkbox_52.212-5[13]}} (ii) Alternate I (JAN 2011) of 52.219-4.</p><p>{{checkbox_52.212-5[14]}} (13) [Reserved]</p><p>{{checkbox_52.212-5[15]}} (14)(i) 52.219-6, Notice of Total Small Business Set-Aside (NOV 2011) (15 U.S.C. 644).</p><p>{{checkbox_52.212-5[16]}} (ii) Alternate I (NOV 2011).</p><p>{{checkbox_52.212-5[17]}} (iii) Alternate II (NOV 2011).</p><p>{{checkbox_52.212-5[18]}} (15)(i) 52.219-7, Notice of Partial Small Business Set-Aside (JUN 2003) (15 U.S.C. 644). </p><p>{{checkbox_52.212-5[19]}} (ii) <i>Alternate I</i> (OCT 1995) of 52.219-7. </p><p>{{checkbox_52.212-5[20]}} (iii) <i>Alternate II</i> (MAR 2004) of 52.219-7. </p><p>{{checkbox_52.212-5[21]}} (16) 52.219-8, Utilization of Small Business Concerns (OCT 2014) (15 U.S.C. 637(d)(2) and (3)).</p><p>{{checkbox_52.212-5[22]}} (17)(i) 52.219-9, Small Business Subcontracting Plan (Oct 2015) (15 U.S.C. 637(d)(4)).</p><p>{{checkbox_52.212-5[23]}} (ii) <i>Alternate I</i> (OCT 2001) of 52.219-9. </p><p>{{checkbox_52.212-5[24]}} (iii) <i>Alternate II</i> (OCT 2001) of 52.219-9. </p><p>{{checkbox_52.212-5[25]}} (iv) Alternate III (Oct 2015) of 52.219-9.</p><p>{{checkbox_52.212-5[26]}} (18) 52.219-13, Notice of Set-Aside of Orders (NOV 2011) (15 U.S.C. 644(r)).</p><p>{{checkbox_52.212-5[27]}} (19) <i>52.219-14,</i> Limitations on Subcontracting (NOV 2011) (15 U.S.C. 637(a)(14)).</p><p>{{checkbox_52.212-5[28]}} (20) 52.219-16, Liquidated Damages-Subcontracting Plan (JAN 1999) (15 U.S.C. 637(d)(4)(F)(i)).</p><p>{{checkbox_52.212-5[29]}} (21) <i>52.219-27,</i> Notice of Service-Disabled Veteran-Owned Small Business Set-Aside (NOV 2011) (15 U.S.C. 657f).</p><p>{{checkbox_52.212-5[30]}} (22) 52.219-28, Post Award Small Business Program Rerepresentation (JUL 2013) (15 U.S.C. 632(a)(2)).</p><p>{{checkbox_52.212-5[31]}} (23) 52.219-29, Notice of Set-Aside for, or Sole Source Award to, Economically Disadvantaged Women-Owned Small Business Concerns (Dec 2015) (15 U.S.C. 637(m)).</p><p>{{checkbox_52.212-5[32]}} (24) 52.219-30, Notice of Set-Aside for, or Sole Source Award to, Women-Owned Small Business Concerns Eligible Under the Women-Owned Small Business Program (Dec 2015) (15 U.S.C. 637(m)).</p><p>{{checkbox_52.212-5[33]}} (25) 52.222-3, Convict Labor (JUN 2003) (E.O. 11755).</p><p>{{checkbox_52.212-5[34]}} (26) 52.222-19, Child Labor-Cooperation with Authorities and Remedies (JAN 2014) (E.O. 13126).</p><p>{{checkbox_52.212-5[35]}} (27) 52.222-21, Prohibition of Segregated Facilities (APR 2015).</p><p>{{checkbox_52.212-5[36]}} (28) 52.222-26, Equal Opportunity (APR 2015) (E.O. 11246).</p><p>{{checkbox_52.212-5[37]}} (29) 52.222-35, Equal Opportunity for Veterans (Oct 2015) (38 U.S.C. 4212).</p><p>{{checkbox_52.212-5[38]}} (30) 52.222-36, Equal Opportunity for Workers with Disabilities (July 2014) (29 U.S.C. 793).</p><p>{{checkbox_52.212-5[39]}} (31) 52.222-37, Employment Reports on Veterans (Oct 2015) (38 U.S.C. 4212).</p><p>{{checkbox_52.212-5[40]}} (32) 52.222-40, Notification of Employee Rights Under the National Labor Relations Act (DEC 2010) (E.O. 13496).</p><p> {{checkbox_52.212-5[41]}} (33)(i) 52.222-50, Combating Trafficking in Persons (Mar 2015) (22 U.S.C. chapter 78 and E.O. 13627).</p><p>{{checkbox_52.212-5[42]}} (ii) <i>Alternate I</i> (Mar 2015) of 52.222-50 (22 U.S.C. chapter 78 and E.O. 13627).</p><p>{{checkbox_52.212-5[43]}} (34) 52.222-54, Employment Eligibility Verification (Oct 2015). (E. O. 12989). (Not applicable to the acquisition of commercially available off-the-shelf items or certain other types of commercial items as prescribed in 22.1803.)</p><p>{{checkbox_52.212-5[44]}} (35)(i) 52.223-9, Estimate of Percentage of Recovered Material Content for EPA-Designated Items (MAY 2008) (42 U.S.C. 6962(c)(3)(A)(ii)). (Not applicable to the acquisition of commercially available off-the-shelf items.)</p><p>{{checkbox_52.212-5[45]}} (ii) Alternate I (MAY 2008) of 52.223-9 (42 U.S.C. 6962(i)(2)(C)). (Not applicable to the acquisition of commercially available off-the-shelf items.)</p><p>{{checkbox_52.212-5[46]}} (36)(i) 52.223-13, Acquisition of EPEAT&reg;-Registered Imaging Equipment (JUN 2014) (E.O.s 13423 and 13514).</p><p>{{checkbox_52.212-5[47]}} (ii) Alternate I (OCT 2015) of 52.223-13.</p><p>{{checkbox_52.212-5[48]}} (37)(i) 52.223-14, Acquisition of EPEAT&reg;-Registered Televisions (Jun 2014) (E.O.s 13423 and 13514).</p><p>(ii) Alternate I (Jun 2014) of 52.223-14.</p><p>{{checkbox_52.212-5[49]}} (38) 52.223-15, Energy Efficiency in Energy-Consuming Products (DEC 2007) (42 U.S.C. 8259b).</p><p>{{checkbox_52.212-5[50]}} (39)(i) 52.223-16, Acquisition of EPEAT&reg;-Registered Personal Computer Products (OCT 2015) (E.O.s 13423 and 13514).</p><p>{{checkbox_52.212-5[51]}} (ii) Alternate I (Jun 2014) of 52.223-16.</p><p>{{checkbox_52.212-5[52]}} (40) 52.223-18, Encouraging Contractor Policies to Ban Text Messaging While Driving (AUG 2011)</p><p>{{checkbox_52.212-5[53]}} (41) 52.225-1, Buy American-Supplies (<b>MAY 2014</b>) (41 U.S.C. chapter 83).</p><p>{{checkbox_52.212-5[54]}} (42)(i) 52.225-3, Buy American-Free Trade Agreements-Israeli Trade Act (<b>MAY 2014</b>) (41 U.S.C. chapter 83, 19 U.S.C. 3301 note, 19 U.S.C. 2112 note, 19 U.S.C. 3805 note, 19 U.S.C. 4001 note, Pub. L. 103-182, 108-77, 108-78, 108-286, 108-302, 109-53, 109-169, 109-283, 110-138, 112-41, 112-42, and 112-43.</p><p>{{checkbox_52.212-5[55]}} (ii) Alternate I (<b>MAY 2014</b>) of 52.225-3.</p><p>{{checkbox_52.212-5[56]}} (iii) Alternate II (<b>MAY 2014</b>) of 52.225-3.</p><p>{{checkbox_52.212-5[57]}} (iv) Alternate III (<b>MAY 2014</b>) of 52.225-3.</p><p>{{checkbox_52.212-5[58]}} (43) 52.225-5, Trade Agreements (NOV 2013) (19 U.S.C. 2501, <i>et seq.</i>, 19 U.S.C. 3301 note).</p><p>{{checkbox_52.212-5[59]}} (44) 52.225-13, Restrictions on Certain Foreign Purchases (JUN 2008) (E.O.''s, proclamations, and statutes administered by the Office of Foreign Assets Control of the Department of the Treasury).</p><p>{{checkbox_52.212-5[60]}} (45) 52.225-26, Contractors Performing Private Security Functions Outside the United States (JUL 2013) (Section 862, as amended, of the National Defense Authorization Act for Fiscal Year 2008; 10 U.S.C. 2302 Note).</p><p>{{checkbox_52.212-5[61]}} (46) 52.226-4, Notice of Disaster or Emergency Area Set-Aside (NOV 2007) (42 U.S.C. 5150).</p><p>{{checkbox_52.212-5[62]}} (47) 52.226-5, Restrictions on Subcontracting Outside Disaster or Emergency Area (NOV 2007) (42 U.S.C. 5150).</p><p>{{checkbox_52.212-5[63]}} (48) 52.232-29, Terms for Financing of Purchases of Commercial Items (FEB 2002) (41 U.S.C.4505, 10 U.S.C. 2307(f)).</p><p>{{checkbox_52.212-5[64]}} (49) 52.232-30, Installment Payments for Commercial Items (OCT 1995) (41 U.S.C.4505, 10 U.S.C. 2307(f)).</p><p>{{checkbox_52.212-5[65]}} (50) 52.232-33, Payment by Electronic Funds Transfer-System for Award Management (JUL 2013) (31 U.S.C. 3332).</p><p>{{checkbox_52.212-5[66]}} (51) 52.232-34, Payment by Electronic Funds Transfer-Other than System for Award Management (JUL 2013) (31 U.S.C. 3332).</p><p>{{checkbox_52.212-5[67]}} (52) 52.232-36, Payment by Third Party (MAY 2014) (31 U.S.C. 3332).</p><p>{{checkbox_52.212-5[68]}} (53) 52.239-1, Privacy or Security Safeguards (AUG 1996) (5 U.S.C. 552a).</p><p>{{checkbox_52.212-5[69]}} (54)(i) 52.247-64, Preference for Privately Owned U.S.-Flag Commercial Vessels (FEB 2006) (46 U.S.C. Appx. 1241(b) and 10 U.S.C. 2631).</p><p>{{checkbox_52.212-5[70]}} (ii) <i>Alternate I</i> (APR 2003) of 52.247-64.</p><p>(c) The Contractor shall comply with the FAR clauses in this paragraph (c), applicable to commercial services, that the Contracting Officer has indicated as being incorporated in this contract by reference to implement provisions of law or Executive orders applicable to acquisitions of commercial items: [Contracting Officer check as appropriate.]</p><p>{{checkbox_52.212-5[71]}} (1) 52.222-17, Nondisplacement of Qualified Workers (May 2014) (E.O. 13495).</p><p>{{checkbox_52.212-5[72]}} (2) 52.222-41, Service Contract Labor Standards (MAY 2014) (41 U.S.C. chapter 67).</p><p>{{checkbox_52.212-5[73]}} (3) 52.222-42, Statement of Equivalent Rates for Federal Hires (MAY 2014) (29 U.S.C. 206 and 41 U.S.C. chapter 67).</p><p>{{checkbox_52.212-5[74]}} (4) 52.222-43, Fair Labor Standards Act and Service Contract Labor Standards-Price Adjustment (Multiple Year and Option Contracts) (MAY 2014) (29 U.S.C. 206 and 41 U.S.C. chapter 67).</p><p>{{checkbox_52.212-5[75]}} (5) 52.222-44, Fair Labor Standards Act and Service Contract Labor Standards-Price Adjustment (MAY 2014) (29 U.S.C 206 and 41 U.S.C. chapter 67).</p><p>{{checkbox_52.212-5[76]}} (6) 52.222-51, Exemption from Application of the Service Contract Labor Standards to Contracts for Maintenance, Calibration, or Repair of Certain Equipment-Requirements (MAY 2014) (41 U.S.C. chapter 67).</p><p>{{checkbox_52.212-5[77]}} (7) 52.222-53, Exemption from Application of the Service Contract Labor Standards to Contracts for Certain Services-Requirements (MAY 2014) (41 U.S.C. chapter 67).</p><p> {{checkbox_52.212-5[78]}} (8) 52.222-55, Minimum Wages Under Executive Order 13658 (DEC 2015) (E.O. 13658).</p><p>{{checkbox_52.212-5[79]}} (9) 52.226-6, Promoting Excess Food Donation to Nonprofit Organizations (MAY 2014) (42 U.S.C. 1792).</p><p>{{checkbox_52.212-5[80]}} (10) 52.237-11, Accepting and Dispensing of $1 Coin (SEP 2008) (31 U.S.C. 5112(p)(1)).</p><p>(d) <i>Comptroller General Examination of Record.</i> The Contractor shall comply with the provisions of this paragraph (d) if this contract was awarded using other than sealed bid, is in excess of the simplified acquisition threshold, and does not contain the clause at 52.215-2, Audit and Records-Negotiation.</p><p>(1) The Comptroller General of the United States, or an authorized representative of the Comptroller General, shall have access to and right to examine any of the Contractor''s directly pertinent records involving transactions related to this contract.</p><p>(2) The Contractor shall make available at its offices at all reasonable times the records, materials, and other evidence for examination, audit, or reproduction, until 3 years after final payment under this contract or for any shorter period specified in FAR Subpart 4.7, Contractor Records Retention, of the other clauses of this contract. If this contract is completely or partially terminated, the records relating to the work terminated shall be made available for 3 years after any resulting final termination settlement. Records relating to appeals under the disputes clause or to litigation or the settlement of claims arising under or relating to this contract shall be made available until such appeals, litigation, or claims are finally resolved.</p><p>(3) As used in this clause, records include books, documents, accounting procedures and practices, and other data, regardless of type and regardless of form. This does not require the Contractor to create or maintain any record that the Contractor does not maintain in the ordinary course of business or pursuant to a provision of law.</p><p>(e)(1) Notwithstanding the requirements of the clauses in paragraphs (a), (b), (c), and (d) of this clause, the Contractor is not required to flow down any FAR clause, other than those in this paragraph (e)(1) of this paragraph in a subcontract for commercial items. Unless otherwise indicated below, the extent of the flow down shall be as required by the clause-</p><p>(i) 52.203-13, Contractor Code of Business Ethics and Conduct (Oct 2015) (41 U.S.C. 3509).</p><p>(ii) 52.219-8, Utilization of Small Business Concerns (OCT 2014) (15 U.S.C. 637(d)(2) and (3)), in all subcontracts that offer further subcontracting opportunities. If the subcontract (except subcontracts to small business concerns) exceeds $700,000 ($1.5 million for construction of any public facility), the subcontractor must include 52.219-8 in lower tier subcontracts that offer subcontracting opportunities.</p><p>(iii) 52.222-17, Nondisplacement of Qualified Workers (MAY 2014) (E.O. 13495). Flow down required in accordance with paragraph (l) of FAR clause 52.222-17.</p><p>(iv) 52.222-21, Prohibition of Segregated Facilities (APR 2015).</p><p>(v) 52.222-26, Equal Opportunity (APR 2015) (E.O. 11246).</p><p>(vi) 52.222-35, Equal Opportunity for Veterans (Oct 2015) (38 U.S.C. 4212).</p><p>(vii) 52.222-36, Equal Opportunity for Workers with Disabilities (July 2014) (29 U.S.C. 793).</p><p>(viii) 52.222-37, Employment Reports on Veterans (Oct 2015) (38 U.S.C. 4212).</p><p>(ix) 52.222-40, Notification of Employee Rights Under the National Labor Relations Act (DEC 2010) (E.O. 13496). Flow down required in accordance with paragraph (f) of FAR clause 52.222-40.</p><p>(x) 52.222-41, Service Contract Labor Standards (MAY 2014) (41 U.S.C. chapter 67).</p><p>(xi) {{checkbox_52.212-5[81]}} (A) 52.222-50, Combating Trafficking in Persons (Mar 2015) (22 U.S.C. chapter 78 and E.O. 13627).</p><p>{{checkbox_52.212-5[82]}} (B) Alternate I (Mar 2015) of 52.222-50 (22 U.S.C. chapter 78 and E.O. 13627).</p><p>(xii) 52.222-51, Exemption from Application of the Service Contract Labor Standards to Contracts for Maintenance, Calibration, or Repair of Certain Equipment-Requirements (MAY 2014) (41 U.S.C. chapter 67).</p><p>(xiii) 52.222-53, Exemption from Application of the Service Contract Labor Standards to Contracts for Certain Services-Requirements (MAY 2014) (41 U.S.C. chapter 67).</p><p>(xiv) 52.222-54, Employment Eligibility Verification (Oct 2015) (E. O. 12989).</p><p>(xv) 52.222-55, Minimum Wages Under Executive Order 13658 (DEC 2015) (E.O. 13658).</p><p>(xvi) 52.225-26, Contractors Performing Private Security Functions Outside the United States (JUL 2013) (Section 862, as amended, of the National Defense Authorization Act for Fiscal Year 2008; 10 U.S.C. 2302 Note).</p><p>(xvii) 52.226-6, Promoting Excess Food Donation to Nonprofit Organizations (MAY 2014) (42 U.S.C. 1792). Flow down required in accordance with paragraph (e) of FAR clause 52.226-6.</p><p>(xviii) 52.247-64, Preference for Privately Owned U.S.-Flag Commercial Vessels (FEB 2006) (46 U.S.C. Appx. 1241(b) and 10 U.S.C. 2631). Flow down required in accordance with paragraph (d) of FAR clause 52.247-64.</p><p>(2) While not required, the contractor May include in its subcontracts for commercial items a minimal number of additional clauses necessary to satisfy its contractual obligations.</p>'
WHERE clause_version_id = 14 AND clause_name = '52.212-5';

UPDATE Clauses
SET clause_title = 'Contract Terms and Conditions Required to Implement Statutes or Executive Orders-Commercial Items.' -- 'Contract Terms and Conditions Required to Implement Statutes or Executive Orders-Commercial Items(DEVIATION 2013-O0019)'
WHERE clause_version_id = 14 AND clause_name = '52.212-5 (DEVIATION 2013-00019)';

UPDATE Clauses
SET clause_title = 'Terms and Conditions--Simplified Acquisitions (Other Than Commercial Items).' -- 'Terms and Conditions-Simplified Acquisitions (Other Than Commercial Items)'
WHERE clause_version_id = 14 AND clause_name = '52.213-4';

UPDATE Clause_Fill_Ins SET fill_in_default_data = '<p>(a) The Contractor shall comply with the following Federal Acquisition Regulation (FAR) clauses that are incorporated by reference:</p><p>(1) The clauses listed below implement provisions of law or Executive order:</p><p>(i) 52.222-3, Convict Labor (JUN 2003) (E.O. 11755).</p><p>(ii) 52.222-21, Prohibition of Segregated Facilities (APR 2015).</p><p>(iii) 52.222-26, Equal Opportunity (APR 2015) (E.O. 11246).</p><p>(iv) 52.225-13, Restrictions on Certain Foreign Purchases (JUN 2008) (E.O.s, proclamations, and statutes administered by the Office of Foreign Assets Control of the Department of the Treasury).</p><p>(v) 52.233-3, Protest After Award (AUG 1996) (31 U.S.C. 3553).</p><p>(vi) 52.233-4, Applicable Law for Breach of Contract Claim (OCT 2004) (Pub. L. 108-77, 108-78 (19 U.S.C. 3805 note).</p><p>(2) Listed below are additional clauses that apply:</p><p>(i) 52.232-1, Payments (APR 1984).</p><p>(ii) 52.232-8, Discounts for Prompt Payment (FEB 2002).</p><p>(iii) 52.232-11, Extras (APR 1984).</p><p>(iv) 52.232-25, Prompt Payment (JUL 2013).</p><p>(v) 52.232-39, Unenforceability of Unauthorized Obligations (JUN 2013).</p><p>(vi) 52.232-40, Providing Accelerated Payments to Small Business Subcontractors (DEC 2013).</p><p>(vii) 52.233-1, Disputes (MAY 2014).</p><p>(viii) 52.244-6, Subcontracts for Commercial Items (DEC 2015).</p><p>(ix) 52.253-1, Computer Generated Forms (JAN 1991).</p><p>(b) The Contractor shall comply with the following FAR clauses, incorporated by reference, unless the circumstances do not apply:</p><p>(1) The clauses listed below implement provisions of law or Executive order:</p><p>(i) 52.204-10, Reporting Executive Compensation and First-Tier Subcontract Awards (Oct 2015) (Pub. L. 109-282) (31 U.S.C. 6101 note) (Applies to contracts valued at $30,000 or more).</p><p>(ii) 52.222-19, Child Labor&mdash;Cooperation with Authorities and Remedies (JAN 2016) (E.O. 13126). (Applies to contracts for supplies exceeding the micro-purchase threshold.)</p><p>(iii) 52.222-20, Contracts for Materials, Supplies, Articles, and Equipment Exceeding $15,000 (MAY 2014) (41 U.S.C. chapter 65) (Applies to supply contracts over $15,000 in the United States, Puerto Rico, or the U.S. Virgin Islands).</p><p>(iv) 52.222-35, Equal Opportunity for Veterans (Oct 2015) (38 U.S.C. 4212) (applies to contracts of $150,000 or more).</p><p>(v) 52.222-36, Equal Employment for Workers with Disabilities (JUL 2014) (29 U.S.C. 793) (Applies to contracts over $15,000, unless the work is to be performed outside the United States by employees recruited outside the United States). (For purposes of this clause, &ldquo;United States" includes the 50 States, the District of Columbia, Puerto Rico, the Northern Mariana Islands, American Samoa, Guam, the U.S. Virgin Islands, and Wake Island.)</p><p>(vi) 52.222-37, Employment Reports on Veterans (Oct 2015) (38 U.S.C. 4212) (applies to contracts of $150,000 or more).</p><p>(vii) 52.222-41, Service Contract Labor Standards (MAY 2014) (41 U.S.C. chapter 67) (Applies to service contracts over $2,500 that are subject to the Service Contract Labor Standards statute and will be performed in the United States, District of Columbia, Puerto Rico, the Northern Mariana Islands, American Samoa, Guam, the U.S. Virgin Islands, Johnston Island, Wake Island, or the outer Continental Shelf).</p><p>(viii)(A) 52.222-50, Combating Trafficking in Persons (MAR 2015) (22 U.S.C. chapter 78 and E.O 13627) (Applies to all solicitations and contracts).</p><p>(B) Alternate I (MAR 2015) (Applies if the Contracting Officer has filled in the following information with regard to applicable directives or notices: Document title(s), source for obtaining document(s), and contract performance location outside the United States to which the document applies).</p><p>(ix) 52.222-55, Minimum Wages Under Executive Order 13658 (DEC 2015) (Executive Order 13658) (Applies when 52.222-6 or 52.222-41 are in the contract and performance in whole or in part is in the United States (the 50 States and the District of Columbia)).</p><p>(x) 52.223-5, Pollution Prevention and Right-to-Know Information (MAY 2011) (E.O. 13423) (Applies to services performed on Federal facilities).</p><p>(xi) 52.223-15, Energy Efficiency in Energy-Consuming Products (DEC 2007) (42 U.S.C. 8259b) (Unless exempt pursuant to 23.204, applies to contracts when energy-consuming products listed in the ENERGY STAR&reg; Program or Federal Energy Management Program (FEMP) will be&mdash;</p><p>(A) Delivered;</p><p>(B) Acquired by the Contractor for use in performing services at a Federally-controlled facility;</p><p>(C) Furnished by the Contractor for use by the Government; or</p><p>(D) Specified in the design of a building or work, or incorporated during its construction, renovation, or maintenance).</p><p>(xii) 52.225-1, Buy American&mdash;Supplies (MAY 2014) (41 U.S.C. chapter 67) (Applies to contracts for supplies, and to contracts for services involving the furnishing of supplies, for use in the United States or its outlying areas, if the value of the supply contract or supply portion of a service contract exceeds the micro-purchase threshold and the acquisition&mdash;</p><p>(A) Is set aside for small business concerns; or</p><p>(B) Cannot be set aside for small business concerns (see 19.502-2), and does not exceed $25,000).</p><p>(xiii) 52.226-6, Promoting Excess Food Donation to Nonprofit Organizations (MAY 2014) (42 U.S.C. 1792) (Applies to contracts greater than $25,000 that provide for the provision, the service, or the sale of food in the United States).</p><p>(xiv) 52.232-33, Payment by Electronic Funds Transfer&mdash;System for Award Management (JUL 2013) (Applies when the payment will be made by electronic funds transfer (EFT) and the payment office uses the System for Award Management (SAM) database as its source of EFT information.)</p><p>(xv) 52.232-34, Payment by Electronic Funds Transfer&mdash;Other than System for Award Management (JUL 2013) (Applies when the payment will be made by EFT and the payment office does not use the SAM database as its source of EFT information.)</p><p>(xvi) 52.247-64, Preference for Privately Owned U.S.-Flag Commercial Vessels (FEB 2006) (46 U.S.C. App. 1241) (Applies to supplies transported by ocean vessels (except for the types of subcontracts listed at 47.504(d).)</p><p>(2) Listed below are additional clauses that may apply:</p><p>(i) 52.209-6, Protecting the Government''s Interest When Subcontracting with Contractors Debarred, Suspended, or Proposed for Debarment (Oct 2015) (Applies to contracts over $35,000).</p><p>(ii) 52.211-17, Delivery of Excess Quantities (SEP 1989) (Applies to fixed-price supplies).</p><p>(iii) 52.247-29, F.o.b. Origin (FEB 2006) (Applies to supplies if delivery is f.o.b. origin).</p><p>(iv) 52.247-34, F.o.b. Destination (NOV 1991) (Applies to supplies if delivery is f.o.b. destination).</p><p>(c) <i>FAR 52.252-2, Clauses Incorporated by Reference (FEB 1998).</i> This contract incorporates one or more clauses by reference, with the same force and effect as if they were given in full text. Upon request, the Contracting Officer will make their full text available. Also, the full text of a clause may be accessed electronically at this/these address(es):</p>&nbsp; {{textbox_52.213-4[0]}}&nbsp; {{textbox_52.213-4[1]}}<p>[<i>Insert one or more Internet addresses</i>]</p><p>(d) <i>Inspection/Acceptance.</i> The Contractor shall tender for acceptance only those items that conform to the requirements of this contract. The Government reserves the right to inspect or test any supplies or services that have been tendered for acceptance. The Government may require repair or replacement of nonconforming supplies or reperformance of nonconforming services at no increase in contract price. The Government must exercise its postacceptance rights&mdash;</p><p>(1) Within a reasonable period of time after the defect was discovered or should have been discovered; and</p><p>(2) Before any substantial change occurs in the condition of the item, unless the change is due to the defect in the item.</p><p>(e) <i>Excusable delays.</i> The Contractor shall be liable for default unless nonperformance is caused by an occurrence beyond the reasonable control of the Contractor and without its fault or negligence, such as acts of God or the public enemy, acts of the Government in either its sovereign or contractual capacity, fires, floods, epidemics, quarantine restrictions, strikes, unusually severe weather, and delays of common carriers. The Contractor shall notify the Contracting Officer in writing as soon as it is reasonably possible after the commencement of any excusable delay, setting forth the full particulars in connection therewith, shall remedy such occurrence with all reasonable dispatch, and shall promptly give written notice to the Contracting Officer of the cessation of such occurrence.</p><p>(f) <i>Termination for the Government''s convenience.</i> The Government reserves the right to terminate this contract, or any part hereof, for its sole convenience. In the event of such termination, the Contractor shall immediately stop all work hereunder and shall immediately cause any and all of its suppliers and subcontractors to cease work. Subject to the terms of this contract, the Contractor shall be paid a percentage of the contract price reflecting the percentage of the work performed prior to the notice of termination, plus reasonable charges that the Contractor can demonstrate to the satisfaction of the Government, using its standard record keeping system, have resulted from the termination. The Contractor shall not be required to comply with the cost accounting standards or contract cost principles for this purpose. This paragraph does not give the Government any right to audit the Contractor''s records. The Contractor shall not be paid for any work performed or costs incurred that reasonably could have been avoided.</p><p>(g) <i>Termination for cause.</i> The Government may terminate this contract, or any part hereof, for cause in the event of any default by the Contractor, or if the Contractor fails to comply with any contract terms and conditions, or fails to provide the Government, upon request, with adequate assurances of future performance. In the event of termination for cause, the Government shall not be liable to the Contractor for any amount for supplies or services not accepted, and the Contractor shall be liable to the Government for any and all rights and remedies provided by law. If it is determined that the Government improperly terminated this contract for default, such termination shall be deemed a termination for convenience.</p><p>(h) <i>Warranty.</i> The Contractor warrants and implies that the items delivered hereunder are merchantable and fit for use for the particular purpose described in this contract.</p>' WHERE fill_in_code = 'memobox_fe_52.213-4[0]'
AND clause_id IN (SELECT clause_id FROM Clauses 
WHERE clause_name = '52.213-4' AND clause_version_id = 14) ;

UPDATE Clauses
SET clause_title = 'Place of Performance-- Sealed Bidding.' -- 'Place of Performance-Sealed Bidding.'
WHERE clause_version_id = 14 AND clause_name = '52.214-14';

UPDATE Clauses
SET clause_title = 'Preparation of Bids-- Construction.' -- 'Preparation of Bids-Construction.'
WHERE clause_version_id = 14 AND clause_name = '52.214-18';

UPDATE Clauses
SET clause_title = 'Contract Award-Sealed Bidding- Construction.' -- 'Contract Award-Sealed Bidding-Construction.'
WHERE clause_version_id = 14 AND clause_name = '52.214-19';

UPDATE Clauses
SET clause_title = 'Late Submissions, Modifications, and Withdrawals of Technical Proposals Under Two-Step Sealed Bidding.' -- 'Late submissions, modifications, revisions, and withdrawals of technical proposals under two-step sealed bidding.'
WHERE clause_version_id = 14 AND clause_name = '52.214-23';

UPDATE Clauses
SET clause_title = 'Audit and Records--Sealed Bidding.' -- 'Audit and Records-Sealed Bidding.'
WHERE clause_version_id = 14 AND clause_name = '52.214-26';

UPDATE Clauses
SET clause_title = 'Price Reduction for Defective Certified Cost or Pricing Data-- Modifications--Sealed Bidding.' -- 'Price Reduction for Defective Certified Cost or Pricing Data-Modifications-Sealed Bidding.'
WHERE clause_version_id = 14 AND clause_name = '52.214-27';

UPDATE Clauses
SET clause_title = 'Subcontractor Certified Cost or Pricing Data--Modifications--Sealed Bidding.' -- 'Subcontractor Certified Cost or Pricing Data-Modifications-Sealed Bidding.'
WHERE clause_version_id = 14 AND clause_name = '52.214-28';

UPDATE Clauses
SET clause_title = 'Order of Precedence--Sealed Bidding.' -- 'Order of Precedence-Sealed Bidding.'
WHERE clause_version_id = 14 AND clause_name = '52.214-29';

UPDATE Clauses
SET clause_title = 'Late Submissions, Modifications, and Withdrawals of Bids.' -- 'Late submissions, modifications, and withdrawals of bids.'
WHERE clause_version_id = 14 AND clause_name = '52.214-7';

UPDATE Clauses
SET clause_title = 'Instructions to Contractors-Competitive.' -- 'Instructions to Offerors-Competitive Acquisition.'
WHERE clause_version_id = 14 AND clause_name = '52.215-1';

UPDATE Clauses
SET clause_title = 'Price Reduction for Defective Certified Cost or Pricing Data--Modifications.' -- 'Price Reduction for Defective Certified Cost or Pricing Data-Modifications.'
WHERE clause_version_id = 14 AND clause_name = '52.215-11';

UPDATE Clauses
SET clause_title = 'Pension Adjustments and Asset Reversions.' -- 'Pension adjustments and asset reversions.'
WHERE clause_version_id = 14 AND clause_name = '52.215-15';

UPDATE Clauses
SET clause_title = 'Revision or Adjustment of Plans for Postretirement Benefits (PRB) Other Than Pensions.' -- 'Reversion or Adjustment of Plans for Postretirement Benefits (PRB) Other Than Pensions.'
WHERE clause_version_id = 14 AND clause_name = '52.215-18';

UPDATE Clauses
SET clause_title = 'Audit and Records- Negotiations.' -- 'Audit and Records-Negotiation.'
WHERE clause_version_id = 14 AND clause_name = '52.215-2';

UPDATE Clauses
SET clause_title = 'Requirements for Certified Cost or Pricing Data and Data Other Than Certified Cost or Pricing Data - Modifications.' -- 'Requirements for Certified Cost or Pricing Data and Data Other Than Certified Cost or Pricing Data-Modifications.'
WHERE clause_version_id = 14 AND clause_name = '52.215-21';

UPDATE Clauses
SET clause_title = 'Economic Price Adjustment- Standard Supplies.' -- 'Economic Price Adjustment-Standard Supplies.'
WHERE clause_version_id = 14 AND clause_name = '52.216-2';

UPDATE Clauses
SET clause_title = 'T&M/LH Proposal Requirements--Non-Commercial Item Acquisition With Adequate Price Competition.' -- 'Time-and-Materials/Labor-Hour Proposal Requirements-Non-Commercial Item Acquisition With Adequate Price Competition.'
WHERE clause_version_id = 14 AND clause_name = '52.216-29';

UPDATE Clauses
SET clause_title = 'T&M/LH Proposal Requirements--Non-Commercial Item Acquisition Without Adequate Price Competition.' -- 'Time-and-Materials/Labor-Hour Proposal Requirements-Non-Commercial Item Acquisition without Adequate Price Competition.'
WHERE clause_version_id = 14 AND clause_name = '52.216-30';

UPDATE Clauses
SET clause_title = 'T&M/LH Proposal Requirements--Commercial Item Acquisition.' -- 'Time-and-Materials/Labor-Hour Proposal Requirements-Commercial Item Acquisition.'
WHERE clause_version_id = 14 AND clause_name = '52.216-31';

UPDATE Clauses
SET clause_title = 'Economic Price Adjustment-- Labor and Material.' -- 'Economic Price Adjustment-Labor and Material.'
WHERE clause_version_id = 14 AND clause_name = '52.216-4';

UPDATE Clauses
SET clause_title = 'Price Redetermination - Prospective.' -- 'Price Redetermination-Prospective.'
WHERE clause_version_id = 14 AND clause_name = '52.216-5';

UPDATE Clauses
SET clause_title = 'Evaluation of Options Exercised At Time of Contract Award.' -- 'Evaluation of Option Exercised at Time of Contract Award.'
WHERE clause_version_id = 14 AND clause_name = '52.217-4';

UPDATE Clauses
SET clause_title = 'Option to Extend Services.' -- 'Option To Extend Services.'
WHERE clause_version_id = 14 AND clause_name = '52.217-8';

UPDATE Clauses
SET clause_title = 'Option to Extend the Term of the Contract.' -- 'Option To Extend the Term of the Contract.'
WHERE clause_version_id = 14 AND clause_name = '52.217-9';

UPDATE Clauses
SET clause_title = 'Small Business Program Representation.' -- 'Small Business Program Representations.'
WHERE clause_version_id = 14 AND clause_name = '52.219-1';

UPDATE Clauses
SET clause_title = 'Special 8(a) Sub-Contract Conditions.' -- 'Special 8(a) Subcontract Conditions.'
WHERE clause_version_id = 14 AND clause_name = '52.219-12';

UPDATE Clauses
SET clause_title = 'Notice of Service-Disabled Veteran-Owned Small Business Set Aside.' -- 'Notice of Service-Disabled Veteran-Owned Small Business Set-Aside.'
WHERE clause_version_id = 14 AND clause_name = '52.219-27';

UPDATE Clauses
SET clause_title = 'Notice of Set-Aside for Economically Disadvantaged Women-Owned Small Business (EDWOSB) Concerns.' -- 'Notice of Set-Aside for, or Sole Source Award to, Economically Disadvantaged Women-Owned Small Business Concerns.'
WHERE clause_version_id = 14 AND clause_name = '52.219-29';

UPDATE Clauses
SET clause_title = 'Notice of Hubzone Set-Aside or Sole Source Award.' -- 'Notice of HUBZone Set-Aside or Sole Source Award.'
WHERE clause_version_id = 14 AND clause_name = '52.219-3';

UPDATE Clauses
SET clause_title = 'Notice of Set-Aside for Women-Owned Small Business Concerns Eligible Under the Women-Owned Small Business Program.' -- 'Notice of Set-Aside for, or Sole Source Award to, Women-Owned Small Business Concerns Eligible Under the Women-Owned Small Business Program.'
WHERE clause_version_id = 14 AND clause_name = '52.219-30';

UPDATE Clauses
SET clause_title = 'Notice of Price Evaluation Preference for Hubzone Small Business Concerns.' -- 'Notice of Price Evaluation Preference for HUBZone Small Business Concerns.'
WHERE clause_version_id = 14 AND clause_name = '52.219-4';

UPDATE Clauses
SET clause_title = 'Small Business Subcontracting Plan.' -- 'Small business subcontracting plan.'
WHERE clause_version_id = 14 AND clause_name = '52.219-9';

UPDATE Clauses
SET clause_title = 'Small Business Subcontracting Plan.' -- 'Small Business Subcontracting Plan'
WHERE clause_version_id = 14 AND clause_name = '52.219-9 (DEVIATION 2013-00014)';

UPDATE Clauses
SET clause_title = 'Compliance With Copeland Act.' -- 'Compliance With Copeland Act Requirements.'
WHERE clause_version_id = 14 AND clause_name = '52.222-10';

UPDATE Clauses
SET clause_title = 'Contract Termination- Debarment.' -- 'Contract Termination-Debarment.'
WHERE clause_version_id = 14 AND clause_name = '52.222-12';

UPDATE Clauses
SET clause_title = 'Child Labor---Cooperation With Authorities and Remedies.' -- 'Child Labor-Cooperation with Authorities and Remedies.'
, clause_data = '<p>(a) <i>Applicability.</i> This clause does not apply to the extent that the Contractor is supplying end products mined, produced, or manufactured in- </p><p>(1) Canada, and the anticipated value of the acquisition is $25,000 or more; </p><p>(2) Israel, and the anticipated value of the acquisition is $50,000 or more; </p><p>(3) Mexico, and the anticipated value of the acquisition is $77,533 or more; or </p><p>(4) Armenia, Aruba, Austria, Belgium, Bulgaria, Croatia, Cyprus, Czech Republic, Denmark, Estonia, Finland, France, Germany, Greece, Hong Kong, Hungary, Iceland, Ireland, Italy, Japan, Korea, Latvia, Liechtenstein, Lithuania, Luxembourg, Malta, Netherlands, Norway, Poland, Portugal, Romania, Singapore, Slovak Republic, Slovenia, Spain, Sweden, Switzerland, Taiwan, or the United Kingdom and the anticipated value of the acquisition is $191,000 or more.</p><p>(b) <i>Cooperation with Authorities.</i> To enforce the laws prohibiting the manufacture or importation of products mined, produced, or manufactured by forced or indentured child labor, authorized officials may need to conduct investigations to determine whether forced or indentured child labor was used to mine, produce, or manufacture any product furnished under this contract. If the solicitation includes the provision 52.222-18, Certification Regarding Knowledge of Child Labor for Listed End Products, or the equivalent at 52.212-3(i), the Contractor agrees to cooperate fully with authorized officials of the contracting agency, the Department of the Treasury, or the Department of Justice by providing reasonable access to records, documents, persons, or premises upon reasonable request by the authorized officials. </p><p>(c) <i>Violations.</i> The Government may impose remedies set forth in paragraph (d) for the following violations: </p><p>(1) The Contractor has submitted a false certification regarding knowledge of the use of forced or indentured child labor for listed end products. </p><p>(2) The Contractor has failed to cooperate, if required, in accordance with paragraph (b) of this clause, with an investigation of the use of forced or indentured child labor by an Inspector General, Attorney General, or the Secretary of the Treasury. </p><p>(3) The Contractor uses forced or indentured child labor in its mining, production, or manufacturing processes. </p><p>(4) The Contractor has furnished under the contract end products or components that have been mined, produced, or manufactured wholly or in part by forced or indentured child labor. (The Government will not pursue remedies at paragraph (d)(2) or paragraph (d)(3) of this clause unless sufficient evidence indicates that the Contractor knew of the violation.) </p><p>(d) <i>Remedies.</i> (1) The Contracting Officer may terminate the contract. </p><p>(2) The suspending official may suspend the Contractor in accordance with procedures in FAR Subpart 9.4. </p><p>(3) The debarring official may debar the Contractor for a period not to exceed 3 years in accordance with the procedures in FAR Subpart 9.4.</p>'
--               '<p>(a) <i>Applicability.</i> This clause does not apply to the extent that the Contractor is supplying end products mined, produced, or manufactured in- </p><p>(1) Canada, and the anticipated value of the acquisition is $25,000 or more; </p><p>(2) Israel, and the anticipated value of the acquisition is $50,000 or more; </p><p>(3) Mexico, and the anticipated value of the acquisition is $79,507 or more; or </p><p>(4) Armenia, Aruba, Austria, Belgium, Bulgaria, Croatia, Cyprus, Czech Republic, Denmark, Estonia, Finland, France, Germany, Greece, Hong Kong, Hungary, Iceland, Ireland, Italy, Japan, Korea, Latvia, Liechtenstein, Lithuania, Luxembourg, Malta, Netherlands, Norway, Poland, Portugal, Romania, Singapore, Slovak Republic, Slovenia, Spain, Sweden, Switzerland, Taiwan, or the United Kingdom and the anticipated value of the acquisition is $204,000 or more.</p><p>(b) <i>Cooperation with Authorities.</i> To enforce the laws prohibiting the manufacture or importation of products mined, produced, or manufactured by forced or indentured child labor, authorized officials may need to conduct investigations to determine whether forced or indentured child labor was used to mine, produce, or manufacture any product furnished under this contract. If the solicitation includes the provision 52.222-18, Certification Regarding Knowledge of Child Labor for Listed End Products, or the equivalent at 52.212-3(i), the Contractor agrees to cooperate fully with authorized officials of the contracting agency, the Department of the Treasury, or the Department of Justice by providing reasonable access to records, documents, persons, or premises upon reasonable request by the authorized officials. </p><p>(c) <i>Violations.</i> The Government may impose remedies set forth in paragraph (d) for the following violations: </p><p>(1) The Contractor has submitted a false certification regarding knowledge of the use of forced or indentured child labor for listed end products. </p><p>(2) The Contractor has failed to cooperate, if required, in accordance with paragraph (b) of this clause, with an investigation of the use of forced or indentured child labor by an Inspector General, Attorney General, or the Secretary of the Treasury. </p><p>(3) The Contractor uses forced or indentured child labor in its mining, production, or manufacturing processes. </p><p>(4) The Contractor has furnished under the contract end products or components that have been mined, produced, or manufactured wholly or in part by forced or indentured child labor. (The Government will not pursue remedies at paragraph (d)(2) or paragraph (d)(3) of this clause unless sufficient evidence indicates that the Contractor knew of the violation.) </p><p>(d) <i>Remedies.</i> (1) The Contracting Officer may terminate the contract. </p><p>(2) The suspending official may suspend the Contractor in accordance with procedures in FAR Subpart 9.4. </p><p>(3) The debarring official may debar the Contractor for a period not to exceed 3 years in accordance with the procedures in FAR Subpart 9.4.</p>'
WHERE clause_version_id = 14 AND clause_name = '52.222-19';

UPDATE Clauses
SET clause_title = 'Contracts for Materials, Supplies, Articles and Equipment Exceeding 15,000.' -- 'Contracts for Materials, Supplies, Articles, and Equipment Exceeding $15,000.'
WHERE clause_version_id = 14 AND clause_name = '52.222-20';

UPDATE Clauses
SET clause_title = 'Prohibition of Segregated Facilities.' -- 'Prohibition of segregated facilities.'
WHERE clause_version_id = 14 AND clause_name = '52.222-21';

UPDATE Clauses
SET clause_title = 'Notice of Requirement for Affirmative Action to Ensure Equal Employment Opportunity for Construction.' -- 'Notice of Requirement for Affirmative Action To Ensure Equal Employment Opportunity for Construction.'
WHERE clause_version_id = 14 AND clause_name = '52.222-23';

UPDATE Clauses
SET clause_title = 'Notification of Visa Denial.' -- 'Notification of visa denial.'
WHERE clause_version_id = 14 AND clause_name = '52.222-29';

UPDATE Clauses
SET clause_title = 'Construction Wage Rate Requirements--Price Adjustment (None or Separately Specified Method).' -- 'Construction Wage Rate Requirements-Price Adjustment (None or Separately Specified Method).'
WHERE clause_version_id = 14 AND clause_name = '52.222-30';

UPDATE Clauses
SET clause_title = 'Construction Wage Rate Requirements--Price Adjustment (Percentage Method).' -- 'Construction Wage Rate Requirements-Price Adjustment (Percentage Method).'
WHERE clause_version_id = 14 AND clause_name = '52.222-31';

UPDATE Clauses
SET clause_title = 'Construction Wage Rate Requirements--Price Adjustment (Actual Method).' -- 'Construction Wage Rate Requirements-Price Adjustment (Actual Method).'
WHERE clause_version_id = 14 AND clause_name = '52.222-32';

UPDATE Clauses
SET clause_title = 'Notice of Requirement Project Labor Agreement.' -- 'Notice of Requirement for Project Labor Agreement.'
WHERE clause_version_id = 14 AND clause_name = '52.222-33';

UPDATE Clauses
SET clause_title = 'Equal Opportunity Veterans.' -- 'Equal Opportunity for Veterans.'
WHERE clause_version_id = 14 AND clause_name = '52.222-35';

UPDATE Clauses
SET clause_title = 'Equal Opportunity for Workers With Disabilities.' -- 'Equal Opportunity for Workers with Disabilities.'
WHERE clause_version_id = 14 AND clause_name = '52.222-36';

UPDATE Clauses
SET clause_title = 'Compliance With Veterans'' Employment Reporting Requirements.' -- 'Compliance with Veterans'' Employment Reporting Requirements.'
WHERE clause_version_id = 14 AND clause_name = '52.222-38';

UPDATE Clauses
SET clause_title = 'Notification of Employee Rights Under The National Labor Relations Act.' -- 'Notification of Employee Rights Under the National Labor Relations Act.'
WHERE clause_version_id = 14 AND clause_name = '52.222-40';

UPDATE Clauses
SET clause_title = 'Fair Labor Standards Act and Service Contract Labor Standards--Price Adjustment (Multiple Year and Option Contracts).' -- 'Fair Labor Standards Act and Service Contract Labor Standards-Price Adjustment (Multiple Year and Option Contracts).'
WHERE clause_version_id = 14 AND clause_name = '52.222-43';

UPDATE Clauses
SET clause_title = 'Fair Labor Standards Act and Service Contract Labor Standards--Price Adjustment.' -- 'Fair Labor Standards Act and Service Contract Labor Standards-Price Adjustment.'
WHERE clause_version_id = 14 AND clause_name = '52.222-44';

UPDATE Clauses
SET clause_title = 'Exemption From Application of the Service Contract Labor Standards to Contracts for Maintenance, Calibration, or Repair of Certain Equipment--Certification.' -- 'Exemption From Application of the Service Contract Labor Standards to Contracts for Maintenance, Calibration, or Repair of Certain Equipment-Certification.'
WHERE clause_version_id = 14 AND clause_name = '52.222-48';

UPDATE Clauses
SET clause_title = 'Service Contract Labor Standards--Place of Performance Unknown.' -- 'Service Contract Labor Standards-Place of Performance Unknown.'
WHERE clause_version_id = 14 AND clause_name = '52.222-49';

UPDATE Clauses
SET clause_title = 'Construction Wage Rate Requirements--Secondary Site of the Work.' -- 'Construction Wage Rate Requirements-Secondary Site of the Work.'
WHERE clause_version_id = 14 AND clause_name = '52.222-5';

UPDATE Clauses
SET clause_title = 'Ozone- Depleting Substances.' -- 'Ozone-Depleting Substances.'
WHERE clause_version_id = 14 AND clause_name = '52.223-11';

UPDATE Clauses
SET clause_title = 'Acquisition of EPEAT-Registered Imaging Equipment.' -- 'Acquisition of EPEAT®-Registered Imaging Equipment.'
WHERE clause_version_id = 14 AND clause_name = '52.223-13';

UPDATE Clauses
SET clause_title = 'Acquisition of EPEAT-Registered Televisions.' -- 'Acquisition of EPEAT®-Registered Televisions.'
WHERE clause_version_id = 14 AND clause_name = '52.223-14';

UPDATE Clauses
SET clause_title = 'Acquisition of EPEAT Registered Personal Computer Products.' -- 'Acquisition of EPEAT®-Registered Personal Computer Products.'
WHERE clause_version_id = 14 AND clause_name = '52.223-16';

UPDATE Clauses
SET clause_title = 'Affirmative Procurement of EPA-Designated Items in Service and Construction Contracts.' -- 'Affirmative Procurement of EPA-designated Items in Service and Construction Contracts.'
WHERE clause_version_id = 14 AND clause_name = '52.223-17';

UPDATE Clauses
SET clause_title = 'Encouraging Contractor Policies to Ban Text Messaging While Driving.' -- 'Encouraging Contractor Policies To Ban Text Messaging While Driving.'
WHERE clause_version_id = 14 AND clause_name = '52.223-18';

UPDATE Clauses
SET clause_title = 'Compliance With Environmental Management Systems.' -- 'Compliance with Environmental Management Systems.'
WHERE clause_version_id = 14 AND clause_name = '52.223-19';

UPDATE Clauses
SET clause_title = 'Pollution Prevention and Right-To-Know Information.' -- 'Pollution Prevention and Right-to-Know Information.'
WHERE clause_version_id = 14 AND clause_name = '52.223-5';

UPDATE Clauses
SET clause_title = 'Notice of Radioactive Materials.' -- 'Notice of radioactive materials.'
WHERE clause_version_id = 14 AND clause_name = '52.223-7';

UPDATE Clauses
SET clause_title = 'Estimate of Percentage of Recovered Material Content for EPA Designated Products.' -- 'Estimate of Percentage of Recovered Material Content for EPA-Designated Items.'
WHERE clause_version_id = 14 AND clause_name = '52.223-9';

UPDATE Clauses
SET clause_title = 'Buy American -- Supplies.' -- 'Buy American-Supplies.'
WHERE clause_version_id = 14 AND clause_name = '52.225-1';

UPDATE Clauses
SET clause_title = 'Notice of Buy American Requirement -- Construction Materials.' -- 'Notice of Buy American Requirement-Construction Materials.'
WHERE clause_version_id = 14 AND clause_name = '52.225-10';

UPDATE Clauses
SET clause_title = 'Buy American -- Construction Materials Under Trade Agreements.' -- 'Buy American-Construction Materials Under Trade Agreements.'
WHERE clause_version_id = 14 AND clause_name = '52.225-11';

UPDATE Clauses
SET clause_title = 'Notice of Buy American Requirement -- Construction Materials Under Trade Agreements.' -- 'Notice of Buy American Requirement-Construction Materials Under Trade Agreements.'
WHERE clause_version_id = 14 AND clause_name = '52.225-12';

UPDATE Clauses
SET clause_title = 'Inconsistency Between English Version and Translation of Contract.' -- 'Inconsistency between English Version and Translation of Contract.'
WHERE clause_version_id = 14 AND clause_name = '52.225-14';

UPDATE Clauses
SET clause_title = 'Prohibition on Contracting With Entities Engaging in Certain Activities or Transactions Relating to Iran--Representation and Certifications.' -- 'Prohibition on Contracting with Entities Engaging in Certain Activities or Transactions Relating to Iran-Representation and Certifications.'
WHERE clause_version_id = 14 AND clause_name = '52.225-25';

UPDATE Clauses
SET clause_title = 'Buy American -- Free Trade Agreements -- Israeli Trade Act.' -- 'Buy American-Free Trade Agreements-Israeli Trade Act.'
WHERE clause_version_id = 14 AND clause_name = '52.225-3';

UPDATE Clauses
SET clause_title = 'Buy American -- Free Trade Agreements -- Israeli Trade Act Certificate.' -- 'Buy American-Free Trade Agreement-Israeli Trade Act Certificate.'
WHERE clause_version_id = 14 AND clause_name = '52.225-4';

UPDATE Clauses
SET clause_title = 'Buy American -- Construction Materials.' -- 'Buy American-Construction Materials.'
WHERE clause_version_id = 14 AND clause_name = '52.225-9';

UPDATE Clauses
SET clause_title = 'Promoting Excess Food Donation to Nonprofit Organizations.' -- 'Promoting excess food donation to nonprofit organizations.'
WHERE clause_version_id = 14 AND clause_name = '52.226-6';

UPDATE Clauses
SET clause_title = 'Patent Rights--Retention by the Contractor (Short Form).' -- 'Patent Rights-Ownership by the Contractor.'
WHERE clause_version_id = 14 AND clause_name = '52.227-11';

UPDATE Clauses
SET clause_title = 'Patent Rights- Acquisition by the Government.' -- 'Patent Rights-Ownership by the Government.'
WHERE clause_version_id = 14 AND clause_name = '52.227-13';

UPDATE Clauses
SET clause_title = 'Rights in Data-Sbir Program.' -- 'Rights in Data-SBIR Program.'
WHERE clause_version_id = 14 AND clause_name = '52.227-20';

UPDATE Clauses
SET clause_title = 'Major System- Minimum Rights.' -- 'Major System-Minimum Rights.'
WHERE clause_version_id = 14 AND clause_name = '52.227-22';

UPDATE Clauses
SET clause_title = 'Patent Indemnity- Construction Contracts.' -- 'Patent Indemnity-Construction Contracts.'
WHERE clause_version_id = 14 AND clause_name = '52.227-4';

UPDATE Clauses
SET clause_title = 'Performance and Payment Bonds- Construction.' -- 'Performance and Payment Bonds-Construction.'
WHERE clause_version_id = 14 AND clause_name = '52.228-15';

UPDATE Clauses
SET clause_title = 'Liability and Insurance- Leased Motor Vehicles.' -- 'Liability and Insurance-Leased Motor Vehicles.'
WHERE clause_version_id = 14 AND clause_name = '52.228-8';

UPDATE Clauses
SET clause_title = 'Taxes-Cost Reimbursement Contracts With Foreign Governments.' -- 'Taxes-Cost-Reimbursement Contracts With Foreign Governments.'
WHERE clause_version_id = 14 AND clause_name = '52.229-9';

UPDATE Clauses
SET clause_title = 'Disclosure and Consistency of Cost Accounting Practices--Foreign Concerns.' -- 'Disclosure and Consistency of Cost Accounting Practices-Foreign Concerns.'
WHERE clause_version_id = 14 AND clause_name = '52.230-4';

UPDATE Clauses
SET clause_title = 'Cost Accounting Standards-- Educational Institution.' -- 'Cost Accounting Standards-Educational Institution.'
WHERE clause_version_id = 14 AND clause_name = '52.230-5';

UPDATE Clauses
SET clause_title = 'Proposal Disclosure--Cost Accounting Practice Changes.' -- 'Proposal Disclosure-Cost Accounting Practice Changes.'
WHERE clause_version_id = 14 AND clause_name = '52.230-7';

UPDATE Clauses
SET clause_title = 'Payments Under Fixed- Price Architect- Engineering Contracts.' -- 'Payments Under Fixed-Price Architect-Engineer Contracts.'
WHERE clause_version_id = 14 AND clause_name = '52.232-10';

UPDATE Clauses
SET clause_title = 'Payments Under Fixed- Price Research and Development Contracts.' -- 'Payments Under Fixed-Price Research and Development Contracts.'
WHERE clause_version_id = 14 AND clause_name = '52.232-2';

UPDATE Clauses
SET clause_title = 'Prompt Payment.' -- 'Prompt payment.'
WHERE clause_version_id = 14 AND clause_name = '52.232-25';

UPDATE Clauses
SET clause_title = 'Prompt Payment for Fixed-Price Architect-Engineer Contracts.' -- 'Prompt payment for fixed-price architect-engineer contracts.'
WHERE clause_version_id = 14 AND clause_name = '52.232-26';

UPDATE Clauses
SET clause_title = 'Prompt Payment for Construction Contracts.' -- 'Prompt payment for construction contracts.'
WHERE clause_version_id = 14 AND clause_name = '52.232-27';

UPDATE Clauses
SET clause_title = 'Invitation to Propose Performance-Based Payments.' -- 'Invitation To Propose Performance-Based Payments.'
WHERE clause_version_id = 14 AND clause_name = '52.232-28';

UPDATE Clauses
SET clause_title = 'Terms for Financing of Purchase of Commercial Items.' -- 'Terms for Financing of Purchases of Commercial Items.'
WHERE clause_version_id = 14 AND clause_name = '52.232-29';

UPDATE Clauses
SET clause_title = 'Invitation to Propose Financing Terms.' -- 'Invitation To Propose Financing Terms.'
WHERE clause_version_id = 14 AND clause_name = '52.232-31';

UPDATE Clauses
SET clause_title = 'Payment by Electronic Funds Transfer--Other Than System for Award Management.' -- 'Payment by Electronic Funds Transfer-Other than System for Award Management.'
WHERE clause_version_id = 14 AND clause_name = '52.232-34';

UPDATE Clauses
SET clause_title = 'Submission of Electronic Funds Transfer Information With Offer.' -- 'Submission of Electronic Funds Transfer Information with Offer.'
WHERE clause_version_id = 14 AND clause_name = '52.232-38';

UPDATE Clauses
SET clause_title = 'Unenforceabilityof Unauthorized Obligations.' -- 'Unenforceability of Unauthorized Obligations.'
WHERE clause_version_id = 14 AND clause_name = '52.232-39';

UPDATE Clauses
SET clause_title = 'Payments Under Time-And- Materials and Labor-Hour Contracts.' -- 'Payments under Time-and-Materials and Labor-Hour Contracts.'
WHERE clause_version_id = 14 AND clause_name = '52.232-7';

UPDATE Clauses
SET clause_title = 'Industrial Resources Developed Under Defense Production Act, Title III.' -- 'Industrial Resources Developed Under Defense Production Act Title III.'
WHERE clause_version_id = 14 AND clause_name = '52.234-1';

UPDATE Clauses
SET clause_title = 'Notice of Earned Value Management System - Pre-Award Ibr.' -- 'Notice of Earned Value Management System-Pre-Award IBR.'
WHERE clause_version_id = 14 AND clause_name = '52.234-2';

UPDATE Clauses
SET clause_title = 'Notice of Earned Value Management System - Post-Award Ibr.' -- 'Notice of Earned Value Management System-Post Award IBR.'
WHERE clause_version_id = 14 AND clause_name = '52.234-3';

UPDATE Clauses
SET clause_title = 'Use & Possession Prior to Completion.' -- 'Use and Possession Prior to Completion.'
WHERE clause_version_id = 14 AND clause_name = '52.236-11';

UPDATE Clauses
SET clause_title = 'Responsibility of the Architect- Engineering Contractor.' -- 'Responsibility of the Architect-Engineer Contractor.'
WHERE clause_version_id = 14 AND clause_name = '52.236-23';

UPDATE Clauses
SET clause_title = 'Work Oversight in Architect- Engineering Contracts.' -- 'Work Oversight in Architect-Engineer Contracts.'
WHERE clause_version_id = 14 AND clause_name = '52.236-24';

UPDATE Clauses
SET clause_title = 'Preparation of Proposal-Construction.' -- 'Preparation of Proposals-Construction.'
WHERE clause_version_id = 14 AND clause_name = '52.236-28';

UPDATE Clauses
SET clause_title = 'Accepting and Dispensing of 1 Coin.' -- 'Accepting and Dispensing of $1 Coin.'
WHERE clause_version_id = 14 AND clause_name = '52.237-11';

UPDATE Clauses
SET clause_title = 'Order of Precedents- Utilities.' -- 'Order of Precedence-Utilities.'
WHERE clause_version_id = 14 AND clause_name = '52.241-2';

UPDATE Clauses
SET clause_title = 'Scope of Duration of Contract.' -- 'Scope and Duration of Contract.'
WHERE clause_version_id = 14 AND clause_name = '52.241-3';

UPDATE Clauses
SET clause_title = 'Notice of Intent to Disallow Costs.' -- 'Notice of Intent To Disallow Costs.'
WHERE clause_version_id = 14 AND clause_name = '52.242-1';

UPDATE Clauses
SET clause_title = 'Changes--Fixed Price.' -- 'Changes-Fixed-Price.'
WHERE clause_version_id = 14 AND clause_name = '52.243-1';

UPDATE Clauses
SET clause_title = 'Changes-Cost Reimbursement.' -- 'Changes-Cost-Reimbursement.'
WHERE clause_version_id = 14 AND clause_name = '52.243-2';

UPDATE Clauses
SET clause_title = 'Changes--Time-And-Materials or Labor-Hours.' -- 'Changes-Time-and-Materials or Labor-Hours.'
WHERE clause_version_id = 14 AND clause_name = '52.243-3';

UPDATE Clauses
SET clause_conditions = '(A) DOCUMENT TYPE IS: SOLICITATION\n(B) DOCUMENT TYPE IS: AWARD\n(C) CONTRACT TYPE IS: COST REIMBURSEMENT\n(D) AWARD TYPE IS: LETTER CONTRACT\n(E) CONTRACT VALUE IS: GREATER THAN SAT\n(F) CONTRACT TYPE IS: FIXED PRICE\n(G) MISCELLANEOUS PROGRAM CHARACTERISTICS IS: THE CONTRACT WILL PROVIDE FOR USE OF UNDEFINITIZED CONTRACT ACTIONS\n(H) CONTRACT TYPE IS: TIME AND MATERIALS\n(I) CONTRACT TYPE IS: LABOR HOUR\n(J) SERVICES IS: ARCHITECT-ENGINEERING SERVICES\n(K) SERVICES IS: NOT MORTUARY SERVICES\n(L) SERVICES IS: NOT REFUSE SERVICES\n(M) SERVICES IS: NOT PREPARATION, SHIPMENT AND STORAGE OF PERSONAL PROPERTY\n(N) 52.244-2 ALTERNATE I IS: NOT INCLUDED\n(O) PROCEDURES IS: NOT COMMERCIAL PROCEDURES (FAR PART 12)\n(P) 52.244-2 ALTERNATE I IS: INCLUDED' -- '(A) DOCUMENT TYPE IS: SOLICITATION\n(B) DOCUMENT TYPE IS: AWARD\n(C) CONTRACT TYPE IS: COST REIMBURSEMENT\n(D) AWARD TYPE IS: LETTER CONTRACT\n(E) CONTRACT VALUE IS: GREATER THAN SAT\n(F) CONTRACT TYPE IS: FIXED PRICE\n(G) MISCELLANEOUS PROGRAM CHARACTERISTICS IS: THE CONTRACT WILL PROVIDE FOR USE OF UNDEFINITIZED CONTRACT ACTIONS\n(H) CONTRACT TYPE IS: TIME AND MATERIALS\n(I) CONTRACT TYPE IS: LABOR HOUR\n(J) SERVICES IS: ARCHITECT - ENGINEERING\n(K) SERVICES IS: NOT MORTUARY SERVICES\n(L) SERVICES IS: NOT REFUSE SERVICES\n(M) SERVICES IS: NOT PREPARATION, SHIPMENT AND STORAGE OF PERSONAL PROPERTY\n(N) 52.244-2 ALTERNATE I IS: NOT INCLUDED\n(O) PROCEDURES IS: NOT COMMERCIAL PROCEDURES (FAR PART 12)\n(P) 52.244-2 ALTERNATE I IS: INCLUDED'
WHERE clause_version_id = 14 AND clause_name = '52.244-2';

UPDATE Clauses
SET clause_title = 'Subcontractors and Outside Associates and Consultants (Architect-Engineer Services).' -- 'Subcontractors and outside associates and consultants (Architect-engineer services).'
WHERE clause_version_id = 14 AND clause_name = '52.244-4';

UPDATE Clauses
SET clause_title = 'Inspection--Dismantling, Demolition, or Removal of Improvements.' -- 'Inspection-Dismantling, Demolition, or Removal of Improvements.'
WHERE clause_version_id = 14 AND clause_name = '52.246-13';

UPDATE Clauses
SET clause_title = 'Warranty of Supplies of a Non- Complex Nature.' -- 'Warranty of Supplies of a Noncomplex Nature.'
WHERE clause_version_id = 14 AND clause_name = '52.246-17';

UPDATE Clauses
SET clause_title = 'Warranty of Systems and Equipment Under Performance Specifications or Design Criteria.' -- 'Warranty of Systems and Equipment under Performance Specifications or Design Criteria.'
WHERE clause_version_id = 14 AND clause_name = '52.246-19';

UPDATE Clauses
SET clause_title = 'Inspection of Supplies--Fixed-Price.' -- 'Inspection of Supplies-Fixed-Price.'
WHERE clause_version_id = 14 AND clause_name = '52.246-2';

UPDATE Clauses
SET clause_title = 'Limitation of Liability--High Value Items.' -- 'Limitation of Liability-High-Value Items.'
WHERE clause_version_id = 14 AND clause_name = '52.246-24';

UPDATE Clauses
SET clause_title = 'Limitation of Liability--Services.' -- 'Limitation of Liability-Services.'
WHERE clause_version_id = 14 AND clause_name = '52.246-25';

UPDATE Clauses
SET clause_title = 'Inspection of Supplies--Cost- Reimbursement.' -- 'Inspection of Supplies-Cost-Reimbursement.'
WHERE clause_version_id = 14 AND clause_name = '52.246-3';

UPDATE Clauses
SET clause_title = 'Inspection of Services--Fixed Price.' -- 'Inspection of Services-Fixed-Price.'
WHERE clause_version_id = 14 AND clause_name = '52.246-4';

UPDATE Clauses
SET clause_title = 'Inspection of Services--Cost-Reimbursement.' -- 'Inspection of Services-Cost-Reimbursement.'
WHERE clause_version_id = 14 AND clause_name = '52.246-5';

UPDATE Clauses
SET clause_title = 'Inspection--Time-And-Material and Labor-Hour.' -- 'Inspection-Time-and-Material and Labor-Hour.'
WHERE clause_version_id = 14 AND clause_name = '52.246-6';

UPDATE Clauses
SET clause_title = 'Inspection of Research and Development--Fixed Price.' -- 'Inspection of Research and Development-Fixed-Price.'
WHERE clause_version_id = 14 AND clause_name = '52.246-7';

UPDATE Clauses
SET clause_title = 'Inspection of Research and Development--Cost Reimbursement.' -- 'Inspection of Research and Development-Cost-Reimbursement.'
WHERE clause_version_id = 14 AND clause_name = '52.246-8';

UPDATE Clauses
SET clause_title = 'Net Weight- Household Goods or Office Furniture.' -- 'Net Weight-Household Goods or Office Furniture.'
WHERE clause_version_id = 14 AND clause_name = '52.247-11';

UPDATE Clauses
SET clause_title = 'Stopping in Transit for Partial Uploading.' -- 'Stopping in Transit for Partial Unloading.'
WHERE clause_version_id = 14 AND clause_name = '52.247-19';

UPDATE Clauses
SET clause_title = 'F.O.B. Origin.' -- 'F.o.b. Origin.'
WHERE clause_version_id = 14 AND clause_name = '52.247-29';

UPDATE Clauses
SET clause_title = 'Capability to Perform a Contract for the Relocation of a Federal Office.' -- 'Capability To Perform a Contract for the Relocation of a Federal Office.'
WHERE clause_version_id = 14 AND clause_name = '52.247-3';

UPDATE Clauses
SET clause_title = 'F.O.B. Origin, Contractor''s Facility.' -- 'F.o.b. Origin, Contractor''s Facility.'
WHERE clause_version_id = 14 AND clause_name = '52.247-30';

UPDATE Clauses
SET clause_title = 'F.O.B. Origin, Freight Allowed.' -- 'F.o.b. Origin, Freight Allowed.'
WHERE clause_version_id = 14 AND clause_name = '52.247-31';

UPDATE Clauses
SET clause_title = 'F.O.B. Origin, Freight Prepaid.' -- 'F.o.b. Origin, Freight Prepaid.'
WHERE clause_version_id = 14 AND clause_name = '52.247-32';

UPDATE Clauses
SET clause_title = 'F.O.B. Origin, With Differentials.' -- 'F.o.b. Origin, With Differentials.'
WHERE clause_version_id = 14 AND clause_name = '52.247-33';

UPDATE Clauses
SET clause_title = 'F.O.B. Destination.' -- 'F.o.b. Destination.'
WHERE clause_version_id = 14 AND clause_name = '52.247-34';

UPDATE Clauses
SET clause_title = 'F.O.B. Destination, Within Consignee''s Premises.' -- 'F.o.b. Destination, Within Consignee''s Premises.'
WHERE clause_version_id = 14 AND clause_name = '52.247-35';

UPDATE Clauses
SET clause_title = 'F.A.S. Vessel, Port of Shipment.' -- 'F.a.s. Vessel, Port of Shipment.'
WHERE clause_version_id = 14 AND clause_name = '52.247-36';

UPDATE Clauses
SET clause_title = 'F.O.B.. Vessel, Port of Shipment.' -- 'F.o.b. Vessel, Port of Shipment.'
WHERE clause_version_id = 14 AND clause_name = '52.247-37';

UPDATE Clauses
SET clause_title = 'F.O.B. Inland Carrier, Point of Exportation.' -- 'F.o.b. Inland Carrier, Point of Exportation.'
WHERE clause_version_id = 14 AND clause_name = '52.247-38';

UPDATE Clauses
SET clause_title = 'F.O.B.. Inland Point, Country of Importation.' -- 'F.o.b. Inland Point, Country of Importation.'
WHERE clause_version_id = 14 AND clause_name = '52.247-39';

UPDATE Clauses
SET clause_title = 'C.&F. Destination.' -- 'C.& f. Destination.'
WHERE clause_version_id = 14 AND clause_name = '52.247-41';

UPDATE Clauses
SET clause_title = 'C.I.F. Destination.' -- 'C.i.f. Destination.'
WHERE clause_version_id = 14 AND clause_name = '52.247-42';

UPDATE Clauses
SET clause_title = 'F.O.B. Designated Air Carrier''s Terminal, Point of Exportation.' -- 'F.o.b. Designated Air Carrier''s Terminal, Point of Exportation.'
WHERE clause_version_id = 14 AND clause_name = '52.247-43';

UPDATE Clauses
SET clause_title = 'F.O.B. Designated Air Carrier''s Terminal, Point of Importation.' -- 'F.o.b. Designated Air Carrier''s Terminal, Point of Importation.'
WHERE clause_version_id = 14 AND clause_name = '52.247-44';

UPDATE Clauses
SET clause_title = 'F.O.B. Origin and/or F.O.B. Destination Evaluation.' -- 'F.o.b. Origin and/or F.o.b. Destination Evaluation.'
WHERE clause_version_id = 14 AND clause_name = '52.247-45';

UPDATE Clauses
SET clause_title = 'Shipping Point(s) Used in Evaluation of F.O.B. Origin Offers.' -- 'Shipping Point(s) Used in Evaluation of F.o.b. Origin Offers.'
WHERE clause_version_id = 14 AND clause_name = '52.247-46';

UPDATE Clauses
SET clause_title = 'Evaluation- F.O.B. Origin.' -- 'Evaluation-F.o.b. Origin.'
WHERE clause_version_id = 14 AND clause_name = '52.247-47';

UPDATE Clauses
SET clause_title = 'F.O.B. Destination--Evidence of Shipment.' -- 'F.o.b. Destination-Evidence of Shipment.'
WHERE clause_version_id = 14 AND clause_name = '52.247-48';

UPDATE Clauses
SET clause_title = 'Clearance and Documentation Requirements--Shipments to DOD Air or Water Terminal Trans-Shipment Points.' -- 'Clearance and Documentation Requirements-Shipments to DOD Air or Water Terminal Transshipment Points.'
WHERE clause_version_id = 14 AND clause_name = '52.247-52';

UPDATE Clauses
SET clause_title = 'F.O.B. Point for Delivery of Government- Furnished Property.' -- 'F.o.b. Point for Delivery of Government-Furnished Property.'
WHERE clause_version_id = 14 AND clause_name = '52.247-55';

UPDATE Clauses
SET clause_title = 'F.O.B. Origin-Car-Load and Truckload Shipments.' -- 'F.o.b. Origin-Carload and Truckload Shipments.'
WHERE clause_version_id = 14 AND clause_name = '52.247-59';

UPDATE Clauses
SET clause_title = 'Guaranteed Shipping.' -- 'Guaranteed Shipping Characteristics.'
WHERE clause_version_id = 14 AND clause_name = '52.247-60';

UPDATE Clauses
SET clause_title = 'F.O.B. Origin- Minimum Size of Shipments.' -- 'F.o.b. Origin-Minimum Size of Shipments.'
WHERE clause_version_id = 14 AND clause_name = '52.247-61';

UPDATE Clauses
SET clause_title = 'F.O.B. Origin, Prepaid Freight--Small Package Shipments.' -- 'F.o.b. Origin, Prepaid Freight-Small Package Shipments.'
WHERE clause_version_id = 14 AND clause_name = '52.247-65';

UPDATE Clauses
SET clause_title = 'Report of Shipment (Repship).' -- 'Report of Shipment (REPSHIP).'
WHERE clause_version_id = 14 AND clause_name = '52.247-68';

UPDATE Clauses
SET clause_title = 'Value Engineering Program-Architect Engineer.' -- 'Value Engineering-Architect-Engineer.'
WHERE clause_version_id = 14 AND clause_name = '52.248-2';

UPDATE Clauses
SET clause_title = 'Value Engineering--Construction.' -- 'Value Engineering-Construction.'
WHERE clause_version_id = 14 AND clause_name = '52.248-3';

UPDATE Clauses
SET clause_title = 'Termination (Cost- Reimbursement).' -- 'Termination (Cost-Reimbursement).'
WHERE clause_version_id = 14 AND clause_name = '52.249-6';

UPDATE Clauses
SET clause_title = 'Termination (Fixed- Price Architect- Engineer).' -- 'Termination (Fixed-Price Architect-Engineer).'
WHERE clause_version_id = 14 AND clause_name = '52.249-7';

UPDATE Clauses
SET clause_title = 'Default (Fixed- Price Supply and Service).' -- 'Default (Fixed-Price Supply and Service).'
WHERE clause_version_id = 14 AND clause_name = '52.249-8';

UPDATE Clauses
SET clause_title = 'Safety Act Coverage Not Applicable.' -- 'SAFETY Act Coverage Not Applicable.'
WHERE clause_version_id = 14 AND clause_name = '52.250-2';

UPDATE Clauses
SET clause_title = 'Safety Act Block Designation/Certification.' -- 'SAFETY Act Block Designation/Certification.'
WHERE clause_version_id = 14 AND clause_name = '52.250-3';

UPDATE Clauses
SET clause_title = 'Safety Act Pre-Qualification Designation Notice.' -- 'SAFETY Act Pre-qualification Designation Notice.'
WHERE clause_version_id = 14 AND clause_name = '52.250-4';

UPDATE Clauses
SET clause_title = 'Safety Act-Equitable Adjustment.' -- 'SAFETY Act-Equitable Adjustment.'
WHERE clause_version_id = 14 AND clause_name = '52.250-5';

UPDATE Clauses
SET clause_title = 'Interagency Fleet Management System Vehicle and Related Services.' -- 'Interagency Fleet Management System Vehicles and Related Services.'
WHERE clause_version_id = 14 AND clause_name = '52.251-2';

UPDATE Clauses
SET clause_title = 'Exercise of Option to Fulfill Foreign Military Sales Commitments.' -- 'Exercise of option to fulfill foreign military sales commitments.'
WHERE clause_version_id = 14 AND clause_name = '252.217-7000 Alternate I';

UPDATE Clauses
SET clause_title = 'Small Business Subcontracting Plan (DOD Contracts).' -- 'Small business subcontracting plan (DoD contracts).'
WHERE clause_version_id = 14 AND clause_name = '252.219-7003 Alternate I';

UPDATE Clauses
SET clause_title = 'Small Business Subcontracting Plan (DOD Contracts).' -- 'Small business subcontracting plan (DoD contracts).'
WHERE clause_version_id = 14 AND clause_name = '252.219-7003 Alternate I (DEVIATION 2013-00014)';

UPDATE Clauses
SET clause_title = 'Trade Agreements.' -- 'Trade agreements.'
WHERE clause_version_id = 14 AND clause_name = '252.225-7021 Alternate II';

UPDATE Clauses
SET clause_title = 'Buy Buy American-Free Trade Agreements-Balance of Payments Program Certificate.' -- 'Buy American-Free Trade Agreements-Balance of Payments Program Certificate.'
WHERE clause_version_id = 14 AND clause_name = '252.225-7035 Alternate II';

UPDATE Clauses
SET clause_title = 'Buy American-Free Trade Agreements--Balance of Payments.' -- 'Buy American-Free Trade Agreements-Balance of Payments Program.'
WHERE clause_version_id = 14 AND clause_name = '252.225-7036 Alternate V';

UPDATE Clauses
SET clause_title = 'Balance of Payments Program - Construction Material Under Trade Agreements.' -- 'Balance of Payments Program-Construction Material Under Trade Agreements.'
WHERE clause_version_id = 14 AND clause_name = '252.225-7045 Alternate I';

UPDATE Clauses
SET clause_title = 'Balance of Payments Program - Construction Material Under Trade Agreements.' -- 'Balance of Payments Program-Construction Material Under Trade Agreements.'
WHERE clause_version_id = 14 AND clause_name = '252.225-7045 Alternate II';

UPDATE Clauses
SET clause_title = 'Balance of Payments Program - Construction Material Under Trade Agreements.' -- 'Balance of Payments Program-Construction Material Under Trade Agreements.'
WHERE clause_version_id = 14 AND clause_name = '252.225-7045 Alternate III';

UPDATE Clauses
SET clause_title = 'License Term.' -- 'License term.'
WHERE clause_version_id = 14 AND clause_name = '252.227-7005 Alternate I';

UPDATE Clauses
SET clause_title = 'License Term.' -- 'License term.'
WHERE clause_version_id = 14 AND clause_name = '252.227-7005 Alternate II';

UPDATE Clauses
SET clause_title = 'Rights in Technical Data--Noncommercial Items.' -- 'Rights in technical data-Noncommercial items.'
WHERE clause_version_id = 14 AND clause_name = '252.227-7013 Alternate I';

UPDATE Clauses
SET clause_title = 'Rights in Technical Data--Noncommercial Items.' -- 'Rights in technical data-Noncommercial items.'
WHERE clause_version_id = 14 AND clause_name = '252.227-7013 Alternate II';

UPDATE Clauses
SET clause_title = 'Rights in Noncommercial Computer Software and Noncommercial Computer Software Documentation.' -- 'Rights in noncommercial computer software and noncommercial computer software documentation.'
WHERE clause_version_id = 14 AND clause_name = '252.227-7014 Alternate I';

UPDATE Clauses
SET clause_title = 'Technical DataCommercial Items.' -- 'Technical data-Commercial items.'
WHERE clause_version_id = 14 AND clause_name = '252.227-7015 Alternate I';

UPDATE Clauses
SET clause_title = 'Rights in Noncommercial Technical Data and Computer Software--Small Business Innovation Research (SBIR) Program.' -- 'Rights in noncommercial technical data and computer software-Small Business Innovation Research (SBIR) Program.'
WHERE clause_version_id = 14 AND clause_name = '252.227-7018 Alternate I';

UPDATE Clauses
SET clause_title = 'Frequency Authorization.' -- 'Frequency authorization.'
WHERE clause_version_id = 14 AND clause_name = '252.235-7003 Alternate I';

UPDATE Clauses
SET clause_title = 'Award to Single Offeror.' -- 'Award to single offeror.'
WHERE clause_version_id = 14 AND clause_name = '252.237-7002 Alternate I';

UPDATE Clauses
SET clause_title = 'Delivery Tickets.' -- 'Delivery tickets.'
WHERE clause_version_id = 14 AND clause_name = '252.237-7016 Alternate I';

UPDATE Clauses
SET clause_title = 'Delivery Tickets.' -- 'Delivery tickets.'
WHERE clause_version_id = 14 AND clause_name = '252.237-7016 Alternate II';

UPDATE Clauses
SET clause_title = 'Contractor Purchasing System Administration.' -- 'Contractor purchasing system administration.'
WHERE clause_version_id = 14 AND clause_name = '252.244-7001 Alternate I';

UPDATE Clauses
SET clause_title = 'Warranty of Data.' -- 'Warranty of data.'
WHERE clause_version_id = 14 AND clause_name = '252.246-7001 Alternate I';

UPDATE Clauses
SET clause_title = 'Warranty of Data.' -- 'Warranty of data.'
WHERE clause_version_id = 14 AND clause_name = '252.246-7001 Alternate II';

UPDATE Clauses
SET clause_title = 'Evaluation of Bids.' -- 'Evaluation of bids.'
WHERE clause_version_id = 14 AND clause_name = '252.247-7008 Alternate I';

UPDATE Clauses
SET clause_title = 'Transportation of Supplies by Sea.' -- 'Transportation of supplies by sea.'
WHERE clause_version_id = 14 AND clause_name = '252.247-7023 Alternate I';

UPDATE Clauses
SET clause_title = 'Transportation of Supplies by Sea.' -- 'Transportation of supplies by sea.'
WHERE clause_version_id = 14 AND clause_name = '252.247-7023 Alternate II';

UPDATE Clauses
SET clause_title = 'First Article Approval- Contractor Testing.' -- 'First Article Approval-Contractor Testing.'
WHERE clause_version_id = 14 AND clause_name = '52.209-3 Alternate I';

UPDATE Clauses
SET clause_title = 'First Article Approval- Contractor Testing.' -- 'First Article Approval-Contractor Testing.'
WHERE clause_version_id = 14 AND clause_name = '52.209-3 Alternate II';

UPDATE Clauses
SET clause_title = 'Contractors Representations and Certifications-Commercial Items.' -- 'Offeror Representations and Certifications-Commercial Items.'
WHERE clause_version_id = 14 AND clause_name = '52.212-3 Alternate I';

UPDATE Clauses
SET clause_title = 'Contract Terms and Conditions--Commercial Items.' -- 'Contract Terms and Conditions-Commercial Items.'
WHERE clause_version_id = 14 AND clause_name = '52.212-4 Alternate I';

UPDATE Clauses
SET clause_title = 'Contract Terms and Conditions Required to Implement Statutes or Executive Orders- Commercial Items.' -- 'Contract Terms and Conditions Required To Implement Statutes or Executive Orders-Commercial Items.'
WHERE clause_version_id = 14 AND clause_name = '52.212-5 Alternate I';

UPDATE Clauses
SET clause_title = 'Contract Terms and Conditions Required to Implement Statutes or Executive Orders-Commercial Items.' -- 'Contract Terms and Conditions Required To Implement Statutes or Executive Orders-Commercial Items.'
WHERE clause_version_id = 14 AND clause_name = '52.212-5 Alternate I (DEVIATION 2013-00019)';

UPDATE Clauses
SET clause_title = 'Contract Terms and Conditions Required to Implement Statutes or Executive Orders- Commercial Items.' -- 'Contract Terms and Conditions Required To Implement Statutes or Executive Orders-Commercial Items.'
WHERE clause_version_id = 14 AND clause_name = '52.212-5 Alternate II';

UPDATE Clauses
SET clause_title = 'Contract Terms and Conditions Required to Implement Statutes or Executive Orders-Commercial Items.' -- 'Contract Terms and Conditions Required To Implement Statutes or Executive Orders-Commercial Items.'
WHERE clause_version_id = 14 AND clause_name = '52.212-5 Alternate II (DEVIATION 2013-00019)';

UPDATE Clauses
SET clause_title = 'Audit and Records--Sealed Bidding.' -- 'Audit and Records-Sealed Bidding.'
WHERE clause_version_id = 14 AND clause_name = '52.214-26 Alternate I';

UPDATE Clauses
SET clause_title = 'Instructions to Contractors-Competitive.' -- 'Instructions to Offerors-Competitive Acquisition.'
WHERE clause_version_id = 14 AND clause_name = '52.215-1 Alternate I';

UPDATE Clauses
SET clause_title = 'Instructions to Contractors-Competitive.' -- 'Instructions to Offerors-Competitive Acquisition.'
, clause_data = '<p>Alternate II (OCT 1997). As prescribed in 15.209(a)(2), add a paragraph (c)(9) substantially the same as the following to the basic clause:</p><p>(9) Offerors may submit proposals that depart from stated requirements. Such proposals shall clearly identify why the acceptance of the proposal would be advantageous to the Government. Any deviations from the terms and conditions of the solicitation, as well as the comparative advantage to the Government, shall be clearly identified and explicitly defined. The Government reserves the right to amend the solicitation to allow all offerors an opportunity to submit revised proposals based on the revised requirements.</p>'
--               '<p>Alternate II (OCT 1997). As prescribed in 15.209(a)(2), add a paragraph (c)(9) substantially the same as the following to the basic clause:</p><p>{{paragraph_c9_52.215-1-AltII}}</p>'
WHERE clause_version_id = 14 AND clause_name = '52.215-1 Alternate II';

DELETE FROM Document_Fill_Ins WHERE clause_fill_in_id IN (SELECT clause_fill_in_id FROM Clause_Fill_Ins CF INNER JOIN Clauses C  ON C.clause_id= CF.clause_id
WHERE clause_name = '52.215-1 Alternate II' AND clause_version_id = 14 AND fill_in_code='paragraph_c9_52.215-1-AltII');
DELETE FROM Clause_Fill_Ins WHERE clause_id IN (SELECT clause_id FROM Clauses 
WHERE clause_name = '52.215-1 Alternate II' AND clause_version_id = 14)  AND fill_in_code='paragraph_c9_52.215-1-AltII';

UPDATE Clauses
SET clause_title = 'Audit and Records- Negotiations.' -- 'Audit and Records-Negotiation.'
WHERE clause_version_id = 14 AND clause_name = '52.215-2 Alternate I';

UPDATE Clauses
SET clause_title = 'Audit and Records- Negotiations.' -- 'Audit and Records-Negotiation.'
WHERE clause_version_id = 14 AND clause_name = '52.215-2 Alternate II';

UPDATE Clauses
SET clause_title = 'Audit and Records- Negotiations.' -- 'Audit and Records-Negotiation.'
WHERE clause_version_id = 14 AND clause_name = '52.215-2 Alternate III';

UPDATE Clauses
SET clause_title = 'Requirements for Certified Cost or Pricing Data and Data Other Than Certified Cost or Pricing Data - Modifications.' -- 'Requirements for Certified Cost or Pricing Data and Data Other Than Certified Cost or Pricing Data-Modifications.'
WHERE clause_version_id = 14 AND clause_name = '52.215-21 Alternate I';

UPDATE Clauses
SET clause_title = 'Requirements for Certified Cost or Pricing Data and Data Other Than Certified Cost or Pricing Data - Modifications.' -- 'Requirements for Certified Cost or Pricing Data and Data Other Than Certified Cost or Pricing Data-Modifications.'
WHERE clause_version_id = 14 AND clause_name = '52.215-21 Alternate II';

UPDATE Clauses
SET clause_title = 'Requirements for Certified Cost or Pricing Data and Data Other Than Certified Cost or Pricing Data - Modifications.' -- 'Requirements for Certified Cost or Pricing Data and Data Other Than Certified Cost or Pricing Data-Modifications.'
WHERE clause_version_id = 14 AND clause_name = '52.215-21 Alternate III';

UPDATE Clauses
SET clause_title = 'Requirements for Certified Cost or Pricing Data and Data Other Than Certified Cost or Pricing Data - Modifications.' -- 'Requirements for Certified Cost or Pricing Data and Data Other Than Certified Cost or Pricing Data-Modifications.'
WHERE clause_version_id = 14 AND clause_name = '52.215-21 Alternate IV';

UPDATE Clauses
SET clause_title = 'Small Business Program Representation.' -- 'Small Business Program Representations.'
WHERE clause_version_id = 14 AND clause_name = '52.219-1 Alternate I';

UPDATE Clauses
SET clause_title = 'Notice of Hubzone Set-Aside or Sole Source Award.' -- 'Notice of HUBZone Set-Aside or Sole Source Award.'
WHERE clause_version_id = 14 AND clause_name = '52.219-3 Alternate I';

UPDATE Clauses
SET clause_title = 'Notice of Price Evaluation Preference for Hubzone Small Business Concerns.' -- 'Notice of Price Evaluation Preference for HUBZone Small Business Concerns.'
WHERE clause_version_id = 14 AND clause_name = '52.219-4 Alternate I';

UPDATE Clauses
SET clause_title = 'Small Business Subcontracting Plan.' -- 'Small business subcontracting plan.'
WHERE clause_version_id = 14 AND clause_name = '52.219-9 Alternate I';

UPDATE Clauses
SET clause_title = 'Small Business Subcontracting Plan.' -- 'Small business subcontracting plan.'
WHERE clause_version_id = 14 AND clause_name = '52.219-9 Alternate I (DEVIATION 2013-00014)';

UPDATE Clauses
SET clause_title = 'Small Business Subcontracting Plan.' -- 'Small business subcontracting plan.'
WHERE clause_version_id = 14 AND clause_name = '52.219-9 Alternate II';

UPDATE Clauses
SET clause_title = 'Small Business Subcontracting Plan.' -- 'Small business subcontracting plan.'
WHERE clause_version_id = 14 AND clause_name = '52.219-9 Alternate II (DEVIATION 2013-00014)';

UPDATE Clauses
SET clause_title = 'Small Business Subcontracting Plan.' -- 'Small business subcontracting plan.'
WHERE clause_version_id = 14 AND clause_name = '52.219-9 Alternate III';

UPDATE Clauses
SET clause_title = 'Small Business Subcontracting Plan.' -- 'Small business subcontracting plan.'
WHERE clause_version_id = 14 AND clause_name = '52.219-9 Alternate III (DEVIATION 2013-00014)';

UPDATE Clauses
SET clause_title = 'Notice of Requirement Project Labor Agreement.' -- 'Notice of Requirement for Project Labor Agreement.'
WHERE clause_version_id = 14 AND clause_name = '52.222-33 Alternate I';

UPDATE Clauses
SET clause_title = 'Notice of Requirement Project Labor Agreement.' -- 'Notice of Requirement for Project Labor Agreement.'
WHERE clause_version_id = 14 AND clause_name = '52.222-33 Alternate II';

UPDATE Clauses
SET clause_title = 'Equal Opportunity Veterans.' -- 'Equal Opportunity for Veterans.'
WHERE clause_version_id = 14 AND clause_name = '52.222-35 Alternate I';

UPDATE Clauses
SET clause_title = 'Equal Opportunity for Workers With Disabilities.' -- 'Equal Opportunity for Workers with Disabilities.'
WHERE clause_version_id = 14 AND clause_name = '52.222-36 Alternate I';

UPDATE Clauses
SET clause_title = 'Acquisition of EPEAT-Registered Imaging Equipment.' -- 'Acquisition of EPEAT®-Registered Imaging Equipment.'
WHERE clause_version_id = 14 AND clause_name = '52.223-13 Alternate I';

UPDATE Clauses
SET clause_title = 'Acquisition of EPEAT-Registered Televisions.' -- 'Acquisition of EPEAT®-Registered Televisions.'
WHERE clause_version_id = 14 AND clause_name = '52.223-14 Alternate I';

UPDATE Clauses
SET clause_title = 'Acquisition of EPEAT Registered Personal Computer Products.' -- 'Acquisition of EPEAT®-Registered Personal Computer Products.'
WHERE clause_version_id = 14 AND clause_name = '52.223-16 Alternate I';

UPDATE Clauses
SET clause_title = 'Pollution Prevention and Right-To-Know Information.' -- 'Pollution Prevention and Right-to-Know Information.'
WHERE clause_version_id = 14 AND clause_name = '52.223-5 Alternate I';

UPDATE Clauses
SET clause_title = 'Pollution Prevention and Right-To-Know Information.' -- 'Pollution Prevention and Right-to-Know Information.'
WHERE clause_version_id = 14 AND clause_name = '52.223-5 Alternate II';

UPDATE Clauses
SET clause_title = 'Notice of Buy American Requirement -- Construction Materials.' -- 'Notice of Buy American Requirement-Construction Materials.'
WHERE clause_version_id = 14 AND clause_name = '52.225-10 Alternate I';

UPDATE Clauses
SET clause_title = 'Buy American -- Construction Materials Under Trade Agreements.' -- 'Buy American-Construction Materials Under Trade Agreements.'
WHERE clause_version_id = 14 AND clause_name = '52.225-11 Alternate I';

UPDATE Clauses
SET clause_title = 'Notice of Buy American Requirement -- Construction Materials Under Trade Agreements.' -- 'Notice of Buy American Requirement-Construction Materials Under Trade Agreements.'
WHERE clause_version_id = 14 AND clause_name = '52.225-12 Alternate I';

UPDATE Clauses
SET clause_title = 'Notice of Buy American Requirement -- Construction Materials Under Trade Agreements.' -- 'Notice of Buy American Requirement-Construction Materials Under Trade Agreements.'
WHERE clause_version_id = 14 AND clause_name = '52.225-12 Alternate II';

UPDATE Clauses
SET clause_title = 'Buy American -- Free Trade Agreements -- Israeli Trade Act.' -- 'Buy American-Free Trade Agreements-Israeli Trade Act.'
WHERE clause_version_id = 14 AND clause_name = '52.225-3 Alternate I';

UPDATE Clauses
SET clause_title = 'Buy American -- Free Trade Agreements -- Israeli Trade Act.' -- 'Buy American-Free Trade Agreements-Israeli Trade Act.'
WHERE clause_version_id = 14 AND clause_name = '52.225-3 Alternate II';

UPDATE Clauses
SET clause_title = 'Buy American -- Free Trade Agreements -- Israeli Trade Act.' -- 'Buy American-Free Trade Agreements-Israeli Trade Act.'
WHERE clause_version_id = 14 AND clause_name = '52.225-3 Alternate III';

UPDATE Clauses
SET clause_title = 'Buy American -- Free Trade Agreements -- Israeli Trade Act Certificate.' -- 'Buy American-Free Trade Agreement-Israeli Trade Act Certificate.'
WHERE clause_version_id = 14 AND clause_name = '52.225-4 Alternate I';

UPDATE Clauses
SET clause_title = 'Buy American -- Free Trade Agreements -- Israeli Trade Act Certificate.' -- 'Buy American-Free Trade Agreement-Israeli Trade Act Certificate.'
WHERE clause_version_id = 14 AND clause_name = '52.225-4 Alternate II';

UPDATE Clauses
SET clause_title = 'American -- Free Trade Agreements -- Israeli Trade Act Certificate.' -- 'Buy American-Free Trade Agreement-Israeli Trade Act Certificate.'
WHERE clause_version_id = 14 AND clause_name = '52.225-4 Alternate III';

UPDATE Clauses
SET clause_title = 'Patent Rights--Retention by the Contractor (Short Form).' -- 'Patent Rights-Ownership by the Contractor.'
WHERE clause_version_id = 14 AND clause_name = '52.227-11 Alternate I';

UPDATE Clauses
SET clause_title = 'Patent Rights--Retention by the Contractor (Short Form).' -- 'Patent Rights-Ownership by the Contractor.'
WHERE clause_version_id = 14 AND clause_name = '52.227-11 Alternate II';

UPDATE Clauses
SET clause_title = 'Patent Rights--Retention by the Contractor (Short Form).' -- 'Patent Rights-Ownership by the Contractor.'
WHERE clause_version_id = 14 AND clause_name = '52.227-11 Alternate III';

UPDATE Clauses
SET clause_title = 'Patent Rights--Retention by the Contractor (Short Form).' -- 'Patent Rights-Ownership by the Contractor.'
WHERE clause_version_id = 14 AND clause_name = '52.227-11 Alternate IV';

UPDATE Clauses
SET clause_title = 'Patent Rights--Retention by the Contractor (Short Form).' -- 'Patent Rights-Ownership by the Contractor.'
WHERE clause_version_id = 14 AND clause_name = '52.227-11 Alternate V';

UPDATE Clauses
SET clause_title = 'Patent Rights- Acquisition by the Government.' -- 'Patent Rights-Ownership by the Government.'
WHERE clause_version_id = 14 AND clause_name = '52.227-13 Alternate I';

UPDATE Clauses
SET clause_title = 'Patent Rights- Acquisition by the Government.' -- 'Patent Rights-Ownership by the Government.'
WHERE clause_version_id = 14 AND clause_name = '52.227-13 Alternate II';

UPDATE Clauses
SET clause_title = 'Patent Indemnity- Construction Contracts.' -- 'Patent Indemnity-Construction Contracts.'
WHERE clause_version_id = 14 AND clause_name = '52.227-4 Alternate I';

UPDATE Clauses
SET clause_title = 'Prompt Payment.' -- 'Prompt payment.'
WHERE clause_version_id = 14 AND clause_name = '52.232-25 Alternate I';

UPDATE Clauses
SET clause_title = 'Invitation to Propose Performance-Based Payments.' -- 'Invitation To Propose Performance-Based Payments.'
WHERE clause_version_id = 14 AND clause_name = '52.232-28 Alternate I';

UPDATE Clauses
SET clause_title = 'Changes--Fixed Price.' -- 'Changes-Fixed-Price.'
WHERE clause_version_id = 14 AND clause_name = '52.243-1 Alternate I';

UPDATE Clauses
SET clause_title = 'Changes--Fixed Price.' -- 'Changes-Fixed-Price.'
WHERE clause_version_id = 14 AND clause_name = '52.243-1 Alternate II';

UPDATE Clauses
SET clause_title = 'Changes--Fixed Price.' -- 'Changes-Fixed-Price.'
WHERE clause_version_id = 14 AND clause_name = '52.243-1 Alternate III';

UPDATE Clauses
SET clause_title = 'Changes--Fixed Price.' -- 'Changes-Fixed-Price.'
WHERE clause_version_id = 14 AND clause_name = '52.243-1 Alternate IV';

UPDATE Clauses
SET clause_title = 'Changes--Fixed Price.' -- 'Changes-Fixed-Price.'
WHERE clause_version_id = 14 AND clause_name = '52.243-1 Alternate V';

UPDATE Clauses
SET clause_title = 'Changes-Cost Reimbursement.' -- 'Changes-Cost-Reimbursement.'
WHERE clause_version_id = 14 AND clause_name = '52.243-2 Alternate I';

UPDATE Clauses
SET clause_title = 'Changes-Cost Reimbursement.' -- 'Changes-Cost-Reimbursement.'
WHERE clause_version_id = 14 AND clause_name = '52.243-2 Alternate II';

UPDATE Clauses
SET clause_title = 'Changes-Cost Reimbursement.' -- 'Changes-Cost-Reimbursement.'
WHERE clause_version_id = 14 AND clause_name = '52.243-2 Alternate III';

UPDATE Clauses
SET clause_title = 'Changes-Cost Reimbursement.' -- 'Changes-Cost-Reimbursement.'
WHERE clause_version_id = 14 AND clause_name = '52.243-2 Alternate V';

UPDATE Clauses
SET clause_title = 'Warranty of Supplies of a Non- Complex Nature.' -- 'Warranty of Supplies of a Noncomplex Nature.'
WHERE clause_version_id = 14 AND clause_name = '52.246-17 Alternate II';

UPDATE Clauses
SET clause_title = 'Warranty of Supplies of a Non- Complex Nature.' -- 'Warranty of Supplies of a Noncomplex Nature.'
WHERE clause_version_id = 14 AND clause_name = '52.246-17 Alternate III';

UPDATE Clauses
SET clause_title = 'Warranty of Supplies of a Non- Complex Nature.' -- 'Warranty of Supplies of a Noncomplex Nature.'
WHERE clause_version_id = 14 AND clause_name = '52.246-17 Alternate IV';

UPDATE Clauses
SET clause_title = 'Warranty of Supplies of a Non- Complex Nature.' -- 'Warranty of Supplies of a Noncomplex Nature.'
WHERE clause_version_id = 14 AND clause_name = '52.246-17 Alternate V';

UPDATE Clauses
SET clause_conditions = '(A) DOCUMENT TYPE IS: SOLICITATION\n(B) DOCUMENT TYPE IS: AWARD\n(C) WARRANTY DESCRIPTION IS: SUPPLIES ARE "COMPLEX" ITEMS\n(D) CONTRACT TYPE IS: FIXED PRICE\n(E) SUPPLIES OR SERVICES IS: SUPPLIES\n(F) SERVICES IS: RESEARCH AND DEVELOPMENT\n(G) WARRANTY CONDITIONS IS: USE OF A WARRANTY CLAUSE IS APPROVED\n(H) WARRANTY RECOVERY AND TRANSPORTATION IS: RECOVERY OF THE WARRANTED ITEMS WILL INVOLVE CONSIDERABLE GOVERNMENT EXPENSE FOR DISASSEMBLY AND/OR REASSEMBLY OF LARGER ITEMS\n(I) PROCEDURES IS: NOT COMMERCIAL PROCEDURES (FAR PART 12)' -- '(A) DOCUMENT TYPE IS: SOLICITATION\n(B) DOCUMENT TYPE IS: AWARD\n(C) WARRANTY DESCRIPTION IS: SUPPLIES ARE "COMPLEX" ITEMS\n(D) CONTRACT TYPE IS: FIXED PRICE\n(E) SUPPLIES OR SERVICES IS: SUPPLIES\n(F) SERVICES IS: RESEARCH AND DEVELOPMENT\n(G) WARRANTY CONDITIONS IS: USE OF A WARRANTY CLAUSE IS APPROVED\n(H) WARRANTY RECOVERY AND TRANSPORTATION IS: RECOVERY OF THE WARRANTED ITEMS WILL INVOLVE CONSIDERABLE GOVERNMENT EXPENSE FOR DISASSEMBLY AND/OR REASSEMBLY OF LARGER ITEMS\n(I) PROCEDURES IS: NOT COMMERICAL PROCEDURES (FAR PART 12)'
WHERE clause_version_id = 14 AND clause_name = '52.246-18 Alternate IV';

UPDATE Clauses
SET clause_title = 'Warranty of Systems and Equipment Under Performance Specifications or Design Criteria.' -- 'Warranty of Systems and Equipment under Performance Specifications or Design Criteria.'
WHERE clause_version_id = 14 AND clause_name = '52.246-19 Alternate I';

UPDATE Clauses
SET clause_title = 'Warranty of Systems and Equipment Under Performance Specifications or Design Criteria.' -- 'Warranty of Systems and Equipment under Performance Specifications or Design Criteria.'
WHERE clause_version_id = 14 AND clause_name = '52.246-19 Alternate II';

UPDATE Clauses
SET clause_title = 'Warranty of Systems and Equipment Under Performance Specifications or Design Criteria.' -- 'Warranty of Systems and Equipment under Performance Specifications or Design Criteria.'
WHERE clause_version_id = 14 AND clause_name = '52.246-19 Alternate III';

UPDATE Clauses
SET clause_title = 'Inspection of Supplies--Fixed-Price.' -- 'Inspection of Supplies-Fixed-Price.'
WHERE clause_version_id = 14 AND clause_name = '52.246-2 Alternate I';

UPDATE Clauses
SET clause_title = 'Inspection of Supplies--Fixed-Price.' -- 'Inspection of Supplies-Fixed-Price.'
WHERE clause_version_id = 14 AND clause_name = '52.246-2 Alternate II';

UPDATE Clauses
SET clause_title = 'Limitation of Liability--High Value Items.' -- 'Limitation of Liability-High-Value Items.'
WHERE clause_version_id = 14 AND clause_name = '52.246-24 Alternate I';

UPDATE Clauses
SET clause_title = 'Inspection--Time-And-Material and Labor-Hour.' -- 'Inspection-Time-and-Material and Labor-Hour.'
WHERE clause_version_id = 14 AND clause_name = '52.246-6 Alternate I';

UPDATE Clauses
SET clause_title = 'Inspection of Research and Development--Cost Reimbursement.' -- 'Inspection of Research and Development-Cost-Reimbursement.'
WHERE clause_version_id = 14 AND clause_name = '52.246-8 Alternate I';

UPDATE Clauses
SET clause_title = 'Capability to Perform a Contract for the Relocation of a Federal Office.' -- 'Capability To Perform a Contract for the Relocation of a Federal Office.'
WHERE clause_version_id = 14 AND clause_name = '52.247-3 Alternate I';

UPDATE Clauses
SET clause_title = 'Value Engineering--Construction.' -- 'Value Engineering-Construction.'
WHERE clause_version_id = 14 AND clause_name = '52.248-3 Alternate I';

UPDATE Clauses
SET clause_title = 'Termination (Cost- Reimbursement).' -- 'Termination (Cost-Reimbursement).'
WHERE clause_version_id = 14 AND clause_name = '52.249-6 Alternate I';

UPDATE Clauses
SET clause_title = 'Termination (Cost- Reimbursement).' -- 'Termination (Cost-Reimbursement).'
WHERE clause_version_id = 14 AND clause_name = '52.249-6 Alternate II';

UPDATE Clauses
SET clause_title = 'Termination (Cost- Reimbursement).' -- 'Termination (Cost-Reimbursement).'
WHERE clause_version_id = 14 AND clause_name = '52.249-6 Alternate III';

UPDATE Clauses
SET clause_title = 'Termination (Cost- Reimbursement).' -- 'Termination (Cost-Reimbursement).'
WHERE clause_version_id = 14 AND clause_name = '52.249-6 Alternate IV';

UPDATE Clauses
SET clause_title = 'Termination (Cost- Reimbursement).' -- 'Termination (Cost-Reimbursement).'
WHERE clause_version_id = 14 AND clause_name = '52.249-6 Alternate V';

UPDATE Clauses
SET clause_title = 'Default (Fixed- Price Supply and Service).' -- 'Default (Fixed-Price Supply and Service).'
WHERE clause_version_id = 14 AND clause_name = '52.249-8 Alternate I';

UPDATE Clauses
SET clause_title = 'Safety Act Block Designation/Certification.' -- 'SAFETY Act Block Designation/Certification.'
WHERE clause_version_id = 14 AND clause_name = '52.250-3 Alternate I';

UPDATE Clauses
SET clause_title = 'Safety Act Block Designation/Certification.' -- 'SAFETY Act Block Designation/Certification.'
, clause_data = '<p>Alternate II (FEB 2009). As prescribed in 50.206(b)(3), substitute the following paragraph (f):</p><p>(f)(1) Offerors are authorized to submit offers presuming that SAFETY Act designation (or SAFETY Act certification, if a block certification exists) will be obtained before or after award.</p><p>(2) An offeror is eligible for award only if the offeror-</p><p>(i) Files a SAFETY Act designation (or SAFETY Act certification) application, limited to the scope of the applicable block designation (or block certification), within 15 days after submission of the proposal;</p><p>(ii) Pursues its SAFETY Act designation (or SAFETY Act certification) application in good faith; and</p><p>(iii) Agrees to obtain the amount of insurance DHS requires for issuing the offeror''s SAFETY Act designation (or SAFETY Act certification).</p><p>(3) If DHS has not issued a SAFETY Act designation (or SAFETY Act certification) to the successful offeror before contract award, the contracting officer will include the clause at 52.250-5 in the resulting contract.</p>'
--               '<p>Alternate II (FEB 2009). As prescribed in 50.206(b)(3), substitute the following paragraph (f):</p><p>(f)(1) Offerors are authorized to submit offers presuming that SAFETY Act designation (or SAFETY Act certification, if a block certification exists) will be obtained before or after award.</p><p>(2) An offeror is eligible for award only if the offeror-</p><p>{{paragraph_f2i_52.250-3_AltII}}</p><p>(ii) Pursues its SAFETY Act designation (or SAFETY Act certification) application in good faith; and</p><p>(iii) Agrees to obtain the amount of insurance DHS requires for issuing the offeror''s SAFETY Act designation (or SAFETY Act certification).</p><p>(3) If DHS has not issued a SAFETY Act designation (or SAFETY Act certification) to the successful offeror before contract award, the contracting officer will include the clause at 52.250-5 in the resulting contract.</p>'
WHERE clause_version_id = 14 AND clause_name = '52.250-3 Alternate II';

DELETE FROM Document_Fill_Ins WHERE clause_fill_in_id IN (SELECT clause_fill_in_id FROM Clause_Fill_Ins CF INNER JOIN Clauses C  ON C.clause_id= CF.clause_id
WHERE clause_name = '52.250-3 Alternate II' AND clause_version_id = 14 AND fill_in_code='paragraph_f2i_52.250-3_AltII');
DELETE FROM Clause_Fill_Ins WHERE clause_id IN (SELECT clause_id FROM Clauses 
WHERE clause_name = '52.250-3 Alternate II' AND clause_version_id = 14)  AND fill_in_code='paragraph_f2i_52.250-3_AltII';

UPDATE Clauses
SET clause_title = 'Safety Act Pre-Qualification Designation Notice.' -- 'SAFETY Act Pre-qualification Designation Notice.'
WHERE clause_version_id = 14 AND clause_name = '52.250-4 Alternate I';

UPDATE Clauses
SET clause_title = 'Safety Act Pre-Qualification Designation Notice.' -- 'SAFETY Act Pre-qualification Designation Notice.'
, clause_data = '<p>Alternate II (FEB 2009). As prescribed in 50.206(c)(3), substitute the following paragraph (f):</p><p>(f)(1) Offerors are authorized to submit proposals presuming SAFETY Act designation before or after award.</p><p>(2) An offeror is eligible for award only if the offeror-</p><p>(i) Files a SAFETY Act designation application, limited to the scope of the applicable prequalification designation notice, within 15 days after submission of the proposal;</p><p>(ii) Pursues its SAFETY Act designation application in good faith; and</p><p>(iii) Agrees to obtain the amount of insurance DHS requires for issuing the offeror''s SAFETY Act designation.</p><p>(3) If DHS has not issued a SAFETY Act designation to the successful offeror before contract award, the contracting officer will include the clause at 52.250-5 in the resulting contract.</p>'
--               '<p>Alternate II (FEB 2009). As prescribed in 50.206(c)(3), substitute the following paragraph (f):</p><p>(f)(1) Offerors are authorized to submit proposals presuming SAFETY Act designation before or after award.</p><p>(2) An offeror is eligible for award only if the offeror-</p><p>{{paragraph_f2i_52.250-4_AltII}}</p><p>(ii) Pursues its SAFETY Act designation application in good faith; and</p><p>(iii) Agrees to obtain the amount of insurance DHS requires for issuing the offeror''s SAFETY Act designation.</p><p>(3) If DHS has not issued a SAFETY Act designation to the successful offeror before contract award, the contracting officer will include the clause at 52.250-5 in the resulting contract.</p>'
WHERE clause_version_id = 14 AND clause_name = '52.250-4 Alternate II';

DELETE FROM Document_Fill_Ins WHERE clause_fill_in_id IN (SELECT clause_fill_in_id FROM Clause_Fill_Ins CF INNER JOIN Clauses C  ON C.clause_id= CF.clause_id
WHERE clause_name = '52.250-4 Alternate II' AND clause_version_id = 14 AND fill_in_code='paragraph_f2i_52.250-4_AltII');
DELETE FROM Clause_Fill_Ins WHERE clause_id IN (SELECT clause_id FROM Clauses 
WHERE clause_name = '52.250-4 Alternate II' AND clause_version_id = 14)  AND fill_in_code='paragraph_f2i_52.250-4_AltII';


DELETE FROM Document_Fill_Ins WHERE Clause_Fill_In_Id
IN (SELECT Clause_Fill_In_Id FROM Clause_Fill_Ins WHERE clause_id = 160040); -- 52.209-2
DELETE FROM Clause_Fill_Ins WHERE clause_id = 160040; -- 52.209-2
DELETE FROM Document_Clauses WHERE clause_id = 160040; -- 52.209-2
DELETE FROM Clauses WHERE clause_id = 160040; -- 52.209-2
/*
*** End of SQL Scripts at Mon Jan 11 01:32:57 EST 2016 ***
*/
