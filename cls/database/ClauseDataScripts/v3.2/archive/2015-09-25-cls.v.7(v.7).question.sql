/*
Clause Parser Run at Fri Sep 25 04:37:05 EDT 2015
(Without Correction Information)
*/
/* ===============================================
 * Questions
   =============================================== */


UPDATE Question_Choices
SET prompt_after_selection = 'If the acquisition includes both commercial and non-commercial line items, the user is responsible for identifying in their Contract Writing System which clauses apply to the commercial line items and which clauses apply to the non-commercial line items.'
WHERE question_id = 34747 AND choice_text = 'COMMERCIAL PROCEDURES (FAR PART 12)'; -- [PROCEDURES] - NULL

/*
*** End of SQL Scripts at Fri Sep 25 04:37:06 EDT 2015 ***
*/
