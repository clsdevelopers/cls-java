/*
Clause Parser Run at Fri Jul 24 15:17:43 EDT 2015
(Without Correction Information)
*/

/* ===============================================
 * Clause
   =============================================== */

UPDATE Clauses
SET clause_data = '<p>Alterate I (AUG 1984). The license hereby granted shall remain in full force and effect for the full term of each of the patents referred to in the ''License Grant'' clause of this contract and any and all patents hereafter issued on applications for patent referred to in such ''License Grant'' clause.</p><p>Alterate II (OCT 2001). The license hereby granted shall terminate on the ____ day of ____ ,____; Provided, however, that said termination shall be without prejudice to the completion of any contract entered into by the Government prior to said date of termination or to the use or disposition thereafter of any articles or materials manufactured by or for the Government under this license.</p>'
--               '<p>Alterate I (AUG 1984). The license hereby granted shall remain in full force and effect for the full term of each of the patents referred to in the ''License Grant'' clause of this contract and any and all patents hereafter issued on applications for patent referred to in such ''License Grant'' clause.</p><p>Alterate II (OCT 2001). The license hereby granted shall terminate on the ____ day of ____ ,____; Provided, however, that said termination shall be without prejudice to the completion of any contract entered into by the Government prior to said date of termination or to the use or disposition thereafter of any articles or materials manufactured by or for the Government under this license.</p><p>[56 FR 36479, July 31, 1991, as amended at 66 FR 49861, Oct. 1, 2001]</p>'
WHERE clause_version_id = 3 AND clause_name = '252.227-7005 Alternate I';

UPDATE Clauses
SET clause_data = '<p>Alterate II (OCT 2001). The license hereby granted shall terminate on the {{textbox_252.227-7005[0]}} day of {{textbox_252.227-7005[1]}}, {{textbox_252.227-7005[2]}}; Provided, however, that said termination shall be without prejudice to the completion of any contract entered into by the Government prior to said date of termination or to the use or disposition thereafter of any articles or materials manufactured by or for the Government under this license.</p>'
--               '<p>Alterate II (OCT 2001). The license hereby granted shall terminate on the {{textbox_252.227-7005[0]}} day of {{textbox_252.227-7005[1]}}, {{textbox_252.227-7005[2]}}; Provided, however, that said termination shall be without prejudice to the completion of any contract entered into by the Government prior to said date of termination or to the use or disposition thereafter of any articles or materials manufactured by or for the Government under this license.</p><p>[56 FR 36479, July 31, 1991, as amended at 66 FR 49861, Oct. 1, 2001]</p>'
WHERE clause_version_id = 3 AND clause_name = '252.227-7005 Alternate II';

UPDATE Clauses
SET clause_data = '<p>Alternate III (APR 1984). If the delivery schedule is to be based on the actual date the contractor receives a written notice of award, the contracting officer may delete paragraph (b) of the basic clause. The time may be expressed by substituting within days after the date of receipt of a written notice of award as the heading for the third column of paragraph (a) of the basic clause.</p>'
--               '<p>Alternate III (APR 1984). If the delivery schedule is to be based on the actual date the contractor receives a written notice of award, the contracting officer may delete paragraph (b) of the basic clause. The time may be expressed by substituting within days after the date of receipt of a written notice of award as the heading for the third column of paragraph (a) of the basic clause.</p><p>[48 FR 42478, Sept. 19, 1983, as amended at 56 FR 41732, Aug. 22, 1991; 60 FR 34739, July 3, 1995. Redesignated and amended at 60 FR 48251, 48256, Sept. 18, 1995; 62 FR 40238, July 25, 1997]</p>'
WHERE clause_version_id = 3 AND clause_name = '52.211-8 Alternate III';

UPDATE Clauses
SET clause_data = '<p>Alternate III (APR 1984). If the delivery schedule is to be based on the actual date the contractor receives a written notice of award, the contracting officer may delete paragraph (b) of the basic clause. The time may be expressed by substituting within days after the date of receipt of a written notice of award as the heading of the third column of paragraph (a) of the basic clause.</p>'
--               '<p>Alternate III (APR 1984). If the delivery schedule is to be based on the actual date the contractor receives a written notice of award, the contracting officer may delete paragraph (b) of the basic clause. The time may be expressed by substituting within days after the date of receipt of a written notice of award as the heading of the third column of paragraph (a) of the basic clause.</p><p>[48 FR 42478, Sept. 19, 1983, as amended at 56 FR 41732, Aug. 22, 1991; 60 FR 34739, July 3, 1995. Redesignated and amended at 60 FR 48251, 48256, Sept. 18, 1995; 62 FR 40238, July 25, 1997]</p>'
WHERE clause_version_id = 3 AND clause_name = '52.211-9 Alternate III';

UPDATE Clauses
SET clause_data = '<p>Alternate I (APR 1984). In a contract for research and development with an educational institution or a nonprofit organization, for which the contracting officer has determined that withholding of a portion of allowable costs is not required, delete paragraph (b) of the basic clause.</p>'
--               '<p>Alternate I (APR 1984). In a contract for research and development with an educational institution or a nonprofit organization, for which the contracting officer has determined that withholding of a portion of allowable costs is not required, delete paragraph (b) of the basic clause.</p><p>[48 FR 42478, Sept. 19, 1983, as amended at 72 FR 27389, May 15, 2007]</p>'
WHERE clause_version_id = 3 AND clause_name = '52.216-11 Alternate I';

UPDATE Clauses
SET clause_data = '<p>Alternate I (APR 1984). In a contract for research and development with an educational institution, for which the contracting officer has determined that withholding of a portion of allowable cost is not required, delete paragraph (b) of the basic clause.</p>'
--               '<p>Alternate I (APR 1984). In a contract for research and development with an educational institution, for which the contracting officer has determined that withholding of a portion of allowable cost is not required, delete paragraph (b) of the basic clause.</p><p>[48 FR 42478, Sept. 19, 1983, as amended at 72 FR 27389, May 15, 2007]</p>'
WHERE clause_version_id = 3 AND clause_name = '52.216-12 Alternate I';

UPDATE Clauses
SET clause_data = '<p>Alternate II (DEC 1996). When the acquisition is for a product in a class for which the Small Business Administration has determined that there are no small business manufacturers or processors in the Federal market in accordance with 19.502-2(c), delete subparagraph (d)(1).</p>'
--               '<p>Alternate II (DEC 1996). When the acquisition is for a product in a class for which the Small Business Administration has determined that there are no small business manufacturers or processors in the Federal market in accordance with 19.502-2(c), delete subparagraph (d)(1).</p><p>[54 FR 46009, Oct. 31, 1989; 54 FR 48105, Nov. 21, 1989, as amended at 55 FR 3889, Feb. 5, 1990; 55 FR 25532, June 21, 1990; 55 FR 38518, Sept. 18, 1990; 60 FR 48267, Sept. 18, 1995; 61 FR 39209, July 26, 1996; 61 FR 67422, Dec. 20, 1996; 62 FR 238, Jan. 2, 1997; 62 FR 12720, Mar. 17, 1997; 64 FR 32745, June 17, 1999; 64 FR 51850, Sept. 24, 1999; 68 FR 28085, May 22, 2003; 70 FR 18959, Apr. 11, 2005]</p>'
WHERE clause_version_id = 3 AND clause_name = '52.219-18 Alternate II';

UPDATE Clauses
SET clause_data = '<p>Alterate I (JUN 1985). As prescribed in 33.106(b), substitute in paragraph (a)(2) the words ''the Termination clause of this contract'' for the words ''the Default, or the Termination for Convenience of the Government clause of this contract.'' In paragraph (b) substitute the words ''an equitable adjustment in the delivery schedule, the estimated cost, the fee, or a combination thereof, and in any other terms of the contract that may be affected'' for the words ''an equitable adjustment in the delivery schedule or contract price, or both.''</p>'
--               '<p>Alterate I (JUN 1985). As prescribed in 33.106(b), substitute in paragraph (a)(2) the words ''the Termination clause of this contract'' for the words ''the Default, or the Termination for Convenience of the Government clause of this contract.'' In paragraph (b) substitute the words ''an equitable adjustment in the delivery schedule, the estimated cost, the fee, or a combination thereof, and in any other terms of the contract that may be affected'' for the words ''an equitable adjustment in the delivery schedule or contract price, or both.''</p><p>[50 FR 25681, June 20, 1985, as amended at 54 FR 29284, July 11, 1989; 60 FR 48231, 48276, Sept. 18, 1995; 61 FR 41472, Aug. 8, 1996]</p>'
WHERE clause_version_id = 3 AND clause_name = '52.233-3 Alternate I';

UPDATE Clauses
SET clause_data = '<p>Alterate I (APR 1984). If this clause is inserted in a cost-reimbursement contract, substitute in paragraph (a)(2) the words ''the Termination clause of this contract'' for the words ''the Default, or the Termination for Convenience of the Government clause of this contract.'' In paragraph (b) substitute the words ''an equitable adjustment in the delivery schedule, the estimated cost, the fee, or a combination thereof, and in any other terms of the contract that may be affected for the words an equitable adjustment in the delivery schedule or contract price, or both.''</p>'
--               '<p>Alterate I (APR 1984). If this clause is inserted in a cost-reimbursement contract, substitute in paragraph (a)(2) the words ''the Termination clause of this contract'' for the words ''the Default, or the Termination for Convenience of the Government clause of this contract.'' In paragraph (b) substitute the words ''an equitable adjustment in the delivery schedule, the estimated cost, the fee, or a combination thereof, and in any other terms of the contract that may be affected for the words an equitable adjustment in the delivery schedule or contract price, or both.''</p><p>[48 FR 42478, Sept. 19, 1983, as amended at 50 FR 2272, Jan. 15, 1985; 54 FR 29283, July 11, 1989. Redesignated and amended at 60 FR 48251, 48256, Sept. 18, 1995]</p>'
WHERE clause_version_id = 3 AND clause_name = '52.242-15 Alternate I';

UPDATE Clauses
SET clause_data = '<p>Alterate I (APR 1984). If a Federal office move is intrastate and the contracting officer determines that it is in the Government''s interest not to apply the requirements for holding or obtaining State authority to operate within the State, and to maintain a facility within the State or Commercial zone, delete paragraph (b) of the basic clause and redesignate the remaining paragraphs (b) and (c). In the 6th line of the new paragraph (b), delete the words paragraphs (a) and (b) above and replace them with paragraph (a) above.</p>'
--               '<p>Alterate I (APR 1984). If a Federal office move is intrastate and the contracting officer determines that it is in the Government''s interest not to apply the requirements for holding or obtaining State authority to operate within the State, and to maintain a facility within the State or Commercial zone, delete paragraph (b) of the basic clause and redesignate the remaining paragraphs (b) and (c). In the 6th line of the new paragraph (b), delete the words paragraphs (a) and (b) above and replace them with paragraph (a) above.</p><p>[48 FR 42478, Sept. 19, 1983, as amended at 71 FR 207, Jan. 3, 2006]</p>'
WHERE clause_version_id = 3 AND clause_name = '52.247-3 Alternate I';

UPDATE Clauses
SET clause_data = '<p>Alternate III (APR 1984). When the head of the contracting activity determines that the cost of calculating and tracking collateral savings will exceed the benefits to be derived in a contract calling for a value engineering incentive, delete paragraph (j) from the basic clause and redesignate the remaining paragraphs accordingly.</p>'
--               '<p>Alternate III (APR 1984). When the head of the contracting activity determines that the cost of calculating and tracking collateral savings will exceed the benefits to be derived in a contract calling for a value engineering incentive, delete paragraph (j) from the basic clause and redesignate the remaining paragraphs accordingly.</p><p>[48 FR 42478, Sept. 19, 1983, as amended at 54 FR 5059, Jan. 31, 1989; 64 FR 51848, Sept. 24, 1999; 64 FR 72449, Dec. 27, 1999; 75 FR 53135, Aug. 30, 2010]</p>'
WHERE clause_version_id = 3 AND clause_name = '52.248-1 Alternate III';

UPDATE Clauses
SET clause_data = '<p>Alterate I (APR 1984). When the head of the contracting activity determines that the cost of calculating and tracking collateral savings will exceed the benefits to be derived in a construction contract, delete paragraph (g) from the basic clause and redesignate the remaining paragraphs accordingly.</p>'
--               '<p>Alterate I (APR 1984). When the head of the contracting activity determines that the cost of calculating and tracking collateral savings will exceed the benefits to be derived in a construction contract, delete paragraph (g) from the basic clause and redesignate the remaining paragraphs accordingly.</p><p>[48 FR 42478, Sept. 19, 1983, as amended at 54 FR 5059, Jan. 31, 1989; 64 FR 72449, Dec. 27, 1999; 71 FR 57369, Sept. 28, 2006;75 FR 53135, Aug. 30, 2010]</p>'
WHERE clause_version_id = 3 AND clause_name = '52.248-3 Alternate I';

UPDATE Clauses
SET clause_data = '<p>Alterate I (SEP 1996). If the contract is with an agency of the U.S. Government or with State, local, or foreign governments or their agencies, and if the contracting officer determines that the requirement to pay interest on excess partial payments is inappropriate, delete subparagraph (m)(2) from the basic clause.</p>'
--               '<p>Alterate I (SEP 1996). If the contract is with an agency of the U.S. Government or with State, local, or foreign governments or their agencies, and if the contracting officer determines that the requirement to pay interest on excess partial payments is inappropriate, delete subparagraph (m)(2) from the basic clause.</p><p>[48 FR 42478, Sept. 19, 1983, as amended at 61 FR 39222, July 26, 1996; 69 FR 17750, Apr. 5, 2004; 77 FR 12947, Mar. 2, 2012]</p>'
WHERE clause_version_id = 3 AND clause_name = '52.249-3 Alternate I';

/*
*** End of SQL Scripts at Fri Jul 24 15:17:46 EDT 2015 ***
*/
