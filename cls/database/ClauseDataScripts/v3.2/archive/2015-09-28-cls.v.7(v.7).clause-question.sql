/*
Clause Parser Run at Mon Sep 28 10:41:03 EDT 2015
(Without Correction Information)
*/
/* ===============================================
 * Prescriptions
   =============================================== */
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '10.003') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '11.107(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '11.107(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '11.204(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '11.204(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '11.204(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '11.204(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '11.208(f)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '11.304') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '11.404(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '11.404(a)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '11.404(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '11.503(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '11.503(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '11.503(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '11.604(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '11.604(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '11.703(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '11.703(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '11.703(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '12.301(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '12.301(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '12.301(b)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '12.301(b)(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '12.301(b)(4)(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '12.301(b)(4)(ii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '12.301(c)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '13.302-5(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '13.302-5(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '13.302-5(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '13.302-5(d)(3)(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '13.404') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(c)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(c)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(c)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(f)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(g)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(g)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(h)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(j)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(l)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(m)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(o)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(o)(2)(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(o)(2)(ii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(p)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(p)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(q)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(r)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(s)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(t)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(v)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(w)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(x)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-7(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-7(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-7(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-7(c)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-7(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.209(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.209(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.209(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.209(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.209(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.209(b)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.209(b)(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.209(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.209(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.209(f)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.209(h)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.408(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.408(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.408(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.408(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.408(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.408(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.408(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.408(f)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.408(f)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.408(g)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.408(h)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.408(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.408(j)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.408(k)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.408(l)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.408(m)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.408(n)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.408(n)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.408(n)(2)(iii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.105') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.203-4(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.203-4(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.203-4(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.205-4') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.206-4') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.307(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.307(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.307(a)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.307(a)(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.307(a)(5)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.307(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.307(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.307(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.307(e)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.307(e)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.307(f)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.307(f)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.307(g)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.406') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.406(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.406(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.506') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.506(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.506(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.506(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.506(d)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.506(d)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.506(d)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.506(d)(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.506(d)(5)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.506(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.506(f)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.506(g)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.601(f)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.601(f)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.601(f)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.603-4(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.603-4(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.603-4(b)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.603-4(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '17.109') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '17.109(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '17.208(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '17.208(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '17.208(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '17.208(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '17.208(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '17.208(f)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '17.208(g)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.1309') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.1309(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.1309(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.1309(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.1309(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.1407') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.1506(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.1506(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.309(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.309(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.309(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.309(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.309(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.508(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.508(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.508(d)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.508(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.508(f)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.708') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.708(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.708(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.708(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.708(b)(1)(iii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.708(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.708(c)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.811-3(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.811-3(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.811-3(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.811-3(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.811-3(d)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.811-3(d)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.811-3(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2.201') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '201.602-70') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2012-O0004') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2012-O0007') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2013-O0006') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2013-O0010') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2013-O0014') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2013-O0014)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2013-O0016') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2013-O0017') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2013-O0019') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2014-00014') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2014-O0004') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2014-O0005') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2014-O0010') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2014-O0016') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2014-O0018') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2014-O0020') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2014-OO0009') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2015-O0001') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2015-O0002') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2015-O0003') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2015-O0007') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2015-O0009') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2015-O0010') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2015-O0012') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2015-O0013') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2015-OO005') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '203.1004(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '203.1004(b)(2)(ii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '203.171-4') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '203.171-4(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '203.570-3') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '203.97') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '204.1105') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '204.1202') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '204.404-70(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '204.404-70(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '204.404-70(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '204.470-3') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '204.7104-1(b)(3)(iv)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '204.7109(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '204.7109(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '204.7303') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '204.7403(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '204.7403(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '204.7403(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '205.47') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '206.302-3-70') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '208.7305(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '209.104-70') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '209.270-5') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '209.409') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '209.470-4(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '209.470-4(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '209.570-4(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '209.570-4(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '209.571-8(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '209.571-8(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '211.002-70') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '211.204(c)(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '211.204(c)(ii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '211.272') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '211.273-4') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '211.274-6(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '211.274-6(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '211.274-6(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '211.275-3') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '212.7103') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '213.106-2-70') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '215.370-3(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '215.370-3(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '215.371-6') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '215.408') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '215.408(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '215.408(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '215.408(3)(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '215.408(3)(ii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '215.408(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '215.408(4)(II)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '215.408(5)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '216.203-4-70(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '216.203-4-70(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '216.203-4-70(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '216.203-4-70(c)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '216.203-4-70(c)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '216.307(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '216.406') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '216.406(e)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '216.506') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '216.506(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '216.506(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '216.506(d)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '216.506(d)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '216.601(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '217.208-70(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '217.208-70(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '217.208-70(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '217.208-70(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '217.7005') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '217.7104(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '217.7303') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '217.7406') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '217.7406(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '217.7702') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '219.309(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '219.708(b)(1)(A)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '219.708(b)(1)(B)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '219.811-3') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '219.811-3(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '219.811-3(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1006') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1006(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1006(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1006(c)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1006(c)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1006(e)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1006(e)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1006(e)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1006(e)(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1006(f)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.103-5(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.103-5(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1103') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1207') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1310(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1310(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1310(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1310(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1408(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1408(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1505(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1505(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1605') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1705(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1705(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1705(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1803') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1906') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.202') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.305') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.407(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.407(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.407(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.407(f)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.407(g)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.407(h)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.505(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.505(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.505(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.505(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.610') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.810(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.810(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.810(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.810(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.810(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.810(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.810(f)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.810(g)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '222.1771') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '222.7004') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '222.7201(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '222.7201(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '222.7201(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '222.7302') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '222.7405') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '223.303') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '223.370-5') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '223.570-2') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '223.7106') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '223.7106(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '223.7106(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '223.7203') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '223.7306') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225-7306') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225-7799-4') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225-7799-4(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225-7799-4(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225-7799-4(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225-7799-4(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225-7799-4(f)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225-7799-4(h)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(1)(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(1)(ii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(10)(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(10)(i)(A)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(10)(i)(B)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(10)(i)(C)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(10)(i)(D)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(10)(i)(E)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(10)(i)(F)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(2)(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(2)(ii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(2)(iii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(5)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(5)(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(5)(ii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(6)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(6)(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(6)(ii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(7)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(8)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(9)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(9)(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(9)(ii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(9)(iii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(9)(iv)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(9)(v)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(9)(vi)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1103(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1103(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1103(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1103(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.302-6') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.371-5') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.371-5(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.372-2') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7002-3(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7002-3(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7003-5(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7003-5(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7003-5(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7006-4(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7006-4(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7007-3') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7009-5') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7011-3') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7012-3') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7017-4(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7017-4(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7102-4') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7204(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7204(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7204(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7307(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7307(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7402-5(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7403-2') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7503(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7503(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7503(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7503(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7503(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7503(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7503(b)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7503(b)(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7605') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7703-4(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7703-4(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7703-4(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7703-4(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.772-5') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7798-6(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7798-6(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7798-6(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7901-4') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7902-5(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7902-5(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '226.104') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.303(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.303(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7009- 4(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7009-1') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7009-2') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7009-2(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7009-2(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7009-3(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7009-3(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7009-4(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7009-4(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7009-4(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7009-4(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7010') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7012') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7102-4(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7102-4(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7103-17') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7103-3(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7103-6(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7103-6(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7103-6(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7103-6(e)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7103-6(e)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7103-6(e)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7103-8(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7103-8(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7104(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7104(e)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7104(e)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7104(e)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7104(e)(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7104(e)(5)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7104(f)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7104(f)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7105-2(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7105-3') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7106(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7107-1(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7107-1(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7107-1(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7107-3') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7203-3(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7203-6(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7203-6(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7203-6(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7203-6(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7203-6(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7203-6(f)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7205(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '228.170') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '228.370') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '228.370(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '228.370(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '228.370(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '228.370(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '228.370(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '229.170-4') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '229.402-1') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '229.402-70(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '229.402-70(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '229.402-70(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '229.402-70(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '229.402-70(c)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '229.402-70(c)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '229.402-70(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '229.402-70(e)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '229.402-70(e)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '229.402-70(f)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '229.402-70(g)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '229.402-70(h)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '229.402-70(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '229.402-70(j)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '23.1005') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '23.1005(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '23.1005(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '23.1105') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '23.206') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '23.303') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '23.303(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '23.406(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '23.406(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '23.406(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '23.406(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '23.406(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '23.505') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '23.602') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '23.705(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '23.705(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '23.705(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '23.705(c)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '23.705(d)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '23.705(d)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '23.804(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '23.804(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '23.903') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '231.100-70') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '232.1005-70(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '232.1005-70(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '232.1110') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '232.412-70') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '232.412-70(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '232.412-70(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '232.412-70(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '232.502-4-70(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '232.502-4-70(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '232.7004(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '232.7004(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '232.705-70') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '232.7102') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '232.7202') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '232.806(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '232.908') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '233.215-70') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '234.203(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '234.203(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '234.7101(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '234.7101(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '234.7101(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '234.7101(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '234.7101(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '235.070-3') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '235.072') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '235.072(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '235.072(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '235.072(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '235.072(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '235.072(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '235.072©') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '236.570') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '236.570(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '236.570(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '236.570(b)(2)(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '236.570(b)(2)(ii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '236.570(b)(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '236.570(b)(5)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '236.570(b)(6)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '236.570(c)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '236.570(c)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '236.570(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '236.609-70') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '236.609-70(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '237-7003(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '237.171-4') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '237.173-5') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '237.270(d)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '237.270(d)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '237.7003(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '237.7003(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '237.7101') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '237.7101(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '237.7101(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '237.7101(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '237.7101(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '237.7101(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '237.7101(e)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '237.7101(e)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '237.7101(e)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '237.7101(f)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '237.7101(g)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '237.7402') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '237.7603(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '239.7103') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '239.7103(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '239.7103(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '239.7306(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '239.7306(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '239.7411(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '239.7411(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '239.7411(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '239.7411(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '239.7411©') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '239.9903') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '24.104(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '24.104(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '241.501-70(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '241.501-70(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '242.7001') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '242.7204') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '242.7503') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '243.205-70') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '243.205-71') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '244.305-71') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '244.305-71(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '244.305-71(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '244.403') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '245.107(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '245.107(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '245.107(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '245.107(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '245.107(5)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '246.270-4') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '246.370') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '246.371(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '246.710(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '246.710(1)(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '246.710(1)(ii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '246.710(1)(iii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '246.710(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '246.710(3)(i)(A)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '246.710(3)(i)(B)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '246.710(3)(i)(B)(iii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '246.870-3') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.207') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.207(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.270-4') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.270-4(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.270-4(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.270-4(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.270-4(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.270-4(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.270-4(f)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.270-4(g)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.271-3') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.271-3(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.271-3(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.271-3(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.271-3(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.271-3(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.271-3(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.271-3(g)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.271-3(h)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.271-3(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.271-3(j)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.271-3(k)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.271-3(l)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.271-3(m)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.271-3(n)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.305-70') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.574(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.574(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.574(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.574(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.574(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.574(f)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '249.501-70') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '249.7003(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1101(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1101(1)(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1101(1)(ii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1101(6)(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1101(6)(ii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1101(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1101(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1101(b)(1)(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1101(b)(1)(ii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1101(b)(1)(iii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1101(b)(1)(iv)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1101(b)(2)(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1101(b)(2)(ii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1101(b)(2)(iii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1101(b)(2)(iv)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1101(c)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1101(c)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1101(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1101(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1101(f)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1102(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1102(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1102(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1102(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1102(c)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1102(d)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1102(d)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1102(d)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1102(e)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1103') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1103(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1103(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1103(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1103(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.301-4') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.302-6') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '251.107') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '251.205') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '26.104') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '26.206(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '26.206(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '26.206(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '26.304') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '26.404') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.201-2') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.201-2(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.201-2(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.201-2(a)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.201-2(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.201-2(c)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.201-2(c)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.201-2(c)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.201-2(d)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.201-2(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.202-5(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.202-5(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.202-5(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.202-5(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.203-2') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.303(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.303(b)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.303(b)(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.303(b)(5)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.303(b)(6)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.303(b)(7)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.303(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.303(e)(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.303(e)(5)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.409(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.409(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.409(b)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.409(b)(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.409(b)(5)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.409(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.409(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.409(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.409(f)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.409(g)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.409(h)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.409(j)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.409(k)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.409(l)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '28.101-2') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '28.103-4') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '28.106-4(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '28.106-4(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '28.1102-3(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '28.1102-3(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '28.203-6') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '28.204-4') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '28.309(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '28.309(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '28.310') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '28.311-1') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '28.312') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '28.313(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '28.313(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '29.401-1') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '29.401-2') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '29.401-3') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '29.401-3(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '29.401-3(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '29.401-4(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '29.402-1(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '29.402-1(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '29.402-2(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '29.402-2(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '3.1004(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '3.1004(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '3.103-1') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '3.104-9(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '3.104-9(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '3.1106') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '3.202') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '3.301-1') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '3.404') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '3.502-3') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '3.503-2') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '3.808(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '3.808(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '3.907-7') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '3.908-9') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '30.201-3') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '30.201-3(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '30.201-3(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '30.201-4(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '30.201-4(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '30.201-4(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '30.201-4(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '30.201-4(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.009-2') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.1005') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.1005(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.1005(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.111(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.111(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.111(a)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.111(a)(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.111(a)(5)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.111(a)(6)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.111(a)(7)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.111(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.111(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.111(c)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.111(c)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.1110') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.1110(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.1110(a)(1)(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.1110(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.1110(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.1110(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.1110(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.1110(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.1110(g)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.205(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.206') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.206(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.206(g)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.412(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.412(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.412(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.412(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.412(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.412(f)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.502-3(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.502-3(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.502-3(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.502-4') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.502-4(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.502-4(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.502-4(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.502-4(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.611(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.611(a) &(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.611(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.705-1') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.706-1') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.706-1(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.706-1(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.706-2(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.706-2(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.706-3') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.806(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.806(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.806(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.908(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.908(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.908(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.908(c)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '33.106(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '33.106(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '33.215') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '33.215(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '33.215(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '34.104') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '34.203(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '34.203(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '34.203(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.501(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.502') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.503') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.504') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.505') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.506') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.507') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.508') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.509') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.510') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.511') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.512') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.513') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.514') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.515') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.516') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.517') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.518') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.519') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.520') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.521') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.522') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.523') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.609-1(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.609-2(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.609-3') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.609-4') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '37.110(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '37.110(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '37.110(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '37.113-2(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '37.113-2(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '37.115-3') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '37.116-2') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '37.304(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '37.304(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '37.304(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '37.403') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '39.106') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '39.107') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '4.103') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '4.1105(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '4.1105(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '4.1202(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '4.1202(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '4.1303') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '4.1403(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '4.1705(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '4.1705(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '4.1804(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '4.1804(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '4.1804(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '4.303') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '4.404(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '4.607(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '4.607(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '4.905') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '41.501(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '41.501(c)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '41.501(c)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '41.501(c)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '41.501(c)(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '41.501(c)(5)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '41.501(d)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '41.501(d)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '41.501(d)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '41.501(d)(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '41.501(d)(5)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '41.501(d)(6)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '41.501(d)(7)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '42.1107(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '42.1305(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '42.1305(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '42.1305(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '42.1305(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '42.703-2(f)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '42.709-6') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '42.802') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '42.903') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '43.107') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '43.205(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '43.205(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '43.205(a)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '43.205(a)(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '43.205(a)(5)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '43.205(a)(6)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '43.205(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '43.205(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '43.205(b)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '43.205(b)(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '43.205(b)(6)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '43.205(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '43.205(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '43.205(f)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '44.204(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '44.204(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '44.204(a)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '44.204(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '44.204(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '44.403') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '45.107(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '45.107(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '45.107(a)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '45.107(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '45.107(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.301') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.302') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.303') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.304') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.305') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.306') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.307(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.308') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.309') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.311') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.312') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.313') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.314') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.315') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.316') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.710(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.710(a)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.710(a)(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.710(a)(5)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.710(a)(6)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.710(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.710(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.710(b)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.710(b)(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.710(c)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.710(c)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.710(c)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.710(c)(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.710(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.710(e)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.710(e)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.805') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.805(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.805(a)(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.103-2') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.104-4') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-1(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-1(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-1(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-1(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-1(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-1(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-3(d)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-3(e)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-4(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-4(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-4(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-5( c )') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-5(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-5(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-5(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-5(f)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-6(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-6(c)(5)(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-6(c)(5)(ii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-6(c)(6)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-7(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-7(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-7(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-8(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-8(a)(2)(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-8(a)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-8(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-9(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.208-2') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.303- 14(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.303-1(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.303-10(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.303-11(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.303-12(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.303-13(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.303-14(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.303-15(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.303-16(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.303-17(f)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.303-2(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.303-3(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.303-4(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.303-5(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.303-6(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.303-7(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.303-8(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.303-9(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.304-7(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.305-12(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.305-13(a)(3)(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.305-14(b)(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.305-15(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.305-16(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.305-16(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.305-16(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.305-16(d)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.305-17') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.305-2(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.305-3(b)(4)(ii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.305-3(f)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.305-4(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.305-5(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.305-5(c)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.305-6(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.305-6(e)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.305-6(e)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.305-6(e)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.305-6(f)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.305-9(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.405') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.507(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.507(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.507(a)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '48.201') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '48.201(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '48.201(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '48.201(e)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '48.201(f)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '48.202') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '49.502(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '49.502(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '49.502(b)(1)(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '49.502(b)(1)(ii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '49.502(b)(1)(iii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '49.502(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '49.502(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '49.502(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '49.503(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '49.503(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '49.503(a)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '49.503(a)(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '49.503(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '49.504(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '49.504(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '49.504(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '49.504(c)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '49.504(c)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '49.504(c)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '49.505(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '49.505(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '49.506(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '50.104-4') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '50.206(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '50.206(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '50.206(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '50.206(b)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '50.206(c)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '50.206(c)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '50.206(c)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '50.206(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '50.403-3') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '51.107') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '51.205') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '52.107(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '52.107(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '52.107(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '52.107(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '52.107(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '52.107(f)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '53.111') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '7.203') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '7.305(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '7.305(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '7.305(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '7.404') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '8.005') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '8.1104(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '8.1104(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '8.1104(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '8.1104(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '8.505') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '9.104-7(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '9.104-7(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '9.104-7(c)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '9.108-5(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '9.108-5(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '9.206-2') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '9.308-1(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '9.308-1(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '9.308-1(a)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '9.308-1(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '9.308-1(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '9.308-1(b)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '9.308-2(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '9.308-2(a)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '9.308-2(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '9.308-2(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '9.308-2(b)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '9.409') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, 'PGI 204.7108(d)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, 'PGI 204.7108(d)(10)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, 'PGI 204.7108(d)(11)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, 'PGI 204.7108(d)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, 'PGI 204.7108(d)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, 'PGI 204.7108(d)(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, 'PGI 204.7108(d)(5)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, 'PGI 204.7108(d)(6)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, 'PGI 204.7108(d)(7)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, 'PGI 204.7108(d)(8)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, 'PGI 204.7108(d)(9)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
/* ===============================================
 * Questions
   =============================================== */

UPDATE Question_Conditions
SET question_condition_sequence = 1200 -- 1360
WHERE question_condition_id = 10754; -- [TYPE OF SYSTEM]

UPDATE Question_Conditions
SET question_condition_sequence = 1230 -- 1390
WHERE question_condition_id = 10755; -- [CONTRACT TYPE]

UPDATE Question_Conditions
SET question_condition_sequence = 1300 -- 1460
WHERE question_condition_id = 10756; -- [QUANTITY VARIATION]

UPDATE Question_Conditions
SET question_condition_sequence = 1310 -- 1470
WHERE question_condition_id = 10757; -- [OPTIONS]

UPDATE Question_Conditions
SET question_condition_sequence = 1220 -- 1380
WHERE question_condition_id = 10791; -- [R&D TECHNOLOGIES]

UPDATE Question_Conditions
SET question_condition_sequence = 1210 -- 1370
WHERE question_condition_id = 10802; -- [OSD APPROVAL FOR DATA SYSTEM]

UPDATE Question_Conditions
SET question_condition_sequence = 1250 -- 1410
WHERE question_condition_id = 10803; -- [SWING FROM TARGET FEE]

UPDATE Question_Conditions
SET question_condition_sequence = 1270 -- 1430
WHERE question_condition_id = 10804; -- [SMALL BUSINESS AWARD FEE]

UPDATE Question_Conditions
SET question_condition_sequence = 1280 -- 1440
, condition_to_question = '("FIXED PRICE WITH ECONOMIC PRICE ADJUSTMENT -- ACTUAL COSTS" = [FIXED PRICE] OR "FIXED PRICE WITH ECONOMIC PRICE ADJUSTMENT -- COST INDICES" = [FIXED PRICE] OR "FIXED PRICE WITH ECONOMIC PRICE ADJUSTMENT -- ESTABLISHED PRICES" = [FIXED PRICE]) AND "COMMERCIAL PROCEDURES (FAR PART 12)" <> [PROCEDURES]' -- '("FIXED PRICE WITH ECONOMIC PRICE ADJUSTMENT -- ACTUAL COSTS" = [FIXED PRICE] OR "FIXED PRICE WITH ECONOMIC PRICE ADJUSTMENT -- COST INDICES" = [FIXED PRICE] OR "FIXED PRICE WITH ECONOMIC PRICE ADJUSTMENT -- ESTABLISHED PRICES" = [FIXED PRICE]) AND "COMME'
WHERE question_condition_id = 10805; -- [MAJOR DESIGN OR DEVELOPMENT]

UPDATE Question_Conditions
SET question_condition_sequence = 1290 -- 1450
, condition_to_question = '("FIXED PRICE WITH ECONOMIC PRICE ADJUSTMENT -- ACTUAL COSTS" = [FIXED PRICE] OR "FIXED PRICE WITH ECONOMIC PRICE ADJUSTMENT -- COST INDICES" = [FIXED PRICE] OR "FIXED PRICE WITH ECONOMIC PRICE ADJUSTMENT -- ESTABLISHED PRICES" = [FIXED PRICE]) AND "COMMERCIAL PROCEDURES (FAR PART 12)" <> [PROCEDURES]' -- '("FIXED PRICE WITH ECONOMIC PRICE ADJUSTMENT -- ACTUAL COSTS" = [FIXED PRICE] OR "FIXED PRICE WITH ECONOMIC PRICE ADJUSTMENT -- COST INDICES" = [FIXED PRICE] OR "FIXED PRICE WITH ECONOMIC PRICE ADJUSTMENT -- ESTABLISHED PRICES" = [FIXED PRICE]) AND "COMME'
WHERE question_condition_id = 10806; -- [PCO EPA DETERMINATION]

UPDATE Question_Conditions
SET question_condition_sequence = 1320 -- 1480
WHERE question_condition_id = 10807; -- [OPTION CONDITIONS]

UPDATE Question_Conditions
SET question_condition_sequence = 1330 -- 1490
WHERE question_condition_id = 10812; -- [LABOR REQUIREMENTS 1]

UPDATE Question_Conditions
SET question_condition_sequence = 1340 -- 1500
WHERE question_condition_id = 10813; -- [LABOR REQUIREMENTS 2]

UPDATE Question_Conditions
SET question_condition_sequence = 1460 -- 1620
WHERE question_condition_id = 10814; -- [LABOR REQUIREMENTS 3]

UPDATE Question_Conditions
SET question_condition_sequence = 1350 -- 1510
WHERE question_condition_id = 10815; -- [CONTRACTOR MATERIAL]

UPDATE Question_Conditions
SET question_condition_sequence = 1360 -- 1520
WHERE question_condition_id = 10816; -- [SUBCONTRACTING]

UPDATE Question_Conditions
SET question_condition_sequence = 1500 -- 1660
WHERE question_condition_id = 10817; -- [ENVIRONMENT]

UPDATE Question_Conditions
SET question_condition_sequence = 1380 -- 1540
WHERE question_condition_id = 10818; -- [PERFORMANCE REQUIREMENTS 1]

UPDATE Question_Conditions
SET question_condition_sequence = 1400 -- 1560
WHERE question_condition_id = 10819; -- [PERFORMANCE REQUIREMENTS 2]

UPDATE Question_Conditions
SET question_condition_sequence = 1390 -- 1550
WHERE question_condition_id = 10820; -- [TECHNICAL CHANGES]

UPDATE Question_Conditions
SET question_condition_sequence = 1410 -- 1570
WHERE question_condition_id = 10821; -- [VALUE ENGINEERING CONDITIONS]

UPDATE Question_Conditions
SET question_condition_sequence = 1420 -- 1580
WHERE question_condition_id = 10822; -- [FIRST ARTICLE CONDITIONS]

UPDATE Question_Conditions
SET question_condition_sequence = 1430 -- 1590
WHERE question_condition_id = 10823; -- [DD FORM 1494]

UPDATE Question_Conditions
SET question_condition_sequence = 1440 -- 1600
WHERE question_condition_id = 10824; -- [LABOR ACTS]

UPDATE Question_Conditions
SET question_condition_sequence = 1450 -- 1610
WHERE question_condition_id = 10825; -- [UNEMPLOYMENT RATE]

UPDATE Question_Conditions
SET question_condition_sequence = 1470 -- 1630
WHERE question_condition_id = 10826; -- [DRUG FREE WORKFORCE REQUIRED]

UPDATE Question_Conditions
SET question_condition_sequence = 1480 -- 1640
WHERE question_condition_id = 10827; -- [GOVERNMENT FURNISHED PROPERTY]

UPDATE Question_Conditions
SET question_condition_sequence = 1490 -- 1650
WHERE question_condition_id = 10828; -- [GOVERNMENT SUPPLY STATEMENT]

UPDATE Question_Conditions
SET question_condition_sequence = 1370 -- 1530
WHERE question_condition_id = 10829; -- [SUBCONTRACTING PLAN CONDITIONS]

UPDATE Question_Conditions
SET question_condition_sequence = 1510 -- 1670
WHERE question_condition_id = 10830; -- [INTEREST ON PARTIAL PAYMENTS]

UPDATE Question_Conditions
SET question_condition_sequence = 1520 -- 1680
WHERE question_condition_id = 10831; -- [TERMINATION]

UPDATE Question_Conditions
SET question_condition_sequence = 1530 -- 1690
WHERE question_condition_id = 10832; -- [END ITEM MARKING]

UPDATE Question_Conditions
SET question_condition_sequence = 1540 -- 1700
WHERE question_condition_id = 10833; -- [PRESERVATION AND PACKING SPECIFICATIONS]

UPDATE Question_Conditions
SET question_condition_sequence = 1550 -- 1710
WHERE question_condition_id = 10834; -- [INSPECTION AND ACCEPTANCE REQUIREMENTS 2]

UPDATE Question_Conditions
SET question_condition_sequence = 1570 -- 1730
WHERE question_condition_id = 10835; -- [USE OF WARRANTY]

UPDATE Question_Conditions
SET question_condition_sequence = 1560 -- 1720
WHERE question_condition_id = 10836; -- [INSPECTION AND ACCEPTANCE REQUIREMENTS]

UPDATE Question_Conditions
SET question_condition_sequence = 1580 -- 1740
WHERE question_condition_id = 10837; -- [WARRANTY CONDITIONS]

UPDATE Question_Conditions
SET question_condition_sequence = 1590 -- 1750
WHERE question_condition_id = 10838; -- [WARRANTY RECOVERY AND TRANSPORTATION]

UPDATE Question_Conditions
SET question_condition_sequence = 1600 -- 1760
WHERE question_condition_id = 10839; -- [WARRANTY DESCRIPTION]

UPDATE Question_Conditions
SET question_condition_sequence = 1610 -- 1770
WHERE question_condition_id = 10840; -- [PERIOD OF PERFORMANCE]

UPDATE Question_Conditions
SET question_condition_sequence = 1620 -- 1780
WHERE question_condition_id = 10841; -- [PLACE OF PERFORMANCE]

UPDATE Question_Conditions
SET question_condition_sequence = 1680 -- 1840
WHERE question_condition_id = 10842; -- [PLACE TYPE]

UPDATE Question_Conditions
SET question_condition_sequence = 1730 -- 1890
WHERE question_condition_id = 10843; -- [SEPARATE DELIVERABLES]

UPDATE Question_Conditions
SET question_condition_sequence = 1630 -- 1790
WHERE question_condition_id = 10844; -- [FOREIGN COUNTRY]

UPDATE Question_Conditions
SET question_condition_sequence = 1650 -- 1810
WHERE question_condition_id = 10845; -- [MILITARY TECHNICAL AGREEMENT]

UPDATE Question_Conditions
SET question_condition_sequence = 1660 -- 1820
WHERE question_condition_id = 10846; -- [STATE]

UPDATE Question_Conditions
SET question_condition_sequence = 1670 -- 1830
WHERE question_condition_id = 10847; -- [OUTSIDE U.S.]

UPDATE Question_Conditions
SET question_condition_sequence = 1700 -- 1860
WHERE question_condition_id = 10848; -- [DELIVERY BASIS]

UPDATE Question_Conditions
SET question_condition_sequence = 1710 -- 1870
WHERE question_condition_id = 10849; -- [CONTINUED PERFORMANCE]

UPDATE Question_Conditions
SET question_condition_sequence = 1720 -- 1880
WHERE question_condition_id = 10850; -- [LIQUIDATED DAMAGES]

UPDATE Questions
SET question_group = 'I' -- D
WHERE question_id = 34840; -- [TRANSPORTATION REQUIREMENTS 1]
UPDATE Question_Conditions
SET question_condition_sequence = 1740 -- 1200
WHERE question_condition_id = 10758; -- [TRANSPORTATION REQUIREMENTS 1]

UPDATE Questions
SET question_group = 'I' -- D
WHERE question_id = 34841; -- [TRANSPORTATION REQUIREMENTS 2]
UPDATE Question_Conditions
SET question_condition_sequence = 1780 -- 1240
WHERE question_condition_id = 10759; -- [TRANSPORTATION REQUIREMENTS 2]

UPDATE Questions
SET question_group = 'I' -- D
WHERE question_id = 34842; -- [TRANSPORTATION REQUIREMENTS 3]
UPDATE Question_Conditions
SET question_condition_sequence = 1790 -- 1250
WHERE question_condition_id = 10760; -- [TRANSPORTATION REQUIREMENTS 3]

UPDATE Questions
SET question_group = 'I' -- D
WHERE question_id = 34843; -- [TRANSPORTATION REQUIREMENTS 4]
UPDATE Question_Conditions
SET question_condition_sequence = 1800 -- 1260
WHERE question_condition_id = 10761; -- [TRANSPORTATION REQUIREMENTS 4]

UPDATE Questions
SET question_group = 'I' -- D
WHERE question_id = 34844; -- [TRANSPORTATION REQUIREMENTS 5]
UPDATE Question_Conditions
SET question_condition_sequence = 1810 -- 1270
WHERE question_condition_id = 10762; -- [TRANSPORTATION REQUIREMENTS 5]

UPDATE Questions
SET question_group = 'I' -- D
WHERE question_id = 34845; -- [TRANSPORTATION REQUIREMENTS 6]
UPDATE Question_Conditions
SET question_condition_sequence = 1830 -- 1290
WHERE question_condition_id = 10763; -- [TRANSPORTATION REQUIREMENTS 6]

UPDATE Questions
SET question_group = 'I' -- D
WHERE question_id = 34846; -- [TRANSPORTATION REQUIREMENTS 7]
UPDATE Question_Conditions
SET question_condition_sequence = 1840 -- 1300
WHERE question_condition_id = 10764; -- [TRANSPORTATION REQUIREMENTS 7]

UPDATE Questions
SET question_group = 'I' -- D
WHERE question_id = 34847; -- [TRANSPORTATION REQUIREMENT 8]
UPDATE Question_Conditions
SET question_condition_sequence = 1850 -- 1310
WHERE question_condition_id = 10765; -- [TRANSPORTATION REQUIREMENT 8]

UPDATE Questions
SET question_group = 'I' -- D
WHERE question_id = 34848; -- [TRANSPORTATION REQUIREMENTS 9]
UPDATE Question_Conditions
SET question_condition_sequence = 1860 -- 1320
WHERE question_condition_id = 10766; -- [TRANSPORTATION REQUIREMENTS 9]

UPDATE Questions
SET question_group = 'I' -- D
WHERE question_id = 34849; -- [TRANSPORTATION REQUIREMENTS 10]
UPDATE Question_Conditions
SET question_condition_sequence = 1870 -- 1330
WHERE question_condition_id = 10767; -- [TRANSPORTATION REQUIREMENTS 10]

UPDATE Questions
SET question_group = 'I' -- D
WHERE question_id = 34850; -- [TRANSPORTATION REQUIREMENTS 11]
UPDATE Question_Conditions
SET question_condition_sequence = 1880 -- 1340
WHERE question_condition_id = 10768; -- [TRANSPORTATION REQUIREMENTS 11]

UPDATE Questions
SET question_group = 'I' -- D
WHERE question_id = 34851; -- [SHIPPING STATEMENTS]
UPDATE Question_Conditions
SET question_condition_sequence = 1890 -- 1350
WHERE question_condition_id = 10769; -- [SHIPPING STATEMENTS]

UPDATE Questions
SET question_group = 'I' -- D
WHERE question_id = 34891; -- [FOB ORIGIN DESIGNATIONS]
UPDATE Question_Conditions
SET question_condition_sequence = 1750 -- 1210
WHERE question_condition_id = 10808; -- [FOB ORIGIN DESIGNATIONS]

UPDATE Questions
SET question_group = 'I' -- D
WHERE question_id = 34892; -- [WITHIN CONSIGNEE'S PREMISES]
UPDATE Question_Conditions
SET question_condition_sequence = 1760 -- 1220
WHERE question_condition_id = 10809; -- [WITHIN CONSIGNEE'S PREMISES]

UPDATE Questions
SET question_group = 'I' -- D
WHERE question_id = 34893; -- [FOB DESIGNATION]
UPDATE Question_Conditions
SET question_condition_sequence = 1770 -- 1230
WHERE question_condition_id = 10810; -- [FOB DESIGNATION]

UPDATE Questions
SET question_group = 'I' -- D
WHERE question_id = 34894; -- [CARGO PREFERENCE ACT]
UPDATE Question_Conditions
SET question_condition_sequence = 1820 -- 1280
WHERE question_condition_id = 10811; -- [CARGO PREFERENCE ACT]

UPDATE Question_Conditions
SET question_condition_sequence = 1240 -- 1400
WHERE question_condition_id = 10931; -- [COST REIMBURSEMENT]

UPDATE Question_Conditions
SET question_condition_sequence = 1260 -- 1420
WHERE question_condition_id = 10932; -- [FIXED PRICE]

UPDATE Question_Conditions
SET question_condition_sequence = 1640 -- 1800
WHERE question_condition_id = 10934; -- [UNITED STATES OUTLYING AREAS]

UPDATE Question_Conditions
SET question_condition_sequence = 1690 -- 1850
WHERE question_condition_id = 10935; -- [GOVERNMENT OWNED OR LEASED FACILITIES/INSTALLATIONS]
/*
<h2>52.204-1</h2>
No identifier
<pre>(If applicable, the rule for the prescription for using this clause will be in the component clause</pre><br />Empty Rule
*//*
<h2>52.226-6</h2>
(F) No " IS:" found
<pre>(F) FOR USE IN UNITED STATES (50 STATES PLUS DISTRICT OF COLUMBIA)</pre>
*/
-- ProduceClause.resolveIssues() Summary: Total(1221); Merged(14); Corrected(342); Error(2)
/* ===============================================
 * Clause
   =============================================== */

UPDATE Clauses
SET clause_data = '<p>(a) <i>Definitions.</i> As used in this clause-</p><p><i>Adequate security</i> means protective measures that are commensurate with the consequences and probability of loss, misuse, or unauthorized access to, or modification of information.</p><p><i>Compromise</i> means disclosure of information to unauthorized persons, or a violation of the security policy of a system, in which unauthorized intentional or unintentional disclosure, modification, destruction, or loss of an object, or the copying of information to unauthorized media may have occurred.</p><p><i>Contractor attributional/proprietary information</i> means information that identifies the contractor(s), whether directly or indirectly, by the grouping of information that can be traced back to the contractor(s) (<i>e.g.,</i> program description, facility locations), personally identifiable information, as well as trade secrets, commercial or financial information, or other commercially sensitive information that is not customarily shared outside of the company.</p><p><i>Contractor information system</i> means an information system belonging to, or operated by or for, the Contractor.</p><p><i>Controlled technical information</i> means technical information with military or space application that is subject to controls on the access, use, reproduction, modification, performance, display, release, disclosure, or dissemination. Controlled technical information would meet the criteria, if disseminated, for distribution statements B through F using the criteria set forth in DoD Instruction 5230.24, Distribution Statements on Technical Documents. The term does not include information that is lawfully publicly available without restrictions.</p><p><i>Covered contractor information system</i> means an information system that is owned, or operated by or for, a contractor and that processes, stores, or transmits covered defense information.</p><p><i>Covered defense information</i> means unclassified information that-</p><p>(i) Is-</p><p>(A) Provided to the contractor by or on behalf of DoD in connection with the performance of the contract; or</p><p>(B) Collected, developed, received, transmitted, used, or stored by or on behalf of the contractor in support of the performance of the contract; and</p><p>(ii) Falls in any of the following categories:</p><p>(A) <i>Controlled technical information.</i></p><p>(B) <i>Critical information (operations security).</i> Specific facts identified through the Operations Security process about friendly intentions, capabilities, and activities vitally needed by adversaries for them to plan and act effectively so as to guarantee failure or unacceptable consequences for friendly mission accomplishment (part of Operations Security process).</p><p>(C) <i>Export control.</i> Unclassified information concerning certain items, commodities, technology, software, or other information whose export could reasonably be expected to adversely affect the United States national security and nonproliferation objectives. To include dual use items; items identified in export administration regulations, international traffic in arms regulations and munitions list; license applications; and sensitive nuclear technology information.</p><p>(D) Any other information, marked or otherwise identified in the contract, that requires safeguarding or dissemination controls pursuant to and consistent with law, regulations, and Governmentwide policies (<i>e.g.,</i> privacy, proprietary business information).</p><p><i>Cyber incident</i> means actions taken through the use of computer networks that result in an actual or potentially adverse effect on an information system and/or the information residing therein.</p><p><i>Forensic analysis</i> means the practice of gathering, retaining, and analyzing computer-related data for investigative purposes in a manner that maintains the integrity of the data.</p><p><i>Malicious software</i> means computer software or firmware intended to perform an unauthorized process that will have adverse impact on the confidentiality, integrity, or availability of an information system. This definition includes a virus, worm, Trojan horse, or other code-based entity that infects a host, as well as spyware and some forms of adware.</p><p><i>Media</i> means physical devices or writing surfaces including, but is not limited to, magnetic tapes, optical disks, magnetic disks, large-scale integration memory chips, and printouts onto which information is recorded, stored, or printed within an information system.</p><p><i>Operationally critical support</i> means supplies or services designated by the Government as critical for airlift, sealift, intermodal transportation services, or logistical support that is essential to the mobilization, deployment, or sustainment of the Armed Forces in a contingency operation.</p><p><i>Rapid(ly) report(ing)</i> means within 72 hours of discovery of any cyber incident.</p><p><i>Technical information</i> means technical data or computer software, as those terms are defined in the clause at DFARS <i>252.227-7013,</i> Rights in Technical Data-Non Commercial Items, regardless of whether or not the clause is incorporated in this solicitation or contract. Examples of technical information include research and engineering data, engineering drawings, and associated lists, specifications, standards, process sheets, manuals, technical reports, technical orders, catalog-item identifications, data sets, studies and analyses and related information, and computer software executable code and source code.</p><p>(b) <i>Adequate security.</i> The Contractor shall provide adequate security for all covered defense information on all covered contractor information systems that support the performance of work under this contract. To provide adequate security, the Contractor shall-</p><p>(1) Implement information systems security protections on all covered contractor information systems including, at a minimum-</p><p>(i) For covered contractor information systems that are part of an Information Technology (IT) service or system operated on behalf of the Government-</p><p>(A) Cloud computing services shall be subject to the security requirements specified in the clause 252.239-7010, Cloud Computing Services, of this contract; and</p><p>(B) Any other such IT service or system (<i>i.e.,</i> other than cloud computing) shall be subject to the security requirements specified elsewhere in this contract; or</p><p>(ii) For covered contractor information systems that are not part of an IT service or system operated on behalf of the Government and therefore are not subject to the security requirement specified at paragraph (b)(1)(i) of this clause-</p><p>(A) The security requirements in National Institute of Standards and Technology (NIST) Special Publication (SP) 800-171, ''Protecting Controlled Unclassified Information in Nonfederal Information Systems and Organizations,'', <i>http://dx.doi.org/10.6028/NIST.SP.800-171</i> that is in effect at the time the solicitation is issued or as authorized by the Contracting Officer; or</p><p>(B) Alternative but equally effective security measures used to compensate for the inability to satisfy a particular requirement and achieve equivalent protection approved in writing by an authorized representative of the DoD CIO prior to contract award; and</p><p>(2) Apply other security measures when the Contractor reasonably determines that such measures, in addition to those identified in paragraph (b)(1) of this clause, may be required to provide adequate security in a dynamic environment based on an assessed risk or vulnerability.</p><p>(c) <i>Cyber incident reporting requirement.</i></p><p>(1) When the Contractor discovers a cyber incident that affects a covered contractor information system or the covered defense information residing therein, or that affects the contractor''s ability to perform the requirements of the contract that are designated as operationally critical support, the Contractor shall-</p><p>(i) Conduct a review for evidence of compromise of covered defense information, including, but not limited to, identifying compromised computers, servers, specific data, and user accounts. This review shall also include analyzing covered contractor information system(s) that were part of the cyber incident, as well as other information systems on the Contractor''s network(s), that may have been accessed as a result of the incident in order to identify compromised covered defense information, or that affect the Contractor''s ability to provide operationally critical support; and</p><p>(ii) Rapidly report cyber incidents to DoD at <i>http://dibnet.dod.mil.</i></p><p>(2) <i>Cyber incident report.</i> The cyber incident report shall be treated as information created by or for DoD and shall include, at a minimum, the required elements at <i>http://dibnet.dod.mil.</i></p><p>(3) <i>Medium assurance certificate requirement.</i> In order to report cyber incidents in accordance with this clause, the Contractor or subcontractor shall have or acquire a DoD-approved medium assurance certificate to report cyber incidents. For information on obtaining a DoD-approved medium assurance certificate, see <i>http://iase.disa.mil/pki/eca/Pages/index.aspx</i>.</p><p>(d) <i>Malicious software.</i> The Contractor or subcontractors that discover and isolate malicious software in connection with a reported cyber incident shall submit the malicious software in accordance with instructions provided by the Contracting Officer.</p><p>(e) <i>Media preservation and protection.</i> When a Contractor discovers a cyber incident has occurred, the Contractor shall preserve and protect images of all known affected information systems identified in paragraph (c)(1)(i) of this clause and all relevant monitoring/packet capture data for at least 90 days from the submission of the cyber incident report to allow DoD to request the media or decline interest.</p><p>(f) <i>Access to additional information or equipment necessary for forensic analysis.</i> Upon request by DoD, the Contractor shall provide DoD with access to additional information or equipment that is necessary to conduct a forensic analysis.</p><p>(g) <i>Cyber incident damage assessment activities.</i> If DoD elects to conduct a damage assessment, the Contracting Officer will request that the Contractor provide all of the damage assessment information gathered in accordance with paragraph (e) of this clause.</p><p>(h) <i>DoD safeguarding and use of contractor attributional/proprietary information.</i> The Government shall protect against the unauthorized use or release of information obtained from the contractor (or derived from information obtained from the contractor) under this clause that includes contractor attributional/proprietary information, including such information submitted in accordance with paragraph (c). To the maximum extent practicable, the Contractor shall identify and mark attributional/proprietary information. In making an authorized release of such information, the Government will implement appropriate procedures to minimize the contractor attributional/proprietary information that is included in such authorized release, seeking to include only that information that is necessary for the authorized purpose(s) for which the information is being released.</p><p>(i) <i>Use and release of contractor attributional/proprietary information not created by or for DoD.</i> Information that is obtained from the contractor (or derived from information obtained from the contractor) under this clause that is not created by or for DoD is authorized to be released outside of DoD-</p><p>(1) To entities with missions that may be affected by such information;</p><p>(2) To entities that may be called upon to assist in the diagnosis, detection, or mitigation of cyber incidents;</p><p>(3) To Government entities that conduct counterintelligence or law enforcement investigations;</p><p>(4) For national security purposes, including cyber situational awareness and defense purposes (including with Defense Industrial Base (DIB) participants in the program at 32 CFR part 236); or</p><p>(5) To a support services contractor (''recipient'') that is directly supporting Government activities under a contract that includes the clause at 252.204-7009, Limitations on the Use or Disclosure of Third-Party Contractor Reported Cyber Incident Information.</p><p>(j) <i>Use and release of contractor attributional/proprietary information created by or for DoD.</i> Information that is obtained from the contractor (or derived from information obtained from the contractor) under this clause that is created by or for DoD (including the information submitted pursuant to paragraph (c) of this clause) is authorized to be used and released outside of DoD for purposes and activities authorized by paragraph (i) of this clause, and for any other lawful Government purpose or activity, subject to all applicable statutory, regulatory, and policy based restrictions on the Government''s use and release of such information.</p><p>(k) The Contractor shall conduct activities under this clause in accordance with applicable laws and regulations on the interception, monitoring, access, use, and disclosure of electronic communications and data.</p><p>(l) <i>Other safeguarding or reporting requirements.</i> The safeguarding and cyber incident reporting required by this clause in no way abrogates the Contractor''s responsibility for other safeguarding or cyber incident reporting pertaining to its unclassified information systems as required by other applicable clauses of this contract, or as a result of other applicable U.S. Government statutory or regulatory requirements.</p><p>(m) <i>Subcontracts.</i> The Contractor shall-</p><p>(1) Include the substance of this clause, including this paragraph (m), in all subcontracts, including subcontracts for commercial items; and</p><p>(2) Require subcontractors to rapidly report cyber incidents directly to DoD at <i>http://dibnet.dod.mil</i> and the prime Contractor. This includes providing the incident report number, automatically assigned by DoD, to the prime Contractor (or next higher-tier subcontractor) as soon as practicable.</p>'
--               '<p>(a) <i>Definitions.</i> As used in this clause-</p><p><i>Adequate security</i> means protective measures that are commensurate with the consequences and probability of loss, misuse, or unauthorized access to, or modification of information.</p><p><i>Compromise</i> means disclosure of information to unauthorized persons, or a violation of the security policy of a system, in which unauthorized intentional or unintentional disclosure, modification, destruction, or loss of an object, or the copying of information to unauthorized media may have occurred.</p><p><i>Contractor attributional/proprietary information</i> means information that identifies the contractor(s), whether directly or indirectly, by the grouping of information that can be traced back to the contractor(s) (<i>e.g.,</i> program description, facility locations), personally identifiable information, as well as trade secrets, commercial or financial information, or other commercially sensitive information that is not customarily shared outside of the company.</p><p><i>Contractor information system</i> means an information system belonging to, or operated by or for, the Contractor.</p><p><i>Controlled technical information</i> means technical information with military or space application that is subject to controls on the access, use, reproduction, modification, performance, display, release, disclosure, or dissemination. Controlled technical information would meet the criteria, if disseminated, for distribution statements B through F using the criteria set forth in DoD Instruction 5230.24, Distribution Statements on Technical Documents. The term does not include information that is lawfully publicly available without restrictions.</p><p><i>Covered contractor information system</i> means an information system that is owned, or operated by or for, a contractor and that processes, stores, or transmits covered defense information.</p><p><i>Covered defense information</i> means unclassified information that-</p><p>(i) Is-</p><p>(A) Provided to the contractor by or on behalf of DoD in connection with the performance of the contract; or</p><p>(B) Collected, developed, received, transmitted, used, or stored by or on behalf of the contractor in support of the performance of the contract; and</p><p>(ii) Falls in any of the following categories:</p><p>(A) <i>Controlled technical information.</i></p><p>(B) <i>Critical information (operations security).</i> Specific facts identified through the Operations Security process about friendly intentions, capabilities, and activities vitally needed by adversaries for them to plan and act effectively so as to guarantee failure or unacceptable consequences for friendly mission accomplishment (part of Operations Security process).</p><p>(C) <i>Export control.</i> Unclassified information concerning certain items, commodities, technology, software, or other information whose export could reasonably be expected to adversely affect the United States national security and nonproliferation objectives. To include dual use items; items identified in export administration regulations, international traffic in arms regulations and munitions list; license applications; and sensitive nuclear technology information.</p><p>(D) Any other information, marked or otherwise identified in the contract, that requires safeguarding or dissemination controls pursuant to and consistent with law, regulations, and Governmentwide policies (<i>e.g.,</i> privacy, proprietary business information).</p><p><i>Cyber incident</i> means actions taken through the use of computer networks that result in an actual or potentially adverse effect on an information system and/or the information residing therein.</p><p><i>Forensic analysis</i> means the practice of gathering, retaining, and analyzing computer-related data for investigative purposes in a manner that maintains the integrity of the data.</p><p><i>Malicious software</i> means computer software or firmware intended to perform an unauthorized process that will have adverse impact on the confidentiality, integrity, or availability of an information system. This definition includes a virus, worm, Trojan horse, or other code-based entity that infects a host, as well as spyware and some forms of adware.</p><p><i>Media</i> means physical devices or writing surfaces including, but is not limited to, magnetic tapes, optical disks, magnetic disks, large-scale integration memory chips, and printouts onto which information is recorded, stored, or printed within an information system.</p><p><i>Operationally critical support</i> means supplies or services designated by the Government as critical for airlift, sealift, intermodal transportation services, or logistical support that is essential to the mobilization, deployment, or sustainment of the Armed Forces in a contingency operation.</p><p><i>Rapid(ly) report(ing)</i> means within 72 hours of discovery of any cyber incident.</p><p><i>Technical information</i> means technical data or computer software, as those terms are defined in the clause at DFARS <i>252.227-7013,</i> Rights in Technical Data-Non Commercial Items, regardless of whether or not the clause is incorporated in this solicitation or contract. Examples of technical information include research and engineering data, engineering drawings, and associated lists, specifications, standards, process sheets, manuals, technical reports, technical orders, catalog-item identifications, data sets, studies and analyses and related information, and computer software executable code and source code.</p><p>(b) <i>Adequate security.</i> The Contractor shall provide adequate security for all covered defense information on all covered contractor information systems that support the performance of work under this contract. To provide adequate security, the Contractor shall-</p><p>(1) Implement information systems security protections on all covered contractor information systems including, at a minimum-</p><p>(i) For covered contractor information systems that are part of an Information Technology (IT) service or system operated on behalf of the Government-</p><p>(A) Cloud computing services shall be subject to the security requirements specified in the clause 252.239-7010, Cloud Computing Services, of this contract; and</p><p>(B) Any other such IT service or system (<i>i.e.,</i> other than cloud computing) shall be subject to the security requirements specified elsewhere in this contract; or</p><p>(ii) For covered contractor information systems that are not part of an IT service of system operated on behalf of the Government and therefore are not subject to the security requirement specified at paragraph (b)(1)(i) of this clause-</p><p>(A) The security requirements in National Institute of Standards and Technology (NIST) Special Publication (SP) 800-171, ''Protecting Controlled Unclassified Information in Nonfederal Information Systems and Organizations, <i>http://dx.doi.org/10.6028/NIST.SP.800-171</i> that is in effect at the time the solicitation is issued or as authorized by the Contracting Officer; or</p><p>(B) Alternative but equally effective security measures used to compensate for the inability to satisfy a particular requirement and achieve equivalent protection approved in writing by an authorized representative of the DoD CIO prior to contract award; and</p><p>(2) Apply other security measures when the Contractor reasonably determines that such measures, in addition to those identified in paragraph (b)(1) of this clause, may be required to provide adequate security in a dynamic environment based on an assessed risk or vulnerability.</p><p>(c) <i>Cyber incident reporting requirement.</i></p><p>(1) When the Contractor discovers a cyber incident that affects a covered contractor information system or the covered defense information residing therein, or that affects the contractor''s ability to perform the requirements of the contract that are designated as operationally critical support, the Contractor shall-</p><p>(i) Conduct a review for evidence of compromise of covered defense information, including, but not limited to, identifying compromised computers, servers, specific data, and user accounts. This review shall also include analyzing covered contractor information system(s) that were part of the cyber incident, as well as other information systems on the Contractor''s network(s), that may have been accessed as a result of the incident in order to identify compromised covered defense information, or that affect the Contractor''s ability to provide operationally critical support; and</p><p>(ii) Rapidly report cyber incidents to DoD at <i>http://dibnet.dod.mil.</i></p><p>(2) <i>Cyber incident report.</i> The cyber incident report shall be treated as information created by or for DoD and shall include, at a minimum, the required elements at <i>http://dibnet.dod.mil.</i></p><p>(3) <i>Medium assurance certificate requirement.</i> In order to report cyber incidents in accordance with this clause, the Contractor or subcontractor shall have or acquire a DoD-approved medium assurance certificate to report cyber incidents. For information on obtaining a DoD-approved medium assurance certificate, see <i>http://iase.disa.mil/pki/eca/certificate.html.</i></p><p>(d) <i>Malicious software.</i> The Contractor or subcontractors that discover and isolate malicious software in connection with a reported cyber incident shall submit the malicious software in accordance with instructions provided by the Contracting Officer.</p><p>(e) <i>Media preservation and protection.</i> When a Contractor discovers a cyber incident has occurred, the Contractor shall preserve and protect images of all known affected information systems identified in paragraph (c)(1)(i) of this clause and all relevant monitoring/packet capture data for at least 90 days from the submission of the cyber incident report to allow DoD to request the media or decline interest.</p><p>(f) <i>Access to additional information or equipment necessary for forensic analysis.</i> Upon request by DoD, the Contractor shall provide DoD with access to additional information or equipment that is necessary to conduct a forensic analysis.</p><p>(g) <i>Cyber incident damage assessment activities.</i> If DoD elects to conduct a damage assessment, the Contracting Officer will request that the Contractor provide all of the damage assessment information gathered in accordance with paragraph (e) of this clause.</p><p>(h) <i>DoD safeguarding and use of contractor attributional/proprietary information.</i> The Government shall protect against the unauthorized use or release of information obtained from the contractor (or derived from information obtained from the contractor) under this clause that includes contractor attributional/proprietary information, including such information submitted in accordance with paragraph (c). To the maximum extent practicable, the Contractor shall identify and mark attributional/proprietary information. In making an authorized release of such information, the Government will implement appropriate procedures to minimize the contractor attributional/proprietary information that is included in such authorized release, seeking to include only that information that is necessary for the authorized purpose(s) for which the information is being released.</p><p>(i) <i>Use and release of contractor attributional/proprietary information not created by or for DoD.</i> Information that is obtained from the contractor (or derived from information obtained from the contractor) under this clause that is not created by or for DoD is authorized to be released outside of DoD-</p><p>(1) To entities with missions that may be affected by such information;</p><p>(2) To entities that may be called upon to assist in the diagnosis, detection, or mitigation of cyber incidents;</p><p>(3) To Government entities that conduct counterintelligence or law enforcement investigations;</p><p>(4) For national security purposes, including cyber situational awareness and defense purposes (including with Defense Industrial Base (DIB) participants in the program at 32CFR 236); or</p><p>(5) To a support services contractor (''recipient'') that is directly supporting Government activities under a contract that includes the clause at 252.204-7009, Limitations on the Use or Disclosure of Third-Party Contractor Reported Cyber Incident Information.</p><p>(j) <i>Use and release of contractor attributional/proprietary information created by or for DoD.</i> Information that is obtained from the contractor (or derived from information obtained from the contractor) under this clause that is created by or for DoD (including the information submitted pursuant to paragraph (c) of this clause) is authorized to be used and released outside of DoD for purposes and activities authorized by paragraph (i) of this clause, and for any other lawful Government purpose or activity, subject to all applicable statutory, regulatory, and policy based restrictions on the Government''s use and release of such information.</p><p>(k) The Contractor shall conduct activities under this clause in accordance with applicable laws and regulations on the interception, monitoring, access, use, and disclosure of electronic communications and data.</p><p>(l) <i>Other safeguarding or reporting requirements.</i> The safeguarding and cyber incident reporting required by this clause in no way abrogates the Contractor''s responsibility for other safeguarding or cyber incident reporting pertaining to its unclassified information systems as required by other applicable clauses of this contract, or as a result of other applicable U.S. Government statutory or regulatory requirements.</p><p>(m) <i>Subcontracts.</i> The Contractor shall-</p><p>(1) Include the substance of this clause, including this paragraph (m), in all subcontracts, including subcontracts for commercial items; and</p><p>(2) Require subcontractors to rapidly report cyber incidents directly to DoD at <i>http://dibnet.dod.mil</i> and the prime Contractor. This includes providing the incident report number, automatically assigned by DoD, to the prime Contractor (or next higher-tier subcontractor) as soon as practicable.</p>'
WHERE clause_version_id = 7 AND clause_name = '252.204-7012';

UPDATE Clauses
SET clause_data = '<p>(a) <i>Definitions.</i> As used in this clause-</p><p><i>Bahrainian end product</i> means an article that-</p><p>(i) Is wholly the growth, product, or manufacture of Bahrain; or</p><p>(ii) In the case of an article that consists in whole or in part of materials from another country, has been substantially transformed in Bahrain into a new and different article of commerce with a name, character, or use distinct from that of the article or articles from which it was transformed. The term refers to a product offered for purchase under a supply contract, but for purposes of calculating the value of the end product includes services (except transportation services) incidental to its supply, provided that the value of those incidental services does not exceed the value of the product itself.</p><p><i>Commercially available off-the-shelf (COTS) item</i>-</p><p>(i) Means any item of supply (including construction material) that is-</p><p>(A) A commercial item (as defined in paragraph (1) of the definition of ''commercial item'' in section 2.101 of the Federal Acquisition Regulation);</p><p>(B) Sold in substantial quantities in the commercial marketplace; and</p><p>(C) Offered to the Government, under a contract or subcontract at any tier, without modification, in the same form in which it is sold in the commercial marketplace; and</p><p>(ii) Does not include bulk cargo, as defined in 46 U.S.C. 40102(4), such as agricultural products and petroleum products.</p><p><i>Component</i> means an article, material, or supply incorporated directly into an end product.</p><p><i>Domestic end product</i> means-</p><p>(i) An unmanufactured end product that has been mined or produced in the United States; or</p><p>(ii) An end product manufactured in the United States if-</p><p>(A) The cost of its qualifying country components and its components that are mined, produced, or manufactured in the United States exceeds 50 percent of the cost of all its components. The cost of components includes transportation costs to the place of incorporation into the end product and U.S. duty (whether or not a duty-free entry certificate is issued). Scrap generated, collected, and prepared for processing in the United States is considered domestic. A component is considered to have been mined, produced, or manufactured in the United States (regardless of its source in fact) if the end product in which it is incorporated is manufactured in the United States and the component is of a class or kind for which the Government has determined that-</p><p>(<i>1</i>) Sufficient and reasonably available commercial quantities of a satisfactory quality are not mined, produced, or manufactured in the United States; or</p><p>(<i>2</i>) It is inconsistent with the public interest to apply the restrictions of the Buy American statute; or</p><p>(B) The end product is a COTS item.</p><p><i>End product</i> means those articles, materials, and supplies to be acquired under this contract for public use.</p><p><i>Foreign end product</i> means an end product other than a domestic end product.</p><p><i>Free Trade Agreement country</i> means Australia, Bahrain, Canada, Chile, Colombia, Costa Rica, Dominican Republic, El Salvador, Guatemala, Honduras, Korea (Republic of), Mexico, Morocco, Nicaragua, Panama, Peru, or Singapore;</p><p><i>Free Trade Agreement country end product</i> means an article that-</p><p>(i) Is wholly the growth, product, or manufacture of a Free Trade Agreement country; or</p><p>(ii) In the case of an article that consists in whole or in part of materials from another country, has been substantially transformed in a Free Trade Agreement country into a new and different article of commerce with a name, character, or use distinct from that of the article or articles from which it was transformed. The term refers to a product offered for purchase under a supply contract, but for purposes of calculating the value of the end product includes services (except transportation services) incidental to its supply, provided that the value of those incidental services does not exceed the value of the product itself.</p><p><i>Moroccan end product</i> means an article that-</p><p>(i) Is wholly the growth, product, or manufacture of Morocco; or</p><p>(ii) In the case of an article that consists in whole or in part of materials from another country, has been substantially transformed in Morocco into a new and different article of commerce with a name, character, or use distinct from that of the article or articles from which it was transformed. The term refers to a product offered for purchase under a supply contract, but for purposes of calculating the value of the end product includes services (except transportation services) incidental to its supply, provided that the value of those incidental services does not exceed the value of the product itself.</p><p><i>Panamanian end product</i> means an article that-</p><p>(i) Is wholly the growth, product, or manufacture of Panama; or</p><p>(ii) In the case of an article that consists in whole or in part of materials from another country, has been substantially transformed in Panama into a new and different article of commerce with a name, character, or use distinct from that of the article or articles from which it was transformed. The term refers to a product offered for purchase under a supply contract, but for purposes of calculating the value of the end product includes services (except transportation services) incidental to its supply, provided that the value of those incidental services does not exceed the value of the product itself.</p><p><i>Peruvian end product</i> means an article that-</p><p>(i) Is wholly the growth, product, or manufacture of Peru; or</p><p>(ii) In the case of an article that consists in whole or in part of materials from another country, has been substantially transformed in Peru into a new and different article of commerce with a name, character, or use distinct from that of the article or articles from which it was transformed. The term refers to a product offered for purchase under a supply contract, but for purposes of calculating the value of the end product includes services (except transportation services) incidental to its supply, provided that the value of those incidental services does not exceed the value of the product itself.</p><p><i>Qualifying country</i> means a country with a reciprocal defense procurement memorandum of understanding or international agreement with the United States in which both countries agree to remove barriers to purchases of supplies produced in the other country or services performed by sources of the other country, and the memorandum or agreement complies, where applicable, with the requirements of section 36 of the Arms Export Control Act (22 U.S.C. 2776) and with 10 U.S.C. 2457. Accordingly, the following are qualifying countries:</p><table><tbody><tr><td>Australia</td><td>&nbsp;&nbsp;&nbsp;Italy</td></tr><tr><td>Austria</td><td>&nbsp;&nbsp;&nbsp;Luxembourg</td></tr><tr><td>Belgium</td><td>&nbsp;&nbsp;&nbsp;Netherlands</td></tr><tr><td>Canada</td><td>&nbsp;&nbsp;&nbsp;Norway</td></tr><tr><td>Czech Republic</td><td>&nbsp;&nbsp;&nbsp;Poland</td></tr><tr><td>Denmark</td><td>&nbsp;&nbsp;&nbsp;Portugal</td></tr><tr><td>Egypt</td><td>&nbsp;&nbsp;&nbsp;Spain</td></tr><tr><td>Finland</td><td>&nbsp;&nbsp;&nbsp;Sweden</td></tr><tr><td>France</td><td>&nbsp;&nbsp;&nbsp;Switzerland</td></tr><tr><td>Germany</td><td>&nbsp;&nbsp;&nbsp;Turkey</td></tr><tr><td>Greece</td><td>&nbsp;&nbsp;&nbsp;United Kingdom of Great Britain and Northern Ireland</td></tr><tr><td>Israel</td><td>&nbsp;</td></tr></tbody></table><p><i>Qualifying country component</i> means a component mined, produced, or manufactured in a qualifying country.</p><p><i>Qualifying country end product</i> means-</p><p>(i) An unmanufactured end product mined or produced in a qualifying country; or</p><p>(ii) An end product manufactured in a qualifying country if-</p><p>(A) The cost of the following types of components exceeds 50 percent of the cost of all its components:</p><p>(<i>1</i>) Components mined, produced, or manufactured in a qualifying country.</p><p>(<i>2</i>) Components mined, produced, or manufactured in the United States.</p><p>(<i>3</i>) Components of foreign origin of a class or kind for which the Government has determined that sufficient and reasonably available commercial quantities of a satisfactory quality are not mined, produced, or manufactured in the United States; or</p><p>(B) The end product is a COTS item.</p><p><i>United States</i> means the 50 States, the District of Columbia, and outlying areas.</p><p>(b) Unless otherwise specified, this clause applies to all items in the Schedule. </p><p>(c) The Contractor shall deliver under this contract only domestic end products unless, in its offer, it specified delivery of qualifying country end products, Free Trade Agreement country end products other than Bahrainian end products, Moroccan end products, Panamanian end products, or Peruvian end products, or other foreign end products in the Buy American-Free Trade Agreements-Balance of Payments Program Certificate-Basic provision of the solicitation. If the Contractor certified in its offer that it will deliver a qualifying country end product or a Free Trade Agreement country end product other than a Bahrainian end product, a Moroccan end product, a Panamanian end product, or a Peruvian end product, the Contractor shall deliver a qualifying country end product, a Free Trade Agreement country end product other than a Bahrainian end product, a Moroccan end product, a Panamanian end product, or a Peruvian end product, or, at the Contractor''s option, a domestic end product. </p><p>(d) The contract price does not include duty for end products or components for which the Contractor will claim duty-free entry.</p>'
--               '<p>(a) <i>Definitions.</i> As used in this clause-</p><p><i>Bahrainian end product</i> means an article that-</p><p>(i) Is wholly the growth, product, or manufacture of Bahrain; or</p><p>(ii) In the case of an article that consists in whole or in part of materials from another country, has been substantially transformed in Bahrain into a new and different article of commerce with a name, character, or use distinct from that of the article or articles from which it was transformed. The term refers to a product offered for purchase under a supply contract, but for purposes of calculating the value of the end product includes services (except transportation services) incidental to its supply, provided that the value of those incidental services does not exceed the value of the product itself.</p><p><i>Commercially available off-the-shelf (COTS) item</i>-</p><p>(i) Means any item of supply (including construction material) that is-</p><p>(A) A commercial item (as defined in paragraph (1) of the definition of ''commercial item'' in section 2.101 of the Federal Acquisition Regulation);</p><p>(B) Sold in substantial quantities in the commercial marketplace; and</p><p>(C) Offered to the Government, under a contract or subcontract at any tier, without modification, in the same form in which it is sold in the commercial marketplace; and</p><p>(ii) Does not include bulk cargo, as defined in 46 U.S.C. 40102(4), such as agricultural products and petroleum products.</p><p><i>Component</i> means an article, material, or supply incorporated directly into an end product.</p><p><i>Domestic end product</i> means-</p><p>(i) An unmanufactured end product that has been mined or produced in the United States; or</p><p>(ii) An end product manufactured in the United States if-</p><p>(A) The cost of its qualifying country components and its components that are mined, produced, or manufactured in the United States exceeds 50 percent of the cost of all its components. The cost of components includes transportation costs to the place of incorporation into the end product and U.S. duty (whether or not a duty-free entry certificate is issued). Scrap generated, collected, and prepared for processing in the United States is considered domestic. A component is considered to have been mined, produced, or manufactured in the United States (regardless of its source in fact) if the end product in which it is incorporated is manufactured in the United States and the component is of a class or kind for which the Government has determined that-</p><p>(<i>1</i>) Sufficient and reasonably available commercial quantities of a satisfactory quality are not mined, produced, or manufactured in the United States; or</p><p>(<i>2</i>) It is inconsistent with the public interest to apply the restrictions of the Buy American statute; or</p><p>(B) The end product is a COTS item.</p><p><i>End product</i> means those articles, materials, and supplies to be acquired under this contract for public use.</p><p><i>Foreign end product</i> means an end product other than a domestic end product.</p><p><i>Free Trade Agreement country</i> means Australia, Bahrain, Canada, Chile, Colombia, Costa Rica, Dominican Republic, El Salvador, Guatemala, Honduras, Korea (Republic of), Mexico, Morocco, Nicaragua, Panama, Peru, or Singapore;</p><p><i>Free Trade Agreement country end product</i> means an article that-</p><p>(i) Is wholly the growth, product, or manufacture of a Free Trade Agreement country; or</p><p>(ii) In the case of an article that consists in whole or in part of materials from another country, has been substantially transformed in a Free Trade Agreement country into a new and different article of commerce with a name, character, or use distinct from that of the article or articles from which it was transformed. The term refers to a product offered for purchase under a supply contract, but for purposes of calculating the value of the end product includes services (except transportation services) incidental to its supply, provided that the value of those incidental services does not exceed the value of the product itself.</p><p><i>Moroccan end product</i> means an article that-</p><p>(i) Is wholly the growth, product, or manufacture of Morocco; or</p><p>(ii) In the case of an article that consists in whole or in part of materials from another country, has been substantially transformed in Morocco into a new and different article of commerce with a name, character, or use distinct from that of the article or articles from which it was transformed. The term refers to a product offered for purchase under a supply contract, but for purposes of calculating the value of the end product includes services (except transportation services) incidental to its supply, provided that the value of those incidental services does not exceed the value of the product itself.</p><p><i>Panamanian end product</i> means an article that-</p><p>(i) Is wholly the growth, product, or manufacture of Panama; or</p><p>(ii) In the case of an article that consists in whole or in part of materials from another country, has been substantially transformed in Panama into a new and different article of commerce with a name, character, or use distinct from that of the article or articles from which it was transformed. The term refers to a product offered for purchase under a supply contract, but for purposes of calculating the value of the end product includes services (except transportation services) incidental to its supply, provided that the value of those incidental services does not exceed the value of the product itself.</p><p><i>Peruvian end product</i> means an article that-</p><p>(i) Is wholly the growth, product, or manufacture of Peru; or</p><p>(ii) In the case of an article that consists in whole or in part of materials from another country, has been substantially transformed in Peru into a new and different article of commerce with a name, character, or use distinct from that of the article or articles from which it was transformed. The term refers to a product offered for purchase under a supply contract, but for purposes of calculating the value of the end product includes services (except transportation services) incidental to its supply, provided that the value of those incidental services does not exceed the value of the product itself.</p><p><i>Qualifying country</i> means a country with a reciprocal defense procurement memorandum of understanding or international agreement with the United States in which both countries agree to remove barriers to purchases of supplies produced in the other country or services performed by sources of the other country, and the memorandum or agreement complies, where applicable, with the requirements of section 36 of the Arms Export Control Act (22 U.S.C. 2776) and with 10 U.S.C. 2457. Accordingly, the following are qualifying countries:</p><table><tbody><tr><td>Australia</td><td>&nbsp;&nbsp;&nbsp;Italy</td></tr><tr><td>Austria</td><td>&nbsp;&nbsp;&nbsp;Luxembourg</td></tr><tr><td>Belgium</td><td>&nbsp;&nbsp;&nbsp;Netherlands</td></tr><tr><td>Canada</td><td>&nbsp;&nbsp;&nbsp;Norway</td></tr><tr><td>Czech Republic</td><td>&nbsp;&nbsp;&nbsp;Poland</td></tr><tr><td>Denmark</td><td>&nbsp;&nbsp;&nbsp;Portugal</td></tr><tr><td>Egypt</td><td>&nbsp;&nbsp;&nbsp;Spain</td></tr><tr><td>Finland</td><td>&nbsp;&nbsp;&nbsp;Sweden</td></tr><tr><td>France</td><td>&nbsp;&nbsp;&nbsp;Switzerland</td></tr><tr><td>Germany</td><td>&nbsp;&nbsp;&nbsp;Turkey</td></tr><tr><td>Greece</td><td>&nbsp;&nbsp;&nbsp;United Kingdom of Great Britain and Northern Ireland</td></tr><tr><td>Israel</td><td>&nbsp;</td></tr></tbody></table><p><i>Qualifying country component</i> means a component mined, produced, or manufactured in a qualifying country.</p><p><i>Qualifying country end product</i> means-</p><p>(i) An unmanufactured end product mined or produced in a qualifying country; or</p><p>(ii) An end product manufactured in a qualifying country if-</p><p>(A) The cost of the following types of components exceeds 50 percent of the cost of all its components:</p><p>(<i>1</i>) Components mined, produced, or manufactured in a qualifying country.</p><p>(<i>2</i>) Components mined, produced, or manufactured in the United States.</p><p>(<i>3</i>) Components of foreign origin of a class or kind for which the Government has determined that sufficient and reasonably available commercial quantities of a satisfactory quality are not mined, produced, or manufactured in the United States; or</p><p>(B) The end product is a COTS item.</p><p><i>United States</i> means the 50 States, the District of Columbia, and outlying areas.</p><p>(b) Unless otherwise specified, this clause applies to all items in the Schedule. </p><p>(c) The Contractor shall deliver under this contract only domestic end products unless, in its offer, it specified delivery of qualifying country end products, Free Trade Agreement country end products other than Bahrainian end products, Moroccan end products, Panamanian end products, or Peruvian end products, or other foreign end products in the Buy American-Free Trade Agreements-Balance of Payments Program Certificate-Basic provision of the solicitation. If the Contractor certified in its offer that it will deliver a qualifying country end product or a Free Trade Agreement country end product other than a Bahrainian end product, a Moroccan end product, a Panamanian end product, or a Peruvian end product, the Contractor shall deliver a qualifying country end product, a Free Trade Agreement country end product other than a Bahrainian end product, a Moroccan end product, a Panamanian end product, or a Peruvian end product, or, at the Contractor''s option, a domestic end product.</p>'
WHERE clause_version_id = 7 AND clause_name = '252.225-7036';

UPDATE Clauses
SET clause_data = '<p>(a) <i>Definitions.</i> As used in this clause-</p><p><i>Data Universal Numbering System (DUNS) number</i> means the 9-digit number assigned by Dun and Bradstreet, Inc. (D&B) to identify unique business entities, which is used as the identification number for Federal Contractors.</p><p><i>Data Universal Numbering System + 4 (DUNS + 4) number</i> means the DUNS number assigned by D&B plus a 4-character suffix that may be assigned by a business concern. (D&B has no affiliation with this 4-character suffix.) This 4-character suffix may be assigned at the discretion of the business concern to establish additional SAM records for identifying alternative Electronic Funds Transfer (EFT) accounts (see the FAR at subpart 32.11) for the same concern.</p><p><i>Registered in the System for Award Management (SAM) database</i> means that-</p><p>(1) The Contractor has entered all mandatory information, including the DUNS number or the DUNS + 4 number, the Contractor and Government Entity (CAGE) code, as well as data required by the Federal Funding Accountability and Transparency Act of 2006 (see subpart 4.14), into the SAM database;</p><p>(2) The Contractor has completed the Core, Assertions, Representations and Certifications, and Points of Contact sections of the registration in the SAM database;</p><p>(3) The Government has validated all mandatory data fields, to include validation of the Taxpayer Identification Number (TIN) with the Internal Revenue Service (IRS). The Contractor will be required to provide consent for TIN validation to the Government as a part of the SAM registration process; and</p><p>(4) The Government has marked the record ''Active''.</p><p><i>System for Award Management (SAM)</i> means the primary Government repository for prospective Federal awardee and Federal awardee information and the centralized Government system for certain contracting, grants, and other assistance-related processes. It includes-</p><p>(1) Data collected from prospective Federal awardees required for the conduct of business with the Government;</p><p>(2) Prospective contractor-submitted annual representations and certifications in accordance with FAR subpart 4.12; and</p><p>(3) Identification of those parties excluded from receiving Federal contracts, certain subcontracts, and certain types of Federal financial and non-financial assistance and benefits.</p><p>(b) The Contractor is responsible for the accuracy and completeness of the data within the SAM database, and for any liability resulting from the Government''s reliance on inaccurate or incomplete data. To remain registered in the SAM database after the initial registration, the Contractor is required to review and update on an annual basis, from the date of initial registration or subsequent updates, its information in the SAM database to ensure it is current, accurate and complete. Updating information in the SAM does not alter the terms and conditions of this contract and is not a substitute for a properly executed contractual document.</p><p>(c)(1)(i) If a Contractor has legally changed its business name, <i>doing business as</i> name, or division name (whichever is shown on the contract), or has transferred the assets used in performing the contract, but has not completed the necessary requirements regarding novation and change-of-name agreements in subpart 42.12, the Contractor shall provide the responsible Contracting Officer a minimum of one business day''s written notification of its intention to-</p><p>(A) Change the name in the SAM database;</p><p>(B) Comply with the requirements of subpart 42.12 of the FAR; and</p><p>(C) Agree in writing to the timeline and procedures specified by the responsible Contracting Officer. The Contractor shall provide with the notification sufficient documentation to support the legally changed name.</p><p>(ii) If the Contractor fails to comply with the requirements of paragraph (c)(1)(i) of this clause, or fails to perform the agreement at paragraph (c)(1)(i)(C) of this clause, and, in the absence of a properly executed novation or change-of-name agreement, the SAM information that shows the Contractor to be other than the Contractor indicated in the contract will be considered to be incorrect information within the meaning of the ''Suspension of Payment'' paragraph of the electronic funds transfer (EFT) clause of this contract.</p><p>(2) The Contractor shall not change the name or address for EFT payments or manual payments, as appropriate, in the SAM record to reflect an assignee for the purpose of assignment of claims (see FAR subpart 32.8, Assignment of Claims). Assignees shall be separately registered in the SAM. Information provided to the Contractor''s SAM record that indicates payments, including those made by EFT, to an ultimate recipient other than that Contractor will be considered to be incorrect information within the meaning of the ''Suspension of Payment'' paragraph of the EFT clause of this contract.</p><p>(3) The Contractor shall ensure that the DUNS number is maintained with Dun & Bradstreet throughout the life of the contract. The Contractor shall communicate any change to the DUNS number to the Contracting Officer within 30 days after the change, so an appropriate modification can be issued to update the data on the contract. A change in the DUNS number does not necessarily require a novation be accomplished. Dun & Bradstreet may be contacted-</p><p>(i) Via the internet at <i>http://fedgov.dnb.com/webform</i> or if the Contractor does not have internet access, it may call Dun and Bradstreet at 1-866-705-5711 if located within the United States; or</p><p>(ii) If located outside the United States, by contacting the local Dun and Bradstreet office.</p><p>(d) Contractors may obtain additional information on registration and annual confirmation requirements at <i>https://www.acquisition.gov.</i></p>'
--               '<p>(a) <i>Definitions.</i> As used in this clause-</p><p><i>Data Universal Numbering System (DUNS) number</i> means the 9-digit number assigned by Dun and Bradstreet, Inc. (D&B) to identify unique business entities, which is used as the identification number for Federal Contractors.</p><p><i>Data Universal Numbering System+4 (DUNS+4) number</i> means the DUNS number assigned by D&B plus a 4-character suffix that may be assigned by a business concern. (D&B has no affiliation with this 4-character suffix.) This 4-character suffix may be assigned at the discretion of the business concern to establish additional SAM records for identifying alternative Electronic Funds Transfer (EFT) accounts (see the FAR at subpart 32.11) for the same concern.</p><p><i>Registered in the System for Award Management (SAM) database</i> means that-</p><p>(1) The Contractor has entered all mandatory information, including the DUNS number or the DUNS+4 number, the Contractor and Government Entity (CAGE) code, as well as data required by the Federal Funding Accountability and Transparency Act of 2006 (see subpart 4.14), into the SAM database;</p><p>(2) The Contractor has completed the Core, Assertions, Representations and Certifications, and Points of Contact sections of the registration in the SAM database;</p><p>(3) The Government has validated all mandatory data fields, to include validation of the Taxpayer Identification Number (TIN) with the Internal Revenue Service (IRS). The Contractor will be required to provide consent for TIN validation to the Government as a part of the SAM registration process; and</p><p>(4) The Government has marked the record ''Active''.</p><p><i>System for Award Management (SAM)</i> means the primary Government repository for prospective Federal awardee and Federal awardee information and the centralized Government system for certain contracting, grants, and other assistance-related processes. It includes-</p><p>(1) Data collected from prospective Federal awardees required for the conduct of business with the Government;</p><p>(2) Prospective contractor-submitted annual representations and certifications in accordance with FAR subpart 4.12; and</p><p>(3) Identification of those parties excluded from receiving Federal contracts, certain subcontracts, and certain types of Federal financial and non-financial assistance and benefits.</p><p>(b) The Contractor is responsible for the accuracy and completeness of the data within the SAM database, and for any liability resulting from the Government''s reliance on inaccurate or incomplete data. To remain registered in the SAM database after the initial registration, the Contractor is required to review and update on an annual basis, from the date of initial registration or subsequent updates, its information in the SAM database to ensure it is current, accurate and complete. Updating information in the SAM does not alter the terms and conditions of this contract and is not a substitute for a properly executed contractual document.</p><p>(c)(1)(i) If a Contractor has legally changed its business name, <i>doing business as</i> name, or division name (whichever is shown on the contract), or has transferred the assets used in performing the contract, but has not completed the necessary requirements regarding novation and change-of-name agreements in subpart 42.12, the Contractor shall provide the responsible Contracting Officer a minimum of one business day''s written notification of its intention to-</p><p>(A) Change the name in the SAM database;</p><p>(B) Comply with the requirements of subpart 42.12 of the FAR; and</p><p>(C) Agree in writing to the timeline and procedures specified by the responsible Contracting Officer. The Contractor shall provide with the notification sufficient documentation to support the legally changed name.</p><p>(ii) If the Contractor fails to comply with the requirements of paragraph (c)(1)(i) of this clause, or fails to perform the agreement at paragraph (c)(1)(i)(C) of this clause, and, in the absence of a properly executed novation or change-of-name agreement, the SAM information that shows the Contractor to be other than the Contractor indicated in the contract will be considered to be incorrect information within the meaning of the ''Suspension of Payment'' paragraph of the electronic funds transfer (EFT) clause of this contract.</p><p>(2) The Contractor shall not change the name or address for EFT payments or manual payments, as appropriate, in the SAM record to reflect an assignee for the purpose of assignment of claims (see FAR subpart 32.8, Assignment of Claims). Assignees shall be separately registered in the SAM. Information provided to the Contractor''s SAM record that indicates payments, including those made by EFT, to an ultimate recipient other than that Contractor will be considered to be incorrect information within the meaning of the ''Suspension of Payment'' paragraph of the EFT clause of this contract.</p><p>(3) The Contractor shall ensure that the DUNS number is maintained with Dun & Bradstreet throughout the life of the contract. The Contractor shall communicate any change to the DUNS number to the Contracting Officer within 30 days after the change, so an appropriate modification can be issued to update the data on the contract. A change in the DUNS number does not necessarily require a novation be accomplished. Dun & Bradstreet may be contacted-</p><p>(i) Via the internet at <i>http://fedgov.dnb.com/webform</i> or if the Contractor does not have internet access, it may call Dun and Bradstreet at 1-866-705-5711 if located within the United States; or</p><p>(ii) If located outside the United States, by contacting the local Dun and Bradstreet office.</p><p>(d) Contractors may obtain additional information on registration and annual confirmation requirements at <i>https://www.acquisition.gov.</i></p>'
WHERE clause_version_id = 7 AND clause_name = '52.204-13';

UPDATE Clauses
SET clause_rule = '(A OR B) AND C' -- 'A OR B AND C'
WHERE clause_version_id = 7 AND clause_name = '52.204-19';

UPDATE Clauses
SET clause_data = '<p>(a) <i>Definition. Data Universal Numbering System (DUNS) number,</i> as used in this provision, means the 9-digit number assigned by Dun and Bradstreet, Inc. (D&B) to identify unique business entities, which is used as the identification number for Federal Contractors.</p><p>(b) The offeror shall enter, in the block with its name and address on the cover page of its offer, the annotation ''DUNS'' or ''DUNS + 4'' followed by the DUNS number or ''DUNS + 4'' that identifies the offeror''s name and address exactly as stated in the offer. The DUNS number is a nine-digit number assigned by Dun and Bradstreet, Inc. The DUNS + 4 is the DUNS number plus a 4-character suffix that may be assigned at the discretion of the offeror to establish additional System for Award Management records for identifying alternative Electronic Funds Transfer (EFT) accounts (see Subpart 32.11) for the same concern. </p><p>(c) If the offeror does not have a DUNS number, it should contact Dun and Bradstreet directly to obtain one. </p><p>(1) An offeror may obtain a DUNS number-</p><p>(i) Via the Internet at <i>http://fedgov.dnb.com/webform</i> or if the offeror does not have internet access, it may call Dun and Bradstreet at 1-866-705-5711 if located within the United States; or</p><p>(ii) If located outside the United States, by contacting the local Dun and Bradstreet office. The offeror should indicate that it is an offeror for a U.S. Government contract when contacting the local Dun and Bradstreet office.</p><p>(2) The offeror should be prepared to provide the following information: </p><p>(i) Company legal business name. </p><p>(ii) Tradestyle, doing business, or other name by which your entity is commonly recognized. </p><p>(iii) Company physical street address, city, state and Zip Code. </p><p>(iv) Company mailing address, city, state and Zip Code (if separate from physical). </p><p>(v) Company telephone number. </p><p>(vi) Date the company was started. </p><p>(vii) Number of employees at your location. </p><p>(viii) Chief executive officer/key manager. </p><p>(ix) Line of business (industry). </p><p>(x) Company Headquarters name and address (reporting relationship within your entity).</p>'
--               '<p>(a) <i>Definition. Data Universal Numbering System (DUNS) number,</i> as used in this provision, means the 9-digit number assigned by Dun and Bradstreet, Inc. (D&B) to identify unique business entities, which is used as the identification number for Federal Contractors.</p><p>(b) The offeror shall enter, in the block with its name and address on the cover page of its offer, the annotation ''DUNS'' or ''DUNS+4'' followed by the DUNS number or ''DUNS+4'' that identifies the offeror''s name and address exactly as stated in the offer. The DUNS number is a nine-digit number assigned by Dun and Bradstreet, Inc. The DUNS+4 is the DUNS number plus a 4-character suffix that may be assigned at the discretion of the offeror to establish additional System for Award Management records for identifying alternative Electronic Funds Transfer (EFT) accounts (see Subpart 32.11) for the same concern. </p><p>(c) If the offeror does not have a DUNS number, it should contact Dun and Bradstreet directly to obtain one. </p><p>(1) An offeror may obtain a DUNS number-</p><p>(i) Via the Internet at <i>http://fedgov.dnb.com/webform</i> or if the offeror does not have internet access, it may call Dun and Bradstreet at 1-866-705-5711 if located within the United States; or</p><p>(ii) If located outside the United States, by contacting the local Dun and Bradstreet office. The offeror should indicate that it is an offeror for a U.S. Government contract when contacting the local Dun and Bradstreet office.</p><p>(2) The offeror should be prepared to provide the following information: </p><p>(i) Company legal business name. </p><p>(ii) Tradestyle, doing business, or other name by which your entity is commonly recognized. </p><p>(iii) Company physical street address, city, state and Zip Code. </p><p>(iv) Company mailing address, city, state and Zip Code (if separate from physical). </p><p>(v) Company telephone number. </p><p>(vi) Date the company was started. </p><p>(vii) Number of employees at your location. </p><p>(viii) Chief executive officer/key manager. </p><p>(ix) Line of business (industry). </p><p>(x) Company Headquarters name and address (reporting relationship within your entity).</p>'
WHERE clause_version_id = 7 AND clause_name = '52.204-6';

UPDATE Clauses
SET clause_data = '<p>(a) <i>Definitions.</i> As used in this provision-</p><p><i>Data Universal Numbering System (DUNS) number</i> means the 9-digit number assigned by Dun and Bradstreet, Inc. (D&B) to identify unique business entities. </p><p><i>Data Universal Numbering System + 4 (DUNS + 4) number</i> means the DUNS number assigned by D&B plus a 4-character suffix that may be assigned by a business concern. (D&B has no affiliation with this 4-character suffix.) This 4-character suffix may be assigned at the discretion of the business concern to establish additional System for Award Management records for identifying alternative Electronic Funds Transfer (EFT) accounts (see the FAR at Subpart 32.11) for the same concern. </p><p><i>Registered in the System for Award Management (SAM) database</i> means that-</p><p>(1) The offeror has entered all mandatory information, including the DUNS number or the DUNS + 4 number, the Contractor and Government Entity (CAGE) code, as well as data required by the Federal Funding Accountability and Transparency Act of 2006 (see Subpart 4.14) into the SAM database;</p><p>(2) The offeror has completed the Core, Assertions, and Representations and Certifications, and Points of Contact sections of the registration in the SAM database;</p><p>(3) The Government has validated all mandatory data fields, to include validation of the Taxpayer Identification Number (TIN) with the Internal Revenue Service (IRS). The offeror will be required to provide consent for TIN validation to the Government as a part of the SAM registration process; and</p><p>(4) The Government has marked the record ''Active''.</p><p>(b)(1) By submission of an offer, the offeror acknowledges the requirement that a prospective awardee shall be registered in the SAM database prior to award, during performance, and through final payment of any contract, basic agreement, basic ordering agreement, or blanket purchasing agreement resulting from this solicitation. </p><p>(2) The offeror shall enter, in the block with its name and address on the cover page of its offer, the annotation ''DUNS'' or ''DUNS + 4'' followed by the DUNS or DUNS + 4 number that identifies the offeror''s name and address exactly as stated in the offer. The DUNS number will be used by the Contracting Officer to verify that the offeror is registered in the SAM database. </p><p>(c) If the offeror does not have a DUNS number, it should contact Dun and Bradstreet directly to obtain one. </p><p>(1) An offeror may obtain a DUNS number- </p><p>(i) Via the Internet at <i>http://fedgov.dnb.comwebform</i> or if the offeror does not have internet access, it may call Dun and Bradstreet at 1-866-705-5711 if located within the United States; or</p><p>(ii) If located outside the United States, by contacting the local Dun and Bradstreet office. The offeror should indicate that it is an offeror for a U.S. Government contract when contacting the local Dun and Bradstreet office.</p><p>(2) The offeror should be prepared to provide the following information: </p><p>(i) Company legal business. </p><p>(ii) Tradestyle, doing business, or other name by which your entity is commonly recognized. </p><p>(iii) Company Physical Street Address, City, State, and Zip Code. </p><p>(iv) Company Mailing Address, City, State and Zip Code (if separate from physical). </p><p>(v) Company Telephone Number. </p><p>(vi) Date the company was started. </p><p>(vii) Number of employees at your location. </p><p>(viii) Chief executive officer/key manager. </p><p>(ix) Line of business (industry). </p><p>(x) Company Headquarters name and address (reporting relationship within your entity). </p><p>(d) If the Offeror does not become registered in the SAM database in the time prescribed by the Contracting Officer, the Contracting Officer will proceed to award to the next otherwise successful registered Offeror. </p><p>(e) Processing time, which normally takes 48 hours, should be taken into consideration when registering. Offerors who are not registered should consider applying for registration immediately upon receipt of this solicitation. </p><p>(f) Offerors may obtain information on registration at <i>https://www.acquisition.gov.</i></p>'
--               '<p>(a) <i>Definitions.</i> As used in this provision-</p><p><i>Data Universal Numbering System (DUNS) number</i> means the 9-digit number assigned by Dun and Bradstreet, Inc. (D&B) to identify unique business entities. </p><p><i>Data Universal Numbering System +4 (DUNS+4) number</i> means the DUNS number assigned by D&B plus a 4-character suffix that may be assigned by a business concern. (D&B has no affiliation with this 4-character suffix.) This 4-character suffix may be assigned at the discretion of the business concern to establish additional System for Award Management records for identifying alternative Electronic Funds Transfer (EFT) accounts (see the FAR at Subpart 32.11) for the same concern. </p><p><i>Registered in the System for Award Management (SAM) database</i> means that-</p><p>(1) The offeror has entered all mandatory information, including the DUNS number or the DUNS+4 number, the Contractor and Government Entity (CAGE) code, as well as data required by the Federal Funding Accountability and Transparency Act of 2006 (see Subpart 4.14) into the SAM database;</p><p>(2) The offeror has completed the Core, Assertions, and Representations and Certifications, and Points of Contact sections of the registration in the SAM database;</p><p>(3) The Government has validated all mandatory data fields, to include validation of the Taxpayer Identification Number (TIN) with the Internal Revenue Service (IRS). The offeror will be required to provide consent for TIN validation to the Government as a part of the SAM registration process; and</p><p>(4) The Government has marked the record ''Active''.</p><p>(b)(1) By submission of an offer, the offeror acknowledges the requirement that a prospective awardee shall be registered in the SAM database prior to award, during performance, and through final payment of any contract, basic agreement, basic ordering agreement, or blanket purchasing agreement resulting from this solicitation. </p><p>(2) The offeror shall enter, in the block with its name and address on the cover page of its offer, the annotation ''DUNS'' or ''DUNS +4'' followed by the DUNS or DUNS +4 number that identifies the offeror''s name and address exactly as stated in the offer. The DUNS number will be used by the Contracting Officer to verify that the offeror is registered in the SAM database. </p><p>(c) If the offeror does not have a DUNS number, it should contact Dun and Bradstreet directly to obtain one. </p><p>(1) An offeror may obtain a DUNS number- </p><p>(i) Via the Internet at <i>http://fedgov.dnb.comwebform</i> or if the offeror does not have internet access, it may call Dun and Bradstreet at 1-866-705-5711 if located within the United States; or</p><p>(ii) If located outside the United States, by contacting the local Dun and Bradstreet office. The offeror should indicate that it is an offeror for a U.S. Government contract when contacting the local Dun and Bradstreet office.</p><p>(2) The offeror should be prepared to provide the following information: </p><p>(i) Company legal business. </p><p>(ii) Tradestyle, doing business, or other name by which your entity is commonly recognized. </p><p>(iii) Company Physical Street Address, City, State, and Zip Code. </p><p>(iv) Company Mailing Address, City, State and Zip Code (if separate from physical). </p><p>(v) Company Telephone Number. </p><p>(vi) Date the company was started. </p><p>(vii) Number of employees at your location. </p><p>(viii) Chief executive officer/key manager. </p><p>(ix) Line of business (industry). </p><p>(x) Company Headquarters name and address (reporting relationship within your entity). </p><p>(d) If the Offeror does not become registered in the SAM database in the time prescribed by the Contracting Officer, the Contracting Officer will proceed to award to the next otherwise successful registered Offeror. </p><p>(e) Processing time, which normally takes 48 hours, should be taken into consideration when registering. Offerors who are not registered should consider applying for registration immediately upon receipt of this solicitation. </p><p>(f) Offerors may obtain information on registration at <i>https://www.acquisition.gov.</i></p>'
WHERE clause_version_id = 7 AND clause_name = '52.204-7';



UPDATE Clause_Fill_Ins SET fill_in_default_data = '<p>(a) <i>North American Industry Classification System (NAICS) code and small business size standard.</i> The NAICS code and small business size standard for this acquisition appear in Block 10 of the solicitation cover sheet (SF 1449). However, the small business size standard for a concern which submits an offer in its own name, but which proposes to furnish an item which it did not itself manufacture, is 500 employees.</p><p>(b) <i>Submission of offers.</i> Submit signed and dated offers to the office specified in this solicitation at or before the exact time specified in this solicitation. Offers may be submitted on the SF 1449, letterhead stationery, or as otherwise specified in the solicitation. As a minimum, offers must show&mdash;</p><p>(1) The solicitation number;</p><p>(2) The time specified in the solicitation for receipt of offers;</p><p>(3) The name, address, and telephone number of the offeror;</p><p>(4) 52.204-10, Reporting Executive Compensation and First-Tier Subcontract Awards (FEB 2012) (Pub. L. 109-282) (31 U.S.C. 6101 note).</p><p>(5) Terms of any express warranty;</p><p>(6) Price and any discount terms;</p><p>(7) &ldquo;Remit to&rdquor; address, if different than mailing address;</p><p>(8) A completed copy of the representations and certifications at FAR 52.212-3 (see FAR 52.212-3(b) for those representations and certifications that the offeror shall complete electronically);</p><p>(9) Acknowledgment of Solicitation Amendments;</p><p>(10) Past performance information, when included as an evaluation factor, to include recent and relevant contracts for the same or similar items and other references (including contract numbers, points of contact with telephone numbers and other relevant information); and</p><p>(11) If the offer is not submitted on the SF 1449, include a statement specifying the extent of agreement with all terms, conditions, and provisions included in the solicitation. Offers that fail to furnish required representations or information, or reject the terms and conditions of the solicitation may be excluded from consideration.</p><p>(c) <i>Period for acceptance of offers.</i> The offeror agrees to hold the prices in its offer firm for 30 calendar days from the date specified for receipt of offers, unless another time period is specified in an addendum to the solicitation.</p><p>(d) <i>Product samples.</i> When required by the solicitation, product samples shall be submitted at or prior to the time specified for receipt of offers. Unless otherwise specified in this solicitation, these samples shall be submitted at no expense to the Government, and returned at the sender''s request and expense, unless they are destroyed during preaward testing.</p><p>(e) <i>Multiple offers.</i> Offerors are encouraged to submit multiple offers presenting alternative terms and conditions or commercial items for satisfying the requirements of this solicitation. Each offer submitted will be evaluated separately.</p><p>(f) <i>Late submissions, modifications, revisions, and withdrawals of offers.</i> (1) Offerors are responsible for submitting offers, and any modifications, revisions, or withdrawals, so as to reach the Government office designated in the solicitation by the time specified in the solicitation. If no time is specified in the solicitation, the time for receipt is 4:30 p.m., local time, for the designated Government office on the date that offers or revisions are due.</p><p>(2)(i) Any offer, modification, revision, or withdrawal of an offer received at the Government office designated in the solicitation after the exact time specified for receipt of offers is &ldquo;late&rdquor; and will not be considered unless it is received before award is made, the Contracting Officer determines that accepting the late offer would not unduly delay the acquisition; and&mdash;</p><p>(A) If it was transmitted through an electronic commerce method authorized by the solicitation, it was received at the initial point of entry to the Government infrastructure not later than 5:00 p.m. one working day prior to the date specified for receipt of offers; or</p><p>(B) There is acceptable evidence to establish that it was received at the Government installation designated for receipt of offers and was under the Government''s control prior to the time set for receipt of offers; or</p><p>(C) If this solicitation is a request for proposals, it was the only proposal received.</p><p>(ii) However, a late modification of an otherwise successful offer, that makes its terms more favorable to the Government, will be considered at any time it is received and may be accepted.</p><p>(3) Acceptable evidence to establish the time of receipt at the Government installation includes the time/date stamp of that installation on the offer wrapper, other documentary evidence of receipt maintained by the installation, or oral testimony or statements of Government personnel.</p><p>(4) If an emergency or unanticipated event interrupts normal Government processes so that offers cannot be received at the Government office designated for receipt of offers by the exact time specified in the solicitation, and urgent Government requirements preclude amendment of the solicitation or other notice of an extension of the closing date, the time specified for receipt of offers will be deemed to be extended to the same time of day specified in the solicitation on the first work day on which normal Government processes resume.</p><p>(5) Offers may be withdrawn by written notice received at any time before the exact time set for receipt of offers. Oral offers in response to oral solicitations may be withdrawn orally. If the solicitation authorizes facsimile offers, offers may be withdrawn via facsimile received at any time before the exact time set for receipt of offers, subject to the conditions specified in the solicitation concerning facsimile offers. An offer may be withdrawn in person by an offeror or its authorized representative if, before the exact time set for receipt of offers, the identity of the person requesting withdrawal is established and the person signs a receipt for the offer.</p><p>(g) <i>Contract award (not applicable to Invitation for Bids).</i> The Government intends to evaluate offers and award a contract without discussions with offerors. Therefore, the offeror''s initial offer should contain the offeror''s best terms from a price and technical standpoint. However, the Government reserves the right to conduct discussions if later determined by the Contracting Officer to be necessary. The Government may reject any or all offers if such action is in the public interest; accept other than the lowest offer; and waive informalities and minor irregularities in offers received.</p><p>(h) <i>Multiple awards.</i> The Government may accept any item or group of items of an offer, unless the offeror qualifies the offer by specific limitations. Unless otherwise provided in the Schedule, offers may not be submitted for quantities less than those specified. The Government reserves the right to make an award on any item for a quantity less than the quantity offered, at the unit prices offered, unless the offeror specifies otherwise in the offer.</p><p>(i) <i>Availability of requirements documents cited in the solicitation.</i> (1)(i) The GSA Index of Federal Specifications, Standards and Commercial Item Descriptions, FPMR Part 101-29, and copies of specifications, standards, and commercial item descriptions cited in this solicitation may be obtained for a fee by submitting a request to&mdash;GSA Federal Supply Service Specifications Section, Suite 8100, 470 East L''Enfant Plaza, SW, Washington, DC 20407, Telephone (202) 619-8925, Facsimile (202) 619-8978.</p><p>(ii) If the General Services Administration, Department of Agriculture, or Department of Veterans Affairs issued this solicitation, a single copy of specifications, standards, and commercial item descriptions cited in this solicitation may be obtained free of charge by submitting a request to the addressee in paragraph (i)(1)(i) of this provision. Additional copies will be issued for a fee.</p><p>(2) Most unclassified Defense specifications and standards may be downloaded from the following ASSIST websites:</p><p>(i) ASSIST (<i>https://assist.dla.mil/online/start/</i>).</p><p>(ii) Quick Search (<i>http://quicksearch.dla.mil/</i>).</p><p>(iii) ASSISTdocs.com (<i>http://assistdocs.com</i>).</p><p>(3) Documents not available from ASSIST may be ordered from the Department of Defense Single Stock Point (DoDSSP) by&mdash;</p><p>(i) Using the ASSIST Shopping Wizard (<i>https://assist.dla.mil/wizard/index.cfm</i>);</p><p>(ii) Phoning the DoDSSP Customer Service Desk (215) 697-2179, Mon-Fri, 0730 to 1600 EST; or</p><p>(iii) Ordering from DoDSSP, Building 4, Section D, 700 Robbins Avenue, Philadelphia, PA 19111-5094, Telephone (215) 697-2667/2179, Facsimile (215) 697-1462.</p><p>(4) Nongovernment (voluntary) standards must be obtained from the organization responsible for their preparation, publication, or maintenance.</p><p>(j) <i>Data Universal Numbering System (DUNS) Number.</i> (Applies to all offers exceeding $3,000, and offers of $3,000 or less if the solicitation requires the Contractor to be registered in the System for Award Management (SAM) database. The offeror shall enter, in the block with its name and address on the cover page of its offer, the annotation &ldquo;DUNS&rdquor; or &ldquo;DUNS + 4&rdquor; followed by the DUNS or DUNS + 4 number that identifies the offeror''s name and address. The DUNS + 4 is the DUNS number plus a 4-character suffix that may be assigned at the discretion of the offeror to establish additional SAM records for identifying alternative Electronic Funds Transfer (EFT) accounts (see FAR Subpart 32.11) for the same concern. If the offeror does not have a DUNS number, it should contact Dun and Bradstreet directly to obtain one. An offeror within the United States may contact Dun and Bradstreet by calling 1-866-705-5711 or via the internet at <i>http://fedgov.dnb.com/webform.</i> An offeror located outside the United States must contact the local Dun and Bradstreet office for a DUNS number. The offeror should indicate that it is an offeror for a Government contract when contacting the local Dun and Bradstreet office.</p><p>(k) <i>System for Award Management.</i> Unless exempted by an addendum to this solicitation, by submission of an offer, the offeror acknowledges the requirement that a prospective awardee shall be registered in the SAM database prior to award, during performance and through final payment of any contract resulting from this solicitation. If the Offeror does not become registered in the SAM database in the time prescribed by the Contracting Officer, the Contracting Officer will proceed to award to the next otherwise successful registered Offeror. Offerors may obtain information on registration and annual confirmation requirements via the SAM database accessed through <i>https://www.acquisition.gov.</i></p><p>(l) <i>Debriefing.</i> If a post-award debriefing is given to requesting offerors, the Government shall disclose the following information, if applicable:</p><p>(1) The agency''s evaluation of the significant weak or deficient factors in the debriefed offeror''s offer.</p><p>(2) The overall evaluated cost or price and technical rating of the successful and the debriefed offeror and past performance information on the debriefed offeror.</p><p>(3) The overall ranking of all offerors, when any ranking was developed by the agency during source selection.</p><p>(4) A summary of the rationale for award;</p><p>(5) For acquisitions of commercial items, the make and model of the item to be delivered by the successful offeror.</p><p>(6) Reasonable responses to relevant questions posed by the debriefed offeror as to whether source-selection procedures set forth in the solicitation, applicable regulations, and other applicable authorities were followed by the agency.</p>' WHERE clause_fill_in_id = 50405;



UPDATE Clause_Fill_Ins SET fill_in_default_data = '<p>(a) The Contractor shall comply with the following Federal Acquisition Regulation (FAR) clauses that are incorporated by reference:</p><p>(1) The clauses listed below implement provisions of law or Executive order:</p><p>(i) 52.222-3, Convict Labor (JUN 2003) (E.O. 11755).</p><p>(ii) 52.222-21, Prohibition of Segregated Facilities (APR 2015).</p><p>(iii) 52.222-26, Equal Opportunity (APR 2015) (E.O. 11246).</p><p>(iv) 52.225-13, Restrictions on Certain Foreign Purchases (JUN 2008) (E.O.s, proclamations, and statutes administered by the Office of Foreign Assets Control of the Department of the Treasury).</p><p>(v) 52.233-3, Protest After Award (AUG 1996) (31 U.S.C. 3553).</p><p>(vi) 52.233-4, Applicable Law for Breach of Contract Claim (OCT 2004) (Pub. L. 108-77, 108-78 (19 U.S.C. 3805 note).</p><p>(2) Listed below are additional clauses that apply:</p><p>(i) 52.232-1, Payments (APR 1984).</p><p>(ii) 52.232-8, Discounts for Prompt Payment (FEB 2002).</p><p>(iii) 52.232-11, Extras (APR 1984).</p><p>(iv) 52.232-25, Prompt Payment (JUL 2013).</p><p>(v) 52.232-39, Unenforceability of Unauthorized Obligations (JUN 2013).</p><p>(vi) 52.232-40, Providing Accelerated Payments to Small Business Subcontractors (DEC 2013).</p><p>(vii) 52.233-1, Disputes (MAY 2014).</p><p>(viii) 52.244-6, Subcontracts for Commercial Items (APR 2015).</p><p>(ix) 52.253-1, Computer Generated Forms (JAN 1991).</p><p>(b) The Contractor shall comply with the following FAR clauses, incorporated by reference, unless the circumstances do not apply:</p><p>(1) The clauses listed below implement provisions of law or Executive order:</p><p>(i) 52.204-10, Reporting Executive Compensation and First-Tier Subcontract Awards (JUL 2013) (Pub. L. 109-282) (31 U.S.C. 6101 note) (Applies to contracts valued at $25,000 or more).</p><p>(ii) 52.222-19, Child Labor&mdash;Cooperation with Authorities and Remedies (JAN 2014) (E.O. 13126). (Applies to contracts for supplies exceeding the micro-purchase threshold).</p><p>(iii) 52.222-20, Contracts for Materials, Supplies, Articles, and Equipment Exceeding $15,000 (MAY 2014) (41 U.S.C. chapter 65) (Applies to supply contracts over $15,000 in the United States, Puerto Rico, or the U.S. Virgin Islands).</p><p>(iv) 52.222-35, Equal Opportunity for Veterans (JUL 2014) (38 U.S.C. 4212) (Applies to contracts of $100,000 or more).</p><p>(v) 52.222-36, Equal Employment for Workers with Disabilities (JUL 2014) (29 U.S.C. 793) (Applies to contracts over $15,000, unless the work is to be performed outside the United States by employees recruited outside the United States). (For purposes of this clause, &ldquo;United States&rdquor; includes the 50 States, the District of Columbia, Puerto Rico, the Northern Mariana Islands, American Samoa, Guam, the U.S. Virgin Islands, and Wake Island.)</p><p>(vi) 52.222-37, Employment Reports on Veterans (JUL 2014) (38 U.S.C. 4212) (Applies to contracts of $100,000 or more).</p><p>(vii) 52.222-41, Service Contract Labor Standards (MAY 2014) (41 U.S.C. chapter 67) (Applies to service contracts over $2,500 that are subject to the Service Contract Labor Standards statute and will be performed in the United States, District of Columbia, Puerto Rico, the Northern Mariana Islands, American Samoa, Guam, the U.S. Virgin Islands, Johnston Island, Wake Island, or the outer Continental Shelf).</p><p>(viii)(A) 52.222-50, Combating Trafficking in Persons (MAR 2015) (22 U.S.C. chapter 78 and E.O 13627) (Applies to all solicitations and contracts).</p><p>(B) Alternate I (MAR 2015) (Applies if the Contracting Officer has filled in the following information with regard to applicable directives or notices: Document title(s), source for obtaining document(s), and contract performance location outside the United States to which the document applies).</p><p>(ix) 52.222-55, Minimum Wages Under Executive Order 13658 (DEC 2014) (Executive Order 13658) (Applies when 52.222-6 or 52.222-41 are in the contract and performance in whole or in part is in the United States (the 50 States and the District of Columbia)).</p><p>(x) 52.223-5, Pollution Prevention and Right-to-Know Information (MAY 2011) (E.O. 13423) (Applies to services performed on Federal facilities).</p><p>(xi) 52.223-15, Energy Efficiency in Energy-Consuming Products (DEC 2007) (42 U.S.C. 8259b) (Unless exempt pursuant to 23.204, applies to contracts when energy-consuming products listed in the ENERGY STAR&reg; Program or Federal Energy Management Program (FEMP) will be&mdash;</p><p>(A) Delivered;</p><p>(B) Acquired by the Contractor for use in performing services at a Federally-controlled facility;</p><p>(C) Furnished by the Contractor for use by the Government; or</p><p>(D) Specified in the design of a building or work, or incorporated during its construction, renovation, or maintenance).</p><p>(xii) 52.225-1, Buy American&mdash;Supplies (MAY 2014) (41 U.S.C. chapter 67) (Applies to contracts for supplies, and to contracts for services involving the furnishing of supplies, for use in the United States or its outlying areas, if the value of the supply contract or supply portion of a service contract exceeds the micro-purchase threshold and the acquisition&mdash;</p><p>(A) Is set aside for small business concerns; or</p><p>(B) Cannot be set aside for small business concerns (see 19.502-2), and does not exceed $25,000).</p><p>(xiii) 52.226-6, Promoting Excess Food Donation to Nonprofit Organizations (MAY 2014) (42 U.S.C. 1792) (Applies to contracts greater than $25,000 that provide for the provision, the service, or the sale of food in the United States).</p><p>(xiv) 52.232-33, Payment by Electronic Funds Transfer&mdash;System for Award Management (JUL 2013) (Applies when the payment will be made by electronic funds transfer (EFT) and the payment office uses the System for Award Management (SAM) database as its source of EFT information.)</p><p>(xv) 52.232-34, Payment by Electronic Funds Transfer&mdash;Other than System for Award Management (JUL 2013) (Applies when the payment will be made by EFT and the payment office does not use the SAM database as its source of EFT information.)</p><p>(xvi) 52.247-64, Preference for Privately Owned U.S.-Flag Commercial Vessels (FEB 2006) (46 U.S.C. App. 1241) (Applies to supplies transported by ocean vessels (except for the types of subcontracts listed at 47.504(d).)</p><p>(2) Listed below are additional clauses that may apply:</p><p>(i) 52.209-6, Protecting the Government''s Interest When Subcontracting with Contractors Debarred, Suspended, or Proposed for Debarment (AUG 2013) (Applies to contracts over $30,000).</p><p>(ii) 52.211-17, Delivery of Excess Quantities (SEP 1989) (Applies to fixed-price supplies).</p><p>(iii) 52.247-29, F.o.b. Origin (FEB 2006) (Applies to supplies if delivery is f.o.b. origin).</p><p>(iv) 52.247-34, F.o.b. Destination (NOV 1991) (Applies to supplies if delivery is f.o.b. destination).</p><p>(c) <i>FAR 52.252-2, Clauses Incorporated by Reference (FEB 1998).</i> This contract incorporates one or more clauses by reference, with the same force and effect as if they were given in full text. Upon request, the Contracting Officer will make their full text available. Also, the full text of a clause may be accessed electronically at this/these address(es):</p>&nbsp; {{textbox_52.213-4[0]}}&nbsp; {{textbox_52.213-4[1]}}<p>[<i>Insert one or more Internet addresses</i>]</p><p>(d) <i>Inspection/Acceptance.</i> The Contractor shall tender for acceptance only those items that conform to the requirements of this contract. The Government reserves the right to inspect or test any supplies or services that have been tendered for acceptance. The Government may require repair or replacement of nonconforming supplies or reperformance of nonconforming services at no increase in contract price. The Government must exercise its postacceptance rights&mdash;</p><p>(1) Within a reasonable period of time after the defect was discovered or should have been discovered; and</p><p>(2) Before any substantial change occurs in the condition of the item, unless the change is due to the defect in the item.</p><p>(e) <i>Excusable delays.</i> The Contractor shall be liable for default unless nonperformance is caused by an occurrence beyond the reasonable control of the Contractor and without its fault or negligence, such as acts of God or the public enemy, acts of the Government in either its sovereign or contractual capacity, fires, floods, epidemics, quarantine restrictions, strikes, unusually severe weather, and delays of common carriers. The Contractor shall notify the Contracting Officer in writing as soon as it is reasonably possible after the commencement of any excusable delay, setting forth the full particulars in connection therewith, shall remedy such occurrence with all reasonable dispatch, and shall promptly give written notice to the Contracting Officer of the cessation of such occurrence.</p><p>(f) <i>Termination for the Government''s convenience.</i> The Government reserves the right to terminate this contract, or any part hereof, for its sole convenience. In the event of such termination, the Contractor shall immediately stop all work hereunder and shall immediately cause any and all of its suppliers and subcontractors to cease work. Subject to the terms of this contract, the Contractor shall be paid a percentage of the contract price reflecting the percentage of the work performed prior to the notice of termination, plus reasonable charges that the Contractor can demonstrate to the satisfaction of the Government, using its standard record keeping system, have resulted from the termination. The Contractor shall not be required to comply with the cost accounting standards or contract cost principles for this purpose. This paragraph does not give the Government any right to audit the Contractor''s records. The Contractor shall not be paid for any work performed or costs incurred that reasonably could have been avoided.</p><p>(g) <i>Termination for cause.</i> The Government may terminate this contract, or any part hereof, for cause in the event of any default by the Contractor, or if the Contractor fails to comply with any contract terms and conditions, or fails to provide the Government, upon request, with adequate assurances of future performance. In the event of termination for cause, the Government shall not be liable to the Contractor for any amount for supplies or services not accepted, and the Contractor shall be liable to the Government for any and all rights and remedies provided by law. If it is determined that the Government improperly terminated this contract for default, such termination shall be deemed a termination for convenience.</p><p>(h) <i>Warranty.</i> The Contractor warrants and implies that the items delivered hereunder are merchantable and fit for use for the particular purpose described in this contract.</p>' WHERE clause_fill_in_id = 50496;

UPDATE Clauses
SET clause_data = '<p>(a) The wage determination issued under the Construction Wage Rate Requirements statute by the Administrator, Wage and Hour Division, Employment Standards Administration, U.S. Department of Labor, that is effective for an option to extend the term of the contract, will apply to that option period.</p><p>(b)(1) The Contractor states that if the prices in this contract contain an allowance for wage or benefit increases, such allowance will not be included in any request for contract price adjustment submitted under this clause.</p><p>(2) The Contractor shall provide with each request for contract price adjustment under this clause a statement that the prices in the contract do not include any allowance for any increased cost for which adjustment is being requested.</p><p>(c) The Contracting Officer will adjust the contract price or contract unit price labor rates to reflect the Contractor''s actual increase or decrease in wages and fringe benefits to the extent that the increase is made to comply with, or the decrease is voluntarily made by the Contractor as a result of-</p><p>(1) Incorporation of the Department of Labor''s Construction Wage Rate Requirements wage determination applicable at the exercise of an option to extend the term of the contract; or</p><p>(2) Incorporation of a Construction Wage Rate Requirements wage determination otherwise applied to the contract by operation of law.</p><p>(d) Any adjustment will be limited to increases or decreases in wages and fringe benefits as described in paragraph (c) of this clause, and the accompanying increases or decreases in social security and unemployment taxes and workers'' compensation insurance, but will not otherwise include any amount for general and administrative costs, overhead, or profit.</p><p>(e) The Contractor shall notify the Contracting Officer of any increase claimed under this clause within 30 days after receiving a revised wage determination unless this notification period is extended in writing by the Contracting Officer. The Contractor shall notify the Contracting Officer promptly of any decrease under this clause, but nothing in this clause precludes the Government from asserting a claim within the period permitted by law. The notice shall contain a statement of the amount claimed and any relevant supporting data, including payroll records that the Contracting Officer may reasonably require. Upon agreement of the parties, the Contracting Officer will modify the contract price or contract unit price in writing. The Contractor shall continue performance pending agreement on or determination of any such adjustment and its effective date.</p><p>(f) Contract price adjustment computations shall be computed as follows:</p><p>(1) <i>Computation for contract unit price per single craft hour for schedule of indefinite-quantity work.</i> For each labor classification, the difference between the actual wage and benefit rates (combined) paid and the wage and benefit rates (combined) required by the new wage determination shall be added to the original contract unit price if the difference results in a combined increase. If the difference computed results in a combined decrease, the contract unit price shall be decreased by that amount if the Contractor provides notification as provided in paragraph (e) of this clause.</p><p>(2) <i>Computation for contract unit price containing multiple craft hours for schedule of indefinite-quantity work.</i> For each labor classification, the difference between the actual wage and benefit rates (combined) paid and the wage and benefit rates (combined) required by the new wage determination shall be multiplied by the actual number of hours expended for each craft involved in accomplishing the unit-priced work item. The product of this computation will then be divided by the actual number of units ordered in the preceding contract period. The total of these computations for each craft will be added to the current contract unit price to obtain the new contract unit price. The extended amount for the contract line item will be obtained by multiplying the new unit price by the estimated quantity. If actual hours are not available from the preceding contract period for computation of the adjustment for a specific contract unit of work, the Contractor, in agreement with the Contracting Officer, shall estimate the total hours per craft per contract unit of work.</p><div><div><p>Example: Asphalt Paving-Current Price $3.38 per Square Yard</p></div><div><table border="1" cellpadding="1" cellspacing="1" frame="void" width="100%"><tbody><tr><th scope="col">DBA craft</th><th scope="col">New WD</th><th scope="col">&nbsp;&nbsp;</th><th scope="col">Hourly rate paid</th><th scope="col">&nbsp;&nbsp;</th><th scope="col">Diff.</th><th scope="col">&nbsp;&nbsp;</th><th scope="col">Actual hrs.</th><th scope="col">Actual units (sq. yard)</th><th scope="col">&nbsp;&nbsp;</th><th scope="col">Increase/sq. yard</th></tr><tr><td align="left" scope="row">Equip. Opr.</td><td align="right">$18.50</td><td align="right">&#x2212;</td><td align="right">$18.00</td><td align="right">=</td><td align="right">$.50</td><td align="right">&times; </td><td align="left">600 hrs./</td><td align="left">3,000 sq. yrd.</td><td align="right">=</td><td align="right">$.10</td></tr><tr><td align="left" scope="row">Truck Driver</td><td align="right">$19.00</td><td align="right">&#x2212;</td><td align="right">$18.25</td><td align="right">=</td><td align="right">$.75</td><td align="right">&times; </td><td align="left">525 hrs./</td><td align="left">3,000 sq. yrd.</td><td align="right">=</td><td align="right">$.13</td></tr><tr><td align="left" scope="row">Laborer</td><td align="right">$11.50</td><td align="right">&#x2212;</td><td align="right">$11.25</td><td align="right">=</td><td align="right">$.25</td><td align="right">&times; </td><td align="left">750 hrs./</td><td align="left">3,000 sq. yrd.</td><td align="right">=</td><td align="right">$.06</td></tr><tr><td align="left" colspan="10" scope="row">Total increase per square yard</td><td align="right">*$.29</td></tr><tr><td align="left" colspan="11" scope="row">*Note: Adjustment for labor rate increases or decreases may be accompanied by social security and unemployment taxes and workers'' compensation insurance.</td></tr><tr><td align="left" scope="row">Current unit price (per square yard)</td><td align="center" colspan="2">$3.38</td><td align="right"></td><td align="right"></td><td align="right"></td><td align="right"></td><td align="right"></td><td align="left"></td><td align="left"></td><td align="right"></td></tr><tr><td align="left" scope="row">Add DBA price adj.</td><td align="right"> + .29</td><td align="right"></td><td align="right"></td><td align="right"></td><td align="right"></td><td align="right"></td><td align="left"></td><td align="left"></td><td align="right"></td><td align="right"></td></tr><tr><td align="left" scope="row">New unit price (per square yard)</td><td align="right">$3.67</td><td align="right"></td><td align="right"></td><td align="right"></td><td align="right"></td><td align="right"></td><td align="left"></td><td align="left"></td><td align="right"></td><td align="right"></td></tr></tbody></table></div></div>'
--               '<p>(a) The wage determination issued under the Construction Wage Rate Requirements statute by the Administrator, Wage and Hour Division, Employment Standards Administration, U.S. Department of Labor, that is effective for an option to extend the term of the contract, will apply to that option period.</p><p>(b)(1) The Contractor states that if the prices in this contract contain an allowance for wage or benefit increases, such allowance will not be included in any request for contract price adjustment submitted under this clause.</p><p>(2) The Contractor shall provide with each request for contract price adjustment under this clause a statement that the prices in the contract do not include any allowance for any increased cost for which adjustment is being requested.</p><p>(c) The Contracting Officer will adjust the contract price or contract unit price labor rates to reflect the Contractor''s actual increase or decrease in wages and fringe benefits to the extent that the increase is made to comply with, or the decrease is voluntarily made by the Contractor as a result of-</p><p>(1) Incorporation of the Department of Labor''s Construction Wage Rate Requirements wage determination applicable at the exercise of an option to extend the term of the contract; or</p><p>(2) Incorporation of a Construction Wage Rate Requirements wage determination otherwise applied to the contract by operation of law.</p><p>(d) Any adjustment will be limited to increases or decreases in wages and fringe benefits as described in paragraph (c) of this clause, and the accompanying increases or decreases in social security and unemployment taxes and workers'' compensation insurance, but will not otherwise include any amount for general and administrative costs, overhead, or profit.</p><p>(e) The Contractor shall notify the Contracting Officer of any increase claimed under this clause within 30 days after receiving a revised wage determination unless this notification period is extended in writing by the Contracting Officer. The Contractor shall notify the Contracting Officer promptly of any decrease under this clause, but nothing in this clause precludes the Government from asserting a claim within the period permitted by law. The notice shall contain a statement of the amount claimed and any relevant supporting data, including payroll records that the Contracting Officer may reasonably require. Upon agreement of the parties, the Contracting Officer will modify the contract price or contract unit price in writing. The Contractor shall continue performance pending agreement on or determination of any such adjustment and its effective date.</p><p>(f) Contract price adjustment computations shall be computed as follows:</p><p>(1) <i>Computation for contract unit price per single craft hour for schedule of indefinite-quantity work.</i> For each labor classification, the difference between the actual wage and benefit rates (combined) paid and the wage and benefit rates (combined) required by the new wage determination shall be added to the original contract unit price if the difference results in a combined increase. If the difference computed results in a combined decrease, the contract unit price shall be decreased by that amount if the Contractor provides notification as provided in paragraph (e) of this clause.</p><p>(2) <i>Computation for contract unit price containing multiple craft hours for schedule of indefinite-quantity work.</i> For each labor classification, the difference between the actual wage and benefit rates (combined) paid and the wage and benefit rates (combined) required by the new wage determination shall be multiplied by the actual number of hours expended for each craft involved in accomplishing the unit-priced work item. The product of this computation will then be divided by the actual number of units ordered in the preceding contract period. The total of these computations for each craft will be added to the current contract unit price to obtain the new contract unit price. The extended amount for the contract line item will be obtained by multiplying the new unit price by the estimated quantity. If actual hours are not available from the preceding contract period for computation of the adjustment for a specific contract unit of work, the Contractor, in agreement with the Contracting Officer, shall estimate the total hours per craft per contract unit of work.</p><div><div><p>Example: Asphalt Paving-Current Price $3.38 per Square Yard</p></div><div><table border="1" cellpadding="1" cellspacing="1" frame="void" width="100%"><tbody><tr><th scope="col">DBA craft</th><th scope="col">New WD</th><th scope="col">&nbsp;&nbsp;</th><th scope="col">Hourly rate paid</th><th scope="col">&nbsp;&nbsp;</th><th scope="col">Diff.</th><th scope="col">&nbsp;&nbsp;</th><th scope="col">Actual hrs.</th><th scope="col">Actual units (sq. yard)</th><th scope="col">&nbsp;&nbsp;</th><th scope="col">Increase/sq. yard</th></tr><tr><td align="left" scope="row">Equip. Opr.</td><td align="right">$18.50</td><td align="right">&#x2212;</td><td align="right">$18.00</td><td align="right">=</td><td align="right">$.50</td><td align="right">&times;</td><td align="left">600 hrs./</td><td align="left">3,000 sq. yrd.</td><td align="right">=</td><td align="right">$.10</td></tr><tr><td align="left" scope="row">Truck Driver</td><td align="right">$19.00</td><td align="right">&#x2212;</td><td align="right">$18.25</td><td align="right">=</td><td align="right">$.75</td><td align="right">&times;</td><td align="left">525 hrs./</td><td align="left">3,000 sq. yrd.</td><td align="right">=</td><td align="right">$.13</td></tr><tr><td align="left" scope="row">Laborer</td><td align="right">$11.50</td><td align="right">&#x2212;</td><td align="right">$11.25</td><td align="right">=</td><td align="right">$.25</td><td align="right">&times;</td><td align="left">750 hrs./</td><td align="left">3,000 sq. yrd.</td><td align="right">=</td><td align="right">$.06</td></tr><tr><td align="left" colspan="10" scope="row">Total increase per square yard</td><td align="right">*$.29</td></tr><tr><td align="left" colspan="11" scope="row">*Note: Adjustment for labor rate increases or decreases may be accompanied by social security and unemployment taxes and workers'' compensation insurance.</td></tr><tr><td align="left" scope="row">Current unit price (per square yard)</td><td align="center" colspan="2">$3.38</td><td align="right"></td><td align="right"></td><td align="right"></td><td align="right"></td><td align="right"></td><td align="left"></td><td align="left"></td><td align="right"></td></tr><tr><td align="left" scope="row">Add DBA price adj.</td><td align="right">+.29</td><td align="right"></td><td align="right"></td><td align="right"></td><td align="right"></td><td align="right"></td><td align="left"></td><td align="left"></td><td align="right"></td><td align="right"></td></tr><tr><td align="left" scope="row">New unit price (per square yard)</td><td align="right">$3.67</td><td align="right"></td><td align="right"></td><td align="right"></td><td align="right"></td><td align="right"></td><td align="left"></td><td align="left"></td><td align="right"></td><td align="right"></td></tr></tbody></table></div></div>'
WHERE clause_version_id = 7 AND clause_name = '52.222-32';

UPDATE Clauses
SET clause_rule = '(A OR B) AND C' -- 'A OR B AND C'
WHERE clause_version_id = 7 AND clause_name = '52.232-39';

UPDATE Clauses
SET clause_rule = '(A OR B) AND C' -- 'A OR B AND C'
WHERE clause_version_id = 7 AND clause_name = '52.247-17';

UPDATE Clauses
SET clause_rule = '(A OR B) AND C' -- 'A OR B AND C'
WHERE clause_version_id = 7 AND clause_name = '52.247-27';

UPDATE Clauses
SET clause_rule = '(A OR B) AND C' -- 'A OR B AND C'
WHERE clause_version_id = 7 AND clause_name = '52.252-2';

UPDATE Clauses
SET clause_rule = '(A OR B) AND C' -- 'A OR B AND C'
WHERE clause_version_id = 7 AND clause_name = '52.252-4';

UPDATE Clauses
SET clause_rule = '(A OR B) AND C' -- 'A OR B AND C'
WHERE clause_version_id = 7 AND clause_name = '52.252-6';

UPDATE Clauses
SET clause_data = '<p>Alterate II (MAR 2011)</p><p>As prescribed in 227.7103-6(b)(2), add the following paragraphs (a)(17) and (b)(7) to the basic clause:</p><p>(a)(17) ''Vessel design'' means the design of a vessel, boat, or craft, and its components, including the hull, decks, superstructure, and the exterior surface shape of all external shipboard equipment and systems. The term includes designs covered by 10 U.S.C. 7317, and designs protectable under 17 U.S.C. 1301, <i>et seq.</i></p><p>(b)(7) <i>Vessel designs.</i> For a vessel design (including a vessel design embodied in a useful article) that is developed or delivered under this contract, the Government shall have the right to make and have made any useful article that embodies the vessel design, to import the article, to sell the article, and to distribute the article for sale or to use the article in trade, to the same extent that the Government is granted rights in the technical data pertaining to the vessel design.</p>'
--               '<p>Alterate II (NOV 2009)</p><p>As prescribed in 227.7103-6(b)(2), add the following paragraphs (a)(17) and (b)(7) to the basic clause:</p><p>(a)(17) ''Vessel design'' means the design of a vessel, boat, or craft, and its components, including the hull, decks, superstructure, and the exterior surface shape of all external shipboard equipment and systems. The term includes designs covered by 10 U.S.C. 7317, and designs protectable under 17 U.S.C. 1301, <i>et seq.</i></p><p>(b)(7) <i>Vessel designs.</i> For a vessel design (including a vessel design embodied in a useful article) that is developed or delivered under this contract, the Government shall have the right to make and have made any useful article that embodies the vessel design, to import the article, to sell the article, and to distribute the article for sale or to use the article in trade, to the same extent that the Government is granted rights in the technical data pertaining to the vessel design.</p>'
WHERE clause_version_id = 7 AND clause_name = '252.227-7013 Alternate II';

UPDATE Clauses
SET clause_data = '<p>Alternate I (SEP 2015). As prescribed in 19.309(a)(2), add the following paragraph (c)(9) to the basic provision:</p><p>(9) [<i>Complete if offeror represented itself as disadvantaged in paragraph (c)(2) of this provision.</i> The offeror shall check the category in which its ownership falls:</p><p>__ Black American.</p><p>__ Hispanic American.</p><p>__ Native American (American Indians, Eskimos, Aleuts, or Native Hawaiians).</p><p>__ Asian-Pacific American (persons with origins from Burma, Thailand, Malaysia, Indonesia, Singapore, Brunei, Japan, China, Taiwan, Laos, Cambodia (Kampuchea), Vietnam, Korea, The Philippines, Republic of Palau, Republic of the Marshall Islands, Federated States of Micronesia, the Commonwealth of the Northern Mariana Islands, Guam, Samoa, Macao, Hong Kong, Fiji, Tonga, Kiribati, Tuvalu, or Nauru).</p><p>__ Subcontinent Asian (Asian-Indian) American (persons with origins from India, Pakistan, Bangladesh, Sri Lanka, Bhutan, the Maldives Islands, or Nepal).</p><p>__ Individual/concern, other than one of the preceding.</p>'
--               '<p>Alternate I (MAY 2014). As prescribed in 19.307(a)(2), add the following paragraph (b)(9) to the basic provision:</p><p>(9) [<i>Complete if offeror represented itself as disadvantaged in paragraph (b)(2) of this provision.</i>] The offeror shall check the category in which its ownership falls:</p><p>__ Black American.</p><p>__ Hispanic American.</p><p>__ Native American (American Indians, Eskimos, Aleuts, or Native Hawaiians).</p><p>__ Asian-Pacific American (persons with origins from Burma, Thailand, Malaysia, Indonesia, Singapore, Brunei, Japan, China, Taiwan, Laos, Cambodia (Kampuchea), Vietnam, Korea, The Philippines, Republic of Palau, Republic of the Marshall Islands, Federated States of Micronesia, the Commonwealth of the Northern Mariana Islands, Guam, Samoa, Macao, Hong Kong, Fiji, Tonga, Kiribati, Tuvalu, or Nauru).</p><p>__ Subcontinent Asian (Asian-Indian) American (persons with origins from India, Pakistan, Bangladesh, Sri Lanka, Bhutan, the Maldives Islands, or Nepal).</p><p>__ Individual/concern, other than one of the preceding.</p>'
WHERE clause_version_id = 7 AND clause_name = '52.219-1 Alternate I';

/*
*** End of SQL Scripts at Mon Sep 28 10:41:06 EDT 2015 ***
*/
