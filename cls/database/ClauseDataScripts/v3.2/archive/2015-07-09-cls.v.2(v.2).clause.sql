/*
Clause Parser Run at Thu Jul 09 10:26:00 EDT 2015
(Without Correction Information)
*/

-- ProduceClause.resolveIssues() Summary: Total(1212); Merged(14); Corrected(339); Error(1)
/* ===============================================
 * Prescriptions
   =============================================== */
/* ===============================================
 * Clause
   =============================================== */

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101391; -- 252.201-7000

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101392; -- 252.203-7000

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101393; -- 252.203-7001

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101394; -- 252.203-7002

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101395; -- 252.203-7003

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101390; -- 252.203-7004

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101396; -- 252.203-7005

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101397; -- 252.203-7998 (DEVIATION 2015-00010)

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101398; -- 252.203-7999 (DEVIATION 2015-00010)

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101399; -- 252.204-0001

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101400; -- 252.204-0002

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101401; -- 252.204-0003

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101402; -- 252.204-0004

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101403; -- 252.204-0005

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101404; -- 252.204-0006

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101405; -- 252.204-0007

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101406; -- 252.204-0008

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101407; -- 252.204-0009

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101408; -- 252.204-0010

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101409; -- 252.204-0011

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101410; -- 252.204-7000

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101411; -- 252.204-7002

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101412; -- 252.204-7003

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101413; -- 252.204-7004

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101414; -- 252.204-7005

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101415; -- 252.204-7006

UPDATE Clauses
SET is_editable = 0
, clause_data = '<p>Substitute the following paragraphs (d) and (e) for paragraph (d) of the provision at FAR 52.204-8:</p><p>(d)(1) The following representations or certifications in the System for Award Management (SAM) database are applicable to this solicitation as indicated:</p><p>(i) 252.209-7003, Reserve Officer Training Corps and Military Recruiting on Campus-Representation. Applies to all solicitations with institutions of higher education.</p><p>(ii) 252.216-7008, Economic Price Adjustment-Wage Rates or Material Prices Controlled by a Foreign Government. Applies to solicitations for fixed-price supply and service contracts when the contract is to be performed wholly or in part in a foreign country, and a foreign government controls wage rates or material prices and may during contract performance impose a mandatory change in wages or prices of materials.</p><p>(iii) 252.222-7007, Representation Regarding Combating Trafficking in Persons, as prescribed in 222.1771. Applies to solicitations with a value expected to exceed the simplified acquisition threshold.</p><p>(iv) 252.225-7042, Authorization to Perform. Applies to all solicitations when performance will be wholly or in part in a foreign country.</p><p>(v) 252.225-7049, Prohibition on Acquisition of Commercial Satellite Services from Certain Foreign Entities-Representations. Applies to solicitations for the acquisition of commercial satellite services.</p><p>(vi) 252.225-7050, Disclosure of Ownership or Control by the Government of a Country that is a State Sponsor of Terrorism. Applies to all solicitations expected to result in contracts of $150,000 or more.</p><p>(vii) 252.229-7012, Tax Exemptions (Italy)-Representation. Applies to solicitations when contract performance will be in Italy.</p><p>(viii) 252.229-7013, Tax Exemptions (Spain)-Representation. Applies to solicitations when contract performance will be in Spain.</p><p>(ix) 252.247-7022, Representation of Extent of Transportation by Sea. Applies to all solicitations except those for direct purchase of ocean transportation services or those with an anticipated value at or below the simplified acquisition threshold.</p><p>(2) The following representations or certifications in SAM are applicable to this solicitation as indicated by the Contracting Officer: [<i>Contracting Officer check as appropriate.</i>]</p><p>{{checkbox_252.204-7007[0]}} (i) 252.209-7002, Disclosure of Ownership or Control by a Foreign Government.</p><p>{{checkbox_252.204-7007[1]}} (ii) 252.225-7000, Buy American-Balance of Payments Program Certificate.</p><p>{{checkbox_252.204-7007[2]}} (iii) 252.225-7020, Trade Agreements Certificate.</p><p>{{checkbox_252.204-7007[3]}} Use with Alternate I.</p><p>{{checkbox_252.204-7007[4]}} (iv) 252.225-7031, Secondary Arab Boycott of Israel.</p><p>{{checkbox_252.204-7007[5]}} (v) 252.225-7035, Buy American-Free Trade Agreements-Balance of Payments Program Certificate.</p><p>{{checkbox_252.204-7007[6]}} Use with Alternate I.</p><p>{{checkbox_252.204-7007[7]}} Use with Alternate II.</p><p>{{checkbox_252.204-7007[8]}} Use with Alternate III.</p><p>{{checkbox_252.204-7007[9]}} Use with Alternate IV.</p><p>{{checkbox_252.204-7007[10]}} Use with Alternate V.</p><p>(e) The offeror has completed the annual representations and certifications electronically via the SAM Web site at <i>https://www.acquisition.gov/.</i> After reviewing the ORCA database information, the offeror verifies by submission of the offer that the representations and certifications currently posted electronically that apply to this solicitation as indicated in FAR 52.204-8(c) and paragraph (d) of this provision have been entered or updated within the last 12 months, are current, accurate, complete, and applicable to this solicitation (including the business size standard applicable to the NAICS code referenced for this solicitation), as of the date of this offer, and are incorporated in this offer by reference (see FAR 4.1201); except for the changes identified below [<i>offeror to insert changes, identifying change by provision number, title, date</i>]. Any changes provided by the offeror are applicable to this solicitation only, and do not result in an update to the representations and certifications located in the SAM database.</p><div><div><table border="1" cellpadding="1" cellspacing="1" frame="void" width="100%"><tbody><tr><th scope="col">FAR/DFARS provision No.</th><th scope="col">Title</th><th scope="col">Date</th><th scope="col">Change</th></tr><tr><td>{{textbox_252.204-7007[0]}}</td><td>{{textbox_252.204-7007[1]}}</td><td>{{textbox_252.204-7007[2]}}</td><td>{{textbox_252.204-7007[3]}}</td></tr></tbody></table></div></div><p>Any changes provided by the offeror are applicable to this solicitation only, and do not result in an update to the representations and certifications posted on ORCA.</p>'
--               '<p>Substitute the following paragraphs (d) and (e) for paragraph (d) of the provision at FAR 52.204-8:</p><p>(d)(1) The following representations or certifications in the System for Award Management (SAM) database are applicable to this solicitation as indicated:</p><p>(i) 252.209-7003, Reserve Officer Training Corps and Military Recruiting on Campus-Representation. Applies to all solicitations with institutions of higher education.</p><p>(ii) 252.216-7008, Economic Price Adjustment-Wage Rates or Material Prices Controlled by a Foreign Government. Applies to solicitations for fixed-price supply and service contracts when the contract is to be performed wholly or in part in a foreign country, and a foreign government controls wage rates or material prices and may during contract performance impose a mandatory change in wages or prices of materials.</p><p>(iii) 252.222-7007, Representation Regarding Combating Trafficking in Persons, as prescribed in 222.1771. Applies to solicitations with a value expected to exceed the simplified acquisition threshold.</p><p>(iv) 252.225-7042, Authorization to Perform. Applies to all solicitations when performance will be wholly or in part in a foreign country.</p><p>(v) 252.225-7049, Prohibition on Acquisition of Commercial Satellite Services from Certain Foreign Entities-Representations. Applies to solicitations for the acquisition of commercial satellite services.</p><p>(vi) 252.225-7050, Disclosure of Ownership or Control by the Government of a Country that is a State Sponsor of Terrorism. Applies to all solicitations expected to result in contracts of $150,000 or more.</p><p>(vii) 252.229-7012, Tax Exemptions (Italy)-Representation. Applies to solicitations when contract performance will be in Italy.</p><p>(viii) 252.229-7013, Tax Exemptions (Spain)-Representation. Applies to solicitations when contract performance will be in Spain.</p><p>(ix) 252.247-7022, Representation of Extent of Transportation by Sea. Applies to all solicitations except those for direct purchase of ocean transportation services or those with an anticipated value at or below the simplified acquisition threshold.</p><p>(2) The following representations or certifications in SAM are applicable to this solicitation as indicated by the Contracting Officer: [<i>Contracting Officer check as appropriate.</i>]</p><p>{{checkbox_252.204-7007[0]}} (i) 252.209-7002, Disclosure of Ownership or Control by a Foreign Government.</p><p>{{checkbox_252.204-7007[1]}} (i) 252.225-7000, Buy American-Balance of Payments Program Certificate.</p><p>{{checkbox_252.204-7007[2]}} (i) 252.225-7020, Trade Agreements Certificate.</p><p>{{checkbox_252.204-7007[3]}} Use with Alternate I.</p><p>{{checkbox_252.204-7007[4]}} (v) 252.225-7031, Secondary Arab Boycott of Israel.</p><p>{{checkbox_252.204-7007[5]}} (v) 252.225-7035, Buy American-Free Trade Agreements-Balance of Payments Program Certificate.</p><p>{{checkbox_252.204-7007[6]}} Use with Alternate I.</p><p>{{checkbox_252.204-7007[7]}} Use with Alternate II.</p><p>{{checkbox_252.204-7007[8]}} Use with Alternate III.</p><p>{{checkbox_252.204-7007[9]}} Use with Alternate IV.</p><p>{{checkbox_252.204-7007[10]}} Use with Alternate V.</p><p>(e) The offeror has completed the annual representations and certifications electronically via the SAM Web site at <i>https://www.acquisition.gov/.</i> After reviewing the ORCA database information, the offeror verifies by submission of the offer that the representations and certifications currently posted electronically that apply to this solicitation as indicated in FAR 52.204-8(c) and paragraph (d) of this provision have been entered or updated within the last 12 months, are current, accurate, complete, and applicable to this solicitation (including the business size standard applicable to the NAICS code referenced for this solicitation), as of the date of this offer, and are incorporated in this offer by reference (see FAR 4.1201); except for the changes identified below [<i>offeror to insert changes, identifying change by provision number, title, date</i>]. Any changes provided by the offeror are applicable to this solicitation only, and do not result in an update to the representations and certifications located in the SAM database.</p><div><div><table border="1" cellpadding="1" cellspacing="1" frame="void" width="100%"><tbody><tr><th scope="col">FAR/DFARS provision No.</th><th scope="col">Title</th><th scope="col">Date</th><th scope="col">Change</th></tr><tr><td>{{textbox_252.204-7007[0]}}</td><td>{{textbox_252.204-7007[1]}}</td><td>{{textbox_252.204-7007[2]}}</td><td>{{textbox_252.204-7007[3]}}</td></tr></tbody></table></div></div><p>Any changes provided by the offeror are applicable to this solicitation only, and do not result in an update to the representations and certifications posted on ORCA.</p>'
WHERE clause_id = 101416; -- 252.204-7007

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101417; -- 252.204-7010

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101418; -- 252.204-7011

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101419; -- 252.204-7012

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101420; -- 252.204-7013

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101421; -- 252.204-7014

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101422; -- 252.204-7015

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101423; -- 252.205-7000

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101424; -- 252.206-7000

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101425; -- 252.208-7000

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101426; -- 252.209-7002

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101427; -- 252.209-7003

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101428; -- 252.209-7004

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101429; -- 252.209-7005

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101430; -- 252.209-7006

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101431; -- 252.209-7007

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101432; -- 252.209-7008

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101433; -- 252.209-7009

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101434; -- 252.209-7010

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101435; -- 252.209-7992 (DEVIATION 2015-00005)

UPDATE Clauses
SET is_editable = 0
, clause_data = '<p>(a) In accordance with sections 8113 and 8114 of the Department of Defense Appropriations Act, 2014, and sections 414 and 415 of the Military Construction and Veterans Affairs and Related Agencies Appropriations Act, 2014 (Public Law 113-76, Divisions C and J), none of the funds made available by those divisions (including Military Construction funds) may be used to enter into a contract with any corporation that-</p> <p>(1) Has any unpaid Federal tax liability that has been assessed, for which all judicial and administrative remedies have been exhausted or have lapsed, and that is not being paid in a timely manner pursuant to an agreement with the authority responsible for collecting the tax liability, where the awarding agency is aware of the unpaid tax liability, unless the agency has considered suspension or debarment of the corporation and made a determination that this further action is not necessary to protect the interests of the Government; or</p> <p>(2) Was convicted of a felony criminal violation under any Federal law within the preceding 24 months, where the awarding agency is aware of the conviction, unless the agency has considered suspension or debarment of the corporation and made a determination that this action is not necessary to protect the interests of the Government.</p> <p>(b) The Offeror represents that-</p> <p>(1) It is [] is not [] a corporation that has any unpaid Federal tax liability that has been assessed, for which all judicial and administrative remedies have been exhausted or have lapsed, and that is not being paid in a timely manner pursuant to an agreement with the authority responsible for collecting the tax liability, </p><p>(2) It is []is not []a corporation that was convicted of a felony criminal violation under a Federal law within the preceding 24 months.</p>'
--               '<p>(a) In accordance with sections 8113 and 8114 of the Department of Defense Appropriations Act, 2014, and sections 414 and 415 of the Military Construction and Veterans Affairs and Related Agencies Appropriations Act, 2014 (Public Law 113-76, Divisions C and J), none of the funds made available by those divisions (including Military Construction funds) may be used to enter into a contract with any corporation that-</p> <p>(1) Has any unpaid Federal tax liability that has been assessed, for which all judicial and administrative remedies have been exhausted or have lapsed, and that is not being paid in a timely manner pursuant to an agreement with the authority responsible for collecting the tax liability, where the awarding agency is aware of the unpaid tax liability, unless the agency has considered suspension or debarment of the corporation and made a determination that this further action is not necessary to protect the interests of the Government; or</p> <p>(2) Was convicted of a felony criminal violation under any Federal law within the preceding 24 months, where the awarding agency is aware of the conviction, unless the agency has considered suspension or debarment of the corporation and made a determination that this action is not necessary to protect the interests of the Government.</p> <p>(b) The Offeror represents that-</p> <p>(1) It is {{checkbox_252.209-7993_(DEVIATION_2014-00009)[0]}} is not {{checkbox_252.209-7993_(DEVIATION_2014-00009)[1]}} a corporation that has any unpaid Federal tax liability that has been assessed, for which all judicial and administrative remedies have been exhausted or have lapsed, and that is not being paid in a timely manner pursuant to an agreement with the authority responsible for collecting the tax liability, </p><p>(2) It is {{checkbox_252.209-7993_(DEVIATION_2014-00009)[2]}} is not {{checkbox_252.209-7993_(DEVIATION_2014-00009)[3]}} a corporation that was convicted of a felony criminal violation under a Federal law within the preceding 24 months.</p>'
WHERE clause_id = 101436; -- 252.209-7993 (DEVIATION 2014-00009)
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 30587; -- checkbox_252.209-7993_(DEVIATION_2014-00009)[0]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 30588; -- checkbox_252.209-7993_(DEVIATION_2014-00009)[1]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 30589; -- checkbox_252.209-7993_(DEVIATION_2014-00009)[2]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 30590; -- checkbox_252.209-7993_(DEVIATION_2014-00009)[3]

UPDATE Clauses
SET is_editable = 0
, clause_data = '<p><b>REPRESENTATION BY CORPORATIONS REGARDING AN UNPAID DELINQUENT TAX LIABILITY OR A FELONY CONVICTION UNDER ANY FEDERAL LAW-FISCAL YEAR 2014 APPROPRIATIONS (DEVIATION 2014-00004) (OCTOBER 2013)</b></p> <p>(a) In accordance with section 101(a) of Division A of the Continuing Appropriations Act, 2014 (Pub. L. 113-46), none of the funds made available by that Act for DoD (including Military Construction funds) may be used to enter into a contract with any corporation that-</p> <p>(1) Has any unpaid Federal tax liability that has been assessed, for which all judicial and administrative remedies have been exhausted or have lapsed, and that is not being paid in a timely manner pursuant to an agreement with the authority responsible for collecting the tax liability, where the awarding agency is aware of the unpaid tax liability, unless the agency has considered suspension or debarment of the corporation and made a determination that this further action is not necessary to protect the interests of the Government; or</p> <p>(2) Was convicted of a felony criminal violation under any Federal law within the preceding 24 months, where the awarding agency is aware of the conviction, unless the agency has considered suspension or debarment of the corporation and made a determination that this action is not necessary to protect the interests of the Government.</p> <p>(b) The Offeror represents that-</p> <p>(1) It is [ ] is not [ ] a corporation that has any unpaid Federal tax liability that has been assessed, for which all judicial and administrative remedies have been exhausted or have lapsed, and that is not being paid in a timely manner pursuant to an agreement with the authority responsible for collecting the tax liability,</p> <p>(2) It is [] is not [ ] a corporation that was convicted of a felony criminal violation under a Federal law within the preceding 24 months.</p>'
--               '<p>(a) In accordance with section 101(a) of Division A of the Continuing Appropriations Act, 2014 (Pub. L. 113-46), none of the funds made available by that Act for DoD (including Military Construction funds) may be used to enter into a contract with any corporation that-</p> <p>(1) Has any unpaid Federal tax liability that has been assessed, for which all judicial and administrative remedies have been exhausted or have lapsed, and that is not being paid in a timely manner pursuant to an agreement with the authority responsible for collecting the tax liability, where the awarding agency is aware of the unpaid tax liability, unless the agency has considered suspension or debarment of the corporation and made a determination that this further action is not necessary to protect the interests of the Government; or</p> <p>(2) Was convicted of a felony criminal violation under any Federal law within the preceding 24 months, where the awarding agency is aware of the conviction, unless the agency has considered suspension or debarment of the corporation and made a determination that this action is not necessary to protect the interests of the Government.</p> <p>(b) The Offeror represents that-</p> <p>(1) It is {{checkbox_252.209-7994_(DEVIATION_2014-00004)[0]}} is not {{checkbox_252.209-7994_(DEVIATION_2014-00004)[1]}} a corporation that has any unpaid Federal tax liability that has been assessed, for which all judicial and administrative remedies have been exhausted or have lapsed, and that is not being paid in a timely manner pursuant to an agreement with the authority responsible for collecting the tax liability,</p> <p>(2) It is {{checkbox_252.209-7994_(DEVIATION_2014-00004)[2]}} is not {{checkbox_252.209-7994_(DEVIATION_2014-00004)[3]}} a corporation that was convicted of a felony criminal violation under a Federal law within the preceding 24 months.</p>'
WHERE clause_id = 101437; -- 252.209-7994 (DEVIATION 2014-00004)
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 30591; -- checkbox_252.209-7994_(DEVIATION_2014-00004)[0]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 30592; -- checkbox_252.209-7994_(DEVIATION_2014-00004)[1]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 30593; -- checkbox_252.209-7994_(DEVIATION_2014-00004)[2]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 30594; -- checkbox_252.209-7994_(DEVIATION_2014-00004)[3]

UPDATE Clauses
SET is_editable = 0
, clause_data = '<p>(a) In accordance with sections 8112 and 8113 of Division C and\nsections 514 and 515 of Division E of the Consolidated and FurtherContinuing Appropriations Act, 2013,(Pub. L. 113-6), none of the\nfunds made available by that Act for DoD (including Military\nConstruction funds) may be used to enter into a contract with any\ncorporation that-\n</p><p>(1) Has any unpaid Federal tax liability that has been\nassessed, for which all judicial and administrative remedies have\nbeen exhausted or have lapsed, and that is not being paid in a\ntimely manner pursuant to an agreement with the authority\nresponsible for collecting the tax liability, where the awarding\nagency is aware of the unpaid tax liability, unless the agency\nhas considered suspension or debarment of the corporation and\nmade a determination that this further action is not necessary to\nprotect the interests of the Government; or\n</p><p>(2) Was convicted of a felony criminal violation under any\nFederal law within the preceding 24 months, where the awarding\nagency is aware of the conviction, unless the agency has\nconsidered suspension or debarment of the corporation and made a\ndetermination that this action is not necessary to protect the\ninterests of the Government.\n</p><p>(b) The Offeror represents that-\n</p><p>(1) It is [ ] is not [ ] a corporation that has any unpaid Federal\ntax liability that has been assessed, for which all judicial and\nadministrative remedies have been exhausted or have lapsed, and that is\nnot being paid in a timely manner pursuant to an agreement with the\nauthority responsible for collecting the tax liability,\n</p><p>(2) It is [ ] is not [ ] a corporation that was convicted of a\nfelony criminal violation under a Federal law within the preceding 24\nmonths.</p>'
--               '<p>(a) In accordance with sections 8112 and 8113 of Division C and\nsections 514 and 515 of Division E of the Consolidated and Further\nContinuing Appropriations Act, 2013,(Pub. L. 113-6), none of the\nfunds made available by that Act for DoD (including Military\nConstruction funds) may be used to enter into a contract with any\ncorporation that-\n</p><p>(1) Has any unpaid Federal tax liability that has been\nassessed, for which all judicial and administrative remedies have\nbeen exhausted or have lapsed, and that is not being paid in a\ntimely manner pursuant to an agreement with the authority\nresponsible for collecting the tax liability, where the awarding\nagency is aware of the unpaid tax liability, unless the agency\nhas considered suspension or debarment of the corporation and\nmade a determination that this further action is not necessary to\nprotect the interests of the Government; or\n</p><p>(2) Was convicted of a felony criminal violation under any\nFederal law within the preceding 24 months, where the awarding\nagency is aware of the conviction, unless the agency has\nconsidered suspension or debarment of the corporation and made a\ndetermination that this action is not necessary to protect the\ninterests of the Government.\n</p><p>(b) The Offeror represents that-\n</p><p>(1) It is [ {{checkbox_252.209-7995_(DEVIATION_2013-00010)[0]}} ] is not [ {{checkbox_252.209-7995_(DEVIATION_2013-00010)[1]}}  ] a corporation that has any unpaid Federal\ntax liability that has been assessed, for which all judicial and\nadministrative remedies have been exhausted or have lapsed, and that is\nnot being paid in a timely manner pursuant to an agreement with the\nauthority responsible for collecting the tax liability,\n</p><p>(2) It is [ {{checkbox_252.209-7995_(DEVIATION_2013-00010)[2]}} ] is not [ {{checkbox_252.209-7995_(DEVIATION_2013-00010)[3]}} ] a corporation that was convicted of a\nfelony criminal violation under a Federal law within the preceding 24\nmonths.</p>'
WHERE clause_id = 101438; -- 252.209-7995 (DEVIATION 2013-00010)
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 30595; -- checkbox_252.209-7995_(DEVIATION_2013-00010)[0]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 30596; -- checkbox_252.209-7995_(DEVIATION_2013-00010)[1]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 30597; -- checkbox_252.209-7995_(DEVIATION_2013-00010)[2]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 30598; -- checkbox_252.209-7995_(DEVIATION_2013-00010)[3]

UPDATE Clauses
SET is_editable = 0
, clause_data = '<p><b>REPRESENTATION BY CORPORATIONS REGARDING A FELONY CONVICTION UNDER\nANY FEDERAL LAW-DoD MILITARY CONSTRUCTION APPROPRIATIONS (DEVIATION\n2013-00006) (DATE)</b></p>\n<p>(a) In accordance with section 101(a) (10) of the\nContinuing Appropriations Resolution, 2013, (Pub. L. 112-175)\nnone of the funds made available by that Act for military\nconstruction may be used to enter into a contract with any\ncorporation that was convicted of a felony criminal violation\nunder any Federal law within the preceding 24 months, where the\nawarding agency is aware of the conviction, unless the agency\nhas considered suspension or debarment of the corporation and\nmade a determination that this action is not necessary to\nprotect the interests of the Government.</p>\n<p>(b) The Offeror represents that it is [ ] is not [ ] a\ncorporation that was convicted of a felony criminal violation under\na Federal law within the preceding 24 months. </p>'
--               '<p>(a) In accordance with section 101(a) (10) of the Continuing Appropriations Resolution, 2013, (Pub. L. 112-175) none of the funds made available by that Act for military construction may be used to enter into a contract with any corporation that was convicted of a felony criminal violation under any Federal law within the preceding 24 months, where the awarding agency is aware of the conviction, unless the agency has considered suspension or debarment of the corporation and made a determination that this action is not necessary to protect the interests of the Government.</p> <p>(b) The Offeror represents that it is {{checkbox_252.209-7996_(DEVIATION_2013-00006)[0]}} is not {{checkbox_252.209-7996_(DEVIATION_2013-00006)[1]}} a corporation that was convicted of a felony criminal violation under a Federal law within the preceding 24 months. </p>'
WHERE clause_id = 101439; -- 252.209-7996 (DEVIATION 2013-00006)
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 30599; -- checkbox_252.209-7996_(DEVIATION_2013-00006)[0]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 30600; -- checkbox_252.209-7996_(DEVIATION_2013-00006)[1]

UPDATE Clauses
SET is_editable = 0
, clause_data = '<p><b>REPRESENTATION BY CORPORATIONS REGARDING AN UNPAID DELINQUENT TAX\nLIABILITY OR A FELONY CONVICTION UNDER ANY FEDERAL LAW-DoD\nAPPROPRIATIONS (DEVIATION 2013-00006) (DATE)</b></p>\n<p>(a) In accordance with section 101(a) (3) of the Continuing\nAppropriations Resolution, 2013, (Pub. L. 112-175) none of the\nfunds made available by that Act for general appropriations for\nDoD may be used to enter into a contract with any corporation\nthat-</p>\n<p>(1) Has any unpaid Federal tax liability that has\nbeen assessed, for which all judicial and administrative\nremedies have been exhausted or have lapsed, and that is\nnot being paid in a timely manner pursuant to an agreement\nwith the authority responsible for collecting the tax\nliability, where the awarding agency is aware of the unpaid\ntax liability, unless the agency has considered suspension\nor debarment of the corporation and made a determination\nthat this further action is not necessary to protect the\ninterests of the Government.</p>\n<p>(2) Was convicted of a felony criminal violation\nunder any Federal law within the preceding 24 months, where\nthe awarding agency is aware of the conviction, unless the\nagency has considered suspension or debarment of the\ncorporation and made a determination that this action is\nnot necessary to protect the interests of the Government.\n<p>(b) The Offeror represents that-</p>\n<p>(1) It is [ ] is not [ ] a corporation that has any unpaid\nFederal tax liability that has been assessed, for which all\njudicial and administrative remedies have been exhausted or have\nlapsed, and that is not being paid in a timely manner pursuant to\nan agreement with the authority responsible for collecting the tax\nliability,</p>\n<p>(2) It is [ ] is not [ ] a corporation that was convicted of\na felony criminal violation under a Federal law within the\npreceding 24 months.</p></p>'
--               '<p>(a) In accordance with section 101(a) (3) of the Continuing Appropriations Resolution, 2013, (Pub. L. 112-175) none of the funds made available by that Act for general appropriations for DoD may be used to enter into a contract with any corporation that-</p> <p>(1) Has any unpaid Federal tax liability that has been assessed, for which all judicial and administrative remedies have been exhausted or have lapsed, and that is not being paid in a timely manner pursuant to an agreement with the authority responsible for collecting the tax liability, where the awarding agency is aware of the unpaid tax liability, unless the agency has considered suspension or debarment of the corporation and made a determination that this further action is not necessary to protect the interests of the Government.</p> <p>(2) Was convicted of a felony criminal violation under any Federal law within the preceding 24 months, where the awarding agency is aware of the conviction, unless the agency has considered suspension or debarment of the corporation and made a determination that this action is not necessary to protect the interests of the Government. <p>(b) The Offeror represents that-</p> <p>(1) It is {{checkbox_252.209-7997_(DEVIATION_2013-00006)[0]}} is not {{checkbox_252.209-7997_(DEVIATION_2013-00006)[1]}} a corporation that has any unpaid Federal tax liability that has been assessed, for which all judicial and administrative remedies have been exhausted or have lapsed, and that is not being paid in a timely manner pursuant to an agreement with the authority responsible for collecting the tax liability,</p> <p>(2) It is {{checkbox_252.209-7997_(DEVIATION_2013-00006)[2]}} is not {{checkbox_252.209-7997_(DEVIATION_2013-00006)[3]}} a corporation that was convicted of a felony criminal violation under a Federal law within the preceding 24 months.</p></p>'
WHERE clause_id = 101440; -- 252.209-7997 (DEVIATION 2013-00006)
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 30601; -- checkbox_252.209-7997_(DEVIATION_2013-00006)[0]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 30602; -- checkbox_252.209-7997_(DEVIATION_2013-00006)[1]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 30603; -- checkbox_252.209-7997_(DEVIATION_2013-00006)[2]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 30604; -- checkbox_252.209-7997_(DEVIATION_2013-00006)[3]

UPDATE Clauses
SET is_editable = 0
, clause_data = '<p><b>REPRESENTATION REGARDING CONVICTION OF A FELONY CRIMINAL\nVIOLATION UNDER ANY FEDERAL OR STATE LAW (DEVIATION 2012-00007)\n(DATE 2012)</b></p>\n<p>(a) In accordance with section 514 of Division H of the\nConsolidated Appropriations Act, 2012, none of the funds made\navailable by that Act may be used to enter into a contract with\nany corporation that was convicted of a felony criminal\nviolation under any Federal or State law within the preceding 24\nmonths, where the awarding agency is aware of the conviction,\nunless the agency has considered suspension or debarment of the\ncorporation and made a determination that this further action is\nnot necessary to protect the interests of the Government.</p>\n<p>(b) The Offeror represents that it is [ ] is not [ ] a\ncorporation that was convicted of a felony criminal violation under\na Federal or State law within the preceding 24 months. </p>'
--               '<p>(a) In accordance with section 514 of Division H of the Consolidated Appropriations Act, 2012, none of the funds made available by that Act may be used to enter into a contract with any corporation that was convicted of a felony criminal violation under any Federal or State law within the preceding 24 months, where the awarding agency is aware of the conviction, unless the agency has considered suspension or debarment of the corporation and made a determination that this further action is not necessary to protect the interests of the Government.</p> <p>(b) The Offeror represents that it is {{checkbox_252.209-7998_(DEVIATION_2012-00007)[0]}} is not {{checkbox_252.209-7998_(DEVIATION_2012-00007)[1]}} a corporation that was convicted of a felony criminal violation under a Federal or State law within the preceding 24 months. </p>'
WHERE clause_id = 101441; -- 252.209-7998 (DEVIATION 2012-00007)
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 30605; -- checkbox_252.209-7998_(DEVIATION_2012-00007)[0]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 30606; -- checkbox_252.209-7998_(DEVIATION_2012-00007)[1]

UPDATE Clauses
SET is_editable = 0
, clause_data = '<p>(a) In accordance with sections 8124 and 8125 of Division\nA of the Consolidated Appropriations Act, 2012, (Pub. L. 112-74)\nnone of the funds made available by that Act may be used to\nenter into a contract with any corporation that-</p>\n<p>(1) Has any unpaid Federal tax liability that has\nbeen assessed, for which all judicial and administrative\nremedies have been exhausted or have lapsed, and that is\nnot being paid in a timely manner pursuant to an agreement\nwith the authority responsible for collecting the tax\nliability, where the awarding agency is aware of the unpaid\ntax liability, unless the agency has considered suspension\nor debarment of the corporation and made a determination\nthat this further action is not necessary to protect the\ninterests of the Government.</p>\n<p>(2) Was convicted of a felony criminal violation\nunder any Federal law within the preceding 24 months, where\nthe awarding agency is aware of the conviction, unless the\nagency has considered suspension or debarment of the\ncorporation and made a determination that this action is\nnot necessary to protect the interests of the Government.</p>\n<p>(b) The Offeror represents that-</p>\n<p>(1) It is [ ] is not [ ] a corporation that has any unpaid\nFederal tax liability that has been assessed, for which all\njudicial and administrative remedies have been exhausted or have\nlapsed, and that is not being paid in a timely manner pursuant to\nan agreement with the authority responsible for collecting the tax\nliability,</p>\n<p>(2) It is [ ] is not [ ] a corporation that was convicted of\na felony criminal violation under a Federal law within the\npreceding 24 months. </p>'
--               '<p>(a) In accordance with sections 8124 and 8125 of Division A of the Consolidated Appropriations Act, 2012, (Pub. L. 112-74) none of the funds made available by that Act may be used to enter into a contract with any corporation that-</p> <p>(1) Has any unpaid Federal tax liability that has been assessed, for which all judicial and administrative remedies have been exhausted or have lapsed, and that is not being paid in a timely manner pursuant to an agreement with the authority responsible for collecting the tax liability, where the awarding agency is aware of the unpaid tax liability, unless the agency has considered suspension or debarment of the corporation and made a determination that this further action is not necessary to protect the interests of the Government.</p> <p>(2) Was convicted of a felony criminal violation under any Federal law within the preceding 24 months, where the awarding agency is aware of the conviction, unless the agency has considered suspension or debarment of the corporation and made a determination that this action is not necessary to protect the interests of the Government.</p> <p>(b) The Offeror represents that-</p> <p>(1) It is {{checkbox_252.209-7999_(DEVIATION_2012-00004)[0]}} is not {{checkbox_252.209-7999_(DEVIATION_2012-00004)[1]}} a corporation that has any unpaid Federal tax liability that has been assessed, for which all judicial and administrative remedies have been exhausted or have lapsed, and that is not being paid in a timely manner pursuant to an agreement with the authority responsible for collecting the tax liability,</p> <p>(2) It is {{checkbox_252.209-7999_(DEVIATION_2012-00004)[2]}} is not {{checkbox_252.209-7999_(DEVIATION_2012-00004)[3]}} a corporation that was convicted of a felony criminal violation under a Federal law within the preceding 24 months. </p>'
WHERE clause_id = 101442; -- 252.209-7999 (DEVIATION 2012-00004)
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 30607; -- checkbox_252.209-7999_(DEVIATION_2012-00004)[0]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 30608; -- checkbox_252.209-7999_(DEVIATION_2012-00004)[1]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 30609; -- checkbox_252.209-7999_(DEVIATION_2012-00004)[2]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 30610; -- checkbox_252.209-7999_(DEVIATION_2012-00004)[3]

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101443; -- 252.211-7000

UPDATE Clauses
SET editable_remarks = 'ALL' -- NULL
WHERE clause_id = 101444; -- 252.211-7001

UPDATE Clauses
SET editable_remarks = 'ALL' -- NULL
WHERE clause_id = 101445; -- 252.211-7002

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101446; -- 252.211-7003

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101447; -- 252.211-7004

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101448; -- 252.211-7005

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101449; -- 252.211-7006

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101450; -- 252.211-7007

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101451; -- 252.211-7008

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101452; -- 252.212-7002

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101453; -- 252.213-7000

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101454; -- 252.215-7000

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101455; -- 252.215-7002

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101456; -- 252.215-7003

UPDATE Clauses
SET editable_remarks = 'PARAGRAPH (b) ONLY ONLY TO SPECIFY A HIGHER THRESHOLD' -- NULL
WHERE clause_id = 101457; -- 252.215-7004

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101458; -- 252.215-7005

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101459; -- 252.215-7006

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101461; -- 252.215-7007

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101462; -- 252.215-7008

UPDATE Clauses
SET is_optional = 1
, is_editable = 0
WHERE clause_id = 101463; -- 252.215-7009

UPDATE Clauses
SET is_optional = 1
, is_editable = 0
WHERE clause_id = 101464; -- 252.216-7000

UPDATE Clauses
SET is_optional = 1
, editable_remarks = 'When this clause is included in invitations for bids, omit Note 6 of the clause and all references to Note 6.' -- NULL
WHERE clause_id = 101465; -- 252.216-7001

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101466; -- 252.216-7002

UPDATE Clauses
SET is_optional = 1
, is_editable = 0
WHERE clause_id = 101467; -- 252.216-7003

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101468; -- 252.216-7004

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101469; -- 252.216-7005

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101470; -- 252.216-7006

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101471; -- 252.216-7007

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101472; -- 252.216-7008

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101473; -- 252.216-7009

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101475; -- 252.216-7010

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101477; -- 252.217-7000

UPDATE Clauses
SET editable_remarks = '(1) Insert the percentage of increase in paragraph (a) of the clause \n (2) Change 30 days in paragraphs (b)(2) and (d)(1) to longer periods, if appropriate.\n (3) Change the 24-month period in paragraph (c)(3), if appropriate.' -- NULL
WHERE clause_id = 101478; -- 252.217-7001

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101479; -- 252.217-7002

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101480; -- 252.217-7003

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101481; -- 252.217-7004

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101482; -- 252.217-7005

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101483; -- 252.217-7006

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101484; -- 252.217-7007

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101485; -- 252.217-7008

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101486; -- 252.217-7009

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101487; -- 252.217-7010

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101488; -- 252.217-7011

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101489; -- 252.217-7012

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101490; -- 252.217-7013

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101491; -- 252.217-7014

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101492; -- 252.217-7015

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101493; -- 252.217-7016

UPDATE Clauses
SET editable_remarks = 'ALL' -- NULL
WHERE clause_id = 101494; -- 252.217-7026

UPDATE Clauses
SET editable_remarks = 'ONLY: The words “and certified cost or pricing data” may be deleted from paragraph (a) of the clause.' -- NULL
WHERE clause_id = 101495; -- 252.217-7027

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101496; -- 252.217-7028

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101497; -- 252.219-7000

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101501; -- 252.219-7003

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101498; -- 252.219-7003 (DEVIATION 2013-00014)

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101502; -- 252.219-7004

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101503; -- 252.219-7009

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101504; -- 252.219-7010

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101505; -- 252.219-7011

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101506; -- 252.222-7000

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101507; -- 252.222-7001

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101508; -- 252.222-7002

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101509; -- 252.222-7003

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101510; -- 252.222-7004

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101511; -- 252.222-7005

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101512; -- 252.222-7006

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101513; -- 252.222-7007

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101514; -- 252.223-7001

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101515; -- 252.223-7002

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101516; -- 252.223-7003

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101517; -- 252.223-7004

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101519; -- 252.223-7006

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101520; -- 252.223-7007

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101521; -- 252.223-7008

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101523; -- 252.225-7000

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101525; -- 252.225-7001

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101526; -- 252.225-7002

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101527; -- 252.225-7003

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101528; -- 252.225-7004

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101529; -- 252.225-7005

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101530; -- 252.225-7007

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101531; -- 252.225-7008

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101532; -- 252.225-7009

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101533; -- 252.225-7010

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101534; -- 252.225-7011

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101535; -- 252.225-7012

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101536; -- 252.225-7013

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101537; -- 252.225-7015

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101538; -- 252.225-7016

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101539; -- 252.225-7017

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101540; -- 252.225-7018

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101541; -- 252.225-7019

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101543; -- 252.225-7020

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101545; -- 252.225-7021

UPDATE Clauses
SET editable_remarks = 'PARAGRAPH (d) "50" CAN BE REVISED' -- NULL
WHERE clause_id = 101546; -- 252.225-7023

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101547; -- 252.225-7024

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101548; -- 252.225-7025

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101549; -- 252.225-7026

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101550; -- 252.225-7027

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101551; -- 252.225-7028

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101552; -- 252.225-7029

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101553; -- 252.225-7030

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101554; -- 252.225-7031

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101555; -- 252.225-7032

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101556; -- 252.225-7033

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101562; -- 252.225-7035

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101568; -- 252.225-7036

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101569; -- 252.225-7037

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101570; -- 252.225-7038

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101571; -- 252.225-7039

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101572; -- 252.225-7040

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101573; -- 252.225-7041

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101574; -- 252.225-7042

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101575; -- 252.225-7043

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101577; -- 252.225-7044

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101581; -- 252.225-7045

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101582; -- 252.225-7046

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101583; -- 252.225-7047

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101584; -- 252.225-7048

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101585; -- 252.225-7049

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101586; -- 252.225-7050

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101587; -- 252.225-7982 (DEVIATION 2015-00012)

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101588; -- 252.225-7983 (DEVIATION 2015-00012)

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101589; -- 252.225-7984 (DEVIATION 2015-00012)

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101590; -- 252.225-7985 (DEVIATION 2015-00003)

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101591; -- 252.225-7987 (DEVIATION 2014-00016)

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101593; -- 252.225-7988 (DEVIATION 2014-00010)

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101592; -- 252.225-7988 (DEVIATION 2015-00007)

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101594; -- 252.225-7989 (DEVIATION 2014-00005)

UPDATE Clauses
SET is_editable = 0
, clause_data = '<p><b>PREFERENCE FOR PRODUCTS OR SERVICES FROM A CENTRAL ASIAN ST A TE OR AFGHANIST AN (APR 2014)(DEVIA TION 2014-00014)</b></p> <p>(a) Definitions. "Product from a Central Asian state or Afghanistan" and "service from a Central Asian state or Afghanistan," as used in this provision, are defined in the clause ofthis solicitation entitled "Requirement for Products or Services from a Central Asian State or Afghanistan" (252.225-7991 (DEVIA TION 2014-00014)). </p><p>(b) Representation. The Offeror represents that all products or services to be delivered under a contract resulting from this solicitation are products from a Central Asian state or Afghanistan or services from a Central Asian state or Afghanistan, except those listed in paragraph (c) ofthis provision. </p><p>(c) Other products or services. The following offered products or services are not products from a Central Asian state or Mghanistan, or services from a Central Asian state or Mghanistan: </p><p>(Line Item Number) (Country of Origin) </p><p>(d) Evaluation. For the purpose of evaluating competitive offers, the Contracting Officer will increase by 100 percent the prices of offers of products or services that are not products or services from a Central Asian state or Mghanistan.</p>'
--               '<p>(a) Definitions. "Product from a Central Asian state or Afghanistan" and "service from a Central Asian state or Afghanistan," as used in this provision, are defined in the clause of this solicitation entitled "Requirement for Products or Services from a Central Asian State or Afghanistan" (252.225-7991 (DEVIA TION 2014-00014)). </p><p>(b) Representation. The Offeror represents that all products or services to be delivered under a contract resulting from this solicitation are products from a Central Asian state or Afghanistan or services from a Central Asian state or Afghanistan, except those listed in paragraph (c) of this provision. </p><p>(c) Other products or services. The following offered products or services are not products from a Central Asian state or Mghanistan, or services from a Central Asian state or Mghanistan: </p><table><tr><td>{{textbox_252.225-7990_(DEVIATION_2014-00014)[0]}}</td><td>{{textbox_252.225-7990_(DEVIATION_2014-00014)[1]}}</td></tr><tr><td>{{textbox_252.225-7990_(DEVIATION_2014-00014)[2]}}</td><td>{{textbox_252.225-7990_(DEVIATION_2014-00014)[3]}}</td></tr><tr><td>{{textbox_252.225-7990_(DEVIATION_2014-00014)[4]}}</td><td>{{textbox_252.225-7990_(DEVIATION_2014-00014)[5]}}</td></tr><tr><td>{{textbox_252.225-7990_(DEVIATION_2014-00014)[6]}}</td><td>{{textbox_252.225-7990_(DEVIATION_2014-00014)[7]}}</td></tr><tr><td>{{textbox_252.225-7990_(DEVIATION_2014-00014)[8]}}</td><td>{{textbox_252.225-7990_(DEVIATION_2014-00014)[9]}}</td></tr><tr><td>(Line Item Number)</td><td>(Country of Origin)</td></tr></table><p>(d) Evaluation. For the purpose of evaluating competitive offers, the Contracting Officer will increase by 100 percent the prices of offers of products or services that are not products or services from a Central Asian state or Mghanistan.</p>'
WHERE clause_id = 101595; -- 252.225-7990 (DEVIATION 2014-00014)
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 30702; -- textbox_252.225-7990_(DEVIATION_2014-00014)[0]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 30703; -- textbox_252.225-7990_(DEVIATION_2014-00014)[1]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 30704; -- textbox_252.225-7990_(DEVIATION_2014-00014)[2]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 30705; -- textbox_252.225-7990_(DEVIATION_2014-00014)[3]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 30706; -- textbox_252.225-7990_(DEVIATION_2014-00014)[4]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 30707; -- textbox_252.225-7990_(DEVIATION_2014-00014)[5]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 30708; -- textbox_252.225-7990_(DEVIATION_2014-00014)[6]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 30709; -- textbox_252.225-7990_(DEVIATION_2014-00014)[7]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 30710; -- textbox_252.225-7990_(DEVIATION_2014-00014)[8]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 30711; -- textbox_252.225-7990_(DEVIATION_2014-00014)[9]

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101596; -- 252.225-7991 (DEVIATION 2014-00014)

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101597; -- 252.225-7992 (DEVIATION 2014-00014)

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101598; -- 252.225-7993 (DEVIATION 2014-00020)

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101599; -- 252.225-7994 (DEVIATION 2015-00013)

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101600; -- 252.225-7995 (DEVIATION 2015-00009)

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101601; -- 252.225-7996 (DEVIATION 2014-00014)

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101602; -- 252.225-7997 (DEVIATION 2013-00017)

UPDATE Clauses
SET is_editable = 0
, clause_data = '<p>(a) Definitions. "Product from Central Asia, Pakistan, the South Caucasus, or Afghanistan" and "service from Central Asia, Pakistan, the South Caucasus, or Afghanistan" as used in this provision, are defined in the clause ofthis solicitation entitled "Requirement for Products or Services from Central Asia, Pakistan, the South Caucasus, or Afghanistan" (252.225-7999 (DEVIATION 2014-00014)).</p>\n<p>(b) Representation. The Offeror represents that all products or services to be delivered under a contract resulting from this solicitation are products from Central Asia, Pakistan, the South Caucasus, or Afghanistan or services from Central Asia, Pakistan, the South Caucasus, or Afghanistan, except those listed in paragraph (c) of this provision</p>\n<p>&#xfffc;&#xfffc;&#xfffc;(c) Other products or services. The following offered products or services are not products from Central Asia, Pakistan, the South Caucasus or Afghanistan or services from Central Asia, Pakistan, the South Caucasus, or Afghanistan:</p>\n<p>(Line Item Number) (Country of Origin)</p>\n<p>(d) Evaluation. For the purpose ofevaluating competitive offers, the Contracting Officer will increase by 100 percent the prices of offers of products or services that are not products or services from Central Asia, Pakistan, the South Caucasus, or Afghanistan.</p>'
--               '<p>(a) Definitions. "Product from Central Asia, Pakistan, the South Caucasus, or Afghanistan" and "service from Central Asia, Pakistan, the South Caucasus, or Afghanistan" as used in this provision, are defined in the clause ofthis solicitation entitled "Requirement for Products or Services from Central Asia, Pakistan, the South Caucasus, or Afghanistan" (252.225-7999 (DEVIATION 2014-00014)).</p><p>(b) Representation. The Offeror represents that all products or services to be delivered under a contract resulting from this solicitation are products from Central Asia, Pakistan, the South Caucasus, or Afghanistan or services from Central Asia, Pakistan, the South Caucasus, or Afghanistan, except those listed in paragraph (c) of this provision</p><p>&#xfffc;&#xfffc;&#xfffc;(c) Other products or services. The following offered products or services are not products from Central Asia, Pakistan, the South Caucasus or Afghanistan or services from Central Asia, Pakistan, the South Caucasus, or Afghanistan:</p><p><table><tr><td>{{textbox_252.225-7998_(DEVIATION_2014-00014)[0]}}</td><td>{{textbox_252.225-7998_(DEVIATION_2014-00014)[1]}}</td></tr><tr><td>{{textbox_252.225-7998_(DEVIATION_2014-00014)[2]}}</td><td>{{textbox_252.225-7998_(DEVIATION_2014-00014)[3]}}</td></tr><tr><td>{{textbox_252.225-7998_(DEVIATION_2014-00014)[4]}}</td><td>{{textbox_252.225-7998_(DEVIATION_2014-00014)[5]}}</td></tr><tr><td>{{textbox_252.225-7998_(DEVIATION_2014-00014)[6]}}</td><td>{{textbox_252.225-7998_(DEVIATION_2014-00014)[7]}}</td></tr><tr><td>{{textbox_252.225-7998_(DEVIATION_2014-00014)[8]}}</td><td>{{textbox_252.225-7998_(DEVIATION_2014-00014)[9]}}</td></tr><tr><td>(Line Item Number)</td><td>(Country of Origin)</td></tr></table></p><p>(d) Evaluation. For the purpose ofevaluating competitive offers, the Contracting Officer will increase by 100 percent the prices of offers of products or services that are not products or services from Central Asia, Pakistan, the South Caucasus, or Afghanistan.</p>'
WHERE clause_id = 101603; -- 252.225-7998 (DEVIATION 2014-00014)
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 30712; -- textbox_252.225-7998_(DEVIATION_2014-00014)[0]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 30713; -- textbox_252.225-7998_(DEVIATION_2014-00014)[1]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 30714; -- textbox_252.225-7998_(DEVIATION_2014-00014)[2]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 30715; -- textbox_252.225-7998_(DEVIATION_2014-00014)[3]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 30716; -- textbox_252.225-7998_(DEVIATION_2014-00014)[4]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 30717; -- textbox_252.225-7998_(DEVIATION_2014-00014)[5]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 30718; -- textbox_252.225-7998_(DEVIATION_2014-00014)[6]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 30719; -- textbox_252.225-7998_(DEVIATION_2014-00014)[7]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 30720; -- textbox_252.225-7998_(DEVIATION_2014-00014)[8]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 30721; -- textbox_252.225-7998_(DEVIATION_2014-00014)[9]

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101604; -- 252.225-7999 (DEVIATION 2014-00014)

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101605; -- 252.226-7001

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101606; -- 252.227-7000

UPDATE Clauses
SET is_optional = 1
WHERE clause_id = 101607; -- 252.227-7001

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101608; -- 252.227-7002

UPDATE Clauses
SET is_optional = 1
WHERE clause_id = 101609; -- 252.227-7003

UPDATE Clauses
SET is_optional = 1
WHERE clause_id = 101610; -- 252.227-7004

UPDATE Clauses
SET is_optional = 1
WHERE clause_id = 101613; -- 252.227-7006

UPDATE Clauses
SET is_optional = 1
WHERE clause_id = 101614; -- 252.227-7007

UPDATE Clauses
SET is_optional = 1
WHERE clause_id = 101615; -- 252.227-7008

UPDATE Clauses
SET is_optional = 1
WHERE clause_id = 101616; -- 252.227-7009

UPDATE Clauses
SET is_optional = 1
WHERE clause_id = 101617; -- 252.227-7010

UPDATE Clauses
SET is_optional = 1
WHERE clause_id = 101618; -- 252.227-7011

UPDATE Clauses
SET is_optional = 1
WHERE clause_id = 101619; -- 252.227-7012

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101622; -- 252.227-7013

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101624; -- 252.227-7014

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101626; -- 252.227-7015

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101627; -- 252.227-7016

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101628; -- 252.227-7017

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101630; -- 252.227-7018

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101631; -- 252.227-7019

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101632; -- 252.227-7020

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101633; -- 252.227-7021

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101634; -- 252.227-7022

UPDATE Clauses
SET is_optional = 1
, is_editable = 0
WHERE clause_id = 101635; -- 252.227-7023

UPDATE Clauses
SET is_optional = 1
, is_editable = 0
WHERE clause_id = 101636; -- 252.227-7024

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101637; -- 252.227-7025

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101638; -- 252.227-7026

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101639; -- 252.227-7027

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101640; -- 252.227-7028

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101641; -- 252.227-7030

UPDATE Clauses
SET is_optional = 1
, is_editable = 0
WHERE clause_id = 101642; -- 252.227-7032

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101643; -- 252.227-7033

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101644; -- 252.227-7037

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101645; -- 252.227-7038

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101646; -- 252.227-7039

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101647; -- 252.228-7000

UPDATE Clauses
SET editable_remarks = 'The clause at 252.228-7001 may be modified only as follows:\n(i) Include a modified definition of “aircraft” if the contract covers other than conventional types of winged aircraft, i.e., helicopters, vertical take-off or landing aircraft, lighter-than-air airships, unmanned aerial vehicles, or other nonconventional t. The modified definition should describe a stage of manufacture comparable to the standard definition.\n(ii) Modify “in the open” to include “hush houses,” test hangars and comparable structures, and other designated areas.\n(iii) Expressly define the “contractor''s premises” where the aircraft will be located during and for contract performance. These locations may include contract\npremises which are owned or leased by the contractor or subcontractor, or premises where the contractor or subcontractor is a permittee or licensee or has a right to use, including Government airfields.\n(iv) Revise paragraph (e)(3) of the clause to provide Government\nassumption of risk for transportation by conveyance on streets or highways when transportation is—\n(A) Limited to the vicinity of contractor premises; and\n(B) Incidental to work performed under the contract.' -- NULL
WHERE clause_id = 101648; -- 252.228-7001

UPDATE Clauses
SET is_optional = 1
, is_editable = 0
WHERE clause_id = 101649; -- 252.228-7003

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101650; -- 252.228-7004

UPDATE Clauses
SET is_optional = 1
, is_editable = 0
WHERE clause_id = 101651; -- 252.228-7005

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101652; -- 252.228-7006

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101653; -- 252.229-7000

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101655; -- 252.229-7001

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101656; -- 252.229-7002

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101657; -- 252.229-7003

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101658; -- 252.229-7004

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101659; -- 252.229-7005

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101660; -- 252.229-7006

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101661; -- 252.229-7007

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101662; -- 252.229-7008

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101663; -- 252.229-7009

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101664; -- 252.229-7010

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101665; -- 252.229-7011

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101666; -- 252.229-7012

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101667; -- 252.229-7013

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101668; -- 252.229-7998 (DEVIATION 2013-00016)

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101669; -- 252.229-7999 (DEVIATION 2013-00016)

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101670; -- 252.231-7000

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101671; -- 252.232-7000

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101672; -- 252.232-7001

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101673; -- 252.232-7002

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101674; -- 252.232-7003

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101675; -- 252.232-7004

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101676; -- 252.232-7005

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101677; -- 252.232-7006

UPDATE Clauses
SET editable_remarks = 'PARAGRAPH (C): ONLY from “ninety” to “thirty” or “sixty” days, as appropriate' -- NULL
WHERE clause_id = 101678; -- 252.232-7007

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101679; -- 252.232-7008

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101680; -- 252.232-7009

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101681; -- 252.232-7010

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101682; -- 252.232-7011

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101683; -- 252.232-7012

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101684; -- 252.232-7013

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101685; -- 252.232-7014

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101686; -- 252.233-7001

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101687; -- 252.234-7001

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101688; -- 252.234-7002

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101690; -- 252.234-7003

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101692; -- 252.234-7004

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101693; -- 252.235-7000

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101694; -- 252.235-7001

UPDATE Clauses
SET editable_remarks = 'ALL' -- NULL
WHERE clause_id = 101695; -- 252.235-7002

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101697; -- 252.235-7003

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101698; -- 252.235-7004

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101699; -- 252.235-7010

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101700; -- 252.235-7011

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101701; -- 252.236-7000

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101702; -- 252.236-7001

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101703; -- 252.236-7002

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101704; -- 252.236-7003

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101705; -- 252.236-7004

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101706; -- 252.236-7005

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101707; -- 252.236-7006

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101708; -- 252.236-7007

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101709; -- 252.236-7008

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101710; -- 252.236-7009

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101711; -- 252.236-7010

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101712; -- 252.236-7011

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101713; -- 252.236-7012

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101714; -- 252.236-7013

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101715; -- 252.237-7000

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101716; -- 252.237-7001

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101718; -- 252.237-7002

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101719; -- 252.237-7003
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size, fill_in_group_number, fill_in_placeholder, fill_in_default_data, fill_in_display_rows, fill_in_for_table, fill_in_heading) VALUES (101719, 'textbox_252.237-7003[2]', 'S', 100, NULL, NULL, NULL, NULL, 0, NULL);

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101720; -- 252.237-7004

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101721; -- 252.237-7005

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101722; -- 252.237-7006

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101723; -- 252.237-7007

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101724; -- 252.237-7008

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101725; -- 252.237-7009

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101726; -- 252.237-7010

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101727; -- 252.237-7011

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101728; -- 252.237-7012

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101729; -- 252.237-7013

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101730; -- 252.237-7014

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101731; -- 252.237-7015

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101734; -- 252.237-7016

UPDATE Clauses
SET editable_remarks = 'ONLY paragraphs (d)(1) and (2) of the clause may be modified to meet local conditions.' -- NULL
WHERE clause_id = 101735; -- 252.237-7017

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101736; -- 252.237-7018

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101737; -- 252.237-7019

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101738; -- 252.237-7022

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101739; -- 252.237-7023

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101740; -- 252.237-7024

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101741; -- 252.239-7000

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101742; -- 252.239-7001

UPDATE Clauses
SET editable_remarks = 'ALL - only if necessary to meet the requirements of a governmental regulatory agency' -- NULL
WHERE clause_id = 101743; -- 252.239-7002

UPDATE Clauses
SET editable_remarks = 'ALL - only if necessary to meet the requirements of a governmental regulatory agency' -- NULL
WHERE clause_id = 101744; -- 252.239-7004

UPDATE Clauses
SET editable_remarks = 'ALL - only if necessary to meet the requirements of a governmental regulatory agency' -- NULL
WHERE clause_id = 101745; -- 252.239-7005

UPDATE Clauses
SET editable_remarks = 'ALL - only if necessary to meet the requirements of a governmental regulatory agency' -- NULL
WHERE clause_id = 101746; -- 252.239-7006

UPDATE Clauses
SET editable_remarks = 'ALL - only if necessary to meet the requirements of a governmental regulatory agency' -- NULL
WHERE clause_id = 101747; -- 252.239-7007

UPDATE Clauses
SET editable_remarks = 'ALL - only if necessary to meet the requirements of a governmental regulatory agency' -- NULL
WHERE clause_id = 101748; -- 252.239-7008

UPDATE Clauses
SET editable_remarks = 'ALL - only if necessary to meet the requirements of a governmental regulatory agency' -- NULL
WHERE clause_id = 101749; -- 252.239-7011

UPDATE Clauses
SET editable_remarks = 'ALL - only if necessary to meet the requirements of a governmental regulatory agency' -- NULL
WHERE clause_id = 101750; -- 252.239-7012

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101751; -- 252.239-7013

UPDATE Clauses
SET editable_remarks = 'Only to Insert the effective date of the agreement in paragraph (a) of the clause' -- NULL
WHERE clause_id = 101752; -- 252.239-7014

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101753; -- 252.239-7015

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101754; -- 252.239-7016

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101755; -- 252.239-7017

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101756; -- 252.239-7018

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101757; -- 252.239-7999 (DEVIATION 2015-00011)

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101758; -- 252.241-7000

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101759; -- 252.241-7001

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101760; -- 252.242-7004

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101761; -- 252.242-7005

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101762; -- 252.242-7006

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101763; -- 252.243-7001

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101764; -- 252.243-7002

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101765; -- 252.244-7000

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101767; -- 252.244-7001

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101768; -- 252.245-7000

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101769; -- 252.245-7001

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101770; -- 252.245-7002

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101771; -- 252.245-7003

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101772; -- 252.245-7004

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101773; -- 252.246-7000

UPDATE Clauses
SET editable_remarks = 'ALL' -- NULL
WHERE clause_id = 101776; -- 252.246-7001

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101777; -- 252.246-7002

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101778; -- 252.246-7003

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101779; -- 252.246-7004

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101780; -- 252.246-7005

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101781; -- 252.246-7006

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101782; -- 252.246-7007

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101783; -- 252.247-7000

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101784; -- 252.247-7001

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101785; -- 252.247-7002

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101786; -- 252.247-7003

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101787; -- 252.247-7004

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101788; -- 252.247-7005

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101789; -- 252.247-7006

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101790; -- 252.247-7007

UPDATE Clauses
SET editable_remarks = 'ONLY IF USINGNEGOTIATION' -- NULL
WHERE clause_id = 101792; -- 252.247-7008

UPDATE Clauses
SET editable_remarks = 'ONLY IF USINGNEGOTIATION' -- NULL
WHERE clause_id = 101793; -- 252.247-7009

UPDATE Clauses
SET editable_remarks = 'ONLY IF USING NEGOTIATION PROCEDURES' -- NULL
WHERE clause_id = 101794; -- 252.247-7010

UPDATE Clauses
SET editable_remarks = 'ONLY IF USING NEGOTIATION PROCEDURES' -- NULL
WHERE clause_id = 101795; -- 252.247-7011

UPDATE Clauses
SET editable_remarks = 'ONLY IF USING NEGOTIATION PROCEDURES' -- NULL
WHERE clause_id = 101796; -- 252.247-7012

UPDATE Clauses
SET editable_remarks = 'ONLY IF USING NEGOTIATION PROCEDURES' -- NULL
WHERE clause_id = 101797; -- 252.247-7013

UPDATE Clauses
SET editable_remarks = 'ONLY IF USING NEGOTIATION PROCEDURES' -- NULL
WHERE clause_id = 101798; -- 252.247-7014

UPDATE Clauses
SET editable_remarks = 'ONLY IF USING NEGOTIATION PROCEDURES' -- NULL
WHERE clause_id = 101799; -- 252.247-7016

UPDATE Clauses
SET editable_remarks = 'ONLY IF USING NEGOTIATION PROCEDURES' -- NULL
WHERE clause_id = 101800; -- 252.247-7017

UPDATE Clauses
SET editable_remarks = 'ONLY IF USING NEGOTIATION PROCEDURES' -- NULL
WHERE clause_id = 101801; -- 252.247-7018

UPDATE Clauses
SET editable_remarks = 'ONLY IF USING NEGOTIATION PROCEDURES' -- NULL
WHERE clause_id = 101802; -- 252.247-7019

UPDATE Clauses
SET editable_remarks = 'ONLY IF USING NEGOTIATION PROCEDURES' -- NULL
WHERE clause_id = 101803; -- 252.247-7020

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101804; -- 252.247-7021

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101805; -- 252.247-7022

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101808; -- 252.247-7023

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101809; -- 252.247-7024

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101810; -- 252.247-7025

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101811; -- 252.247-7026

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101812; -- 252.247-7027

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101813; -- 252.247-7028

UPDATE Clauses
SET is_optional = 1
, is_editable = 0
WHERE clause_id = 101814; -- 252.249-7000

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101815; -- 252.249-7002

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101816; -- 252.251-7000

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101817; -- 252.251-7001

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100610; -- 52.202-1

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100611; -- 52.203-10

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100612; -- 52.203-11

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100613; -- 52.203-12

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100614; -- 52.203-13

UPDATE Clauses
SET editable_remarks = 'Dollar Amount in Paragraph (d) per Agency policy' -- NULL
WHERE clause_id = 100615; -- 52.203-14

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100616; -- 52.203-15

UPDATE Clauses
SET editable_remarks = 'ALL' -- NULL
WHERE clause_id = 100617; -- 52.203-16

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100618; -- 52.203-17

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100619; -- 52.203-2

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100620; -- 52.203-3

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100621; -- 52.203-5

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100623; -- 52.203-6

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100624; -- 52.203-7

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100625; -- 52.203-8

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100626; -- 52.204-10

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100627; -- 52.204-12

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100628; -- 52.204-13

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100629; -- 52.204-14

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100630; -- 52.204-15

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100631; -- 52.204-16

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100632; -- 52.204-17

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100633; -- 52.204-18

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100634; -- 52.204-19

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100637; -- 52.204-2

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100638; -- 52.204-3

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100639; -- 52.204-4

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100640; -- 52.204-5

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100641; -- 52.204-6

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100643; -- 52.204-7

UPDATE Clauses
SET clause_data = '<p>(a)(1) The North American Industry Classification System (NAICS) code for this acquisition is {{textbox_52.204-8[0]}} [<i>insert NAICS code</i>].</p><p>(2) The small business size standard is {{textbox_52.204-8[1]}} [<i>insert size standard</i>].</p><p>(3) The small business size standard for a concern which submits an offer in its own name, other than on a construction or service contract, but which proposes to furnish a product which it did not itself manufacture, is 500 employees.3</p><p>(b)(1) If the provision at 52.204-7, System for Award Management, is included in this solicitation, paragraph (d) of this provision applies.</p><p>(2) If the provision at 52.204-7 is not included in this solicitation, and the offeror is currently registered in the System for Award Management (SAM), and has completed the Representations and Certifications section of SAM electronically, the offeror may choose to use paragraph (d) of this provision instead of completing the corresponding individual representations and certifications in the solicitation. The offeror shall indicate which option applies by checking one of the following boxes:</p><p>{{checkbox_52.204-8[0]}} (i) Paragraph (d) applies.</p><p>{{checkbox_52.204-8[1]}} (ii) Paragraph (d) does not apply and the offeror has completed the individual representations and certifications in the solicitation.</p><p>(c)(1) The following representations or certifications in SAM are applicable to this solicitation as indicated:</p><p>(i) 52.203-2, Certificate of Independent Price Determination. This provision applies to solicitations when a firm-fixed-price contract or fixed-price contract with economic price adjustment is contemplated, unless-</p><p>(A) The acquisition is to be made under the simplified acquisition procedures in Part 13;</p><p>(B) The solicitation is a request for technical proposals under two-step sealed bidding procedures; or</p><p>(C) The solicitation is for utility services for which rates are set by law or regulation.</p><p>(ii) 52.203-11, Certification and Disclosure Regarding Payments to Influence Certain Federal Transactions. This provision applies to solicitations expected to exceed $150,000.</p><p>(iii) 52.204-3, Taxpayer Identification. This provision applies to solicitations that do not include provision at 52.204-7, System for Award Management.</p><p>(iv) 52.204-5, Women-Owned Business (Other Than Small Business). This provision applies to solicitations that-</p><p>(A) Are not set aside for small business concerns;</p><p>(B) Exceed the simplified acquisition threshold; and</p><p>(C) Are for contracts that will be performed in the United States or its outlying areas.</p><p>(v) 52.209-2, Prohibition on Contracting with Inverted Domestic Corporations-Representation.</p><p>(vi) 52.209-5, Certification Regarding Responsibility Matters. This provision applies to solicitations where the contract value is expected to exceed the simplified acquisition threshold.</p><p>(vii) 52.214-14, Place of Performance-Sealed Bidding. This provision applies to invitations for bids except those in which the place of performance is specified by the Government.</p><p>(viii) 52.215-6, Place of Performance. This provision applies to solicitations unless the place of performance is specified by the Government.</p><p>(ix) 52.219-1, Small Business Program Representations (Basic & Alternate I). This provision applies to solicitations when the contract will be performed in the United States or its outlying areas.</p><p>(A) The basic provision applies when the solicitations are issued by other than DoD, NASA, and the Coast Guard.</p><p>(B) The provision with its Alternate I applies to solicitations issued by DoD, NASA, or the Coast Guard.</p><p>(x) 52.219-2, Equal Low Bids. This provision applies to solicitations when contracting by sealed bidding and the contract will be performed in the United States or its outlying areas.</p><p>(xi) 52.222-22, Previous Contracts and Compliance Reports. This provision applies to solicitations that include the clause at 52.222-26, Equal Opportunity.</p><p>(xii) 52.222-25, Affirmative Action Compliance. This provision applies to solicitations, other than those for construction, when the solicitation includes the clause at 52.222-26, Equal Opportunity.</p><p>(xiii) 52.222-38, Compliance with Veterans'' Employment Reporting Requirements. This provision applies to solicitations when it is anticipated the contract award will exceed the simplified acquisition threshold and the contract is not for acquisition of commercial items.</p><p>(xiv) 52.223-1, Biobased Product Certification. This provision applies to solicitations that require the delivery or specify the use of USDA-designated items; or include the clause at 52.223-2, Affirmative Procurement of Biobased Products Under Service and Construction Contracts.</p><p>(xv) 52.223-4, Recovered Material Certification. This provision applies to solicitations that are for, or specify the use of, EPA-designated items.</p><p>(xvi) 52.225-2, Buy American Certificate. This provision applies to solicitations containing the clause at 52.225-1.</p><p>(xvii) 52.225-4, Buy American-Free Trade Agreements-Israeli Trade Act Certificate. (Basic, Alternates I, II, and III.) This provision applies to solicitations containing the clause at 52.225-3.</p><p>(A) If the acquisition value is less than $25,000, the basic provision applies.</p><p>(B) If the acquisition value is $25,000 or more but is less than $50,000, the provision with its Alternate I applies.</p><p>(C) If the acquisition value is $50,000 or more but is less than $79,507, the provision with its Alternate II applies.</p><p>(D) If the acquisition value is $79,507 or more but is less than $100,000, the provision with its Alternate III applies.</p><p>(xviii) 52.225-6, Trade Agreements Certificate. This provision applies to solicitations containing the clause at 52.225-5.</p><p>(xix) 52.225-20, Prohibition on Conducting Restricted Business Operations in Sudan-Certification. This provision applies to all solicitations.</p><p>(xx) 52.225-25, Prohibition on Contracting with Entities Engaging in Certain Activities or Transactions Relating to Iran-Representation and Certifications. This provision applies to all solicitations.</p><p>(xxi) 52.226-2, Historically Black College or University and Minority Institution Representation. This provision applies to solicitations for research, studies, supplies, or services of the type normally acquired from higher educational institutions.</p><p>(2) The following certifications are applicable as indicated by the Contracting Officer:</p><p>[<i>Contracting Officer check as appropriate.</i>]</p><p>{{checkbox_52.204-8[2]}} (i) 52.204-17, Ownership or Control of Offeror.</p><p>{{checkbox_52.204-8[3]}} (ii) 52.222-18, Certification Regarding Knowledge of Child Labor for Listed End Products.</p><p>{{checkbox_52.204-8[4]}} (iii) 52.222-48, Exemption from Application of the Service Contract Labor Standards to Contracts for Maintenance, Calibration, or Repair of Certain Equipment-Certification.</p><p>{{checkbox_52.204-8[5]}} (iv) 52.222-52, Exemption from Application of the Service Contract Labor Standards to Contracts for Certain Services-Certification.</p><p>{{checkbox_52.204-8[6]}} (v) 52.223-9, with its Alternate I, Estimate of Percentage of Recovered Material Content for EPA-Designated Products (Alternate I only).</p><p>{{checkbox_52.204-8[7]}} (vi) 52.227-6, Royalty Information.</p><p>{{checkbox_52.204-8[8]}} (A) Basic.</p><p>{{checkbox_52.204-8[9]}} (B) Alternate I.</p><p>{{checkbox_52.204-8[10]}} (vii) 52.227-15, Representation of Limited Rights Data and Restricted Computer Software.</p><p>(d) The offeror has completed the annual representations and certifications electronically via the SAM Web site accessed through <i>https://www.acquisition.gov.</i> After reviewing the SAM database information, the offeror verifies by submission of the offer that the representations and certifications currently posted electronically that apply to this solicitation as indicated in paragraph (c) of this provision have been entered or updated within the last 12 months, are current, accurate, complete, and applicable to this solicitation (including the business size standard applicable to the NAICS code referenced for this solicitation), as of the date of this offer and are incorporated in this offer by reference (see FAR 4.1201); except for the changes identified below [<i>offeror to insert changes, identifying change by clause number, title, date</i>]. These amended representation(s) and/or certification(s) are also incorporated in this offer and are current, accurate, and complete as of the date of this offer.</p><div><div><table border="1" cellpadding="1" cellspacing="1" frame="void" width="100%"><tbody><tr><th scope="col">FAR Clause No.</th><th scope="col">Title</th><th scope="col">Date</th><th scope="col">Change</th></tr><tr><td>{{textbox_52.204-8[2]}}</td><td>{{textbox_52.204-8[3]}}</td><td>{{textbox_52.204-8[4]}}</td><td>{{textbox_52.204-8[5]}}</td></tr></tbody></table></div></div><p>Any changes provided by the offeror are applicable to this solicitation only, and do not result in an update to the representations and certifications posted on SAM.</p>'
--               '<p>(a)(1) The North American Industry Classification System (NAICS) code for this acquisition is {{textbox_52.204-8[0]}} [<i>insert NAICS code</i>].</p><p>(2) The small business size standard is {{textbox_52.204-8[1]}} [<i>insert size standard</i>].</p><p>(3) The small business size standard for a concern which submits an offer in its own name, other than on a construction or service contract, but which proposes to furnish a product which it did not itself manufacture, is 500 employees.3</p><p>(b)(1) If the provision at 52.204-7, System for Award Management, is included in this solicitation, paragraph (d) of this provision applies.</p><p>(2) If the provision at 52.204-7 is not included in this solicitation, and the offeror is currently registered in the System for Award Management (SAM), and has completed the Representations and Certifications section of SAM electronically, the offeror may choose to use paragraph (d) of this provision instead of completing the corresponding individual representations and certifications in the solicitation. The offeror shall indicate which option applies by checking one of the following boxes:</p><p>{{checkbox_52.204-8[0]}} (i) Paragraph (d) applies.</p><p>{{checkbox_52.204-8[1]}} (ii) Paragraph (d) does not apply and the offeror has completed the individual representations and certifications in the solicitation.</p><p>(c)(1) The following representations or certifications in SAM are applicable to this solicitation as indicated:</p><p>(i) 52.203-2, Certificate of Independent Price Determination. This provision applies to solicitations when a firm-fixed-price contract or fixed-price contract with economic price adjustment is contemplated, unless-</p><p>(A) The acquisition is to be made under the simplified acquisition procedures in Part 13;</p><p>(B) The solicitation is a request for technical proposals under two-step sealed bidding procedures; or</p><p>(C) The solicitation is for utility services for which rates are set by law or regulation.</p><p>(ii) 52.203-11, Certification and Disclosure Regarding Payments to Influence Certain Federal Transactions. This provision applies to solicitations expected to exceed $150,000.</p><p>(iii) 52.204-3, Taxpayer Identification. This provision applies to solicitations that do not include provision at 52.204-7, System for Award Management.</p><p>(iv) 52.204-5, Women-Owned Business (Other Than Small Business). This provision applies to solicitations that-</p><p>(A) Are not set aside for small business concerns;</p><p>(B) Exceed the simplified acquisition threshold; and</p><p>(C) Are for contracts that will be performed in the United States or its outlying areas.</p><p>(v) 52.209-2, Prohibition on Contracting with Inverted Domestic Corporations-Representation.</p><p>(vi) 52.209-5, Certification Regarding Responsibility Matters. This provision applies to solicitations where the contract value is expected to exceed the simplified acquisition threshold.</p><p>(vii) 52.214-14, Place of Performance-Sealed Bidding. This provision applies to invitations for bids except those in which the place of performance is specified by the Government.</p><p>(viii) 52.215-6, Place of Performance. This provision applies to solicitations unless the place of performance is specified by the Government.</p><p>(ix) 52.219-1, Small Business Program Representations (Basic & Alternate I). This provision applies to solicitations when the contract will be performed in the United States or its outlying areas.</p><p>(A) The basic provision applies when the solicitations are issued by other than DoD, NASA, and the Coast Guard.</p><p>(B) The provision with its Alternate I applies to solicitations issued by DoD, NASA, or the Coast Guard.</p><p>(x) 52.219-2, Equal Low Bids. This provision applies to solicitations when contracting by sealed bidding and the contract will be performed in the United States or its outlying areas.</p><p>(xi) 52.222-22, Previous Contracts and Compliance Reports. This provision applies to solicitations that include the clause at 52.222-26, Equal Opportunity.</p><p>(xii) 52.222-25, Affirmative Action Compliance. This provision applies to solicitations, other than those for construction, when the solicitation includes the clause at 52.222-26, Equal Opportunity.</p><p>(xiii) 52.222-38, Compliance with Veterans'' Employment Reporting Requirements. This provision applies to solicitations when it is anticipated the contract award will exceed the simplified acquisition threshold and the contract is not for acquisition of commercial items.</p><p>(xiv) 52.223-1, Biobased Product Certification. This provision applies to solicitations that require the delivery or specify the use of USDA-designated items; or include the clause at 52.223-2, Affirmative Procurement of Biobased Products Under Service and Construction Contracts.</p><p>(xv) 52.223-4, Recovered Material Certification. This provision applies to solicitations that are for, or specify the use of, EPA-designated items.</p><p>(xvi) 52.225-2, Buy American Certificate. This provision applies to solicitations containing the clause at 52.225-1.</p><p>(xvii) 52.225-4, Buy American-Free Trade Agreements-Israeli Trade Act Certificate. (Basic, Alternates I, II, and III.) This provision applies to solicitations containing the clause at 52.225-3.</p><p>(A) If the acquisition value is less than $25,000, the basic provision applies.</p><p>(B) If the acquisition value is $25,000 or more but is less than $50,000, the provision with its Alternate I applies.</p><p>(C) If the acquisition value is $50,000 or more but is less than $79,507, the provision with its Alternate II applies.</p><p>(D) If the acquisition value is $79,507 or more but is less than $100,000, the provision with its Alternate III applies.</p><p>(xviii) 52.225-6, Trade Agreements Certificate. This provision applies to solicitations containing the clause at 52.225-5.</p><p>(xix) 52.225-20, Prohibition on Conducting Restricted Business Operations in Sudan-Certification. This provision applies to all solicitations.</p><p>(xx) 52.225-25, Prohibition on Contracting with Entities Engaging in Certain Activities or Transactions Relating to Iran-Representation and Certifications. This provision applies to all solicitations.</p><p>(xxi) 52.226-2, Historically Black College or University and Minority Institution Representation. This provision applies to solicitations for research, studies, supplies, or services of the type normally acquired from higher educational institutions.</p><p>(2) The following certifications are applicable as indicated by the Contracting Officer:</p><p>[<i>Contracting Officer check as appropriate.</i>]</p><p>{{checkbox_52.204-8[2]}} (i) 52.204-17, Ownership or Control of Offeror.</p><p>{{checkbox_52.204-8[3]}} (i) 52.222-18, Certification Regarding Knowledge of Child Labor for Listed End Products.</p><p>{{checkbox_52.204-8[4]}} (i) 52.222-48, Exemption from Application of the Service Contract Labor Standards to Contracts for Maintenance, Calibration, or Repair of Certain Equipment-Certification.</p><p>{{checkbox_52.204-8[5]}} (v) 52.222-52, Exemption from Application of the Service Contract Labor Standards to Contracts for Certain Services-Certification.</p><p>{{checkbox_52.204-8[6]}} (v) 52.223-9, with its Alternate I, Estimate of Percentage of Recovered Material Content for EPA-Designated Products (Alternate I only).</p><p>{{checkbox_52.204-8[7]}} (i) 52.227-6, Royalty Information.</p><p>{{checkbox_52.204-8[8]}} (A) Basic.</p><p>{{checkbox_52.204-8[9]}} (B) Alternate I.</p><p>{{checkbox_52.204-8[10]}} (i) 52.227-15, Representation of Limited Rights Data and Restricted Computer Software.</p><p>(d) The offeror has completed the annual representations and certifications electronically via the SAM Web site accessed through <i>https://www.acquisition.gov.</i> After reviewing the SAM database information, the offeror verifies by submission of the offer that the representations and certifications currently posted electronically that apply to this solicitation as indicated in paragraph (c) of this provision have been entered or updated within the last 12 months, are current, accurate, complete, and applicable to this solicitation (including the business size standard applicable to the NAICS code referenced for this solicitation), as of the date of this offer and are incorporated in this offer by reference (see FAR 4.1201); except for the changes identified below [<i>offeror to insert changes, identifying change by clause number, title, date</i>]. These amended representation(s) and/or certification(s) are also incorporated in this offer and are current, accurate, and complete as of the date of this offer.</p><div><div><table border="1" cellpadding="1" cellspacing="1" frame="void" width="100%"><tbody><tr><th scope="col">FAR Clause No.</th><th scope="col">Title</th><th scope="col">Date</th><th scope="col">Change</th></tr><tr><td>{{textbox_52.204-8[2]}}</td><td>{{textbox_52.204-8[3]}}</td><td>{{textbox_52.204-8[4]}}</td><td>{{textbox_52.204-8[5]}}</td></tr></tbody></table></div></div><p>Any changes provided by the offeror are applicable to this solicitation only, and do not result in an update to the representations and certifications posted on SAM.</p>'
WHERE clause_id = 100644; -- 52.204-8

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100645; -- 52.204-9

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100646; -- 52.207-1

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100647; -- 52.207-2

UPDATE Clauses
SET editable_remarks = 'Only Paragraph (b) The 10-day period in the clause may be varied by the contracting officer up to a period of 90 days.' -- NULL
WHERE clause_id = 100648; -- 52.207-3

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100649; -- 52.207-4

UPDATE Clauses
SET editable_remarks = 'ALL' -- NULL
WHERE clause_id = 100650; -- 52.207-5

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100651; -- 52.208-4

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100652; -- 52.208-5

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100653; -- 52.208-6

UPDATE Clauses
SET editable_remarks = 'ALL' -- NULL
WHERE clause_id = 100654; -- 52.208-7

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100655; -- 52.208-8

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100656; -- 52.208-9

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100657; -- 52.209-1

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100658; -- 52.209-10

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100659; -- 52.209-2

UPDATE Clauses
SET editable_remarks = 'ALL - COST REIMBURSEMENT ONLY' -- NULL
WHERE clause_id = 100662; -- 52.209-3

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100665; -- 52.209-4

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100666; -- 52.209-5

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100667; -- 52.209-6

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100668; -- 52.209-7

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100669; -- 52.209-9

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100670; -- 52.210-1

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100671; -- 52.211-1

UPDATE Clauses
SET editable_remarks = 'ALL\nThe clause may be changed to accommodate the issuance of orders under indefinite-delivery contracts' -- NULL
WHERE clause_id = 100673; -- 52.211-10

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100674; -- 52.211-11

UPDATE Clauses
SET editable_remarks = 'PARAGRAPH (a) ONLY' -- NULL
WHERE clause_id = 100675; -- 52.211-12

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100676; -- 52.211-13

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100677; -- 52.211-14

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100678; -- 52.211-15

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100679; -- 52.211-16

UPDATE Clauses
SET is_optional = 1
, is_editable = 0
WHERE clause_id = 100680; -- 52.211-17

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100681; -- 52.211-18

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100682; -- 52.211-2

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100683; -- 52.211-3

UPDATE Clauses
SET editable_remarks = 'ALL' -- NULL
WHERE clause_id = 100684; -- 52.211-4
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size, fill_in_group_number, fill_in_placeholder, fill_in_default_data, fill_in_display_rows, fill_in_for_table, fill_in_heading) VALUES (100684, 'textbox_52.211-4[6]', 'S', 100, NULL, NULL, NULL, NULL, 0, NULL);

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100685; -- 52.211-5

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100686; -- 52.211-6

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100687; -- 52.211-7

UPDATE Clauses
SET is_optional = 1
, editable_remarks = 'ALL' -- NULL
WHERE clause_id = 100691; -- 52.211-8

UPDATE Clauses
SET is_optional = 1
, editable_remarks = 'ALL' -- NULL
WHERE clause_id = 100695; -- 52.211-9

UPDATE Clauses
SET editable_remarks = 'ALL' -- NULL
WHERE clause_id = 100696; -- 52.212-1

UPDATE Clauses
SET is_optional = 1
, editable_remarks = 'ALL' -- NULL
WHERE clause_id = 100697; -- 52.212-2

UPDATE Clauses
SET editable_remarks = 'ONLY IF DEVIATION OBTAINED' -- NULL
WHERE clause_id = 100699; -- 52.212-3

UPDATE Clauses
SET editable_remarks = 'COMMERCIAL ITEMS ONLY' -- NULL
WHERE clause_id = 100701; -- 52.212-4

UPDATE Clauses
SET is_editable = 0
, clause_data = '<p>(a) The Contractor shall comply with the following Federal Acquisition Regulation (FAR) clauses, which are incorporated in this contract by reference, to implement provisions of law or Executive orders applicable to acquisitions of commercial items:</p><p>(1) 52.209-10, Prohibition on Contracting with Inverted Domestic Corporations (Dec 2014)</p><p>(2) 52.233-3, Protest After Award (AUG 1996) (31 U.S.C. 3553).</p><p>(3) 52.233-4, Applicable Law for Breach of Contract Claim (OCT 2004) (Public Laws 108-77 and 108-78 (19 U.S.C. 3805 note)).</p><p>(b) The Contractor shall comply with the FAR clauses in this paragraph (b) that the Contracting Officer has indicated as being incorporated in this contract by reference to implement provisions of law or Executive orders applicable to acquisitions of commercial items: [Contracting Officer check as appropriate.]</p><p>{{checkbox_52.212-5[0]}} (1) 52.203-6, Restrictions on Subcontractor Sales to the Government (SEP 2006), with <i>Alternate I</i> (OCT 1995) (41 U.S.C. 4704 and 10 U.S.C. 2402).</p><p>{{checkbox_52.212-5[1]}} (2) 52.203-13, Contractor Code of Business Ethics and Conduct (APR 2010) (41 U.S.C. 3509).</p><p>{{checkbox_52.212-5[2]}} (3) 52.203-15, Whistleblower Protections under the American Recovery and Reinvestment Act of 2009 (JUN 2010) (Section 1553 of Pub. L. 111-5). (Applies to contracts funded by the American Recovery and Reinvestment Act of 2009.)</p><p>{{checkbox_52.212-5[3]}} (4) 52.204-10, Reporting Executive Compensation and First-Tier Subcontract Awards (JUL 2013) (Pub. L. 109-282) (31 U.S.C. 6101 note).</p><p>{{checkbox_52.212-5[4]}} (5) [Reserved]</p><p>{{checkbox_52.212-5[5]}} (6) 52.204-14, Service Contract Reporting Requirements (JAN 2014) (Pub. L. 111-117, section 743 of Div. C).</p><p>{{checkbox_52.212-5[6]}} (7) 52.204-15, Service Contract Reporting Requirements for Indefinite-Delivery Contracts (JAN 2014) (Pub. L. 111-117, section 743 of Div. C).</p><p>{{checkbox_52.212-5[7]}} (8) 52.209-6, Protecting the Government''s Interest When Subcontracting with Contractors Debarred, Suspended, or Proposed for Debarment. (AUG 2013) (31 U.S.C. 6101 note).</p><p>{{checkbox_52.212-5[8]}} (9) 52.209-9, Updates of Publicly Available Information Regarding Responsibility Matters (JUL 2013) (41 U.S.C. 2313).</p><p>{{checkbox_52.212-5[9]}} (10) [Reserved]</p><p>{{checkbox_52.212-5[10]}} (11)(i) 52.219-3, Notice of HUBZone Set-Aside or Sole-Source Award (NOV 2011) (15 U.S.C. 657a).</p><p>{{checkbox_52.212-5[11]}} (ii) Alternate I (NOV 2011) of 52.219-3.</p><p>{{checkbox_52.212-5[12]}} (12)(i) 52.219-4, Notice of Price Evaluation Preference for HUBZone Small Business Concerns (OCT 2014) (if the offeror elects to waive the preference, it shall so indicate in its offer) (15 U.S.C. 657a).</p><p>{{checkbox_52.212-5[13]}} (ii) Alternate I (JAN 2011) of 52.219-4.</p><p>{{checkbox_52.212-5[14]}} (13) [Reserved]</p><p>{{checkbox_52.212-5[15]}} (14)(i) 52.219-6, Notice of Total Small Business Set-Aside (NOV 2011) (15 U.S.C. 644).</p><p>{{checkbox_52.212-5[16]}} (ii) Alternate I (NOV 2011).</p><p>{{checkbox_52.212-5[17]}} (iii) Alternate II (NOV 2011).</p><p>{{checkbox_52.212-5[18]}} (15)(i) 52.219-7, Notice of Partial Small Business Set-Aside (JUN 2003) (15 U.S.C. 644). </p><p>{{checkbox_52.212-5[19]}} (ii) <i>Alternate I</i> (OCT 1995) of 52.219-7. </p><p>{{checkbox_52.212-5[20]}} (iii) <i>Alternate II</i> (MAR 2004) of 52.219-7. </p><p>{{checkbox_52.212-5[21]}} (16) 52.219-8, Utilization of Small Business Concerns (OCT 2014) (15 U.S.C. 637(d)(2) and (3)).</p><p>{{checkbox_52.212-5[22]}} (17)(i) 52.219-9, Small Business Subcontracting Plan (OCT 2014) (15 U.S.C. 637(d)(4)).</p><p>{{checkbox_52.212-5[23]}} (ii) <i>Alternate I</i> (OCT 2001) of 52.219-9. </p><p>{{checkbox_52.212-5[24]}} (iii) <i>Alternate II</i> (OCT 2001) of 52.219-9. </p><p>{{checkbox_52.212-5[25]}} (18) 52.219-13, Notice of Set-Aside of Orders (NOV 2011) (15 U.S.C. 644(r)).</p><p>{{checkbox_52.212-5[26]}} (19) <i>52.219-14,</i> Limitations on Subcontracting (NOV 2011) (15 U.S.C. 637(a)(14)).</p><p>{{checkbox_52.212-5[27]}} (20) 52.219-16, Liquidated Damages-Subcontracting Plan (JAN 1999) (15 U.S.C. 637(d)(4)(F)(i)).</p><p>{{checkbox_52.212-5[28]}} (21) <i>52.219-27,</i> Notice of Service-Disabled Veteran-Owned Small Business Set-Aside (NOV 2011) (15 U.S.C. 657f).</p><p>{{checkbox_52.212-5[29]}} (22) 52.219-28, Post Award Small Business Program Rerepresentation (JUL 2013) (15 U.S.C. 632(a)(2)).</p><p>{{checkbox_52.212-5[30]}} (23) 52.219-29, Notice of Set-Aside for Economically Disadvantaged Women-Owned Small Business (EDWOSB) Concerns (JUL 2013) (15 U.S.C. 637(m)).</p><p>{{checkbox_52.212-5[31]}} (24) 52.219-30, Notice of Set-Aside for Women-Owned Small Business (WOSB) Concerns Eligible Under the WOSB Program (JUL 2013) (15 U.S.C. 637(m)).</p><p>{{checkbox_52.212-5[32]}} (25) 52.222-3, Convict Labor (JUN 2003) (E.O. 11755).</p><p>{{checkbox_52.212-5[33]}} (26) 52.222-19, Child Labor-Cooperation with Authorities and Remedies (JAN 2014) (E.O. 13126).</p><p>{{checkbox_52.212-5[34]}} (27) 52.222-21, Prohibition of Segregated Facilities (APR 2015).</p><p>{{checkbox_52.212-5[35]}} (28) 52.222-26, Equal Opportunity (APR 2015) (E.O. 11246).</p><p>{{checkbox_52.212-5[36]}} (29) 52.222-35, Equal Opportunity for Veterans (July 2014) (38 U.S.C. 4212).</p><p>{{checkbox_52.212-5[37]}} (30) 52.222-36, Equal Opportunity for Workers with Disabilities (July 2014) (29 U.S.C. 793).</p><p>{{checkbox_52.212-5[38]}} (31) 52.222-37, Employment Reports on Veterans (July 2014) (38 U.S.C. 4212).</p><p>{{checkbox_52.212-5[39]}} (32) 52.222-40, Notification of Employee Rights Under the National Labor Relations Act (DEC 2010) (E.O. 13496).</p><p> {{checkbox_52.212-5[40]}} (33)(i) 52.222-50, Combating Trafficking in Persons (Mar 2015) (22 U.S.C. chapter 78 and E.O. 13627).</p><p>{{checkbox_52.212-5[41]}} (ii) <i>Alternate I</i> (Mar 2015) of 52.222-50 (22 U.S.C. chapter 78 and E.O. 13627).</p><p>{{checkbox_52.212-5[42]}} (34) 52.222-54, Employment Eligibility Verification (AUG 2013). (Executive Order 12989). (Not applicable to the acquisition of commercially available off-the-shelf items or certain other types of commercial items as prescribed in 22.1803.)</p><p>{{checkbox_52.212-5[43]}} (35)(i) 52.223-9, Estimate of Percentage of Recovered Material Content for EPA-Designated Items (MAY 2008) (42 U.S.C. 6962(c)(3)(A)(ii)). (Not applicable to the acquisition of commercially available off-the-shelf items.)</p><p>{{checkbox_52.212-5[44]}} (ii) Alternate I (MAY 2008) of 52.223-9 (42 U.S.C. 6962(i)(2)(C)). (Not applicable to the acquisition of commercially available off-the-shelf items.)</p><p>{{checkbox_52.212-5[45]}} (36)(i) 52.223-13, Acquisition of EPEAT&reg;-Registered Imaging Equipment (JUN 2014) (E.O.s 13423 and 13514).</p><p>{{checkbox_52.212-5[46]}} (ii) Alternate I (JUN 2014) of 52.223-13.</p><p>{{checkbox_52.212-5[47]}} (37)(i) 52.223-14, Acquisition of EPEAT&reg;-Registered Televisions (Jun 2014) (E.O.s 13423 and 13514).</p><p>(ii) Alternate I (Jun 2014) of 52.223-14.</p><p>{{checkbox_52.212-5[48]}} (38) 52.223-15, Energy Efficiency in Energy-Consuming Products (DEC 2007) (42 U.S.C. 8259b).</p><p>{{checkbox_52.212-5[49]}} (39)(i) 52.223-16, Acquisition of EPEAT&reg;-Registered Personal Computer Products (Jun 2014) (E.O.s 13423 and 13514).</p><p>{{checkbox_52.212-5[50]}} (ii) Alternate I (Jun 2014) of 52.223-16.</p><p>{{checkbox_52.212-5[51]}} (40) 52.223-18, Encouraging Contractor Policies to Ban Text Messaging While Driving (AUG 2011)</p><p>{{checkbox_52.212-5[52]}} (41) 52.225-1, Buy American-Supplies (<b>MAY 2014</b>) (41 U.S.C. chapter 83).</p><p>{{checkbox_52.212-5[53]}} (42)(i) 52.225-3, Buy American-Free Trade Agreements-Israeli Trade Act (<b>MAY 2014</b>) (41 U.S.C. chapter 83, 19 U.S.C. 3301 note, 19 U.S.C. 2112 note, 19 U.S.C. 3805 note, 19 U.S.C. 4001 note, Pub. L. 103-182, 108-77, 108-78, 108-286, 108-302, 109-53, 109-169, 109-283, 110-138, 112-41, 112-42, and 112-43.</p><p>{{checkbox_52.212-5[54]}} (ii) Alternate I (<b>MAY 2014</b>) of 52.225-3.</p><p>{{checkbox_52.212-5[55]}} (iii) Alternate II (<b>MAY 2014</b>) of 52.225-3.</p><p>{{checkbox_52.212-5[56]}} (iv) Alternate III (<b>MAY 2014</b>) of 52.225-3.</p><p>{{checkbox_52.212-5[57]}} (43) 52.225-5, Trade Agreements (NOV 2013) (19 U.S.C. 2501, <i>et seq.</i>, 19 U.S.C. 3301 note).</p><p>{{checkbox_52.212-5[58]}} (44) 52.225-13, Restrictions on Certain Foreign Purchases (JUN 2008) (E.O.''s, proclamations, and statutes administered by the Office of Foreign Assets Control of the Department of the Treasury).</p><p>{{checkbox_52.212-5[59]}} (45) 52.225-26, Contractors Performing Private Security Functions Outside the United States (JUL 2013) (Section 862, as amended, of the National Defense Authorization Act for Fiscal Year 2008; 10 U.S.C. 2302 Note).</p><p>{{checkbox_52.212-5[60]}} (46) 52.226-4, Notice of Disaster or Emergency Area Set-Aside (NOV 2007) (42 U.S.C. 5150).</p><p>{{checkbox_52.212-5[61]}} (47) 52.226-5, Restrictions on Subcontracting Outside Disaster or Emergency Area (NOV 2007) (42 U.S.C. 5150).</p><p>{{checkbox_52.212-5[62]}} (48) 52.232-29, Terms for Financing of Purchases of Commercial Items (FEB 2002) (41 U.S.C.4505, 10 U.S.C. 2307(f)).</p><p>{{checkbox_52.212-5[63]}} (49) 52.232-30, Installment Payments for Commercial Items (OCT 1995) (41 U.S.C.4505, 10 U.S.C. 2307(f)).</p><p>{{checkbox_52.212-5[64]}} (50) 52.232-33, Payment by Electronic Funds Transfer-System for Award Management (JUL 2013) (31 U.S.C. 3332).</p><p>{{checkbox_52.212-5[65]}} (51) 52.232-34, Payment by Electronic Funds Transfer-Other than System for Award Management (JUL 2013) (31 U.S.C. 3332).</p><p>{{checkbox_52.212-5[66]}} (52) 52.232-36, Payment by Third Party (MAY 2014) (31 U.S.C. 3332).</p><p>{{checkbox_52.212-5[67]}} (53) 52.239-1, Privacy or Security Safeguards (AUG 1996) (5 U.S.C. 552a).</p><p>{{checkbox_52.212-5[68]}} (54)(i) 52.247-64, Preference for Privately Owned U.S.-Flag Commercial Vessels (FEB 2006) (46 U.S.C. Appx. 1241(b) and 10 U.S.C. 2631).</p><p>{{checkbox_52.212-5[69]}} (ii) <i>Alternate I</i> (APR 2003) of 52.247-64.</p><p>(c) The Contractor shall comply with the FAR clauses in this paragraph (c), applicable to commercial services, that the Contracting Officer has indicated as being incorporated in this contract by reference to implement provisions of law or Executive orders applicable to acquisitions of commercial items: [Contracting Officer check as appropriate.]</p><p>{{checkbox_52.212-5[70]}} (1) 52.222-17, Nondisplacement of Qualified Workers (May 2014) (E.O. 13495).</p><p>{{checkbox_52.212-5[71]}} (2) 52.222-41, Service Contract Labor Standards (MAY 2014) (41 U.S.C. chapter 67).</p><p>{{checkbox_52.212-5[72]}} (3) 52.222-42, Statement of Equivalent Rates for Federal Hires (MAY 2014) (29 U.S.C. 206 and 41 U.S.C. chapter 67).</p><p>{{checkbox_52.212-5[73]}} (4) 52.222-43, Fair Labor Standards Act and Service Contract Labor Standards-Price Adjustment (Multiple Year and Option Contracts) (MAY 2014) (29 U.S.C. 206 and 41 U.S.C. chapter 67).</p><p>{{checkbox_52.212-5[74]}} (5) 52.222-44, Fair Labor Standards Act and Service Contract Labor Standards-Price Adjustment (MAY 2014) (29 U.S.C 206 and 41 U.S.C. chapter 67).</p><p>{{checkbox_52.212-5[75]}} (6) 52.222-51, Exemption from Application of the Service Contract Labor Standards to Contracts for Maintenance, Calibration, or Repair of Certain Equipment-Requirements (MAY 2014) (41 U.S.C. chapter 67).</p><p>{{checkbox_52.212-5[76]}} (7) 52.222-53, Exemption from Application of the Service Contract Labor Standards to Contracts for Certain Services-Requirements (MAY 2014) (41 U.S.C. chapter 67).</p><p>{{checkbox_52.212-5[77]}} (8) 52.222-55, Minimum Wages Under Executive Order 13658 (Dec 2014) (E.O. 13658).</p><p>{{checkbox_52.212-5[78]}} (9) 52.226-6, Promoting Excess Food Donation to Nonprofit Organizations (MAY 2014) (42 U.S.C. 1792).</p><p>{{checkbox_52.212-5[79]}} (10) 52.237-11, Accepting and Dispensing of $1 Coin (SEP 2008) (31 U.S.C. 5112(p)(1)).</p><p>(d) <i>Comptroller General Examination of Record.</i> The Contractor shall comply with the provisions of this paragraph (d) if this contract was awarded using other than sealed bid, is in excess of the simplified acquisition threshold, and does not contain the clause at 52.215-2, Audit and Records-Negotiation.</p><p>(1) The Comptroller General of the United States, or an authorized representative of the Comptroller General, shall have access to and right to examine any of the Contractor''s directly pertinent records involving transactions related to this contract.</p><p>(2) The Contractor shall make available at its offices at all reasonable times the records, materials, and other evidence for examination, audit, or reproduction, until 3 years after final payment under this contract or for any shorter period specified in FAR Subpart 4.7, Contractor Records Retention, of the other clauses of this contract. If this contract is completely or partially terminated, the records relating to the work terminated shall be made available for 3 years after any resulting final termination settlement. Records relating to appeals under the disputes clause or to litigation or the settlement of claims arising under or relating to this contract shall be made available until such appeals, litigation, or claims are finally resolved.</p><p>(3) As used in this clause, records include books, documents, accounting procedures and practices, and other data, regardless of type and regardless of form. This does not require the Contractor to create or maintain any record that the Contractor does not maintain in the ordinary course of business or pursuant to a provision of law.</p><p>(e)(1) Notwithstanding the requirements of the clauses in paragraphs (a), (b), (c), and (d) of this clause, the Contractor is not required to flow down any FAR clause, other than those in this paragraph (e)(1) of this paragraph in a subcontract for commercial items. Unless otherwise indicated below, the extent of the flow down shall be as required by the clause-</p><p>(i) 52.203-13, Contractor Code of Business Ethics and Conduct (APR 2010) (41 U.S.C. 3509).</p><p>(ii) 52.219-8, Utilization of Small Business Concerns (OCT 2014) (15 U.S.C. 637(d)(2) and (3)), in all subcontracts that offer further subcontracting opportunities. If the subcontract (except subcontracts to small business concerns) exceeds $650,000 ($1.5 million for construction of any public facility), the subcontractor must include 52.219-8 in lower tier subcontracts that offer subcontracting opportunities.</p><p>(iii) 52.222-17, Nondisplacement of Qualified Workers (MAY 2014) (E.O. 13495). Flow down required in accordance with paragraph (l) of FAR clause 52.222-17.</p><p>(iv) 52.222-21, Prohibition of Segregated Facilities (APR 2015).</p><p>(v) 52.222-26, Equal Opportunity (APR 2015) (E.O. 11246).</p><p>(vi) 52.222-35, Equal Opportunity for Veterans (July 2014) (38 U.S.C. 4212).</p><p>(vii) 52.222-36, Equal Opportunity for Workers with Disabilities (July 2014) (29 U.S.C. 793).</p><p>(viii) 52.222-37, Employment Reports on Veterans (July 2014) (38 U.S.C. 4212).</p><p>(ix) 52.222-40, Notification of Employee Rights Under the National Labor Relations Act (DEC 2010) (E.O. 13496). Flow down required in accordance with paragraph (f) of FAR clause 52.222-40.</p><p>(x) 52.222-41, Service Contract Labor Standards (MAY 2014) (41 U.S.C. chapter 67).</p><p>(xi) {{checkbox_52.212-5[80]}} (A) 52.222-50, Combating Trafficking in Persons (Mar 2015) (22 U.S.C. chapter 78 and E.O. 13627).</p><p>{{checkbox_52.212-5[81]}} (B) Alternate I (Mar 2015) of 52.222-50 (22 U.S.C. chapter 78 and E.O. 13627).</p><p>{{checkbox_52.212-5[82]}} Alternate I (AUG 2007) of 52.222-50 (22 U.S.C. 7104(g)).</p><p>(xii) 52.222-51, Exemption from Application of the Service Contract Labor Standards to Contracts for Maintenance, Calibration, or Repair of Certain Equipment-Requirements (MAY 2014) (41 U.S.C. chapter 67).</p><p>(xiii) 52.222-53, Exemption from Application of the Service Contract Labor Standards to Contracts for Certain Services-Requirements (MAY 2014) (41 U.S.C. chapter 67).</p><p>(xiv) 52.222-54, Employment Eligibility Verification (AUG 2013).</p><p>(xv) 52.222-55, Minimum Wages Under Executive Order 13658 (Dec 2014) (E.O. 13658).</p><p>(xvi) 52.225-26, Contractors Performing Private Security Functions Outside the United States (JUL 2013) (Section 862, as amended, of the National Defense Authorization Act for Fiscal Year 2008; 10 U.S.C. 2302 Note).</p><p>(xvii) 52.226-6, Promoting Excess Food Donation to Nonprofit Organizations (MAY 2014) (42 U.S.C. 1792). Flow down required in accordance with paragraph (e) of FAR clause 52.226-6.</p><p>(xviii) 52.247-64, Preference for Privately Owned U.S.-Flag Commercial Vessels (FEB 2006) (46 U.S.C. Appx. 1241(b) and 10 U.S.C. 2631). Flow down required in accordance with paragraph (d) of FAR clause 52.247-64.</p><p>(2) While not required, the contractor May include in its subcontracts for commercial items a minimal number of additional clauses necessary to satisfy its contractual obligations.</p>'
--               '<p>(a) The Contractor shall comply with the following Federal Acquisition Regulation (FAR) clauses, which are incorporated in this contract by reference, to implement provisions of law or Executive orders applicable to acquisitions of commercial items:</p><p>(1) 52.209-10, Prohibition on Contracting with Inverted Domestic Corporations (Dec 2014)</p><p>(2) 52.233-3, Protest After Award (AUG 1996) (31 U.S.C. 3553).</p><p>(3) 52.233-4, Applicable Law for Breach of Contract Claim (OCT 2004) (Public Laws 108-77 and 108-78 (19 U.S.C. 3805 note)).</p><p>(b) The Contractor shall comply with the FAR clauses in this paragraph (b) that the Contracting Officer has indicated as being incorporated in this contract by reference to implement provisions of law or Executive orders applicable to acquisitions of commercial items: [Contracting Officer check as appropriate.]</p><p>{{checkbox_52.212-5[0]}} (1) 52.203-6, Restrictions on Subcontractor Sales to the Government (SEP 2006), with <i>Alternate I</i> (OCT 1995) (41 U.S.C. 4704 and 10 U.S.C. 2402).</p><p>{{checkbox_52.212-5[1]}} (2) 52.203-13, Contractor Code of Business Ethics and Conduct (APR 2010) (41 U.S.C. 3509).</p><p>{{checkbox_52.212-5[2]}} (3) 52.203-15, Whistleblower Protections under the American Recovery and Reinvestment Act of 2009 (JUN 2010) (Section 1553 of Pub. L. 111-5). (Applies to contracts funded by the American Recovery and Reinvestment Act of 2009.)</p><p>{{checkbox_52.212-5[3]}} (4) 52.204-10, Reporting Executive Compensation and First-Tier Subcontract Awards (JUL 2013) (Pub. L. 109-282) (31 U.S.C. 6101 note).</p><p>{{checkbox_52.212-5[4]}} (5) [Reserved]</p><p>{{checkbox_52.212-5[5]}} (6) 52.204-14, Service Contract Reporting Requirements (JAN 2014) (Pub. L. 111-117, section 743 of Div. C).</p><p>{{checkbox_52.212-5[6]}} (7) 52.204-15, Service Contract Reporting Requirements for Indefinite-Delivery Contracts (JAN 2014) (Pub. L. 111-117, section 743 of Div. C).</p><p>{{checkbox_52.212-5[7]}} (8) 52.209-6, Protecting the Government''s Interest When Subcontracting with Contractors Debarred, Suspended, or Proposed for Debarment. (AUG 2013) (31 U.S.C. 6101 note).</p><p>{{checkbox_52.212-5[8]}} (9) 52.209-9, Updates of Publicly Available Information Regarding Responsibility Matters (JUL 2013) (41 U.S.C. 2313).</p><p>{{checkbox_52.212-5[9]}} (0) [Reserved]</p><p>{{checkbox_52.212-5[10]}} (1)(i) 52.219-3, Notice of HUBZone Set-Aside or Sole-Source Award (NOV 2011) (15 U.S.C. 657a).</p><p>{{checkbox_52.212-5[11]}} (i) Alternate I (NOV 2011) of 52.219-3.</p><p>{{checkbox_52.212-5[12]}} (2)(i) 52.219-4, Notice of Price Evaluation Preference for HUBZone Small Business Concerns (OCT 2014) (if the offeror elects to waive the preference, it shall so indicate in its offer) (15 U.S.C. 657a).</p><p>{{checkbox_52.212-5[13]}} (i) Alternate I (JAN 2011) of 52.219-4.</p><p>{{checkbox_52.212-5[14]}} (3) [Reserved]</p><p>{{checkbox_52.212-5[15]}} (4)(i) 52.219-6, Notice of Total Small Business Set-Aside (NOV 2011) (15 U.S.C. 644).</p><p>{{checkbox_52.212-5[16]}} (i) Alternate I (NOV 2011).</p><p>{{checkbox_52.212-5[17]}} (i) Alternate II (NOV 2011).</p><p>{{checkbox_52.212-5[18]}} (5)(i) 52.219-7, Notice of Partial Small Business Set-Aside (JUN 2003) (15 U.S.C. 644). </p><p>{{checkbox_52.212-5[19]}} (i) <i>Alternate I</i> (OCT 1995) of 52.219-7. </p><p>{{checkbox_52.212-5[20]}} (i) <i>Alternate II</i> (MAR 2004) of 52.219-7. </p><p>{{checkbox_52.212-5[21]}} (6) 52.219-8, Utilization of Small Business Concerns (OCT 2014) (15 U.S.C. 637(d)(2) and (3)).</p><p>{{checkbox_52.212-5[22]}} (7)(i) 52.219-9, Small Business Subcontracting Plan (OCT 2014) (15 U.S.C. 637(d)(4)).</p><p>{{checkbox_52.212-5[23]}} (i) <i>Alternate I</i> (OCT 2001) of 52.219-9. </p><p>{{checkbox_52.212-5[24]}} (i) <i>Alternate II</i> (OCT 2001) of 52.219-9. </p><p>{{checkbox_52.212-5[25]}} (8) 52.219-13, Notice of Set-Aside of Orders (NOV 2011) (15 U.S.C. 644(r)).</p><p>{{checkbox_52.212-5[26]}} (9) <i>52.219-14,</i> Limitations on Subcontracting (NOV 2011) (15 U.S.C. 637(a)(14)).</p><p>{{checkbox_52.212-5[27]}} (0) 52.219-16, Liquidated Damages-Subcontracting Plan (JAN 1999) (15 U.S.C. 637(d)(4)(F)(i)).</p><p>{{checkbox_52.212-5[28]}} (1) <i>52.219-27,</i> Notice of Service-Disabled Veteran-Owned Small Business Set-Aside (NOV 2011) (15 U.S.C. 657f).</p><p>{{checkbox_52.212-5[29]}} (2) 52.219-28, Post Award Small Business Program Rerepresentation (JUL 2013) (15 U.S.C. 632(a)(2)).</p><p>{{checkbox_52.212-5[30]}} (3) 52.219-29, Notice of Set-Aside for Economically Disadvantaged Women-Owned Small Business (EDWOSB) Concerns (JUL 2013) (15 U.S.C. 637(m)).</p><p>{{checkbox_52.212-5[31]}} (4) 52.219-30, Notice of Set-Aside for Women-Owned Small Business (WOSB) Concerns Eligible Under the WOSB Program (JUL 2013) (15 U.S.C. 637(m)).</p><p>{{checkbox_52.212-5[32]}} (5) 52.222-3, Convict Labor (JUN 2003) (E.O. 11755).</p><p>{{checkbox_52.212-5[33]}} (6) 52.222-19, Child Labor-Cooperation with Authorities and Remedies (JAN 2014) (E.O. 13126).</p><p>{{checkbox_52.212-5[34]}} (7) 52.222-21, Prohibition of Segregated Facilities (APR 2015).</p><p>{{checkbox_52.212-5[35]}} (8) 52.222-26, Equal Opportunity (APR 2015) (E.O. 11246).</p><p>{{checkbox_52.212-5[36]}} (9) 52.222-35, Equal Opportunity for Veterans (July 2014) (38 U.S.C. 4212).</p><p>{{checkbox_52.212-5[37]}} (0) 52.222-36, Equal Opportunity for Workers with Disabilities (July 2014) (29 U.S.C. 793).</p><p>{{checkbox_52.212-5[38]}} (1) 52.222-37, Employment Reports on Veterans (July 2014) (38 U.S.C. 4212).</p><p>{{checkbox_52.212-5[39]}} (2) 52.222-40, Notification of Employee Rights Under the National Labor Relations Act (DEC 2010) (E.O. 13496).</p><p> {{checkbox_52.212-5[40]}} (3)(i) 52.222-50, Combating Trafficking in Persons (Mar 2015) (22 U.S.C. chapter 78 and E.O. 13627).</p><p>{{checkbox_52.212-5[41]}} (i) <i>Alternate I</i> (Mar 2015) of 52.222-50 (22 U.S.C. chapter 78 and E.O. 13627).</p><p>{{checkbox_52.212-5[42]}} (4) 52.222-54, Employment Eligibility Verification (AUG 2013). (Executive Order 12989). (Not applicable to the acquisition of commercially available off-the-shelf items or certain other types of commercial items as prescribed in 22.1803.)</p><p>{{checkbox_52.212-5[43]}} (5)(i) 52.223-9, Estimate of Percentage of Recovered Material Content for EPA-Designated Items (MAY 2008) (42 U.S.C. 6962(c)(3)(A)(ii)). (Not applicable to the acquisition of commercially available off-the-shelf items.)</p><p>{{checkbox_52.212-5[44]}} (i) Alternate I (MAY 2008) of 52.223-9 (42 U.S.C. 6962(i)(2)(C)). (Not applicable to the acquisition of commercially available off-the-shelf items.)</p><p>{{checkbox_52.212-5[45]}} (6)(i) 52.223-13, Acquisition of EPEAT&reg;-Registered Imaging Equipment (JUN 2014) (E.O.s 13423 and 13514).</p><p>{{checkbox_52.212-5[46]}} (i) Alternate I (JUN 2014) of 52.223-13.</p><p>{{checkbox_52.212-5[47]}} (7)(i) 52.223-14, Acquisition of EPEAT&reg;-Registered Televisions (Jun 2014) (E.O.s 13423 and 13514).</p><p>(ii) Alternate I (Jun 2014) of 52.223-14.</p><p>{{checkbox_52.212-5[48]}} (8) 52.223-15, Energy Efficiency in Energy-Consuming Products (DEC 2007) (42 U.S.C. 8259b).</p><p>{{checkbox_52.212-5[49]}} (9)(i) 52.223-16, Acquisition of EPEAT&reg;-Registered Personal Computer Products (Jun 2014) (E.O.s 13423 and 13514).</p><p>{{checkbox_52.212-5[50]}} (i) Alternate I (Jun 2014) of 52.223-16.</p><p>{{checkbox_52.212-5[51]}} (0) 52.223-18, Encouraging Contractor Policies to Ban Text Messaging While Driving (AUG 2011)</p><p>{{checkbox_52.212-5[52]}} (1) 52.225-1, Buy American-Supplies (<b>MAY 2014</b>) (41 U.S.C. chapter 83).</p><p>{{checkbox_52.212-5[53]}} (2)(i) 52.225-3, Buy American-Free Trade Agreements-Israeli Trade Act (<b>MAY 2014</b>) (41 U.S.C. chapter 83, 19 U.S.C. 3301 note, 19 U.S.C. 2112 note, 19 U.S.C. 3805 note, 19 U.S.C. 4001 note, Pub. L. 103-182, 108-77, 108-78, 108-286, 108-302, 109-53, 109-169, 109-283, 110-138, 112-41, 112-42, and 112-43.</p><p>{{checkbox_52.212-5[54]}} (i) Alternate I (<b>MAY 2014</b>) of 52.225-3.</p><p>{{checkbox_52.212-5[55]}} (i) Alternate II (<b>MAY 2014</b>) of 52.225-3.</p><p>{{checkbox_52.212-5[56]}} (v) Alternate III (<b>MAY 2014</b>) of 52.225-3.</p><p>{{checkbox_52.212-5[57]}} (3) 52.225-5, Trade Agreements (NOV 2013) (19 U.S.C. 2501, <i>et seq.</i>, 19 U.S.C. 3301 note).</p><p>{{checkbox_52.212-5[58]}} (4) 52.225-13, Restrictions on Certain Foreign Purchases (JUN 2008) (E.O.''s, proclamations, and statutes administered by the Office of Foreign Assets Control of the Department of the Treasury).</p><p>{{checkbox_52.212-5[59]}} (5) 52.225-26, Contractors Performing Private Security Functions Outside the United States (JUL 2013) (Section 862, as amended, of the National Defense Authorization Act for Fiscal Year 2008; 10 U.S.C. 2302 Note).</p><p>{{checkbox_52.212-5[60]}} (6) 52.226-4, Notice of Disaster or Emergency Area Set-Aside (NOV 2007) (42 U.S.C. 5150).</p><p>{{checkbox_52.212-5[61]}} (7) 52.226-5, Restrictions on Subcontracting Outside Disaster or Emergency Area (NOV 2007) (42 U.S.C. 5150).</p><p>{{checkbox_52.212-5[62]}} (8) 52.232-29, Terms for Financing of Purchases of Commercial Items (FEB 2002) (41 U.S.C.4505, 10 U.S.C. 2307(f)).</p><p>{{checkbox_52.212-5[63]}} (9) 52.232-30, Installment Payments for Commercial Items (OCT 1995) (41 U.S.C.4505, 10 U.S.C. 2307(f)).</p><p>{{checkbox_52.212-5[64]}} (0) 52.232-33, Payment by Electronic Funds Transfer-System for Award Management (JUL 2013) (31 U.S.C. 3332).</p><p>{{checkbox_52.212-5[65]}} (1) 52.232-34, Payment by Electronic Funds Transfer-Other than System for Award Management (JUL 2013) (31 U.S.C. 3332).</p><p>{{checkbox_52.212-5[66]}} (2) 52.232-36, Payment by Third Party (MAY 2014) (31 U.S.C. 3332).</p><p>{{checkbox_52.212-5[67]}} (3) 52.239-1, Privacy or Security Safeguards (AUG 1996) (5 U.S.C. 552a).</p><p>{{checkbox_52.212-5[68]}} (4)(i) 52.247-64, Preference for Privately Owned U.S.-Flag Commercial Vessels (FEB 2006) (46 U.S.C. Appx. 1241(b) and 10 U.S.C. 2631).</p><p>{{checkbox_52.212-5[69]}} (i) <i>Alternate I</i> (APR 2003) of 52.247-64.</p><p>(c) The Contractor shall comply with the FAR clauses in this paragraph (c), applicable to commercial services, that the Contracting Officer has indicated as being incorporated in this contract by reference to implement provisions of law or Executive orders applicable to acquisitions of commercial items: [Contracting Officer check as appropriate.]</p><p>{{checkbox_52.212-5[70]}} (1) 52.222-17, Nondisplacement of Qualified Workers (May 2014) (E.O. 13495).</p><p>{{checkbox_52.212-5[71]}} (2) 52.222-41, Service Contract Labor Standards (MAY 2014) (41 U.S.C. chapter 67).</p><p>{{checkbox_52.212-5[72]}} (3) 52.222-42, Statement of Equivalent Rates for Federal Hires (MAY 2014) (29 U.S.C. 206 and 41 U.S.C. chapter 67).</p><p>{{checkbox_52.212-5[73]}} (4) 52.222-43, Fair Labor Standards Act and Service Contract Labor Standards-Price Adjustment (Multiple Year and Option Contracts) (MAY 2014) (29 U.S.C. 206 and 41 U.S.C. chapter 67).</p><p>{{checkbox_52.212-5[74]}} (5) 52.222-44, Fair Labor Standards Act and Service Contract Labor Standards-Price Adjustment (MAY 2014) (29 U.S.C 206 and 41 U.S.C. chapter 67).</p><p>{{checkbox_52.212-5[75]}} (6) 52.222-51, Exemption from Application of the Service Contract Labor Standards to Contracts for Maintenance, Calibration, or Repair of Certain Equipment-Requirements (MAY 2014) (41 U.S.C. chapter 67).</p><p>{{checkbox_52.212-5[76]}} (7) 52.222-53, Exemption from Application of the Service Contract Labor Standards to Contracts for Certain Services-Requirements (MAY 2014) (41 U.S.C. chapter 67).</p><p>{{checkbox_52.212-5[77]}} (8) 52.222-55, Minimum Wages Under Executive Order 13658 (Dec 2014) (E.O. 13658).</p><p>{{checkbox_52.212-5[78]}} (9) 52.226-6, Promoting Excess Food Donation to Nonprofit Organizations (MAY 2014) (42 U.S.C. 1792).</p><p>{{checkbox_52.212-5[79]}} (0) 52.237-11, Accepting and Dispensing of $1 Coin (SEP 2008) (31 U.S.C. 5112(p)(1)).</p><p>(d) <i>Comptroller General Examination of Record.</i> The Contractor shall comply with the provisions of this paragraph (d) if this contract was awarded using other than sealed bid, is in excess of the simplified acquisition threshold, and does not contain the clause at 52.215-2, Audit and Records-Negotiation.</p><p>(1) The Comptroller General of the United States, or an authorized representative of the Comptroller General, shall have access to and right to examine any of the Contractor''s directly pertinent records involving transactions related to this contract.</p><p>(2) The Contractor shall make available at its offices at all reasonable times the records, materials, and other evidence for examination, audit, or reproduction, until 3 years after final payment under this contract or for any shorter period specified in FAR Subpart 4.7, Contractor Records Retention, of the other clauses of this contract. If this contract is completely or partially terminated, the records relating to the work terminated shall be made available for 3 years after any resulting final termination settlement. Records relating to appeals under the disputes clause or to litigation or the settlement of claims arising under or relating to this contract shall be made available until such appeals, litigation, or claims are finally resolved.</p><p>(3) As used in this clause, records include books, documents, accounting procedures and practices, and other data, regardless of type and regardless of form. This does not require the Contractor to create or maintain any record that the Contractor does not maintain in the ordinary course of business or pursuant to a provision of law.</p><p>(e)(1) Notwithstanding the requirements of the clauses in paragraphs (a), (b), (c), and (d) of this clause, the Contractor is not required to flow down any FAR clause, other than those in this paragraph (e)(1) of this paragraph in a subcontract for commercial items. Unless otherwise indicated below, the extent of the flow down shall be as required by the clause-</p><p>(i) 52.203-13, Contractor Code of Business Ethics and Conduct (APR 2010) (41 U.S.C. 3509).</p><p>(ii) 52.219-8, Utilization of Small Business Concerns (OCT 2014) (15 U.S.C. 637(d)(2) and (3)), in all subcontracts that offer further subcontracting opportunities. If the subcontract (except subcontracts to small business concerns) exceeds $650,000 ($1.5 million for construction of any public facility), the subcontractor must include 52.219-8 in lower tier subcontracts that offer subcontracting opportunities.</p><p>(iii) 52.222-17, Nondisplacement of Qualified Workers (MAY 2014) (E.O. 13495). Flow down required in accordance with paragraph (l) of FAR clause 52.222-17.</p><p>(iv) 52.222-21, Prohibition of Segregated Facilities (APR 2015).</p><p>(v) 52.222-26, Equal Opportunity (APR 2015) (E.O. 11246).</p><p>(vi) 52.222-35, Equal Opportunity for Veterans (July 2014) (38 U.S.C. 4212).</p><p>(vii) 52.222-36, Equal Opportunity for Workers with Disabilities (July 2014) (29 U.S.C. 793).</p><p>(viii) 52.222-37, Employment Reports on Veterans (July 2014) (38 U.S.C. 4212).</p><p>(ix) 52.222-40, Notification of Employee Rights Under the National Labor Relations Act (DEC 2010) (E.O. 13496). Flow down required in accordance with paragraph (f) of FAR clause 52.222-40.</p><p>(x) 52.222-41, Service Contract Labor Standards (MAY 2014) (41 U.S.C. chapter 67).</p><p>(xi) {{checkbox_52.212-5[80]}} (A) 52.222-50, Combating Trafficking in Persons (Mar 2015) (22 U.S.C. chapter 78 and E.O. 13627).</p><p>{{checkbox_52.212-5[81]}} (B) Alternate I (Mar 2015) of 52.222-50 (22 U.S.C. chapter 78 and E.O. 13627).</p><p>{{checkbox_52.212-5[82]}} Alternate I (AUG 2007) of 52.222-50 (22 U.S.C. 7104(g)).</p><p>(xii) 52.222-51, Exemption from Application of the Service Contract Labor Standards to Contracts for Maintenance, Calibration, or Repair of Certain Equipment-Requirements (MAY 2014) (41 U.S.C. chapter 67).</p><p>(xiii) 52.222-53, Exemption from Application of the Service Contract Labor Standards to Contracts for Certain Services-Requirements (MAY 2014) (41 U.S.C. chapter 67).</p><p>(xiv) 52.222-54, Employment Eligibility Verification (AUG 2013).</p><p>(xv) 52.222-55, Minimum Wages Under Executive Order 13658 (Dec 2014) (E.O. 13658).</p><p>(xvi) 52.225-26, Contractors Performing Private Security Functions Outside the United States (JUL 2013) (Section 862, as amended, of the National Defense Authorization Act for Fiscal Year 2008; 10 U.S.C. 2302 Note).</p><p>(xvii) 52.226-6, Promoting Excess Food Donation to Nonprofit Organizations (MAY 2014) (42 U.S.C. 1792). Flow down required in accordance with paragraph (e) of FAR clause 52.226-6.</p><p>(xviii) 52.247-64, Preference for Privately Owned U.S.-Flag Commercial Vessels (FEB 2006) (46 U.S.C. Appx. 1241(b) and 10 U.S.C. 2631). Flow down required in accordance with paragraph (d) of FAR clause 52.247-64.</p><p>(2) While not required, the contractor May include in its subcontracts for commercial items a minimal number of additional clauses necessary to satisfy its contractual obligations.</p>'
WHERE clause_id = 100704; -- 52.212-5

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101821; -- 52.212-5 (DEVIATION 2013-00019)

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100705; -- 52.213-1

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100706; -- 52.213-2

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100707; -- 52.213-3

UPDATE Clauses
SET is_optional = 1
, editable_remarks = 'ALL' -- NULL
WHERE clause_id = 100708; -- 52.213-4

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100709; -- 52.214-10

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100710; -- 52.214-12

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100712; -- 52.214-13

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100713; -- 52.214-14

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100714; -- 52.214-15

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100715; -- 52.214-16

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100716; -- 52.214-18

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100717; -- 52.214-19

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100720; -- 52.214-20

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100722; -- 52.214-21

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100723; -- 52.214-22

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100724; -- 52.214-23

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100725; -- 52.214-24

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100726; -- 52.214-25

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100728; -- 52.214-26

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100729; -- 52.214-27

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100730; -- 52.214-28

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100731; -- 52.214-29

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100732; -- 52.214-3

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100733; -- 52.214-31

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100734; -- 52.214-34

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100735; -- 52.214-35

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100736; -- 52.214-4

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100737; -- 52.214-5

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100738; -- 52.214-6

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100739; -- 52.214-7

UPDATE Clauses
SET editable_remarks = 'ONLY PARAGRAPH (c) (9)' -- NULL
WHERE clause_id = 100742; -- 52.215-1

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100743; -- 52.215-10

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100744; -- 52.215-11

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100745; -- 52.215-12

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100746; -- 52.215-13

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100748; -- 52.215-14

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100749; -- 52.215-15

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100750; -- 52.215-16

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100751; -- 52.215-17

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100752; -- 52.215-18

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100753; -- 52.215-19

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100757; -- 52.215-2

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100762; -- 52.215-20

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100767; -- 52.215-21

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100768; -- 52.215-22

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100770; -- 52.215-23

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100771; -- 52.215-3

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100772; -- 52.215-5

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100773; -- 52.215-6

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100774; -- 52.215-8

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100777; -- 52.215-9

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100778; -- 52.216-1

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100779; -- 52.216-10

UPDATE Clauses
SET editable_remarks = '(SEE PREAMBLE TO CLAUSE)\nmay be modified by substituting “$10,000” in lieu of “$100,000” as the maximum reserve in paragraph (b) if the Contractor is a nonprofit organization' -- NULL
WHERE clause_id = 100781; -- 52.216-11

UPDATE Clauses
SET editable_remarks = 'this clause may be modified by substituting “$10,000” in lieu of “$100,000” as the maximum reserve in paragraph (b) if the contract is with a nonprofit organization.' -- NULL
WHERE clause_id = 100783; -- 52.216-12

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100784; -- 52.216-15

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100786; -- 52.216-16

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100788; -- 52.216-17

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100789; -- 52.216-18

UPDATE Clauses
SET editable_remarks = 'ALL' -- NULL
WHERE clause_id = 100790; -- 52.216-19

UPDATE Clauses
SET is_optional = 1
, is_editable = 0
WHERE clause_id = 100791; -- 52.216-2

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100792; -- 52.216-20

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100797; -- 52.216-21

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100798; -- 52.216-22

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100799; -- 52.216-23

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100800; -- 52.216-24

UPDATE Clauses
SET editable_remarks = 'ONLYTHE FOLLOWING WORDS IN PARAGRAPH (a )MAY BE DELETED - THE WORDS: “and certified cost or pricing data in accordance with FAR 15.408, Table 15-2 supporting its proposal”' -- NULL
WHERE clause_id = 100802; -- 52.216-25

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100803; -- 52.216-26

UPDATE Clauses
SET editable_remarks = 'ONLY TO SPECIFIED A DIFFEREND ESTIMATED NUMBER OF AWARDS' -- NULL
WHERE clause_id = 100804; -- 52.216-27

UPDATE Clauses
SET editable_remarks = 'ONLY TO SPECIFIED A DIFFEREND ESTIMATED NUMBER OF AWARDS' -- NULL
WHERE clause_id = 100805; -- 52.216-28

UPDATE Clauses
SET editable_remarks = 'may ONLY amend the provision to make mandatory one of the three approaches in paragraph (c) of the provision, and/or to require the identification of all subcontractors, divisons, subsidiaries, or affiliates included in a blended labor rate.' -- NULL
WHERE clause_id = 100806; -- 52.216-29

UPDATE Clauses
SET is_optional = 1
, is_editable = 0
WHERE clause_id = 100807; -- 52.216-3

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100808; -- 52.216-30

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100809; -- 52.216-31

UPDATE Clauses
SET is_optional = 1
WHERE clause_id = 100810; -- 52.216-4

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100811; -- 52.216-5

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100812; -- 52.216-6

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100813; -- 52.216-7

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100818; -- 52.216-8

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100819; -- 52.216-9

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100820; -- 52.217-2

UPDATE Clauses
SET editable_remarks = 'ALL' -- NULL
WHERE clause_id = 100821; -- 52.217-3

UPDATE Clauses
SET editable_remarks = 'ALL' -- NULL
WHERE clause_id = 100822; -- 52.217-4

UPDATE Clauses
SET editable_remarks = 'ALL' -- NULL
WHERE clause_id = 100823; -- 52.217-5

UPDATE Clauses
SET editable_remarks = 'ALL' -- NULL
WHERE clause_id = 100824; -- 52.217-6

UPDATE Clauses
SET editable_remarks = 'ALL' -- NULL
WHERE clause_id = 100825; -- 52.217-7

UPDATE Clauses
SET editable_remarks = 'ALL' -- NULL
WHERE clause_id = 100826; -- 52.217-8

UPDATE Clauses
SET editable_remarks = 'ALL' -- NULL
WHERE clause_id = 100827; -- 52.217-9

UPDATE Clauses
SET editable_remarks = 'ALL\nSEE CLAUSE PREAMBLE' -- NULL
WHERE clause_id = 100829; -- 52.219-1

UPDATE Clauses
SET is_optional = 1
, editable_remarks = 'ALL' -- NULL
WHERE clause_id = 100830; -- 52.219-10

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100831; -- 52.219-11

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100832; -- 52.219-12

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100833; -- 52.219-13

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100834; -- 52.219-14

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100835; -- 52.219-16

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100836; -- 52.219-17

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100839; -- 52.219-18

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100840; -- 52.219-2

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100841; -- 52.219-27

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100842; -- 52.219-28

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100843; -- 52.219-29

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100845; -- 52.219-3

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100846; -- 52.219-30

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100848; -- 52.219-4

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100849; -- 52.219-6

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100854; -- 52.219-7

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100855; -- 52.219-8

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100859; -- 52.219-9

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101825; -- 52.219-9 (DEVIATION 2013-00014)

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100860; -- 52.222-1

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100861; -- 52.222-10

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100862; -- 52.222-11

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100863; -- 52.222-12

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100864; -- 52.222-13

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100865; -- 52.222-14

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100866; -- 52.222-15

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100867; -- 52.222-16

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100868; -- 52.222-17

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100869; -- 52.222-18

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100870; -- 52.222-19

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100871; -- 52.222-2

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100872; -- 52.222-20

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100873; -- 52.222-21

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100874; -- 52.222-22

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100875; -- 52.222-23

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100876; -- 52.222-24

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100877; -- 52.222-25

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100879; -- 52.222-26

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100880; -- 52.222-27

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100881; -- 52.222-29

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100882; -- 52.222-3

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100884; -- 52.222-30

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100885; -- 52.222-31

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100886; -- 52.222-32

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100889; -- 52.222-33

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100891; -- 52.222-34

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100893; -- 52.222-35

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100895; -- 52.222-36

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100896; -- 52.222-37

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100897; -- 52.222-38

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100898; -- 52.222-4

UPDATE Clauses
SET editable_remarks = 'ONLY TO REFLECT AN EXEMPTION BY THE SECRETARY' -- NULL
WHERE clause_id = 100899; -- 52.222-40

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100900; -- 52.222-41

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100901; -- 52.222-42

UPDATE Clauses
SET is_optional = 1
, editable_remarks = 'ALL' -- NULL
WHERE clause_id = 100902; -- 52.222-43

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100903; -- 52.222-44

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100904; -- 52.222-46

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100905; -- 52.222-48

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100906; -- 52.222-49

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100907; -- 52.222-5

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100909; -- 52.222-50

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100910; -- 52.222-51

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100911; -- 52.222-52

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100912; -- 52.222-53

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100913; -- 52.222-54

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100914; -- 52.222-55

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100915; -- 52.222-56

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100916; -- 52.222-6

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100917; -- 52.222-7

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100918; -- 52.222-8

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100919; -- 52.222-9

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100920; -- 52.223-1

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100921; -- 52.223-10

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100922; -- 52.223-11

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100923; -- 52.223-12

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100924; -- 52.223-13

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100926; -- 52.223-14

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100928; -- 52.223-15

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100930; -- 52.223-16

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100931; -- 52.223-17

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100932; -- 52.223-18

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100933; -- 52.223-19

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100934; -- 52.223-2

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100936; -- 52.223-3

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100937; -- 52.223-4

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100940; -- 52.223-5

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100941; -- 52.223-6

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100942; -- 52.223-7

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100943; -- 52.223-9

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100945; -- 52.224-1

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100946; -- 52.224-2

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100947; -- 52.225-1

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100949; -- 52.225-10

UPDATE Clauses
SET editable_remarks = 'ONLY PARAGRAPH (b)(4)(i) TO SUBSTITUTE A HIGHER PERCENTAGE, IF APPROVED' -- NULL
WHERE clause_id = 100951; -- 52.225-11

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100954; -- 52.225-12

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100955; -- 52.225-13

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100956; -- 52.225-14

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100957; -- 52.225-17

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100958; -- 52.225-18

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100959; -- 52.225-19

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100960; -- 52.225-2

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100961; -- 52.225-20

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100962; -- 52.225-21

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100964; -- 52.225-22

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100966; -- 52.225-23

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100969; -- 52.225-24

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100970; -- 52.225-25

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100971; -- 52.225-26

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100978; -- 52.225-3

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100983; -- 52.225-4

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100984; -- 52.225-5

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100985; -- 52.225-6

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100986; -- 52.225-7

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100987; -- 52.225-8

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100988; -- 52.225-9

UPDATE Clauses
SET is_optional = 1
, is_editable = 0
WHERE clause_id = 100989; -- 52.226-1

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100990; -- 52.226-2

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100991; -- 52.226-3

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100992; -- 52.226-4

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100993; -- 52.226-5

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100994; -- 52.226-6

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100997; -- 52.227-1

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100998; -- 52.227-10

UPDATE Clauses
SET is_optional = 1
, is_editable = 0
WHERE clause_id = 101004; -- 52.227-11

UPDATE Clauses
SET is_optional = 1
, is_editable = 0
WHERE clause_id = 101007; -- 52.227-13

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101013; -- 52.227-14

UPDATE Clauses
SET is_optional = 1
, is_editable = 0
WHERE clause_id = 101014; -- 52.227-15

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101015; -- 52.227-16

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101016; -- 52.227-17

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101017; -- 52.227-18

UPDATE Clauses
SET is_optional = 1
, is_editable = 0
WHERE clause_id = 101018; -- 52.227-19

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101019; -- 52.227-2

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101020; -- 52.227-20

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101021; -- 52.227-21

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101022; -- 52.227-22

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101023; -- 52.227-23

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101027; -- 52.227-3

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101029; -- 52.227-4

UPDATE Clauses
SET is_optional = 1
, is_editable = 0
WHERE clause_id = 101030; -- 52.227-5

UPDATE Clauses
SET editable_remarks = 'ALL' -- NULL
WHERE clause_id = 101032; -- 52.227-6

UPDATE Clauses
SET editable_remarks = 'ALL' -- NULL
WHERE clause_id = 101033; -- 52.227-7

UPDATE Clauses
SET editable_remarks = 'PCO CAN ONLY REVISE "PRICE" TO "TARGET COST AND TARGET PROFIT" IF NEEDED' -- NULL
WHERE clause_id = 101034; -- 52.227-9

UPDATE Clauses
SET editable_remarks = 'ALL' -- NULL
WHERE clause_id = 101035; -- 52.228-1

UPDATE Clauses
SET editable_remarks = 'ALL' -- NULL
WHERE clause_id = 101036; -- 52.228-10

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101037; -- 52.228-11

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101038; -- 52.228-12

UPDATE Clauses
SET editable_remarks = 'ONLY PARAGRAPH B OF THE CLAUSE TO ESTABLISH A LOWER PERCENTAGE' -- NULL
WHERE clause_id = 101039; -- 52.228-13

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101040; -- 52.228-14

UPDATE Clauses
SET editable_remarks = 'ONLY PARAGRAPHS (b)(1) AND/OR (b)(2) TO ESTABLISH A LOWER PERCENTAGE. IN ADDITION MUST ALLOW CO TO SET A PERIOD OF TIME FOR RETURN OF EXECUTED BONDS' -- NULL
WHERE clause_id = 101041; -- 52.228-15

UPDATE Clauses
SET editable_remarks = 'ALL' -- NULL
WHERE clause_id = 101043; -- 52.228-16

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101044; -- 52.228-2

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101045; -- 52.228-3

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101046; -- 52.228-4

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101047; -- 52.228-5

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101048; -- 52.228-7

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101049; -- 52.228-8

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101050; -- 52.228-9

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101051; -- 52.229-1

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101052; -- 52.229-10

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101054; -- 52.229-2

UPDATE Clauses
SET is_optional = 1
, is_editable = 0
WHERE clause_id = 101055; -- 52.229-3

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101056; -- 52.229-4

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101058; -- 52.229-6

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101059; -- 52.229-7

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101060; -- 52.229-8

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101061; -- 52.229-9

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101063; -- 52.230-1

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101064; -- 52.230-2

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101065; -- 52.230-3

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101066; -- 52.230-4

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101067; -- 52.230-5

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101068; -- 52.230-6

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101069; -- 52.230-7

UPDATE Clauses
SET editable_remarks = 'PAYMENT DUE DATES ONLY' -- NULL
WHERE clause_id = 101070; -- 52.232-1

UPDATE Clauses
SET editable_remarks = 'PAYMENT DUE DATES ONLY' -- NULL
WHERE clause_id = 101071; -- 52.232-10

UPDATE Clauses
SET editable_remarks = 'PAYMENT DUE DATES ONLY' -- NULL
WHERE clause_id = 101072; -- 52.232-11

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101073; -- 52.232-12

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101079; -- 52.232-13

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101080; -- 52.232-14

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101081; -- 52.232-15

UPDATE Clauses
SET editable_remarks = 'ONLY PARAGRAPH (a)(1) , (a)(6), AND (b) PROGRESS PAYMENT RATE' -- NULL
WHERE clause_id = 101085; -- 52.232-16

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101086; -- 52.232-17

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101087; -- 52.232-18

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101088; -- 52.232-19

UPDATE Clauses
SET editable_remarks = 'PAYMENT DUE DATES ONLY' -- NULL
WHERE clause_id = 101089; -- 52.232-2

UPDATE Clauses
SET editable_remarks = '- SEE CLAUSE PREAMBLE\nThe 60-day period may be varied from 30 to 90 days and the 75 percent from 75 to 85 percent. “Task Order” or other appropriate designation may be substituted for “Schedule” wherever that word appears in the clause.' -- NULL
WHERE clause_id = 101090; -- 52.232-20

UPDATE Clauses
SET editable_remarks = 'The 60-day period may be varied from 30 to 90 days and the 75 percent from 75 to 85 percent. “Task Order” or other appropriate designation may be substituted for “Schedule” wherever that word appears in the clause.' -- NULL
WHERE clause_id = 101091; -- 52.232-22

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101093; -- 52.232-23

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101094; -- 52.232-24

UPDATE Clauses
SET editable_remarks = 'PARAGRAPH (a)(5)(I) TO MODIFY THE DATE AND PARAGRAPH (a)(1)(I) AND (a)(1)(ii) TO INSERT A PERIOD SHORTER THAN 30 DAYS BUT NOT LESS THAN 7 DAYS' -- NULL
WHERE clause_id = 101096; -- 52.232-25

UPDATE Clauses
SET editable_remarks = '1. subparagraph (a)(4)(i) of the clause to specify a period longer than 7 days \n2. paragraphs (a)(1)(i) and (ii) of the clause to insert a period shorter than 30 days (but not less than 7 days)' -- NULL
WHERE clause_id = 101097; -- 52.232-26

UPDATE Clauses
SET editable_remarks = '1. the contracting officer may modify the date in paragraph (a)(1)(i)(A) of the clause to specify a period longer than 14 days.\n2.the contracting officer may modify the date in paragraph (a)(4)(i) of the clause to specify a period longer than 7 days' -- NULL
WHERE clause_id = 101098; -- 52.232-27

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101100; -- 52.232-28

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101101; -- 52.232-29

UPDATE Clauses
SET editable_remarks = 'PAYMENT DUE DATES ONLY' -- NULL
WHERE clause_id = 101102; -- 52.232-3

UPDATE Clauses
SET is_optional = 1
, is_editable = 0
WHERE clause_id = 101103; -- 52.232-30

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101104; -- 52.232-31

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101105; -- 52.232-32

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101106; -- 52.232-33

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101107; -- 52.232-34

UPDATE Clauses
SET editable_remarks = 'ALL' -- NULL
WHERE clause_id = 101108; -- 52.232-35
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size, fill_in_group_number, fill_in_placeholder, fill_in_default_data, fill_in_display_rows, fill_in_for_table, fill_in_heading) VALUES (101108, 'textbox_52.232-35[7]', 'S', 100, NULL, NULL, NULL, NULL, 0, NULL);

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101109; -- 52.232-36

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101110; -- 52.232-37

UPDATE Clauses
SET editable_remarks = 'ALL' -- NULL
WHERE clause_id = 101111; -- 52.232-38

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101112; -- 52.232-39

UPDATE Clauses
SET editable_remarks = 'PAYMENT DUE DATES ONLY' -- NULL
WHERE clause_id = 101113; -- 52.232-4

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101114; -- 52.232-40

UPDATE Clauses
SET editable_remarks = 'PAYMENT DUE DATES ONLY' -- NULL
WHERE clause_id = 101115; -- 52.232-5

UPDATE Clauses
SET editable_remarks = 'PAYMENT DUE DATES ONLY' -- NULL
WHERE clause_id = 101116; -- 52.232-6

UPDATE Clauses
SET editable_remarks = 'ONLY PARAGRAPH (a)(7)' -- NULL
WHERE clause_id = 101117; -- 52.232-7

UPDATE Clauses
SET editable_remarks = 'PARAGRAPH (b): PAYMENT DUE DATES ONLY' -- NULL
WHERE clause_id = 101118; -- 52.232-8

UPDATE Clauses
SET editable_remarks = 'PARAGRAPH (b): PAYMENT DUE DATES ONLY' -- NULL
WHERE clause_id = 101119; -- 52.232-9

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101121; -- 52.233-1

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101122; -- 52.233-2

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101124; -- 52.233-3

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101125; -- 52.233-4

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101126; -- 52.234-1

UPDATE Clauses
SET editable_remarks = 'ALL' -- NULL
WHERE clause_id = 101127; -- 52.234-2

UPDATE Clauses
SET editable_remarks = 'ALL' -- NULL
WHERE clause_id = 101128; -- 52.234-3

UPDATE Clauses
SET editable_remarks = 'ALL' -- NULL
WHERE clause_id = 101129; -- 52.234-4

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101130; -- 52.236-1

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101131; -- 52.236-10

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101132; -- 52.236-11

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101133; -- 52.236-12

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101134; -- 52.236-13

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101136; -- 52.236-14

UPDATE Clauses
SET is_optional = 1
, is_editable = 0
WHERE clause_id = 101137; -- 52.236-15

UPDATE Clauses
SET is_optional = 1
, is_editable = 0
WHERE clause_id = 101139; -- 52.236-16

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101140; -- 52.236-17

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101141; -- 52.236-18

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101142; -- 52.236-19

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101143; -- 52.236-2

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101146; -- 52.236-21

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101147; -- 52.236-22

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101148; -- 52.236-23

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101149; -- 52.236-24

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101150; -- 52.236-25

UPDATE Clauses
SET editable_remarks = 'ALL' -- NULL
WHERE clause_id = 101151; -- 52.236-26

UPDATE Clauses
SET editable_remarks = 'ALL' -- NULL
WHERE clause_id = 101153; -- 52.236-27
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size, fill_in_group_number, fill_in_placeholder, fill_in_default_data, fill_in_display_rows, fill_in_for_table, fill_in_heading) VALUES (101153, 'textbox_52.236-27[3]', 'S', 100, NULL, NULL, NULL, NULL, 0, NULL);

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101154; -- 52.236-28

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101155; -- 52.236-3

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101156; -- 52.236-4

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101157; -- 52.236-5

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101158; -- 52.236-6

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101159; -- 52.236-7

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101160; -- 52.236-8

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101161; -- 52.236-9

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101162; -- 52.237-1

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101163; -- 52.237-10

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101164; -- 52.237-11

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101165; -- 52.237-2

UPDATE Clauses
SET is_optional = 1
, is_editable = 0
WHERE clause_id = 101166; -- 52.237-3

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101168; -- 52.237-4

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101169; -- 52.237-5

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101170; -- 52.237-6

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101171; -- 52.237-7

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101172; -- 52.237-8

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101173; -- 52.237-9

UPDATE Clauses
SET editable_remarks = 'ALL' -- NULL
WHERE clause_id = 101174; -- 52.239-1

UPDATE Clauses
SET editable_remarks = 'ALL' -- NULL
WHERE clause_id = 101175; -- 52.241-1

UPDATE Clauses
SET editable_remarks = 'ALL' -- NULL
WHERE clause_id = 101176; -- 52.241-10

UPDATE Clauses
SET editable_remarks = 'ALL' -- NULL
WHERE clause_id = 101177; -- 52.241-11

UPDATE Clauses
SET editable_remarks = 'ALL' -- NULL
WHERE clause_id = 101178; -- 52.241-12

UPDATE Clauses
SET editable_remarks = 'ALL' -- NULL
WHERE clause_id = 101179; -- 52.241-13

UPDATE Clauses
SET editable_remarks = 'ALL' -- NULL
WHERE clause_id = 101180; -- 52.241-2

UPDATE Clauses
SET editable_remarks = 'ALL' -- NULL
WHERE clause_id = 101181; -- 52.241-3

UPDATE Clauses
SET editable_remarks = 'ALL' -- NULL
WHERE clause_id = 101182; -- 52.241-4

UPDATE Clauses
SET editable_remarks = 'ALL' -- NULL
WHERE clause_id = 101183; -- 52.241-5

UPDATE Clauses
SET editable_remarks = 'ALL' -- NULL
WHERE clause_id = 101184; -- 52.241-6

UPDATE Clauses
SET editable_remarks = 'ALL' -- NULL
WHERE clause_id = 101185; -- 52.241-7

UPDATE Clauses
SET editable_remarks = 'ALL' -- NULL
WHERE clause_id = 101186; -- 52.241-8

UPDATE Clauses
SET editable_remarks = 'ALL' -- NULL
WHERE clause_id = 101188; -- 52.241-9

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101189; -- 52.242-1

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101190; -- 52.242-13

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101191; -- 52.242-14

UPDATE Clauses
SET is_optional = 1
, is_editable = 0
WHERE clause_id = 101193; -- 52.242-15

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101194; -- 52.242-17

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101195; -- 52.242-2

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101196; -- 52.242-3

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101197; -- 52.242-4

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101203; -- 52.243-1

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101208; -- 52.243-2

UPDATE Clauses
SET editable_remarks = 'MAY VARY THE 30 DAY PERIOD IN PARAGRAPH (c)' -- NULL
WHERE clause_id = 101209; -- 52.243-3

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101210; -- 52.243-4

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101211; -- 52.243-5

UPDATE Clauses
SET is_optional = 1
, editable_remarks = 'ALL' -- NULL
WHERE clause_id = 101212; -- 52.243-6

UPDATE Clauses
SET is_optional = 1
, editable_remarks = 'ALL' -- NULL
WHERE clause_id = 101213; -- 52.243-7

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101215; -- 52.244-2
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size, fill_in_group_number, fill_in_placeholder, fill_in_default_data, fill_in_display_rows, fill_in_for_table, fill_in_heading) VALUES (101215, 'textbox_52.244-2[5]', 'S', 100, NULL, NULL, NULL, NULL, 0, NULL);

UPDATE Clauses
SET is_optional = 1
, is_editable = 0
WHERE clause_id = 101216; -- 52.244-4

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101217; -- 52.244-5

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101218; -- 52.244-6

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101221; -- 52.245-1

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101222; -- 52.245-2

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101223; -- 52.245-9

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101224; -- 52.246-1

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101225; -- 52.246-11

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101226; -- 52.246-12

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101227; -- 52.246-13

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101228; -- 52.246-14

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101229; -- 52.246-15

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101230; -- 52.246-16

UPDATE Clauses
SET is_optional = 1
, editable_remarks = 'ALL' -- NULL
WHERE clause_id = 101235; -- 52.246-17

UPDATE Clauses
SET is_optional = 1
, editable_remarks = 'ALL' -- NULL
WHERE clause_id = 101239; -- 52.246-18

UPDATE Clauses
SET is_optional = 1
, editable_remarks = 'ALL' -- NULL
WHERE clause_id = 101243; -- 52.246-19

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101246; -- 52.246-2

UPDATE Clauses
SET is_optional = 1
, editable_remarks = 'ALL' -- NULL
WHERE clause_id = 101247; -- 52.246-20

UPDATE Clauses
SET is_optional = 1
, editable_remarks = 'ALL' -- NULL
WHERE clause_id = 101249; -- 52.246-21

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101250; -- 52.246-23

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101252; -- 52.246-24

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101253; -- 52.246-25

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101254; -- 52.246-3

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101255; -- 52.246-4

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101256; -- 52.246-5

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101258; -- 52.246-6

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101259; -- 52.246-7

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101261; -- 52.246-8

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101262; -- 52.246-9

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101263; -- 52.247-1

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101264; -- 52.247-10

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101265; -- 52.247-11

UPDATE Clauses
SET editable_remarks = 'ALL' -- NULL
WHERE clause_id = 101266; -- 52.247-12

UPDATE Clauses
SET editable_remarks = 'ALL' -- NULL
WHERE clause_id = 101267; -- 52.247-13

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101268; -- 52.247-14

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101269; -- 52.247-15

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101270; -- 52.247-16

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101271; -- 52.247-17

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101272; -- 52.247-18

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101273; -- 52.247-19

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101274; -- 52.247-2

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101275; -- 52.247-20

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101276; -- 52.247-21

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101277; -- 52.247-22

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101278; -- 52.247-23

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101279; -- 52.247-24

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101280; -- 52.247-25

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101281; -- 52.247-26

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101282; -- 52.247-27

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101283; -- 52.247-28

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101284; -- 52.247-29

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101286; -- 52.247-3

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101287; -- 52.247-30

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101288; -- 52.247-31

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101289; -- 52.247-32

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101290; -- 52.247-33

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101291; -- 52.247-34

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101292; -- 52.247-35

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101293; -- 52.247-36

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101294; -- 52.247-37

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101295; -- 52.247-38

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101296; -- 52.247-39

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101297; -- 52.247-4
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size, fill_in_group_number, fill_in_placeholder, fill_in_default_data, fill_in_display_rows, fill_in_for_table, fill_in_heading) VALUES (101297, 'textbox_52.247-4[5]', 'S', 100, NULL, NULL, NULL, NULL, 0, NULL);

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101298; -- 52.247-40

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101299; -- 52.247-41

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101300; -- 52.247-42

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101301; -- 52.247-43

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101302; -- 52.247-44

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101303; -- 52.247-45

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101304; -- 52.247-46

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101305; -- 52.247-47

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101306; -- 52.247-48

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101307; -- 52.247-49
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size, fill_in_group_number, fill_in_placeholder, fill_in_default_data, fill_in_display_rows, fill_in_for_table, fill_in_heading) VALUES (101307, 'textbox_52.247-49[1]', 'S', 100, NULL, NULL, NULL, NULL, 0, NULL);

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101308; -- 52.247-5

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101309; -- 52.247-50

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101313; -- 52.247-51

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101314; -- 52.247-52

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101315; -- 52.247-53

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101316; -- 52.247-55

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101317; -- 52.247-56

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101318; -- 52.247-57
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size, fill_in_group_number, fill_in_placeholder, fill_in_default_data, fill_in_display_rows, fill_in_for_table, fill_in_heading) VALUES (101318, 'textbox_52.247-57[1]', 'S', 100, NULL, NULL, NULL, NULL, 0, NULL);

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101319; -- 52.247-58

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101320; -- 52.247-59

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101321; -- 52.247-6

UPDATE Clauses
SET editable_remarks = 'PARAGRAPH (a) ONLY' -- NULL
WHERE clause_id = 101322; -- 52.247-60

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101324; -- 52.247-61

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101325; -- 52.247-62

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101326; -- 52.247-63

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101329; -- 52.247-64

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101330; -- 52.247-65

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101331; -- 52.247-66

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101332; -- 52.247-67

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101333; -- 52.247-68

UPDATE Clauses
SET editable_remarks = 'ALL' -- NULL
WHERE clause_id = 101334; -- 52.247-7

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101335; -- 52.247-8

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101336; -- 52.247-9

UPDATE Clauses
SET editable_remarks = '1. Revising paragraph (i)(3)(i) of the clause\n2.Revising the first sentence under paragraph (3) of the definition of “acquisition savings” \n3. paragraph (i)(3)(i) of the clause and the first sentence under paragraph (3) of the definition of “acquisition savings” by substituting “under contracts awarded during the sharing period” for “during the sharing period.”\nCHANGE THE PERCENTAGES IN THE CHART' -- NULL
WHERE clause_id = 101340; -- 52.248-1

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101341; -- 52.248-2

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101343; -- 52.248-3

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101345; -- 52.249-1

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101349; -- 52.249-10

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101350; -- 52.249-12

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101351; -- 52.249-14

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101355; -- 52.249-2

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101357; -- 52.249-3

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101358; -- 52.249-4

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101359; -- 52.249-5

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101365; -- 52.249-6

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101366; -- 52.249-7

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101368; -- 52.249-8

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101369; -- 52.249-9

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101371; -- 52.250-1

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101372; -- 52.250-2

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101375; -- 52.250-3

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101378; -- 52.250-4

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101379; -- 52.250-5

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101380; -- 52.251-1

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101381; -- 52.251-2

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101382; -- 52.252-1

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101383; -- 52.252-2

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101384; -- 52.252-3

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101385; -- 52.252-4

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101386; -- 52.252-5

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101387; -- 52.252-6

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101388; -- 52.253-1

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101474; -- 252.216-7010 Alternate I

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101476; -- 252.217-7000 Alternate I

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101499; -- 252.219-7003 Alternate I

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101518; -- 252.223-7006 Alternate I

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101522; -- 252.225-7000 Alternate I

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101524; -- 252.225-7001 Alternate I

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101542; -- 252.225-7020 Alternate I

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101544; -- 252.225-7021 Alternate II

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101557; -- 252.225-7035 Alternate I

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101558; -- 252.225-7035 Alternate II

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101559; -- 252.225-7035 Alternate III

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101560; -- 252.225-7035 Alternate IV

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101561; -- 252.225-7035 Alternate V

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101563; -- 252.225-7036 Alternate I

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101564; -- 252.225-7036 Alternate II

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101565; -- 252.225-7036 Alternate III

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101566; -- 252.225-7036 Alternate IV

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101567; -- 252.225-7036 Alternate V

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101576; -- 252.225-7044 Alternate I

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101578; -- 252.225-7045 Alternate I

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101579; -- 252.225-7045 Alternate II

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101580; -- 252.225-7045 Alternate III

UPDATE Clauses
SET is_optional = 1
WHERE clause_id = 101611; -- 252.227-7005 Alternate I

UPDATE Clauses
SET is_optional = 1
WHERE clause_id = 101612; -- 252.227-7005 Alternate II

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101620; -- 252.227-7013 Alternate I

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101621; -- 252.227-7013 Alternate II

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101623; -- 252.227-7014 Alternate I

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101625; -- 252.227-7015 Alternate I

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101629; -- 252.227-7018 Alternate I

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101654; -- 252.229-7001 Alternate I

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101689; -- 252.234-7003 Alternate I

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101691; -- 252.234-7004 Alternate I

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101696; -- 252.235-7003 Alternate I

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101717; -- 252.237-7002 Alternate I

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101732; -- 252.237-7016 Alternate I

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101733; -- 252.237-7016 Alternate II

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101766; -- 252.244-7001 Alternate I

UPDATE Clauses
SET editable_remarks = 'ALL' -- NULL
WHERE clause_id = 101774; -- 252.246-7001 Alternate I

UPDATE Clauses
SET editable_remarks = 'ALL' -- NULL
WHERE clause_id = 101775; -- 252.246-7001 Alternate II

UPDATE Clauses
SET editable_remarks = 'ONLY IF USINGNEGOTIATION' -- NULL
WHERE clause_id = 101791; -- 252.247-7008 Alternate I

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101806; -- 252.247-7023 Alternate I

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101807; -- 252.247-7023 Alternate II

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100622; -- 52.203-6 Alternate I

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100635; -- 52.204-2 Alternate I

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100636; -- 52.204-2 Alternate II

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100642; -- 52.204-7 Alternate I

UPDATE Clauses
SET editable_remarks = 'ALL - COST REIMBURSEMENT ONLY' -- NULL
WHERE clause_id = 100660; -- 52.209-3 Alternate I

UPDATE Clauses
SET editable_remarks = 'ALL - COST REIMBURSEMENT ONLY' -- NULL
WHERE clause_id = 100661; -- 52.209-3 Alternate II

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100663; -- 52.209-4 Alternate I

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100664; -- 52.209-4 Alternate II

UPDATE Clauses
SET editable_remarks = 'ALL\nThe clause may be changed to accommodate the issuance of orders under indefinite-delivery contracts' -- NULL
WHERE clause_id = 100672; -- 52.211-10 Alternate I

UPDATE Clauses
SET is_optional = 1
, editable_remarks = 'ALL' -- NULL
WHERE clause_id = 100688; -- 52.211-8 Alternate I

UPDATE Clauses
SET is_optional = 1
, editable_remarks = 'ALL' -- NULL
WHERE clause_id = 100689; -- 52.211-8 Alternate II

UPDATE Clauses
SET is_optional = 1
, editable_remarks = 'ALL' -- NULL
WHERE clause_id = 100690; -- 52.211-8 Alternate III

UPDATE Clauses
SET is_optional = 1
, editable_remarks = 'ALL' -- NULL
WHERE clause_id = 100692; -- 52.211-9 Alternate I

UPDATE Clauses
SET is_optional = 1
, editable_remarks = 'ALL' -- NULL
WHERE clause_id = 100693; -- 52.211-9 Alternate II

UPDATE Clauses
SET is_optional = 1
, editable_remarks = 'ALL' -- NULL
WHERE clause_id = 100694; -- 52.211-9 Alternate III

UPDATE Clauses
SET editable_remarks = 'ONLY IF DEVIATION OBTAINED' -- NULL
WHERE clause_id = 100698; -- 52.212-3 Alternate I

UPDATE Clauses
SET editable_remarks = 'COMMERCIAL ITEMS ONLY' -- NULL
WHERE clause_id = 100700; -- 52.212-4 Alternate I

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100702; -- 52.212-5 Alternate I

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100703; -- 52.212-5 Alternate II

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100711; -- 52.214-13 Alternate I

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100718; -- 52.214-20 Alternate I

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100719; -- 52.214-20 Alternate II

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100721; -- 52.214-21 Alternate I

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100727; -- 52.214-26 Alternate I

UPDATE Clauses
SET editable_remarks = 'ONLY PARAGRAPH (c) (9)' -- NULL
WHERE clause_id = 100740; -- 52.215-1 Alternate I

UPDATE Clauses
SET editable_remarks = 'ONLY PARAGRAPH (c) (9)' -- NULL
WHERE clause_id = 100741; -- 52.215-1 Alternate II

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100747; -- 52.215-14 Alternate I

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100754; -- 52.215-2 Alternate I

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100755; -- 52.215-2 Alternate II

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100756; -- 52.215-2 Alternate III

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100758; -- 52.215-20 Alternate I

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100759; -- 52.215-20 Alternate II

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100760; -- 52.215-20 Alternate III

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100761; -- 52.215-20 Alternate IV

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100763; -- 52.215-21 Alternate I

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100764; -- 52.215-21 Alternate II

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100765; -- 52.215-21 Alternate III

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100766; -- 52.215-21 Alternate IV

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100769; -- 52.215-23 Alternate I

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100775; -- 52.215-9 Alternate I

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100776; -- 52.215-9 Alternate II

UPDATE Clauses
SET editable_remarks = '(SEE PREAMBLE TO CLAUSE)\nmay be modified by substituting “$10,000” in lieu of “$100,000” as the maximum reserve in paragraph (b) if the Contractor is a nonprofit organization' -- NULL
WHERE clause_id = 100780; -- 52.216-11 Alternate I

UPDATE Clauses
SET editable_remarks = 'this clause may be modified by substituting “$10,000” in lieu of “$100,000” as the maximum reserve in paragraph (b) if the contract is with a nonprofit organization.' -- NULL
WHERE clause_id = 100782; -- 52.216-12 Alternate I

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100785; -- 52.216-16 Alternate I

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100787; -- 52.216-17 Alternate I

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100793; -- 52.216-21 Alternate I

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100794; -- 52.216-21 Alternate II

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100795; -- 52.216-21 Alternate III

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100796; -- 52.216-21 Alternate IV

UPDATE Clauses
SET editable_remarks = 'ONLYTHE FOLLOWING WORDS IN PARAGRAPH (a )MAY BE DELETED - THE WORDS: “and certified cost or pricing data in accordance with FAR 15.408, Table 15-2 supporting its proposal”' -- NULL
WHERE clause_id = 100801; -- 52.216-25 Alternate I

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100814; -- 52.216-7 Alternate I

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100815; -- 52.216-7 Alternate II

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100816; -- 52.216-7 Alternate III

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100817; -- 52.216-7 Alternate IV

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100828; -- 52.219-1 Alternate I

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100837; -- 52.219-18 Alternate I

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100838; -- 52.219-18 Alternate II

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100844; -- 52.219-3 Alternate I

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100847; -- 52.219-4 Alternate I

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100850; -- 52.219-6 Alternate I

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100851; -- 52.219-6 Alternate II

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100852; -- 52.219-7 Alternate I

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100853; -- 52.219-7 Alternate II

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100856; -- 52.219-9 Alternate I

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100857; -- 52.219-9 Alternate II

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100858; -- 52.219-9 Alternate III

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100878; -- 52.222-26 Alternate I

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100887; -- 52.222-33 Alternate I

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100888; -- 52.222-33 Alternate II

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100890; -- 52.222-34 Alternate I

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100892; -- 52.222-35 Alternate I

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100894; -- 52.222-36 Alternate I

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100908; -- 52.222-50 Alternate I

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100925; -- 52.223-13 Alternate I

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100927; -- 52.223-14 Alternate I

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100929; -- 52.223-16 Alternate I

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100935; -- 52.223-3 Alternate I

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100938; -- 52.223-5 Alternate I

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100939; -- 52.223-5 Alternate II

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100948; -- 52.225-10 Alternate I

UPDATE Clauses
SET editable_remarks = 'ONLY PARAGRAPH (b)(4)(i) TO SUBSTITUTE A HIGHER PERCENTAGE, IF APPROVED' -- NULL
WHERE clause_id = 100950; -- 52.225-11 Alternate I

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100952; -- 52.225-12 Alternate I

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100953; -- 52.225-12 Alternate II

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100963; -- 52.225-22 Alternate I

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100965; -- 52.225-23 Alternate I

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100967; -- 52.225-24 Alternate I

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100968; -- 52.225-24 Alternate II

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100972; -- 52.225-3 Alternate I

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100974; -- 52.225-3 Alternate II

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100976; -- 52.225-3 Alternate III

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100980; -- 52.225-4 Alternate I

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100981; -- 52.225-4 Alternate II

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100982; -- 52.225-4 Alternate III

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100995; -- 52.227-1 Alternate I

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 100996; -- 52.227-1 Alternate II

UPDATE Clauses
SET is_optional = 1
, is_editable = 0
WHERE clause_id = 100999; -- 52.227-11 Alternate I

UPDATE Clauses
SET is_optional = 1
, is_editable = 0
WHERE clause_id = 101000; -- 52.227-11 Alternate II

UPDATE Clauses
SET is_optional = 1
, is_editable = 0
WHERE clause_id = 101001; -- 52.227-11 Alternate III

UPDATE Clauses
SET is_optional = 1
, is_editable = 0
WHERE clause_id = 101002; -- 52.227-11 Alternate IV

UPDATE Clauses
SET is_optional = 1
, is_editable = 0
WHERE clause_id = 101003; -- 52.227-11 Alternate V

UPDATE Clauses
SET is_optional = 1
, is_editable = 0
WHERE clause_id = 101005; -- 52.227-13 Alternate I

UPDATE Clauses
SET is_optional = 1
, is_editable = 0
WHERE clause_id = 101006; -- 52.227-13 Alternate II

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101008; -- 52.227-14 Alternate I

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101009; -- 52.227-14 Alternate II

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101010; -- 52.227-14 Alternate III

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101011; -- 52.227-14 Alternate IV

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101012; -- 52.227-14 Alternate V

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101024; -- 52.227-3 Alternate I

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101025; -- 52.227-3 Alternate II

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101026; -- 52.227-3 Alternate III

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101028; -- 52.227-4 Alternate I

UPDATE Clauses
SET editable_remarks = 'ALL' -- NULL
WHERE clause_id = 101031; -- 52.227-6 Alternate I

UPDATE Clauses
SET editable_remarks = 'ALL' -- NULL
WHERE clause_id = 101042; -- 52.228-16 Alternate I

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101053; -- 52.229-2 Alternate I

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101074; -- 52.232-12 Alternate I

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101075; -- 52.232-12 Alternate II

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101076; -- 52.232-12 Alternate III

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101077; -- 52.232-12 Alternate IV

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101078; -- 52.232-12 Alternate V

UPDATE Clauses
SET editable_remarks = 'ONLY PARAGRAPH (a)(1) , (a)(6), AND (b) PROGRESS PAYMENT RATE' -- NULL
WHERE clause_id = 101082; -- 52.232-16 Alternate I

UPDATE Clauses
SET editable_remarks = 'ONLY PARAGRAPH (a)(1) , (a)(6), AND (b) PROGRESS PAYMENT RATE' -- NULL
WHERE clause_id = 101083; -- 52.232-16 Alternate II

UPDATE Clauses
SET editable_remarks = 'ONLY PARAGRAPH (a)(1) , (a)(6), AND (b) PROGRESS PAYMENT RATE' -- NULL
WHERE clause_id = 101084; -- 52.232-16 Alternate III

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101092; -- 52.232-23 Alternate I

UPDATE Clauses
SET editable_remarks = 'PARAGRAPH (a)(5)(I) TO MODIFY THE DATE AND PARAGRAPH (a)(1)(I) AND (a)(1)(ii) TO INSERT A PERIOD SHORTER THAN 30 DAYS BUT NOT LESS THAN 7 DAYS' -- NULL
WHERE clause_id = 101095; -- 52.232-25 Alternate I

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101099; -- 52.232-28 Alternate I

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101120; -- 52.233-1 Alternate I

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101123; -- 52.233-3 Alternate I

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101135; -- 52.236-13 Alternate I

UPDATE Clauses
SET is_optional = 1
, is_editable = 0
WHERE clause_id = 101138; -- 52.236-16 Alternate I

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101144; -- 52.236-21 Alternate I

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101145; -- 52.236-21 Alternate II

UPDATE Clauses
SET editable_remarks = 'ALL' -- NULL
WHERE clause_id = 101152; -- 52.236-27 Alternate I

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101167; -- 52.237-4 Alternate I

UPDATE Clauses
SET editable_remarks = 'ALL' -- NULL
WHERE clause_id = 101187; -- 52.241-9 Alternate I

UPDATE Clauses
SET is_optional = 1
, is_editable = 0
WHERE clause_id = 101192; -- 52.242-15 Alternate I

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101198; -- 52.243-1 Alternate I

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101199; -- 52.243-1 Alternate II

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101200; -- 52.243-1 Alternate III

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101201; -- 52.243-1 Alternate IV

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101202; -- 52.243-1 Alternate V

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101204; -- 52.243-2 Alternate I

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101205; -- 52.243-2 Alternate II

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101206; -- 52.243-2 Alternate III

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101207; -- 52.243-2 Alternate V

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101214; -- 52.244-2 Alternate I

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101219; -- 52.245-1 Alternate I

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101220; -- 52.245-1 Alternate II

UPDATE Clauses
SET is_optional = 1
, editable_remarks = 'ALL' -- NULL
WHERE clause_id = 101231; -- 52.246-17 Alternate II

UPDATE Clauses
SET is_optional = 1
, editable_remarks = 'ALL' -- NULL
WHERE clause_id = 101232; -- 52.246-17 Alternate III

UPDATE Clauses
SET is_optional = 1
, editable_remarks = 'ALL' -- NULL
WHERE clause_id = 101233; -- 52.246-17 Alternate IV

UPDATE Clauses
SET is_optional = 1
, editable_remarks = 'ALL' -- NULL
WHERE clause_id = 101234; -- 52.246-17 Alternate V

UPDATE Clauses
SET is_optional = 1
, editable_remarks = 'ALL' -- NULL
WHERE clause_id = 101236; -- 52.246-18 Alternate II

UPDATE Clauses
SET is_optional = 1
, editable_remarks = 'ALL' -- NULL
WHERE clause_id = 101237; -- 52.246-18 Alternate III

UPDATE Clauses
SET is_optional = 1
, editable_remarks = 'ALL' -- NULL
WHERE clause_id = 101238; -- 52.246-18 Alternate IV

UPDATE Clauses
SET is_optional = 1
, editable_remarks = 'ALL' -- NULL
WHERE clause_id = 101240; -- 52.246-19 Alternate I

UPDATE Clauses
SET is_optional = 1
, editable_remarks = 'ALL' -- NULL
WHERE clause_id = 101241; -- 52.246-19 Alternate II

UPDATE Clauses
SET is_optional = 1
, editable_remarks = 'ALL' -- NULL
WHERE clause_id = 101242; -- 52.246-19 Alternate III

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101244; -- 52.246-2 Alternate I

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101245; -- 52.246-2 Alternate II

UPDATE Clauses
SET is_optional = 1
, editable_remarks = 'ALL' -- NULL
WHERE clause_id = 101248; -- 52.246-21 Alternate I

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101251; -- 52.246-24 Alternate I

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101257; -- 52.246-6 Alternate I

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101260; -- 52.246-8 Alternate I

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101285; -- 52.247-3 Alternate I

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101310; -- 52.247-51 Alternate I

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101311; -- 52.247-51 Alternate II

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101312; -- 52.247-51 Alternate III

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101327; -- 52.247-64 Alternate I

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101328; -- 52.247-64 Alternate II

UPDATE Clauses
SET editable_remarks = '1. Revising paragraph (i)(3)(i) of the clause\n2.Revising the first sentence under paragraph (3) of the definition of “acquisition savings” \n3. paragraph (i)(3)(i) of the clause and the first sentence under paragraph (3) of the definition of “acquisition savings” by substituting “under contracts awarded during the sharing period” for “during the sharing period.”' -- NULL
WHERE clause_id = 101337; -- 52.248-1 Alternate I

UPDATE Clauses
SET editable_remarks = '1. Revising paragraph (i)(3)(i) of the clause\n2.Revising the first sentence under paragraph (3) of the definition of “acquisition savings” \n3. paragraph (i)(3)(i) of the clause and the first sentence under paragraph (3) of the definition of “acquisition savings” by substituting “under contracts awarded during the sharing period” for “during the sharing period.”' -- NULL
WHERE clause_id = 101338; -- 52.248-1 Alternate II

UPDATE Clauses
SET editable_remarks = '1. Revising paragraph (i)(3)(i) of the clause\n2.Revising the first sentence under paragraph (3) of the definition of “acquisition savings” \n3. paragraph (i)(3)(i) of the clause and the first sentence under paragraph (3) of the definition of “acquisition savings” by substituting “under contracts awarded during the sharing period” for “during the sharing period.”' -- NULL
WHERE clause_id = 101339; -- 52.248-1 Alternate III

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101342; -- 52.248-3 Alternate I

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101344; -- 52.249-1 Alternate I

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101346; -- 52.249-10 Alternate I

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101347; -- 52.249-10 Alternate II

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101348; -- 52.249-10 Alternate III

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101352; -- 52.249-2 Alternate I

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101353; -- 52.249-2 Alternate II

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101354; -- 52.249-2 Alternate III

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101356; -- 52.249-3 Alternate I

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101360; -- 52.249-6 Alternate I

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101361; -- 52.249-6 Alternate II

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101362; -- 52.249-6 Alternate III

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101363; -- 52.249-6 Alternate IV

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101364; -- 52.249-6 Alternate V

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101367; -- 52.249-8 Alternate I

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101370; -- 52.250-1 Alternate I

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101373; -- 52.250-3 Alternate I

UPDATE Clauses
SET editable_remarks = 'PARAGRAPH (f)(2)(i) ONLY' -- NULL
WHERE clause_id = 101374; -- 52.250-3 Alternate II

UPDATE Clauses
SET is_editable = 0
WHERE clause_id = 101376; -- 52.250-4 Alternate I

UPDATE Clauses
SET editable_remarks = 'PARAGRAPH (f)(2)(i) ONLY' -- NULL
WHERE clause_id = 101377; -- 52.250-4 Alternate II

/*
*** End of SQL Scripts at Thu Jul 09 10:26:03 EDT 2015 ***
*/
