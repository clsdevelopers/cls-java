/*
Clause Parser Run at Fri Jan 15 15:48:29 EST 2016
(Without Correction Information)
*/
/* ===============================================
 * Prescriptions
   =============================================== */
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '10.003') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '11.107(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '11.107(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '11.204(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '11.204(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '11.204(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '11.204(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '11.304') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '11.404(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '11.404(a)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '11.404(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '11.503') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '11.503(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '11.503(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '11.503(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '11.604(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '11.604(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '11.703(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '11.703(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '11.703(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '12.301(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '12.301(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '12.301(b)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '12.301(b)(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '12.301(b)(4)(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '12.301(b)(4)(ii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '12.301(c)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '13.302-5(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '13.302-5(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '13.302-5(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '13.302-5(d)(3)(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '13.404') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(c)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(c)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(c)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(f)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(g)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(g)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(h)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(j)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(l)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(m)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(o)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(o)(2)(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(o)(2)(ii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(p)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(p)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(q)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(r)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(s)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(t)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(v)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(w)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(x)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-7(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-7(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-7(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-7(c)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-7(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.209(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.209(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.209(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.209(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.209(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.209(b)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.209(b)(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.209(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.209(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.209(f)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.209(h)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.408(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.408(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.408(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.408(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.408(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.408(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.408(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.408(f)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.408(f)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.408(g)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.408(h)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.408(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.408(j)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.408(k)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.408(l)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.408(m)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.408(n)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.408(n)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.408(n)(2)(iii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.105') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.203-4(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.203-4(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.203-4(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.205-4') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.206-4') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.30(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.307(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.307(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.307(a)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.307(a)(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.307(a)(5)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.307(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.307(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.307(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.307(e)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.307(e)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.307(f)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.307(f)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.307(g)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.406') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.406(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.406(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.506') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.506(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.506(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.506(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.506(d)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.506(d)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.506(d)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.506(d)(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.506(d)(5)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.506(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.506(f)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.506(g)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.601(f)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.601(f)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.601(f)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.603-4(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.603-4(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.603-4(b)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.603-4(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '17.109') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '17.109(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '17.208(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '17.208(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '17.208(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '17.208(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '17.208(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '17.208(f)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '17.208(g)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.1309(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.1309(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.1309(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.1309(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.1407') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.1506(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.1506(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.309(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.309(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.309(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.309(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.508(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.508(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.508(d)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.508(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.508(f)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.708(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.708(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.708(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.708(b)(1)(iii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.708(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.708(c)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.811-3') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.811-3(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.811-3(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.811-3(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.811-3(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.811-3(d)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.811-3(d)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.811-3(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2.101') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2.201') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '201.602-70') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2012-O0004') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2012-O0007') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2013-00017') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2013-00019') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2013-O0006') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2013-O0010') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2013-O0014') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2013-O0014)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2013-O0016') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2013-O0017') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2013-O0019') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2014-00014') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2014-O0004') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2014-O0005') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2014-O0009') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2014-O0010') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2014-O0016') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2014-O0018') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2014-O0020') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2014-OO0009') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2015-00003') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2015-00009') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2015-00016') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2015-00017') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2015-O0007') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2015-O0010') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2015-O0012') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2015-O0013') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2015-O0016') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2015-OO005') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2016-00001') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2016-00002') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2016-0001') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2016-0002') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2016-0003') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2016-O0003') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '203.1004(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '203.1004(b)(2)(ii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '203.171-4') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '203.171-4(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '203.570-3') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '203.97') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '204.1105') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '204.1202') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '204.404-70(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '204.404-70(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '204.404-70(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '204.470-3') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '204.7104-1(b)(3)(iv)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '204.7109(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '204.7109(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '204.7304(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '204.7304(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '204.7304(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '204.7304[c]') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '204.7403(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '204.7403(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '204.7403(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '205.47') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '206.302-3-70') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '208.7305(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '209.104-70') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '209.270-5') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '209.409') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '209.470-4(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '209.470-4(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '209.570-4(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '209.570-4(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '209.571-8(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '209.571-8(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '211.002-70') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '211.107') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '211.204(c)(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '211.204(c)(ii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '211.272') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '211.273-4') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '211.274-6(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '211.274-6(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '211.274-6(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '211.275-3') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '211.503') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '211.503(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '212.7103') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '213.106-2-70') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '215.370-3(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '215.370-3(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '215.371-6') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '215.408') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '215.408(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '215.408(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '215.408(3)(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '215.408(3)(ii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '215.408(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '215.408(4)(ii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '215.408(5)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '216.203-4-70(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '216.203-4-70(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '216.203-4-70(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '216.203-4-70(c)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '216.203-4-70(c)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '216.307(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '216.406') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '216.406(e)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '216.506(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '216.506(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '216.506(d)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '216.506(d)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '216.601(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '217.208-70(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '217.208-70(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '217.208-70(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '217.208-70(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '217.7005') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '217.7104(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '217.7303') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '217.7406(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '217.7702') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '219.309(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '219.708(b)(1)(A)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '219.708(b)(1)(B)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '219.811-3') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '219.811-3(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '219.811-3(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1006') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1006(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1006(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1006(c)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1006(c)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1006(e)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1006(e)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1006(e)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1006(e)(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1006(f)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.103-5(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.103-5(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1103') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1207') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1310(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1310(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1310(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1310(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1408(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1408(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1505(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1505(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1605') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1705(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1705(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1705(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1803') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1906') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.202') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.305') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.407(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.407(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.407(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.407(f)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.407(g)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.407(h)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.505(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.505(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.505(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.505(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.505-(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.505-(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.610') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.810(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.810(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.810(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.810(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.810(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.810(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.810(f)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.810(g)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '222.1771') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '222.7004') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '222.7102') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '222.7201(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '222.7201(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '222.7201(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '222.7302') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '222.7405') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '223.303') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '223.370-5') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '223.570-2') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '223.7106') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '223.7106(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '223.7106(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '223.7203') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '223.7306') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225-7011-3') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225-7799-4') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225-7799-4(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225-7799-4(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225-7799-4(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225-7799-4(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225-7799-4(f)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225..1101(10)(i)(C)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(1)(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(1)(ii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(10)(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(10)(i)(A)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(10)(i)(B)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(10)(i)(C)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(10)(i)(D)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(10)(i)(E)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(10)(i)(F)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(2)(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(2)(ii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(2)(iii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(5)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(5)(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(5)(ii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(6)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(6)(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(6)(ii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(7)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(8)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(9)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(9)(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(9)(ii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(9)(iii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(9)(iv)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(9)(v)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(9)(vi)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1103(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1103(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1103(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1103(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.302-6') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.371-5') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.371-5(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.372-2') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7002-2(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7002-3(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7002-3(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7003-5(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7003-5(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7003-5(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7006-4(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7006-4(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7007-3') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7009-5') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7011-3') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7012-3') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7017-4(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7017-5(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7017-5(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7102-4') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7204(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7204(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7307(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7307(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7402-5(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7503(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7503(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7503(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7503(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7503(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7503(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7503(b)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7503(b)(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7605') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7703-4(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7703-4(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7703-4(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7703-4(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.771-5') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.772-5') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7798-6(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7798-6(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7798-6(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7901-4') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7902-5(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7902-5(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '226.104') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.303(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.303(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7009-1') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7009-2') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7009-2(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7009-2(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7009-3(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7009-3(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7009-4(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7009-4(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7009-4(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7009-4(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7009-4(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7010') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7012') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7102-4(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7102-4(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7103-17') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7103-3(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7103-6(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7103-6(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7103-6(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7103-6(e)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7103-6(e)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7103-6(e)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7103-8(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7103-8(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7104(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7104(e)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7104(e)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7104(e)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7104(e)(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7104(e)(5)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7104(f)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7104(f)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7105-2(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7105-3') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7106(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7107-1(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7107-1(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7107-1(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7107-3') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7203-3(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7203-6(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7203-6(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7203-6(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7203-6(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7203-6(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7203-6(f)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7205(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '228.170') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '228.370(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '228.370(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '228.370(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '228.370(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '228.370(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '229.170-4') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '229.402-1') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '229.402-70(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '229.402-70(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '229.402-70(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '229.402-70(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '229.402-70(c)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '229.402-70(c)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '229.402-70(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '229.402-70(e)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '229.402-70(e)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '229.402-70(f)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '229.402-70(g)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '229.402-70(h)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '229.402-70(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '229.402-70(j)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '23.1005') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '23.1005(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '23.1005(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '23.1105') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '23.206') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '23.303') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '23.303(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '23.406(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '23.406(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '23.406(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '23.406(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '23.406(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '23.505') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '23.602') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '23.705(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '23.705(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '23.705(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '23.705(c)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '23.705(d)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '23.705(d)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '23.804(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '23.804(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '23.903') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '231.100-70') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '232.1005-70(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '232.1005-70(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '232.1110') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '232.412-70(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '232.412-70(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '232.412-70(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '232.502-4(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '232.502-4-70(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '232.7004(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '232.7004(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '232.705-70') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '232.7102') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '232.7202') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '232.806(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '232.908') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '233.215-70') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '234.203(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '234.203(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '234.7101(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '234.7101(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '234.7101(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '234.7101(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '234.7101(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '235.070-3') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '235.072') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '235.072(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '235.072(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '235.072(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '235.072(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '235.072(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '235.072(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '236.570(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '236.570(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '236.570(b)(2)(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '236.570(b)(2)(ii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '236.570(b)(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '236.570(b)(5)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '236.570(b)(6)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '236.570(c)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '236.570(c)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '236.570(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '236.609-70') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '236.609-70(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '237-7003(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '237.171-4') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '237.173-5') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '237.270(d)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '237.270(d)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '237.7003(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '237.7003(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '237.7003(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '237.7003(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '237.7101(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '237.7101(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '237.7101(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '237.7101(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '237.7101(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '237.7101(e)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '237.7101(e)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '237.7101(e)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '237.7101(f)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '237.7101(g)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '237.7402') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '237.7603(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '239.7103(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '239.7103(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '239.7306(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '239.7306(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '239.7411(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '239.7411(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '239.7411(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '239.7411(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '239.7603(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '239.7603(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '239.7604(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '239.7604(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '24.104(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '24.104(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '241.501-70(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '241.501-70(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '242.7001') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '242.7204') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '242.7503') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '243.205-70') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '243.205-71') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '244.305-71') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '244.305-71(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '244.305-71(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '244.403') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '245.107(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '245.107(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '245.107(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '245.107(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '245.107(5)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '246.270-4') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '246.370') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '246.371(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '246.710(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '246.710(1)(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '246.710(1)(ii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '246.710(1)(iii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '246.710(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '246.710(3)(i)(A)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '246.710(3)(i)(B)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '246.710(3)(i)(B)(iii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '246.870-3') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.207') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.207(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.270-4') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.270-4(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.270-4(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.270-4(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.270-4(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.270-4(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.270-4(f)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.270-4(g)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.271-3') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.271-3(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.271-3(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.271-3(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.271-3(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.271-3(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.271-3(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.271-3(g)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.271-3(h)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.271-3(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.271-3(j)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.271-3(k)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.271-3(l)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.271-3(m)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.271-3(n)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.305-70') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.574(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.574(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.574(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.574(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.574(b)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.574(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.574(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.574(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.574(f)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '249.501-70') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '249.7003(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.110') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1101(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1101(1)(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1101(1)(ii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1101(6)(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1101(6)(ii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1101(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1101(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1101(b)(1)(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1101(b)(1)(ii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1101(b)(1)(iii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1101(b)(1)(iv)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1101(b)(2)(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1101(b)(2)(ii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1101(b)(2)(iii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1101(b)(2)(iv)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1101(c)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1101(c)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1101(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1101(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1101(f)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1102(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1102(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1102(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1102(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1102(c)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1102(d)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1102(d)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1102(d)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1102(e)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1103') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1103(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1103(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1103(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1103(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.301-4') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.302-6') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '251.107') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '251.205') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '26.104') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '26.206(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '26.206(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '26.206(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '26.304') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '26.404') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.201-2') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.201-2(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.201-2(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.201-2(a)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.201-2(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.201-2(c)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.201-2(c)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.201-2(c)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.201-2(d)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.201-2(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.202-5(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.202-5(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.202-5(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.202-5(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.203-2') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.303(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.303(b)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.303(b)(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.303(b)(5)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.303(b)(6)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.303(b)(7)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.303(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.303(e)(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.303(e)(5)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.409(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.409(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.409(b)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.409(b)(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.409(b)(5)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.409(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.409(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.409(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.409(f)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.409(g)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.409(h)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.409(j)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.409(k)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.409(l)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '28.101-2') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '28.103-4') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '28.106-4(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '28.106-4(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '28.1102-3(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '28.1102-3(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '28.203-6') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '28.204-4') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '28.309(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '28.309(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '28.310') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '28.311-1') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '28.312') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '28.313(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '28.313(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '29.401-1') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '29.401-2') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '29.401-3') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '29.401-3(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '29.401-3(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '29.401-4(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '29.402-1(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '29.402-1(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '29.402-2(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '29.402-2(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '3.1004(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '3.1004(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '3.103-1') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '3.104-9(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '3.104-9(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '3.1106') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '3.202') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '3.301-1') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '3.404') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '3.502-3') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '3.503-2') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '3.808(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '3.808(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '3.907-7') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '3.908-9') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '30.201-3') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '30.201-3(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '30.201-3(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '30.201-4(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '30.201-4(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '30.201-4(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '30.201-4(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '30.201-4(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '30.202') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.009-2') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.1005') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.1005(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.1005(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.111(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.111(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.111(a)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.111(a)(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.111(a)(5)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.111(a)(6)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.111(a)(7)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.111(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.111(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.111(c)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.111(c)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.1110(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.1110(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.1110(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.1110(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.1110(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.1110(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.1110(g)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.205(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.206') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.206(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.206(g)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.412(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.412(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.412(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.412(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.412(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.412(f)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.502-3(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.502-3(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.502-3(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.502-4(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.502-4(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.502-4(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.502-4(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.611(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.611(a)&(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.611(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.706-1(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.706-1(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.706-2(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.706-2(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.706-3') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.806(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.806(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.806(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.908(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.908(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.908(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.908(c)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '33.106(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '33.106(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '33.215(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '33.215(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '34.104') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '34.203(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '34.203(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '34.203(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.501(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.502') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.503') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.504') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.505') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.506') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.507') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.508') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.509') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.510') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.511') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.512') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.513') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.514') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.515') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.516') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.517') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.518') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.519') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.520') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.521') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.522') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.523') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.609-1(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.609-2(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.609-3') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.609-4') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '37.110(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '37.110(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '37.110(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '37.113-2(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '37.113-2(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '37.115-3') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '37.116-2') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '37.304(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '37.304(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '37.304(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '37.403') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '39.106') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '4.103') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '4.1105(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '4.1105(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '4.1202(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '4.1202(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '4.1303') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '4.1403(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '4.1705(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '4.1705(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '4.1804(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '4.1804(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '4.1804(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '4.303') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '4.404(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '4.607(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '4.607(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '4.905') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '41.501(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '41.501(c)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '41.501(c)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '41.501(c)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '41.501(c)(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '41.501(c)(5)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '41.501(d)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '41.501(d)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '41.501(d)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '41.501(d)(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '41.501(d)(5)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '41.501(d)(6)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '41.501(d)(7)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '42.1107(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '42.1305(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '42.1305(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '42.1305(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '42.1305(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '42.703-2(f)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '42.709-6') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '42.802') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '42.903') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '43.107') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '43.205(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '43.205(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '43.205(a)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '43.205(a)(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '43.205(a)(5)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '43.205(a)(6)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '43.205(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '43.205(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '43.205(b)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '43.205(b)(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '43.205(b)(6)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '43.205(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '43.205(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '43.205(f)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '44.204(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '44.204(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '44.204(a)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '44.204(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '44.204(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '44.403') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '45.107(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '45.107(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '45.107(a)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '45.107(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '45.107(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.301') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.302') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.303') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.304') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.305') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.306') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.307(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.308') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.309') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.311') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.312') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.313') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.314') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.315') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.316') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.710(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.710(a)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.710(a)(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.710(a)(5)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.710(a)(6)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.710(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.710(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.710(b)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.710(b)(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.710(c)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.710(c)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.710(c)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.710(c)(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.710(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.710(e)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.710(e)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.805') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.805(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.805(a)(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.103-2') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.104-4') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-1(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-1(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-1(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-1(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-1(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-1(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-3(d)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-3(e)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-4(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-4(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-4(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-5(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-5(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-5(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-5(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-5(f)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-6(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-6(c)(5)(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-6(c)(5)(ii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-6(c)(6)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-7(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-7(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-7(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-8(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-8(a)(2)(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-8(a)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-8(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-9(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.208-2') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.303-1(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.303-10(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.303-11(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.303-12(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.303-13(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.303-14(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.303-15(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.303-16(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.303-17(f)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.303-2(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.303-3(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.303-4(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.303-5(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.303-6(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.303-7(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.303-8(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.303-9(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.304-7(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.305-12(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.305-13(a)(3)(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.305-14(b)(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.305-15(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.305-16(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.305-16(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.305-16(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.305-16(d)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.305-17') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.305-2(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.305-3(b)(4)(ii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.305-3(f)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.305-4(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.305-5(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.305-5(c)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.305-6(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.305-6(e)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.305-6(e)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.305-6(e)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.305-6(f)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.305-9(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.405') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.507(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.507(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.507(a)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '48.201') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '48.201(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '48.201(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '48.201(e)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '48.201(f)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '48.202') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '49.502(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '49.502(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '49.502(b)(1)(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '49.502(b)(1)(ii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '49.502(b)(1)(iii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '49.502(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '49.502(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '49.502(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '49.503(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '49.503(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '49.503(a)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '49.503(a)(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '49.503(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '49.504(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '49.504(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '49.504(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '49.504(c)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '49.504(c)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '49.504(c)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '49.505(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '49.505(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '50.104-4') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '50.206(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '50.206(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '50.206(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '50.206(b)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '50.206(c)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '50.206(c)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '50.206(c)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '50.206(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '50.403-3') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '51.107') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '51.205') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '52.107(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '52.107(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '52.107(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '52.107(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '52.107(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '52.107(f)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '53.111') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '7.203') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '7.305(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '7.305(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '7.305(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '7.404') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '8.005') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '8.1104(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '8.1104(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '8.1104(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '8.1104(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '8.505') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '9.104-7(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '9.104-7(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '9.104-7(c)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '9.108-5(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '9.108-5(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '9.206-2') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '9.308-1(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '9.308-1(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '9.308-1(a)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '9.308-1(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '9.308-1(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '9.308-1(b)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '9.308-2(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '9.308-2(a)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '9.308-2(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '9.308-2(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '9.308-2(b)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '9.409') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, 'NOTAPPLICABLE') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, 'PGI 204.7108(d)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, 'PGI 204.7108(d)(10)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, 'PGI 204.7108(d)(11)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, 'PGI 204.7108(d)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, 'PGI 204.7108(d)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, 'PGI 204.7108(d)(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, 'PGI 204.7108(d)(5)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, 'PGI 204.7108(d)(6)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, 'PGI 204.7108(d)(7)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, 'PGI 204.7108(d)(8)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, 'PGI 204.7108(d)(9)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;

UPDATE Prescriptions
JOIN (
	SELECT
		prescription_id,
		concat('http://www.ecfr.gov/cgi-bin/text-idx?node=se48.', volume , '.', part, '_1', subpart, replace(suffix, '-', '_6')) uri
	FROM (
		SELECT
			prescription_id,
			prescription_name,
			subpart,
			part,
			case when suffix like '%(%' then left(suffix,locate('(',suffix)-1) else suffix end suffix,
			case when part regexp '^[0-9]+$'
				then
					case
						when cast(part as UNSIGNED) >= '1' and cast(part as UNSIGNED) <= '51' then 1
						when cast(part as UNSIGNED) >= '52' and cast(part as UNSIGNED) <= '99' then 2
						when cast(part as UNSIGNED) >= '200' and cast(part as UNSIGNED) <= '299' then 3
					end
			end volume
		FROM (
			SELECT
				prescription_id,
				prescription_name,
				substring_index(substring_index(prescription_name,'(', 1),'.',1) part,
				substring_index(substring_index(substring_index(prescription_name,'(', 1),'.',-1),'-',1) subpart,
				case
					when locate('-',prescription_name) != 0
						then right(prescription_name, length(prescription_name) - locate('-',prescription_name)+1)
					else ''
				end suffix
			FROM Prescriptions
		) Prescriptions
	) Prescriptions
) sub ON (sub.prescription_id = Prescriptions.prescription_id)
SET Prescriptions.prescription_url = ifNull(sub.uri,'')
WHERE ifNull(prescription_url, '') = '';

/* ===============================================
 * Questions
   =============================================== */

UPDATE Question_Conditions
SET default_baseline = 0
WHERE question_id IN 
  (SELECT question_id FROM Questions WHERE question_code = '[DOCUMENT SECURITY LEVEL]' AND clause_version_id = 14); -- [DOCUMENT SECURITY LEVEL]


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT Question_Choce_Id, prescription_id FROM Question_Choices, Prescriptions
  WHERE prescription_name IN ('2.101') 
  AND choice_text = 'FOR USE IN DEFENDING AGAINST OR RECOVERY FROM NUCLEAR, BIOLOGICAL, CHEMICAL, OR RADIOLOGICAL ATTACK' AND  question_id IN (SELECT question_id FROM Questions WHERE clause_version_id = 14 AND question_code = '[PURPOSE OF PROCUREMENT]'); -- [PURPOSE OF PROCUREMENT] 'FOR USE IN DEFENDING AGAINST OR RECOVERY FROM NUCLEAR, BIOLOGICAL, CHEMICAL, OR RADIOLOGICAL ATTACK'

UPDATE Question_Conditions
SET question_condition_sequence = 1200 -- 1190
, default_baseline = 1
WHERE question_id IN 
  (SELECT question_id FROM Questions WHERE question_code = '[TYPE OF SYSTEM]' AND clause_version_id = 14); -- [TYPE OF SYSTEM]

UPDATE Question_Conditions
SET question_condition_sequence = 1230 -- 1220
WHERE question_id IN 
  (SELECT question_id FROM Questions WHERE question_code = '[CONTRACT TYPE]' AND clause_version_id = 14); -- [CONTRACT TYPE]

UPDATE Question_Conditions
SET question_condition_sequence = 1300 -- 1290
WHERE question_id IN 
  (SELECT question_id FROM Questions WHERE question_code = '[QUANTITY VARIATION]' AND clause_version_id = 14); -- [QUANTITY VARIATION]

UPDATE Question_Conditions
SET question_condition_sequence = 1310 -- 1300
WHERE question_id IN 
  (SELECT question_id FROM Questions WHERE question_code = '[OPTIONS]' AND clause_version_id = 14); -- [OPTIONS]

UPDATE Question_Conditions
SET question_condition_sequence = 1210 -- 1200
WHERE question_id IN 
  (SELECT question_id FROM Questions WHERE question_code = '[R&D TECHNOLOGIES]' AND clause_version_id = 14); -- [R&D TECHNOLOGIES]

UPDATE Question_Conditions
SET question_condition_sequence = 1090 -- 1075
WHERE question_id IN 
  (SELECT question_id FROM Questions WHERE question_code = '[COVERED SYSTEM]' AND clause_version_id = 14); -- [COVERED SYSTEM]

UPDATE Question_Conditions
SET question_condition_sequence = 1110 -- 1100
WHERE question_id IN 
  (SELECT question_id FROM Questions WHERE question_code = '[PORT OF ENTRY REQUIREMENTS]' AND clause_version_id = 14); -- [PORT OF ENTRY REQUIREMENTS]

UPDATE Question_Conditions
SET question_condition_sequence = 1130 -- 1120
WHERE question_id IN 
  (SELECT question_id FROM Questions WHERE question_code = '[INTRASTATE MOVEMENT]' AND clause_version_id = 14); -- [INTRASTATE MOVEMENT]

UPDATE Question_Conditions
SET question_condition_sequence = 1140 -- 1130
WHERE question_id IN 
  (SELECT question_id FROM Questions WHERE question_code = '[BILLS OF LADING]' AND clause_version_id = 14); -- [BILLS OF LADING]

UPDATE Question_Conditions
SET question_condition_sequence = 1150 -- 1140
WHERE question_id IN 
  (SELECT question_id FROM Questions WHERE question_code = '[UTILITY SERVICE CONDITIONS]' AND clause_version_id = 14); -- [UTILITY SERVICE CONDITIONS]

UPDATE Question_Conditions
SET question_condition_sequence = 1160 -- 1150
WHERE question_id IN 
  (SELECT question_id FROM Questions WHERE question_code = '[SERVICE CHARACTERISTICS]' AND clause_version_id = 14); -- [SERVICE CHARACTERISTICS]

UPDATE Question_Conditions
SET question_condition_sequence = 1170 -- 1160
WHERE question_id IN 
  (SELECT question_id FROM Questions WHERE question_code = '[SERVICE CONFIGURED FOR OCCUPANCY]' AND clause_version_id = 14); -- [SERVICE CONFIGURED FOR OCCUPANCY]

UPDATE Question_Conditions
SET question_condition_sequence = 1180 -- 1170
WHERE question_id IN 
  (SELECT question_id FROM Questions WHERE question_code = '[COMMERCIAL SERVICES]' AND clause_version_id = 14); -- [COMMERCIAL SERVICES]

UPDATE Question_Conditions
SET question_condition_sequence = 1190 -- 1180
WHERE question_id IN 
  (SELECT question_id FROM Questions WHERE question_code = '[SERVICE CONTRACT REPORTING]' AND clause_version_id = 14); -- [SERVICE CONTRACT REPORTING]

UPDATE Question_Conditions
SET question_condition_sequence = 1220 -- 1210
WHERE question_id IN 
  (SELECT question_id FROM Questions WHERE question_code = '[OSD APPROVAL FOR DATA SYSTEM]' AND clause_version_id = 14); -- [OSD APPROVAL FOR DATA SYSTEM]

UPDATE Question_Conditions
SET question_condition_sequence = 1250 -- 1240
WHERE question_id IN 
  (SELECT question_id FROM Questions WHERE question_code = '[SWING FROM TARGET FEE]' AND clause_version_id = 14); -- [SWING FROM TARGET FEE]

UPDATE Question_Conditions
SET question_condition_sequence = 1270 -- 1260
WHERE question_id IN 
  (SELECT question_id FROM Questions WHERE question_code = '[SMALL BUSINESS AWARD FEE]' AND clause_version_id = 14); -- [SMALL BUSINESS AWARD FEE]

UPDATE Question_Conditions
SET question_condition_sequence = 1280 -- 1270
WHERE question_id IN 
  (SELECT question_id FROM Questions WHERE question_code = '[MAJOR DESIGN OR DEVELOPMENT]' AND clause_version_id = 14); -- [MAJOR DESIGN OR DEVELOPMENT]

UPDATE Question_Conditions
SET question_condition_sequence = 1290 -- 1280
WHERE question_id IN 
  (SELECT question_id FROM Questions WHERE question_code = '[PCO EPA DETERMINATION]' AND clause_version_id = 14); -- [PCO EPA DETERMINATION]

UPDATE Question_Conditions
SET question_condition_sequence = 1320 -- 1310
WHERE question_id IN 
  (SELECT question_id FROM Questions WHERE question_code = '[OPTION CONDITIONS]' AND clause_version_id = 14); -- [OPTION CONDITIONS]

UPDATE Question_Conditions
SET question_condition_sequence = 1330 -- 1320
WHERE question_id IN 
  (SELECT question_id FROM Questions WHERE question_code = '[PERFORMANCE REQUIREMENTS 1]' AND clause_version_id = 14); -- [PERFORMANCE REQUIREMENTS 1]

UPDATE Question_Conditions
SET question_condition_sequence = 1350 -- 1340
WHERE question_id IN 
  (SELECT question_id FROM Questions WHERE question_code = '[PERFORMANCE REQUIREMENTS 2]' AND clause_version_id = 14); -- [PERFORMANCE REQUIREMENTS 2]

UPDATE Question_Conditions
SET question_condition_sequence = 1390 -- 1380
WHERE question_id IN 
  (SELECT question_id FROM Questions WHERE question_code = '[LABOR REQUIREMENTS 1]' AND clause_version_id = 14); -- [LABOR REQUIREMENTS 1]

UPDATE Question_Conditions
SET question_condition_sequence = 1400 -- 1390
WHERE question_id IN 
  (SELECT question_id FROM Questions WHERE question_code = '[LABOR REQUIREMENTS 2]' AND clause_version_id = 14); -- [LABOR REQUIREMENTS 2]

UPDATE Question_Conditions
SET question_condition_sequence = 1420 -- 1410
WHERE question_id IN 
  (SELECT question_id FROM Questions WHERE question_code = '[LABOR REQUIREMENTS 3]' AND clause_version_id = 14); -- [LABOR REQUIREMENTS 3]

UPDATE Question_Conditions
SET question_condition_sequence = 1440 -- 1430
WHERE question_id IN 
  (SELECT question_id FROM Questions WHERE question_code = '[CONTRACTOR MATERIAL]' AND clause_version_id = 14); -- [CONTRACTOR MATERIAL]

UPDATE Question_Conditions
SET question_condition_sequence = 1470 -- 1460
WHERE question_id IN 
  (SELECT question_id FROM Questions WHERE question_code = '[SUBCONTRACTING]' AND clause_version_id = 14); -- [SUBCONTRACTING]

UPDATE Question_Conditions
SET question_condition_sequence = 1490 -- 1480
WHERE question_id IN 
  (SELECT question_id FROM Questions WHERE question_code = '[ENVIRONMENT]' AND clause_version_id = 14); -- [ENVIRONMENT]
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT Question_Choce_Id, prescription_id FROM Question_Choices, Prescriptions
  WHERE prescription_name IN ('225.7017-5(a)(1)') 
  AND choice_text = 'CONTRACTOR REQUIRED TO INSTALL A PHOTVOLTAIC DEVICE IN THE UNITED STATES OR IN A FACILITY OWNED BY DOD' AND  question_id IN (SELECT question_id FROM Questions WHERE clause_version_id = 14 AND question_code = '[ENVIRONMENT]'); -- [ENVIRONMENT] 'CONTRACTOR REQUIRED TO INSTALL A PHOTVOLTAIC DEVICE IN THE UNITED STATES OR IN A FACILITY OWNED BY DOD'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT Question_Choce_Id, prescription_id FROM Question_Choices, Prescriptions
  WHERE prescription_name IN ('225.7017-5(a)(1)') 
  AND choice_text = 'CONTRACTOR REQUIRED TO RESERVE A PHOTOVOLTAIC DEVICE FOR THE EXCLUSIVE USE OF DOD IN THE UNITED STATES FOR THE FULL ECONOMIC LIFE OF THE DEVICE' AND  question_id IN (SELECT question_id FROM Questions WHERE clause_version_id = 14 AND question_code = '[ENVIRONMENT]'); -- [ENVIRONMENT] 'CONTRACTOR REQUIRED TO RESERVE A PHOTOVOLTAIC DEVICE FOR THE EXCLUSIVE USE OF DOD IN THE UNITED STATES FOR THE FULL ECONOMIC LIFE OF THE DEVICE'

UPDATE Question_Conditions
SET question_condition_sequence = 1500 -- 1490
WHERE question_id IN 
  (SELECT question_id FROM Questions WHERE question_code = '[INTEREST ON PARTIAL PAYMENTS]' AND clause_version_id = 14); -- [INTEREST ON PARTIAL PAYMENTS]

UPDATE Question_Conditions
SET question_condition_sequence = 1510 -- 1500
WHERE question_id IN 
  (SELECT question_id FROM Questions WHERE question_code = '[TERMINATION]' AND clause_version_id = 14); -- [TERMINATION]

UPDATE Question_Conditions
SET question_condition_sequence = 1340 -- 1330
WHERE question_id IN 
  (SELECT question_id FROM Questions WHERE question_code = '[TECHNICAL CHANGES]' AND clause_version_id = 14); -- [TECHNICAL CHANGES]

UPDATE Question_Conditions
SET question_condition_sequence = 1360 -- 1350
WHERE question_id IN 
  (SELECT question_id FROM Questions WHERE question_code = '[VALUE ENGINEERING CONDITIONS]' AND clause_version_id = 14); -- [VALUE ENGINEERING CONDITIONS]

UPDATE Question_Conditions
SET question_condition_sequence = 1370 -- 1360
WHERE question_id IN 
  (SELECT question_id FROM Questions WHERE question_code = '[FIRST ARTICLE CONDITIONS]' AND clause_version_id = 14); -- [FIRST ARTICLE CONDITIONS]

UPDATE Question_Conditions
SET question_condition_sequence = 1380 -- 1370
WHERE question_id IN 
  (SELECT question_id FROM Questions WHERE question_code = '[DD FORM 1494]' AND clause_version_id = 14); -- [DD FORM 1494]

UPDATE Question_Conditions
SET question_condition_sequence = 1410 -- 1400
WHERE question_id IN 
  (SELECT question_id FROM Questions WHERE question_code = '[LABOR ACTS]' AND clause_version_id = 14); -- [LABOR ACTS]

UPDATE Question_Conditions
SET question_condition_sequence = 1430 -- 1420
WHERE question_id IN 
  (SELECT question_id FROM Questions WHERE question_code = '[DRUG FREE WORKFORCE REQUIRED]' AND clause_version_id = 14); -- [DRUG FREE WORKFORCE REQUIRED]

UPDATE Question_Conditions
SET question_condition_sequence = 1450 -- 1440
WHERE question_id IN 
  (SELECT question_id FROM Questions WHERE question_code = '[GOVERNMENT FURNISHED PROPERTY]' AND clause_version_id = 14); -- [GOVERNMENT FURNISHED PROPERTY]

UPDATE Question_Conditions
SET question_condition_sequence = 1460 -- 1450
WHERE question_id IN 
  (SELECT question_id FROM Questions WHERE question_code = '[GOVERNMENT SUPPLY STATEMENT]' AND clause_version_id = 14); -- [GOVERNMENT SUPPLY STATEMENT]

UPDATE Question_Conditions
SET question_condition_sequence = 1480 -- 1470
WHERE question_id IN 
  (SELECT question_id FROM Questions WHERE question_code = '[SUBCONTRACTING PLAN CONDITIONS]' AND clause_version_id = 14); -- [SUBCONTRACTING PLAN CONDITIONS]

UPDATE Question_Conditions
SET question_condition_sequence = 1520 -- 1510
WHERE question_id IN 
  (SELECT question_id FROM Questions WHERE question_code = '[END ITEM MARKING]' AND clause_version_id = 14); -- [END ITEM MARKING]

UPDATE Question_Conditions
SET question_condition_sequence = 1530 -- 1520
, default_baseline = 1
WHERE question_id IN 
  (SELECT question_id FROM Questions WHERE question_code = '[PRESERVATION AND PACKING SPECIFICATIONS]' AND clause_version_id = 14); -- [PRESERVATION AND PACKING SPECIFICATIONS]

UPDATE Question_Conditions
SET question_condition_sequence = 1540 -- 1530
WHERE question_id IN 
  (SELECT question_id FROM Questions WHERE question_code = '[INSPECTION AND ACCEPTANCE REQUIREMENTS]' AND clause_version_id = 14); -- [INSPECTION AND ACCEPTANCE REQUIREMENTS]

UPDATE Question_Conditions
SET question_condition_sequence = 1550 -- 1540
WHERE question_id IN 
  (SELECT question_id FROM Questions WHERE question_code = '[INSPECTION AND ACCEPTANCE REQUIREMENTS 2]' AND clause_version_id = 14); -- [INSPECTION AND ACCEPTANCE REQUIREMENTS 2]

UPDATE Question_Conditions
SET question_condition_sequence = 1560 -- 1550
WHERE question_id IN 
  (SELECT question_id FROM Questions WHERE question_code = '[USE OF WARRANTY]' AND clause_version_id = 14); -- [USE OF WARRANTY]

UPDATE Question_Conditions
SET question_condition_sequence = 1570 -- 1560
WHERE question_id IN 
  (SELECT question_id FROM Questions WHERE question_code = '[WARRANTY CONDITIONS]' AND clause_version_id = 14); -- [WARRANTY CONDITIONS]

UPDATE Question_Conditions
SET question_condition_sequence = 1580 -- 1570
WHERE question_id IN 
  (SELECT question_id FROM Questions WHERE question_code = '[WARRANTY RECOVERY AND TRANSPORTATION]' AND clause_version_id = 14); -- [WARRANTY RECOVERY AND TRANSPORTATION]

UPDATE Question_Conditions
SET question_condition_sequence = 1590 -- 1580
WHERE question_id IN 
  (SELECT question_id FROM Questions WHERE question_code = '[WARRANTY DESCRIPTION]' AND clause_version_id = 14); -- [WARRANTY DESCRIPTION]

UPDATE Question_Conditions
SET question_condition_sequence = 1600 -- 1590
WHERE question_id IN 
  (SELECT question_id FROM Questions WHERE question_code = '[PERIOD OF PERFORMANCE]' AND clause_version_id = 14); -- [PERIOD OF PERFORMANCE]

UPDATE Question_Conditions
SET question_condition_sequence = 1610 -- 1600
WHERE question_id IN 
  (SELECT question_id FROM Questions WHERE question_code = '[PLACE OF PERFORMANCE]' AND clause_version_id = 14); -- [PLACE OF PERFORMANCE]

UPDATE Question_Conditions
SET question_condition_sequence = 1670 -- 1660
WHERE question_id IN 
  (SELECT question_id FROM Questions WHERE question_code = '[OUTSIDE U.S.]' AND clause_version_id = 14); -- [OUTSIDE U.S.]

UPDATE Question_Conditions
SET question_condition_sequence = 1680 -- 1670
WHERE question_id IN 
  (SELECT question_id FROM Questions WHERE question_code = '[PLACE TYPE]' AND clause_version_id = 14); -- [PLACE TYPE]

UPDATE Question_Conditions
SET question_condition_sequence = 1700 -- 1690
WHERE question_id IN 
  (SELECT question_id FROM Questions WHERE question_code = '[DELIVERY BASIS]' AND clause_version_id = 14); -- [DELIVERY BASIS]

UPDATE Question_Conditions
SET question_condition_sequence = 1710 -- 1700
WHERE question_id IN 
  (SELECT question_id FROM Questions WHERE question_code = '[CONTINUED PERFORMANCE]' AND clause_version_id = 14); -- [CONTINUED PERFORMANCE]

UPDATE Question_Conditions
SET question_condition_sequence = 1720 -- 1710
WHERE question_id IN 
  (SELECT question_id FROM Questions WHERE question_code = '[LIQUIDATED DAMAGES]' AND clause_version_id = 14); -- [LIQUIDATED DAMAGES]

UPDATE Question_Conditions
SET question_condition_sequence = 1730 -- 1720
WHERE question_id IN 
  (SELECT question_id FROM Questions WHERE question_code = '[SEPARATE DELIVERABLES]' AND clause_version_id = 14); -- [SEPARATE DELIVERABLES]

UPDATE Question_Conditions
SET question_condition_sequence = 1740 -- 1730
WHERE question_id IN 
  (SELECT question_id FROM Questions WHERE question_code = '[PACE OF WORK]' AND clause_version_id = 14); -- [PACE OF WORK]

UPDATE Question_Conditions
SET question_condition_sequence = 1620 -- 1610
WHERE question_id IN 
  (SELECT question_id FROM Questions WHERE question_code = '[FOREIGN COUNTRY]' AND clause_version_id = 14); -- [FOREIGN COUNTRY]

UPDATE Question_Conditions
SET question_condition_sequence = 1630 -- 1620
WHERE question_id IN 
  (SELECT question_id FROM Questions WHERE question_code = '[MILITARY TECHNICAL AGREEMENT]' AND clause_version_id = 14); -- [MILITARY TECHNICAL AGREEMENT]

UPDATE Question_Conditions
SET question_condition_sequence = 1650 -- 1640
WHERE question_id IN 
  (SELECT question_id FROM Questions WHERE question_code = '[STATE]' AND clause_version_id = 14); -- [STATE]

UPDATE Question_Conditions
SET question_condition_sequence = 1660 -- 1650
WHERE question_id IN 
  (SELECT question_id FROM Questions WHERE question_code = '[UNEMPLOYMENT RATE]' AND clause_version_id = 14); -- [UNEMPLOYMENT RATE]

UPDATE Question_Conditions
SET question_condition_sequence = 1750 -- 1740
WHERE question_id IN 
  (SELECT question_id FROM Questions WHERE question_code = '[TRANSPORTATION REQUIREMENTS 1]' AND clause_version_id = 14); -- [TRANSPORTATION REQUIREMENTS 1]

UPDATE Question_Conditions
SET question_condition_sequence = 1790 -- 1780
WHERE question_id IN 
  (SELECT question_id FROM Questions WHERE question_code = '[TRANSPORTATION REQUIREMENTS 2]' AND clause_version_id = 14); -- [TRANSPORTATION REQUIREMENTS 2]

UPDATE Question_Conditions
SET question_condition_sequence = 1800 -- 1790
WHERE question_id IN 
  (SELECT question_id FROM Questions WHERE question_code = '[TRANSPORTATION REQUIREMENTS 3]' AND clause_version_id = 14); -- [TRANSPORTATION REQUIREMENTS 3]

UPDATE Question_Conditions
SET question_condition_sequence = 1810 -- 1800
WHERE question_id IN 
  (SELECT question_id FROM Questions WHERE question_code = '[TRANSPORTATION REQUIREMENTS 4]' AND clause_version_id = 14); -- [TRANSPORTATION REQUIREMENTS 4]

UPDATE Question_Conditions
SET question_condition_sequence = 1820 -- 1810
WHERE question_id IN 
  (SELECT question_id FROM Questions WHERE question_code = '[TRANSPORTATION REQUIREMENTS 5]' AND clause_version_id = 14); -- [TRANSPORTATION REQUIREMENTS 5]

UPDATE Question_Conditions
SET question_condition_sequence = 1840 -- 1830
WHERE question_id IN 
  (SELECT question_id FROM Questions WHERE question_code = '[TRANSPORTATION REQUIREMENTS 6]' AND clause_version_id = 14); -- [TRANSPORTATION REQUIREMENTS 6]

UPDATE Question_Conditions
SET question_condition_sequence = 1850 -- 1840
WHERE question_id IN 
  (SELECT question_id FROM Questions WHERE question_code = '[TRANSPORTATION REQUIREMENTS 7]' AND clause_version_id = 14); -- [TRANSPORTATION REQUIREMENTS 7]

UPDATE Question_Conditions
SET question_condition_sequence = 1860 -- 1850
WHERE question_id IN 
  (SELECT question_id FROM Questions WHERE question_code = '[TRANSPORTATION REQUIREMENT 8]' AND clause_version_id = 14); -- [TRANSPORTATION REQUIREMENT 8]

UPDATE Question_Conditions
SET question_condition_sequence = 1870 -- 1860
WHERE question_id IN 
  (SELECT question_id FROM Questions WHERE question_code = '[TRANSPORTATION REQUIREMENTS 9]' AND clause_version_id = 14); -- [TRANSPORTATION REQUIREMENTS 9]

UPDATE Question_Conditions
SET question_condition_sequence = 1880 -- 1870
WHERE question_id IN 
  (SELECT question_id FROM Questions WHERE question_code = '[TRANSPORTATION REQUIREMENTS 10]' AND clause_version_id = 14); -- [TRANSPORTATION REQUIREMENTS 10]

UPDATE Question_Conditions
SET question_condition_sequence = 1890 -- 1880
WHERE question_id IN 
  (SELECT question_id FROM Questions WHERE question_code = '[TRANSPORTATION REQUIREMENTS 11]' AND clause_version_id = 14); -- [TRANSPORTATION REQUIREMENTS 11]

UPDATE Question_Conditions
SET question_condition_sequence = 1900 -- 1890
WHERE question_id IN 
  (SELECT question_id FROM Questions WHERE question_code = '[SHIPPING STATEMENTS]' AND clause_version_id = 14); -- [SHIPPING STATEMENTS]

UPDATE Question_Conditions
SET question_condition_sequence = 1760 -- 1750
WHERE question_id IN 
  (SELECT question_id FROM Questions WHERE question_code = '[FOB ORIGIN DESIGNATIONS]' AND clause_version_id = 14); -- [FOB ORIGIN DESIGNATIONS]

UPDATE Question_Conditions
SET question_condition_sequence = 1770 -- 1760
WHERE question_id IN 
  (SELECT question_id FROM Questions WHERE question_code = '[WITHIN CONSIGNEE''S PREMISES]' AND clause_version_id = 14); -- [WITHIN CONSIGNEE'S PREMISES]

UPDATE Question_Conditions
SET question_condition_sequence = 1780 -- 1770
WHERE question_id IN 
  (SELECT question_id FROM Questions WHERE question_code = '[FOB DESIGNATION]' AND clause_version_id = 14); -- [FOB DESIGNATION]

UPDATE Question_Conditions
SET question_condition_sequence = 1830 -- 1820
WHERE question_id IN 
  (SELECT question_id FROM Questions WHERE question_code = '[CARGO PREFERENCE ACT]' AND clause_version_id = 14); -- [CARGO PREFERENCE ACT]

UPDATE Question_Conditions
SET question_condition_sequence = 1910 -- 1900
WHERE question_id IN 
  (SELECT question_id FROM Questions WHERE question_code = '[ACO RESPONSIBILITIES]' AND clause_version_id = 14); -- [ACO RESPONSIBILITIES]

UPDATE Question_Conditions
SET question_condition_sequence = 1930 -- 1920
WHERE question_id IN 
  (SELECT question_id FROM Questions WHERE question_code = '[FUNDING FY]' AND clause_version_id = 14); -- [FUNDING FY]

UPDATE Question_Conditions
SET question_condition_sequence = 1980 -- 1970
WHERE question_id IN 
  (SELECT question_id FROM Questions WHERE question_code = '[ACRN ASSIGNMENT]' AND clause_version_id = 14); -- [ACRN ASSIGNMENT]

UPDATE Question_Conditions
SET question_condition_sequence = 2010 -- 2000
WHERE question_id IN 
  (SELECT question_id FROM Questions WHERE question_code = '[FULLY OR INCREMENTAL FUNDING]' AND clause_version_id = 14); -- [FULLY OR INCREMENTAL FUNDING]

UPDATE Question_Conditions
SET question_condition_sequence = 2020 -- 2010
WHERE question_id IN 
  (SELECT question_id FROM Questions WHERE question_code = '[FINANCING ARRANGEMENTS]' AND clause_version_id = 14); -- [FINANCING ARRANGEMENTS]

UPDATE Question_Conditions
SET question_condition_sequence = 2060 -- 2050
WHERE question_id IN 
  (SELECT question_id FROM Questions WHERE question_code = '[BILLING ARRANGEMENTS]' AND clause_version_id = 14); -- [BILLING ARRANGEMENTS]

UPDATE Question_Conditions
SET question_condition_sequence = 2090 -- 2080
WHERE question_id IN 
  (SELECT question_id FROM Questions WHERE question_code = '[PAYMENT INSTRUCTIONS]' AND clause_version_id = 14); -- [PAYMENT INSTRUCTIONS]

UPDATE Question_Conditions
SET question_condition_sequence = 2100 -- 2090
WHERE question_id IN 
  (SELECT question_id FROM Questions WHERE question_code = '[RAPID LIQUIDATION]' AND clause_version_id = 14); -- [RAPID LIQUIDATION]

UPDATE Question_Conditions
SET question_condition_sequence = 2110 -- 2100
WHERE question_id IN 
  (SELECT question_id FROM Questions WHERE question_code = '[CONTRACT PRIOR TO FUNDS]' AND clause_version_id = 14); -- [CONTRACT PRIOR TO FUNDS]

UPDATE Question_Conditions
SET question_condition_sequence = 1920 -- 1910
WHERE question_id IN 
  (SELECT question_id FROM Questions WHERE question_code = '[BASE, POST, CAMP OR STATION]' AND clause_version_id = 14); -- [BASE, POST, CAMP OR STATION]

UPDATE Question_Conditions
SET question_condition_sequence = 1940 -- 1930
WHERE question_id IN 
  (SELECT question_id FROM Questions WHERE question_code = '[TYPE OF FUNDS]' AND clause_version_id = 14); -- [TYPE OF FUNDS]

UPDATE Question_Conditions
SET question_condition_sequence = 1950 -- 1940
WHERE question_id IN 
  (SELECT question_id FROM Questions WHERE question_code = '[LEGISLATIVE SOURCE OF FUNDS]' AND clause_version_id = 14); -- [LEGISLATIVE SOURCE OF FUNDS]

UPDATE Question_Conditions
SET question_condition_sequence = 1960 -- 1950
WHERE question_id IN 
  (SELECT question_id FROM Questions WHERE question_code = '[ANNUAL FUNDS EXTENDED]' AND clause_version_id = 14); -- [ANNUAL FUNDS EXTENDED]

UPDATE Question_Conditions
SET question_condition_sequence = 1970 -- 1960
WHERE question_id IN 
  (SELECT question_id FROM Questions WHERE question_code = '[LIABILITY FOR SPECIAL COSTS]' AND clause_version_id = 14); -- [LIABILITY FOR SPECIAL COSTS]

UPDATE Question_Conditions
SET question_condition_sequence = 1990 -- 1980
WHERE question_id IN 
  (SELECT question_id FROM Questions WHERE question_code = '[ACRN CONTRACT WIDE]' AND clause_version_id = 14); -- [ACRN CONTRACT WIDE]

UPDATE Question_Conditions
SET question_condition_sequence = 2000 -- 1990
WHERE question_id IN 
  (SELECT question_id FROM Questions WHERE question_code = '[ACRN BY LINE ITEM]' AND clause_version_id = 14); -- [ACRN BY LINE ITEM]

UPDATE Question_Conditions
SET question_condition_sequence = 2030 -- 2020
WHERE question_id IN 
  (SELECT question_id FROM Questions WHERE question_code = '[ADVANCED PAYMENT CONDITIONS]' AND clause_version_id = 14); -- [ADVANCED PAYMENT CONDITIONS]

UPDATE Question_Conditions
SET question_condition_sequence = 2040 -- 2030
WHERE question_id IN 
  (SELECT question_id FROM Questions WHERE question_code = '[PROGRESS PAYMENT CONDITIONS]' AND clause_version_id = 14); -- [PROGRESS PAYMENT CONDITIONS]

UPDATE Question_Conditions
SET question_condition_sequence = 2050 -- 2040
WHERE question_id IN 
  (SELECT question_id FROM Questions WHERE question_code = '[BASIS OF PERFORMANCE BASED PAYMENTS]' AND clause_version_id = 14); -- [BASIS OF PERFORMANCE BASED PAYMENTS]

UPDATE Question_Conditions
SET question_condition_sequence = 2070 -- 2060
WHERE question_id IN 
  (SELECT question_id FROM Questions WHERE question_code = '[EFT EXCEPTIONS]' AND clause_version_id = 14); -- [EFT EXCEPTIONS]

UPDATE Question_Conditions
SET question_condition_sequence = 2080 -- 2070
WHERE question_id IN 
  (SELECT question_id FROM Questions WHERE question_code = '[PURCHASE CARD]' AND clause_version_id = 14); -- [PURCHASE CARD]

UPDATE Question_Conditions
SET question_condition_sequence = 2120 -- 2110
WHERE question_id IN 
  (SELECT question_id FROM Questions WHERE question_code = '[INSURANCE CONDITIONS]' AND clause_version_id = 14); -- [INSURANCE CONDITIONS]

UPDATE Question_Conditions
SET question_condition_sequence = 2130 -- 2120
WHERE question_id IN 
  (SELECT question_id FROM Questions WHERE question_code = '[BID GUARANTEE OR BOND]' AND clause_version_id = 14); -- [BID GUARANTEE OR BOND]

UPDATE Question_Conditions
SET question_condition_sequence = 2140 -- 2130
WHERE question_id IN 
  (SELECT question_id FROM Questions WHERE question_code = '[NO TIME TO PROCESS BUY AMERICAN DETERMINATION]' AND clause_version_id = 14); -- [NO TIME TO PROCESS BUY AMERICAN DETERMINATION]

UPDATE Question_Conditions
SET question_condition_sequence = 2150 -- 2140
WHERE question_id IN 
  (SELECT question_id FROM Questions WHERE question_code = '[DISPUTES ACT]' AND clause_version_id = 14); -- [DISPUTES ACT]

UPDATE Question_Conditions
SET question_condition_sequence = 2160 -- 2150
WHERE question_id IN 
  (SELECT question_id FROM Questions WHERE question_code = '[WAIVERS]' AND clause_version_id = 14); -- [WAIVERS]

UPDATE Question_Conditions
SET question_condition_sequence = 2170 -- 2160
WHERE question_id IN 
  (SELECT question_id FROM Questions WHERE question_code = '[EXEMPTIONS]' AND clause_version_id = 14); -- [EXEMPTIONS]

UPDATE Question_Conditions
SET question_condition_sequence = 2180 -- 2170
WHERE question_id IN 
  (SELECT question_id FROM Questions WHERE question_code = '[EXCEPTIONS]' AND clause_version_id = 14); -- [EXCEPTIONS]

UPDATE Question_Conditions
SET question_condition_sequence = 2190 -- 2180
WHERE question_id IN 
  (SELECT question_id FROM Questions WHERE question_code = '[PATENT COUNCIL RECOMMENDED]' AND clause_version_id = 14); -- [PATENT COUNCIL RECOMMENDED]

UPDATE Question_Conditions
SET question_condition_sequence = 2200 -- 2190
WHERE question_id IN 
  (SELECT question_id FROM Questions WHERE question_code = '[PRICING REQUIREMENTS]' AND clause_version_id = 14); -- [PRICING REQUIREMENTS]

UPDATE Question_Conditions
SET question_condition_sequence = 2250 -- 2240
WHERE question_id IN 
  (SELECT question_id FROM Questions WHERE question_code = '[CAS]' AND clause_version_id = 14); -- [CAS]

UPDATE Question_Conditions
SET question_condition_sequence = 2260 -- 2250
WHERE question_id IN 
  (SELECT question_id FROM Questions WHERE question_code = '[REGULATION PRICING]' AND clause_version_id = 14); -- [REGULATION PRICING]

UPDATE Question_Conditions
SET question_condition_sequence = 2270 -- 2260
WHERE question_id IN 
  (SELECT question_id FROM Questions WHERE question_code = '[DIRECT COST]' AND clause_version_id = 14); -- [DIRECT COST]

UPDATE Question_Conditions
SET question_condition_sequence = 2280 -- 2270
WHERE question_id IN 
  (SELECT question_id FROM Questions WHERE question_code = '[INDIRECT COST]' AND clause_version_id = 14); -- [INDIRECT COST]

UPDATE Question_Conditions
SET question_condition_sequence = 2290 -- 2280
WHERE question_id IN 
  (SELECT question_id FROM Questions WHERE question_code = '[PRICING LIMITATIONS]' AND clause_version_id = 14); -- [PRICING LIMITATIONS]

UPDATE Question_Conditions
SET question_condition_sequence = 2300 -- 2290
WHERE question_id IN 
  (SELECT question_id FROM Questions WHERE question_code = '[TRANSPORTATION COST]' AND clause_version_id = 14); -- [TRANSPORTATION COST]

UPDATE Question_Conditions
SET question_condition_sequence = 2310 -- 2300
WHERE question_id IN 
  (SELECT question_id FROM Questions WHERE question_code = '[PROPOSAL REQUIREMENTS]' AND clause_version_id = 14); -- [PROPOSAL REQUIREMENTS]

UPDATE Question_Conditions
SET question_condition_sequence = 2320 -- 2310
WHERE question_id IN 
  (SELECT question_id FROM Questions WHERE question_code = '[AWARD SCENARIOS]' AND clause_version_id = 14); -- [AWARD SCENARIOS]

UPDATE Question_Conditions
SET question_condition_sequence = 2210 -- 2200
WHERE question_id IN 
  (SELECT question_id FROM Questions WHERE question_code = '[EXCESS PASS-THROUGH CHARGES]' AND clause_version_id = 14); -- [EXCESS PASS-THROUGH CHARGES]

UPDATE Question_Conditions
SET question_condition_sequence = 2220 -- 2210
WHERE question_id IN 
  (SELECT question_id FROM Questions WHERE question_code = '[UCA]' AND clause_version_id = 14); -- [UCA]

UPDATE Question_Conditions
SET question_condition_sequence = 2230 -- 2220
WHERE question_id IN 
  (SELECT question_id FROM Questions WHERE question_code = '[INAPPROPRIATE CONTINGENCY]' AND clause_version_id = 14); -- [INAPPROPRIATE CONTINGENCY]

UPDATE Question_Conditions
SET question_condition_sequence = 2240 -- 2230
WHERE question_id IN 
  (SELECT question_id FROM Questions WHERE question_code = '[CURRENCY]' AND clause_version_id = 14); -- [CURRENCY]

UPDATE Question_Conditions
SET question_condition_sequence = 1100 -- 1090
WHERE question_id IN 
  (SELECT question_id FROM Questions WHERE question_code = '[SUPPORT SERVICES]' AND clause_version_id = 14); -- [SUPPORT SERVICES]

UPDATE Question_Conditions
SET question_condition_sequence = 1120 -- 1110
WHERE question_id IN 
  (SELECT question_id FROM Questions WHERE question_code = '[TRANSPORTATION/STORAGE SERVICES]' AND clause_version_id = 14); -- [TRANSPORTATION/STORAGE SERVICES]

UPDATE Question_Conditions
SET question_condition_sequence = 1240 -- 1230
WHERE question_id IN 
  (SELECT question_id FROM Questions WHERE question_code = '[COST REIMBURSEMENT]' AND clause_version_id = 14); -- [COST REIMBURSEMENT]

UPDATE Question_Conditions
SET question_condition_sequence = 1260 -- 1250
WHERE question_id IN 
  (SELECT question_id FROM Questions WHERE question_code = '[FIXED PRICE]' AND clause_version_id = 14); -- [FIXED PRICE]

UPDATE Question_Conditions
SET question_condition_sequence = 1640 -- 1630
WHERE question_id IN 
  (SELECT question_id FROM Questions WHERE question_code = '[UNITED STATES OUTLYING AREAS]' AND clause_version_id = 14); -- [UNITED STATES OUTLYING AREAS]

UPDATE Question_Conditions
SET question_condition_sequence = 1690 -- 1680
WHERE question_id IN 
  (SELECT question_id FROM Questions WHERE question_code = '[GOVERNMENT OWNED OR LEASED FACILITIES/INSTALLATIONS]' AND clause_version_id = 14); -- [GOVERNMENT OWNED OR LEASED FACILITIES/INSTALLATIONS]
/*
<h2>252.227-7006</h2>
(A) Value not found: "252.227-7006 -- LICENSE GRANT-RUNNING ROYALTY" in [PATENT COUNCIL RECOMMENDED] choices. Available values: <ul><li>"52.227-1-- AUTHORIZATION AND CONSENT"</li><li>"52.227-1 ALTERNATE I -- AUTHORIZATION AND CONSENT"</li><li>"52.227-1 ALTERNATE II -- AUTHORIZATION AND CONSENT"</li><li>"52.227-2 NOTICE AND ASSISTANCE REGARDING PATENT AND COPYRIGHT INFRINGEMENT"</li><li>"52.227-3 -- PATENT INDEMNITY"</li><li>"52.227-3 ALTERNATE I -- PATENT INDEMNITY"</li><li>"52.227-3 ALTERNATE II -- PATENT INDEMNITY"</li><li>"52.227-3 ALTERNATE III -- PATENT INDEMNITY"</li><li>"52.227-4 -- PATENT INDEMNITY -- CONSTRUCTION CONTRACTS"</li><li>"52.227-4 ALTERNATE I -- PATENT INDEMNITY -- CONSTRUCTION CONTRACTS"</li><li>"52.227-5 -- WAIVER OF INDEMNITY"</li><li>"52.227-6 -- ROYALTY INFORMATION"</li><li>"52.227-6 ALTERNATE I -- ROYALTY INFORMATION"</li><li>"52.227-7 -- PATENTS-- NOTICE OF GOVERNMENT LICENSEE"</li><li>"52.227-9 -- REFUND OF ROYALTIES"</li><li>"52.227-10 -- FILING OF PATENT APPLICATIONS -- CLASSIFIED SUBJECT MATTER"</li><li>"52.227-11 -- PATENT RIGHTS -- OWNERSHIP BY THE CONTRACTOR"</li><li>"52.227-11 ALTERNATE I-- PATENT RIGHTS -- OWNERSHIP BY THE CONTRACTOR"</li><li>"52.227-11 ALTERNATE II -- PATENT RIGHTS -- OWNERSHIP BY THE CONTRACTOR"</li><li>"52.227-11 ALTERNATE III-- PATENT RIGHTS -- OWNERSHIP BY THE CONTRACTOR"</li><li>"52.227-11 ALTERNATE IV -- PATENT RIGHTS -- OWNERSHIP BY THE CONTRACTOR"</li><li>"52.227-11 ALTERNATE V-- PATENT RIGHTS -- OWNERSHIP BY THE CONTRACTOR"</li><li>"52.227-13 -- PATENT RIGHTS -- OWNERSHIP BY THE GOVERNMENT"</li><li>"52.227-13 -- PATENT RIGHTS ALTERNATE I -- OWNERSHIP BY THE GOVERNMENT"</li><li>"52.227-13 -- PATENT RIGHTS ALTERNATE II -- OWNERSHIP BY THE GOVERNMENT"</li><li>"52.227-14 -- RIGHTS IN DATA - GENERAL"</li><li>"52.227-14 ALTERNATE I -- RIGHTS IN DATA - GENERAL"</li><li>"52.227-14 ALTERNATE II - RIGHTS IN DATA - GENERAL"</li><li>"52.227-14 ALTERNATE III-- RIGHTS IN DATA - GENERAL"</li><li>"52.227-14 ALTERNATE IV-- RIGHTS IN DATA - GENERAL"</li><li>"52.227-14 ALTERNATE V-- RIGHTS IN DATA - GENERAL"</li><li>"52.227-15 -- REPRESENTATION OF LIMITED RIGHTS DATA AND RESTRICTED COMPUTER SOFTWARE"</li><li>"52.227-16 -- ADDITIONAL DATA REQUIREMENTS"</li><li>"52.227-17 -- RIGHTS IN DATA -- SPECIAL WORKS"</li><li>"52.227-18 -- RIGHTS IN DATA-EXISTING WORKS"</li><li>"52.227-19 -- COMMERCIAL COMPUTER SOFTWARE LICENSE"</li><li>"52.227-20 -- RIGHTS IN DATA -- SBIR PROGRAM"</li><li>"52.227-21 -- TECHNICAL DATA DECLARATION, REVISION, AND WITHHOLDING OF PAYMENT -- MAJOR SYSTEMS"</li><li>"52.227-22 -- MAJOR SYSTEM - MINIMUM RIGHTS"</li><li>"52.227-23 -- RIGHTS TO PROPOSAL DATA (TECHNICAL)"</li><li>"252.227-7000 -- NON-ESTOPPEL"</li><li>"252.227-7001 -- RELEASE OF PAST INFRINGEMENT"</li><li>"252.227-7002 -- READJUSTMENT OF PAYMENTS"</li><li>"252.227-7003 -- TERMINATION"</li><li>"252.227-7004 -- LICENSE GRANT"</li><li>"252.227-7005 ALTERNATE I-- LICENSE TERM (NOTE: NO BASIC)"</li><li>"252.227-7005 ALTERNATE II -- LICENSE TERM (NOTE: NO BASIC)"</li><li>"252.227-7006 -- LICENSE GRANT---RUNNING ROYALTY"</li><li>"252.227-7007 -- LICENSE TERM--RUNNING ROYALTY"</li><li>"252.227-7008 -- COMPUTATION OF ROYALTIES"</li><li>"252.227-7009 -- REPORTING AND PAYMENT OF ROYALTIES"</li><li>"252.227-7010 -- LICENSE TO OTHER GOVERNMENT AGENCIES"</li><li>"252.227-7011 -- ASSIGNMENTS"</li><li>"252.227-7012 -- PATENT LICENSE AND RELEASE CONTRACT"</li><li>"252.227-7013 -- RIGHTS IN TECHNICAL DATA--NONCOMMERCIAL ITEMS"</li><li>"252.227-7013 ALTERNATE I -- RIGHTS IN TECHNICAL DATA--NONCOMMERCIAL ITEMS"</li><li>"252.227-7013 ALTERNATE II -- RIGHTS IN TECHNICAL DATA--NONCOMMERCIAL ITEMS"</li><li>"252.227-7014 -- RIGHTS IN NONCOMMERCIAL COMPUTER SOFTWARE AND NONCOMMERCIAL COMPUTER SOFTWARE DOCUMENTATION"</li><li>"252.227-7014 ALTERNATE I -- RIGHTS IN NONCOMMERCIAL COMPUTER SOFTWARE AND NONCOMMERCIAL COMPUTER SOFTWARE DOCUMENTATION"</li><li>"252.227-7015 -- TECHNICAL DATA--COMMERCIAL ITEMS"</li><li>"252.227-7015 ALTERNATE I -- TECHNICAL DATA--COMMERCIAL ITEMS"</li><li>"252.227-7016 -- RIGHTS IN BID OR PROPOSAL INFORMATION"</li><li>"252.227-7017 -- IDENTIFICATION AND ASSERTION OF USE, RELEASE, OR DISCLOSURE RESTRICTIONS"</li><li>"252.227-7018 -- RIGHTS IN NONCOMMERCIAL TECHNICAL DATA AND COMPUTER SOFTWARE--SMALL BUSINESS INNOVATION RESEARCH (SBIR) PROGRAM"</li><li>"252.227-7018 ALTERNATE I -- RIGHTS IN NONCOMMERCIAL TECHNICAL DATA AND COMPUTER SOFTWARE--SMALL BUSINESS INNOVATION RESEARCH (SBIR) PROGRAM"</li><li>"252.227-7019 -- VALIDATION OF ASSERTED RESTRICTIONS--COMPUTER SOFTWARE"</li><li>"252.227-7020 -- RIGHTS IN SPECIAL WORKS"</li><li>"252.227-7021 -- RIGHTS IN DATA--EXISTING WORKS"</li><li>"252.227-7022 -- GOVERNMENT RIGHTS (UNLIMITED)"</li><li>"252.227-7023 -- DRAWINGS AND OTHER DATA TO BECOME PROPERTY OF GOVERNMENT"</li><li>"252.227-7024 -- NOTICE AND APPROVAL OF RESTRICTED DESIGN"</li><li>"252.227-7025 -- LIMITATIONS ON THE USE OR DISCLOSURE OF GOVERNMENT-FURNISHED INFORMATION MARKED WITH RESTRICTIVE LEGENDS"</li><li>"252.227-7026 -- DEFERRED DELIVERY OF TECHNICAL DATA OR COMPUTER SOFTWARE"</li><li>"252.227-7027 -- DEFERRED ORDERING OF TECHNICAL DATA OR COMPUTER SOFTWARE"</li><li>"252.227-7028 -- TECHNICAL DATA OR COMPUTER SOFTWARE PREVIOUSLY DELIVERED TO THE GOVERNMENT"</li><li>"252.227-7030 -- TECHNICAL DATA--WITHHOLDING OF PAYMENT"</li><li>"252.227-7032 -- RIGHTS IN TECHNICAL DATA AND COMPUTER SOFTWARE (FOREIGN)"</li><li>"252.227-7033 -- RIGHTS IN SHOP DRAWINGS"</li><li>"252.227-7037 -- VALIDATION OF RESTRICTIVE MARKINGS ON TECHNICAL DATA"</li><li>"252.227-7038 -- PATENT RIGHTS--OWNERSHIP BY THE CONTRACTOR (LARGE BUSINESS)"</li><li>"252.227-7039 -- PATENTS--REPORTING OF SUBJECT INVENTIONS"</li><li>"NONE OF THE ABOVE"</li></ul>
<pre>(A) PATENT COUNCIL RECOMMENDED IS: 252.227-7006 -- LICENSE GRANT-RUNNING ROYALTY</pre>
*//*
<h2>52.211-7</h2>
No identifier
<pre>(ADDITIONAL DIRECTIONS: IF THE REQUIRING ACTIVITY IS: DEPARTMENT OF DEFENSE DO NOT USE THIS CLAUSE</pre>
*//*
<h2>52.212-5</h2>
(K) Clause name not found: {52.212-5 ALTERNATE III}
<pre>(K) 52.212-5 ALTERNATE III IS: INCLUDED</pre>
*//*
<h2>52.226-6</h2>
(F) No " IS:" found
<pre>(F) FOR USE IN UNITED STATES (50 STATES PLUS DISTRICT OF COLUMBIA)</pre>
*//*
<h2>52.237-6</h2>
(D) [DISMANTALLING PAYMENT ARRANGEMENT] Question Name not found
<pre>(D) DISMANTALLING PAYMENT ARRANGEMENT IS: CONTRACTOR IS TO RECEIVE TITLE TO DISMANTLED OR DEMOLISHED PROPERTY AND A NET AMOUNT OF COMENSATION IS DUE TO THE GOVERNMENT</pre><br />
(G) Clause name not found: {52.237-6 ALTERNATE I}
<pre>(G) 52.237-6 ALTERNATE I IS: INCLUDED</pre>
*//*
<h2>52.243-2</h2>
(I) Clause name not found: {52.243-2 ALTERNATE IV}
<pre>(I) 52.243-2 ALTERNATE IV IS: INCLUDED</pre>
*//*
<h2>52.247-5</h2>
(C) [TRANSPORTATION REQUIREMENTS] Question Name not found
<pre>(C) TRANSPORTATION REQUIREMENTS IS: CONTRACTOR RESPONSIBLE FOR FAMILIARIZATION WITH CONDITIONS UNDER WHICH AND WHERE THE SERVICES ARE TO BE PERFORMED</pre>
*/
-- ProduceClause.resolveIssues() Summary: Total(1234); Merged(20); Corrected(351); Error(8)
/* ===============================================
 * Clause
   =============================================== */

UPDATE Clauses
SET clause_title = 'Contracting Officer''s Representative' -- 'Contracting Officer''s Representative.'
WHERE clause_version_id = 14 AND clause_name = '252.201-7000';

UPDATE Clauses
SET clause_title = 'Requirements Relating to Compensation of Former DOD Officials' -- 'Requirements Relating to Compensation of Former DOD Officials.'
WHERE clause_version_id = 14 AND clause_name = '252.203-7000';

UPDATE Clauses
SET clause_title = 'Prohibition on Persons Convicted of Fraud or Other Defense Contract-Related Felonies' -- 'Prohibition on Persons Convicted of Fraud or Other Defense Contract-Related Felonies.'
WHERE clause_version_id = 14 AND clause_name = '252.203-7001';

UPDATE Clauses
SET clause_title = 'Requirement to Inform Employees of Whistleblower Rights' -- 'Requirement to Inform Employees of Whistleblower Rights.'
WHERE clause_version_id = 14 AND clause_name = '252.203-7002';

UPDATE Clauses
SET clause_title = 'Agency Office of the Inspector General' -- 'Agency Office of the Inspector General.'
WHERE clause_version_id = 14 AND clause_name = '252.203-7003';

UPDATE Clauses
SET clause_title = 'Display of Hotline Poster' -- 'Display of Hotline Poster.'
WHERE clause_version_id = 14 AND clause_name = '252.203-7004';

UPDATE Clauses
SET clause_title = 'Representation Relating to Compensation of Former DOD Officials' -- 'Representation Relating to Compensation of Former DOD Officials.'
WHERE clause_version_id = 14 AND clause_name = '252.203-7005';

UPDATE Clauses
SET clause_title = 'Prohibition on Contracting With Entities That Require Certain Internal Confidentiality Agreements - Representation' -- 'Prohibition on Contracting With Entities That Require Certain Internal Confidentiality Agreements-Representation.'
WHERE clause_version_id = 14 AND clause_name = '252.203-7996 (DEVIATION 2016-00003)';

UPDATE Clauses
SET clause_title = 'Prohibition on Contracting With Entities That Require Certain Internal Confidentiality Agreements' -- 'Prohibition on Contracting With Entities That Require Certain Internal Confidentiality Agreements.'
WHERE clause_version_id = 14 AND clause_name = '252.203-7997 (DEVIATION 2016-00003)';

UPDATE Clauses
SET clause_title = 'Prohibition on Contracting With Entities That Require Certain Internal Confidentiality Agreements - Representation' -- 'Prohibition on Contracting With Entities That Require Certain Internal Confidentiality Agreements - Representation.'
WHERE clause_version_id = 14 AND clause_name = '252.203-7998 (DEVIATION 2015-00010)';

UPDATE Clauses
SET clause_title = 'Prohibition on Contracting With Entities That Require Certain Internal Confidentiality Agreements' -- 'Prohibition on Contracting With Entities That Require Certain Internal Confidentiality Agreements.'
WHERE clause_version_id = 14 AND clause_name = '252.203-7999 (DEVIATION 2015-00010)';

UPDATE Clauses
SET clause_title = 'Line Item Specific: Single Funding' -- 'Line Item Specific: Single Funding.'
WHERE clause_version_id = 14 AND clause_name = '252.204-0001';

UPDATE Clauses
SET clause_title = 'Line Item Specific: Sequential Acrn Order' -- 'Line Item Specific: Sequential Acrn Order.'
WHERE clause_version_id = 14 AND clause_name = '252.204-0002';

UPDATE Clauses
SET clause_title = 'Line Item Specific: Contracting Officer Specified Acrn Order' -- 'Line Item Specific: Contracting Officer Specified Acrn Order.'
WHERE clause_version_id = 14 AND clause_name = '252.204-0003';

UPDATE Clauses
SET clause_title = 'Line Item Specific: by Fiscal Year' -- 'Line Item Specific: by Fiscal Year.'
WHERE clause_version_id = 14 AND clause_name = '252.204-0004';

UPDATE Clauses
SET clause_title = 'Line Item Specific: by Cancellation Date' -- 'Line Item Specific: by Cancellation Date.'
WHERE clause_version_id = 14 AND clause_name = '252.204-0005';

UPDATE Clauses
SET clause_title = 'Line Item Specific: Proration' -- 'Line Item Specific: Proration.'
WHERE clause_version_id = 14 AND clause_name = '252.204-0006';

UPDATE Clauses
SET clause_title = 'Contract-Wide: Sequential Acrn Order' -- 'Contract-Wide: Sequential Acrn Order.'
WHERE clause_version_id = 14 AND clause_name = '252.204-0007';

UPDATE Clauses
SET clause_title = 'Contract-Wide: Contracting Officer Specified Acrn Order' -- 'Contract-Wide: Contracting Officer Specified Acrn Order.'
WHERE clause_version_id = 14 AND clause_name = '252.204-0008';

UPDATE Clauses
SET clause_title = 'Contract-Wide: by Fiscal Year' -- 'Contract-Wide: by Fiscal Year.'
WHERE clause_version_id = 14 AND clause_name = '252.204-0009';

UPDATE Clauses
SET clause_title = 'Contract-Wide: by Cancellation Date' -- 'Contract-Wide: by Cancellation Date.'
WHERE clause_version_id = 14 AND clause_name = '252.204-0010';

UPDATE Clauses
SET clause_title = 'Contract-Wide: Proration' -- 'Contract-Wide: Proration.'
WHERE clause_version_id = 14 AND clause_name = '252.204-0011';

UPDATE Clauses
SET clause_title = 'Disclosure of Information' -- 'Disclosure of Information.'
WHERE clause_version_id = 14 AND clause_name = '252.204-7000';

UPDATE Clauses
SET clause_title = 'Payment for Subline Items Not Separately Priced' -- 'Payment for Subline Items Not Separately Priced.'
WHERE clause_version_id = 14 AND clause_name = '252.204-7002';

UPDATE Clauses
SET clause_title = 'Control of Government Personnel Work Product' -- 'Control of Government Personnel Work Product.'
WHERE clause_version_id = 14 AND clause_name = '252.204-7003';

UPDATE Clauses
SET clause_title = 'Alternate A, System for Award Management' -- 'Alter A, System for Award Management.'
WHERE clause_version_id = 14 AND clause_name = '252.204-7004';

UPDATE Clauses
SET clause_title = 'Oral Attestation of Security Responsibilities' -- 'Oral Attestation of Security Responsibilities.'
WHERE clause_version_id = 14 AND clause_name = '252.204-7005';

UPDATE Clauses
SET clause_title = 'Billing Instructions' -- 'Billing Instructions.'
WHERE clause_version_id = 14 AND clause_name = '252.204-7006';

UPDATE Clauses
SET clause_title = 'Alternate A, Annual Representations and Certifications' -- 'Alternate A, Annual Representations and Certifications.'
WHERE clause_version_id = 14 AND clause_name = '252.204-7007';

UPDATE Clauses
SET clause_title = 'Compliance With Safeguarding Covered Defense Information Controls' -- 'Compliance With Safeguarding Covered Defense Information Controls.'
WHERE clause_version_id = 14 AND clause_name = '252.204-7008';

UPDATE Clauses
SET clause_title = 'Compliance With Safeguarding Covered Defense Information Controls' -- 'Compliance With Safeguarding Covered Defense Information Controls.'
WHERE clause_version_id = 14 AND clause_name = '252.204-7008 (DEVIATION 2016-00001)';

UPDATE Clauses
SET clause_title = 'Limitations on the Use or Disclosure of Third - Party Contractor Information' -- 'Limitations on the Use or Disclosure of Third-Party Contractor Information.'
WHERE clause_version_id = 14 AND clause_name = '252.204-7009';

UPDATE Clauses
SET clause_title = 'Requirement for Contractor to Notify DOD If the ContractorS Activities Are Subject to Reporting Under the US-International Atomic Energy Agency Additional Protocol' -- 'Requirement for Contractor to Notify DOD If the ContractorS Activities Are Subject to Reporting Under the U.S.-International Atomic Energy Agency Additional Protocol.'
WHERE clause_version_id = 14 AND clause_name = '252.204-7010';

UPDATE Clauses
SET clause_title = 'Alternative Line Item Structure' -- 'Alternative Line Item Structure.'
WHERE clause_version_id = 14 AND clause_name = '252.204-7011';

UPDATE Clauses
SET clause_title = 'Safeguarding Covered Defense Information and Cyber Incident Reporting' -- 'Safeguarding Covered Defense Information and Cyber Incident Reporting.'
WHERE clause_version_id = 14 AND clause_name = '252.204-7012';

UPDATE Clauses
SET clause_title = 'Safeguarding Covered Defense Information and Cyber Incident Reporting' -- 'Safeguarding Covered Defense Information and Cyber Incident Reporting.'
WHERE clause_version_id = 14 AND clause_name = '252.204-7012 (DEVIATION 2016-00001)';

UPDATE Clauses
SET clause_title = 'Limitations on the Use or Disclosure of Information by Litigation Support Solicitation Offerors' -- 'Limitations on the Use or Disclosure of Information by Litigation Support Solicitation Offerors.'
WHERE clause_version_id = 14 AND clause_name = '252.204-7013';

UPDATE Clauses
SET clause_title = 'Limitations on the Use or Disclosure of Information by Litigation Support Contractors' -- 'Limitations on the Use or Disclosure of Information by Litigation Support Contractors.'
WHERE clause_version_id = 14 AND clause_name = '252.204-7014';

UPDATE Clauses
SET clause_title = 'Disclosure of Information to Litigation Support Contractors' -- 'Disclosure of Information to Litigation Support Contractors.'
WHERE clause_version_id = 14 AND clause_name = '252.204-7015';

UPDATE Clauses
SET clause_title = 'Provision of Information to Cooperative Agreement Holders' -- 'Provision of Information to Cooperative Agreement Holders.'
WHERE clause_version_id = 14 AND clause_name = '252.205-7000';

UPDATE Clauses
SET clause_title = 'Domestic Source Restriction' -- 'Domestic Source Restriction.'
WHERE clause_version_id = 14 AND clause_name = '252.206-7000';

UPDATE Clauses
SET clause_title = 'Intent to Furnish Precious Metals As Government - Furnished Material' -- 'Intent to Furnish Precious Metals As Government-Furnished Material.'
WHERE clause_version_id = 14 AND clause_name = '252.208-7000';

UPDATE Clauses
SET clause_title = 'Disclosure of Ownership or Control by a Foreign Government' -- 'Disclosure of Ownership or Control by a Foreign Government.'
WHERE clause_version_id = 14 AND clause_name = '252.209-7002';

UPDATE Clauses
SET clause_title = 'Reserve Officer Training Corps and Military Recruiting on Campus - Representation' -- 'Reserve Officer Training Corps and Military Recruiting on Campus-Representation.'
WHERE clause_version_id = 14 AND clause_name = '252.209-7003';

UPDATE Clauses
SET clause_title = 'Subcontracting With Firms That Are Owned or Controlled by the Government of a Country That Is a State Sponsor of Terrorism' -- 'Subcontracting With Firms That Are Owned or Controlled by the Government of a Country That Is a State Sponsor of Terrorism.'
WHERE clause_version_id = 14 AND clause_name = '252.209-7004';

UPDATE Clauses
SET clause_title = 'Reserve Officer Training Corps and Military Recruiting on Campus' -- 'Reserve Officer Training Corps and Military Recruiting on Campus.'
WHERE clause_version_id = 14 AND clause_name = '252.209-7005';

UPDATE Clauses
SET clause_title = 'Limitations on Offerors Acting As Lead System Integrators' -- 'Limitations on Offerors Acting As Lead System Integrators.'
WHERE clause_version_id = 14 AND clause_name = '252.209-7006';

UPDATE Clauses
SET clause_title = 'Prohibited Financial Interests for Lead System Integrators' -- 'Prohibited Financial Interests for Lead System Integrators.'
WHERE clause_version_id = 14 AND clause_name = '252.209-7007';

UPDATE Clauses
SET clause_title = 'Notice of Prohibition Relating to Organizational Conflict of Interest - Major Defense Acquisition Program' -- 'Notice of Prohibition Relating to Organizational Conflict of Interest-Major Defense Acquisition Program.'
WHERE clause_version_id = 14 AND clause_name = '252.209-7008';

UPDATE Clauses
SET clause_title = 'Organizational Conflict of Interest - Major Defense Acquisition Program' -- 'Organizational Conflict of Interest-Major Defense Acquisition Program.'
WHERE clause_version_id = 14 AND clause_name = '252.209-7009';

UPDATE Clauses
SET clause_title = 'Organizational Conflict of Interest - Major Defense Acquisition Program' -- 'Organizational Conflict of Interest-Major Defense Acquisition Program.'
WHERE clause_version_id = 14 AND clause_name = '252.209-7010';

UPDATE Clauses
SET clause_title = 'Representation by Corporations Regarding An Unpaid Delinquent Tax Liability or a Felony Conviction Under Any Federal Law - Fiscal Year 2016 Appropriations' -- 'Representation by Corporations Regarding An Unpaid Delinquent Tax Liability or a Felony Conviction Under Any Federal Law-Fiscal Year 2016 Appropriations.'
WHERE clause_version_id = 14 AND clause_name = '252.209-7991 (DEVIATION 2016-00002)';

UPDATE Clauses
SET clause_title = 'Representation by Corporations Regarding An Unpaid Delinquent Tax Liability or a Felony Conviction Under Any Federal Law - Fiscal Year 2015 Appropriations' -- 'Representation by Corporations Regarding An Unpaid Delinquent Tax Liability or a Felony Conviction Under Any Federal Law-Fiscal Year 2015 Appropriations.'
WHERE clause_version_id = 14 AND clause_name = '252.209-7992 (DEVIATION 2015-00005)';

UPDATE Clauses
SET clause_title = 'Representation by Corporations Regarding An Unpaid Delinquent Tax Liability or a Felony Conviction Under Any Federal Law - Fiscal Year 2014 Appropriations' -- 'Representation by Corporations Regarding An Unpaid Delinquent Tax Liability or a Felony Conviction Under Any Federal Law-Fiscal Year 2014 Appropriations.'
WHERE clause_version_id = 14 AND clause_name = '252.209-7993 (DEVIATION 2014-00009)';

UPDATE Clauses
SET clause_title = 'Representation by Corporations Regarding An Unpaid Delinquent Tax Liability or a Felony Conviction Under Any Federal Law - Fiscal Year 2014 Appropriations' -- 'Representation by Corporations Regarding An Unpaid Delinquent Tax Liability or a Felony Conviction Under Any Federal Law-Fiscal Year 2014 Appropriations.'
WHERE clause_version_id = 14 AND clause_name = '252.209-7994 (DEVIATION 2014-00004)';

UPDATE Clauses
SET clause_title = 'Representation by Corporations Regarding An Unpaid Delinquent Tax Liability or a Felony Conviction Under Any Federal Law - DOD Military Construction Appropriations' -- 'Representation by Corporations Regarding An Unpaid Delinquent Tax Liability or a Felony Conviction Under Any Federal Law-DOD Military Construction Appropriations.'
WHERE clause_version_id = 14 AND clause_name = '252.209-7996 (DEVIATION 2013-00006)';

UPDATE Clauses
SET clause_title = 'Representation by Corporations Regarding An Unpaid Delinquent Tax Liability or a Felony Conviction Under Any Federal Law - DOD Appropriations' -- 'Representation by Corporations Regarding An Unpaid Delinquent Tax Liability or a Felony Conviction Under Any Federal Law-DOD Appropriations.'
WHERE clause_version_id = 14 AND clause_name = '252.209-7997 (DEVIATION 2013-00006)';

UPDATE Clauses
SET clause_title = 'Representation Regarding Conviction of a Felony Criminal' -- 'Representation Regarding Conviction of a Felony Criminal.'
WHERE clause_version_id = 14 AND clause_name = '252.209-7998 (DEVIATION 2012-00007)';

UPDATE Clauses
SET clause_title = 'Representation by Corporations Regarding An Unpaid Delinquent Tax Liability or a Felony Conviction Under Any Federal Law' -- 'Representation by Corporations Regarding An Unpaid Delinquent Tax Liability or a Felony Conviction Under Any Federal Law.'
WHERE clause_version_id = 14 AND clause_name = '252.209-7999 (DEVIATION 2012-00004)';

UPDATE Clauses
SET clause_title = 'Acquisition Streamlining' -- 'Acquisition Streamlining.'
WHERE clause_version_id = 14 AND clause_name = '252.211-7000';

UPDATE Clauses
SET clause_title = 'Availability of Specifications, Standards, and Data Item Descriptions Not Listed in the Acquisition Streamlining and Standardization Information System (ASSIST), and Plans, Drawings, and Other Pertinent Documents' -- 'Availability of Specifications, Standards, and Data Item Descriptions Not Listed in the Acquisition Streamlining and Standardization Information System (ASSIST), and Plans, Drawings, and Other Pertinent Documents.'
WHERE clause_version_id = 14 AND clause_name = '252.211-7001';

UPDATE Clauses
SET clause_title = 'Availability for Examination of Specifications, Standards, Plans, Drawings, Data Item Descriptions, and Other Pertinent Documents' -- 'Availability for Examination of Specifications, Standards, Plans, Drawings, Data Item Descriptions, and Other Pertinent Documents.'
WHERE clause_version_id = 14 AND clause_name = '252.211-7002';

UPDATE Clauses
SET clause_title = 'Item Unique Identification and Valuation' -- 'Item Unique Identification and Valuation.'
WHERE clause_version_id = 14 AND clause_name = '252.211-7003';

UPDATE Clauses
SET clause_title = 'Alter Preservation, Packaging, and Packing' -- 'Alter Preservation, Packaging, and Packing.'
WHERE clause_version_id = 14 AND clause_name = '252.211-7004';

UPDATE Clauses
SET clause_title = 'Substitutions for Military or Federal Specifications and Standards' -- 'Substitutions for Military or Federal Specifications and Standards.'
WHERE clause_version_id = 14 AND clause_name = '252.211-7005';

UPDATE Clauses
SET clause_title = 'Passive Radio Frequency Identification' -- 'Passive Radio Frequency Identification.'
WHERE clause_version_id = 14 AND clause_name = '252.211-7006';

UPDATE Clauses
SET clause_title = 'Reporting of Government - Furnished Property' -- 'Reporting of Government-Furnished Property.'
WHERE clause_version_id = 14 AND clause_name = '252.211-7007';

UPDATE Clauses
SET clause_title = 'Use of Government - Assigned Serial Numbers' -- 'Use of Government-Assigned Serial Numbers.'
WHERE clause_version_id = 14 AND clause_name = '252.211-7008';

UPDATE Clauses
SET clause_title = 'Pilot Program for Acquisition of Military-Purpose Nondevelopmental Items' -- 'Pilot Program for Acquisition of Military-Purpose Nondevelopmental Items.'
WHERE clause_version_id = 14 AND clause_name = '252.212-7002';

UPDATE Clauses
SET clause_title = 'Notice to Prospective Suppliers on Use of Past Performance Information Retrieval System - Statistical Reporting in Past Performance Evaluations' -- 'Notice to Prospective Suppliers on Use of Past Performance Information Retrieval System-Statistical Reporting in Past Performance Evaluations.'
WHERE clause_version_id = 14 AND clause_name = '252.213-7000';

UPDATE Clauses
SET clause_title = 'Pricing Adjustments' -- 'Pricing Adjustments.'
WHERE clause_version_id = 14 AND clause_name = '252.215-7000';

UPDATE Clauses
SET clause_title = 'Cost Estimating System Requirements' -- 'Cost Estimating System Requirements.'
WHERE clause_version_id = 14 AND clause_name = '252.215-7002';

UPDATE Clauses
SET clause_title = 'Requirement for Submission of Data Other Than Certified Cost or Pricing Data - Canadian Commercial Corporation' -- 'Requirement for Submission of Data Other Than Certified Cost or Pricing Data-Canadian Commercial Corporation.'
WHERE clause_version_id = 14 AND clause_name = '252.215-7003';

UPDATE Clauses
SET clause_title = 'Requirement for Submission of Data Other Than Certified Cost or Pricing Data-Modifications - Canadian Commercial Corporation' -- 'Requirement for Submission of Data Other Than Certified Cost or Pricing Data-Modifications-Canadian Commercial Corporation.'
WHERE clause_version_id = 14 AND clause_name = '252.215-7004';

UPDATE Clauses
SET clause_title = 'Evaluation Factor for Employing or Subcontracting With Members of the Selected Reserve' -- 'Evaluation Factor for Employing or Subcontracting With Members of the Selected Reserve.'
WHERE clause_version_id = 14 AND clause_name = '252.215-7005';

UPDATE Clauses
SET clause_title = 'Use of Employees or Individual Subcontractors Who Are Members of the Selected Reserve' -- 'Use of Employees or Individual Subcontractors Who Are Members of the Selected Reserve.'
WHERE clause_version_id = 14 AND clause_name = '252.215-7006';

UPDATE Clauses
SET clause_title = 'Notice of Intent to Resolicit' -- 'Notice of Intent to Resolicit.'
WHERE clause_version_id = 14 AND clause_name = '252.215-7007';

UPDATE Clauses
SET clause_title = 'Only One Offer' -- 'Only One Offer.'
WHERE clause_version_id = 14 AND clause_name = '252.215-7008';

UPDATE Clauses
SET clause_title = 'Proposal Adequacy Checklist' -- 'Proposal Adequacy Checklist.'
WHERE clause_version_id = 14 AND clause_name = '252.215-7009';

UPDATE Clauses
SET clause_title = 'Economic Price Adjustment - Basic Steel, Aluminum, Brass, Bronze, or Copper Mill Products' -- 'Economic Price Adjustment-Basic Steel, Aluminum, Brass, Bronze, or Copper Mill Products.'
WHERE clause_version_id = 14 AND clause_name = '252.216-7000';

UPDATE Clauses
SET clause_title = 'Economic Price AdjustmentNonstandard Steel Items' -- 'Economic Price AdjustmentNonstandard Steel Items.'
WHERE clause_version_id = 14 AND clause_name = '252.216-7001';

UPDATE Clauses
SET clause_title = 'Alter A, Time-And-Materials/Labor-Hour Proposal Requirements Non-Commercial Item Acquisition With Adequate Price Competition' -- 'Alter A, Time-And-Materials/Labor-Hour Proposal Requirements Non-Commercial Item Acquisition With Adequate Price Competition.'
WHERE clause_version_id = 14 AND clause_name = '252.216-7002';

UPDATE Clauses
SET clause_title = 'Economic Price Adjustment - Wage Rates or Material Prices Controlled by a Foreign Government' -- 'Economic Price Adjustment-Wage Rates or Material Prices Controlled by a Foreign Government.'
WHERE clause_version_id = 14 AND clause_name = '252.216-7003';

UPDATE Clauses
SET clause_title = 'Award Fee Reduction or Denial for Jeopardizing the Health or Safety of Government Personnel' -- 'Award Fee Reduction or Denial for Jeopardizing the Health or Safety of Government Personnel.'
WHERE clause_version_id = 14 AND clause_name = '252.216-7004';

UPDATE Clauses
SET clause_title = 'Award Fee' -- 'Award Fee.'
WHERE clause_version_id = 14 AND clause_name = '252.216-7005';

UPDATE Clauses
SET clause_title = 'Ordering' -- 'Ordering.'
WHERE clause_version_id = 14 AND clause_name = '252.216-7006';

UPDATE Clauses
SET clause_title = 'Economic Price Adjustment - Basic Steel, Aluminum, Brass, Bronze, or Copper Mill Products - Representation' -- 'Economic Price Adjustment-Basic Steel, Aluminum, Brass, Bronze, or Copper Mill Products-Representation.'
WHERE clause_version_id = 14 AND clause_name = '252.216-7007';

UPDATE Clauses
SET clause_title = 'Economic Price Adjustment - Wage Rates or Material Prices Controlled by a Foreign Government - Representation' -- 'Economic Price Adjustment-Wage Rates or Material Prices Controlled by a Foreign Government-Representation.'
WHERE clause_version_id = 14 AND clause_name = '252.216-7008';

UPDATE Clauses
SET clause_title = 'Allowability of Legal Costs Incurred in Connection With a Whistleblower Proceeding' -- 'Allowability of Legal Costs Incurred in Connection With a Whistleblower Proceeding.'
WHERE clause_version_id = 14 AND clause_name = '252.216-7009';

UPDATE Clauses
SET clause_title = 'Requirements - (Basic) (Apr 2014)' -- 'Requirements.'
WHERE clause_version_id = 14 AND clause_name = '252.216-7010';

UPDATE Clauses
SET clause_title = 'Exercise of Option to Fulfill Foreign Military Sales Commitments - (Basic) (Nov 2014)' -- 'Exercise of Option to Fulfill Foreign Military Sales Commitments.'
WHERE clause_version_id = 14 AND clause_name = '252.217-7000';

UPDATE Clauses
SET clause_title = 'Surge Option' -- 'Surge Option.'
WHERE clause_version_id = 14 AND clause_name = '252.217-7001';

UPDATE Clauses
SET clause_title = 'Offering Property for Exchange' -- 'Offering Property for Exchange.'
WHERE clause_version_id = 14 AND clause_name = '252.217-7002';

UPDATE Clauses
SET clause_title = 'Changes' -- 'Changes.'
WHERE clause_version_id = 14 AND clause_name = '252.217-7003';

UPDATE Clauses
SET clause_title = 'Job Orders and Compensation' -- 'Job Orders and Compensation.'
WHERE clause_version_id = 14 AND clause_name = '252.217-7004';

UPDATE Clauses
SET clause_title = 'Inspection and Manner of Doing Work' -- 'Inspection and Manner of Doing Work.'
WHERE clause_version_id = 14 AND clause_name = '252.217-7005';

UPDATE Clauses
SET clause_title = 'Title' -- 'Title.'
WHERE clause_version_id = 14 AND clause_name = '252.217-7006';

UPDATE Clauses
SET clause_title = 'Payments' -- 'Payments.'
WHERE clause_version_id = 14 AND clause_name = '252.217-7007';

UPDATE Clauses
SET clause_title = 'Bonds' -- 'Bonds.'
WHERE clause_version_id = 14 AND clause_name = '252.217-7008';

UPDATE Clauses
SET clause_title = 'Default' -- 'Default.'
WHERE clause_version_id = 14 AND clause_name = '252.217-7009';

UPDATE Clauses
SET clause_title = 'Performance' -- 'Performance.'
WHERE clause_version_id = 14 AND clause_name = '252.217-7010';

UPDATE Clauses
SET clause_title = 'Access to Vessel' -- 'Access to Vessel.'
WHERE clause_version_id = 14 AND clause_name = '252.217-7011';

UPDATE Clauses
SET clause_title = 'Liability and Insurance' -- 'Liability and Insurance.'
WHERE clause_version_id = 14 AND clause_name = '252.217-7012';

UPDATE Clauses
SET clause_title = 'Guarantees' -- 'Guarantees.'
WHERE clause_version_id = 14 AND clause_name = '252.217-7013';

UPDATE Clauses
SET clause_title = 'Discharge of Liens' -- 'Discharge of Liens.'
WHERE clause_version_id = 14 AND clause_name = '252.217-7014';

UPDATE Clauses
SET clause_title = 'Safety and Health' -- 'Safety and Health.'
WHERE clause_version_id = 14 AND clause_name = '252.217-7015';

UPDATE Clauses
SET clause_title = 'Plant Protection' -- 'Plant Protection.'
WHERE clause_version_id = 14 AND clause_name = '252.217-7016';

UPDATE Clauses
SET clause_title = 'Identification of Sources of Supply' -- 'Identification of Sources of Supply.'
WHERE clause_version_id = 14 AND clause_name = '252.217-7026';

UPDATE Clauses
SET clause_title = 'Contract Definitization' -- 'Contract Definitization.'
WHERE clause_version_id = 14 AND clause_name = '252.217-7027';

UPDATE Clauses
SET clause_title = 'Over and Above Work' -- 'Over and Above Work.'
WHERE clause_version_id = 14 AND clause_name = '252.217-7028';

UPDATE Clauses
SET clause_title = 'Advancing Small Business Growth' -- 'Advancing Small Business Growth.'
WHERE clause_version_id = 14 AND clause_name = '252.219-7000';

UPDATE Clauses
SET clause_title = 'Small Business Subcontracting Plan (DOD Contracts) - (Basic) (Oct 2014)' -- 'Small Business Subcontracting Plan (DOD Contracts).'
WHERE clause_version_id = 14 AND clause_name = '252.219-7003';

UPDATE Clauses
SET clause_title = 'Small Business Subcontracting Plan (DOD Contracts) - (Deviation 2013-O0014) (Aug 2013)' -- 'Small Business Subcontracting Plan (DOD Contracts).'
WHERE clause_version_id = 14 AND clause_name = '252.219-7003 (DEVIATION 2013-00014)';

UPDATE Clauses
SET clause_title = 'Small Business Subcontracting Plan (Test Program)' -- 'Small Business Subcontracting Plan (Test Program).'
WHERE clause_version_id = 14 AND clause_name = '252.219-7004';

UPDATE Clauses
SET clause_title = 'Section 8(a) Direct Award' -- 'Section 8(a) Direct Award.'
WHERE clause_version_id = 14 AND clause_name = '252.219-7009';

UPDATE Clauses
SET clause_title = 'Alternate A' -- 'Alternate A.'
WHERE clause_version_id = 14 AND clause_name = '252.219-7010';

UPDATE Clauses
SET clause_title = 'Notification to Delay Performance' -- 'Notification to Delay Performance.'
WHERE clause_version_id = 14 AND clause_name = '252.219-7011';

UPDATE Clauses
SET clause_title = 'Restrictions on Employment of Personnel' -- 'Restrictions on Employment of Personnel.'
WHERE clause_version_id = 14 AND clause_name = '252.222-7000';

UPDATE Clauses
SET clause_title = 'Right of First Refusal of Employment - Closure of Military Installations' -- 'Right of First Refusal of Employment--Closure of Military Installations.'
WHERE clause_version_id = 14 AND clause_name = '252.222-7001';

UPDATE Clauses
SET clause_title = 'Compliance With Local Labor Laws (Overseas)' -- 'Compliance With Local Labor Laws (Overseas).'
WHERE clause_version_id = 14 AND clause_name = '252.222-7002';

UPDATE Clauses
SET clause_title = 'Permit From Italian Inspectorate of Labor' -- 'Permit From Italian Inspectorate of Labor.'
WHERE clause_version_id = 14 AND clause_name = '252.222-7003';

UPDATE Clauses
SET clause_title = 'Compliance With Spanish Social Security Laws and Regulations' -- 'Compliance With Spanish Social Security Laws and Regulations.'
WHERE clause_version_id = 14 AND clause_name = '252.222-7004';

UPDATE Clauses
SET clause_title = 'Prohibition on Use of Nonimmigrant Aliens - Guam' -- 'Prohibition on Use of Nonimmigrant Aliens--Guam.'
WHERE clause_version_id = 14 AND clause_name = '252.222-7005';

UPDATE Clauses
SET clause_title = 'Restrictions on the Use of Mandatory Arbitration Agreements' -- 'Restrictions on the Use of Mandatory Arbitration Agreements.'
WHERE clause_version_id = 14 AND clause_name = '252.222-7006';

UPDATE Clauses
SET clause_title = 'Representation Regarding Combating Trafficking in Persons' -- 'Representation Regarding Combating Trafficking in Persons.'
WHERE clause_version_id = 14 AND clause_name = '252.222-7007';

UPDATE Clauses
SET clause_title = 'Hazard Warning Labels' -- 'Hazard Warning Labels.'
WHERE clause_version_id = 14 AND clause_name = '252.223-7001';

UPDATE Clauses
SET clause_title = 'Safety Precautions for Ammunition and Explosives' -- 'Safety Precautions for Ammunition and Explosives.'
WHERE clause_version_id = 14 AND clause_name = '252.223-7002';

UPDATE Clauses
SET clause_title = 'Change in Place of Performance - Ammunition and Explosives' -- 'Change in Place of Performance--Ammunition and Explosives.'
WHERE clause_version_id = 14 AND clause_name = '252.223-7003';

UPDATE Clauses
SET clause_title = 'Drug-Free Work Force' -- 'Drug-Free Work Force.'
WHERE clause_version_id = 14 AND clause_name = '252.223-7004';

UPDATE Clauses
SET clause_title = 'Prohibition on Storage, Treatment, and Disposal of Toxic or Hazardous Materials - (Basic) (Sep 2014)' -- 'Prohibition on Storage, Treatment, and Disposal of Toxic or Hazardous Materials.'
WHERE clause_version_id = 14 AND clause_name = '252.223-7006';

UPDATE Clauses
SET clause_title = 'Safeguarding Sensitive Conventional Arms, Ammunition, and Explosives' -- 'Safeguarding Sensitive Conventional Arms, Ammunition, and Explosives.'
WHERE clause_version_id = 14 AND clause_name = '252.223-7007';

UPDATE Clauses
SET clause_title = 'Prohibition of Hexavalent Chromium' -- 'Prohibition of Hexavalent Chromium.'
WHERE clause_version_id = 14 AND clause_name = '252.223-7008';

UPDATE Clauses
SET clause_title = 'Buy American - Balance of Payments Program Certificate - (Basic) (Nov 2014)' -- 'Buy American-Balance of Payments Program Certificate-Basic.'
WHERE clause_version_id = 14 AND clause_name = '252.225-7000';

UPDATE Clauses
SET clause_title = 'Buy American and Balance of Payments Program - (Basic) (Nov 2014)' -- 'Buy American and Balance of Payments Program-Basic (Nov 2014).'
WHERE clause_version_id = 14 AND clause_name = '252.225-7001';

UPDATE Clauses
SET clause_title = 'Qualifying Country Sources As Subcontractors' -- 'Qualifying Country Sources As Subcontractors.'
WHERE clause_version_id = 14 AND clause_name = '252.225-7002';

UPDATE Clauses
SET clause_title = 'Report of Intended Performance Outside the United States and Canada - Submission With Offer' -- 'Report of Intended Performance Outside the United States and Canada-Submission With Offer.'
WHERE clause_version_id = 14 AND clause_name = '252.225-7003';

UPDATE Clauses
SET clause_title = 'Report of Intended Performance Outside the United States and Canada - Submission After Award' -- 'Report of Intended Performance Outside the United States and Canada-Submission After Award.'
WHERE clause_version_id = 14 AND clause_name = '252.225-7004';

UPDATE Clauses
SET clause_title = 'Identification of Expenditures in the United States' -- 'Identification of Expenditures in the United States.'
WHERE clause_version_id = 14 AND clause_name = '252.225-7005';

UPDATE Clauses
SET clause_title = 'Acquisition of the American Flag' -- 'Acquisition of the American Flag.'
WHERE clause_version_id = 14 AND clause_name = '252.225-7006';

UPDATE Clauses
SET clause_title = 'Prohibition on Acquisition of United States Munitions List Items From Communist Chinese Military Companies' -- 'Prohibition on Acquisition of United States Munitions List Items From Communist Chinese Military Companies.'
WHERE clause_version_id = 14 AND clause_name = '252.225-7007';

UPDATE Clauses
SET clause_title = 'Restriction on Acquisition of Specialty Metals' -- 'Restriction on Acquisition of Specialty Metals.'
WHERE clause_version_id = 14 AND clause_name = '252.225-7008';

UPDATE Clauses
SET clause_title = 'Restriction on Acquisition of Certain Articles Containing Specialty Metals' -- 'Restriction on Acquisition of Certain Articles Containing Specialty Metals.'
WHERE clause_version_id = 14 AND clause_name = '252.225-7009';

UPDATE Clauses
SET clause_title = 'Commercial Derivative Military Article - Specialty Metals Compliance Certificate' -- 'Commercial Derivative Military Article-Specialty Metals Compliance Certificate.'
WHERE clause_version_id = 14 AND clause_name = '252.225-7010';

UPDATE Clauses
SET clause_title = 'Restriction on Acquisition of Supercomputers' -- 'Restriction on Acquisition of Supercomputers.'
WHERE clause_version_id = 14 AND clause_name = '252.225-7011';

UPDATE Clauses
SET clause_title = 'Preference for Certain Domestic Commodities' -- 'Preference for Certain Domestic Commodities.'
WHERE clause_version_id = 14 AND clause_name = '252.225-7012';

UPDATE Clauses
SET clause_title = 'Duty-Free Entry' -- 'Duty-Free Entry.'
WHERE clause_version_id = 14 AND clause_name = '252.225-7013';

UPDATE Clauses
SET clause_title = 'Restriction on Acquisition of Hand or Measuring Tools' -- 'Restriction on Acquisition of Hand or Measuring Tools.'
WHERE clause_version_id = 14 AND clause_name = '252.225-7015';

UPDATE Clauses
SET clause_title = 'Restriction on Acquisition of Ball and Roller Bearings' -- 'Restriction on Acquisition of Ball and Roller Bearings.'
WHERE clause_version_id = 14 AND clause_name = '252.225-7016';

UPDATE Clauses
SET clause_title = 'Photovoltaic Devices' -- 'Photovoltaic Devices.'
WHERE clause_version_id = 14 AND clause_name = '252.225-7017';
INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT clause_id, prescription_id FROM Prescriptions, Clauses
WHERE prescription_name IN ('225.7017-5(a)(1)') 
AND clause_id IN (SELECT clause_id FROM Clauses WHERE clause_name = '252.225-7017' AND clause_version_id = 14); 

UPDATE Clauses
SET clause_title = 'Photovoltaic Devices - Certificate' -- 'Photovoltaic Devices-Certificate.'
WHERE clause_version_id = 14 AND clause_name = '252.225-7018';
INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT clause_id, prescription_id FROM Prescriptions, Clauses
WHERE prescription_name IN ('225.7017-5(b)') 
AND clause_id IN (SELECT clause_id FROM Clauses WHERE clause_name = '252.225-7018' AND clause_version_id = 14); 

UPDATE Clauses
SET clause_title = 'Restriction on Acquisition of Anchor and Mooring Chain' -- 'Restriction on Acquisition of Anchor and Mooring Chain.'
WHERE clause_version_id = 14 AND clause_name = '252.225-7019';

UPDATE Clauses
SET clause_title = 'Trade Agreements Certificate - (Basic) (Nov 2014)' -- 'Trade Agreements Certificate-Basic.'
WHERE clause_version_id = 14 AND clause_name = '252.225-7020';

UPDATE Clauses
SET clause_title = 'Trade Agreements - (Basic) (Oct 2015)' -- 'Trade Agreements.'
WHERE clause_version_id = 14 AND clause_name = '252.225-7021';

UPDATE Clauses
SET clause_title = 'Preference for Products or Services From Afghanistan' -- 'Preference for Products or Services From Afghanistan.'
WHERE clause_version_id = 14 AND clause_name = '252.225-7023';

UPDATE Clauses
SET clause_title = 'Requirement for Products or Services From Afghanistan' -- 'Requirement for Products or Services From Afghanistan.'
WHERE clause_version_id = 14 AND clause_name = '252.225-7024';

UPDATE Clauses
SET clause_title = 'Restriction on Acquisition of Forgings' -- 'Restriction on Acquisition of Forgings.'
WHERE clause_version_id = 14 AND clause_name = '252.225-7025';

UPDATE Clauses
SET clause_title = 'Acquisition Restricted to Products or Services From Afghanistan' -- 'Acquisition Restricted to Products or Services From Afghanistan.'
WHERE clause_version_id = 14 AND clause_name = '252.225-7026';

UPDATE Clauses
SET clause_title = 'Restriction on Contingent Fees for Foreign Military Sales' -- 'Restriction on Contingent Fees for Foreign Military Sales.'
WHERE clause_version_id = 14 AND clause_name = '252.225-7027';

UPDATE Clauses
SET clause_title = 'Exclusionary Policies and Practices of Foreign Governments' -- 'Exclusionary Policies and Practices of Foreign Governments.'
WHERE clause_version_id = 14 AND clause_name = '252.225-7028';

UPDATE Clauses
SET clause_title = 'Acquisition of Uniform Components for Afghan Military or Afghan National Police' -- 'Acquisition of Uniform Components for Afghan Military or Afghan National Police.'
WHERE clause_version_id = 14 AND clause_name = '252.225-7029';

UPDATE Clauses
SET clause_title = 'Restriction on Acquisition of Carbon, Alloy, and Armor Steel Plate' -- 'Restriction on Acquisition of Carbon, Alloy, and Armor Steel Plate.'
WHERE clause_version_id = 14 AND clause_name = '252.225-7030';

UPDATE Clauses
SET clause_title = 'Secondary Arab Boycott of Israel' -- 'Secondary Arab Boycott of Israel.'
WHERE clause_version_id = 14 AND clause_name = '252.225-7031';

UPDATE Clauses
SET clause_title = 'Waiver of United Kingdom Levies - Evaluation of Offers' -- 'Waiver of United Kingdom Levies-Evaluation of Offers.'
WHERE clause_version_id = 14 AND clause_name = '252.225-7032';

UPDATE Clauses
SET clause_title = 'Waiver of United Kingdom Levies' -- 'Waiver of United Kingdom Levies.'
WHERE clause_version_id = 14 AND clause_name = '252.225-7033';

UPDATE Clauses
SET clause_title = 'Buy American - Free Trade Agreements - Balance of Payments Program Certificate - (Basic) (Nov 2014)' -- 'Buy American-Free Trade Agreements-Balance of Payments Program Certificate-Basic.'
WHERE clause_version_id = 14 AND clause_name = '252.225-7035';

UPDATE Clauses
SET clause_title = 'Buy American - Free Trade Agreements - Balance of Payments Program - (Basic) (Nov 2014)' -- 'Buy American-Free Trade Agreements-Balance of Payments Program.'
WHERE clause_version_id = 14 AND clause_name = '252.225-7036';

UPDATE Clauses
SET clause_title = 'Evaluation of Offers for Air Circuit Breakers' -- 'Evaluation of Offers for Air Circuit Breakers.'
WHERE clause_version_id = 14 AND clause_name = '252.225-7037';

UPDATE Clauses
SET clause_title = 'Restriction on Acquisition of Air Circuit Breakers' -- 'Restriction on Acquisition of Air Circuit Breakers.'
WHERE clause_version_id = 14 AND clause_name = '252.225-7038';

UPDATE Clauses
SET clause_title = 'Defense Contractors Performing Private Security Functions Outside the United States' -- 'Defense Contractors Performing Private Security Functions Outside the United States.'
WHERE clause_version_id = 14 AND clause_name = '252.225-7039';

UPDATE Clauses
SET clause_title = 'Contractor Personnel Supporting US Armed Forces Deployed Outside the United States' -- 'Contractor Personnel Supporting U.S. Armed Forces Deployed Outside the United States.'
WHERE clause_version_id = 14 AND clause_name = '252.225-7040';

UPDATE Clauses
SET clause_title = 'Correspondence in English' -- 'Correspondence in English.'
WHERE clause_version_id = 14 AND clause_name = '252.225-7041';

UPDATE Clauses
SET clause_title = 'Authorization to Perform' -- 'Authorization to Perform.'
WHERE clause_version_id = 14 AND clause_name = '252.225-7042';

UPDATE Clauses
SET clause_title = 'Antiterrorism/Force Protection for Defense Contractors Outside the United States' -- 'Antiterrorism/Force Protection for Defense Contractors Outside the United States.'
WHERE clause_version_id = 14 AND clause_name = '252.225-7043';

UPDATE Clauses
SET clause_title = 'Balance of Payments Program - Construction Material - (Basic) (Nov 2014)' -- 'Balance of Payments Program-Construction Material-Basic.'
WHERE clause_version_id = 14 AND clause_name = '252.225-7044';

UPDATE Clauses
SET clause_title = 'Balance of Payments Program - Construction Material Under Trade Agreements - (Basic) (Nov 2014)' -- 'Balance of Payments Program - Construction Material Under Trade Agreements-Basic.'
WHERE clause_version_id = 14 AND clause_name = '252.225-7045';

UPDATE Clauses
SET clause_title = 'Exports by Approved Community Members in Response to the Solicitation' -- 'Exports by Approved Community Members in Response to the Solicitation.'
WHERE clause_version_id = 14 AND clause_name = '252.225-7046';

UPDATE Clauses
SET clause_title = 'Exports by Approved Community Members in Performance of the Contract' -- 'Exports by Approved Community Members in Performance of the Contract.'
WHERE clause_version_id = 14 AND clause_name = '252.225-7047';

UPDATE Clauses
SET clause_title = 'Export - Controlled Items' -- 'Export-Controlled Items.'
WHERE clause_version_id = 14 AND clause_name = '252.225-7048';

UPDATE Clauses
SET clause_title = 'Prohibition on Acquisition of Commercial Satellite Services From Certain Foreign Entities - Representations' -- 'Prohibition on Acquisition of Commercial Satellite Services From Certain Foreign Entities-Representations.'
WHERE clause_version_id = 14 AND clause_name = '252.225-7049';

UPDATE Clauses
SET clause_title = 'Disclosure of Ownership or Control by the Government of a Country That Is a State Sponsor of Terrorism' -- 'Disclosure of Ownership or Control by the Government of a Country That Is a State Sponsor of Terrorism.'
WHERE clause_version_id = 14 AND clause_name = '252.225-7050';

UPDATE Clauses
SET clause_title = 'Additional Access to Contractor and Subcontractor Records (Other Than USCENTCOM) - (Deviation 2015-O0016) (Sep 2015)' -- 'Additional Access to Contractor and Subcontractor Records (Other Than USCENTCOM).'
WHERE clause_version_id = 14 AND clause_name = '252.225-7981 (DEVIATION 2015-00016)';

UPDATE Clauses
SET clause_title = 'Preference for Products or Services of Djibouti - (Deviation 2015-O0012) (Feb 2015)' -- 'Preference for Products or Services of Djibouti.'
WHERE clause_version_id = 14 AND clause_name = '252.225-7982 (DEVIATION 2015-00012)';

UPDATE Clauses
SET clause_title = 'Requirement for Products or Services of Djibouti - (Deviation 2015-O0012) (Feb 2015)' -- 'Requirement for Products or Services of Djibouti.'
WHERE clause_version_id = 14 AND clause_name = '252.225-7983 (DEVIATION 2015-00012)';

UPDATE Clauses
SET clause_title = 'Acquisition Restricted to Products or Services of Djibouti - (Deviation 2015-O0012) (Feb 2015)' -- 'Acquisition Restricted to Products or Services of Djibouti.'
WHERE clause_version_id = 14 AND clause_name = '252.225-7984 (DEVIATION 2015-00012)';

UPDATE Clauses
SET clause_title = 'Contractor Personnel Performing in Support of Operation United Assistance (OUA) in the United States Africa Command (AFRICOM) Theater of Operations' -- 'Contractor Personnel Performing in Support of Operation United Assistance (OUA) in the United States Africa Command (AFRICOM) Theater of Operations.'
WHERE clause_version_id = 14 AND clause_name = '252.225-7985 (DEVIATION 2015-00003)';

UPDATE Clauses
SET clause_title = 'Requirements for Contractor Personnel Performing in Ussouthcom Area of Responsibility - (Deviation 2014-O0016) (Oct 2014)' -- 'Requirements for Contractor Personnel Performing in Ussouthcom Area of Responsibility.'
WHERE clause_version_id = 14 AND clause_name = '252.225-7987 (DEVIATION 2014-00016)';

UPDATE Clauses
SET clause_title = 'Requirements for Contractor Personnel Performing in Djibouti - (Deviation 2014-O0005) (Jan 2014)' -- 'Requirements for Contractor Personnel Performing in Djibouti.'
WHERE clause_version_id = 14 AND clause_name = '252.225-7989 (DEVIATION 2014-00005)';

UPDATE Clauses
SET clause_title = 'Preference for Products or Services From a Central Asian State or Afghanistan - (Deviation 2014-O0014) (Apr 2014)' -- 'Preference for Products or Services From a Central Asian State or Afghanistan.'
WHERE clause_version_id = 14 AND clause_name = '252.225-7990 (DEVIATION 2014-00014)';

UPDATE Clauses
SET clause_title = 'Requirement for Products or Services From a Central Asian State or Afghanistan - (Deviation 2014-O0014) (Apr 2014)' -- 'Requirement for Products or Services From a Central Asian State or Afghanistan.'
WHERE clause_version_id = 14 AND clause_name = '252.225-7991 (DEVIATION 2014-00014)';

UPDATE Clauses
SET clause_title = 'Acquisition Restricted to Products or Services From a Central Asian State or Afghanistan - (Deviation 2014-O0014) (Apr 2014)' -- 'Acquisition Restricted to Products or Services From a Central Asian State or Afghanistan.'
WHERE clause_version_id = 14 AND clause_name = '252.225-7992 (DEVIATION 2014-00014)';

UPDATE Clauses
SET clause_title = 'Prohibition on Providing Funds to the Enemy - (Deviation 2015-O0016) (Sep 2015)' -- 'Prohibition on Providing Funds to the Enemy.'
WHERE clause_version_id = 14 AND clause_name = '252.225-7993 (DEVIATION 2015-00016)';

UPDATE Clauses
SET clause_title = 'Additional Access to Contractor and Subcontractor Records in the United States Central Command Theater of Operations' -- 'Additional Access to Contractor and Subcontractor Records in the United States Central Command Theater of Operations.'
WHERE clause_version_id = 14 AND clause_name = '252.225-7994 (DEVIATION 2015-00013)';

UPDATE Clauses
SET clause_title = 'Contractor Personnel Performing in the United States Central Command Area of Responsibility' -- 'Contractor Personnel Performing in the United States Central Command Area of Responsibility.'
WHERE clause_version_id = 14 AND clause_name = '252.225-7995 (DEVIATION 2015-00009)';

UPDATE Clauses
SET clause_title = 'Acquisition Restricted to Products or Services From a Central Asia, Pakistan, the South Caucasus, or Afghanistan - (Deviation 2014-O0014) (Apr 2014)' -- 'Acquisition Restricted to Products or Services From a Central Asia, Pakistan, the South Caucasus, or Afghanistan.'
WHERE clause_version_id = 14 AND clause_name = '252.225-7996 (DEVIATION 2014-00014)';

UPDATE Clauses
SET clause_title = 'Contractor Demobilization - (Deviation 2013-O0017) (Aug 2013)' -- 'Contractor Demobilization.'
WHERE clause_version_id = 14 AND clause_name = '252.225-7997 (DEVIATION 2013-00017)';

UPDATE Clauses
SET clause_title = 'Preference for Products or Services From Central Asia, Pakistan, the South Caucasus or Afghanistan - (Deviation 2014-O0014) (Apr 2014)' -- 'Preference for Products or Services From Central Asia, Pakistan, the South Caucasus or Afghanistan.'
WHERE clause_version_id = 14 AND clause_name = '252.225-7998 (DEVIATION 2014-00014)';

UPDATE Clauses
SET clause_title = 'Requirement for Products or Services From Central Asia, Pakistan, the South Caucasus, or Afghanistan' -- 'Requirement for Products or Services From Central Asia, Pakistan, the South Caucasus, or Afghanistan.'
WHERE clause_version_id = 14 AND clause_name = '252.225-7999 (DEVIATION 2014-00014)';

UPDATE Clauses
SET clause_title = 'Utilization of Indian Organizations, Indian-Owned Economic Enterprises, and Native Hawaiian Small Business Concerns' -- 'Utilization of Indian Organizations, Indian-Owned Economic Enterprises, and Native Hawaiian Small Business Concerns.'
WHERE clause_version_id = 14 AND clause_name = '252.226-7001';

UPDATE Clauses
SET clause_title = 'Non-Estoppel' -- 'Non-Estoppel.'
WHERE clause_version_id = 14 AND clause_name = '252.227-7000';

UPDATE Clauses
SET clause_title = 'Release of Past Infringement' -- 'Release of Past Infringement.'
WHERE clause_version_id = 14 AND clause_name = '252.227-7001';

UPDATE Clauses
SET clause_title = 'Readjustment of Payments' -- 'Readjustment of Payments.'
WHERE clause_version_id = 14 AND clause_name = '252.227-7002';

UPDATE Clauses
SET clause_title = 'Termination' -- 'Termination.'
WHERE clause_version_id = 14 AND clause_name = '252.227-7003';

UPDATE Clauses
SET clause_title = 'License Grant' -- 'License Grant.'
WHERE clause_version_id = 14 AND clause_name = '252.227-7004';

UPDATE Clauses
SET clause_title = 'License Grant - Running Royalty' -- 'License Grant---Running Royalty.'
, clause_conditions = '(A) PATENT COUNCIL RECOMMENDED IS: 252.227-7006 -- LICENSE GRANT-RUNNING ROYALTY' -- '(A) PATENT COUNCIL RECOMMENDED IS: 252.227-7006 -- LICENSE GRANT---RUNNING ROYALTY'
WHERE clause_version_id = 14 AND clause_name = '252.227-7006';

UPDATE Clauses
SET clause_title = 'License Term - Running Royalty' -- 'License Term--Running Royalty.'
WHERE clause_version_id = 14 AND clause_name = '252.227-7007';

UPDATE Clauses
SET clause_title = 'Computation of Royalties' -- 'Computation of Royalties.'
WHERE clause_version_id = 14 AND clause_name = '252.227-7008';

UPDATE Clauses
SET clause_title = 'Reporting and Payment of Royalties' -- 'Reporting and Payment of Royalties.'
WHERE clause_version_id = 14 AND clause_name = '252.227-7009';

UPDATE Clauses
SET clause_title = 'License to Other Government Agencies' -- 'License to Other Government Agencies.'
WHERE clause_version_id = 14 AND clause_name = '252.227-7010';

UPDATE Clauses
SET clause_title = 'Assignments' -- 'Assignments.'
WHERE clause_version_id = 14 AND clause_name = '252.227-7011';

UPDATE Clauses
SET clause_title = 'Patent License and Release Contract' -- 'Patent License and Release Contract.'
WHERE clause_version_id = 14 AND clause_name = '252.227-7012';

UPDATE Clauses
SET clause_title = 'Rights in Technical Data - Noncommercial Items - (Basic) (Feb 2014)' -- 'Rights in Technical Data--Noncommercial Items.'
WHERE clause_version_id = 14 AND clause_name = '252.227-7013';

UPDATE Clauses
SET clause_title = 'Rights in Noncommercial Computer Software and Noncommercial Computer Software Documentation - (Basic) (Feb 2014)' -- 'Rights in Noncommercial Computer Software and Noncommercial Computer Software Documentation.'
WHERE clause_version_id = 14 AND clause_name = '252.227-7014';

UPDATE Clauses
SET clause_title = 'Technical DataCommercial Items - (Basic) (Feb 2014)' -- 'Technical DataCommercial Items.'
WHERE clause_version_id = 14 AND clause_name = '252.227-7015';

UPDATE Clauses
SET clause_title = 'Rights in Bid or Proposal Information' -- 'Rights in Bid or Proposal Information.'
WHERE clause_version_id = 14 AND clause_name = '252.227-7016';

UPDATE Clauses
SET clause_title = 'Identification and Assertion of Use, Release, or Disclosure Restrictions' -- 'Identification and Assertion of Use, Release, or Disclosure Restrictions.'
WHERE clause_version_id = 14 AND clause_name = '252.227-7017';

UPDATE Clauses
SET clause_title = 'Rights in Noncommercial Technical Data and Computer Software - Small Business Innovation Research (SBIR) Program - (Basic) (Feb 2014)' -- 'Rights in Noncommercial Technical Data and Computer Software--Small Business Innovation Research (SBIR) Program.'
WHERE clause_version_id = 14 AND clause_name = '252.227-7018';

UPDATE Clauses
SET clause_title = 'Validation of Asserted Restrictions - Computer Software' -- 'Validation of Asserted Restrictions--Computer Software.'
WHERE clause_version_id = 14 AND clause_name = '252.227-7019';

UPDATE Clauses
SET clause_title = 'Rights in Special Works' -- 'Rights in Special Works.'
WHERE clause_version_id = 14 AND clause_name = '252.227-7020';

UPDATE Clauses
SET clause_title = 'Rights in Data - Existing Works' -- 'Rights in Data--Existing Works.'
WHERE clause_version_id = 14 AND clause_name = '252.227-7021';

UPDATE Clauses
SET clause_title = 'Government Rights (Unlimited)' -- 'Government Rights (Unlimited).'
WHERE clause_version_id = 14 AND clause_name = '252.227-7022';

UPDATE Clauses
SET clause_title = 'Drawings and Other Data to Become Property of Government' -- 'Drawings and Other Data to Become Property of Government.'
WHERE clause_version_id = 14 AND clause_name = '252.227-7023';

UPDATE Clauses
SET clause_title = 'Notice and Approval of Restricted Designs' -- 'Notice and Approval of Restricted Designs.'
WHERE clause_version_id = 14 AND clause_name = '252.227-7024';

UPDATE Clauses
SET clause_title = 'Limitations on the Use or Disclosure of Government - Furnished Information Marked With Restrictive Legends' -- 'Limitations on the Use or Disclosure of Government-Furnished Information Marked With Restrictive Legends.'
WHERE clause_version_id = 14 AND clause_name = '252.227-7025';

UPDATE Clauses
SET clause_title = 'Deferred Delivery of Technical Data or Computer Software' -- 'Deferred Delivery of Technical Data or Computer Software.'
WHERE clause_version_id = 14 AND clause_name = '252.227-7026';

UPDATE Clauses
SET clause_title = 'Deferred Ordering of Technical Data or Computer Software' -- 'Deferred Ordering of Technical Data or Computer Software.'
WHERE clause_version_id = 14 AND clause_name = '252.227-7027';

UPDATE Clauses
SET clause_title = 'Technical Data or Computer Software Previously Delivered to the Government' -- 'Technical Data or Computer Software Previously Delivered to the Government.'
WHERE clause_version_id = 14 AND clause_name = '252.227-7028';

UPDATE Clauses
SET clause_title = 'Technical Data - Withholding of Payment' -- 'Technical Data--Withholding of Payment.'
WHERE clause_version_id = 14 AND clause_name = '252.227-7030';

UPDATE Clauses
SET clause_title = 'Rights in Technical Data and Computer Software (Foreign)' -- 'Rights in Technical Data and Computer Software (Foreign).'
WHERE clause_version_id = 14 AND clause_name = '252.227-7032';

UPDATE Clauses
SET clause_title = 'Rights in Shop Drawings' -- 'Rights in Shop Drawings.'
WHERE clause_version_id = 14 AND clause_name = '252.227-7033';

UPDATE Clauses
SET clause_title = 'Validation of Restrictive Markings on Technical Data' -- 'Validation of Restrictive Markings on Technical Data.'
WHERE clause_version_id = 14 AND clause_name = '252.227-7037';

UPDATE Clauses
SET clause_title = 'Patent Rights - Ownership by the Contractor (Large Business)' -- 'Patent Rights-Ownership by the Contractor (Large Business).'
WHERE clause_version_id = 14 AND clause_name = '252.227-7038';

UPDATE Clauses
SET clause_title = 'Patents - Reporting of Subject Inventions' -- 'Patents--Reporting of Subject Inventions.'
WHERE clause_version_id = 14 AND clause_name = '252.227-7039';

UPDATE Clauses
SET clause_title = 'Reimbursement for War-Hazard Losses' -- 'Reimbursement for War-Hazard Losses.'
WHERE clause_version_id = 14 AND clause_name = '252.228-7000';

UPDATE Clauses
SET clause_title = 'Ground and Flight Risk' -- 'Ground and Flight Risk.'
WHERE clause_version_id = 14 AND clause_name = '252.228-7001';

UPDATE Clauses
SET clause_title = 'Capture and Detention' -- 'Capture and Detention.'
WHERE clause_version_id = 14 AND clause_name = '252.228-7003';

UPDATE Clauses
SET clause_title = 'Bonds or Other Security' -- 'Bonds or Other Security.'
WHERE clause_version_id = 14 AND clause_name = '252.228-7004';

UPDATE Clauses
SET clause_title = 'Accident Reporting and Investigation Involving Aircraft, Missiles, and Space Launch Vehicles' -- 'Accident Reporting and Investigation Involving Aircraft, Missiles, and Space Launch Vehicles.'
WHERE clause_version_id = 14 AND clause_name = '252.228-7005';

UPDATE Clauses
SET clause_title = 'Compliance With Spanish Laws and Insurance' -- 'Compliance With Spanish Laws and Insurance.'
WHERE clause_version_id = 14 AND clause_name = '252.228-7006';

UPDATE Clauses
SET clause_title = 'Invoices Exclusive of Taxes or Duties' -- 'Invoices Exclusive of Taxes or Duties.'
WHERE clause_version_id = 14 AND clause_name = '252.229-7000';

UPDATE Clauses
SET clause_title = 'Tax Relief - (Basic) (Sep 2014)' -- 'Tax Relief.'
WHERE clause_version_id = 14 AND clause_name = '252.229-7001';

UPDATE Clauses
SET clause_title = 'Customs Exemptions (Germany)' -- 'Customs Exemptions (Germany).'
WHERE clause_version_id = 14 AND clause_name = '252.229-7002';

UPDATE Clauses
SET clause_title = 'Tax Exemptions (Italy)' -- 'Tax Exemptions (Italy).'
WHERE clause_version_id = 14 AND clause_name = '252.229-7003';

UPDATE Clauses
SET clause_title = 'Status of Contractor As a Direct Contractor (Spain)' -- 'Status of Contractor As a Direct Contractor (Spain).'
WHERE clause_version_id = 14 AND clause_name = '252.229-7004';

UPDATE Clauses
SET clause_title = 'Tax Exemptions (Spain)' -- 'Tax Exemptions (Spain).'
WHERE clause_version_id = 14 AND clause_name = '252.229-7005';

UPDATE Clauses
SET clause_title = 'Value Added Tax Exclusion (United Kingdom)' -- 'Value Added Tax Exclusion (United Kingdom).'
WHERE clause_version_id = 14 AND clause_name = '252.229-7006';

UPDATE Clauses
SET clause_title = 'Verification of United States Receipt of Goods' -- 'Verification of United States Receipt of Goods.'
WHERE clause_version_id = 14 AND clause_name = '252.229-7007';

UPDATE Clauses
SET clause_title = 'Relief From Import Duty (United Kingdom)' -- 'Relief From Import Duty (United Kingdom).'
WHERE clause_version_id = 14 AND clause_name = '252.229-7008';

UPDATE Clauses
SET clause_title = 'Relief From Customs Duty and Value Added Tax on Fuel (Passenger Vehicles) (United Kingdom)' -- 'Relief From Customs Duty and Value Added Tax on Fuel (Passenger Vehicles) (United Kingdom).'
WHERE clause_version_id = 14 AND clause_name = '252.229-7009';

UPDATE Clauses
SET clause_title = 'Relief From Customs Duty on Fuel (United Kingdom)' -- 'Relief From Customs Duty on Fuel (United Kingdom).'
WHERE clause_version_id = 14 AND clause_name = '252.229-7010';

UPDATE Clauses
SET clause_title = 'Reporting of Foreign Taxes US Assistance Programs' -- 'Reporting of Foreign Taxes U.S. Assistance Programs.'
WHERE clause_version_id = 14 AND clause_name = '252.229-7011';

UPDATE Clauses
SET clause_title = 'Tax Exemptions (Italy) - Representation' -- 'Tax Exemptions (Italy)-Representation.'
WHERE clause_version_id = 14 AND clause_name = '252.229-7012';

UPDATE Clauses
SET clause_title = 'Tax Exemptions (Spain) - Representation' -- 'Tax Exemptions (Spain)-Representation.'
WHERE clause_version_id = 14 AND clause_name = '252.229-7013';

UPDATE Clauses
SET clause_title = 'Taxes Foreign Contracts in Afghanistan (Military Technical Agreement) - (Deviation 2013-O0016) (Jul 2013)' -- 'Taxes Foreign Contracts in Afghanistan (Military Technical Agreement).'
WHERE clause_version_id = 14 AND clause_name = '252.229-7998 (DEVIATION 2013-00016)';

UPDATE Clauses
SET clause_title = 'Taxes Foreign Contracts in Afghanistan - (Deviation 2013-O0016) (Jul 2013)' -- 'Taxes Foreign Contracts in Afghanistan.'
WHERE clause_version_id = 14 AND clause_name = '252.229-7999 (DEVIATION 2013-00016)';

UPDATE Clauses
SET clause_title = 'Supplemental Cost Principles' -- 'Supplemental Cost Principles.'
WHERE clause_version_id = 14 AND clause_name = '252.231-7000';

UPDATE Clauses
SET clause_title = 'Advance Payment Pool' -- 'Advance Payment Pool.'
WHERE clause_version_id = 14 AND clause_name = '252.232-7000';

UPDATE Clauses
SET clause_title = 'Disposition of Payments' -- 'Disposition of Payments.'
WHERE clause_version_id = 14 AND clause_name = '252.232-7001';

UPDATE Clauses
SET clause_title = 'Progress Payments for Foreign Military Sales Acquisitions' -- 'Progress Payments for Foreign Military Sales Acquisitions.'
WHERE clause_version_id = 14 AND clause_name = '252.232-7002';

UPDATE Clauses
SET clause_title = 'Electronic Submission of Payment Requests and Receiving Reports' -- 'Electronic Submission of Payment Requests and Receiving Reports.'
WHERE clause_version_id = 14 AND clause_name = '252.232-7003';

UPDATE Clauses
SET clause_title = 'DOD Progress Payment Rates' -- 'DOD Progress Payment Rates.'
WHERE clause_version_id = 14 AND clause_name = '252.232-7004';

UPDATE Clauses
SET clause_title = 'Reimbursement of Subcontractor Advance Payments - DOD Pilot Mentor-Protege Program' -- 'Reimbursement of Subcontractor Advance Payments--DOD Pilot Mentor-Protege Program.'
WHERE clause_version_id = 14 AND clause_name = '252.232-7005';

UPDATE Clauses
SET clause_title = 'Wide Area Workflow Payment Instructions' -- 'Wide Area Workflow Payment Instructions.'
WHERE clause_version_id = 14 AND clause_name = '252.232-7006';

UPDATE Clauses
SET clause_title = 'Limitation of GovernmentS Obligation' -- 'Limitation of GovernmentS Obligation.'
WHERE clause_version_id = 14 AND clause_name = '252.232-7007';

UPDATE Clauses
SET clause_title = 'Assignment of Claims (Overseas)' -- 'Assignment of Claims (Overseas).'
WHERE clause_version_id = 14 AND clause_name = '252.232-7008';

UPDATE Clauses
SET clause_title = 'Mandatory Payment by Governmentwide Commercial Purchase Card' -- 'Mandatory Payment by Governmentwide Commercial Purchase Card.'
WHERE clause_version_id = 14 AND clause_name = '252.232-7009';

UPDATE Clauses
SET clause_title = 'Levies on Contract Payments' -- 'Levies on Contract Payments.'
WHERE clause_version_id = 14 AND clause_name = '252.232-7010';

UPDATE Clauses
SET clause_title = 'Payments in Support of Emergencies and Contingency Operations' -- 'Payments in Support of Emergencies and Contingency Operations.'
WHERE clause_version_id = 14 AND clause_name = '252.232-7011';

UPDATE Clauses
SET clause_title = 'Performance - Based PaymentsWhole - Contract Basis' -- 'Performance-Based PaymentsWhole-Contract Basis.'
WHERE clause_version_id = 14 AND clause_name = '252.232-7012';

UPDATE Clauses
SET clause_title = 'Performance - Based Payments - Deliverable - Item Basis' -- 'Performance-Based Payments-Deliverable-Item Basis.'
WHERE clause_version_id = 14 AND clause_name = '252.232-7013';

UPDATE Clauses
SET clause_title = 'Notification of Payment in Local Currency (Afghanistan)' -- 'Notification of Payment in Local Currency (Afghanistan).'
WHERE clause_version_id = 14 AND clause_name = '252.232-7014';

UPDATE Clauses
SET clause_title = 'Choice of Law (Overseas)' -- 'Choice of Law (Overseas).'
WHERE clause_version_id = 14 AND clause_name = '252.233-7001';

UPDATE Clauses
SET clause_title = 'Notice of Earned Value Management System - (Deviation 2015-O0017) (Sep 2015)' -- 'Notice of Earned Value Management System.'
WHERE clause_version_id = 14 AND clause_name = '252.234-7001 (DEVIATION 2015-00017)';

UPDATE Clauses
SET clause_title = 'Earned Value Management System - (Deviation 2015-O0017) (Sep 2015)' -- 'Earned Value Management System.'
WHERE clause_version_id = 14 AND clause_name = '252.234-7002 (DEVIATION 2015-00017)';

UPDATE Clauses
SET clause_title = 'Notice of Cost and Software Data Reporting System - (Basic) (Nov 2014)' -- 'Notice of Cost and Software Data Reporting System.'
WHERE clause_version_id = 14 AND clause_name = '252.234-7003';

UPDATE Clauses
SET clause_title = 'Cost and Software Data Reporting System - (Basic) (Nov 2014)' -- 'Cost and Software Data Reporting System.'
WHERE clause_version_id = 14 AND clause_name = '252.234-7004';

UPDATE Clauses
SET clause_title = 'Indemnification Under 10 USC 2354 - Fixed Price' -- 'Indemnification Under 10 U.S.C. 2354--Fixed Price.'
WHERE clause_version_id = 14 AND clause_name = '252.235-7000';

UPDATE Clauses
SET clause_title = 'Indemnification Under 10 USC 2354 - Cost Reimbursement' -- 'Indemnification Under 10 U.S.C. 2354--Cost Reimbursement.'
WHERE clause_version_id = 14 AND clause_name = '252.235-7001';

UPDATE Clauses
SET clause_title = 'Animal Welfare' -- 'Animal Welfare.'
WHERE clause_version_id = 14 AND clause_name = '252.235-7002';

UPDATE Clauses
SET clause_title = 'Frequency Authorization - (Basic) (Mar 2014)' -- 'Frequency Authorization.'
WHERE clause_version_id = 14 AND clause_name = '252.235-7003';

UPDATE Clauses
SET clause_title = 'Protection of Human Subjects' -- 'Protection of Human Subjects.'
WHERE clause_version_id = 14 AND clause_name = '252.235-7004';

UPDATE Clauses
SET clause_title = 'Acknowledgment of Support and Disclaimer' -- 'Acknowledgment of Support and Disclaimer.'
WHERE clause_version_id = 14 AND clause_name = '252.235-7010';

UPDATE Clauses
SET clause_title = 'Final Scientific or Technical Report' -- 'Final Scientific or Technical Report.'
WHERE clause_version_id = 14 AND clause_name = '252.235-7011';

UPDATE Clauses
SET clause_title = 'Modification Proposals - Price Breakdown' -- 'Modification Proposals--Price Breakdown.'
WHERE clause_version_id = 14 AND clause_name = '252.236-7000';

UPDATE Clauses
SET clause_title = 'Contract Drawings and Specifications' -- 'Contract Drawings and Specifications.'
WHERE clause_version_id = 14 AND clause_name = '252.236-7001';

UPDATE Clauses
SET clause_title = 'Obstruction of Navigable Waterways' -- 'Obstruction of Navigable Waterways.'
WHERE clause_version_id = 14 AND clause_name = '252.236-7002';

UPDATE Clauses
SET clause_title = 'Payment for Mobilization and Preparatory Work' -- 'Payment for Mobilization and Preparatory Work.'
WHERE clause_version_id = 14 AND clause_name = '252.236-7003';

UPDATE Clauses
SET clause_title = 'Payment for Mobilization and Demobilization' -- 'Payment for Mobilization and Demobilization.'
WHERE clause_version_id = 14 AND clause_name = '252.236-7004';

UPDATE Clauses
SET clause_title = 'Airfield Safety Precautions' -- 'Airfield Safety Precautions.'
WHERE clause_version_id = 14 AND clause_name = '252.236-7005';

UPDATE Clauses
SET clause_title = 'Cost Limitation' -- 'Cost Limitation.'
WHERE clause_version_id = 14 AND clause_name = '252.236-7006';

UPDATE Clauses
SET clause_title = 'Additive or Deductive Items' -- 'Additive or Deductive Items.'
WHERE clause_version_id = 14 AND clause_name = '252.236-7007';

UPDATE Clauses
SET clause_title = 'Contract Prices - Bidding Schedules' -- 'Contract Prices--Bidding Schedules.'
WHERE clause_version_id = 14 AND clause_name = '252.236-7008';

UPDATE Clauses
SET clause_title = 'Option for Supervision and Inspection Services' -- 'Option for Supervision and Inspection Services.'
WHERE clause_version_id = 14 AND clause_name = '252.236-7009';

UPDATE Clauses
SET clause_title = 'Overseas Military Construction - Preference for United States Firms' -- 'Overseas Military Construction--Preference for United States Firms.'
WHERE clause_version_id = 14 AND clause_name = '252.236-7010';

UPDATE Clauses
SET clause_title = 'Overseas Architect-Engineer Services - Restriction to United States Firms' -- 'Overseas Architect-Engineer Services--Restriction to United States Firms.'
WHERE clause_version_id = 14 AND clause_name = '252.236-7011';

UPDATE Clauses
SET clause_title = 'Military Construction on Kwajalein Atoll - Evaluation Preference' -- 'Military Construction on Kwajalein Atoll--Evaluation Preference.'
WHERE clause_version_id = 14 AND clause_name = '252.236-7012';

UPDATE Clauses
SET clause_title = 'Requirement for Competition Opportunity for American Steel Producers, Fabricators, and Manufacturers' -- 'Requirement for Competition Opportunity for American Steel Producers, Fabricators, and Manufacturers.'
WHERE clause_version_id = 14 AND clause_name = '252.236-7013';

UPDATE Clauses
SET clause_title = 'Notice of Special Standards of Responsibility' -- 'Notice of Special Standards of Responsibility.'
WHERE clause_version_id = 14 AND clause_name = '252.237-7000';

UPDATE Clauses
SET clause_title = 'Compliance With Audit Standards' -- 'Compliance With Audit Standards.'
WHERE clause_version_id = 14 AND clause_name = '252.237-7001';

UPDATE Clauses
SET clause_title = 'Award to Single Offeror - (Basic) (Nov 2014)' -- 'Award to Single Offeror.'
WHERE clause_version_id = 14 AND clause_name = '252.237-7002';

UPDATE Clauses
SET clause_title = 'Requirements' -- 'Requirements.'
WHERE clause_version_id = 14 AND clause_name = '252.237-7003';

UPDATE Clauses
SET clause_title = 'Area of Performance' -- 'Area of Performance.'
WHERE clause_version_id = 14 AND clause_name = '252.237-7004';

UPDATE Clauses
SET clause_title = 'Performance and Delivery' -- 'Performance and Delivery.'
WHERE clause_version_id = 14 AND clause_name = '252.237-7005';

UPDATE Clauses
SET clause_title = 'Subcontracting' -- 'Subcontracting.'
WHERE clause_version_id = 14 AND clause_name = '252.237-7006';

UPDATE Clauses
SET clause_title = 'Termination for Default' -- 'Termination for Default.'
WHERE clause_version_id = 14 AND clause_name = '252.237-7007';

UPDATE Clauses
SET clause_title = 'Group Interment' -- 'Group Interment.'
WHERE clause_version_id = 14 AND clause_name = '252.237-7008';

UPDATE Clauses
SET clause_title = 'Permits' -- 'Permits.'
WHERE clause_version_id = 14 AND clause_name = '252.237-7009';

UPDATE Clauses
SET clause_title = 'Prohibition on Interrogation of Detainees by Contractor Personnel' -- 'Prohibition on Interrogation of Detainees by Contractor Personnel.'
WHERE clause_version_id = 14 AND clause_name = '252.237-7010';

UPDATE Clauses
SET clause_title = 'Preparation History' -- 'Preparation History.'
WHERE clause_version_id = 14 AND clause_name = '252.237-7011';

UPDATE Clauses
SET clause_title = 'Instruction to Offerors (Count-of-Articles)' -- 'Instruction to Offerors (Count-of-Articles).'
WHERE clause_version_id = 14 AND clause_name = '252.237-7012';

UPDATE Clauses
SET clause_title = 'Instruction to Offerors (Bulk Weight)' -- 'Instruction to Offerors (Bulk Weight).'
WHERE clause_version_id = 14 AND clause_name = '252.237-7013';

UPDATE Clauses
SET clause_title = 'Loss or Damage (Count-of-Articles)' -- 'Loss or Damage (Count-of-Articles).'
WHERE clause_version_id = 14 AND clause_name = '252.237-7014';

UPDATE Clauses
SET clause_title = 'Loss or Damage (Weight of Articles)' -- 'Loss or Damage (Weight of Articles).'
WHERE clause_version_id = 14 AND clause_name = '252.237-7015';

UPDATE Clauses
SET clause_title = 'Delivery Tickets - (Basic) (Nov 2014)' -- 'Delivery Tickets.'
WHERE clause_version_id = 14 AND clause_name = '252.237-7016';

UPDATE Clauses
SET clause_title = 'Individual Laundry' -- 'Individual Laundry.'
WHERE clause_version_id = 14 AND clause_name = '252.237-7017';

UPDATE Clauses
SET clause_title = 'Special Definitions of Government Property' -- 'Special Definitions of Government Property.'
WHERE clause_version_id = 14 AND clause_name = '252.237-7018';

UPDATE Clauses
SET clause_title = 'Training for Contractor Personnel Interacting With Detainees' -- 'Training for Contractor Personnel Interacting With Detainees.'
WHERE clause_version_id = 14 AND clause_name = '252.237-7019';

UPDATE Clauses
SET clause_title = 'Services At Installations Being Closed' -- 'Services At Installations Being Closed.'
WHERE clause_version_id = 14 AND clause_name = '252.237-7022';

UPDATE Clauses
SET clause_title = 'Continuation of Essential Contractor Services' -- 'Continuation of Essential Contractor Services.'
WHERE clause_version_id = 14 AND clause_name = '252.237-7023';

UPDATE Clauses
SET clause_title = 'Notice of Continuation of Essential Contractor Services' -- 'Notice of Continuation of Essential Contractor Services.'
WHERE clause_version_id = 14 AND clause_name = '252.237-7024';

UPDATE Clauses
SET clause_title = 'Protection Against Compromising Emanations' -- 'Protection Against Compromising Emanations.'
WHERE clause_version_id = 14 AND clause_name = '252.239-7000';

UPDATE Clauses
SET clause_title = 'Information Assurance Contractor Training and Certification' -- 'Information Assurance Contractor Training and Certification.'
WHERE clause_version_id = 14 AND clause_name = '252.239-7001';

UPDATE Clauses
SET clause_title = 'Access' -- 'Access.'
WHERE clause_version_id = 14 AND clause_name = '252.239-7002';

UPDATE Clauses
SET clause_title = 'Orders for Facilities and Services' -- 'Orders for Facilities and Services.'
WHERE clause_version_id = 14 AND clause_name = '252.239-7004';

UPDATE Clauses
SET clause_title = 'Rates, Charges, and Services' -- 'Rates, Charges, and Services.'
WHERE clause_version_id = 14 AND clause_name = '252.239-7005';

UPDATE Clauses
SET clause_title = 'Tariff Information' -- 'Tariff Information.'
WHERE clause_version_id = 14 AND clause_name = '252.239-7006';

UPDATE Clauses
SET clause_title = 'Cancellation or Termination of Orders' -- 'Cancellation or Termination of Orders.'
WHERE clause_version_id = 14 AND clause_name = '252.239-7007';

UPDATE Clauses
SET clause_title = 'Reuse Arrangements' -- 'Reuse Arrangements.'
WHERE clause_version_id = 14 AND clause_name = '252.239-7008';

UPDATE Clauses
SET clause_title = 'Representation of Use of Cloud Computing' -- 'Representation of Use of Cloud Computing.'
WHERE clause_version_id = 14 AND clause_name = '252.239-7009';
INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT clause_id, prescription_id FROM Prescriptions, Clauses
WHERE prescription_name IN ('239.7604(a)') 
AND clause_id IN (SELECT clause_id FROM Clauses WHERE clause_name = '252.239-7009' AND clause_version_id = 14); 

UPDATE Clauses
SET clause_title = 'Cloud Computing Services' -- 'Cloud Computing Services.'
WHERE clause_version_id = 14 AND clause_name = '252.239-7010';
INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT clause_id, prescription_id FROM Prescriptions, Clauses
WHERE prescription_name IN ('239.7604(b)') 
AND clause_id IN (SELECT clause_id FROM Clauses WHERE clause_name = '252.239-7010' AND clause_version_id = 14); 

UPDATE Clauses
SET clause_title = 'Special Construction and Equipment Charges' -- 'Special Construction and Equipment Charges.'
WHERE clause_version_id = 14 AND clause_name = '252.239-7011';

UPDATE Clauses
SET clause_title = 'Title to Telecommunication Facilities and Equipment' -- 'Title to Telecommunication Facilities and Equipment.'
WHERE clause_version_id = 14 AND clause_name = '252.239-7012';

UPDATE Clauses
SET clause_title = 'Obligation of the Government' -- 'Obligation of the Government.'
WHERE clause_version_id = 14 AND clause_name = '252.239-7013';

UPDATE Clauses
SET clause_title = 'Term of Agreement' -- 'Term of Agreement.'
WHERE clause_version_id = 14 AND clause_name = '252.239-7014';

UPDATE Clauses
SET clause_title = 'Continuation of Communication Service Authorizations' -- 'Continuation of Communication Service Authorizations.'
WHERE clause_version_id = 14 AND clause_name = '252.239-7015';

UPDATE Clauses
SET clause_title = 'Telecommunications Security Equipment, Devices, Techniques, and Services' -- 'Telecommunications Security Equipment, Devices, Techniques, and Services.'
WHERE clause_version_id = 14 AND clause_name = '252.239-7016';

UPDATE Clauses
SET clause_title = 'Notice of Supply Chain Risk' -- 'Notice of Supply Chain Risk.'
WHERE clause_version_id = 14 AND clause_name = '252.239-7017';

UPDATE Clauses
SET clause_title = 'Supply Chain Risk' -- 'Supply Chain Risk.'
WHERE clause_version_id = 14 AND clause_name = '252.239-7018';

UPDATE Clauses
SET clause_title = 'Superseding Contract' -- 'Superseding Contract.'
WHERE clause_version_id = 14 AND clause_name = '252.241-7000';

UPDATE Clauses
SET clause_title = 'Government Access' -- 'Government Access.'
WHERE clause_version_id = 14 AND clause_name = '252.241-7001';

UPDATE Clauses
SET clause_title = 'Material Management and Accounting System' -- 'Material Management and Accounting System.'
WHERE clause_version_id = 14 AND clause_name = '252.242-7004';

UPDATE Clauses
SET clause_title = 'Contractor Business Systems' -- 'Contractor Business Systems.'
WHERE clause_version_id = 14 AND clause_name = '252.242-7005';

UPDATE Clauses
SET clause_title = 'Accounting System Administration' -- 'Accounting System Administration.'
WHERE clause_version_id = 14 AND clause_name = '252.242-7006';

UPDATE Clauses
SET clause_title = 'Pricing of Contract Modifications' -- 'Pricing of Contract Modifications.'
WHERE clause_version_id = 14 AND clause_name = '252.243-7001';

UPDATE Clauses
SET clause_title = 'Requests for Equitable Adjustment' -- 'Requests for Equitable Adjustment.'
WHERE clause_version_id = 14 AND clause_name = '252.243-7002';

UPDATE Clauses
SET clause_title = 'Subcontracts for Commercial Items' -- 'Subcontracts for Commercial Items.'
WHERE clause_version_id = 14 AND clause_name = '252.244-7000';

UPDATE Clauses
SET clause_title = 'Contractor Purchasing System Administration - (Basic) (May 2014)' -- 'Contractor Purchasing System Administration-Basic.'
WHERE clause_version_id = 14 AND clause_name = '252.244-7001';

UPDATE Clauses
SET clause_title = 'Government-Furnished Mapping, Charting, and Geodesy Property' -- 'Government-Furnished Mapping, Charting, and Geodesy Property.'
WHERE clause_version_id = 14 AND clause_name = '252.245-7000';

UPDATE Clauses
SET clause_title = 'Tagging, Labeling, and Marking of Government - Furnished Property' -- 'Tagging, Labeling, and Marking of Government-Furnished Property.'
WHERE clause_version_id = 14 AND clause_name = '252.245-7001';

UPDATE Clauses
SET clause_title = 'Reporting Loss of Government Property' -- 'Reporting Loss of Government Property.'
WHERE clause_version_id = 14 AND clause_name = '252.245-7002';

UPDATE Clauses
SET clause_title = 'Contractor Property Management System Administration' -- 'Contractor Property Management System Administration.'
WHERE clause_version_id = 14 AND clause_name = '252.245-7003';

UPDATE Clauses
SET clause_title = 'Reporting, Reutilization, and Disposal' -- 'Reporting, Reutilization, and Disposal.'
WHERE clause_version_id = 14 AND clause_name = '252.245-7004';

UPDATE Clauses
SET clause_title = 'Material Inspection and Receiving Report' -- 'Material Inspection and Receiving Report.'
WHERE clause_version_id = 14 AND clause_name = '252.246-7000';

UPDATE Clauses
SET clause_title = 'Warranty of Data - (Basic) (Mar 2014)' -- 'Warranty of Data-Basic.'
WHERE clause_version_id = 14 AND clause_name = '252.246-7001';

UPDATE Clauses
SET clause_title = 'Warranty of Construction (Germany)' -- 'Warranty of Construction (Germany).'
WHERE clause_version_id = 14 AND clause_name = '252.246-7002';

UPDATE Clauses
SET clause_title = 'Notification of Potential Safety Issues' -- 'Notification of Potential Safety Issues.'
WHERE clause_version_id = 14 AND clause_name = '252.246-7003';

UPDATE Clauses
SET clause_title = 'Safety of Facilities, Infrastructure, and Equipment for Military Operations' -- 'Safety of Facilities, Infrastructure, and Equipment for Military Operations.'
WHERE clause_version_id = 14 AND clause_name = '252.246-7004';

UPDATE Clauses
SET clause_title = 'Notice of Warranty Tracking of Serialized Items' -- 'Notice of Warranty Tracking of Serialized Items.'
WHERE clause_version_id = 14 AND clause_name = '252.246-7005';

UPDATE Clauses
SET clause_title = 'Warranty Tracking of Serialized Items' -- 'Warranty Tracking of Serialized Items.'
WHERE clause_version_id = 14 AND clause_name = '252.246-7006';

UPDATE Clauses
SET clause_title = 'Contractor Counterfeit Electronic Part Detection and Avoidance System' -- 'Contractor Counterfeit Electronic Part Detection and Avoidance System.'
WHERE clause_version_id = 14 AND clause_name = '252.246-7007';

UPDATE Clauses
SET clause_title = 'Hardship Conditions' -- 'Hardship Conditions.'
WHERE clause_version_id = 14 AND clause_name = '252.247-7000';

UPDATE Clauses
SET clause_title = 'Price Adjustment' -- 'Price Adjustment.'
WHERE clause_version_id = 14 AND clause_name = '252.247-7001';

UPDATE Clauses
SET clause_title = 'Revision of Prices' -- 'Revision of Prices.'
WHERE clause_version_id = 14 AND clause_name = '252.247-7002';

UPDATE Clauses
SET clause_title = 'Pass-Through of Motor Carrier Fuel Surcharge Adjustment to the Cost Bearer' -- 'Pass-Through of Motor Carrier Fuel Surcharge Adjustment to the Cost Bearer.'
WHERE clause_version_id = 14 AND clause_name = '252.247-7003';

UPDATE Clauses
SET clause_title = 'Indefinite Quantities - Fixed Charges' -- 'Indefinite Quantities--Fixed Charges.'
WHERE clause_version_id = 14 AND clause_name = '252.247-7004';

UPDATE Clauses
SET clause_title = 'Indefinite Quantities - No Fixed Charges' -- 'Indefinite Quantities--No Fixed Charges.'
WHERE clause_version_id = 14 AND clause_name = '252.247-7005';

UPDATE Clauses
SET clause_title = 'Removal of Contractor''s Employees' -- 'Removal of Contractor''s Employees.'
WHERE clause_version_id = 14 AND clause_name = '252.247-7006';

UPDATE Clauses
SET clause_title = 'Liability and Insurance' -- 'Liability and Insurance.'
WHERE clause_version_id = 14 AND clause_name = '252.247-7007';

UPDATE Clauses
SET clause_title = 'Evaluation of Bids - (Basic) (Apr 2014)' -- 'Evaluation of Bids-Basic.'
WHERE clause_version_id = 14 AND clause_name = '252.247-7008';

UPDATE Clauses
SET clause_title = 'Award' -- 'Award.'
WHERE clause_version_id = 14 AND clause_name = '252.247-7009';

UPDATE Clauses
SET clause_title = 'Scope of Contract' -- 'Scope of Contract.'
WHERE clause_version_id = 14 AND clause_name = '252.247-7010';

UPDATE Clauses
SET clause_title = 'Period of Contract' -- 'Period of Contract.'
WHERE clause_version_id = 14 AND clause_name = '252.247-7011';

UPDATE Clauses
SET clause_title = 'Ordering Limitation' -- 'Ordering Limitation.'
WHERE clause_version_id = 14 AND clause_name = '252.247-7012';

UPDATE Clauses
SET clause_title = 'Contract Areas of Performance' -- 'Contract Areas of Performance.'
WHERE clause_version_id = 14 AND clause_name = '252.247-7013';

UPDATE Clauses
SET clause_title = 'Demurrage' -- 'Demurrage.'
WHERE clause_version_id = 14 AND clause_name = '252.247-7014';

UPDATE Clauses
SET clause_title = 'Contractor Liability for Loss or Damage' -- 'Contractor Liability for Loss or Damage.'
WHERE clause_version_id = 14 AND clause_name = '252.247-7016';

UPDATE Clauses
SET clause_title = 'Erroneous Shipments' -- 'Erroneous Shipments.'
WHERE clause_version_id = 14 AND clause_name = '252.247-7017';

UPDATE Clauses
SET clause_title = 'Subcontracting' -- 'Subcontracting.'
WHERE clause_version_id = 14 AND clause_name = '252.247-7018';

UPDATE Clauses
SET clause_title = 'Drayage' -- 'Drayage.'
WHERE clause_version_id = 14 AND clause_name = '252.247-7019';

UPDATE Clauses
SET clause_title = 'Additional Services' -- 'Additional Services.'
WHERE clause_version_id = 14 AND clause_name = '252.247-7020';

UPDATE Clauses
SET clause_title = 'Returnable Containers Other Than Cylinders' -- 'Returnable Containers Other Than Cylinders.'
WHERE clause_version_id = 14 AND clause_name = '252.247-7021';

UPDATE Clauses
SET clause_title = 'Representation of Extent of Transportation by Sea' -- 'Representation of Extent of Transportation by Sea.'
WHERE clause_version_id = 14 AND clause_name = '252.247-7022';

UPDATE Clauses
SET clause_title = 'Transportation of Supplies by Sea - (Basic) (Apr 2014)' -- 'Transportation of Supplies by Sea-Basic.'
WHERE clause_version_id = 14 AND clause_name = '252.247-7023';

UPDATE Clauses
SET clause_title = 'Notification of Transportation of Supplies by Sea' -- 'Notification of Transportation of Supplies by Sea.'
WHERE clause_version_id = 14 AND clause_name = '252.247-7024';

UPDATE Clauses
SET clause_title = 'Reflagging or Repair Work' -- 'Reflagging or Repair Work.'
WHERE clause_version_id = 14 AND clause_name = '252.247-7025';

UPDATE Clauses
SET clause_title = 'Evaluation Preference for Use of Domestic Shipyards - Applicable to Acquisition of Carriage by Vessel for DOD Cargo in the Coastwise or Noncontiguous Trade' -- 'Evaluation Preference for Use of Domestic Shipyards - Applicable to Acquisition of Carriage by Vessel for DOD Cargo in the Coastwise or Noncontiguous Trade.'
WHERE clause_version_id = 14 AND clause_name = '252.247-7026';

UPDATE Clauses
SET clause_title = 'Riding Gang Member Requirements' -- 'Riding Gang Member Requirements.'
WHERE clause_version_id = 14 AND clause_name = '252.247-7027';

UPDATE Clauses
SET clause_title = 'Application for US Government Shipping Documentation/Instructions' -- 'Application for U.S. Government Shipping Documentation/Instructions.'
WHERE clause_version_id = 14 AND clause_name = '252.247-7028';

UPDATE Clauses
SET clause_title = 'Special Termination Costs' -- 'Special Termination Costs.'
WHERE clause_version_id = 14 AND clause_name = '252.249-7000';

UPDATE Clauses
SET clause_title = 'Notification of Anticipated Contract Termination or Reduction' -- 'Notification of Anticipated Contract Termination or Reduction.'
WHERE clause_version_id = 14 AND clause_name = '252.249-7002';

UPDATE Clauses
SET clause_title = 'Ordering From Government Supply Sources' -- 'Ordering From Government Supply Sources.'
WHERE clause_version_id = 14 AND clause_name = '252.251-7000';

UPDATE Clauses
SET clause_title = 'Use of Interagency Fleet Management System (IFMS) Vehicles and Related Services' -- 'Use of Interagency Fleet Management System (IFMS) Vehicles and Related Services.'
WHERE clause_version_id = 14 AND clause_name = '252.251-7001';

UPDATE Clauses
SET clause_title = 'Definitions' -- 'Definitions.'
WHERE clause_version_id = 14 AND clause_name = '52.202-1';

UPDATE Clauses
SET clause_title = 'Price or Fee Adjustment for Illegal or Improper Activity' -- 'Price or Fee Adjustment for Illegal or Improper Activity.'
WHERE clause_version_id = 14 AND clause_name = '52.203-10';

UPDATE Clauses
SET clause_title = 'Certification and Disclosure Regarding Payments to Influence Certain Federal Transactions' -- 'Certification and Disclosure Regarding Payments to Influence Certain Federal Transactions.'
WHERE clause_version_id = 14 AND clause_name = '52.203-11';

UPDATE Clauses
SET clause_title = 'Limitation on Payments to Influence Certain Federal Transactions' -- 'Limitation on Payments to Influence Certain Federal Transactions.'
WHERE clause_version_id = 14 AND clause_name = '52.203-12';

UPDATE Clauses
SET clause_title = 'Contractor Code of Business Ethics and Conduct' -- 'Contractor Code of Business Ethics and Conduct.'
WHERE clause_version_id = 14 AND clause_name = '52.203-13';

UPDATE Clauses
SET clause_title = 'Display of Hotline Poster(s)' -- 'Display of Hotline Poster(s).'
WHERE clause_version_id = 14 AND clause_name = '52.203-14';

UPDATE Clauses
SET clause_title = 'Whistleblower Protections Under The American Recovery and Reinvestment Act of 2009' -- 'Whistleblower Protections Under The American Recovery and Reinvestment Act of 2009.'
WHERE clause_version_id = 14 AND clause_name = '52.203-15';

UPDATE Clauses
SET clause_title = 'Preventing Personal Conflicts of Interest' -- 'Preventing Personal Conflicts of Interest.'
WHERE clause_version_id = 14 AND clause_name = '52.203-16';

UPDATE Clauses
SET clause_title = 'Contractor Employee Whistleblower Rights and Requirement to Inform Employees of Whistleblower Rights' -- 'Contractor Employee Whistleblower Rights and Requirement to Inform Employees of Whistleblower Rights.'
WHERE clause_version_id = 14 AND clause_name = '52.203-17';

UPDATE Clauses
SET clause_title = 'Certificate of Independent Price Determination' -- 'Certificate of Independent Price Determination.'
WHERE clause_version_id = 14 AND clause_name = '52.203-2';

UPDATE Clauses
SET clause_title = 'Gratuities' -- 'Gratuities.'
WHERE clause_version_id = 14 AND clause_name = '52.203-3';

UPDATE Clauses
SET clause_title = 'Covenant Against Contingent Fees' -- 'Covenant Against Contingent Fees.'
WHERE clause_version_id = 14 AND clause_name = '52.203-5';

UPDATE Clauses
SET clause_title = 'Restrictions on Subcontractor Sales to the Government' -- 'Restrictions on Subcontractor Sales to the Government.'
WHERE clause_version_id = 14 AND clause_name = '52.203-6';

UPDATE Clauses
SET clause_title = 'Anti-Kickback Procedures' -- 'Anti-Kickback Procedures.'
WHERE clause_version_id = 14 AND clause_name = '52.203-7';

UPDATE Clauses
SET clause_title = 'Cancellation, Recession, and Recovery of Funds for Illegal or Improper Activity' -- 'Cancellation, Recession, and Recovery of Funds for Illegal or Improper Activity.'
WHERE clause_version_id = 14 AND clause_name = '52.203-8';

UPDATE Clauses
SET clause_title = 'Approval of Contract' -- 'Approval of Contract.'
WHERE clause_version_id = 14 AND clause_name = '52.204-1';

UPDATE Clauses
SET clause_title = 'Reporting Executive Compensation and First-Tier Subcontract Awards' -- 'Reporting Executive Compensation and First-Tier Subcontract Awards.'
WHERE clause_version_id = 14 AND clause_name = '52.204-10';

UPDATE Clauses
SET clause_title = 'Data Universal Numbering System Number Maintenance' -- 'Data Universal Numbering System Number Maintenance.'
WHERE clause_version_id = 14 AND clause_name = '52.204-12';

UPDATE Clauses
SET clause_title = 'System for Award Management Maintenance' -- 'System for Award Management Maintenance.'
WHERE clause_version_id = 14 AND clause_name = '52.204-13';

UPDATE Clauses
SET clause_title = 'Service Contract Reporting Requirements' -- 'Service Contract Reporting Requirements.'
WHERE clause_version_id = 14 AND clause_name = '52.204-14';

UPDATE Clauses
SET clause_title = 'Service Contract Reporting Requirements for Indefinite - Delivery Contracts' -- 'Service Contract Reporting Requirements for Indefinite-Delivery Contracts.'
WHERE clause_version_id = 14 AND clause_name = '52.204-15';

UPDATE Clauses
SET clause_title = 'Commercial and Government Entity Code Reporting' -- 'Commercial and Government Entity Code Reporting.'
WHERE clause_version_id = 14 AND clause_name = '52.204-16';

UPDATE Clauses
SET clause_title = 'Ownership or Control of Contractor' -- 'Ownership or Control of Contractor.'
WHERE clause_version_id = 14 AND clause_name = '52.204-17';

UPDATE Clauses
SET clause_title = 'Commercial and Government Entity Code Maintenance' -- 'Commercial and Government Entity Code Maintenance.'
WHERE clause_version_id = 14 AND clause_name = '52.204-18';

UPDATE Clauses
SET clause_title = 'Incorporation by Reference of Representations And Certifications' -- 'Incorporation by Reference of Representations And Certifications.'
WHERE clause_version_id = 14 AND clause_name = '52.204-19';

UPDATE Clauses
SET clause_title = 'Security Requirements' -- 'Security Requirements.'
WHERE clause_version_id = 14 AND clause_name = '52.204-2';

UPDATE Clauses
SET clause_title = 'Taxpayer Identification' -- 'Taxpayer Identification.'
WHERE clause_version_id = 14 AND clause_name = '52.204-3';

UPDATE Clauses
SET clause_title = 'Printed or Copied Double-Sided on Postconsumer Fiber Content Paper' -- 'Printed or Copied Double-Sided on Postconsumer Fiber Content Paper.'
WHERE clause_version_id = 14 AND clause_name = '52.204-4';

UPDATE Clauses
SET clause_title = 'Women-Owned Business (Other Than Small Business)' -- 'Women-Owned Business (Other Than Small Business).'
WHERE clause_version_id = 14 AND clause_name = '52.204-5';

UPDATE Clauses
SET clause_title = 'Data Universal Numbering System Number' -- 'Data Universal Numbering System Number.'
WHERE clause_version_id = 14 AND clause_name = '52.204-6';

UPDATE Clauses
SET clause_title = 'System for Award Management' -- 'System for Award Management.'
WHERE clause_version_id = 14 AND clause_name = '52.204-7';

UPDATE Clauses
SET clause_title = 'Annual Representations and Certifications' -- 'Annual Representations and Certifications.'
WHERE clause_version_id = 14 AND clause_name = '52.204-8';

UPDATE Clauses
SET clause_title = 'Personal Identity Verification of Contractor Personnel' -- 'Personal Identity Verification of Contractor Personnel.'
WHERE clause_version_id = 14 AND clause_name = '52.204-9';

UPDATE Clauses
SET clause_title = 'Notice of Standard Competition' -- 'Notice of Standard Competition.'
WHERE clause_version_id = 14 AND clause_name = '52.207-1';

UPDATE Clauses
SET clause_title = 'Notice of Streamlined Competition' -- 'Notice of Streamlined Competition.'
WHERE clause_version_id = 14 AND clause_name = '52.207-2';

UPDATE Clauses
SET clause_title = 'Right of First Refusal of Employment' -- 'Right of First Refusal of Employment.'
WHERE clause_version_id = 14 AND clause_name = '52.207-3';

UPDATE Clauses
SET clause_title = 'Economic Purchase Quantity - Supplies' -- 'Economic Purchase Quantity-Supplies.'
WHERE clause_version_id = 14 AND clause_name = '52.207-4';

UPDATE Clauses
SET clause_title = 'Option to Purchase Equipment' -- 'Option to Purchase Equipment.'
WHERE clause_version_id = 14 AND clause_name = '52.207-5';

UPDATE Clauses
SET clause_title = 'Vehicle Lease Payments' -- 'Vehicle Lease Payments.'
WHERE clause_version_id = 14 AND clause_name = '52.208-4';

UPDATE Clauses
SET clause_title = 'Condition of Leased Vehicles' -- 'Condition of Leased Vehicles.'
WHERE clause_version_id = 14 AND clause_name = '52.208-5';

UPDATE Clauses
SET clause_title = 'Marking of Leased Vehicles' -- 'Marking of Leased Vehicles.'
WHERE clause_version_id = 14 AND clause_name = '52.208-6';

UPDATE Clauses
SET clause_title = 'Tagging of Leased Vehicles' -- 'Tagging of Leased Vehicles.'
WHERE clause_version_id = 14 AND clause_name = '52.208-7';

UPDATE Clauses
SET clause_title = 'Required Sources for Helium and Helium Usage Data' -- 'Required Sources for Helium and Helium Usage Data.'
WHERE clause_version_id = 14 AND clause_name = '52.208-8';

UPDATE Clauses
SET clause_title = 'Contractor Use of Mandatory Sources of Supply or Services' -- 'Contractor Use of Mandatory Sources of Supply or Services.'
WHERE clause_version_id = 14 AND clause_name = '52.208-9';

UPDATE Clauses
SET clause_title = 'Qualification Requirements' -- 'Qualification Requirements.'
WHERE clause_version_id = 14 AND clause_name = '52.209-1';

UPDATE Clauses
SET clause_title = 'Prohibition on Contracting With Inverted Domestic Corporations' -- 'Prohibition on Contracting With Inverted Domestic Corporations.'
WHERE clause_version_id = 14 AND clause_name = '52.209-10';

UPDATE Clauses
SET clause_title = 'First Article Approval - Contractor Testing' -- 'First Article Approval- Contractor Testing.'
WHERE clause_version_id = 14 AND clause_name = '52.209-3';

UPDATE Clauses
SET clause_title = 'First Article Approval - Government Testing' -- 'First Article Approval-Government Testing.'
WHERE clause_version_id = 14 AND clause_name = '52.209-4';

UPDATE Clauses
SET clause_title = 'Certification Regarding Responsibility Matters' -- 'Certification Regarding Responsibility Matters.'
WHERE clause_version_id = 14 AND clause_name = '52.209-5';

UPDATE Clauses
SET clause_title = 'Protecting the Governments Interest When Subcontracting With Contractor''s Debarred, Suspended, or Proposed for Debarment' -- 'Protecting the Governments Interest When Subcontracting With Contractor''s Debarred, Suspended, or Proposed for Debarment.'
WHERE clause_version_id = 14 AND clause_name = '52.209-6';

UPDATE Clauses
SET clause_title = 'Information Regarding Responsibility Matters' -- 'Information Regarding Responsibility Matters.'
WHERE clause_version_id = 14 AND clause_name = '52.209-7';

UPDATE Clauses
SET clause_title = 'Updates of Publicly Available Information Regarding Responsibility Matters' -- 'Updates of Publicly Available Information Regarding Responsibility Matters.'
WHERE clause_version_id = 14 AND clause_name = '52.209-9';

UPDATE Clauses
SET clause_title = 'Market Research' -- 'Market Research.'
WHERE clause_version_id = 14 AND clause_name = '52.210-1';

UPDATE Clauses
SET clause_title = 'Availability of Specifications Listed in the GSA Index of Federal Specifications, Standards and Commercial Item Descriptions, FPMR Part 101-29' -- 'Availability of Specifications Listed in the GSA Index of Federal Specifications, Standards and Commercial Item Descriptions, FPMR Part 101-29.'
WHERE clause_version_id = 14 AND clause_name = '52.211-1';

UPDATE Clauses
SET clause_title = 'Commencement, Prosecution, and Completion of Work' -- 'Commencement, Prosecution, and Completion of Work.'
WHERE clause_version_id = 14 AND clause_name = '52.211-10';

UPDATE Clauses
SET clause_title = 'Liquidated Damages - Supplies, Services, or Research and Development' -- 'Liquidated Damages--Supplies, Services, or Research and Development.'
WHERE clause_version_id = 14 AND clause_name = '52.211-11';

UPDATE Clauses
SET clause_title = 'Liquidated Damages - Construction' -- 'Liquidated Damages-Construction.'
WHERE clause_version_id = 14 AND clause_name = '52.211-12';

UPDATE Clauses
SET clause_title = 'Time Extensions' -- 'Time Extensions.'
WHERE clause_version_id = 14 AND clause_name = '52.211-13';

UPDATE Clauses
SET clause_title = 'Notice of Priority Rating for National Defense, Emergency Preparedness, and Energy Program Use' -- 'Notice of Priority Rating for National Defense, Emergency Preparedness, and Energy Program Use.'
WHERE clause_version_id = 14 AND clause_name = '52.211-14';

UPDATE Clauses
SET clause_title = 'Defense Priority and Allocation Requirements' -- 'Defense Priority and Allocation Requirements.'
WHERE clause_version_id = 14 AND clause_name = '52.211-15';

UPDATE Clauses
SET clause_title = 'Variation in Quantity' -- 'Variation in Quantity.'
WHERE clause_version_id = 14 AND clause_name = '52.211-16';

UPDATE Clauses
SET clause_title = 'Delivery of Excess Quantities' -- 'Delivery of Excess Quantities.'
WHERE clause_version_id = 14 AND clause_name = '52.211-17';

UPDATE Clauses
SET clause_title = 'Variation in Estimated Quantity' -- 'Variation in Estimated Quantity.'
WHERE clause_version_id = 14 AND clause_name = '52.211-18';

UPDATE Clauses
SET clause_title = 'Availability of Specifications, Standards, and Data Item Descriptions Listed in the Acquisition Streamlining and Standardization Information System (ASSIST)' -- 'Availability of Specifications, Standards, and Data Item Descriptions Listed in the Acquisition Streamlining and Standardization Information System (ASSIST).'
WHERE clause_version_id = 14 AND clause_name = '52.211-2';

UPDATE Clauses
SET clause_title = 'Availability of Specifications Not Listed in the GSA Index of Federal Specifications, Standards and Commercial Item Descriptions' -- 'Availability of Specifications Not Listed in the GSA Index of Federal Specifications, Standards and Commercial Item Descriptions.'
WHERE clause_version_id = 14 AND clause_name = '52.211-3';

UPDATE Clauses
SET clause_title = 'Availability for Examination of Specifications Not Listed in the GSA Index of Federal Specifications, Standards and Commercial Item Descriptions' -- 'Availability for Examination of Specifications Not Listed in the GSA Index of Federal Specifications, Standards and Commercial Item Descriptions.'
WHERE clause_version_id = 14 AND clause_name = '52.211-4';

UPDATE Clauses
SET clause_title = 'Material Requirements' -- 'Material Requirements.'
WHERE clause_version_id = 14 AND clause_name = '52.211-5';

UPDATE Clauses
SET clause_title = 'Brand Name or Equal' -- 'Brand Name or Equal.'
WHERE clause_version_id = 14 AND clause_name = '52.211-6';

UPDATE Clauses
SET clause_title = 'Alternatives to Government - Unique Standards' -- 'Alternatives to Government-Unique Standards.'
WHERE clause_version_id = 14 AND clause_name = '52.211-7';

UPDATE Clauses
SET clause_title = 'Time of Delivery' -- 'Time of Delivery.'
WHERE clause_version_id = 14 AND clause_name = '52.211-8';

UPDATE Clauses
SET clause_title = 'Desired and Required Time of Delivery' -- 'Desired and Required Time of Delivery.'
WHERE clause_version_id = 14 AND clause_name = '52.211-9';

UPDATE Clauses
SET clause_title = 'Instructions to Contractors - Commercial Items' -- 'Instructions to Contractors- Commercial Items.'
WHERE clause_version_id = 14 AND clause_name = '52.212-1';

UPDATE Clauses
SET clause_title = 'Evaluation - Commercial Items' -- 'Evaluation-Commercial Items.'
WHERE clause_version_id = 14 AND clause_name = '52.212-2';

UPDATE Clauses
SET clause_title = 'Offeror Representations and Certifications - Commercial Items' -- 'Offeror Representations and Certifications- Commercial Items.'
WHERE clause_version_id = 14 AND clause_name = '52.212-3';

UPDATE Clauses
SET clause_title = 'Contract Terms and Conditions - Commercial Items' -- 'Contract Terms and Conditions--Commercial Items.'
WHERE clause_version_id = 14 AND clause_name = '52.212-4';

UPDATE Clauses
SET clause_title = 'Contract Terms and Conditions Required to Implement Statutes or Executive Orders - Commercial Items' -- 'Contract Terms and Conditions Required to Implement Statutes or Executive Orders- Commercial Items.'
WHERE clause_version_id = 14 AND clause_name = '52.212-5';

UPDATE Clauses
SET clause_title = 'Contract Terms and Conditions Required to Implement Statutes or Executive Orders - Commercial Items - (Basic) (Deviation 2013-O0019) (Sep 2013)' -- 'Contract Terms and Conditions Required to Implement Statutes or Executive Orders-Commercial Items.'
WHERE clause_version_id = 14 AND clause_name = '52.212-5 (DEVIATION 2013-00019)';

UPDATE Clauses
SET clause_title = 'Fast Payment Procedure' -- 'Fast Payment Procedure.'
WHERE clause_version_id = 14 AND clause_name = '52.213-1';

UPDATE Clauses
SET clause_title = 'Invoices' -- 'Invoices.'
WHERE clause_version_id = 14 AND clause_name = '52.213-2';

UPDATE Clauses
SET clause_title = 'Notice to Supplier' -- 'Notice to Supplier.'
WHERE clause_version_id = 14 AND clause_name = '52.213-3';

UPDATE Clauses
SET clause_title = 'Terms and Conditions - Simplified Acquisitions (Other Than Commercial Items)' -- 'Terms and Conditions--Simplified Acquisitions (Other Than Commercial Items).'
WHERE clause_version_id = 14 AND clause_name = '52.213-4';

UPDATE Clauses
SET clause_title = 'Contract Award - Sealed Bidding' -- 'Contract Award-Sealed Bidding.'
WHERE clause_version_id = 14 AND clause_name = '52.214-10';

UPDATE Clauses
SET clause_title = 'Preparation of Bids' -- 'Preparation of Bids.'
WHERE clause_version_id = 14 AND clause_name = '52.214-12';

UPDATE Clauses
SET clause_title = 'Telegraphic Bids' -- 'Telegraphic Bids.'
WHERE clause_version_id = 14 AND clause_name = '52.214-13';

UPDATE Clauses
SET clause_title = 'Place of Performance - Sealed Bidding' -- 'Place of Performance-- Sealed Bidding.'
WHERE clause_version_id = 14 AND clause_name = '52.214-14';

UPDATE Clauses
SET clause_title = 'Period for Acceptance of Bids' -- 'Period for Acceptance of Bids.'
WHERE clause_version_id = 14 AND clause_name = '52.214-15';

UPDATE Clauses
SET clause_title = 'Minimum Bid Acceptance Period' -- 'Minimum Bid Acceptance Period.'
WHERE clause_version_id = 14 AND clause_name = '52.214-16';

UPDATE Clauses
SET clause_title = 'Preparation of Bids - Construction' -- 'Preparation of Bids-- Construction.'
WHERE clause_version_id = 14 AND clause_name = '52.214-18';

UPDATE Clauses
SET clause_title = 'Contract Award - Sealed Bidding - Construction' -- 'Contract Award-Sealed Bidding- Construction.'
WHERE clause_version_id = 14 AND clause_name = '52.214-19';

UPDATE Clauses
SET clause_title = 'Bid Samples' -- 'Bid Samples.'
WHERE clause_version_id = 14 AND clause_name = '52.214-20';

UPDATE Clauses
SET clause_title = 'Descriptive Literature' -- 'Descriptive Literature.'
WHERE clause_version_id = 14 AND clause_name = '52.214-21';

UPDATE Clauses
SET clause_title = 'Evaluation of Bids for Multiple Awards' -- 'Evaluation of Bids for Multiple Awards.'
WHERE clause_version_id = 14 AND clause_name = '52.214-22';

UPDATE Clauses
SET clause_title = 'Late Submissions, Modifications, and Withdrawals of Technical Proposals Under Two-Step Sealed Bidding' -- 'Late Submissions, Modifications, and Withdrawals of Technical Proposals Under Two-Step Sealed Bidding.'
WHERE clause_version_id = 14 AND clause_name = '52.214-23';

UPDATE Clauses
SET clause_title = 'Multiple Technical Proposals' -- 'Multiple Technical Proposals.'
WHERE clause_version_id = 14 AND clause_name = '52.214-24';

UPDATE Clauses
SET clause_title = 'Step Two of Two-Step Sealed Bidding' -- 'Step Two of Two-Step Sealed Bidding.'
WHERE clause_version_id = 14 AND clause_name = '52.214-25';

UPDATE Clauses
SET clause_title = 'Audit and Records - Sealed Bidding' -- 'Audit and Records--Sealed Bidding.'
WHERE clause_version_id = 14 AND clause_name = '52.214-26';

UPDATE Clauses
SET clause_title = 'Price Reduction for Defective Certified Cost or Pricing Data - Modifications - Sealed Bidding' -- 'Price Reduction for Defective Certified Cost or Pricing Data-- Modifications--Sealed Bidding.'
WHERE clause_version_id = 14 AND clause_name = '52.214-27';

UPDATE Clauses
SET clause_title = 'Subcontractor Certified Cost or Pricing Data - Modifications - Sealed Bidding' -- 'Subcontractor Certified Cost or Pricing Data--Modifications--Sealed Bidding.'
WHERE clause_version_id = 14 AND clause_name = '52.214-28';

UPDATE Clauses
SET clause_title = 'Order of Precedence - Sealed Bidding' -- 'Order of Precedence--Sealed Bidding.'
WHERE clause_version_id = 14 AND clause_name = '52.214-29';

UPDATE Clauses
SET clause_title = 'Amendments to Invitations for Bids' -- 'Amendments to Invitations for Bids.'
WHERE clause_version_id = 14 AND clause_name = '52.214-3';

UPDATE Clauses
SET clause_title = 'Facsimile Bids' -- 'Facsimile Bids.'
WHERE clause_version_id = 14 AND clause_name = '52.214-31';

UPDATE Clauses
SET clause_title = 'Submission of Offers in the English Language' -- 'Submission of Offers in the English Language.'
WHERE clause_version_id = 14 AND clause_name = '52.214-34';

UPDATE Clauses
SET clause_title = 'Submission of Offers in US Currency' -- 'Submission of Offers in U.S. Currency.'
WHERE clause_version_id = 14 AND clause_name = '52.214-35';

UPDATE Clauses
SET clause_title = 'False Statements in Bids' -- 'False Statements in Bids.'
WHERE clause_version_id = 14 AND clause_name = '52.214-4';

UPDATE Clauses
SET clause_title = 'Submission of Bids' -- 'Submission of Bids.'
WHERE clause_version_id = 14 AND clause_name = '52.214-5';

UPDATE Clauses
SET clause_title = 'Explanation to Prospective Bidders' -- 'Explanation to Prospective Bidders.'
WHERE clause_version_id = 14 AND clause_name = '52.214-6';

UPDATE Clauses
SET clause_title = 'Late Submissions, Modifications, and Withdrawals of Bids' -- 'Late Submissions, Modifications, and Withdrawals of Bids.'
WHERE clause_version_id = 14 AND clause_name = '52.214-7';

UPDATE Clauses
SET clause_title = 'Instructions to Contractors - Competitive' -- 'Instructions to Contractors-Competitive.'
WHERE clause_version_id = 14 AND clause_name = '52.215-1';

UPDATE Clauses
SET clause_title = 'Price Reduction for Defective Certified Cost or Pricing Data' -- 'Price Reduction for Defective Certified Cost or Pricing Data.'
WHERE clause_version_id = 14 AND clause_name = '52.215-10';

UPDATE Clauses
SET clause_title = 'Price Reduction for Defective Certified Cost or Pricing Data - Modifications' -- 'Price Reduction for Defective Certified Cost or Pricing Data--Modifications.'
WHERE clause_version_id = 14 AND clause_name = '52.215-11';

UPDATE Clauses
SET clause_title = 'Subcontractor Certified Cost or Pricing Data' -- 'Subcontractor Certified Cost or Pricing Data.'
WHERE clause_version_id = 14 AND clause_name = '52.215-12';

UPDATE Clauses
SET clause_title = 'Subcontractor Certified Cost or Pricing Data - Modifications' -- 'Subcontractor Certified Cost or Pricing Data-Modifications.'
WHERE clause_version_id = 14 AND clause_name = '52.215-13';

UPDATE Clauses
SET clause_title = 'Integrity of Unit Prices' -- 'Integrity of Unit Prices.'
WHERE clause_version_id = 14 AND clause_name = '52.215-14';

UPDATE Clauses
SET clause_title = 'Pension Adjustments and Asset Reversions' -- 'Pension Adjustments and Asset Reversions.'
WHERE clause_version_id = 14 AND clause_name = '52.215-15';

UPDATE Clauses
SET clause_title = 'Facilities Capital Cost of Money' -- 'Facilities Capital Cost of Money.'
WHERE clause_version_id = 14 AND clause_name = '52.215-16';

UPDATE Clauses
SET clause_title = 'Waiver of Facilities Capital Cost of Money' -- 'Waiver of Facilities Capital Cost of Money.'
WHERE clause_version_id = 14 AND clause_name = '52.215-17';

UPDATE Clauses
SET clause_title = 'Revision or Adjustment of Plans for Postretirement Benefits (PRB) Other Than Pensions' -- 'Revision or Adjustment of Plans for Postretirement Benefits (PRB) Other Than Pensions.'
WHERE clause_version_id = 14 AND clause_name = '52.215-18';

UPDATE Clauses
SET clause_title = 'Notification of Ownership Changes' -- 'Notification of Ownership Changes.'
WHERE clause_version_id = 14 AND clause_name = '52.215-19';

UPDATE Clauses
SET clause_title = 'Audit and Records - Negotiations' -- 'Audit and Records- Negotiations.'
WHERE clause_version_id = 14 AND clause_name = '52.215-2';

UPDATE Clauses
SET clause_title = 'Requirements for Certified Cost or Pricing Data and Data Other Than Certified Cost or Pricing Data' -- 'Requirements for Certified Cost or Pricing Data and Data Other Than Certified Cost or Pricing Data.'
WHERE clause_version_id = 14 AND clause_name = '52.215-20';

UPDATE Clauses
SET clause_title = 'Requirements for Certified Cost or Pricing Data and Data Other Than Certified Cost or Pricing Data - Modifications' -- 'Requirements for Certified Cost or Pricing Data and Data Other Than Certified Cost or Pricing Data - Modifications.'
WHERE clause_version_id = 14 AND clause_name = '52.215-21';

UPDATE Clauses
SET clause_title = 'Limitations on Pass - Through Charges-Identification of Subcontract Effort' -- 'Limitations on Pass-Through Charges-Identification of Subcontract Effort.'
WHERE clause_version_id = 14 AND clause_name = '52.215-22';

UPDATE Clauses
SET clause_title = 'Limitations on Pass - Through Charges' -- 'Limitations on Pass-Through Charges.'
WHERE clause_version_id = 14 AND clause_name = '52.215-23';

UPDATE Clauses
SET clause_title = 'Request for Information or Solicitation for Planning Purposes' -- 'Request for Information or Solicitation for Planning Purposes.'
WHERE clause_version_id = 14 AND clause_name = '52.215-3';

UPDATE Clauses
SET clause_title = 'Facsimile Proposals' -- 'Facsimile Proposals.'
WHERE clause_version_id = 14 AND clause_name = '52.215-5';

UPDATE Clauses
SET clause_title = 'Place of Performance' -- 'Place of Performance.'
WHERE clause_version_id = 14 AND clause_name = '52.215-6';

UPDATE Clauses
SET clause_title = 'Order of Precedence - Uniform Contract Format' -- 'Order of Precedence-Uniform Contract Format.'
WHERE clause_version_id = 14 AND clause_name = '52.215-8';

UPDATE Clauses
SET clause_title = 'Changes or Additions to Make-or-Buy Program' -- 'Changes or Additions to Make-or-Buy Program.'
WHERE clause_version_id = 14 AND clause_name = '52.215-9';

UPDATE Clauses
SET clause_title = 'Type of Contract' -- 'Type of Contract.'
WHERE clause_version_id = 14 AND clause_name = '52.216-1';

UPDATE Clauses
SET clause_title = 'Incentive Fee' -- 'Incentive Fee.'
WHERE clause_version_id = 14 AND clause_name = '52.216-10';

UPDATE Clauses
SET clause_title = 'Cost Contract - No Fee' -- 'Cost Contract-No Fee.'
WHERE clause_version_id = 14 AND clause_name = '52.216-11';

UPDATE Clauses
SET clause_title = 'Cost - Sharing Contract - No Fee' -- 'Cost-Sharing Contract-No Fee.'
WHERE clause_version_id = 14 AND clause_name = '52.216-12';

UPDATE Clauses
SET clause_title = 'Predetermined Indirect Cost Rates' -- 'Predetermined Indirect Cost Rates.'
WHERE clause_version_id = 14 AND clause_name = '52.216-15';

UPDATE Clauses
SET clause_title = 'Incentive Price Revision - Firm Target' -- 'Incentive Price Revision-Firm Target.'
WHERE clause_version_id = 14 AND clause_name = '52.216-16';

UPDATE Clauses
SET clause_title = 'Incentive Price Revision - Successive Targets' -- 'Incentive Price Revision-Successive Targets.'
WHERE clause_version_id = 14 AND clause_name = '52.216-17';

UPDATE Clauses
SET clause_title = 'Ordering' -- 'Ordering.'
WHERE clause_version_id = 14 AND clause_name = '52.216-18';

UPDATE Clauses
SET clause_title = 'Order Limitations' -- 'Order Limitations.'
WHERE clause_version_id = 14 AND clause_name = '52.216-19';

UPDATE Clauses
SET clause_title = 'Economic Price Adjustment - Standard Supplies' -- 'Economic Price Adjustment- Standard Supplies.'
WHERE clause_version_id = 14 AND clause_name = '52.216-2';

UPDATE Clauses
SET clause_title = 'Definite Quantity' -- 'Definite Quantity.'
WHERE clause_version_id = 14 AND clause_name = '52.216-20';

UPDATE Clauses
SET clause_title = 'Requirements' -- 'Requirements.'
WHERE clause_version_id = 14 AND clause_name = '52.216-21';

UPDATE Clauses
SET clause_title = 'Indefinite Quantity' -- 'Indefinite Quantity.'
WHERE clause_version_id = 14 AND clause_name = '52.216-22';

UPDATE Clauses
SET clause_title = 'Execution and Commencement of Work' -- 'Execution and Commencement of Work.'
WHERE clause_version_id = 14 AND clause_name = '52.216-23';

UPDATE Clauses
SET clause_title = 'Limitation of Government Liability' -- 'Limitation of Government Liability.'
WHERE clause_version_id = 14 AND clause_name = '52.216-24';

UPDATE Clauses
SET clause_title = 'Contract Definitization' -- 'Contract Definitization.'
WHERE clause_version_id = 14 AND clause_name = '52.216-25';

UPDATE Clauses
SET clause_title = 'Payments of Allowable Costs Before Definitization' -- 'Payments of Allowable Costs Before Definitization.'
WHERE clause_version_id = 14 AND clause_name = '52.216-26';

UPDATE Clauses
SET clause_title = 'Single or Multiple Awards' -- 'Single or Multiple Awards.'
WHERE clause_version_id = 14 AND clause_name = '52.216-27';

UPDATE Clauses
SET clause_title = 'Multiple Awards for Advisory and Assistance Services' -- 'Multiple Awards for Advisory and Assistance Services.'
WHERE clause_version_id = 14 AND clause_name = '52.216-28';

UPDATE Clauses
SET clause_title = 'T&M/LH Proposal Requirements - Non-Commercial Item Acquisition With Adequate Price Competition' -- 'T&M/LH Proposal Requirements--Non-Commercial Item Acquisition With Adequate Price Competition.'
WHERE clause_version_id = 14 AND clause_name = '52.216-29';

UPDATE Clauses
SET clause_title = 'Economic Price Adjustment - Semistandard Supplies' -- 'Economic Price Adjustment-Semistandard Supplies.'
WHERE clause_version_id = 14 AND clause_name = '52.216-3';

UPDATE Clauses
SET clause_title = 'T&M/LH Proposal Requirements - Non-Commercial Item Acquisition Without Adequate Price Competition' -- 'T&M/LH Proposal Requirements--Non-Commercial Item Acquisition Without Adequate Price Competition.'
WHERE clause_version_id = 14 AND clause_name = '52.216-30';

UPDATE Clauses
SET clause_title = 'T&M/LH Proposal Requirements - Commercial Item Acquisition' -- 'T&M/LH Proposal Requirements--Commercial Item Acquisition.'
WHERE clause_version_id = 14 AND clause_name = '52.216-31';

UPDATE Clauses
SET clause_title = 'Economic Price Adjustment - Labor and Material' -- 'Economic Price Adjustment-- Labor and Material.'
WHERE clause_version_id = 14 AND clause_name = '52.216-4';

UPDATE Clauses
SET clause_title = 'Price Redetermination - Prospective' -- 'Price Redetermination - Prospective.'
WHERE clause_version_id = 14 AND clause_name = '52.216-5';

UPDATE Clauses
SET clause_title = 'Price Redetermination - Retroactive' -- 'Price Redetermination-Retroactive.'
WHERE clause_version_id = 14 AND clause_name = '52.216-6';

UPDATE Clauses
SET clause_title = 'Allowable Cost and Payment' -- 'Allowable Cost and Payment.'
WHERE clause_version_id = 14 AND clause_name = '52.216-7';

UPDATE Clauses
SET clause_title = 'Fixed Fee' -- 'Fixed Fee.'
WHERE clause_version_id = 14 AND clause_name = '52.216-8';

UPDATE Clauses
SET clause_title = 'Fixed Fee - Construction' -- 'Fixed Fee-Construction.'
WHERE clause_version_id = 14 AND clause_name = '52.216-9';

UPDATE Clauses
SET clause_title = 'Cancellation Under Multiyear Contracts' -- 'Cancellation Under Multiyear Contracts.'
WHERE clause_version_id = 14 AND clause_name = '52.217-2';

UPDATE Clauses
SET clause_title = 'Evaluation Exclusive of Options' -- 'Evaluation Exclusive of Options.'
WHERE clause_version_id = 14 AND clause_name = '52.217-3';

UPDATE Clauses
SET clause_title = 'Evaluation of Options Exercised At Time of Contract Award' -- 'Evaluation of Options Exercised At Time of Contract Award.'
WHERE clause_version_id = 14 AND clause_name = '52.217-4';

UPDATE Clauses
SET clause_title = 'Evaluation of Options' -- 'Evaluation of Options.'
WHERE clause_version_id = 14 AND clause_name = '52.217-5';

UPDATE Clauses
SET clause_title = 'Option for Increased Quantity' -- 'Option for Increased Quantity.'
WHERE clause_version_id = 14 AND clause_name = '52.217-6';

UPDATE Clauses
SET clause_title = 'Option for Increased Quantity - Separately Priced Line Item' -- 'Option for Increased Quantity-Separately Priced Line Item.'
WHERE clause_version_id = 14 AND clause_name = '52.217-7';

UPDATE Clauses
SET clause_title = 'Option to Extend Services' -- 'Option to Extend Services.'
WHERE clause_version_id = 14 AND clause_name = '52.217-8';

UPDATE Clauses
SET clause_title = 'Option to Extend the Term of the Contract' -- 'Option to Extend the Term of the Contract.'
WHERE clause_version_id = 14 AND clause_name = '52.217-9';

UPDATE Clauses
SET clause_title = 'Small Business Program Representation' -- 'Small Business Program Representation.'
WHERE clause_version_id = 14 AND clause_name = '52.219-1';

UPDATE Clauses
SET clause_title = 'Incentive Subcontracting Program' -- 'Incentive Subcontracting Program.'
WHERE clause_version_id = 14 AND clause_name = '52.219-10';

UPDATE Clauses
SET clause_title = 'Special 8(a) Contract Conditions' -- 'Special 8(a) Contract Conditions.'
WHERE clause_version_id = 14 AND clause_name = '52.219-11';

UPDATE Clauses
SET clause_title = 'Special 8(a) Sub - Contract Conditions' -- 'Special 8(a) Sub-Contract Conditions.'
WHERE clause_version_id = 14 AND clause_name = '52.219-12';

UPDATE Clauses
SET clause_title = 'Notice of Set-Aside of Orders' -- 'Notice of Set-Aside of Orders.'
WHERE clause_version_id = 14 AND clause_name = '52.219-13';

UPDATE Clauses
SET clause_title = 'Limitations on Subcontracting' -- 'Limitations on Subcontracting.'
WHERE clause_version_id = 14 AND clause_name = '52.219-14';

UPDATE Clauses
SET clause_title = 'Liquidated Damages - Subcontracting Plan' -- 'Liquidated Damages-Subcontracting Plan.'
WHERE clause_version_id = 14 AND clause_name = '52.219-16';

UPDATE Clauses
SET clause_title = 'Section 8(a) Award' -- 'Section 8(a) Award.'
WHERE clause_version_id = 14 AND clause_name = '52.219-17';

UPDATE Clauses
SET clause_title = 'Notification of Competition Limited to Eligible 8(a) Concerns' -- 'Notification of Competition Limited to Eligible 8(a) Concerns.'
WHERE clause_version_id = 14 AND clause_name = '52.219-18';

UPDATE Clauses
SET clause_title = 'Equal Low Bids' -- 'Equal Low Bids.'
WHERE clause_version_id = 14 AND clause_name = '52.219-2';

UPDATE Clauses
SET clause_title = 'Notice of Service - Disabled Veteran - Owned Small Business Set Aside' -- 'Notice of Service-Disabled Veteran-Owned Small Business Set Aside.'
WHERE clause_version_id = 14 AND clause_name = '52.219-27';

UPDATE Clauses
SET clause_title = 'Post-Award Small Business Program Rerepresentation' -- 'Post-Award Small Business Program Rerepresentation.'
WHERE clause_version_id = 14 AND clause_name = '52.219-28';

UPDATE Clauses
SET clause_title = 'Notice of Set-Aside for Economically Disadvantaged Women-Owned Small Business (EDWOSB) Concerns' -- 'Notice of Set-Aside for Economically Disadvantaged Women-Owned Small Business (EDWOSB) Concerns.'
WHERE clause_version_id = 14 AND clause_name = '52.219-29';

UPDATE Clauses
SET clause_title = 'Notice of Hubzone Set-Aside or Sole Source Award' -- 'Notice of Hubzone Set-Aside or Sole Source Award.'
WHERE clause_version_id = 14 AND clause_name = '52.219-3';

UPDATE Clauses
SET clause_title = 'Notice of Set-Aside for Women-Owned Small Business Concerns Eligible Under the Women-Owned Small Business Program' -- 'Notice of Set-Aside for Women-Owned Small Business Concerns Eligible Under the Women-Owned Small Business Program.'
WHERE clause_version_id = 14 AND clause_name = '52.219-30';

UPDATE Clauses
SET clause_title = 'Notice of Price Evaluation Preference for Hubzone Small Business Concerns' -- 'Notice of Price Evaluation Preference for Hubzone Small Business Concerns.'
WHERE clause_version_id = 14 AND clause_name = '52.219-4';

UPDATE Clauses
SET clause_title = 'Notice of Total Small Business Set-Aside' -- 'Notice of Total Small Business Set-Aside.'
WHERE clause_version_id = 14 AND clause_name = '52.219-6';

UPDATE Clauses
SET clause_title = 'Notice of Partial Small Business Set-Aside' -- 'Notice of Partial Small Business Set-Aside.'
WHERE clause_version_id = 14 AND clause_name = '52.219-7';

UPDATE Clauses
SET clause_title = 'Utilization of Small Business Concerns' -- 'Utilization of Small Business Concerns.'
WHERE clause_version_id = 14 AND clause_name = '52.219-8';

UPDATE Clauses
SET clause_title = 'Small Business Subcontracting Plan' -- 'Small Business Subcontracting Plan.'
WHERE clause_version_id = 14 AND clause_name = '52.219-9';

UPDATE Clauses
SET clause_title = 'Small Business Subcontracting Plan - (Basic) (Deviation 2013-O0014) (Aug 2013)' -- 'Small Business Subcontracting Plan.'
WHERE clause_version_id = 14 AND clause_name = '52.219-9 (DEVIATION 2013-00014)';

UPDATE Clauses
SET clause_title = 'Notice to the Government of Labor Disputes' -- 'Notice to the Government of Labor Disputes.'
WHERE clause_version_id = 14 AND clause_name = '52.222-1';

UPDATE Clauses
SET clause_title = 'Compliance With Copeland Act' -- 'Compliance With Copeland Act.'
WHERE clause_version_id = 14 AND clause_name = '52.222-10';

UPDATE Clauses
SET clause_title = 'Subcontracts (Labor Standards)' -- 'Subcontracts (Labor Standards).'
WHERE clause_version_id = 14 AND clause_name = '52.222-11';

UPDATE Clauses
SET clause_title = 'Contract Termination - Debarment' -- 'Contract Termination- Debarment.'
WHERE clause_version_id = 14 AND clause_name = '52.222-12';

UPDATE Clauses
SET clause_title = 'Compliance With Construction Wage Rate Requirements and Related Regulations' -- 'Compliance With Construction Wage Rate Requirements and Related Regulations.'
WHERE clause_version_id = 14 AND clause_name = '52.222-13';

UPDATE Clauses
SET clause_title = 'Disputes Concerning Labor Standards' -- 'Disputes Concerning Labor Standards.'
WHERE clause_version_id = 14 AND clause_name = '52.222-14';

UPDATE Clauses
SET clause_title = 'Certification of Eligibility' -- 'Certification of Eligibility.'
WHERE clause_version_id = 14 AND clause_name = '52.222-15';

UPDATE Clauses
SET clause_title = 'Approval of Wage Rates' -- 'Approval of Wage Rates.'
WHERE clause_version_id = 14 AND clause_name = '52.222-16';

UPDATE Clauses
SET clause_title = 'Nondisplacement of Qualified Workers' -- 'Nondisplacement of Qualified Workers.'
WHERE clause_version_id = 14 AND clause_name = '52.222-17';

UPDATE Clauses
SET clause_title = 'Certification Regarding Knowledge of Child Labor for Listed End Products' -- 'Certification Regarding Knowledge of Child Labor for Listed End Products.'
WHERE clause_version_id = 14 AND clause_name = '52.222-18';

UPDATE Clauses
SET clause_title = 'Child Labor - Cooperation With Authorities and Remedies' -- 'Child Labor---Cooperation With Authorities and Remedies.'
WHERE clause_version_id = 14 AND clause_name = '52.222-19';

UPDATE Clauses
SET clause_title = 'Payment for Overtime Premiums' -- 'Payment for Overtime Premiums.'
WHERE clause_version_id = 14 AND clause_name = '52.222-2';

UPDATE Clauses
SET clause_title = 'Contracts for Materials, Supplies, Articles and Equipment Exceeding 15,000' -- 'Contracts for Materials, Supplies, Articles and Equipment Exceeding 15,000.'
WHERE clause_version_id = 14 AND clause_name = '52.222-20';

UPDATE Clauses
SET clause_title = 'Prohibition of Segregated Facilities' -- 'Prohibition of Segregated Facilities.'
WHERE clause_version_id = 14 AND clause_name = '52.222-21';

UPDATE Clauses
SET clause_title = 'Previous Contracts and Compliance Reports' -- 'Previous Contracts and Compliance Reports.'
WHERE clause_version_id = 14 AND clause_name = '52.222-22';

UPDATE Clauses
SET clause_title = 'Notice of Requirement for Affirmative Action to Ensure Equal Employment Opportunity for Construction' -- 'Notice of Requirement for Affirmative Action to Ensure Equal Employment Opportunity for Construction.'
WHERE clause_version_id = 14 AND clause_name = '52.222-23';

UPDATE Clauses
SET clause_title = 'Preaward On-Site Equal Opportunity Compliance Evaluation' -- 'Preaward On-Site Equal Opportunity Compliance Evaluation.'
WHERE clause_version_id = 14 AND clause_name = '52.222-24';

UPDATE Clauses
SET clause_title = 'Affirmative Action Compliance' -- 'Affirmative Action Compliance.'
WHERE clause_version_id = 14 AND clause_name = '52.222-25';

UPDATE Clauses
SET clause_title = 'Equal Opportunity' -- 'Equal Opportunity.'
WHERE clause_version_id = 14 AND clause_name = '52.222-26';

UPDATE Clauses
SET clause_title = 'Affirmative Action Compliance Requirements for Construction' -- 'Affirmative Action Compliance Requirements for Construction.'
WHERE clause_version_id = 14 AND clause_name = '52.222-27';

UPDATE Clauses
SET clause_title = 'Notification of Visa Denial' -- 'Notification of Visa Denial.'
WHERE clause_version_id = 14 AND clause_name = '52.222-29';

UPDATE Clauses
SET clause_title = 'Convict Labor' -- 'Convict Labor.'
WHERE clause_version_id = 14 AND clause_name = '52.222-3';

UPDATE Clauses
SET clause_title = 'Construction Wage Rate Requirements - Price Adjustment (None or Separately Specified Method)' -- 'Construction Wage Rate Requirements--Price Adjustment (None or Separately Specified Method).'
WHERE clause_version_id = 14 AND clause_name = '52.222-30';

UPDATE Clauses
SET clause_title = 'Construction Wage Rate Requirements - Price Adjustment (Percentage Method)' -- 'Construction Wage Rate Requirements--Price Adjustment (Percentage Method).'
WHERE clause_version_id = 14 AND clause_name = '52.222-31';

UPDATE Clauses
SET clause_title = 'Construction Wage Rate Requirements - Price Adjustment (Actual Method)' -- 'Construction Wage Rate Requirements--Price Adjustment (Actual Method).'
WHERE clause_version_id = 14 AND clause_name = '52.222-32';

UPDATE Clauses
SET clause_title = 'Notice of Requirement Project Labor Agreement' -- 'Notice of Requirement Project Labor Agreement.'
WHERE clause_version_id = 14 AND clause_name = '52.222-33';

UPDATE Clauses
SET clause_title = 'Project Labor Agreement' -- 'Project Labor Agreement.'
WHERE clause_version_id = 14 AND clause_name = '52.222-34';

UPDATE Clauses
SET clause_title = 'Equal Opportunity Veterans' -- 'Equal Opportunity Veterans.'
WHERE clause_version_id = 14 AND clause_name = '52.222-35';

UPDATE Clauses
SET clause_title = 'Equal Opportunity for Workers With Disabilities' -- 'Equal Opportunity for Workers With Disabilities.'
WHERE clause_version_id = 14 AND clause_name = '52.222-36';

UPDATE Clauses
SET clause_title = 'Employment Reports on Veterans' -- 'Employment Reports on Veterans.'
WHERE clause_version_id = 14 AND clause_name = '52.222-37';

UPDATE Clauses
SET clause_title = 'Compliance With Veterans'' Employment Reporting Requirements' -- 'Compliance With Veterans'' Employment Reporting Requirements.'
WHERE clause_version_id = 14 AND clause_name = '52.222-38';

UPDATE Clauses
SET clause_title = 'Contract Work Hours and Safety Standards - Overtime Compensation' -- 'Contract Work Hours and Safety Standards-Overtime Compensation.'
WHERE clause_version_id = 14 AND clause_name = '52.222-4';

UPDATE Clauses
SET clause_title = 'Notification of Employee Rights Under The National Labor Relations Act' -- 'Notification of Employee Rights Under The National Labor Relations Act.'
WHERE clause_version_id = 14 AND clause_name = '52.222-40';

UPDATE Clauses
SET clause_title = 'Service Contract Labor Standards' -- 'Service Contract Labor Standards.'
WHERE clause_version_id = 14 AND clause_name = '52.222-41';

UPDATE Clauses
SET clause_title = 'Statement of Equivalent Rates for Federal Hires' -- 'Statement of Equivalent Rates for Federal Hires.'
WHERE clause_version_id = 14 AND clause_name = '52.222-42';

UPDATE Clauses
SET clause_title = 'Fair Labor Standards Act and Service Contract Labor Standards - Price Adjustment (Multiple Year and Option Contracts)' -- 'Fair Labor Standards Act and Service Contract Labor Standards--Price Adjustment (Multiple Year and Option Contracts).'
WHERE clause_version_id = 14 AND clause_name = '52.222-43';

UPDATE Clauses
SET clause_title = 'Fair Labor Standards Act and Service Contract Labor Standards - Price Adjustment' -- 'Fair Labor Standards Act and Service Contract Labor Standards--Price Adjustment.'
WHERE clause_version_id = 14 AND clause_name = '52.222-44';

UPDATE Clauses
SET clause_title = 'Evaluation of Compensation for Professional Employees' -- 'Evaluation of Compensation for Professional Employees.'
WHERE clause_version_id = 14 AND clause_name = '52.222-46';

UPDATE Clauses
SET clause_title = 'Exemption From Application of the Service Contract Labor Standards to Contracts for Maintenance, Calibration, or Repair of Certain Equipment - Certification' -- 'Exemption From Application of the Service Contract Labor Standards to Contracts for Maintenance, Calibration, or Repair of Certain Equipment--Certification.'
WHERE clause_version_id = 14 AND clause_name = '52.222-48';

UPDATE Clauses
SET clause_title = 'Service Contract Labor Standards - Place of Performance Unknown' -- 'Service Contract Labor Standards--Place of Performance Unknown.'
WHERE clause_version_id = 14 AND clause_name = '52.222-49';

UPDATE Clauses
SET clause_title = 'Construction Wage Rate Requirements - Secondary Site of the Work' -- 'Construction Wage Rate Requirements--Secondary Site of the Work.'
WHERE clause_version_id = 14 AND clause_name = '52.222-5';

UPDATE Clauses
SET clause_title = 'Combating Trafficking in Persons' -- 'Combating Trafficking in Persons.'
WHERE clause_version_id = 14 AND clause_name = '52.222-50';

UPDATE Clauses
SET clause_title = 'Exemption From Application of the Service Contract Labor Standards to Contracts for Maintenance, Calibration, or Repair of Certain Equipment - Requirements' -- 'Exemption From Application of the Service Contract Labor Standards to Contracts for Maintenance, Calibration, or Repair of Certain Equipment-Requirements.'
WHERE clause_version_id = 14 AND clause_name = '52.222-51';

UPDATE Clauses
SET clause_title = 'Exemption From Application of the Service Contract Labor Standards to Contracts for Certain Services - Certification' -- 'Exemption From Application of the Service Contract Labor Standards to Contracts for Certain Services-Certification.'
WHERE clause_version_id = 14 AND clause_name = '52.222-52';

UPDATE Clauses
SET clause_title = 'Exemption From Application of the Service Contract Labor Standards to Contracts for Certain Services - Requirements' -- 'Exemption From Application of the Service Contract Labor Standards to Contracts for Certain Services-Requirements.'
WHERE clause_version_id = 14 AND clause_name = '52.222-53';

UPDATE Clauses
SET clause_title = 'Employment Eligibility Verification' -- 'Employment Eligibility Verification.'
WHERE clause_version_id = 14 AND clause_name = '52.222-54';

UPDATE Clauses
SET clause_title = 'Minimum Wages Under Executive Order 13658' -- 'Minimum Wages Under Executive Order 13658.'
WHERE clause_version_id = 14 AND clause_name = '52.222-55';

UPDATE Clauses
SET clause_title = 'Certification Regarding Trafficking in Persons Compliance Plan' -- 'Certification Regarding Trafficking in Persons Compliance Plan.'
WHERE clause_version_id = 14 AND clause_name = '52.222-56';

UPDATE Clauses
SET clause_title = 'Construction Wage Rate Requirements' -- 'Construction Wage Rate Requirements.'
WHERE clause_version_id = 14 AND clause_name = '52.222-6';

UPDATE Clauses
SET clause_title = 'Withholding of Funds' -- 'Withholding of Funds.'
WHERE clause_version_id = 14 AND clause_name = '52.222-7';

UPDATE Clauses
SET clause_title = 'Payrolls and Basic Records' -- 'Payrolls and Basic Records.'
WHERE clause_version_id = 14 AND clause_name = '52.222-8';

UPDATE Clauses
SET clause_title = 'Apprentices and Trainees' -- 'Apprentices and Trainees.'
WHERE clause_version_id = 14 AND clause_name = '52.222-9';

UPDATE Clauses
SET clause_title = 'Biobased Product Certification' -- 'Biobased Product Certification.'
WHERE clause_version_id = 14 AND clause_name = '52.223-1';

UPDATE Clauses
SET clause_title = 'Waste Reduction Program' -- 'Waste Reduction Program.'
WHERE clause_version_id = 14 AND clause_name = '52.223-10';

UPDATE Clauses
SET clause_title = 'Ozone-Depleting Substances' -- 'Ozone- Depleting Substances.'
WHERE clause_version_id = 14 AND clause_name = '52.223-11';

UPDATE Clauses
SET clause_title = 'Refrigeration Equipment and Air Conditioners' -- 'Refrigeration Equipment and Air Conditioners.'
WHERE clause_version_id = 14 AND clause_name = '52.223-12';

UPDATE Clauses
SET clause_title = 'Acquisition of EPEAT-Registered Imaging Equipment' -- 'Acquisition of EPEAT-Registered Imaging Equipment.'
WHERE clause_version_id = 14 AND clause_name = '52.223-13';

UPDATE Clauses
SET clause_title = 'Acquisition of EPEAT-Registered Televisions' -- 'Acquisition of EPEAT-Registered Televisions.'
WHERE clause_version_id = 14 AND clause_name = '52.223-14';

UPDATE Clauses
SET clause_title = 'Energy Efficiency in Energy - Consuming Products' -- 'Energy Efficiency in Energy-Consuming Products.'
WHERE clause_version_id = 14 AND clause_name = '52.223-15';

UPDATE Clauses
SET clause_title = 'Acquisition of EPEAT Registered Personal Computer Products' -- 'Acquisition of EPEAT Registered Personal Computer Products.'
WHERE clause_version_id = 14 AND clause_name = '52.223-16';

UPDATE Clauses
SET clause_title = 'Affirmative Procurement of EPA-Designated Items in Service and Construction Contracts' -- 'Affirmative Procurement of EPA-Designated Items in Service and Construction Contracts.'
WHERE clause_version_id = 14 AND clause_name = '52.223-17';

UPDATE Clauses
SET clause_title = 'Encouraging Contractor Policies to Ban Text Messaging While Driving' -- 'Encouraging Contractor Policies to Ban Text Messaging While Driving.'
WHERE clause_version_id = 14 AND clause_name = '52.223-18';

UPDATE Clauses
SET clause_title = 'Compliance With Environmental Management Systems' -- 'Compliance With Environmental Management Systems.'
WHERE clause_version_id = 14 AND clause_name = '52.223-19';

UPDATE Clauses
SET clause_title = 'Affirmative Procurement of Biobased Products Under Service and Construction Contracts' -- 'Affirmative Procurement of Biobased Products Under Service and Construction Contracts.'
WHERE clause_version_id = 14 AND clause_name = '52.223-2';

UPDATE Clauses
SET clause_title = 'Hazardous Material Identification and Material Safety Data' -- 'Hazardous Material Identification and Material Safety Data.'
WHERE clause_version_id = 14 AND clause_name = '52.223-3';

UPDATE Clauses
SET clause_title = 'Recovered Material Certification' -- 'Recovered Material Certification.'
WHERE clause_version_id = 14 AND clause_name = '52.223-4';

UPDATE Clauses
SET clause_title = 'Pollution Prevention and Right-To-Know Information' -- 'Pollution Prevention and Right-To-Know Information.'
WHERE clause_version_id = 14 AND clause_name = '52.223-5';

UPDATE Clauses
SET clause_title = 'Drug-Free Workplace' -- 'Drug-Free Workplace.'
WHERE clause_version_id = 14 AND clause_name = '52.223-6';

UPDATE Clauses
SET clause_title = 'Notice of Radioactive Materials' -- 'Notice of Radioactive Materials.'
WHERE clause_version_id = 14 AND clause_name = '52.223-7';

UPDATE Clauses
SET clause_title = 'Estimate of Percentage of Recovered Material Content for EPA Designated Products' -- 'Estimate of Percentage of Recovered Material Content for EPA Designated Products.'
WHERE clause_version_id = 14 AND clause_name = '52.223-9';

UPDATE Clauses
SET clause_title = 'Privacy Act Notification' -- 'Privacy Act Notification.'
WHERE clause_version_id = 14 AND clause_name = '52.224-1';

UPDATE Clauses
SET clause_title = 'Privacy Act' -- 'Privacy Act.'
WHERE clause_version_id = 14 AND clause_name = '52.224-2';

UPDATE Clauses
SET clause_title = 'Buy American - Supplies' -- 'Buy American -- Supplies.'
WHERE clause_version_id = 14 AND clause_name = '52.225-1';

UPDATE Clauses
SET clause_title = 'Notice of Buy American Requirement - Construction Materials' -- 'Notice of Buy American Requirement -- Construction Materials.'
WHERE clause_version_id = 14 AND clause_name = '52.225-10';

UPDATE Clauses
SET clause_title = 'Buy American - Construction Materials Under Trade Agreements' -- 'Buy American -- Construction Materials Under Trade Agreements.'
WHERE clause_version_id = 14 AND clause_name = '52.225-11';

UPDATE Clauses
SET clause_title = 'Notice of Buy American Requirement - Construction Materials Under Trade Agreements' -- 'Notice of Buy American Requirement -- Construction Materials Under Trade Agreements.'
WHERE clause_version_id = 14 AND clause_name = '52.225-12';

UPDATE Clauses
SET clause_title = 'Restrictions on Certain Foreign Purchases' -- 'Restrictions on Certain Foreign Purchases.'
WHERE clause_version_id = 14 AND clause_name = '52.225-13';

UPDATE Clauses
SET clause_title = 'Inconsistency Between English Version and Translation of Contract' -- 'Inconsistency Between English Version and Translation of Contract.'
WHERE clause_version_id = 14 AND clause_name = '52.225-14';

UPDATE Clauses
SET clause_title = 'Evaluation of Foreign Currency Offers' -- 'Evaluation of Foreign Currency Offers.'
WHERE clause_version_id = 14 AND clause_name = '52.225-17';

UPDATE Clauses
SET clause_title = 'Place of Manufacture' -- 'Place of Manufacture.'
WHERE clause_version_id = 14 AND clause_name = '52.225-18';

UPDATE Clauses
SET clause_title = 'Contractor Personnel in a Designated Operational Area or Supporting a Diplomatic or Consular Mission Outside the United States' -- 'Contractor Personnel in a Designated Operational Area or Supporting a Diplomatic or Consular Mission Outside the United States.'
WHERE clause_version_id = 14 AND clause_name = '52.225-19';

UPDATE Clauses
SET clause_title = 'Buy American Certificate' -- 'Buy American Certificate.'
WHERE clause_version_id = 14 AND clause_name = '52.225-2';

UPDATE Clauses
SET clause_title = 'Prohibition on Conducting Restricted Business Operations in Sudan - Certification' -- 'Prohibition on Conducting Restricted Business Operations in Sudan-Certification.'
WHERE clause_version_id = 14 AND clause_name = '52.225-20';

UPDATE Clauses
SET clause_title = 'Required Use of American Iron, Steel, and Manufactured Goods - Buy American Statute - Construction Materials' -- 'Required Use of American Iron, Steel, and Manufactured Goods-Buy American Statute-Construction Materials.'
WHERE clause_version_id = 14 AND clause_name = '52.225-21';

UPDATE Clauses
SET clause_title = 'Notice of Required Use of American Iron, Steel, and Manufactured Goods - Buy American Statute - Construction Materials' -- 'Notice of Required Use of American Iron, Steel, and Manufactured Goods-Buy American Statute-Construction Materials.'
WHERE clause_version_id = 14 AND clause_name = '52.225-22';

UPDATE Clauses
SET clause_title = 'Required Use of American Iron, Steel, and Manufactured Goods - Buy American Statute - Construction Materials Under Trade Agreements' -- 'Required Use of American Iron, Steel, and Manufactured Goods-Buy American Statute-Construction Materials Under Trade Agreements.'
WHERE clause_version_id = 14 AND clause_name = '52.225-23';

UPDATE Clauses
SET clause_title = 'Notice of Required Use of American Iron, Steel, and Manufactured Goods - Buy American Statute - Construction Materials Under Trade Agreements' -- 'Notice of Required Use of American Iron, Steel, and Manufactured Goods-Buy American Statute-Construction Materials Under Trade Agreements.'
WHERE clause_version_id = 14 AND clause_name = '52.225-24';

UPDATE Clauses
SET clause_title = 'Prohibition on Contracting With Entities Engaging in Certain Activities or Transactions Relating to Iran - Representation and Certifications' -- 'Prohibition on Contracting With Entities Engaging in Certain Activities or Transactions Relating to Iran--Representation and Certifications.'
WHERE clause_version_id = 14 AND clause_name = '52.225-25';

UPDATE Clauses
SET clause_title = 'Contractors Performing Private Security Functions Outside the United States' -- 'Contractors Performing Private Security Functions Outside the United States.'
WHERE clause_version_id = 14 AND clause_name = '52.225-26';

UPDATE Clauses
SET clause_title = 'Buy American - Free Trade Agreements - Israeli Trade Act' -- 'Buy American -- Free Trade Agreements -- Israeli Trade Act.'
WHERE clause_version_id = 14 AND clause_name = '52.225-3';

UPDATE Clauses
SET clause_title = 'Buy American - Free Trade Agreements - Israeli Trade Act Certificate' -- 'Buy American -- Free Trade Agreements -- Israeli Trade Act Certificate.'
WHERE clause_version_id = 14 AND clause_name = '52.225-4';

UPDATE Clauses
SET clause_title = 'Trade Agreements' -- 'Trade Agreements.'
WHERE clause_version_id = 14 AND clause_name = '52.225-5';

UPDATE Clauses
SET clause_title = 'Trade Agreements Certificate' -- 'Trade Agreements Certificate.'
WHERE clause_version_id = 14 AND clause_name = '52.225-6';

UPDATE Clauses
SET clause_title = 'Waiver of Buy American Statute for Civil Aircraft and Related Articles' -- 'Waiver of Buy American Statute for Civil Aircraft and Related Articles.'
WHERE clause_version_id = 14 AND clause_name = '52.225-7';

UPDATE Clauses
SET clause_title = 'Duty-Free Entry' -- 'Duty-Free Entry.'
WHERE clause_version_id = 14 AND clause_name = '52.225-8';

UPDATE Clauses
SET clause_title = 'Buy American - Construction Materials' -- 'Buy American -- Construction Materials.'
WHERE clause_version_id = 14 AND clause_name = '52.225-9';

UPDATE Clauses
SET clause_title = 'Utilization of Indian Organizations and Indian-Owned Economic Enterprises' -- 'Utilization of Indian Organizations and Indian-Owned Economic Enterprises.'
WHERE clause_version_id = 14 AND clause_name = '52.226-1';

UPDATE Clauses
SET clause_title = 'Historically Black College or University and Minority Institution Representation' -- 'Historically Black College or University and Minority Institution Representation.'
WHERE clause_version_id = 14 AND clause_name = '52.226-2';

UPDATE Clauses
SET clause_title = 'Disaster or Emergency Area Representation' -- 'Disaster or Emergency Area Representation.'
WHERE clause_version_id = 14 AND clause_name = '52.226-3';

UPDATE Clauses
SET clause_title = 'Notice of Disaster or Emergency Area Set-Aside' -- 'Notice of Disaster or Emergency Area Set-Aside.'
WHERE clause_version_id = 14 AND clause_name = '52.226-4';

UPDATE Clauses
SET clause_title = 'Restrictions on Subcontracting Outside Disaster or Emergency Area' -- 'Restrictions on Subcontracting Outside Disaster or Emergency Area.'
WHERE clause_version_id = 14 AND clause_name = '52.226-5';

UPDATE Clauses
SET clause_title = 'Promoting Excess Food Donation to Nonprofit Organizations' -- 'Promoting Excess Food Donation to Nonprofit Organizations.'
WHERE clause_version_id = 14 AND clause_name = '52.226-6';

UPDATE Clauses
SET clause_title = 'Authorization and Consent' -- 'Authorization and Consent.'
WHERE clause_version_id = 14 AND clause_name = '52.227-1';

UPDATE Clauses
SET clause_title = 'Filing of Patent Applications - Classified Subject Matter' -- 'Filing of Patent Applications-Classified Subject Matter.'
WHERE clause_version_id = 14 AND clause_name = '52.227-10';

UPDATE Clauses
SET clause_title = 'Patent Rights - Retention by the Contractor (Short Form)' -- 'Patent Rights--Retention by the Contractor (Short Form).'
WHERE clause_version_id = 14 AND clause_name = '52.227-11';

UPDATE Clauses
SET clause_title = 'Patent Rights- Acquisition by the Government' -- 'Patent Rights- Acquisition by the Government.'
WHERE clause_version_id = 14 AND clause_name = '52.227-13';

UPDATE Clauses
SET clause_title = 'Rights in Data - General' -- 'Rights in Data-General.'
WHERE clause_version_id = 14 AND clause_name = '52.227-14';

UPDATE Clauses
SET clause_title = 'Representation of Limited Rights Data and Restricted Computer Software' -- 'Representation of Limited Rights Data and Restricted Computer Software.'
WHERE clause_version_id = 14 AND clause_name = '52.227-15';

UPDATE Clauses
SET clause_title = 'Additional Data Requirements' -- 'Additional Data Requirements.'
WHERE clause_version_id = 14 AND clause_name = '52.227-16';

UPDATE Clauses
SET clause_title = 'Rights in Data - Special Works' -- 'Rights in Data-Special Works.'
WHERE clause_version_id = 14 AND clause_name = '52.227-17';

UPDATE Clauses
SET clause_title = 'Rights in Data - Existing Works' -- 'Rights in Data-Existing Works.'
WHERE clause_version_id = 14 AND clause_name = '52.227-18';

UPDATE Clauses
SET clause_title = 'Commercial Computer Software License' -- 'Commercial Computer Software License.'
WHERE clause_version_id = 14 AND clause_name = '52.227-19';

UPDATE Clauses
SET clause_title = 'Notice and Assistance Regarding Patent and Copyright Infringement' -- 'Notice and Assistance Regarding Patent and Copyright Infringement.'
WHERE clause_version_id = 14 AND clause_name = '52.227-2';

UPDATE Clauses
SET clause_title = 'Rights in Data - Sbir Program' -- 'Rights in Data-Sbir Program.'
WHERE clause_version_id = 14 AND clause_name = '52.227-20';

UPDATE Clauses
SET clause_title = 'Technical Data Declaration, Revision, and Withholding of Payment - Major Systems' -- 'Technical Data Declaration, Revision, and Withholding of Payment-Major Systems.'
WHERE clause_version_id = 14 AND clause_name = '52.227-21';

UPDATE Clauses
SET clause_title = 'Major System - Minimum Rights' -- 'Major System- Minimum Rights.'
WHERE clause_version_id = 14 AND clause_name = '52.227-22';

UPDATE Clauses
SET clause_title = 'Rights to Proposal Data (Technical)' -- 'Rights to Proposal Data (Technical).'
WHERE clause_version_id = 14 AND clause_name = '52.227-23';

UPDATE Clauses
SET clause_title = 'Patent Indemnity' -- 'Patent Indemnity.'
WHERE clause_version_id = 14 AND clause_name = '52.227-3';

UPDATE Clauses
SET clause_title = 'Patent Indemnity - Construction Contracts' -- 'Patent Indemnity- Construction Contracts.'
WHERE clause_version_id = 14 AND clause_name = '52.227-4';

UPDATE Clauses
SET clause_title = 'Waiver of Indemnity' -- 'Waiver of Indemnity.'
WHERE clause_version_id = 14 AND clause_name = '52.227-5';

UPDATE Clauses
SET clause_title = 'Royalty Information' -- 'Royalty Information.'
WHERE clause_version_id = 14 AND clause_name = '52.227-6';

UPDATE Clauses
SET clause_title = 'Patents - Notice of Government Licensee' -- 'Patents-Notice of Government Licensee.'
WHERE clause_version_id = 14 AND clause_name = '52.227-7';

UPDATE Clauses
SET clause_title = 'Refund of Royalties' -- 'Refund of Royalties.'
WHERE clause_version_id = 14 AND clause_name = '52.227-9';

UPDATE Clauses
SET clause_title = 'Bid Guarantee' -- 'Bid Guarantee.'
WHERE clause_version_id = 14 AND clause_name = '52.228-1';

UPDATE Clauses
SET clause_title = 'Vehicular and General Public Liability Insurance' -- 'Vehicular and General Public Liability Insurance.'
WHERE clause_version_id = 14 AND clause_name = '52.228-10';

UPDATE Clauses
SET clause_title = 'Pledges of Assets' -- 'Pledges of Assets.'
WHERE clause_version_id = 14 AND clause_name = '52.228-11';

UPDATE Clauses
SET clause_title = 'Prospective Subcontractor Requests for Bonds' -- 'Prospective Subcontractor Requests for Bonds.'
WHERE clause_version_id = 14 AND clause_name = '52.228-12';

UPDATE Clauses
SET clause_title = 'Alternative Payment Protections' -- 'Alternative Payment Protections.'
WHERE clause_version_id = 14 AND clause_name = '52.228-13';

UPDATE Clauses
SET clause_title = 'Irrevocable Letter of Credit' -- 'Irrevocable Letter of Credit.'
WHERE clause_version_id = 14 AND clause_name = '52.228-14';

UPDATE Clauses
SET clause_title = 'Performance and Payment Bonds - Construction' -- 'Performance and Payment Bonds- Construction.'
WHERE clause_version_id = 14 AND clause_name = '52.228-15';

UPDATE Clauses
SET clause_title = 'Performance and Payment Bonds - Other Than Construction' -- 'Performance and Payment Bonds-Other Than Construction.'
WHERE clause_version_id = 14 AND clause_name = '52.228-16';

UPDATE Clauses
SET clause_title = 'Additional Bond Security' -- 'Additional Bond Security.'
WHERE clause_version_id = 14 AND clause_name = '52.228-2';

UPDATE Clauses
SET clause_title = 'Workers'' Compensation Insurance (Defense Base Act)' -- 'Workers'' Compensation Insurance (Defense Base Act).'
WHERE clause_version_id = 14 AND clause_name = '52.228-3';

UPDATE Clauses
SET clause_title = 'Workers'' Compensation and War-Hazard Insurance Overseas' -- 'Workers'' Compensation and War-Hazard Insurance Overseas.'
WHERE clause_version_id = 14 AND clause_name = '52.228-4';

UPDATE Clauses
SET clause_title = 'Insurance - Work on a Government Installation' -- 'Insurance-Work on a Government Installation.'
WHERE clause_version_id = 14 AND clause_name = '52.228-5';

UPDATE Clauses
SET clause_title = 'Insurance - Liability to Third Persons' -- 'Insurance-Liability to Third Persons.'
WHERE clause_version_id = 14 AND clause_name = '52.228-7';

UPDATE Clauses
SET clause_title = 'Liability and Insurance - Leased Motor Vehicles' -- 'Liability and Insurance- Leased Motor Vehicles.'
WHERE clause_version_id = 14 AND clause_name = '52.228-8';

UPDATE Clauses
SET clause_title = 'Cargo Insurance' -- 'Cargo Insurance.'
WHERE clause_version_id = 14 AND clause_name = '52.228-9';

UPDATE Clauses
SET clause_title = 'State and Local Taxes' -- 'State and Local Taxes.'
WHERE clause_version_id = 14 AND clause_name = '52.229-1';

UPDATE Clauses
SET clause_title = 'State of New Mexico Gross Receipts and Compensating Tax' -- 'State of New Mexico Gross Receipts and Compensating Tax.'
WHERE clause_version_id = 14 AND clause_name = '52.229-10';

UPDATE Clauses
SET clause_title = 'North Carolina State and Local Sales and Use Tax' -- 'North Carolina State and Local Sales and Use Tax.'
WHERE clause_version_id = 14 AND clause_name = '52.229-2';

UPDATE Clauses
SET clause_title = 'Federal, State, and Local Taxes' -- 'Federal, State, and Local Taxes.'
WHERE clause_version_id = 14 AND clause_name = '52.229-3';

UPDATE Clauses
SET clause_title = 'Federal, State, and Local Taxes (State and Local Adjustments)' -- 'Federal, State, and Local Taxes (State and Local Adjustments).'
WHERE clause_version_id = 14 AND clause_name = '52.229-4';

UPDATE Clauses
SET clause_title = 'Taxes - Foreign Fixed - Price Contracts' -- 'Taxes-Foreign Fixed-Price Contracts.'
WHERE clause_version_id = 14 AND clause_name = '52.229-6';

UPDATE Clauses
SET clause_title = 'Taxes - Fixed - Price Contracts With Foreign Governments' -- 'Taxes-Fixed-Price Contracts With Foreign Governments.'
WHERE clause_version_id = 14 AND clause_name = '52.229-7';

UPDATE Clauses
SET clause_title = 'Taxes - Foreign Cost - Reimbursement Contracts' -- 'Taxes-Foreign Cost-Reimbursement Contracts.'
WHERE clause_version_id = 14 AND clause_name = '52.229-8';

UPDATE Clauses
SET clause_title = 'Taxes - Cost Reimbursement Contracts With Foreign Governments' -- 'Taxes-Cost Reimbursement Contracts With Foreign Governments.'
WHERE clause_version_id = 14 AND clause_name = '52.229-9';

UPDATE Clauses
SET clause_title = 'Cost Accounting Standards Notices and Certification' -- 'Cost Accounting Standards Notices and Certification.'
WHERE clause_version_id = 14 AND clause_name = '52.230-1';

UPDATE Clauses
SET clause_title = 'Cost Accounting Standards' -- 'Cost Accounting Standards.'
WHERE clause_version_id = 14 AND clause_name = '52.230-2';

UPDATE Clauses
SET clause_title = 'Disclosure and Consistency of Cost Accounting Practices' -- 'Disclosure and Consistency of Cost Accounting Practices.'
WHERE clause_version_id = 14 AND clause_name = '52.230-3';

UPDATE Clauses
SET clause_title = 'Disclosure and Consistency of Cost Accounting Practices - Foreign Concerns' -- 'Disclosure and Consistency of Cost Accounting Practices--Foreign Concerns.'
WHERE clause_version_id = 14 AND clause_name = '52.230-4';

UPDATE Clauses
SET clause_title = 'Cost Accounting Standards - Educational Institution' -- 'Cost Accounting Standards-- Educational Institution.'
WHERE clause_version_id = 14 AND clause_name = '52.230-5';

UPDATE Clauses
SET clause_title = 'Administration of Cost Accounting Standards' -- 'Administration of Cost Accounting Standards.'
WHERE clause_version_id = 14 AND clause_name = '52.230-6';

UPDATE Clauses
SET clause_title = 'Proposal Disclosure - Cost Accounting Practice Changes' -- 'Proposal Disclosure--Cost Accounting Practice Changes.'
WHERE clause_version_id = 14 AND clause_name = '52.230-7';

UPDATE Clauses
SET clause_title = 'Payments' -- 'Payments.'
WHERE clause_version_id = 14 AND clause_name = '52.232-1';

UPDATE Clauses
SET clause_title = 'Payments Under Fixed - Price Architect - Engineering Contracts' -- 'Payments Under Fixed- Price Architect- Engineering Contracts.'
WHERE clause_version_id = 14 AND clause_name = '52.232-10';

UPDATE Clauses
SET clause_title = 'Extras' -- 'Extras.'
WHERE clause_version_id = 14 AND clause_name = '52.232-11';

UPDATE Clauses
SET clause_title = 'Advance Payments' -- 'Advance Payments.'
WHERE clause_version_id = 14 AND clause_name = '52.232-12';

UPDATE Clauses
SET clause_title = 'Notice of Progress Payments' -- 'Notice of Progress Payments.'
WHERE clause_version_id = 14 AND clause_name = '52.232-13';

UPDATE Clauses
SET clause_title = 'Notice of Availability of Progress Payments Exclusively for Small Business Concerns' -- 'Notice of Availability of Progress Payments Exclusively for Small Business Concerns.'
WHERE clause_version_id = 14 AND clause_name = '52.232-14';

UPDATE Clauses
SET clause_title = 'Progress Payments Not Included' -- 'Progress Payments Not Included.'
WHERE clause_version_id = 14 AND clause_name = '52.232-15';

UPDATE Clauses
SET clause_title = 'Progress Payments' -- 'Progress Payments.'
WHERE clause_version_id = 14 AND clause_name = '52.232-16';

UPDATE Clauses
SET clause_title = 'Interest' -- 'Interest.'
WHERE clause_version_id = 14 AND clause_name = '52.232-17';

UPDATE Clauses
SET clause_title = 'Availability of Funds' -- 'Availability of Funds.'
WHERE clause_version_id = 14 AND clause_name = '52.232-18';

UPDATE Clauses
SET clause_title = 'Availability of Funds for the Next Fiscal Year' -- 'Availability of Funds for the Next Fiscal Year.'
WHERE clause_version_id = 14 AND clause_name = '52.232-19';

UPDATE Clauses
SET clause_title = 'Payments Under Fixed - Price Research and Development Contracts' -- 'Payments Under Fixed- Price Research and Development Contracts.'
WHERE clause_version_id = 14 AND clause_name = '52.232-2';

UPDATE Clauses
SET clause_title = 'Limitation of Cost' -- 'Limitation of Cost.'
WHERE clause_version_id = 14 AND clause_name = '52.232-20';

UPDATE Clauses
SET clause_title = 'Limitation of Funds' -- 'Limitation of Funds.'
WHERE clause_version_id = 14 AND clause_name = '52.232-22';

UPDATE Clauses
SET clause_title = 'Assignment of Claims' -- 'Assignment of Claims.'
WHERE clause_version_id = 14 AND clause_name = '52.232-23';

UPDATE Clauses
SET clause_title = 'Prohibition of Assignment of Claims' -- 'Prohibition of Assignment of Claims.'
WHERE clause_version_id = 14 AND clause_name = '52.232-24';

UPDATE Clauses
SET clause_title = 'Prompt Payment' -- 'Prompt Payment.'
WHERE clause_version_id = 14 AND clause_name = '52.232-25';

UPDATE Clauses
SET clause_title = 'Prompt Payment for Fixed-Price Architect-Engineer Contracts' -- 'Prompt Payment for Fixed-Price Architect-Engineer Contracts.'
WHERE clause_version_id = 14 AND clause_name = '52.232-26';

UPDATE Clauses
SET clause_title = 'Prompt Payment for Construction Contracts' -- 'Prompt Payment for Construction Contracts.'
WHERE clause_version_id = 14 AND clause_name = '52.232-27';

UPDATE Clauses
SET clause_title = 'Invitation to Propose Performance-Based Payments' -- 'Invitation to Propose Performance-Based Payments.'
WHERE clause_version_id = 14 AND clause_name = '52.232-28';

UPDATE Clauses
SET clause_title = 'Terms for Financing of Purchase of Commercial Items' -- 'Terms for Financing of Purchase of Commercial Items.'
WHERE clause_version_id = 14 AND clause_name = '52.232-29';

UPDATE Clauses
SET clause_title = 'Payments Under Personal Services Contracts' -- 'Payments Under Personal Services Contracts.'
WHERE clause_version_id = 14 AND clause_name = '52.232-3';

UPDATE Clauses
SET clause_title = 'Installment Payments for Commercial Items' -- 'Installment Payments for Commercial Items.'
WHERE clause_version_id = 14 AND clause_name = '52.232-30';

UPDATE Clauses
SET clause_title = 'Invitation to Propose Financing Terms' -- 'Invitation to Propose Financing Terms.'
WHERE clause_version_id = 14 AND clause_name = '52.232-31';

UPDATE Clauses
SET clause_title = 'Performance-Based Payments' -- 'Performance-Based Payments.'
WHERE clause_version_id = 14 AND clause_name = '52.232-32';

UPDATE Clauses
SET clause_title = 'Payment by Electronic Funds Transfer - System for Award Management' -- 'Payment by Electronic Funds Transfer-System for Award Management.'
WHERE clause_version_id = 14 AND clause_name = '52.232-33';

UPDATE Clauses
SET clause_title = 'Payment by Electronic Funds Transfer - Other Than System for Award Management' -- 'Payment by Electronic Funds Transfer--Other Than System for Award Management.'
WHERE clause_version_id = 14 AND clause_name = '52.232-34';

UPDATE Clauses
SET clause_title = 'Designation of Office for Government Receipt of Electronic Funds Transfer Information' -- 'Designation of Office for Government Receipt of Electronic Funds Transfer Information.'
WHERE clause_version_id = 14 AND clause_name = '52.232-35';

UPDATE Clauses
SET clause_title = 'Payment by Third Party' -- 'Payment by Third Party.'
WHERE clause_version_id = 14 AND clause_name = '52.232-36';

UPDATE Clauses
SET clause_title = 'Multiple Payment Arrangements' -- 'Multiple Payment Arrangements.'
WHERE clause_version_id = 14 AND clause_name = '52.232-37';

UPDATE Clauses
SET clause_title = 'Submission of Electronic Funds Transfer Information With Offer' -- 'Submission of Electronic Funds Transfer Information With Offer.'
WHERE clause_version_id = 14 AND clause_name = '52.232-38';

UPDATE Clauses
SET clause_title = 'Unenforceabilityof Unauthorized Obligations' -- 'Unenforceabilityof Unauthorized Obligations.'
WHERE clause_version_id = 14 AND clause_name = '52.232-39';

UPDATE Clauses
SET clause_title = 'Payments Under Transportation Contracts and Transportation - Related Services Contracts' -- 'Payments Under Transportation Contracts and Transportation-Related Services Contracts.'
WHERE clause_version_id = 14 AND clause_name = '52.232-4';

UPDATE Clauses
SET clause_title = 'Providing Accelerated Payments to Small Business Subcontractors' -- 'Providing Accelerated Payments to Small Business Subcontractors.'
WHERE clause_version_id = 14 AND clause_name = '52.232-40';

UPDATE Clauses
SET clause_title = 'Payments Under Fixed - Price Construction Contracts' -- 'Payments Under Fixed-Price Construction Contracts.'
WHERE clause_version_id = 14 AND clause_name = '52.232-5';

UPDATE Clauses
SET clause_title = 'Payment Under Communication Service Contracts With Common Carriers' -- 'Payment Under Communication Service Contracts With Common Carriers.'
WHERE clause_version_id = 14 AND clause_name = '52.232-6';

UPDATE Clauses
SET clause_title = 'Payments Under Time-And-Materials and Labor-Hour Contracts' -- 'Payments Under Time-And- Materials and Labor-Hour Contracts.'
WHERE clause_version_id = 14 AND clause_name = '52.232-7';

UPDATE Clauses
SET clause_title = 'Discounts for Prompt Payment' -- 'Discounts for Prompt Payment.'
WHERE clause_version_id = 14 AND clause_name = '52.232-8';

UPDATE Clauses
SET clause_title = 'Limitation on Withholding of Payments' -- 'Limitation on Withholding of Payments.'
WHERE clause_version_id = 14 AND clause_name = '52.232-9';

UPDATE Clauses
SET clause_title = 'Disputes' -- 'Disputes.'
WHERE clause_version_id = 14 AND clause_name = '52.233-1';

UPDATE Clauses
SET clause_title = 'Service of Protest' -- 'Service of Protest.'
WHERE clause_version_id = 14 AND clause_name = '52.233-2';

UPDATE Clauses
SET clause_title = 'Protest After Award' -- 'Protest After Award.'
WHERE clause_version_id = 14 AND clause_name = '52.233-3';

UPDATE Clauses
SET clause_title = 'Applicable Law for Breach of Contract Claim' -- 'Applicable Law for Breach of Contract Claim.'
WHERE clause_version_id = 14 AND clause_name = '52.233-4';

UPDATE Clauses
SET clause_title = 'Industrial Resources Developed Under Defense Production Act, Title III' -- 'Industrial Resources Developed Under Defense Production Act, Title III.'
WHERE clause_version_id = 14 AND clause_name = '52.234-1';

UPDATE Clauses
SET clause_title = 'Notice of Earned Value Management System - Pre-Award IBR' -- 'Notice of Earned Value Management System - Pre-Award Ibr.'
WHERE clause_version_id = 14 AND clause_name = '52.234-2';

UPDATE Clauses
SET clause_title = 'Notice of Earned Value Management System - Post-Award IBR' -- 'Notice of Earned Value Management System - Post-Award Ibr.'
WHERE clause_version_id = 14 AND clause_name = '52.234-3';

UPDATE Clauses
SET clause_title = 'Earned Value Management System' -- 'Earned Value Management System.'
WHERE clause_version_id = 14 AND clause_name = '52.234-4';

UPDATE Clauses
SET clause_title = 'Performance of Work by the Contractor' -- 'Performance of Work by the Contractor.'
WHERE clause_version_id = 14 AND clause_name = '52.236-1';

UPDATE Clauses
SET clause_title = 'Operations and Storage Areas' -- 'Operations and Storage Areas.'
WHERE clause_version_id = 14 AND clause_name = '52.236-10';

UPDATE Clauses
SET clause_title = 'Use & Possession Prior to Completion' -- 'Use & Possession Prior to Completion.'
WHERE clause_version_id = 14 AND clause_name = '52.236-11';

UPDATE Clauses
SET clause_title = 'Cleaning Up' -- 'Cleaning Up.'
WHERE clause_version_id = 14 AND clause_name = '52.236-12';

UPDATE Clauses
SET clause_title = 'Accident Prevention' -- 'Accident Prevention.'
WHERE clause_version_id = 14 AND clause_name = '52.236-13';

UPDATE Clauses
SET clause_title = 'Availability and Use of Utility Services' -- 'Availability and Use of Utility Services.'
WHERE clause_version_id = 14 AND clause_name = '52.236-14';

UPDATE Clauses
SET clause_title = 'Schedules for Construction Contracts' -- 'Schedules for Construction Contracts.'
WHERE clause_version_id = 14 AND clause_name = '52.236-15';

UPDATE Clauses
SET clause_title = 'Quantity Surveys' -- 'Quantity Surveys.'
WHERE clause_version_id = 14 AND clause_name = '52.236-16';

UPDATE Clauses
SET clause_title = 'Layout of Work' -- 'Layout of Work.'
WHERE clause_version_id = 14 AND clause_name = '52.236-17';

UPDATE Clauses
SET clause_title = 'Work Oversight in Cost - Reimbursement Construction Contracts' -- 'Work Oversight in Cost-Reimbursement Construction Contracts.'
WHERE clause_version_id = 14 AND clause_name = '52.236-18';

UPDATE Clauses
SET clause_title = 'Organization and Direction of the Work' -- 'Organization and Direction of the Work.'
WHERE clause_version_id = 14 AND clause_name = '52.236-19';

UPDATE Clauses
SET clause_title = 'Differing Site Conditions' -- 'Differing Site Conditions.'
WHERE clause_version_id = 14 AND clause_name = '52.236-2';

UPDATE Clauses
SET clause_title = 'Specifications and Drawings for Construction' -- 'Specifications and Drawings for Construction.'
WHERE clause_version_id = 14 AND clause_name = '52.236-21';

UPDATE Clauses
SET clause_title = 'Design Within Funding Limitations' -- 'Design Within Funding Limitations.'
WHERE clause_version_id = 14 AND clause_name = '52.236-22';

UPDATE Clauses
SET clause_title = 'Responsibility of the Architect - Engineering Contractor' -- 'Responsibility of the Architect- Engineering Contractor.'
WHERE clause_version_id = 14 AND clause_name = '52.236-23';

UPDATE Clauses
SET clause_title = 'Work Oversight in Architect - Engineering Contracts' -- 'Work Oversight in Architect- Engineering Contracts.'
WHERE clause_version_id = 14 AND clause_name = '52.236-24';

UPDATE Clauses
SET clause_title = 'Requirements for Registration of Designers' -- 'Requirements for Registration of Designers.'
WHERE clause_version_id = 14 AND clause_name = '52.236-25';

UPDATE Clauses
SET clause_title = 'Preconstruction Conference' -- 'Preconstruction Conference.'
WHERE clause_version_id = 14 AND clause_name = '52.236-26';

UPDATE Clauses
SET clause_title = 'Site Visit (Construction)' -- 'Site Visit (Construction).'
WHERE clause_version_id = 14 AND clause_name = '52.236-27';

UPDATE Clauses
SET clause_title = 'Preparation of Proposal - Construction' -- 'Preparation of Proposal-Construction.'
WHERE clause_version_id = 14 AND clause_name = '52.236-28';

UPDATE Clauses
SET clause_title = 'Site Investigation and Conditions Affecting the Work' -- 'Site Investigation and Conditions Affecting the Work.'
WHERE clause_version_id = 14 AND clause_name = '52.236-3';

UPDATE Clauses
SET clause_title = 'Physical Data' -- 'Physical Data.'
WHERE clause_version_id = 14 AND clause_name = '52.236-4';

UPDATE Clauses
SET clause_title = 'Material and Workmanship' -- 'Material and Workmanship.'
WHERE clause_version_id = 14 AND clause_name = '52.236-5';

UPDATE Clauses
SET clause_title = 'Superintendence by the Contractor' -- 'Superintendence by the Contractor.'
WHERE clause_version_id = 14 AND clause_name = '52.236-6';

UPDATE Clauses
SET clause_title = 'Permits and Responsibilities' -- 'Permits and Responsibilities.'
WHERE clause_version_id = 14 AND clause_name = '52.236-7';

UPDATE Clauses
SET clause_title = 'Other Contracts' -- 'Other Contracts.'
WHERE clause_version_id = 14 AND clause_name = '52.236-8';

UPDATE Clauses
SET clause_title = 'Protection of Existing Vegetation, Structures, Equipment, Utilities, and Improvements' -- 'Protection of Existing Vegetation, Structures, Equipment, Utilities, and Improvements.'
WHERE clause_version_id = 14 AND clause_name = '52.236-9';

UPDATE Clauses
SET clause_title = 'Site Visit' -- 'Site Visit.'
WHERE clause_version_id = 14 AND clause_name = '52.237-1';

UPDATE Clauses
SET clause_title = 'Identification of Uncompensated Overtime' -- 'Identification of Uncompensated Overtime.'
WHERE clause_version_id = 14 AND clause_name = '52.237-10';

UPDATE Clauses
SET clause_title = 'Accepting and Dispensing of 1 Coin' -- 'Accepting and Dispensing of 1 Coin.'
WHERE clause_version_id = 14 AND clause_name = '52.237-11';

UPDATE Clauses
SET clause_title = 'Protection of Government Buildings, Equipment, and Vegetation' -- 'Protection of Government Buildings, Equipment, and Vegetation.'
WHERE clause_version_id = 14 AND clause_name = '52.237-2';

UPDATE Clauses
SET clause_title = 'Continuity of Services' -- 'Continuity of Services.'
WHERE clause_version_id = 14 AND clause_name = '52.237-3';

UPDATE Clauses
SET clause_title = 'Payment by Government to Contractor' -- 'Payment by Government to Contractor.'
WHERE clause_version_id = 14 AND clause_name = '52.237-4';

UPDATE Clauses
SET clause_title = 'Payment by Contractor to Government' -- 'Payment by Contractor to Government.'
WHERE clause_version_id = 14 AND clause_name = '52.237-5';

UPDATE Clauses
SET clause_title = 'Incremental Payment by Contractor to Government' -- 'Incremental Payment by Contractor to Government.'
WHERE clause_version_id = 14 AND clause_name = '52.237-6';

UPDATE Clauses
SET clause_title = 'Indemnification and Medical Liability Insurance' -- 'Indemnification and Medical Liability Insurance.'
WHERE clause_version_id = 14 AND clause_name = '52.237-7';

UPDATE Clauses
SET clause_title = 'Restriction on Severance Payments to Foreign Nationals' -- 'Restriction on Severance Payments to Foreign Nationals.'
WHERE clause_version_id = 14 AND clause_name = '52.237-8';

UPDATE Clauses
SET clause_title = 'Waiver of Limitation on Severance Payments to Foreign Nationals' -- 'Waiver of Limitation on Severance Payments to Foreign Nationals.'
WHERE clause_version_id = 14 AND clause_name = '52.237-9';

UPDATE Clauses
SET clause_title = 'Privacy or Security Safeguards' -- 'Privacy or Security Safeguards.'
WHERE clause_version_id = 14 AND clause_name = '52.239-1';

UPDATE Clauses
SET clause_title = 'Electric Service Territory Compliance Representation' -- 'Electric Service Territory Compliance Representation.'
WHERE clause_version_id = 14 AND clause_name = '52.241-1';

UPDATE Clauses
SET clause_title = 'Termination Liability' -- 'Termination Liability.'
WHERE clause_version_id = 14 AND clause_name = '52.241-10';

UPDATE Clauses
SET clause_title = 'Multiple Service Locations' -- 'Multiple Service Locations.'
WHERE clause_version_id = 14 AND clause_name = '52.241-11';

UPDATE Clauses
SET clause_title = 'Nonrefundable, Nonrecurring Service Charge' -- 'Nonrefundable, Nonrecurring Service Charge.'
WHERE clause_version_id = 14 AND clause_name = '52.241-12';

UPDATE Clauses
SET clause_title = 'Capital Credits' -- 'Capital Credits.'
WHERE clause_version_id = 14 AND clause_name = '52.241-13';

UPDATE Clauses
SET clause_title = 'Order of Precedents - Utilities' -- 'Order of Precedents- Utilities.'
WHERE clause_version_id = 14 AND clause_name = '52.241-2';

UPDATE Clauses
SET clause_title = 'Scope of Duration of Contract' -- 'Scope of Duration of Contract.'
WHERE clause_version_id = 14 AND clause_name = '52.241-3';

UPDATE Clauses
SET clause_title = 'Change in Class of Service' -- 'Change in Class of Service.'
WHERE clause_version_id = 14 AND clause_name = '52.241-4';

UPDATE Clauses
SET clause_title = 'Contractor''s Facilities' -- 'Contractor''s Facilities.'
WHERE clause_version_id = 14 AND clause_name = '52.241-5';

UPDATE Clauses
SET clause_title = 'Service Provisions' -- 'Service Provisions.'
WHERE clause_version_id = 14 AND clause_name = '52.241-6';

UPDATE Clauses
SET clause_title = 'Change in Rates or Terms and Conditions of Service for Regulated Services' -- 'Change in Rates or Terms and Conditions of Service for Regulated Services.'
WHERE clause_version_id = 14 AND clause_name = '52.241-7';

UPDATE Clauses
SET clause_title = 'Change in Rates or Terms and Conditions of Service for Unregulated Services' -- 'Change in Rates or Terms and Conditions of Service for Unregulated Services.'
WHERE clause_version_id = 14 AND clause_name = '52.241-8';

UPDATE Clauses
SET clause_title = 'Connection Charge' -- 'Connection Charge.'
WHERE clause_version_id = 14 AND clause_name = '52.241-9';

UPDATE Clauses
SET clause_title = 'Notice of Intent to Disallow Costs' -- 'Notice of Intent to Disallow Costs.'
WHERE clause_version_id = 14 AND clause_name = '52.242-1';

UPDATE Clauses
SET clause_title = 'Bankruptcy' -- 'Bankruptcy.'
WHERE clause_version_id = 14 AND clause_name = '52.242-13';

UPDATE Clauses
SET clause_title = 'Suspension of Work' -- 'Suspension of Work.'
WHERE clause_version_id = 14 AND clause_name = '52.242-14';

UPDATE Clauses
SET clause_title = 'Stop-Work Order' -- 'Stop-Work Order.'
WHERE clause_version_id = 14 AND clause_name = '52.242-15';

UPDATE Clauses
SET clause_title = 'Government Delay of Work' -- 'Government Delay of Work.'
WHERE clause_version_id = 14 AND clause_name = '52.242-17';

UPDATE Clauses
SET clause_title = 'Production Progress Reports' -- 'Production Progress Reports.'
WHERE clause_version_id = 14 AND clause_name = '52.242-2';

UPDATE Clauses
SET clause_title = 'Penalties for Unallowable Costs' -- 'Penalties for Unallowable Costs.'
WHERE clause_version_id = 14 AND clause_name = '52.242-3';

UPDATE Clauses
SET clause_title = 'Certification of Final Indirect Costs' -- 'Certification of Final Indirect Costs.'
WHERE clause_version_id = 14 AND clause_name = '52.242-4';

UPDATE Clauses
SET clause_title = 'Changes - Fixed Price' -- 'Changes--Fixed Price.'
WHERE clause_version_id = 14 AND clause_name = '52.243-1';

UPDATE Clauses
SET clause_title = 'Changes - Cost Reimbursement' -- 'Changes-Cost Reimbursement.'
WHERE clause_version_id = 14 AND clause_name = '52.243-2';

UPDATE Clauses
SET clause_title = 'Changes - Time-And-Materials or Labor-Hours' -- 'Changes--Time-And-Materials or Labor-Hours.'
WHERE clause_version_id = 14 AND clause_name = '52.243-3';

UPDATE Clauses
SET clause_title = 'Changes' -- 'Changes.'
WHERE clause_version_id = 14 AND clause_name = '52.243-4';

UPDATE Clauses
SET clause_title = 'Changes and Changed Conditions' -- 'Changes and Changed Conditions.'
WHERE clause_version_id = 14 AND clause_name = '52.243-5';

UPDATE Clauses
SET clause_title = 'Change Order Accounting' -- 'Change Order Accounting.'
WHERE clause_version_id = 14 AND clause_name = '52.243-6';

UPDATE Clauses
SET clause_title = 'Notification of Changes' -- 'Notification of Changes.'
WHERE clause_version_id = 14 AND clause_name = '52.243-7';

UPDATE Clauses
SET clause_title = 'Subcontracts' -- 'Subcontracts.'
WHERE clause_version_id = 14 AND clause_name = '52.244-2';

UPDATE Clauses
SET clause_title = 'Subcontractors and Outside Associates and Consultants (Architect-Engineer Services)' -- 'Subcontractors and Outside Associates and Consultants (Architect-Engineer Services).'
WHERE clause_version_id = 14 AND clause_name = '52.244-4';

UPDATE Clauses
SET clause_title = 'Competition in Subcontracting' -- 'Competition in Subcontracting.'
WHERE clause_version_id = 14 AND clause_name = '52.244-5';

UPDATE Clauses
SET clause_title = 'Subcontracts for Commercial Items' -- 'Subcontracts for Commercial Items.'
WHERE clause_version_id = 14 AND clause_name = '52.244-6';

UPDATE Clauses
SET clause_title = 'Government Property' -- 'Government Property.'
WHERE clause_version_id = 14 AND clause_name = '52.245-1';

UPDATE Clauses
SET clause_title = 'Government Property Installation Operation Services' -- 'Government Property Installation Operation Services.'
WHERE clause_version_id = 14 AND clause_name = '52.245-2';

UPDATE Clauses
SET clause_title = 'Use and Charges' -- 'Use and Charges.'
WHERE clause_version_id = 14 AND clause_name = '52.245-9';

UPDATE Clauses
SET clause_title = 'Contractor Inspection Requirements' -- 'Contractor Inspection Requirements.'
WHERE clause_version_id = 14 AND clause_name = '52.246-1';

UPDATE Clauses
SET clause_title = 'Higher-Level Contract Quality Requirement' -- 'Higher-Level Contract Quality Requirement.'
WHERE clause_version_id = 14 AND clause_name = '52.246-11';

UPDATE Clauses
SET clause_title = 'Inspection of Construction' -- 'Inspection of Construction.'
WHERE clause_version_id = 14 AND clause_name = '52.246-12';

UPDATE Clauses
SET clause_title = 'Inspection - Dismantling, Demolition, or Removal of Improvements' -- 'Inspection--Dismantling, Demolition, or Removal of Improvements.'
WHERE clause_version_id = 14 AND clause_name = '52.246-13';

UPDATE Clauses
SET clause_title = 'Inspection of Transportation' -- 'Inspection of Transportation.'
WHERE clause_version_id = 14 AND clause_name = '52.246-14';

UPDATE Clauses
SET clause_title = 'Certificate of Conformance' -- 'Certificate of Conformance.'
WHERE clause_version_id = 14 AND clause_name = '52.246-15';

UPDATE Clauses
SET clause_title = 'Responsibility for Supplies' -- 'Responsibility for Supplies.'
WHERE clause_version_id = 14 AND clause_name = '52.246-16';

UPDATE Clauses
SET clause_title = 'Warranty of Supplies of a Non-Complex Nature' -- 'Warranty of Supplies of a Non- Complex Nature.'
WHERE clause_version_id = 14 AND clause_name = '52.246-17';

UPDATE Clauses
SET clause_title = 'Warranty of Supplies of a Complex Nature' -- 'Warranty of Supplies of a Complex Nature.'
WHERE clause_version_id = 14 AND clause_name = '52.246-18';

UPDATE Clauses
SET clause_title = 'Warranty of Systems and Equipment Under Performance Specifications or Design Criteria' -- 'Warranty of Systems and Equipment Under Performance Specifications or Design Criteria.'
WHERE clause_version_id = 14 AND clause_name = '52.246-19';

UPDATE Clauses
SET clause_title = 'Inspection of Supplies - Fixed-Price' -- 'Inspection of Supplies--Fixed-Price.'
WHERE clause_version_id = 14 AND clause_name = '52.246-2';

UPDATE Clauses
SET clause_title = 'Warranty of Services' -- 'Warranty of Services.'
WHERE clause_version_id = 14 AND clause_name = '52.246-20';

UPDATE Clauses
SET clause_title = 'Warranty of Construction' -- 'Warranty of Construction.'
WHERE clause_version_id = 14 AND clause_name = '52.246-21';

UPDATE Clauses
SET clause_title = 'Limitation of Liability' -- 'Limitation of Liability.'
WHERE clause_version_id = 14 AND clause_name = '52.246-23';

UPDATE Clauses
SET clause_title = 'Limitation of Liability - High Value Items' -- 'Limitation of Liability--High Value Items.'
WHERE clause_version_id = 14 AND clause_name = '52.246-24';

UPDATE Clauses
SET clause_title = 'Limitation of Liability - Services' -- 'Limitation of Liability--Services.'
WHERE clause_version_id = 14 AND clause_name = '52.246-25';

UPDATE Clauses
SET clause_title = 'Inspection of Supplies - Cost-Reimbursement' -- 'Inspection of Supplies--Cost- Reimbursement.'
WHERE clause_version_id = 14 AND clause_name = '52.246-3';

UPDATE Clauses
SET clause_title = 'Inspection of Services - Fixed Price' -- 'Inspection of Services--Fixed Price.'
WHERE clause_version_id = 14 AND clause_name = '52.246-4';

UPDATE Clauses
SET clause_title = 'Inspection of Services - Cost-Reimbursement' -- 'Inspection of Services--Cost-Reimbursement.'
WHERE clause_version_id = 14 AND clause_name = '52.246-5';

UPDATE Clauses
SET clause_title = 'Inspection - Time-And-Material and Labor-Hour' -- 'Inspection--Time-And-Material and Labor-Hour.'
WHERE clause_version_id = 14 AND clause_name = '52.246-6';

UPDATE Clauses
SET clause_title = 'Inspection of Research and Development - Fixed Price' -- 'Inspection of Research and Development--Fixed Price.'
WHERE clause_version_id = 14 AND clause_name = '52.246-7';

UPDATE Clauses
SET clause_title = 'Inspection of Research and Development - Cost Reimbursement' -- 'Inspection of Research and Development--Cost Reimbursement.'
WHERE clause_version_id = 14 AND clause_name = '52.246-8';

UPDATE Clauses
SET clause_title = 'Inspection of Research and Development (Short Form)' -- 'Inspection of Research and Development (Short Form).'
WHERE clause_version_id = 14 AND clause_name = '52.246-9';

UPDATE Clauses
SET clause_title = 'Commercial Bill of Lading Notations' -- 'Commercial Bill of Lading Notations.'
WHERE clause_version_id = 14 AND clause_name = '52.247-1';

UPDATE Clauses
SET clause_title = 'Net Weight - General Freight' -- 'Net Weight-General Freight.'
WHERE clause_version_id = 14 AND clause_name = '52.247-10';

UPDATE Clauses
SET clause_title = 'Net Weight - Household Goods or Office Furniture' -- 'Net Weight- Household Goods or Office Furniture.'
WHERE clause_version_id = 14 AND clause_name = '52.247-11';

UPDATE Clauses
SET clause_title = 'Supervision, Labor, or Materials' -- 'Supervision, Labor, or Materials.'
WHERE clause_version_id = 14 AND clause_name = '52.247-12';

UPDATE Clauses
SET clause_title = 'Accessorial Services - Moving Contracts' -- 'Accessorial Services-Moving Contracts.'
WHERE clause_version_id = 14 AND clause_name = '52.247-13';

UPDATE Clauses
SET clause_title = 'Contractor Responsibility for Receipt of Shipment' -- 'Contractor Responsibility for Receipt of Shipment.'
WHERE clause_version_id = 14 AND clause_name = '52.247-14';

UPDATE Clauses
SET clause_title = 'Contractor Responsibility for Loading and Unloading' -- 'Contractor Responsibility for Loading and Unloading.'
WHERE clause_version_id = 14 AND clause_name = '52.247-15';

UPDATE Clauses
SET clause_title = 'Contractor Responsibility for Returning Undelivered Freight' -- 'Contractor Responsibility for Returning Undelivered Freight.'
WHERE clause_version_id = 14 AND clause_name = '52.247-16';

UPDATE Clauses
SET clause_title = 'Charges' -- 'Charges.'
WHERE clause_version_id = 14 AND clause_name = '52.247-17';

UPDATE Clauses
SET clause_title = 'Multiple Shipments' -- 'Multiple Shipments.'
WHERE clause_version_id = 14 AND clause_name = '52.247-18';

UPDATE Clauses
SET clause_title = 'Stopping in Transit for Partial Uploading' -- 'Stopping in Transit for Partial Uploading.'
WHERE clause_version_id = 14 AND clause_name = '52.247-19';

UPDATE Clauses
SET clause_title = 'Permits, Authorities, or Franchises' -- 'Permits, Authorities, or Franchises.'
WHERE clause_version_id = 14 AND clause_name = '52.247-2';

UPDATE Clauses
SET clause_title = 'Estimated Quantities or Weights for Evaluation of Offers' -- 'Estimated Quantities or Weights for Evaluation of Offers.'
WHERE clause_version_id = 14 AND clause_name = '52.247-20';

UPDATE Clauses
SET clause_title = 'Contractor Liability for Personal Injury and/or Property Damage' -- 'Contractor Liability for Personal Injury and/or Property Damage.'
WHERE clause_version_id = 14 AND clause_name = '52.247-21';

UPDATE Clauses
SET clause_title = 'Contractor Liability for Loss of and/or Damage to Freight Other Than Household Goods' -- 'Contractor Liability for Loss of and/or Damage to Freight Other Than Household Goods.'
WHERE clause_version_id = 14 AND clause_name = '52.247-22';

UPDATE Clauses
SET clause_title = 'Contractor Liability for Loss of and/or Damage to Household Goods' -- 'Contractor Liability for Loss of and/or Damage to Household Goods.'
WHERE clause_version_id = 14 AND clause_name = '52.247-23';

UPDATE Clauses
SET clause_title = 'Advance Notification by the Government' -- 'Advance Notification by the Government.'
WHERE clause_version_id = 14 AND clause_name = '52.247-24';

UPDATE Clauses
SET clause_title = 'Government-Furnished Equipment With or Without Operators' -- 'Government-Furnished Equipment With or Without Operators.'
WHERE clause_version_id = 14 AND clause_name = '52.247-25';

UPDATE Clauses
SET clause_title = 'Government Direction and Marking' -- 'Government Direction and Marking.'
WHERE clause_version_id = 14 AND clause_name = '52.247-26';

UPDATE Clauses
SET clause_title = 'Contract Not Affected by Oral Agreement' -- 'Contract Not Affected by Oral Agreement.'
WHERE clause_version_id = 14 AND clause_name = '52.247-27';

UPDATE Clauses
SET clause_title = 'Contractor''s Invoices' -- 'Contractor''s Invoices.'
WHERE clause_version_id = 14 AND clause_name = '52.247-28';

UPDATE Clauses
SET clause_title = 'FOB Origin' -- 'F.O.B. Origin.'
WHERE clause_version_id = 14 AND clause_name = '52.247-29';

UPDATE Clauses
SET clause_title = 'Capability to Perform a Contract for the Relocation of a Federal Office' -- 'Capability to Perform a Contract for the Relocation of a Federal Office.'
WHERE clause_version_id = 14 AND clause_name = '52.247-3';

UPDATE Clauses
SET clause_title = 'FOB Origin, Contractor''s Facility' -- 'F.O.B. Origin, Contractor''s Facility.'
WHERE clause_version_id = 14 AND clause_name = '52.247-30';

UPDATE Clauses
SET clause_title = 'FOB Origin, Freight Allowed' -- 'F.O.B. Origin, Freight Allowed.'
WHERE clause_version_id = 14 AND clause_name = '52.247-31';

UPDATE Clauses
SET clause_title = 'FOB Origin, Freight Prepaid' -- 'F.O.B. Origin, Freight Prepaid.'
WHERE clause_version_id = 14 AND clause_name = '52.247-32';

UPDATE Clauses
SET clause_title = 'FOB Origin, With Differentials' -- 'F.O.B. Origin, With Differentials.'
WHERE clause_version_id = 14 AND clause_name = '52.247-33';

UPDATE Clauses
SET clause_title = 'FOB Destination' -- 'F.O.B. Destination.'
WHERE clause_version_id = 14 AND clause_name = '52.247-34';

UPDATE Clauses
SET clause_title = 'FOB Destination, Within Consignee''s Premises' -- 'F.O.B. Destination, Within Consignee''s Premises.'
WHERE clause_version_id = 14 AND clause_name = '52.247-35';

UPDATE Clauses
SET clause_title = 'FAS Vessel, Port of Shipment' -- 'F.A.S. Vessel, Port of Shipment.'
WHERE clause_version_id = 14 AND clause_name = '52.247-36';

UPDATE Clauses
SET clause_title = 'FOB Vessel, Port of Shipment' -- 'F.O.B.. Vessel, Port of Shipment.'
WHERE clause_version_id = 14 AND clause_name = '52.247-37';

UPDATE Clauses
SET clause_title = 'FOB Inland Carrier, Point of Exportation' -- 'F.O.B. Inland Carrier, Point of Exportation.'
WHERE clause_version_id = 14 AND clause_name = '52.247-38';

UPDATE Clauses
SET clause_title = 'FOB Inland Point, Country of Importation' -- 'F.O.B.. Inland Point, Country of Importation.'
WHERE clause_version_id = 14 AND clause_name = '52.247-39';

UPDATE Clauses
SET clause_title = 'Inspection of Shipping and Receiving Facilities' -- 'Inspection of Shipping and Receiving Facilities.'
WHERE clause_version_id = 14 AND clause_name = '52.247-4';

UPDATE Clauses
SET clause_title = 'Ex Dock, Pier, or Warehouse, Port of Importation' -- 'Ex Dock, Pier, or Warehouse, Port of Importation.'
WHERE clause_version_id = 14 AND clause_name = '52.247-40';

UPDATE Clauses
SET clause_title = 'C&F Destination' -- 'C.&F. Destination.'
WHERE clause_version_id = 14 AND clause_name = '52.247-41';

UPDATE Clauses
SET clause_title = 'CIF Destination' -- 'C.I.F. Destination.'
WHERE clause_version_id = 14 AND clause_name = '52.247-42';

UPDATE Clauses
SET clause_title = 'FOB Designated Air Carrier''s Terminal, Point of Exportation' -- 'F.O.B. Designated Air Carrier''s Terminal, Point of Exportation.'
WHERE clause_version_id = 14 AND clause_name = '52.247-43';

UPDATE Clauses
SET clause_title = 'FOB Designated Air Carrier''s Terminal, Point of Importation' -- 'F.O.B. Designated Air Carrier''s Terminal, Point of Importation.'
WHERE clause_version_id = 14 AND clause_name = '52.247-44';

UPDATE Clauses
SET clause_title = 'FOB Origin and/or FOB Destination Evaluation' -- 'F.O.B. Origin and/or F.O.B. Destination Evaluation.'
WHERE clause_version_id = 14 AND clause_name = '52.247-45';

UPDATE Clauses
SET clause_title = 'Shipping Point(s) Used in Evaluation of FOB Origin Offers' -- 'Shipping Point(s) Used in Evaluation of F.O.B. Origin Offers.'
WHERE clause_version_id = 14 AND clause_name = '52.247-46';

UPDATE Clauses
SET clause_title = 'Evaluation - FOB Origin' -- 'Evaluation- F.O.B. Origin.'
WHERE clause_version_id = 14 AND clause_name = '52.247-47';

UPDATE Clauses
SET clause_title = 'FOB Destination - Evidence of Shipment' -- 'F.O.B. Destination--Evidence of Shipment.'
WHERE clause_version_id = 14 AND clause_name = '52.247-48';

UPDATE Clauses
SET clause_title = 'Destination Unknown' -- 'Destination Unknown.'
WHERE clause_version_id = 14 AND clause_name = '52.247-49';

UPDATE Clauses
SET clause_title = 'Familiarization With Conditions' -- 'Familiarization With Conditions.'
WHERE clause_version_id = 14 AND clause_name = '52.247-5';

UPDATE Clauses
SET clause_title = 'No Evaluation of Transportation Costs' -- 'No Evaluation of Transportation Costs.'
WHERE clause_version_id = 14 AND clause_name = '52.247-50';

UPDATE Clauses
SET clause_title = 'Evaluation of Export Offers' -- 'Evaluation of Export Offers.'
WHERE clause_version_id = 14 AND clause_name = '52.247-51';

UPDATE Clauses
SET clause_title = 'Clearance and Documentation Requirements - Shipments to DOD Air or Water Terminal Trans-Shipment Points' -- 'Clearance and Documentation Requirements--Shipments to DOD Air or Water Terminal Trans-Shipment Points.'
WHERE clause_version_id = 14 AND clause_name = '52.247-52';

UPDATE Clauses
SET clause_title = 'Freight Classification Description' -- 'Freight Classification Description.'
WHERE clause_version_id = 14 AND clause_name = '52.247-53';

UPDATE Clauses
SET clause_title = 'FOB Point for Delivery of Government - Furnished Property' -- 'F.O.B. Point for Delivery of Government- Furnished Property.'
WHERE clause_version_id = 14 AND clause_name = '52.247-55';

UPDATE Clauses
SET clause_title = 'Transit Arrangements' -- 'Transit Arrangements.'
WHERE clause_version_id = 14 AND clause_name = '52.247-56';

UPDATE Clauses
SET clause_title = 'Transportation Transit Privilege Credits' -- 'Transportation Transit Privilege Credits.'
WHERE clause_version_id = 14 AND clause_name = '52.247-57';

UPDATE Clauses
SET clause_title = 'Loading, Blocking, and Bracing of Freight Car Shipments' -- 'Loading, Blocking, and Bracing of Freight Car Shipments.'
WHERE clause_version_id = 14 AND clause_name = '52.247-58';

UPDATE Clauses
SET clause_title = 'FOB Origin-Car-Load and Truckload Shipments' -- 'F.O.B. Origin-Car-Load and Truckload Shipments.'
WHERE clause_version_id = 14 AND clause_name = '52.247-59';

UPDATE Clauses
SET clause_title = 'Financial Statement' -- 'Financial Statement.'
WHERE clause_version_id = 14 AND clause_name = '52.247-6';

UPDATE Clauses
SET clause_title = 'Guaranteed Shipping' -- 'Guaranteed Shipping.'
WHERE clause_version_id = 14 AND clause_name = '52.247-60';

UPDATE Clauses
SET clause_title = 'FOB Origin - Minimum Size of Shipments' -- 'F.O.B. Origin- Minimum Size of Shipments.'
WHERE clause_version_id = 14 AND clause_name = '52.247-61';

UPDATE Clauses
SET clause_title = 'Specific Quantities Unknown' -- 'Specific Quantities Unknown.'
WHERE clause_version_id = 14 AND clause_name = '52.247-62';

UPDATE Clauses
SET clause_title = 'Preference for US-Flag Air Carriers' -- 'Preference for U.S.-Flag Air Carriers.'
WHERE clause_version_id = 14 AND clause_name = '52.247-63';

UPDATE Clauses
SET clause_title = 'Preference for Privately Owned US-Flag Commercial Vessels' -- 'Preference for Privately Owned U.S.-Flag Commercial Vessels.'
WHERE clause_version_id = 14 AND clause_name = '52.247-64';

UPDATE Clauses
SET clause_title = 'FOB Origin, Prepaid Freight - Small Package Shipments' -- 'F.O.B. Origin, Prepaid Freight--Small Package Shipments.'
WHERE clause_version_id = 14 AND clause_name = '52.247-65';

UPDATE Clauses
SET clause_title = 'Returnable Cylinders' -- 'Returnable Cylinders.'
WHERE clause_version_id = 14 AND clause_name = '52.247-66';

UPDATE Clauses
SET clause_title = 'Submission of Transportation Documents for Audit' -- 'Submission of Transportation Documents for Audit.'
WHERE clause_version_id = 14 AND clause_name = '52.247-67';

UPDATE Clauses
SET clause_title = 'Report of Shipment (Repship)' -- 'Report of Shipment (Repship).'
WHERE clause_version_id = 14 AND clause_name = '52.247-68';

UPDATE Clauses
SET clause_title = 'Freight Excluded' -- 'Freight Excluded.'
WHERE clause_version_id = 14 AND clause_name = '52.247-7';

UPDATE Clauses
SET clause_title = 'Estimated Weights or Quantities Not Guaranteed' -- 'Estimated Weights or Quantities Not Guaranteed.'
WHERE clause_version_id = 14 AND clause_name = '52.247-8';

UPDATE Clauses
SET clause_title = 'Agreed Weight - General Freight' -- 'Agreed Weight-General Freight.'
WHERE clause_version_id = 14 AND clause_name = '52.247-9';

UPDATE Clauses
SET clause_title = 'Value Engineering' -- 'Value Engineering.'
WHERE clause_version_id = 14 AND clause_name = '52.248-1';

UPDATE Clauses
SET clause_title = 'Value Engineering Program-Architect Engineer' -- 'Value Engineering Program-Architect Engineer.'
WHERE clause_version_id = 14 AND clause_name = '52.248-2';

UPDATE Clauses
SET clause_title = 'Value Engineering - Construction' -- 'Value Engineering--Construction.'
WHERE clause_version_id = 14 AND clause_name = '52.248-3';

UPDATE Clauses
SET clause_title = 'Termination for Convenience of the Government (Fixed-Price) (Short Form)' -- 'Termination for Convenience of the Government (Fixed-Price) (Short Form).'
WHERE clause_version_id = 14 AND clause_name = '52.249-1';

UPDATE Clauses
SET clause_title = 'Default (Fixed-Price Construction)' -- 'Default (Fixed-Price Construction).'
WHERE clause_version_id = 14 AND clause_name = '52.249-10';

UPDATE Clauses
SET clause_title = 'Termination (Personal Services)' -- 'Termination (Personal Services).'
WHERE clause_version_id = 14 AND clause_name = '52.249-12';

UPDATE Clauses
SET clause_title = 'Excusable Delays' -- 'Excusable Delays.'
WHERE clause_version_id = 14 AND clause_name = '52.249-14';

UPDATE Clauses
SET clause_title = 'Termination for Convenience of the Government (Fixed-Price)' -- 'Termination for Convenience of the Government (Fixed-Price).'
WHERE clause_version_id = 14 AND clause_name = '52.249-2';

UPDATE Clauses
SET clause_title = 'Termination for Convenience of the Government (Dismantling, Demolition, or Removal of Improvements)' -- 'Termination for Convenience of the Government (Dismantling, Demolition, or Removal of Improvements).'
WHERE clause_version_id = 14 AND clause_name = '52.249-3';

UPDATE Clauses
SET clause_title = 'Termination for Convenience of the Government (Services) (Short Form)' -- 'Termination for Convenience of the Government (Services) (Short Form).'
WHERE clause_version_id = 14 AND clause_name = '52.249-4';

UPDATE Clauses
SET clause_title = 'Termination for Convenience of the Government (Educational and Other Nonprofit Institutions)' -- 'Termination for Convenience of the Government (Educational and Other Nonprofit Institutions).'
WHERE clause_version_id = 14 AND clause_name = '52.249-5';

UPDATE Clauses
SET clause_title = 'Termination (Cost- Reimbursement)' -- 'Termination (Cost- Reimbursement).'
WHERE clause_version_id = 14 AND clause_name = '52.249-6';

UPDATE Clauses
SET clause_title = 'Termination (Fixed- Price Architect- Engineer)' -- 'Termination (Fixed- Price Architect- Engineer).'
WHERE clause_version_id = 14 AND clause_name = '52.249-7';

UPDATE Clauses
SET clause_title = 'Default (Fixed- Price Supply and Service)' -- 'Default (Fixed- Price Supply and Service).'
WHERE clause_version_id = 14 AND clause_name = '52.249-8';

UPDATE Clauses
SET clause_title = 'Default (Fixed-Price Research and Development)' -- 'Default (Fixed-Price Research and Development).'
WHERE clause_version_id = 14 AND clause_name = '52.249-9';

UPDATE Clauses
SET clause_title = 'Indemnification Under Public Law 85-804' -- 'Indemnification Under Public Law 85-804.'
WHERE clause_version_id = 14 AND clause_name = '52.250-1';

UPDATE Clauses
SET clause_title = 'Safety Act Coverage Not Applicable' -- 'Safety Act Coverage Not Applicable.'
WHERE clause_version_id = 14 AND clause_name = '52.250-2';

UPDATE Clauses
SET clause_title = 'Safety Act Block Designation/Certification' -- 'Safety Act Block Designation/Certification.'
WHERE clause_version_id = 14 AND clause_name = '52.250-3';

UPDATE Clauses
SET clause_title = 'Safety Act Pre-Qualification Designation Notice' -- 'Safety Act Pre-Qualification Designation Notice.'
WHERE clause_version_id = 14 AND clause_name = '52.250-4';

UPDATE Clauses
SET clause_title = 'Safety Act - Equitable Adjustment' -- 'Safety Act-Equitable Adjustment.'
WHERE clause_version_id = 14 AND clause_name = '52.250-5';

UPDATE Clauses
SET clause_title = 'Government Supply Sources' -- 'Government Supply Sources.'
WHERE clause_version_id = 14 AND clause_name = '52.251-1';

UPDATE Clauses
SET clause_title = 'Interagency Fleet Management System Vehicle and Related Services' -- 'Interagency Fleet Management System Vehicle and Related Services.'
WHERE clause_version_id = 14 AND clause_name = '52.251-2';

UPDATE Clauses
SET clause_title = 'Solicitation Provisions Incorporated by Reference' -- 'Solicitation Provisions Incorporated by Reference.'
WHERE clause_version_id = 14 AND clause_name = '52.252-1';

UPDATE Clauses
SET clause_title = 'Clauses Incorporated by Reference' -- 'Clauses Incorporated by Reference.'
WHERE clause_version_id = 14 AND clause_name = '52.252-2';

UPDATE Clauses
SET clause_title = 'Alterations in Solicitation' -- 'Alterations in Solicitation.'
WHERE clause_version_id = 14 AND clause_name = '52.252-3';

UPDATE Clauses
SET clause_title = 'Alterations in Contract' -- 'Alterations in Contract.'
WHERE clause_version_id = 14 AND clause_name = '52.252-4';

UPDATE Clauses
SET clause_title = 'Authorized Deviations in Provisions' -- 'Authorized Deviations in Provisions.'
WHERE clause_version_id = 14 AND clause_name = '52.252-5';

UPDATE Clauses
SET clause_title = 'Authorized Deviations in Clauses' -- 'Authorized Deviations in Clauses.'
WHERE clause_version_id = 14 AND clause_name = '52.252-6';

UPDATE Clauses
SET clause_title = 'Computer Generated Forms' -- 'Computer Generated Forms.'
WHERE clause_version_id = 14 AND clause_name = '52.253-1';

UPDATE Clauses
SET clause_title = 'Requirements - (Alternate I) (Apr 2014)' -- 'Requirements.'
WHERE clause_version_id = 14 AND clause_name = '252.216-7010 Alternate I';

UPDATE Clauses
SET clause_title = 'Exercise of Option to Fulfill Foreign Military Sales Commitments - (Alternate I) (Nov 2014)' -- 'Exercise of Option to Fulfill Foreign Military Sales Commitments.'
WHERE clause_version_id = 14 AND clause_name = '252.217-7000 Alternate I';

UPDATE Clauses
SET clause_title = 'Small Business Subcontracting Plan (DOD Contracts) - (Alternate I) (Oct 2014)' -- 'Small Business Subcontracting Plan (DOD Contracts).'
WHERE clause_version_id = 14 AND clause_name = '252.219-7003 Alternate I';

UPDATE Clauses
SET clause_title = 'Small Business Subcontracting Plan (DOD Contracts) - (Deviation 2013-O0014) (Aug 2013)' -- 'Small Business Subcontracting Plan (DOD Contracts).'
WHERE clause_version_id = 14 AND clause_name = '252.219-7003 Alternate I (DEVIATION 2013-00014)';

UPDATE Clauses
SET clause_title = 'Prohibition on Storage, Treatment, and Disposal of Toxic or Hazardous Materials - (Alternate I) (Sep 2014)' -- 'Prohibition on Storage, Treatment, and Disposal of Toxic or Hazardous Materials.'
WHERE clause_version_id = 14 AND clause_name = '252.223-7006 Alternate I';

UPDATE Clauses
SET clause_title = 'Buy American - Balance of Payments Program Certificate - (Alternate I) (Nov 2014)' -- 'Buy American-Balance of Payments Program Certificate.'
WHERE clause_version_id = 14 AND clause_name = '252.225-7000 Alternate I';

UPDATE Clauses
SET clause_title = 'Buy American and Balance of Payments Program - (Alternate I) (Nov 2014)' -- 'Buy American and Balance of Payments Program.'
WHERE clause_version_id = 14 AND clause_name = '252.225-7001 Alternate I';

UPDATE Clauses
SET clause_title = 'Trade Agreements Certificate - (Alternate I) (Nov 2014)' -- 'Trade Agreements Certificate.'
WHERE clause_version_id = 14 AND clause_name = '252.225-7020 Alternate I';

UPDATE Clauses
SET clause_title = 'Trade Agreements - (Alternate II) (Oct 2015)' -- 'Trade Agreements.'
WHERE clause_version_id = 14 AND clause_name = '252.225-7021 Alternate II';

UPDATE Clauses
SET clause_title = 'Buy American - Free Trade Agreements - Balance of Payments Program Certificate - (Alternate I) (Nov 2014)' -- 'Buy American-Free Trade Agreements-Balance of Payments Program Certificate.'
WHERE clause_version_id = 14 AND clause_name = '252.225-7035 Alternate I';

UPDATE Clauses
SET clause_title = 'Buy American - Free Trade Agreements - Balance of Payments Program Certificate - (Alternate II) (Nov 2014)' -- 'Buy Buy American-Free Trade Agreements-Balance of Payments Program Certificate.'
WHERE clause_version_id = 14 AND clause_name = '252.225-7035 Alternate II';

UPDATE Clauses
SET clause_title = 'Buy American - Free Trade Agreements - Balance of Payments Program Certificate - (Alternate III) (Nov 2014)' -- 'Buy American-Free Trade Agreements-Balance of Payments Program Certificate.'
WHERE clause_version_id = 14 AND clause_name = '252.225-7035 Alternate III';

UPDATE Clauses
SET clause_title = 'Buy American - Free Trade Agreements - Balance of Payments Program Certificate - (Alternate IV) (Nov 2014)' -- 'Buy American-Free Trade Agreements-Balance of Payments Program Certificate.'
WHERE clause_version_id = 14 AND clause_name = '252.225-7035 Alternate IV';

UPDATE Clauses
SET clause_title = 'Buy American - Free Trade Agreements - Balance of Payments Program Certificate - (Alternate V) (Nov 2014)' -- 'Buy American-Free Trade Agreements-Balance of Payments Program Certificate.'
WHERE clause_version_id = 14 AND clause_name = '252.225-7035 Alternate V';

UPDATE Clauses
SET clause_title = 'Buy American - Free Trade Agreements - Balance of Payments Program - (Alternate I) (Nov 2014)' -- 'Buy American-Free Trade Agreements-Balance of Payments Program.'
WHERE clause_version_id = 14 AND clause_name = '252.225-7036 Alternate I';

UPDATE Clauses
SET clause_title = 'Buy American - Free Trade Agreements - Balance of Payments Program - (Alternate II) (Nov 2014)' -- 'Buy American-Free Trade Agreements-Balance of Payments Program.'
WHERE clause_version_id = 14 AND clause_name = '252.225-7036 Alternate II';

UPDATE Clauses
SET clause_title = 'Buy American - Free Trade Agreements - Balance of Payments Program - (Alternate III) (Nov 2014)' -- 'Buy American-Free Trade Agreements-Balance of Payments Program.'
WHERE clause_version_id = 14 AND clause_name = '252.225-7036 Alternate III';

UPDATE Clauses
SET clause_title = 'Buy American - Free Trade Agreements - Balance of Payments Program - (Alternate IV) (Nov 2014)' -- 'Buy American-Free Trade Agreements-Balance of Payments Program.'
WHERE clause_version_id = 14 AND clause_name = '252.225-7036 Alternate IV';

UPDATE Clauses
SET clause_title = 'Buy American - Free Trade Agreements - Balance of Payments Program - (Alternate V) (Nov 2014)' -- 'Buy American-Free Trade Agreements--Balance of Payments.'
WHERE clause_version_id = 14 AND clause_name = '252.225-7036 Alternate V';

UPDATE Clauses
SET clause_title = 'Balance of Payments Program - Construction Material - (Alternate I) (Nov 2014)' -- 'Balance of Payments Program-Construction Material.'
WHERE clause_version_id = 14 AND clause_name = '252.225-7044 Alternate I';

UPDATE Clauses
SET clause_title = 'Balance of Payments Program - Construction Material Under Trade Agreements - (Alternate I) (Oct 2015)' -- 'Balance of Payments Program - Construction Material Under Trade Agreements.'
WHERE clause_version_id = 14 AND clause_name = '252.225-7045 Alternate I';

UPDATE Clauses
SET clause_title = 'Balance of Payments Program - Construction Material Under Trade Agreements - (Alternate II) (Oct 2015)' -- 'Balance of Payments Program - Construction Material Under Trade Agreements.'
WHERE clause_version_id = 14 AND clause_name = '252.225-7045 Alternate II';

UPDATE Clauses
SET clause_title = 'Balance of Payments Program - Construction Material Under Trade Agreements - (Alternate III) (Oct 2015)' -- 'Balance of Payments Program - Construction Material Under Trade Agreements.'
WHERE clause_version_id = 14 AND clause_name = '252.225-7045 Alternate III';

UPDATE Clauses
SET clause_title = 'License Term - (Alternate I) (Oct 2001)' -- 'License Term.'
WHERE clause_version_id = 14 AND clause_name = '252.227-7005 Alternate I';

UPDATE Clauses
SET clause_title = 'License Term - (Alternate II) (Oct 2001)' -- 'License Term.'
WHERE clause_version_id = 14 AND clause_name = '252.227-7005 Alternate II';

UPDATE Clauses
SET clause_title = 'Rights in Technical Data - Noncommercial Items - (Alternate I) (Feb 2014)' -- 'Rights in Technical Data--Noncommercial Items.'
WHERE clause_version_id = 14 AND clause_name = '252.227-7013 Alternate I';

UPDATE Clauses
SET clause_title = 'Rights in Technical Data - Noncommercial Items - (Alternate II) (Feb 2014)' -- 'Rights in Technical Data--Noncommercial Items.'
WHERE clause_version_id = 14 AND clause_name = '252.227-7013 Alternate II';

UPDATE Clauses
SET clause_title = 'Rights in Noncommercial Computer Software and Noncommercial Computer Software Documentation - (Alternate I) (Feb 2014)' -- 'Rights in Noncommercial Computer Software and Noncommercial Computer Software Documentation.'
WHERE clause_version_id = 14 AND clause_name = '252.227-7014 Alternate I';

UPDATE Clauses
SET clause_title = 'Technical DataCommercial Items - (Alternate I) (Feb 2014)' -- 'Technical DataCommercial Items.'
WHERE clause_version_id = 14 AND clause_name = '252.227-7015 Alternate I';

UPDATE Clauses
SET clause_title = 'Rights in Noncommercial Technical Data and Computer Software - Small Business Innovation Research (SBIR) Program - (Alternate I) (Feb 2014)' -- 'Rights in Noncommercial Technical Data and Computer Software--Small Business Innovation Research (SBIR) Program.'
WHERE clause_version_id = 14 AND clause_name = '252.227-7018 Alternate I';

UPDATE Clauses
SET clause_title = 'Tax Relief - (Alternate I) (Sep 2014)' -- 'Tax Relief.'
WHERE clause_version_id = 14 AND clause_name = '252.229-7001 Alternate I';

UPDATE Clauses
SET clause_title = 'Notice of Cost and Software Data Reporting System - (Alternate I) (Nov 2014)' -- 'Notice of Cost and Software Data Reporting System.'
WHERE clause_version_id = 14 AND clause_name = '252.234-7003 Alternate I';

UPDATE Clauses
SET clause_title = 'Cost and Software Data Reporting System - (Alternate I) (Nov 2014)' -- 'Cost and Software Data Reporting System.'
WHERE clause_version_id = 14 AND clause_name = '252.234-7004 Alternate I';

UPDATE Clauses
SET clause_title = 'Frequency Authorization - (Alternate I) (Mar 2014)' -- 'Frequency Authorization.'
WHERE clause_version_id = 14 AND clause_name = '252.235-7003 Alternate I';

UPDATE Clauses
SET clause_title = 'Award to Single Offeror - (Alternate I) (Nov 2014)' -- 'Award to Single Offeror.'
WHERE clause_version_id = 14 AND clause_name = '252.237-7002 Alternate I';

UPDATE Clauses
SET clause_title = 'Delivery Tickets - (Alternate I) (Nov 2014)' -- 'Delivery Tickets.'
WHERE clause_version_id = 14 AND clause_name = '252.237-7016 Alternate I';

UPDATE Clauses
SET clause_title = 'Delivery Tickets - (Alternate II) (Nov 2014)' -- 'Delivery Tickets.'
WHERE clause_version_id = 14 AND clause_name = '252.237-7016 Alternate II';

UPDATE Clauses
SET clause_title = 'Contractor Purchasing System Administration - (Alternate I) (May 2014)' -- 'Contractor Purchasing System Administration.'
WHERE clause_version_id = 14 AND clause_name = '252.244-7001 Alternate I';

UPDATE Clauses
SET clause_title = 'Warranty of Data - (Alternate I) (Mar 2014)' -- 'Warranty of Data.'
WHERE clause_version_id = 14 AND clause_name = '252.246-7001 Alternate I';

UPDATE Clauses
SET clause_title = 'Warranty of Data - (Alternate II) (Mar 2014)' -- 'Warranty of Data.'
WHERE clause_version_id = 14 AND clause_name = '252.246-7001 Alternate II';

UPDATE Clauses
SET clause_title = 'Evaluation of Bids - (Alternate I) (Apr 2014)' -- 'Evaluation of Bids.'
WHERE clause_version_id = 14 AND clause_name = '252.247-7008 Alternate I';

UPDATE Clauses
SET clause_title = 'Transportation of Supplies by Sea - (Alternate I) (Apr 2014)' -- 'Transportation of Supplies by Sea.'
WHERE clause_version_id = 14 AND clause_name = '252.247-7023 Alternate I';

UPDATE Clauses
SET clause_title = 'Transportation of Supplies by Sea - (Alternate II) (Apr 2014)' -- 'Transportation of Supplies by Sea.'
WHERE clause_version_id = 14 AND clause_name = '252.247-7023 Alternate II';

UPDATE Clauses
SET clause_title = 'Restrictions on Subcontractor Sales to the Government' -- 'Restrictions on Subcontractor Sales to the Government.'
WHERE clause_version_id = 14 AND clause_name = '52.203-6 Alternate I';

UPDATE Clauses
SET clause_title = 'Security Requirements' -- 'Security Requirements.'
WHERE clause_version_id = 14 AND clause_name = '52.204-2 Alternate I';

UPDATE Clauses
SET clause_title = 'Security Requirements' -- 'Security Requirements.'
WHERE clause_version_id = 14 AND clause_name = '52.204-2 Alternate II';

UPDATE Clauses
SET clause_title = 'System for Award Management' -- 'System for Award Management.'
WHERE clause_version_id = 14 AND clause_name = '52.204-7 Alternate I';

UPDATE Clauses
SET clause_title = 'First Article Approval - Contractor Testing' -- 'First Article Approval- Contractor Testing.'
WHERE clause_version_id = 14 AND clause_name = '52.209-3 Alternate I';

UPDATE Clauses
SET clause_title = 'First Article Approval - Contractor Testing' -- 'First Article Approval- Contractor Testing.'
WHERE clause_version_id = 14 AND clause_name = '52.209-3 Alternate II';

UPDATE Clauses
SET clause_title = 'First Article Approval - Government Testing' -- 'First Article Approval-Government Testing.'
WHERE clause_version_id = 14 AND clause_name = '52.209-4 Alternate I';

UPDATE Clauses
SET clause_title = 'First Article Approval - Government Testing' -- 'First Article Approval-Government Testing.'
WHERE clause_version_id = 14 AND clause_name = '52.209-4 Alternate II';

UPDATE Clauses
SET clause_title = 'Commencement, Prosecution, and Completion of Work' -- 'Commencement, Prosecution, and Completion of Work.'
WHERE clause_version_id = 14 AND clause_name = '52.211-10 Alternate I';

UPDATE Clauses
SET clause_title = 'Time of Delivery' -- 'Time of Delivery.'
WHERE clause_version_id = 14 AND clause_name = '52.211-8 Alternate I';

UPDATE Clauses
SET clause_title = 'Time of Delivery' -- 'Time of Delivery.'
WHERE clause_version_id = 14 AND clause_name = '52.211-8 Alternate II';

UPDATE Clauses
SET clause_title = 'Time of Delivery' -- 'Time of Delivery.'
WHERE clause_version_id = 14 AND clause_name = '52.211-8 Alternate III';

UPDATE Clauses
SET clause_title = 'Desired and Required Time of Delivery' -- 'Desired and Required Time of Delivery.'
WHERE clause_version_id = 14 AND clause_name = '52.211-9 Alternate I';

UPDATE Clauses
SET clause_title = 'Desired and Required Time of Delivery' -- 'Desired and Required Time of Delivery.'
WHERE clause_version_id = 14 AND clause_name = '52.211-9 Alternate II';

UPDATE Clauses
SET clause_title = 'Desired and Required Time of Delivery' -- 'Desired and Required Time of Delivery.'
WHERE clause_version_id = 14 AND clause_name = '52.211-9 Alternate III';

UPDATE Clauses
SET clause_title = 'Contractors Representations and Certifications - Commercial Items' -- 'Contractors Representations and Certifications-Commercial Items.'
WHERE clause_version_id = 14 AND clause_name = '52.212-3 Alternate I';

UPDATE Clauses
SET clause_title = 'Contract Terms and Conditions - Commercial Items' -- 'Contract Terms and Conditions--Commercial Items.'
WHERE clause_version_id = 14 AND clause_name = '52.212-4 Alternate I';

UPDATE Clauses
SET clause_title = 'Contract Terms and Conditions Required to Implement Statutes or Executive Orders - Commercial Items' -- 'Contract Terms and Conditions Required to Implement Statutes or Executive Orders- Commercial Items.'
WHERE clause_version_id = 14 AND clause_name = '52.212-5 Alternate I';

UPDATE Clauses
SET clause_title = 'Contract Terms and Conditions Required to Implement Statutes or Executive Orders - Commercial Items - (Alternate I) (Deviation 2013-O0019) (Sep 2013)' -- 'Contract Terms and Conditions Required to Implement Statutes or Executive Orders-Commercial Items.'
WHERE clause_version_id = 14 AND clause_name = '52.212-5 Alternate I (DEVIATION 2013-00019)';

UPDATE Clauses
SET clause_title = 'Contract Terms and Conditions Required to Implement Statutes or Executive Orders - Commercial Items' -- 'Contract Terms and Conditions Required to Implement Statutes or Executive Orders- Commercial Items.'
WHERE clause_version_id = 14 AND clause_name = '52.212-5 Alternate II';

UPDATE Clauses
SET clause_title = 'Contract Terms and Conditions Required to Implement Statutes or Executive Orders - Commercial Items - (Alternate II) (Deviation 2013-O0019) (Sep 2013)' -- 'Contract Terms and Conditions Required to Implement Statutes or Executive Orders-Commercial Items.'
WHERE clause_version_id = 14 AND clause_name = '52.212-5 Alternate II (DEVIATION 2013-00019)';

UPDATE Clauses
SET clause_title = 'Telegraphic Bids' -- 'Telegraphic Bids.'
WHERE clause_version_id = 14 AND clause_name = '52.214-13 Alternate I';

UPDATE Clauses
SET clause_title = 'Bid Samples' -- 'Bid Samples.'
WHERE clause_version_id = 14 AND clause_name = '52.214-20 Alternate I';

UPDATE Clauses
SET clause_title = 'Bid Samples' -- 'Bid Samples.'
WHERE clause_version_id = 14 AND clause_name = '52.214-20 Alternate II';

UPDATE Clauses
SET clause_title = 'Descriptive Literature' -- 'Descriptive Literature.'
WHERE clause_version_id = 14 AND clause_name = '52.214-21 Alternate I';

UPDATE Clauses
SET clause_title = 'Audit and Records - Sealed Bidding' -- 'Audit and Records--Sealed Bidding.'
WHERE clause_version_id = 14 AND clause_name = '52.214-26 Alternate I';

UPDATE Clauses
SET clause_title = 'Instructions to Contractors - Competitive' -- 'Instructions to Contractors-Competitive.'
WHERE clause_version_id = 14 AND clause_name = '52.215-1 Alternate I';

UPDATE Clauses
SET clause_title = 'Instructions to Contractors - Competitive' -- 'Instructions to Contractors-Competitive.'
WHERE clause_version_id = 14 AND clause_name = '52.215-1 Alternate II';

UPDATE Clauses
SET clause_title = 'Integrity of Unit Prices' -- 'Integrity of Unit Prices.'
WHERE clause_version_id = 14 AND clause_name = '52.215-14 Alternate I';

UPDATE Clauses
SET clause_title = 'Audit and Records - Negotiations' -- 'Audit and Records- Negotiations.'
WHERE clause_version_id = 14 AND clause_name = '52.215-2 Alternate I';

UPDATE Clauses
SET clause_title = 'Audit and Records - Negotiations' -- 'Audit and Records- Negotiations.'
WHERE clause_version_id = 14 AND clause_name = '52.215-2 Alternate II';

UPDATE Clauses
SET clause_title = 'Audit and Records - Negotiations' -- 'Audit and Records- Negotiations.'
WHERE clause_version_id = 14 AND clause_name = '52.215-2 Alternate III';

UPDATE Clauses
SET clause_title = 'Requirements for Certified Cost or Pricing Data and Data Other Than Certified Cost or Pricing Data' -- 'Requirements for Certified Cost or Pricing Data and Data Other Than Certified Cost or Pricing Data.'
WHERE clause_version_id = 14 AND clause_name = '52.215-20 Alternate I';

UPDATE Clauses
SET clause_title = 'Requirements for Certified Cost or Pricing Data and Data Other Than Certified Cost or Pricing Data' -- 'Requirements for Certified Cost or Pricing Data and Data Other Than Certified Cost or Pricing Data.'
WHERE clause_version_id = 14 AND clause_name = '52.215-20 Alternate II';

UPDATE Clauses
SET clause_title = 'Requirements for Certified Cost or Pricing Data and Data Other Than Certified Cost or Pricing Data' -- 'Requirements for Certified Cost or Pricing Data and Data Other Than Certified Cost or Pricing Data.'
WHERE clause_version_id = 14 AND clause_name = '52.215-20 Alternate III';

UPDATE Clauses
SET clause_title = 'Requirements for Certified Cost or Pricing Data and Data Other Than Certified Cost or Pricing Data' -- 'Requirements for Certified Cost or Pricing Data and Data Other Than Certified Cost or Pricing Data.'
WHERE clause_version_id = 14 AND clause_name = '52.215-20 Alternate IV';

UPDATE Clauses
SET clause_title = 'Requirements for Certified Cost or Pricing Data and Data Other Than Certified Cost or Pricing Data - Modifications' -- 'Requirements for Certified Cost or Pricing Data and Data Other Than Certified Cost or Pricing Data - Modifications.'
WHERE clause_version_id = 14 AND clause_name = '52.215-21 Alternate I';

UPDATE Clauses
SET clause_title = 'Requirements for Certified Cost or Pricing Data and Data Other Than Certified Cost or Pricing Data - Modifications' -- 'Requirements for Certified Cost or Pricing Data and Data Other Than Certified Cost or Pricing Data - Modifications.'
WHERE clause_version_id = 14 AND clause_name = '52.215-21 Alternate II';

UPDATE Clauses
SET clause_title = 'Requirements for Certified Cost or Pricing Data and Data Other Than Certified Cost or Pricing Data - Modifications' -- 'Requirements for Certified Cost or Pricing Data and Data Other Than Certified Cost or Pricing Data - Modifications.'
WHERE clause_version_id = 14 AND clause_name = '52.215-21 Alternate III';

UPDATE Clauses
SET clause_title = 'Requirements for Certified Cost or Pricing Data and Data Other Than Certified Cost or Pricing Data - Modifications' -- 'Requirements for Certified Cost or Pricing Data and Data Other Than Certified Cost or Pricing Data - Modifications.'
WHERE clause_version_id = 14 AND clause_name = '52.215-21 Alternate IV';

UPDATE Clauses
SET clause_title = 'Limitations on Pass - Through Charges' -- 'Limitations on Pass-Through Charges.'
WHERE clause_version_id = 14 AND clause_name = '52.215-23 Alternate I';

UPDATE Clauses
SET clause_title = 'Changes or Additions to Make-or-Buy Program' -- 'Changes or Additions to Make-or-Buy Program.'
WHERE clause_version_id = 14 AND clause_name = '52.215-9 Alternate I';

UPDATE Clauses
SET clause_title = 'Changes or Additions to Make-or-Buy Program' -- 'Changes or Additions to Make-or-Buy Program.'
WHERE clause_version_id = 14 AND clause_name = '52.215-9 Alternate II';

UPDATE Clauses
SET clause_title = 'Cost Contract - No Fee' -- 'Cost Contract-No Fee.'
WHERE clause_version_id = 14 AND clause_name = '52.216-11 Alternate I';

UPDATE Clauses
SET clause_title = 'Cost - Sharing Contract - No Fee' -- 'Cost-Sharing Contract-No Fee.'
WHERE clause_version_id = 14 AND clause_name = '52.216-12 Alternate I';

UPDATE Clauses
SET clause_title = 'Incentive Price Revision - Firm Target' -- 'Incentive Price Revision-Firm Target.'
WHERE clause_version_id = 14 AND clause_name = '52.216-16 Alternate I';

UPDATE Clauses
SET clause_title = 'Incentive Price Revision - Successive Targets' -- 'Incentive Price Revision-Successive Targets.'
WHERE clause_version_id = 14 AND clause_name = '52.216-17 Alternate I';

UPDATE Clauses
SET clause_title = 'Requirements' -- 'Requirements.'
WHERE clause_version_id = 14 AND clause_name = '52.216-21 Alternate I';

UPDATE Clauses
SET clause_title = 'Requirements' -- 'Requirements.'
WHERE clause_version_id = 14 AND clause_name = '52.216-21 Alternate II';

UPDATE Clauses
SET clause_title = 'Requirements' -- 'Requirements.'
WHERE clause_version_id = 14 AND clause_name = '52.216-21 Alternate III';

UPDATE Clauses
SET clause_title = 'Requirements' -- 'Requirements.'
WHERE clause_version_id = 14 AND clause_name = '52.216-21 Alternate IV';

UPDATE Clauses
SET clause_title = 'Contract Definitization' -- 'Contract Definitization.'
WHERE clause_version_id = 14 AND clause_name = '52.216-25 Alternate I';

UPDATE Clauses
SET clause_title = 'Allowable Cost and Payment' -- 'Allowable Cost and Payment.'
WHERE clause_version_id = 14 AND clause_name = '52.216-7 Alternate I';

UPDATE Clauses
SET clause_title = 'Allowable Cost and Payment' -- 'Allowable Cost and Payment.'
WHERE clause_version_id = 14 AND clause_name = '52.216-7 Alternate II';

UPDATE Clauses
SET clause_title = 'Allowable Cost and Payment' -- 'Allowable Cost and Payment.'
WHERE clause_version_id = 14 AND clause_name = '52.216-7 Alternate III';

UPDATE Clauses
SET clause_title = 'Allowable Cost and Payment' -- 'Allowable Cost and Payment.'
WHERE clause_version_id = 14 AND clause_name = '52.216-7 Alternate IV';

UPDATE Clauses
SET clause_title = 'Small Business Program Representation' -- 'Small Business Program Representation.'
WHERE clause_version_id = 14 AND clause_name = '52.219-1 Alternate I';

UPDATE Clauses
SET clause_title = 'Notification of Competition Limited to Eligible 8(a) Concerns' -- 'Notification of Competition Limited to Eligible 8(a) Concerns.'
WHERE clause_version_id = 14 AND clause_name = '52.219-18 Alternate I';

UPDATE Clauses
SET clause_title = 'Notification of Competition Limited to Eligible 8(a) Concerns' -- 'Notification of Competition Limited to Eligible 8(a) Concerns.'
WHERE clause_version_id = 14 AND clause_name = '52.219-18 Alternate II';

UPDATE Clauses
SET clause_title = 'Notice of Hubzone Set-Aside or Sole Source Award' -- 'Notice of Hubzone Set-Aside or Sole Source Award.'
WHERE clause_version_id = 14 AND clause_name = '52.219-3 Alternate I';

UPDATE Clauses
SET clause_title = 'Notice of Price Evaluation Preference for Hubzone Small Business Concerns' -- 'Notice of Price Evaluation Preference for Hubzone Small Business Concerns.'
WHERE clause_version_id = 14 AND clause_name = '52.219-4 Alternate I';

UPDATE Clauses
SET clause_title = 'Notice of Total Small Business Set-Aside' -- 'Notice of Total Small Business Set-Aside.'
WHERE clause_version_id = 14 AND clause_name = '52.219-6 Alternate I';

UPDATE Clauses
SET clause_title = 'Notice of Total Small Business Set-Aside' -- 'Notice of Total Small Business Set-Aside.'
WHERE clause_version_id = 14 AND clause_name = '52.219-6 Alternate II';

UPDATE Clauses
SET clause_title = 'Notice of Partial Small Business Set-Aside' -- 'Notice of Partial Small Business Set-Aside.'
WHERE clause_version_id = 14 AND clause_name = '52.219-7 Alternate I';

UPDATE Clauses
SET clause_title = 'Notice of Partial Small Business Set-Aside' -- 'Notice of Partial Small Business Set-Aside.'
WHERE clause_version_id = 14 AND clause_name = '52.219-7 Alternate II';

UPDATE Clauses
SET clause_title = 'Small Business Subcontracting Plan' -- 'Small Business Subcontracting Plan.'
WHERE clause_version_id = 14 AND clause_name = '52.219-9 Alternate I';

UPDATE Clauses
SET clause_title = 'Small Business Subcontracting Plan - (Alternate I) (Deviation 2013-O0014) (Aug 2013)' -- 'Small Business Subcontracting Plan.'
WHERE clause_version_id = 14 AND clause_name = '52.219-9 Alternate I (DEVIATION 2013-00014)';

UPDATE Clauses
SET clause_title = 'Small Business Subcontracting Plan' -- 'Small Business Subcontracting Plan.'
WHERE clause_version_id = 14 AND clause_name = '52.219-9 Alternate II';

UPDATE Clauses
SET clause_title = 'Small Business Subcontracting Plan - (Alternate II) (Deviation 2013-O0014) (Aug 2013)' -- 'Small Business Subcontracting Plan.'
WHERE clause_version_id = 14 AND clause_name = '52.219-9 Alternate II (DEVIATION 2013-00014)';

UPDATE Clauses
SET clause_title = 'Small Business Subcontracting Plan' -- 'Small Business Subcontracting Plan.'
WHERE clause_version_id = 14 AND clause_name = '52.219-9 Alternate III';

UPDATE Clauses
SET clause_title = 'Small Business Subcontracting Plan - (Alternate III) (Deviation 2013-O0014) (Aug 2013)' -- 'Small Business Subcontracting Plan.'
WHERE clause_version_id = 14 AND clause_name = '52.219-9 Alternate III (DEVIATION 2013-00014)';

UPDATE Clauses
SET clause_title = 'Equal Opportunity' -- 'Equal Opportunity.'
WHERE clause_version_id = 14 AND clause_name = '52.222-26 Alternate I';

UPDATE Clauses
SET clause_title = 'Notice of Requirement Project Labor Agreement' -- 'Notice of Requirement Project Labor Agreement.'
WHERE clause_version_id = 14 AND clause_name = '52.222-33 Alternate I';

UPDATE Clauses
SET clause_title = 'Notice of Requirement Project Labor Agreement' -- 'Notice of Requirement Project Labor Agreement.'
WHERE clause_version_id = 14 AND clause_name = '52.222-33 Alternate II';

UPDATE Clauses
SET clause_title = 'Project Labor Agreement' -- 'Project Labor Agreement.'
WHERE clause_version_id = 14 AND clause_name = '52.222-34 Alternate I';

UPDATE Clauses
SET clause_title = 'Equal Opportunity Veterans' -- 'Equal Opportunity Veterans.'
WHERE clause_version_id = 14 AND clause_name = '52.222-35 Alternate I';

UPDATE Clauses
SET clause_title = 'Equal Opportunity for Workers With Disabilities' -- 'Equal Opportunity for Workers With Disabilities.'
WHERE clause_version_id = 14 AND clause_name = '52.222-36 Alternate I';

UPDATE Clauses
SET clause_title = 'Combating Trafficking in Persons' -- 'Combating Trafficking in Persons.'
WHERE clause_version_id = 14 AND clause_name = '52.222-50 Alternate I';

UPDATE Clauses
SET clause_title = 'Acquisition of EPEAT-Registered Imaging Equipment' -- 'Acquisition of EPEAT-Registered Imaging Equipment.'
WHERE clause_version_id = 14 AND clause_name = '52.223-13 Alternate I';

UPDATE Clauses
SET clause_title = 'Acquisition of EPEAT-Registered Televisions' -- 'Acquisition of EPEAT-Registered Televisions.'
WHERE clause_version_id = 14 AND clause_name = '52.223-14 Alternate I';

UPDATE Clauses
SET clause_title = 'Acquisition of EPEAT Registered Personal Computer Products' -- 'Acquisition of EPEAT Registered Personal Computer Products.'
WHERE clause_version_id = 14 AND clause_name = '52.223-16 Alternate I';

UPDATE Clauses
SET clause_title = 'Hazardous Material Identification and Material Safety Data' -- 'Hazardous Material Identification and Material Safety Data.'
WHERE clause_version_id = 14 AND clause_name = '52.223-3 Alternate I';

UPDATE Clauses
SET clause_title = 'Pollution Prevention and Right-To-Know Information' -- 'Pollution Prevention and Right-To-Know Information.'
WHERE clause_version_id = 14 AND clause_name = '52.223-5 Alternate I';

UPDATE Clauses
SET clause_title = 'Pollution Prevention and Right-To-Know Information' -- 'Pollution Prevention and Right-To-Know Information.'
WHERE clause_version_id = 14 AND clause_name = '52.223-5 Alternate II';

UPDATE Clauses
SET clause_title = 'Estimate of Percentage of Recovered Material Content for EPA-Designated Items' -- 'Estimate of Percentage of Recovered Material Content for EPA-Designated Items.'
WHERE clause_version_id = 14 AND clause_name = '52.223-9 Alternate I';

UPDATE Clauses
SET clause_title = 'Notice of Buy American Requirement - Construction Materials' -- 'Notice of Buy American Requirement -- Construction Materials.'
WHERE clause_version_id = 14 AND clause_name = '52.225-10 Alternate I';

UPDATE Clauses
SET clause_title = 'Buy American - Construction Materials Under Trade Agreements' -- 'Buy American -- Construction Materials Under Trade Agreements.'
WHERE clause_version_id = 14 AND clause_name = '52.225-11 Alternate I';

UPDATE Clauses
SET clause_title = 'Notice of Buy American Requirement - Construction Materials Under Trade Agreements' -- 'Notice of Buy American Requirement -- Construction Materials Under Trade Agreements.'
WHERE clause_version_id = 14 AND clause_name = '52.225-12 Alternate I';

UPDATE Clauses
SET clause_title = 'Notice of Buy American Requirement - Construction Materials Under Trade Agreements' -- 'Notice of Buy American Requirement -- Construction Materials Under Trade Agreements.'
WHERE clause_version_id = 14 AND clause_name = '52.225-12 Alternate II';

UPDATE Clauses
SET clause_title = 'Notice of Required Use of American Iron, Steel, and Manufactured Goods - Buy American Statute - Construction Materials' -- 'Notice of Required Use of American Iron, Steel, and Manufactured Goods-Buy American Statute-Construction Materials.'
WHERE clause_version_id = 14 AND clause_name = '52.225-22 Alternate I';

UPDATE Clauses
SET clause_title = 'Required Use of American Iron, Steel, and Manufactured Goods - Buy American Statute - Construction Materials Under Trade Agreements' -- 'Required Use of American Iron, Steel, and Manufactured Goods-Buy American Statute-Construction Materials Under Trade Agreements.'
WHERE clause_version_id = 14 AND clause_name = '52.225-23 Alternate I';

UPDATE Clauses
SET clause_title = 'Notice of Required Use of American Iron, Steel, and Manufactured Goods - Buy American Statute - Construction Materials Under Trade Agreements' -- 'Notice of Required Use of American Iron, Steel, and Manufactured Goods-Buy American Statute-Construction Materials Under Trade Agreements.'
WHERE clause_version_id = 14 AND clause_name = '52.225-24 Alternate I';

UPDATE Clauses
SET clause_title = 'Notice of Required Use of American Iron, Steel, and Manufactured Goods - Buy American Statute - Construction Materials Under Trade Agreements' -- 'Notice of Required Use of American Iron, Steel, and Manufactured Goods-Buy American Statute-Construction Materials Under Trade Agreements.'
WHERE clause_version_id = 14 AND clause_name = '52.225-24 Alternate II';

UPDATE Clauses
SET clause_title = 'Buy American - Free Trade Agreements - Israeli Trade Act' -- 'Buy American -- Free Trade Agreements -- Israeli Trade Act.'
WHERE clause_version_id = 14 AND clause_name = '52.225-3 Alternate I';

UPDATE Clauses
SET clause_title = 'Buy American - Free Trade Agreements - Israeli Trade Act' -- 'Buy American -- Free Trade Agreements -- Israeli Trade Act.'
WHERE clause_version_id = 14 AND clause_name = '52.225-3 Alternate II';

UPDATE Clauses
SET clause_title = 'Buy American - Free Trade Agreements - Israeli Trade Act' -- 'Buy American -- Free Trade Agreements -- Israeli Trade Act.'
WHERE clause_version_id = 14 AND clause_name = '52.225-3 Alternate III';

UPDATE Clauses
SET clause_title = 'Buy American - Free Trade Agreements - Israeli Trade Act Certificate' -- 'Buy American -- Free Trade Agreements -- Israeli Trade Act Certificate.'
WHERE clause_version_id = 14 AND clause_name = '52.225-4 Alternate I';

UPDATE Clauses
SET clause_title = 'Buy American - Free Trade Agreements - Israeli Trade Act Certificate' -- 'Buy American -- Free Trade Agreements -- Israeli Trade Act Certificate.'
WHERE clause_version_id = 14 AND clause_name = '52.225-4 Alternate II';

UPDATE Clauses
SET clause_title = 'American - Free Trade Agreements - Israeli Trade Act Certificate' -- 'American -- Free Trade Agreements -- Israeli Trade Act Certificate.'
WHERE clause_version_id = 14 AND clause_name = '52.225-4 Alternate III';

UPDATE Clauses
SET clause_title = 'Authorization and Consent' -- 'Authorization and Consent.'
WHERE clause_version_id = 14 AND clause_name = '52.227-1 Alternate I';

UPDATE Clauses
SET clause_title = 'Authorization and Consent' -- 'Authorization and Consent.'
WHERE clause_version_id = 14 AND clause_name = '52.227-1 Alternate II';

UPDATE Clauses
SET clause_title = 'Patent Rights - Retention by the Contractor (Short Form)' -- 'Patent Rights--Retention by the Contractor (Short Form).'
WHERE clause_version_id = 14 AND clause_name = '52.227-11 Alternate I';

UPDATE Clauses
SET clause_title = 'Patent Rights - Retention by the Contractor (Short Form)' -- 'Patent Rights--Retention by the Contractor (Short Form).'
WHERE clause_version_id = 14 AND clause_name = '52.227-11 Alternate II';

UPDATE Clauses
SET clause_title = 'Patent Rights - Retention by the Contractor (Short Form)' -- 'Patent Rights--Retention by the Contractor (Short Form).'
WHERE clause_version_id = 14 AND clause_name = '52.227-11 Alternate III';

UPDATE Clauses
SET clause_title = 'Patent Rights - Retention by the Contractor (Short Form)' -- 'Patent Rights--Retention by the Contractor (Short Form).'
WHERE clause_version_id = 14 AND clause_name = '52.227-11 Alternate IV';

UPDATE Clauses
SET clause_title = 'Patent Rights - Retention by the Contractor (Short Form)' -- 'Patent Rights--Retention by the Contractor (Short Form).'
WHERE clause_version_id = 14 AND clause_name = '52.227-11 Alternate V';

UPDATE Clauses
SET clause_title = 'Patent Rights- Acquisition by the Government' -- 'Patent Rights- Acquisition by the Government.'
WHERE clause_version_id = 14 AND clause_name = '52.227-13 Alternate I';

UPDATE Clauses
SET clause_title = 'Patent Rights- Acquisition by the Government' -- 'Patent Rights- Acquisition by the Government.'
WHERE clause_version_id = 14 AND clause_name = '52.227-13 Alternate II';

UPDATE Clauses
SET clause_title = 'Rights in Data - General' -- 'Rights in Data-General.'
WHERE clause_version_id = 14 AND clause_name = '52.227-14 Alternate I';

UPDATE Clauses
SET clause_title = 'Rights in Data - General' -- 'Rights in Data-General.'
WHERE clause_version_id = 14 AND clause_name = '52.227-14 Alternate II';

UPDATE Clauses
SET clause_title = 'Rights in Data - General' -- 'Rights in Data-General.'
WHERE clause_version_id = 14 AND clause_name = '52.227-14 Alternate III';

UPDATE Clauses
SET clause_title = 'Rights in Data - General' -- 'Rights in Data-General.'
WHERE clause_version_id = 14 AND clause_name = '52.227-14 Alternate IV';

UPDATE Clauses
SET clause_title = 'Rights in Data - General' -- 'Rights in Data-General.'
WHERE clause_version_id = 14 AND clause_name = '52.227-14 Alternate V';

UPDATE Clauses
SET clause_title = 'Patent Indemnity' -- 'Patent Indemnity.'
WHERE clause_version_id = 14 AND clause_name = '52.227-3 Alternate I';

UPDATE Clauses
SET clause_title = 'Patent Indemnity' -- 'Patent Indemnity.'
WHERE clause_version_id = 14 AND clause_name = '52.227-3 Alternate II';

UPDATE Clauses
SET clause_title = 'Patent Indemnity' -- 'Patent Indemnity.'
WHERE clause_version_id = 14 AND clause_name = '52.227-3 Alternate III';

UPDATE Clauses
SET clause_title = 'Patent Indemnity - Construction Contracts' -- 'Patent Indemnity- Construction Contracts.'
WHERE clause_version_id = 14 AND clause_name = '52.227-4 Alternate I';

UPDATE Clauses
SET clause_title = 'Royalty Information' -- 'Royalty Information.'
WHERE clause_version_id = 14 AND clause_name = '52.227-6 Alternate I';

UPDATE Clauses
SET clause_title = 'Performance and Payment Bonds - Other Than Construction' -- 'Performance and Payment Bonds-Other Than Construction.'
WHERE clause_version_id = 14 AND clause_name = '52.228-16 Alternate I';

UPDATE Clauses
SET clause_title = 'North Carolina State and Local Sales and Use Tax' -- 'North Carolina State and Local Sales and Use Tax.'
WHERE clause_version_id = 14 AND clause_name = '52.229-2 Alternate I';

UPDATE Clauses
SET clause_title = 'Advance Payments' -- 'Advance Payments.'
WHERE clause_version_id = 14 AND clause_name = '52.232-12 Alternate I';

UPDATE Clauses
SET clause_title = 'Advance Payments' -- 'Advance Payments.'
WHERE clause_version_id = 14 AND clause_name = '52.232-12 Alternate II';

UPDATE Clauses
SET clause_title = 'Advance Payments' -- 'Advance Payments.'
WHERE clause_version_id = 14 AND clause_name = '52.232-12 Alternate III';

UPDATE Clauses
SET clause_title = 'Advance Payments' -- 'Advance Payments.'
WHERE clause_version_id = 14 AND clause_name = '52.232-12 Alternate IV';

UPDATE Clauses
SET clause_title = 'Advance Payments' -- 'Advance Payments.'
WHERE clause_version_id = 14 AND clause_name = '52.232-12 Alternate V';

UPDATE Clauses
SET clause_title = 'Progress Payments' -- 'Progress Payments.'
WHERE clause_version_id = 14 AND clause_name = '52.232-16 Alternate I';

UPDATE Clauses
SET clause_title = 'Progress Payments' -- 'Progress Payments.'
WHERE clause_version_id = 14 AND clause_name = '52.232-16 Alternate II';

UPDATE Clauses
SET clause_title = 'Progress Payments' -- 'Progress Payments.'
WHERE clause_version_id = 14 AND clause_name = '52.232-16 Alternate III';

UPDATE Clauses
SET clause_title = 'Assignment of Claims' -- 'Assignment of Claims.'
WHERE clause_version_id = 14 AND clause_name = '52.232-23 Alternate I';

UPDATE Clauses
SET clause_title = 'Prompt Payment' -- 'Prompt Payment.'
WHERE clause_version_id = 14 AND clause_name = '52.232-25 Alternate I';

UPDATE Clauses
SET clause_title = 'Invitation to Propose Performance-Based Payments' -- 'Invitation to Propose Performance-Based Payments.'
WHERE clause_version_id = 14 AND clause_name = '52.232-28 Alternate I';

UPDATE Clauses
SET clause_title = 'Disputes' -- 'Disputes.'
WHERE clause_version_id = 14 AND clause_name = '52.233-1 Alternate I';

UPDATE Clauses
SET clause_title = 'Protest After Award' -- 'Protest After Award.'
WHERE clause_version_id = 14 AND clause_name = '52.233-3 Alternate I';

UPDATE Clauses
SET clause_title = 'Accident Prevention' -- 'Accident Prevention.'
WHERE clause_version_id = 14 AND clause_name = '52.236-13 Alternate I';

UPDATE Clauses
SET clause_title = 'Quantity Surveys' -- 'Quantity Surveys.'
WHERE clause_version_id = 14 AND clause_name = '52.236-16 Alternate I';

UPDATE Clauses
SET clause_title = 'Specifications and Drawings for Construction' -- 'Specifications and Drawings for Construction.'
WHERE clause_version_id = 14 AND clause_name = '52.236-21 Alternate I';

UPDATE Clauses
SET clause_title = 'Specifications and Drawings for Construction' -- 'Specifications and Drawings for Construction.'
WHERE clause_version_id = 14 AND clause_name = '52.236-21 Alternate II';

UPDATE Clauses
SET clause_title = 'Site Visit (Construction)' -- 'Site Visit (Construction).'
WHERE clause_version_id = 14 AND clause_name = '52.236-27 Alternate I';

UPDATE Clauses
SET clause_title = 'Payment by Government to Contractor' -- 'Payment by Government to Contractor.'
WHERE clause_version_id = 14 AND clause_name = '52.237-4 Alternate I';

UPDATE Clauses
SET clause_title = 'Connection Charge' -- 'Connection Charge.'
WHERE clause_version_id = 14 AND clause_name = '52.241-9 Alternate I';

UPDATE Clauses
SET clause_title = 'Stop-Work Order' -- 'Stop-Work Order.'
WHERE clause_version_id = 14 AND clause_name = '52.242-15 Alternate I';

UPDATE Clauses
SET clause_title = 'Changes - Fixed Price' -- 'Changes--Fixed Price.'
WHERE clause_version_id = 14 AND clause_name = '52.243-1 Alternate I';

UPDATE Clauses
SET clause_title = 'Changes - Fixed Price' -- 'Changes--Fixed Price.'
WHERE clause_version_id = 14 AND clause_name = '52.243-1 Alternate II';

UPDATE Clauses
SET clause_title = 'Changes - Fixed Price' -- 'Changes--Fixed Price.'
WHERE clause_version_id = 14 AND clause_name = '52.243-1 Alternate III';

UPDATE Clauses
SET clause_title = 'Changes - Fixed Price' -- 'Changes--Fixed Price.'
WHERE clause_version_id = 14 AND clause_name = '52.243-1 Alternate IV';

UPDATE Clauses
SET clause_title = 'Changes - Fixed Price' -- 'Changes--Fixed Price.'
WHERE clause_version_id = 14 AND clause_name = '52.243-1 Alternate V';

UPDATE Clauses
SET clause_title = 'Changes - Cost Reimbursement' -- 'Changes-Cost Reimbursement.'
WHERE clause_version_id = 14 AND clause_name = '52.243-2 Alternate I';

UPDATE Clauses
SET clause_title = 'Changes - Cost Reimbursement' -- 'Changes-Cost Reimbursement.'
WHERE clause_version_id = 14 AND clause_name = '52.243-2 Alternate II';

UPDATE Clauses
SET clause_title = 'Changes - Cost Reimbursement' -- 'Changes-Cost Reimbursement.'
WHERE clause_version_id = 14 AND clause_name = '52.243-2 Alternate III';

UPDATE Clauses
SET clause_title = 'Changes - Cost Reimbursement' -- 'Changes-Cost Reimbursement.'
WHERE clause_version_id = 14 AND clause_name = '52.243-2 Alternate V';

UPDATE Clauses
SET clause_title = 'Subcontracts' -- 'Subcontracts.'
WHERE clause_version_id = 14 AND clause_name = '52.244-2 Alternate I';

UPDATE Clauses
SET clause_title = 'Government Property' -- 'Government Property.'
WHERE clause_version_id = 14 AND clause_name = '52.245-1 Alternate I';

UPDATE Clauses
SET clause_title = 'Government Property' -- 'Government Property.'
WHERE clause_version_id = 14 AND clause_name = '52.245-1 Alternate II';

UPDATE Clauses
SET clause_title = 'Warranty of Supplies of a Non-Complex Nature' -- 'Warranty of Supplies of a Non- Complex Nature.'
WHERE clause_version_id = 14 AND clause_name = '52.246-17 Alternate II';

UPDATE Clauses
SET clause_title = 'Warranty of Supplies of a Non-Complex Nature' -- 'Warranty of Supplies of a Non- Complex Nature.'
WHERE clause_version_id = 14 AND clause_name = '52.246-17 Alternate III';

UPDATE Clauses
SET clause_title = 'Warranty of Supplies of a Non-Complex Nature' -- 'Warranty of Supplies of a Non- Complex Nature.'
WHERE clause_version_id = 14 AND clause_name = '52.246-17 Alternate IV';

UPDATE Clauses
SET clause_title = 'Warranty of Supplies of a Non-Complex Nature' -- 'Warranty of Supplies of a Non- Complex Nature.'
WHERE clause_version_id = 14 AND clause_name = '52.246-17 Alternate V';

UPDATE Clauses
SET clause_title = 'Warranty of Supplies of a Complex Nature' -- 'Warranty of Supplies of a Complex Nature.'
WHERE clause_version_id = 14 AND clause_name = '52.246-18 Alternate II';

UPDATE Clauses
SET clause_title = 'Warranty of Supplies of a Complex Nature' -- 'Warranty of Supplies of a Complex Nature.'
WHERE clause_version_id = 14 AND clause_name = '52.246-18 Alternate III';

UPDATE Clauses
SET clause_title = 'Warranty of Supplies of a Complex Nature' -- 'Warranty of Supplies of a Complex Nature.'
WHERE clause_version_id = 14 AND clause_name = '52.246-18 Alternate IV';

UPDATE Clauses
SET clause_title = 'Warranty of Systems and Equipment Under Performance Specifications or Design Criteria' -- 'Warranty of Systems and Equipment Under Performance Specifications or Design Criteria.'
WHERE clause_version_id = 14 AND clause_name = '52.246-19 Alternate I';

UPDATE Clauses
SET clause_title = 'Warranty of Systems and Equipment Under Performance Specifications or Design Criteria' -- 'Warranty of Systems and Equipment Under Performance Specifications or Design Criteria.'
WHERE clause_version_id = 14 AND clause_name = '52.246-19 Alternate II';

UPDATE Clauses
SET clause_title = 'Warranty of Systems and Equipment Under Performance Specifications or Design Criteria' -- 'Warranty of Systems and Equipment Under Performance Specifications or Design Criteria.'
WHERE clause_version_id = 14 AND clause_name = '52.246-19 Alternate III';

UPDATE Clauses
SET clause_title = 'Inspection of Supplies - Fixed-Price' -- 'Inspection of Supplies--Fixed-Price.'
WHERE clause_version_id = 14 AND clause_name = '52.246-2 Alternate I';

UPDATE Clauses
SET clause_title = 'Inspection of Supplies - Fixed-Price' -- 'Inspection of Supplies--Fixed-Price.'
WHERE clause_version_id = 14 AND clause_name = '52.246-2 Alternate II';

UPDATE Clauses
SET clause_title = 'Warranty of Construction' -- 'Warranty of Construction.'
WHERE clause_version_id = 14 AND clause_name = '52.246-21 Alternate I';

UPDATE Clauses
SET clause_title = 'Limitation of Liability - High Value Items' -- 'Limitation of Liability--High Value Items.'
WHERE clause_version_id = 14 AND clause_name = '52.246-24 Alternate I';

UPDATE Clauses
SET clause_title = 'Inspection - Time-And-Material and Labor-Hour' -- 'Inspection--Time-And-Material and Labor-Hour.'
WHERE clause_version_id = 14 AND clause_name = '52.246-6 Alternate I';

UPDATE Clauses
SET clause_title = 'Inspection of Research and Development - Cost Reimbursement' -- 'Inspection of Research and Development--Cost Reimbursement.'
WHERE clause_version_id = 14 AND clause_name = '52.246-8 Alternate I';

UPDATE Clauses
SET clause_title = 'Capability to Perform a Contract for the Relocation of a Federal Office' -- 'Capability to Perform a Contract for the Relocation of a Federal Office.'
WHERE clause_version_id = 14 AND clause_name = '52.247-3 Alternate I';

UPDATE Clauses
SET clause_title = 'Evaluation of Export Offers' -- 'Evaluation of Export Offers.'
WHERE clause_version_id = 14 AND clause_name = '52.247-51 Alternate I';

UPDATE Clauses
SET clause_title = 'Evaluation of Export Offers' -- 'Evaluation of Export Offers.'
WHERE clause_version_id = 14 AND clause_name = '52.247-51 Alternate II';

UPDATE Clauses
SET clause_title = 'Evaluation of Export Offers' -- 'Evaluation of Export Offers.'
WHERE clause_version_id = 14 AND clause_name = '52.247-51 Alternate III';

UPDATE Clauses
SET clause_title = 'Preference for Privately Owned US-Flag Commercial Vessels' -- 'Preference for Privately Owned U.S.-Flag Commercial Vessels.'
WHERE clause_version_id = 14 AND clause_name = '52.247-64 Alternate I';

UPDATE Clauses
SET clause_title = 'Preference for Privately Owned US-Flag Commercial Vessels' -- 'Preference for Privately Owned U.S.-Flag Commercial Vessels.'
WHERE clause_version_id = 14 AND clause_name = '52.247-64 Alternate II';

UPDATE Clauses
SET clause_title = 'Value Engineering' -- 'Value Engineering.'
WHERE clause_version_id = 14 AND clause_name = '52.248-1 Alternate I';

UPDATE Clauses
SET clause_title = 'Value Engineering' -- 'Value Engineering.'
WHERE clause_version_id = 14 AND clause_name = '52.248-1 Alternate II';

UPDATE Clauses
SET clause_title = 'Value Engineering' -- 'Value Engineering.'
WHERE clause_version_id = 14 AND clause_name = '52.248-1 Alternate III';

UPDATE Clauses
SET clause_title = 'Value Engineering - Construction' -- 'Value Engineering--Construction.'
WHERE clause_version_id = 14 AND clause_name = '52.248-3 Alternate I';

UPDATE Clauses
SET clause_title = 'Termination for Convenience of the Government (Fixed-Price) (Short Form)' -- 'Termination for Convenience of the Government (Fixed-Price) (Short Form).'
WHERE clause_version_id = 14 AND clause_name = '52.249-1 Alternate I';

UPDATE Clauses
SET clause_title = 'Default (Fixed-Price Construction)' -- 'Default (Fixed-Price Construction).'
WHERE clause_version_id = 14 AND clause_name = '52.249-10 Alternate I';

UPDATE Clauses
SET clause_title = 'Default (Fixed-Price Construction)' -- 'Default (Fixed-Price Construction).'
WHERE clause_version_id = 14 AND clause_name = '52.249-10 Alternate II';

UPDATE Clauses
SET clause_title = 'Default (Fixed-Price Construction)' -- 'Default (Fixed-Price Construction).'
WHERE clause_version_id = 14 AND clause_name = '52.249-10 Alternate III';

UPDATE Clauses
SET clause_title = 'Termination for Convenience of the Government (Fixed-Price)' -- 'Termination for Convenience of the Government (Fixed-Price).'
WHERE clause_version_id = 14 AND clause_name = '52.249-2 Alternate I';

UPDATE Clauses
SET clause_title = 'Termination for Convenience of the Government (Fixed-Price)' -- 'Termination for Convenience of the Government (Fixed-Price).'
WHERE clause_version_id = 14 AND clause_name = '52.249-2 Alternate II';

UPDATE Clauses
SET clause_title = 'Termination for Convenience of the Government (Fixed-Price)' -- 'Termination for Convenience of the Government (Fixed-Price).'
WHERE clause_version_id = 14 AND clause_name = '52.249-2 Alternate III';

UPDATE Clauses
SET clause_title = 'Termination for Convenience of the Government (Dismantling, Demolition, or Removal of Improvements)' -- 'Termination for Convenience of the Government (Dismantling, Demolition, or Removal of Improvements).'
WHERE clause_version_id = 14 AND clause_name = '52.249-3 Alternate I';

UPDATE Clauses
SET clause_title = 'Termination (Cost- Reimbursement)' -- 'Termination (Cost- Reimbursement).'
WHERE clause_version_id = 14 AND clause_name = '52.249-6 Alternate I';

UPDATE Clauses
SET clause_title = 'Termination (Cost- Reimbursement)' -- 'Termination (Cost- Reimbursement).'
WHERE clause_version_id = 14 AND clause_name = '52.249-6 Alternate II';

UPDATE Clauses
SET clause_title = 'Termination (Cost- Reimbursement)' -- 'Termination (Cost- Reimbursement).'
WHERE clause_version_id = 14 AND clause_name = '52.249-6 Alternate III';

UPDATE Clauses
SET clause_title = 'Termination (Cost- Reimbursement)' -- 'Termination (Cost- Reimbursement).'
WHERE clause_version_id = 14 AND clause_name = '52.249-6 Alternate IV';

UPDATE Clauses
SET clause_title = 'Termination (Cost- Reimbursement)' -- 'Termination (Cost- Reimbursement).'
WHERE clause_version_id = 14 AND clause_name = '52.249-6 Alternate V';

UPDATE Clauses
SET clause_title = 'Default (Fixed- Price Supply and Service)' -- 'Default (Fixed- Price Supply and Service).'
WHERE clause_version_id = 14 AND clause_name = '52.249-8 Alternate I';

UPDATE Clauses
SET clause_title = 'Indemnification Under Public Law 85-804' -- 'Indemnification Under Public Law 85-804.'
WHERE clause_version_id = 14 AND clause_name = '52.250-1 Alternate I';

UPDATE Clauses
SET clause_title = 'Safety Act Block Designation/Certification' -- 'Safety Act Block Designation/Certification.'
WHERE clause_version_id = 14 AND clause_name = '52.250-3 Alternate I';

UPDATE Clauses
SET clause_title = 'Safety Act Block Designation/Certification' -- 'Safety Act Block Designation/Certification.'
WHERE clause_version_id = 14 AND clause_name = '52.250-3 Alternate II';

UPDATE Clauses
SET clause_title = 'Safety Act Pre-Qualification Designation Notice' -- 'Safety Act Pre-Qualification Designation Notice.'
WHERE clause_version_id = 14 AND clause_name = '52.250-4 Alternate I';

UPDATE Clauses
SET clause_title = 'Safety Act Pre-Qualification Designation Notice' -- 'Safety Act Pre-Qualification Designation Notice.'
WHERE clause_version_id = 14 AND clause_name = '52.250-4 Alternate II';

/*
*** End of SQL Scripts at Fri Jan 15 15:48:34 EST 2016 ***
*/
