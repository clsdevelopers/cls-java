/* ===============================================
 * Prescriptions
   =============================================== */
/* ===============================================
 * Questions
   =============================================== */

DELETE FROM Question_Choices WHERE question_choce_id IN (74169, 74170, 74171, 74172, 74173, 74174, 74175, 74176); -- [SECURITY REQUIREMENTS]

INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (76599, 14468, 'CONTRACT REQUIRES ACCESS TO CLASSIFIED INFORMATION');
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 76599, prescription_id FROM Prescriptions WHERE prescription_name IN ('223.570-2','4.404(a)');
INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (76600, 14468, 'THE AGENCY IS COVERED BY THE NISP');
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 76600, prescription_id FROM Prescriptions WHERE prescription_name IN ('4.404(a)');
INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (76601, 14468, 'EMPLOYEE IDENTIFICATION IS REQUIRED FOR SECURITY REASONS');
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 76601, prescription_id FROM Prescriptions WHERE prescription_name IN ('4.404(a)');
INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (76602, 14468, 'THE AWARD INVOLVES UNCLASSIFIED BUT SENSITIVE INFORMATION');
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 76602, prescription_id FROM Prescriptions WHERE prescription_name IN ('204.404-70(a)');
INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (76603, 14468, 'CONTRACT PERFORMANCE REQUIRES CONTRACTOR TO HAVE ROUTINE PHYSICAL ACCESS TO A FEDERALLY-CONTROLLED FACILITY AND/OR ROUTINE ACCESS TO A FEDERALLY-CONTROLLED INFORMATION SYSTEM');
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 76603, prescription_id FROM Prescriptions WHERE prescription_name IN ('4.1303');
INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (76604, 14468, 'ACCESS TO PROSCRIBED INFORMATION IS NECESSARY FOR CONTRACT PERFORMANCE');
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 76604, prescription_id FROM Prescriptions WHERE prescription_name IN ('209.104-70');
INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (76605, 14468, 'CONTRACTOR ONLY REQUIRES INTERMITTENT ACCESS TO FEDERALLY CONTROLLED FACILITIES');
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 76605, prescription_id FROM Prescriptions WHERE prescription_name IN ('4.1303');
INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (76606, 14468, 'NONE OF THE ABOVE');

DELETE FROM Question_Choices WHERE question_choce_id IN (74266, 74267, 74268, 74269, 74270, 74271, 74272); -- [FULL AND OPEN EXCEPTION]

INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (76696, 14486, '6.302-1 ONLY ONE RESPONSIBLE SOURCE AND NO OTHER SUPPLIES OR SERVICES WILL SATISFY AGENCY REQUIREMENTS.');
INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (76697, 14486, '6.302-2 UNUSUAL AND COMPELLING URGENCY');
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 76697, prescription_id FROM Prescriptions WHERE prescription_name IN ('4.1105(a)(1)');
INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (76698, 14486, '6.302-3 INDUSTRIAL MOBILIZATION; ENGINEERING, DEVELOPMENTAL, OR RESEARCH CAPABILITY; OR EXPERT SERVICES');
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 76698, prescription_id FROM Prescriptions WHERE prescription_name IN ('225.1101(2)(ii)','225.1101(2)(iii)');
INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (76699, 14486, '6.302-5 AUTHORIZED OR REQUIRED BY STATUTE');
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 76699, prescription_id FROM Prescriptions WHERE prescription_name IN ('217.7303');
INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (76700, 14486, '6.302-6 NATIONAL SECURITY');
INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (76701, 14486, '6.302-7 PUBLIC INTEREST');
INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (76702, 14486, 'NONE OF THE ABOVE OR NOT REQUIRED');

UPDATE Questions
SET question_type = 'B' -- 1
WHERE question_id = 14525; -- [OPTIONS]
DELETE FROM Question_Choices WHERE question_choce_id IN (74499, 74500, 74501); -- [OPTIONS]

INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (76929, 14525, 'YES');
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 76929, prescription_id FROM Prescriptions WHERE prescription_name IN ('16.406(a)','16.406(b)','17.208(a)','17.208(b)','17.208(c)','17.208(d)','17.208(e)','17.208(f)','17.208(g)','22.1006(c)(1)','22.1006(c)(2)','22.407(e)','22.407(f)','22.407(g)');
INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (76930, 14525, 'NO');

UPDATE Question_Conditions
SET condition_to_question = '"GOVERNMENT SUPPLY SOURCE INFORMATION/AUTHORIZATION APPLIES" = [CONTRACTOR MATERIAL]' -- '" GOVERNMENT SUPPLY SOURCE INFORMATION/AUTHORIZATION APPLIES" = [CONTRACTOR MATERIAL]'
WHERE question_condition_id = 1247; -- [GOVERNMENT SUPPLY STATEMENT]


DELETE FROM Question_Choices WHERE question_choce_id IN (74887, 74888, 74889, 74890, 74891); -- [TRANSPORTATION REQUIREMENTS 4]

INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (77316, 14608, 'GOVERNMENT FURNISHES EQUIPMENT WITH OR WITHOUT OPERATORS');
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 77316, prescription_id FROM Prescriptions WHERE prescription_name IN ('47.207-8(a)(2)(i)');
INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (77317, 14608, 'GOVERNMENT IS RESPONSIBLE FOR NOTIFYING CONTRACTOR OF SPECIFIC SERVICE TIMES OR  UNUSUAL SHIPMENTS');
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 77317, prescription_id FROM Prescriptions WHERE prescription_name IN ('47.207-8(a)(1)');
INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (77318, 14608, 'GOVERNMENT IS RESPONSIBLE FOR GFP TRANSPORTATION ARRANGEMENTS AND COSTS');
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 77318, prescription_id FROM Prescriptions WHERE prescription_name IN ('47.305-12(a)(2)');
INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (77319, 14608, 'GOVERNMENT WILL IDENTIFY COMMODITIES OR TYPES OF SHIPMENTS THAT HAVE BEEN IDENTIFIED FOR EXCLUSION');
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 77319, prescription_id FROM Prescriptions WHERE prescription_name IN ('47.207-3(d)(2)');
INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (77320, 14608, 'NONE OF THE ABOVE');

DELETE FROM Question_Choices WHERE question_choce_id IN (74959, 74960, 74961, 74962, 74963, 74964, 74965, 74966, 74967, 74968, 74969, 74970, 74971); -- [FUNDING FY]

INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (77388, 14622, 'PRE FY2004 FUNDS');
INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (77389, 14622, 'FY2004 FUNDS');
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 77389, prescription_id FROM Prescriptions WHERE prescription_name IN ('225.1101(10)(i)(A)','225.1101(10)(i)(B)','225.1101(10)(i)(C)','25.1101(b)(1)(i)','25.1101(b)(1)(ii)','25.1101(b)(1)(iii)','25.1101(b)(1)(iv)');
INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (77390, 14622, 'FY2005 FUNDS');
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 77390, prescription_id FROM Prescriptions WHERE prescription_name IN ('225.1101(10)(i)(A)','225.1101(10)(i)(B)','225.1101(10)(i)(C)','25.1101(b)(1)(i)','25.1101(b)(1)(ii)','25.1101(b)(1)(iii)','25.1101(b)(1)(iv)');
INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (77391, 14622, 'FY2006 FUNDS');
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 77391, prescription_id FROM Prescriptions WHERE prescription_name IN ('225.1101(10)(i)(A)','225.1101(10)(i)(B)','225.1101(10)(i)(C)','25.1101(b)(1)(i)','25.1101(b)(1)(ii)','25.1101(b)(1)(iii)','25.1101(b)(1)(iv)');
INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (77392, 14622, 'FY2007 FUNDS');
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 77392, prescription_id FROM Prescriptions WHERE prescription_name IN ('225.1101(10)(i)(A)','225.1101(10)(i)(B)','225.1101(10)(i)(C)','25.1101(b)(1)(i)','25.1101(b)(1)(ii)','25.1101(b)(1)(iii)','25.1101(b)(1)(iv)');
INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (77393, 14622, 'FY2008 FUNDS');
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 77393, prescription_id FROM Prescriptions WHERE prescription_name IN ('225.1101(10)(i)(A)','225.1101(10)(i)(B)','225.1101(10)(i)(C)','25.1101(b)(1)(i)','25.1101(b)(1)(ii)','25.1101(b)(1)(iii)','25.1101(b)(1)(iv)');
INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (77394, 14622, 'FY2009 FUNDS');
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 77394, prescription_id FROM Prescriptions WHERE prescription_name IN ('225.1101(10)(i)(A)','225.1101(10)(i)(B)','225.1101(10)(i)(C)','25.1101(b)(1)(i)','25.1101(b)(1)(ii)','25.1101(b)(1)(iii)','25.1101(b)(1)(iv)');
INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (77395, 14622, 'FY2010 FUNDS');
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 77395, prescription_id FROM Prescriptions WHERE prescription_name IN ('222.7405','225.1101(10)(i)(A)','225.1101(10)(i)(B)','225.1101(10)(i)(C)','25.1101(b)(1)(i)','25.1101(b)(1)(ii)','25.1101(b)(1)(iii)','25.1101(b)(1)(iv)');
INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (77396, 14622, 'FY2011 FUNDS');
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 77396, prescription_id FROM Prescriptions WHERE prescription_name IN ('222.7405','225.1101(10)(i)(A)','225.1101(10)(i)(B)','225.1101(10)(i)(C)','25.1101(b)(1)(i)','25.1101(b)(1)(ii)','25.1101(b)(1)(iii)','25.1101(b)(1)(iv)');
INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (77397, 14622, 'FY2012 FUNDS');
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 77397, prescription_id FROM Prescriptions WHERE prescription_name IN ('222.7405','225.1101(10)(i)(A)','225.1101(10)(i)(B)','225.1101(10)(i)(C)','25.1101(b)(1)(i)','25.1101(b)(1)(ii)','25.1101(b)(1)(iii)','25.1101(b)(1)(iv)');
INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (77398, 14622, 'FY2013 FUNDS');
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 77398, prescription_id FROM Prescriptions WHERE prescription_name IN ('222.7405','225.1101(10)(i)(A)','225.1101(10)(i)(B)','225.1101(10)(i)(C)','25.1101(b)(1)(i)','25.1101(b)(1)(ii)','25.1101(b)(1)(iii)','25.1101(b)(1)(iv)');
INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (77399, 14622, 'FY2014 FUNDS');
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 77399, prescription_id FROM Prescriptions WHERE prescription_name IN ('222.7405','225.1101(10)(i)(A)','225.1101(10)(i)(B)','225.1101(10)(i)(C)','225.7002-3(a)','25.1101(b)(1)(i)','25.1101(b)(1)(ii)','25.1101(b)(1)(iii)','25.1101(b)(1)(iv)');
INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (77400, 14622, 'FY2015 FUNDS');
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 77400, prescription_id FROM Prescriptions WHERE prescription_name IN ('2015-O0010','2015-OO005','222.7405','225.1101(10)(i)(A)','225.1101(10)(i)(B)','225.1101(10)(i)(C)','25.1101(b)(1)(i)','25.1101(b)(1)(ii)','25.1101(b)(1)(iii)','25.1101(b)(1)(iv)');

UPDATE Questions
SET question_type = 'B' -- 1
WHERE question_id = 14629; -- [CONTRACT PRIOR TO FUNDS]
DELETE FROM Question_Choices WHERE question_choce_id IN (74997, 74998, 74999); -- [CONTRACT PRIOR TO FUNDS]

INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (77426, 14629, 'YES');
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 77426, prescription_id FROM Prescriptions WHERE prescription_name IN ('32.706-1(a)');
INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (77427, 14629, 'NO');

DELETE FROM Question_Choices WHERE question_choce_id IN (75057, 75058, 75059, 75060, 75061, 75062, 75063, 75064, 75065, 75066, 75067, 75068, 75069); -- [INSURANCE CONDITIONS]

INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (77485, 14641, 'THE NON-DOD CUSTOMER HAS NOT AGREED TO ASSUME RISK OF LOSS OR DAMAGE TO AIRCRAFT');
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 77485, prescription_id FROM Prescriptions WHERE prescription_name IN ('228.370(b)');
INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (77486, 14641, 'A BID GUARANTEE OR BOND APPLIES');
INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (77487, 14641, 'THE DEFENSE BASE ACT APPLIES');
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 77487, prescription_id FROM Prescriptions WHERE prescription_name IN ('28.309(a)');
INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (77488, 14641, 'THE VEHICULAR LIABILITY OR GENERAL PUBLIC LIABILITY INSURANCE REQUIRED BY LAW IS NOT SUFFICIENT');
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 77488, prescription_id FROM Prescriptions WHERE prescription_name IN ('28.313(b)');
INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (77489, 14641, 'THE CONTRACTOR IS TO BE INDEMNIFIED FOR THIRD PARTY CLAIMS AND LOSS/DAMAGE');
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 77489, prescription_id FROM Prescriptions WHERE prescription_name IN ('235.070-3');
INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (77490, 14641, 'THE CONTRACTOR IS TO BE INDEMNIFIED FOR LOSS OF OR DAMAGE TO THE CONTRACTOR''S PROPERTY');
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 77490, prescription_id FROM Prescriptions WHERE prescription_name IN ('235.070-3');
INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (77491, 14641, 'THE CONTRACTOR IS NOT ALLOWED TO BUY WAR-HAZARD INSURANCE');
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 77491, prescription_id FROM Prescriptions WHERE prescription_name IN ('228.370(a)');
INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (77492, 14641, 'EMPLOYEES ARE SUBJECT TO CAPTURE AND NOT COVERED BY THE WAR HAZARDS COMPENSATION ACT');
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 77492, prescription_id FROM Prescriptions WHERE prescription_name IN ('228.370(c)');
INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (77493, 14641, 'THE CONTRACTOR SHALL BE INDEMNIFIED AGAINST UNUSUALLY HAZARDOUS OR NUCLEAR RISKS');
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 77493, prescription_id FROM Prescriptions WHERE prescription_name IN ('50.104-4','50.403-3');
INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (77494, 14641, 'CONTRACTOR LIABILITY FOR PERSONAL INJURY AND/OR PROPERTY DAMAGE');
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 77494, prescription_id FROM Prescriptions WHERE prescription_name IN ('47.207-7(c)');
INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (77495, 14641, 'A NO SET-OFF COMMITMENT HAS BEEN AUTHORIZED');
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 77495, prescription_id FROM Prescriptions WHERE prescription_name IN ('32.806(a)(2)');
INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (77496, 14641, 'A PROHIBITION OF ASSIGNMENT OF CLAIMS IS IN THE BEST INTEREST OF THE GOVERNMENT');
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 77496, prescription_id FROM Prescriptions WHERE prescription_name IN ('32.806(b)');
INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (77497, 14641, 'NONE OF THE ABOVE');
INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (77498, 14641, 'NONE OF THE ABOVE');

DELETE FROM Question_Choices WHERE question_choce_id IN (75205, 75206, 75207, 75208, 75209, 75210, 75211, 75212); -- [REGULATION PRICING]

INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (77633, 14651, 'FORMAT IN TABLE 15.2 IS USED');
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 77633, prescription_id FROM Prescriptions WHERE prescription_name IN ('15.408(I)');
INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (77634, 14651, 'PRICING IS CONSISTENT WITH FAR PART 31, "CONTRACT COST PRINCIPLES AND PROCEDURES"');
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 77634, prescription_id FROM Prescriptions WHERE prescription_name IN ('15.408(g)');
INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (77635, 14651, 'PRICING IS CONSISTENT WITH FAR PART 31.1, "APPLICABILITY"');
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 77635, prescription_id FROM Prescriptions WHERE prescription_name IN ('231.100-70');
INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (77636, 14651, 'PRICING IS CONSISTENT WITH FAR PART 31.2, "CONTRACTS WITH COMMERCIAL ORGANIZATIONS"');
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 77636, prescription_id FROM Prescriptions WHERE prescription_name IN ('15.408(h)','15.408(k)','231.100-70');
INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (77637, 14651, 'PRICING IS CONSISTENT WITH FAR PART 31.6, "CONTRACTS WITH STATE, LOCAL, AND FEDERALLY RECOGNIZED INDIAN TRIBAL GOVERNMENTS"');
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 77637, prescription_id FROM Prescriptions WHERE prescription_name IN ('231.100-70');
INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (77638, 14651, 'PRICING IS CONSISTENT WITH FAR PART 31.7, "CONTRACTS WITH NONPROFIT ORGANIZATIONS"');
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 77638, prescription_id FROM Prescriptions WHERE prescription_name IN ('231.100-70');
INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (77639, 14651, 'CONTRACTORS ARE REQUIRED TO FURNISH FINANCIAL STATEMENTS');
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 77639, prescription_id FROM Prescriptions WHERE prescription_name IN ('47.207-1(e)');
INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (77640, 14651, 'NONE OF THE ABOVE');
INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (77641, 14651, 'NONE OF THE ABOVE');

DELETE FROM Question_Choices WHERE question_choce_id IN (74160, 74161, 74162); -- [SET-ASIDE]

INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (76590, 14464, 'LOCAL AREA SET-ASIDE (STAFFORD ACT SET-ASIDE) (FAR 26.2)');
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 76590, prescription_id FROM Prescriptions WHERE prescription_name IN ('26.206(a)','26.206(b)','26.206(c)');
INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (76591, 14464, 'PARTIAL SMALL BUSINESS SET-ASIDE');
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 76591, prescription_id FROM Prescriptions WHERE prescription_name IN ('16.506(d)(4)','16.506(d)(5)','19.508(d)','19.508(e)','19.811-3(e)','4.607(b)');
INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (76592, 14464, 'TOTAL SMALL BUSINESS SET-ASIDE');
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 76592, prescription_id FROM Prescriptions WHERE prescription_name IN ('19.1506(a)','19.1506(b)','19.508(c)','19.508(d)','19.508(e)','19.811-3(e)','36.501(b)','4.607(b)');

DELETE FROM Question_Choices WHERE question_choce_id IN (74518, 74519, 74520, 74521); -- [THE SUPPLIES ARE COMMERCIAL ITEMS]

INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (76947, 14532, 'THE SUPPLIES ARE MODIFIED COMMERCIAL ITEMS');
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 76947, prescription_id FROM Prescriptions WHERE prescription_name IN ('42.1305(c)');
INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (76948, 14532, 'THE SUPPLIES ARE COMMERCIALLY AVAILABLE OFF-THE- SHELF ITEMS (COTS)');
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 76948, prescription_id FROM Prescriptions WHERE prescription_name IN ('22.1803','23.406(c)','23.406(d)');
INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (76949, 14532, 'THE SUPPLIES ARE NOT ENTIRELY COMMERCIALLY AVAILABLE OFF-THE- SHELF ITEMS (COTS)');
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 76949, prescription_id FROM Prescriptions WHERE prescription_name IN ('22.1705(b)');
INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (76950, 14532, 'COMMERCIAL SERVICES ARE PROVIDED WITH THE COTS ITEM');
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 76950, prescription_id FROM Prescriptions WHERE prescription_name IN ('22.1803');

DELETE FROM Question_Choices WHERE question_id = 14641; -- [INSURANCE CONDITIONS]

INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (79911, 14641, 'THE NON-DOD CUSTOMER HAS NOT AGREED TO ASSUME RISK OF LOSS OR DAMAGE TO AIRCRAFT');
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 79911, prescription_id FROM Prescriptions WHERE prescription_name IN ('228.370(b)');
INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (79912, 14641, 'A BID GUARANTEE OR BOND APPLIES');
INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (79913, 14641, 'THE DEFENSE BASE ACT APPLIES');
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 79913, prescription_id FROM Prescriptions WHERE prescription_name IN ('28.309(a)');
INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (79914, 14641, 'THE VEHICULAR LIABILITY OR GENERAL PUBLIC LIABILITY INSURANCE REQUIRED BY LAW IS NOT SUFFICIENT');
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 79914, prescription_id FROM Prescriptions WHERE prescription_name IN ('28.313(b)');
INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (79915, 14641, 'THE CONTRACTOR IS TO BE INDEMNIFIED FOR THIRD PARTY CLAIMS AND LOSS/DAMAGE');
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 79915, prescription_id FROM Prescriptions WHERE prescription_name IN ('235.070-3');
INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (79916, 14641, 'THE CONTRACTOR IS TO BE INDEMNIFIED FOR LOSS OF OR DAMAGE TO THE CONTRACTOR''S PROPERTY');
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 79916, prescription_id FROM Prescriptions WHERE prescription_name IN ('235.070-3');
INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (79917, 14641, 'THE CONTRACTOR IS NOT ALLOWED TO BUY WAR-HAZARD INSURANCE');
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 79917, prescription_id FROM Prescriptions WHERE prescription_name IN ('228.370(a)');
INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (79918, 14641, 'EMPLOYEES ARE SUBJECT TO CAPTURE AND NOT COVERED BY THE WAR HAZARDS COMPENSATION ACT');
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 79918, prescription_id FROM Prescriptions WHERE prescription_name IN ('228.370(c)');
INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (79919, 14641, 'THE CONTRACTOR SHALL BE INDEMNIFIED AGAINST UNUSUALLY HAZARDOUS OR NUCLEAR RISKS');
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 79919, prescription_id FROM Prescriptions WHERE prescription_name IN ('50.104-4','50.403-3');
INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (79920, 14641, 'CONTRACTOR LIABILITY FOR PERSONAL INJURY AND/OR PROPERTY DAMAGE');
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 79920, prescription_id FROM Prescriptions WHERE prescription_name IN ('47.207-7(c)');
INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (79921, 14641, 'A NO SET-OFF COMMITMENT HAS BEEN AUTHORIZED');
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 79921, prescription_id FROM Prescriptions WHERE prescription_name IN ('32.806(a)(2)');
INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (79922, 14641, 'A PROHIBITION OF ASSIGNMENT OF CLAIMS IS IN THE BEST INTEREST OF THE GOVERNMENT');
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 79922, prescription_id FROM Prescriptions WHERE prescription_name IN ('32.806(b)');
INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (79923, 14641, 'NONE OF THE ABOVE');

DELETE FROM Question_Choices WHERE question_id = 14645; -- [WAIVERS]

INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (79933, 14645, '9-108-4 -- WAIVER OF THE PROHIBITION ON CONTRACTING WITH ANY FOREIGN ENTITY THAT IS TREATED AS AN INVERTED DOMESTIC CORPORATION');
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 79933, prescription_id FROM Prescriptions WHERE prescription_name IN ('9.108-5(a)','9.108-5(b)');
INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (79934, 14645, '15.209/25.1001 -- WAIVER OF REQUIREMENT TO EXAMINATION OF RECORDS BY THE COMPTROLLER GENERAL');
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 79934, prescription_id FROM Prescriptions WHERE prescription_name IN ('12.301(b)(4)(i)','15.209(b)(4)');
INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (79935, 14645, '15.408 -- WAIVER OF FACILITIES CAPITAL COST OF MONEY');
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 79935, prescription_id FROM Prescriptions WHERE prescription_name IN ('15.408(i)');
INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (79936, 14645, '22.1203-3 -- WAIVER OF REQUIREMENT RELATING TO THE "NONDISPLACEMENT OF QUALIFIED WORKERS UNDER SERVICE CONTRACTS"');
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 79936, prescription_id FROM Prescriptions WHERE prescription_name IN ('22.1207');
INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (79937, 14645, '22.1305 - WAIVER OF ALL THE TERMS OF 52.222-35, "EQUAL OPPORTUNITY FOR VETERANS"');
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 79937, prescription_id FROM Prescriptions WHERE prescription_name IN ('22.1310(a)(1)');
INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (79938, 14645, '22.1305 - WAIVER OF ONE OR MORE OF THE TERMS (BUT NOT ALL) OF 52.222-35, "EQUAL OPPORTUNITY FOR VETERANS"');
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 79938, prescription_id FROM Prescriptions WHERE prescription_name IN ('22.1310(a)(2)');
INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (79939, 14645, '22.1403 - WAIVER OF ONE OR MORE (BUT NOT ALL) OF THE TERMS OF 52.222-36, "AFFIRMATIVE ACTION FOR WORKERS WITH DISABILITIES"');
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 79939, prescription_id FROM Prescriptions WHERE prescription_name IN ('22.1408(b)');
INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (79940, 14645, '22.1403 - WAIVER OF ALL OF THE TERMS OF 52.222-36, "AFFIRMATIVE ACTION FOR WORKERS WITH DISABILITIES"');
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 79940, prescription_id FROM Prescriptions WHERE prescription_name IN ('22.1408(a)');
INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (79941, 14645, '22.1603 -- WAIVER OF THE REQUIREMENT TO NOTIFY EMPLOYEES OF THEIR RIGHTS UNDER THE NATIONAL LABOR RELATIONS ACT');
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 79941, prescription_id FROM Prescriptions WHERE prescription_name IN ('22.1605');
INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (79942, 14645, '28.305 - WAIVER OF THE DEFENSE BASE ACT');
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 79942, prescription_id FROM Prescriptions WHERE prescription_name IN ('28.309(b)');
INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (79943, 14645, '236.205 -- WAIVER OF THE REQUIREMENT NOT TO AWARD A CONSTRUCTION CONTRACT EXCEEDING A STATUTORY COST LIMITATION');
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 79943, prescription_id FROM Prescriptions WHERE prescription_name IN ('236.570(b)(4)');
INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (79944, 14645, '37.113-1 - WAIVER OF COST ALLOWABILITY LIMITATIONS ON SEVERANCE PAYMENTS TO FOREIGN NATIONALS');
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 79944, prescription_id FROM Prescriptions WHERE prescription_name IN ('37.113-2(a)','37.113-2(b)');
INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (79945, 14645, '225.7006-3 -- WAIVER/EXCEPTION TO RESTRICTION ON ACQUISITION OF AIR CIRCUIT BREAKERS FOR NAVAL VESSELS');
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 79945, prescription_id FROM Prescriptions WHERE prescription_name IN ('225.7006-4(a)','225.7006-4(b)');
INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (79946, 14645, '225.7007-2 -- WAIVER OF RESTRICTION FOR ACQUISITION OF ANCHOR AND MOORING CHAIN');
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 79946, prescription_id FROM Prescriptions WHERE prescription_name IN ('225.7007-3');
INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (79947, 14645, '225.7009-4 -- WAIVER OF RESTRICTIONS ON BALL OR ROLLER BEARINGS');
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 79947, prescription_id FROM Prescriptions WHERE prescription_name IN ('225.7009-5');
INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (79948, 14645, '225.7011-2 -- WAIVER ON ACQUISITION OF CARBON, ALLOY OR ARMOR STEEL PLATE');
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 79948, prescription_id FROM Prescriptions WHERE prescription_name IN ('225.7011-3');
INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (79949, 14645, '225-7012-2 -- WAIVER OF THE REQUIREMENT TO PURCHASE SUPERCOMPUTERS IN THE UNITED STATES');
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 79949, prescription_id FROM Prescriptions WHERE prescription_name IN ('225.7012-3');
INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (79950, 14645, '225.7102-3 -- WAIVER OF THE REQUIREMENT FOR DOMESTIC MANUFACTURE OF THE ITEMS LISTED IN 225.7102-1');
INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (79951, 14645, '247.572(c)(2) -- WAIVER OF THE REQUIREMENT FOR DOD CONTRACTORS TO TRANSPORT SUPPLIES AS DEFINED IN 252.247-7023, EXCLUSIVELY ON U.S. FLAG VESSELS');
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 79951, prescription_id FROM Prescriptions WHERE prescription_name IN ('247.574(d)');
INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (79952, 14645, '225.7604 -- WAIVER OF REQUIREMENT NOT TO CONTRACT WITH A FOREIGN ENTITY THAT COMPLIES WITH THE SECONDARY ARAB BOYCOTT OF ISRAEL');
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 79952, prescription_id FROM Prescriptions WHERE prescription_name IN ('225.7605');
INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (79953, 14645, '247.572 -- WAIVER OF REQUIREMENT TO HAVE REFLAGGING OR REPAIR WORK PERFORMED IN THE UNITED STATES OR ITS OUTLYING AREAS');
INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (79954, 14645, 'NO WAIVERS APPROVED');

DELETE FROM Question_Choices WHERE question_id = 14646; -- [EXEMPTIONS]

INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (79955, 14646, '225.7501 -- EXEMPT FROM THE BALANCE OF PAYMENT PROGRAM');
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 79955, prescription_id FROM Prescriptions WHERE prescription_name IN ('225.1101(2)(ii)','225.1101(2)(iii)');
INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (79956, 14646, '22.807 -- EXEMPT FROM ALL THE REQUIREMENTS OF E.O. 11246');
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 79956, prescription_id FROM Prescriptions WHERE prescription_name IN ('22.810(e)');
INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (79957, 14646, '22.807 -- EXEMPT FROM ONE OR MORE, BUT NOT ALL THE REQUIREMENTS OF E.O. 11246');
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 79957, prescription_id FROM Prescriptions WHERE prescription_name IN ('22.810(e)');
INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (79958, 14646, '48.102 -- EXEMPT FROM VALUE ENGINEERING');
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 79958, prescription_id FROM Prescriptions WHERE prescription_name IN ('48.201','48.201(c)','48.201(d)','48.201(e)(1)');
INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (79959, 14646, '22.305 -- EXEMPT FROM INCLUSION OF 52.222-4 "CONTRACT WORK HOURS AND SAFETY STANDARDS ACT -- OVERTIME COMPENSATION"');
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 79959, prescription_id FROM Prescriptions WHERE prescription_name IN ('22.305');
INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (79960, 14646, '22.1003-3/4 -- EXEMPT FROM APPLICATION OF THE SERVICE CONTRACT ACT');
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 79960, prescription_id FROM Prescriptions WHERE prescription_name IN ('22.1006(e)(1)','22.1006(e)(3)');
INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (79961, 14646, '23.204 -- ENERGY STAR OR FEMP-DESIGNATED PRODUCT EXEMPTION');
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 79961, prescription_id FROM Prescriptions WHERE prescription_name IN ('23.206');
INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (79962, 14646, '22.1203-2 -- EXEMPT FROM 52.222-17, "NONDISPLACEMENT OF QUALIFIED WORKERS"');
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 79962, prescription_id FROM Prescriptions WHERE prescription_name IN ('22.1207');
INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (79963, 14646, '22.1603 -- SECRETARIAL EXEMPTION TO THE REQUIREMENT TO INCLUDE THE CLAUSE 52.222-40, "NOTIFICATION OF EMPLOYEE RIGHTS UNDER THE NATIONAL LABOR RELATIONS ACT');
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 79963, prescription_id FROM Prescriptions WHERE prescription_name IN ('22.1605');
INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (79964, 14646, 'NO EXEMPTION APPLIES');

DELETE FROM Question_Choices WHERE question_id = 14647; -- [EXCEPTIONS]

INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (79965, 14647, '25.202 -- EXCEPTION TO THE BUY AMERICAN STATUTE');
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 79965, prescription_id FROM Prescriptions WHERE prescription_name IN ('225.1101(2)(ii)','225.1101(2)(iii)','25.1101(a)(1)','25.1101(c)(1)','25.1103(a)');
INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (79966, 14647, '25.401 -- EXCEPTION TO FOREIGN TRADE AGREEMENTS');
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 79966, prescription_id FROM Prescriptions WHERE prescription_name IN ('25.1101(b)(1)(i)','25.1101(b)(1)(ii)','25.1101(b)(1)(iii)','25.1101(b)(1)(iv)');
INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (79967, 14647, '22.1603 --22.1603 -- SECRETARIAL EXEMPTION TO THE REQUIREMENT TO INCLUDE THE CLAUSE 52.222-40, "NOTIFICATION OF EMPLOYEE RIGHTS UNDER THE NATIONAL LABOR RELATIONS ACT"');
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 79967, prescription_id FROM Prescriptions WHERE prescription_name IN ('22.1605');
INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (79968, 14647, '211.274-2(b) -- EXCEPTION TO UNIQUE ITEM IDENTIFICATION');
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 79968, prescription_id FROM Prescriptions WHERE prescription_name IN ('211.274-6(a)(1)');
INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (79969, 14647, '215.371-4 - EXCEPTION/WAIVER TO REQUIREMENT TO RE- SOLICIT IF ONLY ONE OFFER IS RECEIVED');
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 79969, prescription_id FROM Prescriptions WHERE prescription_name IN ('215.371-6','215.408(4)');
INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (79970, 14647, '232.7002(a) - EXCEPTION TO ELECTRONIC SUBMISSION AND PROCESSING OF PAYMENT REQUESTS AND RECEIVING REPORTS');
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 79970, prescription_id FROM Prescriptions WHERE prescription_name IN ('232.7004(a)');
INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (79971, 14647, '225.7002-2 EXCEPTION TO RESTRICTIONS ON FOREIGN ACQUISITION');
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 79971, prescription_id FROM Prescriptions WHERE prescription_name IN ('225.7002-3(a)');
INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (79972, 14647, '225.7102-2 -- EXCEPTION ON THE RESTRICTION ON ACQUISITION OF FORGINGS');
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 79972, prescription_id FROM Prescriptions WHERE prescription_name IN ('225.7102-4');
INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (79973, 14647, '225.7603 -- EXCEPTION TO REQUIREMENT NOT TO CONTRACT WITH A FOREIGN ENTITY THAT DOES NOT COMPLY WITH THE SECONDARY ARAB BOYCOTT OF ISRAEL');
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 79973, prescription_id FROM Prescriptions WHERE prescription_name IN ('225.7605');
INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (79974, 14647, '223.7102 -- EXCEPTION TO 223.7102 (STORAGE AND DISPOSAL OF TOXIC AND HAZARDOUS MATERIALS)');
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 79974, prescription_id FROM Prescriptions WHERE prescription_name IN ('223.7106');
INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (79975, 14647, '225.7003-3 -- NATIONAL SECURITY EXCEPTION TO RESTRICTIONS ON SPECIALTY METALS');
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 79975, prescription_id FROM Prescriptions WHERE prescription_name IN ('225.7003-5(a)(1)');
INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (79976, 14647, '23.704(a) - EXCEPTION TO EPEAT REGISTERED ELECTRONIC PRODUCTS');
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 79976, prescription_id FROM Prescriptions WHERE prescription_name IN ('23.705(b)(1)','23.705(b)(2)','23.705(c)(1)','23.705(d)(2)');
INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (79977, 14647, 'NO EXCEPTIONS APPLY');

DELETE FROM Question_Choices WHERE question_id = 14648; -- [PATENT COUNCIL RECOMMENDED]

INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (79978, 14648, '52.227-3 ALTERNATE III -- PATENT INDEMNITY');
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 79978, prescription_id FROM Prescriptions WHERE prescription_name IN ('27.201-2(c)(3)');
INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (79979, 14648, '52.227-4 -- PATENT INDEMNITY -- CONSTRUCTION CONTRACTS');
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 79979, prescription_id FROM Prescriptions WHERE prescription_name IN ('27.201-2');
INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (79980, 14648, '52.227-4 ALTERNATE I -- PATENT INDEMNITY -- CONSTRUCTION CONTRACTS');
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 79980, prescription_id FROM Prescriptions WHERE prescription_name IN ('27.201-2(d)(2)');
INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (79981, 14648, '52.227-5 -- WAIVER OF INDEMNITY');
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 79981, prescription_id FROM Prescriptions WHERE prescription_name IN ('27.201-2(e)');
INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (79982, 14648, '52.227-6 -- ROYALTY INFORMATION');
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 79982, prescription_id FROM Prescriptions WHERE prescription_name IN ('27.202-5(a)(1)');
INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (79983, 14648, '52.227-6 ALTERNATE I -- ROYALTY INFORMATION');
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 79983, prescription_id FROM Prescriptions WHERE prescription_name IN ('27.202-5(a)(2)');
INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (79984, 14648, '52.227-7 -- PATENTS-- NOTICE OF GOVERNMENT LICENSEE');
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 79984, prescription_id FROM Prescriptions WHERE prescription_name IN ('27.202-5(b)');
INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (79985, 14648, '52.227-9 -- REFUND OF ROYALTIES');
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 79985, prescription_id FROM Prescriptions WHERE prescription_name IN ('27.202-5(c)');
INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (79986, 14648, '52.227-10 -- FILING OF PATENT APPLICATIONS -- CLASSIFIED SUBJECT MATTER');
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 79986, prescription_id FROM Prescriptions WHERE prescription_name IN ('27.203-2');
INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (79987, 14648, '52.227-11 -- PATENT RIGHTS -- OWNERSHIP BY THE CONTRACTOR');
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 79987, prescription_id FROM Prescriptions WHERE prescription_name IN ('27.303(b)(1)');
INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (79988, 14648, '52.227-11 ALTERNATE I-- PATENT RIGHTS -- OWNERSHIP BY THE CONTRACTOR');
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 79988, prescription_id FROM Prescriptions WHERE prescription_name IN ('27.303(b)(3)');
INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (79989, 14648, '52.227-11 ALTERNATE II -- PATENT RIGHTS -- OWNERSHIP BY THE CONTRACTOR');
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 79989, prescription_id FROM Prescriptions WHERE prescription_name IN ('27.303(b)(4)');
INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (79990, 14648, '52.227-11 ALTERNATE III-- PATENT RIGHTS -- OWNERSHIP BY THE CONTRACTOR');
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 79990, prescription_id FROM Prescriptions WHERE prescription_name IN ('27.303(b)(5)');
INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (79991, 14648, '52.227-11 ALTERNATE IV -- PATENT RIGHTS -- OWNERSHIP BY THE CONTRACTOR');
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 79991, prescription_id FROM Prescriptions WHERE prescription_name IN ('27.303(b)(6)');
INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (79992, 14648, '52.227-11 ALTERNATE V-- PATENT RIGHTS -- OWNERSHIP BY THE CONTRACTOR');
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 79992, prescription_id FROM Prescriptions WHERE prescription_name IN ('27.303(b)(7)');
INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (79993, 14648, '52.227-13 -- PATENT RIGHTS -- OWNERSHIP BY THE GOVERNMENT');
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 79993, prescription_id FROM Prescriptions WHERE prescription_name IN ('27.303(e)');
INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (79994, 14648, '52.227-13 -- PATENT RIGHTS ALTERNATE I -- OWNERSHIP BY THE GOVERNMENT');
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 79994, prescription_id FROM Prescriptions WHERE prescription_name IN ('27.303(e)(4)');
INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (79995, 14648, '52.227-13 -- PATENT RIGHTS ALTERNATE II -- OWNERSHIP BY THE GOVERNMENT');
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 79995, prescription_id FROM Prescriptions WHERE prescription_name IN ('27.303(e)(5)');
INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (79996, 14648, '52.227-14 -- RIGHTS IN DATA - GENERAL');
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 79996, prescription_id FROM Prescriptions WHERE prescription_name IN ('27.409(b)(1)');
INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (79997, 14648, '52.227-14 ALTERNATE I -- RIGHTS IN DATA - GENERAL');
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 79997, prescription_id FROM Prescriptions WHERE prescription_name IN ('27.409(b)(2)');
INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (79998, 14648, '52.227-14 ALTERNATE II - RIGHTS IN DATA - GENERAL');
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 79998, prescription_id FROM Prescriptions WHERE prescription_name IN ('27.409(b)(3)');
INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (79999, 14648, '52.227-14 ALTERNATE III-- RIGHTS IN DATA - GENERAL');
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 79999, prescription_id FROM Prescriptions WHERE prescription_name IN ('27.409(b)(4)');
INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (80000, 14648, '52.227-14 ALTERNATE IV-- RIGHTS IN DATA - GENERAL');
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 80000, prescription_id FROM Prescriptions WHERE prescription_name IN ('27.409(b)(4)');
INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (80001, 14648, '52.227-14 ALTERNATE V-- RIGHTS IN DATA - GENERAL');
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 80001, prescription_id FROM Prescriptions WHERE prescription_name IN ('27.409(b)(5)');
INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (80002, 14648, '52.227-15 -- REPRESENTATION OF LIMITED RIGHTS DATA AND RESTRICTED COMPUTER SOFTWARE');
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 80002, prescription_id FROM Prescriptions WHERE prescription_name IN ('27.409(c)');
INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (80003, 14648, '52.227-16 -- ADDITIONAL DATA REQUIREMENTS');
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 80003, prescription_id FROM Prescriptions WHERE prescription_name IN ('27.409(d)');
INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (80004, 14648, '52.227-17 -- RIGHTS IN DATA -- SPECIAL WORKS');
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 80004, prescription_id FROM Prescriptions WHERE prescription_name IN ('27.409(e)');
INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (80005, 14648, '52.227-18 -- RIGHTS IN DATA-EXISTING WORKS');
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 80005, prescription_id FROM Prescriptions WHERE prescription_name IN ('27.409(f)');
INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (80006, 14648, '52.227-19 -- COMMERCIAL COMPUTER SOFTWARE LICENSE');
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 80006, prescription_id FROM Prescriptions WHERE prescription_name IN ('27.409(g)');
INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (80007, 14648, '52.227-20 -- RIGHTS IN DATA -- SBIR PROGRAM');
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 80007, prescription_id FROM Prescriptions WHERE prescription_name IN ('27.409(h)');
INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (80008, 14648, '52.227-21 -- TECHNICAL DATA DECLARATION, REVISION, AND WITHHOLDING OF PAYMENT -- MAJOR SYSTEMS');
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 80008, prescription_id FROM Prescriptions WHERE prescription_name IN ('27.409(j)');
INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (80009, 14648, '52.227-22 -- MAJOR SYSTEM - MINIMUM RIGHTS');
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 80009, prescription_id FROM Prescriptions WHERE prescription_name IN ('27.409(k)');
INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (80010, 14648, '52.227-23 -- RIGHTS TO PROPOSAL DATA (TECHNICAL)');
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 80010, prescription_id FROM Prescriptions WHERE prescription_name IN ('27.409(l)');
INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (80011, 14648, '252.227-7000 -- NON-ESTOPPEL');
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 80011, prescription_id FROM Prescriptions WHERE prescription_name IN ('227.7009-1');
INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (80012, 14648, '252.227-7001 -- RELEASE OF PAST INFRINGEMENT');
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 80012, prescription_id FROM Prescriptions WHERE prescription_name IN ('227.7009-2(a)');
INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (80013, 14648, '252.227-7002 -- READJUSTMENT OF PAYMENTS');
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 80013, prescription_id FROM Prescriptions WHERE prescription_name IN ('227.7009-2(b)');
INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (80014, 14648, '252.227-7003 -- TERMINATION');
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 80014, prescription_id FROM Prescriptions WHERE prescription_name IN ('227.7009-2');
INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (80015, 14648, '252.227-7004 -- LICENSE GRANT');
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 80015, prescription_id FROM Prescriptions WHERE prescription_name IN ('227.7009-3(a)');
INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (80016, 14648, '252.227-7005 ALTERNATE I-- LICENSE TERM (NOTE: NO BASIC)');
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 80016, prescription_id FROM Prescriptions WHERE prescription_name IN ('227.7009-3(b)');
INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (80017, 14648, '252.227-7005 ALTERNATE II -- LICENSE TERM (NOTE: NO BASIC)');
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 80017, prescription_id FROM Prescriptions WHERE prescription_name IN ('227.7009-3(b)');
INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (80018, 14648, '252.227-7006 -- LICENSE GRANT---RUNNING ROYALTY');
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 80018, prescription_id FROM Prescriptions WHERE prescription_name IN ('227.7009-4(a)');
INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (80019, 14648, '252.227-7007 -- LICENSE TERM--RUNNING ROYALTY');
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 80019, prescription_id FROM Prescriptions WHERE prescription_name IN ('227.7009-4(b)');
INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (80020, 14648, '252.227-7008 -- COMPUTATION OF ROYALTIES');
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 80020, prescription_id FROM Prescriptions WHERE prescription_name IN ('227.7009- 4(c)');
INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (80021, 14648, '252.227-7009 -- REPORTING AND PAYMENT OF ROYALTIES');
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 80021, prescription_id FROM Prescriptions WHERE prescription_name IN ('227.7009-4(d)');
INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (80022, 14648, '252.227-7010 -- LICENSE TO OTHER GOVERNMENT AGENCIES');
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 80022, prescription_id FROM Prescriptions WHERE prescription_name IN ('227.7009-4(e)');
INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (80023, 14648, '252.227-7011 -- ASSIGNMENTS');
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 80023, prescription_id FROM Prescriptions WHERE prescription_name IN ('227.7010');
INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (80024, 14648, '252.227-7012 -- PATENT LICENSE AND RELEASE CONTRACT');
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 80024, prescription_id FROM Prescriptions WHERE prescription_name IN ('227.7012');
INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (80025, 14648, '252.227-7013 -- RIGHTS IN TECHNICAL DATA--NONCOMMERCIAL ITEMS');
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 80025, prescription_id FROM Prescriptions WHERE prescription_name IN ('227.7103-6(a)');
INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (80026, 14648, '252.227-7013 ALTERNATE I -- RIGHTS IN TECHNICAL DATA--NONCOMMERCIAL ITEMS');
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 80026, prescription_id FROM Prescriptions WHERE prescription_name IN ('227.7103-6(a)');
INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (80027, 14648, '252.227-7013 ALTERNATE II -- RIGHTS IN TECHNICAL DATA--NONCOMMERCIAL ITEMS');
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 80027, prescription_id FROM Prescriptions WHERE prescription_name IN ('227.7103-6(a)');
INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (80028, 14648, '252.227-7014 -- RIGHTS IN NONCOMMERCIAL COMPUTER SOFTWARE AND NONCOMMERCIAL COMPUTER SOFTWARE DOCUMENTATION');
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 80028, prescription_id FROM Prescriptions WHERE prescription_name IN ('227.7203-6(a)(1)');
INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (80029, 14648, '252.227-7014 ALTERNATE I -- RIGHTS IN NONCOMMERCIAL COMPUTER SOFTWARE AND NONCOMMERCIAL COMPUTER SOFTWARE DOCUMENTATION');
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 80029, prescription_id FROM Prescriptions WHERE prescription_name IN ('227.7203-6(a)(1)');
INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (80030, 14648, '252.227-7015 -- TECHNICAL DATA--COMMERCIAL ITEMS');
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 80030, prescription_id FROM Prescriptions WHERE prescription_name IN ('227.7102-4(a)(1)');
INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (80031, 14648, '252.227-7015 ALTERNATE I -- TECHNICAL DATA--COMMERCIAL ITEMS');
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 80031, prescription_id FROM Prescriptions WHERE prescription_name IN ('227.7102-4(a)(1)');
INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (80032, 14648, '252.227-7016 -- RIGHTS IN BID OR PROPOSAL INFORMATION');
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 80032, prescription_id FROM Prescriptions WHERE prescription_name IN ('227.7103-6(e)(1)','227.7104(e)(1)','227.7203-6(b)');
INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (80033, 14648, '252.227-7017 -- IDENTIFICATION AND ASSERTION OF USE, RELEASE, OR DISCLOSURE RESTRICTIONS');
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 80033, prescription_id FROM Prescriptions WHERE prescription_name IN ('227.7103-3(b)','227.7104(e)(2)','227.7203-3(a)');
INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (80034, 14648, '252.227-7018 -- RIGHTS IN NONCOMMERCIAL TECHNICAL DATA AND COMPUTER SOFTWARE--SMALL BUSINESS INNOVATION RESEARCH (SBIR) PROGRAM');
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 80034, prescription_id FROM Prescriptions WHERE prescription_name IN ('227.7104(a)');
INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (80035, 14648, '252.227-7018 ALTERNATE I -- RIGHTS IN NONCOMMERCIAL TECHNICAL DATA AND COMPUTER SOFTWARE--SMALL BUSINESS INNOVATION RESEARCH (SBIR) PROGRAM');
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 80035, prescription_id FROM Prescriptions WHERE prescription_name IN ('227.7104(a)');
INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (80036, 14648, '252.227-7019 -- VALIDATION OF ASSERTED RESTRICTIONS--COMPUTER SOFTWARE');
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 80036, prescription_id FROM Prescriptions WHERE prescription_name IN ('227.7104(e)(3)','227.7203-6(c)');
INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (80037, 14648, '252.227-7020 -- RIGHTS IN SPECIAL WORKS');
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 80037, prescription_id FROM Prescriptions WHERE prescription_name IN ('227.7105-3','227.7106(a)','227.7205(a)');
INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (80038, 14648, '252.227-7021 -- RIGHTS IN DATA--EXISTING WORKS');
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 80038, prescription_id FROM Prescriptions WHERE prescription_name IN ('227.7105-2(a)');
INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (80039, 14648, '252.227-7022 -- GOVERNMENT RIGHTS (UNLIMITED)');
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 80039, prescription_id FROM Prescriptions WHERE prescription_name IN ('227.7107-1(a)');
INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (80040, 14648, '252.227-7023 -- DRAWINGS AND OTHER DATA TO BECOME PROPERTY OF GOVERNMENT');
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 80040, prescription_id FROM Prescriptions WHERE prescription_name IN ('227.7107-1(b)');
INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (80041, 14648, '252.227-7024 -- NOTICE AND APPROVAL OF RESTRICTED DESIGN');
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 80041, prescription_id FROM Prescriptions WHERE prescription_name IN ('227.7107-3');
INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (80042, 14648, '252.227-7025 -- LIMITATIONS ON THE USE OR DISCLOSURE OF GOVERNMENT-FURNISHED INFORMATION MARKED WITH RESTRICTIVE LEGENDS');
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 80042, prescription_id FROM Prescriptions WHERE prescription_name IN ('227.7103-6(c)','227.7104(f)(1)','227.7203-6(d)');
INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (80043, 14648, '252.227-7026 -- DEFERRED DELIVERY OF TECHNICAL DATA OR COMPUTER SOFTWARE');
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 80043, prescription_id FROM Prescriptions WHERE prescription_name IN ('227.7103-8(a)');
INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (80044, 14648, '252.227-7027 -- DEFERRED ORDERING OF TECHNICAL DATA OR COMPUTER SOFTWARE');
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 80044, prescription_id FROM Prescriptions WHERE prescription_name IN ('227.7103-8(b)');
INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (80045, 14648, '252.227-7028 -- TECHNICAL DATA OR COMPUTER SOFTWARE PREVIOUSLY DELIVERED TO THE GOVERNMENT');
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 80045, prescription_id FROM Prescriptions WHERE prescription_name IN ('227.7103-6(d)','227.7104(f)(2)','227.7203-6(e)');
INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (80046, 14648, '252.227-7030 -- TECHNICAL DATA--WITHHOLDING OF PAYMENT');
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 80046, prescription_id FROM Prescriptions WHERE prescription_name IN ('227.7103-6(e)(2)','227.7104(e)(4)');
INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (80047, 14648, '252.227-7032 -- RIGHTS IN TECHNICAL DATA AND COMPUTER SOFTWARE (FOREIGN)');
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 80047, prescription_id FROM Prescriptions WHERE prescription_name IN ('227.7103-17');
INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (80048, 14648, '252.227-7033 -- RIGHTS IN SHOP DRAWINGS');
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 80048, prescription_id FROM Prescriptions WHERE prescription_name IN ('227.7107-1(c)');
INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (80049, 14648, '252.227-7037 -- VALIDATION OF RESTRICTIVE MARKINGS ON TECHNICAL DATA');
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 80049, prescription_id FROM Prescriptions WHERE prescription_name IN ('227.7102-4(c)','227.7103-6(e)(3)','227.7104(e)(5)','227.7203-6(f)');
INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (80050, 14648, '252.227-7038 -- PATENT RIGHTS--OWNERSHIP BY THE CONTRACTOR (LARGE BUSINESS)');
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 80050, prescription_id FROM Prescriptions WHERE prescription_name IN ('227.303(2)');
INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (80051, 14648, '252.227-7039 -- PATENTS--REPORTING OF SUBJECT INVENTIONS');
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 80051, prescription_id FROM Prescriptions WHERE prescription_name IN ('227.303(1)');
INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (80052, 14648, 'NONE OF THE ABOVE');
