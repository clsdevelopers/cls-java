/*
Clause Parser Run at Thu Aug 06 08:42:08 EDT 2015
(Without Correction Information)
*/

DELETE FROM Clause_Version_Changes
WHERE Clause_Version_Id = 4;

-- Question Version Change Scripts Start
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'Q', '[SAM EXEMPTION]', 'Changed', 'Text');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'Q', '[USE OF WARRANTY]', 'Changed', 'Choices');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'Q', '[PRICING REQUIREMENTS]', 'Changed', 'Choices');
-- Question Version Change Scripts End

-- Clause Version Change Scripts Start
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '252.203-7999 (DEVIATION 2015-00010)', 'Changed', 'Prescription');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '252.209-7007', 'Changed', 'Inclusion, Context, Fill-in');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '252.209-7997 (DEVIATION 2013-00006)', 'Changed', 'Prescription');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '252.211-7002', 'Changed', 'Effective Date');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '252.211-7005', 'Changed', 'Context, Fill-in');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '252.215-7004', 'Changed', 'Condition');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '252.215-7007', 'Changed', 'Inclusion');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '252.215-7008', 'Changed', 'Context, Fill-in');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '252.216-7008', 'Changed', 'Prescription');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '252.216-7010', 'Changed', 'Prescription');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '252.217-7000', 'Changed', 'Prescription');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '252.217-7004', 'Changed', 'Prescription');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '252.217-7005', 'Changed', 'Prescription');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '252.217-7006', 'Changed', 'Prescription');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '252.217-7007', 'Changed', 'Prescription');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '252.217-7008', 'Changed', 'Prescription');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '252.217-7009', 'Changed', 'Prescription');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '252.217-7010', 'Changed', 'Prescription');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '252.217-7011', 'Changed', 'Prescription');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '252.217-7012', 'Changed', 'Prescription');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '252.217-7013', 'Changed', 'Prescription');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '252.217-7014', 'Changed', 'Prescription');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '252.217-7015', 'Changed', 'Prescription');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '252.217-7016', 'Changed', 'Prescription');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '252.219-7003', 'Changed', 'Prescription');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '252.219-7009', 'Changed', 'Effective Date');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '252.223-7003', 'Changed', 'Prescription');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '252.223-7006', 'Changed', 'Prescription');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '252.225-7000', 'Changed', 'Prescription');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '252.225-7001', 'Changed', 'Prescription');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '252.225-7020', 'Changed', 'Prescription');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '252.225-7021', 'Changed', 'Prescription');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '252.225-7031', 'Changed', 'Inclusion');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '252.225-7032', 'Changed', 'Inclusion');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '252.225-7035', 'Changed', 'Prescription');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '252.225-7036', 'Changed', 'Prescription');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '252.225-7040', 'Changed', 'Prescription');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '252.225-7043', 'Changed', 'Effective Date, Prescription');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '252.225-7044', 'Changed', 'Prescription');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '252.225-7045', 'Changed', 'Prescription');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '252.225-7985 (DEVIATION 2015-00003)', 'Changed', 'Prescription');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '252.225-7987 (DEVIATION 2014-00016)', 'Changed', 'Prescription');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '252.225-7991 (DEVIATION 2014-00014)', 'Changed', 'Prescription');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '252.225-7992 (DEVIATION 2014-00014)', 'Changed', 'Prescription');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '252.225-7995 (DEVIATION 2015-00009)', 'Changed', 'Context, Prescription');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '252.225-7996 (DEVIATION 2014-00014)', 'Changed', 'Prescription');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '252.225-7998 (DEVIATION 2014-00014)', 'Changed', 'Prescription');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '252.225-7999 (DEVIATION 2014-00014)', 'Changed', 'Prescription');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '252.227-7001', 'Changed', 'Inclusion');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '252.227-7003', 'Changed', 'Inclusion');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '252.227-7007', 'Changed', 'Inclusion');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '252.227-7010', 'Changed', 'Inclusion');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '252.227-7013', 'Changed', 'Context, Prescription, Fill-in');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '252.227-7014', 'Changed', 'Context, Prescription, Fill-in');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '252.227-7015', 'Changed', 'Prescription');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '252.227-7018', 'Changed', 'Prescription');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '252.228-7006', 'Changed', 'Inclusion');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '252.229-7001', 'Changed', 'Inclusion, Prescription');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '252.229-7003', 'Changed', 'Inclusion, Context, Fill-in');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '252.229-7004', 'Changed', 'Inclusion, Context, Fill-in');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '252.229-7013', 'Changed', 'Inclusion');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '252.229-7998 (DEVIATION 2013-00016)', 'Changed', 'Inclusion, Context');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '252.229-7999 (DEVIATION 2013-00016)', 'Changed', 'Prescription');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '252.232-7001', 'Changed', 'Inclusion, Context, Fill-in');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '252.232-7007', 'Changed', 'Context, Fill-in');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '252.232-7012', 'Changed', 'Inclusion, Context, Fill-in');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '252.232-7014', 'Changed', 'Context');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '252.234-7004', 'Changed', 'Prescription');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '252.235-7001', 'Changed', 'Prescription');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '252.235-7003', 'Changed', 'Prescription');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '252.236-7001', 'Changed', 'Prescription');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '252.236-7006', 'Changed', 'Prescription');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '252.237-7002', 'Changed', 'Prescription');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '252.237-7004', 'Changed', 'Prescription');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '252.237-7005', 'Changed', 'Prescription');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '252.237-7006', 'Changed', 'Prescription');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '252.237-7007', 'Changed', 'Prescription');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '252.237-7008', 'Changed', 'Prescription');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '252.237-7009', 'Changed', 'Prescription');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '252.237-7011', 'Changed', 'Prescription');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '252.237-7016', 'Changed', 'Prescription');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '252.237-7024', 'Changed', 'Prescription');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '252.239-7004', 'Changed', 'Prescription');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '252.239-7005', 'Changed', 'Prescription');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '252.239-7006', 'Changed', 'Prescription');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '252.239-7007', 'Changed', 'Prescription');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '252.239-7008', 'Changed', 'Prescription');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '252.239-7012', 'Changed', 'Prescription');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '252.239-7014', 'Changed', 'Prescription');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '252.239-7015', 'Changed', 'Prescription');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '252.241-7000', 'Changed', 'Context, Fill-in');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '252.244-7001', 'Changed', 'Prescription');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '252.245-7004', 'Changed', 'Inclusion, Context, Fill-in');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '252.246-7001', 'Changed', 'Inclusion, Prescription');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '252.247-7001', 'Changed', 'Inclusion, Context, Fill-in');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '252.247-7008', 'Changed', 'Prescription');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '252.247-7023', 'Changed', 'Prescription');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '52.203-14', 'Changed', 'Rule');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '52.203-6', 'Changed', 'Prescription');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '52.204-1', 'Added', 'Added');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '52.204-16', 'Changed', 'Effective Date');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '52.204-18', 'Changed', 'Effective Date');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '52.204-2', 'Changed', 'Prescription');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '52.204-3', 'Changed', 'Condition, Rule');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '52.204-6', 'Changed', 'Prescription');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '52.204-7', 'Changed', 'Prescription');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '52.209-10', 'Changed', 'Rule');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '52.209-4', 'Changed', 'Prescription');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '52.211-10', 'Changed', 'Prescription');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '52.211-8', 'Changed', 'Prescription');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '52.211-9', 'Changed', 'Prescription');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '52.212-3', 'Changed', 'Prescription');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '52.212-4', 'Changed', 'Condition, Rule, Prescription');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '52.212-5', 'Changed', 'Condition, Rule');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '52.212-5 (DEVIATION 2013-00019)', 'Changed', 'Prescription');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '52.214-16', 'Changed', 'Inclusion');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '52.215-20', 'Changed', 'Prescription');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '52.215-21', 'Changed', 'Condition, Prescription');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '52.216-16', 'Changed', 'Prescription');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '52.216-17', 'Changed', 'Prescription');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '52.216-2', 'Changed', 'Context, Fill-in');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '52.216-25', 'Changed', 'Prescription');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '52.216-3', 'Changed', 'Context, Fill-in');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '52.219-28', 'Changed', 'Context, Prescription, Fill-in');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '52.219-7', 'Changed', 'Prescription');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '52.219-9 (DEVIATION 2013-00014)', 'Changed', 'Prescription');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '52.222-11', 'Changed', 'Prescription');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '52.222-12', 'Changed', 'Prescription');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '52.222-13', 'Changed', 'Prescription');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '52.222-14', 'Changed', 'Prescription');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '52.222-15', 'Changed', 'Prescription');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '52.222-18', 'Changed', 'Context, Fill-in');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '52.222-26', 'Changed', 'Prescription');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '52.222-33', 'Changed', 'Prescription');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '52.222-6', 'Changed', 'Prescription');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '52.222-7', 'Changed', 'Prescription');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '52.222-8', 'Changed', 'Prescription');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '52.222-9', 'Changed', 'Prescription');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '52.223-9', 'Changed', 'Prescription');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '52.225-22', 'Changed', 'Prescription');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '52.225-23', 'Changed', 'Prescription');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '52.225-24', 'Changed', 'Prescription');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '52.227-1', 'Changed', 'Prescription');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '52.227-3', 'Changed', 'Prescription');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '52.228-16', 'Changed', 'Prescription');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '52.229-2', 'Changed', 'Prescription');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '52.232-34', 'Changed', 'Prescription');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '52.232-36', 'Changed', 'Prescription');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '52.233-1', 'Changed', 'Prescription');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '52.233-3', 'Changed', 'Prescription');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '52.236-16', 'Changed', 'Prescription');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '52.236-21', 'Changed', 'Prescription');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '52.236-27', 'Changed', 'Prescription');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '52.237-4', 'Changed', 'Prescription');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '52.241-9', 'Changed', 'Prescription');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '52.242-4', 'Changed', 'Context, Fill-in');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '52.243-4', 'Changed', 'Prescription');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '52.246-2', 'Changed', 'Prescription');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '52.246-24', 'Changed', 'Prescription');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '52.246-6', 'Changed', 'Prescription');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '52.246-8', 'Changed', 'Prescription');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '52.247-1', 'Changed', 'Inclusion, Context, Fill-in');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '52.248-3', 'Changed', 'Prescription');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '52.249-3', 'Changed', 'Prescription');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '252.216-7010 Alternate I', 'Changed', 'Prescription');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '252.217-7000 Alternate I', 'Changed', 'Prescription');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '252.219-7003 Alternate I (DEVIATION 2013-00014)', 'Changed', 'Prescription');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '252.223-7006 Alternate I', 'Changed', 'Prescription');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '252.225-7000 Alternate I', 'Changed', 'Context, Prescription');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '252.225-7001 Alternate I', 'Changed', 'Inclusion, Prescription');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '252.225-7020 Alternate I', 'Changed', 'Context, Prescription');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '252.225-7021 Alternate II', 'Changed', 'Prescription');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '252.225-7035 Alternate I', 'Changed', 'Context, Prescription');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '252.225-7035 Alternate II', 'Changed', 'Context, Prescription');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '252.225-7035 Alternate III', 'Changed', 'Context, Prescription');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '252.225-7035 Alternate IV', 'Changed', 'Context, Prescription');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '252.225-7035 Alternate V', 'Changed', 'Context, Prescription');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '252.225-7036 Alternate I', 'Changed', 'Prescription');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '252.225-7036 Alternate II', 'Changed', 'Prescription');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '252.225-7036 Alternate III', 'Changed', 'Prescription');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '252.225-7036 Alternate IV', 'Changed', 'Prescription');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '252.225-7036 Alternate V', 'Changed', 'Prescription');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '252.225-7044 Alternate I', 'Changed', 'Prescription');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '252.225-7045 Alternate I', 'Changed', 'Prescription');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '252.225-7045 Alternate II', 'Changed', 'Prescription');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '252.225-7045 Alternate III', 'Changed', 'Prescription');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '252.227-7005 Alternate I', 'Changed', 'Inclusion');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '252.227-7005 Alternate II', 'Changed', 'Prescription');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '252.227-7013 Alternate II', 'Changed', 'Prescription');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '252.229-7001 Alternate I', 'Changed', 'Inclusion, Prescription');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '252.234-7003 Alternate I', 'Changed', 'Context');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '252.234-7004 Alternate I', 'Changed', 'Prescription');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '252.235-7003 Alternate I', 'Changed', 'Prescription');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '252.237-7002 Alternate I', 'Changed', 'Context');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '252.237-7016 Alternate I', 'Changed', 'Prescription');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '252.237-7016 Alternate II', 'Changed', 'Prescription');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '252.244-7001 Alternate I', 'Changed', 'Prescription');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '252.246-7001 Alternate I', 'Changed', 'Inclusion, Prescription');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '252.246-7001 Alternate II', 'Changed', 'Inclusion, Prescription');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '252.247-7008 Alternate I', 'Changed', 'Context, Prescription');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '252.247-7023 Alternate II', 'Changed', 'Prescription');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '52.204-2 Alternate II', 'Changed', 'Prescription');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '52.209-4 Alternate I', 'Changed', 'Prescription');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '52.209-4 Alternate II', 'Changed', 'Prescription');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '52.211-8 Alternate II', 'Changed', 'Prescription');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '52.211-8 Alternate III', 'Changed', 'Prescription');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '52.211-9 Alternate II', 'Changed', 'Prescription');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '52.211-9 Alternate III', 'Changed', 'Prescription');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '52.212-3 Alternate I', 'Changed', 'Context');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '52.212-4 Alternate I', 'Changed', 'Condition, Rule');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '52.215-20 Alternate II', 'Changed', 'Prescription');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '52.215-20 Alternate III', 'Changed', 'Prescription');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '52.215-20 Alternate IV', 'Changed', 'Prescription');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '52.215-21 Alternate I', 'Changed', 'Condition');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '52.215-21 Alternate II', 'Changed', 'Condition, Prescription');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '52.215-21 Alternate III', 'Changed', 'Condition, Prescription');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '52.215-21 Alternate IV', 'Changed', 'Condition, Prescription');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '52.216-16 Alternate I', 'Changed', 'Rule');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '52.219-6 Alternate II', 'Changed', 'Prescription');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '52.219-7 Alternate II', 'Changed', 'Prescription');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '52.219-9 Alternate II', 'Changed', 'Prescription');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '52.219-9 Alternate II (DEVIATION 2013-00014)', 'Changed', 'Prescription');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '52.219-9 Alternate III (DEVIATION 2013-00014)', 'Changed', 'Prescription');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '52.223-13 Alternate I', 'Changed', 'Prescription');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '52.223-9 Alternate I', 'Changed', 'Context');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '52.225-22 Alternate I', 'Changed', 'Prescription');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '52.225-23 Alternate I', 'Changed', 'Prescription');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '52.225-24 Alternate I', 'Changed', 'Prescription');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '52.225-24 Alternate II', 'Changed', 'Prescription');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '52.227-1 Alternate II', 'Changed', 'Prescription');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '52.227-14 Alternate II', 'Changed', 'Inclusion, Context, Fill-in');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '52.227-14 Alternate III', 'Changed', 'Context');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '52.227-14 Alternate IV', 'Changed', 'Prescription');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '52.236-13 Alternate I', 'Changed', 'Prescription');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '52.236-21 Alternate II', 'Changed', 'Inclusion, Context, Prescription, Fill-in');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '52.236-27 Alternate I', 'Changed', 'Context');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '52.246-2 Alternate II', 'Changed', 'Prescription');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '52.249-10 Alternate III', 'Changed', 'Prescription');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '52.249-2 Alternate III', 'Changed', 'Prescription');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '52.249-6 Alternate III', 'Changed', 'Prescription');
INSERT INTO Clause_Version_Changes (Clause_Version_Id, Previous_Clause_Version_Id, Is_Question_Or_Clause, Question_Or_Clause_Code, Change_Code, Change_Brief)
VALUES (4, 3, 'C', '52.249-6 Alternate V', 'Changed', 'Prescription');
-- Clause Version Change Scripts End

/*
*** End of SQL Scripts at Thu Aug 06 08:42:11 EDT 2015 ***
*/
