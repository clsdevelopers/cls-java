/*
Clause Parser Run at Sat Jul 11 07:01:23 EDT 2015
(Without Correction Information)
*/

-- ProduceClause.resolveIssues() Summary: Total(1212); Merged(14); Corrected(339); Error(1)
/* ===============================================
 * Prescriptions
   =============================================== */
/* ===============================================
 * Clause
   =============================================== */

UPDATE Clauses
SET is_basic_clause = 1
WHERE clause_id = 101562; -- 252.225-7035

UPDATE Clauses
SET editable_remarks = 'The clause at 252.228-7001 may be modified only as follows:\n(i) Include a modified definition of “aircraft” if the contract covers other than conventional types of winged aircraft, i.e., helicopters, vertical take-off or landing aircraft, lighter-than-air airships, unmanned aerial vehicles, or other nonconventional t. The modified definition should describe a stage of manufacture comparable to the standard definition.\n(ii) Modify “in the open” to include “hush houses,” test hangars and comparable structures, and other designated areas.\n(iii) Expressly define the “contractor''s premises” where the aircraft will be located during and for contract performance. These locations may include contract\npremises which are owned or leased by the contractor or subcontractor, or premises where the contractor or subcontractor is a permittee or licensee or has a right to use, including Government airfields.\n(iv) Revise paragraph (e)(3) of the clause to provide Government\nassumption of risk for transportation by conveyance on streets or highways when transportation is—\n(A) Limited to the vicinity of contractor premises; and\n(B) Incidental to work performed under the contract.' -- NULL
WHERE clause_id = 101648; -- 252.228-7001

UPDATE Clauses
SET is_basic_clause = 1
WHERE clause_id = 101767; -- 252.244-7001

/*
*** End of SQL Scripts at Sat Jul 11 07:01:26 EDT 2015 ***
*/
