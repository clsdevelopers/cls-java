/*
Clause Parser Run at Mon Jun 29 17:25:06 EDT 2015
(Without Correction Information)
*/
/* ===============================================
 * Prescriptions
   =============================================== */
/* ===============================================
 * Questions
   =============================================== */

UPDATE Question_Conditions
SET question_condition_sequence = 1370 -- 1210
WHERE question_condition_id = 5786; -- [TYPE OF SYSTEM]

UPDATE Question_Conditions
SET question_condition_sequence = 1400 -- 1240
WHERE question_condition_id = 5787; -- [CONTRACT TYPE]

UPDATE Question_Conditions
SET question_condition_sequence = 1470 -- 1310
WHERE question_condition_id = 5788; -- [QUANTITY VARIATION]

UPDATE Question_Conditions
SET question_condition_sequence = 1480 -- 1320
WHERE question_condition_id = 5789; -- [OPTIONS]

UPDATE Questions
SET question_group = 'D' -- I
WHERE question_id = 21259; -- [TRANSPORTATION REQUIREMENTS 1]
UPDATE Question_Conditions
SET question_condition_sequence = 1210 -- 1740
, original_condition_text = 'ONLY ASK IF THE USER SELECTS "SUPPLIES" UNDER "SUPPLIES OR SERVICES" OR "TRANSPORTATION/STORAGE SERVICES" UNDER "SERVICES" OR "THE SERVICE BEING PROCURED INCLUDES THE FURNISHING OF SUPPLIES" UNDER "SERVICE CHARACTERISTICS"' -- NULL
, condition_to_question = '"SUPPLIES" = [SUPPLIES OR SERVICES] OR "TRANSPORTATION/STORAGE SERVICES" = [SERVICES] OR "THE SERVICE BEING PROCURED INCLUDES THE FURNISHING OF SUPPLIES" = [SERVICE CHARACTERISTICS]' -- NULL
WHERE question_condition_id = 5867; -- [TRANSPORTATION REQUIREMENTS 1]

UPDATE Questions
SET question_group = 'D' -- I
WHERE question_id = 21260; -- [TRANSPORTATION REQUIREMENTS 2]
UPDATE Question_Conditions
SET question_condition_sequence = 1250 -- 1780
, original_condition_text = 'ONLY ASK IF THE USER SELECTS "SUPPLIES" UNDER "SUPPLIES OR SERVICES" OR "TRANSPORTATION/STORAGE SERVICES" UNDER "SERVICES" OR "THE SERVICE BEING PROCURED INCLUDES THE FURNISHING OF SUPPLIES" UNDER "SERVICE CHARACTERISTICS"' -- NULL
, condition_to_question = '"SUPPLIES" = [SUPPLIES OR SERVICES] OR "TRANSPORTATION/STORAGE SERVICES" = [SERVICES] OR "THE SERVICE BEING PROCURED INCLUDES THE FURNISHING OF SUPPLIES" = [SERVICE CHARACTERISTICS]' -- NULL
WHERE question_condition_id = 5868; -- [TRANSPORTATION REQUIREMENTS 2]

UPDATE Questions
SET question_group = 'D' -- I
WHERE question_id = 21261; -- [TRANSPORTATION REQUIREMENTS 3]
UPDATE Question_Conditions
SET question_condition_sequence = 1260 -- 1790
, original_condition_text = 'ONLY ASK IF THE USER SELECTS "SUPPLIES" UNDER "SUPPLIES OR SERVICES" OR "TRANSPORTATION/STORAGE SERVICES" UNDER "SERVICES" OR "THE SERVICE BEING PROCURED INCLUDES THE FURNISHING OF SUPPLIES" UNDER "SERVICE CHARACTERISTICS"' -- NULL
, condition_to_question = '"SUPPLIES" = [SUPPLIES OR SERVICES] OR "TRANSPORTATION/STORAGE SERVICES" = [SERVICES] OR "THE SERVICE BEING PROCURED INCLUDES THE FURNISHING OF SUPPLIES" = [SERVICE CHARACTERISTICS]' -- NULL
WHERE question_condition_id = 5869; -- [TRANSPORTATION REQUIREMENTS 3]

UPDATE Questions
SET question_group = 'D' -- I
WHERE question_id = 21262; -- [TRANSPORTATION REQUIREMENTS 4]
UPDATE Question_Conditions
SET question_condition_sequence = 1270 -- 1800
, original_condition_text = 'ONLY ASK IF THE USER SELECTS "SUPPLIES" UNDER "SUPPLIES OR SERVICES" OR "TRANSPORTATION/STORAGE SERVICES" UNDER "SERVICES" OR "THE SERVICE BEING PROCURED INCLUDES THE FURNISHING OF SUPPLIES" UNDER "SERVICE CHARACTERISTICS"' -- NULL
, condition_to_question = '"SUPPLIES" = [SUPPLIES OR SERVICES] OR "TRANSPORTATION/STORAGE SERVICES" = [SERVICES] OR "THE SERVICE BEING PROCURED INCLUDES THE FURNISHING OF SUPPLIES" = [SERVICE CHARACTERISTICS]' -- NULL
WHERE question_condition_id = 5870; -- [TRANSPORTATION REQUIREMENTS 4]

UPDATE Questions
SET question_group = 'D' -- I
WHERE question_id = 21263; -- [TRANSPORTATION REQUIREMENTS 5]
UPDATE Question_Conditions
SET question_condition_sequence = 1280 -- 1810
, original_condition_text = 'ONLY ASK IF THE USER SELECTS "SUPPLIES" UNDER "SUPPLIES OR SERVICES" OR "TRANSPORTATION/STORAGE SERVICES" UNDER "SERVICES" OR "THE SERVICE BEING PROCURED INCLUDES THE FURNISHING OF SUPPLIES" UNDER "SERVICE CHARACTERISTICS"' -- NULL
, condition_to_question = '"SUPPLIES" = [SUPPLIES OR SERVICES] OR "TRANSPORTATION/STORAGE SERVICES" = [SERVICES] OR "THE SERVICE BEING PROCURED INCLUDES THE FURNISHING OF SUPPLIES" = [SERVICE CHARACTERISTICS]' -- NULL
WHERE question_condition_id = 5871; -- [TRANSPORTATION REQUIREMENTS 5]

UPDATE Questions
SET question_group = 'D' -- I
WHERE question_id = 21264; -- [TRANSPORTATION REQUIREMENTS 6]
UPDATE Question_Conditions
SET question_condition_sequence = 1300 -- 1830
, original_condition_text = 'ONLY ASK IF THE USER SELECTS "SUPPLIES" UNDER "SUPPLIES OR SERVICES" OR "TRANSPORTATION/STORAGE SERVICES" UNDER "SERVICES" OR "THE SERVICE BEING PROCURED INCLUDES THE FURNISHING OF SUPPLIES" UNDER "SERVICE CHARACTERISTICS"' -- NULL
, condition_to_question = '"SUPPLIES" = [SUPPLIES OR SERVICES] OR "TRANSPORTATION/STORAGE SERVICES" = [SERVICES] OR "THE SERVICE BEING PROCURED INCLUDES THE FURNISHING OF SUPPLIES" = [SERVICE CHARACTERISTICS]' -- NULL
WHERE question_condition_id = 5872; -- [TRANSPORTATION REQUIREMENTS 6]

UPDATE Questions
SET question_group = 'D' -- I
WHERE question_id = 21265; -- [TRANSPORTATION REQUIREMENTS 7]
UPDATE Question_Conditions
SET question_condition_sequence = 1310 -- 1840
, original_condition_text = 'ONLY ASK IF THE USER SELECTS "SUPPLIES" UNDER "SUPPLIES OR SERVICES" OR "TRANSPORTATION/STORAGE SERVICES" UNDER "SERVICES" OR "THE SERVICE BEING PROCURED INCLUDES THE FURNISHING OF SUPPLIES" UNDER "SERVICE CHARACTERISTICS"' -- NULL
, condition_to_question = '"SUPPLIES" = [SUPPLIES OR SERVICES] OR "TRANSPORTATION/STORAGE SERVICES" = [SERVICES] OR "THE SERVICE BEING PROCURED INCLUDES THE FURNISHING OF SUPPLIES" = [SERVICE CHARACTERISTICS]' -- NULL
WHERE question_condition_id = 5873; -- [TRANSPORTATION REQUIREMENTS 7]

INSERT INTO Questions (clause_version_id, question_id, question_code, question_type, question_group, question_text)
VALUES (1, 23498, '[TRANSPORTATION REQUIREMENT 8]', 'M', 'D', 'WHAT IS THE METHOD OF SHIPPING?');
INSERT INTO Question_Conditions (question_condition_sequence, question_id, parent_question_id, original_condition_text, condition_to_question)
VALUES (1320, 23498, NULL
, 'ONLY ASK IF THE USER SELECTS "SUPPLIES" UNDER "SUPPLIES OR SERVICES" OR "TRANSPORTATION/STORAGE SERVICES" UNDER "SERVICES" OR "THE SERVICE BEING PROCURED INCLUDES THE FURNISHING OF SUPPLIES" UNDER "SERVICE CHARACTERISTICS"'
, '"SUPPLIES" = [SUPPLIES OR SERVICES] OR "TRANSPORTATION/STORAGE SERVICES" = [SERVICES] OR "THE SERVICE BEING PROCURED INCLUDES THE FURNISHING OF SUPPLIES" = [SERVICE CHARACTERISTICS]');
INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (122233, 23498, 'SHIPPED IN CARLOAD LOTS');
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 122233, prescription_id FROM Prescriptions WHERE prescription_name IN ('47.305-15(a)(2)','47.305-16(a)');
INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (122234, 23498, 'SHIPPED IN TRUCKLOADS');
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 122234, prescription_id FROM Prescriptions WHERE prescription_name IN ('47.305-16(a)');
INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (122235, 23498, 'SHIPPED IN CARLOADS OR TRUCKLOADS TO DOD INSTALLATIONS, OR CIVILIAN AGENCIES');
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 122235, prescription_id FROM Prescriptions WHERE prescription_name IN ('47.208-2');
INSERT INTO Question_Choices (question_choce_id, question_id, choice_text) VALUES (122236, 23498, 'NONE OF THE ABOVE');

UPDATE Questions
SET question_group = 'D' -- I
WHERE question_id = 21267; -- [TRANSPORTATION REQUIREMENTS 9]
UPDATE Question_Conditions
SET question_condition_sequence = 1330 -- 1860
, original_condition_text = 'ONLY ASK IF THE USER SELECTS "SUPPLIES" UNDER "SUPPLIES OR SERVICES" OR "TRANSPORTATION/STORAGE SERVICES" UNDER "SERVICES" OR "THE SERVICE BEING PROCURED INCLUDES THE FURNISHING OF SUPPLIES" UNDER "SERVICE CHARACTERISTICS"' -- NULL
, condition_to_question = '"SUPPLIES" = [SUPPLIES OR SERVICES] OR "TRANSPORTATION/STORAGE SERVICES" = [SERVICES] OR "THE SERVICE BEING PROCURED INCLUDES THE FURNISHING OF SUPPLIES" = [SERVICE CHARACTERISTICS]' -- NULL
WHERE question_condition_id = 5875; -- [TRANSPORTATION REQUIREMENTS 9]

UPDATE Questions
SET question_group = 'D' -- I
WHERE question_id = 21268; -- [TRANSPORTATION REQUIREMENTS 10]
UPDATE Question_Conditions
SET question_condition_sequence = 1340 -- 1870
, original_condition_text = 'ONLY ASK IF THE USER SELECTS "SUPPLIES" UNDER "SUPPLIES OR SERVICES" OR "TRANSPORTATION/STORAGE SERVICES" UNDER "SERVICES" OR "THE SERVICE BEING PROCURED INCLUDES THE FURNISHING OF SUPPLIES" UNDER "SERVICE CHARACTERISTICS"' -- NULL
, condition_to_question = '"SUPPLIES" = [SUPPLIES OR SERVICES] OR "TRANSPORTATION/STORAGE SERVICES" = [SERVICES] OR "THE SERVICE BEING PROCURED INCLUDES THE FURNISHING OF SUPPLIES" = [SERVICE CHARACTERISTICS]' -- NULL
WHERE question_condition_id = 5876; -- [TRANSPORTATION REQUIREMENTS 10]

UPDATE Questions
SET question_group = 'D' -- I
WHERE question_id = 21269; -- [TRANSPORTATION REQUIREMENTS 11]
UPDATE Question_Conditions
SET question_condition_sequence = 1350 -- 1880
, original_condition_text = 'ONLY ASK IF THE USER SELECTS "SUPPLIES" UNDER "SUPPLIES OR SERVICES" OR "TRANSPORTATION/STORAGE SERVICES" UNDER "SERVICES" OR "THE SERVICE BEING PROCURED INCLUDES THE FURNISHING OF SUPPLIES" UNDER "SERVICE CHARACTERISTICS"' -- NULL
, condition_to_question = '"SUPPLIES" = [SUPPLIES OR SERVICES] OR "TRANSPORTATION/STORAGE SERVICES" = [SERVICES] OR "THE SERVICE BEING PROCURED INCLUDES THE FURNISHING OF SUPPLIES" = [SERVICE CHARACTERISTICS]' -- NULL
WHERE question_condition_id = 5877; -- [TRANSPORTATION REQUIREMENTS 11]

UPDATE Questions
SET question_group = 'D' -- I
WHERE question_id = 21270; -- [SHIPPING STATEMENTS]
UPDATE Question_Conditions
SET question_condition_sequence = 1360 -- 1890
, original_condition_text = 'ONLY ASK IF THE USER SELECTS "SUPPLIES" UNDER "SUPPLIES OR SERVICES" OR "TRANSPORTATION/STORAGE SERVICES" UNDER "SERVICES" OR "THE SERVICE BEING PROCURED INCLUDES THE FURNISHING OF SUPPLIES" UNDER "SERVICE CHARACTERISTICS"' -- NULL
, condition_to_question = '"SUPPLIES" = [SUPPLIES OR SERVICES] OR "TRANSPORTATION/STORAGE SERVICES" = [SERVICES] OR "THE SERVICE BEING PROCURED INCLUDES THE FURNISHING OF SUPPLIES" = [SERVICE CHARACTERISTICS]' -- NULL
WHERE question_condition_id = 5878; -- [SHIPPING STATEMENTS]

UPDATE Question_Conditions
SET question_condition_sequence = 1390 -- 1230
WHERE question_condition_id = 5811; -- [R&D TECHNOLOGIES]

UPDATE Question_Conditions
SET question_condition_sequence = 1380 -- 1220
WHERE question_condition_id = 5822; -- [OSD APPROVAL FOR DATA SYSTEM]

UPDATE Question_Conditions
SET question_condition_sequence = 1420 -- 1260
WHERE question_condition_id = 5823; -- [SWING FROM TARGET FEE]

UPDATE Question_Conditions
SET question_condition_sequence = 1440 -- 1280
WHERE question_condition_id = 5824; -- [SMALL BUSINESS AWARD FEE]

UPDATE Question_Conditions
SET question_condition_sequence = 1450 -- 1290
WHERE question_condition_id = 5825; -- [MAJOR DESIGN OR DEVELOPMENT]

UPDATE Question_Conditions
SET question_condition_sequence = 1460 -- 1300
WHERE question_condition_id = 5826; -- [PCO EPA DETERMINATION]

UPDATE Question_Conditions
SET question_condition_sequence = 1490 -- 1330
WHERE question_condition_id = 5827; -- [OPTION CONDITIONS]

UPDATE Questions
SET question_group = 'D' -- I
WHERE question_id = 21271; -- [FOB ORIGIN DESIGNATIONS]
UPDATE Question_Conditions
SET question_condition_sequence = 1220 -- 1750
WHERE question_condition_id = 5879; -- [FOB ORIGIN DESIGNATIONS]

UPDATE Questions
SET question_group = 'D' -- I
WHERE question_id = 21272; -- [WITHIN CONSIGNEE'S PREMISES]
UPDATE Question_Conditions
SET question_condition_sequence = 1230 -- 1760
WHERE question_condition_id = 5880; -- [WITHIN CONSIGNEE'S PREMISES]

UPDATE Questions
SET question_group = 'D' -- I
WHERE question_id = 21273; -- [FOB DESIGNATION]
UPDATE Question_Conditions
SET question_condition_sequence = 1240 -- 1770
WHERE question_condition_id = 5881; -- [FOB DESIGNATION]

UPDATE Questions
SET question_group = 'D' -- I
WHERE question_id = 21274; -- [CARGO PREFERENCE ACT]
UPDATE Question_Conditions
SET question_condition_sequence = 1290 -- 1820
WHERE question_condition_id = 5882; -- [CARGO PREFERENCE ACT]

UPDATE Question_Conditions
SET question_condition_sequence = 1500 -- 1340
WHERE question_condition_id = 5828; -- [PERFORMANCE REQUIREMENTS 1]

UPDATE Question_Conditions
SET question_condition_sequence = 1520 -- 1360
WHERE question_condition_id = 5829; -- [PERFORMANCE REQUIREMENTS 2]

UPDATE Question_Conditions
SET question_condition_sequence = 1560 -- 1400
WHERE question_condition_id = 5830; -- [LABOR REQUIREMENTS 1]

UPDATE Question_Conditions
SET question_condition_sequence = 1570 -- 1410
WHERE question_condition_id = 5831; -- [LABOR REQUIREMENTS 2]

UPDATE Question_Conditions
SET question_condition_sequence = 1600 -- 1440
WHERE question_condition_id = 5832; -- [LABOR REQUIREMENTS 3]

UPDATE Question_Conditions
SET question_condition_sequence = 1620 -- 1460
WHERE question_condition_id = 5833; -- [CONTRACTOR MATERIAL]

UPDATE Question_Conditions
SET question_condition_sequence = 1650 -- 1490
WHERE question_condition_id = 5834; -- [SUBCONTRACTING]

UPDATE Question_Conditions
SET question_condition_sequence = 1670 -- 1510
WHERE question_condition_id = 5835; -- [ENVIRONMENT]

UPDATE Question_Conditions
SET question_condition_sequence = 1680 -- 1520
WHERE question_condition_id = 5836; -- [INTEREST ON PARTIAL PAYMENTS]

UPDATE Question_Conditions
SET question_condition_sequence = 1690 -- 1530
WHERE question_condition_id = 5837; -- [TERMINATION]

UPDATE Question_Conditions
SET question_condition_sequence = 1510 -- 1350
WHERE question_condition_id = 5838; -- [TECHNICAL CHANGES]

UPDATE Question_Conditions
SET question_condition_sequence = 1530 -- 1370
WHERE question_condition_id = 5839; -- [VALUE ENGINEERING CONDITIONS]

UPDATE Question_Conditions
SET question_condition_sequence = 1540 -- 1380
WHERE question_condition_id = 5840; -- [FIRST ARTICLE CONDITIONS]

UPDATE Question_Conditions
SET question_condition_sequence = 1550 -- 1390
WHERE question_condition_id = 5841; -- [DD FORM 1494]

UPDATE Question_Conditions
SET question_condition_sequence = 1580 -- 1420
WHERE question_condition_id = 5842; -- [LABOR ACTS]

UPDATE Question_Conditions
SET question_condition_sequence = 1590 -- 1430
WHERE question_condition_id = 5843; -- [UNEMPLOYMENT RATE]

UPDATE Question_Conditions
SET question_condition_sequence = 1610 -- 1450
WHERE question_condition_id = 5844; -- [DRUG FREE WORKFORCE REQUIRED]

UPDATE Question_Conditions
SET question_condition_sequence = 1630 -- 1470
WHERE question_condition_id = 5845; -- [GOVERNMENT FURNISHED PROPERTY]

UPDATE Question_Conditions
SET question_condition_sequence = 1640 -- 1480
WHERE question_condition_id = 5846; -- [GOVERNMENT SUPPLY STATEMENT]

UPDATE Question_Conditions
SET question_condition_sequence = 1660 -- 1500
WHERE question_condition_id = 5847; -- [SUBCONTRACTING PLAN CONDITIONS]

UPDATE Question_Conditions
SET question_condition_sequence = 1700 -- 1540
WHERE question_condition_id = 5848; -- [END ITEM MARKING]

UPDATE Question_Conditions
SET question_condition_sequence = 1710 -- 1550
WHERE question_condition_id = 5849; -- [PRESERVATION AND PACKING SPECIFICATIONS]

UPDATE Question_Conditions
SET question_condition_sequence = 1720 -- 1560
WHERE question_condition_id = 5850; -- [INSPECTION AND ACCEPTANCE REQUIREMENTS]

UPDATE Question_Conditions
SET question_condition_sequence = 1730 -- 1570
WHERE question_condition_id = 5851; -- [INSPECTION AND ACCEPTANCE REQUIREMENTS 2]

UPDATE Question_Conditions
SET question_condition_sequence = 1740 -- 1575
WHERE question_condition_id = 5852; -- [USE OF WARRANTY]

UPDATE Question_Conditions
SET question_condition_sequence = 1750 -- 1580
WHERE question_condition_id = 5853; -- [WARRANTY CONDITIONS]

UPDATE Question_Conditions
SET question_condition_sequence = 1760 -- 1590
WHERE question_condition_id = 5854; -- [WARRANTY RECOVERY AND TRANSPORTATION]

UPDATE Question_Conditions
SET question_condition_sequence = 1770 -- 1600
WHERE question_condition_id = 5855; -- [WARRANTY DESCRIPTION]

UPDATE Question_Conditions
SET question_condition_sequence = 1780 -- 1610
WHERE question_condition_id = 5856; -- [PERIOD OF PERFORMANCE]

UPDATE Question_Conditions
SET question_condition_sequence = 1790 -- 1620
WHERE question_condition_id = 5857; -- [PLACE OF PERFORMANCE]

UPDATE Question_Conditions
SET question_condition_sequence = 1840 -- 1670
WHERE question_condition_id = 5858; -- [OUTSIDE U.S.]

UPDATE Question_Conditions
SET question_condition_sequence = 1850 -- 1680
WHERE question_condition_id = 5859; -- [PLACE TYPE]

UPDATE Question_Conditions
SET question_condition_sequence = 1870 -- 1700
WHERE question_condition_id = 5860; -- [DELIVERY BASIS]

UPDATE Question_Conditions
SET question_condition_sequence = 1880 -- 1710
WHERE question_condition_id = 5861; -- [CONTINUED PERFORMANCE]

UPDATE Question_Conditions
SET question_condition_sequence = 1890 -- 1720
WHERE question_condition_id = 5862; -- [LIQUIDATED DAMAGES]

UPDATE Question_Conditions
SET question_condition_sequence = 1900 -- 1730
WHERE question_condition_id = 5863; -- [SEPARATE DELIVERABLES]

UPDATE Question_Conditions
SET question_condition_sequence = 1800 -- 1630
WHERE question_condition_id = 5864; -- [FOREIGN COUNTRY]

UPDATE Question_Conditions
SET question_condition_sequence = 1820 -- 1650
WHERE question_condition_id = 5865; -- [MILITARY TECHNICAL AGREEMENT]

UPDATE Question_Conditions
SET question_condition_sequence = 1830 -- 1660
WHERE question_condition_id = 5866; -- [STATE]

UPDATE Question_Conditions
SET question_condition_sequence = 1910 -- 1900
WHERE question_condition_id = 5883; -- [ACO RESPONSIBILITIES]

UPDATE Question_Conditions
SET question_condition_sequence = 1930 -- 1920
WHERE question_condition_id = 5884; -- [FUNDING FY]

UPDATE Question_Conditions
SET question_condition_sequence = 1980 -- 1970
WHERE question_condition_id = 5885; -- [ACRN ASSIGNMENT]

UPDATE Question_Conditions
SET question_condition_sequence = 2010 -- 2000
WHERE question_condition_id = 5886; -- [FULLY OR INCREMENTAL FUNDING]

UPDATE Question_Conditions
SET question_condition_sequence = 2020 -- 2010
WHERE question_condition_id = 5887; -- [FINANCING ARRANGEMENTS]

UPDATE Question_Conditions
SET question_condition_sequence = 2060 -- 2050
WHERE question_condition_id = 5888; -- [BILLING ARRANGEMENTS]

UPDATE Question_Conditions
SET question_condition_sequence = 2080 -- 2070
WHERE question_condition_id = 5889; -- [PAYMENT INSTRUCTIONS]

UPDATE Question_Conditions
SET question_condition_sequence = 2090 -- 2080
WHERE question_condition_id = 5890; -- [RAPID LIQUIDATION]

UPDATE Question_Conditions
SET question_condition_sequence = 2100 -- 2090
WHERE question_condition_id = 5891; -- [CONTRACT PRIOR TO FUNDS]

UPDATE Question_Conditions
SET question_condition_sequence = 1920 -- 1910
WHERE question_condition_id = 5892; -- [BASE, POST, CAMP OR STATION]

UPDATE Question_Conditions
SET question_condition_sequence = 1940 -- 1930
WHERE question_condition_id = 5893; -- [TYPE OF FUNDS]

UPDATE Question_Conditions
SET question_condition_sequence = 1950 -- 1940
WHERE question_condition_id = 5894; -- [LEGISLATIVE SOURCE OF FUNDS]

UPDATE Question_Conditions
SET question_condition_sequence = 1960 -- 1950
WHERE question_condition_id = 5895; -- [ANNUAL FUNDS EXTENDED]

UPDATE Question_Conditions
SET question_condition_sequence = 1970 -- 1960
WHERE question_condition_id = 5896; -- [LIABILITY FOR SPECIAL COSTS]

UPDATE Question_Conditions
SET question_condition_sequence = 1990 -- 1980
WHERE question_condition_id = 5897; -- [ACRN CONTRACT WIDE]

UPDATE Question_Conditions
SET question_condition_sequence = 2000 -- 1990
WHERE question_condition_id = 5898; -- [ACRN BY LINE ITEM]

UPDATE Question_Conditions
SET question_condition_sequence = 2030 -- 2020
WHERE question_condition_id = 5899; -- [ADVANCED PAYMENT CONDITIONS]

UPDATE Question_Conditions
SET question_condition_sequence = 2040 -- 2030
WHERE question_condition_id = 5900; -- [PROGRESS PAYMENT CONDITIONS]

UPDATE Question_Conditions
SET question_condition_sequence = 2050 -- 2040
WHERE question_condition_id = 5901; -- [BASIS OF PERFORMANCE BASED PAYMENTS]

UPDATE Question_Conditions
SET question_condition_sequence = 2070 -- 2060
WHERE question_condition_id = 5902; -- [EFT EXCEPTIONS]

UPDATE Question_Conditions
SET question_condition_sequence = 2110 -- 2100
WHERE question_condition_id = 5903; -- [INSURANCE CONDITIONS]

UPDATE Question_Conditions
SET question_condition_sequence = 2120 -- 2110
WHERE question_condition_id = 5904; -- [BID GUARANTEE OR BOND]

UPDATE Question_Conditions
SET question_condition_sequence = 2130 -- 2120
WHERE question_condition_id = 5905; -- [NO TIME TO PROCESS BUY AMERICAN DETERMINATION]

UPDATE Question_Conditions
SET question_condition_sequence = 2140 -- 2130
WHERE question_condition_id = 5906; -- [DISPUTES ACT]

UPDATE Question_Conditions
SET question_condition_sequence = 2150 -- 2140
WHERE question_condition_id = 5907; -- [WAIVERS]

UPDATE Question_Conditions
SET question_condition_sequence = 2160 -- 2150
WHERE question_condition_id = 5908; -- [EXEMPTIONS]

UPDATE Question_Conditions
SET question_condition_sequence = 2170 -- 2160
WHERE question_condition_id = 5909; -- [EXCEPTIONS]

UPDATE Question_Conditions
SET question_condition_sequence = 2180 -- 2170
WHERE question_condition_id = 5910; -- [PATENT COUNCIL RECOMMENDED]

UPDATE Question_Conditions
SET question_condition_sequence = 2190 -- 2180
WHERE question_condition_id = 5911; -- [PRICING REQUIREMENTS]

UPDATE Question_Conditions
SET question_condition_sequence = 2240 -- 2230
WHERE question_condition_id = 5912; -- [CAS]

UPDATE Question_Conditions
SET question_condition_sequence = 2250 -- 2240
WHERE question_condition_id = 5913; -- [REGULATION PRICING]

UPDATE Question_Conditions
SET question_condition_sequence = 2260 -- 2250
WHERE question_condition_id = 5914; -- [DIRECT COST]

UPDATE Question_Conditions
SET question_condition_sequence = 2270 -- 2260
WHERE question_condition_id = 5915; -- [INDIRECT COST]

UPDATE Question_Conditions
SET question_condition_sequence = 2280 -- 2270
WHERE question_condition_id = 5916; -- [PRICING LIMITATIONS]

UPDATE Question_Conditions
SET question_condition_sequence = 2290 -- 2280
WHERE question_condition_id = 5917; -- [TRANSPORTATION COST]

UPDATE Question_Conditions
SET question_condition_sequence = 2300 -- 2290
WHERE question_condition_id = 5918; -- [PROPOSAL REQUIREMENTS]

UPDATE Question_Conditions
SET question_condition_sequence = 2310 -- 2300
WHERE question_condition_id = 5919; -- [AWARD SCENARIOS]

UPDATE Question_Conditions
SET question_condition_sequence = 2200 -- 2190
WHERE question_condition_id = 5920; -- [EXCESS PASS-THROUGH CHARGES]

UPDATE Question_Conditions
SET question_condition_sequence = 2210 -- 2200
WHERE question_condition_id = 5921; -- [UCA]

UPDATE Question_Conditions
SET question_condition_sequence = 2220 -- 2210
WHERE question_condition_id = 5922; -- [INAPPROPRIATE CONTINGENCY]

UPDATE Question_Conditions
SET question_condition_sequence = 2230 -- 2220
WHERE question_condition_id = 5923; -- [CURRENCY]

UPDATE Question_Conditions
SET question_condition_sequence = 1410 -- 1250
WHERE question_condition_id = 5962; -- [COST REIMBURSEMENT]

UPDATE Question_Conditions
SET question_condition_sequence = 1430 -- 1270
WHERE question_condition_id = 5963; -- [FIXED PRICE]

UPDATE Question_Conditions
SET question_condition_sequence = 1810 -- 1640
WHERE question_condition_id = 5965; -- [UNITED STATES OUTLYING AREAS]

UPDATE Question_Conditions
SET question_condition_sequence = 1860 -- 1690
WHERE question_condition_id = 5966; -- [GOVERNMENT OWNED OR LEASED FACILITIES/INSTALLATIONS]


-- [USER TO SELECT AT LEAST ONE]
DELETE FROM Question_Choice_Prescriptions
WHERE Question_Choce_Id in
(SELECT Question_Choce_Id FROM Question_Choices WHERE question_id = 22421);
DELETE FROM Question_Choices WHERE question_id = 22421;
DELETE FROM Question_Conditions WHERE question_id = 22421;
DELETE FROM Questions WHERE question_id = 22421;

-- ProduceClause.resolveIssues() Summary: Total(1212); Merged(13); Corrected(336); Error(1)
/* ===============================================
 * Clause
   =============================================== */


DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 27177; -- textbox_252.211-7005[0]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 27178; -- textbox_252.211-7005[1]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 27179; -- textbox_252.211-7005[2]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 27180; -- textbox_252.211-7005[3]


DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 27218; -- textbox_252.216-7006[0]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 27219; -- textbox_252.216-7006[1]


DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 27221; -- textbox_252.216-7008[0]


DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 27226; -- textbox_252.217-7026[0]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 27227; -- textbox_252.217-7026[1]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 27228; -- textbox_252.217-7026[2]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 27229; -- textbox_252.217-7026[3]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 27230; -- textbox_252.217-7026[4]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 27231; -- textbox_252.217-7026[5]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 27232; -- textbox_252.217-7026[6]


DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 27241; -- textbox_252.219-7009[0]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 27242; -- textbox_252.219-7009[1]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 27243; -- textbox_252.219-7009[2]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 27244; -- textbox_252.219-7009[3]


DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 27261; -- textbox_252.225-7000[0]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 27262; -- textbox_252.225-7000[1]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 27263; -- textbox_252.225-7000[2]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 27264; -- textbox_252.225-7000[3]


DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 27265; -- textbox_252.225-7010[0]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 27266; -- textbox_252.225-7010[1]


DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 27267; -- textbox_252.225-7018[0]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 27277; -- textbox_252.225-7018[10]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 27278; -- textbox_252.225-7018[11]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 27279; -- textbox_252.225-7018[12]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 27280; -- textbox_252.225-7018[13]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 27281; -- textbox_252.225-7018[14]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 27282; -- textbox_252.225-7018[15]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 27283; -- textbox_252.225-7018[16]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 27268; -- textbox_252.225-7018[1]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 27269; -- textbox_252.225-7018[2]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 27270; -- textbox_252.225-7018[3]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 27271; -- textbox_252.225-7018[4]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 27272; -- textbox_252.225-7018[5]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 27273; -- textbox_252.225-7018[6]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 27274; -- textbox_252.225-7018[7]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 27275; -- textbox_252.225-7018[8]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 27276; -- textbox_252.225-7018[9]


DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 27285; -- textbox_252.225-7035[0]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 27286; -- textbox_252.225-7035[1]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 27287; -- textbox_252.225-7035[2]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 27288; -- textbox_252.225-7035[3]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 27289; -- textbox_252.225-7035[4]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 27290; -- textbox_252.225-7035[5]


DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 27381; -- textbox_252.227-7018[0]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 27391; -- textbox_252.227-7018[10]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 27392; -- textbox_252.227-7018[11]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 27393; -- textbox_252.227-7018[12]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 27394; -- textbox_252.227-7018[13]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 27395; -- textbox_252.227-7018[14]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 27396; -- textbox_252.227-7018[15]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 27397; -- textbox_252.227-7018[16]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 27382; -- textbox_252.227-7018[1]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 27383; -- textbox_252.227-7018[2]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 27384; -- textbox_252.227-7018[3]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 27385; -- textbox_252.227-7018[4]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 27386; -- textbox_252.227-7018[5]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 27387; -- textbox_252.227-7018[6]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 27388; -- textbox_252.227-7018[7]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 27389; -- textbox_252.227-7018[8]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 27390; -- textbox_252.227-7018[9]


DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 27401; -- textbox_252.232-7000[0]


DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 27402; -- textbox_252.232-7006[0]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 27403; -- textbox_252.232-7006[1]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 27404; -- textbox_252.232-7006[2]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 27405; -- textbox_252.232-7006[3]


DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 27406; -- textbox_252.232-7007[0]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 27407; -- textbox_252.232-7007[1]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 27408; -- textbox_252.232-7007[2]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 27409; -- textbox_252.232-7007[3]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 27410; -- textbox_252.232-7007[4]


DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 27411; -- textbox_252.234-7002[0]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 27412; -- textbox_252.234-7002[1]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 27413; -- textbox_252.234-7002[2]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 27414; -- textbox_252.234-7002[3]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 27415; -- textbox_252.234-7002[4]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 27416; -- textbox_252.234-7002[5]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 27417; -- textbox_252.234-7002[6]


DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 27422; -- textbox_252.236-7010[0]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 27423; -- textbox_252.236-7010[1]


DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 27424; -- textbox_252.236-7012[0]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 27425; -- textbox_252.236-7012[1]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 27426; -- textbox_252.236-7012[2]


DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 27440; -- textbox_252.245-7004[0]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 27441; -- textbox_252.245-7004[1]


DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 27448; -- textbox_252.247-7001[0]


DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 27456; -- textbox_252.247-7022[0]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 27457; -- textbox_252.247-7022[1]


DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 27463; -- textbox_52.203-2[0]


DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 27468; -- checkbox_52.204-17[0]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 27469; -- checkbox_52.204-17[1]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 27470; -- checkbox_52.204-17[2]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 27471; -- checkbox_52.204-17[3]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 27464; -- textbox_52.204-17[0]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 27465; -- textbox_52.204-17[1]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 27466; -- textbox_52.204-17[2]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 27467; -- textbox_52.204-17[3]


DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 27472; -- textbox_52.204-3[0]


DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 27505; -- checkbox_52.209-7[0]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 27506; -- checkbox_52.209-7[1]


DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 27683; -- textbox_52.216-2[0]


DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 27696; -- textbox_52.216-3[0]


DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 27742; -- textbox_52.219-2[0]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 27743; -- textbox_52.219-2[1]


DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 27755; -- textbox_52.223-11[0]


DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 27766; -- textbox_52.225-2[0]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 27767; -- textbox_52.225-2[1]


DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 27768; -- textbox_52.225-6[0]


DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 27769; -- textbox_52.225-8[0]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 27770; -- textbox_52.225-8[1]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 27771; -- textbox_52.225-8[2]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 27772; -- textbox_52.225-8[3]


DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 27773; -- checkbox_52.226-2[0]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 27774; -- checkbox_52.226-2[1]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 27775; -- checkbox_52.226-2[2]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 27776; -- checkbox_52.226-2[3]


DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 27781; -- textbox_52.227-15[0]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 27782; -- textbox_52.227-15[1]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 27783; -- textbox_52.227-15[2]


DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 27784; -- textbox_52.227-19[0]


DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 27785; -- textbox_52.227-20[0]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 27786; -- textbox_52.227-20[1]


DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 27787; -- textbox_52.227-21[0]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 27788; -- textbox_52.227-21[1]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 27789; -- textbox_52.227-21[2]


DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 27805; -- textbox_52.228-14[0]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 27815; -- textbox_52.228-14[10]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 27816; -- textbox_52.228-14[11]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 27817; -- textbox_52.228-14[12]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 27818; -- textbox_52.228-14[13]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 27819; -- textbox_52.228-14[14]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 27820; -- textbox_52.228-14[15]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 27821; -- textbox_52.228-14[16]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 27822; -- textbox_52.228-14[17]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 27823; -- textbox_52.228-14[18]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 27824; -- textbox_52.228-14[19]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 27806; -- textbox_52.228-14[1]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 27825; -- textbox_52.228-14[20]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 27826; -- textbox_52.228-14[21]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 27827; -- textbox_52.228-14[22]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 27828; -- textbox_52.228-14[23]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 27829; -- textbox_52.228-14[24]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 27830; -- textbox_52.228-14[25]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 27831; -- textbox_52.228-14[26]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 27832; -- textbox_52.228-14[27]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 27833; -- textbox_52.228-14[28]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 27834; -- textbox_52.228-14[29]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 27807; -- textbox_52.228-14[2]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 27835; -- textbox_52.228-14[30]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 27836; -- textbox_52.228-14[31]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 27837; -- textbox_52.228-14[32]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 27808; -- textbox_52.228-14[3]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 27809; -- textbox_52.228-14[4]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 27810; -- textbox_52.228-14[5]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 27811; -- textbox_52.228-14[6]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 27812; -- textbox_52.228-14[7]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 27813; -- textbox_52.228-14[8]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 27814; -- textbox_52.228-14[9]


DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 27856; -- textbox_52.229-2[0]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 27857; -- textbox_52.229-2[1]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 27858; -- textbox_52.229-2[2]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 27859; -- textbox_52.229-2[3]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 27860; -- textbox_52.229-2[4]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 27861; -- textbox_52.229-2[5]


DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 27871; -- textbox_52.230-1[0]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 27872; -- textbox_52.230-1[1]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 27873; -- textbox_52.230-1[2]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 27874; -- textbox_52.230-1[3]


DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 27899; -- textbox_52.232-5[0]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 27900; -- textbox_52.232-5[1]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 27901; -- textbox_52.232-5[2]


DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 27949; -- textbox_52.242-4[0]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 27950; -- textbox_52.242-4[1]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 27951; -- textbox_52.242-4[2]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 27952; -- textbox_52.242-4[3]


DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 27963; -- textbox_52.246-15[0]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 27964; -- textbox_52.246-15[1]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 27965; -- textbox_52.246-15[2]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 27966; -- textbox_52.246-15[3]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 27967; -- textbox_52.246-15[4]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 27968; -- textbox_52.246-15[5]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 27969; -- textbox_52.246-15[6]

UPDATE Clauses
SET clause_conditions = '(A) DOCUMENT TYPE IS: SOLICITATION\n(B) DOCUMENT TYPE IS: AWARD\n(C) SUPPLIES OR SERVICES IS: SUPPLIES\n(D) TRANSPORTATION REQUIREMENTS 5 IS: RAIL\n(E) TRANSPORTATION REQUIREMENT 8 IS: SHIPPED IN CARLOAD LOTS' -- '(A) DOCUMENT TYPE IS: SOLICITATION\n(B) DOCUMENT TYPE IS: AWARD\n(C) SUPPLIES OR SERVICES IS: SUPPLIES\n(D) TRANSPORTATION REQUIREMENTS 5 IS: RAIL\n(E) USER TO SELECT AT LEAST ONE IS: SHIPPED IN CARLOAD LOTS'
WHERE clause_id = 80949; -- 52.247-58

UPDATE Clauses
SET clause_conditions = '(A) DOCUMENT TYPE IS: SOLICITATION\n(B) DOCUMENT TYPE IS: AWARD\n(C) TRANSPORTATION REQUIREMENTS 1 IS: F.O.B. ORIGIN\n(D) TRANSPORTATION REQUIREMENT 8 IS: SHIPPED IN TRUCKLOADS\n(E) TRANSPORTATION REQUIREMENT 8 IS: SHIPPED IN CARLOAD LOTS' -- '(A) DOCUMENT TYPE IS: SOLICITATION\n(B) DOCUMENT TYPE IS: AWARD\n(C) TRANSPORTATION REQUIREMENTS 1 IS: F.O.B. ORIGIN\n(D) USER TO SELECT AT LEAST ONE IS: SHIPPED IN TRUCKLOADS\n(E) USER TO SELECT AT LEAST ONE IS: SHIPPED IN CARLOAD LOTS'
WHERE clause_id = 80950; -- 52.247-59


DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 28046; -- textbox_52.247-63[0]

UPDATE Clauses
SET clause_conditions = '(A) DOCUMENT TYPE IS: SOLICITATION\n(B) DOCUMENT TYPE IS: AWARD\n(C) TRANSPORTATION REQUIREMENTS 3 IS: CONTRACTOR IS RESPONSIBLE FOR ISSUANCE OF ADVANCED NOTICE OF SHIPMENT\n(D) TRANSPORTATION REQUIREMENT 8 IS: SHIPPED IN CARLOADS OR TRUCKLOADS TO DOD INSTALLATIONS, OR CIVILIAN AGENCIES' -- '(A) DOCUMENT TYPE IS: SOLICITATION\n(B) DOCUMENT TYPE IS: AWARD\n(C) TRANSPORTATION REQUIREMENTS 3 IS: CONTRACTOR IS RESPONSIBLE FOR ISSUANCE OF ADVANCED NOTICE OF SHIPMENT\n(D) USER TO SELECT AT LEAST ONE IS: SHIPPED IN CARLOADS OR TRUCKLOADS TO DOD INSTALLATIONS, OR CIVILIAN AGENCIES'
WHERE clause_id = 80963; -- 52.247-68
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 28054; -- textbox_52.247-68[0]


DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 28055; -- textbox_52.248-3[0]


DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 28062; -- textbox_252.227-7005[0]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 28063; -- textbox_252.227-7005[1]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 28064; -- textbox_252.227-7005[2]


DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 28083; -- textbox_52.214-21[0]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 28084; -- textbox_52.214-21[1]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 28085; -- textbox_52.214-21[2]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 28086; -- textbox_52.214-21[3]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 28087; -- textbox_52.214-21[4]


DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 28113; -- textbox_52.225-4[0]


DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 28114; -- textbox_52.225-4[0]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 28115; -- textbox_52.225-4[1]


DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 28120; -- textbox_52.227-14[0]
DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 28121; -- textbox_52.227-14[1]


DELETE FROM Clause_Fill_Ins WHERE clause_fill_in_id = 28131; -- textbox_52.236-21[0]

/*
*** End of SQL Scripts at Mon Jun 29 17:25:09 EDT 2015 ***
*/
