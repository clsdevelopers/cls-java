/*
Clause Parser Run at Mon Feb 29 18:03:58 EST 2016
(Without Correction Information)
*/
/* ===============================================
 * Prescriptions
   =============================================== */
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '10.003') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '11.107(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '11.107(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '11.204(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '11.204(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '11.204(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '11.204(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '11.304') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '11.404(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '11.404(a)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '11.404(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '11.503') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '11.503(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '11.503(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '11.503(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '11.604(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '11.604(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '11.703(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '11.703(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '11.703(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '12.301(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '12.301(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '12.301(b)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '12.301(b)(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '12.301(b)(4)(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '12.301(b)(4)(ii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '12.301(c)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '13.302-5(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '13.302-5(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '13.302-5(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '13.302-5(d)(3)(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '13.404') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(c)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(c)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(c)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(f)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(g)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(g)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(h)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(j)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(l)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(m)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(o)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(o)(2)(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(o)(2)(ii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(p)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(p)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(q)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(r)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(s)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(t)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(v)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(w)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(x)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-7(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-7(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-7(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-7(c)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-7(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.209(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.209(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.209(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.209(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.209(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.209(b)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.209(b)(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.209(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.209(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.209(f)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.209(h)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.408(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.408(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.408(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.408(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.408(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.408(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.408(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.408(f)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.408(f)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.408(g)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.408(h)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.408(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.408(j)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.408(k)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.408(l)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.408(m)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.408(n)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.408(n)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.408(n)(2)(iii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.105') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.203-4(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.203-4(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.203-4(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.205-4') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.206-4') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.30(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.307(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.307(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.307(a)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.307(a)(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.307(a)(5)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.307(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.307(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.307(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.307(e)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.307(e)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.307(f)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.307(f)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.307(g)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.406') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.406(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.406(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.506') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.506(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.506(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.506(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.506(d)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.506(d)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.506(d)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.506(d)(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.506(d)(5)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.506(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.506(f)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.506(g)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.601(f)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.601(f)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.601(f)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.603-4(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.603-4(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.603-4(b)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.603-4(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '17.109') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '17.109(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '17.208(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '17.208(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '17.208(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '17.208(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '17.208(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '17.208(f)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '17.208(g)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.1309(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.1309(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.1309(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.1309(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.1407') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.1506(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.1506(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.1507(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.1507(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.309(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.309(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.309(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.309(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.507(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.508(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.508(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.508(d)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.508(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.508(f)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.708(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.708(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.708(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.708(b)(1)(iii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.708(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.708(c)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.811-3') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.811-3(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.811-3(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.811-3(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.811-3(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.811-3(d)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.811-3(d)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.811-3(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2.101') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2.201') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '201.602-70') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2012-O0004') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2012-O0007') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2013-00017') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2013-00019') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2013-O0006') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2013-O0010') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2013-O0014') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2013-O0014)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2013-O0016') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2013-O0017') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2013-O0019') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2014-00014') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2014-O0004') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2014-O0005') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2014-O0009') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2014-O0010') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2014-O0016') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2014-O0018') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2014-O0020') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2014-OO0009') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2015-00003') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2015-00009') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2015-00016') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2015-00017') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2015-O0007') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2015-O0010') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2015-O0012') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2015-O0013') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2015-O0016') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2015-OO005') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2016-00002') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2016-0001') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2016-0002') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2016-0003') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2016-O0003') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '203.1004(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '203.1004(b)(2)(ii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '203.171-4') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '203.171-4(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '203.570-3') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '203.97') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '204.1105') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '204.1202') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '204.404-70(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '204.404-70(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '204.404-70(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '204.470-3') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '204.7104-1(b)(3)(iv)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '204.7109(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '204.7109(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '204.7304(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '204.7304(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '204.7304(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '204.7304[c]') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '204.7403(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '204.7403(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '204.7403(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '205.47') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '206.302-3-70') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '208.7305(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '209.104-70') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '209.270-5') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '209.409') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '209.470-4(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '209.470-4(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '209.570-4(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '209.570-4(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '209.571-8(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '209.571-8(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '211.002-70') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '211.107') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '211.204(c)(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '211.204(c)(ii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '211.272') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '211.273-4') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '211.274-6(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '211.274-6(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '211.274-6(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '211.275-3') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '211.503') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '211.503(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '212.7103') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '213.106-2-70') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '215.370-3(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '215.370-3(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '215.371-6') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '215.408') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '215.408(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '215.408(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '215.408(3)(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '215.408(3)(ii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '215.408(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '215.408(4)(ii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '215.408(5)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '216.203-4-70(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '216.203-4-70(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '216.203-4-70(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '216.203-4-70(c)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '216.203-4-70(c)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '216.307(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '216.406') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '216.406(e)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '216.506(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '216.506(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '216.506(d)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '216.506(d)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '216.601(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '217.208-70(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '217.208-70(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '217.208-70(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '217.208-70(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '217.7005') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '217.7104(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '217.7303') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '217.7406(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '217.7702') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '219.309(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '219.708(b)(1)(A)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '219.708(b)(1)(B)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '219.811-3') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '219.811-3(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '219.811-3(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1006') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1006(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1006(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1006(c)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1006(c)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1006(e)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1006(e)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1006(e)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1006(e)(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1006(f)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.103-5(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.103-5(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1103') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1207') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1310(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1310(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1310(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1310(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1408(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1408(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1505(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1505(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1605') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1705(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1705(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1705(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1803') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1906') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.202') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.305') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.407(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.407(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.407(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.407(f)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.407(g)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.407(h)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.505(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.505(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.505(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.505(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.505-(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.505-(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.610') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.810(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.810(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.810(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.810(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.810(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.810(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.810(f)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.810(g)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '222.1771') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '222.7004') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '222.7102') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '222.7201(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '222.7201(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '222.7201(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '222.7302') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '222.7405') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '223.303') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '223.370-5') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '223.570-2') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '223.7106') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '223.7106(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '223.7106(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '223.7203') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '223.7306') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225-7011-3') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225-7799-4(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225-7799-4(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225-7799-4(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225-7799-4(f)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225..1101(10)(i)(C)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(1)(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(1)(ii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(10)(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(10)(i)(A)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(10)(i)(B)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(10)(i)(C)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(10)(i)(D)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(10)(i)(E)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(10)(i)(F)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(2)(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(2)(ii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(2)(iii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(5)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(5)(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(5)(ii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(6)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(6)(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(6)(ii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(7)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(8)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(9)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(9)(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(9)(ii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(9)(iii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(9)(iv)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(9)(v)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(9)(vi)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1103(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1103(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1103(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1103(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.302-6') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.371-5') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.371-5(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.372-2') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7002-2(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7002-3(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7002-3(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7003-5(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7003-5(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7003-5(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7006-4(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7006-4(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7007-3') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7009-5') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7011-3') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7012-3') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7017-4(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7017-5(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7017-5(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7102-4') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7204(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7204(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7307(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7307(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7402-5(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7503(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7503(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7503(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7503(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7503(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7503(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7503(b)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7503(b)(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7605') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7703-4(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7703-4(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7703-4(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7703-4(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.771-5') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.772-5') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7798-6(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7798-6(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7798-6(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7799-4(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7799-4(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7799-4(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7799-4(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7901-4') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7902-5(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7902-5(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '226.104') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.303(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.303(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7009-1') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7009-2') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7009-2(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7009-2(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7009-3(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7009-3(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7009-4(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7009-4(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7009-4(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7009-4(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7009-4(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7010') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7012') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7102-4(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7102-4(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7103-17') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7103-3(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7103-6(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7103-6(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7103-6(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7103-6(e)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7103-6(e)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7103-6(e)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7103-8(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7103-8(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7104(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7104(e)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7104(e)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7104(e)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7104(e)(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7104(e)(5)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7104(f)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7104(f)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7105-2(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7105-3') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7106(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7107-1(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7107-1(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7107-1(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7107-3') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7203-3(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7203-6(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7203-6(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7203-6(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7203-6(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7203-6(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7203-6(f)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7205(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '228.170') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '228.370(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '228.370(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '228.370(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '228.370(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '228.370(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '229.170-4') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '229.402-1') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '229.402-70(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '229.402-70(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '229.402-70(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '229.402-70(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '229.402-70(c)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '229.402-70(c)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '229.402-70(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '229.402-70(e)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '229.402-70(e)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '229.402-70(f)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '229.402-70(g)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '229.402-70(h)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '229.402-70(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '229.402-70(j)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '229.402-70(k)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '229.402-70(l)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '23.1005') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '23.1005(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '23.1005(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '23.1105') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '23.206') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '23.303') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '23.303(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '23.406(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '23.406(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '23.406(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '23.406(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '23.406(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '23.505') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '23.602') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '23.705(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '23.705(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '23.705(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '23.705(c)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '23.705(d)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '23.705(d)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '23.804(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '23.804(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '23.903') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '231.100-70') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '232.1005-70(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '232.1005-70(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '232.1110') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '232.412-70(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '232.412-70(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '232.412-70(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '232.502-4(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '232.502-4-70(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '232.7004(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '232.7004(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '232.705-70') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '232.7102') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '232.7202') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '232.806(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '232.908') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '233.215-70') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '234.203(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '234.203(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '234.7101(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '234.7101(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '234.7101(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '234.7101(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '234.7101(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '235.070-3') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '235.072') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '235.072(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '235.072(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '235.072(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '235.072(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '235.072(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '235.072(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '236.570(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '236.570(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '236.570(b)(2)(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '236.570(b)(2)(ii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '236.570(b)(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '236.570(b)(5)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '236.570(b)(6)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '236.570(c)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '236.570(c)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '236.570(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '236.609-70') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '236.609-70(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '237-7003(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '237.171-4') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '237.173-5') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '237.270(d)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '237.270(d)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '237.7003(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '237.7003(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '237.7003(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '237.7003(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '237.7101(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '237.7101(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '237.7101(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '237.7101(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '237.7101(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '237.7101(e)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '237.7101(e)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '237.7101(e)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '237.7101(f)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '237.7101(g)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '237.7402') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '237.7603(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '239.7103(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '239.7103(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '239.7306(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '239.7306(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '239.7411(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '239.7411(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '239.7411(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '239.7411(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '239.7603(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '239.7603(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '239.7604(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '239.7604(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '24.104(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '24.104(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '241.501-70(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '241.501-70(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '242.7001') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '242.7204') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '242.7503') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '243.205-70') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '243.205-71') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '244.305-71') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '244.305-71(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '244.305-71(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '244.403') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '245.107(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '245.107(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '245.107(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '245.107(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '245.107(5)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '246.270-4') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '246.370') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '246.371(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '246.710(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '246.710(1)(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '246.710(1)(ii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '246.710(1)(iii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '246.710(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '246.710(3)(i)(A)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '246.710(3)(i)(B)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '246.710(3)(i)(B)(iii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '246.870-3') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.207') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.207(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.270-4') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.270-4(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.270-4(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.270-4(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.270-4(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.270-4(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.270-4(f)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.270-4(g)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.271-3') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.271-3(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.271-3(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.271-3(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.271-3(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.271-3(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.271-3(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.271-3(g)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.271-3(h)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.271-3(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.271-3(j)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.271-3(k)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.271-3(l)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.271-3(m)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.271-3(n)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.305-70') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.574(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.574(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.574(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.574(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.574(b)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.574(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.574(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.574(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.574(f)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '249.501-70') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '249.7003(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.110') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1101(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1101(1)(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1101(1)(ii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1101(6)(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1101(6)(ii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1101(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1101(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1101(b)(1)(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1101(b)(1)(ii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1101(b)(1)(iii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1101(b)(1)(iv)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1101(b)(2)(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1101(b)(2)(ii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1101(b)(2)(iii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1101(b)(2)(iv)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1101(c)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1101(c)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1101(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1101(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1101(f)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1102(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1102(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1102(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1102(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1102(c)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1102(d)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1102(d)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1102(d)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1102(e)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1103') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1103(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1103(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1103(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1103(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.301-4') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.302-6') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '251.107') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '251.205') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '26.104') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '26.206(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '26.206(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '26.206(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '26.304') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '26.404') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.201-2') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.201-2(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.201-2(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.201-2(a)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.201-2(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.201-2(c)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.201-2(c)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.201-2(c)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.201-2(d)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.201-2(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.202-5(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.202-5(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.202-5(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.202-5(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.203-2') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.303(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.303(b)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.303(b)(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.303(b)(5)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.303(b)(6)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.303(b)(7)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.303(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.303(e)(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.303(e)(5)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.409(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.409(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.409(b)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.409(b)(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.409(b)(5)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.409(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.409(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.409(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.409(f)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.409(g)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.409(h)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.409(j)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.409(k)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.409(l)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '28.101-2') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '28.103-4') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '28.106-4(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '28.106-4(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '28.1102-3(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '28.1102-3(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '28.203-6') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '28.204-4') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '28.309(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '28.309(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '28.310') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '28.311-1') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '28.312') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '28.313(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '28.313(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '29.401-1') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '29.401-2') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '29.401-3') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '29.401-3(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '29.401-3(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '29.401-4(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '29.402-1(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '29.402-1(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '29.402-2(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '29.402-2(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '3.1004(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '3.1004(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '3.103-1') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '3.104-9(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '3.104-9(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '3.1106') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '3.202') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '3.301-1') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '3.404') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '3.502-3') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '3.503-2') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '3.808(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '3.808(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '3.907-7') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '3.908-9') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '30.201-3') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '30.201-3(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '30.201-3(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '30.201-4(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '30.201-4(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '30.201-4(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '30.201-4(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '30.201-4(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '30.202') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.009-2') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.1005') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.1005(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.1005(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.111(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.111(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.111(a)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.111(a)(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.111(a)(5)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.111(a)(6)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.111(a)(7)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.111(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.111(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.111(c)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.111(c)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.1110(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.1110(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.1110(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.1110(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.1110(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.1110(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.1110(g)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.205(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.206') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.206(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.206(g)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.412(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.412(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.412(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.412(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.412(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.412(f)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.502-3(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.502-3(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.502-3(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.502-4(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.502-4(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.502-4(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.502-4(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.611(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.611(a)&(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.611(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.706-1(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.706-1(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.706-2(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.706-2(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.706-3') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.806(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.806(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.806(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.908(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.908(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.908(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.908(c)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '33.106(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '33.106(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '33.215(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '33.215(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '34.104') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '34.203(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '34.203(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '34.203(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.501(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.502') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.503') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.504') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.505') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.506') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.507') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.508') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.509') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.510') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.511') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.512') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.513') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.514') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.515') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.516') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.517') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.518') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.519') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.520') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.521') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.522') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.523') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.609-1(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.609-2(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.609-3') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.609-4') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '37.110(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '37.110(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '37.110(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '37.113-2(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '37.113-2(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '37.115-3') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '37.116-2') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '37.304(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '37.304(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '37.304(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '37.403') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '39.106') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '4.103') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '4.1105(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '4.1105(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '4.1202(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '4.1202(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '4.1303') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '4.1403(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '4.1705(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '4.1705(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '4.1804(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '4.1804(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '4.1804(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '4.303') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '4.404(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '4.607(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '4.607(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '4.607(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '4.905') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '41.501(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '41.501(c)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '41.501(c)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '41.501(c)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '41.501(c)(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '41.501(c)(5)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '41.501(d)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '41.501(d)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '41.501(d)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '41.501(d)(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '41.501(d)(5)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '41.501(d)(6)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '41.501(d)(7)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '42.1107(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '42.1305(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '42.1305(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '42.1305(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '42.1305(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '42.703-2(f)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '42.709-6') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '42.802') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '42.903') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '43.107') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '43.205(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '43.205(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '43.205(a)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '43.205(a)(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '43.205(a)(5)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '43.205(a)(6)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '43.205(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '43.205(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '43.205(b)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '43.205(b)(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '43.205(b)(6)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '43.205(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '43.205(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '43.205(f)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '44.204(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '44.204(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '44.204(a)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '44.204(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '44.204(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '44.403') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '45.107(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '45.107(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '45.107(a)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '45.107(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '45.107(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.301') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.302') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.303') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.304') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.305') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.306') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.307(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.308') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.309') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.311') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.312') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.313') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.314') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.315') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.316') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.710(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.710(a)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.710(a)(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.710(a)(5)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.710(a)(6)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.710(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.710(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.710(b)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.710(b)(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.710(c)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.710(c)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.710(c)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.710(c)(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.710(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.710(e)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.710(e)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.805') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.805(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.805(a)(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.103-2') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.104-4') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-1(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-1(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-1(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-1(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-1(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-1(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-3(d)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-3(e)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-4(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-4(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-4(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-5(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-5(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-5(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-5(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-5(f)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-6(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-6(c)(5)(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-6(c)(5)(ii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-6(c)(6)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-7(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-7(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-7(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-8(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-8(a)(2)(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-8(a)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-8(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-9(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.208-2') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.303-1(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.303-10(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.303-11(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.303-12(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.303-13(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.303-14(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.303-15(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.303-16(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.303-17(f)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.303-2(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.303-3(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.303-4(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.303-5(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.303-6(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.303-7(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.303-8(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.303-9(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.304-7(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.305-12(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.305-13(a)(3)(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.305-14(b)(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.305-15(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.305-16(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.305-16(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.305-16(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.305-16(d)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.305-17') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.305-2(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.305-3(b)(4)(ii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.305-3(f)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.305-4(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.305-5(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.305-5(c)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.305-6(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.305-6(e)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.305-6(e)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.305-6(e)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.305-6(f)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.305-9(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.405') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.507(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.507(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.507(a)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '48.201') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '48.201(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '48.201(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '48.201(e)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '48.201(f)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '48.202') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '49.502(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '49.502(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '49.502(b)(1)(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '49.502(b)(1)(ii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '49.502(b)(1)(iii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '49.502(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '49.502(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '49.502(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '49.503(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '49.503(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '49.503(a)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '49.503(a)(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '49.503(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '49.504(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '49.504(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '49.504(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '49.504(c)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '49.504(c)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '49.504(c)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '49.505(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '49.505(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '50.104-4') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '50.206(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '50.206(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '50.206(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '50.206(b)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '50.206(c)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '50.206(c)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '50.206(c)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '50.206(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '50.403-3') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '51.107') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '51.205') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '52.107(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '52.107(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '52.107(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '52.107(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '52.107(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '52.107(f)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '53.111') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '7.203') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '7.305(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '7.305(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '7.305(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '7.404') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '8.005') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '8.1104(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '8.1104(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '8.1104(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '8.1104(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '8.505') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '9.104-7(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '9.104-7(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '9.104-7(c)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '9.108-5(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '9.108-5(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '9.206-2') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '9.308-1(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '9.308-1(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '9.308-1(a)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '9.308-1(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '9.308-1(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '9.308-1(b)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '9.308-2(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '9.308-2(a)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '9.308-2(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '9.308-2(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '9.308-2(b)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '9.409') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, 'NOTAPPLICABLE') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, 'PGI 204.7108(d)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, 'PGI 204.7108(d)(10)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, 'PGI 204.7108(d)(11)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, 'PGI 204.7108(d)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, 'PGI 204.7108(d)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, 'PGI 204.7108(d)(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, 'PGI 204.7108(d)(5)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, 'PGI 204.7108(d)(6)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, 'PGI 204.7108(d)(7)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, 'PGI 204.7108(d)(8)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, 'PGI 204.7108(d)(9)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
/* ===============================================
 * Questions
   =============================================== */


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT Question_Choce_Id, prescription_id FROM Question_Choices, Prescriptions
  WHERE prescription_name IN ('32.205(b)','32.206') 
  AND choice_text = 'SIMPLIFIED ACQUISITION PROCEDURES (FAR PART 13)' AND  question_id IN (SELECT question_id FROM Questions WHERE clause_version_id = 15 AND question_code = '[PROCEDURES]'); -- [PROCEDURES] 'SIMPLIFIED ACQUISITION PROCEDURES (FAR PART 13)'

DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'COMMERCIAL PROCEDURES (FAR PART 12)' INNER JOIN Questions Q  ON Q.question_code = '[PROCEDURES]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '12.301(b)(3)';

DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'COMMERCIAL PROCEDURES (FAR PART 12)' INNER JOIN Questions Q  ON Q.question_code = '[PROCEDURES]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '12.301(b)(4)';

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT Question_Choce_Id, prescription_id FROM Question_Choices, Prescriptions
  WHERE prescription_name IN ('10.003','11.107(a)','11.107(b)','11.204(a)','11.204(b)','11.204(c)','11.204(d)','11.304','11.404(a)(2)','11.404(a)(3)','11.404(b)','11.503(a)','11.503(b)','11.503(c)','11.604(a)','11.604(b)','11.703(a)','11.703(b)','11.703(c)','13.302-5(b)','13.302-5(c)','13.302-5(d)','13.404','14.201-6(b)(1)','14.201-6(b)(2)','14.201-6(c)(1)','14.201-6(c)(2)','14.201-6(c)(3)','14.201-6(e)','14.201-6(f)','14.201-6(g)(1)','14.201-6(g)(2)','14.201-6(h)','14.201-6(i)','14.201-6(j)','14.201-6(l)','14.201-6(m)','14.201-6(o)(1)','14.201-6(o)(2)(i)','14.201-6(o)(2)(ii)','14.201-6(p)(1)','14.201-6(p)(2)','14.201-6(q)','14.201-6(r)','14.201-6(s)','14.201-6(t)','14.201-6(v)','14.201-6(w)','14.201-6(x)','14.201-7(a)(1)','14.201-7(a)(2)','14.201-7(b)(1)','14.201-7(c)(1)','14.201-7(d)','15.209(a)','15.209(a)(1)','15.209(a)(2)','15.209(b)(1)','15.209(b)(2)','15.209(b)(3)','15.209(b)(4)','15.209(c)','15.209(e)','15.209(f)','15.209(h)','15.408(a)','15.408(a)(1)','15.408(a)(2)','15.408(b)','15.408(c)','15.408(d)','15.408(e)','15.408(f)(1)','15.408(f)(2)','15.408(g)','15.408(h)','15.408(i)','15.408(j)','15.408(k)','15.408(m)','15.408(n)(1)','15.408(n)(2)','15.408(n)(2)(iii)','16.105','16.203-4(a)','16.203-4(b)','16.203-4(c)','16.205-4','16.206-4','16.30(d)','16.307(a)','16.307(a)(2)','16.307(a)(3)','16.307(a)(4)','16.307(a)(5)','16.307(b)','16.307(c)','16.307(e)(1)','16.307(e)(2)','16.307(f)(1)','16.307(f)(2)','16.307(g)','16.406(a)','16.406(b)','16.506(a)','16.506(b)','16.506(c)','16.506(d)(1)','16.506(d)(2)','16.506(d)(3)','16.506(d)(4)','16.506(d)(5)','16.506(e)','16.506(f)','16.506(g)','16.601(f)(1)','16.601(f)(2)','16.601(f)(3)','16.603-4(b)(1)','16.603-4(b)(2)','16.603-4(b)(3)','16.603-4(c)','17.109(a)','17.208(a)','17.208(b)','17.208(c)','17.208(d)','17.208(e)','17.208(f)','17.208(g)','19.309(a)(1)','19.309(a)(2)','19.309(c)','19.708(c)(1)','19.811-3(a)','19.811-3(b)','19.811-3(c)','19.811-3(d)','19.811-3(d)(1)','19.811-3(d)(2)','2.201','22.1006(e)(1)','22.1006(e)(3)','22.1006(f)','22.103-5(a)','22.103-5(b)','22.1103','22.1310(c)','22.1505(a)','22.305','22.407(a)','22.407(b)','22.407(e)','22.407(f)','22.407(g)','22.407(h)','22.505(a)(1)','22.505(a)(2)','22.505(b)(1)','22.505(b)(2)','22.610','22.810(a)(2)','22.810(b)','22.810(c)','22.810(d)','22.810(f)','22.810(g)','225.1101(4)','23.1005','23.1005(b)','23.1005(c)','23.303','23.303(b)','23.406(a)','23.406(b)','23.406(c)','23.406(e)','23.505','23.602','23.705(a)','23.804(a)','23.804(b)','23.903','24.104(a)','24.104(b)','25.1101(a)(2)','25.1101(b)(2)(i)','25.1101(b)(2)(ii)','25.1101(b)(2)(iii)','25.1101(b)(2)(iv)','25.1101(c)(2)','25.1101(d)','25.1101(e)','25.1101(f)','25.1102(a)','25.1102(b)(1)','25.1102(b)(2)','25.1102(c)','25.1102(c)(3)','25.1102(d)(1)','25.1102(d)(2)','25.1102(d)(3)','25.1102(e)(1)','25.1103','25.1103(b)','25.1103(c)','25.1103(d)','26.104','26.206(a)','26.304','26.404','28.101-2','28.103-4','28.106-4(a)','28.106-4(b)','28.1102-3(a)','28.1102-3(b)','28.203-6','28.204-4','28.309(a)','28.309(b)','28.310','28.311-1','28.312','28.313(a)','28.313(b)','29.401-1','29.401-2','29.401-3','29.401-3(a)','29.401-3(b)','29.401-4(b)','29.402-1(a)','29.402-1(b)','29.402-2(a)','29.402-2(b)','3.1004(b)','3.103-1','3.104-9(a)','3.104-9(b)','3.1106','3.202','3.404','3.502-3','3.808(a)','3.808(b)','3.908-9','30.201-3','30.201-3(c)','30.201-4(a)','30.201-4(b)(1)','30.201-4(c)','30.201-4(d)','30.201-4(e)','32.1005','32.1005(b)(1)','32.1005(b)(2)','32.111(a)(1)','32.111(a)(2)','32.111(a)(3)','32.111(a)(4)','32.111(a)(5)','32.111(a)(6)','32.111(a)(7)','32.111(b)(1)','32.111(c)(1)','32.111(c)(2)','32.1110(b)(2)','32.1110(c)','32.1110(e)','32.1110(g)','32.205(b)','32.206','32.206(g)','32.412(a)','32.412(b)','32.412(c)','32.412(d)','32.412(e)','32.412(f)','32.502-3(a)','32.502-3(b)(2)','32.502-3(c)','32.502-4(a)','32.502-4(b)','32.502-4(c)','32.502-4(d)','32.611(a)&(b)','32.706-1(a)','32.706-1(b)','32.706-2(a)','32.706-2(b)','32.706-3','32.806(a)(1)','32.806(a)(2)','32.806(b)','32.908(a)','32.908(b)','32.908(c)','32.908(c)(3)','33.106(a)','33.215(a)','34.104','34.203(a)','34.203(b)','34.203(c)','36.501(b)','36.502','36.503','36.504','36.505','36.506','36.507','36.508','36.509','36.510','36.511','36.512','36.513','36.514','36.515','36.516','36.517','36.518','36.519','36.520','36.521','36.522','36.523','36.609-1(c)','36.609-2(b)','36.609-3','36.609-4','37.110(a)','37.110(b)','37.110(c)','37.113-2(a)','37.113-2(b)','37.115-3','37.304(a)','37.304(b)','37.304(c)','37.403','4.1105(a)(1)','4.1105(b)','4.1202(a)','4.1202(b)','4.1303','4.1804(b)','4.303','4.404(a)','4.607(a)','4.607(b)','4.607(c)','41.501(b)','41.501(c)(1)','41.501(c)(2)','41.501(c)(3)','41.501(c)(4)','41.501(c)(5)','41.501(d)(1)','41.501(d)(2)','41.501(d)(3)','41.501(d)(4)','41.501(d)(5)','41.501(d)(6)','41.501(d)(7)','42.1107(a)','42.1305(a)','42.1305(b)(1)','42.1305(b)(2)','42.1305(c)','42.703-2(f)','42.709-6','42.802','42.903','43.107','43.205(a)(1)','43.205(a)(2)','43.205(a)(3)','43.205(a)(4)','43.205(a)(5)','43.205(a)(6)','43.205(b)(1)','43.205(b)(2)','43.205(b)(3)','43.205(b)(4)','43.205(b)(6)','43.205(c)','43.205(e)','43.205(f)','44.204(a)(1)','44.204(a)(2)','44.204(a)(3)','44.204(b)','44.204(c)','44.403','45.107(b)','45.107(c)','46.301','46.302','46.303','46.304','46.305','46.306','46.307(a)','46.308','46.309','46.311','46.312','46.313','46.314','46.315','46.316','46.710(a)(1)','46.710(a)(3)','46.710(a)(4)','46.710(a)(5)','46.710(a)(6)','46.710(b)(1)','46.710(b)(2)','46.710(b)(3)','46.710(b)(4)','46.710(c)(1)','46.710(c)(2)','46.710(c)(3)','46.710(c)(4)','46.710(d)','46.710(e)(1)','46.710(e)(2)','46.805','46.805(a)','46.805(a)(4)','47.103-2','47.104-4','47.207-1(a)','47.207-1(b)(1)','47.207-1(b)(2)','47.207-1(c)','47.207-1(d)','47.207-1(e)','47.207-3(d)(2)','47.207-3(e)(2)','47.207-4(a)(1)','47.207-4(a)(2)','47.207-4(b)','47.207-5(b)','47.207-5(c)','47.207-5(d)','47.207-5(e)','47.207-5(f)','47.207-6(a)(2)','47.207-6(c)(5)(i)','47.207-6(c)(5)(ii)','47.207-6(c)(6)','47.207-7(c)','47.207-7(d)','47.207-7(e)','47.207-8(a)(1)','47.207-8(a)(2)(i)','47.207-8(a)(3)','47.207-8(b)','47.207-9(c)','47.208-2','47.303-1(c)','47.303-10(c)','47.303-11(c)','47.303-12(c)','47.303-13(c)','47.303-14(c)','47.303-15(c)','47.303-16(c)','47.303-17(f)','47.303-2(c)','47.303-3(c)','47.303-4(c)','47.303-5(c)','47.303-6(c)','47.303-7(c)','47.303-8(c)','47.303-9(c)','47.305-12(a)(2)','47.305-13(a)(3)(i)','47.305-14(b)(4)','47.305-15(a)(2)','47.305-16(a)','47.305-16(b)(1)','47.305-16(c)','47.305-16(d)(2)','47.305-17','47.305-2(b)','47.305-3(b)(4)(ii)','47.305-3(f)(2)','47.305-4(c)','47.305-5(b)(2)','47.305-5(c)(1)','47.305-6(e)','47.305-6(e)(1)','47.305-6(e)(2)','47.305-6(e)(3)','47.305-6(f)(2)','47.305-9(b)(1)','47.405','48.201','48.201(c)','48.201(d)','48.201(e)(1)','48.201(f)','48.202','49.502(a)(1)','49.502(a)(2)','49.502(b)(1)(i)','49.502(b)(1)(ii)','49.502(b)(1)(iii)','49.502(b)(2)','49.502(c)','49.502(d)','49.503(a)(1)','49.503(a)(2)','49.503(a)(3)','49.503(a)(4)','49.503(b)','49.504(a)(1)','49.504(a)(2)','49.504(b)','49.504(c)(1)','49.504(c)(2)','49.504(c)(3)','49.505(a)','49.505(b)','50.104-4','50.206(a)','50.206(b)(1)','50.206(b)(2)','50.206(b)(3)','50.206(c)(1)','50.206(c)(2)','50.206(c)(3)','50.206(d)','50.403-3','51.107','51.205','52.107(a)','52.107(b)','52.107(c)','52.107(d)','52.107(e)','52.107(f)','53.111','7.203','7.305(a)','7.305(b)','7.305(c)','7.404','8.005','8.1104(a)','8.1104(b)','8.1104(c)','8.1104(d)','8.505','9.104-7(a)','9.108-5(a)','9.206-2','9.308-1(a)(1)','9.308-1(a)(2)','9.308-1(a)(3)','9.308-1(b)(1)','9.308-1(b)(2)','9.308-1(b)(3)','9.308-2(a)(1)') 
  AND choice_text = 'COMMERCIAL PROCEDURES (FAR PART 12)' AND  question_id IN (SELECT question_id FROM Questions WHERE clause_version_id = 15 AND question_code = '[PROCEDURES]'); -- [PROCEDURES] 'COMMERCIAL PROCEDURES (FAR PART 12)'

DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'NEGOTIATION (FAR PART 15)' INNER JOIN Questions Q  ON Q.question_code = '[PROCEDURES]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '15.408(l)';

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT Question_Choce_Id, prescription_id FROM Question_Choices, Prescriptions
  WHERE prescription_name IN ('237.7003(a)(2)','32.1005(b)(2)') 
  AND choice_text = 'NEGOTIATION (FAR PART 15)' AND  question_id IN (SELECT question_id FROM Questions WHERE clause_version_id = 15 AND question_code = '[PROCEDURES]'); -- [PROCEDURES] 'NEGOTIATION (FAR PART 15)'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT Question_Choce_Id, prescription_id FROM Question_Choices, Prescriptions
  WHERE prescription_name IN ('219.309(1)') 
  AND choice_text = 'SMALL BUSINESS PROGRAM' AND  question_id IN (SELECT question_id FROM Questions WHERE clause_version_id = 15 AND question_code = '[PROCEDURES]'); -- [PROCEDURES] 'SMALL BUSINESS PROGRAM'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT Question_Choce_Id, prescription_id FROM Question_Choices, Prescriptions
  WHERE prescription_name IN ('3.202') 
  AND choice_text = 'AWARD' AND  question_id IN (SELECT question_id FROM Questions WHERE clause_version_id = 15 AND question_code = '[DOCUMENT TYPE]'); -- [DOCUMENT TYPE] 'AWARD'


DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'DEPARTMENT OF DEFENSE' INNER JOIN Questions Q  ON Q.question_code = '[ISSUING OFFICE]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '30.202';

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT Question_Choce_Id, prescription_id FROM Question_Choices, Prescriptions
  WHERE prescription_name IN ('29.401-4(b)') 
  AND choice_text = 'NATIONAL AERONAUTICS AND SPACE ADMINISTRATION' AND  question_id IN (SELECT question_id FROM Questions WHERE clause_version_id = 15 AND question_code = '[ISSUING OFFICE]'); -- [ISSUING OFFICE] 'NATIONAL AERONAUTICS AND SPACE ADMINISTRATION'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT Question_Choce_Id, prescription_id FROM Question_Choices, Prescriptions
  WHERE prescription_name IN ('11.107(b)','2013-O0019') 
  AND choice_text = 'DEPARTMENT OF DEFENSE' AND  question_id IN (SELECT question_id FROM Questions WHERE clause_version_id = 15 AND question_code = '[REQUIRING ACTIVITY]'); -- [REQUIRING ACTIVITY] 'DEPARTMENT OF DEFENSE'


DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'YES' INNER JOIN Questions Q  ON Q.question_code = '[WAIVER OF NON MANUFACTURING RULE]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '19.508(d)(2)';

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT Question_Choce_Id, prescription_id FROM Question_Choices, Prescriptions
  WHERE prescription_name IN ('19.811-3(d)(2)','22.810(g)') 
  AND choice_text = 'YES' AND  question_id IN (SELECT question_id FROM Questions WHERE clause_version_id = 15 AND question_code = '[WAIVER OF NON MANUFACTURING RULE]'); -- [WAIVER OF NON MANUFACTURING RULE] 'YES'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT Question_Choce_Id, prescription_id FROM Question_Choices, Prescriptions
  WHERE prescription_name IN ('22.202') 
  AND choice_text = 'NO' AND  question_id IN (SELECT question_id FROM Questions WHERE clause_version_id = 15 AND question_code = '[FEDERAL PRISON INDUSTRY]'); -- [FEDERAL PRISON INDUSTRY] 'NO'


DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'RESTRICTED TO PRODUCTS OR SERVICES FROM AFGHANISTAN' INNER JOIN Questions Q  ON Q.question_code = '[COMPETITION PREFERENCES]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '2014-00014';

DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'RESTRICTED TO PRODUCTS OR SERVICES FROM AFGHANISTAN' INNER JOIN Questions Q  ON Q.question_code = '[COMPETITION PREFERENCES]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '225-7799-4(c)';

DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'RESTRICTED TO PRODUCTS OR SERVICES FROM AFGHANISTAN' INNER JOIN Questions Q  ON Q.question_code = '[COMPETITION PREFERENCES]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '225-7799-4(d)';

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT Question_Choce_Id, prescription_id FROM Question_Choices, Prescriptions
  WHERE prescription_name IN ('225.7799-4(a)','225.7799-4(c)') 
  AND choice_text = 'RESTRICTED TO PRODUCTS OR SERVICES FROM AFGHANISTAN' AND  question_id IN (SELECT question_id FROM Questions WHERE clause_version_id = 15 AND question_code = '[COMPETITION PREFERENCES]'); -- [COMPETITION PREFERENCES] 'RESTRICTED TO PRODUCTS OR SERVICES FROM AFGHANISTAN'

DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'LIMITED TO PRODUCTS OR SERVICES FROM A CENTRAL ASIAN STATE' INNER JOIN Questions Q  ON Q.question_code = '[COMPETITION PREFERENCES]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '2014-00014';

DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'LIMITED TO PRODUCTS OR SERVICES FROM A CENTRAL ASIAN STATE' INNER JOIN Questions Q  ON Q.question_code = '[COMPETITION PREFERENCES]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '225-7799-4(c)';

DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'LIMITED TO PRODUCTS OR SERVICES FROM A CENTRAL ASIAN STATE' INNER JOIN Questions Q  ON Q.question_code = '[COMPETITION PREFERENCES]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '225-7799-4(d)';

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT Question_Choce_Id, prescription_id FROM Question_Choices, Prescriptions
  WHERE prescription_name IN ('225.7799-4(a)','225.7799-4(c)') 
  AND choice_text = 'LIMITED TO PRODUCTS OR SERVICES FROM A CENTRAL ASIAN STATE' AND  question_id IN (SELECT question_id FROM Questions WHERE clause_version_id = 15 AND question_code = '[COMPETITION PREFERENCES]'); -- [COMPETITION PREFERENCES] 'LIMITED TO PRODUCTS OR SERVICES FROM A CENTRAL ASIAN STATE'

DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'LIMITED TO PRODUCTS OR SERVICES FROM PAKISTAN' INNER JOIN Questions Q  ON Q.question_code = '[COMPETITION PREFERENCES]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '2014-00014';

DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'LIMITED TO PRODUCTS OR SERVICES FROM PAKISTAN' INNER JOIN Questions Q  ON Q.question_code = '[COMPETITION PREFERENCES]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '225-7799-4(d)';

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT Question_Choce_Id, prescription_id FROM Question_Choices, Prescriptions
  WHERE prescription_name IN ('225.7799-4(a)','225.7799-4(c)') 
  AND choice_text = 'LIMITED TO PRODUCTS OR SERVICES FROM PAKISTAN' AND  question_id IN (SELECT question_id FROM Questions WHERE clause_version_id = 15 AND question_code = '[COMPETITION PREFERENCES]'); -- [COMPETITION PREFERENCES] 'LIMITED TO PRODUCTS OR SERVICES FROM PAKISTAN'

DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'LIMITED TO PRODUCTS OR SERVICES FROM SOUTH CAUCASUS' INNER JOIN Questions Q  ON Q.question_code = '[COMPETITION PREFERENCES]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '2014-00014';

DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'LIMITED TO PRODUCTS OR SERVICES FROM SOUTH CAUCASUS' INNER JOIN Questions Q  ON Q.question_code = '[COMPETITION PREFERENCES]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '225-7799-4(d)';

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT Question_Choce_Id, prescription_id FROM Question_Choices, Prescriptions
  WHERE prescription_name IN ('225.7799-4(a)','225.7799-4(c)') 
  AND choice_text = 'LIMITED TO PRODUCTS OR SERVICES FROM SOUTH CAUCASUS' AND  question_id IN (SELECT question_id FROM Questions WHERE clause_version_id = 15 AND question_code = '[COMPETITION PREFERENCES]'); -- [COMPETITION PREFERENCES] 'LIMITED TO PRODUCTS OR SERVICES FROM SOUTH CAUCASUS'

DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'PURCHASE FROM A FOREIGN SOURCE IS RESTRICTED' INNER JOIN Questions Q  ON Q.question_code = '[COMPETITION PREFERENCES]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '225..1101(10)(i)(C)';

DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'PURCHASE FROM A FOREIGN SOURCE IS RESTRICTED' INNER JOIN Questions Q  ON Q.question_code = '[COMPETITION PREFERENCES]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '225.1101(6)';

DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'PURCHASE FROM A FOREIGN SOURCE IS RESTRICTED' INNER JOIN Questions Q  ON Q.question_code = '[COMPETITION PREFERENCES]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '225.1101(6)(i)';

DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'PURCHASE FROM A FOREIGN SOURCE IS RESTRICTED' INNER JOIN Questions Q  ON Q.question_code = '[COMPETITION PREFERENCES]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '225.1101(6)(ii)';

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT Question_Choce_Id, prescription_id FROM Question_Choices, Prescriptions
  WHERE prescription_name IN ('225.1101(10)(i)(C)','25.1101(6)(i)','25.1101(6)(ii)') 
  AND choice_text = 'PURCHASE FROM A FOREIGN SOURCE IS RESTRICTED' AND  question_id IN (SELECT question_id FROM Questions WHERE clause_version_id = 15 AND question_code = '[COMPETITION PREFERENCES]'); -- [COMPETITION PREFERENCES] 'PURCHASE FROM A FOREIGN SOURCE IS RESTRICTED'

DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'PREFERENCE FOR PRODUCTS OR SERVICES FROM PAKISTAN' INNER JOIN Questions Q  ON Q.question_code = '[COMPETITION PREFERENCES]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '2014-00014';

DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'PREFERENCE FOR PRODUCTS OR SERVICES FROM PAKISTAN' INNER JOIN Questions Q  ON Q.question_code = '[COMPETITION PREFERENCES]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '225-7799-4(f)';

DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'PREFERENCE FOR PRODUCTS OR SERVICES FROM THE SOUTH CAUCASUS' INNER JOIN Questions Q  ON Q.question_code = '[COMPETITION PREFERENCES]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '2014-00014';

DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'PREFERENCE FOR PRODUCTS OR SERVICES FROM THE SOUTH CAUCASUS' INNER JOIN Questions Q  ON Q.question_code = '[COMPETITION PREFERENCES]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '225-7799-4(f)';

DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'PREFERENCE FOR PRODUCTS OR SERVICES FROM AFGHANISTAN' INNER JOIN Questions Q  ON Q.question_code = '[COMPETITION PREFERENCES]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '2014-00014';

DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'PREFERENCE FOR PRODUCTS OR SERVICES FROM AFGHANISTAN' INNER JOIN Questions Q  ON Q.question_code = '[COMPETITION PREFERENCES]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '225-7799-4(b)';

DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'PREFERENCE FOR PRODUCTS OR SERVICES FROM AFGHANISTAN' INNER JOIN Questions Q  ON Q.question_code = '[COMPETITION PREFERENCES]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '225-7799-4(f)';

DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'PREFERENCE FOR PRODUCTS OR SERVICES FROM CENTRAL ASIAN STATE' INNER JOIN Questions Q  ON Q.question_code = '[COMPETITION PREFERENCES]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '2014-00014';

DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'PREFERENCE FOR PRODUCTS OR SERVICES FROM CENTRAL ASIAN STATE' INNER JOIN Questions Q  ON Q.question_code = '[COMPETITION PREFERENCES]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '225-7799-4(b)';

DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'PREFERENCE FOR PRODUCTS OR SERVICES FROM CENTRAL ASIAN STATE' INNER JOIN Questions Q  ON Q.question_code = '[COMPETITION PREFERENCES]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '225-7799-4(f)';

DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'PREFERENCE FOR PRODUCTS OR SERVICES OF DJIBOUTI' INNER JOIN Questions Q  ON Q.question_code = '[COMPETITION PREFERENCES]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '2015-O0012';

DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'LIMIT COMPETITION TO PRODUCTS OR SERVICES OF DJIBOUTI' INNER JOIN Questions Q  ON Q.question_code = '[COMPETITION PREFERENCES]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '2015-O0012';


DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'YES' INNER JOIN Questions Q  ON Q.question_code = '[ADEQUATE PRICE COMPETITION]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '15.408(f)(2)';

DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'YES' INNER JOIN Questions Q  ON Q.question_code = '[ADEQUATE PRICE COMPETITION]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '16.601(f)(2)';

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT Question_Choce_Id, prescription_id FROM Question_Choices, Prescriptions
  WHERE prescription_name IN ('15.408(f)(2)','16.601(f)(2)') 
  AND choice_text = 'NO' AND  question_id IN (SELECT question_id FROM Questions WHERE clause_version_id = 15 AND question_code = '[ADEQUATE PRICE COMPETITION]'); -- [ADEQUATE PRICE COMPETITION] 'NO'


DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'YES' INNER JOIN Questions Q  ON Q.question_code = '[SIMPLIFIED ACQUISITION TEST PROGRAM]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '213.106-2-70';


DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'YES' INNER JOIN Questions Q  ON Q.question_code = '[SDB MECHANISMS ON A REGIONAL BASIS]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '12.301(b)(2)';

DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'NOT APPLICABLE' INNER JOIN Questions Q  ON Q.question_code = '[SDB MECHANISMS ON A REGIONAL BASIS]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = 'NOTAPPLICABLE';


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT Question_Choce_Id, prescription_id FROM Question_Choices, Prescriptions
  WHERE prescription_name IN ('225.1101(2)(i)') 
  AND choice_text = '6.302-3 INDUSTRIAL MOBILIZATION; ENGINEERING, DEVELOPMENTAL, OR RESEARCH CAPABILITY; OR EXPERT SERVICES' AND  question_id IN (SELECT question_id FROM Questions WHERE clause_version_id = 15 AND question_code = '[FULL AND OPEN EXCEPTION]'); -- [FULL AND OPEN EXCEPTION] '6.302-3 INDUSTRIAL MOBILIZATION; ENGINEERING, DEVELOPMENTAL, OR RESEARCH CAPABILITY; OR EXPERT SERVICES'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT Question_Choce_Id, prescription_id FROM Question_Choices, Prescriptions
  WHERE prescription_name IN ('32.502-4(d)') 
  AND choice_text = 'INDEFINITE DELIVERY CONTRACT' AND  question_id IN (SELECT question_id FROM Questions WHERE clause_version_id = 15 AND question_code = '[AWARD TYPE]'); -- [AWARD TYPE] 'INDEFINITE DELIVERY CONTRACT'

DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'LETTER CONTRACT' INNER JOIN Questions Q  ON Q.question_code = '[AWARD TYPE]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '46.302';


DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'INDEFINITE DELIVERY INDEFINITE QUANTITY' INNER JOIN Questions Q  ON Q.question_code = '[TYPE OF INDEFINITE DELIVERY]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '16.506(d)(1)';

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT Question_Choce_Id, prescription_id FROM Question_Choices, Prescriptions
  WHERE prescription_name IN ('16.506(e)') 
  AND choice_text = 'INDEFINITE DELIVERY INDEFINITE QUANTITY' AND  question_id IN (SELECT question_id FROM Questions WHERE clause_version_id = 15 AND question_code = '[TYPE OF INDEFINITE DELIVERY]'); -- [TYPE OF INDEFINITE DELIVERY] 'INDEFINITE DELIVERY INDEFINITE QUANTITY'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT Question_Choce_Id, prescription_id FROM Question_Choices, Prescriptions
  WHERE prescription_name IN ('16.506(d)(1)','16.506(d)(2)','16.506(d)(3)','16.506(d)(4)','16.506(d)(5)','216.506(d)(2)') 
  AND choice_text = 'REQUIREMENTS' AND  question_id IN (SELECT question_id FROM Questions WHERE clause_version_id = 15 AND question_code = '[TYPE OF INDEFINITE DELIVERY]'); -- [TYPE OF INDEFINITE DELIVERY] 'REQUIREMENTS'


DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'YES' INNER JOIN Questions Q  ON Q.question_code = '[EXCEEDS GOVERNMENT CAPABILITY]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '16.506';


DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'YES' INNER JOIN Questions Q  ON Q.question_code = '[PAYMENT OF FIXED CHARGES]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '247.270-4';

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT Question_Choce_Id, prescription_id FROM Question_Choices, Prescriptions
  WHERE prescription_name IN ('247.270-4(d)') 
  AND choice_text = 'YES' AND  question_id IN (SELECT question_id FROM Questions WHERE clause_version_id = 15 AND question_code = '[PAYMENT OF FIXED CHARGES]'); -- [PAYMENT OF FIXED CHARGES] 'YES'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT Question_Choce_Id, prescription_id FROM Question_Choices, Prescriptions
  WHERE prescription_name IN ('247.270-4(e)') 
  AND choice_text = 'NO' AND  question_id IN (SELECT question_id FROM Questions WHERE clause_version_id = 15 AND question_code = '[PAYMENT OF FIXED CHARGES]'); -- [PAYMENT OF FIXED CHARGES] 'NO'


DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'DELIVERY ORDER' INNER JOIN Questions Q  ON Q.question_code = '[TYPE OF ORDER]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '203.171-4';

DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'TASK ORDER' INNER JOIN Questions Q  ON Q.question_code = '[TYPE OF ORDER]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '203.171-4';


DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'YES' INNER JOIN Questions Q  ON Q.question_code = '[PROVISIONING ORDER]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '16.406';

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT Question_Choce_Id, prescription_id FROM Question_Choices, Prescriptions
  WHERE prescription_name IN ('16.406(a)','16.406(b)') 
  AND choice_text = 'YES' AND  question_id IN (SELECT question_id FROM Questions WHERE clause_version_id = 15 AND question_code = '[PROVISIONING ORDER]'); -- [PROVISIONING ORDER] 'YES'


DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'MULTI-YEAR CONTRACT' INNER JOIN Questions Q  ON Q.question_code = '[MULTIPLE OR MULTI-YEAR]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '17.109';

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT Question_Choce_Id, prescription_id FROM Question_Choices, Prescriptions
  WHERE prescription_name IN ('17.109(a)') 
  AND choice_text = 'MULTI-YEAR CONTRACT' AND  question_id IN (SELECT question_id FROM Questions WHERE clause_version_id = 15 AND question_code = '[MULTIPLE OR MULTI-YEAR]'); -- [MULTIPLE OR MULTI-YEAR] 'MULTI-YEAR CONTRACT'

DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'MULTIPLE YEAR CONTRACT' INNER JOIN Questions Q  ON Q.question_code = '[MULTIPLE OR MULTI-YEAR]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '22.1006';

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT Question_Choce_Id, prescription_id FROM Question_Choices, Prescriptions
  WHERE prescription_name IN ('22.1006(c)(1)','22.1006(c)(2)','32.706-1(b)') 
  AND choice_text = 'MULTIPLE YEAR CONTRACT' AND  question_id IN (SELECT question_id FROM Questions WHERE clause_version_id = 15 AND question_code = '[MULTIPLE OR MULTI-YEAR]'); -- [MULTIPLE OR MULTI-YEAR] 'MULTIPLE YEAR CONTRACT'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT Question_Choce_Id, prescription_id FROM Question_Choices, Prescriptions
  WHERE prescription_name IN ('4.1105(a)(1)') 
  AND choice_text = 'SOLICITATION/AWARD ISSUED IN THE UNITED STATES (50 STATES PLUS DISTRICT OF COLUMBIA)' AND  question_id IN (SELECT question_id FROM Questions WHERE clause_version_id = 15 AND question_code = '[PLACE OF ISSUANCE]'); -- [PLACE OF ISSUANCE] 'SOLICITATION/AWARD ISSUED IN THE UNITED STATES (50 STATES PLUS DISTRICT OF COLUMBIA)'


DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'THE AWARD INVOLVES UNCLASSIFIED BUT SENSITIVE INFORMATION' INNER JOIN Questions Q  ON Q.question_code = '[SECURITY REQUIREMENTS]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '4.404(a)';


DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'UNCLASSIFIED' INNER JOIN Questions Q  ON Q.question_code = '[DOCUMENT SECURITY LEVEL]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '4.1705(b)';

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT Question_Choce_Id, prescription_id FROM Question_Choices, Prescriptions
  WHERE prescription_name IN ('4.1105(a)(1)') 
  AND choice_text = 'UNCLASSIFIED' AND  question_id IN (SELECT question_id FROM Questions WHERE clause_version_id = 15 AND question_code = '[DOCUMENT SECURITY LEVEL]'); -- [DOCUMENT SECURITY LEVEL] 'UNCLASSIFIED'


DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'FOR USE IN CONTINGENCY OPERATIONS' INNER JOIN Questions Q  ON Q.question_code = '[PURPOSE OF PROCUREMENT]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '2015-00016';

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT Question_Choce_Id, prescription_id FROM Question_Choices, Prescriptions
  WHERE prescription_name IN ('247.574(b)(1)','247.574(b)(2)') 
  AND choice_text = 'FOR USE IN CONTINGENCY OPERATIONS' AND  question_id IN (SELECT question_id FROM Questions WHERE clause_version_id = 15 AND question_code = '[PURPOSE OF PROCUREMENT]'); -- [PURPOSE OF PROCUREMENT] 'FOR USE IN CONTINGENCY OPERATIONS'

DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'FOR USE IN DEFENDING AGAINST OR RECOVERY FROM NUCLEAR, BIOLOGICAL, CHEMICAL, OR RADIOLOGICAL ATTACK' INNER JOIN Questions Q  ON Q.question_code = '[PURPOSE OF PROCUREMENT]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '2.101';

DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'FOR USE IN SUPPORT OF OPERATION UNITED ASSISTANCE (OUA) IN THE USAFRICOM THEATRE OF OPERATIONS' INNER JOIN Questions Q  ON Q.question_code = '[PURPOSE OF PROCUREMENT]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '2015-00003';

DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'FOR USE IN SUPPORTING A DIPLOMATIC OR CONSULAR MISSION THAT HAS BEEN DESIGNATED AS A DANGER PAY POST BY THE DEPARTMENT OF STATE' INNER JOIN Questions Q  ON Q.question_code = '[PURPOSE OF PROCUREMENT]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '4.1105(a)(1)';

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT Question_Choce_Id, prescription_id FROM Question_Choices, Prescriptions
  WHERE prescription_name IN ('247.574(b)(1)','247.574(b)(2)') 
  AND choice_text = 'FOR USE IN HUMANITARIAN OR PEACEKEEPING OPERATIONS' AND  question_id IN (SELECT question_id FROM Questions WHERE clause_version_id = 15 AND question_code = '[PURPOSE OF PROCUREMENT]'); -- [PURPOSE OF PROCUREMENT] 'FOR USE IN HUMANITARIAN OR PEACEKEEPING OPERATIONS'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT Question_Choce_Id, prescription_id FROM Question_Choices, Prescriptions
  WHERE prescription_name IN ('225.302-6') 
  AND choice_text = 'FOR USE IN COMBAT AND OTHER SIGNIFICANT MILITARY OPERATIONS' AND  question_id IN (SELECT question_id FROM Questions WHERE clause_version_id = 15 AND question_code = '[PURPOSE OF PROCUREMENT]'); -- [PURPOSE OF PROCUREMENT] 'FOR USE IN COMBAT AND OTHER SIGNIFICANT MILITARY OPERATIONS'

DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'FOR USE IN SUPPORT OF OPERATIONS IN AFGHANISTAN' INNER JOIN Questions Q  ON Q.question_code = '[PURPOSE OF PROCUREMENT]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '225.1101(1)(i)';

DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'FOR USE IN SUPPORT OF OPERATIONS IN AFGHANISTAN' INNER JOIN Questions Q  ON Q.question_code = '[PURPOSE OF PROCUREMENT]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '225.1101(1)(ii)';

DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'FOR USE IN SUPPORT OF OPERATIONS IN AFGHANISTAN' INNER JOIN Questions Q  ON Q.question_code = '[PURPOSE OF PROCUREMENT]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '225.1101(5)';

DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'FOR USE IN SUPPORT OF OPERATIONS IN AFGHANISTAN' INNER JOIN Questions Q  ON Q.question_code = '[PURPOSE OF PROCUREMENT]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '225.1101(5)(ii)';

DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'FOR USE IN SUPPORT OF OPERATIONS IN AFGHANISTAN' INNER JOIN Questions Q  ON Q.question_code = '[PURPOSE OF PROCUREMENT]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '225.1101(6)';

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT Question_Choce_Id, prescription_id FROM Question_Choices, Prescriptions
  WHERE prescription_name IN ('225.1101(10)(i)(B)','25.1101(1)','25.1101(1)(i)','25.1101(1)(ii)','25.1101(6)(i)') 
  AND choice_text = 'FOR USE IN SUPPORT OF OPERATIONS IN AFGHANISTAN' AND  question_id IN (SELECT question_id FROM Questions WHERE clause_version_id = 15 AND question_code = '[PURPOSE OF PROCUREMENT]'); -- [PURPOSE OF PROCUREMENT] 'FOR USE IN SUPPORT OF OPERATIONS IN AFGHANISTAN'


DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'FOR USE IN A GOVERNMENT OWNED OR DOD CONTROLLED FACILITY' INNER JOIN Questions Q  ON Q.question_code = '[PLACE OF USE]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '225.7011-3';

DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'FOR USE INSIDE THE UNITED STATES (50 STATES, DISTRICT OF COLUMBIA, OR UNITED STATES OUTLYING AREAS)' INNER JOIN Questions Q  ON Q.question_code = '[PLACE OF USE]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '13.302-5(d)(3)(i)';


DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'THIS ACQUISITION IS COVERED BY THE WORLD TRADE ORGANIZATION (WTO) AGREEMENT ON GOVERNMENT PROCUREMENT (GPA)' INNER JOIN Questions Q  ON Q.question_code = '[MISCELLANEOUS PROGRAM CHARACTERISTICS]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '225.1101(6)';

DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'THIS ACQUISITION IS COVERED BY THE WORLD TRADE ORGANIZATION (WTO) AGREEMENT ON GOVERNMENT PROCUREMENT (GPA)' INNER JOIN Questions Q  ON Q.question_code = '[MISCELLANEOUS PROGRAM CHARACTERISTICS]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '225.1101(6)(i)';

DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'THIS ACQUISITION IS COVERED BY THE WORLD TRADE ORGANIZATION (WTO) AGREEMENT ON GOVERNMENT PROCUREMENT (GPA)' INNER JOIN Questions Q  ON Q.question_code = '[MISCELLANEOUS PROGRAM CHARACTERISTICS]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '225.1101(6)(ii)';

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT Question_Choce_Id, prescription_id FROM Question_Choices, Prescriptions
  WHERE prescription_name IN ('25.1101(6)(i)','25.1101(6)(ii)') 
  AND choice_text = 'THIS ACQUISITION IS COVERED BY THE WORLD TRADE ORGANIZATION (WTO) AGREEMENT ON GOVERNMENT PROCUREMENT (GPA)' AND  question_id IN (SELECT question_id FROM Questions WHERE clause_version_id = 15 AND question_code = '[MISCELLANEOUS PROGRAM CHARACTERISTICS]'); -- [MISCELLANEOUS PROGRAM CHARACTERISTICS] 'THIS ACQUISITION IS COVERED BY THE WORLD TRADE ORGANIZATION (WTO) AGREEMENT ON GOVERNMENT PROCUREMENT (GPA)'

DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'THE ACQUISITION COVERS ESTIMATED REQUIREMENTS THAT EXCEED A SPECIFIC GOVERNMENT ACTIVITY''S INTERNAL CAPABILITY TO PRODUCE OR PERFORM' INNER JOIN Questions Q  ON Q.question_code = '[MISCELLANEOUS PROGRAM CHARACTERISTICS]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '16.506(d)(2)';


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT Question_Choce_Id, prescription_id FROM Question_Choices, Prescriptions
  WHERE prescription_name IN ('22.810(d)') 
  AND choice_text = 'YES' AND  question_id IN (SELECT question_id FROM Questions WHERE clause_version_id = 15 AND question_code = '[SAM EXEMPTION]'); -- [SAM EXEMPTION] 'YES'


DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'DHS HAS ISSUED A PRE-QUALIFICATION DESIGNATION NOTICE' INNER JOIN Questions Q  ON Q.question_code = '[SAFETY ACT STATEMENTS]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '50.206(c)(2)';

DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'DHS HAS ISSUED A PRE-QUALIFICATION DESIGNATION NOTICE' INNER JOIN Questions Q  ON Q.question_code = '[SAFETY ACT STATEMENTS]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '50.206(c)(3)';


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT Question_Choce_Id, prescription_id FROM Question_Choices, Prescriptions
  WHERE prescription_name IN ('4.1105(a)(1)') 
  AND choice_text = 'FOREIGN SOURCE' AND  question_id IN (SELECT question_id FROM Questions WHERE clause_version_id = 15 AND question_code = '[BUSINESS TYPE]'); -- [BUSINESS TYPE] 'FOREIGN SOURCE'


DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'YES' INNER JOIN Questions Q  ON Q.question_code = '[GENERIC DUNS]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '4.1705(a)';

DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'YES' INNER JOIN Questions Q  ON Q.question_code = '[GENERIC DUNS]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '4.1705(b)';

DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'YES' INNER JOIN Questions Q  ON Q.question_code = '[GENERIC DUNS]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '4.607(b)';

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT Question_Choce_Id, prescription_id FROM Question_Choices, Prescriptions
  WHERE prescription_name IN ('4.1705(a)','4.1705(b)','4.607(b)') 
  AND choice_text = 'NO' AND  question_id IN (SELECT question_id FROM Questions WHERE clause_version_id = 15 AND question_code = '[GENERIC DUNS]'); -- [GENERIC DUNS] 'NO'


DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'NATO' INNER JOIN Questions Q  ON Q.question_code = '[CONTRACTOR PERFORM ON BEHALF OF FOREIGN COUNTRY]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '229.402-70(l)';

DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'FOREIGN COUNTRY' INNER JOIN Questions Q  ON Q.question_code = '[CONTRACTOR PERFORM ON BEHALF OF FOREIGN COUNTRY]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '22.810(g)';

INSERT INTO Questions (clause_version_id, question_code, question_type, question_group, question_text)
VALUES (15, '[DPAP APPROVAL FOR USE OF 252.229-7015]', 'B', 'C', 'HAS APPROVAL BEEN OBTAINED FROM THE DIRECTOR, DEFENSE PROCUREMENT AND ACQUISITION POLICY, OFFICE OF THE UNDER SECRETARY OF DEFENSE FOR ACQUISITION, TECHNOLOGY, AND LOGISTICS FOR USE OF CLAUSE 252.229-7015, TAXES--FOREIGN CONTRACTS IN AFGHANISTAN (NORTH ATLANTIC TREATY ORGANIZATION STATUS OF FORCES AGREEMENT)');
INSERT INTO Question_Conditions (question_condition_sequence, question_id, parent_question_id, default_baseline, original_condition_text, condition_to_question)
SELECT 655, question_id, NULL , 0
, 'ASK ONLY IF THE USER SELECTS "NATO" UNDER "CONTRACTOR PERFORM ON BEHALF OF FOREIGN COUNTRY" AND DO NOT ASK IF THE USER SELECTED "COMMERCIAL PROCEDURES (FAR PART 12)" UNDER "PROCEDURES"'
, '"NATO" = [CONTRACTOR PERFORM ON BEHALF OF FOREIGN COUNTRY] AND "COMMERCIAL PROCEDURES (FAR PART 12)" <> [PROCEDURES]' FROM Questions WHERE clause_version_id = 15 AND question_code = '[DPAP APPROVAL FOR USE OF 252.229-7015]';
INSERT INTO Question_Choices( question_id, choice_text, prompt_after_selection)
SELECT question_id, 'YES', NULL FROM Questions WHERE clause_version_id = 15 AND question_code = '[DPAP APPROVAL FOR USE OF 252.229-7015]'; -- [DPAP APPROVAL FOR USE OF 252.229-7015]
INSERT INTO Question_Choices( question_id, choice_text, prompt_after_selection)
SELECT question_id, 'NO', NULL FROM Questions WHERE clause_version_id = 15 AND question_code = '[DPAP APPROVAL FOR USE OF 252.229-7015]'; -- [DPAP APPROVAL FOR USE OF 252.229-7015]


DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'SUPPLIES' INNER JOIN Questions Q  ON Q.question_code = '[SUPPLIES OR SERVICES]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '13.302-5(d)(3)(i)';

DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'SUPPLIES' INNER JOIN Questions Q  ON Q.question_code = '[SUPPLIES OR SERVICES]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '225.1101(6)';

DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'SUPPLIES' INNER JOIN Questions Q  ON Q.question_code = '[SUPPLIES OR SERVICES]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '225.7307(b)';

DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'SUPPLIES' INNER JOIN Questions Q  ON Q.question_code = '[SUPPLIES OR SERVICES]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '25.1101(6)(ii)';

DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'SUPPLIES' INNER JOIN Questions Q  ON Q.question_code = '[SUPPLIES OR SERVICES]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '32.111(b)(2)';

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT Question_Choce_Id, prescription_id FROM Question_Choices, Prescriptions
  WHERE prescription_name IN ('225.1101(4)','247.574(b)(3)','25.1103(d)','42.1305(c)','43.205(a)(1)') 
  AND choice_text = 'SUPPLIES' AND  question_id IN (SELECT question_id FROM Questions WHERE clause_version_id = 15 AND question_code = '[SUPPLIES OR SERVICES]'); -- [SUPPLIES OR SERVICES] 'SUPPLIES'

DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'SERVICES' INNER JOIN Questions Q  ON Q.question_code = '[SUPPLIES OR SERVICES]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '11.703(a)';

DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'SERVICES' INNER JOIN Questions Q  ON Q.question_code = '[SUPPLIES OR SERVICES]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '19.811-3';

DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'SERVICES' INNER JOIN Questions Q  ON Q.question_code = '[SUPPLIES OR SERVICES]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '25.110';

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT Question_Choce_Id, prescription_id FROM Question_Choices, Prescriptions
  WHERE prescription_name IN ('19.811-3(e)','211.274-6(a)(1)','222.7201(a)','23.406(b)','23.406(e)','25.1103(d)') 
  AND choice_text = 'SERVICES' AND  question_id IN (SELECT question_id FROM Questions WHERE clause_version_id = 15 AND question_code = '[SUPPLIES OR SERVICES]'); -- [SUPPLIES OR SERVICES] 'SERVICES'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT Question_Choce_Id, prescription_id FROM Question_Choices, Prescriptions
  WHERE prescription_name IN ('15.209(b)(3)','15.408(n)(2)','15.408(n)(2)(iii)','16.307(a)','16.307(a)(2)','16.307(a)(3)','16.307(a)(4)','16.307(a)(5)','16.307(g)','16.603-4(c)','215.408(3)(i)','215.408(3)(ii)','22.103-5(b)','22.407(b)','22.407(e)','232.412-70(a)','234.203(1)','234.203(2)','235.070-3','242.7204','242.7503','28.311-1','29.401-4(b)','29.402-2(a)','29.402-2(b)','32.412(c)','32.706-2(a)','32.706-2(b)','32.908(c)(3)','33.106(b)','36.507','36.518','36.519','4.404(a)','42.1305(b)(2)','42.709-6','42.802','43.205(b)(1)','43.205(b)(2)','43.205(b)(3)','43.205(b)(4)','43.205(b)(6)','44.204(a)(1)','44.204(a)(2)','44.204(a)(3)','45.107(a)','45.107(a)(2)','45.107(a)(3)','46.303','46.305','46.308','47.103-2','47.104-4','49.502(d)','49.503(a)(1)','49.503(a)(2)','49.503(a)(3)','49.505(b)','50.403-3','51.205','9.308-1(a)(1)','9.308-1(a)(2)','9.308-1(a)(3)','9.308-1(b)(1)','9.308-1(b)(2)','9.308-1(b)(3)','9.308-2(a)(1)') 
  AND choice_text = 'COST REIMBURSEMENT' AND  question_id IN (SELECT question_id FROM Questions WHERE clause_version_id = 15 AND question_code = '[CONTRACT TYPE]'); -- [CONTRACT TYPE] 'COST REIMBURSEMENT'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT Question_Choce_Id, prescription_id FROM Question_Choices, Prescriptions
  WHERE prescription_name IN ('11.404(b)','11.503(a)','11.703(a)','11.703(b)','11.703(c)','16.105','16.203-4(a)','16.203-4(b)','16.203-4(c)','215.408(3)(i)','215.408(3)(ii)','216.203-4-70(a)(1)','216.203-4-70(b)','216.203-4-70(c)(1)','22.1006(c)(1)','22.1006(c)(2)','22.407(e)','22.407(f)','22.407(g)','229.402-1','235.070-3','236.570(a)','236.570(b)(1)','236.570(b)(2)(i)','236.570(b)(2)(ii)','236.570(b)(4)','236.570(b)(5)','236.570(b)(6)','236.609-70','242.7204','243.205-70','246.710(2)','28.310','29.401-1','29.401-3','29.401-3(a)','29.401-3(b)','29.402-1(a)','29.402-1(b)','32.1005','32.111(a)(1)','32.111(a)(2)','32.111(a)(5)','32.111(b)(1)','32.111(c)(1)','32.111(c)(2)','32.502-4(a)','32.502-4(b)','32.502-4(c)','32.502-4(d)','36.501(b)','36.502','36.503','36.504','36.506','36.507','36.508','36.509','36.510','36.511','36.512','36.513','36.514','36.515','36.516','36.517','36.521','36.522','36.609-1(c)','36.609-2(b)','42.1305(a)','42.1305(c)','43.205(a)(1)','43.205(a)(2)','43.205(a)(3)','43.205(a)(4)','43.205(a)(5)','43.205(a)(6)','43.205(b)(4)','44.204(a)(1)','44.204(a)(3)','45.107(a)','45.107(a)(2)','45.107(a)(3)','45.107(b)','46.302','46.304','46.307(a)','46.312','46.316','46.710(a)(1)','46.710(a)(3)','46.710(a)(6)','46.710(b)(1)','46.710(b)(2)','46.710(b)(4)','46.710(c)(1)','46.710(c)(2)','46.710(c)(4)','46.710(d)','46.710(e)(1)','46.710(e)(2)','47.104-4','47.303-1(c)','47.303-10(c)','47.303-11(c)','47.303-12(c)','47.303-13(c)','47.303-14(c)','47.303-15(c)','47.303-16(c)','47.303-17(f)','47.303-2(c)','47.303-3(c)','47.303-4(c)','47.303-5(c)','47.303-6(c)','47.303-7(c)','47.303-8(c)','47.303-9(c)','47.305-12(a)(2)','47.305-13(a)(3)(i)','47.305-14(b)(4)','47.305-15(a)(2)','47.305-16(a)','47.305-16(b)(1)','47.305-16(c)','47.305-16(d)(2)','47.305-17','47.305-2(b)','47.305-3(b)(4)(ii)','47.305-4(c)','47.305-5(b)(2)','47.305-5(c)(1)','47.305-6(e)','47.305-6(e)(1)','47.305-6(e)(2)','47.305-6(e)(3)','47.305-6(f)(2)','47.305-9(b)(1)','49.502(a)(1)','49.502(a)(2)','49.502(b)(1)(i)','49.502(b)(1)(ii)','49.502(b)(1)(iii)','49.502(b)(2)','49.502(c)','49.502(d)','49.503(b)','49.504(a)(1)','49.504(a)(2)','49.504(b)','49.504(c)(1)','49.504(c)(2)','49.504(c)(3)','9.308-1(a)(1)','9.308-1(a)(2)','9.308-1(a)(3)','9.308-1(b)(1)','9.308-1(b)(2)','9.308-1(b)(3)','9.308-2(a)(1)') 
  AND choice_text = 'FIXED PRICE' AND  question_id IN (SELECT question_id FROM Questions WHERE clause_version_id = 15 AND question_code = '[CONTRACT TYPE]'); -- [CONTRACT TYPE] 'FIXED PRICE'

DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'TIME AND MATERIALS' INNER JOIN Questions Q  ON Q.question_code = '[CONTRACT TYPE]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '32.111(b)(2)';

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT Question_Choce_Id, prescription_id FROM Question_Choices, Prescriptions
  WHERE prescription_name IN ('32.1110(b)(2)','45.107(a)','45.107(a)(2)','45.107(a)(3)') 
  AND choice_text = 'TIME AND MATERIALS' AND  question_id IN (SELECT question_id FROM Questions WHERE clause_version_id = 15 AND question_code = '[CONTRACT TYPE]'); -- [CONTRACT TYPE] 'TIME AND MATERIALS'


DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'YES' INNER JOIN Questions Q  ON Q.question_code = '[OPTIONS]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '22.1006(c)(2)';

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT Question_Choce_Id, prescription_id FROM Question_Choices, Prescriptions
  WHERE prescription_name IN ('217.208-70(a)','217.208-70(a)(1)','217.208-70(a)(2)') 
  AND choice_text = 'YES' AND  question_id IN (SELECT question_id FROM Questions WHERE clause_version_id = 15 AND question_code = '[OPTIONS]'); -- [OPTIONS] 'YES'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT Question_Choce_Id, prescription_id FROM Question_Choices, Prescriptions
  WHERE prescription_name IN ('22.1006(c)(2)') 
  AND choice_text = 'NO' AND  question_id IN (SELECT question_id FROM Questions WHERE clause_version_id = 15 AND question_code = '[OPTIONS]'); -- [OPTIONS] 'NO'


DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'YES' INNER JOIN Questions Q  ON Q.question_code = '[EPEAT SILVER/GOLD]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '23.705(b)(1)';

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT Question_Choce_Id, prescription_id FROM Question_Choices, Prescriptions
  WHERE prescription_name IN ('23.705(b)(2)') 
  AND choice_text = 'YES' AND  question_id IN (SELECT question_id FROM Questions WHERE clause_version_id = 15 AND question_code = '[EPEAT SILVER/GOLD]'); -- [EPEAT SILVER/GOLD] 'YES'


DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'THE SUPPLIES ARE COMMERCIAL ITEMS' INNER JOIN Questions Q  ON Q.question_code = '[COMMERCIAL ITEMS]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '2013-00019';

DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'THE SUPPLIES ARE COMMERCIAL ITEMS' INNER JOIN Questions Q  ON Q.question_code = '[COMMERCIAL ITEMS]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '4.1202(a)';

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT Question_Choce_Id, prescription_id FROM Question_Choices, Prescriptions
  WHERE prescription_name IN ('2013-O0019','247.574(b)(1)') 
  AND choice_text = 'THE SUPPLIES ARE COMMERCIAL ITEMS' AND  question_id IN (SELECT question_id FROM Questions WHERE clause_version_id = 15 AND question_code = '[COMMERCIAL ITEMS]'); -- [COMMERCIAL ITEMS] 'THE SUPPLIES ARE COMMERCIAL ITEMS'


DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'FINISHED SUPPLIES THAT CAN BE SUPPLIED IN THE OPEN MARKET OR FROM EXISTING STOCK' INNER JOIN Questions Q  ON Q.question_code = '[SUPPLY CLASSIFICATION]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '22.202';

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT Question_Choce_Id, prescription_id FROM Question_Choices, Prescriptions
  WHERE prescription_name IN ('23.406(e)') 
  AND choice_text = 'EPA-DESIGNATED ITEMS' AND  question_id IN (SELECT question_id FROM Questions WHERE clause_version_id = 15 AND question_code = '[SUPPLY CLASSIFICATION]'); -- [SUPPLY CLASSIFICATION] 'EPA-DESIGNATED ITEMS'

DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'COMMODITIES' INNER JOIN Questions Q  ON Q.question_code = '[SUPPLY CLASSIFICATION]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '2013-00017';

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT Question_Choce_Id, prescription_id FROM Question_Choices, Prescriptions
  WHERE prescription_name IN ('2013-O0017') 
  AND choice_text = 'COMMODITIES' AND  question_id IN (SELECT question_id FROM Questions WHERE clause_version_id = 15 AND question_code = '[SUPPLY CLASSIFICATION]'); -- [SUPPLY CLASSIFICATION] 'COMMODITIES'


DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'YES' INNER JOIN Questions Q  ON Q.question_code = '[TRADE AGREEMENT APPLICABILITY]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '13.302-5(d)(3)(i)';


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT Question_Choce_Id, prescription_id FROM Question_Choices, Prescriptions
  WHERE prescription_name IN ('237.7101(e)(1)','237.7101(e)(2)') 
  AND choice_text = 'LAUNDRY AND DRY CLEANING (BULK WEIGHT BASIS)' AND  question_id IN (SELECT question_id FROM Questions WHERE clause_version_id = 15 AND question_code = '[LAUNDRY AND DRY CLEANING]'); -- [LAUNDRY AND DRY CLEANING] 'LAUNDRY AND DRY CLEANING (BULK WEIGHT BASIS)'


DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'YES' INNER JOIN Questions Q  ON Q.question_code = '[WITHHOLDING ALLOWABLE COST]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '16.307(e)(2)';

DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'YES' INNER JOIN Questions Q  ON Q.question_code = '[WITHHOLDING ALLOWABLE COST]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '16.307(f)(2)';

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT Question_Choce_Id, prescription_id FROM Question_Choices, Prescriptions
  WHERE prescription_name IN ('16.307(e)(2)','16.307(f)(2)') 
  AND choice_text = 'NO' AND  question_id IN (SELECT question_id FROM Questions WHERE clause_version_id = 15 AND question_code = '[WITHHOLDING ALLOWABLE COST]'); -- [WITHHOLDING ALLOWABLE COST] 'NO'


DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'YES' INNER JOIN Questions Q  ON Q.question_code = '[PORT OF ENTRY REQUIREMENTS]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '237-7003(b)';

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT Question_Choce_Id, prescription_id FROM Question_Choices, Prescriptions
  WHERE prescription_name IN ('237.7003(b)') 
  AND choice_text = 'YES' AND  question_id IN (SELECT question_id FROM Questions WHERE clause_version_id = 15 AND question_code = '[PORT OF ENTRY REQUIREMENTS]'); -- [PORT OF ENTRY REQUIREMENTS] 'YES'


DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'YES' INNER JOIN Questions Q  ON Q.question_code = '[BILLS OF LADING]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '46.314';

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT Question_Choce_Id, prescription_id FROM Question_Choices, Prescriptions
  WHERE prescription_name IN ('46.314') 
  AND choice_text = 'NO' AND  question_id IN (SELECT question_id FROM Questions WHERE clause_version_id = 15 AND question_code = '[BILLS OF LADING]'); -- [BILLS OF LADING] 'NO'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT Question_Choce_Id, prescription_id FROM Question_Choices, Prescriptions
  WHERE prescription_name IN ('225.1101(4)') 
  AND choice_text = 'THE SERVICE BEING PROCURED INCLUDES THE FURNISHING OF SUPPLIES' AND  question_id IN (SELECT question_id FROM Questions WHERE clause_version_id = 15 AND question_code = '[SERVICE CHARACTERISTICS]'); -- [SERVICE CHARACTERISTICS] 'THE SERVICE BEING PROCURED INCLUDES THE FURNISHING OF SUPPLIES'


DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'YES' INNER JOIN Questions Q  ON Q.question_code = '[COMMERCIAL SERVICES]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '10.003';

DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'YES' INNER JOIN Questions Q  ON Q.question_code = '[COMMERCIAL SERVICES]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '13.302-5(d)';

DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'YES' INNER JOIN Questions Q  ON Q.question_code = '[COMMERCIAL SERVICES]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '15.209(b)(1)';

DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'YES' INNER JOIN Questions Q  ON Q.question_code = '[COMMERCIAL SERVICES]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '15.209(b)(3)';

DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'YES' INNER JOIN Questions Q  ON Q.question_code = '[COMMERCIAL SERVICES]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '15.209(b)(4)';

DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'YES' INNER JOIN Questions Q  ON Q.question_code = '[COMMERCIAL SERVICES]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '15.408(f)(1)';

DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'YES' INNER JOIN Questions Q  ON Q.question_code = '[COMMERCIAL SERVICES]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '15.408(f)(2)';

DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'YES' INNER JOIN Questions Q  ON Q.question_code = '[COMMERCIAL SERVICES]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '16.601(f)(1)';

DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'YES' INNER JOIN Questions Q  ON Q.question_code = '[COMMERCIAL SERVICES]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '16.601(f)(2)';

DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'YES' INNER JOIN Questions Q  ON Q.question_code = '[COMMERCIAL SERVICES]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '203.1004(b)(2)(ii)';

DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'YES' INNER JOIN Questions Q  ON Q.question_code = '[COMMERCIAL SERVICES]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '203.570-3';

DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'YES' INNER JOIN Questions Q  ON Q.question_code = '[COMMERCIAL SERVICES]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '216.601(e)';

DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'YES' INNER JOIN Questions Q  ON Q.question_code = '[COMMERCIAL SERVICES]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '217.7303';

DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'YES' INNER JOIN Questions Q  ON Q.question_code = '[COMMERCIAL SERVICES]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '22.1310(c)';

DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'YES' INNER JOIN Questions Q  ON Q.question_code = '[COMMERCIAL SERVICES]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '22.305';

DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'YES' INNER JOIN Questions Q  ON Q.question_code = '[COMMERCIAL SERVICES]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '222.7405';

DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'YES' INNER JOIN Questions Q  ON Q.question_code = '[COMMERCIAL SERVICES]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '223.570-2';

DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'YES' INNER JOIN Questions Q  ON Q.question_code = '[COMMERCIAL SERVICES]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '223.7203';

DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'YES' INNER JOIN Questions Q  ON Q.question_code = '[COMMERCIAL SERVICES]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '225.7204(a)';

DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'YES' INNER JOIN Questions Q  ON Q.question_code = '[COMMERCIAL SERVICES]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '225.7204(b)';

DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'YES' INNER JOIN Questions Q  ON Q.question_code = '[COMMERCIAL SERVICES]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '228.370(b)';

DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'YES' INNER JOIN Questions Q  ON Q.question_code = '[COMMERCIAL SERVICES]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '23.505';

DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'YES' INNER JOIN Questions Q  ON Q.question_code = '[COMMERCIAL SERVICES]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '242.7204';

DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'YES' INNER JOIN Questions Q  ON Q.question_code = '[COMMERCIAL SERVICES]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '25.1103(d)';

DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'YES' INNER JOIN Questions Q  ON Q.question_code = '[COMMERCIAL SERVICES]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '28.106-4(b)';

DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'YES' INNER JOIN Questions Q  ON Q.question_code = '[COMMERCIAL SERVICES]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '3.104-9(a)';

DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'YES' INNER JOIN Questions Q  ON Q.question_code = '[COMMERCIAL SERVICES]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '3.104-9(b)';

DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'YES' INNER JOIN Questions Q  ON Q.question_code = '[COMMERCIAL SERVICES]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '3.404';

DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'YES' INNER JOIN Questions Q  ON Q.question_code = '[COMMERCIAL SERVICES]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '3.502-3';

DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'YES' INNER JOIN Questions Q  ON Q.question_code = '[COMMERCIAL SERVICES]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '32.111(a)(1)';

DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'YES' INNER JOIN Questions Q  ON Q.question_code = '[COMMERCIAL SERVICES]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '4.1202(a)';

DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'YES' INNER JOIN Questions Q  ON Q.question_code = '[COMMERCIAL SERVICES]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '42.1305(c)';

DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'YES' INNER JOIN Questions Q  ON Q.question_code = '[COMMERCIAL SERVICES]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '44.403';

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT Question_Choce_Id, prescription_id FROM Question_Choices, Prescriptions
  WHERE prescription_name IN ('10.003','13.302-5(d)','15.209(b)(1)','15.209(b)(3)','15.209(b)(4)','15.408(f)(1)','15.408(f)(2)','16.601(f)(1)','16.601(f)(2)','203.1004(b)(2)(ii)','203.570-3','216.601(e)','217.7303','22.1310(c)','22.305','222.7405','223.570-2','223.7203','225.7204(a)','225.7204(b)','228.370(b)','23.505','242.7204','25.1103(d)','28.106-4(b)','3.104-9(a)','3.104-9(b)','3.404','3.502-3','3.503-2','32.111(a)(1)','42.1305(c)','44.403') 
  AND choice_text = 'NO' AND  question_id IN (SELECT question_id FROM Questions WHERE clause_version_id = 15 AND question_code = '[COMMERCIAL SERVICES]'); -- [COMMERCIAL SERVICES] 'NO'


DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'YES' INNER JOIN Questions Q  ON Q.question_code = '[SMALL BUSINESS AWARD FEE]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '19.708(c)(1)';

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT Question_Choce_Id, prescription_id FROM Question_Choices, Prescriptions
  WHERE prescription_name IN ('19.708(c)(1)') 
  AND choice_text = 'NO' AND  question_id IN (SELECT question_id FROM Questions WHERE clause_version_id = 15 AND question_code = '[SMALL BUSINESS AWARD FEE]'); -- [SMALL BUSINESS AWARD FEE] 'NO'


DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'YES' INNER JOIN Questions Q  ON Q.question_code = '[MAJOR DESIGN OR DEVELOPMENT]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '16.203-4(c)';

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT Question_Choce_Id, prescription_id FROM Question_Choices, Prescriptions
  WHERE prescription_name IN ('16.203-4(c)') 
  AND choice_text = 'NO' AND  question_id IN (SELECT question_id FROM Questions WHERE clause_version_id = 15 AND question_code = '[MAJOR DESIGN OR DEVELOPMENT]'); -- [MAJOR DESIGN OR DEVELOPMENT] 'NO'


DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'YES' INNER JOIN Questions Q  ON Q.question_code = '[PCO EPA DETERMINATION]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '16.203-4(a)';

DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'YES' INNER JOIN Questions Q  ON Q.question_code = '[PCO EPA DETERMINATION]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '16.203-4(b)';

DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'YES' INNER JOIN Questions Q  ON Q.question_code = '[PCO EPA DETERMINATION]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '16.203-4(c)';

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT Question_Choce_Id, prescription_id FROM Question_Choices, Prescriptions
  WHERE prescription_name IN ('16.203-4(a)','16.203-4(b)','16.203-4(c)') 
  AND choice_text = 'NO' AND  question_id IN (SELECT question_id FROM Questions WHERE clause_version_id = 15 AND question_code = '[PCO EPA DETERMINATION]'); -- [PCO EPA DETERMINATION] 'NO'


DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'OPTION QUANTITY IS EXPRESSED AS A PERCENTAGE OF BASIC CONTRACT QUANTITY' INNER JOIN Questions Q  ON Q.question_code = '[OPTION CONDITIONS]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '17.208(g)';

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT Question_Choce_Id, prescription_id FROM Question_Choices, Prescriptions
  WHERE prescription_name IN ('17.208(d)') 
  AND choice_text = 'OPTION QUANTITY IS EXPRESSED AS A PERCENTAGE OF BASIC CONTRACT QUANTITY' AND  question_id IN (SELECT question_id FROM Questions WHERE clause_version_id = 15 AND question_code = '[OPTION CONDITIONS]'); -- [OPTION CONDITIONS] 'OPTION QUANTITY IS EXPRESSED AS A PERCENTAGE OF BASIC CONTRACT QUANTITY'


DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'NOT LISTED IN THE DOD ACQUISITION STREAMLINING AND STANDARDIZATION INFORMATION SYSTEM (ASSIST)' INNER JOIN Questions Q  ON Q.question_code = '[PERFORMANCE REQUIREMENTS 1]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '211.204(c)(i)';

DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'NOT LISTED IN THE DOD ACQUISITION STREAMLINING AND STANDARDIZATION INFORMATION SYSTEM (ASSIST)' INNER JOIN Questions Q  ON Q.question_code = '[PERFORMANCE REQUIREMENTS 1]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '211.204(c)(ii)';

DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'NOT LISTED IN THE GSA INDEX, AVAILABLE FOR EXAMINATION AT SPECIFIED LOCATION' INNER JOIN Questions Q  ON Q.question_code = '[PERFORMANCE REQUIREMENTS 1]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '11.204(d)';

DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'NOT LISTED IN THE GSA INDEX, MAY BE OBTAINED FROM DESIGNATED SOURCES' INNER JOIN Questions Q  ON Q.question_code = '[PERFORMANCE REQUIREMENTS 1]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '11.204(c)';


DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'EARNED VALUE MANAGEMENT SYSTEM' INNER JOIN Questions Q  ON Q.question_code = '[PERFORMANCE REQUIREMENTS 2]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '2015-00017';

DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'EARNED VALUE MANAGEMENT SYSTEM' INNER JOIN Questions Q  ON Q.question_code = '[PERFORMANCE REQUIREMENTS 2]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '34.203(a)';

DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'EARNED VALUE MANAGEMENT SYSTEM' INNER JOIN Questions Q  ON Q.question_code = '[PERFORMANCE REQUIREMENTS 2]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '34.203(b)';

DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'FIRST ARTICLE APPROVAL IS REQUIRED' INNER JOIN Questions Q  ON Q.question_code = '[PERFORMANCE REQUIREMENTS 2]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '9.308-2(b)(1)';

DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'FIRST ARTICLE APPROVAL IS REQUIRED' INNER JOIN Questions Q  ON Q.question_code = '[PERFORMANCE REQUIREMENTS 2]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '9.308-2(b)(2)';


DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'THE CONTRACTOR IS AUTHORIZED TO ACCOMPANY US FORCES OUTSIDE THE US' INNER JOIN Questions Q  ON Q.question_code = '[LABOR REQUIREMENTS 2]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '225.7402-5(a)';

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT Question_Choce_Id, prescription_id FROM Question_Choices, Prescriptions
  WHERE prescription_name IN ('225.371-5(a)') 
  AND choice_text = 'THE CONTRACTOR IS AUTHORIZED TO ACCOMPANY US FORCES OUTSIDE THE US' AND  question_id IN (SELECT question_id FROM Questions WHERE clause_version_id = 15 AND question_code = '[LABOR REQUIREMENTS 2]'); -- [LABOR REQUIREMENTS 2] 'THE CONTRACTOR IS AUTHORIZED TO ACCOMPANY US FORCES OUTSIDE THE US'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT Question_Choce_Id, prescription_id FROM Question_Choices, Prescriptions
  WHERE prescription_name IN ('22.505(b)(2)') 
  AND choice_text = 'SUBMISSION OF LABOR AGREEMENT AFTER AWARD' AND  question_id IN (SELECT question_id FROM Questions WHERE clause_version_id = 15 AND question_code = '[LABOR REQUIREMENTS 3]'); -- [LABOR REQUIREMENTS 3] 'SUBMISSION OF LABOR AGREEMENT AFTER AWARD'


DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'IMAGING EQUIPMENT WILL BE FURNISHED BY THE CONTRACTOR FOR USE BY THE GOVERNMENT' INNER JOIN Questions Q  ON Q.question_code = '[CONTRACTOR MATERIAL]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '23.705(c)(1)';

DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'IMAGING EQUIPMENT WILL BE FURNISHED BY THE CONTRACTOR FOR USE BY THE GOVERNMENT' INNER JOIN Questions Q  ON Q.question_code = '[CONTRACTOR MATERIAL]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '23.705(d)(2)';

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT Question_Choce_Id, prescription_id FROM Question_Choices, Prescriptions
  WHERE prescription_name IN ('23.705(b)(1)','23.705(b)(2)') 
  AND choice_text = 'IMAGING EQUIPMENT WILL BE FURNISHED BY THE CONTRACTOR FOR USE BY THE GOVERNMENT' AND  question_id IN (SELECT question_id FROM Questions WHERE clause_version_id = 15 AND question_code = '[CONTRACTOR MATERIAL]'); -- [CONTRACTOR MATERIAL] 'IMAGING EQUIPMENT WILL BE FURNISHED BY THE CONTRACTOR FOR USE BY THE GOVERNMENT'


DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'SUBCONTRACTS FOR SUPPLIES OR SERVICES FOR GOVERNMENT USE ARE REQUIRED TO BE PROCURED FROM THOSE LISTED ON THE PROCUREMENT LIST MAINTAINED BY THE COMMITTEE FOR PURCHASE FROM PEOPLE WHO ARE BLIND OR SEVERELY DISABLE' INNER JOIN Questions Q  ON Q.question_code = '[SUBCONTRACTING]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '8.005';


DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'CONTRACTOR REQUIRED TO INSTALL A PHOTVOLTAIC DEVICE IN THE UNITED STATES OR IN A FACILITY OWNED BY DOD' INNER JOIN Questions Q  ON Q.question_code = '[ENVIRONMENT]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '225.7017-5(a)(1)';


DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'YES' INNER JOIN Questions Q  ON Q.question_code = '[INTEREST ON PARTIAL PAYMENTS]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '49.502(b)(1)(iii)';

DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'YES' INNER JOIN Questions Q  ON Q.question_code = '[INTEREST ON PARTIAL PAYMENTS]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '49.502(b)(2)';


DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'CONTRACTOR IS AUTHORIZED TO PURCHASE MATERIAL OR COMMENCE PRODUCTION BEFORE FIRST ARTICLE APPROVAL' INNER JOIN Questions Q  ON Q.question_code = '[FIRST ARTICLE CONDITIONS]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '9.308-2(a)(3)';

DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'CONTRACTOR IS AUTHORIZED TO PURCHASE MATERIAL OR COMMENCE PRODUCTION BEFORE FIRST ARTICLE APPROVAL' INNER JOIN Questions Q  ON Q.question_code = '[FIRST ARTICLE CONDITIONS]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '9.308-2(b)(3)';

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT Question_Choce_Id, prescription_id FROM Question_Choices, Prescriptions
  WHERE prescription_name IN ('9.308-1(a)(3)','9.308-1(b)(3)') 
  AND choice_text = 'CONTRACTOR IS AUTHORIZED TO PURCHASE MATERIAL OR COMMENCE PRODUCTION BEFORE FIRST ARTICLE APPROVAL' AND  question_id IN (SELECT question_id FROM Questions WHERE clause_version_id = 15 AND question_code = '[FIRST ARTICLE CONDITIONS]'); -- [FIRST ARTICLE CONDITIONS] 'CONTRACTOR IS AUTHORIZED TO PURCHASE MATERIAL OR COMMENCE PRODUCTION BEFORE FIRST ARTICLE APPROVAL'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT Question_Choce_Id, prescription_id FROM Question_Choices, Prescriptions
  WHERE prescription_name IN ('9.308-1(a)(2)','9.308-1(b)(2)') 
  AND choice_text = 'CONTRACTOR TESTING IS REQUIRED' AND  question_id IN (SELECT question_id FROM Questions WHERE clause_version_id = 15 AND question_code = '[FIRST ARTICLE CONDITIONS]'); -- [FIRST ARTICLE CONDITIONS] 'CONTRACTOR TESTING IS REQUIRED'

DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'GOVERNMENT TESTING IS REQUIRED' INNER JOIN Questions Q  ON Q.question_code = '[FIRST ARTICLE CONDITIONS]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '9.308-2(b)(1)';

DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'GOVERNMENT TESTING IS REQUIRED' INNER JOIN Questions Q  ON Q.question_code = '[FIRST ARTICLE CONDITIONS]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '9.308-2(b)(2)';

DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'GOVERNMENT TESTING IS REQUIRED' INNER JOIN Questions Q  ON Q.question_code = '[FIRST ARTICLE CONDITIONS]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '9.308-2(b)(3)';

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT Question_Choce_Id, prescription_id FROM Question_Choices, Prescriptions
  WHERE prescription_name IN ('9.308-1(b)(1)','9.308-1(b)(2)','9.308-1(b)(3)') 
  AND choice_text = 'GOVERNMENT TESTING IS REQUIRED' AND  question_id IN (SELECT question_id FROM Questions WHERE clause_version_id = 15 AND question_code = '[FIRST ARTICLE CONDITIONS]'); -- [FIRST ARTICLE CONDITIONS] 'GOVERNMENT TESTING IS REQUIRED'

DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'FIRST ARTICLE AND PRODUCTION QUANTITY ARE TO BE PRODUCED AT THE SAME FACILITY' INNER JOIN Questions Q  ON Q.question_code = '[FIRST ARTICLE CONDITIONS]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '9.308-2(b)(2)';


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT Question_Choce_Id, prescription_id FROM Question_Choices, Prescriptions
  WHERE prescription_name IN ('45.107(a)(3)') 
  AND choice_text = 'THE GOVERNMENT PROPERTY VALUE IS GREATER THAN SAT' AND  question_id IN (SELECT question_id FROM Questions WHERE clause_version_id = 15 AND question_code = '[GOVERNMENT FURNISHED PROPERTY]'); -- [GOVERNMENT FURNISHED PROPERTY] 'THE GOVERNMENT PROPERTY VALUE IS GREATER THAN SAT'


DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'CONTRACTOR WILL PERFORM INFORMATION ASSURANCE FUNCTION' INNER JOIN Questions Q  ON Q.question_code = '[INSPECTION AND ACCEPTANCE REQUIREMENTS 2]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '239.7103(a)';

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT Question_Choce_Id, prescription_id FROM Question_Choices, Prescriptions
  WHERE prescription_name IN ('239.7103(b)') 
  AND choice_text = 'CONTRACTOR WILL PERFORM INFORMATION ASSURANCE FUNCTION' AND  question_id IN (SELECT question_id FROM Questions WHERE clause_version_id = 15 AND question_code = '[INSPECTION AND ACCEPTANCE REQUIREMENTS 2]'); -- [INSPECTION AND ACCEPTANCE REQUIREMENTS 2] 'CONTRACTOR WILL PERFORM INFORMATION ASSURANCE FUNCTION'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT Question_Choce_Id, prescription_id FROM Question_Choices, Prescriptions
  WHERE prescription_name IN ('46.710(a)(6)') 
  AND choice_text = 'USE OF A WARRANTY CLAUSE IS APPROVED' AND  question_id IN (SELECT question_id FROM Questions WHERE clause_version_id = 15 AND question_code = '[WARRANTY CONDITIONS]'); -- [WARRANTY CONDITIONS] 'USE OF A WARRANTY CLAUSE IS APPROVED'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT Question_Choce_Id, prescription_id FROM Question_Choices, Prescriptions
  WHERE prescription_name IN ('246.710(1)(i)') 
  AND choice_text = 'REQUIRES GREATER PROTECTION THAN FAR PART 46 INSPECTION/WARRANTY CLAUSES' AND  question_id IN (SELECT question_id FROM Questions WHERE clause_version_id = 15 AND question_code = '[WARRANTY CONDITIONS]'); -- [WARRANTY CONDITIONS] 'REQUIRES GREATER PROTECTION THAN FAR PART 46 INSPECTION/WARRANTY CLAUSES'


DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'SUPPLIES INCLUDE BOTH HIGH VALUE ITEMS AND NON HIGH VALUE ITEMS' INNER JOIN Questions Q  ON Q.question_code = '[WARRANTY DESCRIPTION]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '46.805';

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT Question_Choce_Id, prescription_id FROM Question_Choices, Prescriptions
  WHERE prescription_name IN ('246.710(3)(i)(B)') 
  AND choice_text = 'WARRANTY ON SERIALIZED ITEMS IS REQUIRED AND OFFERORS ARE REQUIRED TO ENTER DATA WITH THE OFFER' AND  question_id IN (SELECT question_id FROM Questions WHERE clause_version_id = 15 AND question_code = '[WARRANTY DESCRIPTION]'); -- [WARRANTY DESCRIPTION] 'WARRANTY ON SERIALIZED ITEMS IS REQUIRED AND OFFERORS ARE REQUIRED TO ENTER DATA WITH THE OFFER'


DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = '[NUMERIC DATA ENTRY BY USER]' INNER JOIN Questions Q  ON Q.question_code = '[PERIOD OF PERFORMANCE]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '16.506(f)';

DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = '[NUMERIC DATA ENTRY BY USER]' INNER JOIN Questions Q  ON Q.question_code = '[PERIOD OF PERFORMANCE]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '16.506(g)';

DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = '[NUMERIC DATA ENTRY BY USER]' INNER JOIN Questions Q  ON Q.question_code = '[PERIOD OF PERFORMANCE]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '22.1803';

DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = '[NUMERIC DATA ENTRY BY USER]' INNER JOIN Questions Q  ON Q.question_code = '[PERIOD OF PERFORMANCE]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '249.501-70';

DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = '[NUMERIC DATA ENTRY BY USER]' INNER JOIN Questions Q  ON Q.question_code = '[PERIOD OF PERFORMANCE]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '3.1004(a)';

DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = '[NUMERIC DATA ENTRY BY USER]' INNER JOIN Questions Q  ON Q.question_code = '[PERIOD OF PERFORMANCE]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '36.515';

DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = '[NUMERIC DATA ENTRY BY USER]' INNER JOIN Questions Q  ON Q.question_code = '[PERIOD OF PERFORMANCE]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '8.1104(d)';


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT Question_Choce_Id, prescription_id FROM Question_Choices, Prescriptions
  WHERE prescription_name IN ('23.505','23.804(a)','23.903') 
  AND choice_text = 'ASSOCIATED TERRITORIAL WATERS AND AIR SPACE' AND  question_id IN (SELECT question_id FROM Questions WHERE clause_version_id = 15 AND question_code = '[PLACE OF PERFORMANCE]'); -- [PLACE OF PERFORMANCE] 'ASSOCIATED TERRITORIAL WATERS AND AIR SPACE'

DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'DESIGNATED OPERATIONAL AREA' INNER JOIN Questions Q  ON Q.question_code = '[PLACE OF PERFORMANCE]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '225.7402-5(a)';

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT Question_Choce_Id, prescription_id FROM Question_Choices, Prescriptions
  WHERE prescription_name IN ('229.402-70(a)(1)','8.1104(a)','8.1104(b)','8.1104(c)','8.1104(d)') 
  AND choice_text = 'FOREIGN COUNTRY' AND  question_id IN (SELECT question_id FROM Questions WHERE clause_version_id = 15 AND question_code = '[PLACE OF PERFORMANCE]'); -- [PLACE OF PERFORMANCE] 'FOREIGN COUNTRY'

DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'UNITED STATES OUTLYING AREAS' INNER JOIN Questions Q  ON Q.question_code = '[PLACE OF PERFORMANCE]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '222.7201(a)';

DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'UNITED STATES OUTLYING AREAS' INNER JOIN Questions Q  ON Q.question_code = '[PLACE OF PERFORMANCE]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '4.607(b)';

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT Question_Choce_Id, prescription_id FROM Question_Choices, Prescriptions
  WHERE prescription_name IN ('2015-O0016','225.372-2','36.609-4','4.607(a)') 
  AND choice_text = 'UNITED STATES OUTLYING AREAS' AND  question_id IN (SELECT question_id FROM Questions WHERE clause_version_id = 15 AND question_code = '[PLACE OF PERFORMANCE]'); -- [PLACE OF PERFORMANCE] 'UNITED STATES OUTLYING AREAS'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT Question_Choce_Id, prescription_id FROM Question_Choices, Prescriptions
  WHERE prescription_name IN ('2015-O0016','22.305','23.505','23.804(a)','28.309(a)','28.309(b)','4.607(a)') 
  AND choice_text = 'UNITED STATES (50 STATES PLUS DISTRICT OF COLUMBIA)' AND  question_id IN (SELECT question_id FROM Questions WHERE clause_version_id = 15 AND question_code = '[PLACE OF PERFORMANCE]'); -- [PLACE OF PERFORMANCE] 'UNITED STATES (50 STATES PLUS DISTRICT OF COLUMBIA)'

DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'UNITED STATES CENTRAL COMMAND (USCENTCOM) AREA OF RESPONSIBILITY' INNER JOIN Questions Q  ON Q.question_code = '[PLACE OF PERFORMANCE]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '2014-O0018';

DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'UNITED STATES CENTRAL COMMAND (USCENTCOM) AREA OF RESPONSIBILITY' INNER JOIN Questions Q  ON Q.question_code = '[PLACE OF PERFORMANCE]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '2014-O0020';

DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'UNITED STATES CENTRAL COMMAND (USCENTCOM) AREA OF RESPONSIBILITY' INNER JOIN Questions Q  ON Q.question_code = '[PLACE OF PERFORMANCE]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '2015-00009';

DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'UNITED STATES EUROPEAN COMMAND (USEUCOM) THEATER OF OPERATION' INNER JOIN Questions Q  ON Q.question_code = '[PLACE OF PERFORMANCE]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '2014-O0020';

DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'UNITED STATES AFRICA COMMAND (USAFRICOM) THEATER OF OPERATION' INNER JOIN Questions Q  ON Q.question_code = '[PLACE OF PERFORMANCE]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '2014-O0020';

DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'UNITED STATES SOUTHERN COMMAND (USSOUTHCOM) THEATER OF OPERATION' INNER JOIN Questions Q  ON Q.question_code = '[PLACE OF PERFORMANCE]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '2014-O0016';

DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'UNITED STATES PACIFIC COMMAND (USPACOM) THEATER OF OPERATION' INNER JOIN Questions Q  ON Q.question_code = '[PLACE OF PERFORMANCE]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '2014-O0020';


DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'GOVERNMENT OWNED OR LEASED FACILITIES/INSTALLATIONS' INNER JOIN Questions Q  ON Q.question_code = '[PLACE TYPE]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '225-7011-3';


DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'REQUIRED TIME OF DELIVERY' INNER JOIN Questions Q  ON Q.question_code = '[DELIVERY BASIS]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '11.404(a)(3)';


DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'NO' INNER JOIN Questions Q  ON Q.question_code = '[PACE OF WORK]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '211.503';

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT Question_Choce_Id, prescription_id FROM Question_Choices, Prescriptions
  WHERE prescription_name IN ('211.503(b)') 
  AND choice_text = 'NO' AND  question_id IN (SELECT question_id FROM Questions WHERE clause_version_id = 15 AND question_code = '[PACE OF WORK]'); -- [PACE OF WORK] 'NO'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT Question_Choce_Id, prescription_id FROM Question_Choices, Prescriptions
  WHERE prescription_name IN ('225.1101(10)(i)','225.1101(10)(i)(E)','225.1101(10)(i)(F)') 
  AND choice_text = 'AFGHANISTAN' AND  question_id IN (SELECT question_id FROM Questions WHERE clause_version_id = 15 AND question_code = '[FOREIGN COUNTRY]'); -- [FOREIGN COUNTRY] 'AFGHANISTAN'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT Question_Choce_Id, prescription_id FROM Question_Choices, Prescriptions
  WHERE prescription_name IN ('229.402-70(a)(2)') 
  AND choice_text = 'GERMANY' AND  question_id IN (SELECT question_id FROM Questions WHERE clause_version_id = 15 AND question_code = '[FOREIGN COUNTRY]'); -- [FOREIGN COUNTRY] 'GERMANY'


DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'F.O.B. DESTINATION' INNER JOIN Questions Q  ON Q.question_code = '[TRANSPORTATION REQUIREMENTS 1]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '47.304-7(c)';

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT Question_Choce_Id, prescription_id FROM Question_Choices, Prescriptions
  WHERE prescription_name IN ('47.303-7(c)') 
  AND choice_text = 'F.O.B. DESTINATION' AND  question_id IN (SELECT question_id FROM Questions WHERE clause_version_id = 15 AND question_code = '[TRANSPORTATION REQUIREMENTS 1]'); -- [TRANSPORTATION REQUIREMENTS 1] 'F.O.B. DESTINATION'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT Question_Choce_Id, prescription_id FROM Question_Choices, Prescriptions
  WHERE prescription_name IN ('247.574(b)(1)','247.574(b)(2)','247.574(b)(3)') 
  AND choice_text = 'ITEMS TO BE SHIPPED ARE COMMISSARY OR EXCHANGE CARGOES TRANSPORTED OUTSIDE THE DEFENSE TRANSPORTATION SYSTEM' AND  question_id IN (SELECT question_id FROM Questions WHERE clause_version_id = 15 AND question_code = '[TRANSPORTATION REQUIREMENTS 2]'); -- [TRANSPORTATION REQUIREMENTS 2] 'ITEMS TO BE SHIPPED ARE COMMISSARY OR EXCHANGE CARGOES TRANSPORTED OUTSIDE THE DEFENSE TRANSPORTATION SYSTEM'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT Question_Choce_Id, prescription_id FROM Question_Choices, Prescriptions
  WHERE prescription_name IN ('225.1101(4)') 
  AND choice_text = 'REQUIRES IMPORT OF SUPPLIES INTO THE UNITED STATES (50 STATES, THE DISTRICT OF COLUMBIA AND OUTLYING AREAS)' AND  question_id IN (SELECT question_id FROM Questions WHERE clause_version_id = 15 AND question_code = '[TRANSPORTATION REQUIREMENTS 10]'); -- [TRANSPORTATION REQUIREMENTS 10] 'REQUIRES IMPORT OF SUPPLIES INTO THE UNITED STATES (50 STATES, THE DISTRICT OF COLUMBIA AND OUTLYING AREAS)'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT Question_Choce_Id, prescription_id FROM Question_Choices, Prescriptions
  WHERE prescription_name IN ('247.271-3(a)(1)') 
  AND choice_text = 'INVOLVES PERFORMANCE OF INTRA-CITY OR INTRA-AREA MOVEMENT' AND  question_id IN (SELECT question_id FROM Questions WHERE clause_version_id = 15 AND question_code = '[SHIPPING STATEMENTS]'); -- [SHIPPING STATEMENTS] 'INVOLVES PERFORMANCE OF INTRA-CITY OR INTRA-AREA MOVEMENT'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT Question_Choce_Id, prescription_id FROM Question_Choices, Prescriptions
  WHERE prescription_name IN ('47.207-1(b)(2)') 
  AND choice_text = 'IT IS IN THE GOVERNMENT''S INTEREST NOT TO APPLY REQUIREMENTS TO OBTAIN AUTHORITY FOR OPERATING WITHIN THE STATE' AND  question_id IN (SELECT question_id FROM Questions WHERE clause_version_id = 15 AND question_code = '[SHIPPING STATEMENTS]'); -- [SHIPPING STATEMENTS] 'IT IS IN THE GOVERNMENT'S INTEREST NOT TO APPLY REQUIREMENTS TO OBTAIN AUTHORITY FOR OPERATING WITHIN THE STATE'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT Question_Choce_Id, prescription_id FROM Question_Choices, Prescriptions
  WHERE prescription_name IN ('222.7405') 
  AND choice_text = 'FY2010 FUNDS' AND  question_id IN (SELECT question_id FROM Questions WHERE clause_version_id = 15 AND question_code = '[FUNDING FY]'); -- [FUNDING FY] 'FY2010 FUNDS'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT Question_Choce_Id, prescription_id FROM Question_Choices, Prescriptions
  WHERE prescription_name IN ('222.7405') 
  AND choice_text = 'FY2011 FUNDS' AND  question_id IN (SELECT question_id FROM Questions WHERE clause_version_id = 15 AND question_code = '[FUNDING FY]'); -- [FUNDING FY] 'FY2011 FUNDS'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT Question_Choce_Id, prescription_id FROM Question_Choices, Prescriptions
  WHERE prescription_name IN ('222.7405') 
  AND choice_text = 'FY2012 FUNDS' AND  question_id IN (SELECT question_id FROM Questions WHERE clause_version_id = 15 AND question_code = '[FUNDING FY]'); -- [FUNDING FY] 'FY2012 FUNDS'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT Question_Choce_Id, prescription_id FROM Question_Choices, Prescriptions
  WHERE prescription_name IN ('222.7405') 
  AND choice_text = 'FY2013 FUNDS' AND  question_id IN (SELECT question_id FROM Questions WHERE clause_version_id = 15 AND question_code = '[FUNDING FY]'); -- [FUNDING FY] 'FY2013 FUNDS'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT Question_Choce_Id, prescription_id FROM Question_Choices, Prescriptions
  WHERE prescription_name IN ('222.7405') 
  AND choice_text = 'FY2014 FUNDS' AND  question_id IN (SELECT question_id FROM Questions WHERE clause_version_id = 15 AND question_code = '[FUNDING FY]'); -- [FUNDING FY] 'FY2014 FUNDS'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT Question_Choce_Id, prescription_id FROM Question_Choices, Prescriptions
  WHERE prescription_name IN ('2015-O0010','2015-OO005','222.7405') 
  AND choice_text = 'FY2015 FUNDS' AND  question_id IN (SELECT question_id FROM Questions WHERE clause_version_id = 15 AND question_code = '[FUNDING FY]'); -- [FUNDING FY] 'FY2015 FUNDS'

DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'NON-APPROPRIATED FUNDS' INNER JOIN Questions Q  ON Q.question_code = '[FUNDING FY]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '3.202';


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT Question_Choce_Id, prescription_id FROM Question_Choices, Prescriptions
  WHERE prescription_name IN ('4.1105(a)(1)') 
  AND choice_text = 'ELECTRONIC FUNDS TRANSFER APPLIES' AND  question_id IN (SELECT question_id FROM Questions WHERE clause_version_id = 15 AND question_code = '[BILLING ARRANGEMENTS]'); -- [BILLING ARRANGEMENTS] 'ELECTRONIC FUNDS TRANSFER APPLIES'


DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'FUNDS APPROPRIATED BY THE DEPARTMENT OF DEFENSE APPROPRIATIONS ACT, 2014 AND BY THE MILITARY CONSTRUCTION AND VETERANS AFFAIRS AND RELATED AGENCIES APPROPRIATIONS ACT, 2014 (PUBL L. 113-76, DIVISIONS C AND J)' INNER JOIN Questions Q  ON Q.question_code = '[LEGISLATIVE SOURCE OF FUNDS]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '2014-O0009';

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT Question_Choce_Id, prescription_id FROM Question_Choices, Prescriptions
  WHERE prescription_name IN ('2014-OO0009') 
  AND choice_text = 'FUNDS APPROPRIATED BY THE DEPARTMENT OF DEFENSE APPROPRIATIONS ACT, 2014 AND BY THE MILITARY CONSTRUCTION AND VETERANS AFFAIRS AND RELATED AGENCIES APPROPRIATIONS ACT, 2014 (PUBL L. 113-76, DIVISIONS C AND J)' AND  question_id IN (SELECT question_id FROM Questions WHERE clause_version_id = 15 AND question_code = '[LEGISLATIVE SOURCE OF FUNDS]'); -- [LEGISLATIVE SOURCE OF FUNDS] 'FUNDS APPROPRIATED BY THE DEPARTMENT OF DEFENSE APPROPRIATIONS ACT, 2014 AND BY THE MILITARY CONSTRUCTION AND VETERANS AFFAIRS AND RELATED AGENCIES APPROPRIATIONS ACT, 2014 (PUBL L. 113-76, DIVISIONS C AND J)'

DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'FUNDS MADE AVAILABLE BY THE CONTINUING APPROPRIATIONS ACT, 2016 (Pub. L. 114-53) OR ANY OTHER 2016 APPROPRIATIONS ACT THAT EXTENDS TO FY 2016 FUNDS THE SAME PROHIBITIONS AS CONTAINED IN SECTIONS 744 AND 745 OF DIVISION E, TITLE VII, OF THE CONSOLIDATED AND FURTHER CONTINUING APPROPRIATONS ACT, 2015 (Pub.L. 113-235).' INNER JOIN Questions Q  ON Q.question_code = '[LEGISLATIVE SOURCE OF FUNDS]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '2016-0002';

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT Question_Choce_Id, prescription_id FROM Question_Choices, Prescriptions
  WHERE prescription_name IN ('2016-00002') 
  AND choice_text = 'FUNDS MADE AVAILABLE BY THE CONTINUING APPROPRIATIONS ACT, 2016 (Pub. L. 114-53) OR ANY OTHER 2016 APPROPRIATIONS ACT THAT EXTENDS TO FY 2016 FUNDS THE SAME PROHIBITIONS AS CONTAINED IN SECTIONS 744 AND 745 OF DIVISION E, TITLE VII, OF THE CONSOLIDATED AND FURTHER CONTINUING APPROPRIATONS ACT, 2015 (Pub.L. 113-235).' AND  question_id IN (SELECT question_id FROM Questions WHERE clause_version_id = 15 AND question_code = '[LEGISLATIVE SOURCE OF FUNDS]'); -- [LEGISLATIVE SOURCE OF FUNDS] 'FUNDS MADE AVAILABLE BY THE CONTINUING APPROPRIATIONS ACT, 2016 (Pub. L. 114-53) OR ANY OTHER 2016 APPROPRIATIONS ACT THAT EXTENDS TO FY 2016 FUNDS THE SAME PROHIBITIONS AS CONTAINED IN SECTIONS 744 AND 745 OF DIVISION E, TITLE VII, OF THE CONSOLIDATED AND FURTHER CONTINUING APPROPRIATONS ACT, 2015 (Pub.L. 113-235).'

DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'FUNDS MADE AVAILABLE BY THE CONTINUING APPROPRIATIONS ACT, 2016 (Pub. L 114-53) OR ANY OTHER FY2016 APPROPRIATIONS ACT THAT EXTENDS TO FY2016 FUNDS THE SAME PROHIBITIONS AS CONTAINED IN SECTION 743 OF DIVISION E, TITLE VII, OF THE COLSOLIDATED AND FURTHER CONTINUING APPROPRIATIONS ACT, 2015 (Pub. L. 113-235)' INNER JOIN Questions Q  ON Q.question_code = '[LEGISLATIVE SOURCE OF FUNDS]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '2016-0003';

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT Question_Choce_Id, prescription_id FROM Question_Choices, Prescriptions
  WHERE prescription_name IN ('2016-O0003') 
  AND choice_text = 'FUNDS MADE AVAILABLE BY THE CONTINUING APPROPRIATIONS ACT, 2016 (Pub. L 114-53) OR ANY OTHER FY2016 APPROPRIATIONS ACT THAT EXTENDS TO FY2016 FUNDS THE SAME PROHIBITIONS AS CONTAINED IN SECTION 743 OF DIVISION E, TITLE VII, OF THE COLSOLIDATED AND FURTHER CONTINUING APPROPRIATIONS ACT, 2015 (Pub. L. 113-235)' AND  question_id IN (SELECT question_id FROM Questions WHERE clause_version_id = 15 AND question_code = '[LEGISLATIVE SOURCE OF FUNDS]'); -- [LEGISLATIVE SOURCE OF FUNDS] 'FUNDS MADE AVAILABLE BY THE CONTINUING APPROPRIATIONS ACT, 2016 (Pub. L 114-53) OR ANY OTHER FY2016 APPROPRIATIONS ACT THAT EXTENDS TO FY2016 FUNDS THE SAME PROHIBITIONS AS CONTAINED IN SECTION 743 OF DIVISION E, TITLE VII, OF THE COLSOLIDATED AND FURTHER CONTINUING APPROPRIATIONS ACT, 2015 (Pub. L. 113-235)'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT Question_Choce_Id, prescription_id FROM Question_Choices, Prescriptions
  WHERE prescription_name IN ('228.170') 
  AND choice_text = 'BID GUARANTEE APPLIES' AND  question_id IN (SELECT question_id FROM Questions WHERE clause_version_id = 15 AND question_code = '[BID GUARANTEE OR BOND]'); -- [BID GUARANTEE OR BOND] 'BID GUARANTEE APPLIES'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT Question_Choce_Id, prescription_id FROM Question_Choices, Prescriptions
  WHERE prescription_name IN ('228.170') 
  AND choice_text = 'PAYMENT BONDS APPLY' AND  question_id IN (SELECT question_id FROM Questions WHERE clause_version_id = 15 AND question_code = '[BID GUARANTEE OR BOND]'); -- [BID GUARANTEE OR BOND] 'PAYMENT BONDS APPLY'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT Question_Choce_Id, prescription_id FROM Question_Choices, Prescriptions
  WHERE prescription_name IN ('228.170') 
  AND choice_text = 'PERFORMANCE BONDS APPLY' AND  question_id IN (SELECT question_id FROM Questions WHERE clause_version_id = 15 AND question_code = '[BID GUARANTEE OR BOND]'); -- [BID GUARANTEE OR BOND] 'PERFORMANCE BONDS APPLY'


DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'YES' INNER JOIN Questions Q  ON Q.question_code = '[DISPUTES ACT]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '33.215(b)';

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT Question_Choce_Id, prescription_id FROM Question_Choices, Prescriptions
  WHERE prescription_name IN ('33.215(a)') 
  AND choice_text = 'NO' AND  question_id IN (SELECT question_id FROM Questions WHERE clause_version_id = 15 AND question_code = '[DISPUTES ACT]'); -- [DISPUTES ACT] 'NO'


DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = '9-108-4 -- WAIVER OF THE PROHIBITION ON CONTRACTING WITH ANY FOREIGN ENTITY THAT IS TREATED AS AN INVERTED DOMESTIC CORPORATION' INNER JOIN Questions Q  ON Q.question_code = '[WAIVERS]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '9.108-5(a)';

DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = '9-108-4 -- WAIVER OF THE PROHIBITION ON CONTRACTING WITH ANY FOREIGN ENTITY THAT IS TREATED AS AN INVERTED DOMESTIC CORPORATION' INNER JOIN Questions Q  ON Q.question_code = '[WAIVERS]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '9.108-5(b)';

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT Question_Choce_Id, prescription_id FROM Question_Choices, Prescriptions
  WHERE prescription_name IN ('2013-O0019') 
  AND choice_text = '15.209/25.1001 -- WAIVER OF REQUIREMENT TO EXAMINATION OF RECORDS BY THE COMPTROLLER GENERAL' AND  question_id IN (SELECT question_id FROM Questions WHERE clause_version_id = 15 AND question_code = '[WAIVERS]'); -- [WAIVERS] '15.209/25.1001 -- WAIVER OF REQUIREMENT TO EXAMINATION OF RECORDS BY THE COMPTROLLER GENERAL'

DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = '22.1403 - WAIVER OF ONE OR MORE (BUT NOT ALL) OF THE TERMS OF 52.222-36, "AFFIRMATIVE ACTION FOR WORKERS WITH DISABILITIES"' INNER JOIN Questions Q  ON Q.question_code = '[WAIVERS]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '22.1408(a)';

DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = '22.1603 -- WAIVER OF THE REQUIREMENT TO NOTIFY EMPLOYEES OF THEIR RIGHTS UNDER THE NATIONAL LABOR RELATIONS ACT' INNER JOIN Questions Q  ON Q.question_code = '[WAIVERS]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '22.1605';


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT Question_Choce_Id, prescription_id FROM Question_Choices, Prescriptions
  WHERE prescription_name IN ('225.1101(2)(i)') 
  AND choice_text = '225.7501 -- EXEMPT FROM THE BALANCE OF PAYMENT PROGRAM' AND  question_id IN (SELECT question_id FROM Questions WHERE clause_version_id = 15 AND question_code = '[EXEMPTIONS]'); -- [EXEMPTIONS] '225.7501 -- EXEMPT FROM THE BALANCE OF PAYMENT PROGRAM'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT Question_Choce_Id, prescription_id FROM Question_Choices, Prescriptions
  WHERE prescription_name IN ('225.1101(2)(i)') 
  AND choice_text = '25.202 -- EXCEPTION TO THE BUY AMERICAN STATUTE' AND  question_id IN (SELECT question_id FROM Questions WHERE clause_version_id = 15 AND question_code = '[EXCEPTIONS]'); -- [EXCEPTIONS] '25.202 -- EXCEPTION TO THE BUY AMERICAN STATUTE'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT Question_Choce_Id, prescription_id FROM Question_Choices, Prescriptions
  WHERE prescription_name IN ('225.1101(10)(i)','225.1101(10)(i)(A)','225.1101(10)(i)(B)','225.1101(10)(i)(C)','225.1101(10)(i)(D)','225.1101(10)(i)(E)','225.1101(10)(i)(F)','25.1101(6)(i)','25.1101(6)(ii)') 
  AND choice_text = '25.401 -- EXCEPTION TO FOREIGN TRADE AGREEMENTS' AND  question_id IN (SELECT question_id FROM Questions WHERE clause_version_id = 15 AND question_code = '[EXCEPTIONS]'); -- [EXCEPTIONS] '25.401 -- EXCEPTION TO FOREIGN TRADE AGREEMENTS'

DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = '22.1603 --22.1603 -- SECRETARIAL EXEMPTION TO THE REQUIREMENT TO INCLUDE THE CLAUSE 52.222-40, "NOTIFICATION OF EMPLOYEE RIGHTS UNDER THE NATIONAL LABOR RELATIONS ACT"' INNER JOIN Questions Q  ON Q.question_code = '[EXCEPTIONS]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '22.1605';

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT Question_Choce_Id, prescription_id FROM Question_Choices, Prescriptions
  WHERE prescription_name IN ('23.705(d)(1)') 
  AND choice_text = '23.704(a) - EXCEPTION TO EPEAT REGISTERED ELECTRONIC PRODUCTS' AND  question_id IN (SELECT question_id FROM Questions WHERE clause_version_id = 15 AND question_code = '[EXCEPTIONS]'); -- [EXCEPTIONS] '23.704(a) - EXCEPTION TO EPEAT REGISTERED ELECTRONIC PRODUCTS'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT Question_Choce_Id, prescription_id FROM Question_Choices, Prescriptions
  WHERE prescription_name IN ('223.7106(a)','223.7106(b)') 
  AND choice_text = '223.7102 -- EXCEPTION TO 223.7102 (STORAGE AND DISPOSAL OF TOXIC AND HAZARDOUS MATERIALS)' AND  question_id IN (SELECT question_id FROM Questions WHERE clause_version_id = 15 AND question_code = '[EXCEPTIONS]'); -- [EXCEPTIONS] '223.7102 -- EXCEPTION TO 223.7102 (STORAGE AND DISPOSAL OF TOXIC AND HAZARDOUS MATERIALS)'

DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = '3.704(a) - EXCEPTION TO EPEAT REGISTERED ELECTRONIC PRODUCTS' INNER JOIN Questions Q  ON Q.question_code = '[EXCEPTIONS]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '23.705(b)(2)';

DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = '3.704(a) - EXCEPTION TO EPEAT REGISTERED ELECTRONIC PRODUCTS' INNER JOIN Questions Q  ON Q.question_code = '[EXCEPTIONS]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '23.705(c)(1)';

DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = '3.704(a) - EXCEPTION TO EPEAT REGISTERED ELECTRONIC PRODUCTS' INNER JOIN Questions Q  ON Q.question_code = '[EXCEPTIONS]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '23.705(d)(2)';


DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = '52.227-1 ALTERNATE I -- AUTHORIZATION AND CONSENT' INNER JOIN Questions Q  ON Q.question_code = '[PATENT COUNCIL RECOMMENDED]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '27.201-2(a)(2)';

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT Question_Choce_Id, prescription_id FROM Question_Choices, Prescriptions
  WHERE prescription_name IN ('27.201-2(a)(1)') 
  AND choice_text = '52.227-1 ALTERNATE I -- AUTHORIZATION AND CONSENT' AND  question_id IN (SELECT question_id FROM Questions WHERE clause_version_id = 15 AND question_code = '[PATENT COUNCIL RECOMMENDED]'); -- [PATENT COUNCIL RECOMMENDED] '52.227-1 ALTERNATE I -- AUTHORIZATION AND CONSENT'

DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = '52.227-1 ALTERNATE II -- AUTHORIZATION AND CONSENT' INNER JOIN Questions Q  ON Q.question_code = '[PATENT COUNCIL RECOMMENDED]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '27.201-2(a)(3)';

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT Question_Choce_Id, prescription_id FROM Question_Choices, Prescriptions
  WHERE prescription_name IN ('27.201-2(a)(1)') 
  AND choice_text = '52.227-1 ALTERNATE II -- AUTHORIZATION AND CONSENT' AND  question_id IN (SELECT question_id FROM Questions WHERE clause_version_id = 15 AND question_code = '[PATENT COUNCIL RECOMMENDED]'); -- [PATENT COUNCIL RECOMMENDED] '52.227-1 ALTERNATE II -- AUTHORIZATION AND CONSENT'

DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = '52.227-3 ALTERNATE I -- PATENT INDEMNITY' INNER JOIN Questions Q  ON Q.question_code = '[PATENT COUNCIL RECOMMENDED]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '27.201-2(c)(2)';

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT Question_Choce_Id, prescription_id FROM Question_Choices, Prescriptions
  WHERE prescription_name IN ('27.201-2(c)(1)') 
  AND choice_text = '52.227-3 ALTERNATE I -- PATENT INDEMNITY' AND  question_id IN (SELECT question_id FROM Questions WHERE clause_version_id = 15 AND question_code = '[PATENT COUNCIL RECOMMENDED]'); -- [PATENT COUNCIL RECOMMENDED] '52.227-3 ALTERNATE I -- PATENT INDEMNITY'

DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = '52.227-3 ALTERNATE II -- PATENT INDEMNITY' INNER JOIN Questions Q  ON Q.question_code = '[PATENT COUNCIL RECOMMENDED]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '27.201-2(c)(3)';

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT Question_Choce_Id, prescription_id FROM Question_Choices, Prescriptions
  WHERE prescription_name IN ('27.201-2(c)(2)') 
  AND choice_text = '52.227-3 ALTERNATE II -- PATENT INDEMNITY' AND  question_id IN (SELECT question_id FROM Questions WHERE clause_version_id = 15 AND question_code = '[PATENT COUNCIL RECOMMENDED]'); -- [PATENT COUNCIL RECOMMENDED] '52.227-3 ALTERNATE II -- PATENT INDEMNITY'

DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = '252.227-7002 -- READJUSTMENT OF PAYMENTS' INNER JOIN Questions Q  ON Q.question_code = '[PATENT COUNCIL RECOMMENDED]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '227.7009-2(b)';

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT Question_Choce_Id, prescription_id FROM Question_Choices, Prescriptions
  WHERE prescription_name IN ('229.402-70(k)') 
  AND choice_text = '252.227-7002 -- READJUSTMENT OF PAYMENTS' AND  question_id IN (SELECT question_id FROM Questions WHERE clause_version_id = 15 AND question_code = '[PATENT COUNCIL RECOMMENDED]'); -- [PATENT COUNCIL RECOMMENDED] '252.227-7002 -- READJUSTMENT OF PAYMENTS'

DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = '252.227-7003 -- TERMINATION' INNER JOIN Questions Q  ON Q.question_code = '[PATENT COUNCIL RECOMMENDED]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '227.7009-2';

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT Question_Choce_Id, prescription_id FROM Question_Choices, Prescriptions
  WHERE prescription_name IN ('229.402-70(l)') 
  AND choice_text = '252.227-7003 -- TERMINATION' AND  question_id IN (SELECT question_id FROM Questions WHERE clause_version_id = 15 AND question_code = '[PATENT COUNCIL RECOMMENDED]'); -- [PATENT COUNCIL RECOMMENDED] '252.227-7003 -- TERMINATION'


DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'CERTIFIED COST AND PRICING DATA ARE REQUIRED' INNER JOIN Questions Q  ON Q.question_code = '[PRICING REQUIREMENTS]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '215.408';

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT Question_Choce_Id, prescription_id FROM Question_Choices, Prescriptions
  WHERE prescription_name IN ('215.408(3)(i)','215.408(3)(ii)','215.408(4)(ii)') 
  AND choice_text = 'CONTRACT REQUIRES OTHER THAN CERTIFIED COST AND PRICING DATA' AND  question_id IN (SELECT question_id FROM Questions WHERE clause_version_id = 15 AND question_code = '[PRICING REQUIREMENTS]'); -- [PRICING REQUIREMENTS] 'CONTRACT REQUIRES OTHER THAN CERTIFIED COST AND PRICING DATA'


DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'FORMAT IN TABLE 15.2 IS USED' INNER JOIN Questions Q  ON Q.question_code = '[REGULATION PRICING]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '30.201-4(b)(1)';

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT Question_Choce_Id, prescription_id FROM Question_Choices, Prescriptions
  WHERE prescription_name IN ('15.408(m)','215.408(4)(ii)') 
  AND choice_text = 'FORMAT IN TABLE 15.2 IS USED' AND  question_id IN (SELECT question_id FROM Questions WHERE clause_version_id = 15 AND question_code = '[REGULATION PRICING]'); -- [REGULATION PRICING] 'FORMAT IN TABLE 15.2 IS USED'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT Question_Choce_Id, prescription_id FROM Question_Choices, Prescriptions
  WHERE prescription_name IN ('15.408(j)') 
  AND choice_text = 'PRICING IS CONSISTENT WITH FAR PART 31, "CONTRACT COST PRINCIPLES AND PROCEDURES"' AND  question_id IN (SELECT question_id FROM Questions WHERE clause_version_id = 15 AND question_code = '[REGULATION PRICING]'); -- [REGULATION PRICING] 'PRICING IS CONSISTENT WITH FAR PART 31, "CONTRACT COST PRINCIPLES AND PROCEDURES"'


DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'SUBMISSION VIA ELECTRONIC MEDIA IS REQUIRED (BASIC AWARD AND FUTURE MODIFICATIONS)' INNER JOIN Questions Q  ON Q.question_code = '[PROPOSAL REQUIREMENTS]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '15.408(l)';

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT Question_Choce_Id, prescription_id FROM Question_Choices, Prescriptions
  WHERE prescription_name IN ('15.408(i)') 
  AND choice_text = 'SUBMISSION VIA ELECTRONIC MEDIA IS REQUIRED (BASIC AWARD AND FUTURE MODIFICATIONS)' AND  question_id IN (SELECT question_id FROM Questions WHERE clause_version_id = 15 AND question_code = '[PROPOSAL REQUIREMENTS]'); -- [PROPOSAL REQUIREMENTS] 'SUBMISSION VIA ELECTRONIC MEDIA IS REQUIRED (BASIC AWARD AND FUTURE MODIFICATIONS)'


DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'AWARD WITHOUT DISCUSSIONS' INNER JOIN Questions Q  ON Q.question_code = '[AWARD SCENARIOS]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '15.209(a)(1)';


DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'YES' INNER JOIN Questions Q  ON Q.question_code = '[EXCESS PASS-THROUGH CHARGES]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '15.408(n)(2)(iii)';

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT Question_Choce_Id, prescription_id FROM Question_Choices, Prescriptions
  WHERE prescription_name IN ('15.408(n)(2)(iii)') 
  AND choice_text = 'NO' AND  question_id IN (SELECT question_id FROM Questions WHERE clause_version_id = 15 AND question_code = '[EXCESS PASS-THROUGH CHARGES]'); -- [EXCESS PASS-THROUGH CHARGES] 'NO'


DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'YES' INNER JOIN Questions Q  ON Q.question_code = '[INAPPROPRIATE CONTINGENCY]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '29.401-3';

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT Question_Choce_Id, prescription_id FROM Question_Choices, Prescriptions
  WHERE prescription_name IN ('29.401-3(b)') 
  AND choice_text = 'YES' AND  question_id IN (SELECT question_id FROM Questions WHERE clause_version_id = 15 AND question_code = '[INAPPROPRIATE CONTINGENCY]'); -- [INAPPROPRIATE CONTINGENCY] 'YES'


DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'SET-ASIDE' INNER JOIN Questions Q  ON Q.question_code = '[COMPETITION TYPE]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '13.302-5(d)(3)(i)';

DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'SET-ASIDE' INNER JOIN Questions Q  ON Q.question_code = '[COMPETITION TYPE]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '4.607(b)';

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT Question_Choce_Id, prescription_id FROM Question_Choices, Prescriptions
  WHERE prescription_name IN ('19.1507(a)','19.507(b)','4.607(a)') 
  AND choice_text = 'SET-ASIDE' AND  question_id IN (SELECT question_id FROM Questions WHERE clause_version_id = 15 AND question_code = '[COMPETITION TYPE]'); -- [COMPETITION TYPE] 'SET-ASIDE'

UPDATE Questions
SET question_type = 'M' -- 1
WHERE question_code = '[NEGOTIATION (FAR PART 15)]' AND clause_version_id = 15; -- [NEGOTIATION (FAR PART 15)]
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT Question_Choce_Id, prescription_id FROM Question_Choices, Prescriptions
  WHERE prescription_name IN ('215.408(3)(i)','215.408(3)(ii)') 
  AND choice_text = 'SOLE SOURCE' AND  question_id IN (SELECT question_id FROM Questions WHERE clause_version_id = 15 AND question_code = '[NEGOTIATION (FAR PART 15)]'); -- [NEGOTIATION (FAR PART 15)] 'SOLE SOURCE'


DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'SOLE SOURCE 8(a)' INNER JOIN Questions Q  ON Q.question_code = '[SOLE SOURCE]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '19.309(a)';

DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'SOLE SOURCE 8(a)' INNER JOIN Questions Q  ON Q.question_code = '[SOLE SOURCE]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '19.1507(b)';

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT Question_Choce_Id, prescription_id FROM Question_Choices, Prescriptions
  WHERE prescription_name IN ('19.1309(a)','19.507(b)') 
  AND choice_text = 'SOLE SOURCE 8(a)' AND  question_id IN (SELECT question_id FROM Questions WHERE clause_version_id = 15 AND question_code = '[SOLE SOURCE]'); -- [SOLE SOURCE] 'SOLE SOURCE 8(a)'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT Question_Choce_Id, prescription_id FROM Question_Choices, Prescriptions
  WHERE prescription_name IN ('14.201-6(p)(1)','14.201-6(p)(2)') 
  AND choice_text = 'INVITATION FOR BIDS' AND  question_id IN (SELECT question_id FROM Questions WHERE clause_version_id = 15 AND question_code = '[SOLICITATION TYPE]'); -- [SOLICITATION TYPE] 'INVITATION FOR BIDS'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT Question_Choce_Id, prescription_id FROM Question_Choices, Prescriptions
  WHERE prescription_name IN ('217.7406(b)') 
  AND choice_text = 'BASIC ORDERING AGREEMENT' AND  question_id IN (SELECT question_id FROM Questions WHERE clause_version_id = 15 AND question_code = '[AGREEMENT (INCLUDING BASIC AND LOAN)]'); -- [AGREEMENT (INCLUDING BASIC AND LOAN)] 'BASIC ORDERING AGREEMENT'


DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'WOMEN-OWNED SMALL BUSINESS (WOSB)' INNER JOIN Questions Q  ON Q.question_code = '[SMALL BUSINESS/ SOCIOECONOMIC SOURCE]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '19.1507(b)';

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT Question_Choce_Id, prescription_id FROM Question_Choices, Prescriptions
  WHERE prescription_name IN ('19.507(b)') 
  AND choice_text = 'WOMEN-OWNED SMALL BUSINESS (WOSB)' AND  question_id IN (SELECT question_id FROM Questions WHERE clause_version_id = 15 AND question_code = '[SMALL BUSINESS/ SOCIOECONOMIC SOURCE]'); -- [SMALL BUSINESS/ SOCIOECONOMIC SOURCE] 'WOMEN-OWNED SMALL BUSINESS (WOSB)'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT Question_Choce_Id, prescription_id FROM Question_Choices, Prescriptions
  WHERE prescription_name IN ('232.502-4(b)','32.502-4(d)') 
  AND choice_text = 'SMALL BUSINESS' AND  question_id IN (SELECT question_id FROM Questions WHERE clause_version_id = 15 AND question_code = '[SMALL BUSINESS/ SOCIOECONOMIC SOURCE]'); -- [SMALL BUSINESS/ SOCIOECONOMIC SOURCE] 'SMALL BUSINESS'


DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'FEDERAL GOVERNMENT' INNER JOIN Questions Q  ON Q.question_code = '[GOVERNMENT SOURCE]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '32.611(a)';

DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'FEDERAL GOVERNMENT' INNER JOIN Questions Q  ON Q.question_code = '[GOVERNMENT SOURCE]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '32.611(b)';

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT Question_Choce_Id, prescription_id FROM Question_Choices, Prescriptions
  WHERE prescription_name IN ('32.611(a)&(b)') 
  AND choice_text = 'FEDERAL GOVERNMENT' AND  question_id IN (SELECT question_id FROM Questions WHERE clause_version_id = 15 AND question_code = '[GOVERNMENT SOURCE]'); -- [GOVERNMENT SOURCE] 'FEDERAL GOVERNMENT'

DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'LOCAL GOVERNMENT' INNER JOIN Questions Q  ON Q.question_code = '[GOVERNMENT SOURCE]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '32.611(a)';

DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'LOCAL GOVERNMENT' INNER JOIN Questions Q  ON Q.question_code = '[GOVERNMENT SOURCE]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '32.611(b)';

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT Question_Choce_Id, prescription_id FROM Question_Choices, Prescriptions
  WHERE prescription_name IN ('32.611(a)&(b)') 
  AND choice_text = 'LOCAL GOVERNMENT' AND  question_id IN (SELECT question_id FROM Questions WHERE clause_version_id = 15 AND question_code = '[GOVERNMENT SOURCE]'); -- [GOVERNMENT SOURCE] 'LOCAL GOVERNMENT'

DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'STATE GOVERNMENT' INNER JOIN Questions Q  ON Q.question_code = '[GOVERNMENT SOURCE]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '32.611(a)';

DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'STATE GOVERNMENT' INNER JOIN Questions Q  ON Q.question_code = '[GOVERNMENT SOURCE]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '32.611(b)';

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT Question_Choce_Id, prescription_id FROM Question_Choices, Prescriptions
  WHERE prescription_name IN ('32.611(a)&(b)') 
  AND choice_text = 'STATE GOVERNMENT' AND  question_id IN (SELECT question_id FROM Questions WHERE clause_version_id = 15 AND question_code = '[GOVERNMENT SOURCE]'); -- [GOVERNMENT SOURCE] 'STATE GOVERNMENT'

DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'FEDERAL PRISON INDUSTRIES (FPI)' INNER JOIN Questions Q  ON Q.question_code = '[GOVERNMENT SOURCE]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '22.202';


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT Question_Choce_Id, prescription_id FROM Question_Choices, Prescriptions
  WHERE prescription_name IN ('15.408(m)') 
  AND choice_text = 'CANADIAN COMMERCIAL CORPORATION' AND  question_id IN (SELECT question_id FROM Questions WHERE clause_version_id = 15 AND question_code = '[FOREIGN SOURCE]'); -- [FOREIGN SOURCE] 'CANADIAN COMMERCIAL CORPORATION'

DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'FOREIGN GOVERNMENT' INNER JOIN Questions Q  ON Q.question_code = '[FOREIGN SOURCE]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '32.611(a)';

DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'FOREIGN GOVERNMENT' INNER JOIN Questions Q  ON Q.question_code = '[FOREIGN SOURCE]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '32.611(b)';

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT Question_Choce_Id, prescription_id FROM Question_Choices, Prescriptions
  WHERE prescription_name IN ('32.611(a)&(b)') 
  AND choice_text = 'FOREIGN GOVERNMENT' AND  question_id IN (SELECT question_id FROM Questions WHERE clause_version_id = 15 AND question_code = '[FOREIGN SOURCE]'); -- [FOREIGN SOURCE] 'FOREIGN GOVERNMENT'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT Question_Choce_Id, prescription_id FROM Question_Choices, Prescriptions
  WHERE prescription_name IN ('16.307(a)(5)','32.611(a)&(b)') 
  AND choice_text = 'NON-PROFIT ORGANIZATION' AND  question_id IN (SELECT question_id FROM Questions WHERE clause_version_id = 15 AND question_code = '[MISCELLANEOUS SOURCE]'); -- [MISCELLANEOUS SOURCE] 'NON-PROFIT ORGANIZATION'


DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'AMERICAN FLAG' INNER JOIN Questions Q  ON Q.question_code = '[SUPPLIES]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '2014-O0010';

DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'AMERICAN FLAG' INNER JOIN Questions Q  ON Q.question_code = '[SUPPLIES]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '2015-O0007';

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT Question_Choce_Id, prescription_id FROM Question_Choices, Prescriptions
  WHERE prescription_name IN ('225.7002-3(c)') 
  AND choice_text = 'AMERICAN FLAG' AND  question_id IN (SELECT question_id FROM Questions WHERE clause_version_id = 15 AND question_code = '[SUPPLIES]'); -- [SUPPLIES] 'AMERICAN FLAG'

DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = '(SUB) SYSTEMS AND (SUB) ASSEMBLIES INTEGRAL TO A SYSTEM' INNER JOIN Questions Q  ON Q.question_code = '[SUPPLIES]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '246.371(a)';


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT Question_Choce_Id, prescription_id FROM Question_Choices, Prescriptions
  WHERE prescription_name IN ('11.404(a)(2)','4.404(a)','44.204(a)(1)','44.204(a)(3)') 
  AND choice_text = 'ARCHITECT-ENGINEERING SERVICES' AND  question_id IN (SELECT question_id FROM Questions WHERE clause_version_id = 15 AND question_code = '[SERVICES]'); -- [SERVICES] 'ARCHITECT-ENGINEERING SERVICES'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT Question_Choce_Id, prescription_id FROM Question_Choices, Prescriptions
  WHERE prescription_name IN ('222.7102') 
  AND choice_text = 'ACTIVITIES RELATED TO CLOSURE OF A MILITARY INSTALLATION' AND  question_id IN (SELECT question_id FROM Questions WHERE clause_version_id = 15 AND question_code = '[SERVICES]'); -- [SERVICES] 'ACTIVITIES RELATED TO CLOSURE OF A MILITARY INSTALLATION'

DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'CONSTRUCTION' INNER JOIN Questions Q  ON Q.question_code = '[SERVICES]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '11.503';

DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'CONSTRUCTION' INNER JOIN Questions Q  ON Q.question_code = '[SERVICES]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '22.505-(b)(1)';

DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'CONSTRUCTION' INNER JOIN Questions Q  ON Q.question_code = '[SERVICES]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '22.505-(b)(2)';

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT Question_Choce_Id, prescription_id FROM Question_Choices, Prescriptions
  WHERE prescription_name IN ('11.503(b)','211.503(b)','22.505(a)(1)','22.505(a)(2)','22.505(b)(1)','22.505(b)(2)','225.7204(a)','225.7204(b)','247.574(b)(1)','247.574(b)(2)') 
  AND choice_text = 'CONSTRUCTION' AND  question_id IN (SELECT question_id FROM Questions WHERE clause_version_id = 15 AND question_code = '[SERVICES]'); -- [SERVICES] 'CONSTRUCTION'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT Question_Choce_Id, prescription_id FROM Question_Choices, Prescriptions
  WHERE prescription_name IN ('239.7411(c)') 
  AND choice_text = 'COMMUNICATION SERVICES (AND FACILITIES)' AND  question_id IN (SELECT question_id FROM Questions WHERE clause_version_id = 15 AND question_code = '[SERVICES]'); -- [SERVICES] 'COMMUNICATION SERVICES (AND FACILITIES)'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT Question_Choce_Id, prescription_id FROM Question_Choices, Prescriptions
  WHERE prescription_name IN ('237.7101(e)(2)') 
  AND choice_text = 'LAUNDRY AND DRY CLEANING SERVICES' AND  question_id IN (SELECT question_id FROM Questions WHERE clause_version_id = 15 AND question_code = '[SERVICES]'); -- [SERVICES] 'LAUNDRY AND DRY CLEANING SERVICES'

DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'PAID ADVERTISEMENTS' INNER JOIN Questions Q  ON Q.question_code = '[SERVICES]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '32.611(a)';

DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'PAID ADVERTISEMENTS' INNER JOIN Questions Q  ON Q.question_code = '[SERVICES]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '32.611(b)';

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT Question_Choce_Id, prescription_id FROM Question_Choices, Prescriptions
  WHERE prescription_name IN ('32.611(a)&(b)') 
  AND choice_text = 'PAID ADVERTISEMENTS' AND  question_id IN (SELECT question_id FROM Questions WHERE clause_version_id = 15 AND question_code = '[SERVICES]'); -- [SERVICES] 'PAID ADVERTISEMENTS'

DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'INFORMATION TECHNOLOGY SERVICES' INNER JOIN Questions Q  ON Q.question_code = '[SERVICES]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '239.7603(a)';

DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'INFORMATION TECHNOLOGY SERVICES' INNER JOIN Questions Q  ON Q.question_code = '[SERVICES]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '239.7603(b)';

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT Question_Choce_Id, prescription_id FROM Question_Choices, Prescriptions
  WHERE prescription_name IN ('239.7604(a)','239.7604(b)') 
  AND choice_text = 'INFORMATION TECHNOLOGY SERVICES' AND  question_id IN (SELECT question_id FROM Questions WHERE clause_version_id = 15 AND question_code = '[SERVICES]'); -- [SERVICES] 'INFORMATION TECHNOLOGY SERVICES'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT Question_Choce_Id, prescription_id FROM Question_Choices, Prescriptions
  WHERE prescription_name IN ('222.7302','23.705(a)') 
  AND choice_text = 'SUPPORT SERVICES' AND  question_id IN (SELECT question_id FROM Questions WHERE clause_version_id = 15 AND question_code = '[SERVICES]'); -- [SERVICES] 'SUPPORT SERVICES'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT Question_Choce_Id, prescription_id FROM Question_Choices, Prescriptions
  WHERE prescription_name IN ('239.7306(b)','39.106') 
  AND choice_text = 'INFORMATION TECHNOLOGY' AND  question_id IN (SELECT question_id FROM Questions WHERE clause_version_id = 15 AND question_code = '[COMPUTERS/INFORMATION TECHNOLOGY]'); -- [COMPUTERS/INFORMATION TECHNOLOGY] 'INFORMATION TECHNOLOGY'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT Question_Choce_Id, prescription_id FROM Question_Choices, Prescriptions
  WHERE prescription_name IN ('25.1101(1)(i)','25.1101(6)(i)','25.1101(6)(ii)') 
  AND choice_text = 'END ITEM LISTED IN DFARS SUBPART 225.401-70 (SUBJECT TO TRADE AGREEMENTS)' AND  question_id IN (SELECT question_id FROM Questions WHERE clause_version_id = 15 AND question_code = '[MISCELLANEOUS END ITEMS]'); -- [MISCELLANEOUS END ITEMS] 'END ITEM LISTED IN DFARS SUBPART 225.401-70 (SUBJECT TO TRADE AGREEMENTS)'


DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'ARCHITECT-ENGINEERING' INNER JOIN Questions Q  ON Q.question_code = '[ARCHITECT-ENGINEERING SERVICES]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '4.404(a)';

DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'ARCHITECT-ENGINEERING' INNER JOIN Questions Q  ON Q.question_code = '[ARCHITECT-ENGINEERING SERVICES]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '44.204(a)(1)';

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT Question_Choce_Id, prescription_id FROM Question_Choices, Prescriptions
  WHERE prescription_name IN ('43.205(a)(3)','43.205(a)(4)','44.204(c)','48.201(e)(1)','48.201(f)','49.502(a)(2)','49.502(b)(1)(i)') 
  AND choice_text = 'ARCHITECT-ENGINEERING' AND  question_id IN (SELECT question_id FROM Questions WHERE clause_version_id = 15 AND question_code = '[ARCHITECT-ENGINEERING SERVICES]'); -- [ARCHITECT-ENGINEERING SERVICES] 'ARCHITECT-ENGINEERING'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT Question_Choce_Id, prescription_id FROM Question_Choices, Prescriptions
  WHERE prescription_name IN ('29.401-2') 
  AND choice_text = 'CONVERSION, ALTERATION, OR REPAIR OF SHIPS' AND  question_id IN (SELECT question_id FROM Questions WHERE clause_version_id = 15 AND question_code = '[MAINTENANCE, REPAIR, INSTALLATION AND OVERHAUL SERVICES]'); -- [MAINTENANCE, REPAIR, INSTALLATION AND OVERHAUL SERVICES] 'CONVERSION, ALTERATION, OR REPAIR OF SHIPS'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT Question_Choce_Id, prescription_id FROM Question_Choices, Prescriptions
  WHERE prescription_name IN ('49.502(a)(2)','49.502(b)(1)(i)','49.502(b)(1)(ii)','49.502(b)(1)(iii)') 
  AND choice_text = 'RESEARCH AND DEVELOPMENT' AND  question_id IN (SELECT question_id FROM Questions WHERE clause_version_id = 15 AND question_code = '[RESEARCH, DEVELOPMENT AND EXPERIMENTAL SERVICES]'); -- [RESEARCH, DEVELOPMENT AND EXPERIMENTAL SERVICES] 'RESEARCH AND DEVELOPMENT'


DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'SUPPORT SERVICES' INNER JOIN Questions Q  ON Q.question_code = '[SUPPORT SERVICES]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '222.7302';

DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'SUPPORT SERVICES' INNER JOIN Questions Q  ON Q.question_code = '[SUPPORT SERVICES]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '23.705(a)';

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT Question_Choce_Id, prescription_id FROM Question_Choices, Prescriptions
  WHERE prescription_name IN ('204.7304(b)') 
  AND choice_text = 'SECURITY OF INFORMATION TECHNOLOGY' AND  question_id IN (SELECT question_id FROM Questions WHERE clause_version_id = 15 AND question_code = '[SUPPORT SERVICES]'); -- [SUPPORT SERVICES] 'SECURITY OF INFORMATION TECHNOLOGY'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT Question_Choce_Id, prescription_id FROM Question_Choices, Prescriptions
  WHERE prescription_name IN ('26.404') 
  AND choice_text = 'FOOD SERVICE' AND  question_id IN (SELECT question_id FROM Questions WHERE clause_version_id = 15 AND question_code = '[SUPPORT SERVICES]'); -- [SUPPORT SERVICES] 'FOOD SERVICE'


DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'COST NO FEE' INNER JOIN Questions Q  ON Q.question_code = '[COST REIMBURSEMENT]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '32.611(a)';

DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'COST NO FEE' INNER JOIN Questions Q  ON Q.question_code = '[COST REIMBURSEMENT]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '32.611(b)';

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT Question_Choce_Id, prescription_id FROM Question_Choices, Prescriptions
  WHERE prescription_name IN ('32.611(a)&(b)') 
  AND choice_text = 'COST NO FEE' AND  question_id IN (SELECT question_id FROM Questions WHERE clause_version_id = 15 AND question_code = '[COST REIMBURSEMENT]'); -- [COST REIMBURSEMENT] 'COST NO FEE'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT Question_Choce_Id, prescription_id FROM Question_Choices, Prescriptions
  WHERE prescription_name IN ('211.503(b)') 
  AND choice_text = 'COST PLUS FIXED FEE' AND  question_id IN (SELECT question_id FROM Questions WHERE clause_version_id = 15 AND question_code = '[COST REIMBURSEMENT]'); -- [COST REIMBURSEMENT] 'COST PLUS FIXED FEE'

DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'COST PLUS INCENTIVE FEE (COST BASED)' INNER JOIN Questions Q  ON Q.question_code = '[COST REIMBURSEMENT]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '16.307(d)';

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT Question_Choce_Id, prescription_id FROM Question_Choices, Prescriptions
  WHERE prescription_name IN ('16.30(d)') 
  AND choice_text = 'COST PLUS INCENTIVE FEE (COST BASED)' AND  question_id IN (SELECT question_id FROM Questions WHERE clause_version_id = 15 AND question_code = '[COST REIMBURSEMENT]'); -- [COST REIMBURSEMENT] 'COST PLUS INCENTIVE FEE (COST BASED)'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT Question_Choce_Id, prescription_id FROM Question_Choices, Prescriptions
  WHERE prescription_name IN ('246.710(1)(i)','246.710(1)(iii)') 
  AND choice_text = 'FIRM FIXED PRICE' AND  question_id IN (SELECT question_id FROM Questions WHERE clause_version_id = 15 AND question_code = '[FIXED PRICE]'); -- [FIXED PRICE] 'FIRM FIXED PRICE'

DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'FIXED PRICE INCENTIVE (COST BASED)' INNER JOIN Questions Q  ON Q.question_code = '[FIXED PRICE]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '2015-00017';

DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'FIXED PRICE INCENTIVE (COST BASED)' INNER JOIN Questions Q  ON Q.question_code = '[FIXED PRICE]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '216.406';

DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'FIXED PRICE INCENTIVE (COST BASED)' INNER JOIN Questions Q  ON Q.question_code = '[FIXED PRICE]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '216.406(e)(2)';

DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'FIXED PRICE INCENTIVE (SUCCESSIVE TARGETS)' INNER JOIN Questions Q  ON Q.question_code = '[FIXED PRICE]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '2015-00017';

DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'FIXED PRICE WITH ECONOMIC PRICE ADJUSTMENT -- COST INDICES' INNER JOIN Questions Q  ON Q.question_code = '[FIXED PRICE]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '3.301-1';

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT Question_Choce_Id, prescription_id FROM Question_Choices, Prescriptions
  WHERE prescription_name IN ('3.103-1') 
  AND choice_text = 'FIXED PRICE WITH ECONOMIC PRICE ADJUSTMENT -- COST INDICES' AND  question_id IN (SELECT question_id FROM Questions WHERE clause_version_id = 15 AND question_code = '[FIXED PRICE]'); -- [FIXED PRICE] 'FIXED PRICE WITH ECONOMIC PRICE ADJUSTMENT -- COST INDICES'

DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'FIXED PRICE WITH ECONOMIC PRICE ADJUSTMENT -- ESTABLISHED PRICES' INNER JOIN Questions Q  ON Q.question_code = '[FIXED PRICE]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 15
INNER JOIN Prescriptions P  ON P.prescription_id = Question_Choice_Prescriptions.prescription_id AND P.prescription_name = '3.301-1';

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT Question_Choce_Id, prescription_id FROM Question_Choices, Prescriptions
  WHERE prescription_name IN ('3.103-1') 
  AND choice_text = 'FIXED PRICE WITH ECONOMIC PRICE ADJUSTMENT -- ESTABLISHED PRICES' AND  question_id IN (SELECT question_id FROM Questions WHERE clause_version_id = 15 AND question_code = '[FIXED PRICE]'); -- [FIXED PRICE] 'FIXED PRICE WITH ECONOMIC PRICE ADJUSTMENT -- ESTABLISHED PRICES'

/*
-- [DPAP APPROVAL FOR USE OF 252.229-7015 ]
DELETE FROM Question_Choice_Prescriptions
WHERE Question_Choce_Id in
(SELECT Question_Choce_Id FROM Question_Choices WHERE question_id = 38481);
DELETE FROM Question_Choices WHERE question_id = 38481;
DELETE FROM Question_Conditions WHERE question_id = 38481;
DELETE FROM Questions WHERE question_id = 38481;
*/
/*
<h2>52.212-5</h2>
(K) Clause name not found: {52.212-5 ALTERNATE III}
<pre>(K) 52.212-5 ALTERNATE III IS: INCLUDED</pre>
*//*
<h2>52.214-12</h2>
(C) Clause name not found: {52.214-12 ALTERNATE I}
<pre>(C) 52.214-12 ALTERNATE I IS: INCLUDED</pre>
*//*
<h2>52.215-21</h2>
(H) Clause name not found: {52.215-21 ALTERNATE VI}
<pre>(H) 52.215-21 ALTERNATE VI IS: INCLUDED</pre>
*//*
<h2>52.219-9 (DEVIATION 2013-00014)</h2>
(D) No " IS:" found
<pre>(D) 52.219-9 ALTERNATE I (DEVIATION 2013-00014)</pre><br />
(E) No " IS:" found
<pre>(E) 52.219-9 ALTERNATE II (DEVIATION 2013-00014)</pre><br />
(F) No " IS:" found
<pre>(F) 52.219-9 ALTERNATE III (DEVIATION 2013-00014)</pre>
*//*
<h2>52.222-29</h2>
(E) Value not found: "YES" in [CONTRACTOR PERFORM ON BEHALF OF FOREIGN COUNTRY] choices. Available values: <ul><li>"NATO"</li><li>"FOREIGN COUNTRY"</li><li>"NO"</li><li>"CONTRACTOR HAS NOT BEEN SELECTED"</li></ul>
<pre>(E) CONTRACTOR PERFORM ON BEHALF OF FOREIGN COUNTRY IS: YES</pre>
*//*
<h2>52.230-1</h2>
(D) Clause name not found: {52.230-1 ALTERNATE I}
<pre>(D) 52.230-1 ALTERNATE I IS: INCLUDED</pre>
*//*
<h2>52.237-6</h2>
(G) Clause name not found: {52.237-6 ALTERNATE I}
<pre>(G) 52.237-6 ALTERNATE I IS: INCLUDED</pre>
*//*
<h2>52.247-5</h2>
(C) [TRANSPORTATION REQUIREMENTS] Question Name not found
<pre>(C) TRANSPORTATION REQUIREMENTS IS: CONTRACTOR RESPONSIBLE FOR FAMILIARIZATION WITH CONDITIONS UNDER WHICH AND WHERE THE SERVICES ARE TO BE PERFORMED CONTRACTOR RESPONSIBLE FOR FAMILIARIZATION WITH CONDITIONS UNDER WHICH AND WHERE THE SERVICES ARE TO BE PERFORMED</pre>
*//*
<h2>52.215-20 Alternate I</h2>
(B) Clause name not found: {252.215-7008 ALTERNATE I}
<pre>(B) 252.215-7008 ALTERNATE I IS: INCLUDED</pre>
*//*
<h2>52.215-20 Alternate II</h2>
(B) Clause name not found: {252.215-7008 ALTERNATE II}
<pre>(B) 252.215-7008 ALTERNATE II IS: INCLUDED</pre>
*//*
<h2>52.215-20 Alternate III</h2>
(B) Clause name not found: {252.215-7008 ALTERNATE III}
<pre>(B) 252.215-7008 ALTERNATE III IS: INCLUDED</pre>
*//*
<h2>52.215-20 Alternate IV</h2>
(B) Clause name not found: {252.215-7008 ALTERNATE IV}
<pre>(B) 252.215-7008 ALTERNATE IV IS: INCLUDED</pre>
*/
-- ProduceClause.resolveIssues() Summary: Total(1225); Merged(10); Corrected(343); Error(14)
/* ===============================================
 * Clause
   =============================================== */

DELETE FROM Document_Fill_Ins WHERE Clause_Fill_In_Id
IN (SELECT Clause_Fill_In_Id FROM Clause_Fill_Ins WHERE clause_id IN
 (SELECT Clause_id FROM Clauses WHERE Clause_Name = '252.229-7014' AND Clause_Version_Id = 15));
DELETE FROM Clause_Fill_Ins WHERE clause_id IN
 (SELECT Clause_id FROM Clauses WHERE Clause_Name = '252.229-7014' AND Clause_Version_Id = 15);
DELETE FROM Document_Clauses WHERE clause_id IN
 (SELECT Clause_id FROM Clauses WHERE Clause_Name = '252.229-7014' AND Clause_Version_Id = 15);
DELETE FROM Clauses WHERE Clause_Name = '252.229-7014' AND Clause_Version_Id = 15;

DELETE FROM Document_Fill_Ins WHERE Clause_Fill_In_Id
IN (SELECT Clause_Fill_In_Id FROM Clause_Fill_Ins WHERE clause_id IN
 (SELECT Clause_id FROM Clauses WHERE Clause_Name = '252.229-7015' AND Clause_Version_Id = 15));
DELETE FROM Clause_Fill_Ins WHERE clause_id IN
 (SELECT Clause_id FROM Clauses WHERE Clause_Name = '252.229-7015' AND Clause_Version_Id = 15);
DELETE FROM Document_Clauses WHERE clause_id IN
 (SELECT Clause_id FROM Clauses WHERE Clause_Name = '252.229-7015' AND Clause_Version_Id = 15);
DELETE FROM Clauses WHERE Clause_Name = '252.229-7015' AND Clause_Version_Id = 15;


INSERT INTO Clauses (clause_version_id, clause_name, clause_section_id, inclusion_cd, regulation_id, clause_title, clause_url, effective_date, clause_conditions, clause_rule, additional_conditions, is_active, provision_or_clause, commercial_status, optional_status, optional_conditions, is_optional, is_editable, editable_remarks, is_basic_clause, is_dfars_activity, clause_data)
VALUES (15, '252.229-7014', 9, 'R', 2, 'Foreign Contracts in Afghanistan', 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1229_67014&rgn=div8', '2015-12-01', '(A) DOCUMENT TYPE IS: SOLICITATION (B) DOCUMENT TYPE IS: AWARD (C) FOREIGN COUNTRY IS: AFGHANISTAN (D) 252.229-7015 IS: NOT INCLUDED', '(A OR B) AND C AND D', NULL, 1, 'C', 'N/A', NULL, NULL, 0, 0, NULL, 0, 1
, '<p>(a) This acquisition is covered by the Security and Defense Cooperation Agreement (the Agreement) between the Islamic Republic of Afghanistan and the United States of America signed on September 30, 2014, and entered into force on January 1, 2015.</p><p>(b) The Agreement exempts the Department of Defense (DoD), and its contractors and subcontractors (other than those that are Afghan legal entities or residents), from paying any tax or similar charge assessed on activities associated with this contract within Afghanistan. The Agreement also exempts the acquisition, importation, exportation, reexportation, transportation, and use of supplies and services in Afghanistan, by or on behalf of DoD, from any taxes, customs, duties, fees, or similar charges in Afghanistan.</p><p>(c) The Contractor shall exclude any Afghan taxes, customs, duties, fees, or similar charges from the contract price, other than those charged to Afghan legal entities or residents.</p><p>(d) The Agreement does not exempt Afghan employees of DoD contractors and subcontractors from Afghan tax laws. To the extent required by Afghan law, the Contractor shall withhold tax from the wages of these employees and remit those payments to the appropriate Afghanistan taxing authority. These withholdings are an individual''s liability, not a tax against the Contractor.</p><p>(e) The Contractor shall include the substance of this clause, including this paragraph (e), in all subcontracts, including subcontracts for commercial items.</p>');

INSERT INTO Clauses (clause_version_id, clause_name, clause_section_id, inclusion_cd, regulation_id, clause_title, clause_url, effective_date, clause_conditions, clause_rule, additional_conditions, is_active, provision_or_clause, commercial_status, optional_status, optional_conditions, is_optional, is_editable, editable_remarks, is_basic_clause, is_dfars_activity, clause_data)
VALUES (15, '252.229-7015', 9, 'R', 2, 'Foreign Contracts in Afghanistan (North Atlantic Treaty Organization Status of Forces Agreement)', 'http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1229_67015&rgn=div8', '2015-12-01', '(A) DOCUMENT TYPE IS: SOLICITATION (B) DOCUMENT TYPE IS: AWARD (C) FOREIGN COUNTRY IS: AFGHANISTAN (D) CONTRACTOR PERFORM ON BEHALF OF FOREIGN COUNTRY IS: NATO (E) DPAP APPROVAL FOR USE OF 252.229-7015 IS: YES', '(A OR B) AND C AND D AND E', NULL, 1, 'C', 'N/A', NULL, NULL, 0, 0, NULL, 0, 1
, '<p>(a) This acquisition is covered by the Status of Forces Agreement (SOFA) entered into between the North Atlantic Treaty Organization (NATO) and the Islamic Republic of Afghanistan issued on September 30, 2014, and entered into force on January 1, 2015.</p><p>(b) The SOFA exempts NATO Forces and its contractors and subcontractors (other than those that are Afghan legal entities or residents) from paying any tax or similar charge assessed within Afghanistan. The SOFA also exempts the acquisition, importation, exportation, reexportation, transportation and use of supplies and services in Afghanistan from all Afghan taxes, customs, duties, fees, or similar charges.</p><p>(c) The Contractor shall exclude any Afghan taxes, customs, duties, fees or similar charges from the contract price, other than those that are Afghan legal entities or residents.</p><p>(d) Afghan citizens employed by NATO contractors and subcontractors are subject to Afghan tax laws. To the extent required by Afghan law, the Contractor shall withhold tax from the wages of these employees and remit those withholdings to the Afghanistan Revenue Department. These withholdings are an individual''s liability, not a tax against the Contractor.</p><p>(e) The Contractor shall include the substance of this clause, including this paragraph (e), in all subcontracts including subcontracts for commercial items.</p>');

UPDATE Clauses
SET clause_conditions = '(A) DOCUMENT TYPE IS: SOLICITATION (B) DOCUMENT TYPE IS: AWARD (C) CONTRACT TYPE IS: NOT COST PLUS FIXED FEE (D) LIQUIDATED DAMAGES IS: YES (E) SERVICES IS: CONSTRUCTION (F) PROCEDURES IS: NOT COMMERCIAL PROCEDURES (FAR PART 12)\n(D2) CONTRACT VALUE IS: GREATER THAN  700000\n(F2) PACE OF WORK IS: NO' -- '(A) DOCUMENT TYPE IS: SOLICITATION (B) DOCUMENT TYPE IS: AWARD (C) CONTRACT TYPE IS: NOT COST PLUS FIXED FEE (D) LIQUIDATED DAMAGES IS: YES (E) SERVICES IS: CONSTRUCTION (F) PROCEDURES IS: NOT COMMERCIAL PROCEDURES (FAR PART 12)'
, clause_rule = '((A OR B) AND C AND D AND E AND F) OR ((A OR B) AND E AND D2 AND (C OR F2))' -- '(A OR B) AND C AND D AND E AND F'
WHERE clause_version_id = 15 AND clause_name = '52.211-12';

UPDATE Clauses
SET clause_data = '<p> (a) Comptroller General Examination of Record. The Contractor shall comply with the provisions of this paragraph (a) if this contract was awarded using other than sealed bid, is in excess of the simplified acquisition threshold, and does not contain the clause at 52.215-2, Audit and Records-Negotiation. \n</p><p>(1) The Comptroller General of the United States, or an authorized representative of the Comptroller General, shall have access to and right to examine any of the Contractor&rsquo;s directly pertinent records involving transactions related to this contract. \n</p><p>(2) The Contractor shall make available at its offices at all reasonable times the records, materials, and other evidence for examination, audit, or reproduction, until 3 years after final payment under this contract or for any shorter period specified in FAR Subpart 4.7, Contractor Records Retention, of the other clauses of this contract. If this contract is completely or partially terminated, the records relating to the work terminated shall be made available for 3 years after any resulting final termination settlement. Records relating to appeals under the disputes clause or to litigation or the settlement of claims arising under or relating to this contract shall be made available until such appeals, litigation, or claims are finally resolved. \n</p><p>(3) As used in this clause, records include books, documents, accounting procedures and practices, and other data, regardless of type and regardless of form. This does not require the Contractor to create or maintain any record that the Contractor does not maintain in the ordinary course of business or pursuant to a provision of law. \n</p><p>(b) (1) Notwithstanding the requirements of any other clauses of this contract, the Contractor is not required to flow down any FAR clause, other than those in this paragraph (b) (1) in a subcontract for commercial items. Unless otherwise indicated below, the extent of the flow down shall be as required by the clause- \n</p><p>(i) 52.203-13, Contractor Code of Business Ethics and Conduct (Apr 2010) (41 U.S.C. 3509). \n</p><p>(ii) 52.219-8, Utilization of Small Business Concerns (Dec 2010) (15 U.S.C. 637(d)(2) and (3)), in all subcontracts that offer further subcontracting opportunities. If the subcontract (except subcontracts to small business concerns) exceeds $650,000 ($1.5 million for construction of any public facility), the subcontractor must include 52.219-8 in lower tier subcontracts that offer subcontracting opportunities. \n</p><p>(iii) 52.222-17, Nondisplacement of Qualified Workers (JAN 2013) (E.O. 13495). Flow down required in accordance with paragraph (l) of FAR clause 52.222-17. \n</p><p>(iv) 52.222-26, Equal Opportunity (Mar 2007) (E.O. 11246). \n</p><p>(v) 52.222-35, Equal Opportunity for Veterans (Sep 2010) (38 U.S.C. 4212). \n</p><p>(vi) 52.222-36, Affirmative Action for Workers with Disabilities (Oct 2010) (29 U.S.C. 793). \n</p><p>(vii) 52.222-40, Notification of Employee Rights Under the National Labor Relations Act (Dec 2010) (E.O. 13496). Flow down required in accordance with paragraph (f) of FAR clause 52.222-40. \n</p><p>(viii) 52.222-41, Service Contract Act of 1965 (Nov 2007) (41 U.S.C .Chapter 67). \n</p><p>(ix) 52.222-50, Combating Trafficking in Persons (Feb 2009) (22 U.S.C. 7104(g)). </p>'
--               '<dl>\n<dt><p>(a) Comptroller General Examination of Record. The Contractor shall comply with the provisions of this paragraph (a) if this contract was awarded using other than sealed bid, is in excess of the simplified acquisition threshold, and does not contain the clause at 52.215-2, Audit and Records-Negotiation.</p></dt>\n<dd>(1) The Comptroller General of the United States, or an authorized representative of the Comptroller General, shall have access to and right to examine any of the Contractor''s directly pertinent records involving transactions related to this contract.</p></dd>\n<dd><p>(2) The Contractor shall make available at its offices at all reasonable times the records, materials, and other evidence for examination, audit, or reproduction, until 3 years after final payment under this contract or for any shorter period specified in FAR Subpart 4.7, Contractor Records Retention, of the other clauses of this contract. If this contract is completely or partially terminated, the records relating to the work terminated shall be made available for 3 years after any resulting final termination settlement. Records relating to appeals under the disputes clause or to litigation or the settlement of claims arising under or relating to this contract shall be made available until such appeals, litigation, or claims are finally resolved.</p></dd>\n<dd><p>(3) As used in this clause, records include books, documents, accounting procedures and practices, and other data, regardless of type and regardless of form. This does not require the Contractor to create or maintain any record that the Contractor does not maintain in the ordinary course of business or pursuant to a provision of law.</p></dd>\n<dt><p>(b) (1) Notwithstanding the requirements of any other clauses of this contract, the Contractor is not required to flow down any FAR clause, other than those in this paragraph (b) (1) in a subcontract for commercial items. Unless otherwise indicated below, the extent of the flow down shall be as required by the clause-</p></dt>\n<dd><p>(i) 52.203-13, Contractor Code of Business Ethics and Conduct (Apr 2010) (41 U.S.C. 3509).</p></dd>\n<dd><p>(ii) 52.219-8, Utilization of Small Business Concerns (Dec 2010) (15 U.S.C. 637(d) (2) and (3)), in all subcontracts that offer further subcontracting opportunities. If the subcontract (except subcontracts to small business concerns) exceeds $650,000 ($1.5 million for construction of any public facility), the subcontractor must include 52.219-8 in lower tier subcontracts that offer subcontracting opportunities.</p></dd>\n<dd><p>(iii) 52.222-17, Nondisplacement of Qualified Workers (JAN 2013) (E.O. 13495) . Flow down required in accordance with paragraph (1) of FAR clause 52.222-17.</p></dd>\n<dd><p>(iv) 52.222-26, Equal Opportunity (Mar 2007) (E.O. 11246).</p></dd>\n<dd><p>(v) 52.222-35, Equal Opportunity for Veterans (Sep 2010) (38 U.S.C. 4212)</p></dd>\n<dd><p>(vi) 52.222-36, Affirmative Action for Workers with Disabilities (Oct 2010) (29 U.S.C. 793).</p></dd>\n<dd><p>(vii) 52.222-40, Notification of Employee Rights Under the National Labor Relations Act (Dec 2010) (E.O. 13496). Flow down required in accordance with paragraph (f) of FAR clause 52.222-40.</p></dd>\n<dd><p>(viii) 52.222-41, Service Contract Act of 1965 (Nov 2007) (41 U.S.C Chapter 67).</p></dd>\n<dd><p>(ix) 52.222-50, Combating Trafficking in Persons (Feb 2009) (22 U.S.C 7104(g)). Alternate I (Aug 2007) of 52.222-50 (22 U.S.C. 7104(g)).</p></dd>\n<dd><p>(x) 52.222-51, Exemption from Application of the Service Contract Act to Contracts for Maintenance, Calibration, or Repair of Certain Equipment-Requirements (Nov 2007) (41 U.S.C. Chapter 67).</p></dd>\n<dd><p>(xi) 52.222-53, Exemption from Application of the Service Contract Act to Contracts for Certain Services -Requirements (Feb 2009) (41 U.S.C. Chapter 67).</p></dd>\n<dd><p>(xii) 52.222-54, Employment Eligibility Verification (E.O. 12989) (JUL 2012).</p></dd>\n<dd><p>(xiii) 52.225-26, Contractors Performing Private Security Functions Outside the United States (Jul 2013) (Section 862, as amended, of the National Defense Authorization Act for Fiscal Year 2008; 10 U.S.C. 2302 Note).</p></dd>\n<dd><p>(xiv) 52.226-6, Promoting Excess Food Donation to Nonprofit Organizations (Mar 2009) 42 U.S.C. 1792). Flow down required in accordance with paragraph (e) of FAR clause 52.226-6.</p></dd>\n<dd><p>(xv) 52.247-64, Preference for Privately Owned U.S.-Flag Commercial Vessels  (Feb 2006) (46 U.S.C. Appx. 1241(b) and 10 U.S.C. 2631). Flow down required in accordance with paragraph (d) of FAR clause 52.247-64.</p></dd>\n<dt><p>(b) (2) While not required, the contractor may include in its subcontracts for commercial items a minimal number of additional clauses necessary to satisfy its contractual obligations.</p></dt>\n</dl>'
WHERE clause_version_id = 15 AND clause_name = '52.212-5 (DEVIATION 2013-00019)';

UPDATE Clauses
SET clause_conditions = '(A) DOCUMENT TYPE IS: SOLICITATION (B) PRICING REQUIREMENTS IS: CERTIFIED COST AND PRICING DATA ARE REQUIRED (C) PRICING REQUIREMENTS IS: CONTRACT REQUIRES OTHER THAN CERTIFIED COST AND PRICING DATA (D) PROCEDURES IS: NEGOTIATION (FAR PART 15) (E) PROCEDURES IS: NOT COMMERCIAL PROCEDURES (FAR PART 12) (F) 52.215-20 ALTERNATE I IS: INCLUDED (G) 52.215-20 ALTERNATE II IS: INCLUDED (H) 52.215-20 ALTERNATE III IS: INCLUDED (I) 52.215-20 ALTERNATE IV IS: INCLUDED\n(B2) 252.215-7008 IS: INCLUDED' -- '(A) DOCUMENT TYPE IS: SOLICITATION (B) PRICING REQUIREMENTS IS: CERTIFIED COST AND PRICING DATA ARE REQUIRED (C) PRICING REQUIREMENTS IS: CONTRACT REQUIRES OTHER THAN CERTIFIED COST AND PRICING DATA (D) PROCEDURES IS: NEGOTIATION (FAR PART 15) (E) PROCEDURES IS: NOT COMMERCIAL PROCEDURES (FAR PART 12) (F) 52.215-20 ALTERNATE I IS: INCLUDED (G) 52.215-20 ALTERNATE II IS: INCLUDED (H) 52.215-20 ALTERNATE III IS: INCLUDED (I) 52.215-20 ALTERNATE IV IS: INCLUDED'
, clause_rule = '((A AND (B OR C) AND D AND E) OR (F OR G OR H OR I)) OR ((A AND B2 AND C) OR (F OR G OR H OR I))' -- '(A AND (B OR C) AND D AND E) OR (F OR G OR H OR I)'
WHERE clause_version_id = 15 AND clause_name = '52.215-20';
INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT clause_id, prescription_id FROM Prescriptions, Clauses
WHERE prescription_name IN ('215.408(4)(ii)') 
AND clause_id IN (SELECT clause_id FROM Clauses WHERE clause_name = '52.215-20' AND clause_version_id = 15); 

UPDATE Clauses
SET clause_conditions = '(A) DOCUMENT TYPE IS: SOLICITATION (B) DOCUMENT TYPE IS: AWARD (C) PROCEDURES IS: COMPETITIVE (8a) (FAR PART 19.8) (D) PROCEDURES IS: NOT COMMERCIAL PROCEDURES (FAR PART 12) (E) 52.219-18 ALTERNATE I IS: INCLUDED (F) 52.219-18 ALTERNATE II IS: INCLUDED\n(C2) PROCEDURES IS: COMPETITIVE (8a) (FAR PART 19.8)\n(D) PROCEDURES IS: NOT COMMERCIAL PROCEDURES (FAR PART 12)\n(E) 52.219-18 Alternate I IS: INCLUDED\n(F) 52.219-18 Alternate II IS: INCLUDED' -- '(A) DOCUMENT TYPE IS: SOLICITATION (B) DOCUMENT TYPE IS: AWARD (C) PROCEDURES IS: COMPETITIVE (8a) (FAR PART 19.8) (D) PROCEDURES IS: NOT COMMERCIAL PROCEDURES (FAR PART 12) (E) 52.219-18 ALTERNATE I IS: INCLUDED (F) 52.219-18 ALTERNATE II IS: INCLUDED'
, clause_rule = '(((A OR B) AND C AND D) OR (E OR F)) OR (((A OR B) AND C2 AND D) OR (E OR F))' -- '((A OR B) AND C AND D) OR (E OR F)'
, additional_conditions = 'IF 252.219-7010 ADDED,\nINCLUDE 52.219-18' -- NULL
WHERE clause_version_id = 15 AND clause_name = '52.219-18';
INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT clause_id, prescription_id FROM Prescriptions, Clauses
WHERE prescription_name IN ('219.811-3(2)') 
AND clause_id IN (SELECT clause_id FROM Clauses WHERE clause_name = '52.219-18' AND clause_version_id = 15); 

UPDATE Clauses
SET clause_rule = 'A' -- 'AQ118'
WHERE clause_version_id = 15 AND clause_name = '252.219-7003 Alternate I (DEVIATION 2013-00014)';

UPDATE Clauses
SET clause_data = 'Alternate II (JUL 2015). As prescribed in 12.301(b)(4)(ii), substitute the following paragraphs (d)(1) and (e)(1) for paragraphs (d)(1) and (e)(1) of the basic clause as follows:<p>(d)(1) The Comptroller General of the United States, an appropriate Inspector General appointed under section 3 or 8G of the Inspector General Act of 1978 (5 U.S.C. App.), or an authorized representative of either of the foregoing officials shall have access to and right to-</p><p>(i) Examine any of the Contractor''s or any subcontractors'' records that pertain to, and involve transactions relating to, this contract; and</p><p>(ii) Interview any officer or employee regarding such transactions.</p><p>(e)(1) Notwithstanding the requirements of the clauses in paragraphs (a), (b), and (c), of this clause, the Contractor is not required to flow down any FAR clause in a subcontract for commercial items, other than-</p><p>(i) <i>Paragraph (d) of this clause.</i> This paragraph flows down to all subcontracts, except the authority of the Inspector General under paragraph (d)(1)(ii) does not flow down; and</p><p>(ii) <i>Those clauses listed in this paragraph (e)(1).</i> Unless otherwise indicated below, the extent of the flow down shall be as required by the clause-</p><p>(A) 52.203-13, Contractor Code of Business Ethics and Conduct (APR 2010) ((41 U.S.C. 3509)).</p><p>(B) 52.203-15, Whistleblower Protections Under the American Recovery and Reinvestment Act of 2009 (JUN 2010) (Section 1553 of Pub. L. 111-5).</p><p>(C) 52.219-8, Utilization of Small Business Concerns (OCT 2014) (15 U.S.C. 637(d)(2) and (3)), in all subcontracts that offer further subcontracting opportunities. If the subcontract (except subcontracts to small business concerns) exceeds $650,000 ($1.5 million for construction of any public facility), the subcontractor must include 52.219-8 in lower tier subcontracts that offer subcontracting opportunities.</p><p>(D) 52.222-21, Prohibition of Segregated Facilities (APR 2015).</p><p>(E) 52.222-26, Equal Opportunity (APR 2015) (E.O. 11246).</p><p>(F) 52.222-35, Equal Opportunity for Veterans (July 2014) (38 U.S.C. 4212).</p><p>(G) 52.222-36, Equal Opportunity for Workers with Disabilities (July 2014) (29 U.S.C. 793).</p><p>(H) 52.222-40, Notification of Employee Rights Under the National Labor Relations Act (DEC 2010) (E.O. 13496). Flow down required in accordance with paragraph (f) of FAR clause 52.222-40.</p><p>(I) 52.222-41, Service Contract Labor Standards (MAY 2014) (41 U.S.C. chapter 67).</p><p>(J) __ (<i>1</i>) 52.222-50, Combating Trafficking in Persons (Mar 2015) (22 U.S.C. chapter 78 and E.O. 13627).</p><p>__ (<i>2</i>) Alternate I (Mar 2015) of 52.222-50 (22 U.S.C. chapter 78 and E.O. 13627).</p><p>(K) 52.222-51, Exemption from Application of the Service Contract Labor Standards to Contracts for Maintenance, Calibration, or Repair of Certain Equipment-Requirements (MAY 2014) (41 U.S.C. chapter 67).</p><p>(L) 52.222-53, Exemption from Application of the Service Contract Labor Standards to Contracts for Certain Services-Requirements (MAY 2014) (41 U.S.C. chapter 67).</p><p>(M) 52.222-54, Employment Eligibility Verification (AUG 2013).</p><p>(N) 52.222-55, Minimum Wages Under Executive Order 13658 (Dec 2014) (E.O. 13658).</p><p>(O) 52.226-6, Promoting Excess Food Donation to Nonprofit Organizations. (MAY 2014) (42 U.S.C. 1792). Flow down required in accordance with paragraph (e) of FAR clause 52.226-6.</p><p>(P) 52.247-64, Preference for Privately Owned U.S.-Flag Commercial Vessels (FEB 2006) (46 U.S.C. Appx. 1241(b) and 10 U.S.C. 2631). Flow down required in accordance with paragraph (d) of FAR clause 52.247-64.</p>'
--               '<dl>\n<dt><p>(a) (1) The Comptroller General of the United States, an appropriate Inspector General appointed under section 3 or 8G of the Inspector General Act of 1978 (5 U.S.C. App.), or an authorized representative of either of the foregoing officials shall have access to and right to-</p></dt>\n<dd><p>(i) Examine any of the Contractor''s or any subcontractors'' records that pertain to, and involve transactions relating to, this contract; and</p></dd>\n<dd><p>(ii) Interview any officer or employee regarding such transactions.</p></dd>\n<dt><p>(b) (1) Notwithstanding the requirements of any other clause in this contract, the Contractor is not required to flow down any FAR clause in a subcontract for commercial items, other than-</p></dt>\n<dd><p>(i) Paragraph (a) of this clause. This paragraph flows down to all subcontracts, except the authority of the Inspector General under paragraph (a) (1) (ii) does not flow down; and</p></dd>\n<dd><p>(ii) Those clauses listed in this paragraph (b) (1). Unless otherwise indicated below, the extent of the flow down shall be as required by the clause-\n<dl>\n<dd><p>(A) 52.203-13, Contractor Code of Business Ethics and Conduct (Apr 2010) (Pub. L. 110-252, Title VI, Chapter 1 (41 U.S.C. 3509)).</p></dd>\n<dd><p>(B) 52.203-15, Whistleblower Protections Under the American Recovery and Reinvestment Act of 2009 (June 2010) (Section 1553 of Pub. L. 111-5).</p></dd>\n<dd><p>(C) 52.219-8, Utilization of Small Business Concerns (Dec 2010) (15 U .S.C . 637(d) (2) and (3)), in all subcontracts that offer further subcontracting opportunities. If the subcontract (except subcontracts to small business concerns) exceeds $650,000 ($1.5 million for construction of any public facility), the subcontractor must include 52.219-8 in lower tier subcontracts that offer subcontracting opportunities.</p></dd>\n<dd><p>(D) 52.222-26, Equal Opportunity (Mar 2007) (E.O. 11246)</p></dd>\n<dd><p>(E) 52.222-35, Equal Opportunity for Veterans (Sep 2010) (38 U.S.C. 4212).</p></dd>\n<dd><p>(F) 52.222-36, Affirmative Action for Workers with Disabilities (Oct 2010) (<u>2</u>9 U.S.C. 793).</p></dd>\n<dd><p>(G) 52.222-40, Notification of Employee Rights Under the National Labor Relations Act (Dec 2010) (E.O. 13496). Flow down required in accordance with paragraph (f) of FAR clause 52.222-40.</p></dd>\n<dd><p>(H) 52.222-41, Service Contract Act of 1965 (Nov 2007) (41 U.S .C. Chapter 67, et seq.).</p></dd>\n<dd><p>(I) 52.222-50, Combating Trafficking in Persons (Feb 2009) (22 U.S.C. 7104 (g)).</p></dd>\n<dd><p>(J) 52.222-51, Exemption from Application of the Service Contract Act to Contracts for Maintenance, Calibration, or Repair of Certain Equipment-Requirements (Nov 2007) (41 U.S.C. Chapter 67.).</p></dd>\n<dd><p>(K) 52.222-53, Exemption from Application of the Service Contract Act to Contracts for Certain Services­ Requirements (Feb 2009) (41 U.S.C. 351, et seq.).</p></dd>\n<dd><p>(L) 52.222-54, Employment Eligibility Verification (E.O . 12989) (Jul 2012).</p></dd>\n<dd><p>(M) 52.226-6, Promoting Excess Food Donation to Nonprofit Organizations. (Mar 2009) (42 U.S.C.1792). Flow down required in accordance with paragraph (e) of FAR clause 52.226-6.</p></dd>\n<dd><p>(N) 52.247-64, Preference for Privately Owned U.S.­ Flag Commercial Vessels (Feb 2006) (46 U.S.C. Appx . 124l (b)) and 10 U .S.C. 2631). Flow down required in accordance with paragraph (d) of FAR clause 52.247-64.</p></dd></dl></p></dd></dl>'
WHERE clause_version_id = 15 AND clause_name = '52.212-5 Alternate II (DEVIATION 2013-00019)';

UPDATE Clauses
SET clause_data = '<p>Alternate II (OCT 1997). As prescribed in 15.209(a)(2), add a paragraph (c)(9) substantially the same as the following to the basic clause:</p><p>{{paragraph_c9_52.215-1-AltII}}</p>'
--               '<p>Alternate II (OCT 1997). As prescribed in 15.209(a)(2), add a paragraph (c)(9) substantially the same as the following to the basic clause:</p><p>(9) Offerors may submit proposals that depart from stated requirements. Such proposals shall clearly identify why the acceptance of the proposal would be advantageous to the Government. Any deviations from the terms and conditions of the solicitation, as well as the comparative advantage to the Government, shall be clearly identified and explicitly defined. The Government reserves the right to amend the solicitation to allow all offerors an opportunity to submit revised proposals based on the revised requirements.</p>'
WHERE clause_version_id = 15 AND clause_name = '52.215-1 Alternate II';

INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size, fill_in_group_number, fill_in_placeholder, fill_in_default_data, fill_in_display_rows, fill_in_for_table, fill_in_heading) 
SELECT clause_id, 'paragraph_c9_52.215-1-AltII', 'M', 1000, NULL, NULL, '(9) Offerors may submit proposals that depart from stated requirements. Such proposals shall clearly identify why the acceptance of the proposal would be advantageous to the Government. Any deviations from the terms and conditions of the solicitation, as well as the comparative advantage to the Government, shall be clearly identified and explicitly defined. The Government reserves the right to amend the solicitation to allow all offerors an opportunity to submit revised proposals based on the revised requirements.', 12, 0, NULL FROM Clauses WHERE clause_name = '52.215-1 Alternate II' AND clause_version_id = 15; 


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT clause_id, prescription_id FROM Prescriptions, Clauses
WHERE prescription_name IN ('215.408(4)(ii)') 
AND clause_id IN (SELECT clause_id FROM Clauses WHERE clause_name = '52.215-20 Alternate I' AND clause_version_id = 15); 


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT clause_id, prescription_id FROM Prescriptions, Clauses
WHERE prescription_name IN ('215.408(4)(ii)') 
AND clause_id IN (SELECT clause_id FROM Clauses WHERE clause_name = '52.215-20 Alternate II' AND clause_version_id = 15); 


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT clause_id, prescription_id FROM Prescriptions, Clauses
WHERE prescription_name IN ('215.408(4)(ii)') 
AND clause_id IN (SELECT clause_id FROM Clauses WHERE clause_name = '52.215-20 Alternate III' AND clause_version_id = 15); 


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT clause_id, prescription_id FROM Prescriptions, Clauses
WHERE prescription_name IN ('215.408(4)(ii)') 
AND clause_id IN (SELECT clause_id FROM Clauses WHERE clause_name = '52.215-20 Alternate IV' AND clause_version_id = 15); 

UPDATE Clauses
SET clause_conditions = '(A) DOCUMENT TYPE IS: SOLICITATION (B) DOCUMENT TYPE IS: AWARD (C) 52.219-9 IS: INCLUDED' -- '(A) DOCUMENT TYPE IS: SOLICITATION (B) DOCUMENT TYPE IS: AWARD (C) 52.219-9 ALTERNATE I IS: INCLUDED'
WHERE clause_version_id = 15 AND clause_name = '52.219-9 Alternate I (DEVIATION 2013-00014)';

/*
*** End of SQL Scripts at Mon Feb 29 18:04:16 EST 2016 ***
*/
