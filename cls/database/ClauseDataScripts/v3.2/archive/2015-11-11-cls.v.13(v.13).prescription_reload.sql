/*
Clause Parser Run at Wed Nov 11 15:24:54 EST 2015
(Without Correction Information)
*/
/* ===============================================
 * Prescriptions
   =============================================== */
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '10.003') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '11.107(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '11.107(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '11.204(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '11.204(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '11.204(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '11.204(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '11.304') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '11.404(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '11.404(a)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '11.404(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '11.503') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '11.503(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '11.503(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '11.503(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '11.604(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '11.604(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '11.703(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '11.703(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '11.703(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '12.301(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '12.301(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '12.301(b)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '12.301(b)(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '12.301(b)(4)(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '12.301(b)(4)(ii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '12.301(c)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '13.302-5(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '13.302-5(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '13.302-5(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '13.302-5(d)(3)(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '13.404') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(c)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(c)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(c)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(f)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(g)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(g)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(h)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(j)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(l)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(m)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(o)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(o)(2)(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(o)(2)(ii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(p)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(p)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(q)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(r)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(s)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(t)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(v)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(w)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(x)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-7(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-7(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-7(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-7(c)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-7(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.209(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.209(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.209(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.209(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.209(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.209(b)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.209(b)(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.209(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.209(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.209(f)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.209(h)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.408(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.408(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.408(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.408(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.408(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.408(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.408(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.408(f)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.408(f)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.408(g)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.408(h)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.408(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.408(j)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.408(k)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.408(l)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.408(m)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.408(n)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.408(n)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.408(n)(2)(iii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.105') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.203-4(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.203-4(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.203-4(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.205-4') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.206-4') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.307(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.307(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.307(a)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.307(a)(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.307(a)(5)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.307(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.307(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.307(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.307(e)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.307(e)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.307(f)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.307(f)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.307(g)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.406') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.406(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.406(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.506') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.506(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.506(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.506(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.506(d)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.506(d)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.506(d)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.506(d)(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.506(d)(5)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.506(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.506(f)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.506(g)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.601(f)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.601(f)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.601(f)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.603-4(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.603-4(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.603-4(b)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.603-4(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '17.109') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '17.109(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '17.208(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '17.208(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '17.208(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '17.208(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '17.208(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '17.208(f)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '17.208(g)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.1309(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.1309(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.1309(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.1309(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.1407') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.1506(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.1506(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.309(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.309(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.309(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.309(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.508((c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.508(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.508(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.508(d)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.508(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.508(f)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.708(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.708(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.708(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.708(b)(1)(iii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.708(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.708(c)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.811-3') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.811-3(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.811-3(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.811-3(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.811-3(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.811-3(d)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.811-3(d)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.811-3(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2.201') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '201.602-70') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2012-O0004') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2012-O0007') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2013-00017') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2013-00019') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2013-O0006') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2013-O0010') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2013-O0014') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2013-O0014)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2013-O0016') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2013-O0017') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2013-O0019') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2014-00014') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2014-O0004') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2014-O0005') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2014-O0010') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2014-O0016') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2014-O0018') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2014-O0020') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2014-OO0009') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2015-00003') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2015-00009') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2015-00016') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2015-00017') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2015-O0007') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2015-O0010') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2015-O0012') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2015-O0013') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2015-O0016') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2015-OO005') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2016-00001') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2016-00002') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2016-0001') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2016-0002') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '203.1004(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '203.1004(b)(2)(ii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '203.171-4') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '203.171-4(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '203.570-3') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '203.97') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '204.1105') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '204.1202') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '204.404-70(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '204.404-70(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '204.404-70(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '204.470-3') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '204.7104-1(b)(3)(iv)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '204.7109(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '204.7109(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '204.7304(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '204.7304(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '204.7304(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '204.7304[c]') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '204.7403(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '204.7403(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '204.7403(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '205.47') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '206.302-3-70') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '208.7305(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '209.104-70') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '209.270-5') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '209.409') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '209.470-4(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '209.470-4(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '209.570-4(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '209.570-4(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '209.571-8(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '209.571-8(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '211.002-70') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '211.204(c)(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '211.204(c)(ii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '211.272') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '211.273-4') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '211.274-6(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '211.274-6(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '211.274-6(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '211.275-3') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '211.503') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '211.503(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '212.7103') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '213.106-2-70') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '215.370-3(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '215.370-3(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '215.371-6') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '215.408') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '215.408(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '215.408(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '215.408(3)(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '215.408(3)(ii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '215.408(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '215.408(4)(ii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '215.408(5)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '216.203-4-70(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '216.203-4-70(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '216.203-4-70(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '216.203-4-70(c)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '216.203-4-70(c)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '216.307(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '216.406') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '216.406(e)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '216.506(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '216.506(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '216.506(d)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '216.506(d)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '216.601(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '217.208-70(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '217.208-70(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '217.208-70(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '217.208-70(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '217.7005') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '217.7104(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '217.7303') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '217.7406(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '217.7702') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '219.309(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '219.708(b)(1)(A)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '219.708(b)(1)(B)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '219.811-3') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '219.811-3(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '219.811-3(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1006') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1006(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1006(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1006(c)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1006(c)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1006(e)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1006(e)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1006(e)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1006(e)(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1006(f)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.103-5(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.103-5(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1103') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1207') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1310(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1310(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1310(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1310(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1408(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1408(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1505(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1505(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1605') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1705(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1705(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1705(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1803') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1906') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.202') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.305') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.407(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.407(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.407(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.407(f)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.407(g)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.407(h)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.505(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.505(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.505(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.505(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.505-(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.505-(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.610') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.810(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.810(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.810(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.810(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.810(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.810(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.810(f)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.810(g)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '222.1771') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '222.7004') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '222.7102') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '222.7201(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '222.7201(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '222.7201(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '222.7302') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '222.7405') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '223.303') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '223.370-5') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '223.570-2') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '223.7106') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '223.7106(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '223.7106(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '223.7203') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '223.7306') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225-7011-3') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225-7306') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225-7799-4') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225-7799-4(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225-7799-4(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225-7799-4(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225-7799-4(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225-7799-4(f)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225-7799-4(h)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225..1101(10)(i)(C)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(1)(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(1)(ii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(10)(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(10)(i)(A)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(10)(i)(B)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(10)(i)(C)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(10)(i)(D)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(10)(i)(E)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(10)(i)(F)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(2)(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(2)(ii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(2)(iii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(5)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(5)(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(5)(ii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(6)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(6)(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(6)(ii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(7)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(8)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(9)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(9)(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(9)(ii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(9)(iii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(9)(iv)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(9)(v)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(9)(vi)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1103(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1103(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1103(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1103(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.302-6') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.371-5') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.371-5(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.372-2') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7002-2(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7002-3(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7002-3(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7003-5(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7003-5(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7003-5(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7006-4(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7006-4(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7007-3') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7009-5') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7011-3') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7012-3') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7017-4(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7017-4(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7102-4') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7204(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7204(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7307(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7307(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7402-5(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7503(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7503(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7503(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7503(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7503(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7503(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7503(b)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7503(b)(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7605') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7703-4(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7703-4(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7703-4(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7703-4(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.771-5') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.772-5') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7798-6(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7798-6(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7798-6(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7901-4') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7902-5(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7902-5(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '226.104') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.303(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.303(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7009- 4(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7009-1') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7009-2') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7009-2(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7009-2(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7009-3(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7009-3(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7009-4(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7009-4(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7009-4(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7009-4(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7010') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7012') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7102-4(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7102-4(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7103-17') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7103-3(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7103-6(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7103-6(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7103-6(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7103-6(e)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7103-6(e)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7103-6(e)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7103-8(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7103-8(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7104(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7104(e)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7104(e)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7104(e)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7104(e)(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7104(e)(5)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7104(f)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7104(f)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7105-2(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7105-3') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7106(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7107-1(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7107-1(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7107-1(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7107-3') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7203-3(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7203-6(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7203-6(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7203-6(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7203-6(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7203-6(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7203-6(f)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7205(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '228.170') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '228.370(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '228.370(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '228.370(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '228.370(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '228.370(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '229.170-4') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '229.402-1') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '229.402-70(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '229.402-70(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '229.402-70(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '229.402-70(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '229.402-70(c)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '229.402-70(c)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '229.402-70(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '229.402-70(e)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '229.402-70(e)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '229.402-70(f)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '229.402-70(g)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '229.402-70(h)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '229.402-70(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '229.402-70(j)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '23.1005') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '23.1005(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '23.1005(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '23.1105') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '23.206') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '23.303') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '23.303(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '23.406(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '23.406(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '23.406(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '23.406(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '23.406(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '23.505') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '23.602') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '23.705(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '23.705(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '23.705(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '23.705(c)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '23.705(d)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '23.705(d)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '23.804(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '23.804(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '23.903') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '231.100-70') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '232.1005-70(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '232.1005-70(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '232.1110') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '232.412-70(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '232.412-70(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '232.412-70(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '232.502-4-70(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '232.502-4-70(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '232.7004(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '232.7004(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '232.705-70') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '232.7102') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '232.7202') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '232.806(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '232.908') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '233.215-70') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '234.203(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '234.203(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '234.7101(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '234.7101(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '234.7101(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '234.7101(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '234.7101(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '235.070-3') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '235.072') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '235.072(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '235.072(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '235.072(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '235.072(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '235.072(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '235.072(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '235.072©') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '236.570(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '236.570(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '236.570(b)(2)(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '236.570(b)(2)(ii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '236.570(b)(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '236.570(b)(5)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '236.570(b)(6)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '236.570(c)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '236.570(c)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '236.570(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '236.609-70') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '236.609-70(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '237-7003(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '237.171-4') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '237.173-5') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '237.270(d)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '237.270(d)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '237.7003(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '237.7003(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '237.7003(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '237.7003(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '237.7101(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '237.7101(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '237.7101(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '237.7101(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '237.7101(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '237.7101(e)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '237.7101(e)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '237.7101(e)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '237.7101(f)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '237.7101(g)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '237.7402') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '237.7603(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '239.7103(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '239.7103(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '239.7306(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '239.7306(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '239.7411(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '239.7411(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '239.7411(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '239.7411(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '239.7411©') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '239.7603(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '239.7603(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '24.104(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '24.104(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '241.501-70(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '241.501-70(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '242.7001') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '242.7204') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '242.7503') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '243.205-70') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '243.205-71') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '244.305-71') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '244.305-71(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '244.305-71(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '244.403') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '245.107(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '245.107(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '245.107(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '245.107(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '245.107(5)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '246.270-4') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '246.370') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '246.371(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '246.710(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '246.710(1)(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '246.710(1)(ii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '246.710(1)(iii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '246.710(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '246.710(3)(i)(A)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '246.710(3)(i)(B)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '246.710(3)(i)(B)(iii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '246.870-3') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.207') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.207(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.270-4') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.270-4(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.270-4(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.270-4(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.270-4(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.270-4(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.270-4(f)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.270-4(g)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.271-3') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.271-3(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.271-3(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.271-3(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.271-3(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.271-3(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.271-3(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.271-3(g)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.271-3(h)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.271-3(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.271-3(j)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.271-3(k)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.271-3(l)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.271-3(m)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.271-3(n)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.305-70') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.574(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.574(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.574(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.574(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.574(b)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.574(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.574(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.574(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.574(f)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '249.501-70') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '249.7003(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.110') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1101(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1101(1)(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1101(1)(ii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1101(6)(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1101(6)(ii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1101(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1101(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1101(b)(1)(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1101(b)(1)(ii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1101(b)(1)(iii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1101(b)(1)(iv)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1101(b)(2)(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1101(b)(2)(ii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1101(b)(2)(iii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1101(b)(2)(iv)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1101(c)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1101(c)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1101(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1101(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1101(f)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1102(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1102(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1102(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1102(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1102(c)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1102(d)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1102(d)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1102(d)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1102(e)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1103') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1103(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1103(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1103(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1103(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.301-4') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.302-6') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '251.107') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '251.205') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '26.104') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '26.206(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '26.206(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '26.206(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '26.304') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '26.404') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.201-2') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.201-2(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.201-2(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.201-2(a)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.201-2(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.201-2(c)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.201-2(c)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.201-2(c)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.201-2(d)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.201-2(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.202-5(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.202-5(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.202-5(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.202-5(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.203-2') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.303(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.303(b)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.303(b)(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.303(b)(5)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.303(b)(6)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.303(b)(7)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.303(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.303(e)(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.303(e)(5)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.409(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.409(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.409(b)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.409(b)(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.409(b)(5)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.409(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.409(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.409(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.409(f)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.409(g)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.409(h)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.409(j)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.409(k)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.409(l)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '28.101-2') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '28.103-4') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '28.106-4(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '28.106-4(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '28.1102-3(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '28.1102-3(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '28.203-6') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '28.204-4') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '28.309(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '28.309(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '28.310') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '28.311-1') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '28.312') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '28.313(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '28.313(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '29.401-1') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '29.401-2') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '29.401-3') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '29.401-3(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '29.401-3(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '29.401-4(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '29.402-1(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '29.402-1(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '29.402-2(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '29.402-2(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '3.1004(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '3.1004(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '3.103-1') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '3.104-9(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '3.104-9(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '3.1106') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '3.202') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '3.301-1') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '3.404') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '3.502-3') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '3.503-2') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '3.808(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '3.808(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '3.907-7') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '3.908-9') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '30.201-3') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '30.201-3(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '30.201-3(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '30.201-4(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '30.201-4(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '30.201-4(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '30.201-4(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '30.201-4(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '30.202') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.009-2') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.1005') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.1005(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.1005(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.111(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.111(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.111(a)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.111(a)(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.111(a)(5)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.111(a)(6)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.111(a)(7)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.111(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.111(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.111(c)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.111(c)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.1110(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.1110(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.1110(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.1110(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.1110(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.1110(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.1110(g)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.205(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.206') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.206(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.206(g)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.206(g) ***') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.412(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.412(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.412(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.412(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.412(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.412(f)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.502-3(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.502-3(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.502-3(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.502-4(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.502-4(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.502-4(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.502-4(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.611(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.611(a) &(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.611(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.706-1(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.706-1(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.706-2(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.706-2(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.706-3') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.806(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.806(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.806(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.908(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.908(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.908(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.908(c)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '33.106(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '33.106(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '33.215(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '33.215(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '34.104') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '34.203(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '34.203(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '34.203(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.501(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.502') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.503') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.504') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.505') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.506') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.507') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.508') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.509') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.510') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.511') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.512') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.513') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.514') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.515') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.516') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.517') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.518') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.519') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.520') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.521') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.522') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.523') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.609-1(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.609-2(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.609-3') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.609-4') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '37.110(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '37.110(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '37.110(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '37.113-2(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '37.113-2(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '37.115-3') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '37.116-2') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '37.304(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '37.304(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '37.304(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '37.403') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '39.106') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '4.103') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '4.1105(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '4.1105(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '4.1202(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '4.1202(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '4.1303') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '4.1403(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '4.1705(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '4.1705(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '4.1804(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '4.1804(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '4.1804(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '4.303') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '4.404(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '4.607(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '4.607(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '4.905') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '41.501(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '41.501(c)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '41.501(c)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '41.501(c)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '41.501(c)(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '41.501(c)(5)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '41.501(d)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '41.501(d)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '41.501(d)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '41.501(d)(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '41.501(d)(5)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '41.501(d)(6)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '41.501(d)(7)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '42.1107(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '42.1305(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '42.1305(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '42.1305(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '42.1305(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '42.703-2(f)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '42.709-6') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '42.802') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '42.903') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '43.107') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '43.205(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '43.205(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '43.205(a)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '43.205(a)(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '43.205(a)(5)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '43.205(a)(6)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '43.205(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '43.205(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '43.205(b)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '43.205(b)(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '43.205(b)(6)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '43.205(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '43.205(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '43.205(f)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '44.204(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '44.204(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '44.204(a)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '44.204(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '44.204(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '44.403') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '45.107(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '45.107(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '45.107(a)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '45.107(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '45.107(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.301') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.302') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.303') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.304') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.305') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.306') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.307(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.308') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.309') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.311') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.312') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.313') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.314') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.315') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.316') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.710(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.710(a)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.710(a)(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.710(a)(5)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.710(a)(6)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.710(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.710(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.710(b)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.710(b)(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.710(c)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.710(c)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.710(c)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.710(c)(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.710(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.710(e)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.710(e)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.805') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.805(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.805(a)(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.103-2') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.104-4') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-1(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-1(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-1(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-1(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-1(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-1(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-3(d)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-3(e)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-4(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-4(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-4(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-5( c )') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-5(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-5(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-5(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-5(f)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-6(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-6(c)(5)(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-6(c)(5)(ii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-6(c)(6)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-7(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-7(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-7(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-8(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-8(a)(2)(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-8(a)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-8(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-9(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.208-2') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.303- 14(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.303-1(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.303-10(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.303-11(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.303-12(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.303-13(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.303-14(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.303-15(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.303-16(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.303-17(f)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.303-2(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.303-3(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.303-4(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.303-5(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.303-6(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.303-7(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.303-8(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.303-9(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.304-7(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.305-12(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.305-13(a)(3)(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.305-14(b)(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.305-15(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.305-16(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.305-16(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.305-16(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.305-16(d)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.305-17') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.305-2(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.305-3(b)(4)(ii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.305-3(f)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.305-4(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.305-5(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.305-5(c)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.305-6(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.305-6(e)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.305-6(e)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.305-6(e)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.305-6(f)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.305-9(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.405') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.507(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.507(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.507(a)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '48.201') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '48.201(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '48.201(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '48.201(e)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '48.201(f)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '48.202') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '49.502(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '49.502(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '49.502(b)(1)(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '49.502(b)(1)(ii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '49.502(b)(1)(iii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '49.502(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '49.502(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '49.502(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '49.503(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '49.503(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '49.503(a)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '49.503(a)(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '49.503(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '49.504(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '49.504(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '49.504(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '49.504(c)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '49.504(c)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '49.504(c)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '49.505(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '49.505(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '50.104-4') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '50.206(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '50.206(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '50.206(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '50.206(b)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '50.206(c)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '50.206(c)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '50.206(c)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '50.206(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '50.403-3') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '51.107') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '51.205') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '52.107(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '52.107(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '52.107(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '52.107(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '52.107(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '52.107(f)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '53.111') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '7.203') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '7.305(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '7.305(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '7.305(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '7.404') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '8.005') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '8.1104(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '8.1104(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '8.1104(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '8.1104(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '8.505') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '9.104-7(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '9.104-7(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '9.104-7(c)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '9.108-5(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '9.108-5(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '9.206-2') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '9.308-1(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '9.308-1(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '9.308-1(a)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '9.308-1(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '9.308-1(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '9.308-1(b)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '9.308-2(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '9.308-2(a)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '9.308-2(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '9.308-2(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '9.308-2(b)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '9.409') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, 'PGI 204.7108(d)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, 'PGI 204.7108(d)(10)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, 'PGI 204.7108(d)(11)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, 'PGI 204.7108(d)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, 'PGI 204.7108(d)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, 'PGI 204.7108(d)(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, 'PGI 204.7108(d)(5)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, 'PGI 204.7108(d)(6)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, 'PGI 204.7108(d)(7)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, 'PGI 204.7108(d)(8)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, 'PGI 204.7108(d)(9)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, 'Preamble to clause 252.209-7991 as written in 2016-00002') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;

UPDATE Prescriptions
JOIN (
	SELECT
		prescription_id,
		concat('http://www.ecfr.gov/cgi-bin/text-idx?node=se48.', volume , '.', part, '_1', subpart, replace(suffix, '-', '_6')) uri
	FROM (
		SELECT
			prescription_id,
			prescription_name,
			subpart,
			part,
			case when suffix like '%(%' then left(suffix,locate('(',suffix)-1) else suffix end suffix,
			case when part regexp '^[0-9]+$'
				then
					case
						when cast(part as UNSIGNED) >= '1' and cast(part as UNSIGNED) <= '51' then 1
						when cast(part as UNSIGNED) >= '52' and cast(part as UNSIGNED) <= '99' then 2
						when cast(part as UNSIGNED) >= '200' and cast(part as UNSIGNED) <= '299' then 3
					end
			end volume
		FROM (
			SELECT
				prescription_id,
				prescription_name,
				substring_index(substring_index(prescription_name,'(', 1),'.',1) part,
				substring_index(substring_index(substring_index(prescription_name,'(', 1),'.',-1),'-',1) subpart,
				case
					when locate('-',prescription_name) != 0
						then right(prescription_name, length(prescription_name) - locate('-',prescription_name)+1)
					else ''
				end suffix
			FROM Prescriptions
		) Prescriptions
	) Prescriptions
) sub ON (sub.prescription_id = Prescriptions.prescription_id)
SET Prescriptions.prescription_url = ifNull(sub.uri,'')
WHERE ifNull(prescription_url, '') = '';

/* ===============================================
 * Questions
   =============================================== */


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198381, prescription_id FROM Prescriptions WHERE prescription_name IN ('15.209(a)','15.209(a)(1)','15.209(a)(2)','19.811-3(c)','213.106-2-70','215.371-6','215.408(3)(i)','215.408(3)(ii)','215.408(4)','29.401-3(b)','32.1005(b)(2)'); -- [PROCEDURES] 'COMPETITION'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198382, prescription_id FROM Prescriptions WHERE prescription_name IN ('13.302-5(d)','16.105','213.106-2-70','246.370','3.103-1','47.405'); -- [PROCEDURES] 'SIMPLIFIED ACQUISITION PROCEDURES (FAR PART 13)'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198383, prescription_id FROM Prescriptions WHERE prescription_name IN ('12.301(b)(3)','12.301(b)(4)','204.7109(b)','4.905','45.107(a)','45.107(a)(2)','45.107(a)(3)'); -- [PROCEDURES] 'COMMERCIAL PROCEDURES (FAR PART 12)'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198384, prescription_id FROM Prescriptions WHERE prescription_name IN ('225.1101(10)(i)','225.1101(10)(i)(A)','225.1101(10)(i)(B)','225.1101(10)(i)(C)','225.1101(10)(i)(D)','225.1101(10)(i)(E)','225.1101(10)(i)(F)','225.1101(2)(i)','225.1101(2)(ii)','225.1101(2)(iii)'); -- [PROCEDURES] 'PROCEDURES IN SUPPORT OF OPERATIONS IN AFGHANISTAN (DFARS 225.7703-1)'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198385, prescription_id FROM Prescriptions WHERE prescription_name IN ('15.209(a)','15.209(a)(1)','15.209(a)(2)','15.209(b)(1)','15.209(b)(2)','15.209(b)(3)','15.209(b)(4)','15.209(c)','15.209(e)','15.209(f)','15.209(h)','15.408(a)','15.408(a)(1)','15.408(a)(2)','15.408(b)','15.408(c)','15.408(d)','15.408(e)','15.408(f)(1)','15.408(f)(2)','15.408(g)','15.408(h)','15.408(i)','15.408(j)','15.408(k)','15.408(l)','15.408(m)','16.203-4(a)','16.203-4(b)','16.203-4(c)','16.205-4','16.206-4','19.708(b)(1)','19.708(c)(1)','22.1103','237.7003(a)','246.370','247.270-4(c)','30.201-4(a)','30.201-4(b)(1)','30.201-4(c)','30.201-4(e)','32.1005(b)(1)','36.520','42.1305(b)(1)','42.1305(b)(2)','43.107','44.204(c)'); -- [PROCEDURES] 'NEGOTIATION (FAR PART 15)'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198389, prescription_id FROM Prescriptions WHERE prescription_name IN ('12.301(b)(2)','15.408(n)(2)','15.408(n)(2)(iii)','19.309(a)(2)','23.303','23.303(b)','235.072','25.302-6','3.202','30.202'); -- [ISSUING OFFICE] 'DEPARTMENT OF DEFENSE'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198390, prescription_id FROM Prescriptions WHERE prescription_name IN ('26.104','44.204(a)(2)'); -- [ISSUING OFFICE] 'CIVILIAN'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198391, prescription_id FROM Prescriptions WHERE prescription_name IN ('12.301(b)(2)','19.309(a)(2)','44.204(a)(2)'); -- [ISSUING OFFICE] 'NATIONAL AERONAUTICS AND SPACE ADMINISTRATION'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198395, prescription_id FROM Prescriptions WHERE prescription_name IN ('14.201-6(g)(2)'); -- [SEALED BIDDING REQUIREMENTS] 'OFFERORS WOULD BE UNWILLING TO PROVIDE SOLICITATION ACCEPTANCE PERIODS LONG ENOUGH TO ALLOW WRITTEN CONFIRMATION.'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198396, prescription_id FROM Prescriptions WHERE prescription_name IN ('14.201-6(o)(1)'); -- [SEALED BIDDING REQUIREMENTS] 'BID SAMPLES REQUIRED'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198397, prescription_id FROM Prescriptions WHERE prescription_name IN ('14.201-6(o)(2)(i)','14.201-6(o)(2)(ii)'); -- [SEALED BIDDING REQUIREMENTS] 'BID SAMPLES WAIVED'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198398, prescription_id FROM Prescriptions WHERE prescription_name IN ('14.201-6(o)(2)(i)','14.201-6(o)(2)(ii)'); -- [SEALED BIDDING REQUIREMENTS] 'BID SAMPLES WAIVED FOR PRODUCTS PRODUCED AT SAME PLANT AS PREVIOUSLY PROCURED'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198399, prescription_id FROM Prescriptions WHERE prescription_name IN ('14.201-6(p)(2)'); -- [SEALED BIDDING REQUIREMENTS] 'DESCRIPTIVE LITERATURE WAIVER IS POSSIBLE FOR A BIDDER OFFERING A PREVIOUSLY SUPPLIED PRODUCT THAT MEETS SPECIFICATION REQUIREMENTS'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198400, prescription_id FROM Prescriptions WHERE prescription_name IN ('14.201-6(p)(1)','14.201-6(p)(2)'); -- [SEALED BIDDING REQUIREMENTS] 'DESCRIPTIVE LITERATURE INFORMATION WILL NOT BE READILY AVAILABLE UNLESS IT IS SUBMITTED BY BIDDERS'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198401, prescription_id FROM Prescriptions WHERE prescription_name IN ('14.201-6(v)'); -- [SEALED BIDDING REQUIREMENTS] 'FACSIMILE BIDS ARE AUTHORIZED'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198402, prescription_id FROM Prescriptions WHERE prescription_name IN ('14.201-6(i)','14.201-6(j)'); -- [SEALED BIDDING REQUIREMENTS] 'SOLICITATION CONTAINS A MINIMUM ACCEPTANCE PERIOD'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198403, prescription_id FROM Prescriptions WHERE prescription_name IN ('14.201-6(s)'); -- [SEALED BIDDING REQUIREMENTS] 'MULTIPLE TECHNICAL PROPOSAL ARE PERMISSIBLE'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198404, prescription_id FROM Prescriptions WHERE prescription_name IN ('14.201-6(g)(1)','14.201-6(g)(2)'); -- [SEALED BIDDING REQUIREMENTS] 'TELEGRAPHIC BIDS ARE AUTHORIZED'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198407, prescription_id FROM Prescriptions WHERE prescription_name IN ('19.508(c)','19.508(d)','19.508(d)(2)'); -- [WAIVER OF NON MANUFACTURING RULE] 'YES'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198409, prescription_id FROM Prescriptions WHERE prescription_name IN ('19.508(c)','19.508(d)'); -- [FEDERAL PRISON INDUSTRY] 'YES'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198411, prescription_id FROM Prescriptions WHERE prescription_name IN ('2014-00014','225-7799-4(c)','225-7799-4(d)','225.7703-4(c)'); -- [COMPETITION PREFERENCES] 'RESTRICTED TO PRODUCTS OR SERVICES FROM AFGHANISTAN'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198412, prescription_id FROM Prescriptions WHERE prescription_name IN ('206.302-3-70','225.1101(2)(i)','225.1101(2)(ii)','225.1101(2)(iii)','25.1101(a)(1)'); -- [COMPETITION PREFERENCES] 'RESTRICTED TO DOMESTIC END PRODUCTS'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198413, prescription_id FROM Prescriptions WHERE prescription_name IN ('2014-00014','225-7799-4(c)','225-7799-4(d)'); -- [COMPETITION PREFERENCES] 'LIMITED TO PRODUCTS OR SERVICES FROM A CENTRAL ASIAN STATE'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198414, prescription_id FROM Prescriptions WHERE prescription_name IN ('2014-00014','225-7799-4(d)'); -- [COMPETITION PREFERENCES] 'LIMITED TO PRODUCTS OR SERVICES FROM PAKISTAN'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198415, prescription_id FROM Prescriptions WHERE prescription_name IN ('2014-00014','225-7799-4(d)'); -- [COMPETITION PREFERENCES] 'LIMITED TO PRODUCTS OR SERVICES FROM SOUTH CAUCASUS'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198416, prescription_id FROM Prescriptions WHERE prescription_name IN ('225..1101(10)(i)(C)','225.1101(10)(i)','225.1101(10)(i)(A)','225.1101(10)(i)(B)','225.1101(10)(i)(D)','225.1101(10)(i)(E)','225.1101(10)(i)(F)','225.1101(6)','225.1101(6)(i)','225.1101(6)(ii)'); -- [COMPETITION PREFERENCES] 'PURCHASE FROM A FOREIGN SOURCE IS RESTRICTED'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198417, prescription_id FROM Prescriptions WHERE prescription_name IN ('225.7703-4(c)'); -- [COMPETITION PREFERENCES] 'DIRECTED TO A PARTICULAR SOURCE(S) FROM AFGHANISTAN'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198418, prescription_id FROM Prescriptions WHERE prescription_name IN ('2014-00014','225-7799-4(f)'); -- [COMPETITION PREFERENCES] 'PREFERENCE FOR PRODUCTS OR SERVICES FROM PAKISTAN'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198419, prescription_id FROM Prescriptions WHERE prescription_name IN ('2014-00014','225-7799-4(f)'); -- [COMPETITION PREFERENCES] 'PREFERENCE FOR PRODUCTS OR SERVICES FROM THE SOUTH CAUCASUS'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198420, prescription_id FROM Prescriptions WHERE prescription_name IN ('2014-00014','225-7799-4(b)','225-7799-4(f)','225.7703-4(a)'); -- [COMPETITION PREFERENCES] 'PREFERENCE FOR PRODUCTS OR SERVICES FROM AFGHANISTAN'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198421, prescription_id FROM Prescriptions WHERE prescription_name IN ('2014-00014','225-7799-4(b)','225-7799-4(f)'); -- [COMPETITION PREFERENCES] 'PREFERENCE FOR PRODUCTS OR SERVICES FROM CENTRAL ASIAN STATE'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198422, prescription_id FROM Prescriptions WHERE prescription_name IN ('2015-O0012','225.7798-6(b)'); -- [COMPETITION PREFERENCES] 'PREFERENCE FOR PRODUCTS OR SERVICES OF DJIBOUTI'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198423, prescription_id FROM Prescriptions WHERE prescription_name IN ('2015-O0012','225.7798-6(c)'); -- [COMPETITION PREFERENCES] 'LIMIT COMPETITION TO PRODUCTS OR SERVICES OF DJIBOUTI'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198425, prescription_id FROM Prescriptions WHERE prescription_name IN ('225.1101(7)','225.1101(8)'); -- [UK OFFER] 'YES'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198427, prescription_id FROM Prescriptions WHERE prescription_name IN ('225.7003-5(b)','228.370(b)'); -- [DERIVATIVES ANTICIPATED] 'YES'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198429, prescription_id FROM Prescriptions WHERE prescription_name IN ('15.408(f)(2)','15.408(n)(2)','15.408(n)(2)(iii)','16.601(f)(1)','16.601(f)(2)','16.603-4(b)(3)','216.601(e)','44.204(c)'); -- [ADEQUATE PRICE COMPETITION] 'YES'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198431, prescription_id FROM Prescriptions WHERE prescription_name IN ('213.106-2-70'); -- [SIMPLIFIED ACQUISITION TEST PROGRAM] 'YES'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198433, prescription_id FROM Prescriptions WHERE prescription_name IN ('12.301(b)(2)','19.811-3(d)(1)'); -- [SDB MECHANISMS ON A REGIONAL BASIS] 'YES'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198437, prescription_id FROM Prescriptions WHERE prescription_name IN ('4.1105(a)(1)'); -- [FULL AND OPEN EXCEPTION] '6.302-2 UNUSUAL AND COMPELLING URGENCY'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198438, prescription_id FROM Prescriptions WHERE prescription_name IN ('225.1101(2)(ii)','225.1101(2)(iii)'); -- [FULL AND OPEN EXCEPTION] '6.302-3 INDUSTRIAL MOBILIZATION; ENGINEERING, DEVELOPMENTAL, OR RESEARCH CAPABILITY; OR EXPERT SERVICES'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198439, prescription_id FROM Prescriptions WHERE prescription_name IN ('217.7303'); -- [FULL AND OPEN EXCEPTION] '6.302-5 AUTHORIZED OR REQUIRED BY STATUTE'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198443, prescription_id FROM Prescriptions WHERE prescription_name IN ('219.309(1)'); -- [SMALL BUSINESS SIZE STANDARD] 'THE ESTIMATED ANNUAL VALUE OF THE CONTRACT IS EXPECTED TO EXCEED THE SMALL BUSINESS SIZE STANDARD FOR THE ASSIGNED NAICS CODE, IF EXPRESSED IN DOLLARS'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198444, prescription_id FROM Prescriptions WHERE prescription_name IN ('219.309(1)'); -- [SMALL BUSINESS SIZE STANDARD] 'THE ESTIMATED ANNUAL VALUE OF THE CONTRACT IS EXPECTED TO EXCEED $70 MILLION, IF THE SMALL BUSINESS SIZE STANDARD FOR THE ASSIGNED NAICS CODE IS EXPRESSED AS NUMBER OF EMPLOYEES'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198446, prescription_id FROM Prescriptions WHERE prescription_name IN ('215.371-6'); -- [SOLICITATION TIME] 'YES'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198448, prescription_id FROM Prescriptions WHERE prescription_name IN ('12.301(c)(1)'); -- [EVALUATION FACTORS] 'USE OF EVALUATION FACTORS IS APPROPRIATE'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198450, prescription_id FROM Prescriptions WHERE prescription_name IN ('215.370-3(a)'); -- [EVALUATION FACTORS] 'USE OF MEMBERS OF THE SELECTED RESERVE IS A SELECTION FACTOR'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198451, prescription_id FROM Prescriptions WHERE prescription_name IN ('47.305-16(b)(1)'); -- [EVALUATION FACTORS] 'SHIPPING AND OTHER CHARACTERISTICS ARE REQUIRED TO EVALUATE OFFERS'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198452, prescription_id FROM Prescriptions WHERE prescription_name IN ('47.305-5(b)(2)'); -- [EVALUATION FACTORS] 'PLACE(S) OF DELIVERY ARE TENTATIVE AND ONLY FOR THE PURPOSE OF EVALUATING OFFERS'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198453, prescription_id FROM Prescriptions WHERE prescription_name IN ('14.201-6(p)(1)','14.201-6(p)(2)'); -- [EVALUATION FACTORS] 'DESCRIPTIVE LITERATURE IS REQUIRED TO EVALUATE TECHNICAL ACCEPTABILITY'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198455, prescription_id FROM Prescriptions WHERE prescription_name IN ('32.1005(b)(2)'); -- [PRICE EVALUATION ADJUSTMENT] 'GOVERNMENT INTENDS TO ADJUST PROPOSED PRICES FOR EVALUATION PURPOSES (SEE 32.1004(e))'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198456, prescription_id FROM Prescriptions WHERE prescription_name IN ('47.305-3(b)(4)(ii)'); -- [PRICE EVALUATION ADJUSTMENT] 'PRICE EVALUATION FOR SHIPMENTS FROM VARIOUS SHIPPING POINTS IS CONTEMPLATED'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198458, prescription_id FROM Prescriptions WHERE prescription_name IN ('217.7104(a)','232.1110','239.7411(c)'); -- [AWARD TYPE] 'AGREEMENT (INCLUDING BASIC AND LOAN)'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198460, prescription_id FROM Prescriptions WHERE prescription_name IN ('217.7406(b)'); -- [AWARD TYPE] 'CHANGE ORDER'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198463, prescription_id FROM Prescriptions WHERE prescription_name IN ('16.506(g)','216.506(a)','217.7406(b)','246.370','4.1705(a)','4.1705(b)'); -- [AWARD TYPE] 'INDEFINITE DELIVERY CONTRACT'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198464, prescription_id FROM Prescriptions WHERE prescription_name IN ('16.603-4(b)(1)','16.603-4(b)(2)','16.603-4(b)(3)','16.603-4(c)','32.502-4(c)','44.204(a)(1)','44.204(a)(3)','46.302'); -- [AWARD TYPE] 'LETTER CONTRACT'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198470, prescription_id FROM Prescriptions WHERE prescription_name IN ('13.302-5(b)','13.302-5(c)','219.811-3(3)','32.806(a)(1)','32.806(a)(2)'); -- [AWARD TYPE] 'PURCHASE ORDER'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198472, prescription_id FROM Prescriptions WHERE prescription_name IN ('7.404'); -- [LEASE OPTION] 'YES'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198474, prescription_id FROM Prescriptions WHERE prescription_name IN ('16.506(a)','16.506(b)','16.506(c)','29.401-1'); -- [TYPE OF INDEFINITE DELIVERY] 'INDEFINITE DELIVERY DEFINITE QUANTITY'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198475, prescription_id FROM Prescriptions WHERE prescription_name IN ('16.506(a)','16.506(b)','16.506(d)(1)','16.506(f)','247.270-4(d)','247.270-4(e)','29.401-1','32.706-1(b)'); -- [TYPE OF INDEFINITE DELIVERY] 'INDEFINITE DELIVERY INDEFINITE QUANTITY'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198476, prescription_id FROM Prescriptions WHERE prescription_name IN ('16.506(a)','16.506(b)','216.506(d)','216.506(d)(1)','32.706-1(b)'); -- [TYPE OF INDEFINITE DELIVERY] 'REQUIREMENTS'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198477, prescription_id FROM Prescriptions WHERE prescription_name IN ('22.1605'); -- [INDEFINITE QUANTITY TOTAL ORDER VALUE] 'YES'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198479, prescription_id FROM Prescriptions WHERE prescription_name IN ('16.506'); -- [EXCEEDS GOVERNMENT CAPABILITY] 'YES'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198481, prescription_id FROM Prescriptions WHERE prescription_name IN ('247.270-4'); -- [PAYMENT OF FIXED CHARGES] 'YES'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198483, prescription_id FROM Prescriptions WHERE prescription_name IN ('232.1110'); -- [ORDER VALUE] 'YES'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198486, prescription_id FROM Prescriptions WHERE prescription_name IN ('203.171-4'); -- [TYPE OF ORDER] 'DELIVERY ORDER'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198488, prescription_id FROM Prescriptions WHERE prescription_name IN ('203.171-4','216.307(a)'); -- [TYPE OF ORDER] 'TASK ORDER'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198489, prescription_id FROM Prescriptions WHERE prescription_name IN ('216.307(a)'); -- [TASK ORDER] 'YES'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198491, prescription_id FROM Prescriptions WHERE prescription_name IN ('11.604(a)','11.604(b)'); -- [ORDER RATING] 'YES'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198493, prescription_id FROM Prescriptions WHERE prescription_name IN ('16.406'); -- [PROVISIONING ORDER] 'YES'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198495, prescription_id FROM Prescriptions WHERE prescription_name IN ('17.109'); -- [MULTIPLE OR MULTI-YEAR] 'MULTI-YEAR CONTRACT'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198496, prescription_id FROM Prescriptions WHERE prescription_name IN ('22.1006'); -- [MULTIPLE OR MULTI-YEAR] 'MULTIPLE YEAR CONTRACT'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198498, prescription_id FROM Prescriptions WHERE prescription_name IN ('229.402-70(h)','229.402-70(i)','229.402-70(j)'); -- [PLACE OF ISSUANCE] 'SOLICITATION/AWARD ISSUED IN THE UNITED KINGDOM'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198499, prescription_id FROM Prescriptions WHERE prescription_name IN ('217.7303'); -- [PLACE OF ISSUANCE] 'SOLICITATION/AWARD ISSUED IN THE UNITED STATES (50 STATES PLUS DISTRICT OF COLUMBIA)'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198500, prescription_id FROM Prescriptions WHERE prescription_name IN ('4.1105(a)(1)'); -- [PLACE OF ISSUANCE] 'SOLICITATION/AWARD ISSUED IN THE UNITED STATES OUTLYING AREAS'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198501, prescription_id FROM Prescriptions WHERE prescription_name IN ('246.710(2)'); -- [PLACE OF ISSUANCE] 'SOLICITATION/AWARD ISSUED IN GERMANY'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198503, prescription_id FROM Prescriptions WHERE prescription_name IN ('223.570-2','4.404(a)'); -- [SECURITY REQUIREMENTS] 'CONTRACT REQUIRES ACCESS TO CLASSIFIED INFORMATION'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198504, prescription_id FROM Prescriptions WHERE prescription_name IN ('4.404(a)'); -- [SECURITY REQUIREMENTS] 'THE AGENCY IS COVERED BY THE NISP'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198505, prescription_id FROM Prescriptions WHERE prescription_name IN ('4.404(a)'); -- [SECURITY REQUIREMENTS] 'EMPLOYEE IDENTIFICATION IS REQUIRED FOR SECURITY REASONS'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198506, prescription_id FROM Prescriptions WHERE prescription_name IN ('204.404-70(a)'); -- [SECURITY REQUIREMENTS] 'THE AWARD INVOLVES UNCLASSIFIED BUT SENSITIVE INFORMATION'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198507, prescription_id FROM Prescriptions WHERE prescription_name IN ('4.1303'); -- [SECURITY REQUIREMENTS] 'CONTRACT PERFORMANCE REQUIRES CONTRACTOR TO HAVE ROUTINE PHYSICAL ACCESS TO A FEDERALLY-CONTROLLED FACILITY AND/OR ROUTINE ACCESS TO A FEDERALLY-CONTROLLED INFORMATION SYSTEM'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198508, prescription_id FROM Prescriptions WHERE prescription_name IN ('209.104-70'); -- [SECURITY REQUIREMENTS] 'ACCESS TO PROSCRIBED INFORMATION IS NECESSARY FOR CONTRACT PERFORMANCE'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198509, prescription_id FROM Prescriptions WHERE prescription_name IN ('4.1303'); -- [SECURITY REQUIREMENTS] 'CONTRACTOR ONLY REQUIRES INTERMITTENT ACCESS TO FEDERALLY CONTROLLED FACILITIES'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198511, prescription_id FROM Prescriptions WHERE prescription_name IN ('4.1705(a)'); -- [DOCUMENT SECURITY LEVEL] 'CONFIDENTIAL'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198512, prescription_id FROM Prescriptions WHERE prescription_name IN ('4.1705(a)'); -- [DOCUMENT SECURITY LEVEL] 'SECRET'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198513, prescription_id FROM Prescriptions WHERE prescription_name IN ('4.1705(a)'); -- [DOCUMENT SECURITY LEVEL] 'TOP SECRET'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198514, prescription_id FROM Prescriptions WHERE prescription_name IN ('4.1705(b)'); -- [DOCUMENT SECURITY LEVEL] 'UNCLASSIFIED'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198515, prescription_id FROM Prescriptions WHERE prescription_name IN ('223.7203'); -- [PURPOSE OF PROCUREMENT] 'FOR DOD USE'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198516, prescription_id FROM Prescriptions WHERE prescription_name IN ('2015-00016','225.302-6','225.371-5(a)','247.574(b)','247.574(b)(3)','25.301-4','25.302-6','4.1105(a)(1)','47.507(a)(3)'); -- [PURPOSE OF PROCUREMENT] 'FOR USE IN CONTINGENCY OPERATIONS'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198518, prescription_id FROM Prescriptions WHERE prescription_name IN ('2015-00003','225.371-5'); -- [PURPOSE OF PROCUREMENT] 'FOR USE IN SUPPORT OF OPERATION UNITED ASSISTANCE (OUA) IN THE USAFRICOM THEATRE OF OPERATIONS'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198519, prescription_id FROM Prescriptions WHERE prescription_name IN ('25.301-4','4.1105(a)(1)'); -- [PURPOSE OF PROCUREMENT] 'FOR USE IN SUPPORTING A DIPLOMATIC OR CONSULAR MISSION THAT HAS BEEN DESIGNATED AS A DANGER PAY POST BY THE DEPARTMENT OF STATE'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198520, prescription_id FROM Prescriptions WHERE prescription_name IN ('4.1105(a)(1)','49.504(c)(3)'); -- [PURPOSE OF PROCUREMENT] 'FOR USE IN EMERGENCY OPERATIONS'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198521, prescription_id FROM Prescriptions WHERE prescription_name IN ('25.302-6'); -- [PURPOSE OF PROCUREMENT] 'FOR USE IN INTELLIGENCE, OR CLASSIFIED PURPOSES OR IN INTEREST OF NATIONAL SECURITY'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198522, prescription_id FROM Prescriptions WHERE prescription_name IN ('225.1103(4)'); -- [PURPOSE OF PROCUREMENT] 'FOR USE IN SUPPORT OF TESTING'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198523, prescription_id FROM Prescriptions WHERE prescription_name IN ('225.371-5(a)','247.574(b)','247.574(b)(3)','25.301-4','4.1105(a)(1)'); -- [PURPOSE OF PROCUREMENT] 'FOR USE IN HUMANITARIAN OR PEACEKEEPING OPERATIONS'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198524, prescription_id FROM Prescriptions WHERE prescription_name IN ('47.507(a)(3)'); -- [PURPOSE OF PROCUREMENT] 'FOR USE IN CONNECTION WITH UNITED NATIONS OR NORTH ATLANTIC TREATY ORGANIZATION HUMANITARIAN OR PEACEKEEPING OPERATIONS'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198525, prescription_id FROM Prescriptions WHERE prescription_name IN ('25.302-6','4.1105(a)(1)'); -- [PURPOSE OF PROCUREMENT] 'FOR USE IN COMBAT AND OTHER SIGNIFICANT MILITARY OPERATIONS'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198526, prescription_id FROM Prescriptions WHERE prescription_name IN ('237.7603(b)'); -- [PURPOSE OF PROCUREMENT] 'FOR USE IN SUPPORT OF MISSION ESSENTIAL FUNCTIONS'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198527, prescription_id FROM Prescriptions WHERE prescription_name IN ('225.1101(1)(i)','225.1101(1)(ii)','225.1101(10)(i)','225.1101(10)(i)(A)','225.1101(10)(i)(C)','225.1101(10)(i)(D)','225.1101(2)(i)','225.1101(2)(ii)','225.1101(2)(iii)','225.1101(5)','225.1101(5)(ii)','225.1101(6)','225.7503(a)','225.7503(a)(2)','225.7503(b)','225.7503(b)(1)','225.7503(b)(2)','225.7503(b)(3)','225.7503(b)(4)','25.1101(6)(ii)'); -- [PURPOSE OF PROCUREMENT] 'FOR USE IN SUPPORT OF OPERATIONS IN AFGHANISTAN'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198528, prescription_id FROM Prescriptions WHERE prescription_name IN ('225.302-6','225.371-5(a)','247.574(b)','247.574(b)(1)','247.574(b)(2)','247.574(b)(3)','25.301-4','47.507(a)(3)'); -- [PURPOSE OF PROCUREMENT] 'FOR USE IN MILITARY OPERATIONS OR MILITARY EXERCISES DESIGNATED BY THE COMBATANT COMMANDER'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198529, prescription_id FROM Prescriptions WHERE prescription_name IN ('25.301-4'); -- [PURPOSE OF PROCUREMENT] 'FOR USE IN SUPPORTING A DIPLOMATIC OR CONSULAR MISSION'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198530, prescription_id FROM Prescriptions WHERE prescription_name IN ('217.208-70(a)'); -- [PURPOSE OF PROCUREMENT] 'FOR ESTABLISHMENT OR REPLENISHMENT OF DOD INVENTORIES OR STOCKS'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198531, prescription_id FROM Prescriptions WHERE prescription_name IN ('212.7103'); -- [PURPOSE OF PROCUREMENT] 'PROCURED UNDER A PILOT PROGRAM FOR THE ACQUISITION OF MILITARY- PURPOSE NONDEVELOPMENTAL ITEMS (SEE DFARS 212.7100)'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198532, prescription_id FROM Prescriptions WHERE prescription_name IN ('225.1103(4)'); -- [PURPOSE OF PROCUREMENT] 'FOR USE IN SUPPORT OF GATHERING INTELLIGENCE'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198533, prescription_id FROM Prescriptions WHERE prescription_name IN ('225.302-6'); -- [PURPOSE OF PROCUREMENT] 'FOR USE IN PEACE OPERATIONS, CONSISTENT WITH JOINT PUBLICATION 3-07.3'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198535, prescription_id FROM Prescriptions WHERE prescription_name IN ('225.7011-3'); -- [PLACE OF USE] 'FOR USE IN A GOVERNMENT OWNED OR DOD CONTROLLED FACILITY'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198536, prescription_id FROM Prescriptions WHERE prescription_name IN ('217.7303'); -- [PLACE OF USE] 'FOR USE IN UNITED STATES (50 STATES PLUS DISTRICT OF COLUMBIA)'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198537, prescription_id FROM Prescriptions WHERE prescription_name IN ('13.302-5(d)(3)(i)','25.1101(a)(1)','25.1101(b)(1)(i)','25.1101(b)(1)(ii)','25.1101(b)(1)(iii)','25.1101(b)(1)(iv)'); -- [PLACE OF USE] 'FOR USE INSIDE THE UNITED STATES (50 STATES, DISTRICT OF COLUMBIA, OR UNITED STATES OUTLYING AREAS)'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198538, prescription_id FROM Prescriptions WHERE prescription_name IN ('225.1103(1)','25.1101(a)(1)'); -- [PLACE OF USE] 'FOR USE OUTSIDE THE UNITED STATES (50 STATES, DISTRICT OF COLUMBIA, OR UNITED STATES OUTLYING AREAS)'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198539, prescription_id FROM Prescriptions WHERE prescription_name IN ('225.1103(4)'); -- [MISCELLANEOUS PROGRAM CHARACTERISTICS] 'THIS ACQUISITION IS CONNECTED WITH A US ARMED FORCES VISIT TO CHINA'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198540, prescription_id FROM Prescriptions WHERE prescription_name IN ('225.7017-4(a)'); -- [MISCELLANEOUS PROGRAM CHARACTERISTICS] 'THIS ACQUISITION MAY RESULT IN DOD OWNERSHIP OF PHOTOVOLTAIC DEVICES OTHER THAN END ITEMS'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198541, prescription_id FROM Prescriptions WHERE prescription_name IN ('7.305(c)'); -- [MISCELLANEOUS PROGRAM CHARACTERISTICS] 'THE EFFORT UNDER THE ACQUISITION INVOLVES A CONVERSION OF WORK FROM IN-HOUSE TO CONTRACT'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198542, prescription_id FROM Prescriptions WHERE prescription_name IN ('22.1207'); -- [MISCELLANEOUS PROGRAM CHARACTERISTICS] 'THIS ACQUISITION IS A SUCCEEDING CONTRACT FOR PERFORMANCE OF THE SAME OR SIMILAR WORK AT THE SAME OR SIMILAR LOCATION'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198543, prescription_id FROM Prescriptions WHERE prescription_name IN ('225.1101(6)','225.1101(6)(i)','225.1101(6)(ii)','25.1101(c)(1)'); -- [MISCELLANEOUS PROGRAM CHARACTERISTICS] 'THIS ACQUISITION IS COVERED BY THE WORLD TRADE ORGANIZATION (WTO) AGREEMENT ON GOVERNMENT PROCUREMENT (GPA)'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198544, prescription_id FROM Prescriptions WHERE prescription_name IN ('3.1004(b)'); -- [MISCELLANEOUS PROGRAM CHARACTERISTICS] 'THE GOVERNMENT AGENCY HAS A FRAUD HOTLINE POSTER'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198545, prescription_id FROM Prescriptions WHERE prescription_name IN ('32.1110(e)'); -- [MISCELLANEOUS PROGRAM CHARACTERISTICS] 'THE CONTRACT WILL PROVIDE FOR USE OF DELIVERY ORDERS'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198546, prescription_id FROM Prescriptions WHERE prescription_name IN ('217.7406(b)','44.204(a)(1)','44.204(a)(3)'); -- [MISCELLANEOUS PROGRAM CHARACTERISTICS] 'THE CONTRACT WILL PROVIDE FOR USE OF UNDEFINITIZED CONTRACT ACTIONS'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198547, prescription_id FROM Prescriptions WHERE prescription_name IN ('16.506(d)(2)'); -- [MISCELLANEOUS PROGRAM CHARACTERISTICS] 'THE ACQUISITION COVERS ESTIMATED REQUIREMENTS THAT EXCEED A SPECIFIC GOVERNMENT ACTIVITY'S INTERNAL CAPABILITY TO PRODUCE OR PERFORM'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198548, prescription_id FROM Prescriptions WHERE prescription_name IN ('22.1705(a)(2)'); -- [MISCELLANEOUS PROGRAM CHARACTERISTICS] 'U.S. DIRECTIVES/NOTICES REGARDING COMBATING TRAFFICKING IN PERSONS THAT APPLY TO CONTRACTOR EMPLOYEES AT THE CONTRACT PLACE OF PERFORMANCE HAS BEEN RECEIVED'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198551, prescription_id FROM Prescriptions WHERE prescription_name IN ('50.206(a)'); -- [SAFETY ACT APPLICABILITY] 'THE AGENCY CONSULTED WITH DHS ON A QUESTIONABLE CASE OF SAFETY ACT APPLICABILITY TO AN ACQUISITION IN ACCORDANCE WITH 50.205-1(a), AND AFTER THE CONSULTATION, THE AGENCY HAS DETERMINED THAT SAFETY ACT PROTECTION IS NOT APPLICABLE FOR THE ACQUISITION'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198554, prescription_id FROM Prescriptions WHERE prescription_name IN ('19.708(b)(1)(iii)','4.1105(a)(1)','4.1403(a)'); -- [CAR] 'NO'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198555, prescription_id FROM Prescriptions WHERE prescription_name IN ('4.1105(a)(1)'); -- [SAM EXEMPTION] 'YES'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198557, prescription_id FROM Prescriptions WHERE prescription_name IN ('50.206(a)'); -- [SAFETY ACT STATEMENTS] 'DHS DENIED APPROVAL OF PRE-APPLICATION DESIGNATION NOTICE'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198558, prescription_id FROM Prescriptions WHERE prescription_name IN ('50.206(d)'); -- [SAFETY ACT STATEMENTS] 'DHS HAS NOT ISSUED A SAFETY ACT DESIGNATION OR CERTIFICATION TO SUCCESSFUL OFFEROR BEFORE CONTRACT AWARD'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198559, prescription_id FROM Prescriptions WHERE prescription_name IN ('50.206(c)(1)','50.206(c)(2)','50.206(c)(3)'); -- [SAFETY ACT STATEMENTS] 'DHS HAS ISSUED A PRE-QUALIFICATION DESIGNATION NOTICE'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198560, prescription_id FROM Prescriptions WHERE prescription_name IN ('50.206(b)(1)'); -- [SAFETY ACT STATEMENTS] 'DHS HAS ISSUED A BLOCK DESIGNATION/CERTIFICATION FOR SOLICITED TECHNOLOGIES'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198561, prescription_id FROM Prescriptions WHERE prescription_name IN ('50.206(b)(2)','50.206(c)(2)'); -- [SAFETY ACT STATEMENTS] 'OFFERS ARE CONTINGENT UPON SAFETY ACT DESIGNATION BEING AUTHORIZED'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198562, prescription_id FROM Prescriptions WHERE prescription_name IN ('50.206(b)(3)','50.206(c)(3)'); -- [SAFETY ACT STATEMENTS] 'OFFERS PRESUMING SAFETY ACT DESIGNATION OR CERTIFICATION AUTHORIZATION'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198566, prescription_id FROM Prescriptions WHERE prescription_name IN ('225.1103(1)'); -- [BUSINESS TYPE] 'FOREIGN SOURCE'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198571, prescription_id FROM Prescriptions WHERE prescription_name IN ('4.1705(a)','4.1705(b)','4.607(b)'); -- [GENERIC DUNS] 'YES'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198573, prescription_id FROM Prescriptions WHERE prescription_name IN ('19.1309(a)(1)','19.1309(b)(1)'); -- [HUBZONE 50% WAIVER] 'YES'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198575, prescription_id FROM Prescriptions WHERE prescription_name IN ('25.1103(b)'); -- [TRANSLATION TO ANOTHER LANGUAGE] 'YES'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198577, prescription_id FROM Prescriptions WHERE prescription_name IN ('9.104-7(c)(1)'); -- [ACTIVE FEDERAL CONTRACTS] 'YES'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198580, prescription_id FROM Prescriptions WHERE prescription_name IN ('22.810(g)'); -- [CONTRACTOR PERFORM ON BEHALF OF FOREIGN COUNTRY] 'YES'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198583, prescription_id FROM Prescriptions WHERE prescription_name IN ('14.201-6(i)'); -- [FORMS] 'SF 1447'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198584, prescription_id FROM Prescriptions WHERE prescription_name IN ('16.603-4(b)(1)'); -- [FORMS] 'SF 26'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198585, prescription_id FROM Prescriptions WHERE prescription_name IN ('14.201-6(i)'); -- [FORMS] 'SF 33'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198589, prescription_id FROM Prescriptions WHERE prescription_name IN ('11.304','11.503(a)','11.703(a)','11.703(b)','13.302-5(d)(3)(i)','19.508(e)','19.811-3(e)','213.106-2-70','216.203-4-70(b)','216.203-4-70(c)(1)','217.7303','22.1505(b)','22.305','223.7306','225.1101(6)','225.1103(1)','225.7307(b)','226.104','247.305-70','247.574(b)','247.574(b)(1)','247.574(b)(2)','25.1101(6)(ii)','25.1101(a)(1)','25.1101(a)(2)','25.1101(b)(1)(i)','25.1101(b)(1)(ii)','25.1101(b)(1)(iii)','25.1101(b)(1)(iv)','25.1101(b)(2)(i)','25.1101(b)(2)(ii)','25.1101(b)(2)(iii)','25.1101(b)(2)(iv)','25.1101(c)(1)','25.1101(c)(2)','25.1101(d)','25.1101(e)','25.1101(f)','26.304','28.204-4','32.111(a)(1)','32.111(b)(1)','32.111(b)(2)','32.111(c)(2)','32.1110(b)(2)','42.1305(b)(1)','42.1305(b)(2)','43.107','43.205(b)(1)','43.205(f)','46.301','46.302','46.303','46.315','46.316','46.710(a)(1)','46.710(a)(3)','46.710(a)(4)','46.710(a)(5)','46.710(a)(6)','46.710(b)(1)','46.710(b)(2)','46.710(b)(3)','46.710(b)(4)','46.710(c)(1)','46.710(c)(2)','46.710(c)(3)','46.710(c)(4)','46.805','46.805(a)','47.305-15(a)(2)','47.305-4(c)','47.305-9(b)(1)','49.505(b)','7.203','8.005','9.108-5(a)','9.108-5(b)'); -- [SUPPLIES OR SERVICES] 'SUPPLIES'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198590, prescription_id FROM Prescriptions WHERE prescription_name IN ('11.503(a)','11.703(a)','17.208(d)','17.208(e)','17.208(f)','19.508(e)','19.811-3','216.203-4-70(c)(1)','22.1006(c)(1)','22.1006(c)(2)','22.1006(e)(2)','22.1006(e)(4)','22.1103','22.1207','222.7004','222.7201(c)','225.1103(1)','225.371-5','226.104','228.370(e)','23.406(c)','23.804(b)','237.171-4','237.173-5','237.7603(b)','239.7411(b)','246.370','25.110','28.204-4','29.401-4(b)','32.111(a)(1)','32.111(b)(1)','32.111(c)(2)','32.1110(b)(2)','32.706-1(b)','32.908(c)(3)','37.110(a)','37.110(b)','37.110(c)','37.116-2','4.1705(a)','4.1705(b)','42.1305(b)(1)','42.1305(b)(2)','43.205(a)(2)','43.205(a)(3)','43.205(b)(2)','43.205(b)(3)','46.301','46.304','46.305','46.315','46.710(c)(1)','46.710(c)(2)','46.710(c)(3)','46.710(c)(4)','46.710(d)','46.805(a)(4)','49.502(c)','49.505(b)','8.005','9.108-5(a)','9.108-5(b)'); -- [SUPPLIES OR SERVICES] 'SERVICES'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198591, prescription_id FROM Prescriptions WHERE prescription_name IN ('209.570-4(a)','209.570-4(b)','211.002-70','34.104'); -- [TYPE OF SYSTEM] 'MAJOR SYSTEM'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198592, prescription_id FROM Prescriptions WHERE prescription_name IN ('225.7003-5(a)(2)','43.107'); -- [TYPE OF SYSTEM] 'MAJOR WEAPON SYSTEMS OR PRINCIPAL SUBSYSTEMS'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198593, prescription_id FROM Prescriptions WHERE prescription_name IN ('46.710(c)(1)','46.710(c)(2)','46.710(c)(3)','46.710(c)(4)'); -- [TYPE OF SYSTEM] 'SYSTEMS AND EQUIPMENT'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198594, prescription_id FROM Prescriptions WHERE prescription_name IN ('246.371(a)'); -- [TYPE OF SYSTEM] '(SUB) SYSTEMS AND (SUB) ASSEMBLIES INTEGRAL TO A SYSTEM'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198595, prescription_id FROM Prescriptions WHERE prescription_name IN ('209.571-8(b)'); -- [TYPE OF SYSTEM] 'PRE-MAJOR DEFENSE ACQUISITION PROGRAM'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198596, prescription_id FROM Prescriptions WHERE prescription_name IN ('204.470-3','209.571-8(b)','234.7101(b)','234.7101(b)(1)','234.7101(b)(2)','249.7003(c)'); -- [TYPE OF SYSTEM] 'MAJOR DEFENSE PROGRAM'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198597, prescription_id FROM Prescriptions WHERE prescription_name IN ('234.7101(b)','234.7101(b)(1)','234.7101(b)(2)'); -- [TYPE OF SYSTEM] 'MAJOR INFORMATION SYSTEMS PROGRAM'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198601, prescription_id FROM Prescriptions WHERE prescription_name IN ('12.301(b)(3)','16.307(a)','16.307(a)(2)','16.307(a)(3)','16.307(a)(4)','16.307(a)(5)','16.601(f)(1)','16.601(f)(2)','16.601(f)(3)','216.601(e)','22.1006(c)(1)','22.1006(c)(2)','242.7503','32.111(a)(7)','32.1110(b)(2)','37.115-3','43.205(c)','44.204(a)(1)','44.204(a)(3)','44.204(c)','45.107(a)','45.107(a)(2)','45.107(a)(3)','46.306','49.503(a)(4)','49.505(b)'); -- [CONTRACT TYPE] 'LABOR HOUR'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198602, prescription_id FROM Prescriptions WHERE prescription_name IN ('12.301(b)(3)','16.307(a)','16.307(a)(2)','16.307(a)(3)','16.307(a)(4)','16.307(a)(5)','16.601(f)(1)','16.601(f)(2)','16.601(f)(3)','216.601(e)','22.1006(c)(1)','22.1006(c)(2)','242.7503','32.111(a)(7)','32.111(b)(2)','43.205(c)','44.204(a)(1)','44.204(a)(3)','44.204(c)','46.306','49.503(a)(4)','49.505(b)'); -- [CONTRACT TYPE] 'TIME AND MATERIALS'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198603, prescription_id FROM Prescriptions WHERE prescription_name IN ('11.703(a)'); -- [QUANTITY VARIATION] 'VARIATION IN QUANTITY'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198604, prescription_id FROM Prescriptions WHERE prescription_name IN ('11.703(c)'); -- [QUANTITY VARIATION] 'VARIATION IN ESTIMATED QUANTITY'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198606, prescription_id FROM Prescriptions WHERE prescription_name IN ('16.406(a)','16.406(b)','17.208(a)','17.208(b)','17.208(c)','17.208(d)','17.208(e)','17.208(f)','17.208(g)','22.1006(c)(1)','22.1006(c)(2)','22.407(e)','22.407(f)','22.407(g)'); -- [OPTIONS] 'YES'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198608, prescription_id FROM Prescriptions WHERE prescription_name IN ('23.705(b)(1)','23.705(d)(2)'); -- [EPEAT SILVER/GOLD] 'YES'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198610, prescription_id FROM Prescriptions WHERE prescription_name IN ('36.521'); -- [REPORTS AND DRAWINGS] 'REPRODUCIBLE SHOP DRAWINGS ARE REQUIRED'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198611, prescription_id FROM Prescriptions WHERE prescription_name IN ('36.521'); -- [REPORTS AND DRAWINGS] 'RECORD DRAWINGS ARE REQUIRED'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198612, prescription_id FROM Prescriptions WHERE prescription_name IN ('53.111'); -- [REPORTS AND DRAWINGS] 'CONTRACTOR IS REQUIRED TO SUBMIT DATA ON STANDARD OR OPTIONAL FORMS'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198613, prescription_id FROM Prescriptions WHERE prescription_name IN ('42.1107(a)'); -- [REPORTS AND DRAWINGS] 'PRODUCTION PROGRESS REPORTS ARE REQUIRED'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198614, prescription_id FROM Prescriptions WHERE prescription_name IN ('246.370'); -- [REPORTS AND DRAWINGS] 'SCIENTIFIC OR TECHNICAL REPORTS ARE REQUIRED'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198616, prescription_id FROM Prescriptions WHERE prescription_name IN ('16.506(d)(3)','16.506(d)(5)'); -- [BRAND NAME BASIS] 'YES'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198618, prescription_id FROM Prescriptions WHERE prescription_name IN ('223.303'); -- [HAZARDOUS DATA SHEETS] 'YES'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198620, prescription_id FROM Prescriptions WHERE prescription_name IN ('22.305','46.304','46.305'); -- [INCLUSION OF SERVICES] 'YES'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198622, prescription_id FROM Prescriptions WHERE prescription_name IN ('10.003','11.304','12.301(b)(1)','12.301(b)(2)','12.301(b)(3)','12.301(b)(4)','12.301(b)(4)(i)','12.301(b)(4)(ii)','12.301(c)(1)','13.302-5(d)','15.209(b)(1)','15.209(b)(3)','15.209(b)(4)','15.408(f)(1)','15.408(f)(2)','15.408(n)(2)','15.408(n)(2)(iii)','16.307(a)','16.307(a)(2)','16.307(a)(3)','16.307(a)(4)','16.307(a)(5)','16.601(f)(1)','16.601(f)(2)','16.601(f)(3)','2013-00019','203.1004(b)(2)(ii)','203.570-3','204.7109(b)','216.601(e)','217.7303','22.1310(c)','22.1505(a)','22.305','222.7405','223.570-2','223.7203','225.1101(10)(i)','225.1101(10)(i)(A)','225.1101(10)(i)(B)','225.1101(10)(i)(C)','225.1101(10)(i)(D)','225.1101(10)(i)(E)','225.1101(10)(i)(F)','225.7009-5','225.7204(a)','225.7204(b)','228.370(b)','23.505','242.7204','247.574(b)','247.574(b)(2)','247.574(b)(3)','25.1101(b)(1)(i)','25.1101(b)(1)(ii)','25.1101(b)(1)(iii)','25.1101(b)(1)(iv)','25.1103(d)','26.206(a)','28.106-4(b)','3.1004(b)','3.104-9(a)','3.104-9(b)','3.404','3.502-3','3.503-2','32.111(a)(1)','32.206(g)','4.1202(a)','42.1305(c)','42.709-6','44.403','47.405','47.507(a)(3)','48.201','48.201(c)','48.201(d)','48.201(e)(1)'); -- [COMMERCIAL ITEMS] 'THE SUPPLIES ARE COMMERCIAL ITEMS'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198624, prescription_id FROM Prescriptions WHERE prescription_name IN ('225.7003-5(a)(2)'); -- [MATERIAL CONTENT] 'THE ITEMS CONTAIN SPECIALTY METALS'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198625, prescription_id FROM Prescriptions WHERE prescription_name IN ('23.804(a)'); -- [MATERIAL CONTENT] 'THE ITEMS CONTAIN OZONE DEPLETING SUBSTANCES'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198626, prescription_id FROM Prescriptions WHERE prescription_name IN ('225.7009-5'); -- [MATERIAL CONTENT] 'THE ITEMS CONTAINS BALL OR ROLLER BEARINGS'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198627, prescription_id FROM Prescriptions WHERE prescription_name IN ('216.203-4-70(b)'); -- [MATERIAL CONTENT] 'THE ITEMS ARE NON-STANDARD STEEL ITEMS MADE WITH STANDARD STEEL'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198628, prescription_id FROM Prescriptions WHERE prescription_name IN ('246.870-3'); -- [MATERIAL CONTENT] 'END ITEMS, COMPONENTS, PARTS OR ASSEMBLIES CONTAINING ELECTRONIC PARTS'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198629, prescription_id FROM Prescriptions WHERE prescription_name IN ('208.7305(a)'); -- [MATERIAL CONTENT] 'THE ITEMS DO NOT REQUIRE PRECIOUS METALS IN THEIR MANUFACTURE'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198630, prescription_id FROM Prescriptions WHERE prescription_name IN ('23.602'); -- [MATERIAL CONTENT] 'THE SUPPLIES ARE/CONTAIN RADIOACTIVE MATERIAL NOT REQUIRING LICENSING IN WHICH THE ACTIVITY IS GREATER THAN .002 MICROCURIES PER GRAM OR THE ACTIVITY PER ITEM EQUALS OF EXCEEDS 0.01 MICROCURIES'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198631, prescription_id FROM Prescriptions WHERE prescription_name IN ('23.602'); -- [MATERIAL CONTENT] 'THE SUPPLIES CONTAIN RADIOACTIVE MATERIAL REQUIRING SPECIFIC LICENSING UNDER REGULATIONS ISSUED PURSUANT TO THE ATOMIC ENERGY ACT OF 1954'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198633, prescription_id FROM Prescriptions WHERE prescription_name IN ('25.1101(f)','46.307(a)','46.308'); -- [SUPPLY CLASSIFICATION] 'MANUFACTURED END PRODUCTS'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198634, prescription_id FROM Prescriptions WHERE prescription_name IN ('47.305-9(b)(1)'); -- [SUPPLY CLASSIFICATION] 'ITEM NEW TO THE SUPPLY SYSTEM'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198635, prescription_id FROM Prescriptions WHERE prescription_name IN ('209.270-5','246.371(a)'); -- [SUPPLY CLASSIFICATION] 'CRITICAL SAFETY ITEM'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198636, prescription_id FROM Prescriptions WHERE prescription_name IN ('16.203-4(a)'); -- [SUPPLY CLASSIFICATION] 'STANDARD SUPPLIES'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198637, prescription_id FROM Prescriptions WHERE prescription_name IN ('22.202'); -- [SUPPLY CLASSIFICATION] 'FINISHED SUPPLIES THAT CAN BE SUPPLIED IN THE OPEN MARKET OR FROM EXISTING STOCK'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198638, prescription_id FROM Prescriptions WHERE prescription_name IN ('16.203-4(b)'); -- [SUPPLY CLASSIFICATION] 'SEMI-STANDARD SUPPLIES'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198639, prescription_id FROM Prescriptions WHERE prescription_name IN ('47.305-9(b)(1)'); -- [SUPPLY CLASSIFICATION] 'NONSTANDARD SUPPLIES'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198640, prescription_id FROM Prescriptions WHERE prescription_name IN ('23.406(c)'); -- [SUPPLY CLASSIFICATION] 'EPA-DESIGNATED ITEMS'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198641, prescription_id FROM Prescriptions WHERE prescription_name IN ('23.406(d)'); -- [SUPPLY CLASSIFICATION] 'EPA-DESIGNATED ITEM(S) CONTAINING RECOVERED MATERIALS'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198642, prescription_id FROM Prescriptions WHERE prescription_name IN ('23.206'); -- [SUPPLY CLASSIFICATION] 'LISTED IN THE ENERGY STAR PROGRAM OR FEDERAL EMERGENCY MANAGEMENT PROGRAM (FEMP)'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198643, prescription_id FROM Prescriptions WHERE prescription_name IN ('211.273-4'); -- [SUPPLY CLASSIFICATION] 'PREVIOUSLY DEVELOPED ITEMS'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198644, prescription_id FROM Prescriptions WHERE prescription_name IN ('47.305-9(b)(1)'); -- [SUPPLY CLASSIFICATION] 'MODIFICATIONS OF PREVIOUSLY SHIPPED ITEMS'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198645, prescription_id FROM Prescriptions WHERE prescription_name IN ('2013-00017'); -- [SUPPLY CLASSIFICATION] 'COMMODITIES'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198646, prescription_id FROM Prescriptions WHERE prescription_name IN ('11.107(a)'); -- [SUPPLY CLASSIFICATION] 'BRAND NAME OR EQUAL'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198647, prescription_id FROM Prescriptions WHERE prescription_name IN ('204.7109(b)'); -- [SUPPLY CLASSIFICATION] 'INITIAL PROVISIONING SPARES'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198648, prescription_id FROM Prescriptions WHERE prescription_name IN ('23.406(a)','23.406(b)'); -- [SUPPLY CLASSIFICATION] '"USDA-DESIGNATED" ITEM'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198650, prescription_id FROM Prescriptions WHERE prescription_name IN ('23.406(d)'); -- [RECOVERED MATERIAL ESTIMATES] 'YES'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198652, prescription_id FROM Prescriptions WHERE prescription_name IN ('225.7902-5(b)'); -- [TREATY ELIGIBLE SUPPLY] 'YES'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198654, prescription_id FROM Prescriptions WHERE prescription_name IN ('217.7005'); -- [PROPERTY FOR EXCHANGE] 'YES'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198656, prescription_id FROM Prescriptions WHERE prescription_name IN ('13.302-5(d)(3)(i)'); -- [TRADE AGREEMENT APPLICABILITY] 'YES'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198658, prescription_id FROM Prescriptions WHERE prescription_name IN ('36.609-1(c)'); -- [A&E CONDITIONS] 'DESIGN COST LIMITATIONS ARE SECONDARY TO PERFORMANCE CONSIDERATIONS AND ADDITIONAL PROJECT FUNDING CAN BE EXPECTED'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198659, prescription_id FROM Prescriptions WHERE prescription_name IN ('36.609-1(c)'); -- [A&E CONDITIONS] 'THE DESIGN IS FOR A STANDARD STRUCTURE AND IS NOT INTENDED FOR A SPECIFIC LOCATION'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198660, prescription_id FROM Prescriptions WHERE prescription_name IN ('36.609-1(c)'); -- [A&E CONDITIONS] 'THE EFFORT INVOLVES LITTLE OR NO DESIGN EFFORT'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198661, prescription_id FROM Prescriptions WHERE prescription_name IN ('36.609-4'); -- [A&E CONDITIONS] 'REGISTRATION OF DESIGNERS IS REQUIRED'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198663, prescription_id FROM Prescriptions WHERE prescription_name IN ('236.609-70'); -- [CONSTRUCTION CONDITIONS] 'CONSTRUCTION REQUIRES A&E SUPERVISION/INSPECTION'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198664, prescription_id FROM Prescriptions WHERE prescription_name IN ('36.516'); -- [CONSTRUCTION CONDITIONS] 'THE CONTRACT WILL PROVIDE FOR UNIT PRICING OF ITEMS AND FOR PAYMENT BASED ON QUANTITY SURVEYS'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198665, prescription_id FROM Prescriptions WHERE prescription_name IN ('236.570(b)(2)(i)'); -- [CONSTRUCTION CONDITIONS] 'CONSTRUCTION WILL INVOLVE MAJOR ITEMS OF PLANT AND EQUIPMENT'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198666, prescription_id FROM Prescriptions WHERE prescription_name IN ('36.517'); -- [CONSTRUCTION CONDITIONS] 'THERE IS A NEED FOR ACCURATE WORK LAYOUT AND FOR SITING VERIFICATION DURING WORK PERFORMANCE'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198667, prescription_id FROM Prescriptions WHERE prescription_name IN ('236.570(b)(2)(ii)'); -- [CONSTRUCTION CONDITIONS] 'THE ACQUISITION INCLUDES A MAJOR MOBILIZATION EXPENSE DUE TO LOCATION NATURE OF WORK'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198668, prescription_id FROM Prescriptions WHERE prescription_name IN ('236.570(b)(2)(i)','236.570(b)(2)(ii)'); -- [CONSTRUCTION CONDITIONS] 'THE SOLICITATION/AWARD INCLUDES A SEPARATE LINE ITEM FOR MOBILIZATION AND PREP WORK'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198669, prescription_id FROM Prescriptions WHERE prescription_name IN ('236.570(b)(6)'); -- [CONSTRUCTION CONDITIONS] 'THE SOLICITATION/AWARD WILL CONTAIN UNIT PRICES FOR ONLY SOME ITEMS'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198670, prescription_id FROM Prescriptions WHERE prescription_name IN ('36.516'); -- [CONSTRUCTION CONDITIONS] 'IT IS IMPRATICABLE FOR GOVERNMENT PERSONNEL TO PERFORM THE ORIGINAL AND FINAL SURVEYS AND THE GOVERNMENT WISHES THE CONTRACTOR TO PERFORM THESE SURVEYS'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198671, prescription_id FROM Prescriptions WHERE prescription_name IN ('36.513'); -- [CONSTRUCTION CONDITIONS] 'THE CONTRACT WILL INVOLVE WORK OF A LONG DURATION OR HAZARDOUS NATURE'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198672, prescription_id FROM Prescriptions WHERE prescription_name IN ('36.504'); -- [CONSTRUCTION CONDITIONS] 'PHYSICAL DATA WILL BE FURNISHED OR MADE AVAILABLE TO OFFERORS/CONTRACTORS'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198674, prescription_id FROM Prescriptions WHERE prescription_name IN ('36.522'); -- [PRE-CONSTRUCTION CONFERENCE] 'YES'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198676, prescription_id FROM Prescriptions WHERE prescription_name IN ('36.523'); -- [SITE VISIT] 'YES'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198678, prescription_id FROM Prescriptions WHERE prescription_name IN ('22.407(g)'); -- [CONSTRUCTION PRICING] 'ACTUAL METHOD'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198679, prescription_id FROM Prescriptions WHERE prescription_name IN ('22.407(e)'); -- [CONSTRUCTION PRICING] 'NONE OR SEPARATELY SPECIFIED'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198680, prescription_id FROM Prescriptions WHERE prescription_name IN ('22.407(f)'); -- [CONSTRUCTION PRICING] 'PERCENTAGE METHOD'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198681, prescription_id FROM Prescriptions WHERE prescription_name IN ('32.111(a)(6)'); -- [COMMUNICATION CONDITIONS] 'COMMON CARRIER'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198682, prescription_id FROM Prescriptions WHERE prescription_name IN ('239.7411(b)'); -- [COMMUNICATION CONDITIONS] 'INCLUDES SPECIAL CONSTRUCTION'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198683, prescription_id FROM Prescriptions WHERE prescription_name IN ('32.111(a)(1)','32.111(a)(6)'); -- [COMMUNICATION CONDITIONS] 'THE COMMUNICATION SERVICE IS REGULATED'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198684, prescription_id FROM Prescriptions WHERE prescription_name IN ('239.7411(d)'); -- [COMMUNICATION CONDITIONS] 'SECURE TELECOMMUNICATIONS REQUIRED'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198685, prescription_id FROM Prescriptions WHERE prescription_name IN ('241.501-70(a)'); -- [COMMUNICATION CONDITIONS] 'SUPERSEDING CONTRACT REQUIRED AND CAPITAL CREDITS, CONNECTION CHARGE CREDITS OR TERMINATION LIABILITY EXIST'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198687, prescription_id FROM Prescriptions WHERE prescription_name IN ('37.304(b)','37.304(c)'); -- [DISMANTLING PAYMENT ARRANGEMENT] 'IT IS ADVANTAGEOUS TO GOVERNMENT FOR CONTRACTOR TO PAY IN INCREMENTS AND TRANSFER TITLE IN INCREMENTS'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198688, prescription_id FROM Prescriptions WHERE prescription_name IN ('37.304(a)'); -- [DISMANTLING PAYMENT ARRANGEMENT] 'ALL MATERIAL IS TO BE RETAINED BY GOVERNMENT'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198689, prescription_id FROM Prescriptions WHERE prescription_name IN ('37.304(b)','37.304(c)'); -- [DISMANTLING PAYMENT ARRANGEMENT] 'CONTRACTOR IS TO RECEIVE TITLE TO DISMANTLED OR DEMOLISHED PROPERTY AND A NET AMOUNT OF COMPENSATION IS DUE TO THE GOVERNMENT'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198690, prescription_id FROM Prescriptions WHERE prescription_name IN ('37.304(a)'); -- [DISMANTLING PAYMENT ARRANGEMENT] 'GOVERNMENT SHALL MAKE PAYMENT TO CONTRACTOR IN ADDITION TO ANY TITLE TO PROPERTY THE CONTRACTOR MAY RECEIVE'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198692, prescription_id FROM Prescriptions WHERE prescription_name IN ('237.7101(e)','237.7101(e)(3)'); -- [LAUNDRY AND DRY CLEANING] 'LAUNDRY (BAG TYPE)'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198693, prescription_id FROM Prescriptions WHERE prescription_name IN ('237.7101(e)','237.7101(e)(2)'); -- [LAUNDRY AND DRY CLEANING] 'LAUNDRY (UNSORTED)'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198694, prescription_id FROM Prescriptions WHERE prescription_name IN ('237.7101(b)','237.7101(d)','237.7101(e)','237.7101(e)(3)'); -- [LAUNDRY AND DRY CLEANING] 'LAUNDRY AND DRY CLEANING (BULK WEIGHT BASIS)'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198695, prescription_id FROM Prescriptions WHERE prescription_name IN ('237.7101(a)','237.7101(c)'); -- [LAUNDRY AND DRY CLEANING] 'LAUNDRY AND DRY CLEANING (COUNT BASIS)'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198696, prescription_id FROM Prescriptions WHERE prescription_name IN ('237.7101(f)'); -- [LAUNDRY AND DRY CLEANING] 'LAUNDRY AND DRY CLEANING FOR INDIVIDUAL PERSONNEL'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198697, prescription_id FROM Prescriptions WHERE prescription_name IN ('204.470-3'); -- [R&D TECHNOLOGIES] 'YES'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198699, prescription_id FROM Prescriptions WHERE prescription_name IN ('16.307(e)(2)','16.307(f)(2)'); -- [WITHHOLDING ALLOWABLE COST] 'YES'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198702, prescription_id FROM Prescriptions WHERE prescription_name IN ('246.370'); -- [SEPARATELY PRICED ITEMS] 'NO'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198703, prescription_id FROM Prescriptions WHERE prescription_name IN ('237-7003(b)'); -- [PORT OF ENTRY REQUIREMENTS] 'YES'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198705, prescription_id FROM Prescriptions WHERE prescription_name IN ('47.207-1(a)','47.207-1(b)(2)'); -- [INTRASTATE MOVEMENT] 'YES'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198707, prescription_id FROM Prescriptions WHERE prescription_name IN ('46.314'); -- [BILLS OF LADING] 'YES'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198709, prescription_id FROM Prescriptions WHERE prescription_name IN ('41.501(d)(5)'); -- [UTILITY SERVICE CONDITIONS] 'ALTERNATIVE SERVICE LOCATIONS ARE POSSIBLE, EXCEPT UNDER AREA WIDE CONTRACTS'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198710, prescription_id FROM Prescriptions WHERE prescription_name IN ('41.501(d)(7)'); -- [UTILITY SERVICE CONDITIONS] 'THE FEDERAL GOVERNMENT IS A MEMBER OF A COOPERATIVE AND IS ENTITLED TO CAPITAL CREDITS'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198711, prescription_id FROM Prescriptions WHERE prescription_name IN ('41.501(d)(6)'); -- [UTILITY SERVICE CONDITIONS] 'THE GOVERNMENT IS REQUIRED TO PAY A NONREFUNDABLE, NONRECURRING MEMBERSHIP FEE, A CHARGE FOR INITIATION OF SERVICE, OR A CONTRIBUTION FOR THE COST OF FACILITIES CONSTRUCTION'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198712, prescription_id FROM Prescriptions WHERE prescription_name IN ('41.501(d)(4)'); -- [UTILITY SERVICE CONDITIONS] 'PAYMENT IS TO BE MADE TO THE CONTRACTOR UPON TERMINATION OF SERVICE IN CONJUNCTION WITH OR IN LIEU OF A CONNECTION CHARGE UPON COMPLETION OF THE FACILITIES'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198713, prescription_id FROM Prescriptions WHERE prescription_name IN ('41.501(b)'); -- [UTILITY SERVICE CONDITIONS] 'PROPOSALS FROM ALTERNATIVE ELECTRIC SUPPLIERS ARE BEING SOUGHT'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198714, prescription_id FROM Prescriptions WHERE prescription_name IN ('41.501(d)(3)'); -- [UTILITY SERVICE CONDITIONS] 'A REFUNDABLE CONNECTION CHARGE IS REQUIRED TO BE PAID BY THE GOVERNMENT'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198715, prescription_id FROM Prescriptions WHERE prescription_name IN ('41.501(d)(3)'); -- [UTILITY SERVICE CONDITIONS] 'A NONREFUNDABLE CONNECTION CHARGE IS REQUIRED TO BE PAID BY THE GOVERNMENT'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198716, prescription_id FROM Prescriptions WHERE prescription_name IN ('3.103-1','41.501(d)(1)','41.501(d)(2)'); -- [UTILITY SERVICE CONDITIONS] 'REGULATED'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198717, prescription_id FROM Prescriptions WHERE prescription_name IN ('15.209(b)(1)','15.209(b)(3)','15.209(b)(4)'); -- [UTILITY SERVICE CONDITIONS] 'UTILITY SERVICE RATES DO NOT EXCEED THOSE ESTABLISHED TO APPLY UNIFORMLY TO THE GENERAL PUBLIC, PLUS ANY APPLICABLE REASONABLE CONNECTION CHARGE'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198718, prescription_id FROM Prescriptions WHERE prescription_name IN ('36.514'); -- [UTILITY SERVICE CONDITIONS] 'THE EXISTING UTILITY SYSTEM(S) IS ADEQUATE AND IN THE BEST INTEREST OF THE GOVERNMENT'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198720, prescription_id FROM Prescriptions WHERE prescription_name IN ('11.703(a)','15.408(f)(1)','15.408(f)(2)','16.506(d)(2)','211.274-6(a)(1)','246.370','25.1101(a)(1)','25.1101(a)(2)','25.1101(b)(1)(i)','25.1101(b)(1)(ii)','25.1101(b)(1)(iii)','25.1101(b)(1)(iv)','25.1101(b)(2)(i)','25.1101(b)(2)(ii)','25.1101(b)(2)(iii)','25.1101(b)(2)(iv)','25.1101(c)(1)','25.1101(c)(2)','25.1101(d)','25.1101(e)','25.1101(f)','43.205(a)(2)','43.205(a)(3)','43.205(b)(2)','43.205(b)(3)','46.302','46.303','46.316'); -- [SERVICE CHARACTERISTICS] 'THE SERVICE BEING PROCURED INCLUDES THE FURNISHING OF SUPPLIES'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198721, prescription_id FROM Prescriptions WHERE prescription_name IN ('3.1106'); -- [SERVICE CHARACTERISTICS] 'THE SERVICE BEING PROCURED IS AN INHERENTLY GOVERNMENTAL FUNCTION'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198722, prescription_id FROM Prescriptions WHERE prescription_name IN ('223.370-5','223.7203'); -- [SERVICE CHARACTERISTICS] 'THE SERVICE BEING PROCURED INVOLVES THE USE OF ARMS, AMMUNITION OR EXPLOSIVES'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198723, prescription_id FROM Prescriptions WHERE prescription_name IN ('37.116-2'); -- [SERVICE CHARACTERISTICS] 'THE SERVICE BEING PROCURED INVOLVES BUSINESS OPERATIONS CONDUCTED IN U.S. COINS AND CURRENCY'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198724, prescription_id FROM Prescriptions WHERE prescription_name IN ('37.110(c)'); -- [SERVICE CHARACTERISTICS] 'CONTINUATION OF SERVICES IS VITAL AND TRANSITION IS ANTICIPATED TO BE DIFFICULT'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198726, prescription_id FROM Prescriptions WHERE prescription_name IN ('246.270-4'); -- [SERVICE CONFIGURED FOR OCCUPANCY] 'YES'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198728, prescription_id FROM Prescriptions WHERE prescription_name IN ('10.003','12.301(b)(1)','12.301(b)(2)','12.301(b)(3)','12.301(b)(4)','12.301(b)(4)(i)','12.301(b)(4)(ii)','12.301(c)(1)','13.302-5(d)','15.209(b)(1)','15.209(b)(3)','15.209(b)(4)','15.408(f)(1)','15.408(f)(2)','15.408(n)(2)','15.408(n)(2)(iii)','16.307(a)','16.307(a)(2)','16.307(a)(3)','16.307(a)(4)','16.307(a)(5)','16.601(f)(1)','16.601(f)(2)','16.601(f)(3)','2013-O0019','203.1004(b)(2)(ii)','203.570-3','204.7109(b)','216.601(e)','217.7303','22.1310(c)','22.1505(a)','22.305','222.7405','223.570-2','223.7203','225.1101(10)(i)','225.1101(10)(i)(A)','225.1101(10)(i)(B)','225.1101(10)(i)(C)','225.1101(10)(i)(D)','225.1101(10)(i)(E)','225.1101(10)(i)(F)','225.7009-5','225.7204(a)','225.7204(b)','225.772-5','228.370(b)','23.505','242.7204','25.1103(d)','26.206(a)','28.106-4(b)','3.1004(b)','3.104-9(a)','3.104-9(b)','3.404','3.502-3','3.503-2','32.111(a)(1)','32.206(g) ***','4.1202(a)','42.1305(c)','42.709-6','44.403'); -- [COMMERCIAL SERVICES] 'YES'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198730, prescription_id FROM Prescriptions WHERE prescription_name IN ('4.1705(a)','4.1705(b)'); -- [SERVICE CONTRACT REPORTING] 'YES'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198732, prescription_id FROM Prescriptions WHERE prescription_name IN ('234.7101(b)','234.7101(b)(1)','234.7101(b)(2)'); -- [OSD APPROVAL FOR DATA SYSTEM] 'YES'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198734, prescription_id FROM Prescriptions WHERE prescription_name IN ('22.103-5(b)'); -- [SWING FROM TARGET FEE] 'YES'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198736, prescription_id FROM Prescriptions WHERE prescription_name IN ('19.708(c)(1)'); -- [SMALL BUSINESS AWARD FEE] 'YES'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198738, prescription_id FROM Prescriptions WHERE prescription_name IN ('16.203-4(c)'); -- [MAJOR DESIGN OR DEVELOPMENT] 'YES'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198740, prescription_id FROM Prescriptions WHERE prescription_name IN ('16.203-4(a)','16.203-4(b)','16.203-4(c)'); -- [PCO EPA DETERMINATION] 'YES'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198742, prescription_id FROM Prescriptions WHERE prescription_name IN ('17.208(d)','17.208(e)'); -- [OPTION CONDITIONS] 'OPTION QUANTITY IS EXPRESSED AS AN ADDITIONAL QUANTITY OF A SPECIFIC LINE ITEM'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198743, prescription_id FROM Prescriptions WHERE prescription_name IN ('17.208(g)'); -- [OPTION CONDITIONS] 'OPTION QUANTITY IS EXPRESSED AS A PERCENTAGE OF BASIC CONTRACT QUANTITY'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198744, prescription_id FROM Prescriptions WHERE prescription_name IN ('17.208(b)','17.208(c)'); -- [OPTION CONDITIONS] 'THERE IS A REASONABLE LIKELIHOOD THAT THE OPTION WILL BE EXERCISED'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198745, prescription_id FROM Prescriptions WHERE prescription_name IN ('217.208-70(a)','217.208-70(a)(1)'); -- [OPTION CONDITIONS] 'AN FMS OPTION IS INCLUDED FOR A SPECIFIED COUNTRY'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198746, prescription_id FROM Prescriptions WHERE prescription_name IN ('217.208-70(a)','217.208-70(a)(2)'); -- [OPTION CONDITIONS] 'AN FMS OPTION IS INCLUDED FOR AN UNSPECIFIED COUNTRY'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198747, prescription_id FROM Prescriptions WHERE prescription_name IN ('17.208(b)','17.208(c)'); -- [OPTION CONDITIONS] 'THE OPTION MAY BE EXERCISED AT THE TIME OF CONTRACT AWARD'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198748, prescription_id FROM Prescriptions WHERE prescription_name IN ('17.208(g)'); -- [OPTION CONDITIONS] 'THERE IS A LIMITATION ON THE DURATION OF THE CONTRACT'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198749, prescription_id FROM Prescriptions WHERE prescription_name IN ('17.208(g)'); -- [OPTION CONDITIONS] 'CONTRACT STATES THAT AN EXTENSION OF THE CONTRACT INCLUDES AN EXTENSION OF THE OPTION'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198750, prescription_id FROM Prescriptions WHERE prescription_name IN ('217.208-70(b)'); -- [OPTION CONDITIONS] 'SURGE OPTION IN SUPPORT OF INDUSTRIAL CAPABILITY PRODUCTION PLANNING IS INCLUDED'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198751, prescription_id FROM Prescriptions WHERE prescription_name IN ('17.208(g)'); -- [OPTION CONDITIONS] 'GOVERNMENT MUST PROVIDE A PRELIMINARY WRITTEN NOTICE OF INTENT TO EXTEND THE CONTRACT'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198753, prescription_id FROM Prescriptions WHERE prescription_name IN ('11.204(b)'); -- [PERFORMANCE REQUIREMENTS 1] 'LISTED IN THE DOD ACQUISITION STREAMLINING AND STANDARDIZATION INFORMATION SYSTEM (ASSIST) NOT FURNISHED WITH THE SOLICITATION'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198754, prescription_id FROM Prescriptions WHERE prescription_name IN ('211.204(c)(i)','211.204(c)(ii)'); -- [PERFORMANCE REQUIREMENTS 1] 'NOT LISTED IN THE DOD ACQUISITION STREAMLINING AND STANDARDIZATION INFORMATION SYSTEM (ASSIST)'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198755, prescription_id FROM Prescriptions WHERE prescription_name IN ('11.204(a)'); -- [PERFORMANCE REQUIREMENTS 1] 'LISTED IN THE GSA INDEX, NOT FURNISHED WITH SOLICITATION'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198756, prescription_id FROM Prescriptions WHERE prescription_name IN ('11.204(d)'); -- [PERFORMANCE REQUIREMENTS 1] 'NOT LISTED IN THE GSA INDEX, AVAILABLE FOR EXAMINATION AT SPECIFIED LOCATION'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198757, prescription_id FROM Prescriptions WHERE prescription_name IN ('11.204(c)'); -- [PERFORMANCE REQUIREMENTS 1] 'NOT LISTED IN THE GSA INDEX, MAY BE OBTAINED FROM DESIGNATED SOURCES'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198758, prescription_id FROM Prescriptions WHERE prescription_name IN ('11.107(b)'); -- [PERFORMANCE REQUIREMENTS 1] 'GOVERNMENT-UNIQUE STANDARDS APPLY'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198759, prescription_id FROM Prescriptions WHERE prescription_name IN ('9.206-2'); -- [PERFORMANCE REQUIREMENTS 1] 'THE ACQUISITION IS SUBJECT TO A QUALIFICATION REQUIREMENT'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198762, prescription_id FROM Prescriptions WHERE prescription_name IN ('2015-00017','234.203(1)','234.203(2)','34.203(a)','34.203(b)','34.203(c)'); -- [PERFORMANCE REQUIREMENTS 2] 'EARNED VALUE MANAGEMENT SYSTEM'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198763, prescription_id FROM Prescriptions WHERE prescription_name IN ('34.203(b)'); -- [PERFORMANCE REQUIREMENTS 2] 'INTEGRATED BASELINE REVIEW (AFTER AWARD)'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198764, prescription_id FROM Prescriptions WHERE prescription_name IN ('34.203(a)'); -- [PERFORMANCE REQUIREMENTS 2] 'INTEGRATED BASELINE REVIEW (PRIOR TO AWARD)'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198765, prescription_id FROM Prescriptions WHERE prescription_name IN ('209.570-4(a)','209.570-4(b)'); -- [PERFORMANCE REQUIREMENTS 2] 'USE OF A LEAD SYSTEMS INTEGRATOR'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198766, prescription_id FROM Prescriptions WHERE prescription_name IN ('15.408(a)','15.408(a)(1)','15.408(a)(2)'); -- [PERFORMANCE REQUIREMENTS 2] 'MAKE OR BUY PROGRAM'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198767, prescription_id FROM Prescriptions WHERE prescription_name IN ('9.308-1(a)(1)','9.308-1(a)(2)','9.308-1(a)(3)','9.308-1(b)(1)','9.308-1(b)(2)','9.308-1(b)(3)','9.308-2(a)(1)','9.308-2(b)(1)','9.308-2(b)(2)'); -- [PERFORMANCE REQUIREMENTS 2] 'FIRST ARTICLE APPROVAL IS REQUIRED'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198768, prescription_id FROM Prescriptions WHERE prescription_name IN ('211.275-3'); -- [PERFORMANCE REQUIREMENTS 2] 'RADIO FREQUENCY IDENTIFICATION (RFID) IS REQUIRED'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198770, prescription_id FROM Prescriptions WHERE prescription_name IN ('22.1310(a)(1)','22.1310(a)(2)'); -- [LABOR REQUIREMENTS 1] 'RECRUITMENT IS MADE OUTSIDE THE UNITED STATES (50 STATES PLUS DISTRICT OF COLUMBIA)'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198771, prescription_id FROM Prescriptions WHERE prescription_name IN ('22.1408(a)','22.1408(b)'); -- [LABOR REQUIREMENTS 1] 'RECRUITMENT IS MADE OUTSIDE THE UNITED STATES (50 STATES PLUS DISTRICT OF COLUMBIA), PUERTO RICO, THE NORTHERN MARINA ISLANDS, AMERICAN SAMOA, GUAM, THE U.S. VIRGIN ISLANDS AND WAKE ISLAND'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198773, prescription_id FROM Prescriptions WHERE prescription_name IN ('215.370-3(b)'); -- [LABOR REQUIREMENTS 2] 'THE CONTRACTOR INTENDS TO USE MEMBERS OF THE SELECTIVE RESERVE AND THIS WAS AN EVALUATION FACTOR'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198774, prescription_id FROM Prescriptions WHERE prescription_name IN ('225.7402-5(a)'); -- [LABOR REQUIREMENTS 2] 'THE CONTRACTOR IS AUTHORIZED TO ACCOMPANY US FORCES OUTSIDE THE US'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198775, prescription_id FROM Prescriptions WHERE prescription_name IN ('22.305'); -- [LABOR REQUIREMENTS 2] 'THE REQUIREMENT INVOLVES THE EMPLOYMENT OF LABORERS OR MECHANICS'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198776, prescription_id FROM Prescriptions WHERE prescription_name IN ('22.1103'); -- [LABOR REQUIREMENTS 2] 'THE SERVICE BEING PROCURED REQUIRES A MEANINGFUL NUMBER OF PROFESSIONAL EMPLOYEES'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198777, prescription_id FROM Prescriptions WHERE prescription_name IN ('22.202'); -- [LABOR REQUIREMENTS 2] 'THERE IS A PROHIBITION AGAINST THE EMPLOYMENT OF CONVICT LABOR'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198779, prescription_id FROM Prescriptions WHERE prescription_name IN ('22.505(a)(1)','22.505(a)(2)','22.505(b)(1)','22.505(b)(2)'); -- [LABOR REQUIREMENTS 3] 'PROJECT LABOR AGREEMENT REQUIRED'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198780, prescription_id FROM Prescriptions WHERE prescription_name IN ('22.505(a)(2)'); -- [LABOR REQUIREMENTS 3] 'SUBMISSION OF LABOR AGREEMENT AFTER AWARD'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198781, prescription_id FROM Prescriptions WHERE prescription_name IN ('22.505(a)(1)'); -- [LABOR REQUIREMENTS 3] 'SUBMISSION OF LABOR AGREEMENT FROM SUCCESSFUL OFFEROR PRIOR TO AWARD'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198782, prescription_id FROM Prescriptions WHERE prescription_name IN ('22.103-5(a)'); -- [LABOR REQUIREMENTS 3] 'NOTIFICATION OF LABOR DISPUTES IS REQUIRED'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198783, prescription_id FROM Prescriptions WHERE prescription_name IN ('216.203-4-70(c)(1)'); -- [LABOR REQUIREMENTS 3] 'FOREIGN GOVERNMENT CONTROLS WAGE RATES OR MATERIAL PRICES'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198785, prescription_id FROM Prescriptions WHERE prescription_name IN ('45.107(a)','45.107(a)(2)','45.107(a)(3)','47.305-12(a)(2)'); -- [CONTRACTOR MATERIAL] 'GOVERNMENT FURNISHED PROPERTY (GFP) IS PROVIDED'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198786, prescription_id FROM Prescriptions WHERE prescription_name IN ('236.570(b)(2)(i)'); -- [CONTRACTOR MATERIAL] 'LARGE STOCKPILES OF CONSTRUCTION MATERIAL IN EXCESS OF NORMAL ARE REQUIRED'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198787, prescription_id FROM Prescriptions WHERE prescription_name IN ('246.870-3'); -- [CONTRACTOR MATERIAL] 'THE CONTRACTOR WILL SUPPLY ELECTRONIC PARTS OR COMPONENTS, PARTS,, OR ASSEMBLIES CONTAINING ELECTRONIC PARTS AS PART OF THE SERVICE'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198788, prescription_id FROM Prescriptions WHERE prescription_name IN ('46.710(e)(2)'); -- [CONTRACTOR MATERIAL] 'THE GOVERNMENT SPECIFIES THE USE OF EQUIPMENT BY BRAND NAME AND MODEL'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198789, prescription_id FROM Prescriptions WHERE prescription_name IN ('23.705(c)(1)','23.705(d)(2)'); -- [CONTRACTOR MATERIAL] 'TELEVISIONS WILL BE FURNISHED BY THE CONTRACTOR FOR USE BY THE GOVERNMENT'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198790, prescription_id FROM Prescriptions WHERE prescription_name IN ('23.705(c)(1)','23.705(d)(2)'); -- [CONTRACTOR MATERIAL] 'IMAGING EQUIPMENT WILL BE FURNISHED BY THE CONTRACTOR FOR USE BY THE GOVERNMENT'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198791, prescription_id FROM Prescriptions WHERE prescription_name IN ('23.705(b)(2)','23.705(d)(1)'); -- [CONTRACTOR MATERIAL] 'PERSONAL COMPUTER PRODUCTS WILL BE FURNISHED BY THE CONTRACTOR FOR USE BY THE GOVERNMENT'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198792, prescription_id FROM Prescriptions WHERE prescription_name IN ('23.206'); -- [CONTRACTOR MATERIAL] 'PRODUCTS LISTED IN THE ENERGY STAR PROGRAM WILL BE FURNISHED BY THE CONTRACTOR FOR USE BY THE GOVERNMENT'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198793, prescription_id FROM Prescriptions WHERE prescription_name IN ('23.705(b)(1)','23.705(b)(2)'); -- [CONTRACTOR MATERIAL] 'CONTRACTOR FURNISHING OF IMAGING EQUIPMENT FOR USE IN PERFORMING SERVICES AT A FEDERALLY CONTROLLED FACILITY'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198794, prescription_id FROM Prescriptions WHERE prescription_name IN ('23.705(c)(1)','23.705(d)(2)'); -- [CONTRACTOR MATERIAL] 'CONTRACTOR FURNISHING OF TELEVISIONS FOR USE IN PERFORMING SERVICES AT A FEDERALLY CONTROLLED FACILITY'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198795, prescription_id FROM Prescriptions WHERE prescription_name IN ('23.206'); -- [CONTRACTOR MATERIAL] 'CONTRACTOR FURNISHING OF ENERGY STAR EQUIPMENT FOR USE IN PERFORMING SERVICES AT A FEDERALLY CONTROLLED FACILITY'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198796, prescription_id FROM Prescriptions WHERE prescription_name IN ('23.705(b)(2)','23.705(d)(1)'); -- [CONTRACTOR MATERIAL] 'CONTRACTOR FURNISHING OF EPEAT-REGISTERED PERSONAL COMPUTER PRODUCTS FOR USE IN PERFORMING SERVICES AT A FEDERALLY CONTROLLED FACILITY'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198797, prescription_id FROM Prescriptions WHERE prescription_name IN ('23.206'); -- [CONTRACTOR MATERIAL] 'PRODUCTS LISTED IN THE ENERGY STAR PROGRAM ARE SPECIFIED IN THE DESIGN, INCORPORATED DURING CONSTRUCTION, RENOVATION OR MAINTENANCE'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198798, prescription_id FROM Prescriptions WHERE prescription_name IN ('247.305-70','47.305-17'); -- [CONTRACTOR MATERIAL] 'THE CONTRACTOR WILL SUPPLY CONTRACTOR- FURNISHED RETURNABLE CYLINDERS AND WILL RETAIN TITLE'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198800, prescription_id FROM Prescriptions WHERE prescription_name IN ('225.7902-5(b)'); -- [CONTRACTOR MATERIAL] 'EXPORT-CONTROLLED ITEMS ARE EXPECTED TO BE INVOLVED IN THE PERFORMANCE OF THE CONTRACT'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198802, prescription_id FROM Prescriptions WHERE prescription_name IN ('19.708(b)','19.708(b)(1)','19.708(b)(1)(iii)'); -- [SUBCONTRACTING] 'SUBCONTRACTING POSSIBILITIES EXIST'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198803, prescription_id FROM Prescriptions WHERE prescription_name IN ('225.1101(7)','225.1101(8)'); -- [SUBCONTRACTING] 'A UNITED KINGDOM FIRM IS EXPECTED TO RECEIVE A SUBCONTRACT OVER $1,000,000.'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198804, prescription_id FROM Prescriptions WHERE prescription_name IN ('225.7011-3'); -- [SUBCONTRACTING] 'CONTRACTOR REQUIRED TO PROCURE CARBON, ALLOY OR ARMOR STEEL PLATE'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198805, prescription_id FROM Prescriptions WHERE prescription_name IN ('8.005'); -- [SUBCONTRACTING] 'SUBCONTRACTS FOR SUPPLIES OR SERVICES FOR GOVERNMENT USE ARE REQUIRED TO BE PROCURED FROM THOSE LISTED ON THE PROCUREMENT LIST MAINTAINED BY THE COMMITTEE FOR PURCHASE FROM PEOPLE WHO ARE BLIND OR SEVERELY DISABLE'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198806, prescription_id FROM Prescriptions WHERE prescription_name IN ('236.570(d)'); -- [SUBCONTRACTING] 'THIS ACQUISITION COULD REQUIRE THE PURCHASE OF STEEL AS A CONSTRUCTION MATERIAL'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198808, prescription_id FROM Prescriptions WHERE prescription_name IN ('23.1005(b)'); -- [ENVIRONMENT] 'CONTRACTOR ACTIVITIES AND OPERATIONS ARE COVERED WITHIN THE ENVIRONMENTAL MANAGEMENT SYSTEM (EMS)'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198809, prescription_id FROM Prescriptions WHERE prescription_name IN ('23.1005(b)'); -- [ENVIRONMENT] 'THE AGENCY HAS IMPLEMENTED OR PLANS TO IMPLEMENT AN EMERGENCY MANAGEMENT SYSTEM (EMS)'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198810, prescription_id FROM Prescriptions WHERE prescription_name IN ('23.1005(c)'); -- [ENVIRONMENT] 'CONTRACTOR ACTIVITIES SHOULD BE INCLUDED WITHIN THE FACILITY COMPLIANCE AUDIT (FCA) OR AN ENVIRONMENTAL MANAGEMENT SYSTEM'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198811, prescription_id FROM Prescriptions WHERE prescription_name IN ('8.505'); -- [ENVIRONMENT] 'PERFORMANCE INVOLVES A MAJOR HELIUM REQUIREMENT'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198812, prescription_id FROM Prescriptions WHERE prescription_name IN ('223.7306'); -- [ENVIRONMENT] 'NO SUBSTITUTE FOR HEXAVALENT CHROMIUM CAN MEET PERFORMANCE REQUIREMENTS'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198813, prescription_id FROM Prescriptions WHERE prescription_name IN ('223.7306'); -- [ENVIRONMENT] 'USE OF HEXAVALENT CHROMIUM WAS PREVIOUSLY APPROVED'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198814, prescription_id FROM Prescriptions WHERE prescription_name IN ('223.7306'); -- [ENVIRONMENT] 'LEGACY SYSTEM ALREADY CONTAINS HEXALENT CHROMIUM'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198815, prescription_id FROM Prescriptions WHERE prescription_name IN ('23.505'); -- [ENVIRONMENT] 'APPLICATION OF "DRUG FREE WORKPLACE" SUBPART WOULD BE INAPPROPRIATE IN CONNECTION WITH THE LAW ENFORCEMENTS AGENCYS UNDERCOVER OPERATIONS'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198816, prescription_id FROM Prescriptions WHERE prescription_name IN ('23.505'); -- [ENVIRONMENT] 'APPLICATION OF "DRUG FREE WORKPLACE" SUBPART WOULD BE INCONSISTENT WITH INTERNATIONAL OBLIGATION OF THE US OR WITH THE LAWS OF A FOREIGN COUNTRY'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198818, prescription_id FROM Prescriptions WHERE prescription_name IN ('49.502(b)(1)(iii)','49.502(b)(2)','49.503(a)(3)','49.503(a)(4)'); -- [INTEREST ON PARTIAL PAYMENTS] 'YES'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198820, prescription_id FROM Prescriptions WHERE prescription_name IN ('49.502(c)'); -- [TERMINATION] 'YES'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198822, prescription_id FROM Prescriptions WHERE prescription_name IN ('43.205(f)'); -- [TECHNICAL CHANGES] 'YES'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198824, prescription_id FROM Prescriptions WHERE prescription_name IN ('48.201','48.201(d)','48.201(e)(1)'); -- [VALUE ENGINEERING CONDITIONS] 'A VALUE ENGINEERING INCENTIVE IS INVOLVED'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198825, prescription_id FROM Prescriptions WHERE prescription_name IN ('48.201(c)','48.201(d)'); -- [VALUE ENGINEERING CONDITIONS] 'MANDATORY VALUE ENGINEERING PROGRAM REQUIREMENT IS APPROPRIATE'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198826, prescription_id FROM Prescriptions WHERE prescription_name IN ('48.201(f)'); -- [VALUE ENGINEERING CONDITIONS] 'GOVERNMENT REQUIRES AND PAYS FOR A SPECIFIC VALUE ENGINEERING EFFORT'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198827, prescription_id FROM Prescriptions WHERE prescription_name IN ('48.201','48.201(c)','48.201(d)','48.201(e)(1)'); -- [VALUE ENGINEERING CONDITIONS] 'PACKAGING SPECIFICATIONS ARE INVOLVED'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198828, prescription_id FROM Prescriptions WHERE prescription_name IN ('48.201(e)(1)','48.202'); -- [VALUE ENGINEERING CONDITIONS] 'THE COST OF COMPUTING AND TRACKING COLLATERAL SAVINGS FOR A CONTRACT WILL EXCEED THE BENEFITS TO BE DERIVED'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198829, prescription_id FROM Prescriptions WHERE prescription_name IN ('9.308-2(a)(1)','9.308-2(a)(3)','9.308-2(b)(3)'); -- [FIRST ARTICLE CONDITIONS] 'CONTRACTOR IS AUTHORIZED TO PURCHASE MATERIAL OR COMMENCE PRODUCTION BEFORE FIRST ARTICLE APPROVAL'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198830, prescription_id FROM Prescriptions WHERE prescription_name IN ('9.308-1(a)(1)','9.308-1(a)(3)','9.308-1(b)(1)','9.308-1(b)(3)'); -- [FIRST ARTICLE CONDITIONS] 'CONTRACTOR TESTING IS REQUIRED'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198831, prescription_id FROM Prescriptions WHERE prescription_name IN ('9.308-2(a)(1)','9.308-2(b)(1)','9.308-2(b)(2)','9.308-2(b)(3)'); -- [FIRST ARTICLE CONDITIONS] 'GOVERNMENT TESTING IS REQUIRED'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198832, prescription_id FROM Prescriptions WHERE prescription_name IN ('9.308-1(a)(2)','9.308-1(b)(2)','9.308-2(a)(1)','9.308-2(b)(2)'); -- [FIRST ARTICLE CONDITIONS] 'FIRST ARTICLE AND PRODUCTION QUANTITY ARE TO BE PRODUCED AT THE SAME FACILITY'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198833, prescription_id FROM Prescriptions WHERE prescription_name IN ('235.072(b)','235.072(b)(2)'); -- [DD FORM 1494] 'YES'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198834, prescription_id FROM Prescriptions WHERE prescription_name IN ('235.072(b)','235.072(b)(1)'); -- [DD FORM 1494] 'NO'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198835, prescription_id FROM Prescriptions WHERE prescription_name IN ('22.407(e)','22.407(f)','22.407(g)'); -- [LABOR ACTS] 'THE DAVIS-BACON ACT APPLY APPLIES'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198836, prescription_id FROM Prescriptions WHERE prescription_name IN ('22.610'); -- [LABOR ACTS] 'THE WALSH-HEALEY PUBLIC CONTRACTS ACT APPLIES'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198837, prescription_id FROM Prescriptions WHERE prescription_name IN ('22.1006(a)','22.1006(b)','22.1006(e)(2)','22.1006(e)(4)'); -- [LABOR ACTS] 'THE SERVICE CONTRACT LABOR STANDARDS STATUTE APPLIES'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198839, prescription_id FROM Prescriptions WHERE prescription_name IN ('223.570-2'); -- [DRUG FREE WORKFORCE REQUIRED] 'YES'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198841, prescription_id FROM Prescriptions WHERE prescription_name IN ('223.7203'); -- [GOVERNMENT FURNISHED PROPERTY] 'ARMS, AMMUNITION AND EXPLOSIVES ARE PROVIDED AS GFP'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198842, prescription_id FROM Prescriptions WHERE prescription_name IN ('29.401-4(b)','45.107(a)','45.107(a)(2)','45.107(a)(3)'); -- [GOVERNMENT FURNISHED PROPERTY] 'THE CONTRACTOR IS DIRECTED TO ACQUIRE PROPERTY FOR THE CONTRACT THAT IS TITLED IN THE GOVERNMENT'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198843, prescription_id FROM Prescriptions WHERE prescription_name IN ('45.107(b)'); -- [GOVERNMENT FURNISHED PROPERTY] 'GFP IS PROVIDED (INITIAL PROVISIONING ONLY)'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198844, prescription_id FROM Prescriptions WHERE prescription_name IN ('45.107(b)'); -- [GOVERNMENT FURNISHED PROPERTY] 'THE GOVERNMENT IS NOT RESPONSIBLE FOR REPAIR OR REPLACEMENT OF GOVERNMENT PROPERTY'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198845, prescription_id FROM Prescriptions WHERE prescription_name IN ('45.107(a)','45.107(a)(2)'); -- [GOVERNMENT FURNISHED PROPERTY] 'THE GOVERNMENT PROPERTY VALUE IS GREATER THAN SAT'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198846, prescription_id FROM Prescriptions WHERE prescription_name IN ('245.107(1)'); -- [GOVERNMENT FURNISHED PROPERTY] 'MAPPING, CHARTING AND GEODESY PROPERTY IS PROVIDED AS GFP'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198848, prescription_id FROM Prescriptions WHERE prescription_name IN ('217.7303','7.203'); -- [GOVERNMENT SUPPLY STATEMENT] 'THE GOVERNMENT ALREADY HAS THE SOURCING INFORMATION REQUESTED'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198849, prescription_id FROM Prescriptions WHERE prescription_name IN ('217.7303'); -- [GOVERNMENT SUPPLY STATEMENT] 'IT IS NOT PRACTICABLE FOR THE GOVERNMENT TO REQUEST SOURCING INFORMATION'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198850, prescription_id FROM Prescriptions WHERE prescription_name IN ('51.107'); -- [GOVERNMENT SUPPLY STATEMENT] 'THE CONTRACTING OFFICER AUTHORIZES THE CONTRACTOR TO ACQUIRE SUPPLIES OR SERVICES FROM A GOVERNMENT SUPPLY SOURCE'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198851, prescription_id FROM Prescriptions WHERE prescription_name IN ('7.203'); -- [GOVERNMENT SUPPLY STATEMENT] 'THE SOURCING DATA REQUESTED IS OTHERWISE READILY AVAILABLE'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198852, prescription_id FROM Prescriptions WHERE prescription_name IN ('51.205'); -- [GOVERNMENT SUPPLY STATEMENT] 'THE CONTRACTING OFFICER MAY AUTHORIZE THE CONTRACTOR TO USE INTERAGENCY FLEET MANAGEMENT SYSTEM (IFMS) VEHICLES AND RELATED SERVICES'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198853, prescription_id FROM Prescriptions WHERE prescription_name IN ('7.203'); -- [GOVERNMENT SUPPLY STATEMENT] 'IT IS IMPRACTICABLE FOR THE GOVERNMENT TO VARY ITS FUTURE SOURCING REQUIREMENTS'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198854, prescription_id FROM Prescriptions WHERE prescription_name IN ('208.7305(a)'); -- [GOVERNMENT SUPPLY STATEMENT] 'PRECIOUS METALS ARE REQUIRED AND NOT AVAILABLE FROM DEFENSE SUPPLY CENTER, PHILADELPHIA (DSCP)'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198856, prescription_id FROM Prescriptions WHERE prescription_name IN ('26.104'); -- [SUBCONTRACTING PLAN CONDITIONS] 'SUBCONTRACTING POSSIBILITIES FOR INDIAN ORGANIZATIONS OR INDIAN-OWNED ECONOMIC ENTERPRISES'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198857, prescription_id FROM Prescriptions WHERE prescription_name IN ('219.708(b)(1)(A)(1)','219.708(b)(1)(B)'); -- [SUBCONTRACTING PLAN CONDITIONS] 'SUBCONTRACTING PLAN IS APPROVED UNDER THE TEST PROGRAM'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198858, prescription_id FROM Prescriptions WHERE prescription_name IN ('19.708(c)(1)'); -- [SUBCONTRACTING PLAN CONDITIONS] 'SUBCONTRACTING PLANS ARE REQUIRED WITH INCLUSION OF A MONETARY INCENTIVE'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198859, prescription_id FROM Prescriptions WHERE prescription_name IN ('19.708(b)(1)'); -- [SUBCONTRACTING PLAN CONDITIONS] 'SUBCONTRACTING PLANS ARE REQUIRED WITH INITIAL PROPOSALS'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198861, prescription_id FROM Prescriptions WHERE prescription_name IN ('211.274-6(c)'); -- [END ITEM MARKING] 'YES'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198863, prescription_id FROM Prescriptions WHERE prescription_name IN ('211.272'); -- [PRESERVATION AND PACKING SPECIFICATIONS] 'YES'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198865, prescription_id FROM Prescriptions WHERE prescription_name IN ('46.311'); -- [INSPECTION AND ACCEPTANCE REQUIREMENTS] 'A HIGHER LEVEL CONTRACT QUALITY REQUIREMENT IS APPROPRIATE'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198866, prescription_id FROM Prescriptions WHERE prescription_name IN ('46.301'); -- [INSPECTION AND ACCEPTANCE REQUIREMENTS] 'GOVERNMENT REQUIRES AN EXPLICIT UNDERSTANDING OF THE CONTRACTOR'S INSPECTION RESPONSIBILITIES'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198867, prescription_id FROM Prescriptions WHERE prescription_name IN ('46.301'); -- [INSPECTION AND ACCEPTANCE REQUIREMENTS] 'GOVERNMENT WILL RELY ON CONTRACTOR INSPECTION'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198869, prescription_id FROM Prescriptions WHERE prescription_name IN ('46.315'); -- [INSPECTION AND ACCEPTANCE REQUIREMENTS 2] 'CERTIFICATE OF CONFORMANCE IS IN THE BEST INTEREST OF THE GOVERNMENT IN LIEU OF SOURCE INSPECTION'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198870, prescription_id FROM Prescriptions WHERE prescription_name IN ('246.370'); -- [INSPECTION AND ACCEPTANCE REQUIREMENTS 2] 'DD250 PROCESSING IS NOT PRACTICABLE IN OVERSEAS AREAS'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198871, prescription_id FROM Prescriptions WHERE prescription_name IN ('239.7103(a)'); -- [INSPECTION AND ACCEPTANCE REQUIREMENTS 2] 'CONTRACTOR WILL PERFORM INFORMATION ASSURANCE FUNCTION'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198872, prescription_id FROM Prescriptions WHERE prescription_name IN ('47.305-4(c)'); -- [INSPECTION AND ACCEPTANCE REQUIREMENTS 2] 'INSPECTION AND ACCEPTANCE WILL OCCUR AT ORIGIN'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198873, prescription_id FROM Prescriptions WHERE prescription_name IN ('46.306'); -- [INSPECTION AND ACCEPTANCE REQUIREMENTS 2] 'GOVERNMENT INSPECTION AND ACCEPTANCE ARE TO BE PERFORMED AT THE CONTRACTOR'S PLANT'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198877, prescription_id FROM Prescriptions WHERE prescription_name IN ('46.710(a)(1)','46.710(a)(3)','46.710(a)(4)','46.710(a)(5)','46.710(b)(1)','46.710(b)(2)','46.710(b)(3)','46.710(b)(4)','46.710(c)(1)','46.710(c)(2)','46.710(c)(3)','46.710(c)(4)','46.710(d)','46.710(e)(1)','46.710(e)(2)'); -- [WARRANTY CONDITIONS] 'USE OF A WARRANTY CLAUSE IS APPROVED'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198878, prescription_id FROM Prescriptions WHERE prescription_name IN ('246.710(1)','246.710(1)(ii)','246.710(1)(iii)'); -- [WARRANTY CONDITIONS] 'REQUIRES GREATER PROTECTION THAN FAR PART 46 INSPECTION/WARRANTY CLAUSES'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198879, prescription_id FROM Prescriptions WHERE prescription_name IN ('46.710(c)(1)','46.710(c)(2)','46.710(c)(3)','46.710(c)(4)'); -- [WARRANTY CONDITIONS] 'PERFORMANCE SPECIFICATIONS OR DESIGN ARE OF MAJOR IMPORTANCE'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198880, prescription_id FROM Prescriptions WHERE prescription_name IN ('46.710(a)(3)'); -- [WARRANTY CONDITIONS] 'WARRANTED SUPPLIES CANNOT BE OBTAINED FROM ANOTHER SOURCE'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198882, prescription_id FROM Prescriptions WHERE prescription_name IN ('46.710(a)(6)','46.710(b)(4)','46.710(c)(4)'); -- [WARRANTY RECOVERY AND TRANSPORTATION] 'RECOVERY OF THE WARRANTED ITEMS WILL INVOLVE CONSIDERABLE GOVERNMENT EXPENSE FOR DISASSEMBLY AND/OR REASSEMBLY OF LARGER ITEMS'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198883, prescription_id FROM Prescriptions WHERE prescription_name IN ('46.710(b)(2)','46.710(c)(2)'); -- [WARRANTY RECOVERY AND TRANSPORTATION] 'TRANSPORTATION FOR CORRECTION OR REPLACEMENT AT GOVERNMENT'S EXPENSE'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198885, prescription_id FROM Prescriptions WHERE prescription_name IN ('46.710(b)(1)','46.710(b)(2)','46.710(b)(3)','46.710(b)(4)'); -- [WARRANTY DESCRIPTION] 'SUPPLIES ARE "COMPLEX" ITEMS'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198886, prescription_id FROM Prescriptions WHERE prescription_name IN ('46.710(a)(1)','46.710(a)(3)','46.710(a)(4)','46.710(a)(5)','46.710(a)(6)'); -- [WARRANTY DESCRIPTION] 'SUPPLIES ARE "NON-COMPLEX" ITEMS'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198887, prescription_id FROM Prescriptions WHERE prescription_name IN ('46.805','46.805(a)'); -- [WARRANTY DESCRIPTION] 'SUPPLIES ARE HIGH-VALUE ITEMS'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198888, prescription_id FROM Prescriptions WHERE prescription_name IN ('46.805','46.805(a)'); -- [WARRANTY DESCRIPTION] 'SUPPLIES INCLUDE BOTH HIGH VALUE ITEMS AND NON HIGH VALUE ITEMS'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198889, prescription_id FROM Prescriptions WHERE prescription_name IN ('246.710(3)(i)(A)','246.710(3)(i)(B)(iii)'); -- [WARRANTY DESCRIPTION] 'WARRANTY ON SERIALIZED ITEMS IS REQUIRED AND OFFERORS ARE REQUIRED TO ENTER DATA WITH THE OFFER'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198891, prescription_id FROM Prescriptions WHERE prescription_name IN ('16.506(f)','16.506(g)','22.1803','249.501-70','3.1004(a)','36.515','8.1104(d)'); -- [PERIOD OF PERFORMANCE] '[NUMERIC DATA ENTRY BY USER]'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198892, prescription_id FROM Prescriptions WHERE prescription_name IN ('223.570-2'); -- [PLACE OF PERFORMANCE] 'ASSOCIATED TERRITORIAL WATERS AND AIR SPACE'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198893, prescription_id FROM Prescriptions WHERE prescription_name IN ('22.305'); -- [PLACE OF PERFORMANCE] 'OUTER CONTINENTAL SHELF'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198894, prescription_id FROM Prescriptions WHERE prescription_name IN ('225.371-5(a)','225.7402-5(a)','25.301-4'); -- [PLACE OF PERFORMANCE] 'DESIGNATED OPERATIONAL AREA'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198895, prescription_id FROM Prescriptions WHERE prescription_name IN ('216.203-4-70(c)(1)','22.810(g)','225.1103(2)','225.1103(3)','229.402-70(a)','229.402-70(a)(2)','232.806(a)(1)','29.402-1(a)','29.402-2(a)'); -- [PLACE OF PERFORMANCE] 'FOREIGN COUNTRY'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198896, prescription_id FROM Prescriptions WHERE prescription_name IN ('19.309(a)(1)','19.309(a)(2)','19.309(c)','19.708(a)','2015-00016','22.1705(a)(2)','222.7201(a)','225.1103(1)','225.7503(a)','225.7503(a)(1)','225.7503(a)(2)','225.7503(b)','225.7503(b)(1)','225.7503(b)(2)','225.7503(b)(3)','225.7503(b)(4)','23.505','23.804(b)','233.215-70','236.570(c)(1)','25.1102(a)','25.1102(c)','25.1102(c)(3)','25.1102(e)(1)','28.310','29.401-1','29.401-3','29.401-3(a)','29.401-3(b)','3.1004(b)','4.1105(a)(1)','4.607(b)'); -- [PLACE OF PERFORMANCE] 'UNITED STATES OUTLYING AREAS'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198897, prescription_id FROM Prescriptions WHERE prescription_name IN ('19.309(a)(1)','19.309(a)(2)','19.309(c)','19.708(a)','2015-00016','22.1310(a)(1)','22.1310(a)(2)','22.1408(a)','22.1408(b)','22.1605','22.1705(a)(2)','22.1803','22.1906','22.202','22.407(a)','22.407(b)','22.407(h)','222.7201(a)','223.570-2','225.1103(1)','225.302-6','225.372-2','225.7503(a)','225.7503(a)(1)','225.7503(a)(2)','225.7503(b)','225.7503(b)(1)','225.7503(b)(2)','225.7503(b)(3)','225.7503(b)(4)','23.804(b)','23.903','233.215-70','25.1102(a)','25.1102(c)','25.1102(c)(3)','25.1102(e)(1)','25.301-4','25.302-6','28.310','29.401-1','29.401-3','29.401-3(a)','29.401-3(b)','3.1004(b)','36.609-4','4.1105(a)(1)'); -- [PLACE OF PERFORMANCE] 'UNITED STATES (50 STATES PLUS DISTRICT OF COLUMBIA)'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198898, prescription_id FROM Prescriptions WHERE prescription_name IN ('2014-O0018','2014-O0020','2015-00009','2015-O0013','2015-O0016','225.371-5'); -- [PLACE OF PERFORMANCE] 'UNITED STATES CENTRAL COMMAND (USCENTCOM) AREA OF RESPONSIBILITY'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198899, prescription_id FROM Prescriptions WHERE prescription_name IN ('2014-O0020'); -- [PLACE OF PERFORMANCE] 'UNITED STATES EUROPEAN COMMAND (USEUCOM) THEATER OF OPERATION'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198900, prescription_id FROM Prescriptions WHERE prescription_name IN ('2014-O0020'); -- [PLACE OF PERFORMANCE] 'UNITED STATES AFRICA COMMAND (USAFRICOM) THEATER OF OPERATION'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198901, prescription_id FROM Prescriptions WHERE prescription_name IN ('2014-O0016','225.371-5'); -- [PLACE OF PERFORMANCE] 'UNITED STATES SOUTHERN COMMAND (USSOUTHCOM) THEATER OF OPERATION'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198902, prescription_id FROM Prescriptions WHERE prescription_name IN ('2014-O0020'); -- [PLACE OF PERFORMANCE] 'UNITED STATES PACIFIC COMMAND (USPACOM) THEATER OF OPERATION'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198903, prescription_id FROM Prescriptions WHERE prescription_name IN ('22.1310(a)(1)','22.1310(a)(2)','22.1408(a)','22.1408(b)','22.1605','22.305'); -- [PLACE OF PERFORMANCE] 'WAKE ISLAND'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198904, prescription_id FROM Prescriptions WHERE prescription_name IN ('14.201-6(h)','15.209(f)'); -- [PLACE OF PERFORMANCE] 'SPECIFIED BY THE GOVERNMENT'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198905, prescription_id FROM Prescriptions WHERE prescription_name IN ('22.1006(f)'); -- [PLACE OF PERFORMANCE] 'UNKNOWN'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198907, prescription_id FROM Prescriptions WHERE prescription_name IN ('223.7106','223.7106(a)','223.7106(b)','225-7011-3','225.7011-3','23.1005','23.1005(b)','23.1005(c)','23.705(a)','23.903','28.310','36.514','37.110(a)','37.110(b)','45.107(b)'); -- [PLACE TYPE] 'GOVERNMENT OWNED OR LEASED FACILITIES/INSTALLATIONS'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198908, prescription_id FROM Prescriptions WHERE prescription_name IN ('229.402-70(d)'); -- [PLACE TYPE] 'SPANISH-AMERICAN INSTALLATIONS AND FACILITIES'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198909, prescription_id FROM Prescriptions WHERE prescription_name IN ('23.903'); -- [PLACE TYPE] 'VEHICLES, GOVERNMENT OWNED OR LEASED'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198910, prescription_id FROM Prescriptions WHERE prescription_name IN ('228.370(b)'); -- [PLACE TYPE] 'FEDERAL AVIATION ADMINISTRATION REPAIR STATION'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198911, prescription_id FROM Prescriptions WHERE prescription_name IN ('236.570(b)(4)'); -- [PLACE TYPE] 'AT OR NEAR AIRFIELDS'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198912, prescription_id FROM Prescriptions WHERE prescription_name IN ('236.570(b)(1)'); -- [PLACE TYPE] 'NEAR OR ON NAVIGABLE WATERWAYS'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198914, prescription_id FROM Prescriptions WHERE prescription_name IN ('11.404(a)(2)','11.404(a)(3)'); -- [DELIVERY BASIS] 'ASSUMED DATE OF AWARD'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198915, prescription_id FROM Prescriptions WHERE prescription_name IN ('11.404(a)(2)','11.404(a)(3)'); -- [DELIVERY BASIS] 'ASSUMED DATE OF NOTICE OF AWARD'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198916, prescription_id FROM Prescriptions WHERE prescription_name IN ('11.404(a)(2)','11.404(a)(3)'); -- [DELIVERY BASIS] 'DATE OF CONTRACT'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198917, prescription_id FROM Prescriptions WHERE prescription_name IN ('11.404(a)(2)','11.404(a)(3)'); -- [DELIVERY BASIS] 'NOTICE OF AWARD'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198918, prescription_id FROM Prescriptions WHERE prescription_name IN ('11.404(b)'); -- [DELIVERY BASIS] 'NOTICE TO PROCEED BY A CERTAIN DAY'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198919, prescription_id FROM Prescriptions WHERE prescription_name IN ('11.404(a)(3)'); -- [DELIVERY BASIS] 'DESIRED AND REQUIRED TIME OF DELIVERY'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198920, prescription_id FROM Prescriptions WHERE prescription_name IN ('11.503(c)'); -- [DELIVERY BASIS] 'MORE THAN ONE COMPLETION DATE'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198921, prescription_id FROM Prescriptions WHERE prescription_name IN ('11.404(a)(2)','11.404(a)(3)'); -- [DELIVERY BASIS] 'REQUIRED TIME OF DELIVERY'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198922, prescription_id FROM Prescriptions WHERE prescription_name IN ('11.404(a)(2)','11.404(a)(3)','11.404(b)'); -- [DELIVERY BASIS] 'SPECIFIC CALENDAR DATES'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198924, prescription_id FROM Prescriptions WHERE prescription_name IN ('33.215(a)'); -- [CONTINUED PERFORMANCE] 'YES'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198926, prescription_id FROM Prescriptions WHERE prescription_name IN ('11.503(a)','11.503(b)'); -- [LIQUIDATED DAMAGES] 'YES'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198928, prescription_id FROM Prescriptions WHERE prescription_name IN ('246.370'); -- [SEPARATE DELIVERABLES] 'YES'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198931, prescription_id FROM Prescriptions WHERE prescription_name IN ('211.503'); -- [PACE OF WORK] 'NO'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198932, prescription_id FROM Prescriptions WHERE prescription_name IN ('2013-O0016','2013-O0017','232.7202'); -- [FOREIGN COUNTRY] 'AFGHANISTAN'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198934, prescription_id FROM Prescriptions WHERE prescription_name IN ('2014-O0005'); -- [FOREIGN COUNTRY] 'DJIBOUTI'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198935, prescription_id FROM Prescriptions WHERE prescription_name IN ('236.570(c)(1)','236.609-70(b)'); -- [FOREIGN COUNTRY] 'COUNTRY BORDERING THE ARABIAN GULF'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198936, prescription_id FROM Prescriptions WHERE prescription_name IN ('229.402-70(a)','229.402-70(a)(1)','246.710(2)'); -- [FOREIGN COUNTRY] 'GERMANY'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198937, prescription_id FROM Prescriptions WHERE prescription_name IN ('222.7201(b)','229.402-70(c)(1)'); -- [FOREIGN COUNTRY] 'ITALY'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198938, prescription_id FROM Prescriptions WHERE prescription_name IN ('236.609-70(b)'); -- [FOREIGN COUNTRY] 'JAPAN'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198939, prescription_id FROM Prescriptions WHERE prescription_name IN ('236.570(c)(2)'); -- [FOREIGN COUNTRY] 'KWAJALEIN ATOLL'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198940, prescription_id FROM Prescriptions WHERE prescription_name IN ('236.609-70(b)'); -- [FOREIGN COUNTRY] 'NATO COUNTRY'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198942, prescription_id FROM Prescriptions WHERE prescription_name IN ('222.7201(c)','228.370(e)','229.402-70(e)(1)'); -- [FOREIGN COUNTRY] 'SPAIN'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198943, prescription_id FROM Prescriptions WHERE prescription_name IN ('229.402-70(f)','229.402-70(g)'); -- [FOREIGN COUNTRY] 'UNITED KINGDOM'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198945, prescription_id FROM Prescriptions WHERE prescription_name IN ('2013-O0016'); -- [MILITARY TECHNICAL AGREEMENT] 'YES'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198947, prescription_id FROM Prescriptions WHERE prescription_name IN ('29.401-4(b)'); -- [STATE] 'NEW MEXICO'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198948, prescription_id FROM Prescriptions WHERE prescription_name IN ('222.7004'); -- [STATE] 'NONCONTIGUOUS STATE'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198949, prescription_id FROM Prescriptions WHERE prescription_name IN ('29.401-2'); -- [STATE] 'NORTH CAROLINA'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198951, prescription_id FROM Prescriptions WHERE prescription_name IN ('22.1705(b)'); -- [OUTSIDE U.S.] 'YES'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198953, prescription_id FROM Prescriptions WHERE prescription_name IN ('222.7004'); -- [UNEMPLOYMENT RATE] 'YES'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198955, prescription_id FROM Prescriptions WHERE prescription_name IN ('47.104-4','47.303-1(c)','47.303-17(f)','47.303-3(c)','47.305-16(a)','47.305-2(b)','47.305-3(b)(4)(ii)','47.305-3(f)(2)','47.305-6(e)','47.305-6(e)(1)','47.305-6(e)(2)','47.305-6(e)(3)'); -- [TRANSPORTATION REQUIREMENTS 1] 'F.O.B. ORIGIN'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198956, prescription_id FROM Prescriptions WHERE prescription_name IN ('47.303-6(c)','47.304-7(c)','47.305-2(b)','47.305-4(c)','47.305-6(e)','47.305-6(e)(1)','47.305-6(e)(2)','47.305-6(e)(3)'); -- [TRANSPORTATION REQUIREMENTS 1] 'F.O.B. DESTINATION'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198959, prescription_id FROM Prescriptions WHERE prescription_name IN ('29.401-1'); -- [TRANSPORTATION REQUIREMENTS 2] 'PLACE(S) OF DELIVERY ARE NOT KNOWN AT THE TIME OF CONTRACTING'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198960, prescription_id FROM Prescriptions WHERE prescription_name IN ('47.305-5(c)(1)'); -- [TRANSPORTATION REQUIREMENTS 2] 'EXACT DESTINATIONS ARE NOT KNOWN AND IS IMPRACTICABLE TO ESTABLISH TENTATIVE PLACES OF DELIVERY FOR PURPOSES OF EVALUATION OF TRANSPORTATION COSTS'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198961, prescription_id FROM Prescriptions WHERE prescription_name IN ('247.574(b)'); -- [TRANSPORTATION REQUIREMENTS 2] 'ITEMS TO BE SHIPPED ARE COMMISSARY OR EXCHANGE CARGOES TRANSPORTED OUTSIDE THE DEFENSE TRANSPORTATION SYSTEM'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198963, prescription_id FROM Prescriptions WHERE prescription_name IN ('47.207-5(b)'); -- [TRANSPORTATION REQUIREMENTS 3] 'CONTRACTOR IS REQUIRED TO FURNISH SUPERVISION, LABOR, OR MATERIALS'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198964, prescription_id FROM Prescriptions WHERE prescription_name IN ('47.207-5(e)'); -- [TRANSPORTATION REQUIREMENTS 3] 'CONTRACTOR IS RESPONSIBLE FOR LOADING AND UNLOADING SHIPMENTS'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198965, prescription_id FROM Prescriptions WHERE prescription_name IN ('47.207-5(f)'); -- [TRANSPORTATION REQUIREMENTS 3] 'CONTRACTOR IS RESPONSIBLE FOR RETURNING UNDELIVERED FREIGHT'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198966, prescription_id FROM Prescriptions WHERE prescription_name IN ('47.207-5(d)'); -- [TRANSPORTATION REQUIREMENTS 3] 'CONTRACTOR RESPONSIBLE FOR RECEIPT OF SHIPMENT'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198967, prescription_id FROM Prescriptions WHERE prescription_name IN ('47.207-1(d)'); -- [TRANSPORTATION REQUIREMENTS 3] 'OFFEROR RESPONSIBLE FOR FAMILIARIZATION WITH CONDITIONS UNDER WHICH AND WHERE THE SERVICES ARE TO BE PERFORMED'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198968, prescription_id FROM Prescriptions WHERE prescription_name IN ('47.208-2'); -- [TRANSPORTATION REQUIREMENTS 3] 'CONTRACTOR IS RESPONSIBLE FOR ISSUANCE OF ADVANCED NOTICE OF SHIPMENT'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198969, prescription_id FROM Prescriptions WHERE prescription_name IN ('47.207-1(c)'); -- [TRANSPORTATION REQUIREMENTS 3] 'CONTRACTOR IS TO INSPECT THE SHIPPING, RECEIVING, OR OTHER FACILITIES'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198971, prescription_id FROM Prescriptions WHERE prescription_name IN ('47.207-8(a)(2)(i)'); -- [TRANSPORTATION REQUIREMENTS 4] 'GOVERNMENT FURNISHES EQUIPMENT WITH OR WITHOUT OPERATORS'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198972, prescription_id FROM Prescriptions WHERE prescription_name IN ('47.207-8(a)(1)'); -- [TRANSPORTATION REQUIREMENTS 4] 'GOVERNMENT IS RESPONSIBLE FOR NOTIFYING CONTRACTOR OF SPECIFIC SERVICE TIMES OR UNUSUAL SHIPMENTS'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198973, prescription_id FROM Prescriptions WHERE prescription_name IN ('47.305-12(a)(2)'); -- [TRANSPORTATION REQUIREMENTS 4] 'GOVERNMENT IS RESPONSIBLE FOR GFP TRANSPORTATION ARRANGEMENTS AND COSTS'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198974, prescription_id FROM Prescriptions WHERE prescription_name IN ('47.207-3(d)(2)'); -- [TRANSPORTATION REQUIREMENTS 4] 'GOVERNMENT WILL IDENTIFY COMMODITIES OR TYPES OF SHIPMENTS THAT HAVE BEEN IDENTIFIED FOR EXCLUSION'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198976, prescription_id FROM Prescriptions WHERE prescription_name IN ('247.574(e)','247.574(f)'); -- [TRANSPORTATION REQUIREMENTS 5] 'COVERED VESSEL FOR CARRIAGE OF DOD CARGO REQUIRED'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198977, prescription_id FROM Prescriptions WHERE prescription_name IN ('46.314'); -- [TRANSPORTATION REQUIREMENTS 5] 'DOMESTIC AIR CARRIERS'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198978, prescription_id FROM Prescriptions WHERE prescription_name IN ('46.314'); -- [TRANSPORTATION REQUIREMENTS 5] 'DOMESTIC FREIGHT FORWARDER'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198979, prescription_id FROM Prescriptions WHERE prescription_name IN ('46.314'); -- [TRANSPORTATION REQUIREMENTS 5] 'DOMESTIC WATER CARRIERS'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198980, prescription_id FROM Prescriptions WHERE prescription_name IN ('46.314','47.405'); -- [TRANSPORTATION REQUIREMENTS 5] 'INTERNATIONAL AIR CARRIERS'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198981, prescription_id FROM Prescriptions WHERE prescription_name IN ('247.574(a)','247.574(b)','247.574(b)(1)','247.574(b)(2)','247.574(b)(3)','46.314','47.507(a)','47.507(a)(2)','47.507(a)(3)'); -- [TRANSPORTATION REQUIREMENTS 5] 'INTERNATIONAL OCEAN CARRIERS'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198982, prescription_id FROM Prescriptions WHERE prescription_name IN ('46.314'); -- [TRANSPORTATION REQUIREMENTS 5] 'MOTOR (INCLUDING BUS)'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198983, prescription_id FROM Prescriptions WHERE prescription_name IN ('46.314','47.305-15(a)(2)'); -- [TRANSPORTATION REQUIREMENTS 5] 'RAIL'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198984, prescription_id FROM Prescriptions WHERE prescription_name IN ('47.507(a)(2)'); -- [TRANSPORTATION REQUIREMENTS 5] 'PRIVATELY OWNED U.S. FLAG COMMERCIAL VESSEL'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198985, prescription_id FROM Prescriptions WHERE prescription_name IN ('247.574(f)'); -- [TRANSPORTATION REQUIREMENTS 5] 'U.S. FLAG VESSEL'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198986, prescription_id FROM Prescriptions WHERE prescription_name IN ('247.574(d)','247.574(f)'); -- [TRANSPORTATION REQUIREMENTS 5] 'TIME CHARTER'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198987, prescription_id FROM Prescriptions WHERE prescription_name IN ('247.574(c)'); -- [TRANSPORTATION REQUIREMENTS 5] 'CONTRACTOR DOES NOT ANTICIPATE SUPPLIES WILL BE TRANSPORTED BY SEA'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198989, prescription_id FROM Prescriptions WHERE prescription_name IN ('47.207-4(a)(1)'); -- [TRANSPORTATION REQUIREMENTS 6] 'SHIPPING ACTIVITY DETERMINES THE WEIGHT OF SHIPMENTS OF FREIGHT'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198990, prescription_id FROM Prescriptions WHERE prescription_name IN ('47.207-4(a)(2)'); -- [TRANSPORTATION REQUIREMENTS 6] 'CONTRACTOR IS RESPONSIBLE FOR DETERMINING THE NET WEIGHT OF SHIPMENTS'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198991, prescription_id FROM Prescriptions WHERE prescription_name IN ('47.207-4(a)(1)','47.207-4(a)(2)','47.207-6(c)(6)'); -- [TRANSPORTATION REQUIREMENTS 6] 'WEIGHT OF SHIPMENTS OF FREIGHT IS NOT KNOWN AT THE TIME OF SHIPMENT'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198992, prescription_id FROM Prescriptions WHERE prescription_name IN ('47.207-3(e)(2)','47.207-6(c)(6)'); -- [TRANSPORTATION REQUIREMENTS 6] 'WEIGHTS OR QUANTITIES ARE ESTIMATED'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198993, prescription_id FROM Prescriptions WHERE prescription_name IN ('47.207-6(c)(6)','47.305-16(d)(2)'); -- [TRANSPORTATION REQUIREMENTS 6] 'SPECIFIC QUANTITIES TO BE SHIPPED TO EACH LOCATION ARE UNKNOWN'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198995, prescription_id FROM Prescriptions WHERE prescription_name IN ('47.207-6(c)(5)(ii)'); -- [TRANSPORTATION REQUIREMENTS 7] 'MULTIPLE SHIPMENTS ARE TENDERED AT ONE TIME TO THE CONTRACTOR FOR TRANSPORTATION FROM ONE ORIGIN TO TWO OR MORE CONSIGNEES ALONG THE ROUTE BETWEEN ORIGIN AND LAST DESTINATION'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198996, prescription_id FROM Prescriptions WHERE prescription_name IN ('47.207-6(c)(5)(i)'); -- [TRANSPORTATION REQUIREMENTS 7] 'MULTIPLE SHIPMENTS ARE TENDERED AT ONE TIME TO THE CONTRACTOR FOR TRANSPORTATION FROM ONE ORIGIN TO TWO OR MORE COSIGNEES AT THE SAME DESTINATION'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198998, prescription_id FROM Prescriptions WHERE prescription_name IN ('47.305-15(a)(2)','47.305-16(a)'); -- [TRANSPORTATION REQUIREMENT 8] 'SHIPPED IN CARLOAD LOTS'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 198999, prescription_id FROM Prescriptions WHERE prescription_name IN ('47.305-16(a)'); -- [TRANSPORTATION REQUIREMENT 8] 'SHIPPED IN TRUCKLOADS'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199000, prescription_id FROM Prescriptions WHERE prescription_name IN ('47.208-2'); -- [TRANSPORTATION REQUIREMENT 8] 'SHIPPED IN CARLOADS OR TRUCKLOADS TO DOD INSTALLATIONS, OR CIVILIAN AGENCIES'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199002, prescription_id FROM Prescriptions WHERE prescription_name IN ('47.305-6(e)','47.305-6(e)(1)','47.305-6(e)(2)','47.305-6(e)(3)'); -- [TRANSPORTATION REQUIREMENTS 9] 'SUPPLIES TO BE EXPORTED THROUGH CONUS PORTS'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199003, prescription_id FROM Prescriptions WHERE prescription_name IN ('47.305-6(e)(1)'); -- [TRANSPORTATION REQUIREMENTS 9] 'CONUS PORTS OF EXPORT ARE DOD WATER TERMINALS'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199005, prescription_id FROM Prescriptions WHERE prescription_name IN ('229.402-70(b)'); -- [TRANSPORTATION REQUIREMENTS 10] 'REQUIRES IMPORT OF U.S. MANUFACTURED PRODUCTS INTO GERMANY'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199006, prescription_id FROM Prescriptions WHERE prescription_name IN ('25.1101(e)'); -- [TRANSPORTATION REQUIREMENTS 10] 'REQUIRES IMPORT OF SUPPLIES INTO THE UNITED STATES (50 STATES, THE DISTRICT OF COLUMBIA AND OUTLYING AREAS)'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199007, prescription_id FROM Prescriptions WHERE prescription_name IN ('229.402-70(d)'); -- [TRANSPORTATION REQUIREMENTS 10] 'REQUIRES IMPORT OF SUPPPLIES INTO SPAIN'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199008, prescription_id FROM Prescriptions WHERE prescription_name IN ('225.1101(4)'); -- [TRANSPORTATION REQUIREMENTS 10] 'SUPPLIES WILL NOT ENTER US CUSTOMS TERRITORY'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199010, prescription_id FROM Prescriptions WHERE prescription_name IN ('25.1101(e)'); -- [TRANSPORTATION REQUIREMENTS 11] 'DUTY-FREE ENTRY MAY BE OBTAINED'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199011, prescription_id FROM Prescriptions WHERE prescription_name IN ('25.1101(e)'); -- [TRANSPORTATION REQUIREMENTS 11] 'WAIVING DUTY IS COST EFFECTIVE'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199013, prescription_id FROM Prescriptions WHERE prescription_name IN ('247.207(2)'); -- [SHIPPING STATEMENTS] 'SUPPLIES ARE BEING SHIPPED UNDER BILLS OF LADING AND DOMESTIC ROUTE ORDER UNDER F.O.B. ORIGIN CONTRACTS, EXPORT RELEASE'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199014, prescription_id FROM Prescriptions WHERE prescription_name IN ('47.305-6(f)(2)'); -- [SHIPPING STATEMENTS] 'SHIPMENTS WILL BE CONSIGNED TO DOD AIR OR WATER TERMINAL TRANSSHIPMENT POINTS'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199015, prescription_id FROM Prescriptions WHERE prescription_name IN ('216.506(d)','216.506(d)(1)','216.506(d)(2)','247.271-3','247.271-3(a)','247.271-3(a)(2)','247.271-3(b)','247.271-3(d)','247.271-3(e)','247.271-3(g)','247.271-3(h)','247.271-3(i)','247.271-3(j)','247.271-3(k)','247.271-3(l)','247.271-3(m)','247.271-3(n)'); -- [SHIPPING STATEMENTS] 'INVOLVES PERFORMANCE OF INTRA-CITY OR INTRA-AREA MOVEMENT'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199016, prescription_id FROM Prescriptions WHERE prescription_name IN ('47.207-1(a)'); -- [SHIPPING STATEMENTS] 'IT IS IN THE GOVERNMENT'S INTEREST NOT TO APPLY REQUIREMENTS TO OBTAIN AUTHORITY FOR OPERATING WITHIN THE STATE'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199017, prescription_id FROM Prescriptions WHERE prescription_name IN ('47.305-9(b)(1)'); -- [SHIPPING STATEMENTS] 'DIFFERENT FREIGHT CLASSIFICATIONS APPLY'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199019, prescription_id FROM Prescriptions WHERE prescription_name IN ('47.303-5(c)'); -- [FOB ORIGIN DESIGNATIONS] 'F.O.B. ORIGIN, WITH DIFFERENTIALS'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199020, prescription_id FROM Prescriptions WHERE prescription_name IN ('47.303-4(c)'); -- [FOB ORIGIN DESIGNATIONS] 'F.O.B. ORIGIN FREIGHT PREPAID'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199021, prescription_id FROM Prescriptions WHERE prescription_name IN ('47.303-2(c)'); -- [FOB ORIGIN DESIGNATIONS] 'F.O.B. ORIGIN, CONTRACTOR'S FACILITY'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199023, prescription_id FROM Prescriptions WHERE prescription_name IN ('47.303-7(c)'); -- [WITHIN CONSIGNEE'S PREMISES] 'YES'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199025, prescription_id FROM Prescriptions WHERE prescription_name IN ('47.303-10(c)'); -- [FOB DESIGNATION] 'F.O.B. INLAND CARRIER, POINT OF EXPORTATION'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199026, prescription_id FROM Prescriptions WHERE prescription_name IN ('47.303-11(c)'); -- [FOB DESIGNATION] 'F.O.B. INLAND POINT, COUNTRY OF IMPORTATION'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199027, prescription_id FROM Prescriptions WHERE prescription_name IN ('47.303-9(c)'); -- [FOB DESIGNATION] 'F.O.B VESSEL, PORT OF SHIPMENT'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199028, prescription_id FROM Prescriptions WHERE prescription_name IN ('47.303-15(c)'); -- [FOB DESIGNATION] 'F.O.B. DESIGNATED AIR CARRIER'S TERMINAL, POINT OF EXPORTATION'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199029, prescription_id FROM Prescriptions WHERE prescription_name IN ('47.303-16(c)'); -- [FOB DESIGNATION] 'F.O.B. DESIGNATED AIR CARRIER'S TERMINAL, POINT OF IMPORTATION'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199030, prescription_id FROM Prescriptions WHERE prescription_name IN ('47.303-3(c)'); -- [FOB DESIGNATION] 'F.O.B. FREIGHT ALLOWED'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199031, prescription_id FROM Prescriptions WHERE prescription_name IN ('47.303-13(c)'); -- [FOB DESIGNATION] 'COST AND FREIGHT (C&F) DESTINATION'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199032, prescription_id FROM Prescriptions WHERE prescription_name IN ('47.303-14(c)'); -- [FOB DESIGNATION] 'COST, INSURANCE, FREIGHT (CIF) DESTINATION'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199033, prescription_id FROM Prescriptions WHERE prescription_name IN ('47.303-12(c)'); -- [FOB DESIGNATION] 'EX DOCK, PIER, OR WAREHOUSE, PORT OF IMPORTATION'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199034, prescription_id FROM Prescriptions WHERE prescription_name IN ('47.303-8(c)'); -- [FOB DESIGNATION] 'FOREIGN AGRICULTURAL SERVICE (F.A.S.) VESSEL, PORT OF SHIPMENT'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199036, prescription_id FROM Prescriptions WHERE prescription_name IN ('47.507(a)','47.507(a)(2)','47.507(a)(3)'); -- [CARGO PREFERENCE ACT] 'YES'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199038, prescription_id FROM Prescriptions WHERE prescription_name IN ('201.602-70'); -- [ACO RESPONSIBILITIES] 'APPOINTMENT OF A CONTRACTING OFFICER'S REPRESENTATIVE IS ANTICIPATED'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199039, prescription_id FROM Prescriptions WHERE prescription_name IN ('246.370'); -- [ACO RESPONSIBILITIES] 'CONTRACT ADMINISTRATION IS RETAINED BY THE CONTRACTING OFFICE'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199040, prescription_id FROM Prescriptions WHERE prescription_name IN ('246.370'); -- [ACO RESPONSIBILITIES] 'ONLY BASE, POST, CAMP OR STATION ACTIVITIES ARE AUTHORIZED TO ISSUE ORDERS'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199042, prescription_id FROM Prescriptions WHERE prescription_name IN ('225.1101(10)(i)','225.1101(10)(i)(A)','225.1101(10)(i)(B)','225.1101(10)(i)(C)','225.1101(10)(i)(D)','225.1101(10)(i)(E)','225.1101(10)(i)(F)','25.1101(b)(1)(i)','25.1101(b)(1)(ii)','25.1101(b)(1)(iii)','25.1101(b)(1)(iv)'); -- [FUNDING FY] 'FY2004 FUNDS'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199043, prescription_id FROM Prescriptions WHERE prescription_name IN ('225.1101(10)(i)','225.1101(10)(i)(A)','225.1101(10)(i)(B)','225.1101(10)(i)(C)','225.1101(10)(i)(D)','225.1101(10)(i)(E)','225.1101(10)(i)(F)','25.1101(b)(1)(i)','25.1101(b)(1)(ii)','25.1101(b)(1)(iii)','25.1101(b)(1)(iv)'); -- [FUNDING FY] 'FY2005 FUNDS'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199044, prescription_id FROM Prescriptions WHERE prescription_name IN ('225.1101(10)(i)','225.1101(10)(i)(A)','225.1101(10)(i)(B)','225.1101(10)(i)(C)','225.1101(10)(i)(D)','225.1101(10)(i)(E)','225.1101(10)(i)(F)','25.1101(b)(1)(i)','25.1101(b)(1)(ii)','25.1101(b)(1)(iii)','25.1101(b)(1)(iv)'); -- [FUNDING FY] 'FY2006 FUNDS'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199045, prescription_id FROM Prescriptions WHERE prescription_name IN ('225.1101(10)(i)','225.1101(10)(i)(A)','225.1101(10)(i)(B)','225.1101(10)(i)(C)','225.1101(10)(i)(D)','225.1101(10)(i)(E)','225.1101(10)(i)(F)','25.1101(b)(1)(i)','25.1101(b)(1)(ii)','25.1101(b)(1)(iii)','25.1101(b)(1)(iv)'); -- [FUNDING FY] 'FY2007 FUNDS'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199046, prescription_id FROM Prescriptions WHERE prescription_name IN ('225.1101(10)(i)','225.1101(10)(i)(A)','225.1101(10)(i)(B)','225.1101(10)(i)(C)','225.1101(10)(i)(D)','225.1101(10)(i)(E)','225.1101(10)(i)(F)','25.1101(b)(1)(i)','25.1101(b)(1)(ii)','25.1101(b)(1)(iii)','25.1101(b)(1)(iv)'); -- [FUNDING FY] 'FY2008 FUNDS'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199047, prescription_id FROM Prescriptions WHERE prescription_name IN ('225.1101(10)(i)','225.1101(10)(i)(A)','225.1101(10)(i)(B)','225.1101(10)(i)(C)','225.1101(10)(i)(D)','225.1101(10)(i)(E)','225.1101(10)(i)(F)','25.1101(b)(1)(i)','25.1101(b)(1)(ii)','25.1101(b)(1)(iii)','25.1101(b)(1)(iv)'); -- [FUNDING FY] 'FY2009 FUNDS'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199048, prescription_id FROM Prescriptions WHERE prescription_name IN ('225.1101(10)(i)','225.1101(10)(i)(A)','225.1101(10)(i)(B)','225.1101(10)(i)(C)','225.1101(10)(i)(D)','225.1101(10)(i)(E)','225.1101(10)(i)(F)','25.1101(b)(1)(i)','25.1101(b)(1)(ii)','25.1101(b)(1)(iii)','25.1101(b)(1)(iv)'); -- [FUNDING FY] 'FY2010 FUNDS'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199049, prescription_id FROM Prescriptions WHERE prescription_name IN ('225.1101(10)(i)','225.1101(10)(i)(A)','225.1101(10)(i)(B)','225.1101(10)(i)(C)','225.1101(10)(i)(D)','225.1101(10)(i)(E)','225.1101(10)(i)(F)','25.1101(b)(1)(i)','25.1101(b)(1)(ii)','25.1101(b)(1)(iii)','25.1101(b)(1)(iv)'); -- [FUNDING FY] 'FY2011 FUNDS'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199050, prescription_id FROM Prescriptions WHERE prescription_name IN ('225.1101(10)(i)','225.1101(10)(i)(A)','225.1101(10)(i)(B)','225.1101(10)(i)(C)','225.1101(10)(i)(D)','225.1101(10)(i)(E)','225.1101(10)(i)(F)','25.1101(b)(1)(i)','25.1101(b)(1)(ii)','25.1101(b)(1)(iii)','25.1101(b)(1)(iv)'); -- [FUNDING FY] 'FY2012 FUNDS'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199051, prescription_id FROM Prescriptions WHERE prescription_name IN ('225.1101(10)(i)','225.1101(10)(i)(A)','225.1101(10)(i)(B)','225.1101(10)(i)(C)','225.1101(10)(i)(D)','225.1101(10)(i)(E)','225.1101(10)(i)(F)','25.1101(b)(1)(i)','25.1101(b)(1)(ii)','25.1101(b)(1)(iii)','25.1101(b)(1)(iv)'); -- [FUNDING FY] 'FY2013 FUNDS'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199052, prescription_id FROM Prescriptions WHERE prescription_name IN ('225.1101(10)(i)','225.1101(10)(i)(A)','225.1101(10)(i)(B)','225.1101(10)(i)(C)','225.1101(10)(i)(D)','225.1101(10)(i)(E)','225.1101(10)(i)(F)','25.1101(b)(1)(i)','25.1101(b)(1)(ii)','25.1101(b)(1)(iii)','25.1101(b)(1)(iv)'); -- [FUNDING FY] 'FY2014 FUNDS'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199053, prescription_id FROM Prescriptions WHERE prescription_name IN ('225.1101(10)(i)','225.1101(10)(i)(A)','225.1101(10)(i)(B)','225.1101(10)(i)(C)','225.1101(10)(i)(D)','225.1101(10)(i)(E)','225.1101(10)(i)(F)','25.1101(b)(1)(i)','25.1101(b)(1)(ii)','25.1101(b)(1)(iii)','25.1101(b)(1)(iv)'); -- [FUNDING FY] 'FY2015 FUNDS'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199054, prescription_id FROM Prescriptions WHERE prescription_name IN ('2016-00002'); -- [FUNDING FY] 'FY 2016 FUNDS'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199055, prescription_id FROM Prescriptions WHERE prescription_name IN ('3.202'); -- [FUNDING FY] 'NON-APPROPRIATED FUNDS'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199058, prescription_id FROM Prescriptions WHERE prescription_name IN ('32.706-2(a)'); -- [FULLY OR INCREMENTAL FUNDING] 'FULLY FUNDED'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199059, prescription_id FROM Prescriptions WHERE prescription_name IN ('232.705-70','249.501-70','32.706-2(b)'); -- [FULLY OR INCREMENTAL FUNDING] 'INCREMENTALLY FUNDED (ONE LINE ITEM ONLY)'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199060, prescription_id FROM Prescriptions WHERE prescription_name IN ('232.705-70','249.501-70','32.706-2(b)'); -- [FULLY OR INCREMENTAL FUNDING] 'INCREMENTALLY FUNDED (MORE THAN ONE LINE ITEM ONLY)'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199061, prescription_id FROM Prescriptions WHERE prescription_name IN ('13.302-5(b)','32.412(a)','32.412(b)','32.412(c)','32.412(d)','32.412(e)','32.412(f)'); -- [FINANCING ARRANGEMENTS] 'ADVANCE PAYMENTS'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199062, prescription_id FROM Prescriptions WHERE prescription_name IN ('32.206(g)'); -- [FINANCING ARRANGEMENTS] 'INSTALLMENT PAYMENTS'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199063, prescription_id FROM Prescriptions WHERE prescription_name IN ('232.502-4-70(a)'); -- [FINANCING ARRANGEMENTS] 'PROGRESS PAYMENTS'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199064, prescription_id FROM Prescriptions WHERE prescription_name IN ('32.206(g)'); -- [FINANCING ARRANGEMENTS] 'FOR COMMERCIAL INTERIM PAYMENTS AND COMMERCIAL ADVANCED PAYMENTS THE TERMS IDENTIFIED UNDER FAR 32.202-1(b) EXIST'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199066, prescription_id FROM Prescriptions WHERE prescription_name IN ('232.908'); -- [BILLING ARRANGEMENTS] 'CRITERIA FOR PROMPT PAYMENT MET'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199067, prescription_id FROM Prescriptions WHERE prescription_name IN ('32.1110(a)(2)','32.1110(d)'); -- [BILLING ARRANGEMENTS] 'ELECTRONIC FUNDS TRANSFER APPLIES'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199068, prescription_id FROM Prescriptions WHERE prescription_name IN ('32.1110(a)(1)','32.1110(d)'); -- [BILLING ARRANGEMENTS] 'ELECTRONIC FUNDS TRANSFER EXCEPTION'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199069, prescription_id FROM Prescriptions WHERE prescription_name IN ('13.404'); -- [BILLING ARRANGEMENTS] 'THE CONDITIONS REQUIRED FOR USE OF FAST PAYMENTS AS SET FORTH IN FAR 13.402 HAVE BEEN MET'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199070, prescription_id FROM Prescriptions WHERE prescription_name IN ('232.1110','32.1110(d)','4.1105(a)(1)'); -- [BILLING ARRANGEMENTS] 'GOVERNMENT WIDE PURCHASE CARD USED'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199071, prescription_id FROM Prescriptions WHERE prescription_name IN ('32.908(c)','32.908(c)(3)'); -- [BILLING ARRANGEMENTS] 'PAYMENT TERMS AND LATE PAYMENT PENALTIES ARE ESTABLISHED BY OTHER GOVERNMENTAL AUTHORITY'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199072, prescription_id FROM Prescriptions WHERE prescription_name IN ('32.1110(b)(2)'); -- [BILLING ARRANGEMENTS] 'TWO OR MORE TERMS AUTHORIZING THE TEMPORARY WITHHOLDING OF AMOUNTS DUE'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199073, prescription_id FROM Prescriptions WHERE prescription_name IN ('232.7004(b)'); -- [BILLING ARRANGEMENTS] 'USE OF THIRD PARTY OR OTHER EXEMPTED PAYMENT SYSTEM FOR TRANSPORTATION PERMITTED'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199074, prescription_id FROM Prescriptions WHERE prescription_name IN ('232.7004(b)'); -- [BILLING ARRANGEMENTS] 'USE OF TRICARE ENCOUNTER DATA SYSTEM PERMITTED'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199075, prescription_id FROM Prescriptions WHERE prescription_name IN ('204.7104-1(b)(3)(iv)'); -- [BILLING ARRANGEMENTS] 'WITHHOLDING PAYMENT REQUEST UNTIL ALL NOT SEPARATELY PRICED (NSP) SUBLINE ITEMS DELIVERED'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199076, prescription_id FROM Prescriptions WHERE prescription_name IN ('204.7109(a)'); -- [PAYMENT INSTRUCTIONS] 'IDENTIFICATION OF CLINS ON PAYMENT REQUEST (PGI 204.7108)'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199077, prescription_id FROM Prescriptions WHERE prescription_name IN ('204.7109(a)'); -- [PAYMENT INSTRUCTIONS] 'STANDARD PAYMENT INSTRUCTIONS APPLY (PGI 204.7108)'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199078, prescription_id FROM Prescriptions WHERE prescription_name IN ('232.412-70(a)'); -- [PAYMENT INSTRUCTIONS] 'PAYMENT INSTRUCTIONS SUBJECT TO THE TERMS OF AN ADVANCE PAYMENT POOL AGREEMENT'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199080, prescription_id FROM Prescriptions WHERE prescription_name IN ('32.412(d)'); -- [RAPID LIQUIDATION] 'YES'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199082, prescription_id FROM Prescriptions WHERE prescription_name IN ('32.706-1(a)'); -- [CONTRACT PRIOR TO FUNDS] 'YES'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199084, prescription_id FROM Prescriptions WHERE prescription_name IN ('246.370'); -- [BASE, POST, CAMP OR STATION] 'YES'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199086, prescription_id FROM Prescriptions WHERE prescription_name IN ('4.1705(a)','4.1705(b)'); -- [TYPE OF FUNDS] 'FUNDED ENTIRELY WITH DOD FUNDS'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199087, prescription_id FROM Prescriptions WHERE prescription_name IN ('236.570(c)(1)','236.570(c)(2)','236.570(d)','236.609-70(b)'); -- [TYPE OF FUNDS] 'MILITARY CONSTRUCTION APPROPRIATIONS'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199088, prescription_id FROM Prescriptions WHERE prescription_name IN ('229.170-4'); -- [TYPE OF FUNDS] 'U.S. ASSISTANCE APPROPRIATIONS FUNDS'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199089, prescription_id FROM Prescriptions WHERE prescription_name IN ('3.202'); -- [TYPE OF FUNDS] 'NON-APPROPRIATED FUNDS'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199090, prescription_id FROM Prescriptions WHERE prescription_name IN ('249.501-70'); -- [TYPE OF FUNDS] 'PRODUCTION FUNDS'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199091, prescription_id FROM Prescriptions WHERE prescription_name IN ('249.501-70'); -- [TYPE OF FUNDS] 'RESEARCH, DEVELOPMENT, TEST AND EVALUATION (RDT&E) FUNDS'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199092, prescription_id FROM Prescriptions WHERE prescription_name IN ('225.7307(a)','225.7307(b)','232.502-4-70(a)'); -- [TYPE OF FUNDS] 'FOREIGN MILITARY SALES FUNDS'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199094, prescription_id FROM Prescriptions WHERE prescription_name IN ('3.1004(b)'); -- [LEGISLATIVE SOURCE OF FUNDS] 'DISASTER ASSISTANCE FUNDS'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199095, prescription_id FROM Prescriptions WHERE prescription_name IN ('28.309(a)'); -- [LEGISLATIVE SOURCE OF FUNDS] 'FOREIGN ASSISTANCE ACT OF 1961'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199096, prescription_id FROM Prescriptions WHERE prescription_name IN ('2012-O0004'); -- [LEGISLATIVE SOURCE OF FUNDS] 'FUNDS MADE AVAILABLE BY DIVISION A OF THE CONSOLIDATED APPROPRIATIONS ACT, 2012 (PUB. L. 112-74)'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199097, prescription_id FROM Prescriptions WHERE prescription_name IN ('2012-O0007'); -- [LEGISLATIVE SOURCE OF FUNDS] 'FUNDS MADE AVAILABLE BY DIVISION H OF THE CONSOLIDATED APPROPRIATIONS ACT, 2012 (PUB. 112.74)'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199098, prescription_id FROM Prescriptions WHERE prescription_name IN ('2013-O0010'); -- [LEGISLATIVE SOURCE OF FUNDS] 'FUNDS MADE AVAILABLE BY THE CONSOLIDATED AND FURTHER CONTINUING APPROPRIATIONS ACT, 2013 (Pub. L. 113-6)'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199099, prescription_id FROM Prescriptions WHERE prescription_name IN ('2014-O0004'); -- [LEGISLATIVE SOURCE OF FUNDS] 'FUNDS MADE AVAILABLE BY THE CONTINUING APPROPRIATIONS ACT, 2014 (PUB. L. 113-46)'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199100, prescription_id FROM Prescriptions WHERE prescription_name IN ('2014-OO0009'); -- [LEGISLATIVE SOURCE OF FUNDS] 'FUNDS APPROPRIATED BY THE DEPARTMENT OF DEFENSE APPROPRIATIONS ACT, 2014 AND BY THE MILITARY CONSTRUCTION AND VETERANS AFFAIRS AND RELATED AGENCIES APPROPRIATIONS ACT, 2014 (PUBL L. 113-76, DIVISIONS C AND J)'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199101, prescription_id FROM Prescriptions WHERE prescription_name IN ('2013-O0006'); -- [LEGISLATIVE SOURCE OF FUNDS] 'FUNDS MADE AVAILABLE BY THE CONTINUING APPROPRIATIONS RESOLUTION, 2013 (PUB. L 112-175) DOD MILITARY CONSTRUCTION APPROPRIATIONS'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199102, prescription_id FROM Prescriptions WHERE prescription_name IN ('12.301(b)(4)(ii)','14.201-7(a)(1)','14.201-7(a)(2)','15.209(b)(1)','15.209(b)(2)','2013-O0019','25.1102(a)','25.1102(b)(1)','25.1102(b)(2)','25.1102(c)','25.1102(c)(3)','25.1102(e)(1)','3.907-7'); -- [LEGISLATIVE SOURCE OF FUNDS] 'RECOVERY ACT FUNDS'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199103, prescription_id FROM Prescriptions WHERE prescription_name IN ('2015-O0010'); -- [LEGISLATIVE SOURCE OF FUNDS] 'FUNDS MADE AVAILABLE BY THE FINANCIAL SERVICES AND GENERAL GOVERNMENT APPROPRIATIONS ACT, 2015 (DIVISION E OF THE CONSOLIDATED AND FURTHER CONTINUING APPROPRIATIONS ACT, 2015, PUB. L. 113-235), OR ANY OTHER ACT'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199104, prescription_id FROM Prescriptions WHERE prescription_name IN ('2015-OO005'); -- [LEGISLATIVE SOURCE OF FUNDS] 'FUNDS MADE AVAILABLE BY THE CONSOLIDATED AND CONTINUING APPROPRIATIONS ACT, 2015 (PUB. L. 113-235)'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199105, prescription_id FROM Prescriptions WHERE prescription_name IN ('2016-0002'); -- [LEGISLATIVE SOURCE OF FUNDS] 'FUNDS MADE AVAILABLE BY THE CONTINUING APPROPRIATIONS ACT, 2016 (Pub. L. 114-53) OR ANY OTHER 2016 APPROPRIATIONS ACT THAT EXTENDS TO FY 2016 FUNDS THE SAME PROHIBITIONS AS CONTAINED IN SECTIONS 744 AND 745 OF DIVISION E, TITLE VII, OF THE CONSOLIDATED AND FURTHER CONTINUING APPROPRIATONS ACT, 2015 (Pub.L. 113-235).'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199107, prescription_id FROM Prescriptions WHERE prescription_name IN ('32.706-1(b)'); -- [ANNUAL FUNDS EXTENDED] 'YES'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199109, prescription_id FROM Prescriptions WHERE prescription_name IN ('249.501-70'); -- [LIABILITY FOR SPECIAL COSTS] 'YES'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199111, prescription_id FROM Prescriptions WHERE prescription_name IN ('PGI 204.7108(d)(10)'); -- [ACRN CONTRACT WIDE] 'BY CANCELLATION DATE'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199112, prescription_id FROM Prescriptions WHERE prescription_name IN ('PGI 204.7108(d)(9)'); -- [ACRN CONTRACT WIDE] 'BY FISCAL YEAR'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199113, prescription_id FROM Prescriptions WHERE prescription_name IN ('PGI 204.7108(d)(11)'); -- [ACRN CONTRACT WIDE] 'BY PRORATION'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199114, prescription_id FROM Prescriptions WHERE prescription_name IN ('PGI 204.7108(d)(7)'); -- [ACRN CONTRACT WIDE] 'BY SEQUENTIAL ORDER'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199115, prescription_id FROM Prescriptions WHERE prescription_name IN ('PGI 204.7108(d)(8)'); -- [ACRN CONTRACT WIDE] 'BY SPECIFIED ORDER'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199118, prescription_id FROM Prescriptions WHERE prescription_name IN ('PGI 204.7108(d)(5)'); -- [ACRN BY LINE ITEM] 'BY CANCELLATION DATE'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199119, prescription_id FROM Prescriptions WHERE prescription_name IN ('PGI 204.7108(d)(6)'); -- [ACRN BY LINE ITEM] 'BY PRORATION'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199120, prescription_id FROM Prescriptions WHERE prescription_name IN ('PGI 204.7108(d)(4)'); -- [ACRN BY LINE ITEM] 'OLDEST FIRST'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199121, prescription_id FROM Prescriptions WHERE prescription_name IN ('PGI 204.7108(d)(2)'); -- [ACRN BY LINE ITEM] 'BY SEQUENTIAL ORDER'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199122, prescription_id FROM Prescriptions WHERE prescription_name IN ('PGI 204.7108(d)(3)'); -- [ACRN BY LINE ITEM] 'BY SPECIFIED ORDER'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199123, prescription_id FROM Prescriptions WHERE prescription_name IN ('PGI 204.7108(d)(1)'); -- [ACRN BY LINE ITEM] 'ONE ACRN PER CONTRACT LINE ITEM NUMBER (CLIN)'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199124, prescription_id FROM Prescriptions WHERE prescription_name IN ('32.412(a)','32.412(b)','32.412(c)','32.412(d)','32.412(e)','32.412(f)'); -- [ADVANCED PAYMENT CONDITIONS] 'THE AGENCY DESIRES TO WAIVE THE COUNTERSIGNATURE REQUIREMENT'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199125, prescription_id FROM Prescriptions WHERE prescription_name IN ('32.412(e)'); -- [ADVANCED PAYMENT CONDITIONS] 'THE AGENCY WILL PROVIDE ADVANCED PAYMENT AT NO INTEREST TO PRIME CONTRACTOR'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199126, prescription_id FROM Prescriptions WHERE prescription_name IN ('232.412-70(b)'); -- [ADVANCED PAYMENT CONDITIONS] 'PAYMENT MADE BY DISBURSING OFFICE NOT DESIGNATED IN AGREEMENT'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199127, prescription_id FROM Prescriptions WHERE prescription_name IN ('232.412-70(c)'); -- [ADVANCED PAYMENT CONDITIONS] 'ADVANCE PAYMENTS WILL BE PROVIDED BY THE CONTRACTOR TO THE SUBCONTRACTOR PURSUANT TO AN APPROVED MENTOR-PROTEGE AGREEMENT'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199129, prescription_id FROM Prescriptions WHERE prescription_name IN ('32.412(f)'); -- [ADVANCED PAYMENT CONDITIONS] 'REQUIREMENT FOR A SPECIAL ACCOUNT IS ELIMINATED IN ACCORDANCE WITH 32.409-3 (e) OR (g)'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199131, prescription_id FROM Prescriptions WHERE prescription_name IN ('242.7204','242.7503','32.502-4(a)','32.502-4(b)','32.502-4(c)','32.502-4(d)'); -- [PROGRESS PAYMENT CONDITIONS] 'BASED ON COST'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199132, prescription_id FROM Prescriptions WHERE prescription_name IN ('242.7503','32.1005','32.1005(b)(1)','32.1005(b)(2)'); -- [PROGRESS PAYMENT CONDITIONS] 'BASED ON PERFORMANCE'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199133, prescription_id FROM Prescriptions WHERE prescription_name IN ('32.502-3(b)(2)'); -- [PROGRESS PAYMENT CONDITIONS] 'ONLY SMALL BUSINESS BIDDERS NEED PROGRESS PAYMENTS'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199134, prescription_id FROM Prescriptions WHERE prescription_name IN ('232.1005-70(a)'); -- [BASIS OF PERFORMANCE BASED PAYMENTS] 'WHOLE CONTRACT BASIS'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199135, prescription_id FROM Prescriptions WHERE prescription_name IN ('232.1005-70(b)'); -- [BASIS OF PERFORMANCE BASED PAYMENTS] 'DELIVERABLE ITEM BASIS'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199136, prescription_id FROM Prescriptions WHERE prescription_name IN ('32.1110(c)'); -- [EFT EXCEPTIONS] 'INFORMATION IS SUBMITTED TO OTHER THAN THE PAYMENT OFFICE'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199137, prescription_id FROM Prescriptions WHERE prescription_name IN ('32.1110(e)'); -- [EFT EXCEPTIONS] 'METHOD OF PAYMENT FOR INDIVIDUAL ORDERS WILL BE DESIGNATED BY THE ORDERING OFFICE'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199138, prescription_id FROM Prescriptions WHERE prescription_name IN ('32.1110(g)'); -- [EFT EXCEPTIONS] 'EFT INFORMATION SUBMITTED PRIOR TO AWARD'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199139, prescription_id FROM Prescriptions WHERE prescription_name IN ('32.1110(a)(1)','32.1110(d)'); -- [EFT EXCEPTIONS] 'PAYMENT WILL BE MADE THROUGH A THIRD PARTY ARRANGEMENT'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199140, prescription_id FROM Prescriptions WHERE prescription_name IN ('4.1105(a)(1)'); -- [PURCHASE CARD] 'YES'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199142, prescription_id FROM Prescriptions WHERE prescription_name IN ('228.370(b)'); -- [INSURANCE CONDITIONS] 'THE NON-DOD CUSTOMER HAS NOT AGREED TO ASSUME RISK OF LOSS OR DAMAGE TO AIRCRAFT'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199144, prescription_id FROM Prescriptions WHERE prescription_name IN ('28.309(a)'); -- [INSURANCE CONDITIONS] 'THE DEFENSE BASE ACT APPLIES'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199145, prescription_id FROM Prescriptions WHERE prescription_name IN ('28.313(b)'); -- [INSURANCE CONDITIONS] 'THE VEHICULAR LIABILITY OR GENERAL PUBLIC LIABILITY INSURANCE REQUIRED BY LAW IS NOT SUFFICIENT'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199146, prescription_id FROM Prescriptions WHERE prescription_name IN ('235.070-3'); -- [INSURANCE CONDITIONS] 'THE CONTRACTOR IS TO BE INDEMNIFIED FOR THIRD PARTY CLAIMS AND LOSS/DAMAGE'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199147, prescription_id FROM Prescriptions WHERE prescription_name IN ('235.070-3'); -- [INSURANCE CONDITIONS] 'THE CONTRACTOR IS TO BE INDEMNIFIED FOR LOSS OF OR DAMAGE TO THE CONTRACTOR'S PROPERTY'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199148, prescription_id FROM Prescriptions WHERE prescription_name IN ('228.370(a)'); -- [INSURANCE CONDITIONS] 'THE CONTRACTOR IS NOT ALLOWED TO BUY WAR-HAZARD INSURANCE'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199149, prescription_id FROM Prescriptions WHERE prescription_name IN ('228.370(c)'); -- [INSURANCE CONDITIONS] 'EMPLOYEES ARE SUBJECT TO CAPTURE AND NOT COVERED BY THE WAR HAZARDS COMPENSATION ACT'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199150, prescription_id FROM Prescriptions WHERE prescription_name IN ('50.104-4','50.403-3'); -- [INSURANCE CONDITIONS] 'THE CONTRACTOR SHALL BE INDEMNIFIED AGAINST UNUSUALLY HAZARDOUS OR NUCLEAR RISKS'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199151, prescription_id FROM Prescriptions WHERE prescription_name IN ('47.207-7(c)'); -- [INSURANCE CONDITIONS] 'CONTRACTOR LIABILITY FOR PERSONAL INJURY AND/OR PROPERTY DAMAGE'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199152, prescription_id FROM Prescriptions WHERE prescription_name IN ('32.806(a)(2)'); -- [INSURANCE CONDITIONS] 'A NO SET-OFF COMMITMENT HAS BEEN AUTHORIZED'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199153, prescription_id FROM Prescriptions WHERE prescription_name IN ('32.806(b)'); -- [INSURANCE CONDITIONS] 'A PROHIBITION OF ASSIGNMENT OF CLAIMS IS IN THE BEST INTEREST OF THE GOVERNMENT'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199155, prescription_id FROM Prescriptions WHERE prescription_name IN ('28.101-2','28.203-6','28.204-4'); -- [BID GUARANTEE OR BOND] 'BID GUARANTEE APPLIES'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199156, prescription_id FROM Prescriptions WHERE prescription_name IN ('28.103-4','28.106-4(a)','28.106-4(b)','28.1102-3(a)','28.203-6','28.204-4'); -- [BID GUARANTEE OR BOND] 'PAYMENT BONDS APPLY'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199157, prescription_id FROM Prescriptions WHERE prescription_name IN ('28.103-4','28.106-4(a)','28.1102-3(a)','28.203-6','28.204-4'); -- [BID GUARANTEE OR BOND] 'PERFORMANCE BONDS APPLY'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199158, prescription_id FROM Prescriptions WHERE prescription_name IN ('25.1102(b)(2)','25.1102(d)(2)','25.1102(e)(1)'); -- [NO TIME TO PROCESS BUY AMERICAN DETERMINATION] 'YES'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199161, prescription_id FROM Prescriptions WHERE prescription_name IN ('33.215(a)','33.215(b)'); -- [DISPUTES ACT] 'YES'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199163, prescription_id FROM Prescriptions WHERE prescription_name IN ('9.108-5(a)','9.108-5(b)'); -- [WAIVERS] '9-108-4 -- WAIVER OF THE PROHIBITION ON CONTRACTING WITH ANY FOREIGN ENTITY THAT IS TREATED AS AN INVERTED DOMESTIC CORPORATION'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199164, prescription_id FROM Prescriptions WHERE prescription_name IN ('12.301(b)(4)(i)','15.209(b)(4)'); -- [WAIVERS] '15.209/25.1001 -- WAIVER OF REQUIREMENT TO EXAMINATION OF RECORDS BY THE COMPTROLLER GENERAL'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199165, prescription_id FROM Prescriptions WHERE prescription_name IN ('15.408(i)'); -- [WAIVERS] '15.408 -- WAIVER OF FACILITIES CAPITAL COST OF MONEY'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199166, prescription_id FROM Prescriptions WHERE prescription_name IN ('22.1207'); -- [WAIVERS] '22.1203-3 -- WAIVER OF REQUIREMENT RELATING TO THE "NONDISPLACEMENT OF QUALIFIED WORKERS UNDER SERVICE CONTRACTS"'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199167, prescription_id FROM Prescriptions WHERE prescription_name IN ('22.1310(a)(1)'); -- [WAIVERS] '22.1305 - WAIVER OF ALL THE TERMS OF 52.222-35, "EQUAL OPPORTUNITY FOR VETERANS"'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199168, prescription_id FROM Prescriptions WHERE prescription_name IN ('22.1310(a)(2)'); -- [WAIVERS] '22.1305 - WAIVER OF ONE OR MORE OF THE TERMS (BUT NOT ALL) OF 52.222-35, "EQUAL OPPORTUNITY FOR VETERANS"'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199169, prescription_id FROM Prescriptions WHERE prescription_name IN ('22.1408(b)'); -- [WAIVERS] '22.1403 - WAIVER OF ONE OR MORE (BUT NOT ALL) OF THE TERMS OF 52.222-36, "AFFIRMATIVE ACTION FOR WORKERS WITH DISABILITIES"'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199170, prescription_id FROM Prescriptions WHERE prescription_name IN ('22.1408(a)'); -- [WAIVERS] '22.1403 - WAIVER OF ALL OF THE TERMS OF 52.222-36, "AFFIRMATIVE ACTION FOR WORKERS WITH DISABILITIES"'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199171, prescription_id FROM Prescriptions WHERE prescription_name IN ('22.1605'); -- [WAIVERS] '22.1603 -- WAIVER OF THE REQUIREMENT TO NOTIFY EMPLOYEES OF THEIR RIGHTS UNDER THE NATIONAL LABOR RELATIONS ACT'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199172, prescription_id FROM Prescriptions WHERE prescription_name IN ('28.309(b)'); -- [WAIVERS] '28.305 - WAIVER OF THE DEFENSE BASE ACT'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199173, prescription_id FROM Prescriptions WHERE prescription_name IN ('236.570(b)(4)'); -- [WAIVERS] '236.205 -- WAIVER OF THE REQUIREMENT NOT TO AWARD A CONSTRUCTION CONTRACT EXCEEDING A STATUTORY COST LIMITATION'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199174, prescription_id FROM Prescriptions WHERE prescription_name IN ('37.113-2(a)','37.113-2(b)'); -- [WAIVERS] '37.113-1 - WAIVER OF COST ALLOWABILITY LIMITATIONS ON SEVERANCE PAYMENTS TO FOREIGN NATIONALS'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199175, prescription_id FROM Prescriptions WHERE prescription_name IN ('225.7006-4(a)','225.7006-4(b)'); -- [WAIVERS] '225.7006-3 -- WAIVER/EXCEPTION TO RESTRICTION ON ACQUISITION OF AIR CIRCUIT BREAKERS FOR NAVAL VESSELS'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199176, prescription_id FROM Prescriptions WHERE prescription_name IN ('225.7007-3'); -- [WAIVERS] '225.7007-2 -- WAIVER OF RESTRICTION FOR ACQUISITION OF ANCHOR AND MOORING CHAIN'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199177, prescription_id FROM Prescriptions WHERE prescription_name IN ('225.7009-5'); -- [WAIVERS] '225.7009-4 -- WAIVER OF RESTRICTIONS ON BALL OR ROLLER BEARINGS'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199178, prescription_id FROM Prescriptions WHERE prescription_name IN ('225.7011-3'); -- [WAIVERS] '225.7011-2 -- WAIVER ON ACQUISITION OF CARBON, ALLOY OR ARMOR STEEL PLATE'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199179, prescription_id FROM Prescriptions WHERE prescription_name IN ('225.7012-3'); -- [WAIVERS] '225-7012-2 -- WAIVER OF THE REQUIREMENT TO PURCHASE SUPERCOMPUTERS IN THE UNITED STATES'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199181, prescription_id FROM Prescriptions WHERE prescription_name IN ('247.574(d)'); -- [WAIVERS] '247.572(c)(2) -- WAIVER OF THE REQUIREMENT FOR DOD CONTRACTORS TO TRANSPORT SUPPLIES AS DEFINED IN 252.247-7023, EXCLUSIVELY ON U.S. FLAG VESSELS'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199182, prescription_id FROM Prescriptions WHERE prescription_name IN ('225.7605'); -- [WAIVERS] '225.7604 -- WAIVER OF REQUIREMENT NOT TO CONTRACT WITH A FOREIGN ENTITY THAT COMPLIES WITH THE SECONDARY ARAB BOYCOTT OF ISRAEL'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199185, prescription_id FROM Prescriptions WHERE prescription_name IN ('225.1101(2)(ii)','225.1101(2)(iii)'); -- [EXEMPTIONS] '225.7501 -- EXEMPT FROM THE BALANCE OF PAYMENT PROGRAM'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199186, prescription_id FROM Prescriptions WHERE prescription_name IN ('22.810(e)'); -- [EXEMPTIONS] '22.807 -- EXEMPT FROM ALL THE REQUIREMENTS OF E.O. 11246'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199187, prescription_id FROM Prescriptions WHERE prescription_name IN ('22.810(e)'); -- [EXEMPTIONS] '22.807 -- EXEMPT FROM ONE OR MORE, BUT NOT ALL THE REQUIREMENTS OF E.O. 11246'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199188, prescription_id FROM Prescriptions WHERE prescription_name IN ('48.201','48.201(c)','48.201(d)','48.201(e)(1)'); -- [EXEMPTIONS] '48.102 -- EXEMPT FROM VALUE ENGINEERING'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199189, prescription_id FROM Prescriptions WHERE prescription_name IN ('22.305'); -- [EXEMPTIONS] '22.305 -- EXEMPT FROM INCLUSION OF 52.222-4 "CONTRACT WORK HOURS AND SAFETY STANDARDS ACT -- OVERTIME COMPENSATION"'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199190, prescription_id FROM Prescriptions WHERE prescription_name IN ('22.1006(e)(1)','22.1006(e)(3)'); -- [EXEMPTIONS] '22.1003-3/4 -- EXEMPT FROM APPLICATION OF THE SERVICE CONTRACT ACT'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199191, prescription_id FROM Prescriptions WHERE prescription_name IN ('23.206'); -- [EXEMPTIONS] '23.204 -- ENERGY STAR OR FEMP-DESIGNATED PRODUCT EXEMPTION'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199192, prescription_id FROM Prescriptions WHERE prescription_name IN ('22.1207'); -- [EXEMPTIONS] '22.1203-2 -- EXEMPT FROM 52.222-17, "NONDISPLACEMENT OF QUALIFIED WORKERS"'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199193, prescription_id FROM Prescriptions WHERE prescription_name IN ('22.1605'); -- [EXEMPTIONS] '22.1603 -- SECRETARIAL EXEMPTION TO THE REQUIREMENT TO INCLUDE THE CLAUSE 52.222-40, "NOTIFICATION OF EMPLOYEE RIGHTS UNDER THE NATIONAL LABOR RELATIONS ACT'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199195, prescription_id FROM Prescriptions WHERE prescription_name IN ('225.1101(2)(ii)','225.1101(2)(iii)','25.1101(a)(1)','25.1101(c)(1)','25.1103(a)'); -- [EXCEPTIONS] '25.202 -- EXCEPTION TO THE BUY AMERICAN STATUTE'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199196, prescription_id FROM Prescriptions WHERE prescription_name IN ('25.1101(b)(1)(i)','25.1101(b)(1)(ii)','25.1101(b)(1)(iii)','25.1101(b)(1)(iv)'); -- [EXCEPTIONS] '25.401 -- EXCEPTION TO FOREIGN TRADE AGREEMENTS'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199197, prescription_id FROM Prescriptions WHERE prescription_name IN ('22.1605'); -- [EXCEPTIONS] '22.1603 --22.1603 -- SECRETARIAL EXEMPTION TO THE REQUIREMENT TO INCLUDE THE CLAUSE 52.222-40, "NOTIFICATION OF EMPLOYEE RIGHTS UNDER THE NATIONAL LABOR RELATIONS ACT"'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199198, prescription_id FROM Prescriptions WHERE prescription_name IN ('211.274-6(a)(1)'); -- [EXCEPTIONS] '211.274-2(b) -- EXCEPTION TO UNIQUE ITEM IDENTIFICATION'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199199, prescription_id FROM Prescriptions WHERE prescription_name IN ('215.371-6','215.408(4)'); -- [EXCEPTIONS] '215.371-4 - EXCEPTION/WAIVER TO REQUIREMENT TO RE- SOLICIT IF ONLY ONE OFFER IS RECEIVED'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199200, prescription_id FROM Prescriptions WHERE prescription_name IN ('232.7004(a)'); -- [EXCEPTIONS] '232.7002(a) - EXCEPTION TO ELECTRONIC SUBMISSION AND PROCESSING OF PAYMENT REQUESTS AND RECEIVING REPORTS'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199201, prescription_id FROM Prescriptions WHERE prescription_name IN ('225.7002-2(a)'); -- [EXCEPTIONS] '225.7002-2 EXCEPTION TO RESTRICTIONS ON FOREIGN ACQUISITION'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199202, prescription_id FROM Prescriptions WHERE prescription_name IN ('225.7102-4'); -- [EXCEPTIONS] '225.7102-2 -- EXCEPTION ON THE RESTRICTION ON ACQUISITION OF FORGINGS'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199203, prescription_id FROM Prescriptions WHERE prescription_name IN ('225.7605'); -- [EXCEPTIONS] '225.7603 -- EXCEPTION TO REQUIREMENT NOT TO CONTRACT WITH A FOREIGN ENTITY THAT DOES NOT COMPLY WITH THE SECONDARY ARAB BOYCOTT OF ISRAEL'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199204, prescription_id FROM Prescriptions WHERE prescription_name IN ('223.7106'); -- [EXCEPTIONS] '223.7102 -- EXCEPTION TO 223.7102 (STORAGE AND DISPOSAL OF TOXIC AND HAZARDOUS MATERIALS)'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199205, prescription_id FROM Prescriptions WHERE prescription_name IN ('225.7003-5(a)(1)'); -- [EXCEPTIONS] '225.7003-3 -- NATIONAL SECURITY EXCEPTION TO RESTRICTIONS ON SPECIALTY METALS'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199206, prescription_id FROM Prescriptions WHERE prescription_name IN ('23.705(b)(1)','23.705(b)(2)','23.705(c)(1)','23.705(d)(2)'); -- [EXCEPTIONS] '23.704(a) - EXCEPTION TO EPEAT REGISTERED ELECTRONIC PRODUCTS'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199208, prescription_id FROM Prescriptions WHERE prescription_name IN ('27.201-2(a)(1)'); -- [PATENT COUNCIL RECOMMENDED] '52.227-1-- AUTHORIZATION AND CONSENT'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199209, prescription_id FROM Prescriptions WHERE prescription_name IN ('27.201-2(a)(2)'); -- [PATENT COUNCIL RECOMMENDED] '52.227-1 ALTERNATE I -- AUTHORIZATION AND CONSENT'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199210, prescription_id FROM Prescriptions WHERE prescription_name IN ('27.201-2(a)(3)'); -- [PATENT COUNCIL RECOMMENDED] '52.227-1 ALTERNATE II -- AUTHORIZATION AND CONSENT'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199211, prescription_id FROM Prescriptions WHERE prescription_name IN ('27.201-2(b)'); -- [PATENT COUNCIL RECOMMENDED] '52.227-2 NOTICE AND ASSISTANCE REGARDING PATENT AND COPYRIGHT INFRINGEMENT'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199212, prescription_id FROM Prescriptions WHERE prescription_name IN ('27.201-2(c)(1)'); -- [PATENT COUNCIL RECOMMENDED] '52.227-3 -- PATENT INDEMNITY'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199213, prescription_id FROM Prescriptions WHERE prescription_name IN ('27.201-2(c)(2)'); -- [PATENT COUNCIL RECOMMENDED] '52.227-3 ALTERNATE I -- PATENT INDEMNITY'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199214, prescription_id FROM Prescriptions WHERE prescription_name IN ('27.201-2(c)(3)'); -- [PATENT COUNCIL RECOMMENDED] '52.227-3 ALTERNATE II -- PATENT INDEMNITY'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199215, prescription_id FROM Prescriptions WHERE prescription_name IN ('27.201-2(c)(3)'); -- [PATENT COUNCIL RECOMMENDED] '52.227-3 ALTERNATE III -- PATENT INDEMNITY'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199216, prescription_id FROM Prescriptions WHERE prescription_name IN ('27.201-2'); -- [PATENT COUNCIL RECOMMENDED] '52.227-4 -- PATENT INDEMNITY -- CONSTRUCTION CONTRACTS'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199217, prescription_id FROM Prescriptions WHERE prescription_name IN ('27.201-2(d)(2)'); -- [PATENT COUNCIL RECOMMENDED] '52.227-4 ALTERNATE I -- PATENT INDEMNITY -- CONSTRUCTION CONTRACTS'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199218, prescription_id FROM Prescriptions WHERE prescription_name IN ('27.201-2(e)'); -- [PATENT COUNCIL RECOMMENDED] '52.227-5 -- WAIVER OF INDEMNITY'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199219, prescription_id FROM Prescriptions WHERE prescription_name IN ('27.202-5(a)(1)'); -- [PATENT COUNCIL RECOMMENDED] '52.227-6 -- ROYALTY INFORMATION'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199220, prescription_id FROM Prescriptions WHERE prescription_name IN ('27.202-5(a)(2)'); -- [PATENT COUNCIL RECOMMENDED] '52.227-6 ALTERNATE I -- ROYALTY INFORMATION'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199221, prescription_id FROM Prescriptions WHERE prescription_name IN ('27.202-5(b)'); -- [PATENT COUNCIL RECOMMENDED] '52.227-7 -- PATENTS-- NOTICE OF GOVERNMENT LICENSEE'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199222, prescription_id FROM Prescriptions WHERE prescription_name IN ('27.202-5(c)'); -- [PATENT COUNCIL RECOMMENDED] '52.227-9 -- REFUND OF ROYALTIES'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199223, prescription_id FROM Prescriptions WHERE prescription_name IN ('27.203-2'); -- [PATENT COUNCIL RECOMMENDED] '52.227-10 -- FILING OF PATENT APPLICATIONS -- CLASSIFIED SUBJECT MATTER'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199224, prescription_id FROM Prescriptions WHERE prescription_name IN ('27.303(b)(1)'); -- [PATENT COUNCIL RECOMMENDED] '52.227-11 -- PATENT RIGHTS -- OWNERSHIP BY THE CONTRACTOR'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199225, prescription_id FROM Prescriptions WHERE prescription_name IN ('27.303(b)(3)'); -- [PATENT COUNCIL RECOMMENDED] '52.227-11 ALTERNATE I-- PATENT RIGHTS -- OWNERSHIP BY THE CONTRACTOR'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199226, prescription_id FROM Prescriptions WHERE prescription_name IN ('27.303(b)(4)'); -- [PATENT COUNCIL RECOMMENDED] '52.227-11 ALTERNATE II -- PATENT RIGHTS -- OWNERSHIP BY THE CONTRACTOR'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199227, prescription_id FROM Prescriptions WHERE prescription_name IN ('27.303(b)(5)'); -- [PATENT COUNCIL RECOMMENDED] '52.227-11 ALTERNATE III-- PATENT RIGHTS -- OWNERSHIP BY THE CONTRACTOR'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199228, prescription_id FROM Prescriptions WHERE prescription_name IN ('27.303(b)(6)'); -- [PATENT COUNCIL RECOMMENDED] '52.227-11 ALTERNATE IV -- PATENT RIGHTS -- OWNERSHIP BY THE CONTRACTOR'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199229, prescription_id FROM Prescriptions WHERE prescription_name IN ('27.303(b)(7)'); -- [PATENT COUNCIL RECOMMENDED] '52.227-11 ALTERNATE V-- PATENT RIGHTS -- OWNERSHIP BY THE CONTRACTOR'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199230, prescription_id FROM Prescriptions WHERE prescription_name IN ('27.303(e)'); -- [PATENT COUNCIL RECOMMENDED] '52.227-13 -- PATENT RIGHTS -- OWNERSHIP BY THE GOVERNMENT'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199231, prescription_id FROM Prescriptions WHERE prescription_name IN ('27.303(e)(4)'); -- [PATENT COUNCIL RECOMMENDED] '52.227-13 -- PATENT RIGHTS ALTERNATE I -- OWNERSHIP BY THE GOVERNMENT'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199232, prescription_id FROM Prescriptions WHERE prescription_name IN ('27.303(e)(5)'); -- [PATENT COUNCIL RECOMMENDED] '52.227-13 -- PATENT RIGHTS ALTERNATE II -- OWNERSHIP BY THE GOVERNMENT'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199233, prescription_id FROM Prescriptions WHERE prescription_name IN ('27.409(b)(1)'); -- [PATENT COUNCIL RECOMMENDED] '52.227-14 -- RIGHTS IN DATA - GENERAL'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199234, prescription_id FROM Prescriptions WHERE prescription_name IN ('27.409(b)(2)'); -- [PATENT COUNCIL RECOMMENDED] '52.227-14 ALTERNATE I -- RIGHTS IN DATA - GENERAL'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199235, prescription_id FROM Prescriptions WHERE prescription_name IN ('27.409(b)(3)'); -- [PATENT COUNCIL RECOMMENDED] '52.227-14 ALTERNATE II - RIGHTS IN DATA - GENERAL'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199236, prescription_id FROM Prescriptions WHERE prescription_name IN ('27.409(b)(4)'); -- [PATENT COUNCIL RECOMMENDED] '52.227-14 ALTERNATE III-- RIGHTS IN DATA - GENERAL'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199237, prescription_id FROM Prescriptions WHERE prescription_name IN ('27.409(b)(4)'); -- [PATENT COUNCIL RECOMMENDED] '52.227-14 ALTERNATE IV-- RIGHTS IN DATA - GENERAL'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199238, prescription_id FROM Prescriptions WHERE prescription_name IN ('27.409(b)(5)'); -- [PATENT COUNCIL RECOMMENDED] '52.227-14 ALTERNATE V-- RIGHTS IN DATA - GENERAL'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199239, prescription_id FROM Prescriptions WHERE prescription_name IN ('27.409(c)'); -- [PATENT COUNCIL RECOMMENDED] '52.227-15 -- REPRESENTATION OF LIMITED RIGHTS DATA AND RESTRICTED COMPUTER SOFTWARE'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199240, prescription_id FROM Prescriptions WHERE prescription_name IN ('27.409(d)'); -- [PATENT COUNCIL RECOMMENDED] '52.227-16 -- ADDITIONAL DATA REQUIREMENTS'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199241, prescription_id FROM Prescriptions WHERE prescription_name IN ('27.409(e)'); -- [PATENT COUNCIL RECOMMENDED] '52.227-17 -- RIGHTS IN DATA -- SPECIAL WORKS'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199242, prescription_id FROM Prescriptions WHERE prescription_name IN ('27.409(f)'); -- [PATENT COUNCIL RECOMMENDED] '52.227-18 -- RIGHTS IN DATA-EXISTING WORKS'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199243, prescription_id FROM Prescriptions WHERE prescription_name IN ('27.409(g)'); -- [PATENT COUNCIL RECOMMENDED] '52.227-19 -- COMMERCIAL COMPUTER SOFTWARE LICENSE'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199244, prescription_id FROM Prescriptions WHERE prescription_name IN ('27.409(h)'); -- [PATENT COUNCIL RECOMMENDED] '52.227-20 -- RIGHTS IN DATA -- SBIR PROGRAM'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199245, prescription_id FROM Prescriptions WHERE prescription_name IN ('27.409(j)'); -- [PATENT COUNCIL RECOMMENDED] '52.227-21 -- TECHNICAL DATA DECLARATION, REVISION, AND WITHHOLDING OF PAYMENT -- MAJOR SYSTEMS'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199246, prescription_id FROM Prescriptions WHERE prescription_name IN ('27.409(k)'); -- [PATENT COUNCIL RECOMMENDED] '52.227-22 -- MAJOR SYSTEM - MINIMUM RIGHTS'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199247, prescription_id FROM Prescriptions WHERE prescription_name IN ('27.409(l)'); -- [PATENT COUNCIL RECOMMENDED] '52.227-23 -- RIGHTS TO PROPOSAL DATA (TECHNICAL)'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199248, prescription_id FROM Prescriptions WHERE prescription_name IN ('227.7009-1'); -- [PATENT COUNCIL RECOMMENDED] '252.227-7000 -- NON-ESTOPPEL'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199249, prescription_id FROM Prescriptions WHERE prescription_name IN ('227.7009-2(a)'); -- [PATENT COUNCIL RECOMMENDED] '252.227-7001 -- RELEASE OF PAST INFRINGEMENT'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199250, prescription_id FROM Prescriptions WHERE prescription_name IN ('227.7009-2(b)'); -- [PATENT COUNCIL RECOMMENDED] '252.227-7002 -- READJUSTMENT OF PAYMENTS'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199251, prescription_id FROM Prescriptions WHERE prescription_name IN ('227.7009-2'); -- [PATENT COUNCIL RECOMMENDED] '252.227-7003 -- TERMINATION'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199252, prescription_id FROM Prescriptions WHERE prescription_name IN ('227.7009-3(a)'); -- [PATENT COUNCIL RECOMMENDED] '252.227-7004 -- LICENSE GRANT'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199253, prescription_id FROM Prescriptions WHERE prescription_name IN ('227.7009-3(b)'); -- [PATENT COUNCIL RECOMMENDED] '252.227-7005 ALTERNATE I-- LICENSE TERM (NOTE: NO BASIC)'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199254, prescription_id FROM Prescriptions WHERE prescription_name IN ('227.7009-3(b)'); -- [PATENT COUNCIL RECOMMENDED] '252.227-7005 ALTERNATE II -- LICENSE TERM (NOTE: NO BASIC)'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199255, prescription_id FROM Prescriptions WHERE prescription_name IN ('227.7009-4(a)'); -- [PATENT COUNCIL RECOMMENDED] '252.227-7006 -- LICENSE GRANT---RUNNING ROYALTY'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199256, prescription_id FROM Prescriptions WHERE prescription_name IN ('227.7009-4(b)'); -- [PATENT COUNCIL RECOMMENDED] '252.227-7007 -- LICENSE TERM--RUNNING ROYALTY'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199257, prescription_id FROM Prescriptions WHERE prescription_name IN ('227.7009- 4(c)'); -- [PATENT COUNCIL RECOMMENDED] '252.227-7008 -- COMPUTATION OF ROYALTIES'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199258, prescription_id FROM Prescriptions WHERE prescription_name IN ('227.7009-4(d)'); -- [PATENT COUNCIL RECOMMENDED] '252.227-7009 -- REPORTING AND PAYMENT OF ROYALTIES'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199259, prescription_id FROM Prescriptions WHERE prescription_name IN ('227.7009-4(e)'); -- [PATENT COUNCIL RECOMMENDED] '252.227-7010 -- LICENSE TO OTHER GOVERNMENT AGENCIES'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199260, prescription_id FROM Prescriptions WHERE prescription_name IN ('227.7010'); -- [PATENT COUNCIL RECOMMENDED] '252.227-7011 -- ASSIGNMENTS'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199261, prescription_id FROM Prescriptions WHERE prescription_name IN ('227.7012'); -- [PATENT COUNCIL RECOMMENDED] '252.227-7012 -- PATENT LICENSE AND RELEASE CONTRACT'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199262, prescription_id FROM Prescriptions WHERE prescription_name IN ('227.7103-6(a)'); -- [PATENT COUNCIL RECOMMENDED] '252.227-7013 -- RIGHTS IN TECHNICAL DATA--NONCOMMERCIAL ITEMS'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199263, prescription_id FROM Prescriptions WHERE prescription_name IN ('227.7103-6(a)'); -- [PATENT COUNCIL RECOMMENDED] '252.227-7013 ALTERNATE I -- RIGHTS IN TECHNICAL DATA--NONCOMMERCIAL ITEMS'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199264, prescription_id FROM Prescriptions WHERE prescription_name IN ('227.7103-6(a)'); -- [PATENT COUNCIL RECOMMENDED] '252.227-7013 ALTERNATE II -- RIGHTS IN TECHNICAL DATA--NONCOMMERCIAL ITEMS'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199265, prescription_id FROM Prescriptions WHERE prescription_name IN ('227.7203-6(a)(1)'); -- [PATENT COUNCIL RECOMMENDED] '252.227-7014 -- RIGHTS IN NONCOMMERCIAL COMPUTER SOFTWARE AND NONCOMMERCIAL COMPUTER SOFTWARE DOCUMENTATION'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199266, prescription_id FROM Prescriptions WHERE prescription_name IN ('227.7203-6(a)(1)'); -- [PATENT COUNCIL RECOMMENDED] '252.227-7014 ALTERNATE I -- RIGHTS IN NONCOMMERCIAL COMPUTER SOFTWARE AND NONCOMMERCIAL COMPUTER SOFTWARE DOCUMENTATION'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199267, prescription_id FROM Prescriptions WHERE prescription_name IN ('227.7102-4(a)(1)'); -- [PATENT COUNCIL RECOMMENDED] '252.227-7015 -- TECHNICAL DATA--COMMERCIAL ITEMS'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199268, prescription_id FROM Prescriptions WHERE prescription_name IN ('227.7102-4(a)(1)'); -- [PATENT COUNCIL RECOMMENDED] '252.227-7015 ALTERNATE I -- TECHNICAL DATA--COMMERCIAL ITEMS'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199269, prescription_id FROM Prescriptions WHERE prescription_name IN ('227.7103-6(e)(1)','227.7104(e)(1)','227.7203-6(b)'); -- [PATENT COUNCIL RECOMMENDED] '252.227-7016 -- RIGHTS IN BID OR PROPOSAL INFORMATION'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199270, prescription_id FROM Prescriptions WHERE prescription_name IN ('227.7103-3(b)','227.7104(e)(2)','227.7203-3(a)'); -- [PATENT COUNCIL RECOMMENDED] '252.227-7017 -- IDENTIFICATION AND ASSERTION OF USE, RELEASE, OR DISCLOSURE RESTRICTIONS'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199271, prescription_id FROM Prescriptions WHERE prescription_name IN ('227.7104(a)'); -- [PATENT COUNCIL RECOMMENDED] '252.227-7018 -- RIGHTS IN NONCOMMERCIAL TECHNICAL DATA AND COMPUTER SOFTWARE--SMALL BUSINESS INNOVATION RESEARCH (SBIR) PROGRAM'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199272, prescription_id FROM Prescriptions WHERE prescription_name IN ('227.7104(a)'); -- [PATENT COUNCIL RECOMMENDED] '252.227-7018 ALTERNATE I -- RIGHTS IN NONCOMMERCIAL TECHNICAL DATA AND COMPUTER SOFTWARE--SMALL BUSINESS INNOVATION RESEARCH (SBIR) PROGRAM'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199273, prescription_id FROM Prescriptions WHERE prescription_name IN ('227.7104(e)(3)','227.7203-6(c)'); -- [PATENT COUNCIL RECOMMENDED] '252.227-7019 -- VALIDATION OF ASSERTED RESTRICTIONS--COMPUTER SOFTWARE'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199274, prescription_id FROM Prescriptions WHERE prescription_name IN ('227.7105-3','227.7106(a)','227.7205(a)'); -- [PATENT COUNCIL RECOMMENDED] '252.227-7020 -- RIGHTS IN SPECIAL WORKS'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199275, prescription_id FROM Prescriptions WHERE prescription_name IN ('227.7105-2(a)'); -- [PATENT COUNCIL RECOMMENDED] '252.227-7021 -- RIGHTS IN DATA--EXISTING WORKS'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199276, prescription_id FROM Prescriptions WHERE prescription_name IN ('227.7107-1(a)'); -- [PATENT COUNCIL RECOMMENDED] '252.227-7022 -- GOVERNMENT RIGHTS (UNLIMITED)'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199277, prescription_id FROM Prescriptions WHERE prescription_name IN ('227.7107-1(b)'); -- [PATENT COUNCIL RECOMMENDED] '252.227-7023 -- DRAWINGS AND OTHER DATA TO BECOME PROPERTY OF GOVERNMENT'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199278, prescription_id FROM Prescriptions WHERE prescription_name IN ('227.7107-3'); -- [PATENT COUNCIL RECOMMENDED] '252.227-7024 -- NOTICE AND APPROVAL OF RESTRICTED DESIGN'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199279, prescription_id FROM Prescriptions WHERE prescription_name IN ('227.7103-6(c)','227.7104(f)(1)','227.7203-6(d)'); -- [PATENT COUNCIL RECOMMENDED] '252.227-7025 -- LIMITATIONS ON THE USE OR DISCLOSURE OF GOVERNMENT-FURNISHED INFORMATION MARKED WITH RESTRICTIVE LEGENDS'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199280, prescription_id FROM Prescriptions WHERE prescription_name IN ('227.7103-8(a)'); -- [PATENT COUNCIL RECOMMENDED] '252.227-7026 -- DEFERRED DELIVERY OF TECHNICAL DATA OR COMPUTER SOFTWARE'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199281, prescription_id FROM Prescriptions WHERE prescription_name IN ('227.7103-8(b)'); -- [PATENT COUNCIL RECOMMENDED] '252.227-7027 -- DEFERRED ORDERING OF TECHNICAL DATA OR COMPUTER SOFTWARE'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199282, prescription_id FROM Prescriptions WHERE prescription_name IN ('227.7103-6(d)','227.7104(f)(2)','227.7203-6(e)'); -- [PATENT COUNCIL RECOMMENDED] '252.227-7028 -- TECHNICAL DATA OR COMPUTER SOFTWARE PREVIOUSLY DELIVERED TO THE GOVERNMENT'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199283, prescription_id FROM Prescriptions WHERE prescription_name IN ('227.7103-6(e)(2)','227.7104(e)(4)'); -- [PATENT COUNCIL RECOMMENDED] '252.227-7030 -- TECHNICAL DATA--WITHHOLDING OF PAYMENT'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199284, prescription_id FROM Prescriptions WHERE prescription_name IN ('227.7103-17'); -- [PATENT COUNCIL RECOMMENDED] '252.227-7032 -- RIGHTS IN TECHNICAL DATA AND COMPUTER SOFTWARE (FOREIGN)'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199285, prescription_id FROM Prescriptions WHERE prescription_name IN ('227.7107-1(c)'); -- [PATENT COUNCIL RECOMMENDED] '252.227-7033 -- RIGHTS IN SHOP DRAWINGS'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199286, prescription_id FROM Prescriptions WHERE prescription_name IN ('227.7102-4(c)','227.7103-6(e)(3)','227.7104(e)(5)','227.7203-6(f)'); -- [PATENT COUNCIL RECOMMENDED] '252.227-7037 -- VALIDATION OF RESTRICTIVE MARKINGS ON TECHNICAL DATA'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199287, prescription_id FROM Prescriptions WHERE prescription_name IN ('227.303(2)'); -- [PATENT COUNCIL RECOMMENDED] '252.227-7038 -- PATENT RIGHTS--OWNERSHIP BY THE CONTRACTOR (LARGE BUSINESS)'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199288, prescription_id FROM Prescriptions WHERE prescription_name IN ('227.303(1)'); -- [PATENT COUNCIL RECOMMENDED] '252.227-7039 -- PATENTS--REPORTING OF SUBJECT INVENTIONS'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199290, prescription_id FROM Prescriptions WHERE prescription_name IN ('15.408(b)','15.408(c)','15.408(g)','15.408(I)','15.408(j)','15.408(k)','215.408','215.408(2)','215.408(4)(ii)','215.408(5)','45.107(a)(2)'); -- [PRICING REQUIREMENTS] 'CERTIFIED COST AND PRICING DATA ARE REQUIRED'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199291, prescription_id FROM Prescriptions WHERE prescription_name IN ('15.408(m)'); -- [PRICING REQUIREMENTS] 'CERTIFIED COST AND PRICING DATA ARE REQUIRED FOR MODIFICATIONS'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199292, prescription_id FROM Prescriptions WHERE prescription_name IN ('15.408(I)'); -- [PRICING REQUIREMENTS] 'CONTRACT REQUIRES OTHER THAN CERTIFIED COST AND PRICING DATA'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199293, prescription_id FROM Prescriptions WHERE prescription_name IN ('15.408(m)'); -- [PRICING REQUIREMENTS] 'CONTRACT REQUIRES OTHER THAN CERTIFIED COST AND PRICING DATA FOR MODIFICATIONS'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199295, prescription_id FROM Prescriptions WHERE prescription_name IN ('242.7001','30.201-3','30.201-3(c)'); -- [CAS] 'SUBJECT TO CAS'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199296, prescription_id FROM Prescriptions WHERE prescription_name IN ('30.201-4(a)','30.201-4(c)','30.201-4(e)'); -- [CAS] 'EXEMPT FROM CAS'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199297, prescription_id FROM Prescriptions WHERE prescription_name IN ('30.201-4(a)','30.201-4(b)(1)'); -- [CAS] 'MODIFIED CAS IS AUTHORIZED'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199298, prescription_id FROM Prescriptions WHERE prescription_name IN ('15.408(I)','30.201-4(b)(1)'); -- [REGULATION PRICING] 'FORMAT IN TABLE 15.2 IS USED'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199299, prescription_id FROM Prescriptions WHERE prescription_name IN ('15.408(g)'); -- [REGULATION PRICING] 'PRICING IS CONSISTENT WITH FAR PART 31, "CONTRACT COST PRINCIPLES AND PROCEDURES"'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199300, prescription_id FROM Prescriptions WHERE prescription_name IN ('231.100-70'); -- [REGULATION PRICING] 'PRICING IS CONSISTENT WITH FAR PART 31.1, "APPLICABILITY"'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199301, prescription_id FROM Prescriptions WHERE prescription_name IN ('15.408(h)','15.408(k)','231.100-70'); -- [REGULATION PRICING] 'PRICING IS CONSISTENT WITH FAR PART 31.2, "CONTRACTS WITH COMMERCIAL ORGANIZATIONS"'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199302, prescription_id FROM Prescriptions WHERE prescription_name IN ('231.100-70'); -- [REGULATION PRICING] 'PRICING IS CONSISTENT WITH FAR PART 31.6, "CONTRACTS WITH STATE, LOCAL, AND FEDERALLY RECOGNIZED INDIAN TRIBAL GOVERNMENTS"'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199303, prescription_id FROM Prescriptions WHERE prescription_name IN ('231.100-70'); -- [REGULATION PRICING] 'PRICING IS CONSISTENT WITH FAR PART 31.7, "CONTRACTS WITH NONPROFIT ORGANIZATIONS"'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199304, prescription_id FROM Prescriptions WHERE prescription_name IN ('47.207-1(e)'); -- [REGULATION PRICING] 'CONTRACTORS ARE REQUIRED TO FURNISH FINANCIAL STATEMENTS'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199306, prescription_id FROM Prescriptions WHERE prescription_name IN ('16.203-4(c)'); -- [DIRECT COST] 'ONE OR MORE IDENTIFIABLE LABOR OR MATERIAL COST FACTORS ARE SUBJECT TO CHANGE'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199308, prescription_id FROM Prescriptions WHERE prescription_name IN ('42.703-2(f)'); -- [INDIRECT COST] 'SOLICITATION/AWARD CALLS FOR ESTABLISHMENT OF FINAL INDIRECT COST RATES'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199309, prescription_id FROM Prescriptions WHERE prescription_name IN ('16.307(g)'); -- [INDIRECT COST] 'PREDETERMINED INDIRECT COST RATES ARE USED'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199311, prescription_id FROM Prescriptions WHERE prescription_name IN ('44.204(c)'); -- [PRICING LIMITATIONS] 'PRICES ARE SET BY LAW OR REGULATION'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199312, prescription_id FROM Prescriptions WHERE prescription_name IN ('16.203-4(a)','16.203-4(b)','216.203-4-70(a)(1)'); -- [PRICING LIMITATIONS] 'ESTABLISHED CATALOG OR MARKET PRICE'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199313, prescription_id FROM Prescriptions WHERE prescription_name IN ('236.570(b)(4)'); -- [PRICING LIMITATIONS] 'STATUTORY COST LIMITATIONS APPLY'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199315, prescription_id FROM Prescriptions WHERE prescription_name IN ('247.207'); -- [TRANSPORTATION COST] 'TRANSPORTATION WITH FUEL-RELATED ADJUSTMENTS'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199316, prescription_id FROM Prescriptions WHERE prescription_name IN ('28.313(a)'); -- [TRANSPORTATION COST] 'FREIGHT IS SHIPPED UNDER RATES SUBJECT TO RELEASED OR DECLARED VALUE'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199317, prescription_id FROM Prescriptions WHERE prescription_name IN ('47.305-13(a)(3)(i)','47.305-14(b)(4)'); -- [TRANSPORTATION COST] 'OFFERORS MAY HAVE POTENTIAL TRANSIT CREDITS AVAILABLE AND THE GOVERNMENT MAY REDUCE TRANSPORTATION COSTS'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199318, prescription_id FROM Prescriptions WHERE prescription_name IN ('47.305-16(c)'); -- [TRANSPORTATION COST] 'VOLUME RATES MAY APPLY'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199319, prescription_id FROM Prescriptions WHERE prescription_name IN ('47.207-1(a)'); -- [TRANSPORTATION COST] 'REGULATED TRANSPORTATION IS INVOLVED'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199320, prescription_id FROM Prescriptions WHERE prescription_name IN ('47.103-2'); -- [TRANSPORTATION COST] 'REIMBURSEMENT OF TRANSPORTATION AS A DIRECT CHARGE IS AUTHORIZED'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199321, prescription_id FROM Prescriptions WHERE prescription_name IN ('47.305-16(b)(1)'); -- [TRANSPORTATION COST] 'DETERMINATION OF TRANSPORTATION COSTS REQUIRE INFORMATION REGARDING SHIPPING AND OTHER CHARACTERISTICS'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199323, prescription_id FROM Prescriptions WHERE prescription_name IN ('15.408(I)','15.408(m)','215.408(4)(ii)'); -- [PROPOSAL REQUIREMENTS] 'PROPOSAL COPIES ARE TO BE SENT TO THE ADMINISTRATIVE CONTRACTING OFFICER (ACO) AND CONTRACT AUDITOR (BASIC AWARD AND FUTURE MODIFICATIONS)'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199324, prescription_id FROM Prescriptions WHERE prescription_name IN ('15.408(l)','15.408(m)','215.408(4)(ii)'); -- [PROPOSAL REQUIREMENTS] 'SUBMISSION VIA ELECTRONIC MEDIA IS REQUIRED (BASIC AWARD AND FUTURE MODIFICATIONS)'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199325, prescription_id FROM Prescriptions WHERE prescription_name IN ('15.209(e)'); -- [PROPOSAL REQUIREMENTS] 'FACSIMILE PROPOSALS ARE AUTHORIZED'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199326, prescription_id FROM Prescriptions WHERE prescription_name IN ('15.209(a)(2)'); -- [PROPOSAL REQUIREMENTS] 'ALTERNATE PROPOSALS ARE ACCEPTABLE'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199328, prescription_id FROM Prescriptions WHERE prescription_name IN ('16.506(g)'); -- [AWARD SCENARIOS] 'MULTIPLE AWARDS ARE NOT PRACTICABLE'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199329, prescription_id FROM Prescriptions WHERE prescription_name IN ('14.201-6(q)','16.506(f)'); -- [AWARD SCENARIOS] 'MULTIPLE AWARDS ARE POSSIBLE'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199330, prescription_id FROM Prescriptions WHERE prescription_name IN ('15.209(a)(1)'); -- [AWARD SCENARIOS] 'AWARD AFTER DISCUSSIONS WITH OFFERORS IN THE COMPETITIVE RANGE'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199331, prescription_id FROM Prescriptions WHERE prescription_name IN ('15.209(a)','15.209(a)(1)','15.209(a)(2)'); -- [AWARD SCENARIOS] 'AWARD WITHOUT DISCUSSIONS'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199333, prescription_id FROM Prescriptions WHERE prescription_name IN ('15.408(n)(2)(iii)'); -- [EXCESS PASS-THROUGH CHARGES] 'YES'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199335, prescription_id FROM Prescriptions WHERE prescription_name IN ('13.302-5(c)','217.7406(b)'); -- [UCA] 'YES'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199337, prescription_id FROM Prescriptions WHERE prescription_name IN ('29.401-3'); -- [INAPPROPRIATE CONTINGENCY] 'YES'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199339, prescription_id FROM Prescriptions WHERE prescription_name IN ('25.1103(c)'); -- [CURRENCY] 'YES'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199341, prescription_id FROM Prescriptions WHERE prescription_name IN ('19.1309(b)','19.1309(b)(1)','217.7303'); -- [COMPETITION TYPE] 'FULL AND OPEN COMPETITION (FAR PART 6)'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199342, prescription_id FROM Prescriptions WHERE prescription_name IN ('7.203'); -- [COMPETITION TYPE] 'FEDERAL SUPPLY SCHEDULE (FAR PART 8.4)'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199343, prescription_id FROM Prescriptions WHERE prescription_name IN ('14.201-7(a)(1)','14.201-7(a)(2)','14.201-7(b)(1)','14.201-7(c)(1)','14.201-7(d)','19.309(c)','19.708(b)(1)','237.7003(a)','237.7003(a)(1)','247.270-4(b)'); -- [COMPETITION TYPE] 'SEALED BIDDING (FAR PART 14)'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199344, prescription_id FROM Prescriptions WHERE prescription_name IN ('222.7302','7.305(a)'); -- [COMPETITION TYPE] 'A-76 STANDARD COMPETITION (OMB CIRCULAR A-76)'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199345, prescription_id FROM Prescriptions WHERE prescription_name IN ('222.7302','7.305(b)'); -- [COMPETITION TYPE] 'A-76 STREAMLINED COMPETITION (OMB CIRCULAR A-76)'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199346, prescription_id FROM Prescriptions WHERE prescription_name IN ('19.811-3(c)','19.811-3(d)','19.811-3(d)(1)','19.811-3(d)(2)'); -- [COMPETITION TYPE] 'COMPETITIVE (8a) (FAR PART 19.8)'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199348, prescription_id FROM Prescriptions WHERE prescription_name IN ('13.302-5(d)(3)(i)','19.1309(a)','19.1309(a)(1)','19.708(b)','19.708(b)(1)','19.708(b)(1)(iii)','246.870-3','4.607(b)'); -- [COMPETITION TYPE] 'SET-ASIDE'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199351, prescription_id FROM Prescriptions WHERE prescription_name IN ('19.708(b)','19.708(b)(1)','19.708(b)(1)(iii)','19.811-3(a)','19.811-3(b)','219.811-3','219.811-3(2)','219.811-3(3)','22.305','36.501(b)'); -- [SMALL BUSINESS PROGRAM] '8(a) PROGRAM'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199353, prescription_id FROM Prescriptions WHERE prescription_name IN ('236.570(b)(5)'); -- [SEALED BIDDING (FAR PART 14)] 'SEALED BIDDING FOR CONSTRUCTION'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199355, prescription_id FROM Prescriptions WHERE prescription_name IN ('26.206(a)','26.206(b)','26.206(c)'); -- [SET-ASIDE] 'LOCAL AREA SET-ASIDE (STAFFORD ACT SET-ASIDE) (FAR 26.2)'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199356, prescription_id FROM Prescriptions WHERE prescription_name IN ('16.506(d)(4)','16.506(d)(5)','19.508(d)','19.508(e)','19.811-3(e)','216.506(d)','216.506(d)(1)','216.506(d)(2)'); -- [SET-ASIDE] 'PARTIAL SMALL BUSINESS SET-ASIDE'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199357, prescription_id FROM Prescriptions WHERE prescription_name IN ('19.1407','19.1506(a)','19.1506(b)','19.508((c)','19.508(d)','19.508(e)','19.811-3(e)','36.501(b)'); -- [SET-ASIDE] 'TOTAL SMALL BUSINESS SET-ASIDE'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199358, prescription_id FROM Prescriptions WHERE prescription_name IN ('19.1309(a)(1)','19.1407','19.309(a)','19.811-3(a)','19.811-3(b)','19.811-3(c)'); -- [SOLE SOURCE] 'SOLE SOURCE 8(a)'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199361, prescription_id FROM Prescriptions WHERE prescription_name IN ('14.201-6(b)(1)','14.201-6(b)(2)','14.201-6(c)(1)','14.201-6(c)(2)','14.201-6(c)(3)','14.201-6(e)','14.201-6(f)','14.201-6(g)(1)','14.201-6(g)(2)','14.201-6(h)','14.201-6(i)','14.201-6(j)','14.201-6(l)','14.201-6(m)','14.201-6(o)(1)','14.201-6(o)(2)(i)','14.201-6(o)(2)(ii)','14.201-6(q)','14.201-6(t)','32.502-3(a)','32.502-3(b)(2)','32.502-3(c)'); -- [SOLICITATION TYPE] 'INVITATION FOR BIDS'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199362, prescription_id FROM Prescriptions WHERE prescription_name IN ('15.209(c)','16.105'); -- [SOLICITATION TYPE] 'REQUEST FOR INFORMATION'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199363, prescription_id FROM Prescriptions WHERE prescription_name IN ('32.502-3(a)'); -- [SOLICITATION TYPE] 'REQUEST FOR PROPOSAL'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199370, prescription_id FROM Prescriptions WHERE prescription_name IN ('29.401-4(b)'); -- [DEPARTMENT OF DEFENSE] 'DEPARTMENT OF THE AIR FORCE'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199371, prescription_id FROM Prescriptions WHERE prescription_name IN ('29.401-4(b)'); -- [DEPARTMENT OF DEFENSE] 'DEPARTMENT OF THE NAVY/MARINE CORPS'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199372, prescription_id FROM Prescriptions WHERE prescription_name IN ('29.401-4(b)'); -- [DEPARTMENT OF DEFENSE] 'DEPARTMENT OF THE ARMY'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199373, prescription_id FROM Prescriptions WHERE prescription_name IN ('29.401-4(b)'); -- [DEPARTMENT OF DEFENSE] 'DEFENSE ADVANCED RESEARCH PROJECTS AGENCY'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199374, prescription_id FROM Prescriptions WHERE prescription_name IN ('29.401-4(b)'); -- [DEPARTMENT OF DEFENSE] 'DEFENSE THREAT REDUCTION AGENCY'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199376, prescription_id FROM Prescriptions WHERE prescription_name IN ('29.401-4(b)'); -- [DEPARTMENT OF DEFENSE] 'MISSILE DEFENSE AGENCY'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199377, prescription_id FROM Prescriptions WHERE prescription_name IN ('12.301(b)(2)','19.309(a)(2)','44.204(a)(2)'); -- [CIVILIAN] 'COAST GUARD'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199378, prescription_id FROM Prescriptions WHERE prescription_name IN ('29.401-4(b)'); -- [CIVILIAN] 'DEPARTMENT OF ENERGY'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199379, prescription_id FROM Prescriptions WHERE prescription_name IN ('29.401-4(b)'); -- [CIVILIAN] 'DEPARTMENT OF AGRICULTURE'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199380, prescription_id FROM Prescriptions WHERE prescription_name IN ('29.401-4(b)'); -- [CIVILIAN] 'DEPARTMENT OF HEALTH AND HUMAN SERVICES'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199381, prescription_id FROM Prescriptions WHERE prescription_name IN ('29.401-4(b)'); -- [CIVILIAN] 'DEPARTMENT OF INTERIOR'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199382, prescription_id FROM Prescriptions WHERE prescription_name IN ('29.401-4(b)'); -- [CIVILIAN] 'DEPARTMENT OF LABOR'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199383, prescription_id FROM Prescriptions WHERE prescription_name IN ('29.401-4(b)'); -- [CIVILIAN] 'DEPARTMENT OF TRANSPORTATION'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199384, prescription_id FROM Prescriptions WHERE prescription_name IN ('29.401-4(b)'); -- [CIVILIAN] 'GENERAL SERVICES ADMINISTRATION'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199402, prescription_id FROM Prescriptions WHERE prescription_name IN ('14.201-6(r)','14.201-6(s)','3.103-1'); -- [2 -STEP SEALED BIDDING] 'STEP 1 OF 2 STEP SEALED BIDDING'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199403, prescription_id FROM Prescriptions WHERE prescription_name IN ('14.201-6(t)'); -- [2 -STEP SEALED BIDDING] 'STEP 2 OF 2 STEP SEALED BIDDING'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199404, prescription_id FROM Prescriptions WHERE prescription_name IN ('32.502-4(d)'); -- [AGREEMENT (INCLUDING BASIC AND LOAN)] 'BASIC ORDERING AGREEMENT'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199406, prescription_id FROM Prescriptions WHERE prescription_name IN ('42.1107(a)'); -- [AGREEMENT (INCLUDING BASIC AND LOAN)] 'BLANKET PURCHASE AGREEMENT UNDER A FEDERAL SUPPLY SCHEDULE'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199407, prescription_id FROM Prescriptions WHERE prescription_name IN ('28.312','7.404','8.1104(a)','8.1104(b)','8.1104(c)','8.1104(d)'); -- [AGREEMENT (INCLUDING BASIC AND LOAN)] 'LEASE AGREEMENT'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199408, prescription_id FROM Prescriptions WHERE prescription_name IN ('19.1506(a)'); -- [SMALL BUSINESS/ SOCIOECONOMIC SOURCE] 'ECONOMICALLY DISADVANTAGED WOMEN-OWNED SMALL BUSINESS (EDWOSB)'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199409, prescription_id FROM Prescriptions WHERE prescription_name IN ('36.501(b)'); -- [SMALL BUSINESS/ SOCIOECONOMIC SOURCE] 'WOMEN-OWNED SMALL BUSINESS (WOSB)'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199410, prescription_id FROM Prescriptions WHERE prescription_name IN ('19.1506(b)'); -- [SMALL BUSINESS/ SOCIOECONOMIC SOURCE] 'WOMEN-OWNED SMALL BUSINESS CONCERNS ELIGIBLE UNDER THE WOMEN-OWNED SMALL BUSINESS PROGRAM'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199412, prescription_id FROM Prescriptions WHERE prescription_name IN ('19.1309(a)','19.1309(a)(1)','36.501(b)'); -- [SMALL BUSINESS/ SOCIOECONOMIC SOURCE] 'HISTORICALLY UNDERUTILIZED BUSINESS ZONE (HUBZone)'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199414, prescription_id FROM Prescriptions WHERE prescription_name IN ('19.811-3(b)','19.811-3(c)'); -- [SMALL BUSINESS/ SOCIOECONOMIC SOURCE] 'SBA CERTIFIED 8(A) PROGRAM PARTICIPANT'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199416, prescription_id FROM Prescriptions WHERE prescription_name IN ('19.1407','36.501(b)'); -- [SMALL BUSINESS/ SOCIOECONOMIC SOURCE] 'SERVICE-DISABLED VETERAN-OWNED SMALL BUSINESS (SDVOSB)'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199417, prescription_id FROM Prescriptions WHERE prescription_name IN ('242.7204','32.502-4(b)'); -- [SMALL BUSINESS/ SOCIOECONOMIC SOURCE] 'SMALL BUSINESS'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199418, prescription_id FROM Prescriptions WHERE prescription_name IN ('19.811-3(a)','19.811-3(c)'); -- [SMALL BUSINESS/ SOCIOECONOMIC SOURCE] 'SMALL BUSINESS ADMINISTRATION (SBA)'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199420, prescription_id FROM Prescriptions WHERE prescription_name IN ('32.611(a)','32.611(b)','49.502(b)(1)(iii)','49.502(b)(2)','49.503(a)(3)','49.503(a)(4)'); -- [GOVERNMENT SOURCE] 'FEDERAL GOVERNMENT'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199421, prescription_id FROM Prescriptions WHERE prescription_name IN ('242.7001','30.201-4(e)'); -- [GOVERNMENT SOURCE] 'FEDERALLY FUNDED RESEARCH AND DEVELOPMENT CENTER (FFRDC)'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199422, prescription_id FROM Prescriptions WHERE prescription_name IN ('15.209(b)(3)','16.307(a)(4)','16.307(a)(5)','32.611(a)','32.611(b)','49.502(b)(1)(iii)','49.502(b)(2)','49.503(a)(3)','49.503(a)(4)'); -- [GOVERNMENT SOURCE] 'LOCAL GOVERNMENT'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199424, prescription_id FROM Prescriptions WHERE prescription_name IN ('15.209(b)(3)','16.307(a)(4)','16.307(a)(5)','22.407(b)','32.611(a)','32.611(b)','49.502(b)(1)(iii)','49.502(b)(2)','49.503(a)(3)','49.503(a)(4)'); -- [GOVERNMENT SOURCE] 'STATE GOVERNMENT'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199425, prescription_id FROM Prescriptions WHERE prescription_name IN ('22.202'); -- [GOVERNMENT SOURCE] 'STATE PRISON'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199426, prescription_id FROM Prescriptions WHERE prescription_name IN ('22.202'); -- [GOVERNMENT SOURCE] 'FEDERAL PRISON INDUSTRIES (FPI)'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199427, prescription_id FROM Prescriptions WHERE prescription_name IN ('215.408(3)(i)','215.408(3)(ii)'); -- [FOREIGN SOURCE] 'CANADIAN COMMERCIAL CORPORATION'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199428, prescription_id FROM Prescriptions WHERE prescription_name IN ('225.372-2'); -- [FOREIGN SOURCE] 'REPRESENTATIVE OF A FOREIGN GOVERNMENT'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199429, prescription_id FROM Prescriptions WHERE prescription_name IN ('229.402-1','229.402-70(a)','229.402-70(a)(1)','229.402-70(a)(2)','30.201-4(c)'); -- [FOREIGN SOURCE] 'FOREIGN COMPANY'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199430, prescription_id FROM Prescriptions WHERE prescription_name IN ('225.372-2','29.402-1(a)','29.402-1(b)','29.402-2(a)','29.402-2(b)','3.202','30.201-4(c)','32.611(a)','32.611(b)','33.215(a)','49.502(b)(1)(iii)','49.502(b)(2)','49.503(a)(3)','49.503(a)(4)'); -- [FOREIGN SOURCE] 'FOREIGN GOVERNMENT'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199431, prescription_id FROM Prescriptions WHERE prescription_name IN ('225.372-2'); -- [FOREIGN SOURCE] 'FOREIGN CORPORATION WHOLLY OWNED BY A FOREIGN GOVERNMENT'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199432, prescription_id FROM Prescriptions WHERE prescription_name IN ('228.370(e)'); -- [FOREIGN SOURCE] 'SPANISH COMPANY'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199433, prescription_id FROM Prescriptions WHERE prescription_name IN ('242.7001'); -- [EDUCATIONAL SOURCE] 'UNIVERSITY ASSOCIATED RESEARCH CENTER (UARC)'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199434, prescription_id FROM Prescriptions WHERE prescription_name IN ('15.209(b)(3)','16.307(a)(3)','16.307(a)(5)','16.307(e)(2)','16.307(f)(2)','16.307(g)','209.470-4(a)','209.470-4(b)','232.412-70(a)','242.7001','242.7204','30.201-4(e)','4.404(a)','45.107(a)(3)','49.502(a)(1)','49.502(a)(2)','49.502(b)(1)(i)','49.502(b)(1)(ii)','49.502(b)(1)(iii)','49.502(d)','49.503(a)(1)','49.503(a)(2)','49.503(a)(3)','49.503(a)(4)','49.504(b)'); -- [EDUCATIONAL SOURCE] 'EDUCATIONAL INSTITUTION'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199435, prescription_id FROM Prescriptions WHERE prescription_name IN ('15.209(b)(3)','16.307(e)(2)','16.307(f)(2)','232.412-70(a)','242.7204','45.107(a)(3)','48.201','48.201(c)','48.201(d)','48.201(e)(1)','49.502(a)(1)','49.502(a)(2)','49.502(b)(1)(i)','49.502(b)(1)(ii)','49.502(b)(1)(iii)','49.502(d)','49.503(a)(1)','49.503(a)(2)','49.503(a)(3)','49.503(a)(4)','49.504(b)'); -- [MISCELLANEOUS SOURCE] 'NON-PROFIT ORGANIZATION'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199436, prescription_id FROM Prescriptions WHERE prescription_name IN ('16.307(a)(5)'); -- [MISCELLANEOUS SOURCE] 'NONPROFIT ORGANIZATION EXEMPTED UNDER OMB CIRCULAR NO. A-122'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199437, prescription_id FROM Prescriptions WHERE prescription_name IN ('48.201','48.201(c)','48.201(d)','48.201(e)(1)'); -- [MISCELLANEOUS SOURCE] 'NOT-FOR-PROFIT ORGANIZATION'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199438, prescription_id FROM Prescriptions WHERE prescription_name IN ('216.203-4-70(b)'); -- [MISCELLANEOUS SOURCE] 'PRODUCER OF STANDARD STEEL MILL ITEMS'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199439, prescription_id FROM Prescriptions WHERE prescription_name IN ('25.301-4'); -- [MISCELLANEOUS SOURCE] 'SELF-EMPLOYED INDIVIDUAL'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199441, prescription_id FROM Prescriptions WHERE prescription_name IN ('2014-O0010','2015-O0007'); -- [SUPPLIES] 'AMERICAN FLAG'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199444, prescription_id FROM Prescriptions WHERE prescription_name IN ('23.705(b)(1)'); -- [SUPPLIES] 'IMAGING EQUIPMENT'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199445, prescription_id FROM Prescriptions WHERE prescription_name IN ('46.307(a)','46.308'); -- [SUPPLIES] 'DATA, DRAWINGS AND REPORTS'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199446, prescription_id FROM Prescriptions WHERE prescription_name IN ('246.870-3'); -- [SUPPLIES] 'ELECTRONIC PARTS'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199450, prescription_id FROM Prescriptions WHERE prescription_name IN ('225.7003-5(a)(2)','228.370(d)'); -- [SUPPLIES] 'MISSILES OR SPACE SYSTEMS OR COMPONENTS THEREOF'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199454, prescription_id FROM Prescriptions WHERE prescription_name IN ('246.371(a)'); -- [SUPPLIES] '(SUB) SYSTEMS AND (SUB) ASSEMBLIES INTEGRAL TO A SYSTEM'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199456, prescription_id FROM Prescriptions WHERE prescription_name IN ('239.7306(a)'); -- [SUPPLIES] 'INFORMATION TECHNOLOGY'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199459, prescription_id FROM Prescriptions WHERE prescription_name IN ('48.201','48.201(c)','48.201(d)','48.201(e)(1)'); -- [SERVICES] 'PRODUCT OR COMPONENT IMPROVEMENT'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199460, prescription_id FROM Prescriptions WHERE prescription_name IN ('237.270(d)(1)','237.270(d)(2)'); -- [SERVICES] 'AUDIT SERVICES'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199461, prescription_id FROM Prescriptions WHERE prescription_name IN ('237.7402'); -- [SERVICES] 'ACTIVITIES RELATED TO CLOSURE OF A MILITARY INSTALLATION'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199462, prescription_id FROM Prescriptions WHERE prescription_name IN ('11.404(a)(2)','11.404(a)(3)','11.404(b)','11.503','11.503(c)','11.703(c)','14.201-6(e)','14.201-6(i)','14.201-6(j)','14.201-6(l)','14.201-6(m)','15.408(f)(1)','15.408(f)(2)','16.307(a)(2)','16.307(b)','16.307(c)','19.708(b)','19.708(b)(1)','19.708(b)(1)(iii)','22.407(a)','22.407(b)','22.407(h)','22.505-(b)(1)','22.505-(b)(2)','22.810(b)','22.810(c)','22.810(d)','22.810(f)','223.7306','225.1103(1)','225.371-5','225.7503(a)','225.7503(a)(1)','225.7503(a)(2)','225.7503(b)','225.7503(b)(1)','225.7503(b)(2)','225.7503(b)(3)','225.7503(b)(4)','236.570(a)','236.570(b)(1)','236.570(b)(2)(i)','236.570(b)(2)(ii)','236.570(b)(4)','236.570(b)(5)','236.570(b)(6)','236.570(c)(1)','236.570(c)(2)','246.270-4','246.710(2)','247.574(b)','247.574(b)(3)','25.1102(a)','25.1102(c)','25.1102(c)(3)','25.1102(e)(1)','28.1102-3(a)','28.1102-3(b)','28.311-1','29.401-2','32.111(a)(5)','32.908(b)','36.501(b)','36.502','36.503','36.504','36.505','36.506','36.507','36.508','36.509','36.510','36.511','36.512','36.513','36.514','36.515','36.516','36.517','36.518','36.519','36.520','36.521','36.522','37.110(a)','37.110(b)','4.404(a)','42.1107(a)','42.1305(a)','43.205(b)(4)','43.205(e)','46.312','46.710(e)(1)','46.710(e)(2)','47.507(a)(3)','48.202','49.502(b)(1)(ii)','49.502(b)(1)(iii)','49.503(a)(2)','49.503(a)(3)','49.504(c)(1)','49.504(c)(3)'); -- [SERVICES] 'CONSTRUCTION'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199463, prescription_id FROM Prescriptions WHERE prescription_name IN ('239.7411(a)','32.111(a)(1)','32.111(a)(6)'); -- [SERVICES] 'COMMUNICATION SERVICES (AND FACILITIES)'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199464, prescription_id FROM Prescriptions WHERE prescription_name IN ('228.170','36.502','36.503','36.506','36.507','36.508','36.509','36.510','36.512','36.513','36.514','36.521','36.522','37.304(a)','37.304(b)','37.304(c)','43.205(b)(4)','46.313','49.502(a)(2)','49.502(b)(1)(i)','49.502(b)(1)(ii)','49.502(b)(1)(iii)','49.502(b)(2)','49.504(c)(2)','49.504(c)(3)'); -- [SERVICES] 'DISMANTLING, DEMOLITION, OR REMOVAL OF IMPROVEMENTS'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199465, prescription_id FROM Prescriptions WHERE prescription_name IN ('29.401-1'); -- [SERVICES] 'LEASED EQUIPMENT'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199466, prescription_id FROM Prescriptions WHERE prescription_name IN ('237.7101(e)','237.7101(e)(1)','237.7101(e)(3)','237.7101(g)'); -- [SERVICES] 'LAUNDRY AND DRY CLEANING SERVICES'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199468, prescription_id FROM Prescriptions WHERE prescription_name IN ('22.103-5(b)'); -- [SERVICES] 'OPERATION OF VESSELS'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199469, prescription_id FROM Prescriptions WHERE prescription_name IN ('32.611(a)','32.611(b)'); -- [SERVICES] 'PAID ADVERTISEMENTS'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199470, prescription_id FROM Prescriptions WHERE prescription_name IN ('28.309(a)','28.309(b)'); -- [SERVICES] 'PUBLIC WORKS'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199472, prescription_id FROM Prescriptions WHERE prescription_name IN ('26.304'); -- [SERVICES] 'STUDIES'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199475, prescription_id FROM Prescriptions WHERE prescription_name IN ('15.209(b)(1)','15.209(b)(3)','15.209(b)(4)','15.408(f)(1)','15.408(f)(2)','225.7204(a)','225.7204(b)','3.103-1','41.501(b)','41.501(c)(1)','41.501(c)(2)','41.501(c)(3)','41.501(c)(4)','41.501(c)(5)','41.501(d)(1)','41.501(d)(2)','41.501(d)(3)','41.501(d)(4)','41.501(d)(5)','41.501(d)(6)','41.501(d)(7)'); -- [SERVICES] 'UTILITY SERVICES'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199477, prescription_id FROM Prescriptions WHERE prescription_name IN ('225.7003-5(a)(2)','228.370(b)','228.370(d)'); -- [AIRCRAFT] 'AIRCRAFT OR COMPONENTS THEREOF'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199478, prescription_id FROM Prescriptions WHERE prescription_name IN ('25.1101(d)'); -- [AIRCRAFT] 'CIVIL AIRCRAFT AND RELATED ARTICLES'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199479, prescription_id FROM Prescriptions WHERE prescription_name IN ('225.1103(4)'); -- [AMMUNITION AND EXPLOSIVES] 'ITEMS COVERED BY THE U.S. MUNITIONS LIST'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199480, prescription_id FROM Prescriptions WHERE prescription_name IN ('223.370-5','223.7203','225.7003-5(a)(2)'); -- [AMMUNITION AND EXPLOSIVES] 'ARMS, AMMUNITION OR EXPLOSIVES'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199481, prescription_id FROM Prescriptions WHERE prescription_name IN ('225.1101(10)(i)','225.1101(10)(i)(A)','225.1101(10)(i)(B)','225.1101(10)(i)(C)','225.1101(10)(i)(D)','225.1101(10)(i)(E)','225.1101(10)(i)(F)','239.7103(a)','239.7306(a)','239.7306(b)','239.7603(a)','25.1101(b)(1)(i)','25.1101(b)(1)(ii)','25.1101(b)(1)(iii)','25.1101(b)(1)(iv)'); -- [COMPUTERS/INFORMATION TECHNOLOGY] 'INFORMATION TECHNOLOGY'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199482, prescription_id FROM Prescriptions WHERE prescription_name IN ('23.705(b)(2)','23.705(d)(1)'); -- [COMPUTERS/INFORMATION TECHNOLOGY] 'PERSONAL COMPUTER PRODUCTS'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199483, prescription_id FROM Prescriptions WHERE prescription_name IN ('225.7012-3'); -- [COMPUTERS/INFORMATION TECHNOLOGY] 'SUPERCOMPUTER'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199485, prescription_id FROM Prescriptions WHERE prescription_name IN ('26.404'); -- [FOOD/TEXTILES] 'FOOD'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199486, prescription_id FROM Prescriptions WHERE prescription_name IN ('217.7303'); -- [FOOD/TEXTILES] 'CLOTHING OR TEXTILES'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199487, prescription_id FROM Prescriptions WHERE prescription_name IN ('217.7303','47.305-17'); -- [FUELS AND LUBRICANTS] 'FUELS'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199488, prescription_id FROM Prescriptions WHERE prescription_name IN ('229.402-70(j)'); -- [FUELS AND LUBRICANTS] 'FUELS AND LUBRICANT FOR USE IN TAXI'S (OTHER THAN PASSENGER VEHICLES)'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199489, prescription_id FROM Prescriptions WHERE prescription_name IN ('229.402-70(i)'); -- [FUELS AND LUBRICANTS] 'FUELS AND LUBRICANT FOR USE IN PASSENGER VEHICLES (EXCLUDING TAXI'S)'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199490, prescription_id FROM Prescriptions WHERE prescription_name IN ('15.408(f)(1)','15.408(f)(2)','225.7204(a)','225.7204(b)'); -- [FUELS AND LUBRICANTS] 'PETROLEUM'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199491, prescription_id FROM Prescriptions WHERE prescription_name IN ('225.7204(a)','225.7204(b)'); -- [FUELS AND LUBRICANTS] 'NATURAL GAS'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199492, prescription_id FROM Prescriptions WHERE prescription_name IN ('225.7003-5(a)(1)'); -- [METALS] 'SPECIALTY METALS AS END ITEMS'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199493, prescription_id FROM Prescriptions WHERE prescription_name IN ('225.7011-3'); -- [METALS] 'CARBON, ALLOY OR ARMOR STEEL PLATE'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199494, prescription_id FROM Prescriptions WHERE prescription_name IN ('216.203-4-70(a)(1)'); -- [METALS] 'BASIC STEEL, ALUMINUM, BRASS, BRONZE, OR COPPER MILL PRODUCTS SUCH AS SHEETS, PLATES AND BARS'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199495, prescription_id FROM Prescriptions WHERE prescription_name IN ('225.7204(a)','225.7204(b)'); -- [METALS] 'ORES'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199496, prescription_id FROM Prescriptions WHERE prescription_name IN ('23.303','23.303(b)'); -- [MISCELLANEOUS END ITEMS] 'HAZARDOUS MATERIAL'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199497, prescription_id FROM Prescriptions WHERE prescription_name IN ('23.804(a)'); -- [MISCELLANEOUS END ITEMS] 'OZONE DEPLETING SUBSTANCES'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199498, prescription_id FROM Prescriptions WHERE prescription_name IN ('225.7009-5'); -- [MISCELLANEOUS END ITEMS] 'BALL OR ROLLER BEARINGS AS AN END ITEM'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199499, prescription_id FROM Prescriptions WHERE prescription_name IN ('225.1101(10)(i)','225.1101(10)(i)(A)','225.1101(10)(i)(B)','225.1101(10)(i)(C)','225.1101(10)(i)(D)','225.1101(10)(i)(E)','225.1101(10)(i)(F)','225.1101(2)(i)','225.1101(2)(ii)','225.1101(2)(iii)','25.1101(1)','25.1101(1)(ii)'); -- [MISCELLANEOUS END ITEMS] 'END ITEM LISTED IN DFARS SUBPART 225.401-70 (SUBJECT TO TRADE AGREEMENTS)'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199500, prescription_id FROM Prescriptions WHERE prescription_name IN ('22.1505(a)'); -- [MISCELLANEOUS END ITEMS] 'PRODUCT INCLUDED IN THE LIST OF PRODUCTS REQUIRING CONTRACTOR CERTIFICATION AS TO FORCED OR INDENTURED CHILD LABOR'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199501, prescription_id FROM Prescriptions WHERE prescription_name IN ('13.302-5(b)'); -- [MISCELLANEOUS END ITEMS] 'SUBSCRIPTIONS OR OTHER CHARGES FOR NEWSPAPERS, MAGAZINES, PERIODICALS, OR OTHER PUBLICATIONS'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199502, prescription_id FROM Prescriptions WHERE prescription_name IN ('225.7307(b)'); -- [MISCELLANEOUS END ITEMS] 'SUPPLIES FOR INTERNATIONAL MILITARY EDUCATION TRAINING'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199503, prescription_id FROM Prescriptions WHERE prescription_name IN ('225.7204(a)','225.7204(b)'); -- [MISCELLANEOUS END ITEMS] 'TIMBER'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199504, prescription_id FROM Prescriptions WHERE prescription_name IN ('23.705(c)(1)','23.705(d)(2)'); -- [MISCELLANEOUS END ITEMS] 'TELEVISIONS'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199505, prescription_id FROM Prescriptions WHERE prescription_name IN ('225.7002-3(b)'); -- [MISCELLANEOUS END ITEMS] 'HAND OR MEASURING TOOLS'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199506, prescription_id FROM Prescriptions WHERE prescription_name IN ('23.602'); -- [MISCELLANEOUS END ITEMS] 'RADIOACTIVE MATERIAL REQUIRING SPECIFIC LICENSING UNDER REGULATIONS ISSUED PURSUANT TO THE ATOMIC ENERGY ACT OF 1954'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199507, prescription_id FROM Prescriptions WHERE prescription_name IN ('23.602'); -- [MISCELLANEOUS END ITEMS] 'RADIOACTIVE MATERIAL NOT REQUIRING SPECIFIC LICENSING IN WHICH THE SPECIFIC ACTIVITY IS GREATER THAN 0.002 MICROCURIES PER GRAM OR THE ACTIVITY PER ITEM EQUALS OR EXCEEDS 0.01 MICROCURIES'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199508, prescription_id FROM Prescriptions WHERE prescription_name IN ('225.7102-4'); -- [RING FORGINGS] 'RING FORGINGS FOR BULL GEARS (GREATER THAN 120" DIAMETER)'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199509, prescription_id FROM Prescriptions WHERE prescription_name IN ('225.7102-4'); -- [RING FORGINGS] 'RING FORGINGS FOR BULL GEARS (GREATER THAN 120" DIAMETER) AS A COMPONENT'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199510, prescription_id FROM Prescriptions WHERE prescription_name IN ('225.7003-5(a)(2)'); -- [SHIPBUILDING] 'SHIPBUILDING OR COMPONENTS THEREOF'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199511, prescription_id FROM Prescriptions WHERE prescription_name IN ('225.7102-4'); -- [SHIPBUILDING] 'SHIP PROPULSION SHAFTS (EXCLUDING SERVICE AND LANDING CRAFT SHAFTS)'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199512, prescription_id FROM Prescriptions WHERE prescription_name IN ('225.7102-4'); -- [SHIPBUILDING] 'SHIP PROPULSION SHAFTS (EXCLUDING SERVICE AND LANDING CRAFT SHAFTS) AS A COMPONENT'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199513, prescription_id FROM Prescriptions WHERE prescription_name IN ('225.7102-4'); -- [SHIPBUILDING] 'PERISCOPE TUBES'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199514, prescription_id FROM Prescriptions WHERE prescription_name IN ('225.7102-4'); -- [SHIPBUILDING] 'PERISCOPE TUBES AS A COMPONENT'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199515, prescription_id FROM Prescriptions WHERE prescription_name IN ('225.7006-4(a)','225.7006-4(b)'); -- [SHIPBUILDING] 'AIR CIRCUIT BREAKERS FOR NAVAL VESSELS'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199516, prescription_id FROM Prescriptions WHERE prescription_name IN ('225.7007-3'); -- [SHIPBUILDING] 'PERFORMANCE REQUIRES WELDED SHIPBOARD ANCHOR OR MOORING CHAIN 4" OR LESS DIAMETER'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199517, prescription_id FROM Prescriptions WHERE prescription_name IN ('225.7003-5(a)(2)'); -- [VEHICLES] 'TANK OR AUTOMOTIVE ITEMS OR COMPONENTS'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199518, prescription_id FROM Prescriptions WHERE prescription_name IN ('28.312','8.1104(a)','8.1104(b)','8.1104(c)','8.1104(d)'); -- [VEHICLES] 'MOTOR VEHICLES'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199519, prescription_id FROM Prescriptions WHERE prescription_name IN ('246.370'); -- [FOOD] 'FRESH MILK AND DAIRY PRODUCTS'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199520, prescription_id FROM Prescriptions WHERE prescription_name IN ('14.201-6(g)(2)','225.7204(a)','225.7204(b)'); -- [FOOD] 'PERISHABLE SUBSISTENCE'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199521, prescription_id FROM Prescriptions WHERE prescription_name IN ('217.7303','225.7204(a)','225.7204(b)','246.370'); -- [FOOD] 'SUBSISTENCE'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199522, prescription_id FROM Prescriptions WHERE prescription_name IN ('16.506(d)(3)','16.506(d)(5)','225.7204(a)','225.7204(b)'); -- [FOOD] 'SUBSISTENCE FOR BOTH GOVERNMENT USE AND RESALE IN THE SAME SCHEDULE'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199523, prescription_id FROM Prescriptions WHERE prescription_name IN ('225.7703-4(d)'); -- [CLOTHING OR TEXTILES] 'TEXTILE COMPONENTS INTENDED TO SUPPLY THE AFGHAN NATIONAL ARMY OR THE AFGHAN NATIONAL POLICE FOR PURPOSES OF PRODUCTION OF UNIFORMS'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199525, prescription_id FROM Prescriptions WHERE prescription_name IN ('209.571-8(b)','48.201','48.201(c)','48.201(d)','48.201(e)(1)'); -- [ARCHITECT-ENGINEERING SERVICES] 'ENGINEERING'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199526, prescription_id FROM Prescriptions WHERE prescription_name IN ('11.404(a)(2)','11.404(a)(3)','15.408(f)(1)','15.408(f)(2)','236.609-70','236.609-70(b)','28.311-1','32.111(c)(1)','36.609-1(c)','36.609-2(b)','36.609-3','36.609-4','4.404(a)','42.1305(a)','43.205(a)(2)','44.204(a)(1)','44.204(b)','48.201','48.201(c)','48.201(d)','49.502(a)(1)','49.502(b)(1)(ii)','49.502(b)(1)(iii)','49.503(b)'); -- [ARCHITECT-ENGINEERING SERVICES] 'ARCHITECT-ENGINEERING'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199527, prescription_id FROM Prescriptions WHERE prescription_name IN ('16.603-4(c)','217.7104(a)','217.7702'); -- [MAINTENANCE, REPAIR, INSTALLATION AND OVERHAUL SERVICES] 'CONVERSION, ALTERATION, OR REPAIR OF SHIPS'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199528, prescription_id FROM Prescriptions WHERE prescription_name IN ('223.7306'); -- [MAINTENANCE, REPAIR, INSTALLATION AND OVERHAUL SERVICES] 'MAINTENANCE AND REPAIR'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199529, prescription_id FROM Prescriptions WHERE prescription_name IN ('23.804(b)'); -- [MAINTENANCE, REPAIR, INSTALLATION AND OVERHAUL SERVICES] 'MAINTENANCE, REPAIR, OR DISPOSAL OF ANY EQUIPMENT OR APPLIANCE USING OZONE-DEPLETING SUBSTANCE AS A REFRIGERANT'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199531, prescription_id FROM Prescriptions WHERE prescription_name IN ('222.7201(b)'); -- [MAINTENANCE, REPAIR, INSTALLATION AND OVERHAUL SERVICES] 'PORTER, JANITORIAL, OR FACILITY/EQUIPMENT MAINTENANCE'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199534, prescription_id FROM Prescriptions WHERE prescription_name IN ('228.370(b)','228.370(d)'); -- [MAINTENANCE, REPAIR, INSTALLATION AND OVERHAUL SERVICES] 'DEVELOPMENT, MODIFICATION, MAINTENANCE, REPAIR, OPERATION OR OVERHAUL OF AIRCRAFT, MISSILES OR SPACE VEHICLES'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199536, prescription_id FROM Prescriptions WHERE prescription_name IN ('217.7702'); -- [MAINTENANCE, REPAIR, INSTALLATION AND OVERHAUL SERVICES] 'OVER AND ABOVE WORK'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199537, prescription_id FROM Prescriptions WHERE prescription_name IN ('26.304','45.107(a)(3)'); -- [RESEARCH, DEVELOPMENT AND EXPERIMENTAL SERVICES] 'BASIC RESEARCH'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199538, prescription_id FROM Prescriptions WHERE prescription_name IN ('16.307(e)(2)','16.307(f)(2)','16.307(g)','204.470-3','235.070-3','235.072(c)','235.072(d)','246.370','32.111(a)(2)','4.404(a)','43.107','43.205(a)(3)','43.205(a)(6)','43.205(b)(6)','43.205(f)','46.307(a)','46.308','46.309','46.316','46.710(b)(1)','46.710(b)(2)','46.710(b)(3)','46.710(b)(4)','48.201','48.201(c)','48.201(d)','48.201(e)(1)','49.502(a)(1)','49.502(d)','49.503(a)(1)','49.503(a)(2)','49.503(a)(3)','49.503(a)(4)','49.504(b)'); -- [RESEARCH, DEVELOPMENT AND EXPERIMENTAL SERVICES] 'RESEARCH AND DEVELOPMENT'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199540, prescription_id FROM Prescriptions WHERE prescription_name IN ('235.072'); -- [RESEARCH, DEVELOPMENT AND EXPERIMENTAL SERVICES] 'RESEARCH INVOLVING HUMANS'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199541, prescription_id FROM Prescriptions WHERE prescription_name IN ('235.072(a)'); -- [RESEARCH, DEVELOPMENT AND EXPERIMENTAL SERVICES] 'RESEARCH, DEVELOPMENT, TEST AND EVALUATION OR TRAINING THAT USE LIVE VERTEBRATE ANIMALS'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199542, prescription_id FROM Prescriptions WHERE prescription_name IN ('223.7203'); -- [RESEARCH, DEVELOPMENT AND EXPERIMENTAL SERVICES] 'DEVELOPMENT OF ARMS, AMMUNITION, AND EXPLOSIVES (AA&E)'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199543, prescription_id FROM Prescriptions WHERE prescription_name IN ('24.104(a)','24.104(b)','39.106'); -- [RESEARCH, DEVELOPMENT AND EXPERIMENTAL SERVICES] 'DESIGN DEVELOPMENT, OR OPERATION OF A SYSTEM OF RECORDS ON INDIVIDUALS TO ACCOMPLISH AN AGENCY FUNCTION'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199544, prescription_id FROM Prescriptions WHERE prescription_name IN ('26.304'); -- [SUPPORT SERVICES] 'SERVICES OF THE TYPE NORMALLY ACQUIRED FROM HIGHER EDUCATIONAL INSTITUTIONS'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199545, prescription_id FROM Prescriptions WHERE prescription_name IN ('222.7302','23.705(a)'); -- [SUPPORT SERVICES] 'SUPPORT SERVICES'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199546, prescription_id FROM Prescriptions WHERE prescription_name IN ('225.772-5'); -- [SUPPORT SERVICES] 'SATELLITE SERVICES'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199547, prescription_id FROM Prescriptions WHERE prescription_name IN ('204.7403(a)','204.7403(b)'); -- [SUPPORT SERVICES] 'LITIGATION SUPPORT'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199548, prescription_id FROM Prescriptions WHERE prescription_name IN ('228.370(b)'); -- [SUPPORT SERVICES] 'ACTIVITIES INCIDENTAL TO THE NORMAL OPERATIONS OF AIRCRAFT'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199549, prescription_id FROM Prescriptions WHERE prescription_name IN ('225.7307(b)'); -- [SUPPORT SERVICES] 'SUPPORT OF INTERNATIONAL MILITARY EDUCATION TRAINING'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199550, prescription_id FROM Prescriptions WHERE prescription_name IN ('16.506(f)','16.506(g)'); -- [SUPPORT SERVICES] 'ADVISORY AND ASSISTANCE SERVICES'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199551, prescription_id FROM Prescriptions WHERE prescription_name IN ('16.506(d)(2)','19.708(a)','25.301-4','3.202','32.111(a)(3)','48.201','48.201(c)','48.201(d)','48.201(e)(1)','49.505(a)'); -- [SUPPORT SERVICES] 'PERSONAL SERVICES'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199552, prescription_id FROM Prescriptions WHERE prescription_name IN ('239.7103(a)','39.106'); -- [SUPPORT SERVICES] 'SECURITY OF INFORMATION TECHNOLOGY'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199553, prescription_id FROM Prescriptions WHERE prescription_name IN ('22.305'); -- [SUPPORT SERVICES] 'TRANSMISSION OF INTELLIGENCE'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199554, prescription_id FROM Prescriptions WHERE prescription_name IN ('209.571-8(b)','37.115-3'); -- [SUPPORT SERVICES] 'TECHNICAL SERVICES'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199555, prescription_id FROM Prescriptions WHERE prescription_name IN ('37.115-3','43.205(a)(2)','43.205(a)(4)'); -- [SUPPORT SERVICES] 'PROFESSIONAL SERVICES'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199556, prescription_id FROM Prescriptions WHERE prescription_name IN ('44.204(a)(1)','44.204(a)(2)','44.204(a)(3)'); -- [SUPPORT SERVICES] 'REFUSE SERVICES'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199557, prescription_id FROM Prescriptions WHERE prescription_name IN ('237.7003(a)','237.7003(a)(1)','237.7003(a)(2)','237.7003(b)','44.204(a)(1)','44.204(a)(2)','44.204(a)(3)'); -- [SUPPORT SERVICES] 'MORTUARY SERVICES'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199558, prescription_id FROM Prescriptions WHERE prescription_name IN ('247.270-4(a)','247.270-4(b)','247.270-4(c)','247.270-4(d)','247.270-4(e)','247.270-4(f)','247.270-4(g)'); -- [SUPPORT SERVICES] 'STEVEDORING SERVICES'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199559, prescription_id FROM Prescriptions WHERE prescription_name IN ('37.403'); -- [SUPPORT SERVICES] 'HEALTH CARE SERVICES'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199560, prescription_id FROM Prescriptions WHERE prescription_name IN ('225.302-6','25.302-6'); -- [SUPPORT SERVICES] 'PRIVATE SECURITY FUNCTIONS'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199563, prescription_id FROM Prescriptions WHERE prescription_name IN ('22.305','28.313(a)','28.313(b)','32.111(a)(4)','32.111(c)(2)','43.205(a)(3)','43.205(a)(5)','47.207-5(d)','47.207-7(c)','49.504(a)(2)'); -- [TRANSPORTATION/STORAGE SERVICES] 'TRANSPORTATION'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199564, prescription_id FROM Prescriptions WHERE prescription_name IN ('47.207-4(a)(2)','47.207-4(b)','47.207-5( c )','47.207-7(d)','47.207-7(e)'); -- [TRANSPORTATION/STORAGE SERVICES] 'TRANSPORTATION OF HOUSEHOLD GOODS'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199565, prescription_id FROM Prescriptions WHERE prescription_name IN ('47.405'); -- [TRANSPORTATION/STORAGE SERVICES] 'TRANSPORTATION OF PERSONNEL (AND THEIR PERSONAL EFFECTS)'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199566, prescription_id FROM Prescriptions WHERE prescription_name IN ('28.313(a)','28.313(b)','32.111(a)(4)','49.504(a)(2)'); -- [TRANSPORTATION/STORAGE SERVICES] 'TRANSPORTATION RELATED SERVICES'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199567, prescription_id FROM Prescriptions WHERE prescription_name IN ('216.506(d)','216.506(d)(1)','216.506(d)(2)','247.271-3','247.271-3(a)','247.271-3(a)(1)','247.271-3(a)(2)','247.271-3(b)','247.271-3(d)','247.271-3(e)','247.271-3(g)','247.271-3(h)','247.271-3(i)','247.271-3(j)','247.271-3(k)','247.271-3(l)','247.271-3(m)','247.271-3(n)','44.204(a)(1)','44.204(a)(2)','44.204(a)(3)'); -- [TRANSPORTATION/STORAGE SERVICES] 'PREPARATION, SHIPMENT AND STORAGE OF PERSONAL PROPERTY'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199568, prescription_id FROM Prescriptions WHERE prescription_name IN ('47.207-1(a)','47.207-1(b)(1)','47.207-1(b)(2)','47.207-4(a)(2)','47.207-4(b)','47.207-5( c )','47.207-8(a)(3)'); -- [TRANSPORTATION/STORAGE SERVICES] 'RELOCATION OF FEDERAL OFFICES'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199569, prescription_id FROM Prescriptions WHERE prescription_name IN ('46.314','47.207-7(d)','47.207-9(c)'); -- [TRANSPORTATION/STORAGE SERVICES] 'FREIGHT TRANSPORTATION SERVICES (INCLUDING LOCAL DRAYAGE)'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199570, prescription_id FROM Prescriptions WHERE prescription_name IN ('247.271-3','247.271-3(a)','247.271-3(a)(1)','247.271-3(a)(2)'); -- [TRANSPORTATION/STORAGE SERVICES] 'ADDITIONAL (OTHER) TRANSPORTATION SERVICES'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199571, prescription_id FROM Prescriptions WHERE prescription_name IN ('16.307(e)(1)','16.307(e)(2)','32.611(a)','32.611(b)','46.308','49.502(d)','49.503(a)(1)','49.503(a)(2)','49.503(a)(3)','49.503(a)(4)','49.504(b)','49.505(b)'); -- [COST REIMBURSEMENT] 'COST NO FEE'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199572, prescription_id FROM Prescriptions WHERE prescription_name IN ('216.406','216.406(e)(2)'); -- [COST REIMBURSEMENT] 'COST PLUS AWARD FEE'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199573, prescription_id FROM Prescriptions WHERE prescription_name IN ('11.503(b)','16.307(b)','16.307(c)'); -- [COST REIMBURSEMENT] 'COST PLUS FIXED FEE'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199574, prescription_id FROM Prescriptions WHERE prescription_name IN ('15.408(a)(2)','16.307(d)','22.103-5(b)','48.202'); -- [COST REIMBURSEMENT] 'COST PLUS INCENTIVE FEE (COST BASED)'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199575, prescription_id FROM Prescriptions WHERE prescription_name IN ('16.307(e)(1)','16.307(e)(2)','16.307(f)(1)','16.307(f)(2)'); -- [COST REIMBURSEMENT] 'COST SHARING'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199576, prescription_id FROM Prescriptions WHERE prescription_name IN ('15.408(n)(2)','15.408(n)(2)(iii)','17.208(c)','246.710(1)','3.103-1','42.709-6','44.204(c)'); -- [FIXED PRICE] 'FIRM FIXED PRICE'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199577, prescription_id FROM Prescriptions WHERE prescription_name IN ('216.406','216.406(e)(2)'); -- [FIXED PRICE] 'FIXED PRICE AWARD FEE'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199578, prescription_id FROM Prescriptions WHERE prescription_name IN ('15.408(a)(1)','15.408(n)(2)','15.408(n)(2)(iii)','16.406(a)','2015-00017','216.406','216.406(e)(2)','234.203(1)','234.203(2)','242.7503','246.710(1)','246.710(1)(i)','246.710(1)(ii)','42.709-6','42.802','46.302','46.710(a)(4)','46.710(a)(5)','46.710(b)(3)','46.710(c)(3)','48.202'); -- [FIXED PRICE] 'FIXED PRICE INCENTIVE (COST BASED)'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199579, prescription_id FROM Prescriptions WHERE prescription_name IN ('15.408(a)(1)','15.408(n)(2)','15.408(n)(2)(iii)','16.406(b)','2015-00017','234.203(1)','234.203(2)','242.7503','246.710(1)','246.710(1)(i)','246.710(1)(ii)','42.709-6','42.802','46.302','46.710(a)(4)','46.710(a)(5)','46.710(b)(3)','46.710(c)(3)','48.202'); -- [FIXED PRICE] 'FIXED PRICE INCENTIVE (SUCCESSIVE TARGETS)'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199581, prescription_id FROM Prescriptions WHERE prescription_name IN ('16.205-4','42.802'); -- [FIXED PRICE] 'FIXED PRICE RE-DETERMINATION PROSPECTIVE'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199582, prescription_id FROM Prescriptions WHERE prescription_name IN ('16.206-4','42.802','46.302'); -- [FIXED PRICE] 'FIXED PRICE RE-DETERMINATION RETROACTIVE'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199583, prescription_id FROM Prescriptions WHERE prescription_name IN ('15.408(n)(2)','15.408(n)(2)(iii)','17.208(c)','3.103-1'); -- [FIXED PRICE] 'FIXED PRICE WITH ECONOMIC PRICE ADJUSTMENT -- ACTUAL COSTS'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199584, prescription_id FROM Prescriptions WHERE prescription_name IN ('15.408(n)(2)','15.408(n)(2)(iii)','17.208(c)','3.301-1'); -- [FIXED PRICE] 'FIXED PRICE WITH ECONOMIC PRICE ADJUSTMENT -- COST INDICES'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199585, prescription_id FROM Prescriptions WHERE prescription_name IN ('15.408(n)(2)','15.408(n)(2)(iii)','17.208(c)','3.301-1'); -- [FIXED PRICE] 'FIXED PRICE WITH ECONOMIC PRICE ADJUSTMENT -- ESTABLISHED PRICES'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199586, prescription_id FROM Prescriptions WHERE prescription_name IN ('49.502(a)(1)','49.502(a)(2)','49.502(b)(1)(i)','49.502(b)(1)(ii)','49.502(b)(1)(iii)'); -- [FIXED PRICE] 'FIXED PRICE WITH NO PROFIT'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199587, prescription_id FROM Prescriptions WHERE prescription_name IN ('42.1305(c)'); -- [THE SUPPLIES ARE COMMERCIAL ITEMS] 'THE SUPPLIES ARE MODIFIED COMMERCIAL ITEMS'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199588, prescription_id FROM Prescriptions WHERE prescription_name IN ('22.1803','23.406(c)','23.406(d)'); -- [THE SUPPLIES ARE COMMERCIAL ITEMS] 'THE SUPPLIES ARE COMMERCIALLY AVAILABLE OFF-THE- SHELF ITEMS (COTS)'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199589, prescription_id FROM Prescriptions WHERE prescription_name IN ('22.1705(b)'); -- [THE SUPPLIES ARE COMMERCIAL ITEMS] 'THE SUPPLIES ARE NOT ENTIRELY COMMERCIALLY AVAILABLE OFF-THE- SHELF ITEMS (COTS)'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199590, prescription_id FROM Prescriptions WHERE prescription_name IN ('22.1803'); -- [THE SUPPLIES ARE COMMERCIAL ITEMS] 'COMMERCIAL SERVICES ARE PROVIDED WITH THE COTS ITEM'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199591, prescription_id FROM Prescriptions WHERE prescription_name IN ('22.1310(a)(1)','22.1310(a)(2)','22.1408(a)','22.1408(b)','22.1605','22.202','22.305','223.570-2','23.505','23.804(a)','23.903'); -- [UNITED STATES OUTLYING AREAS] 'AMERICAN SAMOA'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199593, prescription_id FROM Prescriptions WHERE prescription_name IN ('22.1310(a)(1)','22.1310(a)(2)','22.1408(a)','22.1408(b)','22.1605','22.1803','22.202','22.305','222.7302','223.570-2','23.505','23.804(a)','23.903'); -- [UNITED STATES OUTLYING AREAS] 'GUAM'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199596, prescription_id FROM Prescriptions WHERE prescription_name IN ('22.305'); -- [UNITED STATES OUTLYING AREAS] 'JOHNSTON ISLAND'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199600, prescription_id FROM Prescriptions WHERE prescription_name IN ('22.1310(a)(1)','22.1310(a)(2)','22.1408(a)','22.1408(b)','22.1605','22.1803','22.202','223.570-2','23.505','23.804(a)','23.903'); -- [UNITED STATES OUTLYING AREAS] 'NORTHERN MARINA ISLANDS'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199601, prescription_id FROM Prescriptions WHERE prescription_name IN ('22.1310(a)(1)','22.1310(a)(2)','22.1408(a)','22.1408(b)','22.1605','22.1803','22.202','22.305','223.570-2','23.505','23.804(a)','23.903'); -- [UNITED STATES OUTLYING AREAS] 'PUERTO RICO'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199602, prescription_id FROM Prescriptions WHERE prescription_name IN ('22.1310(a)(1)','22.1310(a)(2)','22.1408(a)','22.1408(b)','22.1605','22.1803','22.202','22.305','223.570-2','23.505','23.804(a)','23.903'); -- [UNITED STATES OUTLYING AREAS] 'U.S. VIRGIN ISLANDS'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199603, prescription_id FROM Prescriptions WHERE prescription_name IN ('223.7203','23.1005(b)','23.705(a)'); -- [GOVERNMENT OWNED OR LEASED FACILITIES/INSTALLATIONS] 'CONTRACTOR OPERATED (GOCO)'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199604, prescription_id FROM Prescriptions WHERE prescription_name IN ('23.1005(b)','23.705(a)'); -- [GOVERNMENT OWNED OR LEASED FACILITIES/INSTALLATIONS] 'GOVERNMENT OPERATED'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT 199605, prescription_id FROM Prescriptions WHERE prescription_name IN ('237.171-4'); -- [GOVERNMENT OWNED OR LEASED FACILITIES/INSTALLATIONS] 'FACILITY HOLDING DETAINEES'
/*
<h2>52.204-1</h2>
No identifier
<pre>(If applicable, the rule for the prescription for using this clause will be in the component clause</pre><br />Empty Rule
*//*
<h2>52.208-9</h2>
(C) Not " IS:" syntax
<pre>(C) CONTRACTOR MATERIAL: SUBCONTRACTS FOR SUPPLIES OR SERVICES FOR GOVERNMENT USE ARE REQUIRED TO BE PROCURED FROM THOSE LISTED ON THE PROCUREMENT LIST MAINTAINED BY THE COMMITTEE FOR PURCHASE FROM PEOPLE WHO ARE BLIND OR SEVERELY DISABLE</pre><br>

*//*
<h2>52.226-6</h2>
(F) No " IS:" found
<pre>(F) FOR USE IN UNITED STATES (50 STATES PLUS DISTRICT OF COLUMBIA)</pre>
*/
-- ProduceClause.resolveIssues() Summary: Total(1228); Merged(15); Corrected(347); Error(2)
/* ===============================================
 * Clause
   =============================================== */


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158395, prescription_id FROM Prescriptions
WHERE prescription_name IN ('201.602-70'); -- 252.201-7000


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158396, prescription_id FROM Prescriptions
WHERE prescription_name IN ('203.171-4(a)'); -- 252.203-7000


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158397, prescription_id FROM Prescriptions
WHERE prescription_name IN ('203.570-3'); -- 252.203-7001


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158398, prescription_id FROM Prescriptions
WHERE prescription_name IN ('203.97'); -- 252.203-7002


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158399, prescription_id FROM Prescriptions
WHERE prescription_name IN ('203.1004(a)'); -- 252.203-7003


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158400, prescription_id FROM Prescriptions
WHERE prescription_name IN ('203.1004(b)(2)(ii)'); -- 252.203-7004


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158401, prescription_id FROM Prescriptions
WHERE prescription_name IN ('203.171-4'); -- 252.203-7005


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158402, prescription_id FROM Prescriptions
WHERE prescription_name IN ('2015-O0010'); -- 252.203-7998 (DEVIATION 2015-00010)


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158403, prescription_id FROM Prescriptions
WHERE prescription_name IN ('2015-O0010'); -- 252.203-7999 (DEVIATION 2015-00010)


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158404, prescription_id FROM Prescriptions
WHERE prescription_name IN ('PGI 204.7108(d)(1)'); -- 252.204-0001


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158405, prescription_id FROM Prescriptions
WHERE prescription_name IN ('PGI 204.7108(d)(2)'); -- 252.204-0002


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158406, prescription_id FROM Prescriptions
WHERE prescription_name IN ('PGI 204.7108(d)(3)'); -- 252.204-0003


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158407, prescription_id FROM Prescriptions
WHERE prescription_name IN ('PGI 204.7108(d)(4)'); -- 252.204-0004


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158408, prescription_id FROM Prescriptions
WHERE prescription_name IN ('PGI 204.7108(d)(5)'); -- 252.204-0005


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158409, prescription_id FROM Prescriptions
WHERE prescription_name IN ('PGI 204.7108(d)(6)'); -- 252.204-0006


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158410, prescription_id FROM Prescriptions
WHERE prescription_name IN ('PGI 204.7108(d)(7)'); -- 252.204-0007


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158411, prescription_id FROM Prescriptions
WHERE prescription_name IN ('PGI 204.7108(d)(8)'); -- 252.204-0008


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158412, prescription_id FROM Prescriptions
WHERE prescription_name IN ('PGI 204.7108(d)(9)'); -- 252.204-0009


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158413, prescription_id FROM Prescriptions
WHERE prescription_name IN ('PGI 204.7108(d)(10)'); -- 252.204-0010


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158414, prescription_id FROM Prescriptions
WHERE prescription_name IN ('PGI 204.7108(d)(11)'); -- 252.204-0011


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158415, prescription_id FROM Prescriptions
WHERE prescription_name IN ('204.404-70(a)'); -- 252.204-7000


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158416, prescription_id FROM Prescriptions
WHERE prescription_name IN ('204.7104-1(b)(3)(iv)'); -- 252.204-7002


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158417, prescription_id FROM Prescriptions
WHERE prescription_name IN ('204.404-70(b)'); -- 252.204-7003


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158418, prescription_id FROM Prescriptions
WHERE prescription_name IN ('204.1105'); -- 252.204-7004


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158419, prescription_id FROM Prescriptions
WHERE prescription_name IN ('204.404-70(c)'); -- 252.204-7005


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158420, prescription_id FROM Prescriptions
WHERE prescription_name IN ('204.7109(a)'); -- 252.204-7006


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158421, prescription_id FROM Prescriptions
WHERE prescription_name IN ('204.1202'); -- 252.204-7007


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158422, prescription_id FROM Prescriptions
WHERE prescription_name IN ('2016-00001','204.7304(a)'); -- 252.204-7008


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 24102, prescription_id FROM Prescriptions
WHERE prescription_name IN ('2016-00001','204.7304(a)'); -- 252.204-7008 (DEVIATION 2016-00001)


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158423, prescription_id FROM Prescriptions
WHERE prescription_name IN ('204.7304(b)'); -- 252.204-7009


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158424, prescription_id FROM Prescriptions
WHERE prescription_name IN ('204.470-3'); -- 252.204-7010


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158425, prescription_id FROM Prescriptions
WHERE prescription_name IN ('204.7109(b)'); -- 252.204-7011


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158426, prescription_id FROM Prescriptions
WHERE prescription_name IN ('204.7304[c]'); -- 252.204-7012


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 24107, prescription_id FROM Prescriptions
WHERE prescription_name IN ('2016-0001','204.7304(c)'); -- 252.204-7012 (DEVIATION 2016-00001)


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158427, prescription_id FROM Prescriptions
WHERE prescription_name IN ('204.7403(a)'); -- 252.204-7013


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158428, prescription_id FROM Prescriptions
WHERE prescription_name IN ('204.7403(b)'); -- 252.204-7014


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158429, prescription_id FROM Prescriptions
WHERE prescription_name IN ('204.7403(c)'); -- 252.204-7015


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158430, prescription_id FROM Prescriptions
WHERE prescription_name IN ('205.47'); -- 252.205-7000


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158431, prescription_id FROM Prescriptions
WHERE prescription_name IN ('206.302-3-70'); -- 252.206-7000


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158432, prescription_id FROM Prescriptions
WHERE prescription_name IN ('208.7305(a)'); -- 252.208-7000


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158433, prescription_id FROM Prescriptions
WHERE prescription_name IN ('209.104-70'); -- 252.209-7002


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158434, prescription_id FROM Prescriptions
WHERE prescription_name IN ('209.470-4(a)'); -- 252.209-7003


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158435, prescription_id FROM Prescriptions
WHERE prescription_name IN ('209.409'); -- 252.209-7004


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158436, prescription_id FROM Prescriptions
WHERE prescription_name IN ('209.470-4(b)'); -- 252.209-7005


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158437, prescription_id FROM Prescriptions
WHERE prescription_name IN ('209.570-4(a)'); -- 252.209-7006


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158438, prescription_id FROM Prescriptions
WHERE prescription_name IN ('209.570-4(b)'); -- 252.209-7007


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158439, prescription_id FROM Prescriptions
WHERE prescription_name IN ('209.571-8(a)'); -- 252.209-7008


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158440, prescription_id FROM Prescriptions
WHERE prescription_name IN ('209.571-8(b)'); -- 252.209-7009


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158441, prescription_id FROM Prescriptions
WHERE prescription_name IN ('209.270-5'); -- 252.209-7010


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 24123, prescription_id FROM Prescriptions
WHERE prescription_name IN ('Preamble to clause 252.209-7991 as written in 2016-00002'); -- 252.209-7991 (DEVIATION 2016-00002)


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158442, prescription_id FROM Prescriptions
WHERE prescription_name IN ('2015-OO005'); -- 252.209-7992 (DEVIATION 2015-00005)


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158443, prescription_id FROM Prescriptions
WHERE prescription_name IN ('2014-OO0009'); -- 252.209-7993 (DEVIATION 2014-00009)


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158444, prescription_id FROM Prescriptions
WHERE prescription_name IN ('2014-O0004'); -- 252.209-7994 (DEVIATION 2014-00004)


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158445, prescription_id FROM Prescriptions
WHERE prescription_name IN ('2013-O0010'); -- 252.209-7995 (DEVIATION 2013-00010)


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158446, prescription_id FROM Prescriptions
WHERE prescription_name IN ('2013-O0006'); -- 252.209-7996 (DEVIATION 2013-00006)


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158447, prescription_id FROM Prescriptions
WHERE prescription_name IN ('2013-O0006'); -- 252.209-7997 (DEVIATION 2013-00006)


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158448, prescription_id FROM Prescriptions
WHERE prescription_name IN ('2012-O0007'); -- 252.209-7998 (DEVIATION 2012-00007)


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158449, prescription_id FROM Prescriptions
WHERE prescription_name IN ('2012-O0004'); -- 252.209-7999 (DEVIATION 2012-00004)


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158450, prescription_id FROM Prescriptions
WHERE prescription_name IN ('211.002-70'); -- 252.211-7000


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158451, prescription_id FROM Prescriptions
WHERE prescription_name IN ('211.204(c)(i)'); -- 252.211-7001


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158452, prescription_id FROM Prescriptions
WHERE prescription_name IN ('211.204(c)(ii)'); -- 252.211-7002


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158453, prescription_id FROM Prescriptions
WHERE prescription_name IN ('211.274-6(a)(1)'); -- 252.211-7003


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158454, prescription_id FROM Prescriptions
WHERE prescription_name IN ('211.272'); -- 252.211-7004


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158455, prescription_id FROM Prescriptions
WHERE prescription_name IN ('211.273-4'); -- 252.211-7005


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158456, prescription_id FROM Prescriptions
WHERE prescription_name IN ('211.275-3'); -- 252.211-7006


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158457, prescription_id FROM Prescriptions
WHERE prescription_name IN ('211.274-6(b)'); -- 252.211-7007


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158458, prescription_id FROM Prescriptions
WHERE prescription_name IN ('211.274-6(c)'); -- 252.211-7008


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158459, prescription_id FROM Prescriptions
WHERE prescription_name IN ('212.7103'); -- 252.212-7002


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158460, prescription_id FROM Prescriptions
WHERE prescription_name IN ('213.106-2-70'); -- 252.213-7000


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158461, prescription_id FROM Prescriptions
WHERE prescription_name IN ('215.408(1)'); -- 252.215-7000


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158462, prescription_id FROM Prescriptions
WHERE prescription_name IN ('215.408(2)'); -- 252.215-7002


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158463, prescription_id FROM Prescriptions
WHERE prescription_name IN ('215.408(3)(i)'); -- 252.215-7003


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158464, prescription_id FROM Prescriptions
WHERE prescription_name IN ('215.408(3)(ii)'); -- 252.215-7004


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158465, prescription_id FROM Prescriptions
WHERE prescription_name IN ('215.370-3(a)'); -- 252.215-7005


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158466, prescription_id FROM Prescriptions
WHERE prescription_name IN ('215.370-3(b)'); -- 252.215-7006


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158467, prescription_id FROM Prescriptions
WHERE prescription_name IN ('215.371-6'); -- 252.215-7007


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158468, prescription_id FROM Prescriptions
WHERE prescription_name IN ('215.408(4)'); -- 252.215-7008


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158469, prescription_id FROM Prescriptions
WHERE prescription_name IN ('215.408(5)'); -- 252.215-7009


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158470, prescription_id FROM Prescriptions
WHERE prescription_name IN ('216.203-4-70(a)(1)'); -- 252.216-7000


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158471, prescription_id FROM Prescriptions
WHERE prescription_name IN ('216.203-4-70(b)'); -- 252.216-7001


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158472, prescription_id FROM Prescriptions
WHERE prescription_name IN ('216.601(e)'); -- 252.216-7002


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158473, prescription_id FROM Prescriptions
WHERE prescription_name IN ('216.203-4-70(c)(1)'); -- 252.216-7003


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158474, prescription_id FROM Prescriptions
WHERE prescription_name IN ('216.406'); -- 252.216-7004


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158475, prescription_id FROM Prescriptions
WHERE prescription_name IN ('216.406(e)(2)'); -- 252.216-7005


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158476, prescription_id FROM Prescriptions
WHERE prescription_name IN ('216.506(a)'); -- 252.216-7006


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158477, prescription_id FROM Prescriptions
WHERE prescription_name IN ('216.203-4-70(a)(2)'); -- 252.216-7007


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158478, prescription_id FROM Prescriptions
WHERE prescription_name IN ('216.203-4-70(c)(2)'); -- 252.216-7008


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158479, prescription_id FROM Prescriptions
WHERE prescription_name IN ('216.307(a)'); -- 252.216-7009


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158480, prescription_id FROM Prescriptions
WHERE prescription_name IN ('216.506(d)(1)','216.506(d)(2)'); -- 252.216-7010


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158481, prescription_id FROM Prescriptions
WHERE prescription_name IN ('217.208-70(a)','217.208-70(a)(1)'); -- 252.217-7000


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158482, prescription_id FROM Prescriptions
WHERE prescription_name IN ('217.208-70(b)'); -- 252.217-7001


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158483, prescription_id FROM Prescriptions
WHERE prescription_name IN ('217.7005'); -- 252.217-7002


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158484, prescription_id FROM Prescriptions
WHERE prescription_name IN ('217.7104(a)'); -- 252.217-7003


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158485, prescription_id FROM Prescriptions
WHERE prescription_name IN ('217.7104(a)'); -- 252.217-7004


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158486, prescription_id FROM Prescriptions
WHERE prescription_name IN ('217.7104(a)'); -- 252.217-7005


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158487, prescription_id FROM Prescriptions
WHERE prescription_name IN ('217.7104(a)'); -- 252.217-7006


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158488, prescription_id FROM Prescriptions
WHERE prescription_name IN ('217.7104(a)'); -- 252.217-7007


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158489, prescription_id FROM Prescriptions
WHERE prescription_name IN ('217.7104(a)'); -- 252.217-7008


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158490, prescription_id FROM Prescriptions
WHERE prescription_name IN ('217.7104(a)'); -- 252.217-7009


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158491, prescription_id FROM Prescriptions
WHERE prescription_name IN ('217.7104(a)'); -- 252.217-7010


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158492, prescription_id FROM Prescriptions
WHERE prescription_name IN ('217.7104(a)'); -- 252.217-7011


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158493, prescription_id FROM Prescriptions
WHERE prescription_name IN ('217.7104(a)'); -- 252.217-7012


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158494, prescription_id FROM Prescriptions
WHERE prescription_name IN ('217.7104(a)'); -- 252.217-7013


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158495, prescription_id FROM Prescriptions
WHERE prescription_name IN ('217.7104(a)'); -- 252.217-7014


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158496, prescription_id FROM Prescriptions
WHERE prescription_name IN ('217.7104(a)'); -- 252.217-7015


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158497, prescription_id FROM Prescriptions
WHERE prescription_name IN ('217.7104(a)'); -- 252.217-7016


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158498, prescription_id FROM Prescriptions
WHERE prescription_name IN ('217.7303'); -- 252.217-7026


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158499, prescription_id FROM Prescriptions
WHERE prescription_name IN ('217.7406(b)'); -- 252.217-7027


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158500, prescription_id FROM Prescriptions
WHERE prescription_name IN ('217.7702'); -- 252.217-7028


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158501, prescription_id FROM Prescriptions
WHERE prescription_name IN ('219.309(1)'); -- 252.219-7000


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158502, prescription_id FROM Prescriptions
WHERE prescription_name IN ('219.708(b)(1)(A)(1)'); -- 252.219-7003


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158503, prescription_id FROM Prescriptions
WHERE prescription_name IN ('2013-O0014'); -- 252.219-7003 (DEVIATION 2013-00014)


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158504, prescription_id FROM Prescriptions
WHERE prescription_name IN ('219.708(b)(1)(B)'); -- 252.219-7004


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158505, prescription_id FROM Prescriptions
WHERE prescription_name IN ('219.811-3'); -- 252.219-7009


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158506, prescription_id FROM Prescriptions
WHERE prescription_name IN ('219.811-3(2)'); -- 252.219-7010


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158507, prescription_id FROM Prescriptions
WHERE prescription_name IN ('219.811-3(3)'); -- 252.219-7011


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158508, prescription_id FROM Prescriptions
WHERE prescription_name IN ('222.7004'); -- 252.222-7000


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158509, prescription_id FROM Prescriptions
WHERE prescription_name IN ('222.7102'); -- 252.222-7001


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158510, prescription_id FROM Prescriptions
WHERE prescription_name IN ('222.7201(a)'); -- 252.222-7002


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158511, prescription_id FROM Prescriptions
WHERE prescription_name IN ('222.7201(b)'); -- 252.222-7003


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158512, prescription_id FROM Prescriptions
WHERE prescription_name IN ('222.7201(c)'); -- 252.222-7004


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158513, prescription_id FROM Prescriptions
WHERE prescription_name IN ('222.7302'); -- 252.222-7005


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158514, prescription_id FROM Prescriptions
WHERE prescription_name IN ('222.7405'); -- 252.222-7006


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158515, prescription_id FROM Prescriptions
WHERE prescription_name IN ('222.1771'); -- 252.222-7007


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158516, prescription_id FROM Prescriptions
WHERE prescription_name IN ('223.303'); -- 252.223-7001


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158517, prescription_id FROM Prescriptions
WHERE prescription_name IN ('223.370-5'); -- 252.223-7002


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158518, prescription_id FROM Prescriptions
WHERE prescription_name IN ('223.370-5'); -- 252.223-7003


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158519, prescription_id FROM Prescriptions
WHERE prescription_name IN ('223.570-2'); -- 252.223-7004


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158520, prescription_id FROM Prescriptions
WHERE prescription_name IN ('223.7106','223.7106(a)'); -- 252.223-7006


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158521, prescription_id FROM Prescriptions
WHERE prescription_name IN ('223.7203'); -- 252.223-7007


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158522, prescription_id FROM Prescriptions
WHERE prescription_name IN ('223.7306'); -- 252.223-7008


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158523, prescription_id FROM Prescriptions
WHERE prescription_name IN ('25.1101(1)','25.1101(1)(i)'); -- 252.225-7000


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158524, prescription_id FROM Prescriptions
WHERE prescription_name IN ('225.1101(2)(i)','225.1101(2)(ii)'); -- 252.225-7001


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158525, prescription_id FROM Prescriptions
WHERE prescription_name IN ('225.1101(3)'); -- 252.225-7002


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158526, prescription_id FROM Prescriptions
WHERE prescription_name IN ('225.7204(a)'); -- 252.225-7003


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158527, prescription_id FROM Prescriptions
WHERE prescription_name IN ('225.7204(b)'); -- 252.225-7004


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158528, prescription_id FROM Prescriptions
WHERE prescription_name IN ('225.1103(1)'); -- 252.225-7005


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158529, prescription_id FROM Prescriptions
WHERE prescription_name IN ('225.7002-3(c)'); -- 252.225-7006


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158530, prescription_id FROM Prescriptions
WHERE prescription_name IN ('225.1103(4)'); -- 252.225-7007


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158531, prescription_id FROM Prescriptions
WHERE prescription_name IN ('225.7003-5(a)(1)'); -- 252.225-7008


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158532, prescription_id FROM Prescriptions
WHERE prescription_name IN ('225.7003-5(a)(2)'); -- 252.225-7009


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158534, prescription_id FROM Prescriptions
WHERE prescription_name IN ('225.7012-3'); -- 252.225-7011


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158535, prescription_id FROM Prescriptions
WHERE prescription_name IN ('225.7002-2(a)'); -- 252.225-7012


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158536, prescription_id FROM Prescriptions
WHERE prescription_name IN ('225.1101(4)'); -- 252.225-7013


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158537, prescription_id FROM Prescriptions
WHERE prescription_name IN ('225.7002-3(b)'); -- 252.225-7015


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158538, prescription_id FROM Prescriptions
WHERE prescription_name IN ('225.7009-5'); -- 252.225-7016


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158539, prescription_id FROM Prescriptions
WHERE prescription_name IN ('225.7017-4(a)'); -- 252.225-7017


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158540, prescription_id FROM Prescriptions
WHERE prescription_name IN ('225.7017-4(b)'); -- 252.225-7018


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158541, prescription_id FROM Prescriptions
WHERE prescription_name IN ('225.7007-3'); -- 252.225-7019


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158542, prescription_id FROM Prescriptions
WHERE prescription_name IN ('225.1101(5)','225.1101(5)(i)'); -- 252.225-7020


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158543, prescription_id FROM Prescriptions
WHERE prescription_name IN ('225.1101(6)','25.1101(6)(i)'); -- 252.225-7021


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158544, prescription_id FROM Prescriptions
WHERE prescription_name IN ('225.7703-4(a)'); -- 252.225-7023


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158545, prescription_id FROM Prescriptions
WHERE prescription_name IN ('225.7703-4(b)'); -- 252.225-7024


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158546, prescription_id FROM Prescriptions
WHERE prescription_name IN ('225.7102-4'); -- 252.225-7025


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158547, prescription_id FROM Prescriptions
WHERE prescription_name IN ('225.7703-4(c)'); -- 252.225-7026


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158548, prescription_id FROM Prescriptions
WHERE prescription_name IN ('225.7307(a)'); -- 252.225-7027


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158549, prescription_id FROM Prescriptions
WHERE prescription_name IN ('225.7307(b)'); -- 252.225-7028


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158550, prescription_id FROM Prescriptions
WHERE prescription_name IN ('225.7703-4(d)'); -- 252.225-7029


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158551, prescription_id FROM Prescriptions
WHERE prescription_name IN ('225.7011-3'); -- 252.225-7030


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158552, prescription_id FROM Prescriptions
WHERE prescription_name IN ('225.7605'); -- 252.225-7031


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158553, prescription_id FROM Prescriptions
WHERE prescription_name IN ('225.1101(7)'); -- 252.225-7032


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158554, prescription_id FROM Prescriptions
WHERE prescription_name IN ('225.1101(8)'); -- 252.225-7033


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158555, prescription_id FROM Prescriptions
WHERE prescription_name IN ('225.1101(9)','225.1101(9)(i)'); -- 252.225-7035


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158556, prescription_id FROM Prescriptions
WHERE prescription_name IN ('225.1101(10)(i)','225.1101(10)(i)(A)'); -- 252.225-7036


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158557, prescription_id FROM Prescriptions
WHERE prescription_name IN ('225.7006-4(a)'); -- 252.225-7037


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158558, prescription_id FROM Prescriptions
WHERE prescription_name IN ('225.7006-4(b)'); -- 252.225-7038


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158559, prescription_id FROM Prescriptions
WHERE prescription_name IN ('225.302-6'); -- 252.225-7039


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158560, prescription_id FROM Prescriptions
WHERE prescription_name IN ('225.371-5(a)'); -- 252.225-7040


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158561, prescription_id FROM Prescriptions
WHERE prescription_name IN ('225.1103(2)'); -- 252.225-7041


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158562, prescription_id FROM Prescriptions
WHERE prescription_name IN ('225.1103(3)'); -- 252.225-7042


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158563, prescription_id FROM Prescriptions
WHERE prescription_name IN ('225.372-2'); -- 252.225-7043


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158564, prescription_id FROM Prescriptions
WHERE prescription_name IN ('225.7503(a)','225.7503(a)(1)'); -- 252.225-7044


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158565, prescription_id FROM Prescriptions
WHERE prescription_name IN ('225.7503(b)','225.7503(b)(1)'); -- 252.225-7045


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158566, prescription_id FROM Prescriptions
WHERE prescription_name IN ('225.7902-5(a)'); -- 252.225-7046


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158567, prescription_id FROM Prescriptions
WHERE prescription_name IN ('225.7902-5(b)'); -- 252.225-7047


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158568, prescription_id FROM Prescriptions
WHERE prescription_name IN ('225.7901-4'); -- 252.225-7048


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158569, prescription_id FROM Prescriptions
WHERE prescription_name IN ('225.772-5'); -- 252.225-7049


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158570, prescription_id FROM Prescriptions
WHERE prescription_name IN ('225.771-5'); -- 252.225-7050


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158571, prescription_id FROM Prescriptions
WHERE prescription_name IN ('2015-O0016'); -- 252.225-7981 (DEVIATION 2015-00016)


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158572, prescription_id FROM Prescriptions
WHERE prescription_name IN ('225.7798-6(a)'); -- 252.225-7982 (DEVIATION 2015-00012)


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158573, prescription_id FROM Prescriptions
WHERE prescription_name IN ('225.7798-6(b)'); -- 252.225-7983 (DEVIATION 2015-00012)


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158574, prescription_id FROM Prescriptions
WHERE prescription_name IN ('225.7798-6(c)'); -- 252.225-7984 (DEVIATION 2015-00012)


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158575, prescription_id FROM Prescriptions
WHERE prescription_name IN ('225.371-5'); -- 252.225-7985 (DEVIATION 2015-00003)


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158576, prescription_id FROM Prescriptions
WHERE prescription_name IN ('225.371-5'); -- 252.225-7987 (DEVIATION 2014-00016)


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158577, prescription_id FROM Prescriptions
WHERE prescription_name IN ('2014-O0005'); -- 252.225-7989 (DEVIATION 2014-00005)


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158578, prescription_id FROM Prescriptions
WHERE prescription_name IN ('2014-00014','225-7799-4'); -- 252.225-7990 (DEVIATION 2014-00014)


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158579, prescription_id FROM Prescriptions
WHERE prescription_name IN ('2014-00014','225-7799-4(b)'); -- 252.225-7991 (DEVIATION 2014-00014)


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158580, prescription_id FROM Prescriptions
WHERE prescription_name IN ('2014-00014','225-7799-4(c)'); -- 252.225-7992 (DEVIATION 2014-00014)


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158581, prescription_id FROM Prescriptions
WHERE prescription_name IN ('2015-00016'); -- 252.225-7993 (DEVIATION 2015-00016)


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158582, prescription_id FROM Prescriptions
WHERE prescription_name IN ('2015-O0013'); -- 252.225-7994 (DEVIATION 2015-00013)


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158583, prescription_id FROM Prescriptions
WHERE prescription_name IN ('225.371-5'); -- 252.225-7995 (DEVIATION 2015-00009)


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158584, prescription_id FROM Prescriptions
WHERE prescription_name IN ('2014-00014','225-7799-4(d)'); -- 252.225-7996 (DEVIATION 2014-00014)


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158585, prescription_id FROM Prescriptions
WHERE prescription_name IN ('2013-O0017'); -- 252.225-7997 (DEVIATION 2013-00017)


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158586, prescription_id FROM Prescriptions
WHERE prescription_name IN ('2014-00014','225-7799-4(e)'); -- 252.225-7998 (DEVIATION 2014-00014)


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158587, prescription_id FROM Prescriptions
WHERE prescription_name IN ('2014-00014','225-7799-4(f)'); -- 252.225-7999 (DEVIATION 2014-00014)


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158588, prescription_id FROM Prescriptions
WHERE prescription_name IN ('226.104'); -- 252.226-7001


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158589, prescription_id FROM Prescriptions
WHERE prescription_name IN ('227.7009-1'); -- 252.227-7000


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158590, prescription_id FROM Prescriptions
WHERE prescription_name IN ('227.7009-2(a)'); -- 252.227-7001


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158591, prescription_id FROM Prescriptions
WHERE prescription_name IN ('227.7009-2(b)'); -- 252.227-7002


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158592, prescription_id FROM Prescriptions
WHERE prescription_name IN ('227.7009-2'); -- 252.227-7003


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158593, prescription_id FROM Prescriptions
WHERE prescription_name IN ('227.7009-3(a)'); -- 252.227-7004


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158594, prescription_id FROM Prescriptions
WHERE prescription_name IN ('227.7009-4(a)'); -- 252.227-7006


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158595, prescription_id FROM Prescriptions
WHERE prescription_name IN ('227.7009-4(b)'); -- 252.227-7007


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158596, prescription_id FROM Prescriptions
WHERE prescription_name IN ('227.7009- 4(c)'); -- 252.227-7008


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158597, prescription_id FROM Prescriptions
WHERE prescription_name IN ('227.7009-4(d)'); -- 252.227-7009


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158598, prescription_id FROM Prescriptions
WHERE prescription_name IN ('227.7009-4(e)'); -- 252.227-7010


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158599, prescription_id FROM Prescriptions
WHERE prescription_name IN ('227.7010'); -- 252.227-7011


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158600, prescription_id FROM Prescriptions
WHERE prescription_name IN ('227.7012'); -- 252.227-7012


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158601, prescription_id FROM Prescriptions
WHERE prescription_name IN ('227.7103-6(a)'); -- 252.227-7013


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158602, prescription_id FROM Prescriptions
WHERE prescription_name IN ('227.7203-6(a)(1)'); -- 252.227-7014


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158603, prescription_id FROM Prescriptions
WHERE prescription_name IN ('227.7102-4(a)(1)'); -- 252.227-7015


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158604, prescription_id FROM Prescriptions
WHERE prescription_name IN ('227.7103-6(e)(1)','227.7104(e)(1)','227.7203-6(b)'); -- 252.227-7016


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158605, prescription_id FROM Prescriptions
WHERE prescription_name IN ('227.7103-3(b)','227.7104(e)(2)','227.7203-3(a)'); -- 252.227-7017


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158606, prescription_id FROM Prescriptions
WHERE prescription_name IN ('227.7104(a)'); -- 252.227-7018


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158607, prescription_id FROM Prescriptions
WHERE prescription_name IN ('227.7104(e)(3)','227.7203-6(c)'); -- 252.227-7019


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158608, prescription_id FROM Prescriptions
WHERE prescription_name IN ('227.7105-3','227.7106(a)','227.7205(a)'); -- 252.227-7020


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158609, prescription_id FROM Prescriptions
WHERE prescription_name IN ('227.7105-2(a)'); -- 252.227-7021


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158610, prescription_id FROM Prescriptions
WHERE prescription_name IN ('227.7107-1(a)'); -- 252.227-7022


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158611, prescription_id FROM Prescriptions
WHERE prescription_name IN ('227.7107-1(b)'); -- 252.227-7023


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158612, prescription_id FROM Prescriptions
WHERE prescription_name IN ('227.7107-3'); -- 252.227-7024


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158613, prescription_id FROM Prescriptions
WHERE prescription_name IN ('227.7103-6(c)','227.7104(f)(1)','227.7203-6(d)'); -- 252.227-7025


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158614, prescription_id FROM Prescriptions
WHERE prescription_name IN ('227.7103-8(a)'); -- 252.227-7026


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158615, prescription_id FROM Prescriptions
WHERE prescription_name IN ('227.7103-8(b)'); -- 252.227-7027


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158616, prescription_id FROM Prescriptions
WHERE prescription_name IN ('227.7103-6(d)','227.7104(f)(2)','227.7203-6(e)'); -- 252.227-7028


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158617, prescription_id FROM Prescriptions
WHERE prescription_name IN ('227.7103-6(e)(2)','227.7104(e)(4)'); -- 252.227-7030


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158618, prescription_id FROM Prescriptions
WHERE prescription_name IN ('227.7103-17'); -- 252.227-7032


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158619, prescription_id FROM Prescriptions
WHERE prescription_name IN ('227.7107-1(c)'); -- 252.227-7033


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158620, prescription_id FROM Prescriptions
WHERE prescription_name IN ('227.7102-4(c)','227.7103-6(e)(3)','227.7104(e)(5)','227.7203-6(f)'); -- 252.227-7037


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158621, prescription_id FROM Prescriptions
WHERE prescription_name IN ('227.303(2)'); -- 252.227-7038


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158622, prescription_id FROM Prescriptions
WHERE prescription_name IN ('227.303(1)'); -- 252.227-7039


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158623, prescription_id FROM Prescriptions
WHERE prescription_name IN ('228.370(a)'); -- 252.228-7000


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158624, prescription_id FROM Prescriptions
WHERE prescription_name IN ('228.370(b)'); -- 252.228-7001


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158625, prescription_id FROM Prescriptions
WHERE prescription_name IN ('228.370(c)'); -- 252.228-7003


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158626, prescription_id FROM Prescriptions
WHERE prescription_name IN ('228.170'); -- 252.228-7004


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158627, prescription_id FROM Prescriptions
WHERE prescription_name IN ('228.370(d)'); -- 252.228-7005


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158628, prescription_id FROM Prescriptions
WHERE prescription_name IN ('228.370(e)'); -- 252.228-7006


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158629, prescription_id FROM Prescriptions
WHERE prescription_name IN ('229.402-1'); -- 252.229-7000


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158630, prescription_id FROM Prescriptions
WHERE prescription_name IN ('229.402-70(a)','229.402-70(a)(1)'); -- 252.229-7001


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158631, prescription_id FROM Prescriptions
WHERE prescription_name IN ('229.402-70(b)'); -- 252.229-7002


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158632, prescription_id FROM Prescriptions
WHERE prescription_name IN ('229.402-70(c)(1)'); -- 252.229-7003


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158633, prescription_id FROM Prescriptions
WHERE prescription_name IN ('229.402-70(d)'); -- 252.229-7004


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158634, prescription_id FROM Prescriptions
WHERE prescription_name IN ('229.402-70(e)(1)'); -- 252.229-7005


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158635, prescription_id FROM Prescriptions
WHERE prescription_name IN ('229.402-70(f)'); -- 252.229-7006


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158636, prescription_id FROM Prescriptions
WHERE prescription_name IN ('229.402-70(g)'); -- 252.229-7007


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158637, prescription_id FROM Prescriptions
WHERE prescription_name IN ('229.402-70(h)'); -- 252.229-7008


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158638, prescription_id FROM Prescriptions
WHERE prescription_name IN ('229.402-70(i)'); -- 252.229-7009


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158639, prescription_id FROM Prescriptions
WHERE prescription_name IN ('229.402-70(j)'); -- 252.229-7010


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158640, prescription_id FROM Prescriptions
WHERE prescription_name IN ('229.170-4'); -- 252.229-7011


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158641, prescription_id FROM Prescriptions
WHERE prescription_name IN ('229.402-70(c)(2)'); -- 252.229-7012


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158642, prescription_id FROM Prescriptions
WHERE prescription_name IN ('229.402-70(e)(2)'); -- 252.229-7013


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158643, prescription_id FROM Prescriptions
WHERE prescription_name IN ('2013-O0016'); -- 252.229-7998 (DEVIATION 2013-00016)


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158644, prescription_id FROM Prescriptions
WHERE prescription_name IN ('2013-O0016'); -- 252.229-7999 (DEVIATION 2013-00016)


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158645, prescription_id FROM Prescriptions
WHERE prescription_name IN ('231.100-70'); -- 252.231-7000


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158646, prescription_id FROM Prescriptions
WHERE prescription_name IN ('232.412-70(a)'); -- 252.232-7000


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158647, prescription_id FROM Prescriptions
WHERE prescription_name IN ('232.412-70(b)'); -- 252.232-7001


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158648, prescription_id FROM Prescriptions
WHERE prescription_name IN ('232.502-4-70(a)'); -- 252.232-7002


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158649, prescription_id FROM Prescriptions
WHERE prescription_name IN ('232.7004(a)'); -- 252.232-7003


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158650, prescription_id FROM Prescriptions
WHERE prescription_name IN ('232.502-4-70(b)'); -- 252.232-7004


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158651, prescription_id FROM Prescriptions
WHERE prescription_name IN ('232.412-70(c)'); -- 252.232-7005


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158652, prescription_id FROM Prescriptions
WHERE prescription_name IN ('232.7004(b)'); -- 252.232-7006


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158653, prescription_id FROM Prescriptions
WHERE prescription_name IN ('232.705-70'); -- 252.232-7007


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158654, prescription_id FROM Prescriptions
WHERE prescription_name IN ('232.806(a)(1)'); -- 252.232-7008


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158655, prescription_id FROM Prescriptions
WHERE prescription_name IN ('232.1110'); -- 252.232-7009


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158656, prescription_id FROM Prescriptions
WHERE prescription_name IN ('232.7102'); -- 252.232-7010


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158657, prescription_id FROM Prescriptions
WHERE prescription_name IN ('232.908'); -- 252.232-7011


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158658, prescription_id FROM Prescriptions
WHERE prescription_name IN ('232.1005-70(a)'); -- 252.232-7012


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158659, prescription_id FROM Prescriptions
WHERE prescription_name IN ('232.1005-70(b)'); -- 252.232-7013


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158660, prescription_id FROM Prescriptions
WHERE prescription_name IN ('232.7202'); -- 252.232-7014


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158661, prescription_id FROM Prescriptions
WHERE prescription_name IN ('233.215-70'); -- 252.233-7001


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158662, prescription_id FROM Prescriptions
WHERE prescription_name IN ('234.203(1)'); -- 252.234-7001 (DEVIATION 2015-00017)


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158663, prescription_id FROM Prescriptions
WHERE prescription_name IN ('234.203(2)'); -- 252.234-7002 (DEVIATION 2015-00017)


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158664, prescription_id FROM Prescriptions
WHERE prescription_name IN ('234.7101(a)(1)'); -- 252.234-7003


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158665, prescription_id FROM Prescriptions
WHERE prescription_name IN ('234.7101(b)','234.7101(b)(1)'); -- 252.234-7004


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158666, prescription_id FROM Prescriptions
WHERE prescription_name IN ('235.070-3'); -- 252.235-7000


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158667, prescription_id FROM Prescriptions
WHERE prescription_name IN ('235.070-3'); -- 252.235-7001


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158668, prescription_id FROM Prescriptions
WHERE prescription_name IN ('235.072(a)'); -- 252.235-7002


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158669, prescription_id FROM Prescriptions
WHERE prescription_name IN ('235.072(b)','235.072(b)(1)'); -- 252.235-7003


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158670, prescription_id FROM Prescriptions
WHERE prescription_name IN ('235.072'); -- 252.235-7004


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158671, prescription_id FROM Prescriptions
WHERE prescription_name IN ('235.072©'); -- 252.235-7010


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158672, prescription_id FROM Prescriptions
WHERE prescription_name IN ('235.072(d)'); -- 252.235-7011


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158673, prescription_id FROM Prescriptions
WHERE prescription_name IN ('236.570(a)'); -- 252.236-7000


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158674, prescription_id FROM Prescriptions
WHERE prescription_name IN ('236.570(a)'); -- 252.236-7001


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158675, prescription_id FROM Prescriptions
WHERE prescription_name IN ('236.570(b)(1)'); -- 252.236-7002


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158676, prescription_id FROM Prescriptions
WHERE prescription_name IN ('236.570(b)(2)(i)'); -- 252.236-7003


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158677, prescription_id FROM Prescriptions
WHERE prescription_name IN ('236.570(b)(2)(ii)'); -- 252.236-7004


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158678, prescription_id FROM Prescriptions
WHERE prescription_name IN ('236.570(b)(4)'); -- 252.236-7005


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158679, prescription_id FROM Prescriptions
WHERE prescription_name IN ('236.570(b)(4)'); -- 252.236-7006


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158680, prescription_id FROM Prescriptions
WHERE prescription_name IN ('236.570(b)(5)'); -- 252.236-7007


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158681, prescription_id FROM Prescriptions
WHERE prescription_name IN ('236.570(b)(6)'); -- 252.236-7008


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158682, prescription_id FROM Prescriptions
WHERE prescription_name IN ('236.609-70'); -- 252.236-7009


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158683, prescription_id FROM Prescriptions
WHERE prescription_name IN ('236.570(c)(1)'); -- 252.236-7010


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158684, prescription_id FROM Prescriptions
WHERE prescription_name IN ('236.609-70(b)'); -- 252.236-7011


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158685, prescription_id FROM Prescriptions
WHERE prescription_name IN ('236.570(c)(2)'); -- 252.236-7012


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158686, prescription_id FROM Prescriptions
WHERE prescription_name IN ('236.570(d)'); -- 252.236-7013


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158687, prescription_id FROM Prescriptions
WHERE prescription_name IN ('237.270(d)(1)'); -- 252.237-7000


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158688, prescription_id FROM Prescriptions
WHERE prescription_name IN ('237.270(d)(2)'); -- 252.237-7001


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158689, prescription_id FROM Prescriptions
WHERE prescription_name IN ('237.7003(a)'); -- 252.237-7002


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158690, prescription_id FROM Prescriptions
WHERE prescription_name IN ('237.7003(b)'); -- 252.237-7003


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158691, prescription_id FROM Prescriptions
WHERE prescription_name IN ('237.7003(b)'); -- 252.237-7004


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158692, prescription_id FROM Prescriptions
WHERE prescription_name IN ('237.7003(b)'); -- 252.237-7005


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158693, prescription_id FROM Prescriptions
WHERE prescription_name IN ('237.7003(b)'); -- 252.237-7006


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158694, prescription_id FROM Prescriptions
WHERE prescription_name IN ('237.7003(b)'); -- 252.237-7007


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158695, prescription_id FROM Prescriptions
WHERE prescription_name IN ('237.7003(b)'); -- 252.237-7008


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158696, prescription_id FROM Prescriptions
WHERE prescription_name IN ('237.7003(b)'); -- 252.237-7009


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158697, prescription_id FROM Prescriptions
WHERE prescription_name IN ('237.173-5'); -- 252.237-7010


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158698, prescription_id FROM Prescriptions
WHERE prescription_name IN ('237.7003(b)'); -- 252.237-7011


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158699, prescription_id FROM Prescriptions
WHERE prescription_name IN ('237.7101(a)'); -- 252.237-7012


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158700, prescription_id FROM Prescriptions
WHERE prescription_name IN ('237.7101(b)'); -- 252.237-7013


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158701, prescription_id FROM Prescriptions
WHERE prescription_name IN ('237.7101(c)'); -- 252.237-7014


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158702, prescription_id FROM Prescriptions
WHERE prescription_name IN ('237.7101(d)'); -- 252.237-7015


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158703, prescription_id FROM Prescriptions
WHERE prescription_name IN ('237.7101(e)','237.7101(e)(1)'); -- 252.237-7016


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158704, prescription_id FROM Prescriptions
WHERE prescription_name IN ('237.7101(f)'); -- 252.237-7017


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158705, prescription_id FROM Prescriptions
WHERE prescription_name IN ('237.7101(g)'); -- 252.237-7018


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158706, prescription_id FROM Prescriptions
WHERE prescription_name IN ('237.171-4'); -- 252.237-7019


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158707, prescription_id FROM Prescriptions
WHERE prescription_name IN ('237.7402'); -- 252.237-7022


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158708, prescription_id FROM Prescriptions
WHERE prescription_name IN ('237.7603(b)'); -- 252.237-7023


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158709, prescription_id FROM Prescriptions
WHERE prescription_name IN ('237.7603(b)'); -- 252.237-7024


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158710, prescription_id FROM Prescriptions
WHERE prescription_name IN ('239.7103(a)'); -- 252.239-7000


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158711, prescription_id FROM Prescriptions
WHERE prescription_name IN ('239.7103(b)'); -- 252.239-7001


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158712, prescription_id FROM Prescriptions
WHERE prescription_name IN ('239.7411(a)'); -- 252.239-7002


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158713, prescription_id FROM Prescriptions
WHERE prescription_name IN ('239.7411(a)'); -- 252.239-7004


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158714, prescription_id FROM Prescriptions
WHERE prescription_name IN ('239.7411(a)'); -- 252.239-7005


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158715, prescription_id FROM Prescriptions
WHERE prescription_name IN ('239.7411(a)'); -- 252.239-7006


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158716, prescription_id FROM Prescriptions
WHERE prescription_name IN ('239.7411(a)'); -- 252.239-7007


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158717, prescription_id FROM Prescriptions
WHERE prescription_name IN ('239.7411(a)'); -- 252.239-7008


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158718, prescription_id FROM Prescriptions
WHERE prescription_name IN ('239.7603(a)'); -- 252.239-7009


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158719, prescription_id FROM Prescriptions
WHERE prescription_name IN ('239.7603(b)'); -- 252.239-7010


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158720, prescription_id FROM Prescriptions
WHERE prescription_name IN ('239.7411(b)'); -- 252.239-7011


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158721, prescription_id FROM Prescriptions
WHERE prescription_name IN ('239.7411(b)'); -- 252.239-7012


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158722, prescription_id FROM Prescriptions
WHERE prescription_name IN ('239.7411©'); -- 252.239-7013


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158723, prescription_id FROM Prescriptions
WHERE prescription_name IN ('239.7411©'); -- 252.239-7014


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158724, prescription_id FROM Prescriptions
WHERE prescription_name IN ('239.7411©'); -- 252.239-7015


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158725, prescription_id FROM Prescriptions
WHERE prescription_name IN ('239.7411(d)'); -- 252.239-7016


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158726, prescription_id FROM Prescriptions
WHERE prescription_name IN ('239.7306(a)'); -- 252.239-7017


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158727, prescription_id FROM Prescriptions
WHERE prescription_name IN ('239.7306(b)'); -- 252.239-7018


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158728, prescription_id FROM Prescriptions
WHERE prescription_name IN ('241.501-70(a)'); -- 252.241-7000


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158729, prescription_id FROM Prescriptions
WHERE prescription_name IN ('241.501-70(b)'); -- 252.241-7001


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158730, prescription_id FROM Prescriptions
WHERE prescription_name IN ('242.7204'); -- 252.242-7004


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158731, prescription_id FROM Prescriptions
WHERE prescription_name IN ('242.7001'); -- 252.242-7005


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158732, prescription_id FROM Prescriptions
WHERE prescription_name IN ('242.7503'); -- 252.242-7006


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158733, prescription_id FROM Prescriptions
WHERE prescription_name IN ('243.205-70'); -- 252.243-7001


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158734, prescription_id FROM Prescriptions
WHERE prescription_name IN ('243.205-71'); -- 252.243-7002


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158735, prescription_id FROM Prescriptions
WHERE prescription_name IN ('244.403'); -- 252.244-7000


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158736, prescription_id FROM Prescriptions
WHERE prescription_name IN ('244.305-71','244.305-71(a)'); -- 252.244-7001


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158737, prescription_id FROM Prescriptions
WHERE prescription_name IN ('245.107(1)'); -- 252.245-7000


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158738, prescription_id FROM Prescriptions
WHERE prescription_name IN ('245.107(2)'); -- 252.245-7001


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158739, prescription_id FROM Prescriptions
WHERE prescription_name IN ('245.107(3)'); -- 252.245-7002


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158740, prescription_id FROM Prescriptions
WHERE prescription_name IN ('245.107(4)'); -- 252.245-7003


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158741, prescription_id FROM Prescriptions
WHERE prescription_name IN ('245.107(5)'); -- 252.245-7004


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158742, prescription_id FROM Prescriptions
WHERE prescription_name IN ('246.370'); -- 252.246-7000


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158743, prescription_id FROM Prescriptions
WHERE prescription_name IN ('246.710(1)','246.710(1)(i)'); -- 252.246-7001


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158744, prescription_id FROM Prescriptions
WHERE prescription_name IN ('246.710(2)'); -- 252.246-7002


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158745, prescription_id FROM Prescriptions
WHERE prescription_name IN ('246.371(a)'); -- 252.246-7003


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158746, prescription_id FROM Prescriptions
WHERE prescription_name IN ('246.270-4'); -- 252.246-7004


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158747, prescription_id FROM Prescriptions
WHERE prescription_name IN ('246.710(3)(i)(A)','246.710(3)(i)(B)(iii)'); -- 252.246-7005


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158748, prescription_id FROM Prescriptions
WHERE prescription_name IN ('246.710(3)(i)(B)'); -- 252.246-7006


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158749, prescription_id FROM Prescriptions
WHERE prescription_name IN ('246.870-3'); -- 252.246-7007


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158750, prescription_id FROM Prescriptions
WHERE prescription_name IN ('247.270-4(a)'); -- 252.247-7000


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158751, prescription_id FROM Prescriptions
WHERE prescription_name IN ('247.270-4(b)'); -- 252.247-7001


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158752, prescription_id FROM Prescriptions
WHERE prescription_name IN ('247.270-4(c)'); -- 252.247-7002


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158753, prescription_id FROM Prescriptions
WHERE prescription_name IN ('247.207'); -- 252.247-7003


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158754, prescription_id FROM Prescriptions
WHERE prescription_name IN ('247.270-4(d)'); -- 252.247-7004


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158755, prescription_id FROM Prescriptions
WHERE prescription_name IN ('247.270-4(e)'); -- 252.247-7005


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158756, prescription_id FROM Prescriptions
WHERE prescription_name IN ('247.270-4(f)'); -- 252.247-7006


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158757, prescription_id FROM Prescriptions
WHERE prescription_name IN ('247.270-4(g)'); -- 252.247-7007


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158758, prescription_id FROM Prescriptions
WHERE prescription_name IN ('247.271-3','247.271-3(a)','247.271-3(a)(1)'); -- 252.247-7008


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158759, prescription_id FROM Prescriptions
WHERE prescription_name IN ('247.271-3(b)'); -- 252.247-7009


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158760, prescription_id FROM Prescriptions
WHERE prescription_name IN ('247.271-3(d)'); -- 252.247-7010


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158761, prescription_id FROM Prescriptions
WHERE prescription_name IN ('247.271-3(e)'); -- 252.247-7011


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158762, prescription_id FROM Prescriptions
WHERE prescription_name IN ('247.271-3(g)'); -- 252.247-7012


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158763, prescription_id FROM Prescriptions
WHERE prescription_name IN ('247.271-3(h)'); -- 252.247-7013


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158764, prescription_id FROM Prescriptions
WHERE prescription_name IN ('247.271-3(i)'); -- 252.247-7014


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158765, prescription_id FROM Prescriptions
WHERE prescription_name IN ('247.271-3(j)'); -- 252.247-7016


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158766, prescription_id FROM Prescriptions
WHERE prescription_name IN ('247.271-3(k)'); -- 252.247-7017


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158767, prescription_id FROM Prescriptions
WHERE prescription_name IN ('247.271-3(l)'); -- 252.247-7018


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158768, prescription_id FROM Prescriptions
WHERE prescription_name IN ('247.271-3(m)'); -- 252.247-7019


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158769, prescription_id FROM Prescriptions
WHERE prescription_name IN ('247.271-3(n)'); -- 252.247-7020


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158770, prescription_id FROM Prescriptions
WHERE prescription_name IN ('247.305-70'); -- 252.247-7021


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158771, prescription_id FROM Prescriptions
WHERE prescription_name IN ('247.574(a)'); -- 252.247-7022


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158772, prescription_id FROM Prescriptions
WHERE prescription_name IN ('247.574(b)'); -- 252.247-7023


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158773, prescription_id FROM Prescriptions
WHERE prescription_name IN ('247.574(c)'); -- 252.247-7024


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158774, prescription_id FROM Prescriptions
WHERE prescription_name IN ('247.574(d)'); -- 252.247-7025


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158775, prescription_id FROM Prescriptions
WHERE prescription_name IN ('247.574(e)'); -- 252.247-7026


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158776, prescription_id FROM Prescriptions
WHERE prescription_name IN ('247.574(f)'); -- 252.247-7027


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158777, prescription_id FROM Prescriptions
WHERE prescription_name IN ('247.207(2)'); -- 252.247-7028


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158778, prescription_id FROM Prescriptions
WHERE prescription_name IN ('249.501-70'); -- 252.249-7000


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158779, prescription_id FROM Prescriptions
WHERE prescription_name IN ('249.7003(c)'); -- 252.249-7002


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158780, prescription_id FROM Prescriptions
WHERE prescription_name IN ('251.107'); -- 252.251-7000


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158781, prescription_id FROM Prescriptions
WHERE prescription_name IN ('251.205'); -- 252.251-7001


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158782, prescription_id FROM Prescriptions
WHERE prescription_name IN ('2.201'); -- 52.202-1


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158783, prescription_id FROM Prescriptions
WHERE prescription_name IN ('3.104-9(b)'); -- 52.203-10


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158784, prescription_id FROM Prescriptions
WHERE prescription_name IN ('3.808(a)'); -- 52.203-11


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158785, prescription_id FROM Prescriptions
WHERE prescription_name IN ('3.808(b)'); -- 52.203-12


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158786, prescription_id FROM Prescriptions
WHERE prescription_name IN ('3.1004(a)'); -- 52.203-13


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158787, prescription_id FROM Prescriptions
WHERE prescription_name IN ('3.1004(b)'); -- 52.203-14


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158788, prescription_id FROM Prescriptions
WHERE prescription_name IN ('3.907-7'); -- 52.203-15


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158789, prescription_id FROM Prescriptions
WHERE prescription_name IN ('3.1106'); -- 52.203-16


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158790, prescription_id FROM Prescriptions
WHERE prescription_name IN ('3.908-9'); -- 52.203-17


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158791, prescription_id FROM Prescriptions
WHERE prescription_name IN ('3.103-1'); -- 52.203-2


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158792, prescription_id FROM Prescriptions
WHERE prescription_name IN ('3.202'); -- 52.203-3


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158793, prescription_id FROM Prescriptions
WHERE prescription_name IN ('3.404'); -- 52.203-5


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158794, prescription_id FROM Prescriptions
WHERE prescription_name IN ('3.503-2'); -- 52.203-6


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158795, prescription_id FROM Prescriptions
WHERE prescription_name IN ('3.502-3'); -- 52.203-7


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158796, prescription_id FROM Prescriptions
WHERE prescription_name IN ('3.104-9(a)'); -- 52.203-8


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158797, prescription_id FROM Prescriptions
WHERE prescription_name IN ('4.103'); -- 52.204-1


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158798, prescription_id FROM Prescriptions
WHERE prescription_name IN ('4.1403(a)'); -- 52.204-10


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158799, prescription_id FROM Prescriptions
WHERE prescription_name IN ('4.607(c)'); -- 52.204-12


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158800, prescription_id FROM Prescriptions
WHERE prescription_name IN ('4.1105(b)'); -- 52.204-13


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158801, prescription_id FROM Prescriptions
WHERE prescription_name IN ('4.1705(a)'); -- 52.204-14


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158802, prescription_id FROM Prescriptions
WHERE prescription_name IN ('4.1705(b)'); -- 52.204-15


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158803, prescription_id FROM Prescriptions
WHERE prescription_name IN ('4.1804(a)'); -- 52.204-16


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158804, prescription_id FROM Prescriptions
WHERE prescription_name IN ('4.1804(b)'); -- 52.204-17


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158805, prescription_id FROM Prescriptions
WHERE prescription_name IN ('4.1804(c)'); -- 52.204-18


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158806, prescription_id FROM Prescriptions
WHERE prescription_name IN ('4.1202(b)'); -- 52.204-19


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158807, prescription_id FROM Prescriptions
WHERE prescription_name IN ('4.404(a)'); -- 52.204-2


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158808, prescription_id FROM Prescriptions
WHERE prescription_name IN ('4.905'); -- 52.204-3


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158809, prescription_id FROM Prescriptions
WHERE prescription_name IN ('4.303'); -- 52.204-4


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158810, prescription_id FROM Prescriptions
WHERE prescription_name IN ('4.607(b)'); -- 52.204-5


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158811, prescription_id FROM Prescriptions
WHERE prescription_name IN ('4.607(b)'); -- 52.204-6


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158812, prescription_id FROM Prescriptions
WHERE prescription_name IN ('4.1105(a)(1)'); -- 52.204-7


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158813, prescription_id FROM Prescriptions
WHERE prescription_name IN ('4.1202(a)'); -- 52.204-8


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158814, prescription_id FROM Prescriptions
WHERE prescription_name IN ('4.1303'); -- 52.204-9


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158815, prescription_id FROM Prescriptions
WHERE prescription_name IN ('7.305(a)'); -- 52.207-1


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158816, prescription_id FROM Prescriptions
WHERE prescription_name IN ('7.305(b)'); -- 52.207-2


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158817, prescription_id FROM Prescriptions
WHERE prescription_name IN ('7.305(c)'); -- 52.207-3


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158818, prescription_id FROM Prescriptions
WHERE prescription_name IN ('7.203'); -- 52.207-4


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158819, prescription_id FROM Prescriptions
WHERE prescription_name IN ('7.404'); -- 52.207-5


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158820, prescription_id FROM Prescriptions
WHERE prescription_name IN ('8.1104(a)'); -- 52.208-4


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158821, prescription_id FROM Prescriptions
WHERE prescription_name IN ('8.1104(b)'); -- 52.208-5


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158822, prescription_id FROM Prescriptions
WHERE prescription_name IN ('8.1104(c)'); -- 52.208-6


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158823, prescription_id FROM Prescriptions
WHERE prescription_name IN ('8.1104(d)'); -- 52.208-7


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158824, prescription_id FROM Prescriptions
WHERE prescription_name IN ('8.505'); -- 52.208-8


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158825, prescription_id FROM Prescriptions
WHERE prescription_name IN ('8.005'); -- 52.208-9


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158826, prescription_id FROM Prescriptions
WHERE prescription_name IN ('9.206-2'); -- 52.209-1


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158827, prescription_id FROM Prescriptions
WHERE prescription_name IN ('9.108-5(b)'); -- 52.209-10


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158828, prescription_id FROM Prescriptions
WHERE prescription_name IN ('9.108-5(a)'); -- 52.209-2


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158829, prescription_id FROM Prescriptions
WHERE prescription_name IN ('9.308-1(a)(1)','9.308-1(b)(1)'); -- 52.209-3


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158830, prescription_id FROM Prescriptions
WHERE prescription_name IN ('9.308-1(b)(1)','9.308-2(a)(1)'); -- 52.209-4


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158831, prescription_id FROM Prescriptions
WHERE prescription_name IN ('9.104-7(a)'); -- 52.209-5


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158832, prescription_id FROM Prescriptions
WHERE prescription_name IN ('9.409'); -- 52.209-6


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158833, prescription_id FROM Prescriptions
WHERE prescription_name IN ('9.104-7(b)'); -- 52.209-7


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158834, prescription_id FROM Prescriptions
WHERE prescription_name IN ('9.104-7(c)(1)'); -- 52.209-9


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158835, prescription_id FROM Prescriptions
WHERE prescription_name IN ('10.003'); -- 52.210-1


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158836, prescription_id FROM Prescriptions
WHERE prescription_name IN ('11.204(a)'); -- 52.211-1


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158837, prescription_id FROM Prescriptions
WHERE prescription_name IN ('11.404(b)'); -- 52.211-10


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158838, prescription_id FROM Prescriptions
WHERE prescription_name IN ('11.503(a)'); -- 52.211-11


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158839, prescription_id FROM Prescriptions
WHERE prescription_name IN ('11.503(b)','211.503(b)'); -- 52.211-12


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158840, prescription_id FROM Prescriptions
WHERE prescription_name IN ('11.503(c)'); -- 52.211-13


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158841, prescription_id FROM Prescriptions
WHERE prescription_name IN ('11.604(a)'); -- 52.211-14


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158842, prescription_id FROM Prescriptions
WHERE prescription_name IN ('11.604(b)'); -- 52.211-15


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158843, prescription_id FROM Prescriptions
WHERE prescription_name IN ('11.703(a)'); -- 52.211-16


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158844, prescription_id FROM Prescriptions
WHERE prescription_name IN ('11.703(b)'); -- 52.211-17


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158845, prescription_id FROM Prescriptions
WHERE prescription_name IN ('11.703(c)'); -- 52.211-18


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158846, prescription_id FROM Prescriptions
WHERE prescription_name IN ('11.204(b)'); -- 52.211-2


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158847, prescription_id FROM Prescriptions
WHERE prescription_name IN ('11.204(c)'); -- 52.211-3


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158848, prescription_id FROM Prescriptions
WHERE prescription_name IN ('11.204(d)'); -- 52.211-4


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158849, prescription_id FROM Prescriptions
WHERE prescription_name IN ('11.304'); -- 52.211-5


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158850, prescription_id FROM Prescriptions
WHERE prescription_name IN ('11.107(a)'); -- 52.211-6


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158851, prescription_id FROM Prescriptions
WHERE prescription_name IN ('11.107(b)'); -- 52.211-7


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158852, prescription_id FROM Prescriptions
WHERE prescription_name IN ('11.404(a)(2)'); -- 52.211-8


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158853, prescription_id FROM Prescriptions
WHERE prescription_name IN ('11.404(a)(3)'); -- 52.211-9


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158854, prescription_id FROM Prescriptions
WHERE prescription_name IN ('12.301(b)(1)'); -- 52.212-1


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158855, prescription_id FROM Prescriptions
WHERE prescription_name IN ('12.301(c)(1)'); -- 52.212-2


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158856, prescription_id FROM Prescriptions
WHERE prescription_name IN ('12.301(b)(2)'); -- 52.212-3


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158857, prescription_id FROM Prescriptions
WHERE prescription_name IN ('12.301(b)(3)'); -- 52.212-4


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158858, prescription_id FROM Prescriptions
WHERE prescription_name IN ('12.301(b)(4)'); -- 52.212-5


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158859, prescription_id FROM Prescriptions
WHERE prescription_name IN ('2013-O0019'); -- 52.212-5 (DEVIATION 2013-00019)


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158860, prescription_id FROM Prescriptions
WHERE prescription_name IN ('13.404'); -- 52.213-1


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158861, prescription_id FROM Prescriptions
WHERE prescription_name IN ('13.302-5(b)'); -- 52.213-2


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158862, prescription_id FROM Prescriptions
WHERE prescription_name IN ('13.302-5(c)'); -- 52.213-3


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158863, prescription_id FROM Prescriptions
WHERE prescription_name IN ('13.302-5(d)'); -- 52.213-4


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158864, prescription_id FROM Prescriptions
WHERE prescription_name IN ('14.201-6(e)'); -- 52.214-10


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158865, prescription_id FROM Prescriptions
WHERE prescription_name IN ('14.201-6(f)'); -- 52.214-12


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158866, prescription_id FROM Prescriptions
WHERE prescription_name IN ('14.201-6(g)(1)'); -- 52.214-13


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158867, prescription_id FROM Prescriptions
WHERE prescription_name IN ('14.201-6(h)'); -- 52.214-14


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158868, prescription_id FROM Prescriptions
WHERE prescription_name IN ('14.201-6(i)'); -- 52.214-15


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158869, prescription_id FROM Prescriptions
WHERE prescription_name IN ('14.201-6(j)'); -- 52.214-16


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158870, prescription_id FROM Prescriptions
WHERE prescription_name IN ('14.201-6(l)'); -- 52.214-18


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158871, prescription_id FROM Prescriptions
WHERE prescription_name IN ('14.201-6(m)'); -- 52.214-19


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158872, prescription_id FROM Prescriptions
WHERE prescription_name IN ('14.201-6(o)(1)'); -- 52.214-20


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158873, prescription_id FROM Prescriptions
WHERE prescription_name IN ('14.201-6(p)(1)'); -- 52.214-21


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158874, prescription_id FROM Prescriptions
WHERE prescription_name IN ('14.201-6(q)'); -- 52.214-22


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158875, prescription_id FROM Prescriptions
WHERE prescription_name IN ('14.201-6(r)'); -- 52.214-23


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158876, prescription_id FROM Prescriptions
WHERE prescription_name IN ('14.201-6(s)'); -- 52.214-24


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158877, prescription_id FROM Prescriptions
WHERE prescription_name IN ('14.201-6(t)'); -- 52.214-25


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158878, prescription_id FROM Prescriptions
WHERE prescription_name IN ('14.201-7(a)(1)'); -- 52.214-26


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158879, prescription_id FROM Prescriptions
WHERE prescription_name IN ('14.201-7(b)(1)'); -- 52.214-27


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158880, prescription_id FROM Prescriptions
WHERE prescription_name IN ('14.201-7(c)(1)'); -- 52.214-28


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158881, prescription_id FROM Prescriptions
WHERE prescription_name IN ('14.201-7(d)'); -- 52.214-29


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158882, prescription_id FROM Prescriptions
WHERE prescription_name IN ('14.201-6(b)(1)'); -- 52.214-3


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158883, prescription_id FROM Prescriptions
WHERE prescription_name IN ('14.201-6(v)'); -- 52.214-31


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158884, prescription_id FROM Prescriptions
WHERE prescription_name IN ('14.201-6(w)'); -- 52.214-34


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158885, prescription_id FROM Prescriptions
WHERE prescription_name IN ('14.201-6(x)'); -- 52.214-35


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158886, prescription_id FROM Prescriptions
WHERE prescription_name IN ('14.201-6(b)(2)'); -- 52.214-4


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158887, prescription_id FROM Prescriptions
WHERE prescription_name IN ('14.201-6(c)(1)'); -- 52.214-5


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158888, prescription_id FROM Prescriptions
WHERE prescription_name IN ('14.201-6(c)(2)'); -- 52.214-6


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158889, prescription_id FROM Prescriptions
WHERE prescription_name IN ('14.201-6(c)(3)'); -- 52.214-7


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158890, prescription_id FROM Prescriptions
WHERE prescription_name IN ('15.209(a)'); -- 52.215-1


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158891, prescription_id FROM Prescriptions
WHERE prescription_name IN ('15.408(b)'); -- 52.215-10


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158892, prescription_id FROM Prescriptions
WHERE prescription_name IN ('15.408(c)'); -- 52.215-11


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158893, prescription_id FROM Prescriptions
WHERE prescription_name IN ('15.408(d)'); -- 52.215-12


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158894, prescription_id FROM Prescriptions
WHERE prescription_name IN ('15.408(e)'); -- 52.215-13


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158895, prescription_id FROM Prescriptions
WHERE prescription_name IN ('15.408(f)(1)'); -- 52.215-14


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158896, prescription_id FROM Prescriptions
WHERE prescription_name IN ('15.408(g)'); -- 52.215-15


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158897, prescription_id FROM Prescriptions
WHERE prescription_name IN ('15.408(h)'); -- 52.215-16


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158898, prescription_id FROM Prescriptions
WHERE prescription_name IN ('15.408(i)'); -- 52.215-17


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158899, prescription_id FROM Prescriptions
WHERE prescription_name IN ('15.408(j)'); -- 52.215-18


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158900, prescription_id FROM Prescriptions
WHERE prescription_name IN ('15.408(k)'); -- 52.215-19


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158901, prescription_id FROM Prescriptions
WHERE prescription_name IN ('15.209(b)(1)'); -- 52.215-2


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158902, prescription_id FROM Prescriptions
WHERE prescription_name IN ('15.408(I)','215.408(4)(ii)'); -- 52.215-20


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158903, prescription_id FROM Prescriptions
WHERE prescription_name IN ('15.408(m)'); -- 52.215-21


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158904, prescription_id FROM Prescriptions
WHERE prescription_name IN ('15.408(n)(1)'); -- 52.215-22


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158905, prescription_id FROM Prescriptions
WHERE prescription_name IN ('15.408(n)(2)'); -- 52.215-23


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158906, prescription_id FROM Prescriptions
WHERE prescription_name IN ('15.209(c)'); -- 52.215-3


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158907, prescription_id FROM Prescriptions
WHERE prescription_name IN ('15.209(e)'); -- 52.215-5


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158908, prescription_id FROM Prescriptions
WHERE prescription_name IN ('15.209(f)'); -- 52.215-6


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158909, prescription_id FROM Prescriptions
WHERE prescription_name IN ('15.209(h)'); -- 52.215-8


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158910, prescription_id FROM Prescriptions
WHERE prescription_name IN ('15.408(a)'); -- 52.215-9


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158911, prescription_id FROM Prescriptions
WHERE prescription_name IN ('16.105'); -- 52.216-1


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158912, prescription_id FROM Prescriptions
WHERE prescription_name IN ('16.307(d)'); -- 52.216-10


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158913, prescription_id FROM Prescriptions
WHERE prescription_name IN ('16.307(e)(1)'); -- 52.216-11


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158914, prescription_id FROM Prescriptions
WHERE prescription_name IN ('16.307(f)(1)'); -- 52.216-12


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158915, prescription_id FROM Prescriptions
WHERE prescription_name IN ('16.307(g)'); -- 52.216-15


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158916, prescription_id FROM Prescriptions
WHERE prescription_name IN ('16.406(a)'); -- 52.216-16


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158917, prescription_id FROM Prescriptions
WHERE prescription_name IN ('16.406(b)'); -- 52.216-17


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158918, prescription_id FROM Prescriptions
WHERE prescription_name IN ('16.506(a)'); -- 52.216-18


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158919, prescription_id FROM Prescriptions
WHERE prescription_name IN ('16.506(b)'); -- 52.216-19


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158920, prescription_id FROM Prescriptions
WHERE prescription_name IN ('16.203-4(a)'); -- 52.216-2


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158921, prescription_id FROM Prescriptions
WHERE prescription_name IN ('16.506(c)'); -- 52.216-20


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158922, prescription_id FROM Prescriptions
WHERE prescription_name IN ('16.506(d)(1)'); -- 52.216-21


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158923, prescription_id FROM Prescriptions
WHERE prescription_name IN ('16.506(e)'); -- 52.216-22


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158924, prescription_id FROM Prescriptions
WHERE prescription_name IN ('16.603-4(b)(1)'); -- 52.216-23


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158925, prescription_id FROM Prescriptions
WHERE prescription_name IN ('16.603-4(b)(2)'); -- 52.216-24


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158926, prescription_id FROM Prescriptions
WHERE prescription_name IN ('16.603-4(b)(3)'); -- 52.216-25


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158927, prescription_id FROM Prescriptions
WHERE prescription_name IN ('16.603-4(c)'); -- 52.216-26


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158928, prescription_id FROM Prescriptions
WHERE prescription_name IN ('16.506(f)'); -- 52.216-27


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158929, prescription_id FROM Prescriptions
WHERE prescription_name IN ('16.506(g)'); -- 52.216-28


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158930, prescription_id FROM Prescriptions
WHERE prescription_name IN ('16.601(f)(1)'); -- 52.216-29


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158931, prescription_id FROM Prescriptions
WHERE prescription_name IN ('16.203-4(b)'); -- 52.216-3


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158932, prescription_id FROM Prescriptions
WHERE prescription_name IN ('16.601(f)(2)'); -- 52.216-30


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158933, prescription_id FROM Prescriptions
WHERE prescription_name IN ('16.601(f)(3)'); -- 52.216-31


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158934, prescription_id FROM Prescriptions
WHERE prescription_name IN ('16.203-4(c)'); -- 52.216-4


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158935, prescription_id FROM Prescriptions
WHERE prescription_name IN ('16.205-4'); -- 52.216-5


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158936, prescription_id FROM Prescriptions
WHERE prescription_name IN ('16.206-4'); -- 52.216-6


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158937, prescription_id FROM Prescriptions
WHERE prescription_name IN ('16.307(a)'); -- 52.216-7


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158938, prescription_id FROM Prescriptions
WHERE prescription_name IN ('16.307(b)'); -- 52.216-8


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158939, prescription_id FROM Prescriptions
WHERE prescription_name IN ('16.307(c)'); -- 52.216-9


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158940, prescription_id FROM Prescriptions
WHERE prescription_name IN ('17.109(a)'); -- 52.217-2


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158941, prescription_id FROM Prescriptions
WHERE prescription_name IN ('17.208(a)'); -- 52.217-3


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158942, prescription_id FROM Prescriptions
WHERE prescription_name IN ('17.208(b)'); -- 52.217-4


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158943, prescription_id FROM Prescriptions
WHERE prescription_name IN ('17.208(c)'); -- 52.217-5


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158944, prescription_id FROM Prescriptions
WHERE prescription_name IN ('17.208(d)'); -- 52.217-6


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158945, prescription_id FROM Prescriptions
WHERE prescription_name IN ('17.208(e)'); -- 52.217-7


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158946, prescription_id FROM Prescriptions
WHERE prescription_name IN ('17.208(f)'); -- 52.217-8


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158947, prescription_id FROM Prescriptions
WHERE prescription_name IN ('17.208(g)'); -- 52.217-9


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158948, prescription_id FROM Prescriptions
WHERE prescription_name IN ('19.309(a)(1)'); -- 52.219-1


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158949, prescription_id FROM Prescriptions
WHERE prescription_name IN ('19.708(c)(1)'); -- 52.219-10


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158950, prescription_id FROM Prescriptions
WHERE prescription_name IN ('19.811-3(a)'); -- 52.219-11


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158951, prescription_id FROM Prescriptions
WHERE prescription_name IN ('19.811-3(b)'); -- 52.219-12


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158952, prescription_id FROM Prescriptions
WHERE prescription_name IN ('19.508(f)'); -- 52.219-13


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158953, prescription_id FROM Prescriptions
WHERE prescription_name IN ('19.508(e)','19.811-3(e)'); -- 52.219-14


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158954, prescription_id FROM Prescriptions
WHERE prescription_name IN ('19.708(b)(2)'); -- 52.219-16


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158955, prescription_id FROM Prescriptions
WHERE prescription_name IN ('19.811-3(c)'); -- 52.219-17


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158956, prescription_id FROM Prescriptions
WHERE prescription_name IN ('19.811-3(d)'); -- 52.219-18


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158957, prescription_id FROM Prescriptions
WHERE prescription_name IN ('19.309(c)'); -- 52.219-2


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158958, prescription_id FROM Prescriptions
WHERE prescription_name IN ('19.1407'); -- 52.219-27


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158959, prescription_id FROM Prescriptions
WHERE prescription_name IN ('19.309(c)'); -- 52.219-28


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158960, prescription_id FROM Prescriptions
WHERE prescription_name IN ('19.1506(a)'); -- 52.219-29


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158961, prescription_id FROM Prescriptions
WHERE prescription_name IN ('19.1309(a)'); -- 52.219-3


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158962, prescription_id FROM Prescriptions
WHERE prescription_name IN ('19.1506(b)'); -- 52.219-30


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158963, prescription_id FROM Prescriptions
WHERE prescription_name IN ('19.1309(b)'); -- 52.219-4


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158964, prescription_id FROM Prescriptions
WHERE prescription_name IN ('225-7306'); -- 52.219-6


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158965, prescription_id FROM Prescriptions
WHERE prescription_name IN ('19.508(d)'); -- 52.219-7


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158966, prescription_id FROM Prescriptions
WHERE prescription_name IN ('19.708(a)'); -- 52.219-8


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158967, prescription_id FROM Prescriptions
WHERE prescription_name IN ('19.708(b)'); -- 52.219-9


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158968, prescription_id FROM Prescriptions
WHERE prescription_name IN ('2013-O0014)'); -- 52.219-9 (DEVIATION 2013-00014)


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158969, prescription_id FROM Prescriptions
WHERE prescription_name IN ('22.103-5(a)'); -- 52.222-1


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158970, prescription_id FROM Prescriptions
WHERE prescription_name IN ('22.407(a)'); -- 52.222-10


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158971, prescription_id FROM Prescriptions
WHERE prescription_name IN ('22.407(a)'); -- 52.222-11


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158972, prescription_id FROM Prescriptions
WHERE prescription_name IN ('22.407(a)'); -- 52.222-12


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158973, prescription_id FROM Prescriptions
WHERE prescription_name IN ('22.407(a)'); -- 52.222-13


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158974, prescription_id FROM Prescriptions
WHERE prescription_name IN ('22.407(a)'); -- 52.222-14


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158975, prescription_id FROM Prescriptions
WHERE prescription_name IN ('22.407(a)'); -- 52.222-15


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158976, prescription_id FROM Prescriptions
WHERE prescription_name IN ('22.407(b)'); -- 52.222-16


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158977, prescription_id FROM Prescriptions
WHERE prescription_name IN ('22.1207'); -- 52.222-17


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158978, prescription_id FROM Prescriptions
WHERE prescription_name IN ('22.1505(a)'); -- 52.222-18


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158979, prescription_id FROM Prescriptions
WHERE prescription_name IN ('22.1505(b)'); -- 52.222-19


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158980, prescription_id FROM Prescriptions
WHERE prescription_name IN ('22.103-5(b)'); -- 52.222-2


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158981, prescription_id FROM Prescriptions
WHERE prescription_name IN ('22.610'); -- 52.222-20


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158982, prescription_id FROM Prescriptions
WHERE prescription_name IN ('22.810(a)(1)'); -- 52.222-21


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158983, prescription_id FROM Prescriptions
WHERE prescription_name IN ('22.810(a)(2)'); -- 52.222-22


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158984, prescription_id FROM Prescriptions
WHERE prescription_name IN ('22.810(b)'); -- 52.222-23


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158985, prescription_id FROM Prescriptions
WHERE prescription_name IN ('22.810(c)'); -- 52.222-24


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158986, prescription_id FROM Prescriptions
WHERE prescription_name IN ('22.810(d)'); -- 52.222-25


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158987, prescription_id FROM Prescriptions
WHERE prescription_name IN ('22.810(e)'); -- 52.222-26


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158988, prescription_id FROM Prescriptions
WHERE prescription_name IN ('22.810(f)'); -- 52.222-27


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158989, prescription_id FROM Prescriptions
WHERE prescription_name IN ('22.810(g)'); -- 52.222-29


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158990, prescription_id FROM Prescriptions
WHERE prescription_name IN ('22.202'); -- 52.222-3


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158991, prescription_id FROM Prescriptions
WHERE prescription_name IN ('22.407(e)'); -- 52.222-30


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158992, prescription_id FROM Prescriptions
WHERE prescription_name IN ('22.407(f)'); -- 52.222-31


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158993, prescription_id FROM Prescriptions
WHERE prescription_name IN ('22.407(g)'); -- 52.222-32


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158994, prescription_id FROM Prescriptions
WHERE prescription_name IN ('22.505(a)(1)'); -- 52.222-33


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158995, prescription_id FROM Prescriptions
WHERE prescription_name IN ('22.505(b)(1)'); -- 52.222-34


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158996, prescription_id FROM Prescriptions
WHERE prescription_name IN ('22.1310(a)(1)'); -- 52.222-35


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158997, prescription_id FROM Prescriptions
WHERE prescription_name IN ('22.1408(a)'); -- 52.222-36


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158998, prescription_id FROM Prescriptions
WHERE prescription_name IN ('22.1310(b)'); -- 52.222-37


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 158999, prescription_id FROM Prescriptions
WHERE prescription_name IN ('22.1310(c)'); -- 52.222-38


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159000, prescription_id FROM Prescriptions
WHERE prescription_name IN ('22.305'); -- 52.222-4


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159001, prescription_id FROM Prescriptions
WHERE prescription_name IN ('22.1605'); -- 52.222-40


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159002, prescription_id FROM Prescriptions
WHERE prescription_name IN ('22.1006(a)'); -- 52.222-41


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159003, prescription_id FROM Prescriptions
WHERE prescription_name IN ('22.1006(b)'); -- 52.222-42


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159004, prescription_id FROM Prescriptions
WHERE prescription_name IN ('22.1006(c)(1)'); -- 52.222-43


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159005, prescription_id FROM Prescriptions
WHERE prescription_name IN ('22.1006(c)(2)'); -- 52.222-44


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159006, prescription_id FROM Prescriptions
WHERE prescription_name IN ('22.1103'); -- 52.222-46


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159007, prescription_id FROM Prescriptions
WHERE prescription_name IN ('22.1006(e)(1)'); -- 52.222-48


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159008, prescription_id FROM Prescriptions
WHERE prescription_name IN ('22.1006(f)'); -- 52.222-49


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159009, prescription_id FROM Prescriptions
WHERE prescription_name IN ('22.407(h)'); -- 52.222-5


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159010, prescription_id FROM Prescriptions
WHERE prescription_name IN ('22.1705(a)(1)'); -- 52.222-50


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159011, prescription_id FROM Prescriptions
WHERE prescription_name IN ('22.1006(e)(2)'); -- 52.222-51


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159012, prescription_id FROM Prescriptions
WHERE prescription_name IN ('22.1006(e)(3)'); -- 52.222-52


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159013, prescription_id FROM Prescriptions
WHERE prescription_name IN ('22.1006(e)(4)'); -- 52.222-53


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159014, prescription_id FROM Prescriptions
WHERE prescription_name IN ('22.1803'); -- 52.222-54


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159015, prescription_id FROM Prescriptions
WHERE prescription_name IN ('22.1906'); -- 52.222-55


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159016, prescription_id FROM Prescriptions
WHERE prescription_name IN ('22.1705(b)'); -- 52.222-56


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159017, prescription_id FROM Prescriptions
WHERE prescription_name IN ('22.407(a)'); -- 52.222-6


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159018, prescription_id FROM Prescriptions
WHERE prescription_name IN ('22.407(a)'); -- 52.222-7


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159019, prescription_id FROM Prescriptions
WHERE prescription_name IN ('22.407(a)'); -- 52.222-8


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159020, prescription_id FROM Prescriptions
WHERE prescription_name IN ('22.407(a)'); -- 52.222-9


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159021, prescription_id FROM Prescriptions
WHERE prescription_name IN ('23.406(a)'); -- 52.223-1


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159022, prescription_id FROM Prescriptions
WHERE prescription_name IN ('23.705(a)'); -- 52.223-10


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159023, prescription_id FROM Prescriptions
WHERE prescription_name IN ('23.804(a)'); -- 52.223-11


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159024, prescription_id FROM Prescriptions
WHERE prescription_name IN ('23.804(b)'); -- 52.223-12


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159025, prescription_id FROM Prescriptions
WHERE prescription_name IN ('23.705(b)(1)'); -- 52.223-13


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159026, prescription_id FROM Prescriptions
WHERE prescription_name IN ('23.705(c)(1)'); -- 52.223-14


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159027, prescription_id FROM Prescriptions
WHERE prescription_name IN ('23.206'); -- 52.223-15


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159028, prescription_id FROM Prescriptions
WHERE prescription_name IN ('23.705(d)(1)'); -- 52.223-16


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159029, prescription_id FROM Prescriptions
WHERE prescription_name IN ('23.406(e)'); -- 52.223-17


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159030, prescription_id FROM Prescriptions
WHERE prescription_name IN ('23.1105'); -- 52.223-18


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159031, prescription_id FROM Prescriptions
WHERE prescription_name IN ('23.903'); -- 52.223-19


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159032, prescription_id FROM Prescriptions
WHERE prescription_name IN ('23.406(b)'); -- 52.223-2


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159033, prescription_id FROM Prescriptions
WHERE prescription_name IN ('23.303'); -- 52.223-3


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159034, prescription_id FROM Prescriptions
WHERE prescription_name IN ('23.406(c)'); -- 52.223-4


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159035, prescription_id FROM Prescriptions
WHERE prescription_name IN ('23.1005'); -- 52.223-5


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159036, prescription_id FROM Prescriptions
WHERE prescription_name IN ('23.505'); -- 52.223-6


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159037, prescription_id FROM Prescriptions
WHERE prescription_name IN ('23.602'); -- 52.223-7


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159038, prescription_id FROM Prescriptions
WHERE prescription_name IN ('23.406(d)'); -- 52.223-9


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159039, prescription_id FROM Prescriptions
WHERE prescription_name IN ('24.104(a)'); -- 52.224-1


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159040, prescription_id FROM Prescriptions
WHERE prescription_name IN ('24.104(b)'); -- 52.224-2


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159041, prescription_id FROM Prescriptions
WHERE prescription_name IN ('25.1101(a)(1)'); -- 52.225-1


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159042, prescription_id FROM Prescriptions
WHERE prescription_name IN ('25.1102(b)(1)'); -- 52.225-10


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159043, prescription_id FROM Prescriptions
WHERE prescription_name IN ('25.1102(c)'); -- 52.225-11


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159044, prescription_id FROM Prescriptions
WHERE prescription_name IN ('25.1102(d)(1)'); -- 52.225-12


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159045, prescription_id FROM Prescriptions
WHERE prescription_name IN ('25.1103(a)'); -- 52.225-13


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159046, prescription_id FROM Prescriptions
WHERE prescription_name IN ('25.1103(b)'); -- 52.225-14


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159047, prescription_id FROM Prescriptions
WHERE prescription_name IN ('25.1103(c)'); -- 52.225-17


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159048, prescription_id FROM Prescriptions
WHERE prescription_name IN ('25.1101(f)'); -- 52.225-18


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159049, prescription_id FROM Prescriptions
WHERE prescription_name IN ('25.301-4'); -- 52.225-19


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159050, prescription_id FROM Prescriptions
WHERE prescription_name IN ('25.1101(a)(2)'); -- 52.225-2


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159051, prescription_id FROM Prescriptions
WHERE prescription_name IN ('25.1103(d)'); -- 52.225-20


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159052, prescription_id FROM Prescriptions
WHERE prescription_name IN ('25.1102(e)(1)'); -- 52.225-21


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159053, prescription_id FROM Prescriptions
WHERE prescription_name IN ('25.1102(e)(1)'); -- 52.225-22


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159054, prescription_id FROM Prescriptions
WHERE prescription_name IN ('25.1102(e)(1)'); -- 52.225-23


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159055, prescription_id FROM Prescriptions
WHERE prescription_name IN ('25.1102(e)(1)'); -- 52.225-24


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159056, prescription_id FROM Prescriptions
WHERE prescription_name IN ('25.1103'); -- 52.225-25


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159057, prescription_id FROM Prescriptions
WHERE prescription_name IN ('25.302-6'); -- 52.225-26


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159058, prescription_id FROM Prescriptions
WHERE prescription_name IN ('25.1101(b)(1)(i)'); -- 52.225-3


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159059, prescription_id FROM Prescriptions
WHERE prescription_name IN ('25.1101(b)(2)(i)'); -- 52.225-4


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159060, prescription_id FROM Prescriptions
WHERE prescription_name IN ('25.1101(c)(1)'); -- 52.225-5


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159061, prescription_id FROM Prescriptions
WHERE prescription_name IN ('25.1101(c)(2)'); -- 52.225-6


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159062, prescription_id FROM Prescriptions
WHERE prescription_name IN ('25.1101(d)'); -- 52.225-7


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159063, prescription_id FROM Prescriptions
WHERE prescription_name IN ('25.1101(e)'); -- 52.225-8


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159064, prescription_id FROM Prescriptions
WHERE prescription_name IN ('25.1102(a)'); -- 52.225-9


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159065, prescription_id FROM Prescriptions
WHERE prescription_name IN ('26.104'); -- 52.226-1


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159066, prescription_id FROM Prescriptions
WHERE prescription_name IN ('26.304'); -- 52.226-2


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159067, prescription_id FROM Prescriptions
WHERE prescription_name IN ('26.206(a)'); -- 52.226-3


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159068, prescription_id FROM Prescriptions
WHERE prescription_name IN ('26.206(b)'); -- 52.226-4


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159069, prescription_id FROM Prescriptions
WHERE prescription_name IN ('26.206(c)'); -- 52.226-5


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159070, prescription_id FROM Prescriptions
WHERE prescription_name IN ('26.404'); -- 52.226-6


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159071, prescription_id FROM Prescriptions
WHERE prescription_name IN ('27.201-2(a)(1)'); -- 52.227-1


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159072, prescription_id FROM Prescriptions
WHERE prescription_name IN ('27.203-2'); -- 52.227-10


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159073, prescription_id FROM Prescriptions
WHERE prescription_name IN ('27.303(b)(1)'); -- 52.227-11


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159074, prescription_id FROM Prescriptions
WHERE prescription_name IN ('27.303(e)'); -- 52.227-13


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159075, prescription_id FROM Prescriptions
WHERE prescription_name IN ('27.409(b)(1)'); -- 52.227-14


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159076, prescription_id FROM Prescriptions
WHERE prescription_name IN ('27.409(c)'); -- 52.227-15


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159077, prescription_id FROM Prescriptions
WHERE prescription_name IN ('27.409(d)'); -- 52.227-16


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159078, prescription_id FROM Prescriptions
WHERE prescription_name IN ('27.409(e)'); -- 52.227-17


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159079, prescription_id FROM Prescriptions
WHERE prescription_name IN ('27.409(f)'); -- 52.227-18


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159080, prescription_id FROM Prescriptions
WHERE prescription_name IN ('27.409(g)'); -- 52.227-19


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159081, prescription_id FROM Prescriptions
WHERE prescription_name IN ('27.201-2(b)'); -- 52.227-2


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159082, prescription_id FROM Prescriptions
WHERE prescription_name IN ('27.409(h)'); -- 52.227-20


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159083, prescription_id FROM Prescriptions
WHERE prescription_name IN ('27.409(j)'); -- 52.227-21


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159084, prescription_id FROM Prescriptions
WHERE prescription_name IN ('27.409(k)'); -- 52.227-22


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159085, prescription_id FROM Prescriptions
WHERE prescription_name IN ('27.409(l)'); -- 52.227-23


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159086, prescription_id FROM Prescriptions
WHERE prescription_name IN ('27.201-2(c)(1)'); -- 52.227-3


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159087, prescription_id FROM Prescriptions
WHERE prescription_name IN ('27.201-2'); -- 52.227-4


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159088, prescription_id FROM Prescriptions
WHERE prescription_name IN ('27.201-2(e)'); -- 52.227-5


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159089, prescription_id FROM Prescriptions
WHERE prescription_name IN ('27.202-5(a)(1)'); -- 52.227-6


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159090, prescription_id FROM Prescriptions
WHERE prescription_name IN ('27.202-5(b)'); -- 52.227-7


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159091, prescription_id FROM Prescriptions
WHERE prescription_name IN ('27.202-5(c)'); -- 52.227-9


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159092, prescription_id FROM Prescriptions
WHERE prescription_name IN ('28.101-2'); -- 52.228-1


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159093, prescription_id FROM Prescriptions
WHERE prescription_name IN ('28.313(b)'); -- 52.228-10


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159094, prescription_id FROM Prescriptions
WHERE prescription_name IN ('28.203-6'); -- 52.228-11


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159095, prescription_id FROM Prescriptions
WHERE prescription_name IN ('28.106-4(b)'); -- 52.228-12


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159096, prescription_id FROM Prescriptions
WHERE prescription_name IN ('28.1102-3(b)'); -- 52.228-13


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159097, prescription_id FROM Prescriptions
WHERE prescription_name IN ('28.204-4'); -- 52.228-14


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159098, prescription_id FROM Prescriptions
WHERE prescription_name IN ('28.1102-3(a)'); -- 52.228-15


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159099, prescription_id FROM Prescriptions
WHERE prescription_name IN ('28.103-4'); -- 52.228-16


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159100, prescription_id FROM Prescriptions
WHERE prescription_name IN ('28.106-4(a)'); -- 52.228-2


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159101, prescription_id FROM Prescriptions
WHERE prescription_name IN ('28.309(a)'); -- 52.228-3


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159102, prescription_id FROM Prescriptions
WHERE prescription_name IN ('28.309(b)'); -- 52.228-4


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159103, prescription_id FROM Prescriptions
WHERE prescription_name IN ('28.310'); -- 52.228-5


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159104, prescription_id FROM Prescriptions
WHERE prescription_name IN ('28.311-1'); -- 52.228-7


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159105, prescription_id FROM Prescriptions
WHERE prescription_name IN ('28.312'); -- 52.228-8


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159106, prescription_id FROM Prescriptions
WHERE prescription_name IN ('28.313(a)'); -- 52.228-9


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159107, prescription_id FROM Prescriptions
WHERE prescription_name IN ('29.401-1'); -- 52.229-1


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159108, prescription_id FROM Prescriptions
WHERE prescription_name IN ('29.401-4(b)'); -- 52.229-10


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159109, prescription_id FROM Prescriptions
WHERE prescription_name IN ('29.401-2'); -- 52.229-2


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159110, prescription_id FROM Prescriptions
WHERE prescription_name IN ('29.401-3'); -- 52.229-3


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159111, prescription_id FROM Prescriptions
WHERE prescription_name IN ('29.401-3(a)','29.401-3(b)'); -- 52.229-4


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159112, prescription_id FROM Prescriptions
WHERE prescription_name IN ('29.402-1(a)'); -- 52.229-6


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159113, prescription_id FROM Prescriptions
WHERE prescription_name IN ('29.402-1(b)'); -- 52.229-7


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159114, prescription_id FROM Prescriptions
WHERE prescription_name IN ('29.402-2(a)'); -- 52.229-8


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159115, prescription_id FROM Prescriptions
WHERE prescription_name IN ('29.402-2(b)'); -- 52.229-9


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159116, prescription_id FROM Prescriptions
WHERE prescription_name IN ('30.201-3'); -- 52.230-1


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159117, prescription_id FROM Prescriptions
WHERE prescription_name IN ('30.201-4(a)'); -- 52.230-2


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159118, prescription_id FROM Prescriptions
WHERE prescription_name IN ('30.201-4(b)(1)'); -- 52.230-3


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159119, prescription_id FROM Prescriptions
WHERE prescription_name IN ('30.201-4(c)'); -- 52.230-4


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159120, prescription_id FROM Prescriptions
WHERE prescription_name IN ('30.201-4(e)'); -- 52.230-5


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159121, prescription_id FROM Prescriptions
WHERE prescription_name IN ('30.201-4(d)'); -- 52.230-6


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159122, prescription_id FROM Prescriptions
WHERE prescription_name IN ('30.201-3(c)'); -- 52.230-7


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159123, prescription_id FROM Prescriptions
WHERE prescription_name IN ('32.111(a)(1)'); -- 52.232-1


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159124, prescription_id FROM Prescriptions
WHERE prescription_name IN ('32.111(c)(1)'); -- 52.232-10


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159125, prescription_id FROM Prescriptions
WHERE prescription_name IN ('32.111(c)(2)'); -- 52.232-11


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159126, prescription_id FROM Prescriptions
WHERE prescription_name IN ('32.412(a)'); -- 52.232-12


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159127, prescription_id FROM Prescriptions
WHERE prescription_name IN ('32.502-3(a)'); -- 52.232-13


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159128, prescription_id FROM Prescriptions
WHERE prescription_name IN ('32.502-3(b)(2)'); -- 52.232-14


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159129, prescription_id FROM Prescriptions
WHERE prescription_name IN ('32.502-3(c)'); -- 52.232-15


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159130, prescription_id FROM Prescriptions
WHERE prescription_name IN ('32.502-4(a)'); -- 52.232-16


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159131, prescription_id FROM Prescriptions
WHERE prescription_name IN ('32.611(a) &(b)'); -- 52.232-17


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159132, prescription_id FROM Prescriptions
WHERE prescription_name IN ('32.706-1(a)'); -- 52.232-18


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159133, prescription_id FROM Prescriptions
WHERE prescription_name IN ('32.706-1(b)'); -- 52.232-19


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159134, prescription_id FROM Prescriptions
WHERE prescription_name IN ('32.111(a)(2)'); -- 52.232-2


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159135, prescription_id FROM Prescriptions
WHERE prescription_name IN ('32.706-2(a)'); -- 52.232-20


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159136, prescription_id FROM Prescriptions
WHERE prescription_name IN ('32.706-2(b)'); -- 52.232-22


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159137, prescription_id FROM Prescriptions
WHERE prescription_name IN ('32.806(a)(1)'); -- 52.232-23


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159138, prescription_id FROM Prescriptions
WHERE prescription_name IN ('32.806(b)'); -- 52.232-24


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159139, prescription_id FROM Prescriptions
WHERE prescription_name IN ('32.908(c)'); -- 52.232-25


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159140, prescription_id FROM Prescriptions
WHERE prescription_name IN ('32.908(a)'); -- 52.232-26


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159141, prescription_id FROM Prescriptions
WHERE prescription_name IN ('32.908(b)'); -- 52.232-27


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159142, prescription_id FROM Prescriptions
WHERE prescription_name IN ('32.1005(b)(1)'); -- 52.232-28


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159143, prescription_id FROM Prescriptions
WHERE prescription_name IN ('32.206(b)(2)'); -- 52.232-29


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159144, prescription_id FROM Prescriptions
WHERE prescription_name IN ('32.111(a)(3)'); -- 52.232-3


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159145, prescription_id FROM Prescriptions
WHERE prescription_name IN ('32.206(g)'); -- 52.232-30


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159146, prescription_id FROM Prescriptions
WHERE prescription_name IN ('32.205(b)','32.206'); -- 52.232-31


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159147, prescription_id FROM Prescriptions
WHERE prescription_name IN ('32.1005'); -- 52.232-32


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159148, prescription_id FROM Prescriptions
WHERE prescription_name IN ('32.1110(a)(1)','32.1110(d)'); -- 52.232-33


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159149, prescription_id FROM Prescriptions
WHERE prescription_name IN ('32.1110(a)(2)','32.1110(d)'); -- 52.232-34


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159150, prescription_id FROM Prescriptions
WHERE prescription_name IN ('32.1110(c)'); -- 52.232-35


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159151, prescription_id FROM Prescriptions
WHERE prescription_name IN ('32.1110(d)'); -- 52.232-36


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159152, prescription_id FROM Prescriptions
WHERE prescription_name IN ('32.1110(e)'); -- 52.232-37


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159153, prescription_id FROM Prescriptions
WHERE prescription_name IN ('32.1110(g)'); -- 52.232-38


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159154, prescription_id FROM Prescriptions
WHERE prescription_name IN ('32.706-3'); -- 52.232-39


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159155, prescription_id FROM Prescriptions
WHERE prescription_name IN ('32.111(a)(4)'); -- 52.232-4


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159156, prescription_id FROM Prescriptions
WHERE prescription_name IN ('32.009-2'); -- 52.232-40


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159157, prescription_id FROM Prescriptions
WHERE prescription_name IN ('32.111(a)(5)'); -- 52.232-5


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159158, prescription_id FROM Prescriptions
WHERE prescription_name IN ('32.111(a)(6)'); -- 52.232-6


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159159, prescription_id FROM Prescriptions
WHERE prescription_name IN ('32.111(a)(7)'); -- 52.232-7


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159160, prescription_id FROM Prescriptions
WHERE prescription_name IN ('32.111(b)(1)'); -- 52.232-8


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159161, prescription_id FROM Prescriptions
WHERE prescription_name IN ('32.1110(b)(2)'); -- 52.232-9


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159162, prescription_id FROM Prescriptions
WHERE prescription_name IN ('33.215(a)'); -- 52.233-1


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159163, prescription_id FROM Prescriptions
WHERE prescription_name IN ('33.106(a)'); -- 52.233-2


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159164, prescription_id FROM Prescriptions
WHERE prescription_name IN ('33.106(b)'); -- 52.233-3


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159165, prescription_id FROM Prescriptions
WHERE prescription_name IN ('33.215(b)'); -- 52.233-4


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159166, prescription_id FROM Prescriptions
WHERE prescription_name IN ('34.104'); -- 52.234-1


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159167, prescription_id FROM Prescriptions
WHERE prescription_name IN ('34.203(a)'); -- 52.234-2


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159168, prescription_id FROM Prescriptions
WHERE prescription_name IN ('34.203(b)'); -- 52.234-3


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159169, prescription_id FROM Prescriptions
WHERE prescription_name IN ('34.203(c)'); -- 52.234-4


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159170, prescription_id FROM Prescriptions
WHERE prescription_name IN ('36.501(b)'); -- 52.236-1


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159171, prescription_id FROM Prescriptions
WHERE prescription_name IN ('36.510'); -- 52.236-10


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159172, prescription_id FROM Prescriptions
WHERE prescription_name IN ('36.511'); -- 52.236-11


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159173, prescription_id FROM Prescriptions
WHERE prescription_name IN ('36.512'); -- 52.236-12


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159174, prescription_id FROM Prescriptions
WHERE prescription_name IN ('36.513'); -- 52.236-13


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159175, prescription_id FROM Prescriptions
WHERE prescription_name IN ('36.514'); -- 52.236-14


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159176, prescription_id FROM Prescriptions
WHERE prescription_name IN ('36.515'); -- 52.236-15


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159177, prescription_id FROM Prescriptions
WHERE prescription_name IN ('36.516'); -- 52.236-16


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159178, prescription_id FROM Prescriptions
WHERE prescription_name IN ('36.517'); -- 52.236-17


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159179, prescription_id FROM Prescriptions
WHERE prescription_name IN ('36.518'); -- 52.236-18


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159180, prescription_id FROM Prescriptions
WHERE prescription_name IN ('36.519'); -- 52.236-19


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159181, prescription_id FROM Prescriptions
WHERE prescription_name IN ('36.502'); -- 52.236-2


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159182, prescription_id FROM Prescriptions
WHERE prescription_name IN ('36.521'); -- 52.236-21


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159183, prescription_id FROM Prescriptions
WHERE prescription_name IN ('36.609-1(c)'); -- 52.236-22


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159184, prescription_id FROM Prescriptions
WHERE prescription_name IN ('36.609-2(b)'); -- 52.236-23


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159185, prescription_id FROM Prescriptions
WHERE prescription_name IN ('36.609-3'); -- 52.236-24


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159186, prescription_id FROM Prescriptions
WHERE prescription_name IN ('36.609-4'); -- 52.236-25


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159187, prescription_id FROM Prescriptions
WHERE prescription_name IN ('36.522'); -- 52.236-26


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159188, prescription_id FROM Prescriptions
WHERE prescription_name IN ('36.523'); -- 52.236-27


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159189, prescription_id FROM Prescriptions
WHERE prescription_name IN ('36.520'); -- 52.236-28


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159190, prescription_id FROM Prescriptions
WHERE prescription_name IN ('36.503'); -- 52.236-3


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159191, prescription_id FROM Prescriptions
WHERE prescription_name IN ('36.504'); -- 52.236-4


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159192, prescription_id FROM Prescriptions
WHERE prescription_name IN ('36.505'); -- 52.236-5


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159193, prescription_id FROM Prescriptions
WHERE prescription_name IN ('36.506'); -- 52.236-6


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159194, prescription_id FROM Prescriptions
WHERE prescription_name IN ('36.507'); -- 52.236-7


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159195, prescription_id FROM Prescriptions
WHERE prescription_name IN ('36.508'); -- 52.236-8


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159196, prescription_id FROM Prescriptions
WHERE prescription_name IN ('36.509'); -- 52.236-9


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159197, prescription_id FROM Prescriptions
WHERE prescription_name IN ('37.110(a)'); -- 52.237-1


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159198, prescription_id FROM Prescriptions
WHERE prescription_name IN ('37.115-3'); -- 52.237-10


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159199, prescription_id FROM Prescriptions
WHERE prescription_name IN ('37.116-2'); -- 52.237-11


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159200, prescription_id FROM Prescriptions
WHERE prescription_name IN ('37.110(b)'); -- 52.237-2


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159201, prescription_id FROM Prescriptions
WHERE prescription_name IN ('37.110(c)'); -- 52.237-3


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159202, prescription_id FROM Prescriptions
WHERE prescription_name IN ('37.304(a)'); -- 52.237-4


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159203, prescription_id FROM Prescriptions
WHERE prescription_name IN ('37.304(b)'); -- 52.237-5


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159204, prescription_id FROM Prescriptions
WHERE prescription_name IN ('37.304(c)'); -- 52.237-6


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159205, prescription_id FROM Prescriptions
WHERE prescription_name IN ('37.403'); -- 52.237-7


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159206, prescription_id FROM Prescriptions
WHERE prescription_name IN ('37.113-2(a)'); -- 52.237-8


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159207, prescription_id FROM Prescriptions
WHERE prescription_name IN ('37.113-2(b)'); -- 52.237-9


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159208, prescription_id FROM Prescriptions
WHERE prescription_name IN ('39.106'); -- 52.239-1


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159209, prescription_id FROM Prescriptions
WHERE prescription_name IN ('41.501(b)'); -- 52.241-1


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159210, prescription_id FROM Prescriptions
WHERE prescription_name IN ('41.501(d)(4)'); -- 52.241-10


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159211, prescription_id FROM Prescriptions
WHERE prescription_name IN ('41.501(d)(5)'); -- 52.241-11


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159212, prescription_id FROM Prescriptions
WHERE prescription_name IN ('41.501(d)(6)'); -- 52.241-12


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159213, prescription_id FROM Prescriptions
WHERE prescription_name IN ('41.501(d)(7)'); -- 52.241-13


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159214, prescription_id FROM Prescriptions
WHERE prescription_name IN ('41.501(c)(1)'); -- 52.241-2


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159215, prescription_id FROM Prescriptions
WHERE prescription_name IN ('41.501(c)(2)'); -- 52.241-3


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159216, prescription_id FROM Prescriptions
WHERE prescription_name IN ('41.501(c)(3)'); -- 52.241-4


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159217, prescription_id FROM Prescriptions
WHERE prescription_name IN ('41.501(c)(4)'); -- 52.241-5


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159218, prescription_id FROM Prescriptions
WHERE prescription_name IN ('41.501(c)(5)'); -- 52.241-6


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159219, prescription_id FROM Prescriptions
WHERE prescription_name IN ('41.501(d)(1)'); -- 52.241-7


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159220, prescription_id FROM Prescriptions
WHERE prescription_name IN ('41.501(d)(2)'); -- 52.241-8


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159221, prescription_id FROM Prescriptions
WHERE prescription_name IN ('41.501(d)(3)'); -- 52.241-9


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159222, prescription_id FROM Prescriptions
WHERE prescription_name IN ('42.802'); -- 52.242-1


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159223, prescription_id FROM Prescriptions
WHERE prescription_name IN ('42.903'); -- 52.242-13


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159224, prescription_id FROM Prescriptions
WHERE prescription_name IN ('42.1305(a)'); -- 52.242-14


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159225, prescription_id FROM Prescriptions
WHERE prescription_name IN ('42.1305(b)(1)'); -- 52.242-15


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159226, prescription_id FROM Prescriptions
WHERE prescription_name IN ('42.1305(c)'); -- 52.242-17


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159227, prescription_id FROM Prescriptions
WHERE prescription_name IN ('42.1107(a)'); -- 52.242-2


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159228, prescription_id FROM Prescriptions
WHERE prescription_name IN ('42.709-6'); -- 52.242-3


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159229, prescription_id FROM Prescriptions
WHERE prescription_name IN ('42.703-2(f)'); -- 52.242-4


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159230, prescription_id FROM Prescriptions
WHERE prescription_name IN ('43.205(a)(1)'); -- 52.243-1


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159231, prescription_id FROM Prescriptions
WHERE prescription_name IN ('43.205(b)(1)'); -- 52.243-2


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159232, prescription_id FROM Prescriptions
WHERE prescription_name IN ('43.205(c)'); -- 52.243-3


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159233, prescription_id FROM Prescriptions
WHERE prescription_name IN ('43.205(b)(4)'); -- 52.243-4


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159234, prescription_id FROM Prescriptions
WHERE prescription_name IN ('43.205(e)'); -- 52.243-5


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159235, prescription_id FROM Prescriptions
WHERE prescription_name IN ('43.205(f)'); -- 52.243-6


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159236, prescription_id FROM Prescriptions
WHERE prescription_name IN ('43.107'); -- 52.243-7


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159237, prescription_id FROM Prescriptions
WHERE prescription_name IN ('44.204(a)(1)','44.204(a)(3)'); -- 52.244-2


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159238, prescription_id FROM Prescriptions
WHERE prescription_name IN ('44.204(b)'); -- 52.244-4


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159239, prescription_id FROM Prescriptions
WHERE prescription_name IN ('44.204(c)'); -- 52.244-5


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159240, prescription_id FROM Prescriptions
WHERE prescription_name IN ('44.403'); -- 52.244-6


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159241, prescription_id FROM Prescriptions
WHERE prescription_name IN ('45.107(a)'); -- 52.245-1


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159242, prescription_id FROM Prescriptions
WHERE prescription_name IN ('45.107(b)'); -- 52.245-2


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159243, prescription_id FROM Prescriptions
WHERE prescription_name IN ('45.107(c)'); -- 52.245-9


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159244, prescription_id FROM Prescriptions
WHERE prescription_name IN ('46.301'); -- 52.246-1


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159245, prescription_id FROM Prescriptions
WHERE prescription_name IN ('46.311'); -- 52.246-11


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159246, prescription_id FROM Prescriptions
WHERE prescription_name IN ('46.312'); -- 52.246-12


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159247, prescription_id FROM Prescriptions
WHERE prescription_name IN ('46.313'); -- 52.246-13


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159248, prescription_id FROM Prescriptions
WHERE prescription_name IN ('46.314'); -- 52.246-14


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159249, prescription_id FROM Prescriptions
WHERE prescription_name IN ('46.315'); -- 52.246-15


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159250, prescription_id FROM Prescriptions
WHERE prescription_name IN ('46.316'); -- 52.246-16


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159251, prescription_id FROM Prescriptions
WHERE prescription_name IN ('46.710(a)(1)'); -- 52.246-17


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159252, prescription_id FROM Prescriptions
WHERE prescription_name IN ('46.710(b)(1)'); -- 52.246-18


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159253, prescription_id FROM Prescriptions
WHERE prescription_name IN ('46.710(c)(1)'); -- 52.246-19


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159254, prescription_id FROM Prescriptions
WHERE prescription_name IN ('46.302'); -- 52.246-2


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159255, prescription_id FROM Prescriptions
WHERE prescription_name IN ('46.710(d)'); -- 52.246-20


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159256, prescription_id FROM Prescriptions
WHERE prescription_name IN ('46.710(e)(1)'); -- 52.246-21


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159257, prescription_id FROM Prescriptions
WHERE prescription_name IN ('46.805'); -- 52.246-23


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159258, prescription_id FROM Prescriptions
WHERE prescription_name IN ('46.805(a)'); -- 52.246-24


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159259, prescription_id FROM Prescriptions
WHERE prescription_name IN ('46.805(a)(4)'); -- 52.246-25


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159260, prescription_id FROM Prescriptions
WHERE prescription_name IN ('46.303'); -- 52.246-3


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159261, prescription_id FROM Prescriptions
WHERE prescription_name IN ('46.304'); -- 52.246-4


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159262, prescription_id FROM Prescriptions
WHERE prescription_name IN ('46.305'); -- 52.246-5


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159263, prescription_id FROM Prescriptions
WHERE prescription_name IN ('46.306'); -- 52.246-6


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159264, prescription_id FROM Prescriptions
WHERE prescription_name IN ('46.307(a)'); -- 52.246-7


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159265, prescription_id FROM Prescriptions
WHERE prescription_name IN ('46.308'); -- 52.246-8


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159266, prescription_id FROM Prescriptions
WHERE prescription_name IN ('46.309'); -- 52.246-9


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159267, prescription_id FROM Prescriptions
WHERE prescription_name IN ('47.104-4'); -- 52.247-1


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159268, prescription_id FROM Prescriptions
WHERE prescription_name IN ('47.207-4(a)(2)'); -- 52.247-10


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159269, prescription_id FROM Prescriptions
WHERE prescription_name IN ('47.207-4(b)'); -- 52.247-11


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159270, prescription_id FROM Prescriptions
WHERE prescription_name IN ('47.207-5(b)'); -- 52.247-12


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159271, prescription_id FROM Prescriptions
WHERE prescription_name IN ('47.207-5( c )'); -- 52.247-13


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159272, prescription_id FROM Prescriptions
WHERE prescription_name IN ('47.207-5(d)'); -- 52.247-14


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159273, prescription_id FROM Prescriptions
WHERE prescription_name IN ('47.207-5(e)'); -- 52.247-15


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159274, prescription_id FROM Prescriptions
WHERE prescription_name IN ('47.207-5(f)'); -- 52.247-16


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159275, prescription_id FROM Prescriptions
WHERE prescription_name IN ('47.207-6(a)(2)'); -- 52.247-17


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159276, prescription_id FROM Prescriptions
WHERE prescription_name IN ('47.207-6(c)(5)(i)'); -- 52.247-18


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159277, prescription_id FROM Prescriptions
WHERE prescription_name IN ('47.207-6(c)(5)(ii)'); -- 52.247-19


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159278, prescription_id FROM Prescriptions
WHERE prescription_name IN ('47.207-1(a)'); -- 52.247-2


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159279, prescription_id FROM Prescriptions
WHERE prescription_name IN ('47.207-6(c)(6)'); -- 52.247-20


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159280, prescription_id FROM Prescriptions
WHERE prescription_name IN ('47.207-7(c)'); -- 52.247-21


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159281, prescription_id FROM Prescriptions
WHERE prescription_name IN ('47.207-7(d)'); -- 52.247-22


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159282, prescription_id FROM Prescriptions
WHERE prescription_name IN ('47.207-7(e)'); -- 52.247-23


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159283, prescription_id FROM Prescriptions
WHERE prescription_name IN ('47.207-8(a)(1)'); -- 52.247-24


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159284, prescription_id FROM Prescriptions
WHERE prescription_name IN ('47.207-8(a)(2)(i)'); -- 52.247-25


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159285, prescription_id FROM Prescriptions
WHERE prescription_name IN ('47.207-8(a)(3)'); -- 52.247-26


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159286, prescription_id FROM Prescriptions
WHERE prescription_name IN ('47.207-8(b)'); -- 52.247-27


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159287, prescription_id FROM Prescriptions
WHERE prescription_name IN ('47.207-9(c)'); -- 52.247-28


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159288, prescription_id FROM Prescriptions
WHERE prescription_name IN ('47.303-1(c)'); -- 52.247-29


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159289, prescription_id FROM Prescriptions
WHERE prescription_name IN ('47.207-1(b)(1)'); -- 52.247-3


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159290, prescription_id FROM Prescriptions
WHERE prescription_name IN ('47.303-2(c)'); -- 52.247-30


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159291, prescription_id FROM Prescriptions
WHERE prescription_name IN ('47.303-3(c)'); -- 52.247-31


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159292, prescription_id FROM Prescriptions
WHERE prescription_name IN ('47.303-4(c)'); -- 52.247-32


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159293, prescription_id FROM Prescriptions
WHERE prescription_name IN ('47.303-5(c)'); -- 52.247-33


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159294, prescription_id FROM Prescriptions
WHERE prescription_name IN ('47.303-6(c)'); -- 52.247-34


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159295, prescription_id FROM Prescriptions
WHERE prescription_name IN ('47.303-7(c)'); -- 52.247-35


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159296, prescription_id FROM Prescriptions
WHERE prescription_name IN ('47.303-8(c)'); -- 52.247-36


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159297, prescription_id FROM Prescriptions
WHERE prescription_name IN ('47.303-9(c)'); -- 52.247-37


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159298, prescription_id FROM Prescriptions
WHERE prescription_name IN ('47.303-10(c)'); -- 52.247-38


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159299, prescription_id FROM Prescriptions
WHERE prescription_name IN ('47.303-11(c)'); -- 52.247-39


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159300, prescription_id FROM Prescriptions
WHERE prescription_name IN ('47.207-1(c)'); -- 52.247-4


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159301, prescription_id FROM Prescriptions
WHERE prescription_name IN ('47.303-12(c)'); -- 52.247-40


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159302, prescription_id FROM Prescriptions
WHERE prescription_name IN ('47.303-13(c)'); -- 52.247-41


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159303, prescription_id FROM Prescriptions
WHERE prescription_name IN ('47.303- 14(c)'); -- 52.247-42


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159304, prescription_id FROM Prescriptions
WHERE prescription_name IN ('47.303-15(c)'); -- 52.247-43


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159305, prescription_id FROM Prescriptions
WHERE prescription_name IN ('47.303-16(c)'); -- 52.247-44


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159306, prescription_id FROM Prescriptions
WHERE prescription_name IN ('47.305-2(b)'); -- 52.247-45


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159307, prescription_id FROM Prescriptions
WHERE prescription_name IN ('47.305-3(b)(4)(ii)'); -- 52.247-46


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159308, prescription_id FROM Prescriptions
WHERE prescription_name IN ('47.305-3(f)(2)'); -- 52.247-47


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159309, prescription_id FROM Prescriptions
WHERE prescription_name IN ('47.305-4(c)'); -- 52.247-48


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159310, prescription_id FROM Prescriptions
WHERE prescription_name IN ('47.305-5(b)(2)'); -- 52.247-49


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159311, prescription_id FROM Prescriptions
WHERE prescription_name IN ('47.207-1(d)'); -- 52.247-5


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159312, prescription_id FROM Prescriptions
WHERE prescription_name IN ('47.305-5(c)(1)'); -- 52.247-50


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159313, prescription_id FROM Prescriptions
WHERE prescription_name IN ('47.305-6(e)'); -- 52.247-51


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159314, prescription_id FROM Prescriptions
WHERE prescription_name IN ('47.305-6(f)(2)'); -- 52.247-52


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159315, prescription_id FROM Prescriptions
WHERE prescription_name IN ('47.305-9(b)(1)'); -- 52.247-53


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159316, prescription_id FROM Prescriptions
WHERE prescription_name IN ('47.305-12(a)(2)'); -- 52.247-55


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159317, prescription_id FROM Prescriptions
WHERE prescription_name IN ('47.305-13(a)(3)(i)'); -- 52.247-56


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159318, prescription_id FROM Prescriptions
WHERE prescription_name IN ('47.305-14(b)(4)'); -- 52.247-57


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159319, prescription_id FROM Prescriptions
WHERE prescription_name IN ('47.305-15(a)(2)'); -- 52.247-58


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159320, prescription_id FROM Prescriptions
WHERE prescription_name IN ('47.305-16(a)'); -- 52.247-59


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159321, prescription_id FROM Prescriptions
WHERE prescription_name IN ('47.207-1(e)'); -- 52.247-6


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159322, prescription_id FROM Prescriptions
WHERE prescription_name IN ('47.305-16(b)(1)'); -- 52.247-60


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159323, prescription_id FROM Prescriptions
WHERE prescription_name IN ('47.305-16(c)'); -- 52.247-61


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159324, prescription_id FROM Prescriptions
WHERE prescription_name IN ('47.305-16(d)(2)'); -- 52.247-62


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159325, prescription_id FROM Prescriptions
WHERE prescription_name IN ('47.405'); -- 52.247-63


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159326, prescription_id FROM Prescriptions
WHERE prescription_name IN ('47.507(a)'); -- 52.247-64


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159327, prescription_id FROM Prescriptions
WHERE prescription_name IN ('47.303-17(f)'); -- 52.247-65


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159328, prescription_id FROM Prescriptions
WHERE prescription_name IN ('47.305-17'); -- 52.247-66


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159329, prescription_id FROM Prescriptions
WHERE prescription_name IN ('47.103-2'); -- 52.247-67


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159330, prescription_id FROM Prescriptions
WHERE prescription_name IN ('47.208-2'); -- 52.247-68


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159331, prescription_id FROM Prescriptions
WHERE prescription_name IN ('47.207-3(d)(2)'); -- 52.247-7


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159332, prescription_id FROM Prescriptions
WHERE prescription_name IN ('47.207-3(e)(2)'); -- 52.247-8


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159333, prescription_id FROM Prescriptions
WHERE prescription_name IN ('47.207-4(a)(1)'); -- 52.247-9


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159334, prescription_id FROM Prescriptions
WHERE prescription_name IN ('48.201'); -- 52.248-1


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159335, prescription_id FROM Prescriptions
WHERE prescription_name IN ('48.201(f)'); -- 52.248-2


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159336, prescription_id FROM Prescriptions
WHERE prescription_name IN ('48.202'); -- 52.248-3


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159337, prescription_id FROM Prescriptions
WHERE prescription_name IN ('49.502(a)(1)'); -- 52.249-1


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159338, prescription_id FROM Prescriptions
WHERE prescription_name IN ('49.504(c)(1)'); -- 52.249-10


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159339, prescription_id FROM Prescriptions
WHERE prescription_name IN ('49.505(a)'); -- 52.249-12


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159340, prescription_id FROM Prescriptions
WHERE prescription_name IN ('49.505(b)'); -- 52.249-14


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159341, prescription_id FROM Prescriptions
WHERE prescription_name IN ('49.502(b)(1)(i)'); -- 52.249-2


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159342, prescription_id FROM Prescriptions
WHERE prescription_name IN ('49.502(b)(2)'); -- 52.249-3


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159343, prescription_id FROM Prescriptions
WHERE prescription_name IN ('49.502(c)'); -- 52.249-4


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159344, prescription_id FROM Prescriptions
WHERE prescription_name IN ('49.502(d)'); -- 52.249-5


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159345, prescription_id FROM Prescriptions
WHERE prescription_name IN ('49.503(a)(1)'); -- 52.249-6


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159346, prescription_id FROM Prescriptions
WHERE prescription_name IN ('49.503(b)'); -- 52.249-7


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159347, prescription_id FROM Prescriptions
WHERE prescription_name IN ('49.504(a)(1)'); -- 52.249-8


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159348, prescription_id FROM Prescriptions
WHERE prescription_name IN ('49.504(b)'); -- 52.249-9


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159349, prescription_id FROM Prescriptions
WHERE prescription_name IN ('50.104-4'); -- 52.250-1


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159350, prescription_id FROM Prescriptions
WHERE prescription_name IN ('50.206(a)'); -- 52.250-2


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159351, prescription_id FROM Prescriptions
WHERE prescription_name IN ('50.206(b)(1)'); -- 52.250-3


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159352, prescription_id FROM Prescriptions
WHERE prescription_name IN ('50.206(c)(1)'); -- 52.250-4


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159353, prescription_id FROM Prescriptions
WHERE prescription_name IN ('50.206(d)'); -- 52.250-5


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159354, prescription_id FROM Prescriptions
WHERE prescription_name IN ('51.107'); -- 52.251-1


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159355, prescription_id FROM Prescriptions
WHERE prescription_name IN ('51.205'); -- 52.251-2


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159356, prescription_id FROM Prescriptions
WHERE prescription_name IN ('52.107(a)'); -- 52.252-1


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159357, prescription_id FROM Prescriptions
WHERE prescription_name IN ('52.107(b)'); -- 52.252-2


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159358, prescription_id FROM Prescriptions
WHERE prescription_name IN ('52.107(c)'); -- 52.252-3


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159359, prescription_id FROM Prescriptions
WHERE prescription_name IN ('52.107(d)'); -- 52.252-4


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159360, prescription_id FROM Prescriptions
WHERE prescription_name IN ('52.107(e)'); -- 52.252-5


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159361, prescription_id FROM Prescriptions
WHERE prescription_name IN ('52.107(f)'); -- 52.252-6


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159362, prescription_id FROM Prescriptions
WHERE prescription_name IN ('53.111'); -- 52.253-1


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159363, prescription_id FROM Prescriptions
WHERE prescription_name IN ('216.506(d)','216.506(d)(1)'); -- 252.216-7010 Alternate I


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159364, prescription_id FROM Prescriptions
WHERE prescription_name IN ('217.208-70(a)','217.208-70(a)(2)'); -- 252.217-7000 Alternate I


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159365, prescription_id FROM Prescriptions
WHERE prescription_name IN ('219.708(b)(1)(A)(1)'); -- 252.219-7003 Alternate I


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159366, prescription_id FROM Prescriptions
WHERE prescription_name IN ('2013-O0014'); -- 252.219-7003 Alternate I (DEVIATION 2013-00014)


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159367, prescription_id FROM Prescriptions
WHERE prescription_name IN ('223.7106','223.7106(b)'); -- 252.223-7006 Alternate I


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159368, prescription_id FROM Prescriptions
WHERE prescription_name IN ('25.1101(1)','25.1101(1)(ii)'); -- 252.225-7000 Alternate I


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159369, prescription_id FROM Prescriptions
WHERE prescription_name IN ('225.1101(2)(i)','225.1101(2)(iii)'); -- 252.225-7001 Alternate I


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159370, prescription_id FROM Prescriptions
WHERE prescription_name IN ('225.1101(5)','225.1101(5)(ii)'); -- 252.225-7020 Alternate I


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159371, prescription_id FROM Prescriptions
WHERE prescription_name IN ('225.1101(6)','25.1101(6)(ii)'); -- 252.225-7021 Alternate II


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159372, prescription_id FROM Prescriptions
WHERE prescription_name IN ('225.1101(9)','225.1101(9)(ii)'); -- 252.225-7035 Alternate I


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159373, prescription_id FROM Prescriptions
WHERE prescription_name IN ('225.1101(9)','225.1101(9)(iii)'); -- 252.225-7035 Alternate II


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159374, prescription_id FROM Prescriptions
WHERE prescription_name IN ('225.1101(9)','225.1101(9)(iv)'); -- 252.225-7035 Alternate III


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159375, prescription_id FROM Prescriptions
WHERE prescription_name IN ('225.1101(9)','225.1101(9)(v)'); -- 252.225-7035 Alternate IV


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159376, prescription_id FROM Prescriptions
WHERE prescription_name IN ('225.1101(9)','225.1101(9)(vi)'); -- 252.225-7035 Alternate V


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159377, prescription_id FROM Prescriptions
WHERE prescription_name IN ('225.1101(10)(i)','225.1101(10)(i)(B)'); -- 252.225-7036 Alternate I


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159378, prescription_id FROM Prescriptions
WHERE prescription_name IN ('225.1101(10)(i)','225.1101(10)(i)(C)'); -- 252.225-7036 Alternate II


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159379, prescription_id FROM Prescriptions
WHERE prescription_name IN ('225.1101(10)(i)','225.1101(10)(i)(D)'); -- 252.225-7036 Alternate III


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159380, prescription_id FROM Prescriptions
WHERE prescription_name IN ('225.1101(10)(i)','225.1101(10)(i)(E)'); -- 252.225-7036 Alternate IV


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159381, prescription_id FROM Prescriptions
WHERE prescription_name IN ('225.1101(10)(i)','225.1101(10)(i)(F)'); -- 252.225-7036 Alternate V


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159382, prescription_id FROM Prescriptions
WHERE prescription_name IN ('225.7503(a)','225.7503(a)(2)'); -- 252.225-7044 Alternate I


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159383, prescription_id FROM Prescriptions
WHERE prescription_name IN ('225.7503(b)','225.7503(b)(2)'); -- 252.225-7045 Alternate I


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159384, prescription_id FROM Prescriptions
WHERE prescription_name IN ('225.7503(b)','225.7503(b)(3)'); -- 252.225-7045 Alternate II


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159385, prescription_id FROM Prescriptions
WHERE prescription_name IN ('225.7503(b)','225.7503(b)(4)'); -- 252.225-7045 Alternate III


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159386, prescription_id FROM Prescriptions
WHERE prescription_name IN ('227.7009-3(b)'); -- 252.227-7005 Alternate I


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159387, prescription_id FROM Prescriptions
WHERE prescription_name IN ('227.7009-3(b)'); -- 252.227-7005 Alternate II


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159388, prescription_id FROM Prescriptions
WHERE prescription_name IN ('227.7103-6(a)'); -- 252.227-7013 Alternate I


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159389, prescription_id FROM Prescriptions
WHERE prescription_name IN ('227.7103-6(a)'); -- 252.227-7013 Alternate II


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159390, prescription_id FROM Prescriptions
WHERE prescription_name IN ('227.7203-6(a)(1)'); -- 252.227-7014 Alternate I


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159391, prescription_id FROM Prescriptions
WHERE prescription_name IN ('227.7102-4(a)(1)'); -- 252.227-7015 Alternate I


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159392, prescription_id FROM Prescriptions
WHERE prescription_name IN ('227.7104(a)'); -- 252.227-7018 Alternate I


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159393, prescription_id FROM Prescriptions
WHERE prescription_name IN ('229.402-70(a)','229.402-70(a)(2)'); -- 252.229-7001 Alternate I


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159394, prescription_id FROM Prescriptions
WHERE prescription_name IN ('234.7101(a)(2)'); -- 252.234-7003 Alternate I


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159395, prescription_id FROM Prescriptions
WHERE prescription_name IN ('234.7101(b)','234.7101(b)(2)'); -- 252.234-7004 Alternate I


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159396, prescription_id FROM Prescriptions
WHERE prescription_name IN ('235.072(b)','235.072(b)(1)'); -- 252.235-7003 Alternate I


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159397, prescription_id FROM Prescriptions
WHERE prescription_name IN ('237.7003(a)'); -- 252.237-7002 Alternate I


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159398, prescription_id FROM Prescriptions
WHERE prescription_name IN ('237.7101(e)','237.7101(e)(3)'); -- 252.237-7016 Alternate I


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159399, prescription_id FROM Prescriptions
WHERE prescription_name IN ('237.7101(e)','237.7101(e)(2)'); -- 252.237-7016 Alternate II


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159400, prescription_id FROM Prescriptions
WHERE prescription_name IN ('244.305-71','244.305-71(b)'); -- 252.244-7001 Alternate I


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159401, prescription_id FROM Prescriptions
WHERE prescription_name IN ('246.710(1)','246.710(1)(ii)'); -- 252.246-7001 Alternate I


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159402, prescription_id FROM Prescriptions
WHERE prescription_name IN ('246.710(1)','246.710(1)(iii)'); -- 252.246-7001 Alternate II


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159403, prescription_id FROM Prescriptions
WHERE prescription_name IN ('247.271-3','247.271-3(a)','247.271-3(a)(2)'); -- 252.247-7008 Alternate I


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159404, prescription_id FROM Prescriptions
WHERE prescription_name IN ('247.574(b)'); -- 252.247-7023 Alternate I


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159405, prescription_id FROM Prescriptions
WHERE prescription_name IN ('247.574(b)'); -- 252.247-7023 Alternate II


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159406, prescription_id FROM Prescriptions
WHERE prescription_name IN ('3.503-2'); -- 52.203-6 Alternate I


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159407, prescription_id FROM Prescriptions
WHERE prescription_name IN ('4.404(a)'); -- 52.204-2 Alternate I


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159408, prescription_id FROM Prescriptions
WHERE prescription_name IN ('4.404(a)'); -- 52.204-2 Alternate II


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159409, prescription_id FROM Prescriptions
WHERE prescription_name IN ('4.1105(a)(1)'); -- 52.204-7 Alternate I


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159410, prescription_id FROM Prescriptions
WHERE prescription_name IN ('9.308-1(a)(2)','9.308-1(b)(2)'); -- 52.209-3 Alternate I


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159411, prescription_id FROM Prescriptions
WHERE prescription_name IN ('9.308-1(a)(3)','9.308-1(b)(3)'); -- 52.209-3 Alternate II


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159412, prescription_id FROM Prescriptions
WHERE prescription_name IN ('9.308-1(b)(2)','9.308-2(a)(1)'); -- 52.209-4 Alternate I


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159413, prescription_id FROM Prescriptions
WHERE prescription_name IN ('9.308-1(b)(3)','9.308-2(a)(1)'); -- 52.209-4 Alternate II


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159414, prescription_id FROM Prescriptions
WHERE prescription_name IN ('11.404(b)'); -- 52.211-10 Alternate I


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159415, prescription_id FROM Prescriptions
WHERE prescription_name IN ('11.404(a)(2)'); -- 52.211-8 Alternate I


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159416, prescription_id FROM Prescriptions
WHERE prescription_name IN ('11.404(a)(2)'); -- 52.211-8 Alternate II


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159417, prescription_id FROM Prescriptions
WHERE prescription_name IN ('11.404(a)(2)'); -- 52.211-8 Alternate III


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159418, prescription_id FROM Prescriptions
WHERE prescription_name IN ('11.404(a)(3)'); -- 52.211-9 Alternate I


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159419, prescription_id FROM Prescriptions
WHERE prescription_name IN ('11.404(a)(3)'); -- 52.211-9 Alternate II


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159420, prescription_id FROM Prescriptions
WHERE prescription_name IN ('11.404(a)(3)'); -- 52.211-9 Alternate III


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159421, prescription_id FROM Prescriptions
WHERE prescription_name IN ('12.301(b)(2)'); -- 52.212-3 Alternate I


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159422, prescription_id FROM Prescriptions
WHERE prescription_name IN ('12.301(b)(3)'); -- 52.212-4 Alternate I


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159423, prescription_id FROM Prescriptions
WHERE prescription_name IN ('12.301(b)(4)(i)'); -- 52.212-5 Alternate I


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159424, prescription_id FROM Prescriptions
WHERE prescription_name IN ('2013-O0019'); -- 52.212-5 Alternate I (DEVIATION 2013-00019)


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159425, prescription_id FROM Prescriptions
WHERE prescription_name IN ('12.301(b)(4)(ii)'); -- 52.212-5 Alternate II


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159426, prescription_id FROM Prescriptions
WHERE prescription_name IN ('2013-O0019'); -- 52.212-5 Alternate II (DEVIATION 2013-00019)


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159427, prescription_id FROM Prescriptions
WHERE prescription_name IN ('14.201-6(g)(2)'); -- 52.214-13 Alternate I


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159428, prescription_id FROM Prescriptions
WHERE prescription_name IN ('14.201-6(o)(2)(i)'); -- 52.214-20 Alternate I


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159429, prescription_id FROM Prescriptions
WHERE prescription_name IN ('14.201-6(o)(2)(ii)'); -- 52.214-20 Alternate II


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159430, prescription_id FROM Prescriptions
WHERE prescription_name IN ('14.201-6(p)(2)'); -- 52.214-21 Alternate I


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159431, prescription_id FROM Prescriptions
WHERE prescription_name IN ('14.201-7(a)(2)'); -- 52.214-26 Alternate I


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159432, prescription_id FROM Prescriptions
WHERE prescription_name IN ('15.209(a)(1)'); -- 52.215-1 Alternate I


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159433, prescription_id FROM Prescriptions
WHERE prescription_name IN ('15.209(a)(2)'); -- 52.215-1 Alternate II


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159434, prescription_id FROM Prescriptions
WHERE prescription_name IN ('15.408(f)(2)'); -- 52.215-14 Alternate I


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159435, prescription_id FROM Prescriptions
WHERE prescription_name IN ('15.209(b)(2)'); -- 52.215-2 Alternate I


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159436, prescription_id FROM Prescriptions
WHERE prescription_name IN ('15.209(b)(3)'); -- 52.215-2 Alternate II


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159437, prescription_id FROM Prescriptions
WHERE prescription_name IN ('15.209(b)(4)'); -- 52.215-2 Alternate III


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159438, prescription_id FROM Prescriptions
WHERE prescription_name IN ('15.408(l)','215.408(4)(ii)'); -- 52.215-20 Alternate I


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159439, prescription_id FROM Prescriptions
WHERE prescription_name IN ('15.408(I)','215.408(4)(ii)'); -- 52.215-20 Alternate II


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159440, prescription_id FROM Prescriptions
WHERE prescription_name IN ('15.408(I)','215.408(4)(ii)'); -- 52.215-20 Alternate III


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159441, prescription_id FROM Prescriptions
WHERE prescription_name IN ('15.408(I)','215.408(4)(ii)'); -- 52.215-20 Alternate IV


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159442, prescription_id FROM Prescriptions
WHERE prescription_name IN ('15.408(m)'); -- 52.215-21 Alternate I


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159443, prescription_id FROM Prescriptions
WHERE prescription_name IN ('15.408(m)'); -- 52.215-21 Alternate II


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159444, prescription_id FROM Prescriptions
WHERE prescription_name IN ('15.408(m)'); -- 52.215-21 Alternate III


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159445, prescription_id FROM Prescriptions
WHERE prescription_name IN ('15.408(m)'); -- 52.215-21 Alternate IV


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159446, prescription_id FROM Prescriptions
WHERE prescription_name IN ('15.408(n)(2)(iii)'); -- 52.215-23 Alternate I


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159447, prescription_id FROM Prescriptions
WHERE prescription_name IN ('15.408(a)(1)'); -- 52.215-9 Alternate I


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159448, prescription_id FROM Prescriptions
WHERE prescription_name IN ('15.408(a)(2)'); -- 52.215-9 Alternate II


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159449, prescription_id FROM Prescriptions
WHERE prescription_name IN ('16.307(e)(2)'); -- 52.216-11 Alternate I


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159450, prescription_id FROM Prescriptions
WHERE prescription_name IN ('16.307(f)(2)'); -- 52.216-12 Alternate I


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159451, prescription_id FROM Prescriptions
WHERE prescription_name IN ('16.406(a)'); -- 52.216-16 Alternate I


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159452, prescription_id FROM Prescriptions
WHERE prescription_name IN ('16.406(b)'); -- 52.216-17 Alternate I


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159453, prescription_id FROM Prescriptions
WHERE prescription_name IN ('16.506(d)(2)'); -- 52.216-21 Alternate I


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159454, prescription_id FROM Prescriptions
WHERE prescription_name IN ('16.506(d)(3)'); -- 52.216-21 Alternate II


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159455, prescription_id FROM Prescriptions
WHERE prescription_name IN ('16.506(d)(4)'); -- 52.216-21 Alternate III


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159456, prescription_id FROM Prescriptions
WHERE prescription_name IN ('16.506(d)(5)'); -- 52.216-21 Alternate IV


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159457, prescription_id FROM Prescriptions
WHERE prescription_name IN ('16.603-4(b)(3)'); -- 52.216-25 Alternate I


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159458, prescription_id FROM Prescriptions
WHERE prescription_name IN ('16.307(a)(2)'); -- 52.216-7 Alternate I


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159459, prescription_id FROM Prescriptions
WHERE prescription_name IN ('16.307(a)(3)'); -- 52.216-7 Alternate II


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159460, prescription_id FROM Prescriptions
WHERE prescription_name IN ('16.307(a)(4)'); -- 52.216-7 Alternate III


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159461, prescription_id FROM Prescriptions
WHERE prescription_name IN ('16.307(a)(5)'); -- 52.216-7 Alternate IV


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159462, prescription_id FROM Prescriptions
WHERE prescription_name IN ('19.309(a)(2)'); -- 52.219-1 Alternate I


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159463, prescription_id FROM Prescriptions
WHERE prescription_name IN ('19.811-3(d)(1)'); -- 52.219-18 Alternate I


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159464, prescription_id FROM Prescriptions
WHERE prescription_name IN ('19.811-3(d)(2)'); -- 52.219-18 Alternate II


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159465, prescription_id FROM Prescriptions
WHERE prescription_name IN ('19.1309(a)(1)'); -- 52.219-3 Alternate I


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159466, prescription_id FROM Prescriptions
WHERE prescription_name IN ('19.1309(b)(1)'); -- 52.219-4 Alternate I


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159467, prescription_id FROM Prescriptions
WHERE prescription_name IN ('19.508(c)'); -- 52.219-6 Alternate I


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159468, prescription_id FROM Prescriptions
WHERE prescription_name IN ('19.508(c)'); -- 52.219-6 Alternate II


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159469, prescription_id FROM Prescriptions
WHERE prescription_name IN ('19.508(d)'); -- 52.219-7 Alternate I


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159470, prescription_id FROM Prescriptions
WHERE prescription_name IN ('19.508(d)'); -- 52.219-7 Alternate II


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159471, prescription_id FROM Prescriptions
WHERE prescription_name IN ('19.708(b)(1)'); -- 52.219-9 Alternate I


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159472, prescription_id FROM Prescriptions
WHERE prescription_name IN ('2013-O0014)'); -- 52.219-9 Alternate I (DEVIATION 2013-00014)


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159473, prescription_id FROM Prescriptions
WHERE prescription_name IN ('19.708(b)(1)'); -- 52.219-9 Alternate II


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159474, prescription_id FROM Prescriptions
WHERE prescription_name IN ('2013-O0014)'); -- 52.219-9 Alternate II (DEVIATION 2013-00014)


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159475, prescription_id FROM Prescriptions
WHERE prescription_name IN ('19.708(b)(1)(iii)'); -- 52.219-9 Alternate III


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159476, prescription_id FROM Prescriptions
WHERE prescription_name IN ('2013-O0014)'); -- 52.219-9 Alternate III (DEVIATION 2013-00014)


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159477, prescription_id FROM Prescriptions
WHERE prescription_name IN ('22.810(e)'); -- 52.222-26 Alternate I


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159478, prescription_id FROM Prescriptions
WHERE prescription_name IN ('22.505(a)(1)'); -- 52.222-33 Alternate I


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159479, prescription_id FROM Prescriptions
WHERE prescription_name IN ('22.505(a)(2)'); -- 52.222-33 Alternate II


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159480, prescription_id FROM Prescriptions
WHERE prescription_name IN ('22.505(b)(2)'); -- 52.222-34 Alternate I


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159481, prescription_id FROM Prescriptions
WHERE prescription_name IN ('22.1310(a)(2)'); -- 52.222-35 Alternate I


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159482, prescription_id FROM Prescriptions
WHERE prescription_name IN ('22.1408(b)'); -- 52.222-36 Alternate I


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159483, prescription_id FROM Prescriptions
WHERE prescription_name IN ('22.1705(a)(2)'); -- 52.222-50 Alternate I


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159484, prescription_id FROM Prescriptions
WHERE prescription_name IN ('23.705(b)(1)'); -- 52.223-13 Alternate I


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159485, prescription_id FROM Prescriptions
WHERE prescription_name IN ('23.705(d)(2)'); -- 52.223-14 Alternate I


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159486, prescription_id FROM Prescriptions
WHERE prescription_name IN ('23.705(b)(2)'); -- 52.223-16 Alternate I


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159487, prescription_id FROM Prescriptions
WHERE prescription_name IN ('23.303(b)'); -- 52.223-3 Alternate I


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159488, prescription_id FROM Prescriptions
WHERE prescription_name IN ('23.1005(b)'); -- 52.223-5 Alternate I


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159489, prescription_id FROM Prescriptions
WHERE prescription_name IN ('23.1005(c)'); -- 52.223-5 Alternate II


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159490, prescription_id FROM Prescriptions
WHERE prescription_name IN ('23.406(d)'); -- 52.223-9 Alternate I


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159491, prescription_id FROM Prescriptions
WHERE prescription_name IN ('25.1102(b)(2)'); -- 52.225-10 Alternate I


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159492, prescription_id FROM Prescriptions
WHERE prescription_name IN ('25.1102(c)(3)'); -- 52.225-11 Alternate I


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159493, prescription_id FROM Prescriptions
WHERE prescription_name IN ('25.1102(d)(2)'); -- 52.225-12 Alternate I


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159494, prescription_id FROM Prescriptions
WHERE prescription_name IN ('25.1102(d)(3)'); -- 52.225-12 Alternate II


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159495, prescription_id FROM Prescriptions
WHERE prescription_name IN ('25.1102(e)(1)'); -- 52.225-22 Alternate I


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159496, prescription_id FROM Prescriptions
WHERE prescription_name IN ('25.1102(e)(1)'); -- 52.225-23 Alternate I


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159497, prescription_id FROM Prescriptions
WHERE prescription_name IN ('25.1102(e)(1)'); -- 52.225-24 Alternate I


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159498, prescription_id FROM Prescriptions
WHERE prescription_name IN ('25.1102(e)(1)'); -- 52.225-24 Alternate II


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159499, prescription_id FROM Prescriptions
WHERE prescription_name IN ('25.1101(b)(1)(ii)'); -- 52.225-3 Alternate I


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159500, prescription_id FROM Prescriptions
WHERE prescription_name IN ('25.1101(b)(1)(iii)'); -- 52.225-3 Alternate II


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159501, prescription_id FROM Prescriptions
WHERE prescription_name IN ('25.1101(b)(1)(iv)'); -- 52.225-3 Alternate III


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159502, prescription_id FROM Prescriptions
WHERE prescription_name IN ('25.1101(b)(2)(ii)'); -- 52.225-4 Alternate I


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159503, prescription_id FROM Prescriptions
WHERE prescription_name IN ('25.1101(b)(2)(iii)'); -- 52.225-4 Alternate II


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159504, prescription_id FROM Prescriptions
WHERE prescription_name IN ('25.1101(b)(2)(iv)'); -- 52.225-4 Alternate III


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159505, prescription_id FROM Prescriptions
WHERE prescription_name IN ('27.201-2(a)(1)'); -- 52.227-1 Alternate I


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159506, prescription_id FROM Prescriptions
WHERE prescription_name IN ('27.201-2(a)(1)'); -- 52.227-1 Alternate II


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159507, prescription_id FROM Prescriptions
WHERE prescription_name IN ('27.303(b)(3)'); -- 52.227-11 Alternate I


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159508, prescription_id FROM Prescriptions
WHERE prescription_name IN ('27.303(b)(4)'); -- 52.227-11 Alternate II


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159509, prescription_id FROM Prescriptions
WHERE prescription_name IN ('27.303(b)(5)'); -- 52.227-11 Alternate III


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159510, prescription_id FROM Prescriptions
WHERE prescription_name IN ('27.303(b)(6)'); -- 52.227-11 Alternate IV


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159511, prescription_id FROM Prescriptions
WHERE prescription_name IN ('27.303(b)(7)'); -- 52.227-11 Alternate V


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159512, prescription_id FROM Prescriptions
WHERE prescription_name IN ('27.303(e)(4)'); -- 52.227-13 Alternate I


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159513, prescription_id FROM Prescriptions
WHERE prescription_name IN ('27.303(e)(5)'); -- 52.227-13 Alternate II


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159514, prescription_id FROM Prescriptions
WHERE prescription_name IN ('27.409(b)(2)'); -- 52.227-14 Alternate I


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159515, prescription_id FROM Prescriptions
WHERE prescription_name IN ('27.409(b)(3)'); -- 52.227-14 Alternate II


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159516, prescription_id FROM Prescriptions
WHERE prescription_name IN ('27.409(b)(4)'); -- 52.227-14 Alternate III


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159517, prescription_id FROM Prescriptions
WHERE prescription_name IN ('27.409(b)(4)'); -- 52.227-14 Alternate IV


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159518, prescription_id FROM Prescriptions
WHERE prescription_name IN ('27.409(b)(5)'); -- 52.227-14 Alternate V


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159519, prescription_id FROM Prescriptions
WHERE prescription_name IN ('27.201-2(c)(1)'); -- 52.227-3 Alternate I


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159520, prescription_id FROM Prescriptions
WHERE prescription_name IN ('27.201-2(c)(2)'); -- 52.227-3 Alternate II


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159521, prescription_id FROM Prescriptions
WHERE prescription_name IN ('27.201-2(c)(3)'); -- 52.227-3 Alternate III


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159522, prescription_id FROM Prescriptions
WHERE prescription_name IN ('27.201-2(d)(2)'); -- 52.227-4 Alternate I


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159523, prescription_id FROM Prescriptions
WHERE prescription_name IN ('27.202-5(a)(2)'); -- 52.227-6 Alternate I


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159524, prescription_id FROM Prescriptions
WHERE prescription_name IN ('28.103-4'); -- 52.228-16 Alternate I


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159525, prescription_id FROM Prescriptions
WHERE prescription_name IN ('29.401-2'); -- 52.229-2 Alternate I


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159526, prescription_id FROM Prescriptions
WHERE prescription_name IN ('32.412(b)'); -- 52.232-12 Alternate I


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159527, prescription_id FROM Prescriptions
WHERE prescription_name IN ('32.412(c)'); -- 52.232-12 Alternate II


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159528, prescription_id FROM Prescriptions
WHERE prescription_name IN ('32.412(d)'); -- 52.232-12 Alternate III


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159529, prescription_id FROM Prescriptions
WHERE prescription_name IN ('32.412(e)'); -- 52.232-12 Alternate IV


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159530, prescription_id FROM Prescriptions
WHERE prescription_name IN ('32.412(f)'); -- 52.232-12 Alternate V


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159531, prescription_id FROM Prescriptions
WHERE prescription_name IN ('32.502-4(b)'); -- 52.232-16 Alternate I


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159532, prescription_id FROM Prescriptions
WHERE prescription_name IN ('32.502-4(c)'); -- 52.232-16 Alternate II


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159533, prescription_id FROM Prescriptions
WHERE prescription_name IN ('32.502-4(d)'); -- 52.232-16 Alternate III


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159534, prescription_id FROM Prescriptions
WHERE prescription_name IN ('32.806(a)(2)'); -- 52.232-23 Alternate I


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159535, prescription_id FROM Prescriptions
WHERE prescription_name IN ('32.908(c)(3)'); -- 52.232-25 Alternate I


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159536, prescription_id FROM Prescriptions
WHERE prescription_name IN ('32.1005(b)(2)'); -- 52.232-28 Alternate I


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159537, prescription_id FROM Prescriptions
WHERE prescription_name IN ('33.215(a)'); -- 52.233-1 Alternate I


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159538, prescription_id FROM Prescriptions
WHERE prescription_name IN ('33.106(b)'); -- 52.233-3 Alternate I


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159539, prescription_id FROM Prescriptions
WHERE prescription_name IN ('36.513'); -- 52.236-13 Alternate I


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159540, prescription_id FROM Prescriptions
WHERE prescription_name IN ('36.516'); -- 52.236-16 Alternate I


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159541, prescription_id FROM Prescriptions
WHERE prescription_name IN ('36.521'); -- 52.236-21 Alternate I


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159542, prescription_id FROM Prescriptions
WHERE prescription_name IN ('36.521'); -- 52.236-21 Alternate II


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159543, prescription_id FROM Prescriptions
WHERE prescription_name IN ('36.523'); -- 52.236-27 Alternate I


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159544, prescription_id FROM Prescriptions
WHERE prescription_name IN ('37.304(a)'); -- 52.237-4 Alternate I


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159545, prescription_id FROM Prescriptions
WHERE prescription_name IN ('41.501(d)(3)'); -- 52.241-9 Alternate I


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159546, prescription_id FROM Prescriptions
WHERE prescription_name IN ('42.1305(b)(2)'); -- 52.242-15 Alternate I


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159547, prescription_id FROM Prescriptions
WHERE prescription_name IN ('43.205(a)(2)'); -- 52.243-1 Alternate I


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159548, prescription_id FROM Prescriptions
WHERE prescription_name IN ('43.205(a)(3)'); -- 52.243-1 Alternate II


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159549, prescription_id FROM Prescriptions
WHERE prescription_name IN ('43.205(a)(4)'); -- 52.243-1 Alternate III


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159550, prescription_id FROM Prescriptions
WHERE prescription_name IN ('43.205(a)(5)'); -- 52.243-1 Alternate IV


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159551, prescription_id FROM Prescriptions
WHERE prescription_name IN ('43.205(a)(6)'); -- 52.243-1 Alternate V


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159552, prescription_id FROM Prescriptions
WHERE prescription_name IN ('43.205(b)(2)'); -- 52.243-2 Alternate I


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159553, prescription_id FROM Prescriptions
WHERE prescription_name IN ('43.205(b)(3)'); -- 52.243-2 Alternate II


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159554, prescription_id FROM Prescriptions
WHERE prescription_name IN ('43.205(b)(4)'); -- 52.243-2 Alternate III


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159555, prescription_id FROM Prescriptions
WHERE prescription_name IN ('43.205(b)(6)'); -- 52.243-2 Alternate V


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159556, prescription_id FROM Prescriptions
WHERE prescription_name IN ('44.204(a)(2)'); -- 52.244-2 Alternate I


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159557, prescription_id FROM Prescriptions
WHERE prescription_name IN ('45.107(a)(2)'); -- 52.245-1 Alternate I


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159558, prescription_id FROM Prescriptions
WHERE prescription_name IN ('45.107(a)(3)'); -- 52.245-1 Alternate II


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159559, prescription_id FROM Prescriptions
WHERE prescription_name IN ('46.710(a)(3)'); -- 52.246-17 Alternate II


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159560, prescription_id FROM Prescriptions
WHERE prescription_name IN ('46.710(a)(4)'); -- 52.246-17 Alternate III


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159561, prescription_id FROM Prescriptions
WHERE prescription_name IN ('46.710(a)(5)'); -- 52.246-17 Alternate IV


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159562, prescription_id FROM Prescriptions
WHERE prescription_name IN ('46.710(a)(6)'); -- 52.246-17 Alternate V


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159563, prescription_id FROM Prescriptions
WHERE prescription_name IN ('46.710(b)(2)'); -- 52.246-18 Alternate II


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159564, prescription_id FROM Prescriptions
WHERE prescription_name IN ('46.710(b)(3)'); -- 52.246-18 Alternate III


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159565, prescription_id FROM Prescriptions
WHERE prescription_name IN ('46.710(b)(4)'); -- 52.246-18 Alternate IV


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159566, prescription_id FROM Prescriptions
WHERE prescription_name IN ('46.710(c)(2)'); -- 52.246-19 Alternate I


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159567, prescription_id FROM Prescriptions
WHERE prescription_name IN ('46.710(c)(3)'); -- 52.246-19 Alternate II


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159568, prescription_id FROM Prescriptions
WHERE prescription_name IN ('46.710(c)(4)'); -- 52.246-19 Alternate III


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159569, prescription_id FROM Prescriptions
WHERE prescription_name IN ('46.302'); -- 52.246-2 Alternate I


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159570, prescription_id FROM Prescriptions
WHERE prescription_name IN ('46.302'); -- 52.246-2 Alternate II


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159571, prescription_id FROM Prescriptions
WHERE prescription_name IN ('46.710(e)(2)'); -- 52.246-21 Alternate I


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159572, prescription_id FROM Prescriptions
WHERE prescription_name IN ('46.805(a)'); -- 52.246-24 Alternate I


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159573, prescription_id FROM Prescriptions
WHERE prescription_name IN ('46.306'); -- 52.246-6 Alternate I


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159574, prescription_id FROM Prescriptions
WHERE prescription_name IN ('46.308'); -- 52.246-8 Alternate I


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159575, prescription_id FROM Prescriptions
WHERE prescription_name IN ('47.207-1(b)(2)'); -- 52.247-3 Alternate I


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159576, prescription_id FROM Prescriptions
WHERE prescription_name IN ('47.305-6(e)(1)'); -- 52.247-51 Alternate I


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159577, prescription_id FROM Prescriptions
WHERE prescription_name IN ('47.305-6(e)(2)'); -- 52.247-51 Alternate II


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159578, prescription_id FROM Prescriptions
WHERE prescription_name IN ('47.305-6(e)(3)'); -- 52.247-51 Alternate III


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159579, prescription_id FROM Prescriptions
WHERE prescription_name IN ('47.507(a)(2)'); -- 52.247-64 Alternate I


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159580, prescription_id FROM Prescriptions
WHERE prescription_name IN ('47.507(a)(3)'); -- 52.247-64 Alternate II


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159581, prescription_id FROM Prescriptions
WHERE prescription_name IN ('48.201(c)'); -- 52.248-1 Alternate I


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159582, prescription_id FROM Prescriptions
WHERE prescription_name IN ('48.201(d)'); -- 52.248-1 Alternate II


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159583, prescription_id FROM Prescriptions
WHERE prescription_name IN ('48.201(e)(1)'); -- 52.248-1 Alternate III


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159584, prescription_id FROM Prescriptions
WHERE prescription_name IN ('48.202'); -- 52.248-3 Alternate I


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159585, prescription_id FROM Prescriptions
WHERE prescription_name IN ('49.502(a)(2)'); -- 52.249-1 Alternate I


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159586, prescription_id FROM Prescriptions
WHERE prescription_name IN ('49.504(c)(2)'); -- 52.249-10 Alternate I


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159587, prescription_id FROM Prescriptions
WHERE prescription_name IN ('49.504(c)(3)'); -- 52.249-10 Alternate II


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159588, prescription_id FROM Prescriptions
WHERE prescription_name IN ('49.504(c)(3)'); -- 52.249-10 Alternate III


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159589, prescription_id FROM Prescriptions
WHERE prescription_name IN ('49.502(b)(1)(ii)'); -- 52.249-2 Alternate I


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159590, prescription_id FROM Prescriptions
WHERE prescription_name IN ('49.502(b)(1)(iii)'); -- 52.249-2 Alternate II


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159591, prescription_id FROM Prescriptions
WHERE prescription_name IN ('49.502(b)(1)(iii)'); -- 52.249-2 Alternate III


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159592, prescription_id FROM Prescriptions
WHERE prescription_name IN ('49.502(b)(2)'); -- 52.249-3 Alternate I


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159593, prescription_id FROM Prescriptions
WHERE prescription_name IN ('49.503(a)(2)'); -- 52.249-6 Alternate I


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159594, prescription_id FROM Prescriptions
WHERE prescription_name IN ('49.503(a)(3)'); -- 52.249-6 Alternate II


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159595, prescription_id FROM Prescriptions
WHERE prescription_name IN ('49.503(a)(3)'); -- 52.249-6 Alternate III


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159596, prescription_id FROM Prescriptions
WHERE prescription_name IN ('49.503(a)(4)'); -- 52.249-6 Alternate IV


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159597, prescription_id FROM Prescriptions
WHERE prescription_name IN ('49.503(a)(4)'); -- 52.249-6 Alternate V


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159598, prescription_id FROM Prescriptions
WHERE prescription_name IN ('49.504(a)(2)'); -- 52.249-8 Alternate I


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159599, prescription_id FROM Prescriptions
WHERE prescription_name IN ('50.403-3'); -- 52.250-1 Alternate I


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159600, prescription_id FROM Prescriptions
WHERE prescription_name IN ('50.206(b)(2)'); -- 52.250-3 Alternate I


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159601, prescription_id FROM Prescriptions
WHERE prescription_name IN ('50.206(b)(3)'); -- 52.250-3 Alternate II


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159602, prescription_id FROM Prescriptions
WHERE prescription_name IN ('50.206(c)(2)'); -- 52.250-4 Alternate I


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT 159603, prescription_id FROM Prescriptions
WHERE prescription_name IN ('50.206(c)(3)'); -- 52.250-4 Alternate II

/*
*** End of SQL Scripts at Wed Nov 11 15:24:59 EST 2015 ***
*/
