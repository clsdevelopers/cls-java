/* ===============================================
 * Prescriptions
   =============================================== */
/* ===============================================
 * Questions
   =============================================== */

UPDATE Question_Conditions
SET condition_to_question = '"LEASE AGREEMENT" = [AGREEMENT (INCLUDING BASIC AND LOAN)]' -- '"LEASE AGREEMENT" = [AGREEMENT (INCLUDING BASIC AND LOAN]'
WHERE question_condition_id = 1147; -- [LEASE OPTION]


UPDATE Question_Conditions
SET condition_to_question = '[AGREEMENT (INCLUDING BASIC AND LOAN)] IS NOT NULL OR "REQUIREMENTS" = [TYPE OF INDEFINITE DELIVERY]' -- '[AGREEMENT (INCLUDING BASIC AND LOAN] IS NOT NULL OR "REQUIREMENTS" = [TYPE OF INDEFINITE DELIVERY]'
WHERE question_condition_id = 1152; -- [ORDER VALUE]

