/*
Clause Parser Run at Wed Dec 02 09:21:56 EST 2015
(Without Correction Information)
*/
/* ===============================================
 * Prescriptions
   =============================================== */
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '10.003') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '11.107(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '11.107(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '11.204(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '11.204(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '11.204(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '11.204(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '11.304') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '11.404(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '11.404(a)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '11.404(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '11.503') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '11.503(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '11.503(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '11.503(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '11.604(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '11.604(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '11.703(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '11.703(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '11.703(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '12.301(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '12.301(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '12.301(b)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '12.301(b)(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '12.301(b)(4)(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '12.301(b)(4)(ii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '12.301(c)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '13.302-5(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '13.302-5(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '13.302-5(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '13.302-5(d)(3)(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '13.404') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(c)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(c)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(c)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(f)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(g)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(g)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(h)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(j)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(l)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(m)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(o)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(o)(2)(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(o)(2)(ii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(p)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(p)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(q)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(r)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(s)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(t)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(v)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(w)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-6(x)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-7(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-7(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-7(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-7(c)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '14.201-7(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.209(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.209(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.209(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.209(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.209(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.209(b)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.209(b)(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.209(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.209(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.209(f)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.209(h)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.408(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.408(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.408(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.408(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.408(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.408(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.408(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.408(f)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.408(f)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.408(g)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.408(h)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.408(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.408(j)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.408(k)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.408(l)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.408(m)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.408(n)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.408(n)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '15.408(n)(2)(iii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.105') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.203-4(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.203-4(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.203-4(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.205-4') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.206-4') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.30(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.307(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.307(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.307(a)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.307(a)(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.307(a)(5)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.307(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.307(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.307(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.307(e)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.307(e)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.307(f)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.307(f)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.307(g)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.406') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.406(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.406(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.506') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.506(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.506(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.506(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.506(d)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.506(d)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.506(d)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.506(d)(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.506(d)(5)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.506(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.506(f)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.506(g)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.601(f)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.601(f)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.601(f)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.603-4(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.603-4(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.603-4(b)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '16.603-4(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '17.109') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '17.109(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '17.208(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '17.208(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '17.208(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '17.208(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '17.208(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '17.208(f)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '17.208(g)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.1309(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.1309(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.1309(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.1309(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.1407') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.1506(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.1506(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.309(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.309(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.309(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.309(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.508(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.508(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.508(d)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.508(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.508(f)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.708(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.708(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.708(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.708(b)(1)(iii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.708(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.708(c)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.811-3') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.811-3(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.811-3(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.811-3(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.811-3(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.811-3(d)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.811-3(d)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '19.811-3(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2.201') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '201.602-70') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2012-O0004') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2012-O0007') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2013-00017') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2013-00019') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2013-O0006') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2013-O0010') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2013-O0014') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2013-O0014)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2013-O0016') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2013-O0017') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2013-O0019') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2014-00014') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2014-O0004') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2014-O0005') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2014-O0009') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2014-O0010') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2014-O0016') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2014-O0018') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2014-O0020') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2014-OO0009') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2015-00003') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2015-00009') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2015-00016') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2015-00017') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2015-O0007') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2015-O0010') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2015-O0012') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2015-O0013') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2015-O0016') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2015-OO005') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2016-00001') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2016-00002') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2016-0001') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2016-0002') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2016-0003') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '2016-O0003') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '203.1004(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '203.1004(b)(2)(ii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '203.171-4') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '203.171-4(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '203.570-3') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '203.97') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '204.1105') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '204.1202') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '204.404-70(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '204.404-70(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '204.404-70(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '204.470-3') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '204.7104-1(b)(3)(iv)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '204.7109(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '204.7109(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '204.7304(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '204.7304(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '204.7304(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '204.7304[c]') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '204.7403(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '204.7403(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '204.7403(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '205.47') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '206.302-3-70') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '208.7305(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '209.104-70') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '209.270-5') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '209.409') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '209.470-4(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '209.470-4(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '209.570-4(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '209.570-4(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '209.571-8(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '209.571-8(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '211.002-70') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '211.107') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '211.204(c)(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '211.204(c)(ii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '211.272') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '211.273-4') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '211.274-6(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '211.274-6(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '211.274-6(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '211.275-3') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '211.503') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '211.503(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '212.7103') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '213.106-2-70') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '215.370-3(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '215.370-3(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '215.371-6') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '215.408') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '215.408(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '215.408(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '215.408(3)(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '215.408(3)(ii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '215.408(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '215.408(4)(ii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '215.408(5)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '216.203-4-70(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '216.203-4-70(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '216.203-4-70(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '216.203-4-70(c)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '216.203-4-70(c)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '216.307(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '216.406') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '216.406(e)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '216.506(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '216.506(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '216.506(d)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '216.506(d)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '216.601(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '217.208-70(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '217.208-70(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '217.208-70(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '217.208-70(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '217.7005') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '217.7104(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '217.7303') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '217.7406(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '217.7702') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '219.309(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '219.708(b)(1)(A)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '219.708(b)(1)(B)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '219.811-3') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '219.811-3(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '219.811-3(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1006') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1006(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1006(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1006(c)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1006(c)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1006(e)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1006(e)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1006(e)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1006(e)(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1006(f)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.103-5(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.103-5(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1103') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1207') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1310(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1310(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1310(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1310(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1408(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1408(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1505(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1505(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1605') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1705(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1705(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1705(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1803') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.1906') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.202') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.305') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.407(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.407(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.407(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.407(f)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.407(g)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.407(h)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.505(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.505(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.505(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.505(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.505-(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.505-(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.610') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.810(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.810(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.810(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.810(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.810(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.810(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.810(f)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '22.810(g)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '222.1771') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '222.7004') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '222.7102') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '222.7201(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '222.7201(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '222.7201(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '222.7302') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '222.7405') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '223.303') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '223.370-5') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '223.570-2') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '223.7106') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '223.7106(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '223.7106(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '223.7203') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '223.7306') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225-7011-3') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225-7799-4') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225-7799-4(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225-7799-4(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225-7799-4(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225-7799-4(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225-7799-4(f)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225..1101(10)(i)(C)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(1)(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(1)(ii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(10)(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(10)(i)(A)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(10)(i)(B)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(10)(i)(C)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(10)(i)(D)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(10)(i)(E)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(10)(i)(F)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(2)(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(2)(ii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(2)(iii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(5)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(5)(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(5)(ii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(6)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(6)(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(6)(ii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(7)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(8)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(9)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(9)(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(9)(ii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(9)(iii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(9)(iv)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(9)(v)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1101(9)(vi)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1103(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1103(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1103(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.1103(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.302-6') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.371-5') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.371-5(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.372-2') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7002-2(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7002-3(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7002-3(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7003-5(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7003-5(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7003-5(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7006-4(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7006-4(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7007-3') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7009-5') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7011-3') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7012-3') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7017-4(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7017-4(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7102-4') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7204(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7204(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7307(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7307(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7402-5(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7503(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7503(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7503(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7503(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7503(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7503(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7503(b)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7503(b)(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7605') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7703-4(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7703-4(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7703-4(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7703-4(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.771-5') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.772-5') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7798-6(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7798-6(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7798-6(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7901-4') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7902-5(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '225.7902-5(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '226.104') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.303(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.303(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7009-1') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7009-2') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7009-2(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7009-2(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7009-3(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7009-3(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7009-4(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7009-4(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7009-4(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7009-4(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7009-4(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7010') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7012') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7102-4(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7102-4(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7103-17') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7103-3(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7103-6(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7103-6(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7103-6(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7103-6(e)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7103-6(e)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7103-6(e)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7103-8(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7103-8(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7104(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7104(e)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7104(e)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7104(e)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7104(e)(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7104(e)(5)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7104(f)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7104(f)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7105-2(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7105-3') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7106(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7107-1(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7107-1(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7107-1(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7107-3') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7203-3(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7203-6(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7203-6(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7203-6(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7203-6(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7203-6(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7203-6(f)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '227.7205(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '228.170') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '228.370(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '228.370(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '228.370(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '228.370(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '228.370(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '229.170-4') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '229.402-1') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '229.402-70(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '229.402-70(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '229.402-70(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '229.402-70(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '229.402-70(c)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '229.402-70(c)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '229.402-70(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '229.402-70(e)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '229.402-70(e)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '229.402-70(f)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '229.402-70(g)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '229.402-70(h)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '229.402-70(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '229.402-70(j)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '23.1005') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '23.1005(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '23.1005(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '23.1105') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '23.206') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '23.303') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '23.303(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '23.406(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '23.406(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '23.406(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '23.406(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '23.406(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '23.505') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '23.602') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '23.705(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '23.705(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '23.705(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '23.705(c)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '23.705(d)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '23.705(d)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '23.804(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '23.804(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '23.903') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '231.100-70') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '232.1005-70(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '232.1005-70(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '232.1110') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '232.412-70(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '232.412-70(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '232.412-70(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '232.502-4(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '232.502-4-70(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '232.7004(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '232.7004(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '232.705-70') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '232.7102') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '232.7202') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '232.806(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '232.908') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '233.215-70') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '234.203(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '234.203(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '234.7101(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '234.7101(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '234.7101(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '234.7101(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '234.7101(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '235.070-3') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '235.072') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '235.072(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '235.072(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '235.072(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '235.072(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '235.072(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '235.072(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '236.570(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '236.570(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '236.570(b)(2)(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '236.570(b)(2)(ii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '236.570(b)(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '236.570(b)(5)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '236.570(b)(6)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '236.570(c)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '236.570(c)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '236.570(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '236.609-70') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '236.609-70(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '237-7003(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '237.171-4') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '237.173-5') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '237.270(d)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '237.270(d)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '237.7003(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '237.7003(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '237.7003(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '237.7003(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '237.7101(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '237.7101(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '237.7101(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '237.7101(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '237.7101(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '237.7101(e)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '237.7101(e)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '237.7101(e)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '237.7101(f)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '237.7101(g)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '237.7402') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '237.7603(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '239.7103(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '239.7103(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '239.7306(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '239.7306(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '239.7411(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '239.7411(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '239.7411(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '239.7411(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '239.7603(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '239.7603(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '24.104(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '24.104(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '241.501-70(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '241.501-70(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '242.7001') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '242.7204') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '242.7503') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '243.205-70') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '243.205-71') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '244.305-71') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '244.305-71(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '244.305-71(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '244.403') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '245.107(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '245.107(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '245.107(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '245.107(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '245.107(5)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '246.270-4') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '246.370') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '246.371(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '246.710(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '246.710(1)(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '246.710(1)(ii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '246.710(1)(iii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '246.710(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '246.710(3)(i)(A)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '246.710(3)(i)(B)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '246.710(3)(i)(B)(iii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '246.870-3') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.207') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.207(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.270-4') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.270-4(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.270-4(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.270-4(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.270-4(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.270-4(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.270-4(f)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.270-4(g)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.271-3') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.271-3(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.271-3(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.271-3(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.271-3(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.271-3(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.271-3(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.271-3(g)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.271-3(h)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.271-3(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.271-3(j)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.271-3(k)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.271-3(l)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.271-3(m)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.271-3(n)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.305-70') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.574(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.574(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.574(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.574(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.574(b)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.574(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.574(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.574(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '247.574(f)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '249.501-70') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '249.7003(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.110') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1101(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1101(1)(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1101(1)(ii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1101(6)(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1101(6)(ii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1101(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1101(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1101(b)(1)(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1101(b)(1)(ii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1101(b)(1)(iii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1101(b)(1)(iv)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1101(b)(2)(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1101(b)(2)(ii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1101(b)(2)(iii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1101(b)(2)(iv)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1101(c)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1101(c)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1101(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1101(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1101(f)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1102(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1102(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1102(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1102(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1102(c)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1102(d)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1102(d)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1102(d)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1102(e)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1103') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1103(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1103(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1103(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.1103(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.301-4') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '25.302-6') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '251.107') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '251.205') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '26.104') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '26.206(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '26.206(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '26.206(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '26.304') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '26.404') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.201-2') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.201-2(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.201-2(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.201-2(a)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.201-2(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.201-2(c)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.201-2(c)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.201-2(c)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.201-2(d)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.201-2(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.202-5(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.202-5(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.202-5(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.202-5(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.203-2') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.303(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.303(b)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.303(b)(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.303(b)(5)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.303(b)(6)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.303(b)(7)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.303(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.303(e)(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.303(e)(5)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.409(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.409(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.409(b)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.409(b)(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.409(b)(5)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.409(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.409(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.409(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.409(f)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.409(g)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.409(h)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.409(j)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.409(k)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '27.409(l)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '28.101-2') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '28.103-4') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '28.106-4(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '28.106-4(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '28.1102-3(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '28.1102-3(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '28.203-6') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '28.204-4') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '28.309(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '28.309(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '28.310') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '28.311-1') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '28.312') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '28.313(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '28.313(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '29.401-1') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '29.401-2') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '29.401-3') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '29.401-3(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '29.401-3(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '29.401-4(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '29.402-1(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '29.402-1(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '29.402-2(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '29.402-2(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '3.1004(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '3.1004(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '3.103-1') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '3.104-9(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '3.104-9(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '3.1106') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '3.202') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '3.301-1') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '3.404') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '3.502-3') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '3.503-2') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '3.808(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '3.808(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '3.907-7') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '3.908-9') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '30.201-3') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '30.201-3(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '30.201-3(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '30.201-4(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '30.201-4(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '30.201-4(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '30.201-4(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '30.201-4(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '30.202') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.009-2') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.1005') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.1005(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.1005(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.111(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.111(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.111(a)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.111(a)(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.111(a)(5)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.111(a)(6)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.111(a)(7)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.111(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.111(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.111(c)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.111(c)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.1110(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.1110(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.1110(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.1110(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.1110(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.1110(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.1110(g)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.205(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.206') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.206(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.206(g)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.412(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.412(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.412(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.412(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.412(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.412(f)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.502-3(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.502-3(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.502-3(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.502-4(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.502-4(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.502-4(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.502-4(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.611(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.611(a)&(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.611(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.706-1(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.706-1(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.706-2(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.706-2(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.706-3') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.806(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.806(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.806(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.908(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.908(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.908(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '32.908(c)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '33.106(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '33.106(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '33.215(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '33.215(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '34.104') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '34.203(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '34.203(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '34.203(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.501(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.502') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.503') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.504') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.505') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.506') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.507') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.508') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.509') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.510') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.511') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.512') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.513') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.514') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.515') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.516') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.517') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.518') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.519') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.520') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.521') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.522') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.523') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.609-1(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.609-2(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.609-3') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '36.609-4') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '37.110(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '37.110(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '37.110(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '37.113-2(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '37.113-2(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '37.115-3') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '37.116-2') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '37.304(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '37.304(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '37.304(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '37.403') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '39.106') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '4.103') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '4.1105(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '4.1105(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '4.1202(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '4.1202(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '4.1303') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '4.1403(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '4.1705(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '4.1705(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '4.1804(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '4.1804(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '4.1804(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '4.303') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '4.404(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '4.607(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '4.607(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '4.905') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '41.501(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '41.501(c)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '41.501(c)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '41.501(c)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '41.501(c)(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '41.501(c)(5)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '41.501(d)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '41.501(d)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '41.501(d)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '41.501(d)(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '41.501(d)(5)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '41.501(d)(6)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '41.501(d)(7)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '42.1107(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '42.1305(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '42.1305(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '42.1305(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '42.1305(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '42.703-2(f)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '42.709-6') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '42.802') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '42.903') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '43.107') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '43.205(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '43.205(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '43.205(a)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '43.205(a)(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '43.205(a)(5)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '43.205(a)(6)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '43.205(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '43.205(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '43.205(b)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '43.205(b)(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '43.205(b)(6)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '43.205(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '43.205(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '43.205(f)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '44.204(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '44.204(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '44.204(a)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '44.204(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '44.204(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '44.403') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '45.107(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '45.107(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '45.107(a)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '45.107(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '45.107(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.301') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.302') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.303') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.304') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.305') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.306') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.307(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.308') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.309') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.311') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.312') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.313') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.314') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.315') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.316') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.710(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.710(a)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.710(a)(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.710(a)(5)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.710(a)(6)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.710(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.710(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.710(b)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.710(b)(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.710(c)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.710(c)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.710(c)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.710(c)(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.710(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.710(e)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.710(e)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.805') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.805(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '46.805(a)(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.103-2') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.104-4') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-1(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-1(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-1(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-1(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-1(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-1(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-3(d)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-3(e)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-4(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-4(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-4(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-5(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-5(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-5(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-5(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-5(f)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-6(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-6(c)(5)(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-6(c)(5)(ii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-6(c)(6)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-7(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-7(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-7(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-8(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-8(a)(2)(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-8(a)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-8(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.207-9(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.208-2') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.303-1(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.303-10(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.303-11(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.303-12(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.303-13(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.303-14(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.303-15(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.303-16(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.303-17(f)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.303-2(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.303-3(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.303-4(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.303-5(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.303-6(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.303-7(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.303-8(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.303-9(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.304-7(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.305-12(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.305-13(a)(3)(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.305-14(b)(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.305-15(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.305-16(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.305-16(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.305-16(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.305-16(d)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.305-17') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.305-2(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.305-3(b)(4)(ii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.305-3(f)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.305-4(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.305-5(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.305-5(c)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.305-6(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.305-6(e)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.305-6(e)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.305-6(e)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.305-6(f)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.305-9(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.405') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.507(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.507(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '47.507(a)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '48.201') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '48.201(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '48.201(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '48.201(e)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '48.201(f)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '48.202') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '49.502(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '49.502(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '49.502(b)(1)(i)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '49.502(b)(1)(ii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '49.502(b)(1)(iii)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '49.502(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '49.502(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '49.502(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '49.503(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '49.503(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '49.503(a)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '49.503(a)(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '49.503(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '49.504(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '49.504(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '49.504(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '49.504(c)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '49.504(c)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '49.504(c)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '49.505(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '49.505(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '50.104-4') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '50.206(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '50.206(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '50.206(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '50.206(b)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '50.206(c)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '50.206(c)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '50.206(c)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '50.206(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '50.403-3') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '51.107') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '51.205') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '52.107(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '52.107(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '52.107(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '52.107(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '52.107(e)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '52.107(f)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '53.111') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '7.203') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '7.305(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '7.305(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '7.305(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '7.404') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '8.005') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '8.1104(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '8.1104(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '8.1104(c)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '8.1104(d)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '8.505') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '9.104-7(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '9.104-7(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '9.104-7(c)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '9.108-5(a)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '9.108-5(b)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '9.206-2') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '9.308-1(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '9.308-1(a)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '9.308-1(a)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '9.308-1(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '9.308-1(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '9.308-1(b)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '9.308-2(a)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '9.308-2(a)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '9.308-2(b)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '9.308-2(b)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '9.308-2(b)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, '9.409') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, 'NOTAPPLICABLE') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, 'PGI 204.7108(d)(1)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, 'PGI 204.7108(d)(10)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, 'PGI 204.7108(d)(11)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, 'PGI 204.7108(d)(2)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, 'PGI 204.7108(d)(3)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, 'PGI 204.7108(d)(4)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, 'PGI 204.7108(d)(5)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, 'PGI 204.7108(d)(6)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, 'PGI 204.7108(d)(7)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, 'PGI 204.7108(d)(8)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;
INSERT INTO Prescriptions (regulation_id, prescription_name) VALUES (1, 'PGI 204.7108(d)(9)') ON DUPLICATE KEY UPDATE regulation_id=regulation_id;

UPDATE Prescriptions
JOIN (
	SELECT
		prescription_id,
		concat('http://www.ecfr.gov/cgi-bin/text-idx?node=se48.', volume , '.', part, '_1', subpart, replace(suffix, '-', '_6')) uri
	FROM (
		SELECT
			prescription_id,
			prescription_name,
			subpart,
			part,
			case when suffix like '%(%' then left(suffix,locate('(',suffix)-1) else suffix end suffix,
			case when part regexp '^[0-9]+$'
				then
					case
						when cast(part as UNSIGNED) >= '1' and cast(part as UNSIGNED) <= '51' then 1
						when cast(part as UNSIGNED) >= '52' and cast(part as UNSIGNED) <= '99' then 2
						when cast(part as UNSIGNED) >= '200' and cast(part as UNSIGNED) <= '299' then 3
					end
			end volume
		FROM (
			SELECT
				prescription_id,
				prescription_name,
				substring_index(substring_index(prescription_name,'(', 1),'.',1) part,
				substring_index(substring_index(substring_index(prescription_name,'(', 1),'.',-1),'-',1) subpart,
				case
					when locate('-',prescription_name) != 0
						then right(prescription_name, length(prescription_name) - locate('-',prescription_name)+1)
					else ''
				end suffix
			FROM Prescriptions
		) Prescriptions
	) Prescriptions
) sub ON (sub.prescription_id = Prescriptions.prescription_id)
SET Prescriptions.prescription_url = ifNull(sub.uri,'')
WHERE ifNull(prescription_url, '') = '';

/* ===============================================
 * Questions
   =============================================== */


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT Question_Choce_Id, prescription_id FROM Question_Choices, Prescriptions
  WHERE prescription_name IN ('32.206(b)(2)') 
  AND choice_text = 'SIMPLIFIED ACQUISITION PROCEDURES (FAR PART 13)' AND  question_id IN (SELECT question_id FROM Questions WHERE clause_version_id = 9 AND question_code = '[PROCEDURES]'); -- [PROCEDURES] 'SIMPLIFIED ACQUISITION PROCEDURES (FAR PART 13)'


DELETE FROM Question_Choices WHERE question_id = 29894 AND choice_text = 'NATIONAL SPACE AND AERONAUTICS ADMINISTRATION'; -- [REQUIRING ACTIVITY]
INSERT INTO Question_Choices( question_id, choice_text, prompt_after_selection)
SELECT question_id, 'NATIONAL AERONAUTICS AND SPACE ADMINISTRATION', NULL FROM Questions WHERE clause_version_id = 9 AND question_code = '[REQUIRING ACTIVITY]'; -- [REQUIRING ACTIVITY]

UPDATE Questions
SET question_text = 'DO YOU ANTICIPATE OR HAS IT BEEN DETERMINED THAT "ADEQUATE PRICE COMPETITION" EXISTS?' -- 'DO YOU ANTICIPATE OR HAS IT BEEN DETERMINED THAT "ADEQUATE PRICE COMPETITION" EXITS?'
WHERE question_code = '[ADEQUATE PRICE COMPETITION]' AND clause_version_id = 9; -- [ADEQUATE PRICE COMPETITION]


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT Question_Choce_Id, prescription_id FROM Question_Choices, Prescriptions
  WHERE prescription_name IN ('NOTAPPLICABLE') 
  AND choice_text = 'NOT APPLICABLE' AND  question_id IN (SELECT question_id FROM Questions WHERE clause_version_id = 9 AND question_code = '[SDB MECHANISMS ON A REGIONAL BASIS]'); -- [SDB MECHANISMS ON A REGIONAL BASIS] 'NOT APPLICABLE'

UPDATE Question_Conditions
SET original_condition_text = 'ASK ONLY IF THE USER HAS SELECTED "SMALL BUSINESS PROGRAM" UNDER "PROCEDURES"' -- 'ASK ONLY IF THE USER HAS SELECTED "SMALL BUSINESS PROGRAM" UNDER "PROCEDURES" AND DO NOT ASK IF THE USER SELECTED "COMMERCIAL PROCEDURES (FAR PART 12)" UNDER "PROCEDURES"'
, condition_to_question = '"SMALL BUSINESS PROGRAM" = [PROCEDURES]' -- '"SMALL BUSINESS PROGRAM" = [PROCEDURES] AND "COMMERCIAL PROCEDURES (FAR PART 12)" <> [PROCEDURES]'
WHERE question_id IN 
  (SELECT question_id FROM Questions WHERE question_code = '[SMALL BUSINESS SIZE STANDARD]' AND clause_version_id = 9); -- [SMALL BUSINESS SIZE STANDARD]

UPDATE Questions
SET question_text = 'IS THE TOTAL VALUE OF ORDERS IN ANY CALENDAR YEAR EXPECTED TO EXCEED THE SIMPLIFIED ACQUISITION THRESHOLD?' -- 'IS THE TOTAL VALUE OF ORDERS IS ANY CALENDAR YEAR EXPECTED TO EXCEED THE SIMPLIFIED ACQUISITION THRESHOLD?'
WHERE question_code = '[INDEFINITE QUANTITY TOTAL ORDER VALUE]' AND clause_version_id = 9; -- [INDEFINITE QUANTITY TOTAL ORDER VALUE]


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT Question_Choce_Id, prescription_id FROM Question_Choices, Prescriptions
  WHERE prescription_name IN ('4.404(a)') 
  AND choice_text = 'THE AWARD INVOLVES UNCLASSIFIED BUT SENSITIVE INFORMATION' AND  question_id IN (SELECT question_id FROM Questions WHERE clause_version_id = 9 AND question_code = '[SECURITY REQUIREMENTS]'); -- [SECURITY REQUIREMENTS] 'THE AWARD INVOLVES UNCLASSIFIED BUT SENSITIVE INFORMATION'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT Question_Choce_Id, prescription_id FROM Question_Choices, Prescriptions
  WHERE prescription_name IN ('217.208-70(a)(1)','217.208-70(a)(2)') 
  AND choice_text = 'FOR ESTABLISHMENT OR REPLENISHMENT OF DOD INVENTORIES OR STOCKS' AND  question_id IN (SELECT question_id FROM Questions WHERE clause_version_id = 9 AND question_code = '[PURPOSE OF PROCUREMENT]'); -- [PURPOSE OF PROCUREMENT] 'FOR ESTABLISHMENT OR REPLENISHMENT OF DOD INVENTORIES OR STOCKS'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT Question_Choce_Id, prescription_id FROM Question_Choices, Prescriptions
  WHERE prescription_name IN ('26.404') 
  AND choice_text = 'FOR USE IN UNITED STATES (50 STATES PLUS DISTRICT OF COLUMBIA)' AND  question_id IN (SELECT question_id FROM Questions WHERE clause_version_id = 9 AND question_code = '[PLACE OF USE]'); -- [PLACE OF USE] 'FOR USE IN UNITED STATES (50 STATES PLUS DISTRICT OF COLUMBIA)'

UPDATE Question_Conditions
SET original_condition_text = 'ASK ONLY AFTER USER SELECTS "YES" OR "NO" UNDER "SAFETY ACT APPLICABILITY'' AND DO NOT ASK IF THE USER SELECTED "COMMERCIAL PROCEDURES (FAR PART 12)" UNDER "PROCEDURES"' -- 'ASK ONLY AFTER "SAFETY ACT APPLICABILITY" IS COMPLETED AND DO NOT ASK IF THE USER SELECTED "COMMERCIAL PROCEDURES (FAR PART 12)" UNDER "PROCEDURES"'
, condition_to_question = '("YES" = [SAFETY ACT APPLICABILITY''] OR "NO" = [SAFETY ACT APPLICABILITY'']) AND "COMMERCIAL PROCEDURES (FAR PART 12)" <> [PROCEDURES]' -- '[SAFETY ACT APPLICABILITY] IS NOT NULL AND "COMMERCIAL PROCEDURES (FAR PART 12)" <> [PROCEDURES]'
WHERE question_id IN 
  (SELECT question_id FROM Questions WHERE question_code = '[SAFETY ACT STATEMENTS]' AND clause_version_id = 9); -- [SAFETY ACT STATEMENTS]

UPDATE Questions
SET question_text = 'IF THIS PROCUREMENT IS PART OF A SYSTEM OR PROGRAM, PLEASE SELECT THE APPROPRIATE DESIGNATION, SELECT EACH DESCRIPTION THAT WOULD APPLY.' -- 'IF THIS PROCUREMENT IS PART OF A SYSTEM OR PROGRAM, PLEASE SELECT THE APPROPRIATE DESIGNATION?'
WHERE question_code = '[TYPE OF SYSTEM]' AND clause_version_id = 9; -- [TYPE OF SYSTEM]

INSERT INTO Questions (clause_version_id, question_code, question_type, question_group, question_text)
VALUES (9, '[COVERED SYSTEM]', 'M', 'D', 'IS THE INFORMATION TECHNOLOGY A COVERED SYSTEM, A PART OF A COVERED SYSTEM OR IN SUPPORT OF A COVERED SYSTEM, AS DEFINED AT 239.7301?');
INSERT INTO Question_Conditions (question_condition_sequence, question_id, parent_question_id, default_baseline, original_condition_text, condition_to_question)
SELECT 1075, question_id, NULL , 0
, 'ASK ONLY IF THE USER SELECTS "INFORMATION TECHNOLOGY" UNDER "SUPPLIES" OR "INFORMATION TECHNOLOGY SERVICES" UNDER "SERVICES"'
, '"INFORMATION TECHNOLOGY" = [SUPPLIES] OR "INFORMATION TECHNOLOGY SERVICES" = [SERVICES]' FROM Questions WHERE clause_version_id = 9 AND question_code = '[COVERED SYSTEM]';
INSERT INTO Question_Choices( question_id, choice_text, prompt_after_selection)
SELECT question_id, 'YES', NULL FROM Questions WHERE clause_version_id = 9 AND question_code = '[COVERED SYSTEM]'; -- [COVERED SYSTEM]
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT Question_Choce_Id, prescription_id FROM Question_Choices, Prescriptions
  WHERE prescription_name IN ('239.7306(a)','239.7306(b)') 
  AND choice_text = 'YES' AND  question_id IN (SELECT question_id FROM Questions WHERE clause_version_id = 9 AND question_code = '[COVERED SYSTEM]'); -- [COVERED SYSTEM] 'YES'
INSERT INTO Question_Choices( question_id, choice_text, prompt_after_selection)
SELECT question_id, 'NO', NULL FROM Questions WHERE clause_version_id = 9 AND question_code = '[COVERED SYSTEM]'; -- [COVERED SYSTEM]


DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'YES' INNER JOIN Questions Q  ON Q.question_code = '[COMMERCIAL SERVICES]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 9;

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT Question_Choce_Id, prescription_id FROM Question_Choices, Prescriptions
  WHERE prescription_name IN ('32.206(g)') 
  AND choice_text = 'YES' AND  question_id IN (SELECT question_id FROM Questions WHERE clause_version_id = 9 AND question_code = '[COMMERCIAL SERVICES]'); -- [COMMERCIAL SERVICES] 'YES'


DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'FUNDS APPROPRIATED BY THE DEPARTMENT OF DEFENSE APPROPRIATIONS ACT, 2014 AND BY THE MILITARY CONSTRUCTION AND VETERANS AFFAIRS AND RELATED AGENCIES APPROPRIATIONS ACT, 2014 (PUBL L. 113-76, DIVISIONS C AND J)' INNER JOIN Questions Q  ON Q.question_code = '[LEGISLATIVE SOURCE OF FUNDS]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 9;

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT Question_Choce_Id, prescription_id FROM Question_Choices, Prescriptions
  WHERE prescription_name IN ('2014-O0009') 
  AND choice_text = 'FUNDS APPROPRIATED BY THE DEPARTMENT OF DEFENSE APPROPRIATIONS ACT, 2014 AND BY THE MILITARY CONSTRUCTION AND VETERANS AFFAIRS AND RELATED AGENCIES APPROPRIATIONS ACT, 2014 (PUBL L. 113-76, DIVISIONS C AND J)' AND  question_id IN (SELECT question_id FROM Questions WHERE clause_version_id = 9 AND question_code = '[LEGISLATIVE SOURCE OF FUNDS]'); -- [LEGISLATIVE SOURCE OF FUNDS] 'FUNDS APPROPRIATED BY THE DEPARTMENT OF DEFENSE APPROPRIATIONS ACT, 2014 AND BY THE MILITARY CONSTRUCTION AND VETERANS AFFAIRS AND RELATED AGENCIES APPROPRIATIONS ACT, 2014 (PUBL L. 113-76, DIVISIONS C AND J)'
INSERT INTO Question_Choices( question_id, choice_text, prompt_after_selection)
SELECT question_id, 'FUNDS MADE AVAILABLE BY THE CONTINUING APPROPRIATIONS ACT, 2016 (Pub. L 114-53) OR ANY OTHER FY2016 APPROPRIATIONS ACT THAT EXTENDS TO FY2016 FUNDS THE SAME PROHIBITIONS AS CONTAINED IN SECTION 743 OF DIVISION E, TITLE VII, OF THE COLSOLIDATED AND FURTHER CONTINUING APPROPRIATIONS ACT, 2015 (Pub. L. 113-235)', NULL FROM Questions WHERE clause_version_id = 9 AND question_code = '[LEGISLATIVE SOURCE OF FUNDS]'; -- [LEGISLATIVE SOURCE OF FUNDS]
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT Question_Choce_Id, prescription_id FROM Question_Choices, Prescriptions
  WHERE prescription_name IN ('2016-0003') 
  AND choice_text = 'FUNDS MADE AVAILABLE BY THE CONTINUING APPROPRIATIONS ACT, 2016 (Pub. L 114-53) OR ANY OTHER FY2016 APPROPRIATIONS ACT THAT EXTENDS TO FY2016 FUNDS THE SAME PROHIBITIONS AS CONTAINED IN SECTION 743 OF DIVISION E, TITLE VII, OF THE COLSOLIDATED AND FURTHER CONTINUING APPROPRIATIONS ACT, 2015 (Pub. L. 113-235)' AND  question_id IN (SELECT question_id FROM Questions WHERE clause_version_id = 9 AND question_code = '[LEGISLATIVE SOURCE OF FUNDS]'); -- [LEGISLATIVE SOURCE OF FUNDS] 'FUNDS MADE AVAILABLE BY THE CONTINUING APPROPRIATIONS ACT, 2016 (Pub. L 114-53) OR ANY OTHER FY2016 APPROPRIATIONS ACT THAT EXTENDS TO FY2016 FUNDS THE SAME PROHIBITIONS AS CONTAINED IN SECTION 743 OF DIVISION E, TITLE VII, OF THE COLSOLIDATED AND FURTHER CONTINUING APPROPRIATIONS ACT, 2015 (Pub. L. 113-235)'


DELETE FROM Question_Choices WHERE question_id = 30050 AND choice_text = 'NONE OF THE ABOVE'; -- [ACRN CONTRACT WIDE]


DELETE FROM Question_Choices WHERE question_id = 30052 AND choice_text = 'SBA WILL MAKE ADVANCE PAYMENTS TO ITS 8(a) CONTRACTOR'; -- [ADVANCED PAYMENT CONDITIONS]


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT Question_Choce_Id, prescription_id FROM Question_Choices, Prescriptions
  WHERE prescription_name IN ('22.1310(a)(2)') 
  AND choice_text = '22.1305 - WAIVER OF ALL THE TERMS OF 52.222-35, "EQUAL OPPORTUNITY FOR VETERANS"' AND  question_id IN (SELECT question_id FROM Questions WHERE clause_version_id = 9 AND question_code = '[WAIVERS]'); -- [WAIVERS] '22.1305 - WAIVER OF ALL THE TERMS OF 52.222-35, "EQUAL OPPORTUNITY FOR VETERANS"'

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT Question_Choce_Id, prescription_id FROM Question_Choices, Prescriptions
  WHERE prescription_name IN ('22.1408(a)') 
  AND choice_text = '22.1403 - WAIVER OF ONE OR MORE (BUT NOT ALL) OF THE TERMS OF 52.222-36, "AFFIRMATIVE ACTION FOR WORKERS WITH DISABILITIES"' AND  question_id IN (SELECT question_id FROM Questions WHERE clause_version_id = 9 AND question_code = '[WAIVERS]'); -- [WAIVERS] '22.1403 - WAIVER OF ONE OR MORE (BUT NOT ALL) OF THE TERMS OF 52.222-36, "AFFIRMATIVE ACTION FOR WORKERS WITH DISABILITIES"'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT Question_Choce_Id, prescription_id FROM Question_Choices, Prescriptions
  WHERE prescription_name IN ('22.1006(a)') 
  AND choice_text = '22.1003-3/4 -- EXEMPT FROM APPLICATION OF THE SERVICE CONTRACT ACT' AND  question_id IN (SELECT question_id FROM Questions WHERE clause_version_id = 9 AND question_code = '[EXEMPTIONS]'); -- [EXEMPTIONS] '22.1003-3/4 -- EXEMPT FROM APPLICATION OF THE SERVICE CONTRACT ACT'


DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = '23.704(a) - EXCEPTION TO EPEAT REGISTERED ELECTRONIC PRODUCTS' INNER JOIN Questions Q  ON Q.question_code = '[EXCEPTIONS]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 9;
INSERT INTO Question_Choices( question_id, choice_text, prompt_after_selection)
SELECT question_id, '3.704(a) - EXCEPTION TO EPEAT REGISTERED ELECTRONIC PRODUCTS', NULL FROM Questions WHERE clause_version_id = 9 AND question_code = '[EXCEPTIONS]'; -- [EXCEPTIONS]
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT Question_Choce_Id, prescription_id FROM Question_Choices, Prescriptions
  WHERE prescription_name IN ('23.705(b)(1)','23.705(b)(2)','23.705(c)(1)','23.705(d)(2)') 
  AND choice_text = '3.704(a) - EXCEPTION TO EPEAT REGISTERED ELECTRONIC PRODUCTS' AND  question_id IN (SELECT question_id FROM Questions WHERE clause_version_id = 9 AND question_code = '[EXCEPTIONS]'); -- [EXCEPTIONS] '3.704(a) - EXCEPTION TO EPEAT REGISTERED ELECTRONIC PRODUCTS'


DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = '252.227-7008 -- COMPUTATION OF ROYALTIES' INNER JOIN Questions Q  ON Q.question_code = '[PATENT COUNCIL RECOMMENDED]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 9;

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT Question_Choce_Id, prescription_id FROM Question_Choices, Prescriptions
  WHERE prescription_name IN ('227.7009-4(c)') 
  AND choice_text = '252.227-7008 -- COMPUTATION OF ROYALTIES' AND  question_id IN (SELECT question_id FROM Questions WHERE clause_version_id = 9 AND question_code = '[PATENT COUNCIL RECOMMENDED]'); -- [PATENT COUNCIL RECOMMENDED] '252.227-7008 -- COMPUTATION OF ROYALTIES'


DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'TOTAL SMALL BUSINESS SET-ASIDE' INNER JOIN Questions Q  ON Q.question_code = '[SET-ASIDE]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 9;

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT Question_Choce_Id, prescription_id FROM Question_Choices, Prescriptions
  WHERE prescription_name IN ('19.508(c)') 
  AND choice_text = 'TOTAL SMALL BUSINESS SET-ASIDE' AND  question_id IN (SELECT question_id FROM Questions WHERE clause_version_id = 9 AND question_code = '[SET-ASIDE]'); -- [SET-ASIDE] 'TOTAL SMALL BUSINESS SET-ASIDE'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT Question_Choce_Id, prescription_id FROM Question_Choices, Prescriptions
  WHERE prescription_name IN ('4.1105(a)(1)') 
  AND choice_text = 'SELF-EMPLOYED INDIVIDUAL' AND  question_id IN (SELECT question_id FROM Questions WHERE clause_version_id = 9 AND question_code = '[MISCELLANEOUS SOURCE]'); -- [MISCELLANEOUS SOURCE] 'SELF-EMPLOYED INDIVIDUAL'


INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT Question_Choce_Id, prescription_id FROM Question_Choices, Prescriptions
  WHERE prescription_name IN ('23.705(b)(2)') 
  AND choice_text = 'IMAGING EQUIPMENT' AND  question_id IN (SELECT question_id FROM Questions WHERE clause_version_id = 9 AND question_code = '[SUPPLIES]'); -- [SUPPLIES] 'IMAGING EQUIPMENT'

DELETE FROM Question_Choices WHERE question_id = 30097 AND choice_text = 'INFORMATION TECHNOLOGY'; -- [SUPPLIES]

DELETE FROM Question_Choices WHERE question_id = 30097 AND choice_text = 'ALTHOUGH NOT LISTED ABOVE, OTHER SUPPLY TYPES ARE BEING PROCURED'; -- [SUPPLIES]


INSERT INTO Question_Choices( question_id, choice_text, prompt_after_selection)
SELECT question_id, 'INFORMATION TECHNOLOGY SERVICES', NULL FROM Questions WHERE clause_version_id = 9 AND question_code = '[SERVICES]'; -- [SERVICES]
INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT Question_Choce_Id, prescription_id FROM Question_Choices, Prescriptions
  WHERE prescription_name IN ('239.7603(a)','239.7603(b)') 
  AND choice_text = 'INFORMATION TECHNOLOGY SERVICES' AND  question_id IN (SELECT question_id FROM Questions WHERE clause_version_id = 9 AND question_code = '[SERVICES]'); -- [SERVICES] 'INFORMATION TECHNOLOGY SERVICES'


DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'INFORMATION TECHNOLOGY' INNER JOIN Questions Q  ON Q.question_code = '[COMPUTERS/INFORMATION TECHNOLOGY]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 9;

DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'INFORMATION TECHNOLOGY' INNER JOIN Questions Q  ON Q.question_code = '[COMPUTERS/INFORMATION TECHNOLOGY]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 9;


INSERT INTO Question_Choices( question_id, choice_text, prompt_after_selection)
SELECT question_id, 'NONE OF THE ABOVE', NULL FROM Questions WHERE clause_version_id = 9 AND question_code = '[MISCELLANEOUS END ITEMS]'; -- [MISCELLANEOUS END ITEMS]


DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'TRANSPORTATION OF HOUSEHOLD GOODS' INNER JOIN Questions Q  ON Q.question_code = '[TRANSPORTATION/STORAGE SERVICES]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 9;

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT Question_Choce_Id, prescription_id FROM Question_Choices, Prescriptions
  WHERE prescription_name IN ('47.207-5(c)') 
  AND choice_text = 'TRANSPORTATION OF HOUSEHOLD GOODS' AND  question_id IN (SELECT question_id FROM Questions WHERE clause_version_id = 9 AND question_code = '[TRANSPORTATION/STORAGE SERVICES]'); -- [TRANSPORTATION/STORAGE SERVICES] 'TRANSPORTATION OF HOUSEHOLD GOODS'

DELETE Question_Choice_Prescriptions FROM Question_Choice_Prescriptions INNER JOIN Question_Choices QC  ON QC.question_choce_id = Question_Choice_Prescriptions.Question_Choce_Id
AND QC.choice_text = 'RELOCATION OF FEDERAL OFFICES' INNER JOIN Questions Q  ON Q.question_code = '[TRANSPORTATION/STORAGE SERVICES]' AND Q.question_id = QC.question_id AND Q.clause_version_id = 9;

INSERT INTO Question_Choice_Prescriptions (Question_Choce_Id, prescription_id)
SELECT Question_Choce_Id, prescription_id FROM Question_Choices, Prescriptions
  WHERE prescription_name IN ('47.207-5(c)') 
  AND choice_text = 'RELOCATION OF FEDERAL OFFICES' AND  question_id IN (SELECT question_id FROM Questions WHERE clause_version_id = 9 AND question_code = '[TRANSPORTATION/STORAGE SERVICES]'); -- [TRANSPORTATION/STORAGE SERVICES] 'RELOCATION OF FEDERAL OFFICES'


INSERT INTO Question_Choices( question_id, choice_text, prompt_after_selection)
SELECT question_id, 'NONE OF THE ABOVE', NULL FROM Questions WHERE clause_version_id = 9 AND question_code = '[THE SUPPLIES ARE COMMERCIAL ITEMS]'; -- [THE SUPPLIES ARE COMMERCIAL ITEMS]
/*
<h2>252.225-7998 (DEVIATION 2014-00014)</h2>
(B) Clause name not found: {252.225-7999}
<pre>(B) 252.225-7999 IS: INCLUDED</pre>
*//*
<h2>52.204-7</h2>
(O) Value not found: "UNITED STATES OUTLYING AREA" in [PLACE OF PERFORMANCE] choices. Available values: <ul><li>"ASSOCIATED TERRITORIAL WATERS AND AIR SPACE"</li><li>"OUTER CONTINENTAL SHELF"</li><li>"DESIGNATED OPERATIONAL AREA"</li><li>"FOREIGN COUNTRY"</li><li>"UNITED STATES OUTLYING AREAS"</li><li>"UNITED STATES (50 STATES PLUS DISTRICT OF COLUMBIA)"</li><li>"UNITED STATES CENTRAL COMMAND (USCENTCOM) AREA OF RESPONSIBILITY"</li><li>"UNITED STATES EUROPEAN COMMAND (USEUCOM) THEATER OF OPERATION"</li><li>"UNITED STATES AFRICA COMMAND (USAFRICOM) THEATER OF OPERATION"</li><li>"UNITED STATES SOUTHERN COMMAND (USSOUTHCOM) THEATER OF OPERATION"</li><li>"UNITED STATES PACIFIC COMMAND (USPACOM) THEATER OF OPERATION"</li><li>"WAKE ISLAND"</li><li>"SPECIFIED BY THE GOVERNMENT"</li><li>"UNKNOWN"</li><li>"NONE OF THE ABOVE LISTED PLACES OF PERFORMANCE"</li></ul>
<pre>(O) PLACE OF PERFORMANCE IS: UNITED STATES OUTLYING AREA</pre><br />
(R) Value not found: "FOREIGN" in [BUSINESS TYPE] choices. Available values: <ul><li>"SMALL BUSINESS/ SOCIOECONOMIC SOURCE"</li><li>"GOVERNMENT SOURCE"</li><li>"FOREIGN SOURCE"</li><li>"EDUCATIONAL SOURCE"</li><li>"MISCELLANEOUS SOURCE"</li><li>"UNKNOWN - COMPETITION"</li><li>"NONE OF THE LISTED SOURCES"</li></ul>
<pre>(R) BUSINESS TYPE IS: FOREIGN</pre>
*//*
<h2>52.211-7</h2>
No identifier
<pre>(ADDITIONAL DIRECTIONS: IF THE REQUIRING ACTIVITY IS: DEPARTMENT OF DEFENSE DO NOT USE THIS CLAUSE</pre>
*//*
<h2>52.226-6</h2>
(F) No " IS:" found
<pre>(F) FOR USE IN UNITED STATES (50 STATES PLUS DISTRICT OF COLUMBIA)</pre>
*//*
<h2>52.247-5</h2>
(C) [TRANSPORTATION REQUIREMENTS] Question Name not found
<pre>(C) TRANSPORTATION REQUIREMENTS IS: CONTRACTOR RESPONSIBLE FOR FAMILIARIZATION WITH CONDITIONS UNDER WHICH AND WHERE THE SERVICES ARE TO BE PERFORMED</pre>
*//*
<h2>52.250-5</h2>
(G) Value not found: "DHS HAS NOT ISSUED A SAFETY ACT DESIGNATION OR CERTIFICATION TO SUCCESSFUL CONTRACTOR BEFORE CONTRACT AWARD" in [SAFETY ACT STATEMENTS] choices. Available values: <ul><li>"DHS DENIED APPROVAL OF PRE-APPLICATION DESIGNATION NOTICE"</li><li>"DHS HAS NOT ISSUED A SAFETY ACT DESIGNATION OR CERTIFICATION TO SUCCESSFUL OFFEROR BEFORE CONTRACT AWARD"</li><li>"DHS HAS ISSUED A PRE-QUALIFICATION DESIGNATION NOTICE"</li><li>"DHS HAS ISSUED A BLOCK DESIGNATION/CERTIFICATION FOR SOLICITED TECHNOLOGIES"</li><li>"OFFERS ARE CONTINGENT UPON SAFETY ACT DESIGNATION BEING AUTHORIZED"</li><li>"OFFERS PRESUMING SAFETY ACT DESIGNATION OR CERTIFICATION AUTHORIZATION"</li><li>"NONE OF THE ABOVE"</li></ul>
<pre>(G) SAFETY ACT STATEMENTS IS: DHS HAS NOT ISSUED A SAFETY ACT DESIGNATION OR CERTIFICATION TO SUCCESSFUL CONTRACTOR BEFORE CONTRACT AWARD</pre>
*//*
<h2>52.215-23 Alternate I</h2>
(Q) [EXCESSIVE PASS-THROUGH CHARGES]Found multiple questions for choice value of "NO":<br>
 [WAIVER OF NON MANUFACTURING RULE] [FEDERAL PRISON INDUSTRY] [UK OFFER] [DERIVATIVES ANTICIPATED] [ADEQUATE PRICE COMPETITION] [SIMPLIFIED ACQUISITION TEST PROGRAM] [SDB MECHANISMS ON A REGIONAL BASIS] [SOLICITATION TIME] [LEASE OPTION] [INDEFINITE QUANTITY TOTAL ORDER VALUE] [EXCEEDS GOVERNMENT CAPABILITY] [PAYMENT OF FIXED CHARGES] [ORDER VALUE] [TASK ORDER] [ORDER RATING] [PROVISIONING ORDER] [SAFETY ACT APPLICABILITY] [CAR] [SAM EXEMPTION] [GENERIC DUNS] [HUBZONE 50% WAIVER] [TRANSLATION TO ANOTHER LANGUAGE] [ACTIVE FEDERAL CONTRACTS] [CONTRACTOR PERFORM ON BEHALF OF FOREIGN COUNTRY] [OPTIONS] [EPEAT SILVER/GOLD] [BRAND NAME BASIS] [HAZARDOUS DATA SHEETS] [INCLUSION OF SERVICES] [RECOVERED MATERIAL ESTIMATES] [TREATY ELIGIBLE SUPPLY] [PROPERTY FOR EXCHANGE] [TRADE AGREEMENT APPLICABILITY] [PRE-CONSTRUCTION CONFERENCE] [SITE VISIT] [R&D TECHNOLOGIES] [WITHHOLDING ALLOWABLE COST] [COVERED SYSTEM] [SEPARATELY PRICED ITEMS] [PORT OF ENTRY REQUIREMENTS] [INTRASTATE MOVEMENT] [BILLS OF LADING] [SERVICE CONFIGURED FOR OCCUPANCY] [COMMERCIAL SERVICES] [SERVICE CONTRACT REPORTING] [OSD APPROVAL FOR DATA SYSTEM] [SWING FROM TARGET FEE] [SMALL BUSINESS AWARD FEE] [MAJOR DESIGN OR DEVELOPMENT] [PCO EPA DETERMINATION] [INTEREST ON PARTIAL PAYMENTS] [TERMINATION] [TECHNICAL CHANGES] [DD FORM 1494] [DRUG FREE WORKFORCE REQUIRED] [END ITEM MARKING] [PRESERVATION AND PACKING SPECIFICATIONS] [USE OF WARRANTY] [OUTSIDE U.S.] [CONTINUED PERFORMANCE] [LIQUIDATED DAMAGES] [SEPARATE DELIVERABLES] [PACE OF WORK] [MILITARY TECHNICAL AGREEMENT] [UNEMPLOYMENT RATE] [WITHIN CONSIGNEE'S PREMISES] [CARGO PREFERENCE ACT] [RAPID LIQUIDATION] [CONTRACT PRIOR TO FUNDS] [BASE, POST, CAMP OR STATION] [ANNUAL FUNDS EXTENDED] [LIABILITY FOR SPECIAL COSTS] [PURCHASE CARD] [NO TIME TO PROCESS BUY AMERICAN DETERMINATION] [DISPUTES ACT] [EXCESS PASS-THROUGH CHARGES] [UCA] [INAPPROPRIATE CONTINGENCY] [CURRENCY]
<pre>(Q) EXCESSIVE PASS-THROUGH CHARGES IS: NO</pre>
*/
-- ProduceClause.resolveIssues() Summary: Total(1232); Merged(18); Corrected(352); Error(8)
/* ===============================================
 * Clause
   =============================================== */

UPDATE Clauses
SET clause_conditions = '(A) DOCUMENT TYPE IS: SOLICITATION' -- '(A) DOCUMENT TYPE IS: SOLICITATION (B) TYPE OF ORDER IS: TASK ORDER (C) TYPE OF ORDER IS: DELIVERY ORDER'
, clause_rule = 'A' -- 'A AND (B OR C)'
WHERE clause_version_id = 9 AND clause_name = '252.203-7005';


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT clause_id, prescription_id FROM Prescriptions, Clauses
WHERE prescription_name IN ('2016-00001') 
AND clause_id IN (SELECT clause_id FROM Clauses WHERE clause_name = '252.204-7012' AND clause_version_id = 9); 

UPDATE Clauses
SET clause_data = '<p>(a) The Government intends to furnish precious metals required in the manufacture of items to be delivered under the contract if the Contracting Officer determines it to be in the Government''s best interest. The use of Government-furnished silver is mandatory when the quantity required is one hundred troy ounces or more. The precious metal(s) will be furnished pursuant to the Government Furnished Property clause of the contract.</p><p>(b) The Offeror shall cite the type (silver, gold, platinum, palladium, iridium, rhodium, and ruthenium) and quantity in whole troy ounces of precious metals required in the performance of this contract (including precious metals required for any first article or production sample), and shall specify the national stock number (NSN) and nomenclature, if known, of the deliverable item requiring precious metals.</p><div><div><table border="1" cellpadding="1" cellspacing="1" frame="void" width="100%"><tbody><tr><th scope="col">Precious metal*</th><th scope="col">Quantity</th><th scope="col">Deliverable item (NSN and nomenclature)</th></tr><tr><td>{{textbox_252.208-7000[0]}}</td><td>{{textbox_252.208-7000[1]}}</td><td>{{textbox_252.208-7000[2]}}</td></tr><tr><td>{{textbox_252.208-7000[3]}}</td><td>{{textbox_252.208-7000[4]}}</td><td>{{textbox_252.208-7000[5]}}</td></tr><tr><td>{{textbox_252.208-7000[6]}}</td><td>{{textbox_252.208-7000[7]}}</td><td>{{textbox_252.208-7000[8]}}</td></tr><tr><td>{{textbox_252.208-7000[9]}}</td><td>{{textbox_252.208-7000[10]}}</td><td>{{textbox_252.208-7000[11]}}</td></tr><tr><td>{{textbox_252.208-7000[12]}}</td><td>{{textbox_252.208-7000[13]}}</td><td>{{textbox_252.208-7000[14]}}</td></tr></tbody></table></div><div><p>*If platinum or palladium, specify whether sponge or granules are required.</p></div></div><p>(c) Offerors shall submit two prices for each deliverable item which contains precious metals-one based on the Government furnishing precious metals, and one based on the Contractor furnishing precious metals. Award will be made on the basis which is in the best interest of the Government.</p><p>(d) The Contractor agrees to insert this clause, including this paragraph (d), in solicitations for subcontracts and purchase orders issued in performance of this contract, unless the Contractor knows that the item being purchased contains no precious metals.</p>'
--               '<p>(a) The Government intends to furnish precious metals required in the manufacture of items to be delivered under the contract if the Contracting Officer determines it to be in the Government''s best interest. The use of Government-furnished silver is mandatory when the quantity required is one hundred troy ounces or more. The precious metal(s) will be furnished pursuant to the Government Furnished Property clause of the contract.</p><p>(b) The Offeror shall cite the type (silver, gold, platinum, palladium, iridium, rhodium, and ruthenium) and quantity in whole troy ounces of precious metals required in the performance of this contract (including precious metals required for any first article or production sample), and shall specify the national stock number (NSN) and nomenclature, if known, of the deliverable item requiring precious metals.</p><div><div><table border="1" cellpadding="1" cellspacing="1" frame="void" width="100%"><tbody><tr><th scope="col">Precious metal*</th><th scope="col">Quantity</th><th scope="col">Deliverable item (NSN and nomenclature)!!rs</th></tr><tr><td align="left" colspan="3" scope="row">&nbsp;&nbsp;&nbsp;</td></tr></tbody></table></div><div><p>*If platinum or palladium, specify whether sponge or granules are required.</p></div></div><p>(c) Offerors shall submit two prices for each deliverable item which contains precious metals-one based on the Government furnishing precious metals, and one based on the Contractor furnishing precious metals. Award will be made on the basis which is in the best interest of the Government.</p><p>(d) The Contractor agrees to insert this clause, including this paragraph (d), in solicitations for subcontracts and purchase orders issued in performance of this contract, unless the Contractor knows that the item being purchased contains no precious metals.</p>'
WHERE clause_version_id = 9 AND clause_name = '252.208-7000';

INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size, fill_in_group_number, fill_in_placeholder, fill_in_default_data, fill_in_display_rows, fill_in_for_table, fill_in_heading) 
SELECT clause_id, 'textbox_252.208-7000[0]', 'S', 100, NULL, NULL, NULL, NULL, 0, NULL FROM Clauses WHERE clause_name = '252.208-7000' AND clause_version_id = 9; 
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size, fill_in_group_number, fill_in_placeholder, fill_in_default_data, fill_in_display_rows, fill_in_for_table, fill_in_heading) 
SELECT clause_id, 'textbox_252.208-7000[1]', 'S', 100, NULL, NULL, NULL, NULL, 0, NULL FROM Clauses WHERE clause_name = '252.208-7000' AND clause_version_id = 9; 
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size, fill_in_group_number, fill_in_placeholder, fill_in_default_data, fill_in_display_rows, fill_in_for_table, fill_in_heading) 
SELECT clause_id, 'textbox_252.208-7000[2]', 'S', 100, NULL, NULL, NULL, NULL, 0, NULL FROM Clauses WHERE clause_name = '252.208-7000' AND clause_version_id = 9; 
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size, fill_in_group_number, fill_in_placeholder, fill_in_default_data, fill_in_display_rows, fill_in_for_table, fill_in_heading) 
SELECT clause_id, 'textbox_252.208-7000[3]', 'S', 100, NULL, NULL, NULL, NULL, 0, NULL FROM Clauses WHERE clause_name = '252.208-7000' AND clause_version_id = 9; 
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size, fill_in_group_number, fill_in_placeholder, fill_in_default_data, fill_in_display_rows, fill_in_for_table, fill_in_heading) 
SELECT clause_id, 'textbox_252.208-7000[4]', 'S', 100, NULL, NULL, NULL, NULL, 0, NULL FROM Clauses WHERE clause_name = '252.208-7000' AND clause_version_id = 9; 
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size, fill_in_group_number, fill_in_placeholder, fill_in_default_data, fill_in_display_rows, fill_in_for_table, fill_in_heading) 
SELECT clause_id, 'textbox_252.208-7000[5]', 'S', 100, NULL, NULL, NULL, NULL, 0, NULL FROM Clauses WHERE clause_name = '252.208-7000' AND clause_version_id = 9; 
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size, fill_in_group_number, fill_in_placeholder, fill_in_default_data, fill_in_display_rows, fill_in_for_table, fill_in_heading) 
SELECT clause_id, 'textbox_252.208-7000[6]', 'S', 100, NULL, NULL, NULL, NULL, 0, NULL FROM Clauses WHERE clause_name = '252.208-7000' AND clause_version_id = 9; 
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size, fill_in_group_number, fill_in_placeholder, fill_in_default_data, fill_in_display_rows, fill_in_for_table, fill_in_heading) 
SELECT clause_id, 'textbox_252.208-7000[7]', 'S', 100, NULL, NULL, NULL, NULL, 0, NULL FROM Clauses WHERE clause_name = '252.208-7000' AND clause_version_id = 9; 
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size, fill_in_group_number, fill_in_placeholder, fill_in_default_data, fill_in_display_rows, fill_in_for_table, fill_in_heading) 
SELECT clause_id, 'textbox_252.208-7000[8]', 'S', 100, NULL, NULL, NULL, NULL, 0, NULL FROM Clauses WHERE clause_name = '252.208-7000' AND clause_version_id = 9; 
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size, fill_in_group_number, fill_in_placeholder, fill_in_default_data, fill_in_display_rows, fill_in_for_table, fill_in_heading) 
SELECT clause_id, 'textbox_252.208-7000[9]', 'S', 100, NULL, NULL, NULL, NULL, 0, NULL FROM Clauses WHERE clause_name = '252.208-7000' AND clause_version_id = 9; 
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size, fill_in_group_number, fill_in_placeholder, fill_in_default_data, fill_in_display_rows, fill_in_for_table, fill_in_heading) 
SELECT clause_id, 'textbox_252.208-7000[10]', 'S', 100, NULL, NULL, NULL, NULL, 0, NULL FROM Clauses WHERE clause_name = '252.208-7000' AND clause_version_id = 9; 
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size, fill_in_group_number, fill_in_placeholder, fill_in_default_data, fill_in_display_rows, fill_in_for_table, fill_in_heading) 
SELECT clause_id, 'textbox_252.208-7000[11]', 'S', 100, NULL, NULL, NULL, NULL, 0, NULL FROM Clauses WHERE clause_name = '252.208-7000' AND clause_version_id = 9; 
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size, fill_in_group_number, fill_in_placeholder, fill_in_default_data, fill_in_display_rows, fill_in_for_table, fill_in_heading) 
SELECT clause_id, 'textbox_252.208-7000[12]', 'S', 100, NULL, NULL, NULL, NULL, 0, NULL FROM Clauses WHERE clause_name = '252.208-7000' AND clause_version_id = 9; 
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size, fill_in_group_number, fill_in_placeholder, fill_in_default_data, fill_in_display_rows, fill_in_for_table, fill_in_heading) 
SELECT clause_id, 'textbox_252.208-7000[13]', 'S', 100, NULL, NULL, NULL, NULL, 0, NULL FROM Clauses WHERE clause_name = '252.208-7000' AND clause_version_id = 9; 
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size, fill_in_group_number, fill_in_placeholder, fill_in_default_data, fill_in_display_rows, fill_in_for_table, fill_in_heading) 
SELECT clause_id, 'textbox_252.208-7000[14]', 'S', 100, NULL, NULL, NULL, NULL, 0, NULL FROM Clauses WHERE clause_name = '252.208-7000' AND clause_version_id = 9; 

UPDATE Clauses
SET effective_date = '2015-10-01' -- '2012-12-01'
WHERE clause_version_id = 9 AND clause_name = '252.209-7009';


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT clause_id, prescription_id FROM Prescriptions, Clauses
WHERE prescription_name IN ('2016-00002') 
AND clause_id IN (SELECT clause_id FROM Clauses WHERE clause_name = '252.209-7991 (DEVIATION 2016-00002)' AND clause_version_id = 9); 

UPDATE Clauses
SET clause_data = '<p>(a) <i>Definition.</i> ''SPI process," as used in this clause, means a management or manufacturing process that has been accepted previously by the Department of Defense under the Single Process Initiative (SPI) for use in lieu of a specific military or Federal specification or standard at specific facilities. Under SPI, these processes are reviewed and accepted by a Management Council, which includes representatives of the Contractor, the Defense Contract Management Agency, the Defense Contract Audit Agency, and the military departments.</p><p>(b) Offerors are encouraged to propose SPI processes in lieu of military or Federal specifications and standards cited in the solicitation. A listing of SPI processes accepted at specific facilities is available via the Internet at <i>http://guidebook.dcma.mil/20/guidebook_process.htm</i> (paragraph 4.2).</p><p>(c) An offeror proposing to use an SPI process in lieu of military or Federal specifications or standards cited in the solicitation shall-</p><p>(1) Identify the specific military or Federal specification or standard for which the SPI process has been accepted;</p><p>(2) Identify each facility at which the offeror proposes to use the specific SPI process in lieu of military or Federal specifications or standards cited in the solicitation;</p><p>(3) Identify the contract line items, subline items, components, or elements affected by the SPI process; and</p><p>(4) If the proposed SPI process has been accepted at the facility at which it is proposed for use, but is not yet listed at the Internet site specified in paragraph (b) of this clause, submit documentation of Department of Defense acceptance of the SPI process.</p><p>(d) Absent a determination that an SPI process is not acceptable for this procurement, the Contractor shall use the following SPI processes in lieu of military or Federal specifications or standards:</p><p>(<i>Offeror insert information for each SPI process</i>)</p>SPI Process: {{textbox_252.211-7005[0]}}Facility:  {{textbox_252.211-7005[1]}}Military or Federal Specification or Standard:  {{textbox_252.211-7005[2]}}Affected Contract Line Item Number, Subline Item Number, Component, or Element: {{textbox_252.211-7005[3]}}<p>(e) If a prospective offeror wishes to obtain, prior to the time specified for receipt of offers, verification that an SPI process is an acceptable replacement for military or Federal specifications or standards required by the solicitation, the prospective offeror-</p><p>(1) May submit the information required by paragraph (d) of this clause to the Contracting Officer prior to submission of an offer; but</p><p>(2) Must submit the information to the Contracting Officer at least 10 working days prior to the date specified for receipt of offers.</p>'
--               '<p>(a) <i>Definition.</i> ''SPI process," as used in this clause, means a management or manufacturing process that has been accepted previously by the Department of Defense under the Single Process Initiative (SPI) for use in lieu of a specific military or Federal specification or standard at specific facilities. Under SPI, these processes are reviewed and accepted by a Management Council, which includes representatives of the Contractor, the Defense Contract Management Agency, the Defense Contract Audit Agency, and the military departments.</p><p>(b) Offerors are encouraged to propose SPI processes in lieu of military or Federal specifications and standards cited in the solicitation. A listing of SPI processes accepted at specific facilities is available via the Internet at <i>http://guidebook.dcma.mil/20/guidebook_process.htm</i> (paragraph 4.2).</p><p>(c) An offeror proposing to use an SPI process in lieu of military or Federal specifications or standards cited in the solicitation shall-</p><p>(1) Identify the specific military or Federal specification or standard for which the SPI process has been accepted;</p><p>(2) Identify each facility at which the offeror proposes to use the specific SPI process in lieu of military or Federal specifications or standards cited in the solicitation;</p><p>(3) Identify the contract line items, subline items, components, or elements affected by the SPI process; and</p><p>(4) If the proposed SPI process has been accepted at the facility at which it is proposed for use, but is not yet listed at the Internet site specified in paragraph (b) of this clause, submit documentation of Department of Defense acceptance of the SPI process.</p><p>(d) Absent a determination that an SPI process is not acceptable for this procurement, the Contractor shall use the following SPI processes in lieu of military or Federal specifications or standards:</p><p>(<i>Offeror insert information for each SPI process</i>)</p>SPI Process: _____Facility:  _____Military or Federal Specification or Standard:  _____Affected Contract Line Item Number, Subline Item Number, Component, or Element: _____<p>(e) If a prospective offeror wishes to obtain, prior to the time specified for receipt of offers, verification that an SPI process is an acceptable replacement for military or Federal specifications or standards required by the solicitation, the prospective offeror-</p><p>(1) May submit the information required by paragraph (d) of this clause to the Contracting Officer prior to submission of an offer; but</p><p>(2) Must submit the information to the Contracting Officer at least 10 working days prior to the date specified for receipt of offers.</p>'
WHERE clause_version_id = 9 AND clause_name = '252.211-7005';

INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size, fill_in_group_number, fill_in_placeholder, fill_in_default_data, fill_in_display_rows, fill_in_for_table, fill_in_heading) 
SELECT clause_id, 'textbox_252.211-7005[0]', 'S', 100, NULL, NULL, NULL, NULL, 0, NULL FROM Clauses WHERE clause_name = '252.211-7005' AND clause_version_id = 9; 
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size, fill_in_group_number, fill_in_placeholder, fill_in_default_data, fill_in_display_rows, fill_in_for_table, fill_in_heading) 
SELECT clause_id, 'textbox_252.211-7005[1]', 'S', 100, NULL, NULL, NULL, NULL, 0, NULL FROM Clauses WHERE clause_name = '252.211-7005' AND clause_version_id = 9; 
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size, fill_in_group_number, fill_in_placeholder, fill_in_default_data, fill_in_display_rows, fill_in_for_table, fill_in_heading) 
SELECT clause_id, 'textbox_252.211-7005[2]', 'S', 100, NULL, NULL, NULL, NULL, 0, NULL FROM Clauses WHERE clause_name = '252.211-7005' AND clause_version_id = 9; 
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size, fill_in_group_number, fill_in_placeholder, fill_in_default_data, fill_in_display_rows, fill_in_for_table, fill_in_heading) 
SELECT clause_id, 'textbox_252.211-7005[3]', 'S', 100, NULL, NULL, NULL, NULL, 0, NULL FROM Clauses WHERE clause_name = '252.211-7005' AND clause_version_id = 9; 

UPDATE Clauses
SET clause_conditions = '(A) DOCUMENT TYPE IS: SOLICITATION\n(B) SUPPLIES OR SERVICES IS: SUPPLIES\n(C) PROCEDURES IS: COMPETITION\n(D) PROCEDURES IS: SIMPLIFIED ACQUISITION PROCEDURES (FAR PART 13)' -- '(A) DOCUMENT TYPE IS: SOLICITATION\n(B) SUPPLIES OR SERVICES IS: SUPPLIES\n(C) PROCEDURES IS: COMPETITION\n(D) PROCEDURES IS: SIMPLIFIED ACQUISITION PROCEDURES (FAR PART 13)\n(E) CONTRACT VALUE IS: LESS THAN OR EQUAL TO 1000000'
, clause_rule = 'A AND B AND C AND D' -- 'A AND B AND C AND D AND E'
WHERE clause_version_id = 9 AND clause_name = '252.213-7000';

UPDATE Clauses
SET clause_rule = '(A OR B) AND (C AND L AND ((D AND E) OR (F AND G)) OR ((!(D AND E) OR !(F AND G)) AND M) OR (H AND I AND ((D AND E) OR (F AND G)))' -- '(A OR B) AND C AND L AND ((D AND E) OR (F AND G)) OR (!(D AND E) AND !(F AND G) AND M) OR (H AND I AND ((D AND E) OR (F AND G)))'
WHERE clause_version_id = 9 AND clause_name = '252.215-7004';

UPDATE Clauses
SET effective_date = NULL -- '2014-04-01'
WHERE clause_version_id = 9 AND clause_name = '252.216-7010';
INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT clause_id, prescription_id FROM Prescriptions, Clauses
WHERE prescription_name IN ('216.506(d)') 
AND clause_id IN (SELECT clause_id FROM Clauses WHERE clause_name = '252.216-7010' AND clause_version_id = 9); 
DELETE FROM Clause_Prescriptions
WHERE clause_id IN (SELECT clause_id FROM Clauses WHERE clause_name = '252.216-7010' AND clause_version_id = 9) 
AND prescription_id IN (SELECT prescription_id FROM Prescriptions WHERE prescription_name IN ('216.506(d)(2)')); -- 252.216-7010

UPDATE Clauses
SET clause_data = '<p>(a) ''Hazardous material," as used in this clause, is defined in the Hazardous Material Identification and Material Safety Data clause of this contract.</p><p>(b) The Contractor shall label the item package (unit container) of any hazardous material to be delivered under this contract in accordance with the Hazard Communication Standard (29 CFR 1910.1200 <i>et seq</i>). The Standard requires that the hazard warning label conform to the requirements of the standard unless the material is otherwise subject to the labelling requirements of one of the following statutes:</p><p>(1) Federal Insecticide, Fungicide and Rodenticide Act;</p><p>(2) Federal Food, Drug and Cosmetics Act;</p><p>(3) Consumer Product Safety Act;</p><p>(4) Federal Hazardous Substances Act; or</p><p>(5) Federal Alcohol Administration Act.</p><p>(c) The Offeror shall list which hazardous material listed in the Hazardous Material Identification and Material Safety Data clause of this contract will be labelled in accordance with one of the Acts in paragraphs (b) (1) through (5) of this clause instead of the Hazard Communication Standard. Any hazardous material not listed will be interpreted to mean that a label is required in accordance with the Hazard Communication Standard.</p><div><div><table border="1" cellpadding="1" cellspacing="1" frame="void" width="100%"><tbody><tr><th scope="col">Material (if none, insert ''none.")</th><th scope="col">Act</th></tr><tr><td align="left" scope="row">{{textbox_252.223-7001[0]}}</td><td align="left">{{textbox_252.223-7001[1]}}</td></tr><tr><td align="left" scope="row">{{textbox_252.223-7001[2]}}</td><td align="left">{{textbox_252.223-7001[3]}}</td></tr></tbody></table></div></div><p>(d) The apparently successful Offeror agrees to submit, before award, a copy of the hazard warning label for all hazardous materials not listed in paragraph (c) of this clause. The Offeror shall submit the label with the Material Safety Data Sheet being furnished under the Hazardous Material Identification and Material Safety Data clause of this contract.</p><p>(e) The Contractor shall also comply with MIL-STD-129, Marking for Shipment and Storage (including revisions adopted during the term of this contract).</p>'
--               '<p>(a) ''Hazardous material," as used in this clause, is defined in the Hazardous Material Identification and Material Safety Data clause of this contract.</p><p>(b) The Contractor shall label the item package (unit container) of any hazardous material to be delivered under this contract in accordance with the Hazard Communication Standard (29 CFR 1910.1200 <i>et seq</i>). The Standard requires that the hazard warning label conform to the requirements of the standard unless the material is otherwise subject to the labelling requirements of one of the following statutes:</p><p>(1) Federal Insecticide, Fungicide and Rodenticide Act;</p><p>(2) Federal Food, Drug and Cosmetics Act;</p><p>(3) Consumer Product Safety Act;</p><p>(4) Federal Hazardous Substances Act; or</p><p>(5) Federal Alcohol Administration Act.</p><p>(c) The Offeror shall list which hazardous material listed in the Hazardous Material Identification and Material Safety Data clause of this contract will be labelled in accordance with one of the Acts in paragraphs (b) (1) through (5) of this clause instead of the Hazard Communication Standard. Any hazardous material not listed will be interpreted to mean that a label is required in accordance with the Hazard Communication Standard.</p><div><div><table border="1" cellpadding="1" cellspacing="1" frame="void" width="100%"><tbody><tr><th scope="col">Material (if none, insert ''none.")</th><th scope="col">Act</th></tr><tr><td align="left" scope="row">____</td><td align="left">____</td></tr><tr><td align="left" scope="row">____</td><td align="left">____</td></tr></tbody></table></div></div><p>(d) The apparently successful Offeror agrees to submit, before award, a copy of the hazard warning label for all hazardous materials not listed in paragraph (c) of this clause. The Offeror shall submit the label with the Material Safety Data Sheet being furnished under the Hazardous Material Identification and Material Safety Data clause of this contract.</p><p>(e) The Contractor shall also comply with MIL-STD-129, Marking for Shipment and Storage (including revisions adopted during the term of this contract).</p>'
WHERE clause_version_id = 9 AND clause_name = '252.223-7001';

INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size, fill_in_group_number, fill_in_placeholder, fill_in_default_data, fill_in_display_rows, fill_in_for_table, fill_in_heading) 
SELECT clause_id, 'textbox_252.223-7001[0]', 'S', 100, NULL, NULL, NULL, NULL, 0, NULL FROM Clauses WHERE clause_name = '252.223-7001' AND clause_version_id = 9; 
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size, fill_in_group_number, fill_in_placeholder, fill_in_default_data, fill_in_display_rows, fill_in_for_table, fill_in_heading) 
SELECT clause_id, 'textbox_252.223-7001[1]', 'S', 100, NULL, NULL, NULL, NULL, 0, NULL FROM Clauses WHERE clause_name = '252.223-7001' AND clause_version_id = 9; 
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size, fill_in_group_number, fill_in_placeholder, fill_in_default_data, fill_in_display_rows, fill_in_for_table, fill_in_heading) 
SELECT clause_id, 'textbox_252.223-7001[2]', 'S', 100, NULL, NULL, NULL, NULL, 0, NULL FROM Clauses WHERE clause_name = '252.223-7001' AND clause_version_id = 9; 
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size, fill_in_group_number, fill_in_placeholder, fill_in_default_data, fill_in_display_rows, fill_in_for_table, fill_in_heading) 
SELECT clause_id, 'textbox_252.223-7001[3]', 'S', 100, NULL, NULL, NULL, NULL, 0, NULL FROM Clauses WHERE clause_name = '252.223-7001' AND clause_version_id = 9; 


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT clause_id, prescription_id FROM Prescriptions, Clauses
WHERE prescription_name IN ('225.7003-5(b)') 
AND clause_id IN (SELECT clause_id FROM Clauses WHERE clause_name = '252.225-7010' AND clause_version_id = 9); 

UPDATE Clauses
SET clause_conditions = '(A) DOCUMENT TYPE IS: SOLICITATION\n(B) DOCUMENT TYPE IS: AWARD\n(C) SUPPLIES OR SERVICES IS: SUPPLIES\n(D) TRANSPORTATION REQUIREMENTS 10 IS: REQUIRES IMPORT OF SUPPLIES INTO THE UNITED STATES (50 STATES, THE DISTRICT OF COLUMBIA AND OUTLYING AREAS)\n(E) TRANSPORTATION REQUIREMENTS 11 IS: DUTY-FREE ENTRY MAY BE OBTAINED\n(F) CONTRACT VALUE IS: GREATER THAN SAT\n(G) CONTRACT VALUE IS: LESS THAN OR EQUAL TO SAT\n(H) TRANSPORTATION REQUIREMENTS 11 IS: WAIVING DUTY IS COST EFFECTIVE\n(I) SERVICE CHARACTERISTICS IS: THE SERVICE BEING PROCURED INCLUDES THE FURNISHING OF SUPPLIES\n(J) PROCEDURES IS: NOT COMMERCIAL PROCEDURES (FAR PART 12)\n(K) TRANSPORTATION REQUIREMENTS 10 IS: NOT SUPPLIES WILL NOT ENTER US CUSTOMS TERRITORY' -- '(A) DOCUMENT TYPE IS: SOLICITATION\n(B) DOCUMENT TYPE IS: AWARD\n(C) TRANSPORTATION REQUIREMENTS 10 IS: NOT SUPPLIES WILL NOT ENTER US CUSTOMS TERRITORY'
, clause_rule = '(A OR B) AND (C OR I) AND D AND E AND (F OR (G AND H)) AND J AND K' -- '(A OR B) AND C'
WHERE clause_version_id = 9 AND clause_name = '252.225-7013';

UPDATE Clauses
SET clause_data = '<p>(a) <i>Definitions.</i> As used in this clause-</p><p><i>Bahrainian photovoltaic device</i> means a photovoltaic device that-</p><p>(i) Is wholly manufactured in Bahrain; or</p><p>(ii) In the case of a photovoltaic device that consists in whole or in part of materials from another country, has been substantially transformed in Bahrain into a new and different article of commerce with a name, character, or use distinct from that of the article or articles from which it was transformed, provided that the photovoltaic device is not subsequently substantially transformed outside of Bahrain.</p><p><i>Canadian photovoltaic device</i> means a photovoltaic device that has been substantially transformed in Canada into a new and different article of commerce with a name, character, or use distinct from that of the article or articles from which it was transformed, provided that the photovoltaic device is not subsequently substantially transformed outside of Canada.</p><p><i>Caribbean Basin country photovoltaic device</i> means a photovoltaic device that-</p><p>(i) Is wholly manufactured in a Caribbean Basin country; or</p><p>(ii) In the case of a photovoltaic device that consists in whole or in part of materials from another country, has been substantially transformed in a Caribbean Basin country into a new and different article of commerce with a name, character, or use distinct from that of the article or articles from which it was transformed, provided that the photovoltaic device is not subsequently substantially transformed outside of a Caribbean Basin country.</p><p><i>Designated country</i> means-</p><p>(i) A World Trade Organization Government Procurement Agreement (WTO GPA) country (Armenia, Aruba, Austria, Belgium, Bulgaria, Canada, Croatia, Cyprus, Czech Republic, Denmark, Estonia, Finland, France, Germany, Greece, Hong Kong, Hungary, Iceland, Ireland, Israel, Italy, Japan, Korea (Republic of), Latvia, Liechtenstein, Lithuania, Luxembourg, Malta, Montenegro, Netherlands, New Zealand, Norway, Poland, Portugal, Romania, Singapore, Slovak Republic, Slovenia, Spain, Sweden, Switzerland, Taiwan (known in the World Trade Organization as ''the Separate Customs Territory of Taiwan, Penghu, Kinmen, and Matsu" (Chinese Taipei)), or the United Kingdom);</p><p>(ii) A Free Trade Agreement country (Australia, Bahrain, Canada, Chile, Colombia, Costa Rica, Dominican Republic, El Salvador, Guatemala, Honduras, Korea (Republic of), Mexico, Morocco, Nicaragua, Panama, Peru, or Singapore);</p><p>(iii) A least developed country (Afghanistan, Angola, Bangladesh, Benin, Bhutan, Burkina Faso, Burundi, Cambodia, Central African Republic, Chad, Comoros, Democratic Republic of Congo, Djibouti, Equatorial Guinea, Eritrea, Ethiopia, Gambia, Guinea, Guinea-Bissau, Haiti, Kiribati, Laos, Lesotho, Liberia, Madagascar, Malawi, Mali, Mauritania, Mozambique, Nepal, Niger, Rwanda, Samoa, Sao Tome and Principe, Senegal, Sierra Leone, Solomon Islands, Somalia, South Sudan, Tanzania, Timor-Leste, Togo, Tuvalu, Uganda, Vanuatu, Yemen, or Zambia); or</p><p>(iv) A Caribbean Basin country (Antigua and Barbuda, Aruba, Bahamas, Barbados, Belize, Bonaire, British Virgin Islands, Curacao, Dominica, Grenada, Guyana, Haiti, Jamaica, Montserrat, Saba, St. Kitts and Nevis, St. Lucia, St. Vincent and the Grenadines, Sint Eustatius, Sint Maarten, or Trinidad and Tobago).</p><p><i>Designated country photovoltaic device</i> means a WTO GPA country photovoltaic device, a Free Trade Agreement country photovoltaic device, a least developed country photovoltaic device, or a Caribbean Basin country photovoltaic device.</p><p><i>Domestic photovoltaic device</i> means a photovoltaic device-</p><p>(i) Manufactured in the United States; and</p><p>(ii) The cost of its components that are mined, produced, or manufactured in the United States exceeds 50 percent of the cost of all components. The cost of components includes transportation costs to the place of incorporation into the end product and U.S. duty (whether or not a duty-free entry certificate is issued). Scrap generated, collected, and prepared for processing in the United States is considered domestic.</p><p><i>Foreign photovoltaic device</i> means a photovoltaic device other than a domestic photovoltaic device.</p><p><i>Free Trade Agreement country</i> means Australia, Bahrain, Canada, Chile, Colombia, Costa Rica, Dominican Republic, El Salvador, Guatemala, Honduras, Korea (Republic of), Mexico, Morocco, Nicaragua, Panama, Peru, or Singapore.</p><p><i>Free Trade Agreement country photovoltaic device</i> means a photovoltaic device that-</p><p>(i) Is wholly manufactured in a Free Trade Agreement country; or</p><p>(ii) In the case of a photovoltaic device that consists in whole or in part of materials from another country, has been substantially transformed in a Free Trade Agreement country into a new and different article of commerce with a name, character, or use distinct from that of the article or articles from which it was transformed, provided that the photovoltaic device is not subsequently substantially transformed outside of a Free Trade Agreement country.</p><p><i>Korean photovoltaic device</i> means a photovoltaic device that-</p><p>(i) Is wholly manufactured in Korea; or</p><p>(ii) In the case of a photovoltaic device that consists in whole or in part of materials from another country, has been substantially transformed in Korea (Republic of) into a new and different article of commerce with a name, character, or use distinct from that of the article or articles from which it was transformed, provided that the photovoltaic device is not subsequently substantially transformed outside of Korea (Republic of).</p><p><i>Least developed country photovoltaic device</i> means a photovoltaic device that-</p><p>(i) Is wholly manufactured in a least developed country; or</p><p>(ii) In the case of a photovoltaic device thatconsists in whole or in part of materials from another country, has been substantially transformed in a least developed country into a new and different article of commerce with a name, character, or use distinct from that of the article or articles from which it was transformed, provided that the photovoltaic device is not subsequently substantially transformed outside of a least developed country.</p><p><i>Moroccan photovoltaic device</i> means a photovoltaic device that-</p><p>(i) Is wholly manufactured in Morocco; or	</p><p>(ii) In the case of a photovoltaic device that consists in whole or in part of materials from another country, has been substantially transformed in Morocco into a new and different article of commerce with a name, character, or use distinct from that of the article or articles from which it was transformed, provided that the photovoltaic device is not subsequently substantially transformed outside of Morocco.</p><p><i>Panamanian photovoltaic device</i> means a photovoltaic device that-</p><p>(i) Is wholly manufactured in Panama; or</p><p>(ii) In the case of a photovoltaic device that consists in whole or in part of materials from another country, has been substantially transformed in Panama into a new and different article of commerce with a name, character, or use distinct from that of the article or articles from which it was transformed, provided that the photovoltaic device is not subsequently substantially transformed outside of Panama.</p><p><i>Peruvian photovoltaic device</i> means a photovoltaic device that-</p><p>(i) Is wholly manufactured in Peru; or</p><p>(ii) In the case of a photovoltaic device that consists in whole or in part of materials from another country, has been substantially transformed in Peru into a new and different article of commerce with a name, character, or use distinct from that of the article or articles from which it was transformed, provided that the photovoltaic device is not subsequently substantially transformed outside of Peru.</p><p><i>Photovoltaic device</i> means a device that converts light directly into electricity through a solid-state, semiconductor process.</p><p><i>Qualifying country</i> means a country with a reciprocal defense procurement memorandum of understanding or international agreement with the United States in which both countries agree to remove barriers to purchases of supplies produced in the other country or services performed by sources of the other country, and the memorandum or agreement complies, where applicable, with the requirements of section 36 of the Arms Export Control Act (22 U.S.C. 2776) and with 10 U.S.C. 2457. Accordingly, the following are qualifying countries:</p><table><tbody><tr><td>Australia</td><td>&nbsp;&nbsp;&nbsp;Italy</td></tr><tr><td>Austria</td><td>&nbsp;&nbsp;&nbsp;Luxembourg</td></tr><tr><td>Belgium</td><td>&nbsp;&nbsp;&nbsp;Netherlands</td></tr><tr><td>Canada</td><td>&nbsp;&nbsp;&nbsp;Norway</td></tr><tr><td>Czech Republic</td><td>&nbsp;&nbsp;&nbsp;Poland</td></tr><tr><td>Denmark</td><td>&nbsp;&nbsp;&nbsp;Portugal</td></tr><tr><td>Egypt</td><td>&nbsp;&nbsp;&nbsp;Spain</td></tr><tr><td>Finland</td><td>&nbsp;&nbsp;&nbsp;Sweden</td></tr><tr><td>France</td><td>&nbsp;&nbsp;&nbsp;Switzerland</td></tr><tr><td>Germany</td><td>&nbsp;&nbsp;&nbsp;Turkey</td></tr><tr><td>Greece</td><td>&nbsp;&nbsp;&nbsp;United Kingdom of Great Britain and Northern Ireland</td></tr><tr><td>Israel</td><td>&nbsp;</td></tr></tbody></table><p><i>Qualifying country photovoltaic device</i> means a photovoltaic device manufactured in a qualifying country.</p><p><i>United States</i> means the 50 States, the District of Columbia, and outlying areas.</p><p><i>U.S.-made photovoltaic device</i> means a photovoltaic device that-</p><p>(i) Is manufactured in the United States; or</p><p>(ii) Is substantially transformed in the United States into a new and different article of commerce with a name, character, or use distinct from that of the article or articles from which it was transformed, provided that the photovoltaic device is not subsequently substantially transformed outside of the United States.</p><p><i>WTO GPA country photovoltaic device</i> means a photovoltaic device that-</p><p>(i) Is wholly manufactured in a WTO GPA country; or</p><p>(ii) In the case of a photovoltaic device that consists in whole or in part of materials from another country, has been substantially transformed in a WTO GPA country into a new and different article of commerce with a name, character, or use distinct from that of the article or articles from which it was transformed, provided that the photovoltaic device is not subsequently substantially transformed outside of a WTO GPA country.</p><p>(b) This clause implements section 858 of the National Defense Authorization Act for Fiscal Year 2015 (Pub. L. 113-291).</p><p>(c) <i>Restriction.</i> If the Contractor specified in its offer in the Photovoltaic Devices-Certificate provision of the solicitation that the estimated value of the photovoltaic devices to be utilized in performance of this contract would be-</p><p>(1) Less than $25,000, then the Contractor shall utilize only domestic photovoltaic devices unless, in its offer, it specified utilization of qualifying country or other foreign photovoltaic devices in paragraph (d)(2) of the Photovoltaic Devices-Certificate provision of the solicitation. If the Contractor certified in its offer that it will utilize a qualifying country photovoltaic device, then the Contractor shall utilize a qualifying country photovoltaic device as specified, or, at the Contractor''s option, a domestic photovoltaic device;</p><p>(2) $25,000 or more but less than $79,507, then the Contractor shall utilize in the performance of this contract only domestic photovoltaic devices unless, in its offer, it specified utilization of Canadian, qualifying country, or other foreign photovoltaic devices in paragraph (d)(3) of the Photovoltaic Devices-Certificate provision of the solicitation. If the Contractor certified in its offer that it will utilize a qualifying country photovoltaic device or a Canadian photovoltaic device, then the Contractor shall utilize a qualifying country photovoltaic device or a Canadian photovoltaic device as specified, or, at the Contractor''s option, a domestic photovoltaic device;</p><p>(3) $79,507 or more but less than $100,000, then the Contractor shall utilize under this contract only domestic photovoltaic devices or Free Trade Agreement country photovoltaic devices (other than Bahrainian, Korean, Moroccan, Panamanian, or Peruvian photovoltaic devices), unless, in its offer, it specified utilization of qualifying country or other foreign photovoltaic devices in paragraph (d)(4) of the Photovoltaic Devices-Certificate provision of the solicitation. If the Contractor certified in its offer that it will utilize a qualifying country photovoltaic device or a Free Trade Agreement country photovoltaic device (other than a Bahrainian, Korean, Moroccan, Panamanian, or Peruvian photovoltaic device), then the Contractor shall utilize a qualifying country photovoltaic device; a Free Trade Agreement country photovoltaic device (other than a Bahrainian, Korean, Moroccan, Panamanian, or Peruvian photovoltaic device) as specified; or, at the Contractor''s option, a domestic photovoltaic device;</p><p>(4) $100,000 or more but less than $204,000, then the Contractor shall utilize under this contract only domestic photovoltaic devices or Free Trade Agreement country photovoltaic devices (other than Bahrainian, Moroccan, Panamanian, or Peruvian photovoltaic devices), unless, in its offer, it specified utilization of qualifying country or other foreign photovoltaic devices in paragraph (d)(5) of the Photovoltaic Devices-Certificate provision of the solicitation. If the Contractor certified in its offer that it will utilize a qualifying country photovoltaic device or a Free Trade Agreement country photovoltaic device (other than a Bahrainian, Moroccan, Panamanian, or Peruvian photovoltaic device), then the Contractor shall utilize a qualifying country photovoltaic device; a Free Trade Agreement country photovoltaic device (other than a Bahrainian, Moroccan, Panamanian, or Peruvian photovoltaic device) as specified; or, at the Contractor''s option, a domestic photovoltaic device; or</p><p>(5) $204,000 or more, then the Contractor shall utilize under this contract only domestic or designated country photovoltaic devices unless, in its offer, it specified utilization of U.S.-made or qualifying country photovoltaic devices in paragraph (d)(6)(ii) or (iii) respectively of the Photovoltaic Devices-Certificate provision of the solicitation. If the Contractor certified in its offer that it will utilize a designated country, U.S.-made, or qualifying country photovoltaic device, then the Contractor shall utilize a designated country, U.S.-made, or qualifying country photovoltaic device as specified, or, at the Contractor''s option, a domestic photovoltaic device.</p>'
--               '<p>(a) <i>Definitions.</i> As used in this clause-</p><p><i>Bahrainian photovoltaic device</i> means an article that-</p><p>(i) Is wholly manufactured in Bahrain; or</p><p>(ii) In the case of an article that consists in whole or in part of materials from another country, has been substantially transformed in Bahrain into a new and different article of commerce with a name, character, or use distinct from that of the article or articles from which it was transformed, provided that the photovoltaic device is not subsequently substantially transformed outside of Bahrain.</p><p><i>Canadian photovoltaic device</i> means an article that has been substantially transformed in Canada into a new and different article of commerce with a name, character, or use distinct from that of the article or articles from which it was transformed, provided that the photovoltaic device is not subsequently substantially transformed outside of Canada.</p><p><i>Caribbean Basin country photovoltaic device</i> means an article that-</p><p>(i) Is wholly manufactured in a Caribbean Basin country; or</p><p>(ii) In the case of an article that consists in whole or in part of materials from another country, has been substantially transformed in a Caribbean Basin country into a new and different article of commerce with a name, character, or use distinct from that of the article or articles from which it was transformed, provided that the photovoltaic device is not subsequently substantially transformed outside of a Caribbean Basin country.</p><p><i>Designated country</i> means-</p><p>(i) A World Trade Organization Government Procurement Agreement (WTO GPA) country (Armenia, Aruba, Austria, Belgium, Bulgaria, Canada, Croatia, Cyprus, Czech Republic, Denmark, Estonia, Finland, France, Germany, Greece, Hong Kong, Hungary, Iceland, Ireland, Israel, Italy, Japan, Korea (Republic of), Latvia, Liechtenstein, Lithuania, Luxembourg, Malta, Montenegro, Netherlands, New Zealand, Norway, Poland, Portugal, Romania, Singapore, Slovak Republic, Slovenia, Spain, Sweden, Switzerland, Taiwan (known in the World Trade Organization as ''the Separate Customs Territory of Taiwan, Penghu, Kinmen, and Matsu" (Chinese Taipei)), or the United Kingdom);</p><p>(ii) A Free Trade Agreement country (Australia, Bahrain, Canada, Chile, Colombia, Costa Rica, Dominican Republic, El Salvador, Guatemala, Honduras, Korea (Republic of), Mexico, Morocco, Nicaragua, Panama, Peru, or Singapore);</p><p>(iii) A least developed country (Afghanistan, Angola, Bangladesh, Benin, Bhutan, Burkina Faso, Burundi, Cambodia, Central African Republic, Chad, Comoros, Democratic Republic of Congo, Djibouti, Equatorial Guinea, Eritrea, Ethiopia, Gambia, Guinea, Guinea-Bissau, Haiti, Kiribati, Laos, Lesotho, Liberia, Madagascar, Malawi, Mali, Mauritania, Mozambique, Nepal, Niger, Rwanda, Samoa, Sao Tome and Principe, Senegal, Sierra Leone, Solomon Islands, Somalia, South Sudan, Tanzania, Timor-Leste, Togo, Tuvalu, Uganda, Vanuatu, Yemen, or Zambia); or</p><p>(iv) A Caribbean Basin country (Antigua and Barbuda, Aruba, Bahamas, Barbados, Belize, Bonaire, British Virgin Islands, Curacao, Dominica, Grenada, Guyana, Haiti, Jamaica, Montserrat, Saba, St. Kitts and Nevis, St. Lucia, St. Vincent and the Grenadines, Sint Eustatius, Sint Maarten, or Trinidad and Tobago).</p><p><i>Designated country photovoltaic device</i> means a WTO GPA country photovoltaic device, a Free Trade Agreement country photovoltaic device, a least developed country photovoltaic device, or a Caribbean Basin country photovoltaic device.</p><p><i>Domestic photovoltaic device</i> means a photovoltaic device manufactured in the United States.</p><p><i>Foreign photovoltaic device</i> means a photovoltaic device other than a domestic photovoltaic device.</p><p><i>Free Trade Agreement country</i> means Australia, Bahrain, Canada, Chile, Colombia, Costa Rica, Dominican Republic, El Salvador, Guatemala, Honduras, Korea (Republic of), Mexico, Morocco, Nicaragua, Panama, Peru, or Singapore.</p><p><i>Free Trade Agreement country photovoltaic device</i> means an article that-</p><p>(i) Is wholly manufactured in a Free Trade Agreement country; or</p><p>(ii) In the case of an article that consists in whole or in part of materials from another country, has been substantially transformed in a Free Trade Agreement country into a new and different article of commerce with a name, character, or use distinct from that of the article or articles from which it was transformed, provided that the photovoltaic device is not subsequently substantially transformed outside of a Free Trade Agreement country.</p><p><i>Korean photovoltaic device</i> means an article that-</p><p>(i) Is wholly manufactured in Korea; or</p><p>(ii) In the case of an article that consists in whole or in part of materials from another country, has been substantially transformed in Korea (Republic of) into a new and different article of commerce with a name, character, or use distinct from that of the article or articles from which it was transformed, provided that the photovoltaic device is not subsequently substantially transformed outside of Korea (Republic of).</p><p><i>Least developed country photovoltaic device</i> means an article that-</p><p>(i) Is wholly manufactured in a least developed country; or</p><p>(ii) In the case of an article that consists in whole or in part of materials from another country, has been substantially transformed in a least developed country into a new and different article of commerce with a name, character, or use distinct from that of the article or articles from which it was transformed, provided that the photovoltaic device is not subsequently substantially transformed outside of a least developed country.</p><p><i>Moroccan photovoltaic device</i> means an article that-</p><p>(i) Is wholly manufactured in Morocco; or</p><p>(ii) In the case of an article that consists in whole or in part of materials from another country, has been substantially transformed in Morocco into a new and different article of commerce with a name, character, or use distinct from that of the article or articles from which it was transformed, provided that the photovoltaic device is not subsequently substantially transformed outside of Morocco.</p><p><i>Panamanian photovoltaic device</i> means an article that-</p><p>(i) Is wholly manufactured in Panama; or</p><p>(ii) In the case of an article that consists in whole or in part of materials from another country, has been substantially transformed in Panama into a new and different article of commerce with a name, character, or use distinct from that of the article or articles from which it was transformed, provided that the photovoltaic device is not subsequently substantially transformed outside of Panama.</p><p><i>Peruvian photovoltaic device</i> means an article that-</p><p>(i) Is wholly manufactured in Peru; or</p><p>(ii) In the case of an article that consists in whole or in part of materials from another country, has been substantially transformed in Peru into a new and different article of commerce with a name, character, or use distinct from that of the article or articles from which it was transformed, provided that the photovoltaic device is not subsequently substantially transformed outside of Peru.</p><p><i>Photovoltaic device</i> means a device that converts light directly into electricity through a solid-state, semiconductor process.</p><p><i>Qualifying country</i> means a country with a reciprocal defense procurement memorandum of understanding or international agreement with the United States in which both countries agree to remove barriers to purchases of supplies produced in the other country or services performed by sources of the other country, and the memorandum or agreement complies, where applicable, with the requirements of section 36 of the Arms Export Control Act (22 U.S.C. 2776) and with 10 U.S.C. 2457. Accordingly, the following are qualifying countries:</p><table><tbody><tr><td>Australia</td><td>&nbsp;&nbsp;&nbsp;Italy</td></tr><tr><td>Austria</td><td>&nbsp;&nbsp;&nbsp;Luxembourg</td></tr><tr><td>Belgium</td><td>&nbsp;&nbsp;&nbsp;Netherlands</td></tr><tr><td>Canada</td><td>&nbsp;&nbsp;&nbsp;Norway</td></tr><tr><td>Czech Republic</td><td>&nbsp;&nbsp;&nbsp;Poland</td></tr><tr><td>Denmark</td><td>&nbsp;&nbsp;&nbsp;Portugal</td></tr><tr><td>Egypt</td><td>&nbsp;&nbsp;&nbsp;Spain</td></tr><tr><td>Finland</td><td>&nbsp;&nbsp;&nbsp;Sweden</td></tr><tr><td>France</td><td>&nbsp;&nbsp;&nbsp;Switzerland</td></tr><tr><td>Germany</td><td>&nbsp;&nbsp;&nbsp;Turkey</td></tr><tr><td>Greece</td><td>&nbsp;&nbsp;&nbsp;United Kingdom of Great Britain and Northern Ireland</td></tr><tr><td>Israel</td><td>&nbsp;</td></tr></tbody></table><p><i>Qualifying country photovoltaic device</i> means a photovoltaic device manufactured in a qualifying country.</p><p><i>United States</i> means the 50 States, the District of Columbia, and outlying areas.</p><p><i>U.S.-made photovoltaic device</i> means a photovoltaic device that-</p><p>(i) Is manufactured in the United States; or</p><p>(ii) Is substantially transformed in the United States into a new and different article of commerce with a name, character, or use distinct from that of the article or articles from which it was transformed, provided that the photovoltaic device is not subsequently substantially transformed outside of the United States.</p><p><i>WTO GPA country photovoltaic device</i> means an article that-</p><p>(i) Is wholly manufactured in a WTO GPA country; or</p><p>(ii) In the case of an article that consists in whole or in part of materials from another country, has been substantially transformed in a WTO GPA country into a new and different article of commerce with a name, character, or use distinct from that of the article or articles from which it was transformed, provided that the photovoltaic device is not subsequently substantially transformed outside of a WTO GPA country.</p><p>(b) This clause implements section 846 of the National Defense Authorization Act for Fiscal Year 2011 (Pub. L. 111-383).</p><p>(c) <i>Restriction.</i> If the Contractor specified in its offer in the Photovoltaic Devices-Certificate provision of the solicitation that the estimated value of the photovoltaic devices to be utilized in performance of this contract would be-</p><p>(1) More than $3,500 but less than $25,000, then the Contractor shall utilize only domestic or qualifying country photovoltaic devices unless, in its offer, it specified utilization of other foreign photovoltaic devices in paragraph (c)(2)(ii) of the Photovoltaic Devices-Certificate provision of the solicitation;</p><p>(2) $25,000 or more but less than $79,507, then the Contractor shall utilize in the performance of this contract only domestic or qualifying country photovoltaic devices unless, in its offer, it specified utilization of Canadian or other foreign photovoltaic devices in paragraph (c)(3)(ii) of the Photovoltaic Devices-Certificate provision of the solicitation. If the Contractor certified in its offer that it will utilize a qualifying country photovoltaic device or a Canadian photovoltaic device, the Contractor shall utilize a qualifying country photovoltaic device, a Canadian photovoltaic device, or, at the Contractor''s option, a domestic photovoltaic device;</p><p>(3) $79,507 or more but less than $100,000, then the Contractor shall utilize under this contract only domestic photovoltaic devices, qualifying country photovoltaic devices, or Free Trade Agreement country photovoltaic devices (other than Bahrainian, Korean, Moroccan, Panamanian, or Peruvian photovoltaic devices), unless, in its offer, it specified utilization of other foreign photovoltaic devices in paragraph (c)(4)(ii) of the Photovoltaic Devices-Certificate provision of the solicitation. If the Contractor certified in its offer that it will utilize a qualifying country photovoltaic device or a Free Trade Agreement country photovoltaic device (other than a Bahrainian, Korean, Moroccan, Panamanian, or Peruvian photovoltaic device), the Contractor shall utilize a qualifying country photovoltaic device; a Free Trade Agreement country photovoltaic device (other than a Bahrainian, Korean, Moroccan, Panamanian, or Peruvian photovoltaic device), or, at the Contractor''s option, a domestic photovoltaic device;</p><p>(4) $100,000 or more but less than $204,000, then the Contractor shall utilize under this contract only domestic photovoltaic devices, qualifying country photovoltaic devices, or Free Trade Agreement country photovoltaic devices (other than Bahrainian, Moroccan, Panamanian or Peruvian photovoltaic devices), unless, in its offer, it specified utilization of other foreign photovoltaic devices in paragraph (c)(4)(ii) of the Photovoltaic Devices-Certificate provision of the solicitation. If the Contractor certified in its offer that it will utilize a qualifying country photovoltaic device or a Free Trade Agreement country photovoltaic device (other than a Bahrainian, Moroccan, Panamanian, or Peruvian photovoltaic device), the Contractor shall utilize a qualifying country photovoltaic device; a Free Trade Agreement country photovoltaic device (other than a Bahrainian, Moroccan, Panamanian, or Peruvian photovoltaic device), or, at the Contractor''s option, a domestic photovoltaic device; or</p><p>(5) $204,000 or more, then the Contractor shall utilize under this contract only U.S.-made, qualifying country, or designated country photovoltaic devices.</p>'
WHERE clause_version_id = 9 AND clause_name = '252.225-7017';

UPDATE Clauses
SET effective_date = '2015-10-01' -- '2014-01-01'
, clause_data = '<p>(a) <i>Definitions.</i> ''Bahrainian photovoltaic device," ''Canadian photovoltaic device," ''Caribbean Basin photovoltaic device," ''designated country," ''designated country photovoltaic device," ''domestic photovoltaic device," ''foreign photovoltaic device," ''Free Trade Agreement country," ''Free Trade Agreement photovoltaic device," ''Korean photovoltaic device," ''least developed country photovoltaic device," ''Moroccan photovoltaic device," ''Panamanian photovoltaic device," ''Peruvian photovoltaic device," ''photovoltaic device," ''qualifying country," ''qualifying country photovoltaic device," ''United States," ''U.S.-made photovoltaic device," and ''WTO GPA country photovoltaic device" have the meanings given in the Photovoltaic Devices clause of this solicitation.</p><p>(b) <i>Restrictions.</i> The following restrictions apply, depending on the estimated aggregate value of photovoltaic devices to be utilized under a resultant contract:</p><p>(1) If less than $204,000, then the Government will not accept an offer specifying the use of-</p><p>(i) Other foreign photovoltaic devices in paragraph (d)(2)(iii), (d)(3)(iii), (d)(4)(iii), or (d)(5)(iii) of this provision, unless the offeror documents to the satisfaction of the Contracting Officer that the price of the foreign photovoltaic device plus 50 percent is less than the price of a comparable domestic photovoltaic device and the Government determines in accordance with DFARS 225.217-4(b) that the price of a comparable domestic photovoltaic device would be unreasonable; and</p><p>(ii) A qualifying country photovoltaic device unless the Government determines in accordance with DFARS 225.217-4(a) that it is in the public interest to allow use of a qualifying country photovoltaic device.</p><p>(2) If $204,000 or more, then the Government will consider only offers that utilize photovoltaic devices that are domestic or designated country photovoltaic devices, unless the Government determines in accordance with DFARS 225.7017-4(a) that it is in the public interest to allow use of a qualifying country photovoltaic device from Egypt or Turkey, or a U.S.-made photovoltaic device.</p><p>(c) <i>Country in which a designated country photovoltaic device was wholly manufactured or was substantially transformed.</i> If the estimated value of the photovoltaic devices to be utilized under a resultant contract exceeds $25,000, the Offeror''s certification that such photovoltaic device (e.g., solar panel) is a designated country photovoltaic device shall be consistent with country of origin determinations by the U.S. Customs and Border Protection with regard to importation of the same or similar photovoltaic devices into the United States. If the Offeror is uncertain as to what the country of origin would be determined to be by the U.S. Customs and Border Protection, the Offeror shall request a determination from U.S. Customs and Border Protection. (See <i>http://www.cbp.gov/trade/rulings</i>.)</p><p>(d) <i>Certification and identification of country of origin. [The offeror shall check the block and fill in the blank for one of the following paragraphs, based on the estimated value and the country of origin of photovoltaic devices to be utilized in performance of the contract:]</i></p><p>__(1) No photovoltaic devices will be utilized in performance of the contract.</p><p>(2) If less than $25,000-</p><p>__(i) The offeror certifies that each photovoltaic device to be utilized in performance of the contract is a domestic photovoltaic device;</p><p>__(ii) The offeror certifies that each photovoltaic device to be utilized in performance of the contract is a qualifying country photovoltaic device <i>[Offeror to specify country of origin__];</i> or</p><p>__(iii) The foreign (other than qualifying country) photovoltaic devices to be utilized in performance of the contract are the product of ___. <i>[Offeror to specify country of origin, if known, and provide documentation that the cost of a domestic photovoltaic device would be unreasonable in comparison to the cost of the proposed foreign photovoltaic device, i.e.</i><i>, that the price of the foreign photovoltaic device plus 50 percent is less than the price of a comparable domestic photovoltaic device.]</i></p><p>(3) If $25,000 or more but less than $79,507-</p><p>__(i) The offeror certifies that each photovoltaic device to be utilized in performance of the contract is a domestic photovoltaic device or a Canadian photovoltaic device <i>[Offeror to specify country of origin__];</i></p><p>__(ii) The offeror certifies that each photovoltaic device to be utilized in performance of the contract is a qualifying country photovoltaic device <i>[Offeror to specify country of origin__];</i> or</p><p>__(iii) The foreign (other than qualifying country or Canadian) photovoltaic devices to be utilized in performance of the contract are the product of ___. <i>[Offeror to specify country of origin, if known, and provide documentation that the cost of a domestic photovoltaic device would be unreasonable in comparison to the cost of the proposed foreign photovoltaic device, i.e.</i><i>, that the price of the foreign photovoltaic device plus 50 percent is less than the price of a comparable domestic photovoltaic device.]</i></p><p>(4) If $79,507 or more but less than $100,000-</p><p>__(i) The offeror certifies that each photovoltaic device to be utilized in performance of the contract is a domestic photovoltaic device or a Free Trade Agreement country photovoltaic device (other than a Bahrainian, Korean, Moroccan, Panamanian, or Peruvian photovoltaic device) <i>[Offeror to specify country of origin__];</i></p><p>__(ii) The offeror certifies that each photovoltaic device to be utilized in performance of the contract is a qualifying country photovoltaic device (except an Australian or Canadian photovoltaic device, to be listed in paragraph (d)(4)(i) of this provision as a Free Trade Agreement country photovoltaic device) <i>[Offeror to specify country of origin__];</i> or</p><p>__(iii) The offered foreign photovoltaic devices (other than those from countries listed in paragraph (d)(4)(i) or (d)(4)(ii) of this provision) are the product of ___. <i>[Offeror to specify country of origin, if known, and provide documentation that the cost of a domestic photovoltaic device would be unreasonable in comparison to the cost of the proposed foreign photovoltaic device, i.e.</i><i>, that the price of the foreign photovoltaic device plus 50 percent is less than the price of a comparable domestic photovoltaic device.]</i></p><p>(5) If $100,000 or more but less than $204,000-</p><p>__(i) The offeror certifies that each photovoltaic device to be utilized in performance of the contract is a domestic photovoltaic device or a Free Trade Agreement country photovoltaic device (other than a Bahrainian, Moroccan, Panamanian, or Peruvian photovoltaic device) <i>[Offeror to specify country of origin__];</i></p><p>__(ii) The offeror certifies that each photovoltaic device to be utilized in performance of the contract is a qualifying country photovoltaic device (except an Australian or Canadian photovoltaic device, to be listed in paragraph (d)(5)(i) of this provision as a Free Trade Agreement country photovoltaic device) <i>[Offeror to specify country of origin__];</i> or</p><p>__(iii) The offered foreign photovoltaic devices (other than those from countries listed in paragraph (d)(5)(i) or (d)(5)(ii) of this provision) are the product of ___. <i>[Offeror to specify country of origin, if known, and provide documentation that the cost of a domestic photovoltaic device would be unreasonable in comparison to the cost of the proposed foreign photovoltaic device, i.e.</i><i>, that the price of the foreign photovoltaic device plus 50 percent is less than the price of a comparable domestic photovoltaic device.]</i></p><p>(6) If $204,000 or more, the Offeror certifies that each photovoltaic device to be used in performance of the contract is-</p><p>__(i) A domestic or designated country photovoltaic device <i>[Offeror to specify country of origin__];</i></p><p>__(ii) A U.S.-made photovoltaic device; or</p><p>__(iii) A qualifying country photovoltaic device from Egypt of Turkey (photovoltaic devices from other qualifying countries to be listed in paragraph (d)(6)(i) of this provision as designated country photovoltaic devices). <i>[Offeror to specify country of origin__.]</i></p>'
--               '<p>(a) <i>Definitions.</i> ''Bahrainian photovoltaic device," ''Canadian photovoltaic device," ''Caribbean Basin photovoltaic device," ''designated country," ''designated country photovoltaic device," ''domestic photovoltaic device," ''foreign photovoltaic device," ''Free Trade Agreement country," ''Free Trade Agreement photovoltaic device," ''Korean photovoltaic device," ''least developed country photovoltaic device," ''Moroccan photovoltaic device," ''Panamanian photovoltaic device," ''Peruvian photovoltaic device," ''photovoltaic device," ''qualifying country," ''qualifying country photovoltaic device," ''United States," ''U.S.-made photovoltaic device," and ''WTO GPA country photovoltaic device" have the meanings given in the Photovoltaic Devices clause of this solicitation.</p><p>(b) <i>Restrictions.</i> The following restrictions apply, depending on the estimated value of any photovoltaic devices to be utilized under a resultant contract:</p><p>(1) If more than $3,500 but less than $204,000, then the Government will not accept an offer specifying the use of other foreign photovoltaic devices in paragraph (d)(2)(ii), (d)(3)(ii) or (d)(4)(ii) of this provision, unless the offeror documents to the satisfaction of the Contracting Officer that the price of the foreign photovoltaic device plus 50 percent is less than the price of a comparable domestic photovoltaic device.</p><p>(2) If $204,000 or more, then the Government will consider only offers that utilize photovoltaic devices that are U.S.-made, qualifying country, or designated country photovoltaic devices.</p><p>(c) <i>Country in which a designated country photovoltaic device was wholly manufactured or was substantially transformed.</i> If the estimated value of the photovoltaic devices to be utilized under a resultant contract exceeds $25,000, the Offeror''s certification that such photovoltaic device (e.g., solar panel) is a designated country photovoltaic device shall be consistent with country of origin determinations by the U.S. Customs and Border Protection with regard to importation of the same or similar photovoltaic devices into the United States. If the Offeror is uncertain as to what the country of origin would be determined to be by the U.S. Customs and Border Protection, the Offeror shall request a determination from U.S. Customs and Border Protection. (See <i>http://www.cbp.gov/xp/cgov/trade/legal/rulings/.</i>)</p><p>(d) <i>Certification and identification of country of origin.</i> [<i>The offeror shall check the block and fill in the blank for one of the following paragraphs, based on the estimated value and the country of origin of photovoltaic devices to be utilized in performance of the contract:</i>]</p><p>__ (1) No photovoltaic devices will be utilized in performance of the contract, or such photovoltaic devices have an estimated value of $3,500 or less.</p><p>(2) If more than $3,500 but less than $25,000-</p><p>__ (i) The offeror certifies that each photovoltaic device to be utilized in performance of the contract is a domestic photovoltaic device or a qualifying country photovoltaic device [<i>Offeror to specify country of origin</i> __________]; or</p><p>__ (ii) The foreign (other than qualifying country) photovoltaic devices to be utilized in performance of the contract are the product of __________. [<i>Offeror to specify country of origin, if known, and provide documentation that the cost of a domestic photovoltaic device would be unreasonable in comparison to the cost of the proposed foreign photovoltaic device.</i>]</p><p>(3) If $25,000 or more but less than $79,507-</p><p>__ (i) The offeror certifies that each photovoltaic device to be utilized in performance of the contract is a domestic photovoltaic device; a qualifying country photovoltaic device; or a Canadian photovoltaic device [<i>Offeror to specify country of origin</i>__________]; or</p><p>__ (ii) The foreign (other than qualifying country or Canadian) photovoltaic devices to be utilized in performance of the contract are the product of __________. [<i>Offeror to specify country of origin, if known, and provide documentation that the cost of a domestic photovoltaic device would be unreasonable in comparison to the cost of the proposed foreign photovoltaic device.</i>]</p><p>(4) If $79,507 or more but less than $100,000-</p><p>_(i) The offeror certifies that each photovoltaic device to be utilized in performance of the contract is a domestic photovoltaic device; a qualifying country (except Australian or Canadian) photovoltaic device; a Free Trade Agreement country photovoltaic device (other than a Bahrainian, Korean, Moroccan, Panamanian, or Peruvian photovoltaic device) [<i>Offeror to specify country of origin____]; or</i></p><p>_(ii) The offered foreign photovoltaic devices (other than those from countries listed in paragraph (d)(4)(i) of this provision) are the product of ______. [<i>Offeror to specify country of origin, if known, and provide documentation that the cost of a domestic photovoltaic device would be unreasonable in comparison to the cost of the proposed foreign photovoltaic device.</i>]</p><p>(5) If $100,000 or more but less than $204,000-</p><p>__(i) The offeror certifies that each photovoltaic device to be utilized in performance of the contract is a domestic photovoltaic device; a qualifying country (except Australian or Canadian) photovoltaic device; a Free Trade Agreement country photovoltaic device (other than a Bahrainian, Moroccan, Panamanian, or Peruvian photovoltaic device) [<i>Offeror to specify country of origin____]; or</i></p><p>__(ii) The offered foreign photovoltaic devices (other than those from countries listed in paragraph (d)(4)(i) of this provision) are the product of ______. [<i>Offeror to specify country of origin, if known, and provide documentation that the cost of a domestic photovoltaic device would be unreasonable in comparison to the cost of the proposed foreign photovoltaic device.</i>]</p><p>(6) If $204,000 or more-</p><p>__ The offeror certifies that each photovoltaic device to be utilized in performance of the contract is a U.S.-made, qualifying country, or designated country photovoltaic device. [<i>Offeror to specify country of origin __________.</i>]</p>'
WHERE clause_version_id = 9 AND clause_name = '252.225-7018';

UPDATE Clauses
SET effective_date = '2015-10-01' -- '2014-11-01'
, clause_conditions = '(A) DOCUMENT TYPE IS: SOLICITATION (B) DOCUMENT TYPE IS: AWARD (C) MISCELLANEOUS PROGRAM CHARACTERISTICS IS: THIS ACQUISITION IS COVERED BY THE WORLD TRADE ORGANIZATION (WTO) AGREEMENT ON GOVERNMENT PROCUREMENT (GPA) (D) COMPETITION PREFERENCES IS: NOT PURCHASE FROM A FOREIGN SOURCE IS RESTRICTED (E) 252.225-7026 IS: NOT INCLUDED (F) PURPOSE OF PROCUREMENT IS: NOT FOR USE IN SUPPORT OF OPERATIONS IN AFGHANISTAN' -- '(A) DOCUMENT TYPE IS: SOLICITATION (B) DOCUMENT TYPE IS: AWARD (C) MISCELLANEOUS PROGRAM CHARACTERISTICS IS: THIS ACQUISITION IS COVERED BY THE WORLD TRADE ORGANIZATION (WTO) AGREEMENT ON GOVERNMENT PROCUREMENT (GPA) (D) COMPETITION PREFERENCES IS: NOT PURCHASE FROM A FOREIGN SOURCE IS RESTRICTED (E) 252.225-7026 IS: NOT INCLUDED'
, clause_rule = '(A OR B) AND C AND D AND E AND F' -- '(A OR B) AND C AND D AND E'
WHERE clause_version_id = 9 AND clause_name = '252.225-7021';

UPDATE Clauses
SET clause_rule = '(A OR B) AND (C OR D OR E OR F OR G OR H) AND I' -- '(A OR B) AND !C AND !D AND !E AND !F AND !G AND !H AND I'
WHERE clause_version_id = 9 AND clause_name = '252.225-7025';

UPDATE Clauses
SET clause_conditions = '(A) DOCUMENT TYPE IS: SOLICITATION\n(B) DOCUMENT TYPE IS: AWARD\n(C) PLACE OF PERFORMANCE IS: DESIGNATED OPERATIONAL AREA\n(D) LABOR REQUIREMENTS 2 IS: THE CONTRACTOR IS AUTHORIZED TO ACCOMPANY US FORCES OUTSIDE THE US\n(E) PURPOSE OF PROCUREMENT IS: FOR USE IN CONTINGENCY OPERATIONS\n(G) PURPOSE OF PROCUREMENT IS: FOR USE IN HUMANITARIAN OR PEACEKEEPING OPERATIONS\n(H) PURPOSE OF PROCUREMENT IS: FOR USE IN MILITARY OPERATIONS OR MILITARY EXERCISES DESIGNATED BY THE COMBATANT COMMANDER' -- '(A) DOCUMENT TYPE IS: SOLICITATION (B) DOCUMENT TYPE IS: AWARD (C) PLACE OF PERFORMANCE IS: DESIGNATED OPERATIONAL AREA (D) LABOR REQUIREMENTS 2 IS: THE CONTRACTOR IS AUTHORIZED TO ACCOMPANY US FORCES OUTSIDE THE US (E) PURPOSE OF PROCUREMENT IS: FOR USE IN CONTINGENCY OPERATIONS (G) PURPOSE OF PROCUREMENT IS: FOR USE IN HUMANITARIAN OR PEACEKEEPING OPERATIONS (H) PURPOSE OF PROCUREMENT IS: FOR USE IN MILITARY OPERATIONS OR MILITARY EXERCISES DESIGNATED BY THE COMBATANT COMMANDER'
WHERE clause_version_id = 9 AND clause_name = '252.225-7040';

UPDATE Clauses
SET effective_date = '2015-10-01' -- '2014-11-01'
WHERE clause_version_id = 9 AND clause_name = '252.225-7045';

UPDATE Clauses
SET effective_date = '2015-10-01' -- '2014-12-01'
WHERE clause_version_id = 9 AND clause_name = '252.225-7049';

UPDATE Clauses
SET effective_date = '2015-10-01' -- '2014-12-01'
WHERE clause_version_id = 9 AND clause_name = '252.225-7050';

UPDATE Clauses
SET clause_conditions = '(A) DOCUMENT TYPE IS: SOLICITATION (B) 252.225-7999 IS: INCLUDED' -- '(A) DOCUMENT TYPE IS: SOLICITATION (B) 252.225-7999 (DEVIATION 2014-00014) IS: INCLUDED'
WHERE clause_version_id = 9 AND clause_name = '252.225-7998 (DEVIATION 2014-00014)';

UPDATE Clauses
SET inclusion_cd = 'R' -- F
, editable_remarks = NULL -- 'ALL'
, clause_data = '<p>The Contractor hereby releases each and every claim and demand which he now has or may hereafter have against the Government for the manufacture or use by or for the Government prior to the effective date of this contract, of any inventions covered by (i) any of the patents and applications for patent identified in this contract, and (ii) any other patent or application for patent owned or hereafter acquired by him, insofar as and only to the extent that such other patent or patent application covers the manufacture, use, or disposition of (description of subject matter).*</p><div><p>*Bracketed portions of the clause may be omitted when not appropriate or not encompassed by the release as negotiated.</p></div>'
--               '{{memobox_252.227-7001[0]}}'
WHERE clause_version_id = 9 AND clause_name = '252.227-7001';

DELETE FROM Document_Fill_Ins WHERE clause_fill_in_id IN (SELECT clause_fill_in_id FROM Clause_Fill_Ins CF INNER JOIN Clauses C  ON C.clause_id= CF.clause_id
WHERE clause_name = '252.227-7001' AND clause_version_id = 9 AND fill_in_code='memobox_252.227-7001[0]');
DELETE FROM Clause_Fill_Ins WHERE clause_id IN (SELECT clause_id FROM Clauses 
WHERE clause_name = '252.227-7001' AND clause_version_id = 9)  AND fill_in_code='memobox_252.227-7001[0]';

UPDATE Clauses
SET inclusion_cd = 'R' -- F
WHERE clause_version_id = 9 AND clause_name = '252.227-7003';

UPDATE Clauses
SET inclusion_cd = 'R' -- F
WHERE clause_version_id = 9 AND clause_name = '252.227-7007';


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT clause_id, prescription_id FROM Prescriptions, Clauses
WHERE prescription_name IN ('227.7009-4(c)') 
AND clause_id IN (SELECT clause_id FROM Clauses WHERE clause_name = '252.227-7008' AND clause_version_id = 9); 

UPDATE Clauses
SET inclusion_cd = 'R' -- F
WHERE clause_version_id = 9 AND clause_name = '252.227-7010';

UPDATE Clauses
SET provision_or_clause = 'P' -- 'C'
WHERE clause_version_id = 9 AND clause_name = '252.228-7004';

UPDATE Clauses
SET clause_data = '<p>(a) Prices set forth in this contract are exclusive of all taxes and duties from which the United States Government is exempt by virtue of tax agreements between the United States Government and the Contractor''s government. The following taxes or duties have been excluded from the contract price:</p><p>NAME OF TAX: {{textbox_252.229-7001[0]}} (Offeror Insert) RATE (PERCENTAGE): {{textbox_252.229-7001[1]}} (Offeror Insert)</p><p>(b) The Contractor''s invoice shall list separately the gross price, amount of tax deducted, and net price charged.</p><p>(c) When items manufactured to United States Government specifications are being acquired, the Contractor shall identify the materials or components intended to be imported in order to ensure that relief from import duties is obtained. If the Contractor intends to use imported products from inventories on hand, the price of which includes a factor for import duties, the Contractor shall ensure the United States Government''s exemption from these taxes. The Contractor may obtain a refund of the import duties from its government or request the duty-free import of an amount of supplies or components corresponding to that used from inventory for this contract.</p>'
--               '<p>(a) Prices set forth in this contract are exclusive of all taxes and duties from which the United States Government is exempt by virtue of tax agreements between the United States Government and the Contractor''s government. The following taxes or duties have been excluded from the contract price:</p><p>NAME OF TAX: (Offeror Insert) RATE (PERCENTAGE): (Offeror Insert)</p><p>(b) The Contractor''s invoice shall list separately the gross price, amount of tax deducted, and net price charged.</p><p>(c) When items manufactured to United States Government specifications are being acquired, the Contractor shall identify the materials or components intended to be imported in order to ensure that relief from import duties is obtained. If the Contractor intends to use imported products from inventories on hand, the price of which includes a factor for import duties, the Contractor shall ensure the United States Government''s exemption from these taxes. The Contractor may obtain a refund of the import duties from its government or request the duty-free import of an amount of supplies or components corresponding to that used from inventory for this contract.</p>'
WHERE clause_version_id = 9 AND clause_name = '252.229-7001';

INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size, fill_in_group_number, fill_in_placeholder, fill_in_default_data, fill_in_display_rows, fill_in_for_table, fill_in_heading) 
SELECT clause_id, 'textbox_252.229-7001[0]', 'S', 100, NULL, NULL, NULL, NULL, 0, NULL FROM Clauses WHERE clause_name = '252.229-7001' AND clause_version_id = 9; 
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size, fill_in_group_number, fill_in_placeholder, fill_in_default_data, fill_in_display_rows, fill_in_for_table, fill_in_heading) 
SELECT clause_id, 'textbox_252.229-7001[1]', 'S', 100, NULL, NULL, NULL, NULL, 0, NULL FROM Clauses WHERE clause_name = '252.229-7001' AND clause_version_id = 9; 


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT clause_id, prescription_id FROM Prescriptions, Clauses
WHERE prescription_name IN ('232.502-4(b)') 
AND clause_id IN (SELECT clause_id FROM Clauses WHERE clause_name = '252.232-7004' AND clause_version_id = 9); 


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT clause_id, prescription_id FROM Prescriptions, Clauses
WHERE prescription_name IN ('235.072(c)') 
AND clause_id IN (SELECT clause_id FROM Clauses WHERE clause_name = '252.235-7010' AND clause_version_id = 9); 


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT clause_id, prescription_id FROM Prescriptions, Clauses
WHERE prescription_name IN ('237.7003(a)(1)') 
AND clause_id IN (SELECT clause_id FROM Clauses WHERE clause_name = '252.237-7002' AND clause_version_id = 9); 

UPDATE Clauses
SET editable_remarks = 'PARENT\nONLY paragraphs (d)(1) and (2) of the clause may be modified to meet local conditions."' -- 'PARENT\nONLY paragraphs (d)(1) and (2) of the clause may be modified to meet local conditions.'
WHERE clause_version_id = 9 AND clause_name = '252.237-7017';

UPDATE Clauses
SET provision_or_clause = 'P' -- 'p'
WHERE clause_version_id = 9 AND clause_name = '252.239-7009';


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT clause_id, prescription_id FROM Prescriptions, Clauses
WHERE prescription_name IN ('239.7411(c)') 
AND clause_id IN (SELECT clause_id FROM Clauses WHERE clause_name = '252.239-7013' AND clause_version_id = 9); 


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT clause_id, prescription_id FROM Prescriptions, Clauses
WHERE prescription_name IN ('239.7411(c)') 
AND clause_id IN (SELECT clause_id FROM Clauses WHERE clause_name = '252.239-7014' AND clause_version_id = 9); 


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT clause_id, prescription_id FROM Prescriptions, Clauses
WHERE prescription_name IN ('239.7411(c)') 
AND clause_id IN (SELECT clause_id FROM Clauses WHERE clause_name = '252.239-7015' AND clause_version_id = 9); 

UPDATE Clauses
SET clause_conditions = '(A) DOCUMENT TYPE IS: SOLICITATION\n(B) SUPPLIES OR SERVICES IS: INFORMATION TECHNOLOGY\n(C) SUPPLIES IS: INFORMATION TECHNOLOGY\n(D) COVERED SYSTEM IS: YES' -- '(A) DOCUMENT TYPE IS: SOLICITATION\n(B) SUPPLIES OR SERVICES IS: INFORMATION TECHNOLOGY\n(C) SUPPLIES IS: INFORMATION TECHNOLOGY'
, clause_rule = 'A AND (B OR C) AND D' -- 'A AND (B OR C)'
WHERE clause_version_id = 9 AND clause_name = '252.239-7017';

UPDATE Clauses
SET effective_date = '2015-10-01' -- '2013-11-01'
, clause_conditions = '(A) DOCUMENT TYPE IS: SOLICITATION (B) DOCUMENT TYPE IS: AWARD (C) SUPPLIES IS: INFORMATION TECHNOLOGY (D) SUPPLIES IS: INFORMATION TECHNOLOGY (E) COVERED SYSTEM IS: YES' -- '(A) DOCUMENT TYPE IS: SOLICITATION (B) DOCUMENT TYPE IS: AWARD (C) SUPPLIES IS: INFORMATION TECHNOLOGY (D) SUPPLIES IS: INFORMATION TECHNOLOGY'
, clause_rule = '(A OR B) AND (C OR D) AND E' -- '(A OR B) AND (C OR D)'
WHERE clause_version_id = 9 AND clause_name = '252.239-7018';

UPDATE Clauses
SET inclusion_cd = 'R' -- F
WHERE clause_version_id = 9 AND clause_name = '252.246-7001';

UPDATE Clauses
SET clause_rule = '(A OR B) AND C AND D AND E' -- '(A AND B) AND C AND D AND E'
WHERE clause_version_id = 9 AND clause_name = '252.247-7005';

UPDATE Clauses
SET clause_section_id = 6 -- 9
, clause_rule = '(A OR B) AND C AND D AND !(E AND ((F OR G OR H) OR (I AND J)))' -- '(A OR B) AND C AND D AND !(E AND (F OR G OR H) OR (I AND J))'
WHERE clause_version_id = 9 AND clause_name = '252.247-7023';
INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT clause_id, prescription_id FROM Prescriptions, Clauses
WHERE prescription_name IN ('247.574(b)(1)') 
AND clause_id IN (SELECT clause_id FROM Clauses WHERE clause_name = '252.247-7023' AND clause_version_id = 9); 

UPDATE Clauses
SET effective_date = '2015-10-01' -- '2010-10-01'
WHERE clause_version_id = 9 AND clause_name = '252.249-7002';



UPDATE Clause_Fill_Ins SET fill_in_type = 'S', fill_in_max_size = 50 WHERE fill_in_code = 'subcontract_max_52.203-14'
AND clause_id IN (SELECT clause_id FROM Clauses 
WHERE clause_name = '52.203-14' AND clause_version_id = 9) ;

UPDATE Clauses
SET clause_conditions = '(A) DOCUMENT TYPE IS: SOLICITATION\n(B) DOCUMENT TYPE IS: AWARD\n(C) CONTRACT VALUE IS: GREATER THAN SAT\n(D) SERVICES IS: NOT PERSONAL SERVICES\n(E) ISSUING OFFICE IS: DEPARTMENT OF DEFENSE\n(F) BUSINESS TYPE IS: FOREIGN GOVERNMENT\n(G) TYPE OF FUNDS IS: NON-APPROPRIATED FUNDS\n(H) PROCEDURES IS: NOT COMMERCIAL PROCEDURES (FAR PART 12)' -- '(A) DOCUMENT TYPE IS: SOLICITATION (B) DOCUMENT TYPE IS: AWARD (C) CONTRACT VALUE IS: GREATER THAN SAT (D) SERVICES IS: NOT PERSONAL SERVICES (E) ISSUING OFFICE IS: DEPARTMENT OF DEFENSE (F) BUSINESS TYPE IS: FOREIGN GOVERNMENT (G) TYPE OF FUNDS IS: NON-APPROPRIATED FUNDS (H) PROCEDURES IS: NOT COMMERCIAL PROCEDURES (FAR PART 12)'
WHERE clause_version_id = 9 AND clause_name = '52.203-3';

UPDATE Clauses
SET clause_conditions = NULL -- 'If applicable, the rule for the prescription for using this clause will be in the component clause'
WHERE clause_version_id = 9 AND clause_name = '52.204-1';

UPDATE Clauses
SET clause_conditions = '(A) DOCUMENT TYPE IS: SOLICITATION\n(B) DOCUMENT TYPE IS: AWARD\n(C) AWARD TYPE IS: INDEFINITE DELIVERY CONTRACT\n(D) SUPPLIES OR SERVICES IS: SERVICES\n(E) SERVICE CONTRACT REPORTING IS: YES\n(F) TYPE OF FUNDS IS: NOT FUNDED ENTIRELY WITH DOD FUNDS\n(G) GENERIC DUNS IS: NO' -- '(A) DOCUMENT TYPE IS: SOLICITATION\n(B) DOCUMENT TYPE IS: AWARD\n(C) AWARD TYPE IS: INDEFINITE DELIVERY CONTRACT\n(D) SUPPLIES OR SERVICES IS: SERVICES\n(E) SERVICE CONTRACT REPORTING IS: YES\n(F) TYPE OF FUNDS IS: NOT FUNDED ENTIRELY WITH DOD FUNDS\n(G) GENERIC DUNS IS: NO\n(H) DOCUMENT SECURITY LEVEL IS: UNCLASSIFIED'
, clause_rule = '(A OR B) AND C AND D AND E AND F AND G' -- '(A OR B) AND C AND D AND E AND F AND G AND H'
WHERE clause_version_id = 9 AND clause_name = '52.204-15';

UPDATE Clauses
SET commercial_status = 'R (ALWAYS)' -- 'R (RULE APPLIES)'
WHERE clause_version_id = 9 AND clause_name = '52.204-16';

UPDATE Clauses
SET commercial_status = 'R (ALWAYS)' -- 'R (RULE APPLIES)'
WHERE clause_version_id = 9 AND clause_name = '52.204-18';

UPDATE Clauses
SET clause_rule = 'A AND B AND C AND D AND (E OR F) AND G AND H' -- 'A AND B AND CAND D AND (E OR F) AND G AND H'
WHERE clause_version_id = 9 AND clause_name = '52.204-5';

UPDATE Clauses
SET clause_conditions = '(A) DOCUMENT TYPE IS: SOLICITATION\n(C) CONTRACT VALUE IS: LESS THAN 3000\n(D) BILLING ARRANGEMENTS IS: GOVERNMENT WIDE PURCHASE CARD USED\n(E) DOCUMENT SECURITY LEVEL IS: UNCLASSIFIED\n(F) PURPOSE OF PROCUREMENT IS: FOR USE IN CONTINGENCY OPERATIONS\n(G) PURPOSE OF PROCUREMENT IS: FOR USE IN HUMANITARIAN OR PEACEKEEPING OPERATIONS\n(H) PURPOSE OF PROCUREMENT IS: FOR USE IN COMBAT AND OTHER SIGNIFICANT MILITARY OPERATIONS\n(I) PLACE OF ISSUANCE IS: SOLICITATION/AWARD ISSUED IN THE UNITED STATES (50 STATES PLUS DISTRICT OF COLUMBIA)\n(J) PLACE OF ISSUANCE IS: SOLICITATION/AWARD ISSUED IN THE UNITED STATES OUTLYING AREAS\n(K) PURPOSE OF PROCUREMENT IS: FOR USE IN SUPPORTING A DIPLOMATIC OR CONSULAR MISSION THAT HAS BEEN DESIGNATED AS A DANGER PAY POST BY THE DEPARTMENT OF STATE\n(L) PURPOSE OF PROCUREMENT IS: NOT FOR USE IN EMERGENCY OPERATIONS\n(M) BUSINESS TYPE IS: SELF-EMPLOYED INDIVIDUAL\n(N) PLACE OF PERFORMANCE IS: UNITED STATES (50 STATES PLUS DISTRICT OF COLUMBIA)\n(O) PLACE OF PERFORMANCE IS: UNITED STATES OUTLYING AREA\n(P) FULL AND OPEN EXCEPTION IS: NOT 6.302-2 UNUSUAL AND COMPELLING URGENCY\n(Q) CONTRACT VALUE IS: LESS THAN OR EQUAL TO 30000\n(R) BUSINESS TYPE IS: FOREIGN\n(S) SAM EXEMPTION IS: YES\n(T) CAR IS: NO\n(V) BILLING ARRANGEMENTS IS: NOT ELECTRONIC FUNDS TRANSFER APPLIES\n(W) PURCHASE CARD IS: YES\n(X) PROCEDURES IS: NOT COMMERCIAL PROCEDURES (FAR PART 12)' -- '(A) DOCUMENT TYPE IS: SOLICITATION\n(C) CONTRACT VALUE IS: LESS THAN 3000\n(D) BILLING ARRANGEMENTS IS: GOVERNMENT WIDE PURCHASE CARD USED\n(E) DOCUMENT SECURITY LEVEL IS: UNCLASSIFIED\n(F) PURPOSE OF PROCUREMENT IS: FOR USE IN CONTINGENCY OPERATIONS\n(G) PURPOSE OF PROCUREMENT IS: FOR USE IN HUMANITARIAN OR PEACEKEEPING OPERATIONS\n(H) PURPOSE OF PROCUREMENT IS: FOR USE IN COMBAT AND OTHER SIGNIFICANT MILITARY OPERATIONS\n(I) PLACE OF ISSUANCE IS: SOLICITATION/AWARD ISSUED IN THE UNITED STATES (50 STATES PLUS DISTRICT OF COLUMBIA)\n(J) PLACE OF ISSUANCE IS: SOLICITATION/AWARD ISSUED IN THE UNITED STATES OUTLYING AREAS\n(K) PURPOSE OF PROCUREMENT IS: FOR USE IN SUPPORTING A DIPLOMATIC OR CONSULAR MISSION THAT HAS BEEN DESIGNATED AS A DANGER PAY POST BY THE DEPARTMENT OF STATE\n(L) PURPOSE OF PROCUREMENT IS: NOT FOR USE IN EMERGENCY OPERATIONS\n(M) BUSINESS TYPE IS: SELF-EMPLOYED INDIVIDUAL\n(N) PLACE OF PERFORMANCE IS: UNITED STATES (50 STATES PLUS DISTRICT OF COLUMBIA)\n(O) PLACE OF PERFORMANCE IS: UNITED STATES OUTLYING AREAS\n(P) FULL AND OPEN EXCEPTION IS: NOT 6.302-2 UNUSUAL AND COMPELLING URGENCY\n(Q) CONTRACT VALUE IS: LESS THAN OR EQUAL TO 25000\n(R) BUSINESS TYPE IS: FOREIGN SOURCE\n(S) SAM EXEMPTION IS: YES\n(T) CAR IS: NO\n(V) BILLING ARRANGEMENTS IS: NOT ELECTRONIC FUNDS TRANSFER APPLIES\n(W) PURCHASE CARD IS: YES\n(X) PROCEDURES IS: NOT COMMERCIAL PROCEDURES (FAR PART 12)'
WHERE clause_version_id = 9 AND clause_name = '52.204-7';

UPDATE Clauses
SET clause_conditions = '(A) DOCUMENT TYPE IS: SOLICITATION (D) PROCEDURES IS: NOT COMMERCIAL PROCEDURES (FAR PART 12)' -- '(A) DOCUMENT TYPE IS: SOLICITATION (B) COMMERCIAL ITEMS IS: NOT THE SUPPLIES ARE COMMERCIAL ITEMS (C) COMMERCIAL SERVICES IS: NO (D) PROCEDURES IS: NOT COMMERCIAL PROCEDURES (FAR PART 12)'
, clause_rule = 'A AND D' -- 'A AND B AND C AND D'
WHERE clause_version_id = 9 AND clause_name = '52.204-8';

UPDATE Clauses
SET effective_date = '2015-11-01' -- '2014-12-01'
WHERE clause_version_id = 9 AND clause_name = '52.209-10';

UPDATE Clauses
SET effective_date = '2015-11-01' -- '2014-12-01'
WHERE clause_version_id = 9 AND clause_name = '52.209-2';

UPDATE Clauses
SET commercial_status = 'R (ALWAYS)' -- 'R (RULE APPLIES)'
WHERE clause_version_id = 9 AND clause_name = '52.209-7';

UPDATE Clauses
SET clause_conditions = '(A) DOCUMENT TYPE IS: SOLICITATION\n(B) PERFORMANCE REQUIREMENTS 1 IS: GOVERNMENT-UNIQUE STANDARDS APPLY\n(C) PROCEDURES IS: NOT COMMERCIAL PROCEDURES (FAR PART 12)\n(D) REQUIRING ACTIVITY IS: NOT DEPARTMENT OF DEFENSE' -- '(A) DOCUMENT TYPE IS: SOLICITATION\n(B) PERFORMANCE REQUIREMENTS 1 IS: GOVERNMENT-UNIQUE STANDARDS APPLY\n(C) REQUIRING ACTIVITY IS: NOT DEPARTMENT OF DEFENSE\n(D) PROCEDURES IS: NOT COMMERCIAL PROCEDURES (FAR PART 12)'
WHERE clause_version_id = 9 AND clause_name = '52.211-7';
INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT clause_id, prescription_id FROM Prescriptions, Clauses
WHERE prescription_name IN ('211.107') 
AND clause_id IN (SELECT clause_id FROM Clauses WHERE clause_name = '52.211-7' AND clause_version_id = 9); 

UPDATE Clauses
SET effective_date = '2015-11-01' -- '2015-10-01'
WHERE clause_version_id = 9 AND clause_name = '52.212-3';

UPDATE Clauses
SET effective_date = '2015-11-01' -- '2015-05-01'
WHERE clause_version_id = 9 AND clause_name = '52.212-5';

UPDATE Clauses
SET clause_rule = '(A OR B) AND (D OR E)' -- '(A OR B) AND (E OR D)'
WHERE clause_version_id = 9 AND clause_name = '52.212-5 (DEVIATION 2013-00019)';

UPDATE Clauses
SET clause_conditions = '(A) DOCUMENT TYPE IS: SOLICITATION (B) DOCUMENT TYPE IS: AWARD (C) PROCEDURES IS: SIMPLIFIED ACQUISITION PROCEDURES (FAR PART 13) (D) CONTRACT VALUE IS: GREATER THAN 3500 (E) COMMERCIAL ITEMS IS: NOT THE SUPPLIES ARE COMMERCIAL ITEMS (F) COMMERCIAL SERVICES IS: NO (G) PROCEDURES IS: NOT COMMERCIAL PROCEDURES (FAR PART 12)' -- '(A) DOCUMENT TYPE IS: SOLICITATION (B) DOCUMENT TYPE IS: AWARD (C) PROCEDURES IS: SIMPLIFIED ACQUISITION PROCEDURES (FAR PART 13) (D) CONTRACT VALUE IS: GREATER THAN 3000 (E) COMMERCIAL ITEMS IS: NOT THE SUPPLIES ARE COMMERCIAL ITEMS (F) COMMERCIAL SERVICES IS: NO (G) PROCEDURES IS: NOT COMMERCIAL PROCEDURES (FAR PART 12)'
WHERE clause_version_id = 9 AND clause_name = '52.213-4';

UPDATE Clauses
SET clause_data = '<p>(a) <i>Acceptance period,</i> as used in this provision, means the number of calendar days available to the Government for awarding a contract from the date specified in this solicitation for receipt of bids.</p><p>(b) This provision supersedes any language pertaining to the acceptance period that may appear elsewhere in this solicitation.</p><p>(c) The Government requires a minimum acceptance period of _ calendar days [<i>the Contracting Officer shall insert the number of days</i>].</p><p>(d) In the space provided immediately below, bidders may specify a longer acceptance period than the Government''s minimum requirement.</p><p>The bidder allows the following acceptance period: _ calendar days.</p><p>(e) A bid allowing less than the Government''s minimum acceptance period will be rejected.</p><p>(f) The bidder agrees to execute all that it has undertaken to do, in compliance with its bid, if that bid is accepted in writing within (1) the acceptance period stated in paragraph (c) above or (2) any longer acceptance period stated in paragraph (d) above.</p>'
--               '<p>(a) <i>Acceptance period,</i> as used in this provision, means the number of calendar days available to the Government for awarding a contract from the date specified in this solicitation for receipt of bids.</p><p>(b) This provision supersedes any language pertaining to the acceptance period that may appear elsewhere in this solicitation.</p><p>(c) The Government requires a minimum acceptance period of {{textbox_52.214-16[0]}} calendar days [<i>the Contracting Officer shall insert the number of days</i>].</p><p>(d) In the space provided immediately below, bidders may specify a longer acceptance period than the Government''s minimum requirement.</p><p>The bidder allows the following acceptance period: {{textbox_52.214-16[1]}} calendar days.</p><p>(e) A bid allowing less than the Government''s minimum acceptance period will be rejected.</p><p>(f) The bidder agrees to execute all that it has undertaken to do, in compliance with its bid, if that bid is accepted in writing within (1) the acceptance period stated in paragraph (c) above or (2) any longer acceptance period stated in paragraph (d) above.</p>'
WHERE clause_version_id = 9 AND clause_name = '52.214-16';

DELETE FROM Document_Fill_Ins WHERE clause_fill_in_id IN (SELECT clause_fill_in_id FROM Clause_Fill_Ins CF INNER JOIN Clauses C  ON C.clause_id= CF.clause_id
WHERE clause_name = '52.214-16' AND clause_version_id = 9 AND fill_in_code='textbox_52.214-16[0]');
DELETE FROM Clause_Fill_Ins WHERE clause_id IN (SELECT clause_id FROM Clauses 
WHERE clause_name = '52.214-16' AND clause_version_id = 9)  AND fill_in_code='textbox_52.214-16[0]';
DELETE FROM Document_Fill_Ins WHERE clause_fill_in_id IN (SELECT clause_fill_in_id FROM Clause_Fill_Ins CF INNER JOIN Clauses C  ON C.clause_id= CF.clause_id
WHERE clause_name = '52.214-16' AND clause_version_id = 9 AND fill_in_code='textbox_52.214-16[1]');
DELETE FROM Clause_Fill_Ins WHERE clause_id IN (SELECT clause_id FROM Clauses 
WHERE clause_name = '52.214-16' AND clause_version_id = 9)  AND fill_in_code='textbox_52.214-16[1]';

UPDATE Clauses
SET clause_conditions = '(A) DOCUMENT TYPE IS: INVITATION FOR BIDS\n(B) AWARD SCENARIOS IS: MULTIPLE AWARDS ARE POSSIBLE\n(C) PROCEDURES IS: NOT COMMERCIAL PROCEDURES (FAR PART 12)' -- '(A) DOCUMENT TYPE IS: INVITATION FOR BIDS\n(B) AWARD SCENARIOS IS: MULTIPLE AWARDS ARE POSSIBLE\n(C) PROCEDURES IS: NOT COMMERCIAL PROCEDURES (FAR PART 12)\n(D) PROCEDURES IS: NOT COMMERCIAL PROCEDURES (FAR PART 12)'
, clause_rule = 'A AND B AND C' -- 'A AND B AND C AND D'
WHERE clause_version_id = 9 AND clause_name = '52.214-22';

UPDATE Clauses
SET clause_conditions = '(A) DOCUMENT TYPE IS: SOLICITATION (B) 52.225-1 IS: INCLUDED (C) 52.225-2 IS: INCLUDED (D) 52.225-3 IS: INCLUDED (E) 52.225-3 ALTERNATE I IS: INCLUDED (F) 52.225-3 ALTERNATE II IS: INCLUDED (G) 52.225-3 ALTERNATE III IS: INCLUDED (I) 52.225-4 ALTERNATE I IS: INCLUDED (J) 52.225-4 ALTERNATE II IS: INCLUDED (K) 52.225-4 ALTERNATE III IS: INCLUDED (L) 52.225-5 IS: INCLUDED (M) 52.225-6 IS: INCLUDED (N) 52.225-7 IS: INCLUDED (O) 52.225-8 IS: INCLUDED (P) 52.225-18 IS: INCLUDED (Q) 52.225-9 IS: INCLUDED (R) 52.225-4 IS: INCLUDED (S) 52.225-10 IS: INCLUDED (T) 52.225-10 ALTERNATE I IS: INCLUDED (U) 52.225-11 IS: INCLUDED (V) 52.225-11 ALTERNATE I IS: INCLUDED (W) 52.225-12 IS: INCLUDED (X) 52.225-12 ALTERNATE I IS: INCLUDED (Y) 52.225-12 ALTERNATE II IS: INCLUDED (Z) 52.225-21 IS: INCLUDED (AA) 52.225-22 IS: INCLUDED (AB) 52.225-22 ALTERNATE I IS: INCLUDED (AC) 52.225-23 IS: INCLUDED (AD) 52.225-23 ALTERNATE I IS: INCLUDED (AE) 52.225-24 IS: INCLUDED (AF) 52.225-24 ALTERNATE I IS: INCLUDED (AG) 52.225-24 ALTERNATE II IS: INCLUDED (AH) PROCEDURES IS: NOT COMMERCIAL PROCEDURES (FAR PART 12)' -- '(A) DOCUMENT TYPE IS: SOLICITATION (B) 52.225-1 IS: INCLUDED (C) 52.225-2 IS: INCLUDED (D) 52.225-3 IS: INCLUDED (E) 52.225-3 ALTERNATE I IS: INCLUDED (F) 52.225-3 ALTERNATE II IS: INCLUDED (G) 52.225-3 ALTERNATE III IS: INCLUDED (H) 52.225-4 IS: INCLUDED (I) 52.225-4 ALTERNATE I IS: INCLUDED (J) 52.225-4 ALTERNATE II IS: INCLUDED (K) 52.225-4 ALTERNATE III IS: INCLUDED (L) 52.225-5 IS: INCLUDED (M) 52.225-6 IS: INCLUDED (N) 52.225-7 IS: INCLUDED (O) 52.225-8 IS: INCLUDED (P) 52.225-18 IS: INCLUDED (Q) 52.225-9 IS: INCLUDED (S) 52.225-10 IS: INCLUDED (T) 52.225-10 ALTERNATE I IS: INCLUDED (U) 52.225-11 IS: INCLUDED (V) 52.225-11 ALTERNATE I IS: INCLUDED (W) 52.225-12 IS: INCLUDED (X) 52.225-12 ALTERNATE I IS: INCLUDED (Y) 52.225-12 ALTERNATE II IS: INCLUDED (Z) 52.225-21 IS: INCLUDED (AA) 52.225-22 IS: INCLUDED (AB) 52.225-22 ALTERNATE I IS: INCLUDED (AC) 52.225-23 IS: INCLUDED (AD) 52.225-23 ALTERNATE I IS: INCLUDED (AE) 52.225-24 IS: INCLUDED (AF) 52.225-24 ALTERNATE I IS: INCLUDED (AG) 52.225-24 ALTERNATE II IS: INCLUDED (AH) PROCEDURES IS: NOT COMMERCIAL PROCEDURES (FAR PART 12)'
, clause_rule = 'A AND (B OR C OR D OR E OR F OR G OR H OR I OR J OR K OR L OR M OR N OR O OR P OR Q OR R OR S OR T OR U OR V OR W OR X OR Y OR Z OR AA OR AB OR AC OR AD OR AE OR AF OR AG) AND AH' -- 'A AND (B OR C OR D OR E OR F OR G OR H OR I OR J OR K OR L OR M OR N OR O OR P OR Q OR S OR T OR U OR V OR W OR X OR Y OR Z OR AA OR AB OR AC OR AD OR AE OR AF OR AG) AND AH'
WHERE clause_version_id = 9 AND clause_name = '52.214-34';

UPDATE Clauses
SET clause_conditions = '(A) DOCUMENT TYPE IS: SOLICITATION\n(B) DOCUMENT TYPE IS: AWARD\n(C) CONTRACT VALUE IS: GREATER THAN SAT\n(D) SERVICES IS: UTILITY SERVICES\n(E) UTILITY SERVICE CONDITIONS IS: NOT UTILITY SERVICE RATES DO NOT EXCEED THOSE ESTABLISHED TO APPLY UNIFORMLY TO THE GENERAL PUBLIC, PLUS ANY APPLICABLE REASONABLE CONNECTION CHARGE\n(F) COMMERCIAL ITEMS IS: NOT THE SUPPLIES ARE COMMERCIAL ITEMS\n(G) COMMERCIAL SERVICES IS: NO\n(H) PROCEDURES IS: NEGOTIATION (FAR PART 15)\n(I) LEGISLATIVE SOURCE OF FUNDS IS: NOT RECOVERY ACT FUNDS\n(J) PROCEDURES IS: NOT COMMERCIAL PROCEDURES (FAR PART 12)' -- '(A) DOCUMENT TYPE IS: SOLICITATION (B) DOCUMENT TYPE IS: AWARD (C) CONTRACT VALUE IS: GREATER THAN SAT (D) SERVICES IS: UTILITY SERVICES (E) UTILITY SERVICE CONDITIONS IS: NOT UTILITY SERVICE RATES DO NOT EXCEED THOSE ESTABLISHED TO APPLY UNIFORMLY TO THE GENERAL PUBLIC, PLUS ANY APPLICABLE REASONABLE CONNECTION CHARGE (F) COMMERCIAL ITEMS IS: NOT THE SUPPLIES ARE COMMERCIAL ITEMS (G) COMMERCIAL SERVICES IS: NO (H) PROCEDURES IS: NEGOTIATION (FAR PART 15) (I) LEGISLATIVE SOURCE OF FUNDS IS: NOT RECOVERY ACT FUNDS (J) PROCEDURES IS: NOT COMMERCIAL PROCEDURES (FAR PART 12)'
WHERE clause_version_id = 9 AND clause_name = '52.215-2';

UPDATE Clauses
SET clause_conditions = '(A) DOCUMENT TYPE IS: SOLICITATION (B) PRICING REQUIREMENTS IS: CERTIFIED COST AND PRICING DATA ARE REQUIRED (C) PRICING REQUIREMENTS IS: CONTRACT REQUIRES OTHER THAN CERTIFIED COST AND PRICING DATA (D) PROCEDURES IS: NEGOTIATION (FAR PART 15) (E) PROCEDURES IS: NOT COMMERCIAL PROCEDURES (FAR PART 12) (F) BUSINESS TYPE IS: NOT CANADIAN COMMERCIAL CORPORATION\n(B2) 252.215-7008 IS: INCLUDED\n(E2) REQUIRING ACTIVITY IS: DEPARTMENT OF DEFENSE' -- '(A) DOCUMENT TYPE IS: SOLICITATION (B) PRICING REQUIREMENTS IS: CERTIFIED COST AND PRICING DATA ARE REQUIRED (C) PRICING REQUIREMENTS IS: CONTRACT REQUIRES OTHER THAN CERTIFIED COST AND PRICING DATA (D) PROCEDURES IS: NEGOTIATION (FAR PART 15) (E) PROCEDURES IS: NOT COMMERCIAL PROCEDURES (FAR PART 12)\n(B2) 252.215-7008 IS: INCLUDED\n(E2) REQUIRING ACTIVITY IS: DEPARTMENT OF DEFENSE'
, clause_rule = '(A AND (B OR C) AND D AND E AND F) OR (A AND B2 AND C AND E2)' -- '(A AND (B OR C) AND D AND E) OR (A AND B2 AND C AND E2)'
WHERE clause_version_id = 9 AND clause_name = '52.215-20';

UPDATE Clauses
SET clause_conditions = '(A) DOCUMENT TYPE IS: SOLICITATION (B) DOCUMENT TYPE IS: AWARD (D) CONTRACT VALUE IS: GREATER THAN SAT (E) CONTRACT TYPE IS: COST REIMBURSEMENT (F) ISSUING OFFICE IS: DEPARTMENT OF DEFENSE (G) CONTRACT VALUE IS: GREATER THAN 750000 (H) CONTRACT TYPE IS: FIRM FIXED PRICE (I) ADEQUATE PRICE COMPETITION IS: YES (J) CONTRACT TYPE IS: FIXED PRICE WITH ECONOMIC PRICE ADJUSTMENT -- ACTUAL COSTS (K) CONTRACT TYPE IS: FIXED PRICE WITH ECONOMIC PRICE ADJUSTMENT -- COST INDICES (L) CONTRACT TYPE IS: FIXED PRICE WITH ECONOMIC PRICE ADJUSTMENT -- ESTABLISHED PRICES (M) COMMERCIAL ITEMS IS: THE SUPPLIES ARE COMMERCIAL ITEMS (N) COMMERCIAL SERVICES IS: YES (O) CONTRACT TYPE IS: FIXED PRICE INCENTIVE (COST BASED) (P) CONTRACT TYPE IS: FIXED PRICE INCENTIVE (SUCCESSIVE TARGETS) (Q) PROCEDURES IS: NOT COMMERCIAL PROCEDURES (FAR PART 12)' -- '(A) DOCUMENT TYPE IS: SOLICITATION (B) DOCUMENT TYPE IS: AWARD (C) ISSUING OFFICE IS: NOT DEPARTMENT OF DEFENSE (D) CONTRACT VALUE IS: GREATER THAN SAT (E) CONTRACT TYPE IS: COST REIMBURSEMENT (F) ISSUING OFFICE IS: DEPARTMENT OF DEFENSE (G) CONTRACT VALUE IS: GREATER THAN 750000 (H) CONTRACT TYPE IS: FIRM FIXED PRICE (I) ADEQUATE PRICE COMPETITION IS: YES (J) CONTRACT TYPE IS: FIXED PRICE WITH ECONOMIC PRICE ADJUSTMENT -- ACTUAL COSTS (K) CONTRACT TYPE IS: FIXED PRICE WITH ECONOMIC PRICE ADJUSTMENT -- COST INDICES (L) CONTRACT TYPE IS: FIXED PRICE WITH ECONOMIC PRICE ADJUSTMENT -- ESTABLISHED PRICES (M) COMMERCIAL ITEMS IS: THE SUPPLIES ARE COMMERCIAL ITEMS (N) COMMERCIAL SERVICES IS: YES (O) CONTRACT TYPE IS: FIXED PRICE INCENTIVE (COST BASED) (P) CONTRACT TYPE IS: FIXED PRICE INCENTIVE (SUCCESSIVE TARGETS) (Q) PROCEDURES IS: NOT COMMERCIAL PROCEDURES (FAR PART 12)'
, clause_rule = '((A OR B) AND !F AND D AND E) OR ((A OR B) AND F AND G AND !(H AND I) AND !((J OR K OR L) AND I) AND !(H AND (M OR N)) AND !((J OR K OR L) AND (M OR N)) AND !((O OR P) AND I) AND !((O OR P) AND (M OR N)) AND Q)' -- '((A OR B) AND C AND D AND E) OR ((A OR B) AND !C AND G AND !(H AND I) AND !((J OR K OR L) AND I) AND !(H AND (M OR N)) AND !((J OR K OR L) AND (M OR N)) AND !((O OR P) AND I) AND !((O OR P) AND (M OR N))) AND Q'
WHERE clause_version_id = 9 AND clause_name = '52.215-23';

UPDATE Clauses
SET clause_rule = 'A AND B AND C AND D' -- 'A AND B AND C'
WHERE clause_version_id = 9 AND clause_name = '52.215-3';


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT clause_id, prescription_id FROM Prescriptions, Clauses
WHERE prescription_name IN ('16.30(d)') 
AND clause_id IN (SELECT clause_id FROM Clauses WHERE clause_name = '52.216-10' AND clause_version_id = 9); 
DELETE FROM Clause_Prescriptions
WHERE clause_id IN (SELECT clause_id FROM Clauses WHERE clause_name = '52.216-10' AND clause_version_id = 9) 
AND prescription_id IN (SELECT prescription_id FROM Prescriptions WHERE prescription_name IN ('16.307(d)')); -- 52.216-10

UPDATE Clauses
SET is_editable = 1
, editable_remarks = 'ALL' -- NULL
, clause_data = '{{memobox_fe_52.216-18[0]}}'
--               '<p>(a) Any supplies and services to be furnished under this contract shall be ordered by issuance of delivery orders or task orders by the individuals or activities designated in the Schedule. Such orders may be issued from {{textbox_52.216-18[0]}} through {{textbox_52.216-18[1]}} [<i>insert dates</i>].</p><p>(b) All delivery orders or task orders are subject to the terms and conditions of this contract. In the event of conflict between a delivery order or task order and this contract, the contract shall control.</p><p>(c) If mailed, a delivery order or task order is considered ''issued" when the Government deposits the order in the mail. Orders may be issued orally, by facsimile, or by electronic commerce methods only if authorized in the Schedule.</p>'
WHERE clause_version_id = 9 AND clause_name = '52.216-18';

INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size, fill_in_group_number, fill_in_placeholder, fill_in_default_data, fill_in_display_rows, fill_in_for_table, fill_in_heading) 
SELECT clause_id, 'memobox_fe_52.216-18[0]', 'M', 32000, NULL, NULL, '<p>(a) Any supplies and services to be furnished under this contract shall be ordered by issuance of delivery orders or task orders by the individuals or activities designated in the Schedule. Such orders may be issued from {{textbox_52.216-18[0]}} through {{textbox_52.216-18[1]}} [<i>insert dates</i>].</p><p>(b) All delivery orders or task orders are subject to the terms and conditions of this contract. In the event of conflict between a delivery order or task order and this contract, the contract shall control.</p><p>(c) If mailed, a delivery order or task order is considered &ldquo;issued" when the Government deposits the order in the mail. Orders may be issued orally, by facsimile, or by electronic commerce methods only if authorized in the Schedule.</p>', 12, 0, NULL FROM Clauses WHERE clause_name = '52.216-18' AND clause_version_id = 9; 

UPDATE Clauses
SET clause_data = '<p>(a) The Contractor warrants that the unit price stated in the Schedule for {{textbox_52.216-2[0]}} [<i>offeror insert Schedule line item number</i>] is not in excess of the Contractor''s applicable established price in effect on the contract date for like quantities of the same item. The term <i>unit price</i> excludes any part of the price directly resulting from requirements for preservation, packaging, or packing beyond standard commercial practice. The term <i>established price</i> means a price that (1) is an established catalog or market price for a commercial item sold in substantial quantities to the general public, and (2) is the net price after applying any standard trade discounts offered by the Contractor.</p><p>(b) The Contractor shall promptly notify the Contracting Officer of the amount and effective date of each decrease in any applicable established price. Each corresponding contract unit price shall be decreased by the same percentage that the established price is decreased. The decrease shall apply to those items delivered on and after the effective date of the decrease in the Contractor''s established price, and this contract shall be modified accordingly.</p><p>(c) If the Contractor''s applicable established price is increased after the contract date, the corresponding contract unit price shall be increased, upon the Contractor''s written request to the Contracting Officer, by the same percentage that the established price is increased, and the contract shall be modified accordingly, subject to the following limitations:</p><p>(1) The aggregate of the increases in any contract unit price under this clause shall not exceed 10 percent of the original contract unit price.</p><p>(2) The increased contract unit price shall be effective (i) on the effective date of the increase in the applicable established price if the Contracting Officer receives the Contractor''s written request within 10 days thereafter or (ii) if the written request is received later, on the date the Contracting Officer receives the request.</p><p>(3) The increased contract unit price shall not apply to quantities scheduled under the contract for delivery before the effective date of the increased contract unit price, unless failure to deliver before that date results from causes beyond the control and without the fault or negligence of the Contractor, within the meaning of the Default clause.</p><p>(4) No modification increasing a contract unit price shall be executed under this paragraph (c) until the Contracting Officer verifies the increase in the applicable established price.</p><p>(5) Within 30 days after receipt of the Contractor''s written request, the Contracting Officer may cancel, without liability to either party, any undelivered portion of the contract items affected by the requested increase.</p><p>(d) During the time allowed for the cancellation provided for in subparagraph (c)(5) above, and thereafter if there is no cancellation, the Contractor shall continue deliveries according to the contract delivery schedule, and the Government shall pay for such deliveries at the contract unit price, increased to the extent provided by paragraph (c) above.</p>'
--               '<p>(a) The Contractor warrants that the unit price stated in the Schedule for __ [<i>offeror insert Schedule line item number</i>] is not in excess of the Contractor''s applicable established price in effect on the contract date for like quantities of the same item. The term <i>unit price</i> excludes any part of the price directly resulting from requirements for preservation, packaging, or packing beyond standard commercial practice. The term <i>established price</i> means a price that (1) is an established catalog or market price for a commercial item sold in substantial quantities to the general public, and (2) is the net price after applying any standard trade discounts offered by the Contractor.</p><p>(b) The Contractor shall promptly notify the Contracting Officer of the amount and effective date of each decrease in any applicable established price. Each corresponding contract unit price shall be decreased by the same percentage that the established price is decreased. The decrease shall apply to those items delivered on and after the effective date of the decrease in the Contractor''s established price, and this contract shall be modified accordingly.</p><p>(c) If the Contractor''s applicable established price is increased after the contract date, the corresponding contract unit price shall be increased, upon the Contractor''s written request to the Contracting Officer, by the same percentage that the established price is increased, and the contract shall be modified accordingly, subject to the following limitations:</p><p>(1) The aggregate of the increases in any contract unit price under this clause shall not exceed 10 percent of the original contract unit price.</p><p>(2) The increased contract unit price shall be effective (i) on the effective date of the increase in the applicable established price if the Contracting Officer receives the Contractor''s written request within 10 days thereafter or (ii) if the written request is received later, on the date the Contracting Officer receives the request.</p><p>(3) The increased contract unit price shall not apply to quantities scheduled under the contract for delivery before the effective date of the increased contract unit price, unless failure to deliver before that date results from causes beyond the control and without the fault or negligence of the Contractor, within the meaning of the Default clause.</p><p>(4) No modification increasing a contract unit price shall be executed under this paragraph (c) until the Contracting Officer verifies the increase in the applicable established price.</p><p>(5) Within 30 days after receipt of the Contractor''s written request, the Contracting Officer may cancel, without liability to either party, any undelivered portion of the contract items affected by the requested increase.</p><p>(d) During the time allowed for the cancellation provided for in subparagraph (c)(5) above, and thereafter if there is no cancellation, the Contractor shall continue deliveries according to the contract delivery schedule, and the Government shall pay for such deliveries at the contract unit price, increased to the extent provided by paragraph (c) above.</p>'
WHERE clause_version_id = 9 AND clause_name = '52.216-2';

INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size, fill_in_group_number, fill_in_placeholder, fill_in_default_data, fill_in_display_rows, fill_in_for_table, fill_in_heading) 
SELECT clause_id, 'textbox_52.216-2[0]', 'S', 100, NULL, NULL, NULL, NULL, 0, NULL FROM Clauses WHERE clause_name = '52.216-2' AND clause_version_id = 9; 

UPDATE Clauses
SET clause_data = '<p>(a) The Contractor warrants that the supplies identified as line items {{textbox_52.216-3[0]}} [<i>offeror insert Schedule line item number</i>] in the Schedule are, except for modifications required by the contract specifications, supplies for which it has an established price. The term <i>established price</i> means a price that (1) is an established catalog or market price for a commercial item sold in substantial quantities to the general public, and (2) is the net price after applying any standard trade discounts offered by the Contractor. The Contractor further warrants that, as of the date of this contract, any difference between the unit prices stated in the contract for these line items and the Contractor''s established prices for like quantities of the nearest commercial equivalents are due to compliance with contract specifications and with any contract requirements for preservation, packaging, and packing beyond standard commercial practice.</p><p>(b) The Contractor shall promptly notify the Contracting Officer of the amount and effective date of each decrease in any applicable established price. Each corresponding contract unit price (exclusive of any part of the unit price that reflects modifications resulting from compliance with specifications or with requirements for preservation, packaging, and packing beyond standard commercial practice) shall be decreased by the same percentage that the established price is decreased. The decrease shall apply to those items delivered on and after the effective date of the decrease in the Contractor''s established price, and this contract shall be modified accordingly.</p><p>(c) If the Contractor''s applicable established price is increased after the contract date, the corresponding contract unit price (exclusive of any part of the unit price resulting from compliance with specifications or with requirements for preservation, packaging, and packing beyond standard commercial practice) shall be increased, upon the Contractor''s written request to the Contracting Officer, by the same percentage that the established price is increased, and the contract shall be modified accordingly, subject to the following limitations:</p><p>(1) The aggregate of the increases in any contract unit price under this clause shall not exceed 10 percent of the original contract unit price.</p><p>(2) The increased contract unit price shall be effective (i) on the effective date of the increase in the applicable established price if the Contracting Officer receives the Contractor''s written request within 10 days thereafter or (ii) if the written request is received later, on the date the Contracting Officer receives the request.</p><p>(3) The increased contract unit price shall not apply to quantities scheduled under the contract for delivery before the effective date of the increased contract unit price, unless failure to deliver before that date results from causes beyond the control and without the fault or negligence of the Contractor, within the meaning of the Default clause.</p><p>(4) No modification increasing a contract unit price shall be executed under this paragraph (c) until the Contracting Officer verifies the increase in the applicable established price.</p><p>(5) Within 30 days after receipt of the Contractor''s written request, the Contracting Officer may cancel, without liability to either party, any undelivered portion of the contract items affected by the requested increase.</p><p>(d) During the time allowed for the cancellation provided for in subparagraph (c)(5) above, and thereafter if there is no cancellation, the Contractor shall continue deliveries according to the contract delivery schedule, and the Government shall pay for such deliveries at the contract unit price, increased to the extent provided by paragraph (c) above.</p>'
--               '<p>(a) The Contractor warrants that the supplies identified as line items __ [<i>offeror insert Schedule line item number</i>] in the Schedule are, except for modifications required by the contract specifications, supplies for which it has an established price. The term <i>established price</i> means a price that (1) is an established catalog or market price for a commercial item sold in substantial quantities to the general public, and (2) is the net price after applying any standard trade discounts offered by the Contractor. The Contractor further warrants that, as of the date of this contract, any difference between the unit prices stated in the contract for these line items and the Contractor''s established prices for like quantities of the nearest commercial equivalents are due to compliance with contract specifications and with any contract requirements for preservation, packaging, and packing beyond standard commercial practice.</p><p>(b) The Contractor shall promptly notify the Contracting Officer of the amount and effective date of each decrease in any applicable established price. Each corresponding contract unit price (exclusive of any part of the unit price that reflects modifications resulting from compliance with specifications or with requirements for preservation, packaging, and packing beyond standard commercial practice) shall be decreased by the same percentage that the established price is decreased. The decrease shall apply to those items delivered on and after the effective date of the decrease in the Contractor''s established price, and this contract shall be modified accordingly.</p><p>(c) If the Contractor''s applicable established price is increased after the contract date, the corresponding contract unit price (exclusive of any part of the unit price resulting from compliance with specifications or with requirements for preservation, packaging, and packing beyond standard commercial practice) shall be increased, upon the Contractor''s written request to the Contracting Officer, by the same percentage that the established price is increased, and the contract shall be modified accordingly, subject to the following limitations:</p><p>(1) The aggregate of the increases in any contract unit price under this clause shall not exceed 10 percent of the original contract unit price.</p><p>(2) The increased contract unit price shall be effective (i) on the effective date of the increase in the applicable established price if the Contracting Officer receives the Contractor''s written request within 10 days thereafter or (ii) if the written request is received later, on the date the Contracting Officer receives the request.</p><p>(3) The increased contract unit price shall not apply to quantities scheduled under the contract for delivery before the effective date of the increased contract unit price, unless failure to deliver before that date results from causes beyond the control and without the fault or negligence of the Contractor, within the meaning of the Default clause.</p><p>(4) No modification increasing a contract unit price shall be executed under this paragraph (c) until the Contracting Officer verifies the increase in the applicable established price.</p><p>(5) Within 30 days after receipt of the Contractor''s written request, the Contracting Officer may cancel, without liability to either party, any undelivered portion of the contract items affected by the requested increase.</p><p>(d) During the time allowed for the cancellation provided for in subparagraph (c)(5) above, and thereafter if there is no cancellation, the Contractor shall continue deliveries according to the contract delivery schedule, and the Government shall pay for such deliveries at the contract unit price, increased to the extent provided by paragraph (c) above.</p>'
WHERE clause_version_id = 9 AND clause_name = '52.216-3';

INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size, fill_in_group_number, fill_in_placeholder, fill_in_default_data, fill_in_display_rows, fill_in_for_table, fill_in_heading) 
SELECT clause_id, 'textbox_52.216-3[0]', 'S', 100, NULL, NULL, NULL, NULL, 0, NULL FROM Clauses WHERE clause_name = '52.216-3' AND clause_version_id = 9; 



UPDATE Clause_Fill_Ins SET fill_in_default_data = '<p>(a) <i>Definitions.</i> As used in this provision&mdash;</p><p><i>Economically disadvantaged women-owned small business (EDWOSB) concern</i> means a small business concern that is at least 51 percent directly and unconditionally owned by, and the management and daily business operations of which are controlled by, one or more women who are citizens of the United States and who are economically disadvantaged in accordance with 13 CFR part 127. It automatically qualifies as a women-owned small business concern eligible under the WOSB Program.</p><p><i>Service-disabled veteran-owned small business concern</i>&mdash;</p><p>(1) Means a small business concern&mdash;</p><p>(i) Not less than 51 percent of which is owned by one or more service-disabled veterans or, in the case of any publicly owned business, not less than 51 percent of the stock of which is owned by one or more service-disabled veterans; and</p><p>(ii) The management and daily business operations of which are controlled by one or more service-disabled veterans or, in the case of a service-disabled veteran with permanent and severe disability, the spouse or permanent caregiver of such veteran.</p><p>(2) <i>Service-disabled veteran</i> means a veteran, as defined in 38 U.S.C. 101(2), with a disability that is service-connected, as defined in 38 U.S.C. 101(16).</p><p><i>Small business concern</i> means a concern, including its affiliates, that is independently owned and operated, not dominant in the field of operation in which it is bidding on Government contracts, and qualified as a small business under the criteria in 13 CFR Part 121 and the size standard in paragraph (b) of this provision.</p><p><i>Small disadvantaged business concern,</i> consistent with 13 CFR 124.1002, means a small business concern under the size standard applicable to the acquisition, that&mdash;</p><p>(1) Is at least 51 percent unconditionally and directly owned (as defined at 13 CFR 124.105) by&mdash;</p><p>(i) One or more socially disadvantaged (as defined at 13 CFR 124.103) and economically disadvantaged (as defined at 13 CFR 124.104) individuals who are citizens of the United States, and</p><p>(ii) Each individual claiming economic disadvantage has a net worth not exceeding $750,000 after taking into account the applicable exclusions set forth at 13 CFR 124.104(c)(2); and</p><p>(2) The management and daily business operations of which are controlled (as defined at 13 CFR 124.106) by individuals who meet the criteria in paragraphs (1)(i) and (ii) of this definition.</p><p><i>Veteran-owned small business concern</i> means a small business concern&mdash;</p><p>(1) Not less than 51 percent of which is owned by one or more veterans (as defined at 38 U.S.C. 101(2)) or, in the case of any publicly owned business, not less than 51 percent of the stock of which is owned by one or more veterans; and</p><p>(2) The management and daily business operations of which are controlled by one or more veterans.</p><p><i>Women-owned small business concern</i> means a small business concern&mdash;</p><p>(1) That is at least 51 percent owned by one or more women; or, in the case of any publicly owned business, at least 51 percent of the stock of which is owned by one or more women; and</p><p>(2) Whose management and daily business operations are controlled by one or more women.</p><p><i>Women-owned small business (WOSB) concern eligible under the WOSB Program</i> (in accordance with 13 CFR part 127), means a small business concern that is at least 51 percent directly and unconditionally owned by, and the management and daily business operations of which are controlled by, one or more women who are citizens of the United States.</p><p>(b)(1) The North American Industry Classification System (NAICS) code for this acquisition is&mdash;______[<i>insert NAICS code</i>].</p><p>(2) The small business size standard is ___[<i>insert size standard</i>].</p><p>(3) The small business size standard for a concern which submits an offer in its own name, other than on a construction or service contract, but which proposes to furnish a product which it did not itself manufacture, is 500 employees.</p><p>(c) <i>Representations.</i> (1) The offeror represents as part of its offer that it <b>&#x2610;</b> is, <b>&#x2610;</b> is not a small business concern.</p><p>(2) [<i>Complete only if the offeror represented itself as a small business concern in paragraph (c)(1) of this provision.</i>] The offeror represents that it <b>&#x2610;</b> is, <b>&#x2610;</b> is not, a small disadvantaged business concern as defined in 13 CFR 124.1002.</p><p>(3) [<i>Complete only if the offeror represented itself as a small business concern in paragraph (c)(1) of this provision.</i>] The offeror represents as part of its offer that it <b>&#x2610;</b> is, <b>&#x2610;</b> is not a women-owned small business concern.</p><p>(4) Women-owned small business (WOSB) concern eligible under the WOSB Program. [<i>Complete only if the offeror represented itself as a women-owned small business concern in paragraph (c)(3) of this provision.</i>] The offeror represents as part of its offer that&mdash;</p><p>(i) It <b>&#x2610;</b> is, <b>&#x2610;</b> is not a WOSB concern eligible under the WOSB Program, has provided all the required documents to the WOSB Repository, and no change in circumstances or adverse decisions have been issued that affects its eligibility; and</p><p>(ii) It <b>&#x2610;</b> is, <b>&#x2610;</b> is not a joint venture that complies with the requirements of 13 CFR part 127, and the representation in paragraph (c)(4)(i) of this provision is accurate for each WOSB concern eligible under the WOSB Program participating in the joint venture. [<i>The offeror shall enter the name or names of the WOSB concern eligible under the WOSB Program and other small businesses that are participating in the joint venture:</i> ____.] Each WOSB concern eligible under the WOSB Program participating in the joint venture shall submit a separate signed copy of the WOSB representation.</p><p>(5) Economically disadvantaged women-owned small business (EDWOSB) concern. [<i>Complete only if the offeror represented itself as a women-owned small business concern eligible under the WOSB Program in (c)(4) of this provision.</i>] The offeror represents as part of its offer that&mdash;</p><p>(i) It <b>&#x2610;</b> is, <b>&#x2610;</b> is not an EDWOSB concern eligible under the WOSB Program, has provided all the required documents to the WOSB Repository, and no change in circumstances or adverse decisions have been issued that affects its eligibility; and</p><p>(ii) It <b>&#x2610;</b> is, <b>&#x2610;</b> is not a joint venture that complies with the requirements of 13 CFR part 127, and the representation in paragraph (c)(5)(i) of this provision is accurate for each EDWOSB concern participating in the joint venture. [<i>The offeror shall enter the name or names of the EDWOSB concern and other small businesses that are participating in the joint venture:</i> ____.] Each EDWOSB concern participating in the joint venture shall submit a separate signed copy of the EDWOSB representation.</p><p>(6) [<i>Complete only if the offeror represented itself as a small business concern in paragraph (c)(1) of this provision.</i>] The offeror represents as part of its offer that it <b>&#x2610;</b> is, <b>&#x2610;</b> is not a veteran-owned small business concern.</p><p>(7) [<i>Complete only if the offeror represented itself as a veteran-owned small business concern in paragraph (c)(6) of this provision.</i>] The offeror represents as part of its offer that it <b>&#x2610;</b> is, <b>&#x2610;</b> is not a service-disabled veteran-owned small business concern.</p><p>(8) [<i>Complete only if the offeror represented itself as a small business concern in paragraph (c)(1) of this provision.</i>] The offeror represents, as part of its offer, that&mdash;</p><p>(i) It <b>&#x2610;</b> is, <b>&#x2610;</b> is not a HUBZone small business concern listed, on the date of this representation, on the List of Qualified HUBZone Small Business Concerns maintained by the Small Business Administration, and no material changes in ownership and control, principal office, or HUBZone employee percentage have occurred since it was certified in accordance with 13 CFR Part 126; and</p><p>(ii) It <b>&#x2610;</b> is, <b>&#x2610;</b> is not a HUBZone joint venture that complies with the requirements of 13 CFR Part 126, and the representation in paragraph (c)(8)(i) of this provision is accurate for each HUBZone small business concern participating in the HUBZone joint venture. [<i>The offeror shall enter the names of each of the HUBZone small business concerns participating in the HUBZone joint venture:</i> ____.] Each HUBZone small business concern participating in the HUBZone joint venture shall submit a separate signed copy of the HUBZone representation.</p><p>(d) <i>Notice.</i> (1) If this solicitation is for supplies and has been set aside, in whole or in part, for small business concerns, then the clause in this solicitation providing notice of the set-aside contains restrictions on the source of the end items to be furnished.</p><p>(2) Under 15 U.S.C. 645(d), any person who misrepresents a firm''s status as a business concern that is small, HUBZone small, small disadvantaged, service-disabled veteran-owned small, economically disadvantaged women-owned small, or women-owned small eligible under the WOSB Program in order to obtain a contract to be awarded under the preference programs established pursuant to section 8, 9, 15, 31, and 36 of the Small Business Act or any other provision of Federal law that specifically references section 8(d) for a definition of program eligibility, shall&mdash;</p><p>(i) Be punished by imposition of fine, imprisonment, or both;</p><p>(ii) Be subject to administrative remedies, including suspension and debarment; and</p><p>(iii) Be ineligible for participation in programs conducted under the authority of the Act.</p>' WHERE fill_in_code = 'memobox_fe_52.219-1[0]'
AND clause_id IN (SELECT clause_id FROM Clauses 
WHERE clause_name = '52.219-1' AND clause_version_id = 9) ;
DELETE FROM Document_Fill_Ins WHERE clause_fill_in_id IN (SELECT clause_fill_in_id FROM Clause_Fill_Ins CF INNER JOIN Clauses C  ON C.clause_id= CF.clause_id
WHERE clause_name = '52.219-1' AND clause_version_id = 9 AND fill_in_code='checkbox_52.219-1[0]');
DELETE FROM Clause_Fill_Ins WHERE clause_id IN (SELECT clause_id FROM Clauses 
WHERE clause_name = '52.219-1' AND clause_version_id = 9)  AND fill_in_code='checkbox_52.219-1[0]';
DELETE FROM Document_Fill_Ins WHERE clause_fill_in_id IN (SELECT clause_fill_in_id FROM Clause_Fill_Ins CF INNER JOIN Clauses C  ON C.clause_id= CF.clause_id
WHERE clause_name = '52.219-1' AND clause_version_id = 9 AND fill_in_code='checkbox_52.219-1[10]');
DELETE FROM Clause_Fill_Ins WHERE clause_id IN (SELECT clause_id FROM Clauses 
WHERE clause_name = '52.219-1' AND clause_version_id = 9)  AND fill_in_code='checkbox_52.219-1[10]';
DELETE FROM Document_Fill_Ins WHERE clause_fill_in_id IN (SELECT clause_fill_in_id FROM Clause_Fill_Ins CF INNER JOIN Clauses C  ON C.clause_id= CF.clause_id
WHERE clause_name = '52.219-1' AND clause_version_id = 9 AND fill_in_code='checkbox_52.219-1[11]');
DELETE FROM Clause_Fill_Ins WHERE clause_id IN (SELECT clause_id FROM Clauses 
WHERE clause_name = '52.219-1' AND clause_version_id = 9)  AND fill_in_code='checkbox_52.219-1[11]';
DELETE FROM Document_Fill_Ins WHERE clause_fill_in_id IN (SELECT clause_fill_in_id FROM Clause_Fill_Ins CF INNER JOIN Clauses C  ON C.clause_id= CF.clause_id
WHERE clause_name = '52.219-1' AND clause_version_id = 9 AND fill_in_code='checkbox_52.219-1[12]');
DELETE FROM Clause_Fill_Ins WHERE clause_id IN (SELECT clause_id FROM Clauses 
WHERE clause_name = '52.219-1' AND clause_version_id = 9)  AND fill_in_code='checkbox_52.219-1[12]';
DELETE FROM Document_Fill_Ins WHERE clause_fill_in_id IN (SELECT clause_fill_in_id FROM Clause_Fill_Ins CF INNER JOIN Clauses C  ON C.clause_id= CF.clause_id
WHERE clause_name = '52.219-1' AND clause_version_id = 9 AND fill_in_code='checkbox_52.219-1[13]');
DELETE FROM Clause_Fill_Ins WHERE clause_id IN (SELECT clause_id FROM Clauses 
WHERE clause_name = '52.219-1' AND clause_version_id = 9)  AND fill_in_code='checkbox_52.219-1[13]';
DELETE FROM Document_Fill_Ins WHERE clause_fill_in_id IN (SELECT clause_fill_in_id FROM Clause_Fill_Ins CF INNER JOIN Clauses C  ON C.clause_id= CF.clause_id
WHERE clause_name = '52.219-1' AND clause_version_id = 9 AND fill_in_code='checkbox_52.219-1[14]');
DELETE FROM Clause_Fill_Ins WHERE clause_id IN (SELECT clause_id FROM Clauses 
WHERE clause_name = '52.219-1' AND clause_version_id = 9)  AND fill_in_code='checkbox_52.219-1[14]';
DELETE FROM Document_Fill_Ins WHERE clause_fill_in_id IN (SELECT clause_fill_in_id FROM Clause_Fill_Ins CF INNER JOIN Clauses C  ON C.clause_id= CF.clause_id
WHERE clause_name = '52.219-1' AND clause_version_id = 9 AND fill_in_code='checkbox_52.219-1[15]');
DELETE FROM Clause_Fill_Ins WHERE clause_id IN (SELECT clause_id FROM Clauses 
WHERE clause_name = '52.219-1' AND clause_version_id = 9)  AND fill_in_code='checkbox_52.219-1[15]';
DELETE FROM Document_Fill_Ins WHERE clause_fill_in_id IN (SELECT clause_fill_in_id FROM Clause_Fill_Ins CF INNER JOIN Clauses C  ON C.clause_id= CF.clause_id
WHERE clause_name = '52.219-1' AND clause_version_id = 9 AND fill_in_code='checkbox_52.219-1[16]');
DELETE FROM Clause_Fill_Ins WHERE clause_id IN (SELECT clause_id FROM Clauses 
WHERE clause_name = '52.219-1' AND clause_version_id = 9)  AND fill_in_code='checkbox_52.219-1[16]';
DELETE FROM Document_Fill_Ins WHERE clause_fill_in_id IN (SELECT clause_fill_in_id FROM Clause_Fill_Ins CF INNER JOIN Clauses C  ON C.clause_id= CF.clause_id
WHERE clause_name = '52.219-1' AND clause_version_id = 9 AND fill_in_code='checkbox_52.219-1[17]');
DELETE FROM Clause_Fill_Ins WHERE clause_id IN (SELECT clause_id FROM Clauses 
WHERE clause_name = '52.219-1' AND clause_version_id = 9)  AND fill_in_code='checkbox_52.219-1[17]';
DELETE FROM Document_Fill_Ins WHERE clause_fill_in_id IN (SELECT clause_fill_in_id FROM Clause_Fill_Ins CF INNER JOIN Clauses C  ON C.clause_id= CF.clause_id
WHERE clause_name = '52.219-1' AND clause_version_id = 9 AND fill_in_code='checkbox_52.219-1[18]');
DELETE FROM Clause_Fill_Ins WHERE clause_id IN (SELECT clause_id FROM Clauses 
WHERE clause_name = '52.219-1' AND clause_version_id = 9)  AND fill_in_code='checkbox_52.219-1[18]';
DELETE FROM Document_Fill_Ins WHERE clause_fill_in_id IN (SELECT clause_fill_in_id FROM Clause_Fill_Ins CF INNER JOIN Clauses C  ON C.clause_id= CF.clause_id
WHERE clause_name = '52.219-1' AND clause_version_id = 9 AND fill_in_code='checkbox_52.219-1[19]');
DELETE FROM Clause_Fill_Ins WHERE clause_id IN (SELECT clause_id FROM Clauses 
WHERE clause_name = '52.219-1' AND clause_version_id = 9)  AND fill_in_code='checkbox_52.219-1[19]';
DELETE FROM Document_Fill_Ins WHERE clause_fill_in_id IN (SELECT clause_fill_in_id FROM Clause_Fill_Ins CF INNER JOIN Clauses C  ON C.clause_id= CF.clause_id
WHERE clause_name = '52.219-1' AND clause_version_id = 9 AND fill_in_code='checkbox_52.219-1[1]');
DELETE FROM Clause_Fill_Ins WHERE clause_id IN (SELECT clause_id FROM Clauses 
WHERE clause_name = '52.219-1' AND clause_version_id = 9)  AND fill_in_code='checkbox_52.219-1[1]';
DELETE FROM Document_Fill_Ins WHERE clause_fill_in_id IN (SELECT clause_fill_in_id FROM Clause_Fill_Ins CF INNER JOIN Clauses C  ON C.clause_id= CF.clause_id
WHERE clause_name = '52.219-1' AND clause_version_id = 9 AND fill_in_code='checkbox_52.219-1[20]');
DELETE FROM Clause_Fill_Ins WHERE clause_id IN (SELECT clause_id FROM Clauses 
WHERE clause_name = '52.219-1' AND clause_version_id = 9)  AND fill_in_code='checkbox_52.219-1[20]';
DELETE FROM Document_Fill_Ins WHERE clause_fill_in_id IN (SELECT clause_fill_in_id FROM Clause_Fill_Ins CF INNER JOIN Clauses C  ON C.clause_id= CF.clause_id
WHERE clause_name = '52.219-1' AND clause_version_id = 9 AND fill_in_code='checkbox_52.219-1[21]');
DELETE FROM Clause_Fill_Ins WHERE clause_id IN (SELECT clause_id FROM Clauses 
WHERE clause_name = '52.219-1' AND clause_version_id = 9)  AND fill_in_code='checkbox_52.219-1[21]';
DELETE FROM Document_Fill_Ins WHERE clause_fill_in_id IN (SELECT clause_fill_in_id FROM Clause_Fill_Ins CF INNER JOIN Clauses C  ON C.clause_id= CF.clause_id
WHERE clause_name = '52.219-1' AND clause_version_id = 9 AND fill_in_code='checkbox_52.219-1[2]');
DELETE FROM Clause_Fill_Ins WHERE clause_id IN (SELECT clause_id FROM Clauses 
WHERE clause_name = '52.219-1' AND clause_version_id = 9)  AND fill_in_code='checkbox_52.219-1[2]';
DELETE FROM Document_Fill_Ins WHERE clause_fill_in_id IN (SELECT clause_fill_in_id FROM Clause_Fill_Ins CF INNER JOIN Clauses C  ON C.clause_id= CF.clause_id
WHERE clause_name = '52.219-1' AND clause_version_id = 9 AND fill_in_code='checkbox_52.219-1[3]');
DELETE FROM Clause_Fill_Ins WHERE clause_id IN (SELECT clause_id FROM Clauses 
WHERE clause_name = '52.219-1' AND clause_version_id = 9)  AND fill_in_code='checkbox_52.219-1[3]';
DELETE FROM Document_Fill_Ins WHERE clause_fill_in_id IN (SELECT clause_fill_in_id FROM Clause_Fill_Ins CF INNER JOIN Clauses C  ON C.clause_id= CF.clause_id
WHERE clause_name = '52.219-1' AND clause_version_id = 9 AND fill_in_code='checkbox_52.219-1[4]');
DELETE FROM Clause_Fill_Ins WHERE clause_id IN (SELECT clause_id FROM Clauses 
WHERE clause_name = '52.219-1' AND clause_version_id = 9)  AND fill_in_code='checkbox_52.219-1[4]';
DELETE FROM Document_Fill_Ins WHERE clause_fill_in_id IN (SELECT clause_fill_in_id FROM Clause_Fill_Ins CF INNER JOIN Clauses C  ON C.clause_id= CF.clause_id
WHERE clause_name = '52.219-1' AND clause_version_id = 9 AND fill_in_code='checkbox_52.219-1[5]');
DELETE FROM Clause_Fill_Ins WHERE clause_id IN (SELECT clause_id FROM Clauses 
WHERE clause_name = '52.219-1' AND clause_version_id = 9)  AND fill_in_code='checkbox_52.219-1[5]';
DELETE FROM Document_Fill_Ins WHERE clause_fill_in_id IN (SELECT clause_fill_in_id FROM Clause_Fill_Ins CF INNER JOIN Clauses C  ON C.clause_id= CF.clause_id
WHERE clause_name = '52.219-1' AND clause_version_id = 9 AND fill_in_code='checkbox_52.219-1[6]');
DELETE FROM Clause_Fill_Ins WHERE clause_id IN (SELECT clause_id FROM Clauses 
WHERE clause_name = '52.219-1' AND clause_version_id = 9)  AND fill_in_code='checkbox_52.219-1[6]';
DELETE FROM Document_Fill_Ins WHERE clause_fill_in_id IN (SELECT clause_fill_in_id FROM Clause_Fill_Ins CF INNER JOIN Clauses C  ON C.clause_id= CF.clause_id
WHERE clause_name = '52.219-1' AND clause_version_id = 9 AND fill_in_code='checkbox_52.219-1[7]');
DELETE FROM Clause_Fill_Ins WHERE clause_id IN (SELECT clause_id FROM Clauses 
WHERE clause_name = '52.219-1' AND clause_version_id = 9)  AND fill_in_code='checkbox_52.219-1[7]';
DELETE FROM Document_Fill_Ins WHERE clause_fill_in_id IN (SELECT clause_fill_in_id FROM Clause_Fill_Ins CF INNER JOIN Clauses C  ON C.clause_id= CF.clause_id
WHERE clause_name = '52.219-1' AND clause_version_id = 9 AND fill_in_code='checkbox_52.219-1[8]');
DELETE FROM Clause_Fill_Ins WHERE clause_id IN (SELECT clause_id FROM Clauses 
WHERE clause_name = '52.219-1' AND clause_version_id = 9)  AND fill_in_code='checkbox_52.219-1[8]';
DELETE FROM Document_Fill_Ins WHERE clause_fill_in_id IN (SELECT clause_fill_in_id FROM Clause_Fill_Ins CF INNER JOIN Clauses C  ON C.clause_id= CF.clause_id
WHERE clause_name = '52.219-1' AND clause_version_id = 9 AND fill_in_code='checkbox_52.219-1[9]');
DELETE FROM Clause_Fill_Ins WHERE clause_id IN (SELECT clause_id FROM Clauses 
WHERE clause_name = '52.219-1' AND clause_version_id = 9)  AND fill_in_code='checkbox_52.219-1[9]';
DELETE FROM Document_Fill_Ins WHERE clause_fill_in_id IN (SELECT clause_fill_in_id FROM Clause_Fill_Ins CF INNER JOIN Clauses C  ON C.clause_id= CF.clause_id
WHERE clause_name = '52.219-1' AND clause_version_id = 9 AND fill_in_code='textbox_52.219-1[0]');
DELETE FROM Clause_Fill_Ins WHERE clause_id IN (SELECT clause_id FROM Clauses 
WHERE clause_name = '52.219-1' AND clause_version_id = 9)  AND fill_in_code='textbox_52.219-1[0]';
DELETE FROM Document_Fill_Ins WHERE clause_fill_in_id IN (SELECT clause_fill_in_id FROM Clause_Fill_Ins CF INNER JOIN Clauses C  ON C.clause_id= CF.clause_id
WHERE clause_name = '52.219-1' AND clause_version_id = 9 AND fill_in_code='textbox_52.219-1[1]');
DELETE FROM Clause_Fill_Ins WHERE clause_id IN (SELECT clause_id FROM Clauses 
WHERE clause_name = '52.219-1' AND clause_version_id = 9)  AND fill_in_code='textbox_52.219-1[1]';
DELETE FROM Document_Fill_Ins WHERE clause_fill_in_id IN (SELECT clause_fill_in_id FROM Clause_Fill_Ins CF INNER JOIN Clauses C  ON C.clause_id= CF.clause_id
WHERE clause_name = '52.219-1' AND clause_version_id = 9 AND fill_in_code='textbox_52.219-1[2]');
DELETE FROM Clause_Fill_Ins WHERE clause_id IN (SELECT clause_id FROM Clauses 
WHERE clause_name = '52.219-1' AND clause_version_id = 9)  AND fill_in_code='textbox_52.219-1[2]';
DELETE FROM Document_Fill_Ins WHERE clause_fill_in_id IN (SELECT clause_fill_in_id FROM Clause_Fill_Ins CF INNER JOIN Clauses C  ON C.clause_id= CF.clause_id
WHERE clause_name = '52.219-1' AND clause_version_id = 9 AND fill_in_code='textbox_52.219-1[3]');
DELETE FROM Clause_Fill_Ins WHERE clause_id IN (SELECT clause_id FROM Clauses 
WHERE clause_name = '52.219-1' AND clause_version_id = 9)  AND fill_in_code='textbox_52.219-1[3]';
DELETE FROM Document_Fill_Ins WHERE clause_fill_in_id IN (SELECT clause_fill_in_id FROM Clause_Fill_Ins CF INNER JOIN Clauses C  ON C.clause_id= CF.clause_id
WHERE clause_name = '52.219-1' AND clause_version_id = 9 AND fill_in_code='textbox_52.219-1[4]');
DELETE FROM Clause_Fill_Ins WHERE clause_id IN (SELECT clause_id FROM Clauses 
WHERE clause_name = '52.219-1' AND clause_version_id = 9)  AND fill_in_code='textbox_52.219-1[4]';

UPDATE Clauses
SET clause_conditions = '(A) DOCUMENT TYPE IS: SOLICITATION (B) DOCUMENT TYPE IS: AWARD (C) PROCEDURES IS: COMPETITIVE (8a) (FAR PART 19.8) (D) PROCEDURES IS: NOT COMMERCIAL PROCEDURES (FAR PART 12)\n(C2) PROCEDURES IS: 8(a) PROGRAM' -- '(A) DOCUMENT TYPE IS: SOLICITATION (B) DOCUMENT TYPE IS: AWARD (C) PROCEDURES IS: COMPETITIVE (8a) (FAR PART 19.8) (D) PROCEDURES IS: NOT COMMERCIAL PROCEDURES (FAR PART 12)'
, clause_rule = '((A OR B) AND C AND D) OR ((A OR B) AND C2 )' -- '(A OR B) AND C AND D'
WHERE clause_version_id = 9 AND clause_name = '52.219-18';
INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT clause_id, prescription_id FROM Prescriptions, Clauses
WHERE prescription_name IN ('219.811-3(2)') 
AND clause_id IN (SELECT clause_id FROM Clauses WHERE clause_name = '52.219-18' AND clause_version_id = 9); 

UPDATE Clauses
SET clause_data = '<p>(a) <i>Definitions.</i> As used in this clause-</p><p><i>Long-term contract</i> means a contract of more than five years in duration, including options. However, the term does not include contracts that exceed five years in duration because the period of performance has been extended for a cumulative period not to exceed six months under the clause at 52.217-8, Option to Extend Services, or other appropriate authority.</p><p><i>Small business concern</i> means a concern, including its affiliates, that is independently owned and operated, not dominant in the field of operation in which it is bidding on Government contracts, and qualified as a small business under the criteria in 13 CFR part 121 and the size standard in paragraph (c) of this clause. Such a concern is ''not dominant in its field of operation" when it does not exercise a controlling or major influence on a national basis in a kind of business activity in which a number of business concerns are primarily engaged. In determining whether dominance exists, consideration shall be given to all appropriate factors, including volume of business, number of employees, financial resources, competitive status or position, ownership or control of materials, processes, patents, license agreements, facilities, sales territory, and nature of business activity.</p><p>(b) If the Contractor represented that it was a small business concern prior to award of this contract, the Contractor shall rerepresent its size status according to paragraph (e) of this clause or, if applicable, paragraph (g) of this clause, upon the occurrence of any of the following:</p><p>(1) Within 30 days after execution of a novation agreement or within 30 days after modification of the contract to include this clause, if the novation agreement was executed prior to inclusion of this clause in the contract.</p><p>(2) Within 30 days after a merger or acquisition that does not require a novation or within 30 days after modification of the contract to include this clause, if the merger or acquisition occurred prior to inclusion of this clause in the contract.</p><p>(3) For long-term contracts-</p><p>(i) Within 60 to 120 days prior to the end of the fifth year of the contract; and</p><p>(ii) Within 60 to 120 days prior to the date specified in the contract for exercising any option thereafter.</p><p>(c) The Contractor shall rerepresent its size status in accordance with the size standard in effect at the time of this rerepresentation that corresponds to the North American Industry Classification System (NAICS) code assigned to this contract. The small business size standard corresponding to this NAICS code can be found at <i>http://www.sba.gov/content/table-small-business-size-standards.</i></p><p>(d) The small business size standard for a Contractor providing a product which it does not manufacture itself, for a contract other than a construction or service contract, is 500 employees.</p><p>(e) Except as provided in paragraph (g) of this clause, the Contractor shall make the representation required by paragraph (b) of this clause by validating or updating all its representations in the Representations and Certifications section of the System for Award Management (SAM) and its other data in SAM, as necessary, to ensure that they reflect the Contractor''s current status. The Contractor shall notify the contracting office in writing within the timeframes specified in paragraph (b) of this clause that the data have been validated or updated, and provide the date of the validation or update.</p><p>(f) If the Contractor represented that it was other than a small business concern prior to award of this contract, the Contractor may, but is not required to, take the actions required by paragraphs (e) or (g) of this clause.</p><p>(g) If the Contractor does not have representations and certifications in SAM, or does not have a representation in SAM for the NAICS code applicable to this contract, the Contractor is required to complete the following rerepresentation and submit it to the contracting office, along with the contract number and the date on which the rerepresentation was completed:</p><p>The Contractor represents that it is, is not a small business concern under NAICS Code {{textbox_52.219-28[0]}}{{textbox_52.219-28[1]}} assigned to contract number {{textbox_52.219-28[2]}}{{textbox_52.219-28[3]}}.</p><p>Contractor Name: {{textbox_52.219-28[4]}} Date: {{textbox_52.219-28[5]}} Signer''s Name: {{textbox_52.219-28[6]}} Title: {{textbox_52.219-28[7]}} <br>[<i>Contractor to sign and date and insert authorized signer''s name and title</i>].</br></p>'
--               '<p>(a) <i>Definitions.</i> As used in this clause-</p><p><i>Long-term contract</i> means a contract of more than five years in duration, including options. However, the term does not include contracts that exceed five years in duration because the period of performance has been extended for a cumulative period not to exceed six months under the clause at 52.217-8, Option to Extend Services, or other appropriate authority.</p><p><i>Small business concern</i> means a concern, including its affiliates, that is independently owned and operated, not dominant in the field of operation in which it is bidding on Government contracts, and qualified as a small business under the criteria in 13 CFR part 121 and the size standard in paragraph (c) of this clause. Such a concern is ''not dominant in its field of operation" when it does not exercise a controlling or major influence on a national basis in a kind of business activity in which a number of business concerns are primarily engaged. In determining whether dominance exists, consideration shall be given to all appropriate factors, including volume of business, number of employees, financial resources, competitive status or position, ownership or control of materials, processes, patents, license agreements, facilities, sales territory, and nature of business activity.</p><p>(b) If the Contractor represented that it was a small business concern prior to award of this contract, the Contractor shall rerepresent its size status according to paragraph (e) of this clause or, if applicable, paragraph (g) of this clause, upon the occurrence of any of the following:</p><p>(1) Within 30 days after execution of a novation agreement or within 30 days after modification of the contract to include this clause, if the novation agreement was executed prior to inclusion of this clause in the contract.</p><p>(2) Within 30 days after a merger or acquisition that does not require a novation or within 30 days after modification of the contract to include this clause, if the merger or acquisition occurred prior to inclusion of this clause in the contract.</p><p>(3) For long-term contracts-</p><p>(i) Within 60 to 120 days prior to the end of the fifth year of the contract; and</p><p>(ii) Within 60 to 120 days prior to the date specified in the contract for exercising any option thereafter.</p><p>(c) The Contractor shall rerepresent its size status in accordance with the size standard in effect at the time of this rerepresentation that corresponds to the North American Industry Classification System (NAICS) code assigned to this contract. The small business size standard corresponding to this NAICS code can be found at <i>http://www.sba.gov/content/table-small-business-size-standards.</i></p><p>(d) The small business size standard for a Contractor providing a product which it does not manufacture itself, for a contract other than a construction or service contract, is 500 employees.</p><p>(e) Except as provided in paragraph (g) of this clause, the Contractor shall make the representation required by paragraph (b) of this clause by validating or updating all its representations in the Representations and Certifications section of the System for Award Management (SAM) and its other data in SAM, as necessary, to ensure that they reflect the Contractor''s current status. The Contractor shall notify the contracting office in writing within the timeframes specified in paragraph (b) of this clause that the data have been validated or updated, and provide the date of the validation or update.</p><p>(f) If the Contractor represented that it was other than a small business concern prior to award of this contract, the Contractor may, but is not required to, take the actions required by paragraphs (e) or (g) of this clause.</p><p>(g) If the Contractor does not have representations and certifications in SAM, or does not have a representation in SAM for the NAICS code applicable to this contract, the Contractor is required to complete the following rerepresentation and submit it to the contracting office, along with the contract number and the date on which the rerepresentation was completed:</p><p>The Contractor represents that it is, is not a small business concern under NAICS Code {{textbox_52.219-28[0]}} assigned to contract number {{textbox_52.219-28[1]}}.</p><p>Contractor Name: {{textbox_52.219-28[2]}} Date: {{textbox_52.219-28[3]}} Signer''s Name: {{textbox_52.219-28[4]}} Title: {{textbox_52.219-28[5]}} <br>[<i>Contractor to sign and date and insert authorized signer''s name and title</i>].</br></p>'
WHERE clause_version_id = 9 AND clause_name = '52.219-28';

INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size, fill_in_group_number, fill_in_placeholder, fill_in_default_data, fill_in_display_rows, fill_in_for_table, fill_in_heading) 
SELECT clause_id, 'textbox_52.219-28[6]', 'S', 100, NULL, NULL, NULL, NULL, 0, NULL FROM Clauses WHERE clause_name = '52.219-28' AND clause_version_id = 9; 
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size, fill_in_group_number, fill_in_placeholder, fill_in_default_data, fill_in_display_rows, fill_in_for_table, fill_in_heading) 
SELECT clause_id, 'textbox_52.219-28[7]', 'S', 100, NULL, NULL, NULL, NULL, 0, NULL FROM Clauses WHERE clause_name = '52.219-28' AND clause_version_id = 9; 

UPDATE Clauses
SET clause_data = '<p>(a) <i>Definitions. See</i> 13 CFR 125.6(e) for definitions of terms used in paragraph (d).</p><p>(b) <i>Evaluation preference.</i> (1) Offers will be evaluated by adding a factor of 10 percent to the price of all offers, except-</p><p>(i) Offers from HUBZone small business concerns that have not waived the evaluation preference; and</p><p>(ii) Otherwise successful offers from small business concerns.</p><p>(2) The factor of 10 percent shall be applied on a line item basis or to any group of items on which award may be made. Other evaluation factors described in the solicitation shall be applied before application of the factor.</p><p>(3) When the two highest rated offerors are a HUBZone small business concern and a large business, and the evaluated offer of the HUBZone small business concern is equal to the evaluated offer of the large business after considering the price evaluation preference, award will be made to the HUBZone small business concern.</p><p>(c) <i>Waiver of evaluation preference.</i> A HUBZone small business concern may elect to waive the evaluation preference, in which case the factor will be added to its offer for evaluation purposes. The agreements in paragraphs (d) and (e) of this clause do not apply if the offeror has waived the evaluation preference.</p><p>{{textbox_52.219-4[0]}}&nbsp;&nbsp;Offeror elects to waive the evaluation preference.</p><p>(d) <i>Agreement.</i> A HUBZone small business concern agrees that in the performance of the contract, in the case of a contract for</p><p>(1) Services (except construction), at least 50 percent of the cost of personnel for contract performance will be spent for employees of the concern or employees of other HUBZone small business concerns;</p><p>(2) Supplies (other than procurement from a nonmanufacturer of such supplies), at least 50 percent of the cost of manufacturing, excluding the cost of materials, will be performed by the concern or other HUBZone small business concerns;</p><p>(3) <i>General construction.</i> (i) At least 15 percent of the cost of contract performance to be incurred for personnel will be spent on the prime contractor''s employees;</p><p>(ii) At least 50 percent of the cost of the contract performance to be incurred for personnel will be spent on the prime contractor''s employees or on a combination of the prime contractor''s employees and employees of HUBZone small business concern subcontractors;</p><p>(iii) No more than 50 percent of the cost of contract performance to be incurred for personnel will be subcontracted to concerns that are not HUBZone small business concerns; or</p><p>(4) <i>Construction by special trade contractors.</i> (i) At least 25 percent of the cost of contract performance to be incurred for personnel will be spent on the prime contractor''s employees;</p><p>(ii) At least 50 percent of the cost of the contract performance to be incurred for personnel will be spent on the prime contractor''s employees or on a combination of the prime contractor''s employees and employees of HUBZone small business concern subcontractors;</p><p>(iii) No more than 50 percent of the cost of contract performance to be incurred for personnel will be subcontracted to concerns that are not HUBZone small business concerns.</p><p>(e) A HUBZone joint venture agrees that the aggregate of the HUBZone small business concerns to the joint venture, not each concern separately, will perform the applicable percentage of work requirements.</p><p>(f)(1) When the total value of the contract exceeds $25,000, a HUBZone small business concern nonmanufacturer agrees to furnish in performing this contract only end items manufactured or produced by HUBZone small business concern manufacturers.</p><p>(2) When the total value of the contract is equal to or less than $25,000, a HUBZone small business concern nonmanufacturer may provide end items manufactured by other than a HUBZone small business concern manufacturer provided the end items are produced or manufactured in the United States.</p><p>(3) Paragraphs (f)(1) and (f)(2) of this section do not apply in connection with construction or service contracts.</p><p>(g) <i>Notice.</i> The HUBZone small business offeror acknowledges that a prospective HUBZone awardee must be a HUBZone small business concern at the time of award of this contract. The HUBZone offeror shall provide the Contracting Officer a copy of the notice required by 13 CFR 126.501 if material changes occur before contract award that could affect its HUBZone eligibility. If the apparently successful HUBZone offeror is not a HUBZone small business concern at the time of award of this contract, the Contracting Officer will proceed to award to the next otherwise successful HUBZone small business concern or other offeror.</p>'
--               '<p>(a) <i>Definitions. See</i> 13 CFR 125.6(e) for definitions of terms used in paragraph (d).</p><p>(b) <i>Evaluation preference.</i> (1) Offers will be evaluated by adding a factor of 10 percent to the price of all offers, except-</p><p>(i) Offers from HUBZone small business concerns that have not waived the evaluation preference; and</p><p>(ii) Otherwise successful offers from small business concerns.</p><p>(2) The factor of 10 percent shall be applied on a line item basis or to any group of items on which award may be made. Other evaluation factors described in the solicitation shall be applied before application of the factor.</p><p>(3) When the two highest rated offerors are a HUBZone small business concern and a large business, and the evaluated offer of the HUBZone small business concern is equal to the evaluated offer of the large business after considering the price evaluation preference, award will be made to the HUBZone small business concern.</p><p>(c) <i>Waiver of evaluation preference.</i> A HUBZone small business concern may elect to waive the evaluation preference, in which case the factor will be added to its offer for evaluation purposes. The agreements in paragraphs (d) and (e) of this clause do not apply if the offeror has waived the evaluation preference.</p><p>____&nbsp;&nbsp;Offeror elects to waive the evaluation preference.</p><p>(d) <i>Agreement.</i> A HUBZone small business concern agrees that in the performance of the contract, in the case of a contract for</p><p>(1) Services (except construction), at least 50 percent of the cost of personnel for contract performance will be spent for employees of the concern or employees of other HUBZone small business concerns;</p><p>(2) Supplies (other than procurement from a nonmanufacturer of such supplies), at least 50 percent of the cost of manufacturing, excluding the cost of materials, will be performed by the concern or other HUBZone small business concerns;</p><p>(3) <i>General construction.</i> (i) At least 15 percent of the cost of contract performance to be incurred for personnel will be spent on the prime contractor''s employees;</p><p>(ii) At least 50 percent of the cost of the contract performance to be incurred for personnel will be spent on the prime contractor''s employees or on a combination of the prime contractor''s employees and employees of HUBZone small business concern subcontractors;</p><p>(iii) No more than 50 percent of the cost of contract performance to be incurred for personnel will be subcontracted to concerns that are not HUBZone small business concerns; or</p><p>(4) <i>Construction by special trade contractors.</i> (i) At least 25 percent of the cost of contract performance to be incurred for personnel will be spent on the prime contractor''s employees;</p><p>(ii) At least 50 percent of the cost of the contract performance to be incurred for personnel will be spent on the prime contractor''s employees or on a combination of the prime contractor''s employees and employees of HUBZone small business concern subcontractors;</p><p>(iii) No more than 50 percent of the cost of contract performance to be incurred for personnel will be subcontracted to concerns that are not HUBZone small business concerns.</p><p>(e) A HUBZone joint venture agrees that the aggregate of the HUBZone small business concerns to the joint venture, not each concern separately, will perform the applicable percentage of work requirements.</p><p>(f)(1) When the total value of the contract exceeds $25,000, a HUBZone small business concern nonmanufacturer agrees to furnish in performing this contract only end items manufactured or produced by HUBZone small business concern manufacturers.</p><p>(2) When the total value of the contract is equal to or less than $25,000, a HUBZone small business concern nonmanufacturer may provide end items manufactured by other than a HUBZone small business concern manufacturer provided the end items are produced or manufactured in the United States.</p><p>(3) Paragraphs (f)(1) and (f)(2) of this section do not apply in connection with construction or service contracts.</p><p>(g) <i>Notice.</i> The HUBZone small business offeror acknowledges that a prospective HUBZone awardee must be a HUBZone small business concern at the time of award of this contract. The HUBZone offeror shall provide the Contracting Officer a copy of the notice required by 13 CFR 126.501 if material changes occur before contract award that could affect its HUBZone eligibility. If the apparently successful HUBZone offeror is not a HUBZone small business concern at the time of award of this contract, the Contracting Officer will proceed to award to the next otherwise successful HUBZone small business concern or other offeror.</p>'
WHERE clause_version_id = 9 AND clause_name = '52.219-4';

INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size, fill_in_group_number, fill_in_placeholder, fill_in_default_data, fill_in_display_rows, fill_in_for_table, fill_in_heading) 
SELECT clause_id, 'textbox_52.219-4[0]', 'S', 100, NULL, NULL, NULL, NULL, 0, NULL FROM Clauses WHERE clause_name = '52.219-4' AND clause_version_id = 9; 


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT clause_id, prescription_id FROM Prescriptions, Clauses
WHERE prescription_name IN ('19.508(d)') 
AND clause_id IN (SELECT clause_id FROM Clauses WHERE clause_name = '52.219-6' AND clause_version_id = 9); 

UPDATE Clauses
SET provision_or_clause = 'C' -- 'I'
WHERE clause_version_id = 9 AND clause_name = '52.222-11';

UPDATE Clauses
SET clause_rule = 'A AND B AND (C OR D) AND E AND F' -- 'A AND B AND (C OR D) AND E'
WHERE clause_version_id = 9 AND clause_name = '52.222-25';

UPDATE Clauses
SET effective_date = '2015-10-01' -- '2014-07-01'
WHERE clause_version_id = 9 AND clause_name = '52.222-35';

UPDATE Clauses
SET effective_date = '2015-10-01' -- '2014-07-01'
WHERE clause_version_id = 9 AND clause_name = '52.222-37';

UPDATE Clauses
SET is_optional = 0
WHERE clause_version_id = 9 AND clause_name = '52.222-43';

UPDATE Clauses
SET effective_date = '2015-10-01' -- '2013-08-01'
WHERE clause_version_id = 9 AND clause_name = '52.222-54';

UPDATE Clauses
SET commercial_status = 'R (ALWAYS)' -- 'R (RULE APPLIES)'
WHERE clause_version_id = 9 AND clause_name = '52.222-56';

UPDATE Clauses
SET clause_rule = '(A OR B) AND (C OR (D AND (E OR F))) AND G' -- '(A OR B) AND C OR (D AND (E OR F)) AND G'
WHERE clause_version_id = 9 AND clause_name = '52.223-10';

UPDATE Clauses
SET effective_date = '2014-06-01' -- '1995-05-01'
, clause_conditions = '(A) DOCUMENT TYPE IS: SOLICITATION (B) DOCUMENT TYPE IS: AWARD (C) SUPPLIES IS: IMAGING EQUIPMENT (D) CONTRACTOR MATERIAL IS: CONTRACTOR FURNISHING OF IMAGING EQUIPMENT FOR USE IN PERFORMING SERVICES AT A FEDERALLY CONTROLLED FACILITY (E) CONTRACTOR MATERIAL IS: IMAGING EQUIPMENT WILL BE FURNISHED BY THE CONTRACTOR FOR USE BY THE GOVERNMENT (F) EXCEPTIONS IS: 3.704(a) - EXCEPTION TO EPEAT REGISTERED ELECTRONIC PRODUCTS' -- '(A) DOCUMENT TYPE IS: SOLICITATION (B) DOCUMENT TYPE IS: AWARD (C) SUPPLIES IS: IMAGING EQUIPMENT (D) CONTRACTOR MATERIAL IS: CONTRACTOR FURNISHING OF IMAGING EQUIPMENT FOR USE IN PERFORMING SERVICES AT A FEDERALLY CONTROLLED FACILITY (E) CONTRACTOR MATERIAL IS: IMAGING EQUIPMENT WILL BE FURNISHED BY THE CONTRACTOR FOR USE BY THE GOVERNMENT (F) EXCEPTIONS IS: 23.704(a) - EXCEPTION TO EPEAT REGISTERED ELECTRONIC PRODUCTS'
WHERE clause_version_id = 9 AND clause_name = '52.223-13';

UPDATE Clauses
SET clause_conditions = '(A) DOCUMENT TYPE IS: SOLICITATION (B) DOCUMENT TYPE IS: AWARD (C) LEGISLATIVE SOURCE OF FUNDS IS: NOT RECOVERY ACT FUNDS (D) SERVICES IS: CONSTRUCTION (E) PLACE OF PERFORMANCE IS: UNITED STATES (50 STATES PLUS DISTRICT OF COLUMBIA) (F) CONTRACT VALUE IS: GREATER THAN OR EQUAL TO 7864000 (G) PLACE OF PERFORMANCE IS: UNITED STATES OUTLYING AREAS (H) PROCEDURES IS: NOT COMMERCIAL PROCEDURES (FAR PART 12)' -- '(A) DOCUMENT TYPE IS: SOLICITATION\n(B) DOCUMENT TYPE IS: AWARD\n(C) LEGISLATIVE SOURCE OF FUNDS IS: NOT RECOVERY ACT FUNDS\n(D) SERVICES IS: CONSTRUCTION\n(E) PLACE OF PERFORMANCE IS: UNITED STATES (50 STATES PLUS DISTRICT OF COLUMBIA)\n(F) CONTRACT VALUE IS: GREATER THAN OR EQUAL TO 7864000\n(G) PLACE OF PERFORMANCE IS: UNITED STATES OUTLYING AREAS\n(H) PROCEDURES IS: NOT COMMERCIAL PROCEDURES (FAR PART 12)'
WHERE clause_version_id = 9 AND clause_name = '52.225-11';

UPDATE Clauses
SET commercial_status = 'R (ALWAYS)' -- 'R (RULE APPLIES)'
WHERE clause_version_id = 9 AND clause_name = '52.225-19';

UPDATE Clauses
SET effective_date = NULL -- '2014-05-01'
WHERE clause_version_id = 9 AND clause_name = '52.225-3';
INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT clause_id, prescription_id FROM Prescriptions, Clauses
WHERE prescription_name IN ('13.302-5(d)(3)(i)') 
AND clause_id IN (SELECT clause_id FROM Clauses WHERE clause_name = '52.225-3' AND clause_version_id = 9); 

UPDATE Clauses
SET clause_data = '<p>(a) <i>Set-aside area.</i> The area covered in this contract is: {{textbox_52.226-3[0]}}{{textbox_52.226-3[1]}}_ [<i>Contracting Officer to fill in with definite geographic boundaries.</i>]</p><p>(b) <i>Representations.</i> The offeror represents that it ___does {{checkbox_52.226-3[0]}} does not reside or primarily do business in the set-aside area.</p><p>(c) An offeror is considered to be residing or primarily doing business in the set-aside area if, during the last twelve months-</p><p>(1) The offeror had its main operating office in the area; and</p><p>(2) That office generated at least half of the offeror''s gross revenues and employed at least half of the offeror''s permanent employees.</p><p>(d) If the offeror does not meet the criteria in paragraph (c) of this provision, factors to be considered in determining whether an offeror resides or primarily does business in the set-aside area include-</p><p>(1) Physical location(s) of the offeror''s permanent office(s) and date any office in the set-aside area(s) was established;</p><p>(2) Current state licenses;</p><p>(3) Record of past work in the set-aside area(s) (<i>e.g.</i>, how much and for how long);</p><p>(4) Contractual history the offeror has had with subcontractors and/or suppliers in the set-aside area;</p><p>(5) Percentage of the offeror''s gross revenues attributable to work performed in the set-aside area;</p><p>(6) Number of permanent employees the offeror employs in the set-aside area;</p><p>(7) Membership in local and state organizations in the set-aside area; and</p><p>(8) Other evidence that establishes the offeror resides or primarily does business in the set-aside area. For example, sole proprietorships may submit utility bills and bank statements.</p><p>(e) If the offeror represents it resides or primarily does business in the set-aside area, the offeror shall furnish documentation to support its representation if requested by the Contracting Officer. The solicitation may require the offeror to submit with its offer documentation to support the representation.</p>'
--               '<p>(a) <i>Set-aside area.</i> The area covered in this contract is: {{textbox_52.226-3[0]}} [<i>Contracting Officer to fill in with definite geographic boundaries.</i>]</p><p>(b) <i>Representations.</i> The offeror represents that it {{checkbox_52.226-3[0]}} does {{checkbox_52.226-3[1]}} does not reside or primarily do business in the set-aside area.</p><p>(c) An offeror is considered to be residing or primarily doing business in the set-aside area if, during the last twelve months-</p><p>(1) The offeror had its main operating office in the area; and</p><p>(2) That office generated at least half of the offeror''s gross revenues and employed at least half of the offeror''s permanent employees.</p><p>(d) If the offeror does not meet the criteria in paragraph (c) of this provision, factors to be considered in determining whether an offeror resides or primarily does business in the set-aside area include-</p><p>(1) Physical location(s) of the offeror''s permanent office(s) and date any office in the set-aside area(s) was established;</p><p>(2) Current state licenses;</p><p>(3) Record of past work in the set-aside area(s) (<i>e.g.</i>, how much and for how long);</p><p>(4) Contractual history the offeror has had with subcontractors and/or suppliers in the set-aside area;</p><p>(5) Percentage of the offeror''s gross revenues attributable to work performed in the set-aside area;</p><p>(6) Number of permanent employees the offeror employs in the set-aside area;</p><p>(7) Membership in local and state organizations in the set-aside area; and</p><p>(8) Other evidence that establishes the offeror resides or primarily does business in the set-aside area. For example, sole proprietorships may submit utility bills and bank statements.</p><p>(e) If the offeror represents it resides or primarily does business in the set-aside area, the offeror shall furnish documentation to support its representation if requested by the Contracting Officer. The solicitation may require the offeror to submit with its offer documentation to support the representation.</p>'
WHERE clause_version_id = 9 AND clause_name = '52.226-3';

INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size, fill_in_group_number, fill_in_placeholder, fill_in_default_data, fill_in_display_rows, fill_in_for_table, fill_in_heading) 
SELECT clause_id, 'textbox_52.226-3[1]', 'S', 100, NULL, NULL, NULL, NULL, 0, NULL FROM Clauses WHERE clause_name = '52.226-3' AND clause_version_id = 9; 
DELETE FROM Document_Fill_Ins WHERE clause_fill_in_id IN (SELECT clause_fill_in_id FROM Clause_Fill_Ins CF INNER JOIN Clauses C  ON C.clause_id= CF.clause_id
WHERE clause_name = '52.226-3' AND clause_version_id = 9 AND fill_in_code='checkbox_52.226-3[1]');
DELETE FROM Clause_Fill_Ins WHERE clause_id IN (SELECT clause_id FROM Clauses 
WHERE clause_name = '52.226-3' AND clause_version_id = 9)  AND fill_in_code='checkbox_52.226-3[1]';

UPDATE Clauses
SET clause_rule = '(A OR B) AND C AND (D OR E) AND F AND G' -- '(A OR B) AND C AND (D OR E) AND F'
WHERE clause_version_id = 9 AND clause_name = '52.226-6';

UPDATE Clauses
SET clause_data = '<p>(a) <i>Definitions.</i> As used in this clause-</p><p><i>Original contract price</i> means the award price of the contract; or, for requirements contracts, the price payable for the estimated total quantity; or, for indefinite-quantity contracts, the price payable for the specified minimum quantity. Original contract price does not include the price of any options, except those options exercised at the time of contract award. </p><p>(b) <i>Amount of required bonds.</i> Unless the resulting contract price is $150,000 or less, the successful offeror shall furnish performance and payment bonds to the Contracting Officer as follows:</p><p>(1) <i>Performance bonds (Standard Form 25).</i> The penal amount of performance bonds at the time of contract award shall be {{performance_bonds_percent_52.228-15}} percent of the original contract price.</p><p>(2) <i>Payment Bonds (Standard Form 25-A).</i> The penal amount of payment bonds at the time of contract award shall be {{payment_bonds_percent_52.228-15}} percent of the original contract price.</p><p>(3) <i>Additional bond protection.</i> (i) The Government may require additional performance and payment bond protection if the contract price is increased. The increase in protection generally will equal 100 percent of the increase in contract price.</p><p>(ii) The Government may secure the additional protection by directing the Contractor to increase the penal amount of the existing bond or to obtain an additional bond.</p><p>(c) {{paragraph_c_52.228-15}}</p><p>(d) <i>Surety or other security for bonds.</i> The bonds shall be in the form of firm commitment, supported by corporate sureties whose names appear on the list contained in Treasury Department Circular 570, individual sureties, or by other acceptable security such as postal money order, certified check, cashier''s check, irrevocable letter of credit, or, in accordance with Treasury Department regulations, certain bonds or notes of the United States. Treasury Circular 570 is published in the Federal Register or may be obtained from the U.S. Department of the Treasury, Financial Management Service, Surety Bond Branch, 3700 East West Highway, Room 6F01, Hyattsville, MD 20782. Or via the internet at <i>http://www.fms.treas.gov/c570/.</i></p><p>(e) <i>Notice of subcontractor waiver of protection (40 U.S.C. 3133(c)).</i> Any waiver of the right to sue on the payment bond is void unless it is in writing, signed by the person whose right is waived, and executed after such person has first furnished labor or material for use in the performance of the contract.</p>'
--               '<p>(a) <i>Definitions.</i> As used in this clause-</p><p><i>Original contract price</i> means the award price of the contract; or, for requirements contracts, the price payable for the estimated total quantity; or, for indefinite-quantity contracts, the price payable for the specified minimum quantity. Original contract price does not include the price of any options, except those options exercised at the time of contract award. </p><p>(b) <i>Amount of required bonds.</i> Unless the resulting contract price is $150,000 or less, the successful offeror shall furnish performance and payment bonds to the Contracting Officer as follows:</p><p>(1) <i>Performance bonds (Standard Form 25).</i> The penal amount of performance bonds at the time of contract award shall be {{performance_bonds_percent_52.228-15}} percent of the original contract price.</p><p>(2) <i>Payment Bonds (Standard Form 25-A).</i> The penal amount of payment bonds at the time of contract award shall be 100 percent of the original contract price.</p><p>(3) <i>Additional bond protection.</i> (i) The Government may require additional performance and payment bond protection if the contract price is increased. The increase in protection generally will equal 100 percent of the increase in contract price.</p><p>(ii) The Government may secure the additional protection by directing the Contractor to increase the penal amount of the existing bond or to obtain an additional bond.</p><p>(c) {{paragraph_c_52.228-15}}</p><p>(d) <i>Surety or other security for bonds.</i> The bonds shall be in the form of firm commitment, supported by corporate sureties whose names appear on the list contained in Treasury Department Circular 570, individual sureties, or by other acceptable security such as postal money order, certified check, cashier''s check, irrevocable letter of credit, or, in accordance with Treasury Department regulations, certain bonds or notes of the United States. Treasury Circular 570 is published in the Federal Register or may be obtained from the U.S. Department of the Treasury, Financial Management Service, Surety Bond Branch, 3700 East West Highway, Room 6F01, Hyattsville, MD 20782. Or via the internet at <i>http://www.fms.treas.gov/c570/.</i></p><p>(e) <i>Notice of subcontractor waiver of protection (40 U.S.C. 3133(c)).</i> Any waiver of the right to sue on the payment bond is void unless it is in writing, signed by the person whose right is waived, and executed after such person has first furnished labor or material for use in the performance of the contract.</p>'
WHERE clause_version_id = 9 AND clause_name = '52.228-15';

UPDATE Clauses
SET clause_conditions = '(A) DOCUMENT TYPE IS: SOLICITATION\n(B) DOCUMENT TYPE IS: AWARD\n(C) ISSUING OFFICE IS: DEFENSE ADVANCED RESEARCH PROJECTS AGENCY\n(D) ISSUING OFFICE IS: DEFENSE THREAT REDUCTION AGENCY\n(E) ISSUING OFFICE IS: DEPARTMENT OF AGRICULTURE\n(F) ISSUING OFFICE IS: DEPARTMENT OF THE AIR FORCE\n(G) ISSUING OFFICE IS: DEPARTMENT OF THE ARMY\n(H) ISSUING OFFICE IS: DEPARTMENT OF ENERGY\n(I) ISSUING OFFICE IS: DEPARTMENT OF HEALTH AND HUMAN SERVICES\n(J) ISSUING OFFICE IS: DEPARTMENT OF INTERIOR\n(K) ISSUING OFFICE IS: DEPARTMENT OF LABOR\n(L) ISSUING OFFICE IS: DEPARTMENT OF THE NAVY/MARINE CORPS\n(M) ISSUING OFFICE IS: DEPARTMENT OF TRANSPORTATION\n(N) ISSUING OFFICE IS: GENERAL SERVICES ADMINISTRATION\n(O) ISSUING OFFICE IS: MISSILE DEFENSE AGENCY\n(P) ISSUING OFFICE IS: NATIONAL AERONAUTICS AND SPACE ADMINISTRATION\n(Q) CONTRACT TYPE IS: COST REIMBURSEMENT\n(R) GOVERNMENT FURNISHED PROPERTY IS: THE CONTRACTOR IS DIRECTED TO ACQUIRE PROPERTY FOR THE CONTRACT THAT IS TITLED IN THE GOVERNMENT\n(S) SUPPLIES OR SERVICES IS: SERVICES\n(T) STATE IS: NEW MEXICO\n(U) PROCEDURES IS: NOT COMMERCIAL PROCEDURES (FAR PART 12)' -- '(A) DOCUMENT TYPE IS: SOLICITATION\n(B) DOCUMENT TYPE IS: AWARD\n(C) ISSUING OFFICE IS: DEFENSE ADVANCED RESEARCH PROJECTS AGENCY\n(D) ISSUING OFFICE IS: DEFENSE THREAT REDUCTION AGENCY\n(E) ISSUING OFFICE IS: DEPARTMENT OF AGRICULTURE\n(F) ISSUING OFFICE IS: DEPARTMENT OF THE AIR FORCE\n(G) ISSUING OFFICE IS: DEPARTMENT OF THE ARMY\n(H) ISSUING OFFICE IS: DEPARTMENT OF ENERGY\n(I) ISSUING OFFICE IS: DEPARTMENT OF HEALTH AND HUMAN SERVICES\n(J) ISSUING OFFICE IS: DEPARTMENT OF INTERIOR\n(K) ISSUING OFFICE IS: DEPARTMENT OF LABOR\n(L) ISSUING OFFICE IS: DEPARTMENT OF THE NAVY/MARINE CORPS\n(M) ISSUING OFFICE IS: DEPARTMENT OF TRANSPORTATION\n(N) ISSUING OFFICE IS: GENERAL SERVICES ADMINISTRATION\n(O) ISSUING OFFICE IS: MISSILE DEFENSE AGENCY\n(P) REQUIRING ACTIVITY IS: NATIONAL SPACE AND AERONAUTICS ADMINISTRATION\n(Q) CONTRACT TYPE IS: COST REIMBURSEMENT\n(R) GOVERNMENT FURNISHED PROPERTY IS: THE CONTRACTOR IS DIRECTED TO ACQUIRE PROPERTY FOR THE CONTRACT THAT IS TITLED IN THE GOVERNMENT\n(S) SUPPLIES OR SERVICES IS: SERVICES\n(T) STATE IS: NEW MEXICO\n(U) PROCEDURES IS: NOT COMMERCIAL PROCEDURES (FAR PART 12)'
WHERE clause_version_id = 9 AND clause_name = '52.229-10';

UPDATE Clauses
SET clause_rule = '(A OR B) AND ((C AND (D OR E)) OR F) AND G' -- '(A OR B) AND (C AND (D OR E) OR F) AND G'
WHERE clause_version_id = 9 AND clause_name = '52.232-11';

UPDATE Clauses
SET effective_date = NULL -- '2012-04-01'
WHERE clause_version_id = 9 AND clause_name = '52.232-16';

UPDATE Clauses
SET clause_rule = '(A OR B) AND (C AND D AND E AND F AND G AND !J AND !(H AND I)) AND K' -- '(A OR B) AND (C AND D AND E AND F AND G AND J AND !(H AND I)) AND K'
WHERE clause_version_id = 9 AND clause_name = '52.232-17';
INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT clause_id, prescription_id FROM Prescriptions, Clauses
WHERE prescription_name IN ('32.611(a)&(b)') 
AND clause_id IN (SELECT clause_id FROM Clauses WHERE clause_name = '52.232-17' AND clause_version_id = 9); 

UPDATE Clauses
SET clause_conditions = '(A) DOCUMENT TYPE IS: SOLICITATION (B) DOCUMENT TYPE IS: AWARD (C) PROCEDURES IS: NOT SIMPLIFIED ACQUISITION PROCEDURES (FAR PART 13)' -- '(A) DOCUMENT TYPE IS: SOLICITATION (B) DOCUMENT TYPE IS: AWARD'
, clause_rule = '(A OR B) AND C' -- 'A OR B'
WHERE clause_version_id = 9 AND clause_name = '52.232-29';

UPDATE Clauses
SET clause_conditions = '(A) DOCUMENT TYPE IS: SOLICITATION (B) PROCEDURES IS: NOT COMMERCIAL PROCEDURES (FAR PART 12) (C) PROCEDURES IS: NOT SIMPLIFIED ACQUISITION PROCEDURES (FAR PART 13)' -- '(A) DOCUMENT TYPE IS: SOLICITATION (B) PROCEDURES IS: NOT COMMERCIAL PROCEDURES (FAR PART 12)'
, clause_rule = 'A AND B AND C' -- 'A AND B'
WHERE clause_version_id = 9 AND clause_name = '52.232-31';

UPDATE Clauses
SET commercial_status = 'R (ALWAYS)' -- 'R (RULE APPLIES)'
WHERE clause_version_id = 9 AND clause_name = '52.232-40';

UPDATE Clauses
SET clause_conditions = '(A) DOCUMENT TYPE IS: SOLICITATION (B) DOCUMENT TYPE IS: AWARD (C) BUSINESS TYPE IS: NOT FOREIGN GOVERNMENT (D) DISPUTES ACT IS: YES (E) PROCEDURES IS: NOT COMMERCIAL PROCEDURES (FAR PART 12)' -- '(A) DOCUMENT TYPE IS: SOLICITATION (B) DOCUMENT TYPE IS: AWARD (C) BUSINESS TYPE IS: NOT FOREIGN GOVERNMENT (D) DISPUTES ACT IS: NO (E) PROCEDURES IS: NOT COMMERCIAL PROCEDURES (FAR PART 12)'
WHERE clause_version_id = 9 AND clause_name = '52.233-1';

UPDATE Clauses
SET clause_rule = '((A OR B) AND F AND K AND ((C OR D) OR G OR ((H AND I) OR J))) OR (A AND E AND F)' -- '((A OR B) AND F AND ((C OR D) OR G OR ((H AND I) OR J))) OR (A AND E AND F) AND K'
WHERE clause_version_id = 9 AND clause_name = '52.245-1';


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT clause_id, prescription_id FROM Prescriptions, Clauses
WHERE prescription_name IN ('47.207-5(c)') 
AND clause_id IN (SELECT clause_id FROM Clauses WHERE clause_name = '52.247-13' AND clause_version_id = 9); 

UPDATE Clauses
SET clause_data = '<p>(a) The offeror does {{checkbox_52.247-2[0]}}, does not {{checkbox_52.247-2[1]}}, hold authorization from the Federal Highway Administration (FHWA) or other cognizant regulatory body. If authorization is held, it is as follows:</p>&nbsp; {{textbox_52.247-2[0]}}<p>(Name of regulatory body)</p>&nbsp; {{textbox_52.247-2[1]}}<p>(Authorization No.)</p><p>(b) The offeror shall furnish to the Government, if requested, copies of the authorization before moving the material under any contract awarded. In addition, the offeror shall, at the offeror''s expense, obtain and maintain any permits, franchises, licenses, and other authorities issued by State and local governments.</p>'
--               '<p>(a) The offeror does {{textbox_52.247-2[0]}}, does not {{textbox_52.247-2[1]}}, hold authorization from the Federal Highway Administration (FHWA) or other cognizant regulatory body. If authorization is held, it is as follows:</p>&nbsp; {{textbox_52.247-2[2]}}<p>(Name of regulatory body)</p>&nbsp; {{textbox_52.247-2[3]}}<p>(Authorization No.)</p><p>(b) The offeror shall furnish to the Government, if requested, copies of the authorization before moving the material under any contract awarded. In addition, the offeror shall, at the offeror''s expense, obtain and maintain any permits, franchises, licenses, and other authorities issued by State and local governments.</p>'
WHERE clause_version_id = 9 AND clause_name = '52.247-2';

INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size, fill_in_group_number, fill_in_placeholder, fill_in_default_data, fill_in_display_rows, fill_in_for_table, fill_in_heading) 
SELECT clause_id, 'checkbox_52.247-2[0]', 'C', NULL, NULL, NULL, NULL, NULL, 0, NULL FROM Clauses WHERE clause_name = '52.247-2' AND clause_version_id = 9; 
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size, fill_in_group_number, fill_in_placeholder, fill_in_default_data, fill_in_display_rows, fill_in_for_table, fill_in_heading) 
SELECT clause_id, 'checkbox_52.247-2[1]', 'C', NULL, NULL, NULL, NULL, NULL, 0, NULL FROM Clauses WHERE clause_name = '52.247-2' AND clause_version_id = 9; 
DELETE FROM Document_Fill_Ins WHERE clause_fill_in_id IN (SELECT clause_fill_in_id FROM Clause_Fill_Ins CF INNER JOIN Clauses C  ON C.clause_id= CF.clause_id
WHERE clause_name = '52.247-2' AND clause_version_id = 9 AND fill_in_code='textbox_52.247-2[2]');
DELETE FROM Clause_Fill_Ins WHERE clause_id IN (SELECT clause_id FROM Clauses 
WHERE clause_name = '52.247-2' AND clause_version_id = 9)  AND fill_in_code='textbox_52.247-2[2]';
DELETE FROM Document_Fill_Ins WHERE clause_fill_in_id IN (SELECT clause_fill_in_id FROM Clause_Fill_Ins CF INNER JOIN Clauses C  ON C.clause_id= CF.clause_id
WHERE clause_name = '52.247-2' AND clause_version_id = 9 AND fill_in_code='textbox_52.247-2[3]');
DELETE FROM Clause_Fill_Ins WHERE clause_id IN (SELECT clause_id FROM Clauses 
WHERE clause_name = '52.247-2' AND clause_version_id = 9)  AND fill_in_code='textbox_52.247-2[3]';


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT clause_id, prescription_id FROM Prescriptions, Clauses
WHERE prescription_name IN ('47.303-14(c)') 
AND clause_id IN (SELECT clause_id FROM Clauses WHERE clause_name = '52.247-42' AND clause_version_id = 9); 

UPDATE Clauses
SET clause_rule = 'A AND B AND AND D AND E' -- 'A AND B AND D AND E'
WHERE clause_version_id = 9 AND clause_name = '52.247-45';

UPDATE Clauses
SET clause_conditions = '(A) DOCUMENT TYPE IS: SOLICITATION (B) DOCUMENT TYPE IS: AWARD (C) TRANSPORTATION REQUIREMENTS IS: CONTRACTOR RESPONSIBLE FOR FAMILIARIZATION WITH CONDITIONS UNDER WHICH AND WHERE THE SERVICES ARE TO BE PERFORMED (D) PROCEDURES IS: NOT COMMERCIAL PROCEDURES (FAR PART 12)' -- '(A) DOCUMENT TYPE IS: SOLICITATION\n(B) DOCUMENT TYPE IS: AWARD\n(C) TRANSPORTATION REQUIREMENTS 3 IS: OFFEROR RESPONSIBLE FOR FAMILIARIZATION WITH CONDITIONS UNDER WHICH AND WHERE THE SERVICES ARE TO BE PERFORMED\n(D) PROCEDURES IS: NOT COMMERCIAL PROCEDURES (FAR PART 12)'
WHERE clause_version_id = 9 AND clause_name = '52.247-5';

UPDATE Clauses
SET provision_or_clause = 'P' -- 'C'
, clause_conditions = '(A) DOCUMENT TYPE IS: SOLICITATION (C) CONTRACT VALUE IS: GREATER THAN SAT (D) EVALUATION FACTORS IS: SHIPPING AND OTHER CHARACTERISTICS ARE REQUIRED TO EVALUATE OFFERS (E) CONTRACT TYPE IS: FIXED PRICE (F) PROCEDURES IS: NOT COMMERCIAL PROCEDURES (FAR PART 12)\n(B) DOCUMENT TYPE IS: AWARD\n(D2) TRANSPORTATION COST IS: DETERMINATION OF TRANSPORTATION COSTS REQUIRE INFORMATION REGARDING SHIPPING AND OTHER CHARACTERISTICS' -- '(B) DOCUMENT TYPE IS: AWARD\n(C) CONTRACT VALUE IS: GREATER THAN SAT\n(D) TRANSPORTATION COST IS: DETERMINATION OF TRANSPORTATION COSTS REQUIRE INFORMATION REGARDING SHIPPING AND OTHER CHARACTERISTICS\n(E) CONTRACT TYPE IS: FIXED PRICE\n(F) PROCEDURES IS: NOT COMMERCIAL PROCEDURES (FAR PART 12)\n(A) DOCUMENT TYPE IS: SOLICITATION\n(D2) EVALUATION FACTORS IS: SHIPPING AND OTHER CHARACTERISTICS ARE REQUIRED TO EVALUATE OFFERS'
, clause_rule = '(A AND C AND D AND E AND F) OR (B AND C AND D2 AND E AND F)' -- '(B AND C AND D AND E AND F) OR (A AND C AND D2 AND E AND F)'
, clause_data = '<p>(a) The offeror is requested to complete subparagraph (a)(1) of this clause, for each part or component which is packed or packaged separately. This information will be used to determine transportation costs for evaluation purposes. If the offeror does not furnish sufficient data in subparagraph (a)(1) of this clause, to permit determination by the Government of the item shipping costs, evaluation will be based on the shipping characteristics submitted by the offeror whose offer produces the highest transportation costs or in the absence thereof, by the Contracting Officer''s best estimate of the actual transportation costs. If the item shipping costs, based on the actual shipping characteristics, exceed the item shipping costs used for evaluation purposes, the Contractor agrees that the contract price shall be reduced by an amount equal to the difference between the transportation costs actually incurred, and the costs which would have been incurred if the evaluated shipping characteristics had been accurate.</p><p>(1) To be completed by the offeror:</p><p>(i) Type of container: Wood Box {{textbox_52.247-60[0]}}, Fiber Box {{textbox_52.247-60[1]}}, Barrel {{textbox_52.247-60[2]}}, Reel {{textbox_52.247-60[3]}}, Drum {{textbox_52.247-60[4]}}, Other (Specify) {{textbox_52.247-60[5]}};</p><p>(ii) Shipping configuration: Knocked-down {{textbox_52.247-60[6]}}, Set-up {{textbox_52.247-60[7]}}, Nested {{textbox_52.247-60[8]}}, Other (specify) {{textbox_52.247-60[9]}};</p><p>(iii) Size of container {{textbox_52.247-60[10]}}&Prime; (Length), &times; {{textbox_52.247-60[11]}}&Prime; (Width), &times; {{textbox_52.247-60[12]}}&Prime; (Height) = {{textbox_52.247-60[13]}} Cubic FT;</p><p>(iv) Number of items per container {{textbox_52.247-60[14]}} Each;</p><p>(v) Gross weight of container and contents{{textbox_52.247-60[15]}} LBS</p><p>(vi) Palletized/skidded {{checkbox_52.247-60[0]}} Yes {{checkbox_52.247-60[1]}} No;</p><p>(vii) Number of containers per pallet/skid {{textbox_52.247-60[16]}};</p><p>(viii) Weight of empty pallet bottom/skid and sides{{textbox_52.247-60[17]}} LBS;</p><p>(ix) Size of pallet/skid and contents {{textbox_52.247-60[18]}} LBS Cube {{textbox_52.247-60[19]}};</p><p>(x) Number of containers or pallets/skid per railcar {{textbox_52.247-60[20]}}*-</p><div><p>*Number of complete units (contract line item) to be shipped in carrier''s equipment.</p></div><p>Size of railcar {{textbox_52.247-60[21]}}</p><p>Type of railcar {{textbox_52.247-60[22]}}</p><p>(xi) Number of containers or pallets/skids per trailer {{textbox_52.247-60[23]}}*-</p><p>Size of trailer {{textbox_52.247-60[24]}} FT</p><p>Type of trailer {{textbox_52.247-60[25]}}</p><p>(2) To be completed by the Government after evaluation but before contract award:</p><p>(i) Rate used in evaluation {{textbox_52.247-60[26]}};</p><p>(ii) Tender/Tariff {{textbox_52.247-60[27]}};</p><p>(iii) Item {{textbox_52.247-60[28]}};</p><p>(b) The guaranteed shipping characteristics requested in subparagraph (a)(1) of this clause do not establish actual transportation requirements, which are specified elsewhere in this solicitation. The guaranteed shipping characteristics will be used only for the purpose of evaluating offers and establishing any liability of the successful offeror for increased transportation costs resulting from actual shipping characteristics which differ from those used for evaluation in accordance with paragraph (a) of this clause.</p>'
--               '<p>(a) {{paragraph_a_52.247-60}}</p><p>(b) The guaranteed shipping characteristics requested in subparagraph (a)(1) of this clause do not establish actual transportation requirements, which are specified elsewhere in this solicitation. The guaranteed shipping characteristics will be used only for the purpose of evaluating offers and establishing any liability of the successful offeror for increased transportation costs resulting from actual shipping characteristics which differ from those used for evaluation in accordance with paragraph (a) of this clause.</p>'
WHERE clause_version_id = 9 AND clause_name = '52.247-60';

INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size, fill_in_group_number, fill_in_placeholder, fill_in_default_data, fill_in_display_rows, fill_in_for_table, fill_in_heading) 
SELECT clause_id, 'textbox_52.247-60[0]', 'S', 100, NULL, NULL, NULL, NULL, 0, NULL FROM Clauses WHERE clause_name = '52.247-60' AND clause_version_id = 9; 
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size, fill_in_group_number, fill_in_placeholder, fill_in_default_data, fill_in_display_rows, fill_in_for_table, fill_in_heading) 
SELECT clause_id, 'textbox_52.247-60[1]', 'S', 100, NULL, NULL, NULL, NULL, 0, NULL FROM Clauses WHERE clause_name = '52.247-60' AND clause_version_id = 9; 
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size, fill_in_group_number, fill_in_placeholder, fill_in_default_data, fill_in_display_rows, fill_in_for_table, fill_in_heading) 
SELECT clause_id, 'textbox_52.247-60[2]', 'S', 100, NULL, NULL, NULL, NULL, 0, NULL FROM Clauses WHERE clause_name = '52.247-60' AND clause_version_id = 9; 
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size, fill_in_group_number, fill_in_placeholder, fill_in_default_data, fill_in_display_rows, fill_in_for_table, fill_in_heading) 
SELECT clause_id, 'textbox_52.247-60[3]', 'S', 100, NULL, NULL, NULL, NULL, 0, NULL FROM Clauses WHERE clause_name = '52.247-60' AND clause_version_id = 9; 
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size, fill_in_group_number, fill_in_placeholder, fill_in_default_data, fill_in_display_rows, fill_in_for_table, fill_in_heading) 
SELECT clause_id, 'textbox_52.247-60[4]', 'S', 100, NULL, NULL, NULL, NULL, 0, NULL FROM Clauses WHERE clause_name = '52.247-60' AND clause_version_id = 9; 
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size, fill_in_group_number, fill_in_placeholder, fill_in_default_data, fill_in_display_rows, fill_in_for_table, fill_in_heading) 
SELECT clause_id, 'textbox_52.247-60[5]', 'S', 100, NULL, NULL, NULL, NULL, 0, NULL FROM Clauses WHERE clause_name = '52.247-60' AND clause_version_id = 9; 
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size, fill_in_group_number, fill_in_placeholder, fill_in_default_data, fill_in_display_rows, fill_in_for_table, fill_in_heading) 
SELECT clause_id, 'textbox_52.247-60[6]', 'S', 100, NULL, NULL, NULL, NULL, 0, NULL FROM Clauses WHERE clause_name = '52.247-60' AND clause_version_id = 9; 
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size, fill_in_group_number, fill_in_placeholder, fill_in_default_data, fill_in_display_rows, fill_in_for_table, fill_in_heading) 
SELECT clause_id, 'textbox_52.247-60[7]', 'S', 100, NULL, NULL, NULL, NULL, 0, NULL FROM Clauses WHERE clause_name = '52.247-60' AND clause_version_id = 9; 
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size, fill_in_group_number, fill_in_placeholder, fill_in_default_data, fill_in_display_rows, fill_in_for_table, fill_in_heading) 
SELECT clause_id, 'textbox_52.247-60[8]', 'S', 100, NULL, NULL, NULL, NULL, 0, NULL FROM Clauses WHERE clause_name = '52.247-60' AND clause_version_id = 9; 
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size, fill_in_group_number, fill_in_placeholder, fill_in_default_data, fill_in_display_rows, fill_in_for_table, fill_in_heading) 
SELECT clause_id, 'textbox_52.247-60[9]', 'S', 100, NULL, NULL, NULL, NULL, 0, NULL FROM Clauses WHERE clause_name = '52.247-60' AND clause_version_id = 9; 
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size, fill_in_group_number, fill_in_placeholder, fill_in_default_data, fill_in_display_rows, fill_in_for_table, fill_in_heading) 
SELECT clause_id, 'textbox_52.247-60[10]', 'S', 100, NULL, NULL, NULL, NULL, 0, NULL FROM Clauses WHERE clause_name = '52.247-60' AND clause_version_id = 9; 
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size, fill_in_group_number, fill_in_placeholder, fill_in_default_data, fill_in_display_rows, fill_in_for_table, fill_in_heading) 
SELECT clause_id, 'textbox_52.247-60[11]', 'S', 100, NULL, NULL, NULL, NULL, 0, NULL FROM Clauses WHERE clause_name = '52.247-60' AND clause_version_id = 9; 
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size, fill_in_group_number, fill_in_placeholder, fill_in_default_data, fill_in_display_rows, fill_in_for_table, fill_in_heading) 
SELECT clause_id, 'textbox_52.247-60[12]', 'S', 100, NULL, NULL, NULL, NULL, 0, NULL FROM Clauses WHERE clause_name = '52.247-60' AND clause_version_id = 9; 
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size, fill_in_group_number, fill_in_placeholder, fill_in_default_data, fill_in_display_rows, fill_in_for_table, fill_in_heading) 
SELECT clause_id, 'textbox_52.247-60[13]', 'S', 100, NULL, NULL, NULL, NULL, 0, NULL FROM Clauses WHERE clause_name = '52.247-60' AND clause_version_id = 9; 
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size, fill_in_group_number, fill_in_placeholder, fill_in_default_data, fill_in_display_rows, fill_in_for_table, fill_in_heading) 
SELECT clause_id, 'textbox_52.247-60[14]', 'S', 100, NULL, NULL, NULL, NULL, 0, NULL FROM Clauses WHERE clause_name = '52.247-60' AND clause_version_id = 9; 
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size, fill_in_group_number, fill_in_placeholder, fill_in_default_data, fill_in_display_rows, fill_in_for_table, fill_in_heading) 
SELECT clause_id, 'textbox_52.247-60[15]', 'S', 100, NULL, NULL, NULL, NULL, 0, NULL FROM Clauses WHERE clause_name = '52.247-60' AND clause_version_id = 9; 
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size, fill_in_group_number, fill_in_placeholder, fill_in_default_data, fill_in_display_rows, fill_in_for_table, fill_in_heading) 
SELECT clause_id, 'textbox_52.247-60[16]', 'S', 100, NULL, NULL, NULL, NULL, 0, NULL FROM Clauses WHERE clause_name = '52.247-60' AND clause_version_id = 9; 
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size, fill_in_group_number, fill_in_placeholder, fill_in_default_data, fill_in_display_rows, fill_in_for_table, fill_in_heading) 
SELECT clause_id, 'textbox_52.247-60[17]', 'S', 100, NULL, NULL, NULL, NULL, 0, NULL FROM Clauses WHERE clause_name = '52.247-60' AND clause_version_id = 9; 
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size, fill_in_group_number, fill_in_placeholder, fill_in_default_data, fill_in_display_rows, fill_in_for_table, fill_in_heading) 
SELECT clause_id, 'textbox_52.247-60[18]', 'S', 100, NULL, NULL, NULL, NULL, 0, NULL FROM Clauses WHERE clause_name = '52.247-60' AND clause_version_id = 9; 
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size, fill_in_group_number, fill_in_placeholder, fill_in_default_data, fill_in_display_rows, fill_in_for_table, fill_in_heading) 
SELECT clause_id, 'textbox_52.247-60[19]', 'S', 100, NULL, NULL, NULL, NULL, 0, NULL FROM Clauses WHERE clause_name = '52.247-60' AND clause_version_id = 9; 
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size, fill_in_group_number, fill_in_placeholder, fill_in_default_data, fill_in_display_rows, fill_in_for_table, fill_in_heading) 
SELECT clause_id, 'textbox_52.247-60[20]', 'S', 100, NULL, NULL, NULL, NULL, 0, NULL FROM Clauses WHERE clause_name = '52.247-60' AND clause_version_id = 9; 
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size, fill_in_group_number, fill_in_placeholder, fill_in_default_data, fill_in_display_rows, fill_in_for_table, fill_in_heading) 
SELECT clause_id, 'textbox_52.247-60[21]', 'S', 100, NULL, NULL, NULL, NULL, 0, NULL FROM Clauses WHERE clause_name = '52.247-60' AND clause_version_id = 9; 
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size, fill_in_group_number, fill_in_placeholder, fill_in_default_data, fill_in_display_rows, fill_in_for_table, fill_in_heading) 
SELECT clause_id, 'textbox_52.247-60[22]', 'S', 100, NULL, NULL, NULL, NULL, 0, NULL FROM Clauses WHERE clause_name = '52.247-60' AND clause_version_id = 9; 
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size, fill_in_group_number, fill_in_placeholder, fill_in_default_data, fill_in_display_rows, fill_in_for_table, fill_in_heading) 
SELECT clause_id, 'textbox_52.247-60[23]', 'S', 100, NULL, NULL, NULL, NULL, 0, NULL FROM Clauses WHERE clause_name = '52.247-60' AND clause_version_id = 9; 
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size, fill_in_group_number, fill_in_placeholder, fill_in_default_data, fill_in_display_rows, fill_in_for_table, fill_in_heading) 
SELECT clause_id, 'textbox_52.247-60[24]', 'S', 100, NULL, NULL, NULL, NULL, 0, NULL FROM Clauses WHERE clause_name = '52.247-60' AND clause_version_id = 9; 
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size, fill_in_group_number, fill_in_placeholder, fill_in_default_data, fill_in_display_rows, fill_in_for_table, fill_in_heading) 
SELECT clause_id, 'textbox_52.247-60[25]', 'S', 100, NULL, NULL, NULL, NULL, 0, NULL FROM Clauses WHERE clause_name = '52.247-60' AND clause_version_id = 9; 
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size, fill_in_group_number, fill_in_placeholder, fill_in_default_data, fill_in_display_rows, fill_in_for_table, fill_in_heading) 
SELECT clause_id, 'textbox_52.247-60[26]', 'S', 100, NULL, NULL, NULL, NULL, 0, NULL FROM Clauses WHERE clause_name = '52.247-60' AND clause_version_id = 9; 
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size, fill_in_group_number, fill_in_placeholder, fill_in_default_data, fill_in_display_rows, fill_in_for_table, fill_in_heading) 
SELECT clause_id, 'textbox_52.247-60[27]', 'S', 100, NULL, NULL, NULL, NULL, 0, NULL FROM Clauses WHERE clause_name = '52.247-60' AND clause_version_id = 9; 
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size, fill_in_group_number, fill_in_placeholder, fill_in_default_data, fill_in_display_rows, fill_in_for_table, fill_in_heading) 
SELECT clause_id, 'textbox_52.247-60[28]', 'S', 100, NULL, NULL, NULL, NULL, 0, NULL FROM Clauses WHERE clause_name = '52.247-60' AND clause_version_id = 9; 
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size, fill_in_group_number, fill_in_placeholder, fill_in_default_data, fill_in_display_rows, fill_in_for_table, fill_in_heading) 
SELECT clause_id, 'checkbox_52.247-60[0]', 'C', NULL, NULL, NULL, NULL, NULL, 0, NULL FROM Clauses WHERE clause_name = '52.247-60' AND clause_version_id = 9; 
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size, fill_in_group_number, fill_in_placeholder, fill_in_default_data, fill_in_display_rows, fill_in_for_table, fill_in_heading) 
SELECT clause_id, 'checkbox_52.247-60[1]', 'C', NULL, NULL, NULL, NULL, NULL, 0, NULL FROM Clauses WHERE clause_name = '52.247-60' AND clause_version_id = 9; 
DELETE FROM Document_Fill_Ins WHERE clause_fill_in_id IN (SELECT clause_fill_in_id FROM Clause_Fill_Ins CF INNER JOIN Clauses C  ON C.clause_id= CF.clause_id
WHERE clause_name = '52.247-60' AND clause_version_id = 9 AND fill_in_code='paragraph_a_52.247-60');
DELETE FROM Clause_Fill_Ins WHERE clause_id IN (SELECT clause_id FROM Clauses 
WHERE clause_name = '52.247-60' AND clause_version_id = 9)  AND fill_in_code='paragraph_a_52.247-60';

UPDATE Clauses
SET clause_data = '<p>(a) <i>General.</i> The Contractor is encouraged to develop, prepare, and submit value engineering change proposals (VECP''s) voluntarily. The Contractor shall share in any net acquisition savings realized from accepted VECP''s, in accordance with the incentive sharing rates in paragraph (f) below.</p><p>(b) <i>Definitions. Acquisition savings,</i> as used in this clause, means savings resulting from the application of a VECP to contracts awarded by the same contracting office or its successor for essentially the same unit. Acquisition savings include-</p><p>(1) Instant contract savings, which are the net cost reductions on this, the instant contract, and which are equal to the instant unit cost reduction multiplied by the number of instant contract units affected by the VECP, less the Contractor''s allowable development and implementation costs;</p><p>(2) Concurrent contract savings, which are net reductions in the prices of other contracts that are definitized and ongoing at the time the VECP is accepted; and</p><p>{{paragraph_a3_52.248-1}} On an instant contract, future contract savings include savings on increases in quantities after VECP acceptance that are due to contract modifications, exercise of options, additional orders, and funding of subsequent year requirements on a multiyear contract.</p><p><i>Collateral costs,</i> as used in this clause, means agency cost of operation, maintenance, logistic support, or Government-furnished property.</p><p><i>Collateral savings,</i> as used in this clause, means those measurable net reductions resulting from a VECP in the agency''s overall projected collateral costs, exclusive of acquisition savings, whether or not the acquisition cost changes.</p><p><i>Contracting office</i> includes any contracting office that the acquisition is transferred to, such as another branch of the agency or another agency''s office that is performing a joint acquisition action.</p><p><i>Contractor''s development and implementation costs,</i> as used in this clause, means those costs the Contractor incurs on a VECP specifically in developing, testing, preparing, and submitting the VECP, as well as those costs the Contractor incurs to make the contractual changes required by Government acceptance of a VECP.</p><p><i>Future unit cost reduction,</i> as used in this clause, means the instant unit cost reduction adjusted as the Contracting Officer considers necessary for projected learning or changes in quantity during the sharing period. It is calculated at the time the VECP is accepted and applies either (1) throughout the sharing period, unless the Contracting Officer decides that recalculation is necessary because conditions are significantly different from those previously anticipated or (2) to the calculation of a lump-sum payment, which cannot later be revised.</p><p><i>Government costs,</i> as used in this clause, means those agency costs that result directly from developing and implementing the VECP, such as any net increases in the cost of testing, operations, maintenance, and logistics support. The term does not include the normal administrative costs of processing the VECP or any increase in this contract''s cost or price resulting from negative instant contract savings.</p><p><i>Instant contract,</i> as used in this clause, means this contract, under which the VECP is submitted. It does not include increases in quantities after acceptance of the VECP that are due to contract modifications, exercise of options, or additional orders. If this is a multiyear contract, the term does not include quantities funded after VECP acceptance. If this contract is a fixed-price contract with prospective price redetermination, the term refers to the period for which firm prices have been established.</p><p><i>Instant unit cost reduction</i> means the amount of the decrease in unit cost of performance (without deducting any Contractor''s development or implementation costs) resulting from using the VECP on this, the instant contract. If this is a service contract, the instant unit cost reduction is normally equal to the number of hours per line-item task saved by using the VECP on this contract, multiplied by the appropriate contract labor rate.</p><p><i>Negative instant contract savings</i> means the increase in the cost or price of this contract when the acceptance of a VECP results in an excess of the Contractor''s allowable development and implementation costs over the product of the instant unit cost reduction multiplied by the number of instant contract units affected.</p><p><i>Net acquisition savings</i> means total acquisition savings, including instant, concurrent, and future contract savings, less Government costs.</p><p><i>Sharing base,</i> as used in this clause, means the number of affected end items on contracts of the contracting office accepting the VECP.</p><p><i>Sharing period,</i> as used in this clause, means the period beginning with acceptance of the first unit incorporating the VECP and ending at a calendar date or event determined by the contracting officer for each VECP.</p><p><i>Unit,</i> as used in this clause, means the item or task to which the Contracting Officer and the Contractor agree the VECP applies.</p><p><i>Value engineering change proposal (VECP)</i> means a proposal that-</p><p>(1) Requires a change to this, the instant contract, to implement; and</p><p>(2) Results in reducing the overall projected cost to the agency without impairing essential functions or characteristics; <i>provided,</i> that it does not involve a change-</p><p>(i) In deliverable end item quantities only;</p><p>(ii) In research and development (R&D) end items or R&D test quantities that is due solely to results of previous testing under this contract; or</p><p>(iii) To the contract type only.</p><p>(c) <i>VECP preparation.</i> As a minimum, the Contractor shall include in each VECP the information described in subparagraphs (1) through (8) below. If the proposed change is affected by contractually required configuration management or similar procedures, the instructions in those procedures relating to format, identification, and priority assignment shall govern VECP preparation. The VECP shall include the following:</p><p>(1) A description of the difference between the existing contract requirement and the proposed requirement, the comparative advantages and disadvantages of each, a justification when an item''s function or characteristics are being altered, the effect of the change on the end item''s performance, and any pertinent objective test data.</p><p>(2) A list and analysis of the contract requirements that must be changed if the VECP is accepted, including any suggested specification revisions.</p><p>(3) Identification of the unit to which the VECP applies.</p><p>(4) A separate, detailed cost estimate for (i) the affected portions of the existing contract requirement and (ii) the VECP. The cost reduction associated with the VECP shall take into account the Contractor''s allowable development and implementation costs, including any amount attributable to subcontracts under the Subcontracts paragraph of this clause, below.</p><p>(5) A description and estimate of costs the Government may incur in implementing the VECP, such as test and evaluation and operating and support costs.</p><p>(6) A prediction of any effects the proposed change would have on collateral costs to the agency.</p><p>(7) A statement of the time by which a contract modification accepting the VECP must be issued in order to achieve the maximum cost reduction, noting any effect on the contract completion time or delivery schedule.</p><p>(8) Identification of any previous submissions of the VECP, including the dates submitted, the agencies and contract numbers involved, and previous Government actions, if known.</p><p>(d) <i>Submission.</i> The Contractor shall submit VECP''s to the Contracting Officer, unless this contract states otherwise. If this contract is administered by other than the contracting office, the Contractor shall submit a copy of the VECP simultaneously to the Contracting Officer and to the Administrative Contracting Officer.</p><p>(e) <i>Government action.</i> (1) The Contracting Officer will notify the Contractor of the status of the VECP within 45 calendar days after the contracting office receives it. If additional time is required, the Contracting Officer will notify the Contractor within the 45-day period and provide the reason for the delay and the expected date of the decision. The Government will process VECP''s expeditiously; however, it will not be liable for any delay in acting upon a VECP.</p><p>(2) If the VECP is not accepted, the Contracting Officer will notify the Contractor in writing, explaining the reasons for rejection. The Contractor may withdraw any VECP, in whole or in part, at any time before it is accepted by the Government. The Contracting Officer may require that the Contractor provide written notification before undertaking significant expenditures for VECP effort.</p><p>(3) Any VECP may be accepted, in whole or in part, by the Contracting Officer''s award of a modification to this contract citing this clause and made either before or within a reasonable time after contract performance is completed. Until such a contract modification applies a VECP to this contract, the Contractor shall perform in accordance with the existing contract. The decision to accept or reject all or part of any VECP is a unilateral decision made solely at the discretion of the Contracting Officer.</p><p>(f) <i>Sharing rates.</i> If a VECP is accepted, the Contractor shall share in net acquisition savings according to the percentages shown in the table below. The percentage paid the Contractor depends upon (1) this contract''s type (fixed-price, incentive, or cost-reimbursement), (2) the sharing arrangement specified in paragraph (a) above (incentive, program requirement, or a combination as delineated in the Schedule), and (3) the source of the savings (the instant contract, or concurrent and future contracts), as follows:</p><div><div><p>Contractor''s Share of Net Acquisition Savings</p><p>[Figures in Percent]</p></div><div><table border="1" cellpadding="1" cellspacing="1" frame="void" width="100%"><tbody><tr><th rowspan="3" scope="col">Contract type</th><th colspan="4" scope="col">Sharing arrangement</th></tr><tr><th colspan="2" scope="col">Incentive (voluntary)</th><th colspan="2" scope="col">Program requirement (mandatory)</th></tr><tr><th scope="col">Instant contract rate</th><th scope="col">Con-current and future contract rate</th><th scope="col">Instant contract rate</th><th scope="col">Con-current and future contract rate</th></tr><tr><td align="left" scope="row">Fixed-price (includes fixed-price-award-fee; excludes other fixed-price incentive contracts)</td><td align="center"><sup>1</sup>{{number_5_0_placeholder_52.248-1[0]}}</td><td align="center"><sup>1</sup>{{number_5_0_placeholder_52.248-1[1]}}</td><td align="center">{{number_2_5_placeholder_52.248-1[0]}}</td><td align="center">{{number_2_5_placeholder_52.248-1[1]}}</td></tr><tr><td align="left" scope="row">Incentive (fixed-price or cost) (other than award fee)</td><td align="center">(<sup>2</sup>)</td><td align="center"><sup>1</sup>{{number_5_0_placeholder_52.248-1[2]}}</td><td align="center">(<sup>2</sup>)</td><td align="center">{{number_2_5_placeholder_52.248-1[2]}}</td></tr><tr><td align="left" scope="row">Cost-reimbursement (includes cost-plus-award-fee; excludes other cost-type incentive contracts)</td><td align="center"><sup>3</sup>{{number_2_5_placeholder_52.248-1[3]}}</td><td align="center"><sup>3</sup>{{number_2_5_placeholder_52.248-1[4]}}</td><td align="center">{{number_1_5_placeholder_52.248-1[0]}}</td><td align="center">{{number_1_5_placeholder_52.248-1[1]}}</td></tr></tbody></table></div><div><p><sup>1</sup>The Contracting Officer may increase the Contractor''s sharing rate to as high as 75 percent for each VECP.</p><p><sup>2</sup>Same sharing arrangement as the contract''s profit or fee adjustment formula.</p><p><sup>3</sup>The Contracting Officer may increase the Contractor''s sharing rate to as high as 50 percent for each VECP.</p></div></div><p>(g) <i>Calculating net acquisition savings.</i> (1) Acquisition savings are realized when (i) the cost or price is reduced on the instant contract, (ii) reductions are negotiated in concurrent contracts, (iii) future contracts are awarded, or (iv) agreement is reached on a lump-sum payment for future contract savings (see subparagraph (i)(4) below). Net acquisition savings are first realized, and the Contractor shall be paid a share, when Government costs and any negative instant contract savings have been fully offset against acquisition savings.</p><p>(2) Except in incentive contracts, Government costs and any price or cost increases resulting from negative instant contract savings shall be offset against acquisition savings each time such savings are realized until they are fully offset. Then, the Contractor''s share is calculated by multiplying net acquisition savings by the appropriate Contractor''s percentage sharing rate (see paragraph (f) above). Additional Contractor shares of net acquisition savings shall be paid to the Contractor at the time realized.</p><p>(3) If this is an incentive contract, recovery of Government costs on the instant contract shall be deferred and offset against concurrent and future contract savings. The Contractor shall share through the contract incentive structure in savings on the instant contract items affected. Any negative instant contract savings shall be added to the target cost or to the target price and ceiling price, and the amount shall be offset against concurrent and future contract savings.</p><p>(4) If the Government does not receive and accept all items on which it paid the Contractor''s share, the Contractor shall reimburse the Government for the proportionate share of these payments.</p><p>(h) <i>Contract adjustment.</i> The modification accepting the VECP (or a subsequent modification issued as soon as possible after any negotiations are completed) shall-</p><p>(1) Reduce the contract price or estimated cost by the amount of instant contract savings, unless this is an incentive contract;</p><p>(2) When the amount of instant contract savings is negative, increase the contract price, target price and ceiling price, target cost, or estimated cost by that amount;</p><p>(3) Specify the Contractor''s dollar share per unit on future contracts, or provide the lump-sum payment;</p><p>(4) Specify the amount of any Government costs or negative instant contract savings to be offset in determining net acquisition savings realized from concurrent or future contract savings; and</p><p>(5) Provide the Contractor''s share of any net acquisition savings under the instant contract in accordance with the following:</p><p>(i) Fixed-price contracts-add to contract price.</p><p>(ii) Cost-reimbursement contracts-add to contract fee.</p><p>(i) <i>Concurrent and future contract savings.</i> (1) Payments of the Contractor''s share of concurrent and future contract savings shall be made by a modification to the instant contract in accordance with subparagraph (h)(5) above. For incentive contracts, shares shall be added as a separate firm-fixed-price line item on the instant contract. The Contractor shall maintain records adequate to identify the first delivered unit for 3 years after final payment under this contract.</p><p>(2) The Contracting Officer shall calculate the Contractor''s share of concurrent contract savings by (i) subtracting from the reduction in price negotiated on the concurrent contract any Government costs or negative instant contract savings not yet offset and (ii) multiplying the result by the Contractor''s sharing rate.</p><p>(3) The Contracting Officer shall calculate the Contractor''s share of future contract savings by (i) {{paragraph_i3i_52.248-1}}, (ii) subtracting any Government costs or negative instant contract savings not yet offset, and (iii) multiplying the result by the Contractor''s sharing rate.</p><p>(4) When the Government wishes and the Contractor agrees, the Contractor''s share of future contract savings may be paid in a single lump sum rather than in a series of payments over time as future contracts are awarded. Under this alternate procedure, the future contract savings may be calculated when the VECP is accepted, on the basis of the Contracting Officer''s forecast of the number of units that will be delivered during the sharing period. The Contractor''s share shall be included in a modification to this contract (see subparagraph (h)(3) above) and shall not be subject to subsequent adjustment.</p><p>(5) Alternate no-cost settlement method. When, in accordance with subsection 48.104-4 of the Federal Acquisition Regulation, the Government and the Contractor mutually agree to use the no-cost settlement method, the following applies:</p><p>(i) The Contractor will keep all the savings on the instant contract and on its concurrent contracts only.</p><p>(ii) The Government will keep all the savings resulting from concurrent contracts placed on other sources, savings from all future contracts, and all collateral savings.</p><p>(j) <i>Collateral savings.</i> If a VECP is accepted, the Contracting Officer will increase the instant contract amount, as specified in paragraph (h)(5) of this clause, by a rate from 20 to 100 percent, as determined by the Contracting Officer, of any projected collateral savings determined to be realized in a typical year of use after subtracting any Government costs not previously offset. However, the Contractor''s share of collateral savings will not exceed the contract''s firm-fixed-price, target price, target cost, or estimated cost, at the time the VECP is accepted, or $100,000, whichever is greater. The Contracting Officer will be the sole determiner of the amount of collateral savings.</p><p>(k) <i>Relationship to other incentives.</i> Only those benefits of an accepted VECP not rewardable under performance, design-to-cost (production unit cost, operating and support costs, reliability and maintainability), or similar incentives shall be rewarded under this clause. However, the targets of such incentives affected by the VECP shall not be adjusted because of VECP acceptance. If this contract specifies targets but provides no incentive to surpass them, the value engineering sharing shall apply only to the amount of achievement better than target.</p><p>(l) <i>Subcontracts.</i> The Contractor shall include an appropriate value engineering clause in any subcontract of $150,000 or more and may include one in subcontracts of lesser value. In calculating any adjustment in this contract''s price for instant contract savings (or negative instant contract savings), the Contractor''s allowable development and implementation costs shall include any subcontractor''s allowable development and implementation costs, and any value engineering incentive payments to a subcontractor, clearly resulting from a VECP accepted by the Government under this contract. The Contractor may choose any arrangement for subcontractor value engineering incentive payments; <i>provided,</i> that the payments shall not reduce the Government''s share of concurrent or future contract savings or collateral savings.</p><p>(m) <i>Data.</i> The Contractor may restrict the Government''s right to use any part of a VECP or the supporting data by marking the following legend on the affected parts:</p><p>''These data, furnished under the Value Engineering clause of contract {{textbox_52.248-1[0]}}, shall not be disclosed outside the Government or duplicated, used, or disclosed, in whole or in part, for any purpose other than to evaluate a value engineering change proposal submitted under the clause. This restriction does not limit the Government''s right to use information contained in these data if it has been obtained or is otherwise available from the Contractor or from another source without limitations."</p><p>If a VECP is accepted, the Contractor hereby grants the Government unlimited rights in the VECP and supporting data, except that, with respect to data qualifying and submitted as limited rights technical data, the Government shall have the rights specified in the contract modification implementing the VECP and shall appropriately mark the data. (The terms <i>unlimited rights</i> and <i>limited rights</i> are defined in part 27 of the Federal Acquisition Regulation.)</p>'
--               '<p>(a) <i>General.</i> The Contractor is encouraged to develop, prepare, and submit value engineering change proposals (VECP''s) voluntarily. The Contractor shall share in any net acquisition savings realized from accepted VECP''s, in accordance with the incentive sharing rates in paragraph (f) below.</p><p>(b) <i>Definitions. Acquisition savings,</i> as used in this clause, means savings resulting from the application of a VECP to contracts awarded by the same contracting office or its successor for essentially the same unit. Acquisition savings include-</p><p>(1) Instant contract savings, which are the net cost reductions on this, the instant contract, and which are equal to the instant unit cost reduction multiplied by the number of instant contract units affected by the VECP, less the Contractor''s allowable development and implementation costs;</p><p>(2) Concurrent contract savings, which are net reductions in the prices of other contracts that are definitized and ongoing at the time the VECP is accepted; and</p><p>{{paragraph_a3_52.248-1}}</p><p><i>Collateral costs,</i> as used in this clause, means agency cost of operation, maintenance, logistic support, or Government-furnished property.</p><p><i>Collateral savings,</i> as used in this clause, means those measurable net reductions resulting from a VECP in the agency''s overall projected collateral costs, exclusive of acquisition savings, whether or not the acquisition cost changes.</p><p><i>Contracting office</i> includes any contracting office that the acquisition is transferred to, such as another branch of the agency or another agency''s office that is performing a joint acquisition action.</p><p><i>Contractor''s development and implementation costs,</i> as used in this clause, means those costs the Contractor incurs on a VECP specifically in developing, testing, preparing, and submitting the VECP, as well as those costs the Contractor incurs to make the contractual changes required by Government acceptance of a VECP.</p><p><i>Future unit cost reduction,</i> as used in this clause, means the instant unit cost reduction adjusted as the Contracting Officer considers necessary for projected learning or changes in quantity during the sharing period. It is calculated at the time the VECP is accepted and applies either (1) throughout the sharing period, unless the Contracting Officer decides that recalculation is necessary because conditions are significantly different from those previously anticipated or (2) to the calculation of a lump-sum payment, which cannot later be revised.</p><p><i>Government costs,</i> as used in this clause, means those agency costs that result directly from developing and implementing the VECP, such as any net increases in the cost of testing, operations, maintenance, and logistics support. The term does not include the normal administrative costs of processing the VECP or any increase in this contract''s cost or price resulting from negative instant contract savings.</p><p><i>Instant contract,</i> as used in this clause, means this contract, under which the VECP is submitted. It does not include increases in quantities after acceptance of the VECP that are due to contract modifications, exercise of options, or additional orders. If this is a multiyear contract, the term does not include quantities funded after VECP acceptance. If this contract is a fixed-price contract with prospective price redetermination, the term refers to the period for which firm prices have been established.</p><p><i>Instant unit cost reduction</i> means the amount of the decrease in unit cost of performance (without deducting any Contractor''s development or implementation costs) resulting from using the VECP on this, the instant contract. If this is a service contract, the instant unit cost reduction is normally equal to the number of hours per line-item task saved by using the VECP on this contract, multiplied by the appropriate contract labor rate.</p><p><i>Negative instant contract savings</i> means the increase in the cost or price of this contract when the acceptance of a VECP results in an excess of the Contractor''s allowable development and implementation costs over the product of the instant unit cost reduction multiplied by the number of instant contract units affected.</p><p><i>Net acquisition savings</i> means total acquisition savings, including instant, concurrent, and future contract savings, less Government costs.</p><p><i>Sharing base,</i> as used in this clause, means the number of affected end items on contracts of the contracting office accepting the VECP.</p><p><i>Sharing period,</i> as used in this clause, means the period beginning with acceptance of the first unit incorporating the VECP and ending at a calendar date or event determined by the contracting officer for each VECP.</p><p><i>Unit,</i> as used in this clause, means the item or task to which the Contracting Officer and the Contractor agree the VECP applies.</p><p><i>Value engineering change proposal (VECP)</i> means a proposal that-</p><p>(1) Requires a change to this, the instant contract, to implement; and</p><p>(2) Results in reducing the overall projected cost to the agency without impairing essential functions or characteristics; <i>provided,</i> that it does not involve a change-</p><p>(i) In deliverable end item quantities only;</p><p>(ii) In research and development (R&D) end items or R&D test quantities that is due solely to results of previous testing under this contract; or</p><p>(iii) To the contract type only.</p><p>(c) <i>VECP preparation.</i> As a minimum, the Contractor shall include in each VECP the information described in subparagraphs (1) through (8) below. If the proposed change is affected by contractually required configuration management or similar procedures, the instructions in those procedures relating to format, identification, and priority assignment shall govern VECP preparation. The VECP shall include the following:</p><p>(1) A description of the difference between the existing contract requirement and the proposed requirement, the comparative advantages and disadvantages of each, a justification when an item''s function or characteristics are being altered, the effect of the change on the end item''s performance, and any pertinent objective test data.</p><p>(2) A list and analysis of the contract requirements that must be changed if the VECP is accepted, including any suggested specification revisions.</p><p>(3) Identification of the unit to which the VECP applies.</p><p>(4) A separate, detailed cost estimate for (i) the affected portions of the existing contract requirement and (ii) the VECP. The cost reduction associated with the VECP shall take into account the Contractor''s allowable development and implementation costs, including any amount attributable to subcontracts under the Subcontracts paragraph of this clause, below.</p><p>(5) A description and estimate of costs the Government may incur in implementing the VECP, such as test and evaluation and operating and support costs.</p><p>(6) A prediction of any effects the proposed change would have on collateral costs to the agency.</p><p>(7) A statement of the time by which a contract modification accepting the VECP must be issued in order to achieve the maximum cost reduction, noting any effect on the contract completion time or delivery schedule.</p><p>(8) Identification of any previous submissions of the VECP, including the dates submitted, the agencies and contract numbers involved, and previous Government actions, if known.</p><p>(d) <i>Submission.</i> The Contractor shall submit VECP''s to the Contracting Officer, unless this contract states otherwise. If this contract is administered by other than the contracting office, the Contractor shall submit a copy of the VECP simultaneously to the Contracting Officer and to the Administrative Contracting Officer.</p><p>(e) <i>Government action.</i> (1) The Contracting Officer will notify the Contractor of the status of the VECP within 45 calendar days after the contracting office receives it. If additional time is required, the Contracting Officer will notify the Contractor within the 45-day period and provide the reason for the delay and the expected date of the decision. The Government will process VECP''s expeditiously; however, it will not be liable for any delay in acting upon a VECP.</p><p>(2) If the VECP is not accepted, the Contracting Officer will notify the Contractor in writing, explaining the reasons for rejection. The Contractor may withdraw any VECP, in whole or in part, at any time before it is accepted by the Government. The Contracting Officer may require that the Contractor provide written notification before undertaking significant expenditures for VECP effort.</p><p>(3) Any VECP may be accepted, in whole or in part, by the Contracting Officer''s award of a modification to this contract citing this clause and made either before or within a reasonable time after contract performance is completed. Until such a contract modification applies a VECP to this contract, the Contractor shall perform in accordance with the existing contract. The decision to accept or reject all or part of any VECP is a unilateral decision made solely at the discretion of the Contracting Officer.</p><p>(f) <i>Sharing rates.</i> If a VECP is accepted, the Contractor shall share in net acquisition savings according to the percentages shown in the table below. The percentage paid the Contractor depends upon (1) this contract''s type (fixed-price, incentive, or cost-reimbursement), (2) the sharing arrangement specified in paragraph (a) above (incentive, program requirement, or a combination as delineated in the Schedule), and (3) the source of the savings (the instant contract, or concurrent and future contracts), as follows:</p><div><div><p>Contractor''s Share of Net Acquisition Savings</p><p>[Figures in Percent]</p></div><div><table border="1" cellpadding="1" cellspacing="1" frame="void" width="100%"><tbody><tr><th rowspan="3" scope="col">Contract type</th><th colspan="4" scope="col">Sharing arrangement</th></tr><tr><th colspan="2" scope="col">Incentive (voluntary)</th><th colspan="2" scope="col">Program requirement (mandatory)</th></tr><tr><th scope="col">Instant contract rate</th><th scope="col">Con-current and future contract rate</th><th scope="col">Instant contract rate</th><th scope="col">Con-current and future contract rate</th></tr><tr><td align="left" scope="row">Fixed-price (includes fixed-price-award-fee; excludes other fixed-price incentive contracts)</td><td align="center"><sup>1</sup>{{number_5_0_placeholder_52.248-1[0]}}</td><td align="center"><sup>1</sup>{{number_5_0_placeholder_52.248-1[1]}}</td><td align="center">{{number_2_5_placeholder_52.248-1[0]}}</td><td align="center">{{number_2_5_placeholder_52.248-1[1]}}</td></tr><tr><td align="left" scope="row">Incentive (fixed-price or cost) (other than award fee)</td><td align="center">(<sup>2</sup>)</td><td align="center"><sup>1</sup>{{number_5_0_placeholder_52.248-1[2]}}</td><td align="center">(<sup>2</sup>)</td><td align="center">{{number_2_5_placeholder_52.248-1[2]}}</td></tr><tr><td align="left" scope="row">Cost-reimbursement (includes cost-plus-award-fee; excludes other cost-type incentive contracts)</td><td align="center"><sup>3</sup>{{number_2_5_placeholder_52.248-1[3]}}</td><td align="center"><sup>3</sup>{{number_2_5_placeholder_52.248-1[4]}}</td><td align="center">{{number_1_5_placeholder_52.248-1[0]}}</td><td align="center">{{number_1_5_placeholder_52.248-1[1]}}</td></tr></tbody></table></div><div><p><sup>1</sup>The Contracting Officer may increase the Contractor''s sharing rate to as high as 75 percent for each VECP.</p><p><sup>2</sup>Same sharing arrangement as the contract''s profit or fee adjustment formula.</p><p><sup>3</sup>The Contracting Officer may increase the Contractor''s sharing rate to as high as 50 percent for each VECP.</p></div></div><p>(g) <i>Calculating net acquisition savings.</i> (1) Acquisition savings are realized when (i) the cost or price is reduced on the instant contract, (ii) reductions are negotiated in concurrent contracts, (iii) future contracts are awarded, or (iv) agreement is reached on a lump-sum payment for future contract savings (see subparagraph (i)(4) below). Net acquisition savings are first realized, and the Contractor shall be paid a share, when Government costs and any negative instant contract savings have been fully offset against acquisition savings.</p><p>(2) Except in incentive contracts, Government costs and any price or cost increases resulting from negative instant contract savings shall be offset against acquisition savings each time such savings are realized until they are fully offset. Then, the Contractor''s share is calculated by multiplying net acquisition savings by the appropriate Contractor''s percentage sharing rate (see paragraph (f) above). Additional Contractor shares of net acquisition savings shall be paid to the Contractor at the time realized.</p><p>(3) If this is an incentive contract, recovery of Government costs on the instant contract shall be deferred and offset against concurrent and future contract savings. The Contractor shall share through the contract incentive structure in savings on the instant contract items affected. Any negative instant contract savings shall be added to the target cost or to the target price and ceiling price, and the amount shall be offset against concurrent and future contract savings.</p><p>(4) If the Government does not receive and accept all items on which it paid the Contractor''s share, the Contractor shall reimburse the Government for the proportionate share of these payments.</p><p>(h) <i>Contract adjustment.</i> The modification accepting the VECP (or a subsequent modification issued as soon as possible after any negotiations are completed) shall-</p><p>(1) Reduce the contract price or estimated cost by the amount of instant contract savings, unless this is an incentive contract;</p><p>(2) When the amount of instant contract savings is negative, increase the contract price, target price and ceiling price, target cost, or estimated cost by that amount;</p><p>(3) Specify the Contractor''s dollar share per unit on future contracts, or provide the lump-sum payment;</p><p>(4) Specify the amount of any Government costs or negative instant contract savings to be offset in determining net acquisition savings realized from concurrent or future contract savings; and</p><p>(5) Provide the Contractor''s share of any net acquisition savings under the instant contract in accordance with the following:</p><p>(i) Fixed-price contracts-add to contract price.</p><p>(ii) Cost-reimbursement contracts-add to contract fee.</p><p>(i) <i>Concurrent and future contract savings.</i> (1) Payments of the Contractor''s share of concurrent and future contract savings shall be made by a modification to the instant contract in accordance with subparagraph (h)(5) above. For incentive contracts, shares shall be added as a separate firm-fixed-price line item on the instant contract. The Contractor shall maintain records adequate to identify the first delivered unit for 3 years after final payment under this contract.</p><p>(2) The Contracting Officer shall calculate the Contractor''s share of concurrent contract savings by (i) subtracting from the reduction in price negotiated on the concurrent contract any Government costs or negative instant contract savings not yet offset and (ii) multiplying the result by the Contractor''s sharing rate.</p><p>(3) The Contracting Officer shall calculate the Contractor''s share of future contract savings by (i) {{paragraph_i3i_52.248-1}}, (ii) subtracting any Government costs or negative instant contract savings not yet offset, and (iii) multiplying the result by the Contractor''s sharing rate.</p><p>(4) When the Government wishes and the Contractor agrees, the Contractor''s share of future contract savings may be paid in a single lump sum rather than in a series of payments over time as future contracts are awarded. Under this alternate procedure, the future contract savings may be calculated when the VECP is accepted, on the basis of the Contracting Officer''s forecast of the number of units that will be delivered during the sharing period. The Contractor''s share shall be included in a modification to this contract (see subparagraph (h)(3) above) and shall not be subject to subsequent adjustment.</p><p>(5) Alternate no-cost settlement method. When, in accordance with subsection 48.104-4 of the Federal Acquisition Regulation, the Government and the Contractor mutually agree to use the no-cost settlement method, the following applies:</p><p>(i) The Contractor will keep all the savings on the instant contract and on its concurrent contracts only.</p><p>(ii) The Government will keep all the savings resulting from concurrent contracts placed on other sources, savings from all future contracts, and all collateral savings.</p><p>(j) <i>Collateral savings.</i> If a VECP is accepted, the Contracting Officer will increase the instant contract amount, as specified in paragraph (h)(5) of this clause, by a rate from 20 to 100 percent, as determined by the Contracting Officer, of any projected collateral savings determined to be realized in a typical year of use after subtracting any Government costs not previously offset. However, the Contractor''s share of collateral savings will not exceed the contract''s firm-fixed-price, target price, target cost, or estimated cost, at the time the VECP is accepted, or $100,000, whichever is greater. The Contracting Officer will be the sole determiner of the amount of collateral savings.</p><p>(k) <i>Relationship to other incentives.</i> Only those benefits of an accepted VECP not rewardable under performance, design-to-cost (production unit cost, operating and support costs, reliability and maintainability), or similar incentives shall be rewarded under this clause. However, the targets of such incentives affected by the VECP shall not be adjusted because of VECP acceptance. If this contract specifies targets but provides no incentive to surpass them, the value engineering sharing shall apply only to the amount of achievement better than target.</p><p>(l) <i>Subcontracts.</i> The Contractor shall include an appropriate value engineering clause in any subcontract of $150,000 or more and may include one in subcontracts of lesser value. In calculating any adjustment in this contract''s price for instant contract savings (or negative instant contract savings), the Contractor''s allowable development and implementation costs shall include any subcontractor''s allowable development and implementation costs, and any value engineering incentive payments to a subcontractor, clearly resulting from a VECP accepted by the Government under this contract. The Contractor may choose any arrangement for subcontractor value engineering incentive payments; <i>provided,</i> that the payments shall not reduce the Government''s share of concurrent or future contract savings or collateral savings.</p><p>(m) <i>Data.</i> The Contractor may restrict the Government''s right to use any part of a VECP or the supporting data by marking the following legend on the affected parts:</p><p>''These data, furnished under the Value Engineering clause of contract {{textbox_52.248-1[0]}}, shall not be disclosed outside the Government or duplicated, used, or disclosed, in whole or in part, for any purpose other than to evaluate a value engineering change proposal submitted under the clause. This restriction does not limit the Government''s right to use information contained in these data if it has been obtained or is otherwise available from the Contractor or from another source without limitations."</p><p>If a VECP is accepted, the Contractor hereby grants the Government unlimited rights in the VECP and supporting data, except that, with respect to data qualifying and submitted as limited rights technical data, the Government shall have the rights specified in the contract modification implementing the VECP and shall appropriately mark the data. (The terms <i>unlimited rights</i> and <i>limited rights</i> are defined in part 27 of the Federal Acquisition Regulation.)</p>'
WHERE clause_version_id = 9 AND clause_name = '52.248-1';

UPDATE Clause_Fill_Ins SET fill_in_default_data = '(3) Future contract savings, which are the product of the future unit cost reduction multiplied by the number of future contract units in the sharing base.' WHERE fill_in_code = 'paragraph_a3_52.248-1'
AND clause_id IN (SELECT clause_id FROM Clauses 
WHERE clause_name = '52.248-1' AND clause_version_id = 9) ;

UPDATE Clauses
SET clause_conditions = '(A) DOCUMENT TYPE IS: SOLICITATION (B) DOCUMENT TYPE IS: AWARD (C) 52.250-3 IS: INCLUDED (D) 52.250-4 ALTERNATE II IS: INCLUDED (E) 52.250-3 ALTERNATE I IS: INCLUDED (F) 52.250-3 ALTERNATE II IS: INCLUDED (G) SAFETY ACT STATEMENTS IS: DHS HAS NOT ISSUED A SAFETY ACT DESIGNATION OR CERTIFICATION TO SUCCESSFUL CONTRACTOR BEFORE CONTRACT AWARD (H) PROCEDURES IS: NOT COMMERCIAL PROCEDURES (FAR PART 12)' -- '(A) DOCUMENT TYPE IS: SOLICITATION (B) DOCUMENT TYPE IS: AWARD (C) 52.250-3 IS: INCLUDED (D) 52.250-4 ALTERNATE II IS: INCLUDED (E) 52.250-3 ALTERNATE I IS: INCLUDED (F) 52.250-3 ALTERNATE II IS: INCLUDED (G) SAFETY ACT STATEMENTS IS: DHS HAS NOT ISSUED A SAFETY ACT DESIGNATION OR CERTIFICATION TO SUCCESSFUL OFFEROR BEFORE CONTRACT AWARD (H) PROCEDURES IS: NOT COMMERCIAL PROCEDURES (FAR PART 12)'
WHERE clause_version_id = 9 AND clause_name = '52.250-5';

UPDATE Clauses
SET effective_date = NULL -- '2014-04-01'
WHERE clause_version_id = 9 AND clause_name = '252.216-7010 Alternate I';
INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT clause_id, prescription_id FROM Prescriptions, Clauses
WHERE prescription_name IN ('216.506(d)(2)') 
AND clause_id IN (SELECT clause_id FROM Clauses WHERE clause_name = '252.216-7010 Alternate I' AND clause_version_id = 9); 
DELETE FROM Clause_Prescriptions
WHERE clause_id IN (SELECT clause_id FROM Clauses WHERE clause_name = '252.216-7010 Alternate I' AND clause_version_id = 9) 
AND prescription_id IN (SELECT prescription_id FROM Prescriptions WHERE prescription_name IN ('216.506(d)')); -- 252.216-7010 Alternate I

UPDATE Clauses
SET clause_conditions = '(A) DOCUMENT TYPE IS: SOLICITATION (B) DOCUMENT TYPE IS: AWARD (C) FULL AND OPEN EXCEPTION IS: NOT 6.302-3 INDUSTRIAL MOBILIZATION; ENGINEERING, DEVELOPMENTAL, OR RESEARCH CAPABILITY; OR EXPERT SERVICES (D) COMPETITION PREFERENCES IS: NOT RESTRICTED TO DOMESTIC END PRODUCTS (E) EXCEPTIONS IS: NOT 25.202 -- EXCEPTION TO THE BUY AMERICAN STATUTE (F) 252.225-7021 IS: INCLUDED (G) 252.225-7021 ALTERNATE II IS: NOT INCLUDED (H) 252.225-7036 IS: NOT INCLUDED (I) 252.225-7036 ALTERNATE I IS: NOT INCLUDED (J) 252.225-7036 ALTERNATE II IS: NOT INCLUDED (K) 252.225-7036 ALTERNATE III IS: NOT INCLUDED (L) 252.225-7036 ALTERNATE IV IS: NOT INCLUDED (M) 252.225-7036 ALTERNATE V IS: NOT INCLUDED (N) PURPOSE OF PROCUREMENT IS: FOR USE IN SUPPORT OF OPERATIONS IN AFGHANISTAN (O) 252.225-7001 IS: NOT INCLUDED (P) EXEMPTIONS IS: NOT 225.7501 -- EXEMPT FROM THE BALANCE OF PAYMENT PROGRAM (Q) SUPPLIES IS: END ITEM LISTED IN DFARS SUBPART 225.401-70 (SUBJECT TO TRADE AGREEMENTS) (R) PROCEDURES IS: PROCEDURES IN SUPPORT OF OPERATIONS IN AFGHANISTAN (DFARS 225.7703-1)' -- '(A) DOCUMENT TYPE IS: SOLICITATION (B) DOCUMENT TYPE IS: AWARD (C) FULL AND OPEN EXCEPTION IS: 6.302-3 INDUSTRIAL MOBILIZATION; ENGINEERING, DEVELOPMENTAL, OR RESEARCH CAPABILITY; OR EXPERT SERVICES (D) COMPETITION PREFERENCES IS: NOT RESTRICTED TO DOMESTIC END PRODUCTS (E) EXCEPTIONS IS: NOT 25.202 -- EXCEPTION TO THE BUY AMERICAN STATUTE (F) 252.225-7021 IS: INCLUDED (G) 252.225-7021 ALTERNATE II IS: NOT INCLUDED (H) 252.225-7036 IS: NOT INCLUDED (I) 252.225-7036 ALTERNATE I IS: NOT INCLUDED (J) 252.225-7036 ALTERNATE II IS: NOT INCLUDED (K) 252.225-7036 ALTERNATE III IS: NOT INCLUDED (L) 252.225-7036 ALTERNATE IV IS: NOT INCLUDED (M) 252.225-7036 ALTERNATE V IS: NOT INCLUDED (N) PURPOSE OF PROCUREMENT IS: FOR USE IN SUPPORT OF OPERATIONS IN AFGHANISTAN (O) 252.225-7001 IS: NOT INCLUDED (P) EXEMPTIONS IS: NOT 225.7501 -- EXEMPT FROM THE BALANCE OF PAYMENT PROGRAM (Q) SUPPLIES IS: END ITEM LISTED IN DFARS SUBPART 225.401-70 (SUBJECT TO TRADE AGREEMENTS) (R) PROCEDURES IS: PROCEDURES IN SUPPORT OF OPERATIONS IN AFGHANISTAN (DFARS 225.7703-1)'
WHERE clause_version_id = 9 AND clause_name = '252.225-7001 Alternate I';

UPDATE Clauses
SET effective_date = '2015-10-01' -- '2014-11-01'
WHERE clause_version_id = 9 AND clause_name = '252.225-7021 Alternate II';

UPDATE Clauses
SET effective_date = '2015-10-01' -- '2014-11-01'
WHERE clause_version_id = 9 AND clause_name = '252.225-7045 Alternate I';

UPDATE Clauses
SET effective_date = '2015-10-01' -- '2014-11-01'
WHERE clause_version_id = 9 AND clause_name = '252.225-7045 Alternate II';

UPDATE Clauses
SET effective_date = '2015-10-01' -- '2014-11-01'
WHERE clause_version_id = 9 AND clause_name = '252.225-7045 Alternate III';

UPDATE Clauses
SET inclusion_cd = 'R' -- F
WHERE clause_version_id = 9 AND clause_name = '252.227-7005 Alternate I';

UPDATE Clauses
SET clause_data = '<p>Alternate I. As prescribed in 229.402-70(a) and (a)(2), use the following clause, which adds a paragraph (d) not included in the basic clause;</p><p>(a) Prices set forth in this contract are exclusive of all taxes and duties from which the United States Government is exempt by virtue of tax agreements between the United States Government and the Contractor''s government. The following taxes or duties have been excluded from the contract price:</p><p>NAME OF TAX: {{textbox_252.229-7001[0]}} <i>[Offeror insert]</i></p><p>RATE (PERCENTAGE): {{textbox_252.229-7001[1]}} <i>[Offeror insert]</i></p><p>(b) The Contractor''s invoice shall list separately the gross price, amount of tax deducted, and net price charged.</p><p>(c) When items manufactured to United States Government specifications are being acquired, the Contractor shall identify the materials or components intended to be imported in order to ensure that relief from import duties is obtained. If the Contractor intends to use imported products from inventories on hand, the price of which includes a factor for import duties, the Contractor shall ensure the United States Government''s exemption from these taxes. The Contractor may obtain a refund of the import duties from its government or request the duty-free import of an amount of supplies or components corresponding to that used from inventory for this contract.</p><p>(d) Tax relief will be claimed in Germany pursuant to the provisions of the Agreement Between the United States of America and Germany Concerning Tax Relief to be Accorded by Germany to United States Expenditures in the Interest of Common Defense. The Contractor shall use Abwicklungsschein fuer abgabenbeguenstigte Lieferungen/Leistungen nach dem Offshore Steuerabkommen (Performance Certificate for Tax-Free Deliveries/Performance according to the Offshore Tax Relief Agreement) or other documentary evidence acceptable to the German tax authorities. All purchases made and paid for on a tax-free basis during a 30-day period may be accumulated, totaled, and reported as tax-free.</p>'
--               '<p>Alternate I. As prescribed in 229.402-70(a) and (a)(2), use the following clause, which adds a paragraph (d) not included in the basic clause;</p><p>(a) Prices set forth in this contract are exclusive of all taxes and duties from which the United States Government is exempt by virtue of tax agreements between the United States Government and the Contractor''s government. The following taxes or duties have been excluded from the contract price:</p><p>NAME OF TAX: <i>[Offeror insert]</i></p><p>RATE (PERCENTAGE): <i>[Offeror insert]</i></p><p>(b) The Contractor''s invoice shall list separately the gross price, amount of tax deducted, and net price charged.</p><p>(c) When items manufactured to United States Government specifications are being acquired, the Contractor shall identify the materials or components intended to be imported in order to ensure that relief from import duties is obtained. If the Contractor intends to use imported products from inventories on hand, the price of which includes a factor for import duties, the Contractor shall ensure the United States Government''s exemption from these taxes. The Contractor may obtain a refund of the import duties from its government or request the duty-free import of an amount of supplies or components corresponding to that used from inventory for this contract.</p><p>(d) Tax relief will be claimed in Germany pursuant to the provisions of the Agreement Between the United States of America and Germany Concerning Tax Relief to be Accorded by Germany to United States Expenditures in the Interest of Common Defense. The Contractor shall use Abwicklungsschein fuer abgabenbeguenstigte Lieferungen/Leistungen nach dem Offshore Steuerabkommen (Performance Certificate for Tax-Free Deliveries/Performance according to the Offshore Tax Relief Agreement) or other documentary evidence acceptable to the German tax authorities. All purchases made and paid for on a tax-free basis during a 30-day period may be accumulated, totaled, and reported as tax-free.</p>'
WHERE clause_version_id = 9 AND clause_name = '252.229-7001 Alternate I';

INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size, fill_in_group_number, fill_in_placeholder, fill_in_default_data, fill_in_display_rows, fill_in_for_table, fill_in_heading) 
SELECT clause_id, 'textbox_252.229-7001[0]', 'S', 100, NULL, NULL, NULL, NULL, 0, NULL FROM Clauses WHERE clause_name = '252.229-7001 Alternate I' AND clause_version_id = 9; 
INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size, fill_in_group_number, fill_in_placeholder, fill_in_default_data, fill_in_display_rows, fill_in_for_table, fill_in_heading) 
SELECT clause_id, 'textbox_252.229-7001[1]', 'S', 100, NULL, NULL, NULL, NULL, 0, NULL FROM Clauses WHERE clause_name = '252.229-7001 Alternate I' AND clause_version_id = 9; 


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT clause_id, prescription_id FROM Prescriptions, Clauses
WHERE prescription_name IN ('235.072(b)(2)') 
AND clause_id IN (SELECT clause_id FROM Clauses WHERE clause_name = '252.235-7003 Alternate I' AND clause_version_id = 9); 
DELETE FROM Clause_Prescriptions
WHERE clause_id IN (SELECT clause_id FROM Clauses WHERE clause_name = '252.235-7003 Alternate I' AND clause_version_id = 9) 
AND prescription_id IN (SELECT prescription_id FROM Prescriptions WHERE prescription_name IN ('235.072(b)(1)')); -- 252.235-7003 Alternate I


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT clause_id, prescription_id FROM Prescriptions, Clauses
WHERE prescription_name IN ('237.7003(a)(2)') 
AND clause_id IN (SELECT clause_id FROM Clauses WHERE clause_name = '252.237-7002 Alternate I' AND clause_version_id = 9); 

UPDATE Clauses
SET inclusion_cd = 'R' -- F
WHERE clause_version_id = 9 AND clause_name = '252.246-7001 Alternate I';

UPDATE Clauses
SET inclusion_cd = 'R' -- F
WHERE clause_version_id = 9 AND clause_name = '252.246-7001 Alternate II';

UPDATE Clauses
SET clause_rule = '(A OR B) AND C AND D AND (E AND (F OR G OR H) AND !(I AND J))' -- '(A OR B) AND C AND D AND (E AND (F OR G OR H) OR (I AND J))'
WHERE clause_version_id = 9 AND clause_name = '252.247-7023 Alternate I';
INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT clause_id, prescription_id FROM Prescriptions, Clauses
WHERE prescription_name IN ('247.574(b)(3)') 
AND clause_id IN (SELECT clause_id FROM Clauses WHERE clause_name = '252.247-7023 Alternate I' AND clause_version_id = 9); 

UPDATE Clauses
SET clause_rule = '(A OR B) AND C AND D AND (E AND (!(F OR G OR H) AND (I AND J)))' -- '(A OR B) AND C AND D AND !(F OR G OR H) AND E AND I AND J'
WHERE clause_version_id = 9 AND clause_name = '252.247-7023 Alternate II';
INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT clause_id, prescription_id FROM Prescriptions, Clauses
WHERE prescription_name IN ('247.574(b)(2)') 
AND clause_id IN (SELECT clause_id FROM Clauses WHERE clause_name = '252.247-7023 Alternate II' AND clause_version_id = 9); 

UPDATE Clauses
SET clause_conditions = '(A) DOCUMENT TYPE IS: SOLICITATION\n(B) DOCUMENT TYPE IS: AWARD\n(C) CONTRACT TYPE IS: FIXED PRICE\n(D) PERFORMANCE REQUIREMENTS 2 IS: FIRST ARTICLE APPROVAL IS REQUIRED\n(E) FIRST ARTICLE CONDITIONS IS: CONTRACTOR TESTING IS REQUIRED\n(F) CONTRACT TYPE IS: COST REIMBURSEMENT\n(G) FIRST ARTICLE CONDITIONS IS: FIRST ARTICLE AND PRODUCTION QUANTITY ARE TO BE PRODUCED AT THE SAME FACILITY\n(H) PROCEDURES IS: NOT COMMERCIAL PROCEDURES (FAR PART 12)' -- '(A) DOCUMENT TYPE IS: SOLICITATION\n(B) DOCUMENT TYPE IS: AWARD\n(C) CONTRACT TYPE IS: FIXED PRICE\n(D) PERFORMANCE REQUIREMENTS 2 IS: FIRST ARTICLE APPROVAL IS REQUIRED\n(E) FIRST ARTICLE CONDITIONS IS: GOVERNMENT TESTING IS REQUIRED\n(F) CONTRACT TYPE IS: COST REIMBURSEMENT\n(G) FIRST ARTICLE CONDITIONS IS: FIRST ARTICLE AND PRODUCTION QUANTITY ARE TO BE PRODUCED AT THE SAME FACILITY\n(H) PROCEDURES IS: NOT COMMERCIAL PROCEDURES (FAR PART 12)'
WHERE clause_version_id = 9 AND clause_name = '52.209-3 Alternate I';

UPDATE Clauses
SET clause_conditions = '(A) DOCUMENT TYPE IS: SOLICITATION\n(B) DOCUMENT TYPE IS: AWARD\n(C) CONTRACT TYPE IS: FIXED PRICE\n(D) PERFORMANCE REQUIREMENTS 2 IS: FIRST ARTICLE APPROVAL IS REQUIRED\n(E) FIRST ARTICLE CONDITIONS IS: CONTRACTOR TESTING IS REQUIRED\n(F) CONTRACT TYPE IS: COST REIMBURSEMENT\n(G) FIRST ARTICLE CONDITIONS IS: CONTRACTOR IS AUTHORIZED TO PURCHASE MATERIAL OR COMMENCE PRODUCTION BEFORE FIRST ARTICLE APPROVAL\n(H) PROCEDURES IS: NOT COMMERCIAL PROCEDURES (FAR PART 12)' -- '(A) DOCUMENT TYPE IS: SOLICITATION\n(B) DOCUMENT TYPE IS: AWARD\n(C) CONTRACT TYPE IS: FIXED PRICE\n(D) PERFORMANCE REQUIREMENTS 2 IS: FIRST ARTICLE APPROVAL IS REQUIRED\n(E) FIRST ARTICLE CONDITIONS IS: GOVERNMENT TESTING IS REQUIRED\n(F) CONTRACT TYPE IS: COST REIMBURSEMENT\n(G) FIRST ARTICLE CONDITIONS IS: CONTRACTOR IS AUTHORIZED TO PURCHASE MATERIAL OR COMMENCE PRODUCTION BEFORE FIRST ARTICLE APPROVAL\n(H) PROCEDURES IS: NOT COMMERCIAL PROCEDURES (FAR PART 12)'
WHERE clause_version_id = 9 AND clause_name = '52.209-3 Alternate II';

UPDATE Clauses
SET clause_conditions = '(A) DOCUMENT TYPE IS: SOLICITATION\n(B) DOCUMENT TYPE IS: AWARD\n(C) CONTRACT TYPE IS: FIXED PRICE\n(D) PERFORMANCE REQUIREMENTS 2 IS: FIRST ARTICLE APPROVAL IS REQUIRED\n(E) FIRST ARTICLE CONDITIONS IS: GOVERNMENT TESTING IS REQUIRED\n(F) CONTRACT TYPE IS: COST REIMBURSEMENT\n(G) FIRST ARTICLE CONDITIONS IS: FIRST ARTICLE AND PRODUCTION QUANTITY ARE TO BE PRODUCED AT THE SAME FACILITY\n(H) PROCEDURES IS: NOT COMMERCIAL PROCEDURES (FAR PART 12)' -- '(A) DOCUMENT TYPE IS: SOLICITATION\n(B) DOCUMENT TYPE IS: AWARD\n(C) CONTRACT TYPE IS: FIXED PRICE\n(D) PERFORMANCE REQUIREMENTS 2 IS: FIRST ARTICLE APPROVAL IS REQUIRED\n(E) FIRST ARTICLE CONDITIONS IS: CONTRACTOR TESTING IS REQUIRED\n(F) CONTRACT TYPE IS: COST REIMBURSEMENT\n(G) FIRST ARTICLE CONDITIONS IS: FIRST ARTICLE AND PRODUCTION QUANTITY ARE TO BE PRODUCED AT THE SAME FACILITY\n(H) PROCEDURES IS: NOT COMMERCIAL PROCEDURES (FAR PART 12)'
WHERE clause_version_id = 9 AND clause_name = '52.209-4 Alternate I';

UPDATE Clauses
SET clause_conditions = '(A) DOCUMENT TYPE IS: SOLICITATION\n(B) DOCUMENT TYPE IS: AWARD\n(C) CONTRACT TYPE IS: FIXED PRICE\n(D) PERFORMANCE REQUIREMENTS 2 IS: FIRST ARTICLE APPROVAL IS REQUIRED\n(E) FIRST ARTICLE CONDITIONS IS: GOVERNMENT TESTING IS REQUIRED\n(F) CONTRACT TYPE IS: COST REIMBURSEMENT\n(G) FIRST ARTICLE CONDITIONS IS: CONTRACTOR IS AUTHORIZED TO PURCHASE MATERIAL OR COMMENCE PRODUCTION BEFORE FIRST ARTICLE APPROVAL\n(H) PROCEDURES IS: NOT COMMERCIAL PROCEDURES (FAR PART 12)' -- '(A) DOCUMENT TYPE IS: SOLICITATION\n(B) DOCUMENT TYPE IS: AWARD\n(C) CONTRACT TYPE IS: FIXED PRICE\n(D) PERFORMANCE REQUIREMENTS 2 IS: FIRST ARTICLE APPROVAL IS REQUIRED\n(E) FIRST ARTICLE CONDITIONS IS: CONTRACTOR TESTING IS REQUIRED\n(F) CONTRACT TYPE IS: COST REIMBURSEMENT\n(G) FIRST ARTICLE CONDITIONS IS: CONTRACTOR IS AUTHORIZED TO PURCHASE MATERIAL OR COMMENCE PRODUCTION BEFORE FIRST ARTICLE APPROVAL\n(H) PROCEDURES IS: NOT COMMERCIAL PROCEDURES (FAR PART 12)'
WHERE clause_version_id = 9 AND clause_name = '52.209-4 Alternate II';

UPDATE Clauses
SET commercial_status = 'R (RULE APPLIES)' -- 'R (ALWAYS)'
WHERE clause_version_id = 9 AND clause_name = '52.212-3 Alternate I';

UPDATE Clauses
SET commercial_status = 'R (RULE APPLIES)' -- 'R (ALWAYS)'
WHERE clause_version_id = 9 AND clause_name = '52.212-4 Alternate I';

UPDATE Clauses
SET effective_date = '2015-10-01' -- '2015-04-01'
WHERE clause_version_id = 9 AND clause_name = '52.212-5 Alternate II';

UPDATE Clauses
SET clause_data = '<p>Alternate II (OCT 1997). As prescribed in 15.209(a)(2), add a paragraph (c)(9) substantially the same as the following to the basic clause:</p><p>{{paragraph_c9_52.215-1-AltII}}</p>'
--               '<p>Alternate II (OCT 1997). As prescribed in 15.209(a)(2), add a paragraph (c)(9) substantially the same as the following to the basic clause:</p><p>(9) Offerors may submit proposals that depart from stated requirements. Such proposals shall clearly identify why the acceptance of the proposal would be advantageous to the Government. Any deviations from the terms and conditions of the solicitation, as well as the comparative advantage to the Government, shall be clearly identified and explicitly defined. The Government reserves the right to amend the solicitation to allow all offerors an opportunity to submit revised proposals based on the revised requirements.</p>'
WHERE clause_version_id = 9 AND clause_name = '52.215-1 Alternate II';

INSERT INTO Clause_Fill_Ins (clause_id, fill_in_code, fill_in_type, fill_in_max_size, fill_in_group_number, fill_in_placeholder, fill_in_default_data, fill_in_display_rows, fill_in_for_table, fill_in_heading) 
SELECT clause_id, 'paragraph_c9_52.215-1-AltII', 'M', 1000, NULL, NULL, '(9) Offerors may submit proposals that depart from stated requirements. Such proposals shall clearly identify why the acceptance of the proposal would be advantageous to the Government. Any deviations from the terms and conditions of the solicitation, as well as the comparative advantage to the Government, shall be clearly identified and explicitly defined. The Government reserves the right to amend the solicitation to allow all offerors an opportunity to submit revised proposals based on the revised requirements.', 12, 0, NULL FROM Clauses WHERE clause_name = '52.215-1 Alternate II' AND clause_version_id = 9; 

UPDATE Clauses
SET clause_conditions = '(A) DOCUMENT TYPE IS: SOLICITATION\n(B) DOCUMENT TYPE IS: AWARD\n(C) CONTRACT VALUE IS: GREATER THAN SAT\n(D) SERVICES IS: UTILITY SERVICES\n(E) UTILITY SERVICE CONDITIONS IS: NOT UTILITY SERVICE RATES DO NOT EXCEED THOSE ESTABLISHED TO APPLY UNIFORMLY TO THE GENERAL PUBLIC, PLUS ANY APPLICABLE REASONABLE CONNECTION CHARGE\n(F) COMMERCIAL ITEMS IS: NOT THE SUPPLIES ARE COMMERCIAL ITEMS\n(G) COMMERCIAL SERVICES IS: NO\n(H) PROCEDURES IS: NEGOTIATION (FAR PART 15)\n(I) CONTRACT TYPE IS: COST REIMBURSEMENT\n(J) BUSINESS TYPE IS: STATE GOVERNMENT\n(K) BUSINESS TYPE IS: LOCAL GOVERNMENT\n(L) BUSINESS TYPE IS: EDUCATIONAL INSTITUTION\n(M) BUSINESS TYPE IS: NON-PROFIT ORGANIZATION\n(N) PROCEDURES IS: NOT COMMERCIAL PROCEDURES (FAR PART 12)' -- '(A) DOCUMENT TYPE IS: SOLICITATION (B) DOCUMENT TYPE IS: AWARD (C) CONTRACT VALUE IS: GREATER THAN SAT (D) SERVICES IS: UTILITY SERVICES (E) UTILITY SERVICE CONDITIONS IS: NOT UTILITY SERVICE RATES DO NOT EXCEED THOSE ESTABLISHED TO APPLY UNIFORMLY TO THE GENERAL PUBLIC, PLUS ANY APPLICABLE REASONABLE CONNECTION CHARGE (F) COMMERCIAL ITEMS IS: NOT THE SUPPLIES ARE COMMERCIAL ITEMS (G) COMMERCIAL SERVICES IS: NO (H) PROCEDURES IS: NEGOTIATION (FAR PART 15) (I) CONTRACT TYPE IS: COST REIMBURSEMENT (J) BUSINESS TYPE IS: STATE GOVERNMENT (K) BUSINESS TYPE IS: LOCAL GOVERNMENT (L) BUSINESS TYPE IS: EDUCATIONAL INSTITUTION (M) BUSINESS TYPE IS: NON-PROFIT ORGANIZATION (N) PROCEDURES IS: NOT COMMERCIAL PROCEDURES (FAR PART 12)'
WHERE clause_version_id = 9 AND clause_name = '52.215-2 Alternate II';

UPDATE Clauses
SET clause_conditions = '(A) DOCUMENT TYPE IS: SOLICITATION\n(B) DOCUMENT TYPE IS: AWARD\n(C) CONTRACT VALUE IS: GREATER THAN SAT\n(D) SERVICES IS: UTILITY SERVICES\n(E) UTILITY SERVICE CONDITIONS IS: NOT UTILITY SERVICE RATES DO NOT EXCEED THOSE ESTABLISHED TO APPLY UNIFORMLY TO THE GENERAL PUBLIC, PLUS ANY APPLICABLE REASONABLE CONNECTION CHARGE\n(F) COMMERCIAL ITEMS IS: NOT THE SUPPLIES ARE COMMERCIAL ITEMS\n(G) COMMERCIAL SERVICES IS: NO\n(H) PROCEDURES IS: NEGOTIATION (FAR PART 15)\n(I) WAIVERS IS: 15.209/25.1001 -- WAIVER OF REQUIREMENT TO EXAMINATION OF RECORDS BY THE COMPTROLLER GENERAL\n(J) PROCEDURES IS: NOT COMMERCIAL PROCEDURES (FAR PART 12)' -- '(A) DOCUMENT TYPE IS: SOLICITATION (B) DOCUMENT TYPE IS: AWARD (C) CONTRACT VALUE IS: GREATER THAN SAT (D) SERVICES IS: UTILITY SERVICES (E) UTILITY SERVICE CONDITIONS IS: NOT UTILITY SERVICE RATES DO NOT EXCEED THOSE ESTABLISHED TO APPLY UNIFORMLY TO THE GENERAL PUBLIC, PLUS ANY APPLICABLE REASONABLE CONNECTION CHARGE (F) COMMERCIAL ITEMS IS: NOT THE SUPPLIES ARE COMMERCIAL ITEMS (G) COMMERCIAL SERVICES IS: NO (H) PROCEDURES IS: NEGOTIATION (FAR PART 15) (I) WAIVERS IS: 15.209/25.1001 -- WAIVER OF REQUIREMENT TO EXAMINATION OF RECORDS BY THE COMPTROLLER GENERAL (J) PROCEDURES IS: NOT COMMERCIAL PROCEDURES (FAR PART 12)'
WHERE clause_version_id = 9 AND clause_name = '52.215-2 Alternate III';


INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT clause_id, prescription_id FROM Prescriptions, Clauses
WHERE prescription_name IN ('15.408(I)') 
AND clause_id IN (SELECT clause_id FROM Clauses WHERE clause_name = '52.215-20 Alternate I' AND clause_version_id = 9); 
DELETE FROM Clause_Prescriptions
WHERE clause_id IN (SELECT clause_id FROM Clauses WHERE clause_name = '52.215-20 Alternate I' AND clause_version_id = 9) 
AND prescription_id IN (SELECT prescription_id FROM Prescriptions WHERE prescription_name IN ('15.408(l)')); -- 52.215-20 Alternate I

UPDATE Clauses
SET clause_conditions = '(A) DOCUMENT TYPE IS: SOLICITATION (B) DOCUMENT TYPE IS: AWARD (C) ISSUING OFFICE IS: NOT DEPARTMENT OF DEFENSE (D) CONTRACT VALUE IS: GREATER THAN SAT (E) CONTRACT TYPE IS: COST REIMBURSEMENT (F) ISSUING OFFICE IS: DEPARTMENT OF DEFENSE (G) CONTRACT VALUE IS: GREATER THAN 750000 (H) CONTRACT TYPE IS: FIRM FIXED PRICE (I) ADEQUATE PRICE COMPETITION IS: YES (J) CONTRACT TYPE IS: FIXED PRICE WITH ECONOMIC PRICE ADJUSTMENT -- ACTUAL COSTS (K) CONTRACT TYPE IS: FIXED PRICE WITH ECONOMIC PRICE ADJUSTMENT -- COST INDICES (L) CONTRACT TYPE IS: FIXED PRICE WITH ECONOMIC PRICE ADJUSTMENT -- ESTABLISHED PRICES (M) COMMERCIAL ITEMS IS: THE SUPPLIES ARE COMMERCIAL ITEMS (N) COMMERCIAL SERVICES IS: YES (O) CONTRACT TYPE IS: FIXED PRICE INCENTIVE (COST BASED) (P) CONTRACT TYPE IS: FIXED PRICE INCENTIVE (SUCCESSIVE TARGETS) (Q) EXCESSIVE PASS-THROUGH CHARGES IS: NO (R) PROCEDURES IS: NOT COMMERCIAL PROCEDURES (FAR PART 12)' -- '(A) DOCUMENT TYPE IS: SOLICITATION (B) DOCUMENT TYPE IS: AWARD (C) ISSUING OFFICE IS: NOT DEPARTMENT OF DEFENSE (D) CONTRACT VALUE IS: GREATER THAN SAT (E) CONTRACT TYPE IS: COST REIMBURSEMENT (F) ISSUING OFFICE IS: DEPARTMENT OF DEFENSE (G) CONTRACT VALUE IS: GREATER THAN 750000 (H) CONTRACT TYPE IS: FIRM FIXED PRICE (I) ADEQUATE PRICE COMPETITION IS: YES (J) CONTRACT TYPE IS: FIXED PRICE WITH ECONOMIC PRICE ADJUSTMENT -- ACTUAL COSTS (K) CONTRACT TYPE IS: FIXED PRICE WITH ECONOMIC PRICE ADJUSTMENT -- COST INDICES (L) CONTRACT TYPE IS: FIXED PRICE WITH ECONOMIC PRICE ADJUSTMENT -- ESTABLISHED PRICES (M) COMMERCIAL ITEMS IS: THE SUPPLIES ARE COMMERCIAL ITEMS (N) COMMERCIAL SERVICES IS: YES (O) CONTRACT TYPE IS: FIXED PRICE INCENTIVE (COST BASED) (P) CONTRACT TYPE IS: FIXED PRICE INCENTIVE (SUCCESSIVE TARGETS) (Q) EXCESS PASS-THROUGH CHARGES IS: NO (R) PROCEDURES IS: NOT COMMERCIAL PROCEDURES (FAR PART 12)'
WHERE clause_version_id = 9 AND clause_name = '52.215-23 Alternate I';

UPDATE Clauses
SET clause_conditions = '(A) DOCUMENT TYPE IS: SOLICITATION\n(B) DOCUMENT TYPE IS: AWARD\n(C) PERFORMANCE REQUIREMENTS 2 IS: MAKE OR BUY PROGRAM\n(D) PROCEDURES IS: NEGOTIATION (FAR PART 15)\n(E) CONTRACT TYPE IS: COST PLUS INCENTIVE FEE (COST BASED)\n(F) PROCEDURES IS: NOT COMMERCIAL PROCEDURES (FAR PART 12)' -- '(A) DOCUMENT TYPE IS: SOLICITATION\n(B) DOCUMENT TYPE IS: AWARD\n(C) PERFORMANCE REQUIREMENTS 2 IS: MAKE OR BUY PROGRAM\n(D) PROCEDURES IS: NEGOTIATION (FAR PART 15)\n(E) CONTRACT TYPE IS: COST PLUS INCENTIVE FEE (COST BASED)'
, clause_rule = '(A OR B) AND C AND D AND E AND F' -- '(A OR B) AND C AND D AND E'
WHERE clause_version_id = 9 AND clause_name = '52.215-9 Alternate II';

UPDATE Clauses
SET clause_conditions = '(A) DOCUMENT TYPE IS: SOLICITATION\n(B) DOCUMENT TYPE IS: AWARD\n(C) TYPE OF INDEFINITE DELIVERY IS: REQUIREMENTS\n(D) SERVICES IS: NOT PERSONAL SERVICES\n(E) SERVICE CHARACTERISTICS IS: THE SERVICE BEING PROCURED INCLUDES THE FURNISHING OF SUPPLIES\n(F) MISCELLANEOUS PROGRAM CHARACTERISTICS IS: THE ACQUISITION COVERS ESTIMATED REQUIREMENTS THAT EXCEED A SPECIFIC GOVERNMENT ACTIVITY''S INTERNAL CAPABILITY TO PRODUCE OR PERFORM\n(G) PROCEDURES IS: NOT COMMERCIAL PROCEDURES (FAR PART 12)' -- '(A) DOCUMENT TYPE IS: SOLICITATION (B) DOCUMENT TYPE IS: AWARD (C) TYPE OF INDEFINITE DELIVERY IS: REQUIREMENTS (D) SERVICES IS: NOT PERSONAL SERVICES (E) SERVICE CHARACTERISTICS IS: THE SERVICE BEING PROCURED INCLUDES THE FURNISHING OF SUPPLIES (F) MISCELLANEOUS PROGRAM CHARACTERISTICS IS: THE ACQUISITION COVERS ESTIMATED REQUIREMENTS THAT EXCEED A SPECIFIC GOVERNMENT ACTIVITY''S INTERNAL CAPABILITY TO PRODUCE OR PERFORM (G) PROCEDURES IS: NOT COMMERCIAL PROCEDURES (FAR PART 12)'
WHERE clause_version_id = 9 AND clause_name = '52.216-21 Alternate I';

UPDATE Clauses
SET clause_conditions = '(A) DOCUMENT TYPE IS: SOLICITATION (B) DOCUMENT TYPE IS: AWARD (C) CONTRACT TYPE IS: COST REIMBURSEMENT (D) CONTRACT TYPE IS: TIME AND MATERIALS (E) CONTRACT TYPE IS: NOT LABOR HOUR (F) COMMERCIAL ITEMS IS: THE SUPPLIES ARE COMMERCIAL ITEMS (G) COMMERCIAL SERVICES IS: YES (H) BUSINESS TYPE IS: NOT LOCAL GOVERNMENT (I) BUSINESS TYPE IS: NOT STATE GOVERNMENT (J) BUSINESS TYPE IS: NON-PROFIT ORGANIZATION (K) BUSINESS TYPE IS: NOT EDUCATIONAL INSTITUTION (L) BUSINESS TYPE IS: NONPROFIT ORGANIZATION EXEMPTED UNDER OMB CIRCULAR NO. A-122 (M) PROCEDURES IS: NOT COMMERCIAL PROCEDURES (FAR PART 12)' -- '(A) DOCUMENT TYPE IS: SOLICITATION (B) DOCUMENT TYPE IS: AWARD (C) CONTRACT TYPE IS: COST REIMBURSEMENT (D) CONTRACT TYPE IS: TIME AND MATERIALS (E) CONTRACT TYPE IS: NOT LABOR HOUR (F) COMMERCIAL ITEMS IS: THE SUPPLIES ARE COMMERCIAL ITEMS (G) COMMERCIAL SERVICES IS: YES (H) BUSINESS TYPE IS: NOT LOCAL GOVERNMENT (I) BUSINESS TYPE IS: NOT STATE GOVERNMENT (J) BUSINESS TYPE IS: NOT NON-PROFIT ORGANIZATION (K) BUSINESS TYPE IS: NOT EDUCATIONAL INSTITUTION (L) BUSINESS TYPE IS: NONPROFIT ORGANIZATION EXEMPTED UNDER OMB CIRCULAR NO. A-122 (M) PROCEDURES IS: NOT COMMERCIAL PROCEDURES (FAR PART 12)'
WHERE clause_version_id = 9 AND clause_name = '52.216-7 Alternate IV';

UPDATE Clauses
SET effective_date = '2014-10-01' -- '2001-10-01'
WHERE clause_version_id = 9 AND clause_name = '52.219-9 Alternate I';

UPDATE Clauses
SET effective_date = '2015-10-01' -- '2014-06-01'
WHERE clause_version_id = 9 AND clause_name = '52.223-13 Alternate I';
INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
SELECT clause_id, prescription_id FROM Prescriptions, Clauses
WHERE prescription_name IN ('23.705(b)(2)') 
AND clause_id IN (SELECT clause_id FROM Clauses WHERE clause_name = '52.223-13 Alternate I' AND clause_version_id = 9); 
DELETE FROM Clause_Prescriptions
WHERE clause_id IN (SELECT clause_id FROM Clauses WHERE clause_name = '52.223-13 Alternate I' AND clause_version_id = 9) 
AND prescription_id IN (SELECT prescription_id FROM Prescriptions WHERE prescription_name IN ('23.705(b)(1)')); -- 52.223-13 Alternate I

UPDATE Clauses
SET clause_conditions = '(A) DOCUMENT TYPE IS: SOLICITATION (B) DOCUMENT TYPE IS: AWARD (C) SERVICES IS: DISMANTLING, DEMOLITION, OR REMOVAL OF IMPROVEMENTS (D) DISMANTLING PAYMENT ARRANGEMENT IS: GOVERNMENT SHALL MAKE PAYMENT TO CONTRACTOR IN ADDITION TO ANY TITLE TO PROPERTY THE CONTRACTOR MAY RECEIVE (E) DISMANTLING PAYMENT ARRANGEMENT IS: ALL MATERIAL IS TO BE RETAINED BY GOVERNMENT (F) PROCEDURES IS: NOT COMMERCIAL PROCEDURES (FAR PART 12)' -- '(A) DOCUMENT TYPE IS: SOLICITATION\n(B) DOCUMENT TYPE IS: AWARD\n(C) SERVICES IS: DISMANTLING, DEMOLITION, OR REMOVAL OF IMPROVEMENTS\n(D) DISMANTLING PAYMENT ARRANGEMENT IS: GOVERNMENT SHALL MAKE PAYMENT TO CONTRACTOR IN ADDITION TO ANY TITLE TO PROPERTY THE CONTRACTOR MAY RECEIVE\n(E) DISMANTLING PAYMENT ARRANGEMENT IS: ALL MATERIAL IS TO BE RETAINED BY GOVERNMENT\n(F) PROCEDURES IS: NOT COMMERCIAL PROCEDURES (FAR PART 12)'
WHERE clause_version_id = 9 AND clause_name = '52.237-4 Alternate I';

UPDATE Clauses
SET clause_rule = '((A OR B) AND F AND L AND ((!C AND !D) OR G OR ((H AND I) OR J))) AND !(G AND K)' -- '((A OR B) AND F AND ((!C AND !D) OR G OR ((H AND I) OR J))) AND !(G AND K) AND L'
WHERE clause_version_id = 9 AND clause_name = '52.245-1 Alternate I';

UPDATE Clauses
SET clause_rule = '((A OR B) AND F AND N AND ((C OR D) OR G OR ((H AND I) OR J))) OR (A AND E AND F) AND (K AND L AND M)' -- '((A OR B) AND F AND ((C OR D) OR G OR ((H AND I) OR J))) OR (A AND E AND F) AND (K AND L AND M) AND N'
WHERE clause_version_id = 9 AND clause_name = '52.245-1 Alternate II';

UPDATE Clauses
SET clause_conditions = '(A) DOCUMENT TYPE IS: SOLICITATION\n(B) DOCUMENT TYPE IS: AWARD\n(C) CONTRACT VALUE IS: GREATER THAN SAT\n(D) SERVICES IS: NOT RESEARCH AND DEVELOPMENT\n(E) SERVICES IS: ENGINEERING\n(F) BUSINESS TYPE IS: NOT-FOR-PROFIT ORGANIZATION\n(G) BUSINESS TYPE IS: NON-PROFIT ORGANIZATION\n(H) SERVICES IS: NOT PERSONAL SERVICES\n(I) SERVICES IS: NOT PRODUCT OR COMPONENT IMPROVEMENT\n(J) COMMERCIAL ITEMS IS: THE SUPPLIES ARE COMMERCIAL ITEMS\n(K) VALUE ENGINEERING CONDITIONS IS: NOT PACKAGING SPECIFICATIONS ARE INVOLVED\n(L) EXEMPTIONS IS: NOT 48.102 -- EXEMPT FROM VALUE ENGINEERING\n(M) VALUE ENGINEERING CONDITIONS IS: MANDATORY VALUE ENGINEERING PROGRAM REQUIREMENT IS APPROPRIATE\n(N) SERVICES IS: NOT ARCHITECT-ENGINEERING\n(O) PROCEDURES IS: NOT COMMERCIAL PROCEDURES (FAR PART 12)' -- '(A) DOCUMENT TYPE IS: SOLICITATION (B) DOCUMENT TYPE IS: AWARD (C) CONTRACT VALUE IS: GREATER THAN SAT (D) SERVICES IS: NOT RESEARCH AND DEVELOPMENT (E) SERVICES IS: ENGINEERING (F) BUSINESS TYPE IS: NOT-FOR-PROFIT ORGANIZATION (G) BUSINESS TYPE IS: NON-PROFIT ORGANIZATION (H) SERVICES IS: NOT PERSONAL SERVICES (I) SERVICES IS: NOT PRODUCT OR COMPONENT IMPROVEMENT (J) COMMERCIAL ITEMS IS: THE SUPPLIES ARE COMMERCIAL ITEMS (K) VALUE ENGINEERING CONDITIONS IS: NOT PACKAGING SPECIFICATIONS ARE INVOLVED (L) EXEMPTIONS IS: NOT 48.102 -- EXEMPT FROM VALUE ENGINEERING (M) VALUE ENGINEERING CONDITIONS IS: MANDATORY VALUE ENGINEERING PROGRAM REQUIREMENT IS APPROPRIATE (N) SERVICES IS: NOT ARCHITECT-ENGINEERING (O) PROCEDURES IS: NOT COMMERCIAL PROCEDURES (FAR PART 12)'
WHERE clause_version_id = 9 AND clause_name = '52.248-1 Alternate I';

UPDATE Clause_Versions SET FAC_Number = '2005-83', DAC_Number = '20151030' WHERE Clause_Version_Id = 9;

/*
*** End of SQL Scripts at Wed Dec 02 09:22:00 EST 2015 ***
*/
