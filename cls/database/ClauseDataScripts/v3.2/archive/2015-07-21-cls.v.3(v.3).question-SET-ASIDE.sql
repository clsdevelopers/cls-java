/*
Clause Parser Run at Tue Jul 21 17:43:11 EDT 2015
(Without Correction Information)
*/
/* ===============================================
 * Prescriptions
   =============================================== */
/* ===============================================
 * Questions
   =============================================== */

UPDATE Question_Conditions
SET original_condition_text = 'SELECT ONE OR MORE BUT NOT "PARTIAL SMALL BUSINESS SET-ASIDE" AND "LOCAL AREA SET-ASIDE (STAFFORD ACT SET-ASIDE) (FAR 26.2)" TOGETHER' -- NULL
-- SET original_condition_text = 'SELECT ONE OR MORE BUT NOT "PARTIAL SMALL BUSINESS SET-ASIDE" AND "LOCAL AREA SET-ASIDE" TOGETHER' -- NULL
WHERE Question_Id = (
	SELECT A.Question_Id
    FROM Questions A
    WHERE Question_Conditions.Question_Id = A.Question_Id
    AND A.Clause_Version_Id = 3
	AND A.Question_Code = '[SET-ASIDE]'
)
;
/*
UPDATE Question_Conditions
SET original_condition_text = 'SELECT ONE OR MORE BUT NOT "PARTIAL SMALL BUSINESS SET-ASIDE" AND "LOCAL AREA SET-ASIDE (STAFFORD ACT SET-ASIDE) (FAR 26.2)" TOGETHER' -- NULL
-- SET original_condition_text = 'SELECT ONE OR MORE BUT NOT "PARTIAL SMALL BUSINESS SET-ASIDE" AND "LOCAL AREA SET-ASIDE" TOGETHER' -- NULL
WHERE question_condition_id = 7560; -- [SET-ASIDE]
*/

/*
*** End of SQL Scripts at Tue Jul 21 17:43:12 EDT 2015 ***
*/
