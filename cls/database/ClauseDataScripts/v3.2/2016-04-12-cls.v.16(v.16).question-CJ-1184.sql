UPDATE Question_Conditions
SET original_condition_text = 'ASK ONLY IF THE USER HAS SELECTED "SOLICITATION" UNDER "DOCUMENT TYPE" AND "CONTRACT VALUE" IS GREATER THAN OR EQUAL TO $500,000. DO NOT ASK IF THE USER SELECTED "COMMERCIAL PROCEDURES FAR PART (12)" UNDER "PROCEDURES"' -- 'ASK IF USER SELECTS "SOLICITATION" UNDER "DOCUMENT TYPE" AND DO NOT ASK IF THE USER SELECTED "COMMERCIAL PROCEDURES (FAR PART 12)" UNDER "PROCEDURES"'
, condition_to_question = '("SOLICITATION" = [DOCUMENT TYPE] AND [CONTRACT VALUE] IS GREATER THAN OR EQUAL TO 500000) AND "COMMERCIAL PROCEDURES (FAR PART 12)" <> [PROCEDURES]' -- '"SOLICITATION" = [DOCUMENT TYPE] AND "COMMERCIAL PROCEDURES (FAR PART 12)" <> [PROCEDURES]'
WHERE question_id IN 
  (SELECT question_id FROM Questions WHERE question_code = '[OUTSIDE U.S.]' AND clause_version_id = 16); -- [OUTSIDE U.S.]
