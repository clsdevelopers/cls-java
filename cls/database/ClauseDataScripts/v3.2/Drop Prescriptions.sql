


-- DROP TABLE Clause_Presciptions;
-- DROP TABLE Question_Presciptions;
-- DROP TABLE Prescriptions;


CREATE TABLE Prescriptions
(
	Prescription_Id      SMALLINT AUTO_INCREMENT,
	Regulation_Id        SMALLINT NOT NULL,
	Prescription_Name    VARCHAR(60) NOT NULL,
	Prescription_Url     VARCHAR(120) NULL,
	Prescription_Content TEXT NULL,
	Created_At           TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	Updated_At           TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	PRIMARY KEY (Prescription_Id)
);

CREATE UNIQUE INDEX XAKPrescriptions_Name ON Prescriptions
(
	Prescription_Name
);

CREATE INDEX XIFPrescriptions_Regulation ON Prescriptions
(
	Regulation_Id
);


-- ----------------------------------------------
CREATE TABLE Clause_Prescriptions
(
	Clause_Prescription_Id integer AUTO_INCREMENT,
	Clause_Id            integer NOT NULL,
	Prescription_Id      SMALLINT NOT NULL,
	Created_At           TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	Updated_At           TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	PRIMARY KEY (Clause_Prescription_Id)
);

CREATE UNIQUE INDEX XAK1Clause_Prescriptions ON Clause_Prescriptions
(
	Clause_Id,
	Prescription_Id
);

CREATE INDEX XIFClause_Prescriptions_Prescription_Id ON Clause_Prescriptions
(
	Prescription_Id
);

-- ----------------------------------------------
CREATE TABLE Question_Choice_Prescriptions
(
	Question_Choice_Prescription_Id INTEGER AUTO_INCREMENT,
	Question_Choce_Id    integer NOT NULL,
	Prescription_Id      SMALLINT NOT NULL,
	Created_At           TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	Updated_At           TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	PRIMARY KEY (Question_Choice_Prescription_Id)
);

CREATE UNIQUE INDEX XAKQuestion_Choice_Prescriptions ON Question_Choice_Prescriptions
(
	Question_Choce_Id,
	Prescription_Id
);

CREATE INDEX XIFQuestion_Choice_Prescriptions_Prescription_Id ON Question_Choice_Prescriptions
(
	Prescription_Id
);