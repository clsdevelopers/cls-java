## This folder has three sub-folders:
### 1 AEC Develop
For AEC develop database

### 2 AEC UAT
For AEC UAT database

### 3 ALTESS UAT
For ALTESS UAT database

## Any database changes should be stored in '1 AEC Develop' sub-folder.
<ul>
<li>When any scripts applied in the database server for the sub-folder, it should be moved to its next sub-folder.</li>
<li>When the last sub-folder' database is applied, it should move to changes-applied folder located the parent folder.</li>
</ul>

## For formal release, all files should be cleared with a formal Git version Tag.