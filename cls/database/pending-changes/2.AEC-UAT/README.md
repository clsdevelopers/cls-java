### 2 AEC UAT
For AEC UAT database

Note that the Gold Copy version 3.3 includes the following scripts

  2016-05-25-Alter-Fillin-Placeholder.sql
  2016-06-14-Document_Date.sql
  2016-3-30-New-Prescriptions.sql

This is slightly ahead of UAT, which has an earlier deployment.
  So, 2016-06-14-Document_Date.sql has NOT been deployed to UAT or Staging, but it will be deployed to GC with 3.3.