package gov.dod.cls.filter;

import gov.dod.cls.PageApi;
import gov.dod.cls.bean.session.UserSession;
import gov.dod.cls.config.ClsConfig;
import gov.dod.cls.reference.ClsPages;
import gov.dod.cls.utils.CommonUtils;

import java.io.IOException;
import java.util.Date;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class SessionInCookieFilter implements Filter {

    private static final String COOKIE_NAME = "session";
    //private static ThreadLocal<HttpServletRequest> requestLocalStorage = new ThreadLocal<HttpServletRequest>();
    //private static ThreadLocal<HttpServletResponse> responseLocalStorage = new ThreadLocal<HttpServletResponse>();
    private static SessionCryptor sessionCryptor = new SessionCryptor();
    
    public static final String SESSION_LAST_ACCESSED = "lastAccessed";

    private ClsConfig clsConfig = null;
    
    public void init(FilterConfig filterConfig) throws ServletException {
    	this.clsConfig = ClsConfig.getInstance();
    	System.out.println("SessionTimeout: " + this.clsConfig.getSessionTimeout());
    }
    
    public void destroy() {
    }
    
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
    	HttpServletRequest req = (HttpServletRequest)request;
    	String uri = req.getRequestURI();
		String token = req.getParameter(ClsPages.PARAM_TOKEN);
		boolean isToken = CommonUtils.isNotEmpty(token);
		boolean isInterviewOrFinalClause = isInterviewOrFinalClause(uri);
		if (isInterviewOrFinalClause && isToken) {
    		String queryString = req.getQueryString();
    		System.out.println(uri + "?" + queryString);
    		HttpSession session = req.getSession(true);
    		this.clsConfig.setSessionTimeout(session, true);
    		UserSession userSession = PageApi.getUserSession(session);
    		    		
    		if (userSession.validateToken(token)) {
    			SessionInCookieFilter.update(session, req, (HttpServletResponse)response);
    		} else {
    			PageApi.sendRedirect(session, req, (HttpServletResponse)response, ClsPages.index(req));
    			System.out.println("Invalid token: " + token);
    			return;
    		}
    		
    	} else {
	    	HttpSession session = null;
	    	if (!PageApi.hasUserSession(req)) {
	        	session = this.loadAttributes(req, (HttpServletResponse) response, isToken);
	        	if ((session != null) && uri.endsWith(".jsp"))
	        		SessionInCookieFilter.update(session, req, (HttpServletResponse) response);
	    	} else
	    	if (uri.endsWith(".jsp"))  {
	    		if (session == null) {
	    			session = req.getSession(true);
	    			this.clsConfig.setSessionTimeout(session, isToken);
	    		}
	    		SessionInCookieFilter.update(session, req, (HttpServletResponse) response);
	    	}
	    	if (isInterviewOrFinalClause && (session == null || !PageApi.getUserSession(session).isVerified()))
	    	{
				PageApi.sendRedirect(req, (HttpServletResponse)response, ClsPages.PAGE_INDEX);
				return;
			}

    	}
        chain.doFilter(request, response);
    }
    
    private boolean isInterviewOrFinalClause(String uri)
    {    
    	return uri.endsWith(ClsPages.PAGE_INTERVIEW) || uri.endsWith(ClsPages.PAGE_FINAL_CLAUSES) 
    			|| uri.endsWith(ClsPages.PAGE_REPORT_CLAUSES) || uri.endsWith(ClsPages.PAGE_CLAUSE_DETAIL);
    }

    private HttpSession loadAttributes(HttpServletRequest req, HttpServletResponse response, boolean isToken)  {
    	HttpSession session = null;
        Map<String, Object> attributesFromCookie = this.getSessionAttributesFromCookie(req);
        //if (attributesFromCookie != null) {
            session = req.getSession(true);
            this.clsConfig.setSessionTimeout(session, false);
            Date oLastAccessed = null;
            if (attributesFromCookie != null) {
	            for (Map.Entry<String, Object> entry : attributesFromCookie.entrySet()) {
	            	String key = entry.getKey();
	                session.setAttribute(key, entry.getValue());
	                if (SessionInCookieFilter.SESSION_LAST_ACCESSED.equals(key))
	                	oLastAccessed = (Date)entry.getValue();
	            }
            }
            else {           	
            	Date dNow = CommonUtils.getNow();
            	session.setAttribute(SessionInCookieFilter.SESSION_LAST_ACCESSED, dNow);
            	oLastAccessed = dNow;
            }
            
            if (oLastAccessed != null) {
            	Integer iTimeout = this.clsConfig.getTimeout(isToken) * 60;
            	long duration = CommonUtils.getNow().getTime() - oLastAccessed.getTime();
            	long seconds = TimeUnit.MILLISECONDS.toSeconds(duration);
            	if (seconds > iTimeout) { // (session.getMaxInactiveInterval() + 1800)) {
                	System.out.println("Session Expired (" + iTimeout + " secs). Last Accessed: " + oLastAccessed.toString());
                	SessionInCookieFilter.clearCookie(response);
            		return null;
            	}
            	if (seconds > 3) {
            		UserSession userSession = PageApi.getUserSession(session);
            		if (userSession != null) {
            			userSession.setSessionMessage(null);
            		}
            	}
            }
            	
       // }
        
        return session;
    }

    private Map<String, Object> getSessionAttributesFromCookie(HttpServletRequest req) {
        Cookie[] aCookies = req.getCookies();
        if (aCookies == null)
        	return null;
        
        Cookie cookieTarget = null;
        for (Cookie c : aCookies) {
            if (c.getName().equals(SessionInCookieFilter.COOKIE_NAME)) {
            	cookieTarget = c;
            	break;
            }
        }

        Map<String, Object> attributes = null;
        if (cookieTarget != null) {
            attributes = SessionInCookieFilter.sessionCryptor.decrypt(cookieTarget.getValue());
        }
        return attributes;
    }

    public static void update(HttpSession session, HttpServletRequest req, HttpServletResponse response) {
        if ((session != null) && session.getAttributeNames().hasMoreElements()) {       	
        	session.setAttribute(SessionInCookieFilter.SESSION_LAST_ACCESSED, CommonUtils.getNow());

        	String sSessionData = SessionInCookieFilter.sessionCryptor.encrypt(session); // + COOKIE_TAIL_HTTP_ONLY;
            Cookie cookie = new Cookie(SessionInCookieFilter.COOKIE_NAME, sSessionData);
            
            // setSecure doesn't work locally.
            if (req.getRequestURL().toString().toUpperCase().indexOf("LOCALHOST") < 0)
            	cookie.setSecure(true);

            cookie.setPath("/");
        	cookie.setHttpOnly(true);
        	
            // Below doesn't work. Don't use with addCookie.
            // response.addHeader("SET-COOKIE", "session=" + cookie.getValue() + "; Path=/; Secure; HttpOnly");
            response.addCookie(cookie);
        }
    }
    
    public static void clearCookie(HttpServletResponse response) {
        Cookie cookie = new Cookie(SessionInCookieFilter.COOKIE_NAME, null);
        cookie.setPath("/");
        response.addCookie(cookie);
    }

    /*
    public static void update() {
        if ((SessionInCookieFilter.requestLocalStorage.get() != null) &&
        	(SessionInCookieFilter.responseLocalStorage.get() == null)) {
        	return;
        }
        HttpSession session = SessionInCookieFilter.requestLocalStorage.get().getSession(false);
        if ((session != null) && session.getAttributeNames().hasMoreElements()) {
            Cookie cookie = new Cookie(SessionInCookieFilter.COOKIE_NAME, SessionInCookieFilter.sessionCryptor.encrypt(session));
            cookie.setPath("/");
            SessionInCookieFilter.responseLocalStorage.get().addCookie(cookie);
        }
    }
    */
}