package gov.dod.cls.filter;

import java.math.BigInteger;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import javax.crypto.Cipher;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import javax.servlet.http.HttpSession;
import javax.xml.bind.DatatypeConverter;

import org.apache.log4j.Logger;
import org.springframework.util.SerializationUtils;

public class SessionCryptor {
	
    private static String KEY = ""; // "1234567890123456";
	private static Logger log = Logger.getLogger(SessionCryptor.class);
	
	public static void setKey(String newKey) {
		SessionCryptor.KEY = newKey;
	}
	
	public String encrypt(HttpSession session) {
        try {
            Cipher aes = this.createChiper(Cipher.ENCRYPT_MODE);
            byte[] bytes = SerializationUtils.serialize(this.readAttributeMap(session));
            String encryptedSession = DatatypeConverter.printHexBinary(aes.doFinal(bytes));
            String signature = this.calculateSignature(bytes).toUpperCase();
            return encryptedSession + signature;
        } catch (Exception e) {
            log.error("Can't encrypt session", e);
        }
        return null;
    }
	
	@SuppressWarnings("unchecked")
	public Map<String, Object> decrypt(String session) {
        try {
        	if ((session != null) && (!session.isEmpty())) {
	        	if (session.length() >= 40) {
		            String signature = session.substring(session.length() - 40);
		            String encryptedSession = session.substring(0,session.length() - 40);
		
		            Cipher aes = this.createChiper(Cipher.DECRYPT_MODE);
		            byte[] bytes = aes.doFinal(DatatypeConverter.parseHexBinary(encryptedSession));
		
		            if (!signature.equals(this.calculateSignature(bytes).toUpperCase())) {
		                log.error("Session has been tampered with");
		                return null;
		            }
		
		            //noinspection unchecked
		            return (Map<String, Object>) SerializationUtils.deserialize(bytes);
	        	} else {
	        		log.info("SessionCryptor.decrypt('" + session + "'): session string is too short");
	        	}
        	}
        } catch (Exception e) {
            log.error("Can't decrypt session", e);
        }

        return null;
    }

    private Cipher createChiper(int mode) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, InvalidAlgorithmParameterException {
        Cipher aes = Cipher.getInstance("AES/CBC/PKCS5Padding");
        aes.init(mode, new SecretKeySpec(SessionCryptor.KEY.getBytes(), "AES"), new IvParameterSpec(new byte[16]));
        return aes;
    }

    private Map<String, Object> readAttributeMap(HttpSession session) {
        HashMap<String, Object> result = new HashMap<String, Object>();
        @SuppressWarnings("unchecked")
		Enumeration<String> attributeNames = session.getAttributeNames();
        while (attributeNames.hasMoreElements()) {
            String el = attributeNames.nextElement();
            result.put(el, session.getAttribute(el));
        }
        return result;
    }

    private String calculateSignature(byte[] serialisedSession) {
        try {
            MessageDigest cript = MessageDigest.getInstance("SHA-1");
            cript.reset();
            cript.update(serialisedSession);
            return String.format("%1$40s", new BigInteger(1, cript.digest()).toString(16));
        } catch (Exception e) {
            log.error("Cant calculate signature", e);
        }
        return null;
    }
}
