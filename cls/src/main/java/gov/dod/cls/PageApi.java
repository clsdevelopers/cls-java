package gov.dod.cls;

import gov.dod.cls.bean.ClsDocument;
import gov.dod.cls.bean.Dashboard;
import gov.dod.cls.bean.Oauth;
import gov.dod.cls.bean.session.UserSession;
import gov.dod.cls.bean.utils.DocGenerateDoc;
import gov.dod.cls.config.ClsConfig;
import gov.dod.cls.db.AuditEventTable;
import gov.dod.cls.db.ClauseSectionTable;
import gov.dod.cls.db.ClauseTable;
import gov.dod.cls.db.ClauseVersionTable;
import gov.dod.cls.db.ClauseVersionChangeTable;
import gov.dod.cls.db.DeptTable;
import gov.dod.cls.db.DocumentTable;
import gov.dod.cls.db.OauthApplicationTable;
import gov.dod.cls.db.OrgTable;
import gov.dod.cls.db.PrescriptionTable;
import gov.dod.cls.db.QuestionTable;
import gov.dod.cls.db.RegulationTable;
import gov.dod.cls.db.RoleTable;
import gov.dod.cls.db.UserTable;
import gov.dod.cls.db.reports.DocGenerateMetrics;
import gov.dod.cls.filter.SessionInCookieFilter;
import gov.dod.cls.reference.ClsPages;
import gov.dod.cls.bean.FinalClauses;
import gov.dod.cls.model.JsonAble;

import java.io.IOException;
import java.util.Date;
import java.util.Locale;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.jasypt.commons.CommonUtils;

@WebServlet("/page")
public class PageApi extends HttpServlet {

	private static final long serialVersionUID = 1L;

	public final static String PARAM_DO = "do";

	// ============================================================================================
	@Override
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
	        throws ServletException, IOException {
		this.doProcess(false, req, resp);
	}

	// ----------------------------------------------------------------------------
	@Override
	public void doPost(HttpServletRequest req, HttpServletResponse resp)
	        throws ServletException, IOException {
		this.doProcess(true, req, resp);
	}

	public void doProcess(boolean isPost, HttpServletRequest req, HttpServletResponse resp)
	        throws ServletException, IOException 
	{
		String sDo = req.getParameter(PARAM_DO);
		if (CommonUtils.isEmpty(sDo))
			return;
		
		HttpSession session = req.getSession(true);
		ClsConfig.getInstance().setSessionTimeout(session, CommonUtils.isNotEmpty(req.getParameter(ClsPages.PARAM_TOKEN)));
		UserSession oUserSession = PageApi.getUserSession(session);

		try {
			// Check if session related actions
			if (sDo.startsWith(UserSession.PREFIX_DO_SESSION)) {
				oUserSession.processRequest(sDo, req, resp, session);
				return;
			}
			
			if (sDo.equals(OrgTable.DO_ORG_REFERENCES)) {
				OrgTable.processRequest(sDo, oUserSession, req, resp);
				return;
			}
			
			// Check if Oauth related actions
			if (sDo.startsWith(Oauth.PREFIX_DO_OAUTH)) {
				Oauth.processRequest(sDo, oUserSession, req, resp);
				return;
			}
			
			if ("login".equals(sDo)) {
				oUserSession.submitLogin(req, resp, session);
				return;
			}
			
			if ("user-detail-resetpassword".equals(sDo)) {
				UserTable.processRequest(sDo, null, req, resp);
				return;
			}
			
			if ("user-forgot-password".equals(sDo)) {
				UserTable.processRequest(sDo, null, req, resp);
				return;
			}
				
			if ("index".equals(sDo)) {
				oUserSession.initialize(req, resp);
				return;
			}
			// Regular user actions block -----------------------------------------------------------
			// Check if user logged in
			if (oUserSession.isVerified() == false) {
				oUserSession.setMessage("Invalid access");
				// PageApi.sendRedirect(req, resp, "");
				String response = JsonAble.jsonResult(JsonAble.STATUS_FAIL, null, "no session");
				PageApi.send(resp, JsonAble.JSON_RESPONSE_FORMAT, response);
				return;
			}
	
			// Check if request is for document process
			if (sDo.startsWith(ClsDocument.PREFIX_DO_DOCUMENT)) {
				ClsDocument.processRequest(sDo, oUserSession, req, resp);
				return;
			}
	
			// Check if request is for user administration
			if (sDo.startsWith(Dashboard.PREFIX_DO_DASHBOARD)) {
				Dashboard.processRequest(sDo, oUserSession, req, resp);
				return;
			}
			
			// Check if request is for Final Clause page administration
			if (sDo.startsWith(FinalClauses.PREFIX_DO_FINAL_CLAUSE)) {
				FinalClauses.processRequest(sDo, oUserSession, req, resp);
				return;
			}
			
			// Check if request is for Clause Version Changes administration
			if (sDo.startsWith(ClauseVersionChangeTable.PREFIX_DO_CLAUSE_VERSION_CHANGE)) {
				ClauseVersionChangeTable.processRequest(sDo, oUserSession, req, resp);
				return;
			}			
			
			// CJ-1301, Add 'Print Metadata' link to API users interface
			// Check if request is for Clause administration
			if (("clause-current-list".equals(sDo)) 
			|| ("clause-print".equals(sDo)) 
			|| ("clause-detail-print".equals(sDo))){
				ClauseTable.processRequest(sDo, oUserSession, req, resp);
				return;
			}
			// CJ-1301, Add 'Print Metadata' link to API users interface
			// Check if request is for Clause Version administration
			if (sDo.startsWith(ClauseVersionTable.PREFIX_DO_CLAUSE_VERSION)) {
				ClauseVersionTable.processRequest(sDo, oUserSession, req, resp);
				return;
			}
			// Administrative actions block -------------------
			// Check if user is super user
			if ((oUserSession.isSuperUser() || oUserSession.isSubsuperUser()) == false) {
				oUserSession.setMessage("Unauthorized access");
				PageApi.sendRedirect(req, resp, "");
				return;
			}
			
			// Check if request is for user administration
			if (sDo.startsWith(UserTable.PREFIX_DO_USER)) {
				UserTable.processRequest(sDo, oUserSession, req, resp);
				return;
			}
			// Check if request is for Role administration
			if (sDo.startsWith(RoleTable.PREFIX_DO_ROLE)) {
				RoleTable.processRequest(sDo, oUserSession, req, resp);
				return;
			}
			// Check if request is for Org administration
			if (sDo.startsWith(OrgTable.PREFIX_DO_ORG)) {
				OrgTable.processRequest(sDo, oUserSession, req, resp);
				return;
			}
			// Check if request is for Department administration
			if (sDo.startsWith(DeptTable.PREFIX_DO_DEPT)) {    // 04/01/2015 IC
				DeptTable.processRequest(sDo, oUserSession, req, resp);
				return;
			}		
			// Check if request is for Org administration
			if (sDo.startsWith(OauthApplicationTable.PREFIX_DO_OAUTH_APPLICATION)) {
				OauthApplicationTable.processRequest(sDo, oUserSession, req, resp);
				return;
			}
			// Check if request is for Document administration
			if (sDo.startsWith(DocumentTable.PREFIX_DO_DOCUMENT)) {
				DocumentTable.processRequest(sDo, oUserSession, req, resp);
				return;
			}
			// Check if request is for AuditEvent administration
			if (sDo.startsWith(AuditEventTable.PREFIX_DO_AUDIT_EVENT)) {
				AuditEventTable.processRequest(sDo, oUserSession, req, resp);
				return;
			}
			// Check if request is for AuditEvent administration
			if (sDo.startsWith(QuestionTable.PREFIX_DO_QUESTION)) {
				QuestionTable.processRequest(sDo, oUserSession, req, resp);
				return;
			}
			// Check if request is for Clause administration
			if (sDo.startsWith(ClauseTable.PREFIX_DO_CLAUSE)) {
				ClauseTable.processRequest(sDo, oUserSession, req, resp);
				return;
			}
			// Check if request is for Clause Section administration
			if (sDo.startsWith(ClauseSectionTable.PREFIX_DO_CLAUSE_SECTION)) {
				ClauseSectionTable.processRequest(sDo, oUserSession, req, resp);
				return;
			}
			// Check if request is for Prescription administration
			if (sDo.startsWith(PrescriptionTable.PREFIX_DO_PRESCRIPTION)) {
				PrescriptionTable.processRequest(sDo, oUserSession, req, resp);
				return;
			}
			// Check if request is for Regulation administration
			if (sDo.startsWith(RegulationTable.PREFIX_DO_REGULATION)) {
				RegulationTable.processRequest(sDo, oUserSession, req, resp);
				return;
			}
			// Generate database metrics report
			if ("reports-metrics".equals(sDo)) {
				DocGenerateMetrics.downloadDoc(req, resp);
				return;
			}
			
		}
		finally {
			//SessionInCookieFilter.update(session, (HttpServletResponse) resp);
			//session.setAttribute(UserSession.class.getName(), null);
		}
	}
	
	// =====================================================================================
	public static boolean send(HttpServletResponse response, String pType, String pResult) {
		return PageApi.send(response, pType, null, pResult);
	}
	
	public static boolean send(HttpServletResponse response, String pType, String pMimeType, XWPFDocument doc) {
		ServletOutputStream oOutputStream = null;
		try {
			oOutputStream = response.getOutputStream();
			if (pMimeType != null) {
				response.setContentType(pMimeType); // "application/octet-stream"; // set to binary type if MIME mapping not found
	        }
			if (pType != null)
				response.setHeader("Content-Disposition", pType);
			if (doc != null) {
				doc.write(oOutputStream);
			}
		} catch (Exception oError) {
			oError.printStackTrace();
		} finally {
			try {
				if (oOutputStream != null)
					oOutputStream.close();
			} catch (Exception oIgnore) {
				oIgnore.printStackTrace();
			}
		}
		return true;
	}
	
	public static boolean send(HttpServletResponse response, String pType, String pMimeType, PDDocument doc) {
		ServletOutputStream oOutputStream = null;
		try {
			oOutputStream = response.getOutputStream();
			if (pMimeType != null) {
				response.setContentType(pMimeType); // "application/octet-stream"; // set to binary type if MIME mapping not found
	        }
			if (pType != null)
				response.setHeader("Content-Disposition", pType);
			if (doc != null) {
				doc.save(oOutputStream);
			}
		} catch (Exception oError) {
			oError.printStackTrace();
		} finally {
			try {
				if (oOutputStream != null)
					oOutputStream.close();
			} catch (Exception oIgnore) {
				oIgnore.printStackTrace();
			}
		}
		return true;
	}
	
	public static boolean send(HttpServletResponse response, String pType, String pMimeType, HSSFWorkbook doc) {
		ServletOutputStream oOutputStream = null;
		try {
			oOutputStream = response.getOutputStream();
			if (pMimeType != null) {
				response.setContentType(pMimeType); // "application/octet-stream"; // set to binary type if MIME mapping not found
	        }
			if (pType != null)
				response.setHeader("Content-Disposition", pType);
			if (doc != null) {
				doc.write(oOutputStream);
			}
		} catch (Exception oError) {
			oError.printStackTrace();
		} finally {
			try {
				if (oOutputStream != null)
					oOutputStream.close();
			} catch (Exception oIgnore) {
				oIgnore.printStackTrace();
			}
		}
		return true;
	}
	public static boolean send(HttpServletResponse response, String pType, String pMimeType, String pResult) {
		ServletOutputStream oOutputStream = null;
		try {
			oOutputStream = response.getOutputStream();
			if (pMimeType != null) {
				response.setContentType(pMimeType); // "application/octet-stream"; // set to binary type if MIME mapping not found
	        }
			if (pType != null)
				response.setHeader("Content-Disposition", pType);
			if (pResult != null) {
				//response.setContentLength(pResult.length());
				oOutputStream.write(pResult.getBytes());
			}
		} catch (Exception oError) {
			oError.printStackTrace();
		} finally {
			try {
				if (oOutputStream != null)
					oOutputStream.close();
			} catch (Exception oIgnore) {
				oIgnore.printStackTrace();
			}
		}
		return true;
	}
	
	public static boolean hasUserSession(HttpServletRequest request) {
		HttpSession session = request.getSession(false);
		if (session == null)
			return false;
		ClsConfig clsConfig = ClsConfig.getInstance();
		clsConfig.setSessionTimeout(session, CommonUtils.isNotEmpty(request.getParameter(ClsPages.PARAM_TOKEN)));
		if (clsConfig.isSessionExpired(session.getLastAccessedTime())) {
			session.setAttribute(UserSession.class.getName(), null);
			return false;
		}
		UserSession oUserSession = (UserSession)session.getAttribute(UserSession.class.getName());
		return (oUserSession != null);
	}

	public static UserSession getUserSession(HttpSession session) {
		UserSession oUserSession = null;
		ClsConfig clsConfig = ClsConfig.getInstance();
		if (clsConfig.isSessionExpired(session.getLastAccessedTime())) {
			System.out.println("getUserSession: session has expired");
			session.setAttribute(UserSession.class.getName(), null);
		} else {
			oUserSession = (UserSession)session.getAttribute(UserSession.class.getName());
		}
		if (oUserSession == null) {
			oUserSession = new UserSession(new Date(session.getCreationTime()));
			session.setAttribute(UserSession.class.getName(), oUserSession);
		}
		oUserSession.setSession(session); // CJ-573
		return oUserSession;
	}
	
	public static Object getSessionObject(HttpSession session, String psAttribute) {
		return PageApi.setSessionObject(session, psAttribute, null);
	}
	
	public static Object setSessionObject(HttpSession session, String psAttribute, Object pObject) {
		try {
			Object oResult = session.getAttribute(psAttribute);
			if (oResult != null) {
				session.removeAttribute(psAttribute);
			}
			if (pObject != null) {
				session.setAttribute(psAttribute, pObject);
			}
			return oResult;
		} catch (Exception oIgnore) {
			return null;
		}
	}
	
	public static void sendRedirect(HttpServletRequest req, HttpServletResponse resp, String page) {
		try {
			String sUrl = req.getContextPath() + "/" + page;
			resp.sendRedirect(sUrl);
		}
		catch (Exception oIgnore) {
			oIgnore.printStackTrace();
		}
	}
	
	public static void sendRedirect(HttpSession session, HttpServletRequest req, HttpServletResponse resp, String page) {
		if (session == null)
			session = req.getSession(false);
		SessionInCookieFilter.update(session, req, resp);
		PageApi.sendRedirect(req, resp, page);
	}
	
	public static String getRemoteIp(HttpServletRequest req) {
		Locale.setDefault(Locale.ENGLISH);
		String ipAddress = req.getHeader("X-FORWARDED-FOR");  
		if (ipAddress == null) {  
			ipAddress = req.getRemoteAddr();  
		}
		return ipAddress;
	}
	
}
