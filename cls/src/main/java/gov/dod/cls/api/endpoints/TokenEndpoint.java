package gov.dod.cls.api.endpoints;

import java.sql.Connection;

import gov.dod.cls.api.model.ApiAccessToken;
import gov.dod.cls.api.utils.ApiErrorCodes;
import gov.dod.cls.api.utils.ApiResponseError;
import gov.dod.cls.api.utils.ApiResponseUtils;
import gov.dod.cls.config.ClsConfig;
import gov.dod.cls.utils.sql.SqlUtil;

import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.apache.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.node.ObjectNode;

@Path("/{version}/token")
public class TokenEndpoint {

	private static Logger log = Logger.getLogger(TokenEndpoint.class);

	@Context HttpHeaders httpHeaders;
	@Context UriInfo uriInfo;

	@POST	// http://localhost:8080/cls/api/v2/token
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response createToken(@PathParam("version") String version,
			String jsonRequest) {
		ApiResponseError apiResponseError = new ApiResponseError(ApiErrorCodes.TOKEN_CREATE);
		String sPath = this.uriInfo.getRequestUri().toString();

		Connection conn = null;
		try {
			conn = ClsConfig.getDbConnection();
			
			ApiAccessToken apiAccessToken = new ApiAccessToken();
			ObjectNode objectNode = apiAccessToken.parse(jsonRequest);
			if (objectNode != null)
				return Response.status(HttpServletResponse.SC_BAD_REQUEST).entity(objectNode.toString()).build();
			
			objectNode = apiAccessToken.validateRequestEntries();
			if (objectNode != null)
				return Response.status(HttpServletResponse.SC_BAD_REQUEST).entity(objectNode.toString()).build();

			if (!apiAccessToken.isAuthorized(conn))
				return apiResponseError.sendNotAuthorized(sPath);
			
			objectNode = apiAccessToken.createToken(conn);
			if (objectNode == null) {
				ObjectMapper mapper = new ObjectMapper();
				objectNode = mapper.createObjectNode();
				objectNode.put("status", "Unable to create a token");
				return Response.status(HttpServletResponse.SC_INTERNAL_SERVER_ERROR).entity(objectNode.toString()).build();
			}
			
			return ApiResponseUtils.build(objectNode.toString());
		}
		catch (Exception oError) {
			apiResponseError.recordUnrecoverableError(oError, log, sPath);
		}
		finally {
			SqlUtil.closeInstance(conn);
		}
		return apiResponseError.sendError();
	}
}
