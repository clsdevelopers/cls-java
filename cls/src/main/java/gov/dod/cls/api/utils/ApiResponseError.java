package gov.dod.cls.api.utils;

import java.io.IOException;
import java.util.Date;

import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.node.ObjectNode;

public class ApiResponseError {

	public final static String HEADER_LINK = "Link";
	
	//private final static String ATTRIB_DEVELOPER_MESSAGE = "developer_message";
	private final static String ATTRIB_USER_MESSAGE = "message"; // user_message
	private final static String ATTRIB_ERROR_CODE = "error_code";
	//private final static String ATTRIB_MORE_INFO = "more_info";
	
	private int responseStatus = HttpServletResponse.SC_ACCEPTED;

	private String developer_message = null; // Provides developer information about error; Response Body; Required
	private String user_message = null; // Informational message for end-users; Response Body; Not Required
	private String error_code = null; // Application specific error code; Response Body; Required
	private String more_info = "about-api-errors.html#{" + ATTRIB_ERROR_CODE + "}"; // "No additional information available"; // Links where developers can find additional information about the error; Response Body; Required
	
	private String module_id;
	
	private boolean fromJson = false;
	
	/**
	 * ApiResponseError constructor
	 */
	public ApiResponseError() {
		this.module_id = null;
	}
	
	/**
	 * ApiResponseError constructor
	 * @param pModuleId - a string representing a API function defined in ApiErrorCodes class
	 */
	public ApiResponseError(String pModuleId) {
		this.module_id = pModuleId;
	}
	
	/**
	 * Record unrecoverable error
	 * @param oError - a Exception instance
	 * @param pErrorCategory - a string for error category
	 * @param pErrorCode - a string for error code defined in ApiErrorCodes class
	 */
	public void recordUnrecoverableError(
			Exception oError, 
			String pErrorCategory, 
			String pErrorCode, 
			String pMoreInfo,
			String pPath)
	{
		this.responseStatus = HttpServletResponse.SC_INTERNAL_SERVER_ERROR;
		this.developer_message = "Internal un-recoverable error occurred at "
				+ (new Date()).toString() + ".\nPlease contact us with followings:"
				+ "\nPath: " + pPath
				+ "\nError:\n"
				+ oError.getMessage();
		this.error_code = pErrorCode;
		this.user_message = (pErrorCategory == null ? "An unknown" : pErrorCategory) + " error occurred. Please contact a system administrator";
		if (pMoreInfo != null)
			more_info = pMoreInfo;
	}

	/**
	 * Record unrecoverable error
	 * @param oError - a Exception instance
	 * @param log - a org.apache.log4j.Logger instance
	 * @param sPath - a string for an API path
	 */
	public void recordUnrecoverableError(
			Exception oError,
			org.apache.log4j.Logger log,
			String sPath)
	{
		this.recordUnrecoverableError(oError, log, this.module_id, sPath);
	}

	/**
	 * Record unrecoverable error
	 * @param oError - a Exception instance
	 * @param log - a org.apache.log4j.Logger instance
	 * @param apiId - a string for a module id defined in ApiErrorCodes class
	 * @param sPath - a string for an API path
	 */
	public void recordUnrecoverableError(
			Exception oError,
			org.apache.log4j.Logger log,
			String apiId,
			String sPath)
	{
		if (oError instanceof NullPointerException)
		{
			log.error("NPE Error", oError);
			this.recordUnrecoverableError(oError, "A NPE", apiId + ApiErrorCodes.EXCEPTION_NULLPOINTER, null, sPath);
		}
		else if (oError instanceof JsonGenerationException)
		{
			log.error("Error mapping JSON", oError);
			this.recordUnrecoverableError(oError, "A JSON generation", apiId + ApiErrorCodes.EXCEPTION_JSONGENERATION, null, sPath);
		}
		else if (oError instanceof JsonMappingException)
		{
			log.error("Error mapping JSON", oError);
			this.recordUnrecoverableError(oError, "A JSON mapping", apiId + ApiErrorCodes.EXCEPTION_JSONMAPPING, null, sPath);
		}
		/*
		else if (oError instanceof com.mongodb.MongoServerSelectionException)
		{
			log.error("Error Mongo", oError);
			this.recordUnrecoverableError(oError, "A MongoDB", apiId + ApiErrorCodes.EXCEPTION_MONGODB_SELECTION, null, sPath);
		}
		else if (oError instanceof com.mongodb.CommandFailureException)
		{
			//{ "serverUsed" : "localhost:27017" , "ok" : 0.0 , "errmsg" : "auth failed" , "code" : 18}
			log.error("Error Mongo", oError);
			this.recordUnrecoverableError(oError, "A MongoDB Database ", apiId + ApiErrorCodes.EXCEPTION_MONGODB_DB, null, sPath);
		}
		else if (oError instanceof com.mongodb.MongoException)
		{
			log.error("Error Mongo", oError);
			this.recordUnrecoverableError(oError, "A MongoDB", apiId + ApiErrorCodes.EXCEPTION_MONGODB, null, sPath);
		}
		*/
		else if (oError instanceof java.lang.IllegalArgumentException) {
			log.error("Illegal Argument Error", oError);
			String sError = oError.getMessage();
			if ((sError != null) && sError.startsWith("invalid ObjectId")) {
				this.recordUnrecoverableError(oError, "Invalid Jbook Id", apiId + ApiErrorCodes.INVALID_JBOOK_ID, null, sPath);
			} else {
				this.recordUnrecoverableError(oError, "An Illegal Argument", apiId + ApiErrorCodes.EXCEPTION_IO, null, sPath);
			}
		}
		else if (oError instanceof IOException)
		{
			log.error("IO Error", oError);
			this.recordUnrecoverableError(oError, "An IO", apiId + ApiErrorCodes.EXCEPTION_IO, null, sPath);
		}
		else
		{
			log.error(oError);
			this.recordUnrecoverableError(oError, null, apiId + ApiErrorCodes.EXCEPTION_GENERAL, null, sPath);
		}
		oError.printStackTrace();
	}

	public int getResponseStatus() {
		return this.responseStatus;
	}
	public void setResponseStatus(int status) {
		this.responseStatus = status;
	}
	
	public String getDeveloper_message() {
		return this.developer_message;
	}
	public void setDeveloper_message(String developer_message) {
		this.developer_message = developer_message;
	}
	
	public String getUser_message() {
		return this.user_message;
	}
	
	public void setUser_message(String user_message) {
		this.user_message = user_message;
	}
	
	public String getError_code() {
		return this.error_code;
	}
	public void setError_code(String error_code) {
		this.error_code = error_code;
	}
	public void applyError(String code) {
		this.error_code = ((this.module_id == null) ? "" : this.module_id) + code;
	}
	
	public String getMore_info() {
		return this.more_info;
	}
	public void setMore_info(String more_info) {
		this.more_info = more_info;
	}
	
	public String getModule_Id() {
		return this.module_id;
	}
	public void setModule_Id(String module_id) {
		if ((this.error_code != null) && (!this.error_code.startsWith(this.module_id))) {
			String sCode = this.error_code.substring(this.module_id.length()); 
			this.error_code = module_id + sCode; 
			this.more_info = "about-api-errors.html#{" + ATTRIB_ERROR_CODE + "}";
		}
		this.module_id = module_id;
	}
	
	public boolean isFromJson() {
		return this.fromJson;
	}
	
	/**
	 * Load predefined not found message and returns a Response instance
	 * @param pUserMessage - a string for a user message
	 * @param pDeveloperMessage - a string for developer message
	 * @return - a Response instance
	 */
	public Response sendNotFound(String pUserMessage, String pDeveloperMessage) {
		return sendNotFound(pUserMessage, pDeveloperMessage, ApiErrorCodes.NOT_FOUND);
		/*
		this.status = HttpServletResponse.SC_NOT_FOUND;
		this.applyError(ApiErrorCodes.NOT_FOUND);
		this.setUser_message(pUserMessage);
		this.setDeveloper_message(pDeveloperMessage);
		return this.sendError();
		*/
	}
	
	public Response sendNotFound(String pDeveloperMessage) {
		return sendNotFound("The requested resource could not be found", pDeveloperMessage, ApiErrorCodes.NOT_FOUND);
		/*
		this.status = HttpServletResponse.SC_NOT_FOUND;
		this.applyError(ApiErrorCodes.NOT_FOUND);
		this.setUser_message(pUserMessage);
		this.setDeveloper_message(pDeveloperMessage);
		return this.sendError();
		*/
	}
	
	public Response sendNotFound(String pUserMessage, String pDeveloperMessage, String psErrorCode) {
		this.responseStatus = HttpServletResponse.SC_NOT_FOUND;
		this.applyError(psErrorCode);
		this.setUser_message(pUserMessage);
		this.setDeveloper_message(pDeveloperMessage);
		return this.sendError();
	}
	
	public Response sendBadRequest(String pUserMessage, String pDeveloperMessage, String psErrorCode) {
		this.responseStatus = HttpServletResponse.SC_BAD_REQUEST;
		this.applyError(psErrorCode);
		this.setUser_message(pUserMessage);
		this.setDeveloper_message(pDeveloperMessage);
		return this.sendError();
	}
	
	public Response sendNotAuthorized(String pDeveloperMessage) {
		this.responseStatus = HttpServletResponse.SC_UNAUTHORIZED;
		this.applyError(ApiErrorCodes.NOT_AUTHORIZED);
		this.setUser_message("Not Authorized");
		this.setDeveloper_message(pDeveloperMessage);
		return this.sendError();
	}
	
	/**
	 * Creates and returns a JSON string for this error
	 * @return - a Response instance
	 */
	public Response sendError() {
		ObjectMapper mapper = new ObjectMapper();
		ObjectNode json = mapper.createObjectNode();
		json.put("response_type",  "ERROR");
		json.put("response_code",  this.responseStatus);
		json.put(ATTRIB_USER_MESSAGE, this.user_message);
		json.put(ATTRIB_ERROR_CODE, this.error_code);
		/*
		if (this.more_info.contains(ATTRIB_ERROR_CODE)) {
			String sMoreInfo = this.more_info.replace("{" + ATTRIB_ERROR_CODE + "}", this.error_code);
			json.put(ATTRIB_MORE_INFO, sMoreInfo);
		} else
			json.put(ATTRIB_MORE_INFO, this.more_info);
		*/
		return Response.status(this.responseStatus).entity(json.toString()).type(MediaType.APPLICATION_JSON).build();
	}
	
	/**
	 * Load predefined bad request (parameters) message and returns a Response instance
	 * @param pUserMessage - a string for a user message
	 * @param pDeveloperMessage - a string for developer message
	 * @return - a Response instance
	 */
	public Response sendBadRequest(String pUserMessage, String pDeveloperMessage) {
		this.responseStatus = HttpServletResponse.SC_BAD_REQUEST;
		this.applyError(ApiErrorCodes.INVALID_PARAMETER);
		this.setUser_message(pUserMessage);
		this.setDeveloper_message(pDeveloperMessage);
		return this.sendError();
	}
	
}
