package gov.dod.cls.api.model;

import gov.dod.cls.bean.Oauth;
import gov.dod.cls.config.ClsConfig;
import gov.dod.cls.db.DocumentRecord;
import gov.dod.cls.db.DocumentTable;
import gov.dod.cls.db.OauthInterviewRecord;
import gov.dod.cls.utils.CommonUtils;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Iterator;

import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.node.ObjectNode;

public class ApiInterview {

	private int applicationId;
	private Integer documentId;
	
	public ObjectNode parse(String jsonRequest) {
		String invalidRequests = "";
		try {
			if (CommonUtils.isNotEmpty(jsonRequest)) {
				ObjectMapper mapper = new ObjectMapper();
				JsonNode actualObj = mapper.readTree(jsonRequest);
				Iterator<String> fieldNames = actualObj.getFieldNames();
				while (fieldNames.hasNext()) {
					String fieldName = fieldNames.next();
					if ("document_id".equals(fieldName)) {
						this.documentId = CommonUtils.asInteger(actualObj, fieldName);
					} else {
						invalidRequests = ApiDocument.addInvalidEntry(invalidRequests, fieldName);
					}
				}
				/*
				this.documentId = CommonUtils.asInteger(actualObj, "document_id");
				if (this.documentId != null)
					return null;
				*/
				if (invalidRequests.isEmpty()) {
					return null;
				}
			}
		} catch (Exception oError) {
			oError.printStackTrace();
		}
		
		ObjectMapper mapper = new ObjectMapper();
		ObjectNode objectNode = mapper.createObjectNode();
		objectNode.put("satus", "Invalid Json request" + invalidRequests);
		return objectNode;
	}
	
	public ObjectNode validateForParemters() throws SQLException {
		ObjectMapper mapper = new ObjectMapper();
		ObjectNode result = mapper.createObjectNode();
		
		if (this.documentId == null) {
			result.put("document_id", "required");
		}
		
		return ((result.size() == 0) ? null : result);	
	}
	
	public boolean isValidDocument(Connection conn) throws SQLException {
		if (this.documentId == null) {
			return false;
		}
		DocumentRecord oRecord = DocumentTable.get(this.documentId, conn);
		if (oRecord == null) {
			return false;
		}
		Integer oApplicationId = oRecord.getApplicationId();
		if (oApplicationId == null)
			return false;
		return (oApplicationId.intValue() == this.applicationId); 
	}
	
	public ObjectNode genJson(OauthInterviewRecord oRecord, String name) {
		ObjectMapper mapper = new ObjectMapper();
		ObjectNode resultNode = mapper.createObjectNode();
		
		ObjectNode recordNode = resultNode.objectNode();
		oRecord.fillJson(recordNode, true);

		resultNode.put(name, recordNode);
		return resultNode;
	}
	
	public ObjectNode hasInterview(Connection conn, int oauthAccessId) throws SQLException {
		if (this.documentId == null)
			return null;

		OauthInterviewRecord oRecord = OauthInterviewRecord.getRecord(conn, this.applicationId, this.documentId); // oauthAccessId, 
		if (oRecord == null)
			return null;

		if (oRecord.isExpired(conn, true))
			return null;
		
		ObjectMapper mapper = new ObjectMapper();
		ObjectNode resultNode = mapper.createObjectNode();
		resultNode.put("document_id", "Interview already exists");
		return resultNode;	
	}
	
	public ObjectNode createInterview(Connection conn, int oauthAccessId) throws Exception {
		if (this.documentId == null)
			return null;

		String interviewToken = Oauth.genCode();

		OauthInterviewRecord oRecord = OauthInterviewRecord.getRecord(conn, this.applicationId, this.documentId); // oauthAccessId, 
		if (oRecord != null) {
			if (!oRecord.isExpired(conn, true))
				return null;
			oRecord.assignToken(conn, interviewToken, oauthAccessId);
		} else {
			oRecord = OauthInterviewRecord.insert(conn, this.applicationId, oauthAccessId, this.documentId, interviewToken);
		}
		return this.genJson(oRecord, "interview_launcher");
	}
	
	public void assignToken(OauthInterviewRecord oRecord, Connection conn, int oauthAccessId) throws Exception {
		if (oRecord == null)
			throw new Exception("No OauthInterviewRecord instance passed");

		String interviewToken = Oauth.genCode();
		oRecord.assignToken(conn, interviewToken, oauthAccessId);
	}
	
	public OauthInterviewRecord getInterview(int interviewId, Connection conn) throws SQLException {
		OauthInterviewRecord oRecord = OauthInterviewRecord.getRecord(conn, interviewId);
		if (oRecord == null)
			return null;

		//if (oRecord.isExpired(null, true))
		//	return null;

		if (oRecord.getApplicationId().intValue() != this.applicationId)
			return null;

		return oRecord;
	}
	
	public ObjectNode deleteRecord(OauthInterviewRecord oRecord, Connection conn) throws Exception {
		oRecord.deleteRecord(conn);
		ObjectMapper mapper = new ObjectMapper();
		ObjectNode result = mapper.createObjectNode();
		result.put("status", "record deleted");
		return result;
	}
	
	public ObjectNode getLaunchUrl(OauthInterviewRecord oRecord, String docType) {
		ObjectMapper mapper = new ObjectMapper();
		ObjectNode result = mapper.createObjectNode();
		
		ClsConfig clsConfig = ClsConfig.getInstance();
		if (docType.equals("O"))
			result.put("launch_url", clsConfig.getLaunchOrdersUrlPrefix() + oRecord.getInterviewToken());
		else
			result.put("launch_url", clsConfig.getLaunchInterviewUrlPrefix() + oRecord.getInterviewToken());
		return result;
	}
	
	public int getApplicationId() {
		return applicationId;
	}

	public void setApplicationId(int applicationId) {
		this.applicationId = applicationId;
	}

	public Integer getDocumentId() {
		return documentId;
	}

	public void setDocumentId(Integer documentId) {
		this.documentId = documentId;
	}
	
}
