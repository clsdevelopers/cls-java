package gov.dod.cls.api.endpoints;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.HashMap;

import gov.dod.cls.api.model.ApiDocument;
import gov.dod.cls.api.utils.ApiErrorCodes;
import gov.dod.cls.api.utils.ApiResponseError;
import gov.dod.cls.api.utils.ApiResponseUtils;
import gov.dod.cls.bean.ClsDocument;
import gov.dod.cls.bean.Oauth;
import gov.dod.cls.bean.utils.DocGenerateXml;
import gov.dod.cls.bean.utils.DocShowHtml;
import gov.dod.cls.config.ClsConfig;
import gov.dod.cls.db.ClauseTable;
import gov.dod.cls.db.DocumentRecord;
import gov.dod.cls.db.DocumentTable;
import gov.dod.cls.db.QuestionConditionTable;
import gov.dod.cls.utils.CommonUtils;
import gov.dod.cls.utils.Pagination;
import gov.dod.cls.utils.sql.SqlUtil;

import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.apache.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.node.ArrayNode;
import org.codehaus.jackson.node.ObjectNode;

@Path("/{version}/documents")
public class DocumentEndpoint {

	private static Logger log = Logger.getLogger(ClausesEndpoint.class);

	@Context HttpHeaders httpHeaders;
	@Context UriInfo uriInfo;
	
	@GET	// http://localhost:8080/cls/api/v2/documents
	@Produces(MediaType.APPLICATION_JSON)
	public Response getList(
			@PathParam("version") String version,
			@QueryParam("limit") Integer limit,
			@QueryParam("offset") Integer offset) 
	{
		ApiResponseError apiResponseError = new ApiResponseError(ApiErrorCodes.DOCUMENT_LIST);
		String sPath = this.uriInfo.getRequestUri().toString();

		Connection conn = null;
		try {
			conn = ClsConfig.getDbConnection();

			Oauth oauth = new Oauth(this.httpHeaders);
			if (oauth.isNotAuthorized(conn))
				return apiResponseError.sendNotAuthorized(sPath);

			Response oResponse = Oauth.checkParameters(version, null, apiResponseError);
			if (oResponse != null)
				return oResponse;

			boolean bVer1 =  Oauth.VERSION_1.equals(version);
			Pagination pagination = new Pagination(limit, offset, this.uriInfo);
			pagination.setDataTag("documents");
			DocumentTable.processApiList(oauth.getApplicatonId(), bVer1, pagination, conn);
			if (pagination.getCount() <= 0)
				return apiResponseError.sendNotFound("Path: " + sPath);
			
			if (bVer1) {
				ArrayNode arrayNode = pagination.getArrayNode();
				HashMap<Integer, ClauseTable> mapClauseTable = new HashMap<Integer, ClauseTable>(); 
				for (int index = 0; index < arrayNode.size(); index++) {
					ObjectNode docNode = (ObjectNode)arrayNode.get(index);
					int docId = CommonUtils.asInteger(docNode, "id");
					ClsDocument clsDocument = new ClsDocument();
					if (clsDocument.loadDocumentForApi(docId, conn, true, mapClauseTable)) {
						clsDocument.populateJsonForApiVer1(docNode, bVer1);
					} else {
						ArrayNode childrenNode = docNode.arrayNode();
						docNode.put("clauses", childrenNode);
						childrenNode = docNode.arrayNode();
						docNode.put("document_answers", childrenNode);
					}
					oauth.populateApplicationJsonForApi(bVer1, docNode);
				}
			}

			return ApiResponseUtils.build(pagination.toJson(bVer1).toString());
		}
		catch (Exception oError) {
			apiResponseError.recordUnrecoverableError(oError, log, sPath);
		}
		finally {
			SqlUtil.closeInstance(conn);
		}
		return apiResponseError.sendError();
	}
	
	@GET	// http://localhost:8080/cls/api/v2/documents/7543
	@Path("/{id}")
	public Response getItem(
			@PathParam("version") String version,
			@PathParam("id") Integer id,
			@QueryParam("request_format") String format)
	{
		ApiResponseError apiResponseError = new ApiResponseError(ApiErrorCodes.DOCUMENT_RECORD);
		String sPath = this.uriInfo.getRequestUri().toString();

		Connection conn = null;
		try {
			conn = ClsConfig.getDbConnection();

			Oauth oauth = new Oauth(this.httpHeaders);
			if (oauth.isNotAuthorized(conn))
				return apiResponseError.sendNotAuthorized(sPath);

			Object[] params = {id};
			Response oResponse = Oauth.checkParameters(version, params, apiResponseError);
			if (oResponse != null)
				return oResponse;
			
			if (!DocumentTable.hasApplicationDocument(oauth.getApplicatonId(), id.intValue(), conn))
				return apiResponseError.sendNotFound("Path: " + sPath);
			
			boolean bVer1 =  Oauth.VERSION_1.equals(version);
			ClsDocument clsDocument = new ClsDocument();
			if (!clsDocument.loadDocumentForApi(id.intValue(), conn, true, null))
				return apiResponseError.sendNotFound("Path: " + sPath);
			
			if (CommonUtils.isEmpty(format) || "JSON".equalsIgnoreCase(format)) {
				ObjectNode objectNode = clsDocument.getApiJson(bVer1, conn); //  ClsDocument.getRecordApiJson(id.intValue());
				if (objectNode == null) {
					return apiResponseError.sendNotFound("Path: " + sPath);
				}
				if (bVer1) {
					ObjectNode documentNode = (ObjectNode)objectNode.get("document");
					oauth.populateApplicationJsonForApi(bVer1, documentNode);
				}
				oResponse = Response.status(HttpServletResponse.SC_OK).entity(objectNode.toString()).type(MediaType.APPLICATION_JSON).build();
				return oResponse;
			}

			if ("XML".equalsIgnoreCase(format) || "TEXT".equalsIgnoreCase(format)) {
				DocGenerateXml docGenerateXml = new DocGenerateXml(clsDocument);
				String sOutput = docGenerateXml.generate("TEXT".equalsIgnoreCase(format));
				oResponse = Response.status(HttpServletResponse.SC_OK).entity(sOutput).type(MediaType.APPLICATION_XML).build();
				return oResponse;
			}

			if ("HTML".equalsIgnoreCase(format)) {
				DocShowHtml docShowHtml = new DocShowHtml(clsDocument);
				oResponse = Response.status(HttpServletResponse.SC_OK).entity(docShowHtml.genHtml(true)).type(MediaType.TEXT_HTML).build();
				return oResponse;
			}
			//PDF, DOCX, DOC,  
			
			return apiResponseError.sendBadRequest("Not supported request_format: " + format, null);
		}
		catch (Exception oError) {
			apiResponseError.recordUnrecoverableError(oError, log, sPath);
		}
		finally {
			SqlUtil.closeInstance(conn);
		}
		return apiResponseError.sendError();
	}
	
	@GET	// http://localhost:8080/cls/api/v1/documents/1094/answers
	@Path("/{id}/answers")
	public Response getQuestions(
			@PathParam("version") String version,
			@PathParam("id") Integer id)
	{
		ApiResponseError apiResponseError = new ApiResponseError(ApiErrorCodes.DOCUMENT_RECORD);
		String sPath = this.uriInfo.getRequestUri().toString();

		Connection conn = null;
		try {
			conn = ClsConfig.getDbConnection();

			Oauth oauth = new Oauth(this.httpHeaders);
			if (oauth.isNotAuthorized(conn))
				return apiResponseError.sendNotAuthorized(sPath);

			Object[] params = {id};
			Response oResponse = Oauth.checkParameters(version, params, apiResponseError);
			if (oResponse != null)
				return oResponse;
			
			if (!DocumentTable.hasApplicationDocument(oauth.getApplicatonId(), id.intValue(), conn))
				return apiResponseError.sendNotFound("Path: " + sPath);
			
			ClsDocument clsDocument = new ClsDocument();
			if (!clsDocument.loadDocument(id.intValue(), conn, false))
				return apiResponseError.sendNotFound("Path: " + sPath);
			
			boolean bVer1 =  Oauth.VERSION_1.equals(version);
			ObjectNode objectNode = clsDocument.getJsonForQuestionApi(bVer1, clsDocument.getQuestions(), conn);
			if (objectNode == null) {
				return apiResponseError.sendNotFound("Path: " + sPath);
			}
			oResponse = Response.status(HttpServletResponse.SC_OK).entity(objectNode.toString()).type(MediaType.APPLICATION_JSON).build();
			return oResponse;
		}
		catch (Exception oError) {
			apiResponseError.recordUnrecoverableError(oError, log, sPath);
		}
		finally {
			SqlUtil.closeInstance(conn);
		}
		return apiResponseError.sendError();
	}
	
	@POST	// http://localhost:8080/cls/api/v2/documents
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response createDocument(
			@PathParam("version") String version,
			String jsonRequest) {
		ApiResponseError apiResponseError = new ApiResponseError(ApiErrorCodes.DOCUMENT_CREATE);
		String sPath = this.uriInfo.getRequestUri().toString();

		Connection conn = null;
		try {
			conn = ClsConfig.getDbConnection();

			Oauth oauth = new Oauth(this.httpHeaders);
			if (oauth.isNotAuthorized(conn))
				return apiResponseError.sendNotAuthorized(sPath);

			Response oResponse = Oauth.checkParameters(version, null, apiResponseError);
			if (oResponse != null)
				return oResponse;
			
			ObjectNode objectNode;
			ApiDocument apiDocument = new ApiDocument(oauth.getApplicatonId());
			objectNode = apiDocument.parse(jsonRequest, false); //apiDocument.parse(formParams);
			if (objectNode != null)
				return Response.status(HttpServletResponse.SC_BAD_REQUEST).entity(objectNode.toString()).build();
			
			objectNode = apiDocument.validateForNewDocument(conn);
			if (objectNode != null)
				return Response.status(HttpServletResponse.SC_BAD_REQUEST).entity(objectNode.toString()).build();

			boolean bVer1 =  Oauth.VERSION_1.equals(version);
			objectNode = apiDocument.createDocument(conn, bVer1);
			if (objectNode == null)
				return Response.status(HttpServletResponse.SC_NOT_FOUND).entity("").build();

			if (bVer1) {
				ObjectNode documentNode = (ObjectNode)objectNode.get("document");
				oauth.populateApplicationJsonForApi(bVer1, documentNode);
			}
			return ApiResponseUtils.build(objectNode.toString());
		}
		catch (Exception oError) {
			apiResponseError.recordUnrecoverableError(oError, log, sPath);
		}
		finally {
			SqlUtil.closeInstance(conn);
		}
		return apiResponseError.sendError();
	}
	
	@POST	// http://localhost:8080/cls/api/v1/documents/1884/answers
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{id}/answers")
	public Response postAnswers (
			@PathParam("version") String version,
			@PathParam("id") Integer id,
			String jsonRequest) {
		ApiResponseError apiResponseError = new ApiResponseError(ApiErrorCodes.DOCUMENT_UPDATE);
		String sPath = this.uriInfo.getRequestUri().toString();

		Connection conn = null;
		try {
			conn = ClsConfig.getDbConnection();

			Oauth oauth = new Oauth(this.httpHeaders);
			if (oauth.isNotAuthorized(conn))
				return apiResponseError.sendNotAuthorized(sPath);

			Response oResponse = Oauth.checkParameters(version, null, apiResponseError);
			if (oResponse != null)
				return oResponse;
			
			if (!DocumentTable.hasApplicationDocument(oauth.getApplicatonId(), id.intValue(), conn))
				return apiResponseError.sendNotFound("Path: " + sPath);
			
			ClsDocument clsDocument = new ClsDocument();
			if (!clsDocument.loadDocument(id.intValue(), conn, false))
				return apiResponseError.sendNotFound("Path: " + sPath);
			
			ApiDocument apiDocument = new ApiDocument(oauth.getApplicatonId());
			ObjectNode objectNode = apiDocument.validateDocumentForAnswers(clsDocument, conn);
			if (objectNode != null)
				return Response.status(HttpServletResponse.SC_BAD_REQUEST).entity(objectNode.toString()).build();
			
			boolean bVer1 =  Oauth.VERSION_1.equals(version);
			QuestionConditionTable questionConditionTable = QuestionConditionTable.getQuestionSubset(clsDocument.getQuestions(), conn);
			ArrayList<String> invalidFields = new ArrayList<String>(); // CJ-803
			// CJ-641 System should accept questions still valid between versions for POST request (Appian)
			String status = apiDocument.parseQuestionAnswers(jsonRequest, clsDocument, questionConditionTable, invalidFields); //apiDocument.parse(formParams);
			if ((!apiDocument.hasPostedAnswers()) || (!invalidFields.isEmpty())) {
				ObjectMapper mapper = new ObjectMapper();
				objectNode = mapper.createObjectNode();
				objectNode.put("status", status);
				return Response.status(HttpServletResponse.SC_BAD_REQUEST).entity(objectNode.toString()).build();
			}
			
			apiDocument.saveQuestionAnswers(id.intValue(), conn, clsDocument);
			
			objectNode = clsDocument.getJsonForQuestionApi(bVer1, clsDocument.getQuestions(), conn);
			objectNode.put("status", status); // CJ-641
			oResponse = Response.status(HttpServletResponse.SC_OK).entity(objectNode.toString()).type(MediaType.APPLICATION_JSON).build();
			return oResponse;
		}
		catch (Exception oError) {
			apiResponseError.recordUnrecoverableError(oError, log, sPath);
		}
		finally {
			SqlUtil.closeInstance(conn);
		}
		return apiResponseError.sendError();
	}
	
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{id}")
	public Response updateDocument(
			@PathParam("version") String version,
			@PathParam("id") Integer id,
			String jsonRequest) {
		ApiResponseError apiResponseError = new ApiResponseError(ApiErrorCodes.DOCUMENT_UPDATE);
		String sPath = this.uriInfo.getRequestUri().toString();

		Connection conn = null;
		try {
			conn = ClsConfig.getDbConnection();

			Oauth oauth = new Oauth(this.httpHeaders);
			if (oauth.isNotAuthorized(conn))
				return apiResponseError.sendNotAuthorized(sPath);

			Object[] params = {id};
			Response oResponse = Oauth.checkParameters(version, params, apiResponseError);
			if (oResponse != null)
				return oResponse;

			DocumentRecord oRecord = DocumentTable.get(id.intValue(), conn);
			if (oRecord == null)
				return apiResponseError.sendNotFound("Path: " + sPath);
			
			if (oRecord.getApplicationId() == null
			|| oauth.getApplicatonId() != oRecord.getApplicationId()) {
				return apiResponseError.sendBadRequest("Invalid Application Id", null);
			}
			
			ObjectNode objectNode;
			ApiDocument apiDocument = new ApiDocument(oauth.getApplicatonId());
			objectNode = apiDocument.parse(jsonRequest, true); //apiDocument.parse(formParams);
			if (objectNode != null)
				return Response.status(HttpServletResponse.SC_BAD_REQUEST).entity(objectNode.toString()).build();
			
			boolean bVer1 =  Oauth.VERSION_1.equals(version);
			objectNode = apiDocument.updateDocument(oRecord, bVer1);
			return ApiResponseUtils.build(objectNode.toString());
		}
		catch (Exception oError) {
			apiResponseError.recordUnrecoverableError(oError, log, sPath);
		}
		finally {
			SqlUtil.closeInstance(conn);
		}
		return apiResponseError.sendError();
	}
	
	@DELETE
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{id}")
	public Response deleteDocument(
			@PathParam("version") String version,
			@PathParam("id") Integer id) {
		ApiResponseError apiResponseError = new ApiResponseError(ApiErrorCodes.DOCUMENT_DELETE);
		String sPath = this.uriInfo.getRequestUri().toString();

		Connection conn = null;
		try {
			conn = ClsConfig.getDbConnection();

			Oauth oauth = new Oauth(this.httpHeaders);
			if (oauth.isNotAuthorized(conn))
				return apiResponseError.sendNotAuthorized(sPath);

			Object[] params = {id};
			Response oResponse = Oauth.checkParameters(version, params, apiResponseError);
			if (oResponse != null)
				return oResponse;

			DocumentRecord oRecord = DocumentTable.get(id.intValue(), conn);
			if (oRecord == null)
				return apiResponseError.sendNotFound("Path: " + sPath);
			
			ApiDocument apiDocument = new ApiDocument(oauth.getApplicatonId());
			ObjectNode objectNode = apiDocument.deleteDocument(oRecord, conn);
			return ApiResponseUtils.build(objectNode.toString());
		}
		catch (Exception oError) {
			apiResponseError.recordUnrecoverableError(oError, log, sPath);
		}
		finally {
			SqlUtil.closeInstance(conn);
		}
		return apiResponseError.sendError();
	}
	
	
	public static String getUrl(boolean bVer1, int id) {
		return "/cls/api/" + (bVer1 ? Oauth.VERSION_1 : Oauth.VERSION_2) + "/documents/" + id;
	}
	
}
