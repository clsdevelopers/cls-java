package gov.dod.cls.api.utils;

public final class ApiErrorCodes {

	public final static String SVC_ID_TOKEN = "1";
	public final static String SVC_ID_CLAUSES = "2";
	public final static String SVC_ID_DOCUMENT = "3";
	public final static String SVC_ID_INTERVIEW = "4";
	
	public final static String TOKEN_CREATE = SVC_ID_TOKEN + "00";
	
	public final static String CLAUSES_LIST = SVC_ID_CLAUSES + "00";
	public final static String CLAUSES_RECORD = SVC_ID_CLAUSES + "01";

	public final static String DOCUMENT_LIST = SVC_ID_DOCUMENT + "10";
	public final static String DOCUMENT_RECORD = SVC_ID_DOCUMENT + "11";
	public final static String DOCUMENT_CREATE = SVC_ID_DOCUMENT + "12";
	public final static String DOCUMENT_UPDATE = SVC_ID_DOCUMENT + "13";
	public final static String DOCUMENT_DELETE = SVC_ID_DOCUMENT + "14";

	public final static String INTERVIEW_CREATE = SVC_ID_INTERVIEW + "20";
	public final static String INTERVIEW_RECORD = SVC_ID_INTERVIEW + "21";
	public final static String INTERVIEW_DELETE = SVC_ID_INTERVIEW + "22";
	public final static String INTERVIEW_URL = SVC_ID_INTERVIEW + "23";
	public final static String INTERVIEW_LIST = SVC_ID_INTERVIEW + "24";

	/*
	 * Error Codes
	 */
	public final static String ERROR_DATA = "0";
	public final static String ERROR_SYSTEM = "1";

	public final static String NOT_FOUND = ERROR_DATA + "01";
	public final static String INVALID_JBOOK_ID = ERROR_DATA + "02";
	public final static String INVALID_PARAMETER = ERROR_DATA + "03";
	public final static String NO_NOT_SUNK_PE = ERROR_DATA + "04";
	public final static String REQUIRE_DAMIR_CACHE = ERROR_DATA + "05";
	public final static String BAD_PAGINATION = ERROR_DATA + "06";
	
	public final static String DB_NOT_AVAILABLE = ERROR_SYSTEM + "99";
	
	public final static String EXCEPTION_GENERAL = ERROR_SYSTEM + "00";
	public final static String EXCEPTION_IO = ERROR_SYSTEM + "01";
	public final static String EXCEPTION_JSONMAPPING = ERROR_SYSTEM + "02";
	public final static String EXCEPTION_JSONGENERATION = ERROR_SYSTEM + "03";
	public final static String EXCEPTION_NULLPOINTER = ERROR_SYSTEM + "04";
	public final static String EXCEPTION_MONGODB = ERROR_SYSTEM + "05";
	public final static String EXCEPTION_MONGODB_SELECTION = ERROR_SYSTEM + "06";
	public final static String EXCEPTION_MONGODB_DB = ERROR_SYSTEM + "07";

	public final static String API_CLIENT_ERROR = ERROR_SYSTEM + "08";
	public final static String BUSY = ERROR_SYSTEM + "10";

	public final static String NOT_AUTHORIZED = ERROR_SYSTEM + "99";

}
