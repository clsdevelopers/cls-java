package gov.dod.cls.api.endpoints;

import java.sql.Connection;

import gov.dod.cls.api.utils.ApiErrorCodes;
import gov.dod.cls.api.utils.ApiResponseError;
import gov.dod.cls.api.utils.ApiResponseUtils;
import gov.dod.cls.bean.Oauth;
import gov.dod.cls.config.ClsConfig;
import gov.dod.cls.db.ClauseTable;
import gov.dod.cls.utils.Pagination;
import gov.dod.cls.utils.sql.SqlUtil;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.apache.log4j.Logger;
import org.codehaus.jackson.node.ObjectNode;

@Path("/{version}/clauses")
public class ClausesEndpoint {
	
	private static Logger log = Logger.getLogger(ClausesEndpoint.class);

	@Context HttpHeaders httpHeaders;
	@Context UriInfo uriInfo;
	
	@GET	// http://localhost:8080/cls/api/v2/clauses
	@Produces(MediaType.APPLICATION_JSON)
	public Response getList(
			@PathParam("version") String version,
			@QueryParam("clause_version_id") Integer clause_version_id,
			@QueryParam("limit") Integer limit,
			@QueryParam("offset") Integer offset) 
	{
		ApiResponseError apiResponseError = new ApiResponseError(ApiErrorCodes.CLAUSES_LIST);
		String sPath = this.uriInfo.getRequestUri().toString();
		
		Connection conn = null;
		try {
			conn = ClsConfig.getDbConnection();

			Oauth oauth = new Oauth(this.httpHeaders);
			if (oauth.isNotAuthorized(conn))
				return apiResponseError.sendNotAuthorized(sPath);

			Response oResponse = Oauth.checkParameters(version, null, apiResponseError);
			if (oResponse != null)
				return oResponse;

			boolean bVer1 =  Oauth.VERSION_1.equals(version);
			Pagination pagination = new Pagination(limit, offset, this.uriInfo);
			pagination.setDataTag("clauses");
			ClauseTable.loadPaginationForApi(pagination, bVer1,
					(clause_version_id == null) ? null : clause_version_id.toString(), conn);
			if (pagination.getCount() <= 0)
				return apiResponseError.sendNotFound("Path: " + sPath);

			return ApiResponseUtils.build(pagination.toJson(bVer1).toString());
		}
		catch (Exception oError) {
			apiResponseError.recordUnrecoverableError(oError, log, sPath);
		}
		finally {
			SqlUtil.closeInstance(conn);
		}
		return apiResponseError.sendError();
	}

	@GET	// http://localhost:8080/cls/api/v2/clauses/7543
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{id}")
	public Response getItem(
			@PathParam("version") String version,
			@PathParam("id") Integer id)
	{
		ApiResponseError apiResponseError = new ApiResponseError(ApiErrorCodes.CLAUSES_RECORD);
		String sPath = this.uriInfo.getRequestUri().toString();

		Connection conn = null;
		try {
			conn = ClsConfig.getDbConnection();

			Oauth oauth = new Oauth(this.httpHeaders);
			if (oauth.isNotAuthorized(conn))
				return apiResponseError.sendNotAuthorized(sPath);

			Object[] params = {id};
			Response oResponse = Oauth.checkParameters(version, params, apiResponseError);
			if (oResponse != null)
				return oResponse;
			
			ObjectNode objectNode = ClauseTable.getRecordJson(id.intValue(), true, Oauth.VERSION_1.equals(version), conn);
			if (objectNode == null)
				return apiResponseError.sendNotFound("Path: " + sPath);

			return ApiResponseUtils.build(objectNode.toString());
		}
		catch (Exception oError) {
			apiResponseError.recordUnrecoverableError(oError, log, sPath);
		}
		finally {
			SqlUtil.closeInstance(conn);
		}
		return apiResponseError.sendError();
	}

	public static String getUrl(boolean bVer1, int id) {
		return "/cls/api/" + (bVer1 ? Oauth.VERSION_1 : Oauth.VERSION_2) + "/clauses/" + id;
	}

}
