package gov.dod.cls.api.endpoints;

import java.sql.Connection;

import gov.dod.cls.api.model.ApiInterview;
import gov.dod.cls.api.utils.ApiErrorCodes;
import gov.dod.cls.api.utils.ApiResponseError;
import gov.dod.cls.api.utils.ApiResponseUtils;
import gov.dod.cls.bean.Oauth;
import gov.dod.cls.config.ClsConfig;
import gov.dod.cls.db.DocumentRecord;
import gov.dod.cls.db.DocumentTable;
import gov.dod.cls.db.OauthInterviewRecord;
import gov.dod.cls.db.OauthInterviewTable;
import gov.dod.cls.utils.Pagination;
import gov.dod.cls.utils.sql.SqlUtil;

import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.apache.log4j.Logger;
import org.codehaus.jackson.node.ObjectNode;

@Path("/{version}/interviews")
public class InterviewEndpoint {

	private static Logger log = Logger.getLogger(InterviewEndpoint.class);

	@Context HttpHeaders httpHeaders;
	@Context UriInfo uriInfo;
	@Context Request request;
	
	@GET	// http://localhost:8080/cls/api/v2/interviews
	@Produces(MediaType.APPLICATION_JSON)
	public Response listInterviews(@PathParam("version") String version,
			@QueryParam("limit") Integer limit,
			@QueryParam("offset") Integer offset) {
		ApiResponseError apiResponseError = new ApiResponseError(ApiErrorCodes.INTERVIEW_LIST);
		String sPath = this.uriInfo.getRequestUri().toString();

		Connection conn = null;
		try {
			conn = ClsConfig.getDbConnection();

			Oauth oauth = new Oauth(this.httpHeaders);
			if (oauth.isNotAuthorized(conn))
				return apiResponseError.sendNotAuthorized(sPath);
			
			Response oResponse = Oauth.checkParameters(version, null, apiResponseError);
			if (oResponse != null)
				return oResponse;
			
			//ApiInterview apiInterview = new ApiInterview();
			//apiInterview.setApplicationId(oauth.getApplicatonId());
			
			Pagination pagination = new Pagination(limit, offset, this.uriInfo);
			pagination.setDataTag("interviews");

			OauthInterviewTable.loadPaginationForApi(pagination, oauth.getApplicatonId(), conn); // oauth.getOauthAccessId();
			if (pagination.getCount() <= 0)
				return apiResponseError.sendNotFound("Path: " + sPath);

			boolean bVer1 =  Oauth.VERSION_1.equals(version);
			return ApiResponseUtils.build(pagination.toJson(bVer1).toString());
		}
		catch (Exception oError) {
			apiResponseError.recordUnrecoverableError(oError, log, sPath);
		}
		finally {
			SqlUtil.closeInstance(conn);
		}
		return apiResponseError.sendError();
	}
	
	
	@POST	// http://localhost:8080/cls/api/v2/interviews
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response createInterview(
			@PathParam("version") String version,
			String jsonRequest) { // MultivaluedMap<String, String> formParams) { // ApiCreateInerview ApiCreateInerview) {
		ApiResponseError apiResponseError = new ApiResponseError(ApiErrorCodes.INTERVIEW_CREATE);
		String sPath = this.uriInfo.getRequestUri().toString();

		Connection conn = null;
		try {
			conn = ClsConfig.getDbConnection();

			Oauth oauth = new Oauth(this.httpHeaders);
			if (oauth.isNotAuthorized(conn))
				return apiResponseError.sendNotAuthorized(sPath);
			
			Response oResponse = Oauth.checkParameters(version, null, apiResponseError);
			if (oResponse != null)
				return oResponse;
			
			ObjectNode objectNode = null;
			ApiInterview apiInterview = new ApiInterview();
			apiInterview.setApplicationId(oauth.getApplicatonId());
			objectNode = apiInterview.parse(jsonRequest);
			if (objectNode != null)
				return Response.status(HttpServletResponse.SC_BAD_REQUEST).entity(objectNode.toString()).build();
			
			objectNode = apiInterview.validateForParemters();
			if (objectNode != null)
				return Response.status(HttpServletResponse.SC_BAD_REQUEST).entity(objectNode.toString()).build();

			if (apiInterview.isValidDocument(conn) == false)
				return apiResponseError.sendNotFound("Path: " + sPath);

			objectNode = apiInterview.hasInterview(conn, oauth.getOauthAccessId());
			if (objectNode != null)
				return Response.status(HttpServletResponse.SC_BAD_REQUEST).entity(objectNode.toString()).build();

			objectNode = apiInterview.createInterview(conn, oauth.getOauthAccessId());
			if (objectNode == null)
				return apiResponseError.sendNotFound("Path: " + sPath);
			return ApiResponseUtils.build(objectNode.toString());
		}
		catch (Exception oError) {
			apiResponseError.recordUnrecoverableError(oError, log, sPath);
		}
		finally {
			SqlUtil.closeInstance(conn);
		}
		return apiResponseError.sendError();
	}
	
	@GET	// http://localhost:8080/cls/api/v2/interviews/?
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{id}")
	public Response getItem(
			@PathParam("version") String version,
			@PathParam("id") Integer id)
	{
		ApiResponseError apiResponseError = new ApiResponseError(ApiErrorCodes.INTERVIEW_RECORD);
		String sPath = this.uriInfo.getRequestUri().toString();

		Connection conn = null;
		try {
			conn = ClsConfig.getDbConnection();

			Oauth oauth = new Oauth(this.httpHeaders);
			if (oauth.isNotAuthorized(conn))
				return apiResponseError.sendNotAuthorized(sPath);

			Object[] params = {id};
			Response oResponse = Oauth.checkParameters(version, params, apiResponseError);
			if (oResponse != null)
				return oResponse;
			
			ApiInterview apiInterview = new ApiInterview();
			apiInterview.setApplicationId(oauth.getApplicatonId());
			OauthInterviewRecord oRecord = apiInterview.getInterview(id, conn); 
			if (oRecord == null)
				return apiResponseError.sendNotFound("Path: " + sPath);

			if (oRecord.isExpired(null, false)) {
				apiInterview.assignToken(oRecord, conn, oauth.getOauthAccessId());
			}

			ObjectNode objectNode = apiInterview.genJson(oRecord, "interview");
			return ApiResponseUtils.build(objectNode.toString());
		}
		catch (Exception oError) {
			apiResponseError.recordUnrecoverableError(oError, log, sPath);
		}
		finally {
			SqlUtil.closeInstance(conn);
		}
		return apiResponseError.sendError();
	}
	
	@DELETE
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{id}")
	public Response deleteItem(
			@PathParam("version") String version,
			@PathParam("id") Integer id) {
		ApiResponseError apiResponseError = new ApiResponseError(ApiErrorCodes.INTERVIEW_DELETE);
		String sPath = this.uriInfo.getRequestUri().toString();
		
		Connection conn = null;
		try {
			conn = ClsConfig.getDbConnection();

			Oauth oauth = new Oauth(this.httpHeaders);
			if (oauth.isNotAuthorized(conn))
				return apiResponseError.sendNotAuthorized(sPath);

			Object[] params = {id};
			Response oResponse = Oauth.checkParameters(version, params, apiResponseError);
			if (oResponse != null)
				return oResponse;

			ApiInterview apiInterview = new ApiInterview();
			apiInterview.setApplicationId(oauth.getApplicatonId());
			OauthInterviewRecord oRecord = apiInterview.getInterview(id, conn); 
			if (oRecord == null)
				return apiResponseError.sendNotFound("Path: " + sPath);
			
			ObjectNode objectNode = apiInterview.deleteRecord(oRecord, conn);
			return ApiResponseUtils.build(objectNode.toString());
		}
		catch (Exception oError) {
			apiResponseError.recordUnrecoverableError(oError, log, sPath);
		}
		finally {
			SqlUtil.closeInstance(conn);
		}
		return apiResponseError.sendError();
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{id}/launch_url")
	public Response getLaunchUrl(
			@PathParam("version") String version,
			@PathParam("id") Integer id) {
		ApiResponseError apiResponseError = new ApiResponseError(ApiErrorCodes.INTERVIEW_URL);
		String sPath = this.uriInfo.getRequestUri().toString();
		
		Connection conn = null;
		try {
			conn = ClsConfig.getDbConnection();

			Oauth oauth = new Oauth(this.httpHeaders);
			if (oauth.isNotAuthorized(conn))
				return apiResponseError.sendNotAuthorized(sPath);

			Object[] params = {id};
			Response oResponse = Oauth.checkParameters(version, params, apiResponseError);
			if (oResponse != null)
				return oResponse;
			
			ApiInterview apiInterview = new ApiInterview();
			apiInterview.setApplicationId(oauth.getApplicatonId());
			OauthInterviewRecord oRecord = apiInterview.getInterview(id, conn); 
			if (oRecord == null)
				return apiResponseError.sendNotFound("Path: " + sPath);
			
			if (oRecord.isExpired(null, false)) {
				apiInterview.assignToken(oRecord, conn, oauth.getOauthAccessId());
			}
			
			// CJ-308; For Orders, launch a different URL.
			String docType = "";
			DocumentRecord docRecord = DocumentTable.find(oRecord.getDocumentId(), conn);
			if (docRecord != null)
				docType = docRecord.getDocumentType();
			
			ObjectNode objectNode = apiInterview.getLaunchUrl(oRecord, docType);
			return ApiResponseUtils.build(objectNode.toString());
		}
		catch (Exception oError) {
			apiResponseError.recordUnrecoverableError(oError, log, sPath);
		}
		finally {
			SqlUtil.closeInstance(conn);
		}
		return apiResponseError.sendError();
	}
	
}
