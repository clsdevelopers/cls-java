package gov.dod.cls.api.endpoints;

import java.sql.Connection;
import java.util.ArrayList;

import gov.dod.cls.api.utils.ApiErrorCodes;
import gov.dod.cls.api.utils.ApiResponseError;
import gov.dod.cls.api.utils.ApiResponseUtils;
import gov.dod.cls.bean.Oauth;
import gov.dod.cls.config.ClsConfig;
import gov.dod.cls.db.ClauseVersionRecord;
import gov.dod.cls.db.ClauseVersionTable;
import gov.dod.cls.db.QuestionConditionTable;
import gov.dod.cls.db.QuestionGroupRefTable;
import gov.dod.cls.db.QuestionTable;
import gov.dod.cls.utils.Pagination;
import gov.dod.cls.utils.sql.SqlUtil;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.apache.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.node.ObjectNode;

@Path("/{version}/clsVersion")
public class ClsVersionEndPoint {

	private static Logger log = Logger.getLogger(ClausesEndpoint.class);

	@Context HttpHeaders httpHeaders;
	@Context UriInfo uriInfo;
	
	@GET	// http://localhost:8080/cls/api/v1/clsVersion
	@Produces(MediaType.APPLICATION_JSON)
	public Response getList(
			@PathParam("version") String version,
			@QueryParam("limit") Integer limit,
			@QueryParam("offset") Integer offset) 
	{
		ApiResponseError apiResponseError = new ApiResponseError(ApiErrorCodes.CLAUSES_LIST);
		String sPath = this.uriInfo.getRequestUri().toString();
		
		Connection conn = null;
		try {
			conn = ClsConfig.getDbConnection();

			Oauth oauth = new Oauth(this.httpHeaders);
			if (oauth.isNotAuthorized(conn))
				return apiResponseError.sendNotAuthorized(sPath);

			Response oResponse = Oauth.checkParameters(version, null, apiResponseError);
			if (oResponse != null)
				return oResponse;
			
			ArrayList<Integer> supportedClsVersionIds = oauth.getSupportedClsVersions(conn); // CJ-582

			boolean bVer1 =  Oauth.VERSION_1.equals(version);
			Pagination pagination = new Pagination(limit, offset, this.uriInfo);
			pagination.setDataTag("cls_versions");
			ClauseVersionTable.loadPaginationForApi(pagination, bVer1, supportedClsVersionIds, conn);
			if (pagination.getCount() <= 0)
				return apiResponseError.sendNotFound("Path: " + sPath);

			return ApiResponseUtils.build(pagination.toJson(bVer1).toString());
		}
		catch (Exception oError) {
			apiResponseError.recordUnrecoverableError(oError, log, sPath);
		}
		finally {
			SqlUtil.closeInstance(conn);
		}
		return apiResponseError.sendError();
	}	

	@GET	// http://localhost:8080/cls/api/v2/clsVersion/1
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{id}")
	public Response getItem(
			@PathParam("version") String version,
			@PathParam("id") Integer id)
	{
		ApiResponseError apiResponseError = new ApiResponseError(ApiErrorCodes.CLAUSES_RECORD);
		String sPath = this.uriInfo.getRequestUri().toString();

		Connection conn = null;
		try {
			conn = ClsConfig.getDbConnection();

			Oauth oauth = new Oauth(this.httpHeaders);
			if (oauth.isNotAuthorized(conn))
				return apiResponseError.sendNotAuthorized(sPath);

			Object[] params = {id};
			Response oResponse = Oauth.checkParameters(version, params, apiResponseError);
			if (oResponse != null)
				return oResponse;
			
			if (!oauth.isClsVersionSupported(id.intValue(), conn)) // CJ-582
				return apiResponseError.sendNotFound("Path: " + sPath);
			
			ObjectNode objectNode = ClauseVersionTable.getRecordJsonForApi(id.intValue(), Oauth.VERSION_1.equals(version), conn);
			if (objectNode == null)
				return apiResponseError.sendNotFound("Path: " + sPath);

			return ApiResponseUtils.build(objectNode.toString());
		}
		catch (Exception oError) {
			apiResponseError.recordUnrecoverableError(oError, log, sPath);
		}
		finally {
			SqlUtil.closeInstance(conn);
		}
		return apiResponseError.sendError();
	}

	@GET	// http://localhost:8080/cls/api/v2/clsVersion/1/questions
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{id}/questions")
	public Response getQuestions(
			@PathParam("version") String version,
			@PathParam("id") Integer id)
	{
		ApiResponseError apiResponseError = new ApiResponseError(ApiErrorCodes.CLAUSES_RECORD);
		String sPath = this.uriInfo.getRequestUri().toString();

		Connection conn = null;
		try {
			conn = ClsConfig.getDbConnection();

			Oauth oauth = new Oauth(this.httpHeaders);
			if (oauth.isNotAuthorized(conn))
				return apiResponseError.sendNotAuthorized(sPath);

			Object[] params = {id};
			Response oResponse = Oauth.checkParameters(version, params, apiResponseError);
			if (oResponse != null)
				return oResponse;
			
			ClauseVersionRecord clauseVersionRecord = ClauseVersionTable.getRecord(id.intValue()); 
			if (clauseVersionRecord == null)
				return apiResponseError.sendNotFound("Path: " + sPath);
			
			if (!oauth.isClsVersionSupported(id.intValue(), conn)) // CJ-582
				return apiResponseError.sendNotFound("Path: " + sPath);
			
			QuestionTable questionTable = QuestionTable.getSubsetByClauseVersion(id, conn);
			QuestionGroupRefTable questionGroupRefTable = QuestionGroupRefTable.getInstance(conn);
			QuestionConditionTable questionConditionTable = QuestionConditionTable.getQuestionSubset(questionTable, conn);
			
			ObjectMapper mapper = new ObjectMapper();
			ObjectNode objectNode = mapper.createObjectNode();
			objectNode.put(ClauseVersionRecord.FIELD_CLAUSE_VERSION_ID, clauseVersionRecord.getId());
			objectNode.put(ClauseVersionRecord.FIELD_CLAUSE_VERSION_NAME, clauseVersionRecord.getName());
			
			objectNode.put("questions", questionTable.toJsonForVersionApi(mapper, questionGroupRefTable, questionConditionTable));

			return ApiResponseUtils.build(objectNode.toString());
		}
		catch (Exception oError) {
			apiResponseError.recordUnrecoverableError(oError, log, sPath);
		}
		finally {
			SqlUtil.closeInstance(conn);
		}
		return apiResponseError.sendError();
	}
	@GET	// http://localhost:8080/cls/api/v2/clsVersion/1/combine
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{id}/combine")
	public Response getAllChanges(
			@PathParam("version") String version,
			@PathParam("id") Integer id)
	{
		ApiResponseError apiResponseError = new ApiResponseError(ApiErrorCodes.CLAUSES_RECORD);
		String sPath = this.uriInfo.getRequestUri().toString();

		Connection conn = null;
		try {
			conn = ClsConfig.getDbConnection();

			Oauth oauth = new Oauth(this.httpHeaders);
			if (oauth.isNotAuthorized(conn))
				return apiResponseError.sendNotAuthorized(sPath);

			Object[] params = {id};
			Response oResponse = Oauth.checkParameters(version, params, apiResponseError);
			if (oResponse != null)
				return oResponse;
			
			if (!oauth.isClsVersionSupported(id.intValue(), conn)) // CJ-582
				return apiResponseError.sendNotFound("Path: " + sPath);
			
			ObjectNode objectNode = ClauseVersionTable.getAllChangesJsonForApi(id.intValue(), Oauth.VERSION_1.equals(version), conn);
			if (objectNode == null)
				return apiResponseError.sendNotFound("Path: " + sPath);

			return ApiResponseUtils.build(objectNode.toString());
		}
		catch (Exception oError) {
			apiResponseError.recordUnrecoverableError(oError, log, sPath);
		}
		finally {
			SqlUtil.closeInstance(conn);
		}
		return apiResponseError.sendError();
	}

}
