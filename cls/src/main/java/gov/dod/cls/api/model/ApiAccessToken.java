package gov.dod.cls.api.model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Iterator;

import gov.dod.cls.bean.Oauth;
import gov.dod.cls.config.ClsConfig;
import gov.dod.cls.db.OauthAccessRecord;
import gov.dod.cls.db.OauthApplicationRecord;
import gov.dod.cls.utils.CommonUtils;
import gov.dod.cls.utils.sql.SqlUtil;

import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.node.ObjectNode;

public class ApiAccessToken {

	private String grantType;
	private String clientId;
	private String clientSecret;
	
	private OauthApplicationRecord oauthApplicationRecord;
	private String lastError;
	
	public void clear() {
		this.clientId = null;
		this.clientSecret = null;
		this.oauthApplicationRecord = null;
		this.lastError = null;
	}
	
	public ObjectNode parse(String jsonRequest) {
		String invalidRequests = "";
		try {
			if (CommonUtils.isNotEmpty(jsonRequest)) {
				ObjectMapper mapper = new ObjectMapper();
				JsonNode actualObj = mapper.readTree(jsonRequest);
				
				Iterator<String> fieldNames = actualObj.getFieldNames();
				while (fieldNames.hasNext()) {
					String fieldName = fieldNames.next(); 
					switch (fieldName) {
					case "grant_type":
						this.grantType = CommonUtils.asString(actualObj, fieldName);
						break;
					case "client_id":
						this.clientId = CommonUtils.asString(actualObj, fieldName);
						break;
					case "client_secret":
						this.clientSecret = CommonUtils.asString(actualObj, fieldName);
						break;
					default:
						invalidRequests = ApiDocument.addInvalidEntry(invalidRequests, fieldName);
						break;
					}
				}
				if (invalidRequests.isEmpty()) {
					return null;
				}
				/* CJ-803 replaced followings with above
				this.grantType = CommonUtils.asString(actualObj, "grant_type");
				this.clientId = CommonUtils.asString(actualObj, "client_id");
				this.clientSecret = CommonUtils.asString(actualObj, "client_secret");
				return null;
				*/
			}
		} catch (Exception oError) {
			oError.printStackTrace();
		}
		
		ObjectMapper mapper = new ObjectMapper();
		ObjectNode objectNode = mapper.createObjectNode();
		objectNode.put("status", "Invalid JSON request" + invalidRequests);
		return objectNode;
	}

	public ObjectNode validateRequestEntries() {
		ObjectMapper mapper = new ObjectMapper();
		ObjectNode result = mapper.createObjectNode();
		
		if (!("client_credentials".equals(this.grantType)))
			result.put("client_credentials", "required");

		if (CommonUtils.isEmpty(this.clientId)) {
			result.put("client_id", "required");
		} else if (this.clientId.length() > 255) {
			result.put("client_id", "exceeds maximum size of 255");
		}
		
		if (CommonUtils.isNotEmpty(this.clientSecret)) {
			if (this.clientSecret.length() > 255) {
				result.put("client_secret", "exceeds maximum size of 255");
			}
		} else {
			result.put("client_secret", "required");
		}
		
		return ((result.size() == 0) ? null : result);	
	}
	
	public boolean isAuthorized(Connection conn) throws SQLException {
		if (CommonUtils.isEmpty(this.clientId) || CommonUtils.isEmpty(this.clientSecret))
			return false;
		
		boolean bCreateConnection = (conn == null);
		PreparedStatement ps1 = null;
		ResultSet rs1 = null;
		try {
			if (bCreateConnection)
				conn = ClsConfig.getDbConnection();
			String sSql = OauthApplicationRecord.SQL_SELECT
					+ " WHERE " + "A." + OauthApplicationRecord.FIELD_APPLICATION_UID + " = ?";
			ps1 = conn.prepareStatement(sSql);
			ps1.setString(1, this.clientId);
			rs1 = ps1.executeQuery();
			if (rs1.next()) {
				OauthApplicationRecord oRecord = new OauthApplicationRecord();
				oRecord.read(rs1);
				if (oRecord.getApplicationSecret().equals(this.clientSecret)) {
					this.oauthApplicationRecord = oRecord;
				} else {
					this.lastError = "Invalid Secret Code";
				}
			} else {
				this.lastError = "Invalid UID";
			}
		}
		finally {
			SqlUtil.closeInstance(rs1, ps1);
			if (bCreateConnection)
				SqlUtil.closeInstance(conn);
		}
		return (this.oauthApplicationRecord != null);
	}

	public ObjectNode createToken(Connection conn) throws Exception {
		String accessToken = Oauth.genCode();
		OauthAccessRecord oRecord = OauthAccessRecord.insert(conn, 
				this.oauthApplicationRecord.getApplicationId().intValue(), accessToken);
		if (oRecord == null) {
			return null;
		}
		
		ObjectMapper mapper = new ObjectMapper();
		ObjectNode result = mapper.createObjectNode();
		result.put("access_token", accessToken);
		result.put("token_type", Oauth.TOKEN_TYPE);
		result.put("expires_in", Oauth.EXPIRES_IN);
		return result;
	}

	public OauthApplicationRecord getOauthApplicationRecord() { return oauthApplicationRecord; }
	public String getLastError() { return lastError; }

	public String getGrantType() {
		return grantType;
	}
	
}
