package gov.dod.cls.api.model;

import java.text.NumberFormat;
import java.text.ParseException;
import java.util.ArrayList;

import org.jasypt.commons.CommonUtils;

import gov.dod.cls.db.DocumentAnswerRecord;
import gov.dod.cls.db.QuestionChoiceRecord;
import gov.dod.cls.db.QuestionChoiceTable;
import gov.dod.cls.db.QuestionRecord;
import gov.dod.cls.db.QuestionTypeRefRecord;

public class ApiDocumentQuestionAnswerItem {

	private DocumentAnswerRecord documentAnswerRecord = null;
	private QuestionRecord questionRecord = null;
	private ArrayList<String> answers = null;

	// ----------------------------------------------------------------
	public void addAnswer(String answer) {
		if (CommonUtils.isNotEmpty(answer)) {
			if (this.answers == null)
				this.answers = new ArrayList<String>();
			this.answers.add(answer);
		}
	}
	
	public String detectInvalidAnswers() {
		if (this.questionRecord == null) {
			return "Unknown question_id";
		} else if ((this.answers == null) || (this.answers.size() == 0)) {
			if ((documentAnswerRecord == null) || CommonUtils.isEmpty(documentAnswerRecord.getAnswer())) {
				return "Empty answers";
			}
		} else {
			String sQuestionType = this.questionRecord.getQuestionType();
			if (!QuestionTypeRefRecord.TYPE_CD_MULTIPLE_CHOICES.equals(sQuestionType)) {
				if (this.answers.size() > 1) {
					return "'" + sQuestionType + "' type question does not accept multiple answers";
				}
				String value = this.answers.get(0);
				switch (sQuestionType) {
				case QuestionTypeRefRecord.TYPE_CD_BOOLEAN:
					if (!(QuestionTypeRefRecord.VALUE_YES.equals(value) || QuestionTypeRefRecord.VALUE_NO.equals(value))) {
						return "not a valid YES or NO answer";
					}
					break;
				case QuestionTypeRefRecord.TYPE_CD_NUMERIC:
					try {
						NumberFormat format = NumberFormat.getCurrencyInstance();
						format.parse(value);
					} catch (ParseException oParseException) {
						try { // CJ-631
							String sValue = value;
							if (sValue.startsWith("$"))
								sValue = sValue.substring(1);
							sValue = sValue.replace(",", "");
							Double.parseDouble(sValue);
						} catch (Exception oError) {
							return "not valid amount formatted answer";
						}
					}
					if (value.length() > 12) { // CJ-787
						return "exceed the maximum number of characters, 12";
					}
					break;
				case QuestionTypeRefRecord.TYPE_CD_SINGLE_CHOICE:
					QuestionChoiceTable questionChoiceTable = this.questionRecord.getQuestionChoiceTable();
					QuestionChoiceRecord questionChoiceRecord = questionChoiceTable.findByText(value);
					if (questionChoiceRecord == null) {
						return "answer is invalid";
					}
					break;
				}
			} else {
				String sNoneOfAbove = null;
				QuestionChoiceTable questionChoiceTable = this.questionRecord.getQuestionChoiceTable();
				for (int index = 0; index < this.answers.size(); index++) {
					String value = this.answers.get(index);
					QuestionChoiceRecord questionChoiceRecord = questionChoiceTable.findByText(value);
					if (questionChoiceRecord == null) {
						return "#" + (index + 1) + " answer is invalid";
					} else {
						switch (value.toUpperCase()) {
						case "NO EXEMPTION APPLIES":
						case "NO EXCEPTIONS APPLY":
						case "NO WAIVERS APPROVED":
						case "NONE APPLY":
						case "NONE OF THE ABOVE":
						case "NONE OF THE ABOVE SUBCATEGORIES":
						case "NONE OF THE ABOVE OR NOT REQUIRED":
						case "NONE OF THE ABOVE APPLY":
						case "NONE OF THE ABOVE LISTED PLACES OF PERFORMANCE":
						case "NONE OF THE ABOVE FOREIGN COUNTRIES":
						case "NONE OF THE ABOVE STATES":
						case "NONE OF THE LISTED SOURCES":
						case "VARIATION IN QUANITY DOES NOT APPLY":
							sNoneOfAbove = value;
							break;
						default:
							if (value.toUpperCase().startsWith("NONE OF THE ABOVE")) {
								sNoneOfAbove = value;
							}
							break;
						}
					}
				}
				if ((sNoneOfAbove != null) && (this.answers.size() > 1)) {
					return "'" + sNoneOfAbove + "' answer must be selected alone"; // is conflicted with other";
				}
			}
		}
		return null;
	}
	
	// ----------------------------------------------------------------
	public DocumentAnswerRecord getDocumentAnswerRecord() {
		return documentAnswerRecord;
	}
	public void setDocumentAnswerRecord(DocumentAnswerRecord documentAnswerRecord) {
		this.documentAnswerRecord = documentAnswerRecord;
	}
	public QuestionRecord getQuestionRecord() {
		return questionRecord;
	}
	public void setQuestionRecord(QuestionRecord questionRecord) {
		this.questionRecord = questionRecord;
	}
	public ArrayList<String> getAnswers() {
		return answers;
	}
	public void setAnswers(ArrayList<String> answers) {
		this.answers = answers;
	}
	
	private String DIVIDER = "\t";
	
	public String getAnswerStr() {
		String result = null;
		if (this.answers != null) {
			for (String value : this.answers) {
				if (result == null)
					result = value;
				else
					result += DIVIDER + value;
			}
		}
		return result;
	}
	
}
