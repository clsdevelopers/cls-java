package gov.dod.cls.api.model;

import gov.dod.cls.bean.ClsDocument;
import gov.dod.cls.config.ClsConfig;
import gov.dod.cls.db.AuditEventRecord;
import gov.dod.cls.db.ClauseVersionTable;
import gov.dod.cls.db.DocStatusRefRecord;
import gov.dod.cls.db.DocTypeRefRecord;
import gov.dod.cls.db.DocumentAnswerRecord;
import gov.dod.cls.db.DocumentAnswerTable;
import gov.dod.cls.db.DocumentRecord;
import gov.dod.cls.db.DocumentTable;
import gov.dod.cls.db.OauthInterviewRecord;
import gov.dod.cls.db.QuestionConditionTable;
import gov.dod.cls.db.QuestionRecord;
import gov.dod.cls.db.QuestionTable;
import gov.dod.cls.utils.CommonUtils;
import gov.dod.cls.utils.sql.SqlUpdate;
import gov.dod.cls.utils.sql.SqlUtil;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;

import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.node.ArrayNode;
import org.codehaus.jackson.node.ObjectNode;

public class ApiDocument {
	
	private static final String ATTRIBUTE_ANSWERS = "answers";
	private static final String APPIAN_CODE1 = "$randomInt";

	private static final String ATTR_DOCUMENT_TYPE = "document_type";
	private static final String ATTR_ACQUISITION_TITLE = "acquisition_title";
	private static final String ATTR_DOCUMENT_NUMBER = "document_number";
	private static final String ATTR_CONTRACT_NUMBER = "contract_number";
	private static final String ATTR_ORDER_NUMBER = "order_number";
	private static final String ATTR_FULL_NAME = "full_name";
	private static final String ATTR_COPY_TEMPLATE = "copy_template";

	private static final String ATTR_STATUS = "status";
	
	private static final String EMPTY_REQUEST = "Empty request";
	private static final String UNKNOWN_JSON_REQUEST = "Unknown Json request";
	private static final String REQUIRED = "required";

	private int applicationId;
	private String documentType;
	private String acquisitionTitle;
	private String documentNumber;
	private String contractNumber;
	private String orderNumber;
	private String fullName;
	private Boolean interviewComplete;
	private String docStatusCd; // CJ-764
	private Boolean bCopyDoc = false; // CJ-1049
	private Date documentDate = null; // CJ-1312

	private Integer solicitationId = null; 
	private Integer clauseVersionId = null;
	
	private DocumentRecord oRecord = null; 
	private ClsDocument solClsDocument = null;
	
	private ArrayList<ApiDocumentQuestionAnswerItem> alAnswers = null;
	
	public ApiDocument(int applicationId) {
		this.applicationId = applicationId;
	}
	
	public void clear() {
		this.documentType = null;
		this.acquisitionTitle = null;
		this.documentNumber = null;
		this.contractNumber = null;
		this.orderNumber = null;
		this.fullName = null;
		this.interviewComplete = null;
		this.docStatusCd = null; // CJ-764
		this.bCopyDoc = false;

		this.solicitationId = null; 
		this.clauseVersionId = null;
		this.solClsDocument = null;
	}
	
	public static String addInvalidEntry(String invalidRequests, String fieldName) {
		if (invalidRequests.isEmpty())
			invalidRequests = " containing unknown attribute: " + fieldName;
		else
			invalidRequests += ", " + fieldName;
		return invalidRequests;
	}
	
	public ObjectNode parse(String jsonRequest, boolean forUpdate) {
		String invalidRequests = "";
		try {
			ObjectMapper mapper = new ObjectMapper();
			JsonNode actualObj = mapper.readTree(jsonRequest);
			
			String sCopyDoc;
			this.bCopyDoc = false;
			
			Iterator<String> fieldNames = actualObj.getFieldNames();
			while (fieldNames.hasNext()) {
				String fieldName = fieldNames.next(); 
				switch (fieldName) {
				case ATTR_DOCUMENT_TYPE:
					this.documentType = CommonUtils.asString(actualObj, fieldName);
					break;
				case ATTR_ACQUISITION_TITLE:
					this.acquisitionTitle = CommonUtils.asString(actualObj, fieldName);
					break;
				case ATTR_DOCUMENT_NUMBER:
					this.documentNumber = CommonUtils.asString(actualObj, fieldName);
					break;
				case ATTR_CONTRACT_NUMBER:
					this.contractNumber = CommonUtils.asString(actualObj, fieldName);
					break;
				case ATTR_ORDER_NUMBER:
					this.orderNumber = CommonUtils.asString(actualObj, fieldName);
					break;
				case ATTR_FULL_NAME:
					this.fullName = CommonUtils.asString(actualObj, fieldName);
					break;
				case DocumentRecord.FIELD_DOCUMENT_STATUS:
					this.docStatusCd = CommonUtils.asString(actualObj, fieldName);
					break;
				case DocumentRecord.FIELD_SOLICITATION_ID:
					this.solicitationId = CommonUtils.asInteger(actualObj, fieldName);
					break;
				case DocumentRecord.FIELD_CLAUSE_VERSION_ID:
					this.clauseVersionId = CommonUtils.asInteger(actualObj, fieldName);
					break;
				case ATTR_COPY_TEMPLATE:
					sCopyDoc = CommonUtils.asString(actualObj, fieldName);
					if ((sCopyDoc.toUpperCase().equals("TRUE")) || (sCopyDoc.equals("1")) || (sCopyDoc.toUpperCase().equals("YES")))
						this.bCopyDoc = true;
					break;
				case DocumentRecord.FIELD_DOCUMENT_DATE:
					String sDocumentDate = CommonUtils.asString(actualObj, fieldName);
					try {
						this.documentDate = CommonUtils.yyyymmjddToDate(sDocumentDate); // CJ-1312
					} catch (ParseException oError) {
						invalidRequests = (invalidRequests.isEmpty() ? "" : ", ")
								+ " containing invalid " + DocumentRecord.FIELD_DOCUMENT_DATE + " value";
					}
				default:
					if (forUpdate)
						invalidRequests = ApiDocument.addInvalidEntry(invalidRequests, fieldName); // CJ-1252
					break;
				}
			}
		
			if (forUpdate && this.bCopyDoc)
				invalidRequests = ApiDocument.addInvalidEntry(invalidRequests, ATTR_COPY_TEMPLATE);
			
			// CJ-1160,	solicitationId and clauseVersionId are REQUIRED for creating Awards.
			// 			However, the parameters are invalid for creating Solicitations and Orders.
			if (!forUpdate && (DocTypeRefRecord.DOC_TYPE_CD_ORDER.equals(this.documentType)) 
			|| (!this.bCopyDoc && DocTypeRefRecord.DOC_TYPE_CD_SOLICITATION.equals(this.documentType))) {	// Not Linked Award
				// Do not accept solicitationId parameter
				if (this.solicitationId != null) {
					this.solicitationId = null;
					invalidRequests = ApiDocument.addInvalidEntry(invalidRequests, DocumentRecord.FIELD_SOLICITATION_ID);
				}
				// Do not accept clauseVersionId parameter
				if (this.clauseVersionId != null) {
					this.clauseVersionId = null;
					invalidRequests = ApiDocument.addInvalidEntry(invalidRequests, DocumentRecord.FIELD_CLAUSE_VERSION_ID);
				}
			}
			
			if (invalidRequests.isEmpty()) {
				return null;
			}
		} catch (Exception oError) {
			oError.printStackTrace();
		}
		
		ObjectMapper mapper = new ObjectMapper();
		ObjectNode objectNode = mapper.createObjectNode();
		objectNode.put(ATTR_STATUS, "Invalid Json request" + invalidRequests);
		return objectNode;
	}

	public ObjectNode validateDocumentForAnswers(ClsDocument clsDocument, Connection conn) throws SQLException {
		ObjectMapper mapper = new ObjectMapper();
		ObjectNode result = null;
		
		if (DocStatusRefRecord.CD_FINALIZED.equals(clsDocument.getDocumentStatus())) {
			result = mapper.createObjectNode();
			result.put(ATTR_STATUS, "Finalized document can't be changed");
		} else {
			OauthInterviewRecord oauthInterviewRecord = OauthInterviewRecord.getRecord(conn, applicationId, clsDocument.getDocId());
			if ((oauthInterviewRecord != null) && oauthInterviewRecord.isLaunchedBefore()) {
				result = mapper.createObjectNode();
				result.put(ATTR_STATUS, "CLS launched for requested document");
			}
		}
		
		return result;
	}
	
	public String parseQuestionAnswers(String jsonRequest, ClsDocument clsDocument,
			QuestionConditionTable questionConditionTable, ArrayList<String> listInvalidFields)
			throws JsonProcessingException, IOException {
		ObjectMapper mapper = new ObjectMapper();
		String sStatus = "";
		ArrayNode anQuestionAnswers = null;
		
		if (CommonUtils.isEmpty(jsonRequest)) {
			sStatus = EMPTY_REQUEST;
		} else {
			JsonNode actualObj = mapper.readTree(jsonRequest);
			if (!(actualObj instanceof ArrayNode)) {
				sStatus = UNKNOWN_JSON_REQUEST;
			} else {
				anQuestionAnswers = (ArrayNode)actualObj;
				if (anQuestionAnswers.size() == 0) {
					sStatus = EMPTY_REQUEST;
				}
			}
		}

		if (sStatus.isEmpty()) {
			String sQuestionCode;
			Integer iQuestionId;
			QuestionRecord questionRecord;
			QuestionTable questionTable = clsDocument.getQuestions();
			DocumentAnswerTable documentAnswerTable = clsDocument.getAnswers();
			this.alAnswers = new ArrayList<ApiDocumentQuestionAnswerItem>();
			for (int iItem = 0; iItem < anQuestionAnswers.size(); iItem++) {
				JsonNode jnItem = anQuestionAnswers.get(iItem);

				Iterator<String> fieldNames = jnItem.getFieldNames(); // CJ-803
				while (fieldNames.hasNext()) {
					String fieldName = fieldNames.next(); 
					switch (fieldName) {
					case QuestionRecord.FIELD_QUESTION_CODE:
					case QuestionRecord.FIELD_QUESTION_ID:
					case ATTRIBUTE_ANSWERS:
						break;
					default:
						if (!listInvalidFields.contains(fieldName)) {
							listInvalidFields.add(fieldName);
						}
						break;
					}
				}
				
				if ((jnItem.has(QuestionRecord.FIELD_QUESTION_ID) || jnItem.has(QuestionRecord.FIELD_QUESTION_CODE)) // CJ-640
						&& jnItem.has(ATTRIBUTE_ANSWERS)) {
					questionRecord = null;
					// CJ-640 Allow user to submit "question_codes" for POST Answers call for Indicators (Appian)
					if (jnItem.has(QuestionRecord.FIELD_QUESTION_CODE)) {
						sQuestionCode = CommonUtils.asString(jnItem, QuestionRecord.FIELD_QUESTION_CODE);
						questionRecord = questionTable.findByCode(sQuestionCode);
						if (questionRecord == null) {
							sStatus += ((sStatus == null) ? "" : "\n") + "#" + (iItem + 1) + " element does not have a valid question_code, '" + sQuestionCode + "'.";
							continue;
						}
					} else
						sQuestionCode = null;

					if (jnItem.has(QuestionRecord.FIELD_QUESTION_ID)) {
						iQuestionId = CommonUtils.asInteger(jnItem, QuestionRecord.FIELD_QUESTION_ID);
						if (iQuestionId == null) {
							sStatus += (sStatus.isEmpty() ? "" : "\n") + "#" + (iItem + 1) + " element does not have a numeric question_id, " +
									CommonUtils.asString(jnItem, QuestionRecord.FIELD_QUESTION_ID) + ".";
							continue;
						}
						QuestionRecord questionRecordByCode = questionRecord;
						questionRecord = questionTable.findById(iQuestionId);
						if (questionRecord == null) {
							sStatus += (sStatus.isEmpty() ? "" : "\n") + "#" + (iItem + 1) + " element does not have a valid question_id, " +
									iQuestionId + ".";
							continue;
						}
						if ((questionRecordByCode != null) && (questionRecordByCode != questionRecord)) {
							sStatus += (sStatus.isEmpty() ? "" : "\n") + "#" + (iItem + 1) + " element does not have a valid question_id, " +
									iQuestionId + ", and question_code, '" + sQuestionCode + "', combinations.";
							continue;
						}
					} else {
						iQuestionId = null;
					}
					
					if (!questionRecord.isBaseQuestion(questionConditionTable)) { // CJ-581
						sStatus += (sStatus.isEmpty() ? "" : "\n") + "#" + (iItem + 1) + " element does not have a valid base question, '"
								+ questionRecord.getQuestionCode() + "' .";
						continue;
					}
					
					ApiDocumentQuestionAnswerItem oItem = new ApiDocumentQuestionAnswerItem();
					oItem.setQuestionRecord(questionRecord);
					oItem.setDocumentAnswerRecord(documentAnswerTable.findByQuestionId(questionRecord.getId()));
					JsonNode jnAnswers = jnItem.get(ATTRIBUTE_ANSWERS);
					if (jnAnswers != null) {
						if (jnAnswers.isArray()) {
							for (int iValue = 0; iValue < jnAnswers.size(); iValue++) {
								oItem.addAnswer(jnAnswers.get(iValue).asText());
							}
						} else {
							String sAnswer = jnAnswers.asText();
							if (! "null".equalsIgnoreCase(sAnswer))
								oItem.addAnswer(jnAnswers.asText());
						}
					}
					String sError = oItem.detectInvalidAnswers();
					
					for (ApiDocumentQuestionAnswerItem oAnswerItem : this.alAnswers) {
						if (oAnswerItem.getQuestionRecord() == questionRecord) {
							sError += ((sError == null) ? "" : "\n") + " repeated question/answer.";
							break;
						}
					}
					
					if (sError != null)
						sStatus += (sStatus.isEmpty() ? "" : "\n") + questionRecord.getQuestionCode() + " " + sError + ".";
					else
						this.alAnswers.add(oItem);
				} else {
					sStatus += (sStatus.isEmpty() ? "" : "\n") + "#" + (iItem + 1) + " element does not have required attributes.";
				}
			}
		}
		
		if (!listInvalidFields.isEmpty()) { // CJ-803
			String invalidRequests = "";
			for (String fieldName : listInvalidFields) {
				invalidRequests = ApiDocument.addInvalidEntry(invalidRequests, fieldName);
			}
			sStatus += (sStatus.isEmpty() ? "" : "\n") + invalidRequests;
		}
		
		return sStatus;
	}
	
	public void saveQuestionAnswers(int documentId, Connection conn, ClsDocument clsDocument) throws Exception {
		// this.alAnswers = new ArrayList<ApiDocumentQuestionAnswerItem>();
		AuditEventRecord auditEventRecord = new AuditEventRecord(AuditEventRecord.ACTION_DOCUMENT_UPDATED, null, null);
		auditEventRecord.addAttribute(AuditEventRecord.FIELD_APPLICATION_ID, this.applicationId);
		
		auditEventRecord.addAttribute(DocumentRecord.FIELD_DOCUMENT_ID, documentId);
		PreparedStatement psInsert = null;
		PreparedStatement psUpdate = null;
		PreparedStatement psDelete = null;
		try {
			for (ApiDocumentQuestionAnswerItem oAnswer : this.alAnswers) {
				String sAnswer = oAnswer.getAnswerStr();
				DocumentAnswerRecord documentAnswerRecord = oAnswer.getDocumentAnswerRecord();
				if (documentAnswerRecord == null) {
					if (psInsert == null)
						psInsert = DocumentAnswerTable.createInertPrepareStatement(conn);
					psInsert.setInt(1, documentId);
					psInsert.setInt(2, oAnswer.getQuestionRecord().getId());
					if (sAnswer == null)
						psInsert.setNull(3, java.sql.Types.VARCHAR);
					else
						psInsert.setString(3, sAnswer);
					psInsert.execute();
				} else if (sAnswer != null) {
					if (CommonUtils.isNotSame(documentAnswerRecord.getAnswer(), sAnswer)) {
						if (psUpdate == null)
							psUpdate = DocumentAnswerTable.createUpdatePrepareStatement(conn);
						psUpdate.setString(1, sAnswer);
						psUpdate.setInt(2, documentAnswerRecord.getId());
						psUpdate.execute();
					}
				} else {
					if (psDelete == null)
						psDelete = DocumentAnswerTable.createAnswerDeletePrepareStatement(conn);
					psDelete.setInt(1, documentAnswerRecord.getId());
					psDelete.execute();
				}
				auditEventRecord.addAttribute(oAnswer.getQuestionRecord().getQuestionCode(), sAnswer);
			}
		} finally {
			SqlUtil.closeInstance(psInsert);
			SqlUtil.closeInstance(psUpdate);
			SqlUtil.closeInstance(psDelete);
		}
		clsDocument.saveUpdatedByApi(auditEventRecord, conn);
		clsDocument.loadDocumentForApi(documentId, conn, false, null); // clsDocument.reloadAnswers(conn);
	}
	
	public ObjectNode validateForNewDocument(Connection conn) throws SQLException {
		ObjectMapper mapper = new ObjectMapper();
		ObjectNode result = mapper.createObjectNode();
		
		if (CommonUtils.isEmpty(this.documentType)) {
			result.put(ATTR_DOCUMENT_TYPE, REQUIRED);
		} else if (!(DocTypeRefRecord.DOC_TYPE_CD_SOLICITATION.equals(this.documentType) ||
				DocTypeRefRecord.DOC_TYPE_CD_AWARD.equals(this.documentType) ||
				DocTypeRefRecord.DOC_TYPE_CD_ORDER.equals(this.documentType))) {
			result.put(ATTR_DOCUMENT_TYPE, "must be either '" + DocTypeRefRecord.DOC_TYPE_CD_SOLICITATION
					+ "', '" + DocTypeRefRecord.DOC_TYPE_CD_SOLICITATION
					+ "', or '" + DocTypeRefRecord.DOC_TYPE_CD_ORDER
					+ "'");
		}
		if (CommonUtils.isEmpty(this.acquisitionTitle)) {
			result.put(ATTR_ACQUISITION_TITLE, REQUIRED);
		} else if (this.acquisitionTitle.length() > 100) {
			result.put(ATTR_ACQUISITION_TITLE, "exceeds maximum size of 100");
		} else if (this.acquisitionTitle.contains(APPIAN_CODE1)) { // CJ-1230
			result.put(ATTR_ACQUISITION_TITLE, "invalid");
		}
		//ATTR_ACQUISITION_TITLE: "test-document ML-090490{{$randomInt}}",
		//"document_number": "09052015 ML-{{$randomInt}}",+
	
		if (CommonUtils.isNotEmpty(this.documentNumber)) {
			if (this.documentNumber.length() > 50) {
				result.put(ATTR_DOCUMENT_NUMBER, "exceeds maximum size of 50");
			} else if (this.documentNumber.contains(APPIAN_CODE1)) { // CJ-1230
				result.put(ATTR_DOCUMENT_NUMBER, "invalid");
			} else {
				DocumentRecord oRecord = DocumentTable.find(this.applicationId, this.documentNumber, null);
				if (oRecord != null)
					result.put(ATTR_DOCUMENT_NUMBER, "has already been taken");
			}
		} else {
			result.put(ATTR_DOCUMENT_NUMBER, REQUIRED);
		}

		if (CommonUtils.isNotEmpty(this.contractNumber) && (this.contractNumber.length() > 17)) {
			result.put(ATTR_CONTRACT_NUMBER, "exceeds maximum size of 17");
		}
		if (CommonUtils.isNotEmpty(this.orderNumber) && (this.orderNumber.length() > 255)) {
			result.put(ATTR_ORDER_NUMBER, "exceeds maximum size of 255");
		}
		if (CommonUtils.isNotEmpty(this.fullName) && (this.fullName.length() > 100)) {
			result.put(ATTR_FULL_NAME, "exceeds maximum size of 100");
		}
		
		// CJ-1049
		if (this.bCopyDoc) {
			if (!(DocTypeRefRecord.DOC_TYPE_CD_SOLICITATION.equals(this.documentType) ||
				DocTypeRefRecord.DOC_TYPE_CD_AWARD.equals(this.documentType)))
			result.put(ATTR_COPY_TEMPLATE, "can only be used to copy solicitations or awards");
			
			if (this.solicitationId == null) 
				result.put(ATTR_COPY_TEMPLATE, "requires solicitation id");
		}
		
		//if (!DocTypeRefRecord.DOC_TYPE_CD_AWARD.equals(this.documentType)) {
		if ((DocTypeRefRecord.DOC_TYPE_CD_ORDER.equals(this.documentType)) 
		|| ((DocTypeRefRecord.DOC_TYPE_CD_SOLICITATION.equals(this.documentType) && !this.bCopyDoc))) {
			if (this.solicitationId != null)
				result.put(DocumentRecord.FIELD_SOLICITATION_ID, "Not applicable for '" + this.documentType + "' document type");
			if (this.clauseVersionId != null)
				result.put(DocumentRecord.FIELD_CLAUSE_VERSION_ID, "Not applicable for '" + this.documentType + "' document type");
		} else {
			if (this.solicitationId != null) {
				this.solClsDocument = new ClsDocument();
				if (!solClsDocument.loadDocument(this.solicitationId, conn, true)) {
					result.put(DocumentRecord.FIELD_SOLICITATION_ID, "Invalid number");
					this.solClsDocument = null;
				} else {
					if (this.bCopyDoc) {	// CJ-1049
						if (!solClsDocument.getInterviewComplete()) 
							result.put(DocumentRecord.FIELD_SOLICITATION_ID, "Specified Solicitation document needs to be finalized to use as a copy template.");
					}
					else {
						if (!DocTypeRefRecord.DOC_TYPE_CD_SOLICITATION.equals(solClsDocument.getDocumentType())) {
							result.put(DocumentRecord.FIELD_SOLICITATION_ID, "Specified document needs to be a Solicitation document to create a linked Award document");
						}
						else if (!solClsDocument.getInterviewComplete()) {
							result.put(DocumentRecord.FIELD_SOLICITATION_ID, "Specified Solicitation document needs to be finalized for its linked Award document");
						}
					}
					
					ClauseVersionTable.getInstance(conn);
					Integer activeClsVersionId = ClauseVersionTable.getActiveVersionId();
					
					if (this.clauseVersionId == null) {
						this.clauseVersionId = this.solClsDocument.getClauseVersionId();
					}
					// CJ-595
					//else if (this.clauseVersionId < this.solClsDocument.getClauseVersionId()) 
					if ((this.clauseVersionId != this.solClsDocument.getClauseVersionId())
						&& (this.clauseVersionId != activeClsVersionId))  {
						result.put(DocumentRecord.FIELD_CLAUSE_VERSION_ID, "Must be the same as the specified Solicitation document or the current CLS version");
					}
				}
			}
		}
		
		return ((result.size() == 0) ? null : result);	
	}
	
	public ObjectNode createDocument(Connection conn, boolean bVer1) throws Exception {
		
		DocumentRecord oRecord = DocumentTable.create(this.applicationId, this.documentType, this.acquisitionTitle,
				this.documentNumber, this.contractNumber, this.orderNumber, this.fullName, 
				(this.bCopyDoc) ? null : this.solicitationId,	// CJ-1049
				this.clauseVersionId, this.documentDate, conn);
		int iDocid = oRecord.getId().intValue();

		ClsDocument clsDocument = new ClsDocument();
		if (!clsDocument.loadDocument(iDocid, conn, false))
			return null;
		
		if (this.solClsDocument != null) {
			if (this.bCopyDoc)
				clsDocument.populateDocFromTemplate(this.solClsDocument, conn);
			else
				clsDocument.populateAwardFromSolicitation(this.solClsDocument, conn);
			if (!clsDocument.loadDocument(iDocid, conn, false))
				return null;
		}
		
		return clsDocument.getApiJson(bVer1, conn); //  ClsDocument.getRecordApiJson(id.intValue());
	}
	
	public ObjectNode deleteDocument(DocumentRecord oRecord, Connection conn) throws Exception {
		oRecord.deleteDocument(null, conn);

		ObjectMapper mapper = new ObjectMapper();
		ObjectNode result = mapper.createObjectNode();
		result.put(ATTR_STATUS, "document deleted");
		return result;
	}
	
	public ObjectNode updateDocument(DocumentRecord oRecord, boolean bVer1) throws Exception {
		ObjectMapper mapper = new ObjectMapper();
		ObjectNode result = mapper.createObjectNode();
		
		if (CommonUtils.isNotEmpty(this.acquisitionTitle) && (this.acquisitionTitle.length() > 100)) {
			result.put(ATTR_ACQUISITION_TITLE, "exceeds maximum size of 100");
		}
		if (CommonUtils.isNotEmpty(this.documentNumber) && (this.documentNumber.length() > 50)) {
			result.put(ATTR_DOCUMENT_NUMBER, "exceeds maximum size of 50");
		}
		if (CommonUtils.isNotEmpty(this.contractNumber) && (this.contractNumber.length() > 17)) {
			result.put(ATTR_CONTRACT_NUMBER, "exceeds maximum size of 17");
		}
		if (CommonUtils.isNotEmpty(this.orderNumber) && (this.orderNumber.length() > 255)) {
			result.put(ATTR_ORDER_NUMBER, "exceeds maximum size of 255");
		}
		if (CommonUtils.isNotEmpty(this.fullName) && (this.fullName.length() > 100)) {
			result.put(ATTR_FULL_NAME, "exceeds maximum size of 100");
		}
		
		if (this.docStatusCd != null) {
			if (DocStatusRefRecord.CD_IN_PROGRESS.equals(this.docStatusCd)) {
				if (this.interviewComplete != null) {
					if (this.interviewComplete.booleanValue()) {
						result.put(DocumentRecord.FIELD_DOCUMENT_STATUS, "not match with interview_complete value");
					}
				} else
					this.interviewComplete = Boolean.FALSE;
			} else if (DocStatusRefRecord.CD_FINALIZED.equals(this.docStatusCd)) {
				if (this.interviewComplete != null) {
					if (!this.interviewComplete.booleanValue()) {
						result.put(DocumentRecord.FIELD_DOCUMENT_STATUS, "not match with interview_complete value");
					}
				} else
					this.interviewComplete = Boolean.TRUE;
			} else {
				result.put(DocumentRecord.FIELD_DOCUMENT_STATUS, "value must be either '" +
						DocStatusRefRecord.CD_IN_PROGRESS + "' for 'In progress' or '" +
						DocStatusRefRecord.CD_FINALIZED + "' for 'Finalized'" );
			}
		}

		if (result.size() > 0)
			return result;
		
		Connection conn = null;
		try {
			conn = ClsConfig.getDbConnection();
			//AuditEventRecord auditEventRecord = new AuditEventRecord(AuditEventRecord.ACTION_DOCUMENT_UPDATED, null, null);
			AuditEventRecord auditEventRecord = new AuditEventRecord(AuditEventRecord.ACTION_ORGANIZATION_DOC_UPDATED, null, null);
			auditEventRecord.addAttribute(AuditEventRecord.FIELD_APPLICATION_ID, this.applicationId);
			
			SqlUpdate sqlUpdate = new SqlUpdate(null, DocumentRecord.TABLE_DOCUMENTS);
			
			if ((this.acquisitionTitle != null) && CommonUtils.isNotSame(oRecord.getAcquisitionTitle(), this.acquisitionTitle))
				sqlUpdate.addString(DocumentRecord.FIELD_ACQUISITION_TITLE, this.acquisitionTitle, auditEventRecord);

			if ((this.documentNumber != null) && CommonUtils.isNotSame(oRecord.getDocumentNumber(), this.documentNumber))
				sqlUpdate.addString(DocumentRecord.FIELD_DOCUMENT_NUMBER, this.documentNumber, auditEventRecord);
			
			if ((this.interviewComplete != null) && CommonUtils.isNotSame(oRecord.getInterviewComplete(), this.interviewComplete)) {
				if (this.interviewComplete)
					sqlUpdate.addString(DocumentRecord.FIELD_DOCUMENT_STATUS, DocStatusRefRecord.CD_FINALIZED, auditEventRecord);
				else
					sqlUpdate.addString(DocumentRecord.FIELD_DOCUMENT_STATUS, DocStatusRefRecord.CD_IN_PROGRESS, auditEventRecord);
				// sqlUpdate.addBoolean(DocumentRecord.FIELD_IS_INTERVIEW_COMPLETE, this.interviewComplete, auditEventRecord);
			}
			
			if ((this.contractNumber != null) && CommonUtils.isNotSame(oRecord.getContractNumber(), this.contractNumber))
				sqlUpdate.addString(DocumentRecord.FIELD_CONTRACT_NUMBER, this.contractNumber, auditEventRecord);
			
			if ((this.orderNumber != null) && CommonUtils.isNotSame(oRecord.getOrderNumber(), this.orderNumber))
				sqlUpdate.addString(DocumentRecord.FIELD_ORDER_NUMBER, this.orderNumber, auditEventRecord);
			
			if ((this.fullName != null) && CommonUtils.isNotSame(oRecord.getFullName(), this.fullName))
				sqlUpdate.addString(DocumentRecord.FIELD_FULL_NAME, this.fullName, auditEventRecord);
			
			if (sqlUpdate.size() == 0) {
				result.put(ATTR_STATUS, "no changes found");
				return result;
			}
			
			oRecord.setUpdatedAt(CommonUtils.getNow());
			sqlUpdate.addString(DocumentRecord.FIELD_UPDATED_AT, oRecord.getUpdatedAt(), auditEventRecord);
			sqlUpdate.addCriteria(DocumentRecord.FIELD_DOCUMENT_ID, oRecord.getId(), java.sql.Types.INTEGER, auditEventRecord);
			sqlUpdate.execute(conn);
	
			oRecord = DocumentTable.get(oRecord, oRecord.getId(), conn);
			oRecord.populateJsonForApi(result, bVer1, conn);
			
			try {
				auditEventRecord.saveNew(conn);
			}
			catch (Exception oIgnore) {
				oIgnore.printStackTrace();
			}
		}
		finally {
			SqlUtil.closeInstance(conn);
		}
		return ((result.size() == 0) ? null : result);	
	}
	
	// ----------------------------------------------
	public String getDocumentType() {
		return documentType;
	}
	public void setDocumentType(String documentType) {
		this.documentType = documentType;
	}
	public String getAcquisitionTitle() {
		return acquisitionTitle;
	}
	public void setAcquisitionTitle(String acquisitionTitle) {
		this.acquisitionTitle = acquisitionTitle;
	}
	public String getDocumentNumber() {
		return documentNumber;
	}
	public void setDocumentNumber(String documentNumber) {
		this.documentNumber = documentNumber;
	}
	public String getContractNumber() {
		return contractNumber;
	}
	public void setContractNumber(String contractNumber) {
		this.contractNumber = contractNumber;
	}
	public String getOrderNumber() {
		return orderNumber;
	}
	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}
	public String getFullName() {
		return fullName;
	}
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public Boolean getInterviewComplete() {
		return interviewComplete;
	}

	public void setInterviewComplete(Boolean interviewComplete) {
		this.interviewComplete = interviewComplete;
	}

	public DocumentRecord getoRecord() {
		return oRecord;
	}

	public void setoRecord(DocumentRecord oRecord) {
		this.oRecord = oRecord;
	}
	
	public boolean hasPostedAnswers() {
		return !this.alAnswers.isEmpty();
	}
}
