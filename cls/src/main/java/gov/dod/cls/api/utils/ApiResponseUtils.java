package gov.dod.cls.api.utils;

import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.Response;

public final class ApiResponseUtils {

	public static Response build(String responseContent) {
		return Response.status(HttpServletResponse.SC_OK).entity(responseContent).build();
	}
	
}
