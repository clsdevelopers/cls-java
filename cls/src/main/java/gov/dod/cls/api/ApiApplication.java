package gov.dod.cls.api;

import gov.dod.cls.api.endpoints.ClausesEndpoint;
import gov.dod.cls.api.endpoints.ClsVersionEndPoint;
import gov.dod.cls.api.endpoints.DocumentEndpoint;
import gov.dod.cls.api.endpoints.InterviewEndpoint;
import gov.dod.cls.api.endpoints.TokenEndpoint;

import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.core.Application;

public class ApiApplication extends Application {

	public Set<Class<?>> getClasses() {
        Set<Class<?>> s = new HashSet<Class<?>>();
        s.add(TokenEndpoint.class);
        s.add(ClausesEndpoint.class);
        s.add(DocumentEndpoint.class);
        s.add(InterviewEndpoint.class);
        s.add(ClsVersionEndPoint.class);
        return s;
    }
	
}
