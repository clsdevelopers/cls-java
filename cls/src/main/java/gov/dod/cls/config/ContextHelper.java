package gov.dod.cls.config;

import javax.servlet.ServletContext;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.web.context.ServletContextAware;
import org.springframework.web.context.support.WebApplicationContextUtils;

public class ContextHelper implements ServletContextAware {

	private ApplicationContext applicationContext;
	private static ContextHelper instance;
	
	private ContextHelper(){}
	
	public static synchronized ContextHelper getInstance() {		
		if (ContextHelper.instance == null) {
			ContextHelper.instance = new ContextHelper();			
		}		      
		return ContextHelper.instance;
	}
	
	public void setServletContext(ServletContext servletContext) {		
		this.applicationContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);				
	}

	public ApplicationContext getApplicationContext() {
		if (this.applicationContext == null) {
			String springFile = "classpath:./clsServiceApplicationContext.xml";			
			this.applicationContext = new ClassPathXmlApplicationContext(springFile);
		}		
		return this.applicationContext;
	}

	@SuppressWarnings("unchecked")
	public <T> T getSpringBean(Class<T> clazz, String name){
		T bean;
		bean = (T) getInstance().getApplicationContext().getBean(name);		
		return bean;
	}
}
