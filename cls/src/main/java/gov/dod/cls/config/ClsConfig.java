package gov.dod.cls.config;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Calendar;

import javax.servlet.http.HttpSession;

import gov.dod.cls.filter.SessionCryptor;
import gov.dod.cls.utils.CommonUtils;

public class ClsConfig {

	public synchronized static ClsConfig getInstance() {
		ClsConfig clsConfig = ContextHelper.getInstance().getSpringBean(ClsConfig.class, "clsConfig");
		return clsConfig;
	}
	
	public static Connection getDbConnection() {
		ClsConfig clsConfig = ClsConfig.getInstance();
		return clsConfig.getConnection();
	}
	
	// ====================================================================
	private final static String MYSQL_DRIVER = "com.mysql.jdbc.Driver";
	
	// ====================================================================
	private String host = null;
	private String user = null;
	private String password = null;
	private String schema = null;
	private String clsUrl = null;
	private String cryptorKey = null;
	private String expireHours = null;
	private Integer sessionTimeout = null; // session.timeout
	private Integer tokenInterviewTimeout = null; // token.interview.timeout
	
	private boolean jdbcDriverLoaded = false;
	
	public ClsConfig() {
		super();
		this.loadJdbcDriver();
	}
	
	public boolean loadJdbcDriver() {
		if (!this.jdbcDriverLoaded)
			try { // The newInstance() call is a work around for some broken Java implementations
	            Class.forName(MYSQL_DRIVER).newInstance();
	            this.jdbcDriverLoaded = true;
	        } catch (Exception ex) {
			    System.out.println("SQLException: " + ex.getMessage());
	        	ex.printStackTrace();
	        	this.jdbcDriverLoaded = false;
	        }
		return this.jdbcDriverLoaded;
	}
	
	public Connection getConnection() {
		Connection conn = null;
		if (CommonUtils.isEmpty(this.host) || CommonUtils.isEmpty(this.user) ||
			CommonUtils.isEmpty(this.password) || CommonUtils.isEmpty(this.schema)) {
			return null;
		}
		
		// http://www.mkyong.com/jdbc/how-to-connect-to-mysql-with-jdbc-driver-java/
		// .getConnection("jdbc:mysql://localhost:3306/mkyongcom","root", "password");
		try {
			this.loadJdbcDriver();
			
			String sUrl = "jdbc:mysql://" + this.host + ":3306/" + this.schema;
			conn = DriverManager.getConnection(sUrl, this.user, this.password);

		} catch (SQLException ex) {
		    System.out.println("SQLException: " + ex.getMessage());
		    System.out.println("SQLState: " + ex.getSQLState());
		    System.out.println("VendorError: " + ex.getErrorCode());
		    return null;
		}
		catch (Exception oError) {
			oError.printStackTrace();
			return null;
		}
		return conn;
	}
	
	public String getHost() {
		return this.host;
	}
	public void setHost(String host) {
		this.host = host;
	}

	public String getUser() {
		return this.user;
	}
	public void setUser(String user) {
		this.user = user;
	}

	public String getPassword() {
		return this.password;
	}
	public void setPassword(String password) {
		this.password = password;
	}

	public String getSchema() {
		return this.schema;
	}
	public void setSchema(String schema) {
		this.schema = schema;
	}

	public String getLaunchOrdersUrlPrefix() {
		return ((this.clsUrl == null) ? "" : this.clsUrl) + "account/final_clauses.jsp?token=";
	}
	
	public String getLaunchInterviewUrlPrefix() {
		return ((this.clsUrl == null) ? "" : this.clsUrl) + "account/interview.jsp?token=";
	}

	public String getClsUrl() {
		return this.clsUrl;
	}
	public void setClsUrl(String clsUrl) {
		if ((clsUrl != null) && (!clsUrl.endsWith("/")))
			clsUrl += "/";
		this.clsUrl = clsUrl;
	}
	
	public String getAppPath() {
		if (this.clsUrl == null)
			return "";
		int iPos = this.clsUrl.indexOf("://");
		if (iPos > 0) {
			String sAppPath = this.clsUrl.substring(iPos + "://".length());
			iPos = sAppPath.indexOf('/');
			if (iPos > 0) {
				sAppPath = sAppPath.substring(iPos);
				return sAppPath;
			}
		}
		return this.clsUrl;
	}

	public String getCryptorKey() {
		return cryptorKey;
	}
	public void setCryptorKey(String cryptorKey) {
		this.cryptorKey = cryptorKey;
		SessionCryptor.setKey(this.cryptorKey);
	}
	
	public String getExpireHours() {
		return this.expireHours;
	}
	public void setExpireHours(String expireHours) {
		this.expireHours = expireHours;
	}

	public Integer getSessionTimeout() {
		return (this.sessionTimeout == null) ? 120 : this.sessionTimeout;
	}
	public void setSessionTimeout(Integer sessionTimeout) {
		this.sessionTimeout = sessionTimeout;
	}
	
	public boolean isSessionExpired(long lLastAccessed) {
		Calendar oExpiresAt = Calendar.getInstance();
		oExpiresAt.setTimeInMillis(lLastAccessed);
		oExpiresAt.add(Calendar.MINUTE, this.getSessionTimeout());
		Calendar calNow = Calendar.getInstance();
		return (calNow.after(oExpiresAt));
	}

	public Integer getTokenInterviewTimeout() {
		return (this.tokenInterviewTimeout == null) ? 120 : this.tokenInterviewTimeout;
	}
	public void setTokenInterviewTimeout(Integer tokenInterviewTimeout) {
		this.tokenInterviewTimeout = tokenInterviewTimeout;
	}
	
	public Integer getTimeout(boolean isTokenInterview) {
		return (isTokenInterview ? this.getTokenInterviewTimeout() : this.getSessionTimeout());
	}
	
	public void setSessionTimeout(HttpSession session, boolean isTokenInterview) {
		Integer iTimeout = this.getTimeout(isTokenInterview) * 60;
		if (session.getMaxInactiveInterval() != iTimeout) {
        	session.setMaxInactiveInterval(iTimeout);
        }
	}
	
	public Calendar getTokenExpiresIn() {
		Calendar calEnd = Calendar.getInstance();
		calEnd.add(Calendar.MINUTE, this.getTokenInterviewTimeout());
		return calEnd;
	}

}
