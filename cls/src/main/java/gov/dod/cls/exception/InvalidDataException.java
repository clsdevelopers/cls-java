package gov.dod.cls.exception;

/**
 * The <code>InvalidDataException</code> is designed to handle all business exception that can be recovered
 * 
 * @author jzhang
 *
 */
public class InvalidDataException extends CLSBusinessException
{

	/**
	 * 
	 */
	private static final long serialVersionUID = -4623146975716838199L;
	
	/**
     * Constructs a new exception with {@code null} as its detail message  
     */
	public InvalidDataException()
	{
		super();
	}

	 /**
     * Constructs a new exception with the specified detail message.  
     *
     * @param   msg   the detail message. 
     */
	public InvalidDataException(String msg)
	{
		super(msg);
	}

	/**
     * Constructs a new exception with the specified detail message and
     * cause.  
     *
     * @param  msg the detail message 
     * @param  cause the cause 
     */
	public InvalidDataException(String msg, Throwable cause)
	{
		super(msg, cause);
	}

	/**
     * Constructs a new exception with cause.  
     *  
     * @param  cause the cause 
     */
	public InvalidDataException(Throwable cause)
	{
		super(cause);
	}
}
