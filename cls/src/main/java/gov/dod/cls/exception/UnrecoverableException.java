package gov.dod.cls.exception;

/**
 * The <code>UnrecoverableException</code> is designed to handle the fatal error that is unrecoverable error 
 * sunch as db connection can be established etc.
 */
public class UnrecoverableException extends Exception
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 798418758451596951L;

	/**
     * Constructs a new exception with {@code null} as its detail message  
     */
	public UnrecoverableException()
	{
		super();
	}
	
	 /**
     * Constructs a new exception with the specified detail message.  
     *
     * @param   msg   the detail message. 
     */
	public UnrecoverableException(String message)
	{
		super(message);
	}

	/**
     * Constructs a new exception with the specified detail message and
     * cause.  
     *
     * @param  msg the detail message 
     * @param  cause the cause 
     */
	public UnrecoverableException(String message, Throwable cause)
	{
		super(message, cause);
	}

	/**
     * Constructs a new exception with cause.  
     *  
     * @param  cause the cause 
     */
	public UnrecoverableException(Throwable cause)
	{
		super(cause);
	}

}
