package gov.dod.cls.exception;

/**
 * The <code>NotAllowedException</code> is designed to be thrown when the operation is not allowed to performed 
 * 
 * @author jzhang
 *
 */
public class NotAllowedException extends CLSBusinessException
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 7315779583729091993L;

	/**
     * Constructs a new exception with {@code null} as its detail message  
     */
	public NotAllowedException()
	{
		super();
	}

	 /**
     * Constructs a new exception with the specified detail message.  
     *
     * @param   msg   the detail message. 
     */
	public NotAllowedException(String msg)
	{
		super(msg);
	}

	/**
     * Constructs a new exception with the specified detail message and
     * cause.  
     *
     * @param  msg the detail message 
     * @param  cause the cause 
     */
	public NotAllowedException(String msg, Throwable cause)
	{
		super(msg, cause);
	}

	/**
     * Constructs a new exception with cause.  
     *  
     * @param  cause the cause 
     */
	public NotAllowedException(Throwable cause)
	{
		super(cause);
	}
}

