package gov.dod.cls.exception;
/**
 * The <code>CLSBusinessException</code> will be used for CLS exception
 * @author jzhang
 *
 */
public class CLSBusinessException extends Exception
{

	/**
	 * 
	 */
	private static final long serialVersionUID = -7857583441641216988L;

	 /**
     * Constructs a new exception with {@code null} as its detail message  
     */
	public CLSBusinessException()
	{
		super();
	}

    /**
     * Constructs a new exception with the specified detail message.  
     *
     * @param   msg   the detail message. 
     */
	public CLSBusinessException(String msg)
	{
		super(msg);
	}

	/**
     * Constructs a new exception with the specified detail message and
     * cause.  
     *
     * @param  msg the detail message 
     * @param  cause the cause 
     */
	public CLSBusinessException(String msg, Throwable cause)
	{
		super(msg, cause);
	}

	/**
     * Constructs a new exception with cause.  
     *  
     * @param  cause the cause 
     */
	public CLSBusinessException(Throwable cause)
	{
		super(cause);
	}
}
