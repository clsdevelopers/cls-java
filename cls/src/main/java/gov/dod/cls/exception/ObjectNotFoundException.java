package gov.dod.cls.exception;

/**
 * The <code>ObjectNotFoundException</code> is designed to be thrown when the object is not found
 * 
 * @author jzhang
 *
 */
public class ObjectNotFoundException extends CLSBusinessException
{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 6968652172131607055L;

	/**
     * Constructs a new exception with {@code null} as its detail message  
     */
	public ObjectNotFoundException()
	{
		super();
	}

	 /**
     * Constructs a new exception with the specified detail message.  
     *
     * @param   msg   the detail message. 
     */
	public ObjectNotFoundException(String msg)
	{
		super(msg);
	}

	/**
     * Constructs a new exception with the specified detail message and
     * cause.  
     *
     * @param  msg the detail message 
     * @param  cause the cause 
     */
	public ObjectNotFoundException(String msg, Throwable cause)
	{
		super(msg, cause);
	}

	/**
     * Constructs a new exception with cause.  
     *  
     * @param  cause the cause 
     */
	public ObjectNotFoundException(Throwable cause)
	{
		super(cause);
	}
}
