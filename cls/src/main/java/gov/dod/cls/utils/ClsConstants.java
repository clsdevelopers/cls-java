package gov.dod.cls.utils;

public final class ClsConstants {

	public final static String RESPONSE_FORMAT_JSON = "application/json";
	public final static String RESPONSE_FORMAT_TEXT = "text/plain";

	public enum OutputType { // CJ-1105
		UI, XML, HTML, PDF, DOC
	}
	
	public final static String HTML_BR = "<br />";

}
