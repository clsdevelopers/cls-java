package gov.dod.cls.utils;

import javax.script.Invocable;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
/**
 * This utility is defined to handle the encoding to match the JavaScript encoding
 * @author jzhang
 *
 */
public abstract class URLEncodeUtils
{
	/**
	 * Get the javascript encoding 
	 * @param encodeValue the value need to be encoded
	 * @return the encoding match the javascript encoding/decoding
	 */
	public static String encodeURIComponent(String encodeValue)
	{
		ScriptEngineManager factory = new ScriptEngineManager();
		ScriptEngine engine = factory.getEngineByName("JavaScript");

		try
		{
			return (String) ((Invocable) engine).invokeFunction("encodeURIComponent", encodeValue);

		}
		catch (NoSuchMethodException | ScriptException e)
		{
			return encodeValue;
		}

	}
}
