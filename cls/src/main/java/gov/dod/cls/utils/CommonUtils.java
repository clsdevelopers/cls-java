package gov.dod.cls.utils;

import java.io.ByteArrayInputStream;
import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateExpiredException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.codec.binary.Hex;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.node.NullNode;

public final class CommonUtils {

	public final static String DATE_FORMAT = "MM/dd/yyyy";
	public final static String DATE_FORMAL_FORMAT = "MMMMM d, yyyy";
	public final static String DATETIME_FORMAT = "MM/dd/yyyy HH:mm:ss";
	public final static String DATETIME_BRIEF_FORMAT = "MM/dd/yyyy HH:mm";
	public final static String DATETIME_12_HOUR_FORMAT = "MM/dd/yyyy hh:mm a";

	public final static SimpleDateFormat _SimpleDateFormat = new SimpleDateFormat(CommonUtils.DATE_FORMAT);
	public final static SimpleDateFormat _SimpleDateFormatFormat = new SimpleDateFormat(CommonUtils.DATE_FORMAL_FORMAT);
	public final static SimpleDateFormat _MonthYearDateFormat = new SimpleDateFormat("MMM yyyy");
	public final static SimpleDateFormat _YearMonthDateFormat = new SimpleDateFormat("yyyy-MM");
	public final static SimpleDateFormat _YearMonthDayDateFormat = new SimpleDateFormat("yyyy-MM-dd");
	public final static SimpleDateFormat _ZuluFormat = new java.text.SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
	
	
	private static final String EMAIL_ERGEX = 
			"^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
			+ "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
	public final static Pattern EMIAL_PATTERN = Pattern.compile(EMAIL_ERGEX);


	public final static boolean isValidEmmail(String emailValidated)
	{	  
		if (isEmpty(emailValidated))
		{
			return false;
		}
        Matcher matcher = EMIAL_PATTERN.matcher(emailValidated);
       return matcher.matches();
       
    }
  
   
	
	public final static boolean isEmpty(String value) {
		return (value == null) || value.isEmpty();
	}

	public final static boolean isNotEmpty(String value) {
		return ! CommonUtils.isEmpty(value);
	}
	
	public final static String iif(String value, String defaultValue) {
		return (CommonUtils.isEmpty(value) ? defaultValue : value);
	}
	
	public final static String iif(String value) {
		return CommonUtils.iif(value, "");
	}
	
	public static Date getNow() {
		Calendar calStart = Calendar.getInstance();
		return calStart.getTime();
	}
	
	public static boolean isSame(String source, String target, boolean ignoreCase) {
		if ((source == null) || source.isEmpty()) {
			return (target == null) || target.isEmpty();
		} else if ((target == null) || target.isEmpty())
			return false;
		if (ignoreCase)
			return source.equalsIgnoreCase(target);
		return source.equals(target);
	}
	
	public static boolean isSame(String source, String target) {
		return CommonUtils.isSame(source, target, false);
	}

	public static boolean isNotSame(String source, String target, boolean ignoreCase) {
		return ! CommonUtils.isSame(source, target, ignoreCase);
	}

	public static boolean isNotSame(String source, String target) {
		return ! CommonUtils.isSame(source, target, false);
	}
	
	public static boolean isSame(Object source, Object target) {
		if (source == null) {
			return (target == null);
		}
		return source.equals(target);
	}
	
	public static boolean isNotSame(Object source, Object target) {
		return ! CommonUtils.isSame(source, target);
	}
	
	// -----------------------------------------------------------------
	public static Integer toInteger(String value) {
		if (CommonUtils.isEmpty(value))
			return null;
		try {
			return Integer.parseInt(value);
		}
		catch (Exception oError) {
			return null;
		}
	}

	public static Integer toInteger(Object value) {
		if (value == null)
			return null;
		try {
			if (value instanceof Integer)
				return (Integer)value;
			return Integer.parseInt(value.toString());
		}
		catch (Exception oError) {
			return null;
		}
	}

	public static Boolean toBoolean(String value) {
		if (CommonUtils.isEmpty(value))
			return null;
		try {
			return Boolean.parseBoolean(value);
		}
		catch (Exception oError) {
			return null;
		}
	}

	public static Boolean toBoolean(Object value) {
		if (value == null)
			return null;
		try {
			if (value instanceof Boolean)
				return (Boolean)value;
			if (value instanceof Byte)
				return (((Byte)value) == 1);
			if (value instanceof Integer)
				return (((Integer)value) == 1);
			return Boolean.parseBoolean(value.toString());
		}
		catch (Exception oError) {
			return null;
		}
	}

	public static Long toJsonDate(Date value) {
		if (value == null)
			return null;
		return value.getTime();
	}
	
	// ----------------------------------------------------------
	public final static String toString(Date pDate) {
		return CommonUtils.toString(pDate, null);
	}
	
	public final static String toMonthYear(Date pDate) {
		if (pDate == null)
			return "";
		return CommonUtils._MonthYearDateFormat.format(pDate);
	}
	
	public final static String toYearMonth(Date pDate) {
		if (pDate == null)
			return "";
		return CommonUtils._YearMonthDateFormat.format(pDate);
	}
	
	public final static String toYearMonthDay(Date pDate) {
		if (pDate == null)
			return "";
		return CommonUtils._YearMonthDayDateFormat.format(pDate);
	}
	
	public final static String toYearMonthDay(Object pDate) {
		if (pDate == null)
			return "";
		return CommonUtils._YearMonthDayDateFormat.format(pDate);
	}
	
	/*
	public final static String toApi(String value) {
		return (value == null) ? "" : value;
	}
	*/

	public final static String toZulu(Date pDate) {
		if (pDate == null)
			return null; // "";
		_ZuluFormat.setTimeZone(java.util.TimeZone.getTimeZone("Zulu")); // explicitly set timezone of input if needed
		return _ZuluFormat.format(pDate);
	}
	
	public final static String toString(Date pDate, String psNull) {
		if (pDate == null) {
			return (psNull == null ? "" : psNull);
		} else {
			return CommonUtils._SimpleDateFormat.format(pDate);
		}
	}
	
	public final static String toFormalString(Date pDate) {
		if (pDate == null) {
			return "";
		} else {
			return CommonUtils._SimpleDateFormatFormat.format(pDate);
		}
	}
	
	// ----------------------------------------------------------
	public final static DecimalFormat FloatFormatter = new DecimalFormat("#,###,###,###,##0.00"); // 12/6/2012 Kwan: Added
	public static String format(Float poValue) { // 12/6/2012 Kwan: Added
		if (poValue == null)
			return "0.00";
	    return FloatFormatter.format(poValue);
	}
	  
	public static String format(Double poValue) { // 12/11/2012 Kwan: Added
		if (poValue == null)
			return "0.00";
	    return FloatFormatter.format(poValue);
	}
	  
	public final static DecimalFormat IntFormatter = new DecimalFormat("#,###,###,###");
	public final static String format(Integer piValue) {
		if (piValue == null)
			return "";
		else
			return IntFormatter.format(piValue.intValue());
	}
		
	public final static String format(int piValue) {
		return IntFormatter.format(piValue);
	}
	
	public final static Date toDateE(Object poValue) throws Exception {
		if (poValue == null)
			return null;

		if (poValue instanceof java.sql.Date)
			return (java.sql.Date)poValue;

		if (poValue instanceof Date)
			return (Date)poValue;
		
		DateFormat dfm = new SimpleDateFormat(CommonUtils.DATETIME_FORMAT);
		try {
			if (poValue instanceof String)
				return dfm.parse(((String)poValue).trim()); // "2007-02-26 20:15:00");
			else
				return dfm.parse(poValue.toString()); // "2007-02-26 20:15:00");
		} catch (Exception oE1) {
			dfm = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			return dfm.parse(poValue.toString());
		}
	}
	
	public final static Date yyyymmjddToDate(String psValue) throws ParseException {
		if (CommonUtils.isEmpty(psValue))
			return null;
		return _YearMonthDayDateFormat.parse(psValue);
	}
	
	public final static Date toDate(Object poValue) {
		try {
			return CommonUtils.toDateE(poValue);
		}
		catch (Exception oError) {
			return null;
		}
	}

	public final static Date toDateTime(ResultSet rs, String field) {
		try {
			Object oValue = rs.getTimestamp(field); 
			return CommonUtils.toDateE(oValue);
		}
		catch (Exception oError) {
			return null;
		}
	}

	public final static String toDateTimeString(Date pDate) {
		return CommonUtils.toDateTimeString(pDate, CommonUtils.DATETIME_FORMAT);
	}

	public final static String toDateTimeBrief(Date pDate) {
		return CommonUtils.toDateTimeString(pDate, CommonUtils.DATETIME_BRIEF_FORMAT);
	}

	public final static String toDateTime12Hour(Date pDate) {
		return CommonUtils.toDateTimeString(pDate, CommonUtils.DATETIME_12_HOUR_FORMAT);
	}
	
	public final static String toDateTimeString(Date pDate, String psFormat) {
		if (pDate == null) {
			return "";
		} else {
			SimpleDateFormat oSimpleDateFormat = new SimpleDateFormat(psFormat);
			return oSimpleDateFormat.format(pDate);
		}
	}
	
	// =====================================================
	public static String asString(JsonNode jsonNode, String fieldName) {
		JsonNode jsonValue = jsonNode.get(fieldName);
		if (jsonValue != null) {
			return jsonValue.getTextValue();
		} else
			return null;
	}
	
	public static Date asDate(JsonNode jsonNode, String fieldName) {
		JsonNode jsonValue = jsonNode.get(fieldName);
		if ((jsonValue != null) && (!(jsonValue instanceof NullNode))) {
			return new Date(jsonValue.asLong());
		} else
			return null;
	}
  
	public static Integer asInteger(JsonNode jsonNode, String fieldName) {
		JsonNode jsonValue = jsonNode.get(fieldName);
		if ((jsonValue != null) && (!(jsonValue instanceof NullNode))) {
			return jsonValue.getIntValue();
		} else
			return null;
	}
	
	public static Boolean asBoolean(JsonNode jsonNode, String fieldName) {
		JsonNode jsonValue = jsonNode.get(fieldName);
		if ((jsonValue != null) && (!(jsonValue instanceof NullNode))) {
			return jsonValue.getBooleanValue();
		} else
			return null;
	}
	
	public static X509Certificate getX509Cert(String header){
    	
    	try{
    		String x509Header = header;
    		x509Header = x509Header.replace("-----BEGIN CERTIFICATE-----", "");
        	x509Header = x509Header.replace("-----END CERTIFICATE-----", "");
        	x509Header = x509Header.replace(" ", "");
        	
        	return isValidCert(x509Header);
        	
    	} catch (IndexOutOfBoundsException e){
    		return null;
    	}
	}
	
	public static String getDN(X509Certificate cert){
		String dn = "";
		if(cert != null){
			String[] temp = cert.getSubjectDN().getName().split(", ");
			
			for(String s : temp){
				
				String[] items = s.split("=");
				
				dn = dn + "," + items[0].toLowerCase() + "=" + items[1];
			}
			
			dn = dn.substring(1);
		}
		
		
		return dn;
	}
	
	public static String getCN(X509Certificate cert){
		String cn = "";
		if(cert != null){
			cn = cert.getSubjectDN().getName().split(", ")[0].split("=")[1];
		}
		
		return cn;
	}
	
	public static String getCacHashFromCert(X509Certificate cert){
		String cacHash = null;
		String rawDN = CommonUtils.getCN(cert);
		MessageDigest messageDigest;
		try {
			messageDigest = MessageDigest.getInstance("SHA-256");
			messageDigest.reset();
			messageDigest.update(rawDN.getBytes(Charset.forName("UTF8")));
			byte[] resultByte = messageDigest.digest();
			cacHash = new String(Hex.encodeHex(resultByte));
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return cacHash;
	}
	
	
	private static X509Certificate isValidCert(String x509Header){
    	
    	try {
			CertificateFactory cf = CertificateFactory.getInstance("X.509");
			
			byte[] bencoded = javax.xml.bind.DatatypeConverter.parseBase64Binary((String) x509Header);
			ByteArrayInputStream bisb = new ByteArrayInputStream(bencoded);
			X509Certificate cert = (X509Certificate) cf.generateCertificate(bisb);
			
			try{
				cert.checkValidity();
			} catch (CertificateExpiredException e){
				return null;
			}
			
			return cert;
			
			
		} catch (CertificateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
    	
    	return null;
    }
	
	// Map the strings in the interface with the standard java timezones.
	public static String mapTimezone(String userZone) {
		String stdZone = "";
		
		// all items below Tbilisi have been validated. Most country/cities map correctly. 
		// However, the GMT offsets may need DST.
		
		if (userZone.equals("Eastern Time (US & Canada)")) stdZone = "EST5EDT";
		if (userZone.equals("Central Time (US & Canada)")) stdZone = "CST6CDT";
		if (userZone.equals("Mountain Time (US & Canada)")) stdZone = "MST7MDT";
		if (userZone.equals("Pacific Time (US & Canada)")) stdZone = "PST8PDT";
		if (userZone.equals("Alaska")) stdZone = "US/Alaska";
		if (userZone.equals("Hawaii")) stdZone = "US/Hawaii";
		if (userZone.equals("UTC")) stdZone = "GMT";
		if (userZone.equals("Abu Dhabi")) stdZone = "Etc/GMT-4";
		if (userZone.equals("Adelaide")) stdZone = "Australia/Adelaide";
		if (userZone.equals("Almaty")) stdZone = "Etc/GMT-6";
		if (userZone.equals("American Samoa")) stdZone = "US/Samoa";
		if (userZone.equals("Amsterdam")) stdZone = "Europe/Amsterdam";
		if (userZone.equals("Arizona")) stdZone = "US/Arizona";
		if (userZone.equals("Astana")) stdZone = "Etc/GMT-6";
		if (userZone.equals("Athens")) stdZone = "Europe/Athens";
		if (userZone.equals("Atlantic Time (Canada)")) stdZone = "Canada/Atlantic";
		if (userZone.equals("Auckland")) stdZone = "Pacific/Auckland";
		if (userZone.equals("Azores")) stdZone = "Atlantic/Azores";
		if (userZone.equals("Baghdad")) stdZone = "Asia/Baghdad";
		if (userZone.equals("Baku")) stdZone = "Asia/Baku";
		if (userZone.equals("Bangkok")) stdZone = "Asia/Bangkok";
		if (userZone.equals("Beijing")) stdZone = "CTT";
		if (userZone.equals("Belgrade")) stdZone = "Europe/Belgrade";
		if (userZone.equals("Berlin")) stdZone = "Europe/Berlin";
		if (userZone.equals("Bern")) stdZone = "Europe/Luxembourg";
		if (userZone.equals("Bogota")) stdZone = "America/Bogota";
		if (userZone.equals("Brasilia")) stdZone = "Brazil/East";		
		if (userZone.equals("Bratislava")) stdZone = "Europe/Bratislava";
		if (userZone.equals("Brisbane")) stdZone = "Australia/Brisbane";
		if (userZone.equals("Brussels")) stdZone = "Europe/Brussels";
		if (userZone.equals("Bucharest")) stdZone = "Europe/Bucharest";
		if (userZone.equals("Budapest")) stdZone = "Europe/Budapest";
		if (userZone.equals("Buenos Aires")) stdZone = "America/Argentina/Buenos_Aires";
		if (userZone.equals("Cairo")) stdZone = "Africa/Cairo";
		if (userZone.equals("Canberra")) stdZone = "Australia/Canberra";
		if (userZone.equals("Cape Verde Is.")) stdZone = "Atlantic/Cape_Verde";
		if (userZone.equals("Caracas")) stdZone = " America/Caracas";
		if (userZone.equals("Casablanca")) stdZone = "Africa/Casablanca";
		if (userZone.equals("Chennai")) stdZone = "IST";
		if (userZone.equals("Chihuahua")) stdZone = "America/Chihuahua";
		if (userZone.equals("Chongqing")) stdZone = "Asia/Chongqing";
		if (userZone.equals("Copenhagen")) stdZone = "Europe/Copenhagen";
		if (userZone.equals("Darwin")) stdZone = "Australia/Darwin";
		if (userZone.equals("Dhaka")) stdZone = "Asia/Dhaka";
		if (userZone.equals("Dublin")) stdZone = "Europe/Dublin";
		if (userZone.equals("Edinburgh")) stdZone = "Europe/London";	
		if (userZone.equals("Ekaterinburg")) stdZone = "Asia/Yekaterinburg";
		if (userZone.equals("Fiji")) stdZone = "Pacific/Fiji";
		if (userZone.equals("Guadalajara")) stdZone = "America/Mexico_City"; 
		if (userZone.equals("Guam")) stdZone = "Pacific/Guam";
		if (userZone.equals("Hanoi")) stdZone = "Etc/GMT-7";			
		if (userZone.equals("Harare")) stdZone = "Africa/Harare";
		if (userZone.equals("Helsinki")) stdZone = "Europe/Helsinki";
		if (userZone.equals("Hobart")) stdZone = "Australia/Hobart";
		if (userZone.equals("Hong Kong")) stdZone = "Asia/Hong_Kong";
		if (userZone.equals("Indiana (East)")) stdZone = "US/East-Indiana";
		if (userZone.equals("Irkutsk")) stdZone = "Asia/Irkutsk";
		if (userZone.equals("Islamabad")) stdZone = "Etc/GMT-5";			
		if (userZone.equals("Istanbul")) stdZone = "Europe/Istanbul";
		if (userZone.equals("Jakarta")) stdZone = "Asia/Jakarta";
		if (userZone.equals("Jerusalem")) stdZone = "Asia/Jerusalem";
		if (userZone.equals("Kabul")) stdZone = "Asia/Kabul";
		if (userZone.equals("Kamchatka")) stdZone = "Asia/Kamchatka";
		if (userZone.equals("Karachi")) stdZone = "Asia/Karachi";
		if (userZone.equals("Kathmandu")) stdZone = "";
		if (userZone.equals("Kolkata")) stdZone = "Asia/Kolkata";
		if (userZone.equals("Krasnoyarsk")) stdZone = "Asia/Krasnoyarsk";
		if (userZone.equals("Kuala Lumpur")) stdZone = "Asia/Kuala_Lumpur";
		if (userZone.equals("Kuwait")) stdZone = "Asia/Kuwait";
		if (userZone.equals("Kyiv")) stdZone = "Europe/Kiev";			
		if (userZone.equals("La Paz")) stdZone = "America/La_Paz";
		if (userZone.equals("Lima")) stdZone = "America/Lima";
		if (userZone.equals("Lisbon")) stdZone = "Europe/Lisbon";
		if (userZone.equals("Ljubljana")) stdZone = "Europe/Ljubljana";
		if (userZone.equals("London")) stdZone = "Europe/London";
		if (userZone.equals("Madrid")) stdZone = "Europe/Madrid";
		if (userZone.equals("Magadan")) stdZone = "Asia/Magadan";
		if (userZone.equals("Marshall Is.")) stdZone = "Pacific/Majuro"; 	
		if (userZone.equals("Mazatlan")) stdZone = "America/Mazatlan";
		if (userZone.equals("Melbourne")) stdZone = "Australia/Melbourne";
		if (userZone.equals("Mexico City")) stdZone = "America/Mexico_City";
		if (userZone.equals("Midway Island")) stdZone = "Pacific/Midway";
		if (userZone.equals("Minsk")) stdZone = "Europe/Minsk";
		if (userZone.equals("Monrovia")) stdZone = "Africa/Monrovia";
		if (userZone.equals("Monterrey")) stdZone = "America/Monterrey";
		if (userZone.equals("Moscow")) stdZone = "Europe/Moscow";
		if (userZone.equals("Mumbai")) stdZone = "IST"; 					
		if (userZone.equals("Muscat")) stdZone = "Asia/Muscat";
		if (userZone.equals("Nairobi")) stdZone = "Africa/Nairobi";
		if (userZone.equals("New Delhi")) stdZone = "IST"; 					
		if (userZone.equals("Newfoundland")) stdZone = "Canada/Newfoundland";
		if (userZone.equals("Novosibirsk")) stdZone = "Asia/Novosibirsk";
		if (userZone.equals("Osaka")) stdZone = "JST";	
		if (userZone.equals("Paris")) stdZone = "Europe/Paris";
		if (userZone.equals("Perth")) stdZone = "Australia/Perth";
		if (userZone.equals("Port Moresby")) stdZone = "Pacific/Port_Moresby";
		if (userZone.equals("Prague")) stdZone = "Europe/Prague";
		if (userZone.equals("Pretoria")) stdZone = "Africa/Johannesburg"; 
		if (userZone.equals("Quito")) stdZone = "Etc/GMT-7";	
		if (userZone.equals("Rangoon")) stdZone = "Asia/Rangoon";
		if (userZone.equals("Riga")) stdZone = "Europe/Riga";
		if (userZone.equals("Riyadh")) stdZone = "Asia/Riyadh";
		if (userZone.equals("Rome")) stdZone = "Europe/Rome";
		if (userZone.equals("Samoa")) stdZone = "Pacific/Samoa";
		if (userZone.equals("Santiago")) stdZone = "America/Santiago";
		if (userZone.equals("Sapporo")) stdZone = "JST"; 
		if (userZone.equals("Sarajevo")) stdZone = "Europe/Sarajevo";
		if (userZone.equals("Saskatchewan")) stdZone = "Canada/Saskatchewan";
		if (userZone.equals("Seoul")) stdZone = "Asia/Seoul";
		if (userZone.equals("Singapore")) stdZone = "Singapore";
		if (userZone.equals("Skopje")) stdZone = "Europe/Skopje";
		if (userZone.equals("Sofia")) stdZone = "Europe/Sofia";
		if (userZone.equals("Solomon Is.")) stdZone = "Etc/GMT-11"; 
		if (userZone.equals("Sri Jayawardenepura")) stdZone = "IST";
		if (userZone.equals("St. Petersburg")) stdZone = "America/Indiana/Petersburg";
		if (userZone.equals("Stockholm")) stdZone = "Europe/Stockholm";
		if (userZone.equals("Sydney")) stdZone = "Australia/Sydney";
		if (userZone.equals("Taipei")) stdZone = "Asia/Taipei";
		if (userZone.equals("Tallinn")) stdZone = "Europe/Tallinn";
		if (userZone.equals("Tashkent")) stdZone = "Asia/Tashkent";
		if (userZone.equals("Tbilisi")) stdZone = "Asia/Tbilisi";
		if (userZone.equals("Tehran")) stdZone = "Asia/Tehran";
		if (userZone.equals("Tijuana")) stdZone = "America/Tijuana";
		if (userZone.equals("Tokelau Is.")) stdZone = "Etc/GMT-13";
		if (userZone.equals("Tokyo")) stdZone = "Asia/Tokyo";
		if (userZone.equals("Urumqi")) stdZone = "Etc/GMT-8"; // "Asia/Urumqi";
		if (userZone.equals("Vienna")) stdZone = "Europe/Vienna";
		if (userZone.equals("Vilnius")) stdZone = "Europe/Vilnius";
		if (userZone.equals("Vladivostok")) stdZone = "Asia/Vladivostok";
		if (userZone.equals("Volgograd")) stdZone = "Europe/Volgograd";
		if (userZone.equals("Warsaw")) stdZone = "Europe/Warsaw";
		if (userZone.equals("Yakutsk")) stdZone = "Asia/Yakutsk";
		if (userZone.equals("Yerevan")) stdZone = "Asia/Yerevan";
		if (userZone.equals("Zagreb")) stdZone = "Europe/Zagreb";

		return stdZone;
	}
}
