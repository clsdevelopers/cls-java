package gov.dod.cls.utils.sql;

import gov.dod.cls.db.AuditEventRecord;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.Date;

import org.apache.log4j.Logger;

/**
 * This class provide a simple way to perform SQL update DML statement using parameterized SQL statement
 */
public class SqlUpdate extends ArrayList<SqlParamValue> {

	final static Logger logger = Logger.getLogger(SqlUpdate.class);
	
	private static final long serialVersionUID = 1L;

	private StringBuffer oStringBuffer;
	private int setCount;
	private int criteriaCount;
	
	/**
     * SqlUpdate class constructor
     * @param psSchema
     * @param psTable
     */
	public SqlUpdate(String psSchema, String psTable) {
		this.clear();
		this.addSql("UPDATE " + (psSchema == null ? "" : psSchema + ".") + psTable);
	}
	
	/**
     * Clear properties
     */
	public void clear() {
		super.clear();
		this.oStringBuffer = new StringBuffer();
		this.setCount = 0;
		this.criteriaCount = 0;
	}
	
	/**
     * Add an SQL part
     * @param psSql
     */
	public void addSql(String psSql) {
		this.oStringBuffer.append(psSql);
	}
	
	/*
	public void addSet(String psField, Object poValue) {
		String sSql = ((this.setCount++ == 0) ? " SET " : ", ") + psField + " = ";
		if (poValue == null)
			sSql += "NULL";
		else {
			sSql += "?";
			this.add(new SqlParamValue(poValue, -1));
		}
		this.oStringBuffer.append(sSql);
	}
	*/
	
	/**
     * Add a SQL set
     * @param psField
     * @param poValue
     * @param piDataType	java.sql.Types
     */
	public void addSet(String psField, Object poValue, int piDataType) {
		String sSql = ((this.setCount++ == 0) ? " SET " : ", ") + psField + " = ?";
		this.add(new SqlParamValue(poValue, piDataType));
		this.oStringBuffer.append(sSql);
	}
	
	public void addSet(String psField, Object poValue, int piDataType, AuditEventRecord auditEventRecord) {
		this.addSet(psField, poValue, piDataType);
		auditEventRecord.addAttribute(psField, poValue);
	}

	public void addString(String psField, Object poValue) {
		this.addSet(psField, poValue, java.sql.Types.VARCHAR);
	}

	public void addString(String psField, Object poValue, AuditEventRecord auditEventRecord) {
		this.addString(psField, poValue);
		auditEventRecord.addAttribute(psField, poValue);
	}

	public void addInteger(String psField, Integer poValue) {
		this.addSet(psField, poValue, java.sql.Types.INTEGER);
	}

	public void addInteger(String psField, Integer poValue, AuditEventRecord auditEventRecord) {
		this.addInteger(psField, poValue);
		auditEventRecord.addAttribute(psField, poValue);
	}

	public void addBoolean(String psField, Boolean poValue) {
		this.addSet(psField, poValue, java.sql.Types.BOOLEAN);
	}

	public void addBoolean(String psField, Boolean poValue, AuditEventRecord auditEventRecord) {
		this.addBoolean(psField, poValue);
		auditEventRecord.addAttribute(psField, poValue);
	}

	public void addDate(String psField, Date poValue) {
		this.addSet(psField, poValue, java.sql.Types.DATE);
	}

	public void addDate(String psField, Date poValue, AuditEventRecord auditEventRecord) {
		this.addDate(psField, poValue);
		auditEventRecord.addAttribute(psField, poValue);
	}

	/*
	public void addCriteria(String psField, Object poValue) {
		String sSql = ((this.criteriaCount++ == 0) ? " WHERE " : " AND ") + psField + " = ";
		if (poValue == null)
			sSql += "NULL";
		else {
			sSql += "?";
			this.add(new SqlParamValue(poValue, -1));
		}
		this.oStringBuffer.append(sSql);
	}
	*/
	
	/**
     * Add a SQL criteria
     * @param psField
     * @param poValue
     * @param piDataType	java.sql.Types
     */
	public void addCriteria(String psField, Object poValue, int piDataType) {
		String sSql = ((this.criteriaCount++ == 0) ? " WHERE " : " AND ") + psField + " = ?";
		this.add(new SqlParamValue(poValue, piDataType));
		this.oStringBuffer.append(sSql);
	}
	
	public void addCriteria(String psField, Object poValue, int piDataType, AuditEventRecord auditEventRecord) {
		this.addCriteria(psField, poValue, piDataType);
		auditEventRecord.addAttribute(psField, poValue);
	}
	
	/**
     * Get SQL UPDATE statement
     * @return	SQL Statement
     */
	public String getSql() {
		return this.oStringBuffer.toString();
	}
	
	/**
     * Apply parameters against prepared statement object
     */
	public void applyParams(PreparedStatement ps) throws Exception {
		SqlParamValue oSqlParamValue = null;
		try {
			for (int iIndex = 0; iIndex < this.size(); iIndex++) {
				oSqlParamValue = this.get(iIndex);
				if (oSqlParamValue instanceof org.apache.commons.fileupload.FileItem) {
					org.apache.commons.fileupload.FileItem oItem = (org.apache.commons.fileupload.FileItem) oSqlParamValue;
					ps.setBinaryStream(iIndex + 1, oItem.getInputStream(), (int) oItem.getSize());
				} else
					oSqlParamValue.setParam(ps, iIndex + 1);
			}
		} catch (Exception oError) {
			logger.info("FAILED");
		}
	}
	
	/**
     * Execute SQL Update
     * @return Number of Rows applied
     */
	public int execute(Connection conn) throws Exception {
		PreparedStatement ps = null;
		try {
			String sSql = this.getSql();
			ps = conn.prepareStatement(sSql);
			this.applyParams(ps);
			return ps.executeUpdate();
		} finally {
			SqlUtil.closeInstance(ps);
		}
	}
	
}
