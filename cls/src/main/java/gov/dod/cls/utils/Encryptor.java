package gov.dod.cls.utils;

import org.jasypt.encryption.pbe.PooledPBEStringEncryptor;
import org.jasypt.util.password.StrongPasswordEncryptor;

public final class Encryptor {
	
	// http://www.jasypt.org/
	public static boolean isSamePassword(String toCheck, String encyipted) {
		try {
			StrongPasswordEncryptor passwordEncryptor = new StrongPasswordEncryptor();
			return passwordEncryptor.checkPassword(toCheck, encyipted);
		} catch (Exception oIgnore) {
			return false;
		}
	}
	
	public static String encryptPassword(String value) {
		StrongPasswordEncryptor passwordEncryptor = new StrongPasswordEncryptor();
		return passwordEncryptor.encryptPassword(value);
	}
	
	public static void main(String[] args) {
		String test = "test@hgs-worldwide.";
		String encrypted = "0xGIuayiX/MCh6aMok3yqZQ0vpTZqzyGaq1Xd9j1aIrBatq8UiQKxG4Hj2cGjLb3"; // encryptPassword(test);
		if (isSamePassword(test, encrypted)) {
			System.out.println(encrypted);
			System.out.println("success");
		} else
			System.out.println("failed");
	}

	// ==============================================================================
	private static String cryptorKey = null;
	public static void setCryptorKey(String newKey) {
		Encryptor.cryptorKey = newKey;
	}
	
	private static PooledPBEStringEncryptor stringEncryptor = null; // new PooledPBEStringEncryptor ();
	
	private static PooledPBEStringEncryptor  getDigester() {
		if (Encryptor.stringEncryptor == null) {
			Encryptor.stringEncryptor = new PooledPBEStringEncryptor ();
			Encryptor.stringEncryptor.setPoolSize(4);
			Encryptor.stringEncryptor.setPassword((Encryptor.cryptorKey == null) ? "hgs-worldwide" : Encryptor.cryptorKey);
			Encryptor.stringEncryptor.setAlgorithm("PBEWithMD5AndTripleDES");
		}
		return stringEncryptor;
	}
	
	public static String encryptString(String value) {
		PooledPBEStringEncryptor oEncryptor = Encryptor.getDigester();
		return oEncryptor.encrypt(value);
	}

	public static String decryptString(String value) {
		PooledPBEStringEncryptor oEncryptor = Encryptor.getDigester();
		return oEncryptor.decrypt(value);
	}

}
