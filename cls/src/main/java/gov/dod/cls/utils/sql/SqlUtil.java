package gov.dod.cls.utils.sql;

import java.sql.Blob;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Calendar;
import java.util.Date;

import org.apache.log4j.Logger;

/**
 * This class provides static functions to support Database SQL
 */
public class SqlUtil {

	private static final Logger logger = Logger.getLogger(SqlUtil.class);
	/**
	 * This loads parameter values into a PreparedStatement object
	 * @param ps
	 * @param piIndex
	 * @param poValue
	 * @param piDataType	java.sql.Types
	 */
	public static void setParam(PreparedStatement ps, int piIndex, Object poValue, int piDataType) throws SQLException {
		if (poValue == null)
			ps.setNull(piIndex, piDataType);
		else {
			if (poValue instanceof String)
				ps.setString(piIndex, (String)poValue);
			else if (poValue instanceof Integer)
				ps.setInt(piIndex, (Integer)poValue);
			else if (poValue instanceof Double)
				ps.setDouble(piIndex, (Double)poValue);
			else if (poValue instanceof Date)
				ps.setTimestamp(piIndex, new java.sql.Timestamp(((Date) poValue).getTime())); // new java.sql.Date(((Date) poValue).getTime()));
			else if (poValue instanceof java.sql.Timestamp)
				ps.setTimestamp(piIndex, (java.sql.Timestamp) poValue);
			else if (poValue instanceof java.sql.Date)
				ps.setDate(piIndex, (java.sql.Date) poValue);
			else if (poValue instanceof Float)
				ps.setFloat(piIndex, (Float)poValue);
			else if (poValue instanceof Long)
				ps.setLong(piIndex, (Long)poValue);
			else if (poValue instanceof javax.sql.rowset.serial.SerialBlob)
				ps.setBlob(piIndex, (javax.sql.rowset.serial.SerialBlob)poValue);
			else if (poValue instanceof Blob)
				ps.setBlob(piIndex, (Blob)poValue);
			else if (poValue instanceof Clob)
				ps.setClob(piIndex, (Clob)poValue);
			else
				ps.setObject(piIndex, poValue);
		}
	}
	
	/**
	 * This close allocated database object
	 * @param poRS
	 * @param poPS
	 * @param poConn
	 */
	public static void closeInstance(ResultSet poRS, PreparedStatement poPS, Connection poConn) {
		SqlUtil.closeInstance(poRS);
		SqlUtil.closeInstance(poPS);
		SqlUtil.closeInstance(poConn);
	}

	public static void closeInstance(ResultSet poRS, PreparedStatement poPS) {
		SqlUtil.closeInstance(poRS);
		SqlUtil.closeInstance(poPS);
	}

	/**
	 * This close allocated database object
	 * @param poInstance
	 */
	public static Connection closeInstance(Connection poInstance) {
		if (poInstance != null)
			try {
				poInstance.close();
    		} catch (Exception oError) {
    		}
		return null;
	}
	

	/**
	 * This rollback transaction
	 * @param poInstance
	 */
	public static void rollback(Connection poInstance) {
		if (poInstance != null)
			try {
				poInstance.rollback();
    		} catch (Exception ex) {
    			logger.warn("Problem to rollback transaction", ex);
    		}
	}
	
	

	/**
	 * This close allocated database object
	 * @param poInstance
	 */
	public static PreparedStatement closeInstance(PreparedStatement poInstance) {
		if (poInstance != null)
			try {
				poInstance.close();
    		} catch (Exception oError) {
    		}
		return null;
	}

	/**
	 * This close allocated database object
	 * @param poInstance
	 */
	public static Statement closeInstance(Statement poInstance) {
		if (poInstance != null)
			try {
				poInstance.close();
    		} catch (Exception oError) {
    		}
		return null;
	}

	/**
	 * This close allocated database object
	 * @param poInstance
	 */
	public static ResultSet closeInstance(ResultSet poInstance) {
		if (poInstance != null)
			try {
				poInstance.close();
    		} catch (Exception oError) {
    		}
		return null;
	}

	/**
	 * This generates SQL VALUE clause based on SQL SELECT string
	 * @param psSelect	SQL SELECT clause
	 * @return SQL VALUE clause
	 */
	public static String getSqlInsertValueClause(String psSelect) {
		StringBuffer oStringBuffer = new StringBuffer();
		boolean bFirst = true;
		for(int i =0; i < psSelect.length(); i++)
		    if (psSelect.charAt(i) == ',') {
		    	if (bFirst) {
		    		oStringBuffer.append(" VALUES (?");
		    		bFirst = false;
		    	}
		    	oStringBuffer.append(",?");
		    }
		if (oStringBuffer.length() > 0)
			oStringBuffer.append(")");
		return oStringBuffer.toString();
	}
	
	/**
	 * This creates a java.sql.Date for a current date/time 
	 * @return java.sql.Date instance
	 */
	public static java.sql.Date getNow() {
		Calendar cal = Calendar.getInstance();
		return new java.sql.Date( cal.getTimeInMillis() );

	}
	
	public static String quoteString(String psValue) {
		if ((psValue == null) || psValue.isEmpty())
			return "NULL";
		
		if (psValue.contains("\n")) {
			String[] aItems = psValue.split("\n");
			psValue = "";
			for (int index = 0; index < aItems.length; index++) {
				if (index > 0)
					psValue += "\\n";
				psValue += aItems[index];
			}
		}
		
		return "'" + psValue.replaceAll("'", "''") + "'"; //
	}
	
}
