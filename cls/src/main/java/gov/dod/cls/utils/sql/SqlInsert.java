package gov.dod.cls.utils.sql;

import gov.dod.cls.db.AuditEventRecord;
import gov.dod.cls.utils.CommonUtils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.ArrayList;

import org.apache.log4j.Logger;

/**
 * This class provide a simple way to perform SQL INSERT DML statement using parameterized SQL statement
 */
public class SqlInsert extends ArrayList<Object> {

	final static Logger logger = Logger.getLogger(SqlInsert.class);
	
	private static final long serialVersionUID = -956479015178982833L;
	
	private StringBuffer oStringBufferFields = new StringBuffer();
	private StringBuffer oStringBufferValues = new StringBuffer();
	private int setCount = 0;
	private String db;
	private String table;
	
	/**
     * SqlUpdate class constructor
     * @param psSchema
     * @param psTable
     */
	public SqlInsert(String psSchema, String psTable) {
		this.db = psSchema;
		this.table = psTable;
	}
	
	/**
     * Add a SQL set
     * @param psField
     * @param poValue
     */
	public boolean addField(String psField, Object poValue) {
		if (poValue == null)
			return false;
		if (poValue instanceof String)
			if (((String)poValue).equals(""))
				return false;

		if (this.setCount++ > 0) {
			this.oStringBufferFields.append(", ");
			this.oStringBufferValues.append(", ");
		}
		this.oStringBufferFields.append(psField);
		this.oStringBufferValues.append("?");
		this.add(poValue);
		return true;
	}
	
	public boolean addField(String psField, Object poValue, AuditEventRecord auditEventRecord) {
		boolean result = this.addField(psField, poValue);
		if (result)
			auditEventRecord.addAttribute(psField, poValue);
		return result;
	}
	/**
     * Get SQL INSERT statement
     * @return	SQL Statement
     */
	public String getSql() {
		StringBuffer oStringBuffer = new StringBuffer();
		oStringBuffer.append("INSERT INTO " + (CommonUtils.isEmpty(this.db) ? "" : this.db + ".") + this.table + "(");
		oStringBuffer.append(this.oStringBufferFields);
		oStringBuffer.append(") VALUES (");
		oStringBuffer.append(this.oStringBufferValues);
		oStringBuffer.append(")");
		return oStringBuffer.toString();
	}
	
	/**
     * Apply parameters against prepared statement object
     */
	public void applyParams(PreparedStatement ps) throws Exception {
		try {
			for (int iIndex = 0; iIndex < this.size(); iIndex++) {
				Object oValue = this.get(iIndex);
				if (oValue instanceof org.apache.commons.fileupload.FileItem) {
					org.apache.commons.fileupload.FileItem oItem = (org.apache.commons.fileupload.FileItem) oValue;
					ps.setBinaryStream(iIndex + 1, oItem.getInputStream(), (int) oItem.getSize());
				} else
					ps.setObject(iIndex + 1, oValue);
			}
		} catch (Exception oError) {
			logger.info("FAILED");
		}
	}
	
	/**
     * Execute SQL INSERT
     * @return Number of Rows applied
     */
	public int execute(Connection conn) throws Exception {
		PreparedStatement ps = null;
		String sSql = this.getSql();
		try {
			ps = conn.prepareStatement(sSql);
			this.applyParams(ps);
			return ps.executeUpdate();
		} catch (Exception oError) {
			logger.info("FAILED SQL: " + sSql);
			throw oError;
		} finally {
			SqlUtil.closeInstance(ps);
		}
	}
	

}
