package gov.dod.cls.utils.sql;

import java.sql.PreparedStatement;

/**
 * This class is to store SQL parameter values
 */
public class SqlParamValue {

	private Object value;
	private int dataType;
	
	public Object getValue() { return this.value; }
	public void setValue(Object value) { this.value = value; }

	public int getDataType() { return this.dataType; }
	public void setDataType(int dataType) { this.dataType = dataType; }

	/**
     * SqlParamValue class constructor
     * @param pValue
     * @param piDataType	java.sql.Types
     */
	public SqlParamValue(Object pValue, int piDataType) {
		this.value = pValue;
		this.dataType = piDataType;
	}
	
	/**
     * Set parameter of a SQL prepared statement
     * @param poPreparedStatement
     * @param piIndex
     */
	public void setParam(PreparedStatement poPreparedStatement, int piIndex) throws Exception {
		SqlUtil.setParam(poPreparedStatement, piIndex, this.value, this.dataType);
	}

}
