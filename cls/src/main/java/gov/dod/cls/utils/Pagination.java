package gov.dod.cls.utils;

import gov.dod.cls.PageApi;
import gov.dod.cls.bean.session.UserSession;
import gov.dod.cls.model.JsonAble;

import java.util.Date;
import java.util.Enumeration;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.UriInfo;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.node.ArrayNode;
import org.codehaus.jackson.node.ObjectNode;

public class Pagination {
	
	public final static String HEADER_LINK = "Link";
	
	public static final String PARAM_LIMIT = "limit";
	public static final String PARAM_OFFSET = "offset";

	public static final String TAG_META = "meta";
	public static final String TAG_DATA = "data";

	public static final String TAG_DATA_SOURCE = "data_source";

	public static final String TAG_ORIGIN = "origin";
	public static final String TAG_SECURITY_DESCRIPTOR = "security_descriptor";
	public static final String TAG_EFFECTIVE_DATE = "effective_date";
	public static final String TAG_PRODUCT_SECURITY_DESCRIPTOR = "product_security_descriptor";
	public static final String TAG_HAS_SIPERNET_BREACHES = "has_sipernet_breaches";
	public static final String TAG_HAS_SIPERNET_DATA = "has_sipernet_data";
	
	public final static String TAG_PARAMS = "params";

	public final static String TAG_LIMIT = "limit";
	public final static String TAG_OFFSET = "offset";

	public final static String TAG_COUNT = "count";
	public final static String TAG_TOTAL_COUNT = "total_count";

	public static void send(String dataSource, HttpServletRequest req, HttpServletResponse resp, ArrayNode arrayNode) {
		Pagination pagination = new Pagination(req);
		pagination.dataSource = dataSource;
		pagination.setArrayNode(arrayNode);
		pagination.send(resp);
	}
	
	// ==================================================================
	private String dataTag = TAG_DATA;
	private String dataSource;
	private String origin = "CLS";
	private String securityDescriptor = "U";
	private Date effectiveDate = CommonUtils.getNow();
	private String productSecurityDescriptor = "FOUO";
	private Boolean hasSipernetBreaches = Boolean.FALSE;
	private Boolean hasSipernetData = Boolean.FALSE;
	
	private int limit = 0;
	private int offset = 1;
	private int count = 0;
	private int total_count = 0;
	private boolean acceptedRange = true;
	private String originalPath = null;
	private ObjectMapper mapper = new ObjectMapper();
	private ArrayNode arrayNode = mapper.createArrayNode();
	
	public Pagination(HttpServletRequest req) {
		super();
		if (req != null) {
			Integer iLimit = CommonUtils.toInteger(req.getParameter(PARAM_LIMIT));
			Integer iOffset = CommonUtils.toInteger(req.getParameter(PARAM_OFFSET));
			this.set(iLimit, iOffset);
			this.parseOrginalPath(req);
		}
	}
	
	public Pagination(Integer piLimit, Integer piOffset) {
		super();
		this.set(piLimit, piOffset);
	}
	
	public Pagination(Integer piLimit, Integer piOffset, UriInfo uriInfo) {
		super();
		this.set(piLimit, piOffset);
		this.parseOrginalPath(uriInfo);
	}
	
	private void set(Integer piLimit, Integer piOffset) {
		if ((piLimit != null) && (piLimit.intValue() > 0))
			this.limit = piLimit.intValue(); 
		if ((piOffset != null) && (piOffset.intValue() > 0))
			this.offset = piOffset.intValue();
	}
	
	protected ObjectNode metaDataToJson()
	{
		ObjectNode json = this.mapper.createObjectNode();
		
		json.put(TAG_DATA_SOURCE,  this.dataSource );
		json.put(TAG_ORIGIN, this.origin );
		json.put(TAG_SECURITY_DESCRIPTOR,  this.securityDescriptor );
		json.put(TAG_EFFECTIVE_DATE, (this.effectiveDate == null ? null : this.effectiveDate.getTime()));
		json.put(TAG_PRODUCT_SECURITY_DESCRIPTOR, this.productSecurityDescriptor);
		json.put(TAG_HAS_SIPERNET_BREACHES, this.hasSipernetBreaches);
		json.put(TAG_HAS_SIPERNET_DATA, this.hasSipernetData);

		//if (this.hasValidRequests()) {
			ObjectNode jsonExtra = mapper.createObjectNode();
			jsonExtra.put(TAG_LIMIT, this.limit);
			jsonExtra.put(TAG_OFFSET, this.offset);
			json.put(TAG_PARAMS, jsonExtra);
		//}
		json.put(TAG_COUNT, this.count);
		json.put(TAG_TOTAL_COUNT, this.total_count);
		
		return json;
	}
	
	public ObjectNode toJson() {
		return this.toJson(false);
	}
	
	public ObjectNode toJson(boolean onlyDataTag) {
		ObjectNode json = this.mapper.createObjectNode();
		if (onlyDataTag == false)
			json.put(TAG_META, this.metaDataToJson());
		json.put(this.dataTag, this.arrayNode);
		return json;
	}
	
	public Response generateResponse() {
		String jsonResponse = this.toJson().toString();
		ResponseBuilder responseBuilder = Response.status(HttpServletResponse.SC_OK).entity(jsonResponse);
		this.addLink(responseBuilder);
		return responseBuilder.build();
	}
	
	public void send(HttpServletResponse resp) {
		this.send(resp, null);
		/*
		resp.setCharacterEncoding("UTF-8");
		resp.setContentType(ClsConstants.RESPONSE_FORMAT_JSON);
		resp.addHeader(Pagination.HEADER_LINK, this.getLinkValue());
		PageApi.send(resp, JsonAble.JSON_RESPONSE_FORMAT, this.toJson().toString());
		*/
	}
	
	public void send(HttpServletResponse resp, UserSession userSession) { // CJ-649
		resp.setCharacterEncoding("UTF-8");
		resp.setContentType(ClsConstants.RESPONSE_FORMAT_JSON);
		resp.addHeader(Pagination.HEADER_LINK, this.getLinkValue());
		ObjectNode onResult = this.toJson();
		if (userSession != null)
			JsonAble.addSession(onResult, userSession);
		PageApi.send(resp, JsonAble.JSON_RESPONSE_FORMAT, onResult.toString());
	}
	
	// ---------------------------------------------------------
	public String getDataSource() {
		return dataSource;
	}
	public void setDataSource(String dataSource) {
		this.dataSource = dataSource;
	}
	public void addDataSource(String dataSource) {
		if (CommonUtils.isEmpty(this.dataSource)) {
			this.dataSource = dataSource;
		} else {
			if (!this.dataSource.contains(dataSource))
				this.dataSource += "; " + dataSource;
		}
	}

	public String getOrigin() {
		return origin;
	}
	public void setOrigin(String origin) {
		this.origin = origin;
	}
	public void addOrigin(String origin) {
		if (CommonUtils.isEmpty(this.origin)) {
			this.origin = origin;
		} else {
			if (!this.origin.contains(origin))
				this.origin += "; " + origin;
		}
	}

	public String getSecurityDescriptor() {
		return securityDescriptor;
	}
	public void setSecurityDescriptor(String securityDescriptor) {
		this.securityDescriptor = securityDescriptor;
	}

	public Date getEffectiveDate() {
		return effectiveDate;
	}
	public void setEffectiveDate(Date effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	public String getProductSecurityDescriptor() {
		return productSecurityDescriptor;
	}

	public void setProductSecurityDescriptor(String productSecurityDescriptor) {
		this.productSecurityDescriptor = productSecurityDescriptor;
	}

	public Boolean getHasSipernetBreaches() {
		return hasSipernetBreaches;
	}

	public void setHasSipernetBreaches(Boolean hasSipernetBreaches) {
		this.hasSipernetBreaches = hasSipernetBreaches;
	}

	public Boolean getHasSipernetData() {
		return hasSipernetData;
	}

	public void setHasSipernetData(Boolean hasSipernetData) {
		this.hasSipernetData = hasSipernetData;
	}

	// ---------------------------------------------------------
	public int getLimit() {
		return limit;
	}
	public void setLimit(int limit) {
		this.limit = limit;
	}
	
	public int getOffset() {
		return this.offset;
	}
	public void setOffset(int offset) {
		this.offset = offset;
	}
	
	public boolean hasValidRequests() {
		return (this.limit > 0) && (this.offset > 0); 
	}
	
	public int getToSkip() {
		return (this.offset > 0 ? this.offset - 1 : 0);  
	}
	
	public int getToCount() {
		int result = this.getToSkip() + (this.limit > 0 ? this.limit : this.total_count);
		if (result > this.total_count)
			result = this.total_count;
		return result;
	}
	
	public String sqlLimit() {
		if (this.limit > 0) {
			String result = " LIMIT ";
			int iSkip = this.getToSkip();
			if (iSkip > 1)
				result += iSkip + ",";
			result += this.limit;
			return result;
		} else
			return "";
	}
	
	public int getCount() {
		return this.count;
	}
	public void setCount(int count) {
		this.count = count;
	}
	public int incCount() {
		return ++this.count;
	}
	
	public int getTotal_count() {
		return total_count;
	}
	public void setTotal_count(int total_count) {
		this.total_count = total_count;
	}
	
	public boolean getAcceptedRange() {
		return this.acceptedRange;
	}
	public boolean isAcceptedRange(int iTotalCount) {
		this.total_count = iTotalCount;
		this.acceptedRange = (iTotalCount >= this.offset);
		return this.acceptedRange;
	}
	public void setAcceptedRange(boolean value) {
		this.acceptedRange = value;
	}
	
	public String getBadRequestMessage() {
		String result = "Requested offset(" + this.offset + ") is more than total records(" + this.total_count + ")";
		return result;
	}

	public String getOriginalPath() {
		return originalPath;
	}
	public void setOriginalPath(String originalPath) {
		this.originalPath = originalPath;
	}
	public void parseOrginalPath(UriInfo uriInfo) {
		if (uriInfo == null)
			return;
		this.originalPath = uriInfo.getBaseUri().toString() + uriInfo.getPath();
		String sOpr = "?";
		for (String sParamId : uriInfo.getQueryParameters().keySet()) {
			List<String> values = uriInfo.getQueryParameters().get(sParamId);
			for (String value : values) {
				if (!("limit".equalsIgnoreCase(sParamId) || "offset".equalsIgnoreCase(sParamId))) {
					this.originalPath += sOpr + sParamId + "=" + value;
					sOpr = "&";
				}
				//this.addDataSource(sParamId + "=" + value);
			}
		}
	}
	public void parseOrginalPath(HttpServletRequest req) {
		if (req == null)
			return;
		this.originalPath = req.getRequestURL().toString();
		String sOpr = "?";
		@SuppressWarnings("unchecked")
		Enumeration<String> paramNames = (Enumeration<String>)(req.getParameterNames());
		while (paramNames.hasMoreElements()){
			String sParamId = (String)paramNames.nextElement();
			if (!("limit".equalsIgnoreCase(sParamId) || "offset".equalsIgnoreCase(sParamId))) {
				this.originalPath += sOpr + sParamId + "=" + req.getParameter(sParamId);
				sOpr = "&";
			}
		}
	}
	
	public boolean isLinkAvailable() {
		if ((this.limit <= 0) || (this.total_count <= 1) || (this.limit >= this.total_count))
			return false;
		
		return true;
	}
	
	private String getLink(String opr, int offset, String relTag) {
		return "<" + this.originalPath + opr + "limit=" + this.limit + "&offset=" + offset + ">"
				+ ";rel=\"" + relTag + "\"";
	}
	
	public String getLinkValue() {
		if (this.originalPath != null) {
			String sOpr = (this.originalPath.contains("?") ? "&" : "?");
			String sLinkValue;
			if (this.getToSkip() > 1) {
				int iFirst = this.getToSkip() + 1 - this.limit;
				if (iFirst < 1)
					iFirst = 1;
				sLinkValue = this.getLink(sOpr, iFirst, "previous");
				sOpr = "&";
			} else
				sLinkValue = "";
			int offset = this.getToSkip() + 1 + this.limit;
			if (offset <= this.total_count) {
				sLinkValue += (sLinkValue.equals("") ? "" : ";") + this.getLink(sOpr, offset, "next");
			}
			return sLinkValue;
		} else
			return "";
	}
	
	public void addLink(ResponseBuilder responseBuilder) {
		responseBuilder.header(Pagination.HEADER_LINK, this.getLinkValue());
	}

	public ObjectMapper getMapper() {
		return mapper;
	}

	public ArrayNode getArrayNode() {
		return arrayNode;
	}
	public void setArrayNode(ArrayNode arrayNode) {
		this.arrayNode = arrayNode;
		if ((arrayNode != null) && (this.count == 0) && (this.total_count == 0)) {
			this.count = this.arrayNode.size();
			this.total_count = this.arrayNode.size();
		}
		
	}

	public String getDataTag() {
		return dataTag;
	}

	public void setDataTag(String dataTag) {
		this.dataTag = dataTag;
	}

}
