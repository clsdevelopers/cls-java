package gov.dod.cls.model;

import java.util.Date;

import javax.servlet.http.HttpSession;

import gov.dod.cls.bean.session.UserSession;
import gov.dod.cls.utils.CommonUtils;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.node.ArrayNode;
import org.codehaus.jackson.node.ObjectNode;

public class JsonAble {

	public final static String JSON_RESPONSE_FORMAT = "application/json";
	
	public final static String TAG_STATUS = "status";
	public final static String TAG_DATA = "data";
	public final static String TAG_MESSAGE = "message";

	public final static String TAG_USER_SESSION = "user_session"; // CJ-573
	public final static String TAG_LAST_ACCESSED_TIME = "lastAccessedTime"; // CJ-573
	public final static String TAG_MAX_INACTIVE_INTERVAL = "maxInactiveInterval"; // CJ-573
	public final static String TAG_SESSION_TIMEOUT = "sessionTimeout"; // CJ-573
	public final static String TAG_SERVER_TIME = "serverTime"; // CJ-634
	
	public final static String STATUS_SUCCESS = "success";
	public final static String STATUS_FAIL = "fail";
	public final static String STATUS_ERROR = "error";
	
	public static String jsonResult(Object pStatus, Object pData, String pMessage) {
		return JsonAble.jsonResult(pStatus, pData, pMessage, null);
	}
	
	public static String jsonResult(Object pStatus, Object pData, String pMessage, UserSession userSession) {
		ObjectMapper mapper = new ObjectMapper();
		ObjectNode json = mapper.createObjectNode();
		json.put(TAG_STATUS, pStatus.toString());
		if (pData == null)
			json.put(TAG_DATA, (String)null);
		else if (pData instanceof ObjectNode)
			json.put(TAG_DATA, (ObjectNode)pData);
		else if (pData instanceof ArrayNode)
			json.put(TAG_DATA, (ArrayNode)pData);
		else
			json.put(TAG_DATA, pData.toString());
		json.put(TAG_MESSAGE, pMessage);

		JsonAble.addSession(json, userSession); // CJ-573
		
		return json.toString();
	}
	
	public static void addSession(ObjectNode json, UserSession userSession) { // CJ-573
		ObjectNode jsSession = json.objectNode(); // null;
		if (userSession != null) {
			// jsSession = json.objectNode();
			jsSession.put(TAG_LAST_ACCESSED_TIME, userSession.getLastAccessedTime());
			jsSession.put(TAG_MAX_INACTIVE_INTERVAL, userSession.getMaxInactiveInterval());
			jsSession.put(TAG_SESSION_TIMEOUT, CommonUtils.toJsonDate(userSession.getExpiresAt()));
			jsSession.put(TAG_SERVER_TIME, CommonUtils.toJsonDate(CommonUtils.getNow())); // "serverTime"; // CJ-634
		}
		json.put(TAG_USER_SESSION, jsSession);
	}
	
	public static Long getJsonValue(Date value) {
		return ((value == null) ? null : value.getTime());
	}

	// =============================================================

	private String status = STATUS_SUCCESS;
	private String message = null;
	private HttpSession session = null; // CJ-573
	
	// Need to be overwritten usign @Override
	protected void populateJson(ObjectNode json, boolean forInterview) {
	};
	
	// Need to be overwritten usign @Override
	protected void populateJson(ObjectNode json, ObjectMapper mapper) {
		this.populateJson(json, false);
	};
	
	public ObjectNode toJson(ObjectMapper mapper) {
		if (mapper == null)
			mapper = new ObjectMapper();
		ObjectNode json = mapper.createObjectNode();
		this.populateJson(json, mapper);
		return json;
	}

	public ObjectNode toJson() {
		ObjectMapper mapper = new ObjectMapper();
		return this.toJson(mapper);
	}
	
	public ObjectNode toJson(boolean forInterview) {
		if (forInterview) {
			ObjectMapper mapper = new ObjectMapper();
			ObjectNode json = mapper.createObjectNode();
			this.populateJson(json, forInterview);
			return json;
		} else {
			return this.toJson();
		}
	}
	
	public void addMessage(String newMessage) {
		if (CommonUtils.isNotEmpty(newMessage)) {
			if (this.message == null)
				this.message = newMessage;
			else
				this.message += "\n" + newMessage;
		}
	}

	public boolean addErrorMessage(String newMessage) {
		this.status = STATUS_ERROR;
		this.addMessage(newMessage);
		return false;
	}

	public boolean addFailMessage(String newMessage) {
		this.status = STATUS_FAIL;
		this.addMessage(newMessage);
		return false;
	}

	public String genJson() {
		return JsonAble.jsonResult(this.status, 
				(this.isSuccess() ? this.toJson() : (String)null), 
				this.message);
	}
	
	// =============================================================
	public String getStatus() {
		return status;
	}
	public void setSuccess() {
		this.status = STATUS_SUCCESS;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public boolean isSuccess() {
		return STATUS_SUCCESS.equals(this.status);
	}
	public boolean isNotSuccess() {
		return (! this.isSuccess());
	}
	public boolean isFail() {
		return STATUS_FAIL.equals(this.status);
	}
	public boolean isError() {
		return STATUS_ERROR.equals(this.status);
	}

	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}

	public HttpSession getSession() {
		return session;
	}
	public void setSession(HttpSession session) {
		this.session = session;
	}

}
