package gov.dod.cls.reference;

import javax.servlet.http.HttpServletRequest;

import org.jasypt.commons.CommonUtils;

import gov.dod.cls.bean.session.UserSession;

public final class ClsPages {
	
	private volatile static String appPath = null;
	
	public static final String PARAM_TOKEN = "token";
	
	public static final String PAGE_INDEX = "index.html";
	public static final String PAGE_LOGIN = "logon.html";
	public static final String PAGE_REGISTER = "register.html";
	public static final String PAGE_RESETPASSWORD = "resetpassword.html";
	public static final String PAGE_FORGETPASSWORD = "resetpswd.html";
	
	public static final String PAGE_INTERVIEW = "interview.jsp";
	public static final String PAGE_FINAL_CLAUSES = "final_clauses.jsp";
	public static final String PAGE_REPORT_CLAUSES = "clauses_print.jsp";
	public static final String PAGE_CLAUSE_DETAIL = "clause_detail.jsp";

	public static final String PAGE_ACCOUNT = "account";
	public static final String PAGE_ACCOUNT_DASHBOARD = PAGE_ACCOUNT + "/dashboard.jsp"; // dashboard.html
	public static final String PAGE_ACCOUNT_SETTINGS = PAGE_ACCOUNT + "/settings.html";
	public static final String PAGE_ACCOUNT_CHANGE_PASSWORD = PAGE_ACCOUNT + "/change_password.html";
	
	public synchronized static String getAppPath(HttpServletRequest req) {
		if (req == null)
			return "";
		if (ClsPages.appPath == null) {
			ClsPages.appPath = req.getContextPath() + "/";
		}
		return ClsPages.appPath;
	}

	public static String index(HttpServletRequest req) {
		return ClsPages.getAppPath(req) + PAGE_INDEX;
	}

	public static String afterLogon(HttpServletRequest req, UserSession userSession) {
		//if (userSession.isSubsuperUser())
		return ClsPages.getAppPath(req) + PAGE_ACCOUNT_DASHBOARD;
	}

	public static String afterLogout(HttpServletRequest req, UserSession userSession) {
		return ClsPages.getAppPath(req); // ""; // + "index.html";
	}

	public static String afterChangePassword(HttpServletRequest req, UserSession userSession) {
		//if (userSession.isSubsuperUser())
		return ClsPages.getAppPath(req) + PAGE_ACCOUNT_SETTINGS;
	}
	
	public static boolean canAcessPage(String path, UserSession userSession) {
		boolean result = true;
		
		if (CommonUtils.isNotEmpty(path)) {
			if (path.contains("admin/")) {
				if (!(userSession.isSuperUser() || userSession.isSubsuperUser()))
					result = false;
			}
		}
		
		return result;
	}

}
