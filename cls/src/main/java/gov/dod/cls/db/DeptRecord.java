package gov.dod.cls.db;

public class DeptRecord {

	public final static String TABLE_DEPTS = "Depts";
	
	public final static String FIELD_DEPT_ID = "Dept_Id";
	public final static String FIELD_DEPT_NAME = "Dept_Name";
	public static final String FIELD_CREATED_AT = "Created_At";
	public static final String FIELD_UPDATED_AT = "Updated_At"; 
	
}
