package gov.dod.cls.db;

import gov.dod.cls.model.JsonAble;

public class QuestionTypeRefRecord extends JsonAble {
	public static final String TABLE_QUESTION_TYPE_REF = "Question_Type_Ref";
	
	public static final String TYPE_CD_SINGLE_CHOICE = "1";
	public static final String TYPE_TEXT_SINGLE_CHOICE = "Single Choice";

	public static final String TYPE_CD_BOOLEAN = "B";
	public static final String TYPE_TEXT_BOOLEAN = "Boolean";

	public static final String TYPE_CD_MULTIPLE_CHOICES = "M";
	public static final String TYPE_TEXT_MULTIPLE_CHOICES = "Multiple Choices";

	public static final String TYPE_CD_NUMERIC = "N";
	public static final String TYPE_TEXT_NUMERIC = "Numeric";
	
	public static final String VALUE_YES = "YES";
	public static final String VALUE_NO = "NO"; 
	
	public static final String FIELD_QUESTION_TYPE = "question_type";
	public static final String FIELD_QUESTION_TYPE_NAME = "question_type_name";
	public static final String FIELD_CREATED_AT = "created_at"; 
	public static final String FIELD_UPDATED_AT = "updated_at"; 
}
