package gov.dod.cls.db;

import gov.dod.cls.bean.Oauth;
import gov.dod.cls.config.ClsConfig;
import gov.dod.cls.utils.CommonUtils;
import gov.dod.cls.utils.sql.SqlInsert;
import gov.dod.cls.utils.sql.SqlUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.Date;

import org.codehaus.jackson.node.ObjectNode;

public class OauthAccessRecord {
	
	public static final String TABLE_OAUTH_ACCESS = "OAuth_Access";
	
	public static final String FIELD_OAUTH_ACCESS_ID	= "oauth_access_id";
	public static final String FIELD_APPLICATION_ID       = "application_id";
	public static final String FIELD_ACCESS_TOKEN      = "Access_Token";
	public static final String FIELD_EXPIRES_AT           = "expires_at";
	public static final String FIELD_CREATED_AT           = "created_at";
	
	public static final String SQL_SELECT =
			"SELECT " + FIELD_OAUTH_ACCESS_ID
			+ ", " + FIELD_APPLICATION_ID
			+ ", " + FIELD_ACCESS_TOKEN
			+ ", " + FIELD_EXPIRES_AT
			+ ", " + FIELD_CREATED_AT
			+ " FROM " + TABLE_OAUTH_ACCESS;

	private Integer oauthAccessId;
	private Integer applicationId;
	private String accessToken;
	private Date expiresAt;
	private Date createdAt;
	
	public void read(ResultSet rs) throws SQLException {
		this.oauthAccessId = rs.getInt(FIELD_OAUTH_ACCESS_ID);
		this.applicationId = rs.getInt(FIELD_APPLICATION_ID);
		this.accessToken = rs.getString(FIELD_ACCESS_TOKEN);
		this.expiresAt = rs.getTimestamp(FIELD_EXPIRES_AT);
		this.createdAt = rs.getTimestamp(FIELD_CREATED_AT);
	}
	
	public void fillJson(ObjectNode recordNode, boolean forApi) {
		recordNode.put("id", this.oauthAccessId);
		recordNode.put("access_token", this.accessToken);
		if (forApi) {
		} else {
			recordNode.put("expires_at", CommonUtils.toJsonDate(this.expiresAt));
			recordNode.put("application_id", this.applicationId);
		}
	}
	
	public static OauthAccessRecord getRecord(Connection conn, String accessToken) throws SQLException {
		boolean bCreateConnection = (conn == null);
		PreparedStatement ps1 = null;
		ResultSet rs1 = null;
		try {
			String sSql = SQL_SELECT
					+ " WHERE " + FIELD_ACCESS_TOKEN + " = ?";
			if (bCreateConnection)
				conn = ClsConfig.getDbConnection();
			ps1 = conn.prepareStatement(sSql);
			ps1.setString(1, accessToken);
			rs1 = ps1.executeQuery();
			if (rs1.next()) {
				OauthAccessRecord oRecord = new OauthAccessRecord();
				oRecord.read(rs1);
				return oRecord;
			}
			return null;
		}
		finally {
			SqlUtil.closeInstance(rs1, ps1);
			if (bCreateConnection)
				SqlUtil.closeInstance(conn);
		}
	}
	
	public static OauthAccessRecord getRecord(Connection conn, int oauthAccessId) throws SQLException {
		boolean bCreateConnection = (conn == null);
		PreparedStatement ps1 = null;
		ResultSet rs1 = null;
		try {
			String sSql = SQL_SELECT
					+ " WHERE " + FIELD_OAUTH_ACCESS_ID + " = ?";
			if (bCreateConnection)
				conn = ClsConfig.getDbConnection();
			ps1 = conn.prepareStatement(sSql);
			ps1.setInt(1, oauthAccessId);
			rs1 = ps1.executeQuery();
			if (rs1.next()) {
				OauthAccessRecord oRecord = new OauthAccessRecord();
				oRecord.read(rs1);
				return oRecord;
			}
			return null;
		}
		finally {
			SqlUtil.closeInstance(rs1, ps1);
			if (bCreateConnection)
				SqlUtil.closeInstance(conn);
		}
	}
	
	public static OauthAccessRecord getRecord(Connection conn, int oauthAccessId, int applicationId) throws SQLException {
		boolean bCreateConnection = (conn == null);
		PreparedStatement ps1 = null;
		ResultSet rs1 = null;
		try {
			String sSql = SQL_SELECT
					+ " WHERE " + FIELD_APPLICATION_ID + " = ?"
					+ " AND " + FIELD_OAUTH_ACCESS_ID + " = ?";
			if (bCreateConnection)
				conn = ClsConfig.getDbConnection();
			ps1 = conn.prepareStatement(sSql);
			ps1.setInt(1, oauthAccessId);
			ps1.setInt(2, applicationId);
			rs1 = ps1.executeQuery();
			if (rs1.next()) {
				OauthAccessRecord oRecord = new OauthAccessRecord();
				oRecord.read(rs1);
				return oRecord;
			}
			return null;
		}
		finally {
			SqlUtil.closeInstance(rs1, ps1);
			if (bCreateConnection)
				SqlUtil.closeInstance(conn);
		}
	}
	
	public static OauthAccessRecord insert(Connection conn, int applicationId, String accessToken) 
			throws Exception {
		
		boolean bCreateConnection = (conn == null);
		try {
			Calendar calStart = Calendar.getInstance();
			java.sql.Timestamp createdAt = new java.sql.Timestamp(calStart.getTimeInMillis());
			
			Calendar calEnd = Calendar.getInstance();
			calEnd.add(Calendar.SECOND, Oauth.EXPIRES_IN);
			java.sql.Timestamp expiresAt = new java.sql.Timestamp(calEnd.getTimeInMillis());
			
			AuditEventRecord auditEventRecord = new AuditEventRecord(AuditEventRecord.ACTION_ORGANIZATION_AUTHENTICATED, null, null);
			//auditEventRecord.setApiDetails (applicationId, null, null, conn);
			
			SqlInsert sqlInsert = new SqlInsert(null, TABLE_OAUTH_ACCESS);
			sqlInsert.addField(FIELD_APPLICATION_ID, applicationId, auditEventRecord);
			sqlInsert.addField(FIELD_ACCESS_TOKEN, accessToken, auditEventRecord);
			sqlInsert.addField(FIELD_EXPIRES_AT, expiresAt, auditEventRecord);
			sqlInsert.addField(FIELD_CREATED_AT, createdAt, auditEventRecord);
			
			if (bCreateConnection)
				conn = ClsConfig.getDbConnection();
			
			sqlInsert.execute(conn);
			auditEventRecord.saveNew(conn);
			
			return OauthAccessRecord.getRecord(conn, accessToken);
		}
		finally {
			if (bCreateConnection)
				SqlUtil.closeInstance(conn);
		}
	}
	
	public void delete(Connection conn) throws SQLException {
		boolean bCreateConnection = (conn == null);
		PreparedStatement ps1 = null;
		try {
			String sSql = "DELETE FROM " + TABLE_OAUTH_ACCESS
					+ " WHERE " + FIELD_OAUTH_ACCESS_ID + " = ?";
			if (bCreateConnection)
				conn = ClsConfig.getDbConnection();
			ps1 = conn.prepareStatement(sSql);
			ps1.setInt(1, this.oauthAccessId);
			ps1.execute();
		}
		finally {
			SqlUtil.closeInstance(ps1);
			if (bCreateConnection)
				SqlUtil.closeInstance(conn);
		}
	}
	
	public boolean isExpired(Connection conn, boolean remove) throws SQLException {
		Calendar calStart = Calendar.getInstance();
		Date now = calStart.getTime();
		if (now.after(this.expiresAt)) {
			if (remove) {
				this.delete(conn);
			}
			return true;
		}
		return false;
	}

	// ----------------------------------------------------------
	public Integer getApplicationId() {
		return applicationId;
	}

	public Integer getOauthAccessId() {
		return oauthAccessId;
	}

	public String getAccessToken() {
		return accessToken;
	}

	public Date getExpiresAt() {
		return expiresAt;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

}
