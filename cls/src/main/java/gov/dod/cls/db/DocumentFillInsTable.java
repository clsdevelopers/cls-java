package gov.dod.cls.db;

import gov.dod.cls.utils.CommonUtils;
import gov.dod.cls.utils.sql.SqlUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.node.ArrayNode;
import org.codehaus.jackson.node.ObjectNode;

public class DocumentFillInsTable extends ArrayList<DocumentFillInsRecord> {

	private static final long serialVersionUID = -8576017368877142855L;

	public static DocumentFillInsTable loadByDocument(int docId, Connection conn) throws SQLException {
		DocumentFillInsTable documentAnswerTable = new DocumentFillInsTable();
		String sSql = DocumentFillInsRecord.SQL_SELECT
				+ " WHERE " + DocumentFillInsRecord.FIELD_DOCUMENT_ID + " = ?"
				+ " ORDER BY " + DocumentFillInsRecord.FIELD_CLAUSE_FILL_IN_ID; // + ", " + DocumentFillInsRecord.FIELD_NAME;
		
		PreparedStatement ps1 = null;
		ResultSet rs1 = null;
		try {
			ps1 = conn.prepareStatement(sSql);
			ps1.setInt(1, docId);
			rs1 = ps1.executeQuery();
			while (rs1.next()) {
				DocumentFillInsRecord documentFillInsRecord = new DocumentFillInsRecord();
				documentFillInsRecord.read(rs1);
				documentAnswerTable.add(documentFillInsRecord);
			}
		}
		finally {
			SqlUtil.closeInstance(rs1, ps1);
		}
		return documentAnswerTable;
	}
	public static DocumentFillInsTable loadByDocumentForApi(int docId, Connection conn) throws SQLException {
		DocumentFillInsTable documentAnswerTable = new DocumentFillInsTable();

		String sSql = DocumentFillInsRecord.SQL_SELECT + 
				  " INNER JOIN " + DocumentClauseRecord.TABLE_DOCUMENT_CLAUSES + " DC "
				+ " ON DC."  + DocumentClauseRecord.FIELD_DOCUMENT_ID
				+ " = A." + DocumentFillInsRecord.FIELD_DOCUMENT_ID
				+ " AND (DC." + DocumentClauseRecord.FIELD_OPTIONAL_USER_ACTION_CODE + " = 'A' "
				+ " OR DC." + DocumentClauseRecord.FIELD_OPTIONAL_USER_ACTION_CODE + " IS NULL )"
				+ " AND DC." + DocumentClauseRecord.FIELD_CLAUSE_ID + " = " + "B." + ClauseFillInRecord.FIELD_CLAUSE_ID 
				+ " WHERE A." + DocumentFillInsRecord.FIELD_DOCUMENT_ID + " = ?"
				+ " ORDER BY " + DocumentFillInsRecord.FIELD_CLAUSE_FILL_IN_ID; // + ", " + DocumentFillInsRecord.FIELD_NAME;
		PreparedStatement ps1 = null;
		ResultSet rs1 = null;
		try {
			ps1 = conn.prepareStatement(sSql);
			ps1.setInt(1, docId);
			rs1 = ps1.executeQuery();
			while (rs1.next()) {
				DocumentFillInsRecord documentFillInsRecord = new DocumentFillInsRecord();
				documentFillInsRecord.read(rs1);
				documentAnswerTable.add(documentFillInsRecord);
			}
		}
		finally {
			SqlUtil.closeInstance(rs1, ps1);
		}
		return documentAnswerTable;
	}
	public static void deleteDocument(int docId, Connection conn) throws SQLException {
		PreparedStatement ps1 = null;
		try {
			String sSql = "DELETE FROM " + DocumentFillInsRecord.TABLE_DOCUMENT_FILL_INS
					+ " WHERE " + DocumentFillInsRecord.FIELD_DOCUMENT_ID + " = ?";
			ps1 = conn.prepareStatement(sSql);
			ps1.setInt(1, docId);
			ps1.execute();
		}
		finally {
			SqlUtil.closeInstance(ps1);
		}
	}

	public static PreparedStatement createUpgradeClsVersionPrepareStatement(Connection conn) throws SQLException {
		String sSql = "UPDATE " + DocumentFillInsRecord.TABLE_DOCUMENT_FILL_INS
				+ " SET " + DocumentFillInsRecord.FIELD_CLAUSE_FILL_IN_ID + " = ?"
				+ " WHERE " + DocumentFillInsRecord.FIELD_DOCUMENT_FILL_IN_ID + " = ?";
		return conn.prepareStatement(sSql);
	}

	public static PreparedStatement createDeletePrepareStatement(Connection conn) throws SQLException {
		String sSql = "DELETE FROM " + DocumentFillInsRecord.TABLE_DOCUMENT_FILL_INS
				+ " WHERE " + DocumentFillInsRecord.FIELD_DOCUMENT_FILL_IN_ID + " = ?";
		return conn.prepareStatement(sSql);
	}

	// =================================================================
	public DocumentFillInsTable collectByClause(Integer pClauseId) {
		DocumentFillInsTable result = new DocumentFillInsTable();
		for (DocumentFillInsRecord documentFillInsRecord : this) {
			Integer clauseId = documentFillInsRecord.getClauseId();
			if (clauseId.equals(pClauseId))
				result.add(documentFillInsRecord);
		}
		return result;
	}
	
	public DocumentFillInsRecord findByCode(String code) {
		for (DocumentFillInsRecord documentFillInsRecord : this) {
			if (code.equals(documentFillInsRecord.getFillInCd()))
				return documentFillInsRecord;
		}
		return null;
	}

	public DocumentFillInsRecord findById(int documentFillInId) { // clauseFillInId
		for (DocumentFillInsRecord documentFillInsRecord : this) {
			if (documentFillInId == documentFillInsRecord.getId().intValue()) // .getClauseFillInId()
				return documentFillInsRecord;
		}
		return null;
	}

	public ArrayNode toJson(ObjectMapper mapper, boolean forInterview) {
		if (mapper == null)
			mapper = new ObjectMapper();
		ArrayNode json = mapper.createArrayNode();
		for (DocumentFillInsRecord record : this) {
			json.add(record.toJson(forInterview));
		}
		return json;
	}
	
	public void fromJson(ArrayNode pArrayNode) {
		for (JsonNode jsonNode : pArrayNode) {
			DocumentFillInsRecord record = new DocumentFillInsRecord();
			record.fromJson(jsonNode);
			this.add(record);
		}
	}
	
	public void populateJsonForApi(ObjectNode json, boolean bForVer1) {
		HashMap<String, Integer> hmap = new HashMap<String, Integer>(); // CJ-813
		for (DocumentFillInsRecord record : this) {
			String fillInCd = record.getFillInCd();
			if (hmap.containsKey(fillInCd)) {
				int iCount = hmap.get(fillInCd) + 1;
				hmap.put(fillInCd, iCount);
				fillInCd += String.valueOf(iCount);
			} else {
				hmap.put(fillInCd, 1);
			}
			json.put(fillInCd, record.getAnswer());
		}
	}
	
	public DocumentFillInsRecord findByClauseFillInId(int clauseFillInId) {
		for (DocumentFillInsRecord oRecord : this) {
			if (oRecord.getClauseFillInId().intValue() == clauseFillInId)
				return oRecord;
		}
		return null;
	}
	
	public DocumentFillInsRecord findByClauseFillInCode(String clauseFillInCode) {
		for (DocumentFillInsRecord oRecord : this) {
			if (oRecord.getFillInCd().equals(clauseFillInCode))
				return oRecord;
		}
		return null;
	}
	
	public static PreparedStatement createInertPrepareStatement(Connection conn) throws SQLException {
		String sSql
			= "INSERT INTO " + DocumentFillInsRecord.TABLE_DOCUMENT_FILL_INS
				+ " (" + DocumentFillInsRecord.FIELD_DOCUMENT_ID
				+ ", " + DocumentFillInsRecord.FIELD_CLAUSE_FILL_IN_ID
				+ ", " + DocumentFillInsRecord.FIELD_DOCUMENT_FILL_IN_ANSWER
				+ ", " + DocumentFillInsRecord.FIELD_FULL_TEXT_MODIFIED
				+ ") VALUES (?, ?, ?, ?)";
		return conn.prepareStatement(sSql);
	}
	
	public void populateFromPrevious(int documentId, DocumentFillInsTable previousDocumentFillInsTable,
			PreparedStatement psInsertFillIns, ClauseFillInTable previousClauseFillInTable, 
			ClauseFillInTable newClauseFillInTable) 
					throws SQLException { // CJ-1061
		for (DocumentFillInsRecord record : previousDocumentFillInsTable) {
			ClauseFillInRecord previousClauseFillInRecord = previousClauseFillInTable.findById(record.getClauseFillInId());
			if (previousClauseFillInRecord == null) {
				//System.out.println("DocumentFillInsTable.populateFromPrevious(" + documentId + ") Missing previousClauseFillInRecord");
				continue;
			}
			
			ClauseFillInRecord newClauseFillInRecord = newClauseFillInTable.findByCode(previousClauseFillInRecord.getFillInCode());
			if (newClauseFillInRecord == null) {
				//System.out.println("DocumentFillInsTable.populateFromPrevious(" + documentId + ") Missing newClauseFillInRecord for '" + previousClauseFillInRecord.getFillInCode() + "'");
				continue;
			}
			
			psInsertFillIns.setInt(1, documentId);
			psInsertFillIns.setInt(2, newClauseFillInRecord.getId());
			psInsertFillIns.setString(3, record.getAnswer());
			psInsertFillIns.setBoolean(4, record.getFullTtextModified());
			psInsertFillIns.execute();
		}
	}
	
	public static void upgradeClsVersion(ClauseRecord origClauseRecord, ClauseRecord newClauseRecord,
			DocumentFillInsTable origDocumentFillInsTable,
			PreparedStatement psUpdateFillIns) throws SQLException {
		DocumentFillInsTable oItemsToRemove = new DocumentFillInsTable();
		ClauseFillInTable oOrigClauseFillInTable = origClauseRecord.getClauseFillInTable();
		ClauseFillInTable oNewClauseFillInTable = newClauseRecord.getClauseFillInTable();
		for (DocumentFillInsRecord oRecord : origDocumentFillInsTable) {
			Integer clauseId = oRecord.getClauseId();
			if (clauseId.equals(origClauseRecord.getId())) {
				Integer iOrigClauseFillInId = oRecord.getClauseFillInId();
				ClauseFillInRecord oOrigClauseFillInRecord = oOrigClauseFillInTable.findById(iOrigClauseFillInId);
				if (oOrigClauseFillInRecord != null) {
					ClauseFillInRecord oNewClauseFillInRecord = oNewClauseFillInTable.findByCode(oOrigClauseFillInRecord.getFillInCode());
					if (oNewClauseFillInRecord != null) {
						psUpdateFillIns.setInt(1, oNewClauseFillInRecord.getId()); // + " SET " + DocumentFillInsRecord.FIELD_CLAUSE_FILL_IN_ID + " = ?"
						psUpdateFillIns.setInt(2, oRecord.getId()); // + " WHERE " + DocumentFillInsRecord.FIELD_DOCUMENT_FILL_IN_ID + " = ?";
						psUpdateFillIns.execute();
						oItemsToRemove.add(oRecord);
					}
				}
			}
		}
		for (DocumentFillInsRecord oRecord : oItemsToRemove) {
			origDocumentFillInsTable.remove(oRecord);
		}
	}
	
	public void deleteForClsVersion(PreparedStatement psDeleteFillIns) throws SQLException {
		for (DocumentFillInsRecord oRecord : this) {
			psDeleteFillIns.setInt(1, oRecord.getId()); // " WHERE " + DocumentFillInsRecord.FIELD_DOCUMENT_FILL_IN_ID + " = ?";
			psDeleteFillIns.execute();
		}
	}
	
	public void save(int documentId, DocumentFillInsTable postTable, ClauseTable oClauseTable,
			DocumentClauseTable postDocumentClauseTable, Connection conn, AuditEventRecord auditEventRecord) // CJ-745
			throws SQLException {
		PreparedStatement psInsert = null;
		PreparedStatement psUpdate = null;
		PreparedStatement psDelete = null;
		try {
			String sSql;
			DocumentFillInsTable processed = new DocumentFillInsTable();
			
			for (DocumentFillInsRecord postRecord : postTable) {
				String postedAnswer = postRecord.getAnswer();
				ClauseFillInRecord clauseFillInRecord = oClauseTable.findFillInById(postRecord.getClauseFillInId()); // CJ-745
				if (clauseFillInRecord == null) {
					System.out.println("DocumentFillInsTable.save() not ClauseFillInId found error:"
							+ " DocumentId(" + documentId + ")"
							+ " ClauseFillInId(" + postRecord.getClauseFillInId() + ")"
							+ " Answer(" + postedAnswer + ")"
							+ " FullTtextModified(" + postRecord.getFullTtextModified() + ")");
					continue;
				}
				DocumentClauseRecord postedDocumentClauseRecord = postDocumentClauseTable.findByClauseId(clauseFillInRecord.getClauseId()); // CJ-745
				if ((postedDocumentClauseRecord != null) && (!postedDocumentClauseRecord.isRemoved())) { // CJ-745
					DocumentFillInsRecord oRecord = this.findByClauseFillInId(postRecord.getClauseFillInId().intValue());
					if (oRecord == null) {
						//if (oClauseTable.findFillInById(postRecord.getClauseFillInId()) != null) {
							if (psInsert == null) {
								psInsert = DocumentFillInsTable.createInertPrepareStatement(conn);
							}
							psInsert.setInt(1, documentId);
							psInsert.setInt(2, postRecord.getClauseFillInId());
							if (postedAnswer == null)
								psInsert.setNull(3, java.sql.Types.VARCHAR);
							else
								psInsert.setString(3, postedAnswer);
							psInsert.setBoolean(4, postRecord.getFullTtextModified());
							psInsert.execute();
						/*
						} else {
							System.out.println("DocumentFillInsTable.save() not ClauseFillInId found error:"
									+ " DocumentId(" + documentId + ")"
									+ " ClauseFillInId(" + postRecord.getClauseFillInId() + ")"
									+ " Answer(" + postedAnswer + ")"
									+ " FullTtextModified(" + postRecord.getFullTtextModified() + ")");
						}
						*/
					} else {
						if (psUpdate == null) {
							sSql = "UPDATE " + DocumentFillInsRecord.TABLE_DOCUMENT_FILL_INS
								+ " SET " + DocumentFillInsRecord.FIELD_DOCUMENT_FILL_IN_ANSWER + " = ?"
								+ ", " + DocumentFillInsRecord.FIELD_FULL_TEXT_MODIFIED + " = ?"
								+ " WHERE " + DocumentFillInsRecord.FIELD_DOCUMENT_FILL_IN_ID + " = ?";
							psUpdate = conn.prepareStatement(sSql);
						}
						if (CommonUtils.isNotSame(oRecord.getAnswer(), postedAnswer)) {
							if (postedAnswer == null)
								psUpdate.setNull(1, java.sql.Types.VARCHAR);
							else
								psUpdate.setString(1, postedAnswer);
							psUpdate.setBoolean(2, postRecord.getFullTtextModified());
							psUpdate.setInt(3, oRecord.getId());
							psUpdate.execute();
						}
						processed.add(oRecord);
					}
				}
			}
			
			for (DocumentFillInsRecord oRecord : this) {
				if (!processed.contains(oRecord)) {
					if (psDelete == null) {
						sSql = "DELETE FROM " + DocumentFillInsRecord.TABLE_DOCUMENT_FILL_INS
							+ " WHERE " + DocumentFillInsRecord.FIELD_DOCUMENT_FILL_IN_ID + " = ?";
						psDelete = conn.prepareStatement(sSql);
					}
					psDelete.setInt(1, oRecord.getId());
					psDelete.execute();
				}
			}
			
		} finally {
			SqlUtil.closeInstance(psInsert);
			SqlUtil.closeInstance(psUpdate);
			SqlUtil.closeInstance(psDelete);
		}
	}
	
	// CJ-versioning
	// When the document is updated to reflect a new version, the answers must be updated also.
	public static void updateDocVersion(DocumentRecord docOrigRecord, DocumentRecord docUpdRecord, Connection conn) throws SQLException {
		PreparedStatement ps1 = null;
		String sSql = null; 
		try {
			sSql = 
				"INSERT INTO " + DocumentFillInsRecord.TABLE_DOCUMENT_FILL_INS +
				" (" + DocumentFillInsRecord.FIELD_DOCUMENT_ID +
				", " + DocumentFillInsRecord.FIELD_CLAUSE_FILL_IN_ID +
				", " + DocumentFillInsRecord.FIELD_DOCUMENT_FILL_IN_ANSWER +
				", " + DocumentFillInsRecord.FIELD_CREATED_AT +
				", " + DocumentFillInsRecord.FIELD_UPDATED_AT +
				", " + DocumentFillInsRecord.FIELD_FULL_TEXT_MODIFIED +
				// ", " + DocumentFillInsRecord.FIELD_IS_FULL_TEXT +  
				") " + 
				" SELECT ?," + " CFI2." + ClauseFillInRecord.FIELD_CLAUSE_FILL_IN_ID +
					"," + " DFI." + DocumentFillInsRecord.FIELD_DOCUMENT_FILL_IN_ANSWER +
					"," + " DFI." + DocumentFillInsRecord.FIELD_CREATED_AT +
					"," + " DFI." + DocumentFillInsRecord.FIELD_UPDATED_AT +
					"," + " DFI." + DocumentFillInsRecord.FIELD_FULL_TEXT_MODIFIED +
					// "," + " DFI." + DocumentFillInsRecord.FIELD_IS_FULL_TEXT + 
				" FROM " + ClauseFillInRecord.TABLE_CLAUSE_FILL_INS + "  CFI1" +
				" INNER JOIN " + ClauseFillInRecord.TABLE_CLAUSE_FILL_INS + " CFI2" +
					" ON CFI1." + ClauseFillInRecord.FIELD_FILL_IN_CODE + " = CFI2." + ClauseFillInRecord.FIELD_FILL_IN_CODE +
					" AND CFI1." + ClauseFillInRecord.FIELD_FILL_IN_TYPE + " = CFI2." + ClauseFillInRecord.FIELD_FILL_IN_TYPE +
				" INNER JOIN " + ClauseRecord.TABLE_CLAUSES + " C1" +
					" ON CFI1." + ClauseRecord.FIELD_CLAUSE_ID + " = C1." + ClauseRecord.FIELD_CLAUSE_ID +
					" AND C1." + ClauseRecord.FIELD_CLAUSE_VERSION_ID + " = ? " +
				" INNER JOIN " + ClauseRecord.TABLE_CLAUSES + " C2" +
					" ON CFI2." + ClauseRecord.FIELD_CLAUSE_ID + " = C2." + ClauseRecord.FIELD_CLAUSE_ID +
					" AND C2." + ClauseRecord.FIELD_CLAUSE_VERSION_ID + " = ? " +
				" INNER JOIN " + DocumentFillInsRecord.TABLE_DOCUMENT_FILL_INS + " DFI" +
					" ON DFI." + DocumentFillInsRecord.FIELD_CLAUSE_FILL_IN_ID + " = CFI1." + ClauseFillInRecord.FIELD_CLAUSE_FILL_IN_ID +	
				" WHERE DFI." + DocumentFillInsRecord.FIELD_DOCUMENT_ID + " = ?";

			ps1 = conn.prepareStatement(sSql);
			ps1.setInt(1, docUpdRecord.getId());
			ps1.setInt(2, docOrigRecord.getClauseVersionId());
			ps1.setInt(3, docUpdRecord.getClauseVersionId());
			ps1.setInt(4, docOrigRecord.getId());
			ps1.execute();
		}
		catch (Exception oError) {
			System.out.println("DocumentFillInsTable.updateDocVersion(" + docOrigRecord.getId() + ", " + docUpdRecord.getId() + ") failed due to : " + oError.getMessage() + "\n" + sSql);
			throw oError;
		}
		finally {
			SqlUtil.closeInstance(ps1);
		}
	}
		
}
