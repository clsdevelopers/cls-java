package gov.dod.cls.db;

import gov.dod.cls.config.ClsConfig;
import gov.dod.cls.model.JsonAble;
import gov.dod.cls.utils.CommonUtils;
import gov.dod.cls.utils.sql.SqlUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.node.ArrayNode;
import org.codehaus.jackson.node.ObjectNode;

public class QuestionRecord extends JsonAble {
	
	public static final String CODE_DOCUMENT_TYPE = "[DOCUMENT TYPE]";
	public static final String DOCUMENT_TYPE_AWARD = "AWARD";
	public static final String DOCUMENT_TYPE_SOLICITATION = "SOLICITATION";
	public static final String EXPRESSON_SOLICITATION = "\"" + DOCUMENT_TYPE_SOLICITATION + "\" = [DOCUMENT TYPE]";
	
	// -------------------------------------------------------------
	public static final String TABLE_QUESTIONS = "Questions";
	
	public static final String FIELD_QUESTION_ID = "question_id";
	public static final String FIELD_CLAUSE_VERSION_ID = "clause_version_id";
	public static final String FIELD_QUESTION_CODE = "question_code";
	public static final String FIELD_QUESTION_TYPE = "question_type";
	public static final String FIELD_QUESTION_TEXT = "question_text";
	public static final String FIELD_QUESTION_GROUP = "question_group";

	public static final String FIELD_IS_ACTIVE = "is_active";
	public static final String FIELD_CREATED_AT = "created_at";
	public static final String FIELD_UPDATED_AT = "updated_at";

	public static final String SQL_SELECT
		= "SELECT A." + QuestionRecord.FIELD_QUESTION_ID 
		+ ", A." + QuestionRecord.FIELD_QUESTION_CODE
		+ ", A." + QuestionRecord.FIELD_QUESTION_TYPE
		+ ", A." + QuestionRecord.FIELD_QUESTION_GROUP 
		+ ", A." + QuestionRecord.FIELD_QUESTION_TEXT		
		+ ", A." + QuestionRecord.FIELD_CLAUSE_VERSION_ID
		+ ", A." + QuestionRecord.FIELD_IS_ACTIVE
		+ ", A." + QuestionRecord.FIELD_CREATED_AT
		+ ", A." + QuestionRecord.FIELD_UPDATED_AT
		+ ", B." + QuestionGroupRefRecord.FIELD_QUESTION_GROUP_NAME 
		+ ", C." + QuestionTypeRefRecord.FIELD_QUESTION_TYPE_NAME
		+ ", D." + ClauseVersionRecord.FIELD_CLAUSE_VERSION_NAME
		+ " FROM " + QuestionRecord.TABLE_QUESTIONS + " A" 
		+ " LEFT OUTER JOIN " + QuestionGroupRefRecord.TABLE_QUESTION_GROUP_REF + " B"  
		+ " ON (B." + QuestionGroupRefRecord.FIELD_QUESTION_GROUP + " = A." + QuestionRecord.FIELD_QUESTION_GROUP + ")"
		+ " LEFT OUTER JOIN " + QuestionTypeRefRecord.TABLE_QUESTION_TYPE_REF + " C"
		+ " ON (C." + QuestionTypeRefRecord.FIELD_QUESTION_TYPE + " = A." + QuestionRecord.FIELD_QUESTION_TYPE + ")"
		+ " LEFT OUTER JOIN " + ClauseVersionRecord.TABLE_CLAUSE_VERSIONS + " D"  
		+ " ON (D." + ClauseVersionRecord.FIELD_CLAUSE_VERSION_ID + " = A." + QuestionRecord.FIELD_CLAUSE_VERSION_ID + ")";
	
	public static final String SQL_CHOICE_SELECT
		= "SELECT " + QuestionChoiceRecord.FIELD_CHOICE_TEXT
		+ " FROM " + QuestionChoiceRecord.TABLE_QUESTION_CHOICES
		+ " WHERE " + QuestionChoiceRecord.FIELD_QUESTION_ID + " = ?"
		+ " ORDER BY " + QuestionChoiceRecord.FIELD_QUESTION_CHOCE_ID;
	
	public static final String SQL_CONDITION_SELECT
		= "SELECT " + QuestionConditionRecord.FIELD_ORIGINAL_CONDITION_TEXT
		+ ", " + QuestionConditionRecord.FIELD_CONDITION_TO_QUESTION
		+ ", " + QuestionConditionRecord.FIELD_QUESTION_CONDITION_SEQUENCE
		+ ", " + QuestionConditionRecord.FIELD_DEFAULT_BASELINE // CJ-584
		+ " FROM " + QuestionConditionRecord.TABLE_QUESTION_CONDITIONS
		+ " WHERE " + QuestionConditionRecord.FIELD_QUESTION_ID + " = ?"
		+ " ORDER BY " + QuestionConditionRecord.FIELD_QUESTION_CONDITION_ID;
	
	public static final String SQL_PRESCRIPTION_SELECT
		= "SELECT A." + QuestionChoiceRecord.FIELD_CHOICE_TEXT
		+ ", C." + PrescriptionRecord.FIELD_PRESCRIPTION_ID
		+ ", C." + PrescriptionRecord.FIELD_PRESCRIPTION_NAME
		+ ", C." + PrescriptionRecord.FIELD_PRESCRIPTION_URL
		+ " FROM " + QuestionChoiceRecord.TABLE_QUESTION_CHOICES + " A"		
		+ " LEFT OUTER JOIN " + QuestionChoicePrescriptionRecord.TABLE_QUESTION_CHOICE_PRESCRIPTIONS + " B"  
		+ " ON (B." + QuestionChoicePrescriptionRecord.FIELD_QUESTION_CHOCE_ID + " = A." + QuestionChoiceRecord.FIELD_QUESTION_CHOCE_ID + ")"		
		+ " LEFT OUTER JOIN " + PrescriptionRecord.TABLE_PRESCRIPTIONS + " C"  
		+ " ON (C." + PrescriptionRecord.FIELD_PRESCRIPTION_ID + " = B." + QuestionChoicePrescriptionRecord.FIELD_PRESCRIPTION_ID + ")"		
		+ " WHERE A." + QuestionChoiceRecord.FIELD_QUESTION_ID + " = ?"
		+ " ORDER BY A." + QuestionChoiceRecord.FIELD_QUESTION_CHOCE_ID
		+ ", C." + PrescriptionRecord.FIELD_PRESCRIPTION_NAME;
			
/*	
	public static final String SQL_SELECT
		= "SELECT " + FIELD_QUESTION_ID
		+ ", " + FIELD_CLAUSE_VERSION_ID
		+ ", " + FIELD_QUESTION_CODE
		+ ", " + FIELD_QUESTION_GROUP
		+ ", " + FIELD_IS_ACTIVE
		+ ", " + FIELD_QUESTION_TYPE
		+ ", " + FIELD_CREATED_AT
		+ ", " + FIELD_UPDATED_AT
		+ ", " + FIELD_QUESTION_TEXT
		+ " FROM " + TABLE_QUESTIONS;
*/			
	private Integer id; 
	private Integer clauseVersionId; 
	private String questionCode; 
	private String questionType; 
	private String questionText;
	private String questionGroup;
	private String questionTypeName;
	private String questionGroupName;
	private Boolean active; 
	private Date createdAt; 
	private Date updatedAt;
	private QuestionChoiceTable questionChoiceTable = null;
	private static ArrayNode arrayNodeChoice;
	// private static ArrayNode arrayNodeCondition1;
	// private static ArrayNode arrayNodeCondition2;
	private String originalConditionText;
	private String conditionToQuestion;
	private Integer questionConditionSeq;
	private boolean defaultBaseline = false;
	private String clauseVersionName;

	private static ArrayNode arrayNodePres;

	public void read(ResultSet rs) throws SQLException {
		this.id = rs.getInt(FIELD_QUESTION_ID); 
		this.questionCode = rs.getString(FIELD_QUESTION_CODE);
		this.active = rs.getBoolean(FIELD_IS_ACTIVE);
		this.questionType = rs.getString(FIELD_QUESTION_TYPE);
		this.questionText = rs.getString(FIELD_QUESTION_TEXT);
		this.questionGroup = rs.getString(FIELD_QUESTION_GROUP);
		this.createdAt = CommonUtils.toDateTime(rs, FIELD_CREATED_AT);
		this.updatedAt = CommonUtils.toDateTime(rs, FIELD_UPDATED_AT); 
		this.clauseVersionId = CommonUtils.toInteger(rs.getObject(FIELD_CLAUSE_VERSION_ID));
		this.questionTypeName = rs.getString("question_type_name");
		this.questionGroupName = rs.getString(QuestionGroupRefRecord.FIELD_QUESTION_GROUP_NAME);	
		this.clauseVersionName = rs.getString(ClauseVersionRecord.FIELD_CLAUSE_VERSION_NAME);
	}
	
	public void readCondition(ResultSet rs) throws SQLException {
		this.originalConditionText = rs.getString(QuestionConditionRecord.FIELD_ORIGINAL_CONDITION_TEXT);
		this.conditionToQuestion = rs.getString(QuestionConditionRecord.FIELD_CONDITION_TO_QUESTION);		
		this.questionConditionSeq = rs.getInt(QuestionConditionRecord.FIELD_QUESTION_CONDITION_SEQUENCE);
		this.defaultBaseline = rs.getBoolean(QuestionConditionRecord.FIELD_DEFAULT_BASELINE); // CJ-584
	}

	@Override
	protected void populateJson(ObjectNode json, boolean forInterview) {
		json.put(FIELD_QUESTION_ID, this.id);
		json.put(FIELD_QUESTION_CODE, this.questionCode);
		json.put(FIELD_QUESTION_TYPE, this.questionType);
		json.put(FIELD_QUESTION_TEXT, this.questionText);
		json.put(FIELD_QUESTION_GROUP, this.questionGroup);
		if (forInterview) {
			json.put("choices", (questionChoiceTable == null) ? null : questionChoiceTable.toJson(null, forInterview));
		} else {
			json.put(FIELD_IS_ACTIVE, this.active);
			json.put(FIELD_CLAUSE_VERSION_ID, this.clauseVersionId);
			json.put(FIELD_CREATED_AT, CommonUtils.toJsonDate(this.createdAt));
			json.put(FIELD_UPDATED_AT, CommonUtils.toJsonDate(this.updatedAt));
			json.put("question_type_name", this.questionTypeName);
			json.put(QuestionGroupRefRecord.FIELD_QUESTION_GROUP_NAME, this.questionGroupName);
			json.put(ClauseVersionRecord.FIELD_CLAUSE_VERSION_NAME, this.clauseVersionName);			
			json.put(QuestionChoiceRecord.TABLE_QUESTION_CHOICES, arrayNodeChoice);			
			json.put(QuestionConditionRecord.FIELD_ORIGINAL_CONDITION_TEXT, this.originalConditionText);
			json.put(QuestionConditionRecord.FIELD_CONDITION_TO_QUESTION, this.conditionToQuestion);
			json.put(QuestionConditionRecord.FIELD_QUESTION_CONDITION_SEQUENCE, this.questionConditionSeq);
			
			json.put(QuestionConditionRecord.FIELD_DEFAULT_BASELINE, this.defaultBaseline); // CJ-584
			
			json.put(PrescriptionRecord.TABLE_PRESCRIPTIONS, arrayNodePres);			
			//json.put(QuestionConditionRecord.FIELD_ORIGINAL_CONDITION_TEXT, arrayNodeCondition1);
			//json.put(QuestionConditionRecord.FIELD_CONDITION_TO_QUESTION, arrayNodeCondition2);
		}
	}
	
	public void populateJsonForApi(boolean bForVer1, ObjectNode json) {
		if (bForVer1) {
			json.put("id", this.id);
			json.put("question_name", this.questionCode);
			json.put("question_type", this.questionType);
			json.put("question_text", this.questionText);
		} else {
			json.put(FIELD_QUESTION_ID, this.id);
			json.put(FIELD_QUESTION_CODE, this.questionCode);
			json.put(FIELD_QUESTION_TYPE, this.questionType);
			json.put(FIELD_QUESTION_TEXT, this.questionText);
			json.put(FIELD_QUESTION_GROUP, this.questionGroup);
			json.put(FIELD_IS_ACTIVE, this.active);
			json.put(FIELD_CLAUSE_VERSION_ID, this.clauseVersionId);
			json.put(FIELD_CREATED_AT, CommonUtils.toZulu(this.createdAt));
			json.put(FIELD_UPDATED_AT, CommonUtils.toZulu(this.updatedAt));
		}
	}
	
	public static QuestionRecord getRecord(Connection conn, Integer questionId, QuestionRecord pQuestionRecord) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			String sSql = SQL_SELECT;
			String operator = " WHERE ";
			if (questionId != null) {
				// sSql += operator + " " + FIELD_QUESTION_ID + "=?";
				sSql += operator + " A." + FIELD_QUESTION_ID + "=?";
				// operator = " AND ";
			}
			ps = conn.prepareStatement(sSql);
			int iParam = 1;
			if (questionId != null)
				ps.setInt(iParam++, questionId);
			rs = ps.executeQuery();
			if (rs.next()) {
				if (pQuestionRecord == null)
					pQuestionRecord = new QuestionRecord();
				pQuestionRecord.read(rs);
			}
			//---------------
			SqlUtil.closeInstance(rs, ps);
			rs = null;
			ps = null;
			ps = conn.prepareStatement(SQL_CHOICE_SELECT);
			ObjectMapper mapper = new ObjectMapper();
			ObjectNode json = mapper.createObjectNode();
			arrayNodeChoice = json.arrayNode();
			ps.setInt(1, questionId);
			rs = ps.executeQuery();
			while (rs.next()) {
				arrayNodeChoice.add(rs.getString(QuestionChoiceRecord.FIELD_CHOICE_TEXT));
			}
			rs.close();
			rs = null;
			// json.put(QuestionChoiceRecord.TABLE_QUESTION_CHOICES, arrayNode);
			
			ps = conn.prepareStatement(SQL_CONDITION_SELECT);
			ps.setInt(1, questionId);
			rs = ps.executeQuery();
			if (rs.next()) {
				if (pQuestionRecord == null)
					pQuestionRecord = new QuestionRecord();
				pQuestionRecord.readCondition(rs);
			}
			// while (rs.next()) {
			//	arrayNodeCondition1.add(rs.getString(QuestionConditionRecord.FIELD_ORIGINAL_CONDITION_TEXT));
			//	arrayNodeCondition2.add(rs.getString(QuestionConditionRecord.FIELD_CONDITION_TO_QUESTION)); 
			// }
			SqlUtil.closeInstance(rs, ps);
			rs.close();
			rs = null;
			
			ps = conn.prepareStatement(SQL_PRESCRIPTION_SELECT);
			mapper = new ObjectMapper();
			json = mapper.createObjectNode();
			arrayNodePres = json.arrayNode();
			ps.setInt(1, questionId);
			rs = ps.executeQuery();
			while (rs.next()) {
				ObjectNode elementNode = json.objectNode();
				load(rs, elementNode);
				arrayNodePres.add(elementNode);
				// arrayNodeChoice.add(rs.getString(QuestionChoiceRecord.FIELD_CHOICE_TEXT));
			}
			SqlUtil.closeInstance(rs, ps);
			rs.close();
			rs = null;
		}
		catch (Exception oError) {
			oError.printStackTrace();
		}
		finally {
			SqlUtil.closeInstance(rs, ps);
		}
		return pQuestionRecord;
	}
	
	private static void load(ResultSet rs, ObjectNode elementNode) throws SQLException {
		elementNode.put(QuestionChoiceRecord.FIELD_CHOICE_TEXT, rs.getString(QuestionChoiceRecord.FIELD_CHOICE_TEXT));
		elementNode.put(PrescriptionRecord.FIELD_PRESCRIPTION_ID, rs.getString(PrescriptionRecord.FIELD_PRESCRIPTION_ID));
		elementNode.put(PrescriptionRecord.FIELD_PRESCRIPTION_NAME, rs.getString(PrescriptionRecord.FIELD_PRESCRIPTION_NAME));
		elementNode.put(PrescriptionRecord.FIELD_PRESCRIPTION_URL, rs.getString(PrescriptionRecord.FIELD_PRESCRIPTION_URL));
	}	
	
	public static QuestionRecord getRecord(Integer id) {
		if (id == null) 
			return null;
		
		Connection conn = null;
		try {
			conn = ClsConfig.getDbConnection();
			return QuestionRecord.getRecord(conn, id, null);
		}
		catch (Exception oError) {
			oError.printStackTrace();
		}
		finally {
			SqlUtil.closeInstance(conn);
		}
		return null;
	}
	
	// -----------------------------------------------------
	public Integer getId() { return id; }

	public Integer getClauseVersionId() { return clauseVersionId; }
	public void setClauseVersionId(Integer clauseVersionId) { this.clauseVersionId = clauseVersionId; }
	
	public String getQuestionCode() { return questionCode; }
	public void setQuestionCode(String questionCode) { this.questionCode = questionCode; }
	
	public Boolean getActive() { return active; }
	public boolean isActive() {
		if (this.active == null)
			return false;
		return this.active.booleanValue();
	}
	public void setActive(Boolean active) { this.active = active; }
	
	public String getQuestionType() { return questionType; }
	public void setQuestionType(String questionType) { this.questionType = questionType; }
	
	public String getQuestionGroup() {
		return questionGroup;
	}

	public String getQuestionText() { return questionText; }
	public void setQuestionText(String questionText) { this.questionText = questionText; }
	
	public String getConditionToQuestion() { return this.conditionToQuestion; }
	public boolean isSimpleSolicitationQuestion() {
		if (this.conditionToQuestion == null)
			return false;
		return this.conditionToQuestion.startsWith(EXPRESSON_SOLICITATION);
	}
	
	public Date getCreatedAt() { return createdAt; }
	public void setCreatedAt(Date createdAt) { this.createdAt = createdAt; }
	
	public Date getUpdatedAt() { return updatedAt; }
	public void setUpdatedAt(Date updatedAt) { this.updatedAt = updatedAt; }

	public QuestionChoiceTable getQuestionChoiceTable() { return questionChoiceTable; }
	public void setQuestionChoiceTable(QuestionChoiceTable questionChoiceTable) { this.questionChoiceTable = questionChoiceTable; }

	public boolean isDefaultBaseline() {
		return defaultBaseline;
	}

	public void setDefaultBaseline(boolean defaultBaseline) {
		this.defaultBaseline = defaultBaseline;
	}
	
	public boolean isBaseQuestion(QuestionConditionTable questionConditionTable) { // CJ-581
		QuestionConditionRecord questionConditionRecord = questionConditionTable.findByQuestion(this);
		return (questionConditionRecord != null) && questionConditionRecord.isBaseQuestion();
	}
	
	public ObjectNode toJsonForVersionApi(ObjectMapper mapper, QuestionGroupRefTable questionGroupRefTable,
			QuestionConditionRecord questionConditionRecord // QuestionConditionTable questionConditionTable
			) {
		ObjectNode result = mapper.createObjectNode();
		// this.populateJsonForApi(false, result);

		result.put(FIELD_QUESTION_ID, this.id);
		result.put(FIELD_QUESTION_GROUP, this.questionGroup);
		result.put(QuestionGroupRefRecord.FIELD_QUESTION_GROUP_NAME, questionGroupRefTable.getNameByCode(this.questionGroup));
		result.put(FIELD_QUESTION_CODE, this.questionCode);
		result.put(FIELD_QUESTION_TEXT, this.questionText);
		result.put(FIELD_QUESTION_TYPE, this.questionType);
		
		// QuestionConditionRecord questionConditionRecord = questionConditionTable.findByQuestion(this);
		boolean isBaseQuestion = false;
		String originalConditionText = null;
		if (questionConditionRecord != null) {
			isBaseQuestion = questionConditionRecord.isBaseQuestion(); // CommonUtils.isEmpty(questionConditionRecord.getConditionToQuestion());
			originalConditionText = questionConditionRecord.getOriginalConditionText();
		}
		result.put("is_base_question", isBaseQuestion);
		result.put("question_condition_text", originalConditionText);
		
		// result.put(FIELD_IS_ACTIVE, this.active);
		// result.put(FIELD_CLAUSE_VERSION_ID, this.clauseVersionId);
		result.put(FIELD_CREATED_AT, CommonUtils.toZulu(this.createdAt));
		result.put(FIELD_UPDATED_AT, CommonUtils.toZulu(this.updatedAt));
		
		ArrayNode anChoices = mapper.createArrayNode();
		if (questionChoiceTable != null)
			for (QuestionChoiceRecord oChoiceRecord : questionChoiceTable) {
				anChoices.add(oChoiceRecord.getChoiceText());
			}
		result.put("choices", anChoices);
		
		return result;
	}
}
