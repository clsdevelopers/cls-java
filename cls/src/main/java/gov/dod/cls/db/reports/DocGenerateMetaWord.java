package gov.dod.cls.db.reports;

import java.math.BigInteger;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.xwpf.usermodel.ParagraphAlignment;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.apache.poi.xwpf.usermodel.XWPFStyles;
import org.apache.poi.xwpf.usermodel.XWPFTable;
import org.apache.poi.xwpf.usermodel.XWPFTableCell;
import org.apache.poi.xwpf.usermodel.XWPFTableRow;
import org.jsoup.Jsoup;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTFonts;

import gov.dod.cls.PageApi;
import gov.dod.cls.bean.utils.DocGenerateDoc;
import gov.dod.cls.db.ClauseFillInTable;
import gov.dod.cls.db.ClauseRecord;
import gov.dod.cls.utils.CommonUtils;


public class DocGenerateMetaWord {
	
	public static final int 	TITLE_HEADER_FONT_SIZE = 14; // 14
	public static final int 	DEFAULT_FONT_SIZE = 12;
	
	ArrayList<ClauseRecord> clauseArray = new ArrayList<ClauseRecord>();
	
	public static void downloadDoc(ArrayList<ClauseRecord> clsArray, HttpServletRequest req, HttpServletResponse resp) {
		DocGenerateMetaWord docGenerateDoc = new DocGenerateMetaWord(clsArray);
		try {
			PageApi.send(resp, 
					"attachment; filename=cls_clauses.docx",
					"application/msword",
					docGenerateDoc.createDoc());
		} catch (Exception oError) {
			oError.printStackTrace();
			PageApi.send(resp, "text/plain", oError.getMessage());
		}
	}
	
	public DocGenerateMetaWord(ArrayList<ClauseRecord> clsArray) {
		
		this.clauseArray.addAll(clsArray);
	}
	
	public XWPFDocument createDoc() throws Exception {
		XWPFDocument document = new XWPFDocument();
        
        CTFonts fonts = CTFonts.Factory.newInstance();
        fonts.setHAnsi("Times New Roman");

        XWPFStyles xWpfStyles = document.createStyles();
        xWpfStyles.setDefaultFonts(fonts);
    
        printSelectedClauses (document);
        
        return document;
	}
	
    private  void printSelectedClauses (XWPFDocument document) throws Exception {
    	
        printTitle ("Metadata for Selected Clauses", document);
        
        for (Integer item = 0; item<this.clauseArray.size(); item++) {
        	
            XWPFTable table = document.createTable();
            table.getCTTbl().getTblPr().unsetTblBorders();
            
            ClauseRecord rec = this.clauseArray.get(item);
        
	        printFirstNameAndValue("Clause Id:", rec.getId().toString(), table);
	        printNameAndValue("Section:", rec.getSectionCode() + " - " + rec.getSectionHeader(), table);
	        printNameAndValue("Regulation:", rec.getRegulationName(), table);
	        printNameAndValue("Clause Name:", rec.getClauseName(), table);
	        printNameAndValue("Clause Title:", rec.getClauseTitle(), table);
	        printNameAndValue("Effective Date:", CommonUtils.toMonthYear(rec.getEffectiveDate()), table);
	        printNameAndValue("Provision or Clause:", (rec.getProvisonOrClause().equals("C") ? "Clause" : "Provision"), table);
	        printNameAndValue("Inclusion Code:", rec.getInclusion()  + " - " + rec.getInclusionName(), table);
	        printNameAndValue("Clause URL:", rec.getUrl(), table);
	        printNameAndValue("Clause Version:", rec.getClauseVersionName(), table);
	        printNameAndValue("Is Active:", (rec.isActive() ? "True" : "False"), table);
	        printNameAndValue("Is Editable:", (rec.isEditable() ? "True" : "False"), table);
	        printNameAndValue("Editable Remarks:", rec.getEditableRemarks(), table);
	        printNameAndValue("Is Optional:", (rec.isOptional() ? "True" : "False"), table);
	        printNameAndValue("Commercial Status:", rec.getCommercialStatus(), table);
	        printNameAndValue("Is Basic Clause:", (rec.isBasicClause() ? "True" : "False"), table);
	        
	        String sConditions = formatConditions (rec.getClauseRule(), rec.getClauseConditions());
	        printNameAndHTMLString("Clause Conditions:", sConditions, table);
	        
	        printNameAndValue("Clause Rule:", rec.getClauseRule(), table);
	        printNameAndValue("Additional Conditions:", rec.getAdditionalConditions(), table);
	        
	        String sPrescriptions = "";
	        for (Integer i = 0; i< rec.getPrescriptionTable().size(); i++) {
	        	sPrescriptions += ((sPrescriptions.length() > 0) ? "; " : "") +  rec.getPrescriptionTable().get(i).getName();
	        }
	        printNameAndValue("Prescriptions:", sPrescriptions, table);
	        
	        printNameAndHTMLString("Clause Data:", rec.getClauseData(), table);
	        printFillinTable("Fill Ins:", rec.getClauseFillInTable(), table);
	        printNameAndValue("Created At:", CommonUtils.toDateTime12Hour(rec.getCreatedAt()), table);
	        printNameAndValue("Updated At:", CommonUtils.toDateTime12Hour(rec.getUpdatedAt()), table);
	        
	        // evenly space the table columns.
	        int[] cols = {3000, 12000}; 
	        for (int i = 0; i < table.getNumberOfRows(); i++){
	            XWPFTableRow row = table.getRow(i);
	            int numCells = row.getTableCells().size();
	            for (int j = 0; j < numCells; j++){
	                XWPFTableCell cell = row.getCell(j);
	                cell.getCTTc().addNewTcPr().addNewTcW().setW(BigInteger.valueOf(cols[j]));
	            }
	        }
	        
	        printSeparator(document);
        }
    }
	
	private void printTitle(String sTitle, XWPFDocument document) {

		XWPFParagraph paragraph;
        
		paragraph = document.createParagraph();
        paragraph.setAlignment(ParagraphAlignment.CENTER); 
        XWPFRun runTitle = paragraph.createRun();
        runTitle.setText(sTitle);
        runTitle.setBold(true);
        runTitle.setFontSize(TITLE_HEADER_FONT_SIZE); 
        runTitle.addBreak();
        
        XWPFRun runTimestamp = paragraph.createRun();
        runTimestamp.setText(CommonUtils.toDateTimeBrief(CommonUtils.getNow()));
        runTimestamp.setBold(false);
        runTimestamp.setFontSize(DEFAULT_FONT_SIZE); 
        runTimestamp.addBreak();
        runTimestamp.addBreak();

	}
	
	private void printSeparator(XWPFDocument document) {

		XWPFParagraph paragraph;
		String sSep = "--------------------------------------------------------------------------------";
		
		paragraph = document.createParagraph();
        paragraph.setAlignment(ParagraphAlignment.CENTER); 
        XWPFRun runTitle = paragraph.createRun();
        runTitle.addBreak();
        runTitle.setText(sSep);
        runTitle.setBold(true);
        runTitle.setFontSize(DEFAULT_FONT_SIZE); 
        runTitle.addBreak();


	}
	
	// Row is created differently for the first row.
	private void printFirstNameAndValue(String sName, String sValue, XWPFTable table ) {
		XWPFTableRow nextRow = table.getRow(0);
        nextRow.addNewTableCell();
        printTblText(sName, true, nextRow.getCell(0));
		printTblText(sValue, false, nextRow.getCell(1));
	}
	
	private void printNameAndValue(String sName, String sValue, XWPFTable table ) {
		XWPFTableRow nextRow = table.createRow();
		printTblText(sName + " ", true, nextRow.getCell(0));
		printTblText(sValue, false, nextRow.getCell(1));
	}
	
	private void printNameAndHTMLString(String sName, String sValue, XWPFTable table ) {
		XWPFTableRow nextRow = table.createRow();
		printTblText(sName + " ", true, nextRow.getCell(0));
		
		String [] clauseTextElements = sValue.split("<p>");
        for (String element : clauseTextElements) {
        	if ((element == null) || (element.length() == 0))
        		continue;
        	printTblText(Jsoup.parse(element).text(), false, nextRow.getCell(1));
        	//printTblBreak(nextRow.getCell(1));
        }
	}
	
	private void printTblText(String text, boolean bold, XWPFTableCell cell) {
		
		XWPFParagraph newPara = cell.addParagraph();
		XWPFRun run = newPara.createRun(); 
		run.setBold(bold);
		run.setText(text);
		run.setFontSize(DEFAULT_FONT_SIZE); 
		newPara.setAlignment(ParagraphAlignment.LEFT); 
	}
	
	private void printTblBreak(XWPFTableCell cell) {
		
		XWPFParagraph newPara = cell.addParagraph();
		XWPFRun run = newPara.createRun(); 
		run.addBreak();
	}
	
	private void printFillinTable(String sName, ClauseFillInTable clsTable, XWPFTable table ) {
		XWPFTableRow nextRow = table.createRow();
		printTblText(sName + " ", true, nextRow.getCell(0));
        
		if (clsTable.size() == 0) {
			printTblText(" ", false, nextRow.getCell(1));
			return;
		}
		
		for (Integer item = 0; item<clsTable.size(); item++) {
			printTblText("Code: " + clsTable.get(item).getFillInCode() , false, nextRow.getCell(1));
			printTblText("Type: " + clsTable.get(item).getFillInType(), false, nextRow.getCell(1));
			printTblText("Max Size: " + clsTable.get(item).getFillInMaxSize(), false, nextRow.getCell(1));
			printTblText("Group Number: " + clsTable.get(item).getFillInGroupNumber(), false, nextRow.getCell(1));
			printTblText("Place Holder: " + clsTable.get(item).getFillInPlaceholder(), false, nextRow.getCell(1));
			printTblText("Display Rows: " + clsTable.get(item).getFillInDisplayRows(), false, nextRow.getCell(1));
			printTblText("For Table: " + (clsTable.get(item).isFillInForTable() ? "True" : "False"), false, nextRow.getCell(1));
			printTblText("Heading: " + clsTable.get(item).getFillInHeading(), false, nextRow.getCell(1));
			printTblText("Default Data: " + clsTable.get(item).getFillInDefaultData(), false, nextRow.getCell(1));
			printTblBreak (nextRow.getCell(1));
		}
	}
	
	// Condition strings need to be formatted, since some contain newlines per condition and others do not.
	// Sample Input: (A) DOCUMENT TYPE IS: SOLICITATION (B) DOCUMENT TYPE IS: AWARD (C) ACO RESPONSIBILITIES IS: APPOINTMENT OF A CONTRACTING OFFICER'S REPRESENTATIVE IS ANTICIPATED
	// Sample Output:	(A) DOCUMENT TYPE IS: SOLICITATION 
	//					(B) DOCUMENT TYPE IS: AWARD 
	//					(C) ACO RESPONSIBILITIES IS: APPOINTMENT OF A CONTRACTING OFFICER'S REPRESENTATIVE IS ANTICIPATED
	private String formatConditions (String sClauseRules, String sClauseConditions) {
		
        String sRules = sClauseRules;
        String sConditions = sClauseConditions;
        sConditions = sConditions.replaceAll("\\n", "");
        
        // Format rules first.
        // Input: (A OR B) AND C
        // Output: A, B, C
        sRules = sRules.replaceAll("\\(", "");
        sRules = sRules.replaceAll("\\)", "");
        sRules = sRules.replaceAll("OR", "");
        sRules = sRules.replaceAll("AND", "");
        sRules = sRules.replaceAll("!", "");
        Integer iPos = sRules.indexOf("  ");
        while (iPos >= 0) {
        	sRules = sRules.replaceAll("  ", " ");
        	iPos = sRules.indexOf("  ");
        }
        
        // Find each matching rule in the condition string, and insert a newline.
        String[] aRules = sRules.split(" ");
        Boolean bFirst = true;
        for (String element : aRules) {
        	if ((element == null) || (element.length() == 0))
        		continue;
        	if (bFirst) {
        		bFirst = false;
        		continue;
        	}
        	element = "(" +  element + ")";
        	sConditions = sConditions.replace(element, "<p>" + element);
        }
        
        return sConditions;
	}
}