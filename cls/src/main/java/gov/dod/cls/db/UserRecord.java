package gov.dod.cls.db;

import gov.dod.cls.config.ClsConfig;
import gov.dod.cls.db.utils.UserRoles;
import gov.dod.cls.model.JsonAble;
import gov.dod.cls.utils.CommonUtils;
import gov.dod.cls.utils.Encryptor;
import gov.dod.cls.utils.sql.SqlInsert;
import gov.dod.cls.utils.sql.SqlUpdate;
import gov.dod.cls.utils.sql.SqlUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.TimeZone;

import gov.dod.cls.db.RoleRecord.ROLE_ID;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.node.ObjectNode;

public class UserRecord extends JsonAble implements java.io.Serializable {

	private static final long serialVersionUID = -4701127095289357329L;

	public final static String TABLE_USERS = "Users";
	
	public final static String FIELD_USER_ID = "User_Id";
	public final static String FIELD_EMAIL = "email";
	public final static String FIELD_FIRST_NAME = "first_name";
	public final static String FIELD_LAST_NAME = "last_name";
	public final static String FIELD_PASSWORD = "password";
	public final static String FIELD_PASSWORD_UPDATED_AT = "password_updated_at";
	public final static String FIELD_LOGON_COUNT = "logon_count";
	public final static String FIELD_CURRENT_LOGON_AT = "current_logon_at";
	public final static String FIELD_CURRENT_LOGON_IP = "current_logon_ip";
	public final static String FIELD_LAST_LOGON_AT = "last_logon_at";
	public final static String FIELD_LAST_LOGON_IP = "last_logon_ip";
	public final static String FIELD_RESET_PASSWORD_TOKEN = "reset_password_token";
	public final static String FIELD_RESET_PASSWORD_SENT_AT = "reset_password_sent_at";
	public final static String FIELD_CAC_HASH = "cac_hash";
	public final static String FIELD_TIME_ZONE = "time_zone";
	public final static String FIELD_ORG_ID = "org_id";
	public final static String FIELD_CREATED_AT = "created_at";
	public final static String FIELD_UPDATED_AT = "updated_at";
	public final static String FIELD_IS_ACTIVE = "is_active";  
	
	public final static String SQL_SELECT
		= "SELECT A." + FIELD_USER_ID
		+ ", A." + FIELD_EMAIL
		+ ", A." + FIELD_FIRST_NAME
		+ ", A." + FIELD_LAST_NAME
		+ ", A." + FIELD_PASSWORD
		+ ", A." + FIELD_PASSWORD_UPDATED_AT
		+ ", A." + FIELD_LOGON_COUNT
		+ ", A." + FIELD_CURRENT_LOGON_AT
		+ ", A." + FIELD_CURRENT_LOGON_IP
		+ ", A." + FIELD_LAST_LOGON_AT
		+ ", A." + FIELD_LAST_LOGON_IP
		+ ", A." + FIELD_CREATED_AT
		+ ", A." + FIELD_UPDATED_AT
		+ ", A." + FIELD_RESET_PASSWORD_TOKEN
		+ ", A." + FIELD_RESET_PASSWORD_SENT_AT
		+ ", A." + FIELD_CAC_HASH
		+ ", A." + FIELD_TIME_ZONE
		+ ", A." + FIELD_IS_ACTIVE  
		+ ", A." + FIELD_ORG_ID
		// + ", B." + OrgRecord.FIELD_ORG_NAME + " AS OrgName"
		+ ", B." + OrgRecord.FIELD_ORG_NAME
		+ ", B." + OrgRecord.FIELD_DEPT_ID
		+ ", C." + DeptRecord.FIELD_DEPT_NAME
		+ " FROM " + TABLE_USERS + " A"
		+ " INNER JOIN " + OrgRecord.TABLE_ORGS + " B ON (B." + OrgRecord.FIELD_ORG_ID + "=A." + FIELD_ORG_ID + ")"
		+ " INNER JOIN " + DeptRecord.TABLE_DEPTS + " C ON (C." + DeptRecord.FIELD_DEPT_ID + "=B." + OrgRecord.FIELD_DEPT_ID + ")";
		
		// + " LEFT OUTER JOIN " + OrgRecord.TABLE_ORGS + " B"
		// + " ON (B." + OrgRecord.FIELD_ORG_ID + "=A." + FIELD_ORG_ID + ")";
	
	//private final static String PASSWORD_PATTERN =
			//"^(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])\\w{8,}$";
	//private static Pattern passwordPattern = Pattern.compile(UserRecord.PASSWORD_PATTERN);
	public static boolean isValidPassword(String password) {
		return true;
		//Matcher matcher = UserRecord.passwordPattern.matcher(password);
		//return matcher.matches();
	}
	
	//public static UserRecord getRecord(Connection conn, Integer id, String email) {
	public static UserRecord getRecord(Connection conn, Integer id, String email, UserRecord pUserRecord) {
		//UserRecord usersRecord = pUserRecord;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			String sSql = SQL_SELECT;
			String operator = " WHERE ";
			if (id != null) {
				sSql += operator + "A." + FIELD_USER_ID + "=?";
				operator = " AND ";
			}
			if (email != null) {
				sSql += operator + "LOWER(A." + FIELD_EMAIL + ")=?";
				email = email.toLowerCase();
			}
			ps = conn.prepareStatement(sSql);
			int iParam = 1;
			if (id != null)
				ps.setInt(iParam++, id);
			if (email != null)
				ps.setString(iParam++, email);
			rs = ps.executeQuery();
			if (rs.next()) {
				if (pUserRecord == null)
					pUserRecord = new UserRecord();
				pUserRecord.read(rs);
				pUserRecord.roles.load(pUserRecord.id, conn);
			}
		}
		catch (Exception oError) {
			oError.printStackTrace();
		}
		finally {
			SqlUtil.closeInstance(rs, ps);
		}
		return pUserRecord;
	}
	
	public static UserRecord getRecordByCac(Connection conn, Integer id, String cacHash, UserRecord pUserRecord) {
		//UserRecord usersRecord = pUserRecord;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			String sSql = SQL_SELECT;
			String operator = " WHERE ";
			if (id != null) {
				sSql += operator + "A." + FIELD_USER_ID + "=?";
				operator = " AND ";
			}
			if (cacHash != null) {
				sSql += operator + "LOWER(A." + FIELD_CAC_HASH + ")=?";
				cacHash = cacHash.toLowerCase();
			}
			ps = conn.prepareStatement(sSql);
			int iParam = 1;
			if (id != null)
				ps.setInt(iParam++, id);
			if (cacHash != null)
				ps.setString(iParam++, cacHash);
			rs = ps.executeQuery();
			if (rs.next()) {
				if (pUserRecord == null)
					pUserRecord = new UserRecord();
				pUserRecord.read(rs);
				pUserRecord.roles.load(pUserRecord.id, conn);
			}
		}
		catch (Exception oError) {
			oError.printStackTrace();
		}
		finally {
			SqlUtil.closeInstance(rs, ps);
		}
		return pUserRecord;
	}

	public static UserRecord getRecord(Integer id, String email, Integer orgId) {
		if ((id == null) && CommonUtils.isEmpty(email))
			return null;
		
		Connection conn = null;
		try {
			conn = ClsConfig.getDbConnection();
			UserRecord oUserRecord = UserRecord.getRecord(conn, id, email, null);
			if (oUserRecord != null) { // CJ-680
				if ((orgId != null) && (!orgId.equals(oUserRecord.getOrgId())))
					oUserRecord = null;
			}
			return oUserRecord;
		}
		catch (Exception oError) {
			oError.printStackTrace();
		}
		finally {
			SqlUtil.closeInstance(conn);
		}
		return null;
	}
	
	public static UserRecord getRecordByCac(Integer id, String cacHash) {
		if ((id == null) && CommonUtils.isEmpty(cacHash))
			return null;
		
		Connection conn = null;
		try {
			conn = ClsConfig.getDbConnection();
			return UserRecord.getRecordByCac(conn, id, cacHash, null);
		}
		catch (Exception oError) {
			oError.printStackTrace();
		}
		finally {
			SqlUtil.closeInstance(conn);
		}
		return null;
	}
	
	public static boolean checkRecord(Connection conn, String pToken) {
		
		if (CommonUtils.isEmpty(pToken))
			return false;
				
		PreparedStatement ps = null;
		ResultSet rs = null;
		boolean bResult = false;
		try {
			String sSql = "SELECT " + FIELD_EMAIL + " FROM " + TABLE_USERS;
			sSql += " WHERE " + FIELD_RESET_PASSWORD_TOKEN + "=?";
			
			ps = conn.prepareStatement(sSql);
			int iParam = 1;
			if (pToken != null)
				ps.setString(iParam, pToken);
			rs = ps.executeQuery();
			if (rs.next()) {
				if ((rs.getString(FIELD_EMAIL) != null) || (rs.getString(FIELD_EMAIL) != "")) 
					bResult = true;
			}
		}
		catch (Exception oError) {
			oError.printStackTrace();
		}
		finally {
			SqlUtil.closeInstance(rs, ps);
		}
		return bResult;
	}

	public static UserRecord register(String email, String password, String firstName, 
			String lastName, String agency, Connection conn, String currentLogonIp) throws Exception {
		Integer orgId = CommonUtils.toInteger(agency);
		String encryptedPassword = Encryptor.encryptPassword(password);
		Date dPasswordUpdatedAt = CommonUtils.getNow();
		
		AuditEventRecord auditEventRecord = new AuditEventRecord(AuditEventRecord.ACTION_REGISTRATION_ATTEMPT, null, null);
		auditEventRecord.addAttribute(FIELD_EMAIL, email);
		auditEventRecord.addAttribute(FIELD_CURRENT_LOGON_IP, currentLogonIp);
		auditEventRecord.addAttribute(FIELD_FIRST_NAME, firstName);
		auditEventRecord.addAttribute(FIELD_LAST_NAME, lastName);
		auditEventRecord.addAttribute(FIELD_ORG_ID, orgId);
		
		
		SqlInsert sqlInsert = new SqlInsert(null, TABLE_USERS);
		sqlInsert.addField(FIELD_EMAIL, email);
		sqlInsert.addField(FIELD_PASSWORD, encryptedPassword);
		sqlInsert.addField(FIELD_PASSWORD_UPDATED_AT, dPasswordUpdatedAt);
		sqlInsert.addField(FIELD_FIRST_NAME, firstName);
		sqlInsert.addField(FIELD_LAST_NAME, lastName);
		if (orgId != null)
			sqlInsert.addField(FIELD_ORG_ID, orgId);
		sqlInsert.execute(conn);
		
		UserRecord newUser = UserRecord.getRecord(conn, null, email, null);
		//Add Statndard User role to the new user
		ArrayList<Integer> roles = new ArrayList<Integer>();
		roles.add(ROLE_ID.STANDARD_USER);
		UserRoleTable.adminUpdate(conn, newUser.getId(), roles, auditEventRecord);
		
		auditEventRecord.saveNew(conn);
		
		UserRecord.getRecord(conn, null, email, newUser);
		return newUser;
	}
	
	public static boolean resetPassword(String userID, String password, Connection conn, String currentLogonIp) throws Exception {
		
		// this.lastError = null;
		boolean result = false;
		
		SqlUpdate sqlUpdate = new SqlUpdate(null, TABLE_USERS);
		Integer user_id = CommonUtils.toInteger(userID);
		String encryptedPassword = Encryptor.encryptPassword(password);
		Date dPasswordUpdatedAt = CommonUtils.getNow();
		sqlUpdate.addString(FIELD_PASSWORD, encryptedPassword);
		sqlUpdate.addString(FIELD_RESET_PASSWORD_TOKEN, null);
		sqlUpdate.addSet(FIELD_RESET_PASSWORD_SENT_AT, null, java.sql.Types.DATE);
		sqlUpdate.addSet(FIELD_PASSWORD_UPDATED_AT, dPasswordUpdatedAt, java.sql.Types.DATE);
		// Connection conn = null;
		try {
			// conn = ClsConfig.getDbConnection();
			sqlUpdate.addCriteria(FIELD_USER_ID, user_id, java.sql.Types.INTEGER);
			sqlUpdate.execute(conn);
			// UserRecord.getRecord(conn, user_id, null, null);
			AuditEventRecord auditEventRecord = new AuditEventRecord(AuditEventRecord.ACTION_PASSWORD_CHANGED, user_id, user_id);
			auditEventRecord.saveNew(conn);
			result = true;
		}
		catch (Exception oError) {
			oError.printStackTrace();
			// this.lastError = oError;
		}
		finally {
			SqlUtil.closeInstance(conn);
		}
		return result;
	}
	
	// ---------------------------------------------------------------
	private Integer id;
	private String email;
	private String firstName;
	private String lastName;
	private String password;
	private Date passwordUpdatedAt;
	private Integer logonCount;
	private Date currentLogonAt;
	private String currentLogonIp;
	private Date lastLogonAt;
	private String lastLogonIp;
	private Date createdAt;
	private Date updatedAt;
	private String resetPasswordToken;
	private Date resetPasswordSentAt;
	private String cacHash;
	private String timeZone;
	private Integer orgId;
	private String orgName;
	private Integer deptId;
	private String deptName;
	private UserRoles roles = new UserRoles();
	private boolean isActive;
	
	private Date lastRefreshed = null;
	private Exception lastError = null;

	public void read(ResultSet rs) throws SQLException {
		this.id = rs.getInt(FIELD_USER_ID);
		this.email = rs.getString(FIELD_EMAIL);
		this.firstName = rs.getString(FIELD_FIRST_NAME);
		this.lastName = rs.getString(FIELD_LAST_NAME);
		this.password = rs.getString(FIELD_PASSWORD);
		this.passwordUpdatedAt = rs.getTimestamp(FIELD_PASSWORD_UPDATED_AT);
		this.logonCount = rs.getInt(FIELD_LOGON_COUNT);
		this.currentLogonAt = rs.getTimestamp(FIELD_CURRENT_LOGON_AT);
		this.currentLogonIp = rs.getString(FIELD_CURRENT_LOGON_IP);
		this.lastLogonAt = rs.getTimestamp(FIELD_LAST_LOGON_AT);
		this.lastLogonIp = rs.getString(FIELD_LAST_LOGON_IP);
		this.createdAt = rs.getTimestamp(FIELD_CREATED_AT);
		this.updatedAt = rs.getTimestamp(FIELD_UPDATED_AT);
		this.resetPasswordToken = rs.getString(FIELD_RESET_PASSWORD_TOKEN);
		this.resetPasswordSentAt = rs.getTimestamp(FIELD_RESET_PASSWORD_SENT_AT);
		this.cacHash = rs.getString(FIELD_CAC_HASH);
		this.timeZone = rs.getString(FIELD_TIME_ZONE);
		this.orgId = rs.getInt(FIELD_ORG_ID);
		// this.orgName = rs.getString("OrgName");
		this.orgName = rs.getString(OrgRecord.FIELD_ORG_NAME);
		this.deptId = rs.getInt(OrgRecord.FIELD_DEPT_ID);
		this.deptName = rs.getString(DeptRecord.FIELD_DEPT_NAME);
		this.lastRefreshed = CommonUtils.getNow();
		this.isActive = rs.getBoolean(FIELD_IS_ACTIVE);
	}
	
	@Override
	protected void populateJson(ObjectNode json, ObjectMapper mapper) {
		json.put(FIELD_USER_ID, this.id);
		json.put(FIELD_EMAIL, this.email);
		json.put(FIELD_FIRST_NAME, this.firstName);
		json.put(FIELD_LAST_NAME, this.lastName);
		json.put(FIELD_PASSWORD_UPDATED_AT, JsonAble.getJsonValue(this.passwordUpdatedAt));
		json.put(FIELD_LOGON_COUNT, this.logonCount);
		json.put(FIELD_CURRENT_LOGON_AT, JsonAble.getJsonValue(this.currentLogonAt));
		json.put(FIELD_CURRENT_LOGON_IP, this.currentLogonIp);
		json.put(FIELD_LAST_LOGON_AT, JsonAble.getJsonValue(this.lastLogonAt));
		json.put(FIELD_LAST_LOGON_IP, this.lastLogonIp);
		json.put(FIELD_CREATED_AT, JsonAble.getJsonValue(this.createdAt));
		json.put(FIELD_UPDATED_AT, JsonAble.getJsonValue(this.updatedAt));
		//json.put(FIELD_RESET_PASSWORD_TOKEN, this.resetPasswordToken);
		json.put(FIELD_RESET_PASSWORD_SENT_AT, JsonAble.getJsonValue(this.resetPasswordSentAt));
		//json.put(FIELD_CAC_HASH, this.cacHash);
		json.put(FIELD_TIME_ZONE, this.timeZone);
		json.put(FIELD_IS_ACTIVE, this.isActive);   
		json.put(FIELD_ORG_ID, this.orgId);
		// json.put("orgName", this.orgName);
		json.put("orgName", this.orgName); // OrgRecord.FIELD_ORG_NAME
		json.put(OrgRecord.FIELD_DEPT_ID, this.deptId);
		json.put(DeptRecord.FIELD_DEPT_NAME, this.deptName);
		
		json.put("isSuperUser", this.roles.isSuperUser());
		json.put("isSubsuperUser", this.roles.isSubsuperUser());
		json.put("isAgencyReviewer", this.roles.isAgencyReviewer());
		json.put("isReviewer", this.roles.isReviewer());
		json.put("isRegularUser", this.roles.isRegularUser());
		
		json.put("user_timezone_offset", getTimezoneOffset()); // CJ-361
		
		this.roles.addJson(json);
	}
	
	private Integer getTimezoneOffset() {
		
		if ((this.timeZone == null) || (this.timeZone.length() == 0))
			return 0;
		
		String mappedZone = CommonUtils.mapTimezone (this.timeZone);
		
		TimeZone tzUser = TimeZone.getTimeZone(mappedZone);
		Integer iUserOffset = tzUser.getOffset(new Date().getTime()) / 1000 / 60 ;
		
		// Calculate the offset in the client.
		//TimeZone tzServer = TimeZone.getDefault();
		//Integer iServerOffset = tzServer.getOffset(new Date().getTime()) / 1000 / 60 ; 
		//Integer timeDifference = iUserOffset - iServerOffset;
		//return timeDifference;
		
		return iUserOffset;
	}
	public boolean isPasswordSame(String password) {
		if (CommonUtils.isEmpty(this.password)) {
			//if (CommonUtils.isEmpty(this.passwordHash)) {
			//	return CommonUtils.isEmpty(password);
			//}
			if (CommonUtils.isEmpty(password))
				return false;
		} else {
			return Encryptor.isSamePassword(password, this.password);
		}
		return true;
	}
	
	public boolean isActiveUser() {   
		return this.isActive; 
	}
	
	public void updateCurrentLogin(String currentLogonIp) {
		this.lastError = null;
		AuditEventRecord auditEventRecord = new AuditEventRecord(AuditEventRecord.ACTION_LOGGED_IN_WITH_EMAIL, this.id, this.id);
		Connection conn = null;
		PreparedStatement ps = null;
		try {
			conn = ClsConfig.getDbConnection();
			this.currentLogonAt = CommonUtils.getNow();
			this.currentLogonIp = currentLogonIp;
			String sSql = "UPDATE " + TABLE_USERS
					+ " SET " + FIELD_CURRENT_LOGON_AT + " =?"
					+ ", " + FIELD_CURRENT_LOGON_IP + " =?"
					+ " WHERE " + FIELD_USER_ID + " =?";
			ps = conn.prepareStatement(sSql);
			ps.setTimestamp(1, new java.sql.Timestamp(this.currentLogonAt.getTime()));
			ps.setString(2, this.currentLogonIp);
			ps.setInt(3, this.id);
			ps.execute();
			
			auditEventRecord.addAttribute(FIELD_EMAIL, this.email);
			auditEventRecord.addAttribute(FIELD_CURRENT_LOGON_IP, currentLogonIp);
			auditEventRecord.addAttribute(FIELD_FIRST_NAME, this.firstName);
			auditEventRecord.addAttribute(FIELD_LAST_NAME, this.lastName);
			auditEventRecord.addAttribute(FIELD_ORG_ID, this.orgId);
			auditEventRecord.saveNew(conn);
		}
		catch (Exception oError) {
			oError.printStackTrace();
			this.lastError = oError;
		}
		finally {
			SqlUtil.closeInstance(null, ps, conn);
		}
	}
	
	public boolean needToRefresh() {
		boolean result = (this.lastRefreshed == null);
		if (!result) {
			Date now = CommonUtils.getNow();
			long minDifference = (now.getTime() - this.lastRefreshed.getTime()) / (1000 * 60);
			result = (minDifference >= 1);
		}
		return result;
	}
	
	public boolean accountSettingUpdate(String email, String first_name, String last_name, String time_zone) {
		this.lastError = null;
		boolean result = false;
		
		AuditEventRecord auditEventRecord = new AuditEventRecord(AuditEventRecord.ACTION_USER_UPDATED, this.id, this.id);
		SqlUpdate sqlUpdate = new SqlUpdate(null, TABLE_USERS);
		if (!CommonUtils.isSame(this.email, email))
			sqlUpdate.addString(FIELD_EMAIL, email, auditEventRecord);
		if (!CommonUtils.isSame(this.firstName, first_name))
			sqlUpdate.addString(FIELD_FIRST_NAME, first_name, auditEventRecord);
		if (!CommonUtils.isSame(this.lastName, last_name))
			sqlUpdate.addString(FIELD_LAST_NAME, last_name, auditEventRecord);
		if (!CommonUtils.isSame(this.timeZone, time_zone))
			sqlUpdate.addString(FIELD_TIME_ZONE, time_zone, auditEventRecord);
		if (sqlUpdate.size() > 0) {
			Connection conn = null;
			try {
				conn = ClsConfig.getDbConnection();
				sqlUpdate.addCriteria(FIELD_USER_ID, this.id, java.sql.Types.INTEGER);
				sqlUpdate.execute(conn);
				UserRecord.getRecord(conn, this.id, null, this);
				auditEventRecord.saveNew(conn);
				result = true;
			}
			catch (Exception oError) {
				oError.printStackTrace();
				this.lastError = oError;
			}
			finally {
				SqlUtil.closeInstance(conn);
			}
		}
		return result;
	}
	
	public boolean assignCac(String cacHash){
		this.lastError = null;
		boolean result = false;
		
		SqlUpdate sqlUpdate = new SqlUpdate(null, TABLE_USERS);
		sqlUpdate.addString(FIELD_CAC_HASH, cacHash);
		Connection conn = null;
		try {
			conn = ClsConfig.getDbConnection();
			sqlUpdate.addCriteria(FIELD_USER_ID, this.id, java.sql.Types.INTEGER);
			sqlUpdate.execute(conn);
			UserRecord.getRecord(conn, this.id, null, this);
			AuditEventRecord auditEventRecord = new AuditEventRecord(AuditEventRecord.ACTION_USER_UPDATED, this.id, this.id);
			auditEventRecord.saveNew(conn);
			result = true;
		}
		catch (Exception oError) {
			oError.printStackTrace();
			this.lastError = oError;
		}
		finally {
			SqlUtil.closeInstance(conn);
		}
		return result;
	}
	
	public boolean changePassword(String new_password) {
		this.lastError = null;
		boolean result = false;
		
		SqlUpdate sqlUpdate = new SqlUpdate(null, TABLE_USERS);
		String encryptedPassword = Encryptor.encryptPassword(new_password);
		Date dPasswordUpdatedAt = CommonUtils.getNow();
		sqlUpdate.addString(FIELD_PASSWORD, encryptedPassword);
		sqlUpdate.addSet(FIELD_PASSWORD_UPDATED_AT, dPasswordUpdatedAt, java.sql.Types.DATE);
		Connection conn = null;
		try {
			conn = ClsConfig.getDbConnection();
			sqlUpdate.addCriteria(FIELD_USER_ID, this.id, java.sql.Types.INTEGER);
			sqlUpdate.execute(conn);
			UserRecord.getRecord(conn, this.id, null, this);
			AuditEventRecord auditEventRecord = new AuditEventRecord(AuditEventRecord.ACTION_PASSWORD_CHANGED, this.id, this.id);
			auditEventRecord.saveNew(conn);
			result = true;
		}
		catch (Exception oError) {
			oError.printStackTrace();
			this.lastError = oError;
		}
		finally {
			SqlUtil.closeInstance(conn);
		}
		return result;
	}
	
	public boolean adminUpdate(Connection conn, String email, String first_name, String last_name, 
			Integer newOrgId, String newPassword, ArrayList<Integer> newRoleIds, UserRecord adminUserRecord, boolean newIsAcive) 
			throws Exception {
		this.lastError = null;
		boolean result = false;
		
		AuditEventRecord auditEventRecord = new AuditEventRecord(AuditEventRecord.ACTION_USER_UPDATED, this.id, adminUserRecord.getId());
		
		SqlUpdate sqlUpdate = new SqlUpdate(null, TABLE_USERS);
		if (!CommonUtils.isSame(this.email, email))
			sqlUpdate.addString(FIELD_EMAIL, email, auditEventRecord);
		if (!CommonUtils.isSame(this.firstName, first_name))
			sqlUpdate.addString(FIELD_FIRST_NAME, first_name, auditEventRecord);
		if (!CommonUtils.isSame(this.lastName, last_name))
			sqlUpdate.addString(FIELD_LAST_NAME, last_name, auditEventRecord);
		if (this.isActive != newIsAcive)
			sqlUpdate.addString(FIELD_IS_ACTIVE, newIsAcive, auditEventRecord);
		if (newOrgId != null) {
			if (!newOrgId.equals(orgId))
				sqlUpdate.addSet(FIELD_ORG_ID, newOrgId, java.sql.Types.INTEGER, auditEventRecord);
		}
		String encryptedPassword = null;
		if (CommonUtils.isNotEmpty(newPassword)) {
			encryptedPassword = Encryptor.encryptPassword(newPassword);
			Date dPasswordUpdatedAt = CommonUtils.getNow();
			sqlUpdate.addString(FIELD_PASSWORD, encryptedPassword);
			sqlUpdate.addSet(FIELD_PASSWORD_UPDATED_AT, dPasswordUpdatedAt, java.sql.Types.DATE, auditEventRecord);
		}
		
		try {
			if (sqlUpdate.size() > 0) {
				sqlUpdate.addCriteria(FIELD_USER_ID, this.id, java.sql.Types.INTEGER);
				sqlUpdate.execute(conn);
				result = true;
			}
			result |= UserRoleTable.adminUpdate(conn, this.id, newRoleIds, auditEventRecord);
			if (result)
				auditEventRecord.saveNew(conn);
		}
		catch (Exception oError) {
			oError.printStackTrace();
			this.lastError = oError;
			result = false;
			throw oError;
		}
		return result;
	}
	
	public void adminCreate(Connection conn, String email, String first_name, String last_name, 
			Integer newOrgId, String newPassword, ArrayList<Integer> newRoleIds, UserRecord adminUserRecord, boolean newIsAcive) 
			throws Exception {
		this.lastError = null;
		
		AuditEventRecord auditEventRecord = new AuditEventRecord(AuditEventRecord.ACTION_USER_CREATED, null, adminUserRecord.getId());
		
		SqlInsert sqlInsert = new SqlInsert(null, TABLE_USERS);
		sqlInsert.addField(FIELD_EMAIL, email, auditEventRecord);
		sqlInsert.addField(FIELD_FIRST_NAME, first_name, auditEventRecord);
		sqlInsert.addField(FIELD_LAST_NAME, last_name, auditEventRecord);
		sqlInsert.addField(FIELD_IS_ACTIVE, newIsAcive, auditEventRecord);
		if (newOrgId != null) {
			sqlInsert.addField(FIELD_ORG_ID, newOrgId, auditEventRecord);
		}
		String encryptedPassword = Encryptor.encryptPassword(newPassword);
		Date dNow = CommonUtils.getNow();
		sqlInsert.addField(FIELD_PASSWORD, encryptedPassword);
		sqlInsert.addField(FIELD_CREATED_AT, dNow, auditEventRecord);
		sqlInsert.addField(FIELD_UPDATED_AT, dNow, auditEventRecord);
		
		try {
			sqlInsert.execute(conn);
			UserRecord.getRecord(conn, null, email, this);
			auditEventRecord.setUserId(this.id);
			UserRoleTable.adminUpdate(conn, this.id, newRoleIds, auditEventRecord);
			auditEventRecord.saveNew(conn);
		}
		catch (Exception oError) {
			oError.printStackTrace();
			this.lastError = oError;
			throw oError;
		}
	}
	
	public void adminRemove(Connection conn, UserRecord adminUserRecord) 
			throws Exception {
		this.lastError = null;
		
		AuditEventRecord auditEventRecord = new AuditEventRecord(AuditEventRecord.ACTION_USER_DESTROYED, this.id, adminUserRecord.getId());
		PreparedStatement ps = null;
		try {
			UserRoleTable.adminRemoveUser(conn, this.id);
			String sSql = "DELETE FROM " + TABLE_USERS + " WHERE " + FIELD_USER_ID + " = ?";
			ps = conn.prepareStatement(sSql);
			ps.setInt(1, this.id);
			ps.execute();

			auditEventRecord.addAttribute(FIELD_USER_ID, this.id);
			auditEventRecord.addAttribute(FIELD_EMAIL, this.email);
			auditEventRecord.addAttribute(FIELD_FIRST_NAME, this.firstName);
			auditEventRecord.addAttribute(FIELD_LAST_NAME, this.lastName);
			//auditEventRecord.addAttribute(FIELD_PASSWORD_HASH, this.passwordHash);
			auditEventRecord.addAttribute(FIELD_PASSWORD_UPDATED_AT, this.passwordUpdatedAt);
			auditEventRecord.addAttribute(FIELD_LOGON_COUNT, this.logonCount);
			auditEventRecord.addAttribute(FIELD_CURRENT_LOGON_AT, this.currentLogonAt);
			auditEventRecord.addAttribute(FIELD_CURRENT_LOGON_IP, this.currentLogonIp);
			auditEventRecord.addAttribute(FIELD_LAST_LOGON_AT, this.lastLogonAt);
			auditEventRecord.addAttribute(FIELD_LAST_LOGON_IP, this.lastLogonIp);
			auditEventRecord.addAttribute(FIELD_CREATED_AT, this.createdAt);
			auditEventRecord.addAttribute(FIELD_UPDATED_AT, this.updatedAt);
			//auditEventRecord.addAttribute(FIELD_RESET_PASSWORD_TOKEN, this.resetPasswordToken);
			auditEventRecord.addAttribute(FIELD_RESET_PASSWORD_SENT_AT, this.resetPasswordSentAt);
			//auditEventRecord.addAttribute(FIELD_CAC_HASH, this.cacHash);
			auditEventRecord.addAttribute(FIELD_TIME_ZONE, this.timeZone);
			auditEventRecord.addAttribute(FIELD_ORG_ID, this.orgId);

			auditEventRecord.addAttribute("orgName", this.orgName);
			
			auditEventRecord.addAttribute("isSuperUser", this.roles.isSuperUser());
			auditEventRecord.addAttribute("isSubsuperUser", this.roles.isSubsuperUser());
			auditEventRecord.addAttribute("isAgencyReviewer", this.roles.isAgencyReviewer());
			auditEventRecord.addAttribute("isReviewer", this.roles.isReviewer());
			auditEventRecord.addAttribute("isRegularUser", this.roles.isRegularUser());
			auditEventRecord.saveNew(conn);
		}
		finally {
			SqlUtil.closeInstance(ps);
		}
	}
	
	// -------------------------------------------------------
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	public Date getPasswordUpdatedAt() {
		return passwordUpdatedAt;
	}
	public void setPasswordUpdatedAt(Date passwordUpdatedAt) {
		this.passwordUpdatedAt = passwordUpdatedAt;
	}
	
	public Integer getLogonCount() {
		return logonCount;
	}
	public void setLogonCount(Integer logonCount) {
		this.logonCount = logonCount;
	}
	
	public Date getCurrentLogonAt() {
		return currentLogonAt;
	}
	public void setCurrentLogonAt(Date currentLogonAt) {
		this.currentLogonAt = currentLogonAt;
	}
	
	public String getCurrentLogonIp() {
		return currentLogonIp;
	}
	public void setCurrentLogonIp(String currentLogonIp) {
		this.currentLogonIp = currentLogonIp;
	}
	
	public Date getLastLogonAt() {
		return lastLogonAt;
	}
	public void setLastLogonAt(Date lastLogonAt) {
		this.lastLogonAt = lastLogonAt;
	}
	
	public String getLastLogonIp() {
		return lastLogonIp;
	}
	public void setLastLogonIp(String lastLogonIp) {
		this.lastLogonIp = lastLogonIp;
	}
	
	public Date getCreatedAt() {
		return createdAt;
	}
	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}
	
	public Date getUpdatedAt() {
		return updatedAt;
	}
	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}
	
	public String getResetPasswordToken() {
		return resetPasswordToken;
	}
	public void setResetPasswordToken(String resetPasswordToken) {
		this.resetPasswordToken = resetPasswordToken;
	}
	
	public Date getResetPasswordSentAt() {
		return resetPasswordSentAt;
	}
	public void setResetPasswordSentAt(Date resetPasswordSentAt) {
		this.resetPasswordSentAt = resetPasswordSentAt;
	}
	
	public String getCacHash() {
		return cacHash;
	}
	public void setCacHash(String cacHash) {
		this.cacHash = cacHash;
	}
	
	public String getTimeZone() {
		return timeZone;
	}
	public void setTimeZone(String timeZone) {
		this.timeZone = timeZone;
	}
	
	public Integer getOrgId() {
		return orgId;
	}
	public void setOrgId(Integer orgId) {
		this.orgId = orgId;
	}
		
	public Integer getDeptId() {
		return deptId;
	}

	public void setDeptId(Integer deptId) {
		this.deptId = deptId;
	}
	
	public String getOrgName() {
		return orgName;
	}

	public String getDeptName() {
		return deptName;
	};

	public UserRoles getRoles() {
		return this.roles;
	}

	public Date getLastRefreshed() {
		return lastRefreshed;
	}
	public void setLastRefreshed(Date lastRefreshed) {
		this.lastRefreshed = lastRefreshed;
	}

	public Exception getLastError() {
		return lastError;
	}
	
	public boolean getIsActive() {   
		return isActive;
	}
	public void setIsActive(boolean isActive) {  
		this.isActive = isActive;
	}
	
}
