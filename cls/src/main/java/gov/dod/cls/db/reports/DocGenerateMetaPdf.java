package gov.dod.cls.db.reports;

import java.io.IOException;
import java.math.BigInteger;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.edit.PDPageContentStream;
import org.apache.pdfbox.pdmodel.font.PDFont;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.apache.poi.xwpf.usermodel.ParagraphAlignment;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.apache.poi.xwpf.usermodel.XWPFStyles;
import org.apache.poi.xwpf.usermodel.XWPFTable;
import org.apache.poi.xwpf.usermodel.XWPFTableCell;
import org.apache.poi.xwpf.usermodel.XWPFTableRow;
import org.jsoup.Jsoup;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTFonts;

import gov.dod.cls.PageApi;
import gov.dod.cls.bean.utils.DocGenerateDoc;
import gov.dod.cls.db.ClauseFillInTable;
import gov.dod.cls.db.ClauseRecord;
import gov.dod.cls.utils.ClsConstants;
import gov.dod.cls.utils.CommonUtils;


public class DocGenerateMetaPdf {
	
	public static final PDFont FONT =  PDType1Font.TIMES_ROMAN;
	public static final PDFont FONT_BOLD =  PDType1Font.TIMES_BOLD;
	public static final int DEFAULT_FONT_SIZE = 10;
	public static final int TITLE_FONT_SIZE = 14;
	public static final int MARGIN = 72;
	
	private PDPageContentStream contentStream;
	private PDPage page;
	private PDDocument document;
	private float startY, startX, currentX, currentY, pageWidth, pageHeight, leading;
	
	ArrayList<ClauseRecord> clauseArray = new ArrayList<ClauseRecord>();
	ArrayList<Integer> tblCols = new ArrayList<Integer>();
	
	public static void downloadDoc(ArrayList<ClauseRecord> clsArray, HttpServletRequest req, HttpServletResponse resp) {
		DocGenerateMetaPdf docGenerateDoc = new DocGenerateMetaPdf(clsArray);
		try {
			PageApi.send(resp, 
					"attachment; filename=cls_clauses.pdf",
					"application/pdf",
					docGenerateDoc.createDoc());
		} catch (Exception oError) {
			oError.printStackTrace();
			PageApi.send(resp, "text/plain", oError.getMessage());
		}
	}
	
	public DocGenerateMetaPdf(ArrayList<ClauseRecord> clsArray) {
		
		this.clauseArray.addAll(clsArray);
	}
	
	public PDDocument createDoc() throws Exception {
		initializeDoc();
		
        printSelectedClauses ();
        
	    contentStream.endText(); 
	    contentStream.close();
	    
        return document;
	}
	
    private  void printSelectedClauses () throws Exception {
    	
        printTitle ("Metadata for Selected Clauses");
        
        
        for (Integer item = 0; item<this.clauseArray.size(); item++) {
        	
            ClauseRecord rec = this.clauseArray.get(item);
        
            printNameAndValue("Clause Id:", rec.getId().toString());
	        printNameAndValue("Section:", rec.getSectionCode() + " - " + rec.getSectionHeader());
	        printNameAndValue("Regulation:", rec.getRegulationName());
	        printNameAndValue("Clause Name:", rec.getClauseName());
	        printNameAndValue("Clause Title:", rec.getClauseTitle());
	        printNameAndValue("Effective Date:", CommonUtils.toMonthYear(rec.getEffectiveDate()));
	        printNameAndValue("Provision or Clause:", (rec.getProvisonOrClause().equals("C") ? "Clause" : "Provision"));
	        printNameAndValue("Inclusion Code:", rec.getInclusion()  + " - " + rec.getInclusionName());
	        printNameAndValue("Clause URL:", rec.getUrl());
	        printNameAndValue("Clause Version:", rec.getClauseVersionName());
	        printNameAndValue("Is Active:", (rec.isActive() ? "True" : "False"));
	        printNameAndValue("Is Editable:", (rec.isEditable() ? "True" : "False"));
	        printNameAndValue("Editable Remarks:", rec.getEditableRemarks());
	        printNameAndValue("Is Optional:", (rec.isOptional() ? "True" : "False"));
	        printNameAndValue("Commercial Status:", rec.getCommercialStatus());
	        printNameAndValue("Is Basic Clause:", (rec.isBasicClause() ? "True" : "False"));
	        
	        
	        String sConditions = formatConditions (rec.getClauseRule(), rec.getClauseConditions());
	        printNameAndValue("Clause Conditions:", sConditions);
	        
	        
	        printNameAndValue("Clause Rule:", rec.getClauseRule());
	        printNameAndValue("Additional Conditions:", rec.getAdditionalConditions());
	        
	        String sPrescriptions = "";
	        for (Integer i = 0; i< rec.getPrescriptionTable().size(); i++) {
	        	sPrescriptions += ((sPrescriptions.length() > 0) ? "; " : "") +  rec.getPrescriptionTable().get(i).getName();
	        }
	        printNameAndValue("Prescriptions:", sPrescriptions);
	        
	        printNameAndValue("Clause Data:", rec.getClauseData());
	        printFillinTable("Fill Ins:", rec.getClauseFillInTable());
	        printNameAndValue("Created At:", CommonUtils.toDateTime12Hour(rec.getCreatedAt()));
	        printNameAndValue("Updated At:", CommonUtils.toDateTime12Hour(rec.getUpdatedAt()));
	        
	        // printPdfLine(" ", false, 2,  true);
	        // printNameAndValue(" ", " ---------- ");
	        printSeparator();
        }
        
    }
	
	private void printTitle(String sTitle) {

	    try {
	    	String sDate = CommonUtils.toDateTimeBrief(CommonUtils.getNow());
	    	
	    	float titleWidth = FONT_BOLD.getStringWidth(sTitle) / 1000 * TITLE_FONT_SIZE;
	    	float dateWidth = FONT.getStringWidth(sDate) / 1000 * DEFAULT_FONT_SIZE;
	    	
			contentStream.setFont(FONT_BOLD, TITLE_FONT_SIZE);
	        contentStream.moveTextPositionByAmount((this.pageWidth - titleWidth) /2, -leading);	// center
			contentStream.drawString(sTitle);
			contentStream.moveTextPositionByAmount(-((this.pageWidth - titleWidth) /2), 0); // left-justified
			
	        contentStream.setFont(FONT, DEFAULT_FONT_SIZE);
	        contentStream.moveTextPositionByAmount((this.pageWidth - dateWidth) /2, -leading);	// center
	        contentStream.drawString(sDate);
	        contentStream.moveTextPositionByAmount(-((this.pageWidth - dateWidth) /2), -leading); // left-justified
	        contentStream.drawString(""); // blank line
	        currentY -= (leading*4);
	        
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
	}

	private void printSeparator() {

	    try {
	    	String sSep = "--------------------------------------------------------------------------------";
	    	float sepWidth = FONT.getStringWidth(sSep) / 1000 * DEFAULT_FONT_SIZE;
	    	
	        contentStream.setFont(FONT, DEFAULT_FONT_SIZE);
	        contentStream.moveTextPositionByAmount((this.pageWidth - sepWidth) /2, -leading);	// center
	        contentStream.drawString(sSep);
	        contentStream.moveTextPositionByAmount(-((this.pageWidth - sepWidth) /2), -leading); // left-justified
	        contentStream.drawString(""); // blank line
	        currentY -= (leading*2);
	        
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
	}
	// The document layout is formatted like a two column table.
	private void printNameAndValue(String sName, String sValue) throws Exception {
		
		if (sValue == null) sValue = "";
		
		checkPageSpace(leading);
        printPdfLine(sName, true, 1, true);
        
        Boolean bLinefeed = false;
        String [] textElements = sValue.split("<p>");
        for (String element : textElements) {
        	if ((element == null) || (element.length() == 0))
        		continue;
        	
        	printPdfLine(Jsoup.parse(element).text(), false, 2, bLinefeed);
        	
        	// adjust spacing 
        	bLinefeed = true;
        	currentY -= leading;
        	checkPageSpace(leading);
        }
        
        if (sValue.equals("")) // adjust spacing for empty value
        	currentY -= leading;
        
        // Print blank line between rows.
        checkPageSpace(leading);
        printPdfLine(" ", false, 2,  true);
        currentY -= leading;
        
	}

	private void printFillinTable(String sName, ClauseFillInTable clsTable) throws Exception {
		
		if (clsTable.size() > 0)
			checkPageSpace(10*leading);
		else
			checkPageSpace(leading);
		
        printPdfLine(sName, true, 1, true);
		
        Boolean bLinefeed = false;
        
		for (Integer item = 0; item<clsTable.size(); item++) {
			
			checkPageSpace(10*leading); 
			
			printPdfLine("Code: " + clsTable.get(item).getFillInCode(), false, 2,  bLinefeed);
			printPdfLine("Type: " + clsTable.get(item).getFillInType(), false, 2,  true);
			printPdfLine("Max Size: " + clsTable.get(item).getFillInMaxSize(), false, 2,  true);
			printPdfLine("Group Number: " + clsTable.get(item).getFillInGroupNumber(), false, 2,  true);
			printPdfLine("Place Holder: " + clsTable.get(item).getFillInPlaceholder(), false, 2,  true);
			printPdfLine("Display Rows: " + clsTable.get(item).getFillInDisplayRows(), false, 2,  true);
			printPdfLine("For Table: " + (clsTable.get(item).isFillInForTable() ? "True" : "False"), false, 2,  true);
			printPdfLine("Heading: " + clsTable.get(item).getFillInHeading(), false, 2,  true);
			printPdfLine("Default Data: " + clsTable.get(item).getFillInDefaultData(), false, 2,  true);
			printPdfLine(" ", false, 2,  true);
			bLinefeed = true;
			currentY -= 10*leading; // adjust spacing for fillin group
		}
		
        // Print blank line after fill-in.
		if (clsTable.size() == 0) {
	        checkPageSpace(leading);
	        printPdfLine(" ", false, 2,  true);
	        currentY -= leading;
		}
	}
	
	private void printPdfLine(String text, boolean bold, Integer iCol, Boolean bLinefeed) throws Exception{
		
		
		text = text.replaceAll("[^\\p{ASCII}]", ""); //strip all non-ASCII, find better solution later
		
		float cellWidth = (float) tblCols.get(iCol);
		float fColMargin = tblCols.get(iCol-1);
		float fRowMargin = (bLinefeed ? -leading : 0);
		checkPageSpace((bLinefeed ? leading : 0));
		
		ArrayList<String> lines = getParagraphLines(text,cellWidth);
		
		PDFont oPdfFont = (bold ? FONT_BOLD : FONT);
		contentStream.setFont(oPdfFont, DEFAULT_FONT_SIZE);
		
		Integer index = 0;
		for (String aLine : lines) {
	    	contentStream.moveTextPositionByAmount(fColMargin, fRowMargin);
	        contentStream.drawString(aLine);
	        contentStream.moveTextPositionByAmount(-fColMargin, 0);
	        
	        // adjust spacing for data that exceeds one line
	        if (index++ > 0) {
	        	currentY -= leading;
	        	checkPageSpace(leading);
	        }
	        fRowMargin =  -leading;
	        
		}
    	
		contentStream.setFont(FONT, DEFAULT_FONT_SIZE);
	}
	
	// Condition strings need to be formatted, since some contain newlines per condition and others do not.
	// Sample Input: (A) DOCUMENT TYPE IS: SOLICITATION (B) DOCUMENT TYPE IS: AWARD (C) ACO RESPONSIBILITIES IS: APPOINTMENT OF A CONTRACTING OFFICER'S REPRESENTATIVE IS ANTICIPATED
	// Sample Output:	(A) DOCUMENT TYPE IS: SOLICITATION 
	//					(B) DOCUMENT TYPE IS: AWARD 
	//					(C) ACO RESPONSIBILITIES IS: APPOINTMENT OF A CONTRACTING OFFICER'S REPRESENTATIVE IS ANTICIPATED
	private String formatConditions (String sClauseRules, String sClauseConditions) {
		
        String sRules = sClauseRules;
        String sConditions = sClauseConditions;
        sConditions = sConditions.replaceAll("\\n", "");
        
        // Format rules first.
        // Input: (A OR B) AND C
        // Output: A, B, C
        sRules = sRules.replaceAll("\\(", "");
        sRules = sRules.replaceAll("\\)", "");
        sRules = sRules.replaceAll("OR", "");
        sRules = sRules.replaceAll("AND", "");
        sRules = sRules.replaceAll("!", "");
        Integer iPos = sRules.indexOf("  ");
        while (iPos >= 0) {
        	sRules = sRules.replaceAll("  ", " ");
        	iPos = sRules.indexOf("  ");
        }
        
        // Find each matching rule in the condition string, and insert a newline.
        String[] aRules = sRules.split(" ");
        Boolean bFirst = true;
        for (String element : aRules) {
        	if ((element == null) || (element.length() == 0))
        		continue;
        	if (bFirst) {
        		bFirst = false;
        		continue;
        	}
        	element = "(" +  element + ")";
        	sConditions = sConditions.replace(element, "<p>" + element);
        }
        
        return sConditions;
	}
	//===============================================
	// Routine PDF methods follow.
	//===============================================
	
	private void initializeDoc() throws IOException{
		document = new PDDocument();
	    page = new PDPage();
	    document.addPage(page);
	    contentStream = new PDPageContentStream(document, page);
	    contentStream.beginText();
	    contentStream.setFont(FONT, DEFAULT_FONT_SIZE);
	    PDRectangle mediabox = page.findMediaBox();
		pageWidth = mediabox.getWidth() - 2*MARGIN;
		leading = 1.5f * DEFAULT_FONT_SIZE;
		startX = mediabox.getLowerLeftX() + MARGIN;
		startY = mediabox.getUpperRightY() - MARGIN;
		currentY = startY;
		currentX = startX;
	    contentStream.moveTextPositionByAmount(startX, startY);
	    pageHeight = mediabox.getHeight() - 2*MARGIN;
	    
	    tblCols.add(0);
	    tblCols.add((int) (pageWidth * .30));
	    tblCols.add((int) (pageWidth * .68));
	    
	}
	
	private void checkPageSpace(float ySpaceNeeded) throws IOException{
		if((currentY-ySpaceNeeded) < MARGIN){
    	    contentStream.endText(); 
    	    contentStream.close();
    	    page = new PDPage();
    	    document.addPage(page);
    	    contentStream = new PDPageContentStream(document, page);
    		contentStream.beginText();
    		contentStream.setFont(FONT, DEFAULT_FONT_SIZE);
    		contentStream.moveTextPositionByAmount(startX, startY);
    		currentY=startY;
    		currentX=startX;
		}
	}
	
	//Takes a string and returns the lines based on allowed width
	private ArrayList<String> getParagraphLines(String text, float width) throws Exception{
		ArrayList<String> lines = new ArrayList<String>();
	    int lastSpace = -1;
	    while (text.length() > 0) {
	        int spaceIndex = text.indexOf(' ', lastSpace + 1);
	        if (spaceIndex < 0) {
	        	// CJ-511, if substring is longer than col length
	        	float fsize = DEFAULT_FONT_SIZE * FONT.getStringWidth(text) / 1000;
	        	if (fsize < width)
	        		lines.add(text);
	        	// last substring broken on a space
	        	else if (text.indexOf(' ', lastSpace) >= 0) {
	        		lines.add (text.substring (0, lastSpace));
	        		lines.add (text.substring (lastSpace + 1));
	        	}
	        	// long string of characters, not broken by space.
	        	else {
	        		int iLines = (int) (fsize / width) + 1;
	        		int iCharsMax = (text.length() / iLines) -1;
	        		int pBeg, pEnd = 0;
	        		for (int i = 0; i<iLines; i++) {
	        			pBeg = i*iCharsMax;
	        			pEnd = pBeg + iCharsMax;
	        			lines.add(text.substring(pBeg, pEnd));
	        		}
	        		if (pEnd<=text.length())
	        			lines.add(text.substring(pEnd));
	        	}
	        		
	            text = "";
	        }
	        else {
	            String subString = text.substring(0, spaceIndex);
	            float size = DEFAULT_FONT_SIZE * FONT.getStringWidth(subString) / 1000;
	            size += 5;	// CJ-511, add padding to the column text.
	            if (size > width) {
	                if (lastSpace < 0)
	                    lastSpace = spaceIndex;
	                subString = text.substring(0, lastSpace);
	                //lines.add(subString);
	                
		        	// CJ-511, if substring is longer than col length
		        	float fsize = DEFAULT_FONT_SIZE * FONT.getStringWidth(subString) / 1000;
		        	if (fsize < width)
		        		lines.add(subString);
		        	else {
		        		int iLines = (int) (fsize / width) + 1;
		        		int iCharsMax = subString.length() / iLines;
		        		int pBeg, pEnd = 0;
		        		for (int i = 0; i<iLines; i++) {
		        			pBeg = i*iCharsMax;
		        			pEnd = pBeg + iCharsMax;
		        			lines.add(subString.substring(pBeg, pEnd));
		        		}
		        		if (pEnd<=subString.length())
		        			lastSpace -= subString.length() - pEnd;
		        			//lines.add(subString.substring(pEnd));
		        	}
		        	// end CJ-511
		        	
	                text = text.substring(lastSpace).trim();
	                lastSpace = -1;
	            }
	            else {
	                lastSpace = spaceIndex;
	            }
	        }
	    }
		return lines;
	}
}