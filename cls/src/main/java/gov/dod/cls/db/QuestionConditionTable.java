package gov.dod.cls.db;

import gov.dod.cls.config.ClsConfig;
import gov.dod.cls.utils.CommonUtils;
import gov.dod.cls.utils.sql.SqlUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.node.ArrayNode;
import org.codehaus.jackson.node.ObjectNode;

public class QuestionConditionTable extends ArrayList<QuestionConditionRecord> {
	
	private static final long serialVersionUID = -4529634031223938826L;

	// ========================================================
	private volatile static QuestionConditionTable instance = null;
	private volatile static Date refreshed = null;
	
	public synchronized static QuestionConditionTable getInstance(Connection conn) {
		if (QuestionConditionTable.instance == null) {
			QuestionConditionTable.instance = new QuestionConditionTable();
			QuestionConditionTable.instance.refresh(conn);
		} else {
			QuestionConditionTable.instance.refreshWhenNeeded(conn);
		}
		return QuestionConditionTable.instance;
	}
	
	public static QuestionConditionTable getQuestionSubset(ArrayList<Integer> aIds) {
		QuestionConditionTable questionConditionTable = QuestionConditionTable.getInstance(null);
		return questionConditionTable.subsetByQuestions(aIds);
	}

	public static QuestionConditionTable getQuestionSubset(QuestionTable questionTable, Connection conn) {
		QuestionConditionTable questionConditionTable = QuestionConditionTable.getInstance(conn);
		return questionConditionTable.subsetByQuestions(questionTable);
	}

	public static ArrayNode getJson(Integer clauseVersionId, Connection conn, ObjectMapper mapper, boolean forInterview)
			throws SQLException { // CJ-584
		if (clauseVersionId == null) {
			clauseVersionId = ClauseVersionTable.getActiveVersionId();
			if (clauseVersionId == null) {
				QuestionTable emptyQuestions = new QuestionTable();
				return emptyQuestions.toJson(mapper, forInterview);
			}
		}
		
		// ===========================================================================================
		String sSql
				= "SELECT A." + QuestionConditionRecord.FIELD_QUESTION_CONDITION_SEQUENCE
				+ ", A." + QuestionConditionRecord.FIELD_QUESTION_ID
				+ ", A." + QuestionConditionRecord.FIELD_PARENT_QUESTION_ID
				+ ", IF(A." + QuestionConditionRecord.FIELD_DEFAULT_BASELINE + ", 'Y', NULL) dfbase" // CJ-584
				+ ", A." + QuestionConditionRecord.FIELD_ORIGINAL_CONDITION_TEXT
				+ ", A." + QuestionConditionRecord.FIELD_CONDITION_TO_QUESTION
				+ " FROM " + QuestionRecord.TABLE_QUESTIONS + " B"
				+ " JOIN " + QuestionConditionRecord.TABLE_QUESTION_CONDITIONS + " A"
				+ " ON (B." + QuestionRecord.FIELD_QUESTION_ID + " = A." + QuestionConditionRecord.FIELD_QUESTION_ID + ")"
				+ " WHERE B." + QuestionRecord.FIELD_CLAUSE_VERSION_ID + " = ?"
				+ " ORDER BY A." + QuestionConditionRecord.FIELD_QUESTION_CONDITION_SEQUENCE; // + ", A." + FIELD_QUESTION_ID;

		ArrayNode result = mapper.createArrayNode();
		boolean bCreateConnection = (conn == null);
		PreparedStatement ps1 = null;
		ResultSet rs1 = null;
		try {
			if (bCreateConnection)
				conn = ClsConfig.getDbConnection();
			ps1 = conn.prepareStatement(sSql);
			ps1.setInt(1, clauseVersionId);
			rs1 = ps1.executeQuery();
			while (rs1.next()) {
				ObjectNode json = mapper.createObjectNode();
				int iQuestionId = rs1.getInt(2);
				json.put(QuestionConditionRecord.FIELD_QUESTION_CONDITION_SEQUENCE, rs1.getInt(1));
				json.put(QuestionConditionRecord.FIELD_QUESTION_ID, iQuestionId);
				json.put(QuestionConditionRecord.FIELD_PARENT_QUESTION_ID, CommonUtils.toInteger(rs1.getObject(3)));
				json.put(QuestionConditionRecord.FIELD_ORIGINAL_CONDITION_TEXT, rs1.getString(5));
				json.put(QuestionConditionRecord.FIELD_CONDITION_TO_QUESTION, rs1.getString(6));
				json.put(QuestionConditionRecord.FIELD_DEFAULT_BASELINE, rs1.getBoolean(4)); // CommonUtils.toBoolean(rs1.getObject(FIELD_DEFAULT_BASELINE))

				result.add(json);
			}
		}
		finally {
			SqlUtil.closeInstance(rs1, ps1);
			if (bCreateConnection)
				SqlUtil.closeInstance(conn);
		}
		
		return result;
		/*
		QuestionTable questionTable = QuestionTable.getSubsetByClauseVersion(clauseVersionId, conn);
		QuestionConditionTable questionConditionTable = QuestionConditionTable.getQuestionSubset(questionTable, conn);
		return questionConditionTable.toJson(mapper, forInterview);
		*/
	}

	// Functions -----------------------------------------------------------------
	public static Date getReferHistoryDate(Connection conn) {
		return SysLastTableChangeAtTable.getLastUpdateDate(conn, QuestionConditionRecord.TABLE_QUESTION_CONDITIONS);
	}
	  
	public static boolean needToRefreh(Connection conn) {
		return SysLastTableChangeAtTable.needToLoad(QuestionConditionTable.refreshed, getReferHistoryDate(conn));
	}
	
	// ========================================================
	private Exception lastError;

	public void refreshWhenNeeded(Connection conn) {
		if (QuestionGroupRefTable.needToRefreh(conn))
			this.refresh(conn);
	}

	private boolean refresh(Connection conn) {
		boolean result = false;
		String sSql = QuestionConditionRecord.SQL_SELECT;
		boolean bCreateConnection = (conn == null);
		PreparedStatement ps1 = null;
		ResultSet rs1 = null;
		try {
			if (bCreateConnection)
				conn = ClsConfig.getDbConnection();
			ps1 = conn.prepareStatement(sSql);
			rs1 = ps1.executeQuery();
			this.clear();
			while (rs1.next()) {
				QuestionConditionRecord clauseSectionRecord = new QuestionConditionRecord();
				clauseSectionRecord.read(rs1);
				this.add(clauseSectionRecord);
			}
			QuestionConditionTable.refreshed = getReferHistoryDate(conn); // CommonUtils.getNow();
			result = true;
			System.out.println("QuestionConditionTable refreshed");
		}
		catch (Exception oError) {
			this.lastError = oError;
			oError.printStackTrace();
		}
		finally {
			SqlUtil.closeInstance(rs1, ps1);
			if (bCreateConnection)
				SqlUtil.closeInstance(conn);
		}
		return result;
	}
	
	public QuestionConditionTable subsetByQuestions(ArrayList<Integer> aIds) {
		QuestionConditionTable result = new QuestionConditionTable();
		for (QuestionConditionRecord clauseSectionRecord : this) {
			if (aIds.contains(clauseSectionRecord.getQuestionId()))
				result.add(clauseSectionRecord);
		}
		return result;
	}
	
	public QuestionConditionTable subsetByQuestions(QuestionTable questionTable) {
		QuestionConditionTable result = new QuestionConditionTable();
		for (QuestionConditionRecord clauseSectionRecord : this) {
			Integer iQustionId = clauseSectionRecord.getQuestionId();
			for (QuestionRecord question : questionTable) {
				if (question.getId().equals(iQustionId)) {
					result.add(clauseSectionRecord);
					break;
				}
			}
		}
		return result;
	}
	

	public ArrayNode toJson(ObjectMapper mapper, boolean forInterview) {
		if (mapper == null)
			mapper = new ObjectMapper();
		ArrayNode json = mapper.createArrayNode();
		for (QuestionConditionRecord questionConditionRecord : this) {
			json.add(questionConditionRecord.toJson(forInterview));
		}
		return json;
	}

	public Date getRefreshed() {
		return refreshed;
	}
	
	public QuestionConditionRecord findById(int id) {
		for (QuestionConditionRecord clauseSectionRecord : this) {
			if (clauseSectionRecord.getId().intValue() == id)
				return clauseSectionRecord;
		}
		return null;
	}

	public QuestionConditionRecord findByQuestion(QuestionRecord oQuestionRecord) {
		int iQuestionId = oQuestionRecord.getId().intValue();
		for (QuestionConditionRecord clauseSectionRecord : this) {
			if (clauseSectionRecord.getQuestionId().intValue() == iQuestionId)
				return clauseSectionRecord;
		}
		return null;
	}

	public Exception getLastError() {
		return lastError;
	}
	
}
