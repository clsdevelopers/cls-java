package gov.dod.cls.db;

import gov.dod.cls.PageApi;
import gov.dod.cls.bean.session.UserSession;
import gov.dod.cls.config.ClsConfig;
import gov.dod.cls.model.JsonAble;
import gov.dod.cls.utils.CommonUtils;
import gov.dod.cls.utils.Pagination;
import gov.dod.cls.utils.sql.SqlUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.node.ArrayNode;
import org.codehaus.jackson.node.ObjectNode;

public class ClauseVersionTable extends ArrayList<ClauseVersionRecord> {
	
	private static final long serialVersionUID = -5902765681931547348L;

	// ----------------------------------------------------------
	public static final String PREFIX_DO_CLAUSE_VERSION = "clauseversion-";
	public static final String DO_CLAUSE_VERSION_LIST = PREFIX_DO_CLAUSE_VERSION + "list";
	public static final String DO_CLAUSE_VERSION_REFERENCES = PREFIX_DO_CLAUSE_VERSION + "references";
	public static final String DO_CLAUSE_VERSION_PRINT_REFERENCES = PREFIX_DO_CLAUSE_VERSION + "references-print";

	public static void processRequest(String sDo, UserSession oUserSession, 
			HttpServletRequest req, HttpServletResponse resp) {
		
		// CJ-1301	- Check for Token
		Boolean hasToken = false;
		OauthInterviewRecord oAuth =  oUserSession.getOauthInterviewRecord();
		if (oAuth != null) {
			String sToken = oAuth.getInterviewToken();
			if ((sToken != null) && (sToken.length() > 0))
				hasToken = true;
		}
		
		if (!oUserSession.isSuperUser() && !hasToken // CJ-680
				&& !sDo.equals (DO_CLAUSE_VERSION_PRINT_REFERENCES)) { // Print clauses does not require admin.
			return;
		}
		
		switch (sDo) {
		case ClauseVersionTable.DO_CLAUSE_VERSION_LIST:
			ClauseVersionTable.processList(req, resp);
			break;
		case ClauseVersionTable.DO_CLAUSE_VERSION_REFERENCES:
			String response;
			try {
				ClauseVersionTable oClauseVersionTable = ClauseVersionTable.getInstance(null);
				response = JsonAble.jsonResult(JsonAble.STATUS_SUCCESS, oClauseVersionTable.referenceJson(), null); 
			} catch (Exception oError) {
				response = JsonAble.jsonResult(JsonAble.STATUS_ERROR, null, oError.getMessage()); 
			}
			PageApi.send(resp, JsonAble.JSON_RESPONSE_FORMAT, response);
			return;
		
		case ClauseVersionTable.DO_CLAUSE_VERSION_PRINT_REFERENCES:
			try {
				ClauseVersionTable oClauseVersionTable = ClauseVersionTable.getInstance(null);
				response = JsonAble.jsonResult(JsonAble.STATUS_SUCCESS, oClauseVersionTable.referenceJson(), null); 
			} catch (Exception oError) {
				response = JsonAble.jsonResult(JsonAble.STATUS_ERROR, null, oError.getMessage()); 
			}
			PageApi.send(resp, JsonAble.JSON_RESPONSE_FORMAT, response);
			return;
			
		}
	}
	
	// ----------------------------------------------------------
	public static final String SQL_LIST
		= "SELECT A." + ClauseVersionRecord.FIELD_CLAUSE_VERSION_ID
		+ ", A." + ClauseVersionRecord.FIELD_CLAUSE_VERSION_NAME
		+ ", A." + ClauseVersionRecord.FIELD_CLAUSE_VERSION_DATE
		+ ", A." + ClauseVersionRecord.FIELD_IS_ACTIVE
		+ ", A." + ClauseVersionRecord.FIELD_FAC_NUMBER
		+ ", A." + ClauseVersionRecord.FIELD_DAC_NUMBER
		+ ", A." + ClauseVersionRecord.FIELD_DFARS_NUMBER
		+ ", A." + ClauseVersionRecord.FIELD_FAC_DATE
		+ ", A." + ClauseVersionRecord.FIELD_DAC_DATE
		+ ", A." + ClauseVersionRecord.FIELD_DFARS_DATE
		+ ", A." + ClauseVersionRecord.FIELD_SAT_CONDITIONS
		+ ", A." + ClauseVersionRecord.FIELD_SAT_300K_RULE
		+ ", A." + ClauseVersionRecord.FIELD_SAT_1MILLION_RULE
		+ " FROM " + ClauseVersionRecord.TABLE_CLAUSE_VERSIONS + " A";
		// + " ORDER BY 3 DESC";

	@SuppressWarnings("resource")
	private static void processList(HttpServletRequest req, HttpServletResponse resp) {
		String query = req.getParameter("query");
		//String sort_field = req.getParameter("sort_field");
		String sort_order = req.getParameter("sort_order");
		
		Pagination pagination = new Pagination(req);
		pagination.addDataSource(ClauseVersionRecord.TABLE_CLAUSE_VERSIONS);
		Connection conn = null;
		PreparedStatement ps1 = null;
		ResultSet rs1 = null;
		try {
			conn = ClsConfig.getDbConnection();
			String sSql = "SELECT COUNT(" + ClauseVersionRecord.FIELD_CLAUSE_VERSION_ID + ") FROM " + ClauseVersionRecord.TABLE_CLAUSE_VERSIONS;
			String operator = " WHERE ";
			if (CommonUtils.isNotEmpty(query)) {
				sSql += operator + "(LOWER(" + ClauseVersionRecord.FIELD_CLAUSE_VERSION_NAME + ") LIKE ?" 
					+ ")";
				operator = " AND ";
				query = "%" + query.toLowerCase() + "%";
			}	
						
			ps1 = conn.prepareStatement(sSql);
			int iParam = 1;
			if (CommonUtils.isNotEmpty(query)) {
				ps1.setString(iParam++, query);
			}			
			
			rs1 = ps1.executeQuery();
			boolean isAcceptableRange = false;
			if (rs1.next())
				isAcceptableRange = pagination.isAcceptedRange(rs1.getInt(1));
			
			if (isAcceptableRange) {
				rs1 = SqlUtil.closeInstance(rs1);
				rs1 = null;
				ps1 = SqlUtil.closeInstance(ps1);
				ps1 = null;

				sSql = "SELECT clause_version_id, count(*)"
					+ " FROM " + ClauseRecord.TABLE_CLAUSES
					+ " where clause_version_id is not null"
					+ " group by clause_version_id";
				ps1 = conn.prepareStatement(sSql);
				rs1 = ps1.executeQuery();
				HashMap<Integer, Integer> oClauseCounts = new HashMap<Integer, Integer>(); 
				while (rs1.next()) {
					oClauseCounts.put(rs1.getInt(1), rs1.getInt(2));
				}
				rs1 = SqlUtil.closeInstance(rs1);
				ps1 = SqlUtil.closeInstance(ps1);
				
				sSql = "SELECT clause_version_id, COUNT(*)"
					+ " FROM " + QuestionRecord.TABLE_QUESTIONS
					+ " WHERE clause_version_id IS NOT NULL"
					+ " GROUP BY clause_version_id";
				ps1 = conn.prepareStatement(sSql);
				rs1 = ps1.executeQuery();
				HashMap<Integer, Integer> oQuestionCounts = new HashMap<Integer, Integer>(); 
				while (rs1.next()) {
					oQuestionCounts.put(rs1.getInt(1), rs1.getInt(2));
				}
				rs1 = SqlUtil.closeInstance(rs1);
				ps1 = SqlUtil.closeInstance(ps1);
					
				sSql = "SELECT clause_version_id, COUNT(*)"
						+ " FROM " + DocumentRecord.TABLE_DOCUMENTS
						+ " WHERE clause_version_id IS NOT NULL"
						+ " GROUP BY clause_version_id";
				ps1 = conn.prepareStatement(sSql);
				rs1 = ps1.executeQuery();
				HashMap<Integer, Integer> oDocumentCounts = new HashMap<Integer, Integer>(); 
				while (rs1.next()) {
					oDocumentCounts.put(rs1.getInt(1), rs1.getInt(2));
				}
				rs1 = SqlUtil.closeInstance(rs1);
				rs1 = null;
				ps1 = SqlUtil.closeInstance(ps1);
				ps1 = null;
				
				sSql = SQL_LIST;
				operator = " WHERE ";
				if (CommonUtils.isNotEmpty(query)) {
					sSql += operator + "(LOWER(A." + ClauseVersionRecord.FIELD_CLAUSE_VERSION_NAME + ") LIKE ?" 
							+ ")";
						operator = " AND ";
				}
				String sAsc = ("desc".equals(sort_order) ? " DESC" : "");
				String orderField = ClauseVersionRecord.FIELD_CLAUSE_VERSION_NAME;
				// if ("version_name".equals(sort_field))
				//	orderField = ClauseVersionRecord.FIELD_CLAUSE_VERSION_NAME;
				sSql += " ORDER BY " + orderField + sAsc;				
				ps1 = conn.prepareStatement(sSql + pagination.sqlLimit());
						
				iParam = 1;
				if (CommonUtils.isNotEmpty(query)) {
					ps1.setString(iParam++, query);
				}				
				
				rs1 = ps1.executeQuery();
				while (rs1.next()) {
					ObjectNode json = pagination.getMapper().createObjectNode();
					int id = rs1.getInt(ClauseVersionRecord.FIELD_CLAUSE_VERSION_ID);
					json.put(ClauseVersionRecord.FIELD_CLAUSE_VERSION_ID, id);
					json.put(ClauseVersionRecord.FIELD_CLAUSE_VERSION_NAME, rs1.getString(ClauseVersionRecord.FIELD_CLAUSE_VERSION_NAME));
					json.put(ClauseVersionRecord.FIELD_CLAUSE_VERSION_DATE, CommonUtils.toJsonDate(rs1.getDate(ClauseVersionRecord.FIELD_CLAUSE_VERSION_DATE)));
					json.put(ClauseVersionRecord.FIELD_IS_ACTIVE, rs1.getBoolean(ClauseVersionRecord.FIELD_IS_ACTIVE));
					
					Integer iQuestionCounts = new Integer(0);
					if (oQuestionCounts.containsKey(id))
						iQuestionCounts = oQuestionCounts.get(id);
					json.put("QuestionCount", iQuestionCounts);

					Integer iClauseCount = new Integer(0);
					if (oClauseCounts.containsKey(id))
						iClauseCount = oClauseCounts.get(id);
					json.put("ClauseCount", iClauseCount);
					
					Integer iDocumentCount = new Integer(0);
					if (oDocumentCounts.containsKey(id))
						iDocumentCount = oDocumentCounts.get(id);
					json.put("DocumentCount", iDocumentCount);
					
					pagination.incCount();
					pagination.getArrayNode().add(json);
				}
			}
			pagination.send(resp);
		}
		catch (Exception oError) {
			oError.printStackTrace();
		}
		finally {
			SqlUtil.closeInstance(rs1, ps1, conn);
		}
	}

	// ========================================================
	private volatile static ClauseVersionTable instance = null;
	private volatile static Date refreshed = null;
	
	public synchronized static ClauseVersionTable getInstance(Connection conn) {
		if (ClauseVersionTable.instance == null) {
			ClauseVersionTable.instance = new ClauseVersionTable();
			ClauseVersionTable.instance.refresh(conn);
		} else {
			ClauseVersionTable.instance.refreshWhenNeeded(conn);
		}
		return ClauseVersionTable.instance;
	}
	
	public static ClauseVersionRecord getActive() {
		ClauseVersionTable clauseVersionTable = ClauseVersionTable.getInstance(null);
		return clauseVersionTable.findActive();
	}
	
	public static Integer getActiveVersionId() {
		ClauseVersionRecord clauseVersionRecord = ClauseVersionTable.getActive();
		if (clauseVersionRecord == null)
			return null;
		else
			return clauseVersionRecord.getId();
	}
	
	public static ClauseVersionRecord getRecord(int id) {
		ClauseVersionTable clauseVersionTable = ClauseVersionTable.getInstance(null);
		return clauseVersionTable.findById(id);
	}
	
	public static String getName(int id) {
		ClauseVersionRecord clauseVersionRecord = ClauseVersionTable.getRecord(id);
		if (clauseVersionRecord != null)
			return clauseVersionRecord.getName();
		return "";
	}
	
	// Functions -----------------------------------------------------------------
	public static Date getReferHistoryDate(Connection conn) {
		return SysLastTableChangeAtTable.getLastUpdateDate(conn, ClauseVersionRecord.TABLE_CLAUSE_VERSIONS);
	}
	  
	// ========================================================
	public static void loadPaginationForApi(Pagination pagination, boolean pbApiVer1, 
			ArrayList<Integer> supportedClsVersionIds, Connection conn) 
			throws SQLException {
		ClauseVersionTable clauseVersionTable = ClauseVersionTable.getInstance(conn);
		pagination.addDataSource(ClauseVersionRecord.TABLE_CLAUSE_VERSIONS);
		boolean isAcceptableRange = pagination.isAcceptedRange(supportedClsVersionIds.size()); //  clauseVersionTable.size()
		if (isAcceptableRange) {
			int iIndex = pagination.getToSkip();
			int iEnd = pagination.getLimit();
			if (iEnd == 0)
				iEnd = supportedClsVersionIds.size(); // clauseVersionTable.size();
			else {
				iEnd += iIndex;
				if (iEnd > supportedClsVersionIds.size()) // clauseVersionTable.size())
					iEnd = supportedClsVersionIds.size(); // clauseVersionTable.size();
			}
			for (; iIndex < iEnd; iIndex++) {
				Integer clsVersionid = supportedClsVersionIds.get(iIndex); // CJ-582
				ClauseVersionRecord oRecord = clauseVersionTable.findById(clsVersionid.intValue());
				// ClauseVersionRecord oRecord = clauseVersionTable.get(iIndex);
				ObjectNode json = oRecord.genJson(true, pagination.getMapper());
				pagination.getArrayNode().add(json);
				pagination.incCount();
			}
		}
	}
	
	public static ObjectNode getRecordJsonForApi(int id, boolean pbApiVer1, Connection conn) throws SQLException {
		ClauseVersionTable clauseVersionTable = ClauseVersionTable.getInstance(conn);
		ClauseVersionRecord oRecord = clauseVersionTable.findById(id);
		if (oRecord == null) {
			return null;
		}
		ObjectMapper mapper = new ObjectMapper();
		ObjectNode json = oRecord.genJson(true, mapper);
		ClauseVersionChangeTable.populateJsonForApi(json, id, conn);
		return json;
	}

	public static ObjectNode getAllChangesJsonForApi(int id, boolean pbApiVer1, Connection conn) throws SQLException {
		ClauseVersionTable clauseVersionTable = ClauseVersionTable.getInstance(conn);
		ClauseVersionRecord oRecord = clauseVersionTable.findById(id);
		if (oRecord == null) {
			return null;
		}
		ObjectMapper mapper = new ObjectMapper();
		ObjectNode json = oRecord.genJson(true, mapper);
		ClauseVersionChangeTable.populateCombinedChangesJsonForApi(json, id, conn);
		return json;
	}
	
	// ========================================================
	private Exception lastError;
	
	public void refreshWhenNeeded(Connection conn) {
		if (SysLastTableChangeAtTable.needToLoad(ClauseVersionTable.refreshed, getReferHistoryDate(conn)))
			this.refresh(conn);
	}

	private boolean refresh(Connection conn) {
		boolean result = false;
		String sSql = ClauseVersionRecord.SQL_SELECT;
		boolean bCreateConnection = (conn == null);
		PreparedStatement ps1 = null;
		ResultSet rs1 = null;
		try {
			if (bCreateConnection)
				conn = ClsConfig.getDbConnection();
			ps1 = conn.prepareStatement(sSql);
			rs1 = ps1.executeQuery();
			this.clear();
			while (rs1.next()) {
				ClauseVersionRecord clauseVersionRecord = new ClauseVersionRecord();
				clauseVersionRecord.read(rs1);
				this.add(clauseVersionRecord);
			}
			ClauseVersionTable.refreshed = getReferHistoryDate(conn); // CommonUtils.getNow();
			result = true;
			System.out.println("ClauseVersionTable refreshed");
		}
		catch (Exception oError) {
			this.lastError = oError;
			oError.printStackTrace();
		}
		finally {
			SqlUtil.closeInstance(rs1, ps1);
			if (bCreateConnection)
				SqlUtil.closeInstance(conn);
		}
		return result;
	}
	
	public Date getRefreshed() {
		return refreshed;
	}
	
	public ClauseVersionRecord findById(int id) {
		for (ClauseVersionRecord clauseVersionRecord : this) {
			if (clauseVersionRecord.getId().intValue() == id)
				return clauseVersionRecord;
		}
		return null;
	}
	
	public ClauseVersionRecord findActive() {
		for (ClauseVersionRecord clauseVersionRecord : this) {
			if (clauseVersionRecord.isActive())
				return clauseVersionRecord;
		}
		return null;
	}
	
	private ArrayNode referenceJson() {
		ObjectMapper mapper = new ObjectMapper();
		ArrayNode result = mapper.createArrayNode();
		for (ClauseVersionRecord clauseVersionRecord : this) {
			ObjectNode json = mapper.createObjectNode();
			json.put(ClauseVersionRecord.FIELD_CLAUSE_VERSION_ID, clauseVersionRecord.getId());
			json.put(ClauseVersionRecord.FIELD_CLAUSE_VERSION_NAME, clauseVersionRecord.getName());
			result.add(json);
		}
		return result;
	}
	
	public ClauseVersionRecord getRecordById(int id, boolean pbCreateWhenMissing, String psName, Connection conn) 
			throws SQLException {
		ClauseVersionRecord result = this.findById(id);
		if ((result == null) && pbCreateWhenMissing) {
			String sSql = "INSERT INTO " + ClauseVersionRecord.TABLE_CLAUSE_VERSIONS
					+ " (" + ClauseVersionRecord.FIELD_CLAUSE_VERSION_ID
					+ ", " + ClauseVersionRecord.FIELD_CLAUSE_VERSION_NAME
					+ ") VALUES (?, ?)";
			PreparedStatement ps1 = null;
			try {
				if (psName == null)
					psName = id + "";
				ps1 = conn.prepareStatement(sSql);
				ps1.setInt(1, id);
				ps1.setString(2, (psName == null) ? id + "" : psName);
				ps1.execute();
				System.out.println("Created " + ClauseVersionRecord.TABLE_CLAUSE_VERSIONS +
						" record for Id(" + id+ "), Name(" + psName + ")");
				
			} finally {
				SqlUtil.closeInstance(ps1);
			}
			this.refresh(conn);
			result = this.findById(id);
		}
		return result;
	}

	public Exception getLastError() {
		return lastError;
	}
	
	public static ArrayNode getJson(Integer clauseVersionId,  ObjectMapper mapper, Connection conn) throws SQLException { // CJ-584
		
		ArrayNode result = mapper.createArrayNode();
		boolean bCreateConnection = (conn == null);
		
		PreparedStatement ps1 = null;
		ResultSet rs1 = null;
		try {
			
			String sSql = SQL_LIST + 
					" WHERE " + ClauseVersionRecord.FIELD_CLAUSE_VERSION_ID + " = ? ";
			ps1 = conn.prepareStatement(sSql);
			ps1.setInt(1, clauseVersionId);
			rs1 = ps1.executeQuery();
			while (rs1.next()) {
				ObjectNode json = mapper.createObjectNode();
				
				int id = rs1.getInt(ClauseVersionRecord.FIELD_CLAUSE_VERSION_ID);
				json.put(ClauseVersionRecord.FIELD_CLAUSE_VERSION_ID, id);
				json.put(ClauseVersionRecord.FIELD_CLAUSE_VERSION_NAME, rs1.getString(ClauseVersionRecord.FIELD_CLAUSE_VERSION_NAME));
				json.put(ClauseVersionRecord.FIELD_CLAUSE_VERSION_DATE, CommonUtils.toJsonDate(rs1.getDate(ClauseVersionRecord.FIELD_CLAUSE_VERSION_DATE)));
				json.put(ClauseVersionRecord.FIELD_IS_ACTIVE, rs1.getBoolean(ClauseVersionRecord.FIELD_IS_ACTIVE));			
				json.put(ClauseVersionRecord.FIELD_FAC_NUMBER, rs1.getString(ClauseVersionRecord.FIELD_FAC_NUMBER));
				json.put(ClauseVersionRecord.FIELD_DAC_NUMBER, rs1.getString(ClauseVersionRecord.FIELD_DAC_NUMBER));
				json.put(ClauseVersionRecord.FIELD_DFARS_NUMBER, rs1.getString(ClauseVersionRecord.FIELD_DFARS_NUMBER));
				json.put(ClauseVersionRecord.FIELD_FAC_DATE, CommonUtils.toString(rs1.getDate(ClauseVersionRecord.FIELD_FAC_DATE)));
				json.put(ClauseVersionRecord.FIELD_DAC_DATE, CommonUtils.toString(rs1.getDate(ClauseVersionRecord.FIELD_DAC_DATE)));
				json.put(ClauseVersionRecord.FIELD_DFARS_DATE, CommonUtils.toString(rs1.getDate(ClauseVersionRecord.FIELD_DFARS_DATE)));
				// CJ-1360
				json.put(ClauseVersionRecord.FIELD_SAT_CONDITIONS, rs1.getString(ClauseVersionRecord.FIELD_SAT_CONDITIONS));
				json.put(ClauseVersionRecord.FIELD_SAT_300K_RULE, rs1.getString(ClauseVersionRecord.FIELD_SAT_300K_RULE));
				json.put(ClauseVersionRecord.FIELD_SAT_1MILLION_RULE, rs1.getString(ClauseVersionRecord.FIELD_SAT_1MILLION_RULE));


				result.add(json);
			}
		}
		finally {
			SqlUtil.closeInstance(rs1, ps1);
			if (bCreateConnection)
				SqlUtil.closeInstance(conn);
		}
		return result;
		
	}
}
