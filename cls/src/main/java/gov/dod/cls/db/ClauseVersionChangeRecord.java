package gov.dod.cls.db;

import gov.dod.cls.utils.sql.SqlUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import org.jasypt.commons.CommonUtils;

public class ClauseVersionChangeRecord {
	
	public static final String IS_CLAUSE = "C";
	public static final String IS_QUESTION = "Q";
	
	public static final String CODE_ADDED = "Added";
	public static final String CODE_CHANGED = "Changed";
	public static final String CODE_REMOVED = "Removed";

	public static final String TABLE_CLAUSE_VERSION_CHANGES = "Clause_Version_Changes";
	
	public static final String FIELD_CLAUSE_VER_CHANGE_ID = "Clause_Ver_Change_Id";
	public static final String FIELD_CLAUSE_VERSION_ID    = "Clause_Version_Id";
	public static final String FIELD_PREVIOUS_CLAUSE_VERSION_ID = "Previous_Clause_Version_Id";
	public static final String FIELD_IS_QUESTION_OR_CLAUSE = "Is_Question_Or_Clause";
	public static final String FIELD_QUESTION_OR_CLAUSE_CODE = "Question_Or_Clause_Code";
	public static final String FIELD_CHANGE_CODE          = "Change_Code";
	public static final String FIELD_CHANGE_BRIEF         = "Change_Brief";
	public static final String FIELD_CREATED_AT           = "Created_At";

	public static final String SQL_SELECT
		= "SELECT " + FIELD_CLAUSE_VER_CHANGE_ID
		+ ", " + FIELD_CLAUSE_VERSION_ID
		+ ", " + FIELD_PREVIOUS_CLAUSE_VERSION_ID
		+ ", " + FIELD_IS_QUESTION_OR_CLAUSE
		+ ", " + FIELD_QUESTION_OR_CLAUSE_CODE
		+ ", " + FIELD_CHANGE_CODE
		+ ", " + FIELD_CHANGE_BRIEF
		+ ", " + FIELD_CREATED_AT
		+ " FROM " + TABLE_CLAUSE_VERSION_CHANGES;
		
	public static PreparedStatement preparInsert(Connection conn) throws SQLException {
		String sSql = "INSERT INTO " + TABLE_CLAUSE_VERSION_CHANGES
				+  "(" + FIELD_CLAUSE_VERSION_ID
				+ ", " + FIELD_PREVIOUS_CLAUSE_VERSION_ID
				+ ", " + FIELD_IS_QUESTION_OR_CLAUSE
				+ ", " + FIELD_QUESTION_OR_CLAUSE_CODE
				+ ", " + FIELD_CHANGE_CODE
				+ ", " + FIELD_CHANGE_BRIEF + ")"
				+ "VALUES (?, ?, ?, ?, ?, ?)";
		return conn.prepareCall(sSql);
	}
	
	// ------------------------------------------------------------------
	private Integer clauseVerChangeId;
	private Integer clauseVersionId;
	private Integer previousClauseVersionId;
	private String isQuestionOrClause; // CHAR NOT NULL CHECK (IsQuestionOr_Clause IN ('C', 'Q')),
	private String questionOrClauseCode;
	private String changeCode;
	private String changeBrief;
	private Date createdAt;
	
	public void insert(PreparedStatement ps) throws SQLException {
		SqlUtil.setParam(ps, 1, this.clauseVersionId, java.sql.Types.INTEGER);
		SqlUtil.setParam(ps, 1, this.previousClauseVersionId, java.sql.Types.INTEGER);
		SqlUtil.setParam(ps, 1, this.isQuestionOrClause, java.sql.Types.VARCHAR);
		SqlUtil.setParam(ps, 1, this.questionOrClauseCode, java.sql.Types.VARCHAR);
		SqlUtil.setParam(ps, 1, this.changeCode, java.sql.Types.VARCHAR);
		SqlUtil.setParam(ps, 1, this.changeBrief, java.sql.Types.VARCHAR);
		ps.execute();
	}
	
	public void read(ResultSet rs) throws SQLException {
		this.clauseVerChangeId = rs.getInt(FIELD_CLAUSE_VER_CHANGE_ID);
		this.clauseVersionId = rs.getInt(FIELD_CLAUSE_VERSION_ID);
		this.previousClauseVersionId = rs.getInt(FIELD_PREVIOUS_CLAUSE_VERSION_ID);
		this.isQuestionOrClause = rs.getString(FIELD_IS_QUESTION_OR_CLAUSE);
		this.questionOrClauseCode = rs.getString(FIELD_QUESTION_OR_CLAUSE_CODE);
		this.changeCode = rs.getString(FIELD_CHANGE_CODE);
		this.changeBrief = rs.getString(FIELD_CHANGE_BRIEF);
		this.createdAt = rs.getTimestamp(FIELD_CREATED_AT);
	}

	public String generateInsertSql() {
		String sChangeBrief;
		if (CommonUtils.isEmpty(this.changeBrief)) {
			sChangeBrief = this.changeCode;
			if (!CODE_ADDED.equals(this.changeCode))
				System.out.println("ClauseVersionChangeRecord.generateInsertSql() Empty changeBrief for (" +
						this.isQuestionOrClause + ") " + this.questionOrClauseCode + " [" + this.changeCode + "]");
		} else
			sChangeBrief = this.changeBrief;
		String sSql =
				"\nINSERT INTO " + TABLE_CLAUSE_VERSION_CHANGES
				+ " (" + FIELD_CLAUSE_VERSION_ID
				+ ", " + FIELD_PREVIOUS_CLAUSE_VERSION_ID
				+ ", " + FIELD_IS_QUESTION_OR_CLAUSE
				+ ", " + FIELD_QUESTION_OR_CLAUSE_CODE
				+ ", " + FIELD_CHANGE_CODE
				+ ", " + FIELD_CHANGE_BRIEF + ")"
				+ "\nVALUES (" + this.clauseVersionId +
				", " + this.previousClauseVersionId +
				", " + SqlUtil.quoteString(this.isQuestionOrClause) +
				", " + SqlUtil.quoteString(this.questionOrClauseCode) +
				", " + SqlUtil.quoteString(this.changeCode) +
				", " + SqlUtil.quoteString(sChangeBrief) +
				");";
		
		return sSql;
	}
	
	public void clear(Integer piClauseVersionId, Integer piPreviousClauseVersionId) {
		this.clauseVerChangeId = null;
		this.clauseVersionId = piClauseVersionId;
		if ((piPreviousClauseVersionId == null) && (piClauseVersionId != null))
			this.previousClauseVersionId = piClauseVersionId;
		else
			this.previousClauseVersionId = piPreviousClauseVersionId;
		this.isQuestionOrClause = null;
		this.questionOrClauseCode = null;
		this.changeCode = null;
		this.changeBrief = null;
		this.createdAt = null;
	}
	
	// ------------------------------------------------------------------
	public Integer getClauseVersionId() {
		return clauseVersionId;
	}
	public void setClauseVersionId(Integer clauseVersionId) {
		this.clauseVersionId = clauseVersionId;
	}
	public Integer getPreviousClauseVersionId() {
		return previousClauseVersionId;
	}
	public void setPreviousClauseVersionId(Integer previousClauseVersionId) {
		this.previousClauseVersionId = previousClauseVersionId;
	}
	public String getIsQuestionOrClause() {
		return isQuestionOrClause;
	}
	public void setIsQuestionOrClause(String isQuestionOrClause) {
		this.isQuestionOrClause = isQuestionOrClause;
	}
	public String getQuestionOrClauseCode() {
		return questionOrClauseCode;
	}
	public void setQuestionOrClauseCode(String questionOrClauseCode) {
		this.questionOrClauseCode = questionOrClauseCode;
	}
	public String getChangeCode() {
		return changeCode;
	}
	public void setChangeCode(String changeCode) {
		this.changeCode = changeCode;
	}
	public String getChangeBrief() {
		return changeBrief;
	}
	public void setChangeBrief(String changeBrief) {
		this.changeBrief = changeBrief;
	}
	public Date getCreatedAt() {
		return createdAt;
	}
	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}
	public Integer getClauseVerChangeId() {
		return clauseVerChangeId;
	}
	
	public boolean isClause() {
		return IS_CLAUSE.equals(this.isQuestionOrClause);
	}
	public void setClause(String psClauseNumber) {
		this.isQuestionOrClause = IS_CLAUSE;
		this.questionOrClauseCode = psClauseNumber;
	}
	
	public boolean isQuestion() {
		return IS_QUESTION.equals(this.isQuestionOrClause);
	}
	public void setQuestion(String psQuestionName) {
		this.isQuestionOrClause = IS_QUESTION; 
		this.questionOrClauseCode = psQuestionName;
	}

	public void setAdded() {
		this.changeCode = CODE_ADDED;
	}
	public void setChanged() {
		this.changeCode = CODE_CHANGED;
	}
	public void setRemoved() {
		this.changeCode = CODE_REMOVED;
	}
	
	public boolean isEmpty() {
		if ((this.questionOrClauseCode == null) || (this.changeCode == null))
			return true;
		else
			return false;
	}
	
	public boolean isNotEmpty() {
		return !this.isEmpty();
	}
	
	public void addChangeBrief(String psBrief) {
		if (CommonUtils.isEmpty(this.changeBrief))
			this.changeBrief = psBrief;
		else
			this.changeBrief += ", " + psBrief;
	}

}
