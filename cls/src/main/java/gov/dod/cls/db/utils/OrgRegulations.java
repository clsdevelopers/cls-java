package gov.dod.cls.db.utils;

import gov.dod.cls.config.ClsConfig;
import gov.dod.cls.db.OrgRegulationRecord;
import gov.dod.cls.db.RegulationRecord;
import gov.dod.cls.utils.sql.SqlUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.codehaus.jackson.node.ObjectNode;

public class OrgRegulations extends ArrayList<String> {
	
	private static final long serialVersionUID = 1276063692364731172L;

	public static final String TAG_REGULATIONS = "Regulations";
	
	public static final String SQL_SELECT	
	= "SELECT " + RegulationRecord.TABLE_REGULATIONS + "." + RegulationRecord.FIELD_REGULATION_NAME
	+ " FROM " + OrgRegulationRecord.TABLE_ORG_REGULATIONS
	+ " INNER JOIN " + RegulationRecord.TABLE_REGULATIONS
	+ " ON (" + RegulationRecord.TABLE_REGULATIONS + "." + RegulationRecord.FIELD_REGULATION_ID
		+ "=" + OrgRegulationRecord.TABLE_ORG_REGULATIONS + "." + OrgRegulationRecord.FIELD_REGULATION_ID + ")"
	+ " WHERE " + OrgRegulationRecord.TABLE_ORG_REGULATIONS + "." + OrgRegulationRecord.FIELD_ORG_ID + " = ?";
	
	// ------------------------------------------------------
	private Integer orgId;
	
	public void load(Integer orgId, Connection conn) throws SQLException {
		this.orgId = orgId;
		this.clear();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = conn.prepareStatement(SQL_SELECT);
			ps.setInt(1, this.orgId);
			rs = ps.executeQuery();
			while (rs.next()) {
				this.add(rs.getString(1));
			}
		}
		//catch (Exception oError) {
		//	oError.printStackTrace();
		//}
		finally {
			SqlUtil.closeInstance(rs, ps);
		}
	}

	public void load(Integer orgId) throws SQLException {
		Connection conn = null;
		try {
			conn = ClsConfig.getDbConnection();
			this.load(orgId, conn); 
		}
		finally {
			SqlUtil.closeInstance(conn);
		}
	}
	
	public void addJson(ObjectNode json) {
		String regulations = null;
		for (String regulation : this) {
			if (regulations == null)
				regulations = regulation;
			else
				regulations += ", " + regulation;
		}
		json.put(TAG_REGULATIONS, regulations);
	}
	
	public Integer getOrgId() {
		return orgId;
	}
	
	public boolean isREGUL_FAR() {
		return this.contains(RegulationRecord.REGUL_FAR);
	}
	
	public boolean isREGUL_DFARS() {
		return this.contains(RegulationRecord.REGUL_DFARS);
	}
	
	public boolean isREGUL_AFFARS() {
		return this.contains(RegulationRecord.REGUL_AFFARS);
	}
	
	public boolean isREGUL_AFARS() {
		return this.contains(RegulationRecord.REGUL_AFARS);
	}
	
	public boolean isREGUL_NMCARS() {
		return this.contains(RegulationRecord.REGUL_NMCARS);
	}
	
	public boolean isREGUL_USSOCOM() {
		return this.contains(RegulationRecord.REGUL_USSOCOM);
	}
	
	public boolean isREGUL_USTRANSCOM() {
		return this.contains(RegulationRecord.REGUL_USTRANSCOM);
	}

	public boolean isREGUL_DARS() {
		return this.contains(RegulationRecord.REGUL_DARS);
	}

}
