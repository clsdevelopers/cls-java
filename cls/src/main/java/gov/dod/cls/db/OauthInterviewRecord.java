package gov.dod.cls.db;

import gov.dod.cls.config.ClsConfig;
import gov.dod.cls.utils.CommonUtils;
import gov.dod.cls.utils.sql.SqlInsert;
import gov.dod.cls.utils.sql.SqlUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.node.ObjectNode;

public class OauthInterviewRecord implements java.io.Serializable {
	
	private static final long serialVersionUID = 6249715823695341401L;

	public static final String TABLE_OAUTH_INTERVIEW = "OAuth_Interview";
	
	public static final String FIELD_OAUTH_INTERVIEW_ID   = "oauth_interview_id";
	public static final String FIELD_APPLICATION_ID       = "application_id";
	public static final String FIELD_OAUTH_ACCESS_ID	= "oauth_access_id";
	public static final String FIELD_DOCUMENT_ID          = "document_id";
	public static final String FIELD_INTERVIEW_TOKEN      = "interview_token";
	public static final String FIELD_EXPIRES_AT           = "expires_at";
	public static final String FIELD_CREATED_AT           = "created_at";
	public static final String FIELD_LAUNCHED_AT	= "launched_at";
	
	public static final String SQL_SELECT =
			"SELECT " + FIELD_OAUTH_INTERVIEW_ID
			+ ", " + FIELD_APPLICATION_ID
			+ ", " + FIELD_OAUTH_ACCESS_ID
			+ ", " + FIELD_DOCUMENT_ID
			+ ", " + FIELD_INTERVIEW_TOKEN
			+ ", " + FIELD_EXPIRES_AT
			+ ", " + FIELD_CREATED_AT
			+ ", " + FIELD_LAUNCHED_AT
			+ " FROM " + TABLE_OAUTH_INTERVIEW;

	private Integer oauthInterviewId;
	private Integer applicationId;
	private Integer oauthAccessId;
	private Integer documentId;
	private String interviewToken;
	private Date expiresAt;
	private Date createdAt;
	private Date launchedAt;
	private String orgName = null;
	
	public void read(ResultSet rs) throws SQLException {
		this.oauthInterviewId = rs.getInt(FIELD_OAUTH_INTERVIEW_ID);
		this.applicationId = rs.getInt(FIELD_APPLICATION_ID);
		this.oauthAccessId = CommonUtils.toInteger(rs.getObject(FIELD_OAUTH_ACCESS_ID));
		this.documentId = rs.getInt(FIELD_DOCUMENT_ID);
		this.interviewToken = rs.getString(FIELD_INTERVIEW_TOKEN);
		this.expiresAt = rs.getTimestamp(FIELD_EXPIRES_AT);
		this.createdAt = rs.getTimestamp(FIELD_CREATED_AT);
		this.launchedAt = rs.getTimestamp(FIELD_LAUNCHED_AT);
	}
	
	public void fillJson(ObjectNode recordNode, boolean forApi) {
		recordNode.put("id", this.oauthInterviewId);
		recordNode.put("launch_token", this.interviewToken);
		recordNode.put("document_id", this.documentId);
		if (forApi) {
			recordNode.put("launch_at", CommonUtils.toZulu(this.launchedAt));
		} else {
			recordNode.put("launch_at", CommonUtils.toJsonDate(this.launchedAt));
			recordNode.put("expires_at", CommonUtils.toJsonDate(this.expiresAt));
			recordNode.put("application_id", this.applicationId);
			recordNode.put("oauth_access_id", this.oauthAccessId);
			recordNode.put("orgName", this.orgName);
			
			
			if (this.launchedAt != null) System.out.println("oauthInterviewRecord: launch_at = " + new SimpleDateFormat("d MMM yy H:mm:ss").format(this.launchedAt));
			if (this.expiresAt != null) System.out.println("oauthInterviewRecord: expires_at = " + new SimpleDateFormat("d MMM yy H:mm:ss").format(this.expiresAt));
			if (this.applicationId != null) System.out.println("oauthInterviewRecord: applicationId = " + this.applicationId);
			if (this.oauthAccessId != null) System.out.println("oauthInterviewRecord: oauthAccessId = " + this.oauthAccessId);
			if (this.orgName != null) System.out.println("oauthInterviewRecord: orgName = " + this.orgName);
		}
	}
	
	public ObjectNode genJson(boolean forApi, ResultSet rs1, ObjectMapper mapper) throws SQLException {
		ObjectNode recordNode = mapper.createObjectNode();
		
		recordNode.put("id", this.oauthInterviewId);
		recordNode.put("application_id", this.applicationId);
		recordNode.put("document_id", this.documentId);
//		if (this.isExpired(null, false)) {
//			this.interviewToken = null;
//			this.expiresAt = null;
//		}
		recordNode.put("launch_token", this.interviewToken);
		if (forApi) {
			recordNode.put("expires_at", CommonUtils.toZulu(this.expiresAt)); 
			recordNode.put("launch_at", CommonUtils.toZulu(this.launchedAt));
			recordNode.put("created_at", CommonUtils.toZulu(this.createdAt));
		} else {
			recordNode.put("expires_at", CommonUtils.toJsonDate(this.expiresAt)); 
			recordNode.put("launch_at", CommonUtils.toJsonDate(this.launchedAt));
			recordNode.put("created_at", CommonUtils.toJsonDate(this.createdAt));
			recordNode.put("oauth_access_id", this.oauthAccessId);
		}
		
		return recordNode;
	}
	
	public static OauthInterviewRecord getRecord(Connection conn, String interviewToken) throws SQLException {
		boolean bCreateConnection = (conn == null);
		PreparedStatement ps1 = null;
		ResultSet rs1 = null;
		try {
			String sSql = SQL_SELECT
					+ " WHERE " + FIELD_INTERVIEW_TOKEN + " = ?";
			if (bCreateConnection)
				conn = ClsConfig.getDbConnection();
			ps1 = conn.prepareStatement(sSql);
			ps1.setString(1, interviewToken);
			rs1 = ps1.executeQuery();
			if (rs1.next()) {
				OauthInterviewRecord oRecord = new OauthInterviewRecord();
				oRecord.read(rs1);
				return oRecord;
			}
			return null;
		}
		finally {
			SqlUtil.closeInstance(rs1, ps1);
			if (bCreateConnection)
				SqlUtil.closeInstance(conn);
		}
	}
	
	public static OauthInterviewRecord getRecord(Connection conn, int oauthInterviewId) throws SQLException {
		boolean bCreateConnection = (conn == null);
		PreparedStatement ps1 = null;
		ResultSet rs1 = null;
		try {
			String sSql = SQL_SELECT
					+ " WHERE " + FIELD_OAUTH_INTERVIEW_ID + " = ?";
			if (bCreateConnection)
				conn = ClsConfig.getDbConnection();
			ps1 = conn.prepareStatement(sSql);
			ps1.setInt(1, oauthInterviewId);
			rs1 = ps1.executeQuery();
			if (rs1.next()) {
				OauthInterviewRecord oRecord = new OauthInterviewRecord();
				oRecord.read(rs1);
				return oRecord;
			}
			return null;
		}
		finally {
			SqlUtil.closeInstance(rs1, ps1);
			if (bCreateConnection)
				SqlUtil.closeInstance(conn);
		}
	}
	
	public static OauthInterviewRecord getRecord(Connection conn, int applicationId, int documentId) throws SQLException { // int oauthAccessId, 
		boolean bCreateConnection = (conn == null);
		PreparedStatement ps1 = null;
		ResultSet rs1 = null;
		try {
			String sSql = SQL_SELECT
					+ " WHERE " + FIELD_APPLICATION_ID + " = ?"
					// + " AND " + FIELD_OAUTH_ACCESS_ID + " = ?"
					+ " AND " + FIELD_DOCUMENT_ID + " = ?";
			if (bCreateConnection)
				conn = ClsConfig.getDbConnection();
			ps1 = conn.prepareStatement(sSql);
			ps1.setInt(1, applicationId);
			// ps1.setInt(2, oauthAccessId);
			ps1.setInt(2, documentId);
			rs1 = ps1.executeQuery();
			if (rs1.next()) {
				OauthInterviewRecord oRecord = new OauthInterviewRecord();
				oRecord.read(rs1);
				return oRecord;
			}
			return null;
		}
		finally {
			SqlUtil.closeInstance(rs1, ps1);
			if (bCreateConnection)
				SqlUtil.closeInstance(conn);
		}
	}
	
	public static OauthInterviewRecord insert(Connection conn, int applicationId, int oauthAccessId, int documentId, String interviewToken) 
			throws Exception {
		
		boolean bCreateConnection = (conn == null);
		try {
			Calendar calStart = Calendar.getInstance();
			java.sql.Timestamp createdAt = new java.sql.Timestamp(calStart.getTimeInMillis());
			
			Calendar calEnd = ClsConfig.getInstance().getTokenExpiresIn();
			// Calendar calEnd = Calendar.getInstance();
			// calEnd.add(Calendar.SECOND, Oauth.EXPIRES_IN);
			java.sql.Timestamp expiresAt = new java.sql.Timestamp(calEnd.getTimeInMillis());
			
			AuditEventRecord auditEventRecord = new AuditEventRecord(AuditEventRecord.ACTION_ORGANIZATION_CREATED, null, null);
			SqlInsert sqlInsert = new SqlInsert(null, TABLE_OAUTH_INTERVIEW);
			sqlInsert.addField(FIELD_APPLICATION_ID, applicationId, auditEventRecord);
			sqlInsert.addField(FIELD_OAUTH_ACCESS_ID, oauthAccessId, auditEventRecord);
			sqlInsert.addField(FIELD_DOCUMENT_ID, documentId, auditEventRecord);
			sqlInsert.addField(FIELD_INTERVIEW_TOKEN, interviewToken, auditEventRecord);
			sqlInsert.addField(FIELD_EXPIRES_AT, expiresAt, auditEventRecord);
			sqlInsert.addField(FIELD_CREATED_AT, createdAt, auditEventRecord);
			
			if (bCreateConnection)
				conn = ClsConfig.getDbConnection();
			
			sqlInsert.execute(conn);
			
			// CJ-1410. Save interview_id in audit additional details.
			OauthInterviewRecord oaRec =  OauthInterviewRecord.getRecord(conn, applicationId, documentId); // oauthAccessId, 
			if (oaRec != null)
				auditEventRecord.addAttribute("interview_id", oaRec.getOauthInterviewId().toString());
			auditEventRecord.saveNew(conn);
			return oaRec;
		}
		finally {
			if (bCreateConnection)
				SqlUtil.closeInstance(conn);
		}
	}
	
	public void deleteRecord(Connection conn) throws Exception {
		boolean bCreateConnection = (conn == null);
		PreparedStatement ps1 = null;
		try {
			String sSql = "DELETE FROM " + TABLE_OAUTH_INTERVIEW
					+ " WHERE " + FIELD_OAUTH_INTERVIEW_ID + " = ?";
			// String sSql = "DELETE FROM " + TABLE_OAUTH_INTERVIEW + " WHERE " + FIELD_OAUTH_INTERVIEW_ID + " = ?";
			if (bCreateConnection)
				conn = ClsConfig.getDbConnection();
			ps1 = conn.prepareStatement(sSql);
			ps1.setInt(1, this.oauthInterviewId);
			
			ps1.execute();
			
			AuditEventRecord auditEventRecord = new AuditEventRecord(AuditEventRecord.ACTION_ORGANIZATION_DESTROYED, null,null);
			auditEventRecord.addAttribute(FIELD_APPLICATION_ID, this.applicationId);
			auditEventRecord.addAttribute(FIELD_OAUTH_ACCESS_ID, this.oauthAccessId);
			auditEventRecord.addAttribute(FIELD_DOCUMENT_ID, documentId);
			auditEventRecord.addAttribute(FIELD_INTERVIEW_TOKEN, this.interviewToken);
			auditEventRecord.addAttribute(FIELD_OAUTH_INTERVIEW_ID, this.oauthInterviewId);
			auditEventRecord.saveNew(conn);
		}
		finally {
			SqlUtil.closeInstance(ps1);
			if (bCreateConnection)
				SqlUtil.closeInstance(conn);
		}
	}
	
	public void removeToken(Connection conn) throws SQLException {
		boolean bCreateConnection = (conn == null);
		PreparedStatement ps1 = null;
		try {
			String sSql = "UPDATE " + TABLE_OAUTH_INTERVIEW
					+ " SET " + FIELD_INTERVIEW_TOKEN + " = NULL"
					+ ", " + FIELD_OAUTH_ACCESS_ID + " = NULL"
					+ ", " + FIELD_EXPIRES_AT + " = NULL"
					+ " WHERE " + FIELD_OAUTH_INTERVIEW_ID + " = ?";
			// String sSql = "DELETE FROM " + TABLE_OAUTH_INTERVIEW + " WHERE " + FIELD_OAUTH_INTERVIEW_ID + " = ?";
			if (bCreateConnection)
				conn = ClsConfig.getDbConnection();
			ps1 = conn.prepareStatement(sSql);
			ps1.setInt(1, this.oauthInterviewId);
			ps1.execute();
			this.interviewToken = null;
			this.expiresAt = null;
			this.oauthAccessId = null;
		}
		finally {
			SqlUtil.closeInstance(ps1);
			if (bCreateConnection)
				SqlUtil.closeInstance(conn);
		}
	}
	
	public void assignToken(Connection conn, String psToken, int piOauthAccessId) throws SQLException {
		boolean bCreateConnection = (conn == null);
		PreparedStatement ps1 = null;
		try {
			Calendar calStart = Calendar.getInstance();
			java.sql.Timestamp createdAt = new java.sql.Timestamp(calStart.getTimeInMillis());
			
			Calendar calEnd = ClsConfig.getInstance().getTokenExpiresIn();
			java.sql.Timestamp expiresAt = new java.sql.Timestamp(calEnd.getTimeInMillis());
			
			String sSql = "UPDATE " + TABLE_OAUTH_INTERVIEW
					+ " SET " + FIELD_INTERVIEW_TOKEN + " = ?"
					+ ", " + FIELD_OAUTH_ACCESS_ID + " = ?"
					+ ", " + FIELD_EXPIRES_AT + " = ?"
					// + ", " + FIELD_LAUNCHED_AT + " = null"
					+ " WHERE " + FIELD_OAUTH_INTERVIEW_ID + " = ?";
			if (bCreateConnection)
				conn = ClsConfig.getDbConnection();
			ps1 = conn.prepareStatement(sSql);
			ps1.setString(1, psToken);
			ps1.setInt(2, piOauthAccessId);
			ps1.setTimestamp(3, expiresAt);
			ps1.setInt(4, this.oauthInterviewId);
			ps1.execute();

			this.oauthAccessId = piOauthAccessId;
			this.interviewToken = psToken;
			this.expiresAt = calStart.getTime();
			this.launchedAt = null;
			if (this.createdAt == null)
				this.createdAt = createdAt;
		}
		finally {
			SqlUtil.closeInstance(ps1);
			if (bCreateConnection)
				SqlUtil.closeInstance(conn);
		}
	}
	
	public void extendExpiration(Connection conn) throws SQLException {
		PreparedStatement ps1 = null;
		try {
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(this.expiresAt);
			ClsConfig clsConfig = ClsConfig.getInstance(); // .getTokenExpiresIn();
			calendar.add(Calendar.MINUTE, clsConfig.getTokenInterviewTimeout());

			java.sql.Timestamp timestamp = new java.sql.Timestamp(calendar.getTimeInMillis());

			String sSql = "UPDATE " + TABLE_OAUTH_INTERVIEW
					+ " SET " + FIELD_EXPIRES_AT + " = ?"
					+ " WHERE " + FIELD_OAUTH_INTERVIEW_ID + " = ?";
			ps1 = conn.prepareStatement(sSql);
			ps1.setTimestamp(1, timestamp);
			ps1.setInt(2, this.oauthInterviewId);
			ps1.execute();
			this.launchedAt = timestamp;
		}
		finally {
			SqlUtil.closeInstance(ps1);
		}
	}

	public boolean isExpired(Connection conn, boolean remove) throws SQLException {
		if (this.interviewToken == null)
			return true;
		Calendar calNow = Calendar.getInstance();
		//Date now = calNow.getTime();
		Calendar calExpiresAt = Calendar.getInstance();
		calExpiresAt.setTimeInMillis(this.expiresAt.getTime());
		if (calNow.after(calExpiresAt)) {
			System.out.println("Interview token expired at " + calNow.getTime().toString() + " for " + calExpiresAt.getTime().toString());
			if (remove) {
				this.removeToken(conn); // delete()
			}
			return true;
		}
		return false;
	}
	
	public void updateLaunchedAt(Connection conn) throws SQLException {
		boolean bCreateConnection = (conn == null);
		PreparedStatement ps1 = null;
		try {
			Calendar calNow = Calendar.getInstance();
			java.sql.Timestamp timestamp = new java.sql.Timestamp(calNow.getTimeInMillis());
			String sSql = "UPDATE " + TABLE_OAUTH_INTERVIEW
					+ " SET " + FIELD_LAUNCHED_AT + " = ?"
					+ " WHERE " + FIELD_OAUTH_INTERVIEW_ID + " = ?";
			if (bCreateConnection)
				conn = ClsConfig.getDbConnection();
			ps1 = conn.prepareStatement(sSql);
			ps1.setTimestamp(1, timestamp);
			ps1.setInt(2, this.oauthInterviewId);
			ps1.execute();
			this.launchedAt = timestamp;
		}
		finally {
			SqlUtil.closeInstance(ps1);
			if (bCreateConnection)
				SqlUtil.closeInstance(conn);
		}
	}

	// ----------------------------------------------------------
	public Integer getOauthInterviewId() {
		return oauthInterviewId;
	}

	public Integer getApplicationId() {
		return applicationId;
	}

	public Integer getOauthAccessId() {
		return oauthAccessId;
	}

	public Integer getDocumentId() {
		return documentId;
	}

	public String getInterviewToken() {
		return interviewToken;
	}

	public Date getExpiresAt() {
		return expiresAt;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public Date getLaunchedAt() {
		return launchedAt;
	}
	
	public boolean isLaunchedBefore() {
		return (this.launchedAt != null);
	}

	public String getOrgName() {
		return orgName;
	}

	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}

}
