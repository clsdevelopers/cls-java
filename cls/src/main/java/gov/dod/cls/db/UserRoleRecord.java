package gov.dod.cls.db;

import java.util.Date;

public class UserRoleRecord {
	
	public final static String TABLE_USER_ROLES = "User_Roles";

	public final static String FIELD_USER_ROLE_ID = "User_Role_Id";
	public final static String FIELD_USER_ID = "user_id";
	public final static String FIELD_ROLE_ID = "role_id";
	public final static String FIELD_CREATED_AT = "created_at";
	public final static String FIELD_UPDATED_AT = "updated_at";
	
	// ---------------------------------------------------
	private Integer id;
	private Integer userId;
	private Integer roleId;
	private Date createdAt;
	private Date updatedAt;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	
	public Integer getRoleId() {
		return roleId;
	}
	public void setRoleId(Integer roleId) {
		this.roleId = roleId;
	}
	
	public Date getCreatedAt() {
		return createdAt;
	}
	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}
	
	public Date getUpdatedAt() {
		return updatedAt;
	}
	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

}
