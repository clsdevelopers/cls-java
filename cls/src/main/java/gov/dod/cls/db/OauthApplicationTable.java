package gov.dod.cls.db;

import gov.dod.cls.PageApi;
import gov.dod.cls.bean.session.UserSession;
import gov.dod.cls.config.ClsConfig;
import gov.dod.cls.model.JsonAble;
import gov.dod.cls.utils.CommonUtils;
import gov.dod.cls.utils.Pagination;
import gov.dod.cls.utils.sql.SqlUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.codehaus.jackson.node.ObjectNode;

public class OauthApplicationTable {

	public static final String PREFIX_DO_OAUTH_APPLICATION = "oauthapp-";
	
	public static final String DO_OAUTH_APPLICATION_LIST = PREFIX_DO_OAUTH_APPLICATION + "list";
	public static final String DO_OAUTH_APPLICATION_DETAIL_GET = PREFIX_DO_OAUTH_APPLICATION + "detail-get";
	public static final String DO_OAUTH_APPLICATION_DETAIL_EDIT = PREFIX_DO_OAUTH_APPLICATION + "detail-edit";
	public static final String DO_OAUTH_APPLICATION_DELETE = PREFIX_DO_OAUTH_APPLICATION + "delete";

	public static void processRequest(String sDo, UserSession oUserSession, 
			HttpServletRequest req, HttpServletResponse resp) {
		if (!(oUserSession.isSuperUser() || oUserSession.isSubsuperUser())) { // CJ-680
			return;
		}
		
		String errorMessage = null;
		OauthApplicationRecord oRecord = null;
		Integer applicationId;
		String response;
		
		switch (sDo) {
		case OauthApplicationTable.DO_OAUTH_APPLICATION_LIST:
			OauthApplicationTable.processList(req, resp, oUserSession);
			return;

		case OauthApplicationTable.DO_OAUTH_APPLICATION_DETAIL_GET:
			applicationId = CommonUtils.toInteger(req.getParameter("id"));
			if (applicationId == null)
				return;
			try {
				Integer orgId = oUserSession.isSuperUser() ? null : oUserSession.getOrgId(); // CJ-680
				oRecord = OauthApplicationRecord.getRecord(applicationId, null, orgId, null);
				if (oRecord == null) {
					errorMessage = "The requested record is not found.";
				}
			} catch (Exception oError) {
				errorMessage = "Unable to retrieve record. Error: " + oError.getMessage();
			}
			if (errorMessage == null)
				response = JsonAble.jsonResult(JsonAble.STATUS_SUCCESS, oRecord.toJson(), null);
			else
				response = JsonAble.jsonResult(JsonAble.STATUS_FAIL, null, errorMessage);
			PageApi.send(resp, JsonAble.JSON_RESPONSE_FORMAT, response);
			return;

		case DO_OAUTH_APPLICATION_DETAIL_EDIT:
			PageApi.send(resp, JsonAble.JSON_RESPONSE_FORMAT, OauthApplicationTable.postEdit(oUserSession, req, resp));
			return;

		case DO_OAUTH_APPLICATION_DELETE:
			applicationId = CommonUtils.toInteger(req.getParameter("id"));
			if (applicationId == null)
				return;
			
			errorMessage = OauthApplicationTable.deleteRecord(oUserSession, req, resp, applicationId.intValue());
			if (errorMessage == null)
				response = JsonAble.jsonResult(JsonAble.STATUS_SUCCESS, null, "record deleted");
			else
				response = JsonAble.jsonResult(JsonAble.STATUS_FAIL, null, errorMessage);
			PageApi.send(resp, JsonAble.JSON_RESPONSE_FORMAT, response);
			return;
		}
	}
	
	private static String postEdit(UserSession oUserSession, HttpServletRequest req, HttpServletResponse resp) {
		String errorMessage = null;

		String sMode = req.getParameter("mode");
		String sId = req.getParameter("id");
		OauthApplicationRecord postRecord = new OauthApplicationRecord();
		postRecord.setApplicationName(req.getParameter("application_name"));
		postRecord.setApplicationUid(req.getParameter("application_uid"));
		postRecord.setApplicationSecret(req.getParameter("application_secret"));
		postRecord.setOrgId(CommonUtils.toInteger(req.getParameter("org_id")));

		Connection conn = null;
		try {
			conn = ClsConfig.getDbConnection();
		
			OauthApplicationRecord checkRecord;
			if ("new".equals(sMode)) {
				checkRecord = OauthApplicationRecord.getRecord(null, postRecord.getApplicationName(), postRecord.getOrgId(), conn);
				if (checkRecord != null) {
					errorMessage = "Application name is used by the same organization.";
				} else {
					postRecord.insert(conn, oUserSession.getUserId());
				}
			} else {
				Integer orgId = oUserSession.isSuperUser() ? null : oUserSession.getOrgId(); // CJ-680
				checkRecord = OauthApplicationRecord.getRecord(CommonUtils.toInteger(sId), null, orgId, conn);
				if (checkRecord == null) {
					errorMessage = "Unable to locate the requested record.";
				} else {
					if (!checkRecord.getApplicationName().equals(postRecord.getApplicationName())) {
						OauthApplicationRecord checkNameRecord = OauthApplicationRecord.getRecord(null, postRecord.getApplicationName(), postRecord.getOrgId(), conn);
						if (checkNameRecord != null)
							errorMessage = "Application name is used by the same organization.";
					}
				}
				if (errorMessage == null) {
					checkRecord.update(postRecord, conn, oUserSession.getUserId());
				}
			}
		}
		catch (Exception oError) {
			oError.printStackTrace();
			errorMessage = "Unable to process due to unexpected error: " + oError.getMessage();
		}
		finally {
			SqlUtil.closeInstance(conn);
		}
		String response;
		if (errorMessage == null) {
			oUserSession.setSessionMessage("Application data is saved.");
			response = JsonAble.jsonResult(JsonAble.STATUS_SUCCESS, null, "Saved");
		} else {
			response = JsonAble.jsonResult(JsonAble.STATUS_ERROR, null, errorMessage);
		}
		return response;
	}

	// ----------------------------------------------------------
	private static void processList(HttpServletRequest req, HttpServletResponse resp, UserSession oUserSession) {
		Pagination pagination = new Pagination(req);
		pagination.addDataSource(OauthApplicationRecord.TABLE_OAUTH_APPLICATIONS);
		
		Connection conn = null;
		PreparedStatement ps1 = null;
		ResultSet rs1 = null;
		try {
			if (oUserSession.isSuperUser() || oUserSession.isSubsuperUser()) {
				String sSql = OauthApplicationRecord.SQL_SELECT;
				if (!oUserSession.isSuperUser())
					sSql += " WHERE A." + OauthApplicationRecord.FIELD_ORG_ID + " = " + oUserSession.getOrgId(); 
				sSql += OauthApplicationRecord.SQL_ORDER_BY;
				conn = ClsConfig.getDbConnection();
				ps1 = conn.prepareStatement(sSql);
				rs1 = ps1.executeQuery();
				OauthApplicationRecord oRecord = new OauthApplicationRecord();
				while (rs1.next()) {
					oRecord.read(rs1);
					ObjectNode json = oRecord.toJson();
					pagination.getArrayNode().add(json);
					pagination.incCount();
				}
			}
			pagination.setTotal_count(pagination.getCount());
			pagination.send(resp);
		}
		catch (Exception oError) {
			oError.printStackTrace();
		}
		finally {
			SqlUtil.closeInstance(rs1, ps1, conn);
		}
	}
	
	private static String deleteRecord(UserSession oUserSession, HttpServletRequest req, HttpServletResponse resp, int applicationId) {
		//String errorMessage = null;
		OauthApplicationRecord oRecord = null;
		Connection conn;
		try {
			Integer orgId = oUserSession.isSuperUser() ? null : oUserSession.getOrgId(); // CJ-680
			conn = ClsConfig.getDbConnection();
			oRecord = OauthApplicationRecord.getRecord(applicationId, null, orgId, conn);
			if (oRecord == null)
				return "Thre requested record is not found.";
			
			int iSessions = OauthAccessTable.countActiveSession(conn, applicationId);
			if (iSessions > 0)
				return "There are active access sessions.";
			
			iSessions = OauthInterviewTable.countActiveSession(conn, applicationId);
			if (iSessions > 0)
				return "There are active interview sessions.";
			
			int iDocuments = DocumentTable.countOauthRecord(conn, applicationId);
			if (iDocuments > 0)
				return "There are " + iDocuments + " documents for the application.";
			
			AuditEventTable.deleteApplication(applicationId, conn);
			OauthInterviewTable.deleteApplication(conn, applicationId);
			OauthAccessTable.deleteApplication(conn, applicationId);
			oRecord.delete(conn);
			
			return null;
		} catch (Exception oError) {
			return oError.getMessage();
		}
	}
	
	public static String getOrgName(Connection conn, int applicationId) {
		String result = null;
		boolean bCreateConnection = (conn == null);
		PreparedStatement ps1 = null;
		ResultSet rs1 = null;
		try {
			String sSql = "SELECT B." + OrgRecord.FIELD_ORG_NAME
				+ " FROM " + OauthApplicationRecord.TABLE_OAUTH_APPLICATIONS + " A"
				+ " INNER JOIN " + OrgRecord.TABLE_ORGS + " B"
				+ " ON (B." + OrgRecord.FIELD_ORG_ID + " = A." + OauthApplicationRecord.FIELD_ORG_ID + ")"
				+ " WHERE A." + OauthApplicationRecord.FIELD_APPLICATION_ID + " = ?";
			if (bCreateConnection)
				conn = ClsConfig.getDbConnection();
			ps1 = conn.prepareStatement(sSql);
			ps1.setInt(1, applicationId);
			rs1 = ps1.executeQuery();
			if (rs1.next()) {
				result = rs1.getString(1);
			}
		}
		catch (Exception oError) {
			oError.printStackTrace();
		}
		finally {
			SqlUtil.closeInstance(rs1, ps1);
			if (bCreateConnection)
				SqlUtil.closeInstance(conn);
		}
		return result;
	}
	
}
