package gov.dod.cls.db;

import gov.dod.cls.model.JsonAble;
import gov.dod.cls.utils.CommonUtils;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.node.ObjectNode;

public class DocumentFillInsRecord extends JsonAble {

	public static final String TABLE_DOCUMENT_FILL_INS = "Document_Fill_Ins";
	
	public static final String FIELD_DOCUMENT_FILL_IN_ID = "document_fill_in_id"; 
	public static final String FIELD_DOCUMENT_ID = "document_id"; 
	public static final String FIELD_CLAUSE_FILL_IN_ID = "clause_fill_in_id"; 
	public static final String FIELD_DOCUMENT_FILL_IN_ANSWER = "document_fill_in_answer"; 
	public static final String FIELD_FULL_TEXT_MODIFIED = "full_text_modified";
	//public static final String FIELD_IS_FULL_TEXT = "is_full_text";
	public static final String FIELD_CREATED_AT = "created_at"; 
	public static final String FIELD_UPDATED_AT = "updated_at"; 
	//public static final String FIELD_NAME = "name"; 
	//public static final String FIELD_IS_FULL_TEXT = "is_full_text";	
	
	public static final String SQL_SELECT
		= "SELECT A." + FIELD_DOCUMENT_FILL_IN_ID
		+ ", A." + FIELD_DOCUMENT_ID
		+ ", A." + FIELD_CLAUSE_FILL_IN_ID
		+ ", A." + FIELD_DOCUMENT_FILL_IN_ANSWER
		+ ", A." + FIELD_FULL_TEXT_MODIFIED // FIELD_IS_FULL_TEXT
		+ ", A." + FIELD_CREATED_AT
		+ ", A." + FIELD_UPDATED_AT
		+ ", B." + ClauseFillInRecord.FIELD_CLAUSE_ID
		+ ", B." + ClauseFillInRecord.FIELD_FILL_IN_CODE
		+ " FROM " + TABLE_DOCUMENT_FILL_INS + " A"
		+ " INNER JOIN " + ClauseFillInRecord.TABLE_CLAUSE_FILL_INS + " B"
		+ " ON (B." + ClauseFillInRecord.FIELD_CLAUSE_FILL_IN_ID + " = A." + FIELD_CLAUSE_FILL_IN_ID + ")";

	// ----------------------------------------------------------------
	private Integer id;
	private Integer documentId;
	private Integer clauseFillInId; 
	private String answer;
	private Boolean fullTtextModified;
	private Date createdAt;
	private Date updatedAt;
	private Integer clauseId;
	
	private String fillInCd;
	
	public void read(ResultSet rs) throws SQLException {
		this.id = rs.getInt(FIELD_DOCUMENT_FILL_IN_ID); 
		this.documentId = CommonUtils.toInteger(rs.getObject(FIELD_DOCUMENT_ID));
		this.clauseFillInId = CommonUtils.toInteger(rs.getObject(FIELD_CLAUSE_FILL_IN_ID));
		//this.name = rs.getString(FIELD_NAME);
		this.answer = rs.getString(FIELD_DOCUMENT_FILL_IN_ANSWER);
		this.fullTtextModified = rs.getBoolean(FIELD_FULL_TEXT_MODIFIED); // FIELD_IS_FULL_TEXT
		this.createdAt = rs.getTimestamp(FIELD_CREATED_AT);
		this.updatedAt = rs.getTimestamp(FIELD_UPDATED_AT); 
		this.clauseId = CommonUtils.toInteger(rs.getObject(ClauseFillInRecord.FIELD_CLAUSE_ID));
		this.fillInCd = rs.getString(ClauseFillInRecord.FIELD_FILL_IN_CODE);
	}

	@Override
	protected void populateJson(ObjectNode json, boolean forInterview) {
		json.put(FIELD_DOCUMENT_FILL_IN_ID, this.id);
		json.put(FIELD_CLAUSE_FILL_IN_ID, this.clauseFillInId);
		json.put(FIELD_DOCUMENT_FILL_IN_ANSWER, this.answer);
		json.put(FIELD_FULL_TEXT_MODIFIED, this.fullTtextModified);
		if (!forInterview) {
			json.put(FIELD_DOCUMENT_ID, this.documentId);
			json.put(ClauseFillInRecord.FIELD_FILL_IN_CODE, this.fillInCd);
			json.put(FIELD_CREATED_AT, CommonUtils.toJsonDate(this.createdAt));
			json.put(FIELD_UPDATED_AT, CommonUtils.toJsonDate(this.updatedAt));
		}
	}
	
	public void populateJsonForApi(ObjectNode json) {
		json.put(FIELD_DOCUMENT_FILL_IN_ANSWER, this.answer);
		json.put(FIELD_FULL_TEXT_MODIFIED, this.fullTtextModified);
	}

	public void fromJson(JsonNode pJsonNode) {
		this.id = CommonUtils.asInteger(pJsonNode, FIELD_DOCUMENT_FILL_IN_ID);
		this.clauseFillInId = CommonUtils.asInteger(pJsonNode, FIELD_CLAUSE_FILL_IN_ID);
		this.answer = CommonUtils.asString(pJsonNode, FIELD_DOCUMENT_FILL_IN_ANSWER); 
		this.fullTtextModified = CommonUtils.asBoolean(pJsonNode, FIELD_FULL_TEXT_MODIFIED); 
	}

	// ----------------------------------------------------------------
	public Integer getId() { return id; }

	public Integer getDocumentId() { return documentId; }
	public void setDocumentId(Integer documentId) { this.documentId = documentId; }

	public Integer getClauseFillInId() { return clauseFillInId; }
	public void setClauseFillInId(Integer clauseFillInId) { this.clauseFillInId = clauseFillInId; }

	public String getAnswer() { return answer; }
	public void setAnswer(String answer) { this.answer = answer; }

	public Date getCreatedAt() { return createdAt; }
	public void setCreatedAt(Date createdAt) { this.createdAt = createdAt; }

	public Date getUpdatedAt() { return updatedAt; }
	public void setUpdatedAt(Date updatedAt) { this.updatedAt = updatedAt; }

	public Boolean getFullTtextModified() { return fullTtextModified; }
	public void setFullTtextModified(Boolean fullTtextModified) { this.fullTtextModified = fullTtextModified; }

	public Integer getClauseId() { return clauseId; }

	public String getFillInCd() { return fillInCd; }
	
}
