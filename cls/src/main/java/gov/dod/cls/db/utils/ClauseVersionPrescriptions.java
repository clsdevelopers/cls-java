package gov.dod.cls.db.utils;

import gov.dod.cls.config.ClsConfig;
import gov.dod.cls.db.ClausePrescriptionRecord;
import gov.dod.cls.db.ClauseRecord;
import gov.dod.cls.db.PrescriptionTable;
import gov.dod.cls.db.SysLastTableChangeAtTable;
import gov.dod.cls.utils.sql.SqlUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

public class ClauseVersionPrescriptions extends HashMap<Integer, ArrayList<Integer>> {

	private static final long serialVersionUID = 4186675504069781690L;

	// ---------------------------------------------------------------------------
	private volatile static ClauseVersionPrescriptions instance = null;
	private volatile static Date refreshed = null;
	
	public synchronized static ClauseVersionPrescriptions getInstance(Connection conn) {
		if (ClauseVersionPrescriptions.instance == null) {
			ClauseVersionPrescriptions.instance = new ClauseVersionPrescriptions();
			ClauseVersionPrescriptions.instance.refresh(conn);
		} else {
			ClauseVersionPrescriptions.instance.refreshWhenNeeded(conn);
		}
		return ClauseVersionPrescriptions.instance;
	}
	
	public static ArrayList<Integer> getSubsetByClauseVersion(int clauseVersionId, Connection conn) {
		ClauseVersionPrescriptions clauseVersionPrescriptions = ClauseVersionPrescriptions.getInstance(conn);
		if (clauseVersionPrescriptions.containsKey(clauseVersionId))
			return clauseVersionPrescriptions.get(clauseVersionId);
		return new ArrayList<Integer>();
	}
	
	// Functions -----------------------------------------------------------------
	public static Date getReferHistoryDate(Connection conn) {
		return SysLastTableChangeAtTable.getLastUpdateDate(conn, ClausePrescriptionRecord.TABLE_CLAUSE_PRESCRIPTIONS);
	}
	  
	public static boolean needToRefreh(Connection conn) {
		return SysLastTableChangeAtTable.needToLoad(ClauseVersionPrescriptions.refreshed, getReferHistoryDate(conn));
	}
	
	public void refreshWhenNeeded(Connection conn) {
		if (PrescriptionTable.needToRefreh(conn))
			this.refresh(conn);
	}

	// ======================================================
	private Exception lastError = null;

	private boolean refresh(Connection conn) {
		boolean result = false;
		String sSql = "SELECT A." + ClauseRecord.FIELD_CLAUSE_VERSION_ID
				+ ", B." + ClausePrescriptionRecord.FIELD_PRESCRIPTION_ID
				+ " FROM " + ClauseRecord.TABLE_CLAUSES + " A"
				+ " INNER JOIN " + ClausePrescriptionRecord.TABLE_CLAUSE_PRESCRIPTIONS + " B"
				+ " ON (B.Clause_Id = A.Clause_Id)"
				+ " GROUP BY A." + ClauseRecord.FIELD_CLAUSE_VERSION_ID
				+ ", B." + ClausePrescriptionRecord.FIELD_PRESCRIPTION_ID
				+ " ORDER BY 1";
		boolean bCreateConnection = (conn == null); 
		PreparedStatement ps1 = null;
		ResultSet rs1 = null;
		try {
			if (bCreateConnection)
				conn = ClsConfig.getDbConnection();
			ps1 = conn.prepareStatement(sSql);
			rs1 = ps1.executeQuery();
			this.clear();
			Integer prevClauseVesionId = null;
			ArrayList<Integer> prescriptions = null;
			while (rs1.next()) {
				int clauseVesionId = rs1.getInt(ClauseRecord.FIELD_CLAUSE_VERSION_ID);
				if ((prevClauseVesionId == null) || (prevClauseVesionId.intValue() != clauseVesionId)) {
					if (prevClauseVesionId != null)
						this.put(prevClauseVesionId, prescriptions);
					prevClauseVesionId = clauseVesionId;
					prescriptions = new ArrayList<Integer>();
				}
				prescriptions.add(rs1.getInt(ClausePrescriptionRecord.FIELD_PRESCRIPTION_ID));
			}
			if (prevClauseVesionId != null)
				this.put(prevClauseVesionId, prescriptions);
			ClauseVersionPrescriptions.refreshed = getReferHistoryDate(conn); // CommonUtils.getNow();
			result = true;
			System.out.println("ClauseVersionPrescriptions refreshed");
		}
		catch (Exception oError) {
			this.lastError = oError;
			oError.printStackTrace();
		}
		finally {
			SqlUtil.closeInstance(rs1, ps1);
			if (bCreateConnection)
				SqlUtil.closeInstance(conn);
		}
		return result;
	}

	public Date getRefreshed() {
		return refreshed;
	}

	public Exception getLastError() {
		return lastError;
	}
	
}
