package gov.dod.cls.db;

import gov.dod.cls.config.ClsConfig;
import gov.dod.cls.db.utils.OrgRegulations;
import gov.dod.cls.model.JsonAble;
import gov.dod.cls.utils.CommonUtils;
import gov.dod.cls.utils.sql.SqlInsert;
import gov.dod.cls.utils.sql.SqlUpdate;
import gov.dod.cls.utils.sql.SqlUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.node.ObjectNode;

public class OrgRecord extends JsonAble {

	public static final String TABLE_ORGS = "Orgs";

	public static final String FIELD_ORG_ID = "Org_Id";
	public static final String FIELD_ORG_NAME = "Org_Name";
	//public static final String FIELD_ABBREVIATION = "abbreviation";
	public static final String FIELD_DEPT_ID = "Dept_Id";
	public static final String FIELD_ORG_DESCRIPTION = "Org_Description";
	public static final String FIELD_ORG_SOURCE_ID = "Org_Source_Id";
	public static final String FIELD_ORG_SOURCE_PARENT_ID = "Org_Source_Parent_Id";
	public static final String FIELD_ORG_SOURCE_LEVEL = "Org_Source_Level";
	public static final String FIELD_CREATED_AT = "created_at";
	public static final String FIELD_UPDATED_AT = "updated_at";
	
	public final static String SQL_SELECT
	= "SELECT A." + FIELD_ORG_ID
	+ ", A." + FIELD_ORG_NAME
	+ ", A." + FIELD_DEPT_ID
	+ ", A." + FIELD_ORG_DESCRIPTION
	+ ", A." + FIELD_ORG_SOURCE_ID
	+ ", A." + FIELD_ORG_SOURCE_PARENT_ID
	+ ", A." + FIELD_ORG_SOURCE_LEVEL
	+ ", A." + FIELD_CREATED_AT
	+ ", A." + FIELD_UPDATED_AT
	+ ", B." + DeptRecord.FIELD_DEPT_NAME + " AS DeptName"
	+ " FROM " + TABLE_ORGS + " A"
	+ " LEFT OUTER JOIN " + DeptRecord.TABLE_DEPTS + " B"
	+ " ON (B." + DeptRecord.FIELD_DEPT_ID + "=A." + FIELD_DEPT_ID + ")";
	
	public static OrgRecord getRecord(Connection conn, Integer id, String orgName, OrgRecord pOrgRecord) {		
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			String sSql = SQL_SELECT;
			String operator = " WHERE ";
			if (id != null) {
				sSql += operator + "A." + FIELD_ORG_ID + "=?";
				operator = " AND ";
			}
			if (orgName != null) {
				sSql += operator + "LOWER(A." + FIELD_ORG_NAME + ")=?";
				orgName = orgName.toLowerCase();
			}
			ps = conn.prepareStatement(sSql);
			int iParam = 1;
			if (id != null)
				ps.setInt(iParam++, id);
			if (orgName != null)
				ps.setString(iParam++, orgName);
			rs = ps.executeQuery();
			if (rs.next()) {
				if (pOrgRecord == null)
					pOrgRecord = new OrgRecord();
				pOrgRecord.read(rs);
				pOrgRecord.regulations.load(pOrgRecord.id, conn);
			}
		}
		catch (Exception oError) {
			oError.printStackTrace();
		}
		finally {
			SqlUtil.closeInstance(rs, ps);
		}
		return pOrgRecord;
	}

	public static OrgRecord getRecord(Integer id, String orgName) {
		if ((id == null) && CommonUtils.isEmpty(orgName))
			return null;
		
		Connection conn = null;
		try {
			conn = ClsConfig.getDbConnection();
			return OrgRecord.getRecord(conn, id, orgName, null);
		}
		catch (Exception oError) {
			oError.printStackTrace();
		}
		finally {
			SqlUtil.closeInstance(conn);
		}
		return null;
	}
	

	// -------------------------------------------------------
	private Integer id;
	private String name;
	private String abbreviation;
	private Integer deptId;
	private String deptname;
	private String description;
	private Integer sourceId;
	private Integer sourceParentId;
	private Integer sourceLevel;
	private OrgRegulations regulations = new OrgRegulations();
	private Date createdAt;
	private Date updatedAt;
	
	private Date lastRefreshed = null;
	private Exception lastError = null;
	
	public void read(ResultSet rs) throws SQLException {
		this.id = rs.getInt(FIELD_ORG_ID);
		this.name = rs.getString(FIELD_ORG_NAME);
		this.deptId = rs.getInt(FIELD_DEPT_ID);
		this.deptname = rs.getString("DeptName");
		this.description = rs.getString(FIELD_ORG_DESCRIPTION);
		this.createdAt = rs.getTimestamp(FIELD_CREATED_AT);
		this.updatedAt = rs.getTimestamp(FIELD_UPDATED_AT);		
	}
	
	@Override
	protected void populateJson(ObjectNode json, ObjectMapper mapper) {
		json.put(FIELD_ORG_ID, this.id);
		json.put(FIELD_ORG_NAME, this.name);
		json.put(FIELD_DEPT_ID, this.deptId);
		json.put("deptName", this.deptname);
		json.put(FIELD_ORG_DESCRIPTION, this.description);
		json.put(FIELD_CREATED_AT, JsonAble.getJsonValue(this.createdAt));
		json.put(FIELD_UPDATED_AT, JsonAble.getJsonValue(this.updatedAt));
		
		json.put("isREGUL_FAR", this.regulations.isREGUL_FAR());
		json.put("isREGUL_DFARS", this.regulations.isREGUL_DFARS());
		json.put("isREGUL_AFFARS", this.regulations.isREGUL_AFFARS());
		json.put("isREGUL_AFARS", this.regulations.isREGUL_AFARS());
		
		json.put("isREGUL_NMCARS", this.regulations.isREGUL_NMCARS());
		json.put("isREGUL_USSOCOM", this.regulations.isREGUL_USSOCOM());
		json.put("isREGUL_USTRANSCOM", this.regulations.isREGUL_USTRANSCOM());
		json.put("isREGUL_DARS", this.regulations.isREGUL_DARS());
						
		this.regulations.addJson(json);
	}
	
	
	public boolean adminUpdate(Connection conn, String orgname, String orgdesc, 
			Integer newDeptId, ArrayList<Integer> newRegulIds, UserRecord adminUserRecord)
			throws Exception {
		this.lastError = null;
		boolean result = false;
		
		AuditEventRecord auditEventRecord = new AuditEventRecord(AuditEventRecord.ACTION_ORGANIZATION_UPDATED, adminUserRecord.getId(), adminUserRecord.getId());
		
		SqlUpdate sqlUpdate = new SqlUpdate(null, TABLE_ORGS);
		if (!CommonUtils.isSame(this.name, orgname))
			sqlUpdate.addString(FIELD_ORG_NAME, orgname, auditEventRecord);
		if (!CommonUtils.isSame(this.description, orgdesc))
			sqlUpdate.addString(FIELD_ORG_DESCRIPTION, orgdesc, auditEventRecord);		
		if (newDeptId != null) {
			if (!newDeptId.equals(deptId))
				sqlUpdate.addSet(FIELD_DEPT_ID, newDeptId, java.sql.Types.INTEGER, auditEventRecord);
		} else {
			// setNull
			sqlUpdate.addSet(FIELD_DEPT_ID, null, java.sql.Types.INTEGER, auditEventRecord);
		}
		try {
			if (sqlUpdate.size() > 0) {
				sqlUpdate.addCriteria(FIELD_ORG_ID, this.id, java.sql.Types.INTEGER);
				sqlUpdate.execute(conn);
				result = true;
			}
			result |= OrgRegulationTable.adminUpdate(conn, this.id, newRegulIds, auditEventRecord);
			if (result){
				auditEventRecord.setOrgId(this.id);  // 4/5/2015 IC
				auditEventRecord.saveNew(conn);
			}
		}
		catch (Exception oError) {
			oError.printStackTrace();
			this.lastError = oError;
			result = false;
			throw oError;
		}
		return result;
	}
	
	public void adminCreate(Connection conn, String orgname, String orgdesc, 
			Integer newDeptId, ArrayList<Integer> newRegulIds, UserRecord adminUserRecord) 
			throws Exception {
		this.lastError = null;
		
		AuditEventRecord auditEventRecord = new AuditEventRecord(AuditEventRecord.ACTION_ORGANIZATION_CREATED, adminUserRecord.getId(), adminUserRecord.getId());
		
		SqlInsert sqlInsert = new SqlInsert(null, TABLE_ORGS);
		sqlInsert.addField(FIELD_ORG_NAME, orgname, auditEventRecord);
		sqlInsert.addField(FIELD_ORG_DESCRIPTION, orgdesc, auditEventRecord);
		if (newDeptId != null) {
			sqlInsert.addField(FIELD_DEPT_ID, newDeptId, auditEventRecord);
		}
		sqlInsert.addField(FIELD_ORG_SOURCE_ID, 0, auditEventRecord);
		sqlInsert.addField(FIELD_ORG_SOURCE_PARENT_ID, 0, auditEventRecord);
		sqlInsert.addField(FIELD_ORG_SOURCE_LEVEL, 0, auditEventRecord);
		Date dNow = CommonUtils.getNow();
		sqlInsert.addField(FIELD_CREATED_AT, dNow, auditEventRecord);
		sqlInsert.addField(FIELD_UPDATED_AT, dNow, auditEventRecord);
		
		try {
			sqlInsert.execute(conn);
			OrgRecord.getRecord(conn, null, orgname, this);
			auditEventRecord.setOrgId(this.id);
			OrgRegulationTable.adminUpdate(conn, this.id, newRegulIds, auditEventRecord);
			auditEventRecord.saveNew(conn);
		}
		catch (Exception oError) {
			oError.printStackTrace();
			this.lastError = oError;
			throw oError;
		}
	}

	//--------------------------------------------------------------
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAbbreviation() {
		return abbreviation;
	}
	public void setAbbreviation(String abbreviation) {
		this.abbreviation = abbreviation;
	}
	public Integer getDeptId() {
		return deptId;
	}
	public void setDeptId(Integer deptId) {
		this.deptId = deptId;
	}
	
	public String getDeptName() {
		return deptname;
	}
	public void setDeptName(String deptname) {
		this.deptname = deptname;
	}
	
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Integer getSourceId() {
		return sourceId;
	}
	public void setSourceId(Integer sourceId) {
		this.sourceId = sourceId;
	}
	public Integer getSourceParentId() {
		return sourceParentId;
	}
	public void setSourceParentId(Integer sourceParentId) {
		this.sourceParentId = sourceParentId;
	}
	public Integer getSourceLevel() {
		return sourceLevel;
	}
	public void setSourceLevel(Integer sourceLevel) {
		this.sourceLevel = sourceLevel;
	}
	public Date getCreatedAt() {
		return createdAt;
	}
	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}
	public Date getUpdatedAt() {
		return updatedAt;
	}
	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	public Date getLastRefreshed() {
		return lastRefreshed;
	}

	public Exception getLastError() {
		return lastError;
	} 
}
