package gov.dod.cls.db;

import gov.dod.cls.config.ClsConfig;
import gov.dod.cls.utils.sql.SqlUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.node.ArrayNode;

public class QuestionChoicePrescriptionTable extends ArrayList<QuestionChoicePrescriptionRecord> {

	private static final long serialVersionUID = -6681696779781456387L;

	private volatile static QuestionChoicePrescriptionTable instance = null;
	private volatile static Date refreshed = null;
	
	public synchronized static QuestionChoicePrescriptionTable getInstance(Connection conn) {
		if (QuestionChoicePrescriptionTable.instance == null) {
			QuestionChoicePrescriptionTable.instance = new QuestionChoicePrescriptionTable();
			QuestionChoicePrescriptionTable.instance.refresh(conn);
		} else {
			QuestionChoicePrescriptionTable.instance.refreshWhenNeeded(conn);
		}
		return QuestionChoicePrescriptionTable.instance;
	}
	
	// Functions -----------------------------------------------------------------
	public static Date getReferHistoryDate(Connection conn) {
		return SysLastTableChangeAtTable.getLastUpdateDate(conn, QuestionChoicePrescriptionRecord.TABLE_QUESTION_CHOICE_PRESCRIPTIONS);
	}
	  
	public static boolean needToRefreh(Connection conn) {
		return SysLastTableChangeAtTable.needToLoad(QuestionChoicePrescriptionTable.refreshed, getReferHistoryDate(conn));
	}
	
	// ---------------------------------------------------------------
	private Exception lastError;

	public void refreshWhenNeeded(Connection conn) {
		if (ClausePrescriptionTable.needToRefreh(conn))
			this.refresh(conn);
	}

	public ArrayNode toJson(ObjectMapper mapper, boolean forInterview) {
		if (mapper == null)
			mapper = new ObjectMapper();
		ArrayNode json = mapper.createArrayNode();
		for (QuestionChoicePrescriptionRecord record : this) {
			json.add(record.toJson(forInterview));
		}
		return json;
	}
	
	private boolean refresh(Connection conn) {
		boolean result = false;
		boolean bCreateConnection = (conn == null);
		PreparedStatement ps1 = null;
		ResultSet rs1 = null;
		try {
			if (bCreateConnection)
				conn = ClsConfig.getDbConnection();
			String sSql = QuestionChoicePrescriptionRecord.SQL_SELECT
					+ " ORDER BY " + QuestionChoicePrescriptionRecord.FIELD_QUESTION_CHOCE_ID;
			ps1 = conn.prepareStatement(sSql);
			rs1 = ps1.executeQuery();
			this.clear();
			while (rs1.next()) {
				QuestionChoicePrescriptionRecord record = new QuestionChoicePrescriptionRecord();
				record.read(rs1);
				this.add(record);
			}
			QuestionChoicePrescriptionTable.refreshed = getReferHistoryDate(conn); // CommonUtils.getNow();
			result = true;
			System.out.println("QuestionChoicePrescriptionTable refreshed");
		}
		catch (Exception oError) {
			this.lastError = oError;
			oError.printStackTrace();
		}
		finally {
			SqlUtil.closeInstance(rs1, ps1);
			if (bCreateConnection)
				SqlUtil.closeInstance(conn);
		}
		return result;
	}
	
	public QuestionChoicePrescriptionTable subset(int questionChoiceId) {
		QuestionChoicePrescriptionTable result = new QuestionChoicePrescriptionTable();
		boolean found = false;
		for (QuestionChoicePrescriptionRecord record : this) {
			if (record.getQuestionChoceId().intValue() == questionChoiceId) {
				result.add(record);
				found = true;
			} else if (found)
				break;
		}
		return result;
	}

	public Exception getLastError() {
		return lastError;
	}

}
