package gov.dod.cls.db;

import gov.dod.cls.model.JsonAble;
import gov.dod.cls.utils.CommonUtils;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import org.codehaus.jackson.node.ObjectNode;

public class QuestionChoicePrescriptionRecord extends JsonAble {

	public static final String TABLE_QUESTION_CHOICE_PRESCRIPTIONS = "Question_Choice_Prescriptions";
	
	public static final String FIELD_QUESTION_CHOICE_PRESCRIPTION_ID = "question_choice_prescription_id";
	public static final String FIELD_QUESTION_CHOCE_ID = "Question_Choce_Id";
	public static final String FIELD_PRESCRIPTION_ID = "prescription_id";
	public static final String FIELD_CREATED_AT = "created_at";
	public static final String FIELD_UPDATED_AT = "updated_at";

	public static final String SQL_SELECT
		= "SELECT " + FIELD_QUESTION_CHOICE_PRESCRIPTION_ID
		+ ", " + FIELD_QUESTION_CHOCE_ID
		+ ", " + FIELD_PRESCRIPTION_ID
		+ ", " + FIELD_CREATED_AT
		+ ", " + FIELD_UPDATED_AT
		+ " FROM " + TABLE_QUESTION_CHOICE_PRESCRIPTIONS;
	
	// ======================================================
	private Integer id;
	private Integer questionChoceId;
	private Integer prescriptionId;
	private Date createdAt;
	private Date updatedAt;
	
	public void read(ResultSet rs) throws SQLException {
		this.id = rs.getInt(FIELD_QUESTION_CHOICE_PRESCRIPTION_ID); 
		this.questionChoceId = CommonUtils.toInteger(rs.getObject(FIELD_QUESTION_CHOCE_ID));
		this.prescriptionId = CommonUtils.toInteger(rs.getObject(FIELD_PRESCRIPTION_ID));
		this.createdAt = CommonUtils.toDate(rs.getObject(FIELD_CREATED_AT));
		this.updatedAt = CommonUtils.toDate(rs.getObject(FIELD_UPDATED_AT));
	}

	@Override
	protected void populateJson(ObjectNode json, boolean forInterview) {
		json.put(FIELD_PRESCRIPTION_ID, this.prescriptionId);
		if (!forInterview) {
			json.put(FIELD_QUESTION_CHOICE_PRESCRIPTION_ID, this.id);
			json.put(FIELD_QUESTION_CHOCE_ID, this.questionChoceId);
			json.put(FIELD_CREATED_AT, CommonUtils.toJsonDate(this.createdAt));
			json.put(FIELD_UPDATED_AT, CommonUtils.toJsonDate(this.updatedAt));
		}
	}

	// ----------------------------------------------------------
	public Integer getQuestionChoceId() {
		return questionChoceId;
	}
	public void setQuestionChoceId(Integer questionChoceId) {
		this.questionChoceId = questionChoceId;
	}

	public Integer getPrescriptionId() {
		return prescriptionId;
	}
	public void setPrescriptionId(Integer prescriptionId) {
		this.prescriptionId = prescriptionId;
	}

	public Date getCreatedAt() {
		return createdAt;
	}
	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public Date getUpdatedAt() {
		return updatedAt;
	}
	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	public Integer getId() {
		return id;
	}

}
