package gov.dod.cls.db;

import gov.dod.cls.utils.CommonUtils;
import gov.dod.cls.utils.sql.SqlInsert;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Date;

public class AuditEventRecord {
	
	public static final String ACTION_CLAUSE_CREATED = "clause_created";
	public static final String ACTION_CLAUSE_UPDATE_SERVICE_COMPLETED = "clause_update_service_completed";
	public static final String ACTION_CLAUSE_UPDATE_SERVICE_STARTED = "clause_update_service_started";
	public static final String ACTION_CLAUSE_UPDATED = "clause_updated";
	public static final String ACTION_CLAUSE_VIEWED = "clause_viewed";

	public static final String ACTION_DOCUMENT_CREATED = "document_created";
	public static final String ACTION_DOCUMENT_DESTROYED = "document_destroyed";
	public static final String ACTION_DOCUMENT_UPDATED = "document_updated";
	public static final String ACTION_DOCUMENT_VERSION_UPDATED = "document_version_updated";
	public static final String ACTION_DOCUMENT_VIEWED = "document_viewed";
	public static final String ACTION_DOCUMENT_REOPEN = "document_reopen";
	public static final String ACTION_DOCUMENT_RESUME = "document_resume";
	public static final String ACTION_DOCUMENT_FINALIZED = "document_finalized";
	
	public static final String ACTION_INTERVIEW_COMPLETED = "interview_completed";
	public static final String ACTION_INTERVIEW_RESUMED = "interview_resumed";
	
	public static final String ACTION_LOGGED_IN_WITH_CAC_CARD = "logged_in_with_cac_card";
	public static final String ACTION_LOGGED_IN_WITH_EMAIL = "logged_in_with_email";

	public static final String ACTION_ORGANIZATION_AUTHENTICATED = "organization_authenticated";	// new with CJ-1410
	public static final String ACTION_ORGANIZATION_CREATED = "organization_interview_created";      // 4/3/2015 IC Created
	public static final String ACTION_ORGANIZATION_UPDATED = "organization_interview_updated";
	public static final String ACTION_ORGANIZATION_VIEWED = "organization_interview_viewed";
	public static final String ACTION_ORGANIZATION_DESTROYED = "organization_interview_destroyed"; // new with CJ-1410
	public static final String ACTION_ORGANIZATION_DOC_UPDATED = "organization_document_updated";

	public static final String ACTION_PASSWORD_CHANGED = "password_changed";

	public static final String ACTION_QUESTION_CREATED = "question_created";
	public static final String ACTION_QUESTION_UPDATED = "question_updated";
	public static final String ACTION_QUESTION_VIEWED = "question_viewed";

	public static final String ACTION_REGISTRATION_ATTEMPT = "registration_attempt";
	public static final String ACTION_REGULATION_VIEWED = "regulation_viewed";

	public static final String ACTION_USER_CREATED = "user_created";
	public static final String ACTION_USER_DESTROYED = "user_destroyed";
	public static final String ACTION_USER_UPDATED = "user_updated";
	public static final String ACTION_USER_VIEWED = "user_viewed";

	public static final String ACTION_VERSION_ACTIVATED = "version_activated";
	
	// ----------------------------------------------------------------------------------------
	public static final String TABLE_AUDIT_EVENTS = "Audit_Events";
	
	public static final String FIELD_AUDIT_EVENT_ID = "audit_event_id";
	public static final String FIELD_ACTION = "action";
	public static final String FIELD_USER_ID = "user_id";
	public static final String FIELD_INITIATED_USER_ID = "initiated_user_id";
	public static final String FIELD_REFERENCE_USER_NAME = "ref_user_name";
	public static final String FIELD_DOCUMENT_ID = "document_id";
	public static final String FIELD_REFERENCE_DOCUMENT_ID = "ref_document_id";
	public static final String FIELD_REFERENCE_DOCUMENT_NUMBER = "ref_document_number";
	public static final String FIELD_CLAUSE_ID = "clause_id";
	public static final String FIELD_QUESTION_ID = "question_id";
	public static final String FIELD_ORG_ID = "org_id";
	public static final String FIELD_APPLICATION_ID = "application_id";
	public static final String FIELD_REFERENCE_APPLICATION_ID = "ref_application_id";
	public static final String FIELD_ADDITIONAL_DATA = "additional_data";
	public static final String FIELD_CREATED_AT = "created_at";
	public static final String FIELD_UPDATED_AT = "updated_at";

	public static void log(Connection conn, String action, Integer userId, Integer initiatedUserId, 
			Integer documentId, Integer clauseId, Integer questionId, Integer orgId, 
			Integer applicationId, Integer referenceApplicationId, String additionalData) 
			throws Exception {
		AuditEventRecord auditEventRecord = new AuditEventRecord();
		auditEventRecord.action = action;
		auditEventRecord.userId = userId;
		auditEventRecord.initiatedUserId = initiatedUserId;
		auditEventRecord.documentId = documentId;
		auditEventRecord.referenceDocumentId = documentId;
		auditEventRecord.clauseId = clauseId;
		auditEventRecord.questionId = questionId;
		auditEventRecord.orgId = orgId;
		auditEventRecord.applicationId = applicationId;
		auditEventRecord.referenceApplicationId = referenceApplicationId;
		auditEventRecord.additionalData = additionalData;
		auditEventRecord.saveNew(conn);
	}
	
	// ===================================================================
	private Integer id;
	private String action;
	private Integer userId;
	private Integer initiatedUserId;
	private String referenceUserName;
	private Integer documentId;
	private Integer referenceDocumentId;
	private String referenceDocumentNumber;
	private Integer clauseId;
	private Integer questionId;
	private Integer orgId;
	private Integer applicationId;
	private Integer referenceApplicationId;
	private String additionalData = null;
	private Date createdAt;
	private Date updatedAt;
	
	public AuditEventRecord() {
		super();
	}
	
	public AuditEventRecord(String action, Integer userId, Integer initiatedUserId) {
		super();
		this.action = action;
		this.userId = userId;
		this.initiatedUserId = initiatedUserId;
	}
	
	public void addAttribute(String attribute, Object value) {
		
		if (this.additionalData == null)
			this.additionalData = "---\r\n:attributes:";
		this.additionalData += "\r\n  " + attribute + ": " + ((value == null) ? "" : value.toString());

		if (value == null)
			return;
		
		// CJ-1410, beef up API audit.
		if (attribute.equals(FIELD_APPLICATION_ID)) {
			if (this.referenceApplicationId == null)
			{
				this.referenceApplicationId = (Integer) value;
			}
		}
		if (attribute.equals(FIELD_DOCUMENT_ID)) {
			if (this.referenceDocumentId == null)
			{
				this.referenceDocumentId = (Integer) value;
			}
		}
	}
	
	public boolean isAttributeEmpty() {
		return (this.additionalData == null);
	}
	
	// CJ-1410, beef up API audit.
	/*
	private void addOrgId (Connection conn) {
		
		if (this.orgId != null) 
			return;
		
		if ((this.initiatedUserId == null) && (this.referenceApplicationId == null))
			return;
		
		if (this.initiatedUserId != null) {
			UserRecord uRec = UserRecord.getRecord(this.initiatedUserId, null, null);
			if (uRec != null)
				this.orgId = uRec.getOrgId();
		}
		else if (this.referenceApplicationId != null) {
			this.orgId = OauthApplicationRecord.getOrgId(this.referenceApplicationId, conn);
		}
	}
	*/
	// CJ-1410, beef up API audit.
	private void addUserName (Connection conn) {
		
		if ((this.initiatedUserId == null) && (this.referenceApplicationId == null))
			return;
		
		try {
			if (this.initiatedUserId != null) {
				UserRecord uRec = UserRecord.getRecord(this.initiatedUserId, null, null);
				if (uRec != null)
					this.referenceUserName = uRec.getFirstName() + " " + uRec.getLastName();
			}
			else if (this.referenceApplicationId != null) {
				OauthApplicationRecord oRec = OauthApplicationRecord.getRecord(this.referenceApplicationId, null, null, conn);
				this.referenceUserName = oRec.getApplicationName();					
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	// CJ-1410, maintain the title separately, in case the orig document record is deleted.
	private void addRefDocumentTitle (Connection conn) {
		
		if (this.referenceDocumentNumber != null) 
			return;
		
		if (this.referenceDocumentId == null) 
			return;
		
		try {
			DocumentRecord oRec;
			oRec = DocumentTable.find(this.referenceDocumentId, conn);
			if (oRec != null)
				this.referenceDocumentNumber = oRec.getDocumentNumber();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	public void saveNew(Connection conn) throws Exception {
		
		// if initiatedUserId is null, default to userId
		if (this.initiatedUserId == null)
			this.initiatedUserId = this.userId;
		
		//addOrgId(conn);
		addUserName(conn);
		addRefDocumentTitle(conn);
		
		Date dNow = CommonUtils.getNow();
		SqlInsert sqlInsert = new SqlInsert(null, TABLE_AUDIT_EVENTS);
		sqlInsert.addField(FIELD_ACTION, this.action);
		sqlInsert.addField(FIELD_USER_ID, this.userId);
		sqlInsert.addField(FIELD_INITIATED_USER_ID, this.initiatedUserId);
		if (this.referenceUserName != null)
			sqlInsert.addField(FIELD_REFERENCE_USER_NAME, this.referenceUserName);
		if (this.documentId != null)
			sqlInsert.addField(FIELD_DOCUMENT_ID, this.documentId);
		if (this.referenceDocumentId != null)
			sqlInsert.addField(FIELD_REFERENCE_DOCUMENT_ID, this.referenceDocumentId);
		if (this.referenceDocumentNumber != null)
			sqlInsert.addField(FIELD_REFERENCE_DOCUMENT_NUMBER, this.referenceDocumentNumber);
		if (this.clauseId != null)
			sqlInsert.addField(FIELD_CLAUSE_ID, this.clauseId);
		if (this.questionId != null)
			sqlInsert.addField(FIELD_QUESTION_ID, this.questionId);
		if (this.orgId != null)
			sqlInsert.addField(FIELD_ORG_ID, this.orgId);
		if (this.applicationId != null)
			sqlInsert.addField(FIELD_APPLICATION_ID, this.applicationId);
		if (this.referenceApplicationId != null)
			sqlInsert.addField(FIELD_REFERENCE_APPLICATION_ID, this.referenceApplicationId);
		if (this.additionalData != null)
			sqlInsert.addField(FIELD_ADDITIONAL_DATA, this.additionalData);
		sqlInsert.addField(FIELD_CREATED_AT, dNow);
		sqlInsert.addField(FIELD_UPDATED_AT, dNow);
		sqlInsert.execute(conn);
	}

	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getAction() {
		return action;
	}
	public void setAction(String action) {
		this.action = action;
	}
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	public Integer getInitiatedUserId() {
		return initiatedUserId;
	}
	public void setInitiatedUserId(Integer initiatedUserId) {
		this.initiatedUserId = initiatedUserId;
	}
	public Integer getreferenceApplicationId() {
		return referenceApplicationId;
	}
	public void setreferenceApplicationId(Integer referenceApplicationId) {
		this.referenceApplicationId = referenceApplicationId;
	}
	public Integer getReferenceDocumentId() {
		return referenceDocumentId;
	}
	public void setReferenceDocumentId(Integer documentId) {
		this.referenceDocumentId = documentId;
	}
	public String getReferenceDocumentNumber() {
		return referenceDocumentNumber;
	}
	public void setReferenceDocumentNumber(String s) {
		this.referenceDocumentNumber = s;
	}
	public Integer getClauseId() {
		return clauseId;
	}
	public void setClauseId(Integer clauseId) {
		this.clauseId = clauseId;
	}
	public Integer getQuestionId() {
		return questionId;
	}
	public void setQuestionId(Integer questionId) {
		this.questionId = questionId;
	}
	public Integer getOrgId() {
		return orgId;
	}
	public void setOrgId(Integer orgId) {
		this.orgId = orgId;
	}
	public Integer getApplicationId() {
		return applicationId;
	}
	public void setApplicationId(Integer applicationId) {
		this.applicationId = applicationId;
	}
	public String getAdditionalData() {
		return additionalData;
	}
	public void setAdditionalData(String additionalData) {
		this.additionalData = additionalData;
	}
	public Date getCreatedAt() {
		return createdAt;
	}
	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}
	public Date getUpdatedAt() {
		return updatedAt;
	}
	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}
	
	
}
