package gov.dod.cls.db;

import gov.dod.cls.model.JsonAble;
import gov.dod.cls.utils.CommonUtils;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.node.ObjectNode;

public class DocumentClauseRecord extends JsonAble {

	public static final String USER_ACTION_CODE_ADDED = "A"; // Added
	public static final String USER_ACTION_CODE_REMOVED = "R"; // Remove
	
	public static final String TABLE_DOCUMENT_CLAUSES = "Document_Clauses";
	
	public static final String FIELD_DOCUMENT_ID = "document_id"; 
	public static final String FIELD_CLAUSE_ID = "clause_id"; 
	public static final String FIELD_IS_FILLIN_COMPLETED = "is_fillin_completed";
	public static final String FIELD_OPTIONAL_USER_ACTION_CODE = "optional_user_action_code";
	// public static final String FIELD_IS_REMOVED_BY_USER = "is_removed_by_user"; 
	public static final String FIELD_CREATED_AT = "created_at";
	public static final String FIELD_UPDATED_AT = "updated_at";
	
	public static final String SQL_SELECT
		= "SELECT " + FIELD_DOCUMENT_ID
		+ ", " + FIELD_CLAUSE_ID
		+ ", " + FIELD_IS_FILLIN_COMPLETED
		+ ", " + FIELD_OPTIONAL_USER_ACTION_CODE // FIELD_IS_REMOVED_BY_USER
		+ ", " + FIELD_CREATED_AT
		+ ", " + FIELD_UPDATED_AT
		+ " FROM " + TABLE_DOCUMENT_CLAUSES;
	/*
	public static final String SQL_SELECT
		= "SELECT A." + FIELD_DOCUMENT_ID
		+ ", A." + FIELD_CLAUSE_ID
		+ ", A." + FIELD_IS_FILLIN_COMPLETED
		+ ", A." + FIELD_OPTIONAL_USER_ACTION_CODE // FIELD_IS_REMOVED_BY_USER
		+ ", A." + FIELD_CREATED_AT
		+ ", A." + FIELD_UPDATED_AT
		+ " FROM " + TABLE_DOCUMENT_CLAUSES + " A";
	 */

	private Integer documentId;
	private Integer clauseId;
	private boolean isFillinCompleted;
	private String optionalUserActionCode; // private boolean isRemovedByUser;
	private Date createdAt;
	private Date updatedAt;
	private ClauseRecord clauseRecord;
	
	public void read(ResultSet rs) throws SQLException {
		this.documentId = rs.getInt(FIELD_DOCUMENT_ID); 
		this.clauseId = CommonUtils.toInteger(rs.getObject(FIELD_CLAUSE_ID));
		this.isFillinCompleted = rs.getBoolean(FIELD_IS_FILLIN_COMPLETED);
		this.optionalUserActionCode = rs.getString(FIELD_OPTIONAL_USER_ACTION_CODE);
		// this.isRemovedByUser = rs.getBoolean(FIELD_IS_REMOVED_BY_USER);
		this.createdAt = CommonUtils.toDateTime(rs, FIELD_CREATED_AT);
		this.updatedAt = CommonUtils.toDateTime(rs, FIELD_UPDATED_AT);
	}
	
	@Override
	protected void populateJson(ObjectNode json, boolean forInterview) {
		json.put(FIELD_CLAUSE_ID, this.clauseId);
		json.put(FIELD_IS_FILLIN_COMPLETED, this.isFillinCompleted);
		json.put(FIELD_OPTIONAL_USER_ACTION_CODE, this.optionalUserActionCode);
		//json.put(FIELD_IS_REMOVED_BY_USER, this.isRemovedByUser);
		if (!forInterview) {
			json.put(FIELD_DOCUMENT_ID, this.documentId);
			json.put(FIELD_CREATED_AT, CommonUtils.toJsonDate(this.createdAt));
			json.put(FIELD_UPDATED_AT, CommonUtils.toJsonDate(this.updatedAt));
		}
	}

	public void populateJsonForApi(ObjectNode json, boolean bForVer1, ClauseTable clauseTable, DocumentFillInsTable documentFillInsTable) {
		if (bForVer1 == false) {
			json.put(FIELD_CLAUSE_ID, this.clauseId);
			json.put(FIELD_OPTIONAL_USER_ACTION_CODE, this.optionalUserActionCode);
			json.put("is_removed_by_user", USER_ACTION_CODE_REMOVED.equals(this.optionalUserActionCode)); // FIELD_IS_REMOVED_BY_USER
			json.put(FIELD_CREATED_AT, CommonUtils.toZulu(this.createdAt));
			json.put(FIELD_UPDATED_AT, CommonUtils.toZulu(this.updatedAt));
		}
		ClauseRecord clauseRecord = clauseTable.findById(this.clauseId.intValue());
		if (clauseRecord != null) {
			if (bForVer1) {
				clauseRecord.populateJsonForApi(json, bForVer1, documentFillInsTable);
			} else {
				ObjectNode clauseJson = json.objectNode();
				clauseRecord.populateJsonForApi(clauseJson, bForVer1, documentFillInsTable);
				json.put(ClauseRecord.TABLE_CLAUSES.toLowerCase(), clauseJson);
			}
		}
	}

	public void fromJson(JsonNode pJsonNode) {
		this.clauseId = CommonUtils.asInteger(pJsonNode, FIELD_CLAUSE_ID);
		this.isFillinCompleted = CommonUtils.asBoolean(pJsonNode, FIELD_IS_FILLIN_COMPLETED); 
		this.optionalUserActionCode = CommonUtils.asString(pJsonNode, FIELD_OPTIONAL_USER_ACTION_CODE);
		//this.isRemovedByUser = CommonUtils.asBoolean(pJsonNode, FIELD_IS_REMOVED_BY_USER); 
	}
	
	public boolean hasDifferentAnswer(DocumentClauseRecord postRecord) {
		return ((this.isFillinCompleted != postRecord.isFillinCompleted) ||
				CommonUtils.isNotSame(this.optionalUserActionCode, postRecord.optionalUserActionCode));
				//(this.isRemovedByUser != postRecord.isRemovedByUser));
	}

	// ---------------------------------------------------------------------
	public Integer getClauseId() {
		return clauseId;
	}
	public void setClauseId(Integer clauseId) {
		this.clauseId = clauseId;
	}

	public boolean isFillinCompleted() {
		return isFillinCompleted;
	}
	public void setFillinCompleted(boolean isFillinCompleted) {
		this.isFillinCompleted = isFillinCompleted;
	}

	public String getOptionalUserActionCode() {
		return optionalUserActionCode;
	}
	public void setOptionalUserActionCode(String optionalUserActionCode) {
		this.optionalUserActionCode = optionalUserActionCode;
	}
	public boolean isRemoved() {
		return USER_ACTION_CODE_REMOVED.equals(this.optionalUserActionCode);
	}

	//public boolean getIsRemovedByUser() { return isRemovedByUser; }
	//public void setIsRemovedByUser(boolean isRemovedByUser) { this.isRemovedByUser = isRemovedByUser; }

	public Date getCreatedAt() {
		return createdAt;
	}
	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}
	public Date getUpdatedAt() {
		return updatedAt;
	}
	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	public Integer getDocumentId() {
		return documentId;
	}

	public void setDocumentId(Integer documentId) {
		this.documentId = documentId;
	}

	// CJ-576, If a Full Text, Editable Clause has not been modified then return false.
	public boolean isEditFullText(DocumentFillInsTable docFillIns) {
		
		boolean bresult = clauseRecord.isFullText();
		
		if (!bresult)
			return bresult;
		if (!clauseRecord.isEditable()) 
			return bresult;
		
		ClauseFillInTable clauseFillins = clauseRecord.getClauseFillInTable();
		
		/* CJ-1099 and  CJ-1100
		To change to By Reference:
		The clause is editable.
		The clause contains no "Fill-ins" for the user to edit.
		The user does not make changes to the editable text
		*/
		
		if (clauseFillins.size() == 0)
			return false;
		
		// The user does not make changes to the editable text
		for (ClauseFillInRecord clsFillin : clauseFillins) {
			if (clsFillin.getFillInDefaultData() == null) // isEditable?
					return true;
			
			// The document fillin for editables will be null, if they have not modified the editable
			DocumentFillInsRecord docFillin = docFillIns.findByClauseFillInId(clsFillin.getId());
			if (docFillin == null) {
					bresult = false;
			}
		}
		
		return bresult;
	}
	
	public ClauseRecord getClauseRecord() { return clauseRecord; }
	public void setClauseRecord(ClauseRecord clauseRecord) { this.clauseRecord = clauseRecord; }
	
	public String getClauseName() 
	{
		if (this.clauseRecord == null)
			return "";
		return this.clauseRecord.getClauseName().trim();
	}
}
