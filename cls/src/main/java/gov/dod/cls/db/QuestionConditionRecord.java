package gov.dod.cls.db;

import gov.dod.cls.model.JsonAble;
import gov.dod.cls.utils.CommonUtils;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.node.ObjectNode;

public class QuestionConditionRecord extends JsonAble {

	public static final String TABLE_QUESTION_CONDITIONS = "Question_Conditions";
	
	public static final String FIELD_QUESTION_CONDITION_ID = "question_condition_id";
	public static final String FIELD_QUESTION_CONDITION_SEQUENCE = "question_condition_sequence";
	public static final String FIELD_QUESTION_ID = "question_id";
	
	public static final String FIELD_PARENT_QUESTION_ID = "parent_question_id"; // new

	public static final String FIELD_ORIGINAL_CONDITION_TEXT = "original_condition_text";
	public static final String FIELD_CONDITION_TO_QUESTION = "condition_to_question";
	public static final String FIELD_DEFAULT_BASELINE      = "default_baseline"; // CJ-584

	public static final String FIELD_CREATED_AT = "created_at";
	public static final String FIELD_UPDATED_AT = "updated_at";
	
	public static final String SQL_SELECT
		= "SELECT A." + FIELD_QUESTION_CONDITION_ID
		+ ", A." + FIELD_QUESTION_CONDITION_SEQUENCE
		+ ", A." + FIELD_QUESTION_ID
		+ ", A." + FIELD_PARENT_QUESTION_ID
		+ ", A." + FIELD_DEFAULT_BASELINE // CJ-584
		+ ", A." + FIELD_ORIGINAL_CONDITION_TEXT
		+ ", A." + FIELD_CONDITION_TO_QUESTION
		+ ", A." + FIELD_CREATED_AT
		+ ", A." + FIELD_UPDATED_AT
		+ ", (SELECT C." + QuestionRecord.FIELD_QUESTION_CODE
			+ " FROM " + QuestionRecord.TABLE_QUESTIONS + " C"
			+ " WHERE C." + QuestionRecord.FIELD_QUESTION_ID + " = A." + FIELD_PARENT_QUESTION_ID + ") ParentQustionCode" // + ", B." + QuestionRecord.FIELD_QUESTION_CODE + " ParentQustionCode"
		+ " FROM " + TABLE_QUESTION_CONDITIONS + " A"
		+ " JOIN " + QuestionRecord.TABLE_QUESTIONS + " B ON (B." + QuestionRecord.FIELD_QUESTION_ID + " = A." + FIELD_QUESTION_ID + ")"
		+ " ORDER BY B." + QuestionRecord.FIELD_QUESTION_GROUP + ", A." + FIELD_QUESTION_CONDITION_SEQUENCE; // + ", A." + FIELD_QUESTION_ID;

	private Integer id; 
	private Integer sequence; 
	private Integer questionId; 
	private Integer parentQuestionId; // new
	private String parentQuestionCode; // new
	private Boolean defaultBaseline; // CJ-584
	private String originalConditionText; 
	private String conditionToQuestion; 
	private Date createdAt; 
	private Date updatedAt;
	
	public void read(ResultSet rs) throws SQLException {
		this.id = rs.getInt(FIELD_QUESTION_CONDITION_ID); 
		this.sequence = CommonUtils.toInteger(rs.getObject(FIELD_QUESTION_CONDITION_SEQUENCE));
		this.questionId = CommonUtils.toInteger(rs.getObject(FIELD_QUESTION_ID));
		this.parentQuestionId = CommonUtils.toInteger(rs.getObject(FIELD_PARENT_QUESTION_ID)); // new
		this.defaultBaseline = CommonUtils.toBoolean(rs.getObject(FIELD_DEFAULT_BASELINE)); // CJ-584
		this.originalConditionText = rs.getString(FIELD_ORIGINAL_CONDITION_TEXT); 
		this.conditionToQuestion = rs.getString(FIELD_CONDITION_TO_QUESTION); 
		this.createdAt = CommonUtils.toDateTime(rs, FIELD_CREATED_AT);
		this.updatedAt = CommonUtils.toDateTime(rs, FIELD_UPDATED_AT);
		this.parentQuestionCode = rs.getString("ParentQustionCode"); // new
	}
	
	@Override
	protected void populateJson(ObjectNode json, boolean forInterview) {
		json.put(FIELD_QUESTION_CONDITION_SEQUENCE, this.sequence);
		json.put(FIELD_QUESTION_ID, this.questionId);
		json.put(FIELD_PARENT_QUESTION_ID, this.parentQuestionId); // new
		json.put(FIELD_DEFAULT_BASELINE, this.defaultBaseline); // CJ-584
		json.put(FIELD_ORIGINAL_CONDITION_TEXT, this.originalConditionText);
		json.put(FIELD_CONDITION_TO_QUESTION, this.conditionToQuestion);
		if (!forInterview) {
			json.put(FIELD_QUESTION_CONDITION_ID, this.id);
			json.put(FIELD_CREATED_AT, CommonUtils.toJsonDate(this.createdAt));
			json.put(FIELD_UPDATED_AT, CommonUtils.toJsonDate(this.updatedAt));
		}
	}

	// ---------------------------------------------------------------------
	public Integer getId() { return id; }
	
	public Integer getSequence() { return sequence; }
	public void setSequence(Integer sequence) { this.sequence = sequence; }

	public Integer getQuestionId() { return questionId; }
	public void setQuestionId(Integer questionId) { this.questionId = questionId; }

	public Integer getParentQuestionId() { return parentQuestionId; }
	public void setParentQuestionId(Integer parentQuestionId) { this.parentQuestionId = parentQuestionId; }

	public String getParentQuestionCode() { return parentQuestionCode; }

	public String getOriginalConditionText() { return originalConditionText; }
	public void setOriginalConditionText(String originalConditionText) { this.originalConditionText = originalConditionText; }

	public String getConditionToQuestion() { return conditionToQuestion; }
	public void setConditionToQuestion(String conditionToQuestion) { this.conditionToQuestion = conditionToQuestion; }
	
	public boolean isBaseQuestion() { // CJ-581
		return CommonUtils.isEmpty(this.conditionToQuestion);
	}

	public Date getCreatedAt() { return createdAt; }
	public void setCreatedAt(Date createdAt) { this.createdAt = createdAt; }

	public Date getUpdatedAt() { return updatedAt; }
	public void setUpdatedAt(Date updatedAt) { this.updatedAt = updatedAt; }

	public Boolean getDefaultBaseline() { return defaultBaseline; } // CJ-584
	public void setDefaultBaseline(Boolean defaultBaseline) { this.defaultBaseline = defaultBaseline; } // CJ-584

}
