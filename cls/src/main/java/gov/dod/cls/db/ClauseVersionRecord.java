package gov.dod.cls.db;

import gov.dod.cls.utils.CommonUtils;
import gov.dod.cls.utils.sql.SqlUtil;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.node.ObjectNode;

public class ClauseVersionRecord {

	public static final String TABLE_CLAUSE_VERSIONS = "Clause_Versions";
	
	public static final String FIELD_CLAUSE_VERSION_ID = "clause_version_id";
	public static final String FIELD_CLAUSE_VERSION_NAME = "clause_version_name";
	public static final String FIELD_CLAUSE_VERSION_DATE = "clause_version_date";
	public static final String FIELD_IS_ACTIVE = "is_active";
	public static final String FIELD_FAC_NUMBER = "fac_number";
	public static final String FIELD_DAC_NUMBER = "dac_number";
	public static final String FIELD_DFARS_NUMBER = "dfars_number";
	public static final String FIELD_FAC_DATE = "fac_date";
	public static final String FIELD_DAC_DATE = "dac_date";
	public static final String FIELD_DFARS_DATE = "dfars_date";
	public static final String FIELD_SAT_CONDITIONS = "sat_conditions";
	public static final String FIELD_SAT_300K_RULE = "sat_300k_rule";
	//CJ-1465 SV
	public static final String FIELD_SAT_750K_RULE = "sat_750k_rule";
	public static final String FIELD_SAT_1MILLION_RULE = "sat_1million_rule";
	
	public static final String FIELD_CREATED_AT = "created_at";
	public static final String FIELD_UPDATED_AT = "updated_at";

	public static final String SQL_SELECT
		= "SELECT " + FIELD_CLAUSE_VERSION_ID
		+ ", " + FIELD_CLAUSE_VERSION_NAME
		+ ", " + FIELD_CLAUSE_VERSION_DATE
		+ ", " + FIELD_IS_ACTIVE
		+ ", " + FIELD_FAC_NUMBER
		+ ", " + FIELD_DAC_NUMBER
		+ ", " + FIELD_DFARS_NUMBER
		+ ", " + FIELD_FAC_DATE
		+ ", " + FIELD_DAC_DATE
		+ ", " + FIELD_DFARS_DATE
		+ ", " + FIELD_SAT_CONDITIONS
		+ ", " + FIELD_SAT_300K_RULE
		//CJ-1465
		+ ", " + FIELD_SAT_750K_RULE
		+ ", " + FIELD_SAT_1MILLION_RULE
		+ ", " + FIELD_CREATED_AT
		+ ", " + FIELD_UPDATED_AT
		+ " FROM " + TABLE_CLAUSE_VERSIONS
		+ " ORDER BY " + FIELD_CLAUSE_VERSION_DATE + " DESC";
	
	
	public static Integer getActiveClauseVersionId(Connection conn) throws SQLException {
		String sSql = "SELECT MAX(" + FIELD_CLAUSE_VERSION_ID + ")"
				+ " FROM " + TABLE_CLAUSE_VERSIONS
				+ " WHERE " + FIELD_IS_ACTIVE + " = 1";
		Statement statement = null;
		ResultSet rs1 = null;
		try {
			statement = conn.createStatement();
			rs1 = statement.executeQuery(sSql);
			if (rs1.next()) {
				return rs1.getInt(1);
			} else
				return null;
		} finally {
			SqlUtil.closeInstance(rs1);
			SqlUtil.closeInstance(statement);
		}
	}

	// ======================================================
	private Integer id;
	private String name;
	private Date date;
	private Boolean active;
	private String facNumber;
	private String dacNumber;
	private String dfarsNumber;
	private Date createdAt;
	private Date updatedAt;
	private Date facDate;
	private Date dacDate;
	private Date dfarsDate;
	
	private String satCondition;
	private String sat300KRule = "";
	private String sat750KRule = "";
	private String sat1MillionRule = "";


	public void read(ResultSet rs) throws SQLException {
		this.id = rs.getInt(FIELD_CLAUSE_VERSION_ID); 
		this.name = rs.getString(FIELD_CLAUSE_VERSION_NAME);
		this.date = CommonUtils.toDate(rs.getObject(FIELD_CLAUSE_VERSION_DATE));
		this.active = CommonUtils.toBoolean(rs.getObject(FIELD_IS_ACTIVE));
		this.facNumber = rs.getString(FIELD_FAC_NUMBER);
		this.dacNumber = rs.getString(FIELD_DAC_NUMBER);
		this.dfarsNumber = rs.getString(FIELD_DFARS_NUMBER);
		this.facDate = CommonUtils.toDate(rs.getObject(FIELD_FAC_DATE));
		this.dacDate = CommonUtils.toDate(rs.getObject(FIELD_DAC_DATE));
		this.dfarsDate = CommonUtils.toDate(rs.getObject(FIELD_DFARS_DATE));
		this.satCondition = rs.getString(FIELD_SAT_CONDITIONS);
		this.sat300KRule = rs.getString(FIELD_SAT_300K_RULE);
		this.sat1MillionRule = rs.getString(FIELD_SAT_1MILLION_RULE);
		this.createdAt = CommonUtils.toDate(rs.getObject(FIELD_CREATED_AT));
		this.updatedAt = CommonUtils.toDate(rs.getObject(FIELD_UPDATED_AT));
	}
	
	public ObjectNode genJson(boolean forApi, ObjectMapper mapper) {
		ObjectNode json = mapper.createObjectNode();
		json.put("id", this.id);
		json.put(FIELD_CLAUSE_VERSION_NAME, this.name);
		json.put(FIELD_IS_ACTIVE, this.active);
		if (forApi) {
			json.put(FIELD_CLAUSE_VERSION_DATE, CommonUtils.toZulu(this.date));
			json.put(FIELD_CREATED_AT, CommonUtils.toZulu(this.createdAt));
			json.put(FIELD_UPDATED_AT, CommonUtils.toZulu(this.updatedAt));
		} else {
			json.put(FIELD_CLAUSE_VERSION_DATE, CommonUtils.toJsonDate(this.date));
			json.put(FIELD_CREATED_AT, CommonUtils.toJsonDate(this.createdAt));
			json.put(FIELD_UPDATED_AT, CommonUtils.toJsonDate(this.updatedAt));
			// CJ-477
			json.put(FIELD_FAC_NUMBER, this.facNumber);
			json.put(FIELD_DAC_NUMBER, this.dacNumber);
			json.put(FIELD_DFARS_NUMBER, this.dfarsNumber);
			json.put(FIELD_FAC_DATE, CommonUtils.toJsonDate(this.facDate));
			json.put(FIELD_DAC_DATE, CommonUtils.toJsonDate(this.dacDate));
			json.put(FIELD_DFARS_DATE, CommonUtils.toJsonDate(this.dfarsDate));
			// CJ-1360
			json.put(FIELD_SAT_CONDITIONS, this.satCondition);
			json.put(FIELD_SAT_300K_RULE, this.sat300KRule);
			json.put(FIELD_SAT_1MILLION_RULE, this.sat1MillionRule);
			
		}
		return json;
	}

	// ----------------------------------------------------------
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Boolean getActive() {
		return active;
	}
	public boolean isActive() {
		if (this.active == null)
			return false;
		return this.active.booleanValue();
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public Date getClauseVersionDate() {
		return date;
	}
	
	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public Date getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	public Integer getId() {
		return id;
	}
	
	public String getFacNumber() {
		return this.facNumber;
	}
	
	public void setFacNumber(String facNumber) {
		this.facNumber = facNumber;
	}
	
	public String getDacNumber() {
		return this.dacNumber;
	}
	
	public void setDacNumber(String dacNumber) {
		this.dacNumber = dacNumber;
	}

	
	public String getDFarsNumber() {
		return this.dfarsNumber;
	}
	
	public void setDFarsNumber(String dfarsNumber) {
		this.dfarsNumber = dfarsNumber;
	}
	
	public Date getFacDate() {
		return this.facDate;
	}
	
	public void setFacDate(Date facDate) {
		this.facDate = facDate;
	}
	
	public Date getDacDate() {
		return this.dacDate;
	}
	
	public void setDacDate(Date dacDate) {
		this.dacDate = dacDate;
	}

	
	public Date getDFarsDate() {
		return this.dfarsDate;
	}
	
	public void setDFarsDate(Date dfarsDate) {
		this.dfarsDate = dfarsDate;
	}

	
	public String getSatCondition() {
		return this.satCondition;
	}
	
	public void setSatCondition(String s) {
		this.satCondition = s;
	}
	
	public String getSat300KRule() {
		return this.sat300KRule;
	}
	
	public void setSat300KRule(String s) {
		this.sat300KRule = s;
	}
	
	public String getSat1MillionRule() {
		return this.sat1MillionRule;
	}
	
	public void setSat1MillionRule(String s) {
		this.sat1MillionRule = s;
	}
}
