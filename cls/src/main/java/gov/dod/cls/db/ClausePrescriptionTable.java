package gov.dod.cls.db;

import gov.dod.cls.config.ClsConfig;
import gov.dod.cls.utils.sql.SqlUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;

public class ClausePrescriptionTable extends ArrayList<ClausePrescriptionRecord> {

	private static final long serialVersionUID = 237647845962974343L;

	/*
	public static ClausePrescriptionTable loadSet(Integer clauseId, Integer prescriptionId, Connection conn) {
		ClausePrescriptionTable clausePrescriptionTable = new ClausePrescriptionTable();
		clausePrescriptionTable.load(clauseId, prescriptionId, conn);
		return clausePrescriptionTable;
	}
	*/
	
	public static PreparedStatement prepareStatementForClausePrescriptionIds(Connection conn) throws SQLException {
		String sSql = "SELECT " + ClausePrescriptionRecord.FIELD_PRESCRIPTION_ID
				+ ClausePrescriptionRecord.SQL_FROM
				+ " WHERE A." + ClausePrescriptionRecord.FIELD_CLAUSE_ID + " = ?"
				+ " AND A." + ClausePrescriptionRecord.FIELD_PRESCRIPTION_ID + " IS NOT NULL"
				+ ClausePrescriptionRecord.SQL_ORDER_BY;
		PreparedStatement ps = conn.prepareStatement(sSql);
		return ps;
	}
	
	public static ArrayList<Integer> getArrayByClause(int clauseId, PreparedStatement ps) throws SQLException {
		ArrayList<Integer> result = new ArrayList<Integer>();
		ResultSet rs = null;
		try {
			ps.setInt(1, clauseId);
			rs = ps.executeQuery();
			while (rs.next()) {
				result.add(rs.getInt(1));
			}
		}
		finally {
			SqlUtil.closeInstance(rs);
		}
		return result;
	}
	
	public static PreparedStatement prepareStatementForClause(Connection conn) throws SQLException {
		String sSql = ClausePrescriptionRecord.SQL_SELECT
				+ " WHERE A." + ClausePrescriptionRecord.FIELD_CLAUSE_ID + " = ?"
				+ ClausePrescriptionRecord.SQL_ORDER_BY;
		PreparedStatement ps = conn.prepareStatement(sSql);
		return ps;
	}
	
	/*
	public static ClausePrescriptionTable createByClause(int clauseId, PreparedStatement ps) throws SQLException {
		ClausePrescriptionTable clausePrescriptionTable = new ClausePrescriptionTable();
		clausePrescriptionTable.clauseId = clauseId;
		ResultSet rs = null;
		try {
			ps.setInt(1, clauseId);
			rs = ps.executeQuery();
			while (rs.next()) {
				ClausePrescriptionRecord clausePrescriptionRecord = new ClausePrescriptionRecord();
				clausePrescriptionRecord.read(rs);
				clausePrescriptionTable.add(clausePrescriptionRecord);
			}
		}
		finally {
			SqlUtil.closeInstance(rs);
		}
		return clausePrescriptionTable;
	}
	*/
	
	// ========================================================
	private volatile static ClausePrescriptionTable instance = null;
	private volatile static Date refreshed = null;
	
	public synchronized static ClausePrescriptionTable getInstance(Connection conn) {
		if (ClausePrescriptionTable.instance == null) {
			ClausePrescriptionTable.instance = new ClausePrescriptionTable();
			ClausePrescriptionTable.instance.refresh(conn);
		} else {
			ClausePrescriptionTable.instance.refreshWhenNeeded(conn);
		}
		return ClausePrescriptionTable.instance;
	}
	
	// Functions -----------------------------------------------------------------
	public static Date getReferHistoryDate(Connection conn) {
		return SysLastTableChangeAtTable.getLastUpdateDate(conn, ClausePrescriptionRecord.TABLE_CLAUSE_PRESCRIPTIONS);
	}
	  
	public static boolean needToRefreh(Connection conn) {
		return SysLastTableChangeAtTable.needToLoad(ClausePrescriptionTable.refreshed, getReferHistoryDate(conn));
	}
	
	// ---------------------------------------------------------------
	private Exception lastError;

	//private Integer clauseId;
	//private Integer prescriptionId;

	public void refreshWhenNeeded(Connection conn) {
		if (ClausePrescriptionTable.needToRefreh(conn))
			this.refresh(conn);
	}

	private boolean refresh(Connection conn) {
		boolean result = false;
		String sSql = ClausePrescriptionRecord.SQL_SELECT;
		sSql += ClausePrescriptionRecord.SQL_ORDER_BY;
		boolean bCreateConnection = (conn == null);
			
		PreparedStatement ps1 = null;
		ResultSet rs1 = null;
		try {
			if (bCreateConnection)
				conn = ClsConfig.getDbConnection();
			ps1 = conn.prepareStatement(sSql);
			rs1 = ps1.executeQuery();
			this.clear();
			while (rs1.next()) {
				ClausePrescriptionRecord clausePrescriptionRecord = new ClausePrescriptionRecord();
				clausePrescriptionRecord.read(rs1);
				this.add(clausePrescriptionRecord);
			}
			ClausePrescriptionTable.refreshed = getReferHistoryDate(conn); // CommonUtils.getNow();
			result = true;
			System.out.println("ClausePrescriptionTable refreshed");
		}
		catch (Exception oError) {
			this.lastError = oError;
			oError.printStackTrace();
		}
		finally {
			SqlUtil.closeInstance(rs1, ps1);
			if (bCreateConnection)
				SqlUtil.closeInstance(conn);
		}
		return result;
	}
	
	/*
	public boolean load(Integer clauseId, Integer prescriptionId, Connection conn) {
		boolean result = false;
		this.clauseId = clauseId;
		this.prescriptionId = prescriptionId;
		String sSql = ClausePrescriptionRecord.SQL_SELECT;
		String operator = " WHERE ";
		if (clauseId != null) {
			sSql += operator + ClausePrescriptionRecord.FIELD_CLAUSE_ID + " = ?";
			operator = " AND ";
		}
		if (prescriptionId != null) {
			sSql += operator + ClausePrescriptionRecord.FIELD_PRESCRIPTION_ID + " = ?";
		}
		sSql += ClausePrescriptionRecord.SQL_ORDER_BY;
		boolean bCreateConnection = (conn == null);
			
		PreparedStatement ps1 = null;
		ResultSet rs1 = null;
		try {
			if (bCreateConnection)
				conn = ClsConfig.getDbConnection();
			ps1 = conn.prepareStatement(sSql);
			int iParam = 1;
			if (clauseId != null)
				ps1.setInt(iParam++, clauseId);
			if (prescriptionId != null)
				ps1.setInt(iParam, prescriptionId);
			rs1 = ps1.executeQuery();
			this.clear();
			while (rs1.next()) {
				ClausePrescriptionRecord clausePrescriptionRecord = new ClausePrescriptionRecord();
				clausePrescriptionRecord.read(rs1);
				this.add(clausePrescriptionRecord);
			}
			result = true;
		}
		catch (Exception oError) {
			this.lastError = oError;
			oError.printStackTrace();
		}
		finally {
			SqlUtil.closeInstance(rs1, ps1);
			if (bCreateConnection)
				SqlUtil.closeInstance(conn);
		}
		return result;
	}
	*/
	
	public ClausePrescriptionTable subsetByClauseId(int clauseId) {
		boolean bFound = false;
		ClausePrescriptionTable result = new ClausePrescriptionTable();
		for (ClausePrescriptionRecord record : this) {
			if (record.getClauseId().intValue() == clauseId) {
				result.add(record);
				bFound = true;
			} else
				if (bFound)
					break;
		}
		return result;
	}
	
	public ClausePrescriptionRecord findByPrescriptonId(Integer piPrescriptionId) {
		for (ClausePrescriptionRecord record : this) {
			if (record.getPrescriptionId().equals(piPrescriptionId))
				return record;
		}
		return null;
	}
	
	// ---------------------------------------------
	/*
	public Integer getClauseId() {
		return clauseId;
	}
	public Integer getQuestionId() {
		return prescriptionId;
	}
	*/

	public Exception getLastError() {
		return lastError;
	}

}
