package gov.dod.cls.db.utils;

import gov.dod.cls.config.ClsConfig;
import gov.dod.cls.db.RoleRecord;
import gov.dod.cls.db.UserRoleRecord;
import gov.dod.cls.utils.sql.SqlUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.codehaus.jackson.node.ObjectNode;

public class UserRoles extends ArrayList<String> {

	private static final long serialVersionUID = 1276063692364731172L;
	
	public static final String TAG_ROLES = "Roles";
	
	public static final String SQL_SELECT
		= "SELECT " + RoleRecord.TABLE_ROLES + "." + RoleRecord.FIELD_ROLE_NAME
		+ " FROM " + UserRoleRecord.TABLE_USER_ROLES
		+ " INNER JOIN " + RoleRecord.TABLE_ROLES
		+ " ON (" + RoleRecord.TABLE_ROLES + "." + RoleRecord.FIELD_ROLE_ID
			+ "=" + UserRoleRecord.TABLE_USER_ROLES + "." + UserRoleRecord.FIELD_ROLE_ID + ")"
		+ " WHERE " + UserRoleRecord.TABLE_USER_ROLES + "." + UserRoleRecord.FIELD_USER_ID + " = ?";

	// ------------------------------------------------------
	private Integer userId;
	
	public void load(Integer userId, Connection conn) throws SQLException {
		this.userId = userId;
		this.clear();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = conn.prepareStatement(SQL_SELECT);
			ps.setInt(1, this.userId);
			rs = ps.executeQuery();
			while (rs.next()) {
				this.add(rs.getString(1));
			}
		}
		//catch (Exception oError) {
		//	oError.printStackTrace();
		//}
		finally {
			SqlUtil.closeInstance(rs, ps);
		}
	}

	public void load(Integer userId) throws SQLException {
		Connection conn = null;
		try {
			conn = ClsConfig.getDbConnection();
			this.load(userId, conn); 
		}
		finally {
			SqlUtil.closeInstance(conn);
		}
	}
	
	public void addJson(ObjectNode json) {
		String roles = null;
		for (String role : this) {
			if (roles == null)
				roles = role;
			else
				roles += ", " + role;
		}
		json.put(TAG_ROLES, roles);
	}
	
	public Integer getUserId() {
		return userId;
	}

	public boolean isSuperUser() {
		return this.contains(RoleRecord.ROLE_SYSTEM_ADMINTRATOR);
	}
	
	public boolean isSubsuperUser() {
		return this.contains(RoleRecord.ROLE_AGENCY_ADMINISTRATOR);
	}
	
	public boolean isAgencyReviewer() {
		return this.contains(RoleRecord.ROLE_AGENCY_REVIEWER);
	}
	
	public boolean isReviewer() {
		return this.contains(RoleRecord.ROLE_REVIEWER);
	}
	
	public boolean isRegularUser() {
		return this.contains(RoleRecord.ROLE_REGULAR_USER);
	}
	
}
