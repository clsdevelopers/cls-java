package gov.dod.cls.db;

public class OrgRegulationRecord {

	public static final String TABLE_ORG_REGULATIONS = "Org_Regulations";

	public static final String FIELD_ORG_REGULATION_ID = "org_regulation_id";
	public static final String FIELD_ORG_ID = "org_id";
	public static final String FIELD_REGULATION_ID = "regulation_id";
	public static final String FIELD_CREATED_AT = "created_at";
	public static final String FIELD_UPDATED_AT = "updated_at"; 

}
