package gov.dod.cls.db;

public class DocTypeRefRecord {
	
	public static final String DOC_TYPE_CD_AWARD = "A"; 
	public static final String DOC_TYPE_CD_SOLICITATION = "S";
	public static final String DOC_TYPE_CD_ORDER = "O";
	
	public static final String TABLE_DOC_TYPE_REF = "Doc_Type_Ref";
	
	public static final String FIELD_DOCUMENT_TYPE = "Document_Type";
	public static final String FIELD_DOCUMENT_TYPE_NAME = "Document_Type_Name";
	public static final String FIELD_CREATED_AT = "Created_At";
	public static final String FIELD_UPDATED_AT = "Updated_At";

}
