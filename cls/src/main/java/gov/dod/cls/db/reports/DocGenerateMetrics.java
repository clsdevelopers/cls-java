package gov.dod.cls.db.reports;

import gov.dod.cls.PageApi;
import gov.dod.cls.config.ClsConfig;
import gov.dod.cls.db.DocumentRecord;
import gov.dod.cls.db.OrgRecord;
import gov.dod.cls.db.UserRecord;
import gov.dod.cls.utils.sql.SqlUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DecimalFormat;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.CellStyle;


public class DocGenerateMetrics {


	HSSFWorkbook document = null;
	Integer cntTotalUsers;
	Integer cntTotalDocuments;
	Integer cntTotalUserDocs;
	Integer cntOpenUserDocs;
	Integer cntFinalizedUserDocs;
	Integer cntTotalApiDocs;
	Integer cntOpenApiDocs;
	Integer cntFinalizedApiDocs;
	
	// ------------------------------------------------------

	public static void downloadDoc(HttpServletRequest req, HttpServletResponse resp) {
		DocGenerateMetrics docGenerateDoc = new DocGenerateMetrics();
		try {
			PageApi.send(resp, 
					"attachment; filename=" + "cls_metrics.xls",
					"application/msexcel",
					docGenerateDoc.createDoc());
		} catch (Exception oError) {
			oError.printStackTrace();
			PageApi.send(resp, "text/plain", oError.getMessage());
		}
	}
	
	public DocGenerateMetrics() {
		 cntTotalUsers = 0;
		 cntTotalDocuments = 0;
		 
		 cntTotalUserDocs = 0;
		 cntOpenUserDocs = 0;
		 cntFinalizedUserDocs = 0;
		 
		 cntTotalApiDocs = 0;
		 cntOpenApiDocs = 0;
		 cntFinalizedApiDocs = 0;
	}
	
	public HSSFWorkbook createDoc() throws SQLException{
		Connection conn = null;
		document = new HSSFWorkbook();
		
		try {
			conn = ClsConfig.getDbConnection();
						
			getDailyEnrollees(conn);
			getWeeklyEnrollees(conn);
			getMonthlyEnrollees(conn);
			getAnnualEnrollees(conn);
			getDocsPerAgency(conn);
			getDocsPerAPI(conn);
			getDocumentCounts(conn);
			getDocStats(conn);
		}
		finally {
			SqlUtil.closeInstance(conn);
		}
		
		return document;
        
	}
	
	// http://www.avajava.com/tutorials/lessons/how-do-i-write-to-an-excel-file-using-poi.html
	
	private Integer printHeader (HSSFSheet worksheet, Integer iRow, String sCol1, String sCol2, String sCol3) {

		HSSFRow row = worksheet.createRow(iRow++);
		HSSFCell cellA1 = row.createCell(0);
		HSSFCell cellB1 = row.createCell(1);
		
		cellA1.setCellValue(sCol1);
		cellB1.setCellValue(sCol2);
		
		if (sCol3 != null) {
			HSSFCell cellC1 = row.createCell(2);
			cellC1.setCellValue(sCol3);
		}
		
		return iRow;
	}
	
	private Integer printTotal (HSSFSheet worksheet, Integer iRow, String sTitle, Integer iTotal1, Integer iTotal2) {
		worksheet.createRow(iRow++);
		HSSFRow row = worksheet.createRow(iRow);
		HSSFCell cellA1 = row.createCell(0);
		HSSFCell cellB1 = row.createCell(1);
		
		CellStyle style = document.createCellStyle();
		style.setWrapText(true);
		cellA1.setCellStyle (style);
		
		cellA1.setCellValue(sTitle);
		cellB1.setCellValue(iTotal1);
		
		if (iTotal2 != null) {
			HSSFCell cellC1 = row.createCell(2);
			cellC1.setCellValue(iTotal2);
		}
		
		return iRow;
	}
	
	private Integer printStat (HSSFSheet worksheet, Integer iRow, String sTitle, double dTotal) {

		HSSFRow row = worksheet.createRow(iRow);
		HSSFCell cellA1 = row.createCell(0);
		HSSFCell cellB1 = row.createCell(1);
		 
		cellA1.setCellValue(sTitle);
		cellB1.setCellValue(dTotal);
		
		return iRow;
	}
	
	private Integer printStat (HSSFSheet worksheet, Integer iRow, String sTitle, String sTotal) {

		HSSFRow row = worksheet.createRow(iRow);
		HSSFCell cellA1 = row.createCell(0);
		HSSFCell cellB1 = row.createCell(1);
		 
		cellA1.setCellValue(sTitle);
		cellB1.setCellValue(sTotal);
		
		return iRow;
	}
	
	private void getDailyEnrollees(Connection conn) throws SQLException {
		
		HSSFSheet worksheet = document.createSheet("Daily Registration");
		worksheet.setColumnWidth(0, 3000);
		
		String sSql 
			= "SELECT DATE(" + UserRecord.FIELD_CREATED_AT + "), "
		    + " COUNT(*)"
			+ " FROM " + UserRecord.TABLE_USERS
			+ " WHERE " + UserRecord.FIELD_CREATED_AT + " > DATE(NOW() - INTERVAL 365 DAY) "
			+ " GROUP BY DATE(" + UserRecord.FIELD_CREATED_AT + ") desc";
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		Integer iRow = 0;
		Integer iTotal = 0;
		try {
			ps = conn.prepareStatement(sSql);
			rs = ps.executeQuery();
			
			iRow = printHeader (worksheet, iRow, "Date", "Sum", null);
			
			while (rs.next()) {
				HSSFRow row = worksheet.createRow(iRow);
				HSSFCell cellA1 = row.createCell(0);
				HSSFCell cellB1 = row.createCell(1);
				
				cellA1.setCellValue(rs.getString(1));
				cellB1.setCellValue(rs.getInt(2));
				
				iTotal += rs.getInt(2);
				iRow++;
			}
			
			iRow = printTotal (worksheet, iRow, "Total Users\nPast Year", iTotal, null);
			
		}
		catch (Exception oError) {
			oError.printStackTrace();
		}
		finally {
			SqlUtil.closeInstance(rs, ps);
		}
		
	}

	private void getWeeklyEnrollees(Connection conn) throws SQLException {
		
		HSSFSheet worksheet = document.createSheet("Weekly Registration");
		worksheet.setColumnWidth(0, 3000);
		
		String sSql 
			= "SELECT DATE_FORMAT(" + UserRecord.FIELD_CREATED_AT + ", '%Y') AS 'year_logged', "
			+ " DATE_FORMAT(" + UserRecord.FIELD_CREATED_AT + ", '%U') AS 'week_logged', "
			+ " DATE(" + UserRecord.FIELD_CREATED_AT + " - INTERVAL DATE_FORMAT (" + UserRecord.FIELD_CREATED_AT + ", '%w') DAY),"
		    + " COUNT(*) AS 'week_count'"
			+ " FROM " + UserRecord.TABLE_USERS
			+ " WHERE " + UserRecord.FIELD_CREATED_AT + " > DATE(NOW() - INTERVAL 365 DAY) "
			+ " GROUP BY year_logged, week_logged ORDER BY Created_At desc";
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		Integer iRow = 0;
		Integer iTotal = 0;
		try {
			ps = conn.prepareStatement(sSql);
			rs = ps.executeQuery();
			
			iRow = printHeader (worksheet, iRow, "Date", "Sum", null);
			
			while (rs.next()) {
				HSSFRow row = worksheet.createRow(iRow);
				HSSFCell cellA1 = row.createCell(0);
				HSSFCell cellB1 = row.createCell(1);
				
				cellA1.setCellValue(rs.getString(3));
				cellB1.setCellValue(rs.getInt(4));
				
				iTotal += rs.getInt(4);
				iRow++;
			}
			
			iRow = printTotal (worksheet, iRow, "Total Users\nPast Year", iTotal, null);
		}
		catch (Exception oError) {
			oError.printStackTrace();
		}
		finally {
			SqlUtil.closeInstance(rs, ps);
		}
		
	}
	
	private void getMonthlyEnrollees(Connection conn) throws SQLException {
		
		HSSFSheet worksheet = document.createSheet("Monthly Registration");
		worksheet.setColumnWidth(0, 2500);
		
		String sSql 
			= "SELECT DATE_FORMAT(" + UserRecord.FIELD_CREATED_AT + ", '%Y-%m') AS 'month_logged',"
		    + " COUNT(*) AS 'month_count'"
			+ " FROM " + UserRecord.TABLE_USERS
			+ " WHERE " + UserRecord.FIELD_CREATED_AT + " > DATE(NOW() - INTERVAL 365 DAY) "
			+ " GROUP BY month_logged desc";
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		Integer iRow = 0;
		Integer iTotal = 0;
		try {
			ps = conn.prepareStatement(sSql);
			rs = ps.executeQuery();
			
			iRow = printHeader (worksheet, iRow, "Date", "Sum", null);
			
			while (rs.next()) {
				HSSFRow row = worksheet.createRow(iRow);
				HSSFCell cellA1 = row.createCell(0);
				HSSFCell cellB1 = row.createCell(1);
				
				cellA1.setCellValue(rs.getString(1));
				cellB1.setCellValue(rs.getInt(2));
				
				iTotal += rs.getInt(2);
				iRow++;
			}
			
			iRow = printTotal (worksheet, iRow, "Total Users\nPast Year", iTotal, null);
			
		}
		catch (Exception oError) {
			oError.printStackTrace();
		}
		finally {
			SqlUtil.closeInstance(rs, ps);
		}
		
	}
	
	private void getAnnualEnrollees(Connection conn) throws SQLException {
		
		HSSFSheet worksheet = document.createSheet("Annual Registration");
		
		String sSql 
			= "SELECT DATE_FORMAT(" + UserRecord.FIELD_CREATED_AT + ", '%Y') AS 'year_logged',"
		    + " COUNT(*) AS 'year_count'"
			+ " FROM " + UserRecord.TABLE_USERS
			//+ " WHERE " + UserRecord.FIELD_CREATED_AT + " > DATE(NOW() - INTERVAL 365 DAY) "
			+ " GROUP BY year_logged desc";
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		Integer iRow = 0;
		Integer iTotal = 0;
		try {
			ps = conn.prepareStatement(sSql);
			rs = ps.executeQuery();
			
			iRow = printHeader (worksheet, iRow, "Date", "Sum", null);
			
			while (rs.next()) {
				HSSFRow row = worksheet.createRow(iRow);
				HSSFCell cellA1 = row.createCell(0);
				HSSFCell cellB1 = row.createCell(1);
				
				cellA1.setCellValue(rs.getInt(1));
				cellB1.setCellValue(rs.getInt(2));
				iTotal += rs.getInt(2);
				iRow++;
			}
			
			iRow = printTotal (worksheet, iRow, "Total Users", iTotal, null);
			this.cntTotalUsers = iTotal;
			
		}
		catch (Exception oError) {
			oError.printStackTrace();
		}
		finally {
			SqlUtil.closeInstance(rs, ps);
		}
		
	}

	private void getDocsPerAgency(Connection conn) throws SQLException {
		
		HSSFSheet worksheet = document.createSheet("Docs, per Agency");
		worksheet.setColumnWidth(0, 10000);

		CellStyle style = document.createCellStyle();
		style.setAlignment(HSSFCellStyle.ALIGN_LEFT);
		
		String sSql 
			= "SELECT O.Org_Name, COUNT(D2." + DocumentRecord.FIELD_DOCUMENT_ID + ") , COUNT(D3." + DocumentRecord.FIELD_DOCUMENT_ID + ")"
			+ " FROM " + DocumentRecord.TABLE_DOCUMENTS + " D"
			+ " INNER JOIN " + UserRecord.TABLE_USERS + " U"
			+ " ON D." + DocumentRecord.FIELD_USER_ID + " = U." + UserRecord.FIELD_USER_ID
			+ " INNER JOIN " + OrgRecord.TABLE_ORGS + "  O"
			+ " ON O." + OrgRecord.FIELD_ORG_ID + " = U." + UserRecord.FIELD_ORG_ID
			+ " LEFT OUTER JOIN " + DocumentRecord.TABLE_DOCUMENTS + " D2"
			+ " ON D2." + DocumentRecord.FIELD_DOCUMENT_ID + " = D." + DocumentRecord.FIELD_DOCUMENT_ID
			+ " AND D2." + DocumentRecord.FIELD_DOCUMENT_STATUS + " = 'F'"
			+ " LEFT OUTER JOIN " + DocumentRecord.TABLE_DOCUMENTS + " D3"
			+ " ON D3." + DocumentRecord.FIELD_DOCUMENT_ID + " = D." + DocumentRecord.FIELD_DOCUMENT_ID
			+ " AND D3." + DocumentRecord.FIELD_DOCUMENT_STATUS + " != 'F'"
			+ " GROUP BY " + OrgRecord.FIELD_ORG_NAME;
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		Integer iRow = 0;
		Integer iOpenTotal = 0;
		Integer iFinalTotal = 0;
		
		try {
			ps = conn.prepareStatement(sSql);
			rs = ps.executeQuery();
			
			iRow = printHeader (worksheet, iRow, "Agency", "Open", "Finalized");
			
			while (rs.next()) {
				HSSFRow row = worksheet.createRow(iRow);
				HSSFCell cellA1 = row.createCell(0);
				HSSFCell cellB1 = row.createCell(1);
				HSSFCell cellC1 = row.createCell(2);
				
				boolean bNumber = false;
				Integer iNumber = 0;
				try {
					iNumber = Integer.parseInt(rs.getString(1));
				    bNumber = true;
				} catch (NumberFormatException e) {
				}
				
				if (bNumber) {
					cellA1.setCellValue(iNumber);
					cellA1.setCellStyle(style);
				}
				else
					cellA1.setCellValue(rs.getString(1));
				cellB1.setCellValue(rs.getInt(3));	// open doc count.
				cellC1.setCellValue(rs.getInt(2));	// finalized doc count.
				
				iOpenTotal += rs.getInt(3);
				iFinalTotal += rs.getInt(2);
				iRow++;
			}
			
			iRow = printTotal (worksheet, iRow, "Total Agency Documents",  iOpenTotal, iFinalTotal);
			
		}
		catch (Exception oError) {
			oError.printStackTrace();
		}
		finally {
			SqlUtil.closeInstance(rs, ps);
		}
		
	}
	
	private void getDocsPerAPI(Connection conn) throws SQLException {
		
		HSSFSheet worksheet = document.createSheet("Monthly - API Docs");
		worksheet.setColumnWidth(0, 3500);
		
		String sSql 
			= "SELECT DATE_FORMAT(" + UserRecord.FIELD_CREATED_AT + ", '%Y-%m') AS 'month_logged',"
		    + " COUNT(*) AS 'month_count'"
			+ " FROM " + DocumentRecord.TABLE_DOCUMENTS 
			+ " WHERE " + DocumentRecord.FIELD_USER_ID + " IS NULL "
			+ " AND " + UserRecord.FIELD_CREATED_AT + " > DATE(NOW() - INTERVAL 365 DAY) "
			+ " GROUP BY month_logged desc";
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		Integer iRow = 0;
		Integer iTotal = 0;
		try {
			ps = conn.prepareStatement(sSql);
			rs = ps.executeQuery();
			
			iRow = printHeader (worksheet, iRow, "Date", "Sum", null);
			
			while (rs.next()) {
				HSSFRow row = worksheet.createRow(iRow);
				HSSFCell cellA1 = row.createCell(0);
				HSSFCell cellB1 = row.createCell(1);
				
				cellA1.setCellValue(rs.getString(1));
				cellB1.setCellValue(rs.getInt(2));
				
				iTotal += rs.getInt(2);
				iRow++;
			}
			
			iRow = printTotal (worksheet, iRow, "Total Documents\nPast Year", iTotal, null);
			
		}
		catch (Exception oError) {
			oError.printStackTrace();
		}
		finally {
			SqlUtil.closeInstance(rs, ps);
		}
		
	}
	/*
		Avg number of open documents per user (Avg = sum(opendocs)/sum(users))
		Avg number of finalized documents per user (Avg = sum(finalizeddocs)/sum(users))
		Avg number of total documents per user (Avg = sum(totaldocs)/sum(users))
		Percentage of open documents (Percent Open = sum(totaldocs)/sum(opendocs))
		Percentage of finalized documents (Percent Finalized = sum(totaldocs)/sum(finalizeddocs))
		Sum of all open documents created altogether
		Sum of finalized documents created altogether
	*/
	private void getDocStats(Connection conn) {
		
		HSSFSheet worksheet = document.createSheet("Document Stats");
		worksheet.setColumnWidth(0, 10000);
		
		Integer iRow = 0;
		
		if ((this.cntOpenUserDocs > 0) && (this.cntTotalUsers > 0))
			printStat (worksheet, iRow++, "Avg number of open documents per user", roundTwoDecimals ((double) this.cntOpenUserDocs / this.cntTotalUsers)) ;
		else
			printStat (worksheet, iRow++, "Avg number of open documents per user", 0) ;
		
		if ((this.cntFinalizedUserDocs > 0) && (this.cntTotalUsers > 0))
			printStat (worksheet, iRow++, "Avg number of finalized documents per user", roundTwoDecimals ((double) this.cntFinalizedUserDocs / this.cntTotalUsers)) ;	
		else 
			printStat (worksheet, iRow++, "Avg number of finalized documents per user", 0) ;

		if ((this.cntTotalUserDocs > 0) && (this.cntTotalUsers > 0))
			printStat (worksheet, iRow++, "Avg number of total documents per user", roundTwoDecimals ((double) this.cntTotalUserDocs / this.cntTotalUsers)) ;
		else
			printStat (worksheet, iRow++, "Avg number of total documents per user", 0) ;
		
		if ((this.cntOpenUserDocs > 0) && (this.cntTotalUserDocs > 0))
			printStat (worksheet, iRow++, "Percentage of open documents", (int)(roundTwoDecimals ((double) this.cntOpenUserDocs / this.cntTotalUserDocs) * 100)) ;
		else
			printStat (worksheet, iRow++, "Percentage of open documents", 0) ;
		
		if ((this.cntFinalizedUserDocs > 0) && (this.cntTotalUserDocs > 0))
			printStat (worksheet, iRow++, "Percentage of finalized documents", (int)(roundTwoDecimals ((double) this.cntFinalizedUserDocs / this.cntTotalUserDocs) * 100)) ;
		else
			printStat (worksheet, iRow++, "Percentage of finalized documents", 0) ;
		
		printStat (worksheet, iRow++, "", "") ;	
		printStat (worksheet, iRow++, "Number of open documents, per users", this.cntOpenUserDocs) ;
		printStat (worksheet, iRow++, "Number of finalized documents, per users", this.cntFinalizedUserDocs) ;
		printStat (worksheet, iRow++, "Total Number of documents, per users", this.cntOpenUserDocs + this.cntFinalizedUserDocs) ;
		printStat (worksheet, iRow++, "", "") ;	
		printStat (worksheet, iRow++, "Number of open documents, per API",  this.cntOpenApiDocs) ;
		printStat (worksheet, iRow++, "Number of finalized documents, per API",  this.cntFinalizedApiDocs) ;
		printStat (worksheet, iRow++, "Total Number of documents, per API", this.cntOpenApiDocs + this.cntFinalizedApiDocs) ;

	}
	
	private void getDocumentCounts(Connection conn) throws SQLException {
	
		String sSelectCnt = "SELECT COUNT(*) FROM " + DocumentRecord.TABLE_DOCUMENTS ;
		String sJoinUser =  " D INNER JOIN " + UserRecord.TABLE_USERS 
				+ " U ON D." + DocumentRecord.FIELD_USER_ID + " = U." + UserRecord.FIELD_USER_ID;
		String sWhereNullUser = " WHERE " + DocumentRecord.FIELD_USER_ID + " IS NULL ";
		String sAndNullUser = " AND " + DocumentRecord.FIELD_USER_ID + " IS NULL ";
		String sFinalStatus = " WHERE " + DocumentRecord.FIELD_DOCUMENT_STATUS + " = 'F'";
		String sIncompleteStatus = " WHERE " + DocumentRecord.FIELD_DOCUMENT_STATUS + " != 'F'"; 
		
		String sTotalUserDocs = sSelectCnt + sJoinUser;
		String sFinalUserDocs = sSelectCnt + sJoinUser + sFinalStatus;
		String sOpenUserDocs = sSelectCnt + sJoinUser + sIncompleteStatus;
		
		String sTotalApiDocs = sSelectCnt +  sWhereNullUser;
		String sFinalApiDocs = sSelectCnt + sFinalStatus + sAndNullUser;
		String sOpenApiDocs = sSelectCnt +  sIncompleteStatus + sAndNullUser;

		this.cntTotalDocuments = getSqlCounts (sSelectCnt, conn);
		
		this.cntTotalUserDocs = getSqlCounts (sTotalUserDocs, conn);
		this.cntOpenUserDocs = getSqlCounts (sOpenUserDocs, conn);
		this.cntFinalizedUserDocs = getSqlCounts (sFinalUserDocs, conn);
		
		this.cntTotalApiDocs = getSqlCounts (sTotalApiDocs, conn);
		this.cntOpenApiDocs = getSqlCounts (sOpenApiDocs, conn);
		this.cntFinalizedApiDocs = getSqlCounts (sFinalApiDocs, conn);
	}
	
	private Integer getSqlCounts(String sSql, Connection conn) throws SQLException {
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		Integer iCount = 0;
		
		try {
			ps = conn.prepareStatement(sSql);
			rs = ps.executeQuery();

			if (rs.next()) {
				iCount = rs.getInt(1);	
			}
		}
		catch (Exception oError) {
			oError.printStackTrace();
		}
		finally {
			SqlUtil.closeInstance(rs, ps);
		}
		
		return iCount;
		
	}
	
	double roundTwoDecimals(double d) {
		DecimalFormat twoDForm = new DecimalFormat("#.##");
		return Double.valueOf(twoDForm.format(d));
	}
}
