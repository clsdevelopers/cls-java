package gov.dod.cls.db;

import gov.dod.cls.PageApi;
import gov.dod.cls.bean.session.UserSession;
import gov.dod.cls.config.ClsConfig;
import gov.dod.cls.utils.CommonUtils;
import gov.dod.cls.utils.Pagination;
import gov.dod.cls.utils.sql.SqlInsert;
import gov.dod.cls.utils.sql.SqlUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.codehaus.jackson.node.ArrayNode;
import org.codehaus.jackson.node.ObjectNode;

public class DocumentTable {
	
	// ----------------------------------------------------------
	public static final String PREFIX_DO_DOCUMENT = "document-";
	public static final String DO_DOCUMENT_LIST = PREFIX_DO_DOCUMENT + "list";

	public static void processRequest(String sDo, UserSession oUserSession, 
			HttpServletRequest req, HttpServletResponse resp) {

		if ((oUserSession.isSuperUser() || oUserSession.isSubsuperUser()) == false) {
			oUserSession.setMessage("Unauthorized access");
			PageApi.sendRedirect(req, resp, "");
			return;
		}
		
		switch (sDo) {
		case DocumentTable.DO_DOCUMENT_LIST:
			DocumentTable.processAdminList(req, resp);
		}
	}
	
	// ----------------------------------------------------------
	private static final String SQL_ADMIN_LIST
		= "SELECT A." + DocumentRecord.FIELD_DOCUMENT_ID
		+ ", A." + DocumentRecord.FIELD_DOCUMENT_TYPE
		+ ", A." + DocumentRecord.FIELD_ACQUISITION_TITLE
		+ ", A." + DocumentRecord.FIELD_DOCUMENT_NUMBER
		+ ", A." + DocumentRecord.FIELD_CLAUSE_VERSION_ID		
		+ ", A." + DocumentRecord.FIELD_CREATED_AT
		+ ", CONCAT(B." + UserRecord.FIELD_FIRST_NAME + ", ' ', B." + UserRecord.FIELD_LAST_NAME + ") AS UserName"
		+ ", A." + DocumentRecord.FIELD_DOCUMENT_STATUS
		+ " FROM " + DocumentRecord.TABLE_DOCUMENTS + " A"
		+ " LEFT OUTER JOIN " + UserRecord.TABLE_USERS + " B ON (B." + UserRecord.FIELD_USER_ID + " = A." + DocumentRecord.FIELD_USER_ID + ")"
		+ " ORDER BY 2, 3";

	private static void processAdminList(HttpServletRequest req, HttpServletResponse resp) {
		Pagination pagination = new Pagination(req);
		pagination.addDataSource(DocumentRecord.TABLE_DOCUMENTS);
		Connection conn = null;
		PreparedStatement ps1 = null;
		ResultSet rs1 = null;
		try {
			conn = ClsConfig.getDbConnection();
			String sSql = "SELECT COUNT(" + DocumentRecord.FIELD_DOCUMENT_ID + ") FROM " + DocumentRecord.TABLE_DOCUMENTS;
			ps1 = conn.prepareStatement(sSql);
			rs1 = ps1.executeQuery();
			boolean isAcceptableRange = false;
			if (rs1.next())
				isAcceptableRange = pagination.isAcceptedRange(rs1.getInt(1));
			
			if (isAcceptableRange) {
				SqlUtil.closeInstance(rs1);
				rs1 = null;
				SqlUtil.closeInstance(ps1);
				ps1 = null;

				ps1 = conn.prepareStatement(SQL_ADMIN_LIST + pagination.sqlLimit());
				rs1 = ps1.executeQuery();
				while (rs1.next()) {
					ObjectNode json = pagination.getMapper().createObjectNode();
					json.put(DocumentRecord.FIELD_DOCUMENT_ID, rs1.getInt(DocumentRecord.FIELD_DOCUMENT_ID));
					json.put(DocumentRecord.FIELD_DOCUMENT_TYPE, rs1.getString(DocumentRecord.FIELD_DOCUMENT_TYPE));
					json.put(DocumentRecord.FIELD_ACQUISITION_TITLE, rs1.getString(DocumentRecord.FIELD_ACQUISITION_TITLE));
					json.put(DocumentRecord.FIELD_DOCUMENT_NUMBER, rs1.getString(DocumentRecord.FIELD_DOCUMENT_NUMBER));
					json.put(DocumentRecord.FIELD_CREATED_AT, CommonUtils.toJsonDate(rs1.getTimestamp(DocumentRecord.FIELD_CREATED_AT)));
					json.put("UserName", rs1.getString("UserName"));
					json.put(DocumentRecord.FIELD_DOCUMENT_STATUS, rs1.getString(DocumentRecord.FIELD_DOCUMENT_STATUS));
					json.put("clause_version", ClauseVersionTable.getName(CommonUtils.toInteger(rs1.getObject(DocumentRecord.FIELD_CLAUSE_VERSION_ID))));
						
					pagination.incCount();
					pagination.getArrayNode().add(json);
				}
			}
			pagination.send(resp);
		}
		catch (Exception oError) {
			oError.printStackTrace();
		}
		finally {
			SqlUtil.closeInstance(rs1, ps1, conn);
		}
	}
	
	public static boolean hasApplicationDocument(int applicationId, int docId, Connection conn) throws Exception {
		boolean result = false;
		boolean bCreateConnection = (conn == null);
		PreparedStatement ps1 = null;
		ResultSet rs1 = null;
		try {
			if (bCreateConnection)
				conn = ClsConfig.getDbConnection();
			String sSql = "SELECT " + DocumentRecord.FIELD_DOCUMENT_ID
					+ " FROM " + DocumentRecord.TABLE_DOCUMENTS
					+ " WHERE " + DocumentRecord.FIELD_APPLICATION_ID + " = ?"
					+ " AND " + DocumentRecord.FIELD_DOCUMENT_ID + " = ?";
			ps1 = conn.prepareStatement(sSql);
			ps1.setInt(1, applicationId);
			ps1.setInt(2, docId);
			rs1 = ps1.executeQuery();
			if (rs1.next())
				result = true;
		}
		catch (Exception oError) {
			oError.printStackTrace();
			throw oError;
		}
		finally {
			SqlUtil.closeInstance(rs1, ps1);
			if (bCreateConnection)
				SqlUtil.closeInstance(conn);
		}
		return result;
	}
	
	public static boolean hasUserDocument(UserSession oUserSession, int docId, Connection conn) {
		if (oUserSession == null)
			return false;
		if (oUserSession.getOauthInterviewRecord() != null) {
			if (oUserSession.getOauthInterviewRecord().getDocumentId().intValue() == docId) {
				return true;
			} else {
				return false;
			}
		}
		boolean result = false;
		boolean bCreateConnection = (conn == null);
		PreparedStatement ps1 = null;
		ResultSet rs1 = null;
		try {
			if (bCreateConnection)
				conn = ClsConfig.getDbConnection();
			Integer userId = null;
			Integer orgId = null;
			if (!(oUserSession.isSuperUser() || oUserSession.isReviewer())) {
				if (oUserSession.isSubsuperUser() || oUserSession.isAgencyReviewer())
					orgId = oUserSession.getOrgId();
				else
					userId = oUserSession.getUserId();
			}
			String sSql = "SELECT A." + DocumentRecord.FIELD_DOCUMENT_ID
					+ " FROM " + DocumentRecord.TABLE_DOCUMENTS + " A "
					+ ((userId == null) ? "LEFT OUTER JOIN " : "INNER JOIN ") + UserRecord.TABLE_USERS + " B"
					+ " ON (B." + UserRecord.FIELD_USER_ID + "=A." + DocumentRecord.FIELD_USER_ID + ")"
					+ " WHERE A." + DocumentRecord.FIELD_DOCUMENT_ID + " = ?";
			if ((userId != null) || (orgId != null)) {
				String sSecurityCriteris = "";
				if (userId != null) {
					sSecurityCriteris = "A." + DocumentRecord.FIELD_USER_ID + "=" + userId;
				}
				if (orgId != null) {
					sSecurityCriteris += (sSecurityCriteris.isEmpty() ? "" : " OR ")
							+ "B." + UserRecord.FIELD_ORG_ID + " = " + orgId
							+ " OR A." + DocumentRecord.FIELD_APPLICATION_ID
							+ " IN (SELECT " + OauthApplicationRecord.FIELD_APPLICATION_ID
							+ " FROM " + OauthApplicationRecord.TABLE_OAUTH_APPLICATIONS
							+ " WHERE " + OauthApplicationRecord.FIELD_ORG_ID + " = " + orgId
							+ ")";
				}
				sSql += " AND (" + sSecurityCriteris + ")";
			}
			/* CJ-680 replaced followings with above
			boolean bSuperUser = (oUserSession.isSuperUser() || oUserSession.isSubsuperUser());
			if (bSuperUser == false) {
				sSql += " AND " + DocumentRecord.FIELD_USER_ID + " = ?";
			}
			*/
			ps1 = conn.prepareStatement(sSql);
			ps1.setInt(1, docId);
			//if (bSuperUser == false)
			//	ps1.setInt(2, oUserSession.getUserId());
			rs1 = ps1.executeQuery();
			if (rs1.next())
				result = true;
		}
		catch (Exception oError) {
			oError.printStackTrace();
		}
		finally {
			SqlUtil.closeInstance(rs1, ps1);
			if (bCreateConnection)
				SqlUtil.closeInstance(conn);
		}
		return result;
	}
	
	public static void processApiList(int applicationId, boolean bVer1, Pagination pagination, Connection conn) 
			throws Exception {
		boolean bCreateConnection = (conn == null);
		PreparedStatement ps1 = null;
		ResultSet rs1 = null;
		PreparedStatement ps2 = null;
		ResultSet rs2 = null;
		try {
			if (bCreateConnection)
				conn = ClsConfig.getDbConnection();
			String sSql = "SELECT COUNT(" + DocumentRecord.FIELD_DOCUMENT_ID + ")"
					+ " FROM " + DocumentRecord.TABLE_DOCUMENTS
					+ " WHERE " + DocumentRecord.FIELD_APPLICATION_ID + " = ?";
			ps1 = conn.prepareStatement(sSql);
			
			ps1.setInt(1, applicationId);
			rs1 = ps1.executeQuery();
			boolean isAcceptableRange = false;
			if (rs1.next())
				isAcceptableRange = pagination.isAcceptedRange(rs1.getInt(1));
			
			if (isAcceptableRange) {
				SqlUtil.closeInstance(rs1);
				rs1 = null;
				SqlUtil.closeInstance(ps1);
				ps1 = null;

				sSql = "SELECT " + DocumentRecord.FIELD_DOCUMENT_ID
						+ " FROM " + DocumentRecord.TABLE_DOCUMENTS
						+ " WHERE " + DocumentRecord.FIELD_SOLICITATION_ID + " = ?"
						+ " ORDER BY 1";
				ps2 = conn.prepareStatement(sSql);

				sSql = DocumentRecord.SQL_SELECT
					+ " WHERE " + DocumentRecord.FIELD_APPLICATION_ID + " = ?";
				ps1 = conn.prepareStatement(sSql + pagination.sqlLimit());
				ps1.setInt(1, applicationId);
				rs1 = ps1.executeQuery();
				while (rs1.next()) {
					ObjectNode json = pagination.getMapper().createObjectNode();
					DocumentRecord.populateJsonForApi(json, bVer1, rs1, false);
					
					// CJ-401
					ArrayNode anLinkedAwardDocId = pagination.getMapper().createArrayNode();
					if (DocTypeRefRecord.DOC_TYPE_CD_SOLICITATION.equals(rs1.getString(DocumentRecord.FIELD_DOCUMENT_TYPE))) {
						Integer solicitationId = rs1.getInt(DocumentRecord.FIELD_DOCUMENT_ID);
						ps2.setInt(1, solicitationId);
						rs2 = ps2.executeQuery();
						while (rs2.next()) {
							Integer linkedAwardId = rs2.getInt(DocumentRecord.FIELD_DOCUMENT_ID);
							anLinkedAwardDocId.add(linkedAwardId);
						}
						SqlUtil.closeInstance(rs2);
						rs2 = null;
					}
					json.put(DocumentRecord.TAG_LINKED_AWARD_DOCUMENT_ID, anLinkedAwardDocId);
					
					pagination.getArrayNode().add(json);
					pagination.incCount();
				}
			}
		}
		catch (Exception oError) {
			oError.printStackTrace();
			throw oError;
		}
		finally {
			SqlUtil.closeInstance(rs2, ps2);
			SqlUtil.closeInstance(rs1, ps1);
			if (bCreateConnection)
				SqlUtil.closeInstance(conn);
		}
	}

	public static int countOauthRecord(Connection conn, int applicationId) throws SQLException {
		int result = 0;
		boolean bCreateConnection = (conn == null);
		PreparedStatement ps1 = null;
		ResultSet rs1 = null;
		try {
			String sSql = "SELECT COUNT(*)"
					+ " FROM " + DocumentRecord.TABLE_DOCUMENTS
					+ " WHERE " + DocumentRecord.FIELD_APPLICATION_ID + " = ?";
			if (bCreateConnection)
				conn = ClsConfig.getDbConnection();
			ps1 = conn.prepareStatement(sSql);
			ps1.setInt(1, applicationId);
			rs1 = ps1.executeQuery();
			if (rs1.next()) {
				result = rs1.getInt(1);
			}
		}
		finally {
			SqlUtil.closeInstance(rs1, ps1);
			if (bCreateConnection)
				SqlUtil.closeInstance(conn);
		}
		return result;
	}
	
	// ----------------------------------------------------------
	/*
	private static final String SQL_DASHBOARD_LIST
		= "SELECT A." + DocumentRecord.FIELD_DOCUMENT_ID
		+ ", A." + DocumentRecord.FIELD_DOCUMENT_TYPE
		+ ", A." + DocumentRecord.FIELD_ACQUISITION_TITLE
		+ ", A." + DocumentRecord.FIELD_DOCUMENT_NUMBER
		+ ", A." + DocumentRecord.FIELD_CREATED_AT
		+ ", CONCAT(B." + UserRecord.FIELD_FIRST_NAME + ", ' ', B." + UserRecord.FIELD_LAST_NAME + ") AS UserName"
		+ ", A." + DocumentRecord.FIELD_IS_INTERVIEW_COMPLETE
		+ " FROM " + DocumentRecord.TABLE_DOCUMENTS + " A"
		+ " LEFT OUTER JOIN " + UserRecord.TABLE_USERS + " B ON (B.id = A." + DocumentRecord.FIELD_USER_ID + ")"
		+ " ORDER BY 2, 3";
	*/
	
	private static final String TAG_LINKED_DOCUMENTS = "LinkedDocuments";

	private static String getSotrOrder(String sort_field, String sort_order) {
		String sortField;
		switch (sort_field) {
		case DocumentRecord.FIELD_CREATED_AT:
			sortField = DocumentRecord.FIELD_CREATED_AT;
			break;
		case DocumentRecord.FIELD_ACQUISITION_TITLE:
			sortField = DocumentRecord.FIELD_ACQUISITION_TITLE;
			break;
		case DocumentRecord.FIELD_DOCUMENT_NUMBER:
			sortField = DocumentRecord.FIELD_DOCUMENT_NUMBER;
			break;	
		case DocumentRecord.FIELD_CLAUSE_VERSION_ID:
			sortField = DocumentRecord.FIELD_CLAUSE_VERSION_ID;
			break;			
		case UserRecord.FIELD_EMAIL:
			sortField = UserRecord.FIELD_EMAIL;
			break;
		case DocumentRecord.FIELD_DOCUMENT_TYPE:
			sortField = DocumentRecord.FIELD_DOCUMENT_TYPE;
			break;
		case DocumentRecord.FIELD_UPDATED_AT:
			sortField = DocumentRecord.FIELD_UPDATED_AT;
			break;
		default: return "";
			
		}
		return " ORDER BY " + sortField + ("desc".equals(sort_order) ? " DESC" : "");
	}
	
	@SuppressWarnings("resource")
	public static void processDashboardList(HttpServletRequest req, HttpServletResponse resp,
			String interviewStatus, Integer userId, Integer orgId, // CJ-680
			String query, String sortField, String sortOrder) {
		Pagination pagination = new Pagination(req);
		pagination.addDataSource(DocumentRecord.TABLE_DOCUMENTS);
		Connection conn = null;
		PreparedStatement ps1 = null;
		ResultSet rs1 = null;
		PreparedStatement ps2 = null;
		ResultSet rs2 = null;
		try {
			conn = ClsConfig.getDbConnection();
			String sFromSql = 
					" FROM " + DocumentRecord.TABLE_DOCUMENTS + " A "
					+ ((userId == null) ? "LEFT OUTER JOIN " : "INNER JOIN ") + UserRecord.TABLE_USERS + " B"
					+ " ON (B." + UserRecord.FIELD_USER_ID + "=A." + DocumentRecord.FIELD_USER_ID + ")";
			String sFromWhereSql = sFromSql
					//+ " WHERE A." + DocumentRecord.FIELD_DOCUMENT_STATUS + "=" + (interview_complete ? "'C'" : "'I'"); 
					+ " WHERE A." + DocumentRecord.FIELD_DOCUMENT_STATUS + "='" + interviewStatus + "' ";
			if ((userId != null) || (orgId != null)) {
				String sSecurityCriteris = "";
				if (userId != null) {
					sSecurityCriteris = "A." + DocumentRecord.FIELD_USER_ID + "=?";
				}
				if (orgId != null) {
					sSecurityCriteris += (sSecurityCriteris.isEmpty() ? "" : " OR ")
							+ "B." + UserRecord.FIELD_ORG_ID + " = " + orgId
							+ " OR A." + DocumentRecord.FIELD_APPLICATION_ID
							+ " IN (SELECT " + OauthApplicationRecord.FIELD_APPLICATION_ID
							+ " FROM " + OauthApplicationRecord.TABLE_OAUTH_APPLICATIONS
							+ " WHERE " + OauthApplicationRecord.FIELD_ORG_ID + " = " + orgId
							+ ")";
				}
				sFromWhereSql += " AND (" + sSecurityCriteris + ")";
			}
			if (CommonUtils.isNotEmpty(query)) {
				sFromWhereSql += " AND ("
					+ "LOWER(A." + DocumentRecord.FIELD_ACQUISITION_TITLE + ") LIKE ?"
					+ " OR LOWER(A." + DocumentRecord.FIELD_DOCUMENT_NUMBER + ") LIKE ?"
					+ " OR LOWER(B." + UserRecord.FIELD_EMAIL + ") LIKE ?"
					+ ")";
				query = "%" + query.toLowerCase() + "%";
			}
			String sSql = "SELECT COUNT(A." + DocumentRecord.FIELD_DOCUMENT_ID + ")" + sFromWhereSql;
			ps1 = conn.prepareStatement(sSql);
			int iParam = 1;
			if (userId != null)
				ps1.setInt(iParam++, userId);
			if (CommonUtils.isNotEmpty(query)) {
				ps1.setString(iParam++, query);
				ps1.setString(iParam++, query);
				ps1.setString(iParam, query);
			}
			rs1 = ps1.executeQuery();
			boolean isAcceptableRange = false;
			if (rs1.next())
				isAcceptableRange = pagination.isAcceptedRange(rs1.getInt(1));
			
			if (isAcceptableRange) {
				rs1 = SqlUtil.closeInstance(rs1);
				ps1 = SqlUtil.closeInstance(ps1);
				
				String sSqlSelect = "SELECT A." + DocumentRecord.FIELD_DOCUMENT_ID // 1
						+ ", A." + DocumentRecord.FIELD_CREATED_AT // 2
						+ ", A." + DocumentRecord.FIELD_ACQUISITION_TITLE // 3
						+ ", A." + DocumentRecord.FIELD_DOCUMENT_NUMBER // 4
						+ ", A." + DocumentRecord.FIELD_CLAUSE_VERSION_ID // 5
						+ ", B." + UserRecord.FIELD_EMAIL // 6
						+ ", A." + DocumentRecord.FIELD_DOCUMENT_TYPE // 7
						+ ", A." + DocumentRecord.FIELD_SOLICITATION_ID
						+ ", A." + DocumentRecord.FIELD_UPDATED_AT;

				if (DocStatusRefRecord.CD_FINALIZED.equals(interviewStatus)) { // ("C".equals(interviewStatus)
					sSql = sSqlSelect + ", A." + DocumentRecord.FIELD_DOCUMENT_STATUS
							+ sFromSql
							+ " WHERE A." + DocumentRecord.FIELD_SOLICITATION_ID + " = ?"
							+ ((userId != null) ? " AND A." + DocumentRecord.FIELD_USER_ID + "=?" : "")
							+ getSotrOrder(sortField, sortOrder);
					ps2 = conn.prepareStatement(sSql);
				}
				
				sSql = sSqlSelect + sFromWhereSql + getSotrOrder(sortField, sortOrder);
				ps1 = conn.prepareStatement(sSql + pagination.sqlLimit());
				iParam = 1;
				if (userId != null)
					ps1.setInt(iParam++, userId);
				if (CommonUtils.isNotEmpty(query)) {
					ps1.setString(iParam++, query);
					ps1.setString(iParam++, query);
					ps1.setString(iParam, query);
				}
				rs1 = ps1.executeQuery();
				while (rs1.next()) {
					String docType = rs1.getString(DocumentRecord.FIELD_DOCUMENT_TYPE);
					
					ObjectNode json = pagination.getMapper().createObjectNode();
					DocumentTable.loadContent(json, rs1);
					if (DocStatusRefRecord.CD_FINALIZED.equals(interviewStatus)) { // ("C".equals(interviewStatus)
						ArrayNode arrayNodeLinked = json.arrayNode();
						if (DocTypeRefRecord.DOC_TYPE_CD_SOLICITATION.equals(docType)) {
							ps2.setInt(1, rs1.getInt(DocumentRecord.FIELD_DOCUMENT_ID));
							if (userId != null)
								ps2.setInt(2, userId);
							rs2 = ps2.executeQuery();
							while (rs2.next()) {
								ObjectNode jsonLinked = pagination.getMapper().createObjectNode();
								DocumentTable.loadContent(jsonLinked, rs2);
								jsonLinked.put(DocumentRecord.FIELD_DOCUMENT_STATUS, rs2.getString(DocumentRecord.FIELD_DOCUMENT_STATUS));
								arrayNodeLinked.add(jsonLinked);
							}
							rs2 = SqlUtil.closeInstance(rs2);
						}
						json.put(TAG_LINKED_DOCUMENTS, arrayNodeLinked);
					}
					pagination.getArrayNode().add(json);
					pagination.incCount();
				}
			}
			pagination.send(resp);
		}
		catch (Exception oError) {
			oError.printStackTrace();
		}
		finally {
			SqlUtil.closeInstance(rs2, ps2);
			SqlUtil.closeInstance(rs1, ps1, conn);
		}
	}
	
	private static void loadContent(ObjectNode json, ResultSet rs) throws SQLException {
		json.put(DocumentRecord.FIELD_DOCUMENT_ID, rs.getInt(DocumentRecord.FIELD_DOCUMENT_ID));
		json.put(DocumentRecord.FIELD_CREATED_AT, CommonUtils.toJsonDate(rs.getTimestamp(DocumentRecord.FIELD_CREATED_AT)));
		json.put(DocumentRecord.FIELD_ACQUISITION_TITLE, rs.getString(DocumentRecord.FIELD_ACQUISITION_TITLE));
		json.put(DocumentRecord.FIELD_DOCUMENT_NUMBER, rs.getString(DocumentRecord.FIELD_DOCUMENT_NUMBER));
		json.put(UserRecord.FIELD_EMAIL, rs.getString(UserRecord.FIELD_EMAIL));
		String sDocType = rs.getString(DocumentRecord.FIELD_DOCUMENT_TYPE);
		Integer iSolicitationId = CommonUtils.toInteger(rs.getObject(DocumentRecord.FIELD_SOLICITATION_ID));
		json.put(DocumentRecord.FIELD_DOCUMENT_TYPE, sDocType);
		json.put(DocumentRecord.FIELD_SOLICITATION_ID, iSolicitationId);
		json.put(DocumentRecord.FIELD_UPDATED_AT, CommonUtils.toJsonDate(rs.getTimestamp(DocumentRecord.FIELD_UPDATED_AT)));
		json.put(DocumentRecord.FIELD_CLAUSE_VERSION_ID, rs.getString(DocumentRecord.FIELD_CLAUSE_VERSION_ID));
		json.put("clause_version", ClauseVersionTable.getName(rs.getInt(DocumentRecord.FIELD_CLAUSE_VERSION_ID)));
		boolean bLinkedAward = (iSolicitationId != null) && (DocTypeRefRecord.DOC_TYPE_CD_AWARD.equals(sDocType));
		json.put(DocumentRecord.TAG_IS_LINKED_AWARD, bLinkedAward);
	}
	
	public static DocumentRecord get(int docId, Connection conn) throws SQLException {
		return DocumentTable.get(null, docId, conn);
	}

	public static DocumentRecord get(DocumentRecord documentRecord, int docId, Connection conn) throws SQLException {
		String sSql = DocumentRecord.SQL_SELECT
				+ " WHERE " + DocumentRecord.FIELD_DOCUMENT_ID + " = ?";
		boolean bCreateConnection = (conn == null);
		PreparedStatement ps1 = null;
		ResultSet rs1 = null;
		try {
			if (bCreateConnection)
				conn = ClsConfig.getDbConnection();
			ps1 = conn.prepareStatement(sSql);
			ps1.setInt(1, docId);
			rs1 = ps1.executeQuery();
			if (rs1.next()) {
				if (documentRecord == null)
					documentRecord = new DocumentRecord();
				documentRecord.read(rs1);
			}
		}
		finally {
			SqlUtil.closeInstance(rs1, ps1);
			if (bCreateConnection)
				SqlUtil.closeInstance(conn);
		}
		return documentRecord;
	}

	// Larry confirmed that no duplicates are allowed regardless of userid or agency.
	// If Document Number exists, then a new document can not be created with same number.
	public static DocumentRecord findbyDocNumber(int userId, String documentNumber, Connection conn) throws SQLException {
		DocumentRecord result = null;
		String sSql = DocumentRecord.SQL_SELECT
				// + " WHERE " + DocumentRecord.FIELD_USER_ID + " = ?"	-- CJ-1247
				+ " WHERE LOWER(" + DocumentRecord.FIELD_DOCUMENT_NUMBER + ") = ?";
		PreparedStatement ps1 = null;
		ResultSet rs1 = null;
		try {
			ps1 = conn.prepareStatement(sSql);
			// ps1.setInt(1, userId); -- CJ-1247
			ps1.setString(1, documentNumber.toLowerCase());
			rs1 = ps1.executeQuery();
			if (rs1.next()) {
				result = new DocumentRecord();
				result.read(rs1);
			}
		}
		finally {
			SqlUtil.closeInstance(rs1, ps1);
		}
		return result;
	}
	
	public static DocumentRecord find(int userId, String docType, String acquisitionTitle, String documentNumber, 
			Connection conn) throws SQLException {
		DocumentRecord result = null;
		String sSql = DocumentRecord.SQL_SELECT
				+ " WHERE " + DocumentRecord.FIELD_USER_ID + " = ?"
				+ " AND " + DocumentRecord.FIELD_DOCUMENT_TYPE + " = ?"
				+ " AND LOWER(" + DocumentRecord.FIELD_ACQUISITION_TITLE + ") = ?"
				+ " AND LOWER(" + DocumentRecord.FIELD_DOCUMENT_NUMBER + ") = ?";
		PreparedStatement ps1 = null;
		ResultSet rs1 = null;
		try {
			ps1 = conn.prepareStatement(sSql);
			ps1.setInt(1, userId);
			ps1.setString(2, docType);
			ps1.setString(3, acquisitionTitle.toLowerCase());
			ps1.setString(4, documentNumber.toLowerCase());
			rs1 = ps1.executeQuery();
			if (rs1.next()) {
				result = new DocumentRecord();
				result.read(rs1);
			}
		}
		finally {
			SqlUtil.closeInstance(rs1, ps1);
		}
		return result;
	}
	
	public static DocumentRecord create(int userId, String docType, String acquisitionTitle, String documentNumber, 
			int clauseVesionId, Connection conn) throws Exception {
		return DocumentTable.create(userId, docType, acquisitionTitle, documentNumber, null, clauseVesionId, conn);
	}
	
	public static DocumentRecord create(int userId, String docType, String acquisitionTitle, String documentNumber, 
			Integer solicitationId, int clauseVesionId, Connection conn) throws Exception {
		AuditEventRecord auditEventRecord = new AuditEventRecord(AuditEventRecord.ACTION_DOCUMENT_CREATED, userId, null);
		SqlInsert sqlInsert = new SqlInsert(null, DocumentRecord.TABLE_DOCUMENTS);
		sqlInsert.addField(DocumentRecord.FIELD_USER_ID, userId, auditEventRecord);
		sqlInsert.addField(DocumentRecord.FIELD_DOCUMENT_TYPE, docType, auditEventRecord);
		sqlInsert.addField(DocumentRecord.FIELD_ACQUISITION_TITLE, acquisitionTitle, auditEventRecord);
		sqlInsert.addField(DocumentRecord.FIELD_DOCUMENT_NUMBER, documentNumber, auditEventRecord);
		sqlInsert.addField(DocumentRecord.FIELD_CLAUSE_VERSION_ID, clauseVesionId, auditEventRecord);
		if (solicitationId != null)
			sqlInsert.addField(DocumentRecord.FIELD_SOLICITATION_ID, solicitationId, auditEventRecord);
		//if (DocTypeRefRecord.DOC_TYPE_CD_ORDER.equals(docType)) // CJ-1005
		//	sqlInsert.addField(DocumentRecord.FIELD_IS_MANUAL_ORDERS, 1, auditEventRecord);
		sqlInsert.execute(conn);
		
		// CJ-1410, save document_id to additional audit comments.
		//auditEventRecord.saveNew(conn);
		//return DocumentTable.find(userId, docType, acquisitionTitle, documentNumber, conn);
		DocumentRecord docRec = DocumentTable.find(userId, docType, acquisitionTitle, documentNumber, conn);
		if (docRec != null)
			auditEventRecord.addAttribute("document_id", docRec.getId());
		auditEventRecord.saveNew(conn);
		return docRec;
	}

	public static DocumentRecord find(int applicationId, String documentNumber, Connection conn) throws SQLException {
		DocumentRecord result = null;
		String sSql = DocumentRecord.SQL_SELECT
				+ " WHERE " + DocumentRecord.FIELD_APPLICATION_ID + " = ?"
				+ " AND LOWER(" + DocumentRecord.FIELD_DOCUMENT_NUMBER + ") = ?";
		boolean bCreateConnection = (conn == null);
		PreparedStatement ps1 = null;
		ResultSet rs1 = null;
		try {
			if (bCreateConnection)
				conn = ClsConfig.getDbConnection();
			ps1 = conn.prepareStatement(sSql);
			ps1.setInt(1, applicationId);
			ps1.setString(2, documentNumber.toLowerCase());
			rs1 = ps1.executeQuery();
			if (rs1.next()) {
				result = new DocumentRecord();
				result.read(rs1);
			}
		}
		finally {
			SqlUtil.closeInstance(rs1, ps1);
			if (bCreateConnection)
				SqlUtil.closeInstance(conn);
		}
		return result;
	}
	
	public static DocumentRecord create(int applicationId, String docType, String acquisitionTitle, String documentNumber, 
			String contractNumber, String orderNumber, String fullName, Integer solicitationId, Integer clauseVesionId,
			Date documentDate, // CJ-1312
			Connection conn) throws Exception {
		boolean bCreateConnection = (conn == null);
		try {
			if (bCreateConnection)
				conn = ClsConfig.getDbConnection();
			if (clauseVesionId == null) {
				ClauseVersionTable.getInstance(conn);
				clauseVesionId = ClauseVersionTable.getActiveVersionId();
			}
			AuditEventRecord auditEventRecord = new AuditEventRecord(AuditEventRecord.ACTION_DOCUMENT_CREATED, null, null);
			SqlInsert sqlInsert = new SqlInsert(null, DocumentRecord.TABLE_DOCUMENTS);
			sqlInsert.addField(DocumentRecord.FIELD_APPLICATION_ID, applicationId, auditEventRecord);
			sqlInsert.addField(DocumentRecord.FIELD_DOCUMENT_TYPE, docType, auditEventRecord);
			sqlInsert.addField(DocumentRecord.FIELD_ACQUISITION_TITLE, acquisitionTitle, auditEventRecord);
			sqlInsert.addField(DocumentRecord.FIELD_DOCUMENT_NUMBER, documentNumber, auditEventRecord);
			sqlInsert.addField(DocumentRecord.FIELD_CLAUSE_VERSION_ID, clauseVesionId, auditEventRecord);
			if (CommonUtils.isNotEmpty(contractNumber))
				sqlInsert.addField(DocumentRecord.FIELD_CONTRACT_NUMBER, contractNumber, auditEventRecord);
			if (CommonUtils.isNotEmpty(orderNumber))
				sqlInsert.addField(DocumentRecord.FIELD_ORDER_NUMBER, orderNumber, auditEventRecord);
			if (CommonUtils.isNotEmpty(fullName))
				sqlInsert.addField(DocumentRecord.FIELD_FULL_NAME, fullName, auditEventRecord);
			//if (DocTypeRefRecord.DOC_TYPE_CD_ORDER.equals(docType)) // CJ-1005
			//	sqlInsert.addField(DocumentRecord.FIELD_IS_MANUAL_ORDERS, 1, auditEventRecord);
			if (solicitationId != null)
				sqlInsert.addField(DocumentRecord.FIELD_SOLICITATION_ID, solicitationId, auditEventRecord);
			if (documentDate != null)
				sqlInsert.addField(DocumentRecord.FIELD_DOCUMENT_DATE, documentDate, auditEventRecord);
			sqlInsert.execute(conn);
			
			// CJ-1410, save document_id to additional audit comments.
			//auditEventRecord.saveNew(conn);
			//return DocumentTable.find(applicationId, documentNumber, conn);
			DocumentRecord docRec = DocumentTable.find(applicationId, documentNumber, conn);
			if (docRec != null)
				auditEventRecord.addAttribute("document_id", docRec.getId());
			auditEventRecord.saveNew(conn);
			return docRec;
		}
		finally {
			if (bCreateConnection)
				SqlUtil.closeInstance(conn);
		}
	}
	
	// CJ-versioning
	// When the document is updated to reflect a new version, the answers must be updated also.
	public static DocumentRecord updateDocVersion(int userId, DocumentRecord docOrigRecord, 
			int clauseVesionId, Connection conn) throws Exception {
		
		AuditEventRecord auditEventRecord = new AuditEventRecord(AuditEventRecord.ACTION_DOCUMENT_CREATED, null, null);
		SqlInsert sqlInsert = new SqlInsert(null, DocumentRecord.TABLE_DOCUMENTS);
		sqlInsert.addField(DocumentRecord.FIELD_USER_ID, docOrigRecord.getUserId(), auditEventRecord);
		sqlInsert.addField(DocumentRecord.FIELD_DOCUMENT_TYPE, docOrigRecord.getDocumentType(), auditEventRecord);
		sqlInsert.addField(DocumentRecord.FIELD_ACQUISITION_TITLE, docOrigRecord.getAcquisitionTitle(), auditEventRecord);
		sqlInsert.addField(DocumentRecord.FIELD_DOCUMENT_NUMBER, docOrigRecord.getDocumentNumber(), auditEventRecord);
		sqlInsert.addField(DocumentRecord.FIELD_CLAUSE_VERSION_ID, clauseVesionId, auditEventRecord);
		sqlInsert.addField(DocumentRecord.FIELD_DOCUMENT_STATUS, "I", auditEventRecord); // new versions - always InProgress
		sqlInsert.addField(DocumentRecord.FIELD_SOLICITATION_ID, docOrigRecord.getSolicitationId(), auditEventRecord);
		sqlInsert.addField(DocumentRecord.FIELD_CREATED_AT, docOrigRecord.getCreatedAt(), auditEventRecord);
		//sqlInsert.addField(DocumentRecord.FIELD_IS_MANUAL_ORDERS, docOrigRecord.getIsManualOrders(), auditEventRecord); // CJ-1005
		if (CommonUtils.isNotEmpty(docOrigRecord.getContractNumber()))
			sqlInsert.addField(DocumentRecord.FIELD_CONTRACT_NUMBER, docOrigRecord.getContractNumber(), auditEventRecord);
		if (CommonUtils.isNotEmpty(docOrigRecord.getOrderNumber()))
			sqlInsert.addField(DocumentRecord.FIELD_ORDER_NUMBER, docOrigRecord.getOrderNumber(), auditEventRecord);
		if (CommonUtils.isNotEmpty(docOrigRecord.getFullName()))
			sqlInsert.addField(DocumentRecord.FIELD_FULL_NAME, docOrigRecord.getFullName(), auditEventRecord);
		sqlInsert.execute(conn);
		
		// Retrieve row with the latest version of the document.
		DocumentRecord result = null;
		PreparedStatement ps1 = null;
		ResultSet rs1 = null;
		try {
			String sSql = DocumentRecord.SQL_SELECT
					+ " WHERE " + DocumentRecord.FIELD_USER_ID + " = ? " 
					+ " ORDER BY " + DocumentRecord.FIELD_DOCUMENT_ID + " DESC";
			ps1 = conn.prepareStatement(sSql);
			ps1.setInt(1, docOrigRecord.getUserId());
			rs1 = ps1.executeQuery();
			if (rs1.next()) {			
				// Double-check that the correct record was retrieved.
				if ((rs1.getString(DocumentRecord.FIELD_ACQUISITION_TITLE).equals(docOrigRecord.getAcquisitionTitle())) 
				&& (rs1.getString(DocumentRecord.FIELD_DOCUMENT_NUMBER).equals(docOrigRecord.getDocumentNumber()))
				&& (rs1.getInt(DocumentRecord.FIELD_DOCUMENT_ID) != docOrigRecord.getId()))
				{
					result = new DocumentRecord();
					result.read(rs1);
				}
			}
		} finally {
			SqlUtil.closeInstance(rs1);
			//rs1 = null;
			SqlUtil.closeInstance(ps1);
			//ps1 = null;
		}
		
		if (result != null)
			auditEventRecord.addAttribute("document_id", result.getId());
		auditEventRecord.saveNew(conn);
		return result;
	}
	
	// CJ-versioning
	public static DocumentRecord find(int documentId, Connection conn) throws SQLException {
		DocumentRecord result = null;
		String sSql = DocumentRecord.SQL_SELECT
				+ " WHERE " + DocumentRecord.FIELD_DOCUMENT_ID + " = ? ";
		boolean bCreateConnection = (conn == null);
		PreparedStatement ps1 = null;
		ResultSet rs1 = null;
		try {
			if (bCreateConnection)
				conn = ClsConfig.getDbConnection();
			ps1 = conn.prepareStatement(sSql);
			ps1.setInt(1, documentId);
			rs1 = ps1.executeQuery();
			while (rs1.next()) {
				result = new DocumentRecord();
				result.read(rs1);
				break;
			}
		}
		finally {
			SqlUtil.closeInstance(rs1, ps1);
			if (bCreateConnection)
				SqlUtil.closeInstance(conn);
		}
		return result;
	}

	// CJ-versioning
	// To finalize the document, change the status.
	public static void updateDocStatus(DocumentRecord docOrigRecord, String status, Connection conn) throws SQLException {
		PreparedStatement ps1 = null;
		try {
			String sSql = 
				"UPDATE " + DocumentRecord.TABLE_DOCUMENTS +
				" SET " + DocumentRecord.FIELD_DOCUMENT_STATUS + " = ? " +
				" WHERE " + DocumentRecord.FIELD_DOCUMENT_ID + " = ? " ;
			
			ps1 = conn.prepareStatement(sSql);
			ps1.setString(1, status);
			ps1.setInt(2, docOrigRecord.getId());
			ps1.execute();
		}
		finally {
			SqlUtil.closeInstance(ps1);
		}
	}
	
	/**
	 * Check whether this document has linked award.
	 * If the document type is not S, <code>false</code> is returned.
	 * 
	 * @param documentId the document identifier
	 * @param conn the jdbc connection
	 * @return <code>true</code> is returned when the document type is S and has linked award created.
	 * Otherwise <code>false</code> is returned.
	 * 
	 * @throws SQLException fatal error when check the document
	 */
	public static boolean hasLinkedAward(int documentId, Connection conn) throws SQLException
	{
		DocumentRecord docRecord = DocumentTable.get(null, documentId, conn);
		return hasLinkedAward(docRecord, conn);
		
	}
	
	/**
	 * Check whether this document has linked award.
	 * If the document type is not S, <code>false</code> is returned.
	 * 
	 * @param docRecord the document 
	 * @param conn the jdbc connection
	 * @return <code>true</code> is returned when the document type is S and has linked award created.
	 * Otherwise <code>false</code> is returned.
	 * 
	 * @throws SQLException fatal error when check the document
	 */
	public static boolean hasLinkedAward(DocumentRecord docRecord, Connection conn) throws SQLException
	{
		
		if (CommonUtils.isNotSame(DocTypeRefRecord.DOC_TYPE_CD_SOLICITATION, docRecord.getDocumentType()) )
		{
			return false;
		}
		boolean hasLinkedAward = false;
		String sSql = "SELECT COUNT(*) FROM " + DocumentRecord.TABLE_DOCUMENTS + " A "				
				+ " WHERE A." + DocumentRecord.FIELD_SOLICITATION_ID + " = ?";
		PreparedStatement ps1 = null;
		ResultSet rs1 = null;
		try
		{
			ps1 = conn.prepareStatement(sSql);
			ps1.setInt(1, docRecord.getId());
			rs1 = ps1.executeQuery();
			
			if (rs1.next())
			{
				hasLinkedAward = (rs1.getInt(1) > 0);
			}
		
		}
		finally
		{
			SqlUtil.closeInstance(rs1, ps1);
		}
		return hasLinkedAward;
		
	}
	
}
