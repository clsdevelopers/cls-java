package gov.dod.cls.db;

import gov.dod.cls.config.ClsConfig;
import gov.dod.cls.utils.sql.SqlUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.node.ArrayNode;

public class QuestionGroupRefTable extends ArrayList<QuestionGroupRefRecord> {
	
	private static final long serialVersionUID = -5207028406923589360L;

	// ========================================================
	private volatile static QuestionGroupRefTable instance = null;
	private volatile static Date refreshed = null;
	
	public synchronized static QuestionGroupRefTable getInstance(Connection conn) {
		if (QuestionGroupRefTable.instance == null) {
			QuestionGroupRefTable.instance = new QuestionGroupRefTable();
			QuestionGroupRefTable.instance.refresh(conn);
		} else {
			QuestionGroupRefTable.instance.refreshWhenNeeded(conn);
		}
		return QuestionGroupRefTable.instance;
	}
	
	public static ArrayNode getJson(ObjectMapper mapper, boolean forInterview) {
		QuestionGroupRefTable clauseSectionTable = QuestionGroupRefTable.getInstance(null);
		return clauseSectionTable.toJson(mapper, forInterview);
	}

	// Functions -----------------------------------------------------------------
	public static Date getReferHistoryDate(Connection conn) {
		return SysLastTableChangeAtTable.getLastUpdateDate(conn, QuestionGroupRefRecord.TABLE_QUESTION_GROUP_REF);
	}
	  
	public static boolean needToRefreh(Connection conn) {
		return SysLastTableChangeAtTable.needToLoad(QuestionGroupRefTable.refreshed, getReferHistoryDate(conn));
	}
	
	// ========================================================
	private Exception lastError;

	public void refreshWhenNeeded(Connection conn) {
		if (QuestionGroupRefTable.needToRefreh(conn))
			this.refresh(conn);
	}

	private boolean refresh(Connection conn) {
		boolean result = false;
		String sSql = QuestionGroupRefRecord.SQL_SELECT;
		boolean bCreateConnection = (conn == null);
		PreparedStatement ps1 = null;
		ResultSet rs1 = null;
		try {
			if (bCreateConnection)
				conn = ClsConfig.getDbConnection();
			ps1 = conn.prepareStatement(sSql);
			rs1 = ps1.executeQuery();
			this.clear();
			while (rs1.next()) {
				QuestionGroupRefRecord clauseSectionRecord = new QuestionGroupRefRecord();
				clauseSectionRecord.read(rs1);
				this.add(clauseSectionRecord);
			}
			QuestionGroupRefTable.refreshed = getReferHistoryDate(conn); // CommonUtils.getNow();
			result = true;
			System.out.println("QuestionGroupRefTable refreshed");
		}
		catch (Exception oError) {
			this.lastError = oError;
			oError.printStackTrace();
		}
		finally {
			SqlUtil.closeInstance(rs1, ps1);
			if (bCreateConnection)
				SqlUtil.closeInstance(conn);
		}
		return result;
	}
	
	public Date getRefreshed() {
		return QuestionGroupRefTable.refreshed;
	}
	
	public QuestionGroupRefRecord findByCode(String code) {
		for (QuestionGroupRefRecord clauseSectionRecord : this) {
			if (clauseSectionRecord.getCode().equals(code))
				return clauseSectionRecord;
		}
		return null;
	}
	
	public String getNameByCode(String code) {
		QuestionGroupRefRecord oRecord = this.findByCode(code);
		if (oRecord == null)
			return null;
		else
			return oRecord.getName();
	}

	public ArrayNode toJson(ObjectMapper mapper, boolean forInterview) {
		if (mapper == null)
			mapper = new ObjectMapper();
		ArrayNode json = mapper.createArrayNode();
		for (QuestionGroupRefRecord clauseSectionRecord : this) {
			json.add(clauseSectionRecord.toJson(forInterview));
		}
		return json;
	}

	public Exception getLastError() {
		return lastError;
	}
	
}
