package gov.dod.cls.db;

import gov.dod.cls.config.ClsConfig;
import gov.dod.cls.model.JsonAble;
import gov.dod.cls.utils.CommonUtils;
import gov.dod.cls.utils.sql.SqlUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.node.ArrayNode;
import org.codehaus.jackson.node.ObjectNode;

public class PrescriptionRecord extends JsonAble {

	public static final String TABLE_PRESCRIPTIONS = "Prescriptions";
	
	public static final String FIELD_PRESCRIPTION_ID = "prescription_id";
	public static final String FIELD_PRESCRIPTION_NAME = "prescription_name";
	public static final String FIELD_PRESCRIPTION_URL = "prescription_url";
	public static final String FIELD_PRESCRIPTION_CONTENT = "prescription_content"; 
	public static final String FIELD_REGULATION_ID = "regulation_id"; 
	public static final String FIELD_CREATED_AT = "created_at";
	public static final String FIELD_UPDATED_AT = "updated_at";
	
	public static final String SQL_SELECT
		= "SELECT " + FIELD_PRESCRIPTION_ID
		+ ", " + FIELD_PRESCRIPTION_NAME
		+ ", " + FIELD_REGULATION_ID
		+ ", " + FIELD_PRESCRIPTION_URL
		+ ", " + FIELD_PRESCRIPTION_CONTENT
		+ ", " + FIELD_CREATED_AT
		+ ", " + FIELD_UPDATED_AT
		+ " FROM " + TABLE_PRESCRIPTIONS
		+ " ORDER BY " + FIELD_PRESCRIPTION_NAME; 
	
	public static final String SQL_SELECT_DETAIL
		= "SELECT A." + FIELD_PRESCRIPTION_ID
		+ ", A." + FIELD_PRESCRIPTION_NAME
		+ ", A." + FIELD_REGULATION_ID
		+ ", A." + FIELD_PRESCRIPTION_URL
		+ ", A." + FIELD_PRESCRIPTION_CONTENT
		+ ", B." + RegulationRecord.FIELD_REGULATION_NAME
		+ ", A." + FIELD_CREATED_AT
		+ ", A." + FIELD_UPDATED_AT
		+ " FROM " + TABLE_PRESCRIPTIONS + " A"
		+ " INNER JOIN " + RegulationRecord.TABLE_REGULATIONS + " B"
		+ " ON (B." + RegulationRecord.FIELD_REGULATION_ID + " = A." + FIELD_REGULATION_ID + ") ";
	
	public static final String SQL_CLAUSES_SELECT
		= "SELECT A." + ClausePrescriptionRecord.FIELD_CLAUSE_ID 
		+ ", B." + ClauseRecord.FIELD_CLAUSE_NAME
		+ ", B." + ClauseRecord.FIELD_CLAUSE_TITLE
		+ " FROM " + ClausePrescriptionRecord.TABLE_CLAUSE_PRESCRIPTIONS + " A"
		+ " INNER JOIN " + ClauseRecord.TABLE_CLAUSES + " B"
		+ " ON (B." + ClauseRecord.FIELD_CLAUSE_ID + " = A." + ClausePrescriptionRecord.FIELD_CLAUSE_ID + ") "
		+ " WHERE A." + ClausePrescriptionRecord.FIELD_PRESCRIPTION_ID + " = ?"
		+ " ORDER BY B." + ClauseRecord.FIELD_CLAUSE_NAME;
	
	public static final String SQL_QUESTIONS_SELECT
		= "SELECT C." + QuestionRecord.FIELD_QUESTION_ID 
		+ ", C." + QuestionRecord.FIELD_QUESTION_CODE		
		+ ", C." + QuestionRecord.FIELD_QUESTION_TEXT
		+ ", B." + QuestionChoiceRecord.FIELD_CHOICE_TEXT
		+ " FROM " + QuestionChoicePrescriptionRecord.TABLE_QUESTION_CHOICE_PRESCRIPTIONS + " A"
		+ " INNER JOIN " + QuestionChoiceRecord.TABLE_QUESTION_CHOICES + " B"
		+ " ON (B." + QuestionChoiceRecord.FIELD_QUESTION_CHOCE_ID + " = A." + QuestionChoicePrescriptionRecord.FIELD_QUESTION_CHOCE_ID + ") "
		+ " INNER JOIN " + QuestionRecord.TABLE_QUESTIONS + " C"
		+ " ON (C." + QuestionRecord.FIELD_QUESTION_ID + " = B." + QuestionChoiceRecord.FIELD_QUESTION_ID + ") "		
		+ " WHERE A." + QuestionChoicePrescriptionRecord.FIELD_PRESCRIPTION_ID + " = ?"
		+ " ORDER BY C." + QuestionRecord.FIELD_QUESTION_CODE;
	
	// ================================================================
	private Integer id;
	private String name;
	private Integer regulationId;
	private String url;
	private String content;
	private Date createdAt;
	private Date updatedAt;
	
	private String regulationName;
	private static ArrayNode arrayNodeClauses;
	private static ArrayNode arrayNodeQuestions;
	
	public void read(ResultSet rs) throws SQLException {
		this.id = rs.getInt(FIELD_PRESCRIPTION_ID); 
		this.name = rs.getString(FIELD_PRESCRIPTION_NAME);
		this.regulationId = CommonUtils.toInteger(rs.getObject(FIELD_REGULATION_ID));
		this.url = rs.getString(FIELD_PRESCRIPTION_URL);
		this.content = rs.getString(FIELD_PRESCRIPTION_CONTENT);
		this.createdAt = CommonUtils.toDate(rs.getObject(FIELD_CREATED_AT));
		this.updatedAt = CommonUtils.toDate(rs.getObject(FIELD_UPDATED_AT));
	}
	
	public void readDetail(ResultSet rs) throws SQLException {
		this.id = rs.getInt(FIELD_PRESCRIPTION_ID); 
		this.name = rs.getString(FIELD_PRESCRIPTION_NAME);
		this.regulationId = CommonUtils.toInteger(rs.getObject(FIELD_REGULATION_ID));
		this.url = rs.getString(FIELD_PRESCRIPTION_URL);
		this.content = rs.getString(FIELD_PRESCRIPTION_CONTENT);
		this.createdAt = CommonUtils.toDate(rs.getObject(FIELD_CREATED_AT));
		this.updatedAt = CommonUtils.toDate(rs.getObject(FIELD_UPDATED_AT));
		
		this.regulationName = rs.getString(RegulationRecord.FIELD_REGULATION_NAME);
	}
	
	@Override
	protected void populateJson(ObjectNode json, boolean forInterview) {
		json.put(FIELD_PRESCRIPTION_ID, this.id);
		json.put(FIELD_PRESCRIPTION_NAME, this.name);
		json.put(FIELD_REGULATION_ID, this.regulationId);
		json.put(FIELD_PRESCRIPTION_URL, this.url);
		json.put(FIELD_PRESCRIPTION_CONTENT, this.content);
		if (!forInterview) {
			json.put(FIELD_CREATED_AT, CommonUtils.toJsonDate(this.createdAt));
			json.put(FIELD_UPDATED_AT, CommonUtils.toJsonDate(this.updatedAt));
			json.put(RegulationRecord.FIELD_REGULATION_NAME, this.regulationName);
			json.put(ClauseRecord.TABLE_CLAUSES, arrayNodeClauses);
			json.put(QuestionRecord.TABLE_QUESTIONS, arrayNodeQuestions);
		}
	};

	@Override
	protected void populateJson(ObjectNode json, ObjectMapper mapper) {
		this.populateJson(json, false);
	};
	
	public static PrescriptionRecord getRecord(Connection conn, Integer prescriptionId, PrescriptionRecord pPrescriptionRecord) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			String sSql = SQL_SELECT_DETAIL;
			String operator = " WHERE ";
			if (prescriptionId != null) {
				// sSql += operator + " " + FIELD_QUESTION_ID + "=?";
				sSql += operator + " A." + FIELD_PRESCRIPTION_ID + "=?";
				// operator = " AND ";
			}
			ps = conn.prepareStatement(sSql);
			int iParam = 1;
			if (prescriptionId != null)
				ps.setInt(iParam++, prescriptionId);
			rs = ps.executeQuery();
			if (rs.next()) {
				if (pPrescriptionRecord == null)
					pPrescriptionRecord = new PrescriptionRecord();
				pPrescriptionRecord.readDetail(rs);
			}
			//---------------
			SqlUtil.closeInstance(rs, ps);
			rs = null;
			ps = null;
			ps = conn.prepareStatement(SQL_CLAUSES_SELECT);
			ObjectMapper mapper = new ObjectMapper();
			ObjectNode json = mapper.createObjectNode();
			arrayNodeClauses = json.arrayNode();
			ps.setInt(1, prescriptionId);
			rs = ps.executeQuery();
			while (rs.next()) {
				ObjectNode elementNode = json.objectNode();
				loadClause(rs, elementNode);
				arrayNodeClauses.add(elementNode);				
			}
			SqlUtil.closeInstance(rs, ps);
			//---------------
			rs = null;
			ps = null;
			ps = conn.prepareStatement(SQL_QUESTIONS_SELECT);
			mapper = new ObjectMapper();
			json = mapper.createObjectNode();
			arrayNodeQuestions = json.arrayNode();
			ps.setInt(1, prescriptionId);
			rs = ps.executeQuery();
			while (rs.next()) {
				ObjectNode elementNode = json.objectNode();
				loadQuestion(rs, elementNode);
				arrayNodeQuestions.add(elementNode);				
			}
			
		}
		catch (Exception oError) {
			oError.printStackTrace();
		}
		finally {
			SqlUtil.closeInstance(rs, ps);
		}
		return pPrescriptionRecord;
	
	}
	
	private static void loadClause(ResultSet rs, ObjectNode elementNode) throws SQLException {	
		elementNode.put(ClausePrescriptionRecord.FIELD_CLAUSE_ID , rs.getInt(ClausePrescriptionRecord.FIELD_CLAUSE_ID ));
		elementNode.put(ClauseRecord.FIELD_CLAUSE_NAME, rs.getString(ClauseRecord.FIELD_CLAUSE_NAME));
		elementNode.put(ClauseRecord.FIELD_CLAUSE_TITLE, rs.getString(ClauseRecord.FIELD_CLAUSE_TITLE));
	}
	
	private static void loadQuestion(ResultSet rs, ObjectNode elementNode) throws SQLException {	
		elementNode.put(QuestionRecord.FIELD_QUESTION_ID , rs.getInt(QuestionRecord.FIELD_QUESTION_ID ));
		elementNode.put(QuestionRecord.FIELD_QUESTION_CODE, rs.getString(QuestionRecord.FIELD_QUESTION_CODE));
		elementNode.put(QuestionRecord.FIELD_QUESTION_TEXT, rs.getString(QuestionRecord.FIELD_QUESTION_TEXT));
		elementNode.put(QuestionChoiceRecord.FIELD_CHOICE_TEXT, rs.getString(QuestionChoiceRecord.FIELD_CHOICE_TEXT));
	}	
		
	public static PrescriptionRecord getRecord(Integer id) {
		if (id == null) 
			return null;
		
		Connection conn = null;
		try {
			conn = ClsConfig.getDbConnection();
			return PrescriptionRecord.getRecord(conn, id, null);
		}
		catch (Exception oError) {
			oError.printStackTrace();
		}
		finally {
			SqlUtil.closeInstance(conn);
		}
		return null;
	}	
	
	// ----------------------------------------------------------------
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Integer getRegulationId() {
		return regulationId;
	}
	public boolean isSameRegulationId(int regulationId) {
		if (this.regulationId == null)
			return false;
		return (this.regulationId.intValue() == regulationId);
	}
	public void setRegulationId(Integer regulationId) {
		this.regulationId = regulationId;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public Date getCreatedAt() {
		return createdAt;
	}
	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}
	public Date getUpdatedAt() {
		return updatedAt;
	}
	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}
	public Integer getId() {
		return id;
	}

}
