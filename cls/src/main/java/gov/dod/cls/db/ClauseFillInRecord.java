package gov.dod.cls.db;

import gov.dod.cls.model.JsonAble;
import gov.dod.cls.utils.CommonUtils;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import org.codehaus.jackson.node.ObjectNode;

public class ClauseFillInRecord extends JsonAble {

	public static final String TABLE_CLAUSE_FILL_INS = "Clause_Fill_Ins";
	
	public static final String FIELD_CLAUSE_FILL_IN_ID = "clause_fill_in_id";
	public static final String FIELD_CLAUSE_ID = "clause_id";
	public static final String FIELD_FILL_IN_CODE = "fill_in_code";
	public static final String FIELD_FILL_IN_TYPE = "fill_in_type";
	public static final String FIELD_FILL_IN_MAX_SIZE = "fill_in_max_size";
	public static final String FIELD_FILL_IN_GROUP_NUMBER = "fill_in_group_number";

	public static final String FIELD_FILL_IN_PLACEHOLDER = "fill_in_placeholder";
	public static final String FIELD_FILL_IN_DEFAULT_DATA = "fill_in_default_data";
	public static final String FIELD_FILL_IN_DISPLAY_ROWS = "fill_in_display_rows";
	public static final String FIELD_FILL_IN_FOR_TABLE = "fill_in_for_table";
	public static final String FIELD_FILL_IN_HEADING = "fill_in_heading";
	
	public static final String FIELD_CREATED_AT = "created_at"; 
	public static final String FIELD_UPDATED_AT = "updated_at"; 

	public static final String SQL_SELECT
		= "SELECT A." + FIELD_CLAUSE_FILL_IN_ID
		+ ", A." + FIELD_CLAUSE_ID
		+ ", A." + FIELD_FILL_IN_CODE
		+ ", A." + FIELD_FILL_IN_TYPE
		+ ", A." + FIELD_FILL_IN_MAX_SIZE
		+ ", A." + FIELD_FILL_IN_GROUP_NUMBER

		+ ", A." + FIELD_FILL_IN_PLACEHOLDER
		+ ", A." + FIELD_FILL_IN_DEFAULT_DATA
		+ ", A." + FIELD_FILL_IN_DISPLAY_ROWS
		+ ", A." + FIELD_FILL_IN_FOR_TABLE
		+ ", A." + FIELD_FILL_IN_HEADING

		+ ", A." + FIELD_CREATED_AT
		+ ", A." + FIELD_UPDATED_AT
		+ " FROM " + TABLE_CLAUSE_FILL_INS + " A";

	// CJ-1249
	public static final String EMPTY_DATA = "_____";
	public static final String SELECTED_DATA = "X";
	
	public static final String HTML_DATA_BEGIN = "<span class='spanData'>";
	public static final String HTML_DATA_END = "</span>";
	
	public static final String HTML_EMPTY_DATA = HTML_DATA_BEGIN + EMPTY_DATA + HTML_DATA_END;
	public static final String HTML_SELECTED_DATA = HTML_DATA_BEGIN + SELECTED_DATA + HTML_DATA_END;

	public static final String CHECK_HTML_EMPTY_DATA = "[" + HTML_EMPTY_DATA + "]";
	public static final String CHECK_HTML_SELECTED_DATA = "[" + HTML_SELECTED_DATA + "]";

	public static final String RADIO_HTML_EMPTY_DATA = "(" + HTML_EMPTY_DATA + ")";
	public static final String RADIO_HTML_SELECTED_DATA = "(" + HTML_SELECTED_DATA + ")";
	
	// ----------------------------------------------------------------
	private Integer id;
	private Integer clauseId;
	private String fillInCode;
	private String fillInType;
	private Integer fillInMaxSize;
	private Integer fillInGroupNumber;
	
	private String fillInPlaceholder;
	private String fillInDefaultData;
	private Integer fillInDisplayRows;
	private boolean fillInForTable;
	private String fillInHeading;
	
	private Date createdAt;
	private Date updatedAt;

	public void read(ResultSet rs) throws SQLException {
		this.id = rs.getInt(FIELD_CLAUSE_FILL_IN_ID); 
		this.clauseId = CommonUtils.toInteger(rs.getObject(FIELD_CLAUSE_ID));
		this.fillInCode = rs.getString(FIELD_FILL_IN_CODE);
		this.fillInType = rs.getString(FIELD_FILL_IN_TYPE);
		this.fillInMaxSize = CommonUtils.toInteger(rs.getObject(FIELD_FILL_IN_MAX_SIZE));
		this.fillInGroupNumber = CommonUtils.toInteger(rs.getObject(FIELD_FILL_IN_GROUP_NUMBER));

		this.fillInPlaceholder = rs.getString(FIELD_FILL_IN_PLACEHOLDER);
		this.fillInDefaultData = rs.getString(FIELD_FILL_IN_DEFAULT_DATA);
		this.fillInDisplayRows = CommonUtils.toInteger(rs.getObject(FIELD_FILL_IN_DISPLAY_ROWS));
		this.fillInForTable = rs.getBoolean(FIELD_FILL_IN_FOR_TABLE);
		this.fillInHeading = rs.getString(FIELD_FILL_IN_HEADING);
		
		this.createdAt = rs.getTimestamp(FIELD_CREATED_AT);
		this.updatedAt = rs.getTimestamp(FIELD_UPDATED_AT); 
	}

	@Override
	protected void populateJson(ObjectNode json, boolean forInterview) {
		json.put(FIELD_CLAUSE_FILL_IN_ID, this.id);
		json.put(FIELD_CLAUSE_ID, this.clauseId);
		json.put(FIELD_FILL_IN_CODE, this.fillInCode);
		json.put(FIELD_FILL_IN_TYPE, this.fillInType);
		json.put(FIELD_FILL_IN_MAX_SIZE, this.fillInMaxSize);
		json.put(FIELD_FILL_IN_GROUP_NUMBER, this.fillInGroupNumber);
		json.put(FIELD_FILL_IN_PLACEHOLDER, this.fillInPlaceholder);
		json.put(FIELD_FILL_IN_DEFAULT_DATA, this.fillInDefaultData);
		json.put(FIELD_FILL_IN_DISPLAY_ROWS, this.fillInDisplayRows);
		json.put(FIELD_FILL_IN_FOR_TABLE, this.fillInForTable);
		json.put(FIELD_FILL_IN_HEADING, this.fillInHeading);
		if (!forInterview) {
			json.put(FIELD_CREATED_AT, CommonUtils.toJsonDate(this.createdAt));
			json.put(FIELD_UPDATED_AT, CommonUtils.toJsonDate(this.updatedAt));
		}
	}
	
	public void populateJsonForApi(ObjectNode json, DocumentFillInsTable documentFillInsTable) {
		json.put(FIELD_CLAUSE_FILL_IN_ID, this.id);
		json.put(FIELD_FILL_IN_CODE, this.fillInCode);
		json.put(FIELD_FILL_IN_TYPE, this.fillInType);
		json.put(FIELD_CREATED_AT, CommonUtils.toZulu(this.createdAt));
		json.put(FIELD_UPDATED_AT, CommonUtils.toZulu(this.updatedAt));
		if (documentFillInsTable != null) {
			DocumentFillInsRecord oDocumentFillInsRecord = documentFillInsTable.findByClauseFillInId(this.id.intValue());
			if (oDocumentFillInsRecord != null) {
				ObjectNode childNode = json.objectNode();
				oDocumentFillInsRecord.populateJsonForApi(childNode);
				json.put(DocumentFillInsRecord.TABLE_DOCUMENT_FILL_INS.toLowerCase(), childNode);
			}
		}
	}
	
	public String getInputValue(String answer, String sPlaceholderText) {

		boolean isEmptyAnswer = ((answer == null) || answer.isEmpty());

		// CJ-1304. include placeholder for missing answers.
		String sPlaceholder = HTML_DATA_BEGIN + EMPTY_DATA + HTML_DATA_END;
		String sGroupPlaceholder = "";
		if ((answer == null) && (sPlaceholderText != null)) {
			sPlaceholder = HTML_DATA_BEGIN + EMPTY_DATA + " [" + sPlaceholderText + "]"+ HTML_DATA_END;
			sGroupPlaceholder =  " <b>[" + sPlaceholderText + "]</b> ";
			
			if ((FillInTypeRefRecord.TYPE_CD_CHECKBOX.equals(this.fillInType)) ||
			(FillInTypeRefRecord.TYPE_CD_RADIO.equals(this.fillInType))) {
				if ((this.fillInDisplayRows != null) && (this.fillInDisplayRows  == 1))
					sGroupPlaceholder =  "</p><p><b>[" + sPlaceholderText + "]</b></p><p>";
			}
		}
		
		if (FillInTypeRefRecord.TYPE_CD_CHECKBOX.equals(this.fillInType)) { // "C"
			if (isEmptyAnswer)
				return sGroupPlaceholder + CHECK_HTML_EMPTY_DATA; // "[<span class='spanData'>_____</span>]";
			return CHECK_HTML_SELECTED_DATA; // "[<span class='spanData'>X</span>]";
		}
		if (FillInTypeRefRecord.TYPE_CD_RADIO.equals(this.fillInType)) { // "R"
			if (isEmptyAnswer)
				return sGroupPlaceholder + RADIO_HTML_EMPTY_DATA; // "(<span class='spanData'>_____</span>)";
			return RADIO_HTML_SELECTED_DATA; // "(<span class='spanData'>X</span>)";
		}
		if (FillInTypeRefRecord.TYPE_CD_MEMO.equals(this.fillInType)) { // "M"
			if (isEmptyAnswer) {
				// CJ-533
				if (!CommonUtils.isEmpty(this.fillInDefaultData))
					return this.fillInDefaultData;
			} else
				return answer;
		}
		
		// CJ-1304
		if (isEmptyAnswer)
			return sPlaceholder;
		else
			return HTML_DATA_BEGIN + answer + HTML_DATA_END; // "<span class='spanData'>" + (isEmptyAnswer ? "_____" : answer) + "</span>";

		
		//return HTML_DATA_BEGIN + (isEmptyAnswer ? EMPTY_DATA : answer) + HTML_DATA_END; // "<span class='spanData'>" + (isEmptyAnswer ? "_____" : answer) + "</span>";
	}
	
	// ----------------------------------------------------------------
	public Integer getId() { return id; }

	public Integer getClauseId() {
		return clauseId;
	}

	public void setClauseId(Integer clauseId) {
		this.clauseId = clauseId;
	}

	public String getFillInCode() {
		return fillInCode;
	}

	public void setFillInCode(String fillInCode) {
		this.fillInCode = fillInCode;
	}

	public String getFillInType() {
		return fillInType;
	}

	public void setFillInType(String fillInType) {
		this.fillInType = fillInType;
	}

	public Integer getFillInMaxSize() {
		return fillInMaxSize;
	}

	public void setFillInMaxSize(Integer fillInMaxSize) {
		this.fillInMaxSize = fillInMaxSize;
	}

	public Integer getFillInGroupNumber() {
		return fillInGroupNumber;
	}

	public void setFillInGroupNumber(Integer fillInGroupNumber) {
		this.fillInGroupNumber = fillInGroupNumber;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public Date getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	public String getFillInPlaceholder() {
		return fillInPlaceholder;
	}

	public void setFillInPlaceholder(String fillInPlaceholder) {
		this.fillInPlaceholder = fillInPlaceholder;
	}

	public String getFillInDefaultData() {
		return fillInDefaultData;
	}

	public void setFillInDefaultData(String fillInDefaultData) {
		this.fillInDefaultData = fillInDefaultData;
	}

	public Integer getFillInDisplayRows() {
		return fillInDisplayRows;
	}

	public void setFillInDisplayRows(Integer fillInDisplayRows) {
		this.fillInDisplayRows = fillInDisplayRows;
	}

	public String getFillInHeading() {
		return fillInHeading;
	}

	public void setFillInHeading(String fillInHeading) {
		this.fillInHeading = fillInHeading;
	}

	public boolean isFillInForTable() {
		return fillInForTable;
	}

	public void setFillInForTable(boolean fillInForTable) {
		this.fillInForTable = fillInForTable;
	}

}
