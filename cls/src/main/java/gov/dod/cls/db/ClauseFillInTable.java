package gov.dod.cls.db;

import gov.dod.cls.config.ClsConfig;
import gov.dod.cls.utils.ClsConstants;
import gov.dod.cls.utils.sql.SqlUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.node.ArrayNode;

public class ClauseFillInTable extends ArrayList<ClauseFillInRecord> {
	
	private static final long serialVersionUID = -8730209090424083050L;

	public ArrayNode toJson(ObjectMapper mapper, boolean forInterview) {
		if (mapper == null)
			mapper = new ObjectMapper();
		ArrayNode json = mapper.createArrayNode();
		for (ClauseFillInRecord clauseFillInRecord : this) {
			json.add(clauseFillInRecord.toJson(forInterview));
		}
		return json;
	}
	
	// ========================================================
	private volatile static ClauseFillInTable instance = null;
	private volatile static Date refreshed = null;
	
	public synchronized static ClauseFillInTable getInstance(Connection conn) {
		if (ClauseFillInTable.instance == null) {
			ClauseFillInTable.instance = new ClauseFillInTable();
			ClauseFillInTable.instance.refresh(conn);
		} else {
			ClauseFillInTable.instance.refreshWhenNeeded(conn);
		}
		return ClauseFillInTable.instance;
	}
	
	// Functions -----------------------------------------------------------------
	public static Date getReferHistoryDate(Connection conn) {
		return SysLastTableChangeAtTable.getLastUpdateDate(conn, ClauseFillInRecord.TABLE_CLAUSE_FILL_INS);
	}
	  
	public static boolean needToRefreh(Connection conn) {
		return SysLastTableChangeAtTable.needToLoad(ClauseFillInTable.refreshed, getReferHistoryDate(conn));
	}
	
	// ---------------------------------------------------------------
	private Exception lastError;

	public void refreshWhenNeeded(Connection conn) {
		if (ClauseFillInTable.needToRefreh(conn))
			this.refresh(conn);
	}

	private boolean refresh(Connection conn) {
		boolean result = false;
		String sSql = ClauseFillInRecord.SQL_SELECT
				+ " ORDER BY " + ClauseFillInRecord.FIELD_CLAUSE_ID
				+ ", " + ClauseFillInRecord.FIELD_FILL_IN_CODE;;
		boolean bCreateConnection = (conn == null);
			
		PreparedStatement ps1 = null;
		ResultSet rs1 = null;
		try {
			if (bCreateConnection)
				conn = ClsConfig.getDbConnection();
			ps1 = conn.prepareStatement(sSql);
			rs1 = ps1.executeQuery();
			this.clear();
			while (rs1.next()) {
				ClauseFillInRecord clausePrescriptionRecord = new ClauseFillInRecord();
				clausePrescriptionRecord.read(rs1);
				this.add(clausePrescriptionRecord);
			}
			ClauseFillInTable.refreshed = getReferHistoryDate(conn); // CommonUtils.getNow();
			result = true;
			System.out.println("ClauseFillInTable refreshed");
		}
		catch (Exception oError) {
			this.lastError = oError;
			oError.printStackTrace();
		}
		finally {
			SqlUtil.closeInstance(rs1, ps1);
			if (bCreateConnection)
				SqlUtil.closeInstance(conn);
		}
		return result;
	}
	
	public ClauseFillInTable subsetByClauseId(int clauseId) {
		boolean bFound = false;
		ClauseFillInTable result = new ClauseFillInTable();
		for (ClauseFillInRecord record : this) {
			if (record.getClauseId().intValue() == clauseId) {
				result.add(record);
				bFound = true;
			} else
				if (bFound)
					break;
		}
		return result;
	}
	
	public ClauseFillInRecord findById(int fillInId) {
		for (ClauseFillInRecord record : this) {
			if (record.getId().intValue() == fillInId)
				return record;
		}
		return null;
	}

	public ClauseFillInRecord findByCode(String psFillInCode) {
		for (ClauseFillInRecord record : this) {
			if (record.getFillInCode().equals(psFillInCode))
				return record;
		}
		return null;
	}
	
	public boolean hasSameFillInCodes(ClauseFillInTable otherClauseFillInTable) {
		if (otherClauseFillInTable == null)
			return false;
		if (this.size() != otherClauseFillInTable.size())
			return false;
		
		for (ClauseFillInRecord record : this) {
			if (otherClauseFillInTable.findByCode(record.getFillInCode()) == null)
				return false;
		}
		return true;
	}

	public Exception getLastError() {
		return lastError;
	}
	

	// CJ-1304, see if fillin was answered. If not, return placeholder.
	public String getFillinPlaceholder(DocumentFillInsTable aFillIns, ClauseFillInRecord oSelRecord) {
		
		// If answered, return null placeholder.
		// Otherwise, return placeholder.
		if (oSelRecord.getFillInGroupNumber() == null) {
			DocumentFillInsRecord oDocFillIn = aFillIns.findByClauseFillInId(oSelRecord.getId().intValue());
			if ((oDocFillIn != null) && (oDocFillIn.getAnswer() != null) && (oDocFillIn.getAnswer().length() > 0))
				return null;
			else
				return oSelRecord.getFillInPlaceholder();
		}

		// for group items, placeholders are only relevant for the first item in the group.
		if (oSelRecord.getFillInCode().indexOf("[0]") < 0) 
			return null;
		
		// if the group does not have a placeholder, then return;
		if (oSelRecord.getFillInPlaceholder() == null)
			return null;
		
		// Check if any items in the group were answered.
		boolean bAnswered = false;
		ClauseFillInTable cfiClauses = this.subsetByClauseId (oSelRecord.getClauseId());
		for (ClauseFillInRecord oRecord : cfiClauses) {
			if ((oRecord.getFillInGroupNumber() == null) || (oSelRecord.getFillInGroupNumber() != oRecord.getFillInGroupNumber()))
				continue;
			
			DocumentFillInsRecord oDocFillIn = aFillIns.findByClauseFillInId(oRecord.getId().intValue());
			if ((oDocFillIn != null) && (oDocFillIn.getAnswer() != null) && (oDocFillIn.getAnswer().length() > 0)) {
				bAnswered = true;
				break;
			}
		}

		// if group item was answered
		if (bAnswered)
			return null;
		
		return oSelRecord.getFillInPlaceholder();
	}
	
	public String getFillinPlaceholder (ArrayList<ClauseFillInRecord> aColumns, DocumentFillInsTable aFillIns) {
		
		if (aColumns.size() == 0)
			return null;
		
		ClauseFillInRecord oRecord = aColumns.get(0);
		DocumentFillInsRecord oDocFillIn = aFillIns.findByCode(oRecord.getFillInCode());
		if (oDocFillIn == null)
			return oRecord.getFillInPlaceholder();;
		
		return null;
	}
		
	public String resolveFillIns(String pClauseData, DocumentFillInsTable aFillIns, ClsConstants.OutputType outputType) {
		if (this.isEmpty())
			return pClauseData;
		
		ArrayList<ArrayList<ClauseFillInRecord>> aTables = new ArrayList<ArrayList<ClauseFillInRecord>>();
		for (ClauseFillInRecord oRecord : this) {
			DocumentFillInsRecord oDocFillIn = aFillIns.findByClauseFillInId(oRecord.getId().intValue()); // findById()
			if (oRecord.isFillInForTable()) {
				int iGroup = (oRecord.getFillInGroupNumber() != null ? oRecord.getFillInGroupNumber() : -1);
				ArrayList<ClauseFillInRecord> oTable = null;
				for (ArrayList<ClauseFillInRecord> aColumns : aTables) {
					ClauseFillInRecord oColumnFillIn = aColumns.get(0);
					int iColumnGroup = (oColumnFillIn.getFillInGroupNumber() != null ? oColumnFillIn.getFillInGroupNumber() : -1);
					if (iColumnGroup == iGroup) {
						oTable = aColumns;
						break;
					}
				}
				if (oTable == null) {
					oTable = new ArrayList<ClauseFillInRecord>();
					aTables.add(oTable);
				}
				oTable.add(oRecord);
			} else {
				// CJ-823, Editables show blank lines if partial fillin not saved.
				String sAnswer;
				
				// boolean bAnswered = checkClauseFillInGroupAnswered (aFillIns, oRecord);
				// CJ-1304
				String sPlaceholder = getFillinPlaceholder (aFillIns, oRecord);
				//System.out.println(oRecord.getFillInCode() + " Placeholder? " + (sPlaceholder == null ? "" : sPlaceholder));
				
				if ((oDocFillIn == null) && oRecord.getFillInDefaultData() != null) {
					sAnswer = oRecord.getInputValue(oRecord.getFillInDefaultData(), sPlaceholder);
				}
				else {
					sAnswer = oRecord.getInputValue(((oDocFillIn != null) ? oDocFillIn.getAnswer() : null), sPlaceholder);
				}
				
				// CJ-658, strip trailing whitespace
				if (" ".equals(sAnswer))
					sAnswer = "";
				else if (FillInTypeRefRecord.TYPE_CD_MEMO.equals(oRecord.getFillInType())) {
					switch (outputType) {
					case HTML:
						if (!sAnswer.contains(ClsConstants.HTML_BR))
							sAnswer = sAnswer.replaceAll("\n", ClsConstants.HTML_BR);
						break;
					case PDF:
					case DOC:
						sAnswer = ClsConstants.HTML_BR + sAnswer.replaceAll("\n", ClsConstants.HTML_BR);
						break;
					default: // XML, UI
						break;
					}
				}
				pClauseData = pClauseData.replace("{{" + oRecord.getFillInCode() + "}}", sAnswer);
			}
		}
		
		for (ArrayList<ClauseFillInRecord> aColumns : aTables) {
			String sAnswer = this.genTable(aColumns, aFillIns);
			
			// CJ-1304, add placeholder
			String sPlaceholder = getFillinPlaceholder (aColumns, aFillIns);
			//System.out.println("Table Placeholder? " + (sPlaceholder == null ? "" : sPlaceholder));
			if (sPlaceholder != null)
				sAnswer = "<br/><b>[" + sPlaceholder + "]</b><br/>" + sAnswer;
			
			for (int iItem = 0; iItem < aColumns.size(); iItem++) {
				ClauseFillInRecord oRecord = aColumns.get(iItem);
				pClauseData = pClauseData.replace("{{" + oRecord.getFillInCode() + "}}", (iItem == 0) ? sAnswer : "");
			}
		}
		
		/*
		ArrayList<ClauseFillInRecord> aColumns = new ArrayList<ClauseFillInRecord>();
		String sFirstTableColumnName = null;
		for (ClauseFillInRecord oRecord : this) {
			DocumentFillInsRecord oDocFillIn = aFillIns.findByClauseFillInId(oRecord.getId().intValue()); // findById()
			if (oRecord.isFillInForTable()) {
				if (sFirstTableColumnName == null)
					sFirstTableColumnName = oRecord.getFillInCode(); 
				aColumns.add(oRecord);
			} else {
				String sAnswer;
				if (sFirstTableColumnName != null) {
					sAnswer = this.genTable(aColumns, aFillIns);
					pClauseData = pClauseData.replace("{{" + sFirstTableColumnName + "}}", sAnswer);
					sFirstTableColumnName = null;
					aColumns.clear();
				}
				sAnswer = oRecord.getInputValue((oDocFillIn != null) ? oDocFillIn.getAnswer() : null);
				pClauseData = pClauseData.replace("{{" + oRecord.getFillInCode() + "}}", sAnswer);
			}
		}
		if (sFirstTableColumnName != null) {
			String sAnswer = this.genTable(aColumns, aFillIns);
			pClauseData = pClauseData.replace("{{" + sFirstTableColumnName + "}}", sAnswer);
		}
		*/
		return pClauseData;
	}

	private final static String DIVIDER = "\t";

	private String genTable(ArrayList<ClauseFillInRecord> aColumns, DocumentFillInsTable aFillIns) {
		ClauseFillInRecord baseFillIn = aColumns.get(0);
		ArrayList<String[]> aValues = new ArrayList<String[]>(); // Array(aColumns.length);
		int iPrevRows = 0;
		String sHeaderTr = "<thead><tr>";
		for (int item = 0; item < aColumns.size(); item++) {
			ClauseFillInRecord oFillIn = aColumns.get(item);
			sHeaderTr += "<th>" + (oFillIn.getFillInHeading() != null ? oFillIn.getFillInHeading()  : "") + "</th>";
			DocumentFillInsRecord oDocFillIn = aFillIns.findByClauseFillInId(oFillIn.getId().intValue()); // findById()
			String value = (oDocFillIn == null) ? null : oDocFillIn.getAnswer();
			if (value == null) {
				aValues.add(null);
			} else {
				String[] aAnswers = value.split(DIVIDER);
				aValues.add(aAnswers);
				if (iPrevRows < aAnswers.length)
					iPrevRows = aAnswers.length;
			}
		}
		sHeaderTr += "</tr></thead>";
		int rows = (baseFillIn.getFillInDisplayRows() != null ? baseFillIn.getFillInDisplayRows().intValue() : 5);
		if (iPrevRows > rows)
			rows = iPrevRows; 

		String html = "<table class='tableEditClause'>" + sHeaderTr + "<tbody>";
		for (int iRow = 0; iRow < rows; iRow++) {
			html += "<tr>";
			for (int item = 0; item < aColumns.size(); item++) {
				ClauseFillInRecord oFillIn = aColumns.get(item);
				String sValue = null;
				String[] aAnswers = aValues.get(item);
				if (aAnswers != null) {
					if (aAnswers.length > iRow)
						sValue = aAnswers[iRow];
				}
				String sEditor = oFillIn.getInputValue(sValue, null);
				html += "<td>" + sEditor + "</td>";
			}
			html += "</tr>";
		}
		html += "</tbody></table>";
		
		return html;
	}
}
