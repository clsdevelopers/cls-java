package gov.dod.cls.db;

import gov.dod.cls.model.JsonAble;
import gov.dod.cls.utils.CommonUtils;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.node.ArrayNode;
import org.codehaus.jackson.node.ObjectNode;

public class DocumentAnswerRecord extends JsonAble {

	public static final String TABLE_DOCUMENT_ANSWERS = "Document_Answers";
	
	public static final String FIELD_DOCUMENT_ANSWER_ID = "document_answer_id"; 
	public static final String FIELD_DOCUMENT_ID = "document_id"; 
	public static final String FIELD_QUESTION_ID = "question_id"; 
	public static final String FIELD_QUESTION_ANSWER = "question_answer";
	public static final String FIELD_CREATED_AT = "created_at";
	public static final String FIELD_UPDATED_AT = "updated_at";
	
	public static final String SQL_SELECT
		= "SELECT " + FIELD_DOCUMENT_ANSWER_ID
		+ ", " + FIELD_DOCUMENT_ID
		+ ", " + FIELD_QUESTION_ID
		+ ", " + FIELD_QUESTION_ANSWER
		+ ", " + FIELD_CREATED_AT
		+ ", " + FIELD_UPDATED_AT
		+ " FROM " + TABLE_DOCUMENT_ANSWERS;

	private Integer id; 
	private Integer documentId; 
	private Integer questionId; 
	private String answer; 
	private Date createdAt; 
	private Date updatedAt;
	
	public void read(ResultSet rs) throws SQLException {
		this.id = rs.getInt(FIELD_DOCUMENT_ANSWER_ID); 
		this.documentId = CommonUtils.toInteger(rs.getObject(FIELD_DOCUMENT_ID));
		this.questionId = CommonUtils.toInteger(rs.getObject(FIELD_QUESTION_ID));
		this.answer = rs.getString(FIELD_QUESTION_ANSWER); 
		this.createdAt = CommonUtils.toDateTime(rs, FIELD_CREATED_AT);
		this.updatedAt = CommonUtils.toDateTime(rs, FIELD_UPDATED_AT);
	}
	
	protected void fillAnswerForApi(ObjectNode json) {
		ArrayNode arrayNode = json.arrayNode();
		if (this.answer != null) {
			String[] aAnswers = this.answer.split("\t");
			if (aAnswers.length > 1) {
				for (String value : aAnswers)
					arrayNode.add(value);
			} else {
				arrayNode.add(this.answer);
			}
		}
		json.put(FIELD_QUESTION_ANSWER, arrayNode);
	}
	
	@Override
	protected void populateJson(ObjectNode json, boolean forInterview) {
		json.put(FIELD_DOCUMENT_ANSWER_ID, this.id);
		json.put(FIELD_QUESTION_ID, this.questionId);
		if (forInterview) {
			json.put(FIELD_QUESTION_ANSWER, this.answer);
		} else {
			this.fillAnswerForApi(json);
			/*
			if (this.answer == null) {
				json.put(FIELD_QUESTION_ANSWER, this.answer);
			} else {
				String[] aAnswers = this.answer.split("\t");
				if (aAnswers.length > 1) {
					ArrayNode arrayNode = json.arrayNode();
					for (String value : aAnswers)
						arrayNode.add(value);
					json.put(FIELD_QUESTION_ANSWER, arrayNode);
				} else {
					json.put(FIELD_QUESTION_ANSWER, this.answer);
				}
			}
			*/
			/*
			json.put(FIELD_DOCUMENT_ID, this.documentId);
			json.put(FIELD_CREATED_AT, CommonUtils.toJsonDate(this.createdAt));
			json.put(FIELD_UPDATED_AT, CommonUtils.toJsonDate(this.updatedAt));
			*/
		}
	}
	
	public void populateJsonForApi(ObjectNode json, boolean bForVer1, QuestionTable questionTable) {
		if (bForVer1) {
			json.put("id", this.id);
			// json.put("answer", this.answer);
		} else {
			json.put(FIELD_DOCUMENT_ANSWER_ID, this.id);
			json.put(FIELD_QUESTION_ID, this.questionId);
			json.put(FIELD_CREATED_AT, CommonUtils.toZulu(this.createdAt));
			json.put(FIELD_UPDATED_AT, CommonUtils.toZulu(this.updatedAt));
		}
		this.fillAnswerForApi(json);
		/*
		if (this.answer == null) {
			json.put("answer", this.answer);
		} else {
			String[] aAnswers = this.answer.split("\t");
			if (aAnswers.length > 1) {
				ArrayNode arrayNode = json.arrayNode();
				for (String value : aAnswers)
					arrayNode.add(value);
				json.put("answer", arrayNode);
			} else {
				json.put("answer", this.answer);
			}
		}
		*/
		
		QuestionRecord questionRecord = questionTable.findById(this.questionId);
		if (questionRecord != null) {
			ObjectNode questionJson = json.objectNode();
			questionRecord.populateJsonForApi(bForVer1, questionJson);
			json.put((bForVer1 ? "question" : QuestionRecord.TABLE_QUESTIONS.toLowerCase()), questionJson);
			/* removed for CJ-631
			if (bForVer1) {
				ArrayNode clauseNode = json.arrayNode();
				json.put("clauses", clauseNode);
			}
			*/
		}
	}
	
	public void fromJson(JsonNode pJsonNode) {
		this.id = CommonUtils.asInteger(pJsonNode, FIELD_DOCUMENT_ANSWER_ID);
		this.questionId = CommonUtils.asInteger(pJsonNode, FIELD_QUESTION_ID);
		this.answer = CommonUtils.asString(pJsonNode, FIELD_QUESTION_ANSWER); 
	}

	// ---------------------------------------------------------------------
	public Integer getDocumentId() {
		return documentId;
	}
	public void setDocumentId(Integer documentId) {
		this.documentId = documentId;
	}

	public Integer getQuestionId() {
		return questionId;
	}
	public void setQuestionId(Integer questionId) {
		this.questionId = questionId;
	}

	public String getAnswer() {
		return answer;
	}
	public void setAnswer(String answer) {
		this.answer = answer;
	}

	public Date getCreatedAt() {
		return createdAt;
	}
	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public Date getUpdatedAt() {
		return updatedAt;
	}
	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	public Integer getId() {
		return id;
	}

}
