package gov.dod.cls.db;

public class DocStatusRefRecord {
	
	public static final String CD_IN_PROGRESS = "I";
	// public static final String CD_COMPLETED = "C";
	public static final String CD_FINALIZED = "F";
	
	public static final String TABLE_DOC_STATUS_REF = "Doc_Status_Ref";
	
	public static final String FIELD_DOC_STATUS_CD        = "Doc_Status_Cd";
	public static final String FIELD_DOC_STATUS_NAME      = "Doc_Status_Name";
	public static final String FIELD_CREATED_AT           = "Created_At";
	public static final String FIELD_UPDATED_AT           = "Updated_At";
	

}
