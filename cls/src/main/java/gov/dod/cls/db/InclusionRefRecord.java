package gov.dod.cls.db;

import gov.dod.cls.model.JsonAble;

public class InclusionRefRecord extends JsonAble{
	public static final String TABLE_INCLUSION_REF = "Inclusion_Ref";
	
	public static final String FIELD_INCLUSION_CD = "inclusion_cd";
	public static final String FIELD_INCLUSION_NAME = "inclusion_name";
	public static final String FIELD_CREATED_AT = "created_at"; 
	public static final String FIELD_UPDATED_AT = "updated_at";
	
	public static final String INCLUDE_CD_FULL_TEXT = "F";
	public static final String INCLUDE_CD_REFERENCE = "R";
}
