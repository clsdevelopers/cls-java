package gov.dod.cls.db;

import gov.dod.cls.config.ClsConfig;
import gov.dod.cls.utils.CommonUtils;
import gov.dod.cls.utils.Pagination;
import gov.dod.cls.utils.sql.SqlUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.codehaus.jackson.node.ObjectNode;

public class OauthInterviewTable {

	public static int countActiveSession(Connection conn, int applicationId) throws SQLException {
		int result = 0;
		boolean bCreateConnection = (conn == null);
		PreparedStatement ps1 = null;
		ResultSet rs1 = null;
		try {
			java.sql.Timestamp timestamp = new java.sql.Timestamp(CommonUtils.getNow().getTime());
			String sSql = "SELECT COUNT(*)"
					+ " FROM " + OauthInterviewRecord.TABLE_OAUTH_INTERVIEW
					+ " WHERE " + OauthInterviewRecord.FIELD_APPLICATION_ID + " = ?"
					+ " AND " + OauthInterviewRecord.FIELD_EXPIRES_AT + " > ?";
			if (bCreateConnection)
				conn = ClsConfig.getDbConnection();
			ps1 = conn.prepareStatement(sSql);
			ps1.setInt(1, applicationId);
			ps1.setTimestamp(2, timestamp);
			rs1 = ps1.executeQuery();
			if (rs1.next()) {
				result = rs1.getInt(1);
			}
		}
		finally {
			SqlUtil.closeInstance(rs1, ps1);
			if (bCreateConnection)
				SqlUtil.closeInstance(conn);
		}
		return result;
	}
	
	public static void deleteApplication(Connection conn, int applicationId) throws SQLException {
		boolean bCreateConnection = (conn == null);
		PreparedStatement ps1 = null;
		try {
			String sSql = "DELETE FROM " + OauthInterviewRecord.TABLE_OAUTH_INTERVIEW
					+ " WHERE " + OauthInterviewRecord.FIELD_APPLICATION_ID + " = ?";
			if (bCreateConnection)
				conn = ClsConfig.getDbConnection();
			ps1 = conn.prepareStatement(sSql);
			ps1.setInt(1, applicationId);
			ps1.execute();
		}
		finally {
			SqlUtil.closeInstance(ps1);
			if (bCreateConnection)
				SqlUtil.closeInstance(conn);
		}
	}
	
	public static void loadPaginationForApi(Pagination pagination, int applicationId, Connection conn) 
			throws SQLException {
		pagination.addDataSource(OauthInterviewRecord.TABLE_OAUTH_INTERVIEW);
		boolean bCreateConnection = (conn == null);
		PreparedStatement ps1 = null;
		ResultSet rs1 = null;
		try {
			if (bCreateConnection)
				conn = ClsConfig.getDbConnection();
			String sSql = "SELECT COUNT(" + OauthInterviewRecord.FIELD_OAUTH_INTERVIEW_ID + ")"
					+ " FROM " + OauthInterviewRecord.TABLE_OAUTH_INTERVIEW
					+ " WHERE " + OauthInterviewRecord.FIELD_APPLICATION_ID + " = ?";
			ps1 = conn.prepareStatement(sSql);
			ps1.setInt(1, applicationId);
			rs1 = ps1.executeQuery();
			boolean isAcceptableRange = false;
			if (rs1.next())
				isAcceptableRange = pagination.isAcceptedRange(rs1.getInt(1));
			
			if (isAcceptableRange) {
				SqlUtil.closeInstance(rs1);
				rs1 = null;
				SqlUtil.closeInstance(ps1);
				ps1 = null;
				
				sSql = OauthInterviewRecord.SQL_SELECT
						+ " WHERE " + OauthInterviewRecord.FIELD_APPLICATION_ID + " = ?";
				sSql += " ORDER BY " + OauthInterviewRecord.FIELD_OAUTH_INTERVIEW_ID;

				ps1 = conn.prepareStatement(sSql + pagination.sqlLimit());
				ps1.setInt(1, applicationId);
				rs1 = ps1.executeQuery();
				OauthInterviewRecord oRecord = new OauthInterviewRecord();
				while (rs1.next()) {
					oRecord.read(rs1);
					oRecord.isExpired(conn, true);
					ObjectNode json = oRecord.genJson(true, rs1, pagination.getMapper());
					pagination.getArrayNode().add(json);
					pagination.incCount();
				}
			}
		}
		finally {
			SqlUtil.closeInstance(rs1, ps1);
			if (bCreateConnection)
				SqlUtil.closeInstance(conn);
		}

	}
	
}
