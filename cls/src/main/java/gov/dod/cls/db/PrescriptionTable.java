package gov.dod.cls.db;

import gov.dod.cls.PageApi;
import gov.dod.cls.bean.session.UserSession;
import gov.dod.cls.config.ClsConfig;
import gov.dod.cls.db.utils.ClauseVersionPrescriptions;
import gov.dod.cls.model.JsonAble;
import gov.dod.cls.utils.CommonUtils;
import gov.dod.cls.utils.Pagination;
import gov.dod.cls.utils.sql.SqlUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.node.ArrayNode;
import org.codehaus.jackson.node.ObjectNode;

public class PrescriptionTable extends ArrayList<PrescriptionRecord> {
	
	private static final long serialVersionUID = -3693611311643311893L;

	// ----------------------------------------------------------
	public static final String PREFIX_DO_PRESCRIPTION = "prescription-";
	public static final String DO_PRESCRIPTION_LIST = PREFIX_DO_PRESCRIPTION + "list";
	public static final String DO_PRESCRIPTION_DETAILS_GET = PREFIX_DO_PRESCRIPTION + "detail-get";

	public static void processRequest(String sDo, UserSession oUserSession, 
			HttpServletRequest req, HttpServletResponse resp) {
		if (!oUserSession.isSuperUser()) { // CJ-680
			return;
		}
		
		switch (sDo) {
		case PrescriptionTable.DO_PRESCRIPTION_LIST:
			PrescriptionTable.processList(req, resp);
			return;
		case PrescriptionTable.DO_PRESCRIPTION_DETAILS_GET:
			PrescriptionTable.getPrescriptionDetails(sDo, oUserSession, req, resp);
			return;			
		}
	}
	
	// ----------------------------------------------------------
	public static final String SQL_LIST
		= "SELECT A." + PrescriptionRecord.FIELD_PRESCRIPTION_ID
		+ ", A." + PrescriptionRecord.FIELD_PRESCRIPTION_NAME
		+ ", A." + PrescriptionRecord.FIELD_PRESCRIPTION_URL
		+ ", B." + RegulationRecord.FIELD_REGULATION_NAME + " AS RegulationName"
		+ " FROM " + PrescriptionRecord.TABLE_PRESCRIPTIONS + " A"
		+ " LEFT OUTER JOIN " + RegulationRecord.TABLE_REGULATIONS + " B"
		+ " ON (B." + RegulationRecord.FIELD_REGULATION_ID + " = A." + PrescriptionRecord.FIELD_REGULATION_ID + ")";
		// + " ORDER BY 2";

	private static void processList(HttpServletRequest req, HttpServletResponse resp) {
		String query = req.getParameter("query");
		String sort_field = req.getParameter("sort_field");
		String sort_order = req.getParameter("sort_order");
		
		Pagination pagination = new Pagination(req);
		pagination.addDataSource(PrescriptionRecord.TABLE_PRESCRIPTIONS);
		Connection conn = null;
		PreparedStatement ps1 = null;
		ResultSet rs1 = null;
		try {
			conn = ClsConfig.getDbConnection();
			String sSql = "SELECT COUNT(A." + PrescriptionRecord.FIELD_PRESCRIPTION_ID + ") FROM " + PrescriptionRecord.TABLE_PRESCRIPTIONS + " A"
						+ " LEFT OUTER JOIN " + RegulationRecord.TABLE_REGULATIONS + " B"
						+ " ON (B." + RegulationRecord.FIELD_REGULATION_ID + " = A." + PrescriptionRecord.FIELD_REGULATION_ID + ")";					
			// 4/14/2015 IC fix
			String operator = " WHERE ";
			if (CommonUtils.isNotEmpty(query)) {
				sSql += operator + "(LOWER(A." + PrescriptionRecord.FIELD_PRESCRIPTION_NAME + ") LIKE ?" 
					+ " OR LOWER(B." + RegulationRecord.FIELD_REGULATION_NAME + ") LIKE ?"
					+ ")";
				operator = " AND ";
				query = "%" + query.toLowerCase() + "%";
			}	
			
			ps1 = conn.prepareStatement(sSql);
			
			int iParam = 1;
			if (CommonUtils.isNotEmpty(query)) {
				ps1.setString(iParam++, query);
				ps1.setString(iParam++, query);
			}
			
			rs1 = ps1.executeQuery();
			boolean isAcceptableRange = false;
			if (rs1.next())
				isAcceptableRange = pagination.isAcceptedRange(rs1.getInt(1));
			
			if (isAcceptableRange) {
				SqlUtil.closeInstance(rs1);
				rs1 = null;
				SqlUtil.closeInstance(ps1);
				ps1 = null;

				sSql = SQL_LIST;
				operator = " WHERE ";
				if (CommonUtils.isNotEmpty(query)) {
					sSql += operator + "(LOWER(A." + PrescriptionRecord.FIELD_PRESCRIPTION_NAME + ") LIKE ?" 
							+ " OR LOWER(B." + RegulationRecord.FIELD_REGULATION_NAME + ") LIKE ?"
							+ ")";
						operator = " AND ";
				}
				String sAsc = ("desc".equals(sort_order) ? " DESC" : "");
				String orderField = PrescriptionRecord.FIELD_PRESCRIPTION_NAME;
				if ("prescription_regulation".equals(sort_field))
					orderField = RegulationRecord.FIELD_REGULATION_NAME;
				sSql += " ORDER BY " + orderField + sAsc;				
				
				ps1 = conn.prepareStatement(sSql + pagination.sqlLimit());
				
				iParam = 1;
				if (CommonUtils.isNotEmpty(query)) {
					ps1.setString(iParam++, query);
					ps1.setString(iParam++, query);
				}
				
				rs1 = ps1.executeQuery();
				while (rs1.next()) {
					ObjectNode json = pagination.getMapper().createObjectNode();
					json.put(PrescriptionRecord.FIELD_PRESCRIPTION_ID, rs1.getInt(PrescriptionRecord.FIELD_PRESCRIPTION_ID));
					json.put(PrescriptionRecord.FIELD_PRESCRIPTION_NAME, rs1.getString(PrescriptionRecord.FIELD_PRESCRIPTION_NAME));
					json.put(PrescriptionRecord.FIELD_PRESCRIPTION_URL, rs1.getString(PrescriptionRecord.FIELD_PRESCRIPTION_URL));
					json.put("RegulationName", rs1.getString("RegulationName"));

					pagination.incCount();
					pagination.getArrayNode().add(json);
				}
			}
			pagination.send(resp);
		}
		catch (Exception oError) {
			oError.printStackTrace();
		}
		finally {
			SqlUtil.closeInstance(rs1, ps1, conn);
		}
	}
	
	private static void getPrescriptionDetails(String sDo, UserSession oUserSession, 
			HttpServletRequest req, HttpServletResponse resp) {
		Integer id = CommonUtils.toInteger(req.getParameter("id"));
		String errorMessage = null;
		PrescriptionRecord prescription = null;
		if (id != null) {
			try {
				prescription = PrescriptionRecord.getRecord(id);
				if (prescription == null)
					errorMessage = "The requested record is not found.";
			} catch (Exception oError) {
				errorMessage = "Unable to retrieve record. Error: " + oError.getMessage();
			}
		} else
			errorMessage = "Unauthorized request";
		String response;
		if (errorMessage == null) {
			ObjectNode objectNode = (ObjectNode)prescription.toJson();
			response = JsonAble.jsonResult(JsonAble.STATUS_SUCCESS, objectNode, null);
		} else
			response = JsonAble.jsonResult(JsonAble.STATUS_FAIL, null, errorMessage);
		PageApi.send(resp, JsonAble.JSON_RESPONSE_FORMAT, response);
	}	
	

	// ========================================================
	private volatile static PrescriptionTable instance = null;
	private volatile static Date refreshed = null;
	
	public synchronized static PrescriptionTable getInstance(Connection conn) {
		if (PrescriptionTable.instance == null) {
			PrescriptionTable.instance = new PrescriptionTable();
			PrescriptionTable.instance.refresh(conn);
		} else {
			PrescriptionTable.instance.refreshWhenNeeded(conn);
		}
		return PrescriptionTable.instance;
	}
	
	public static PrescriptionTable regulationSubset(int regulationId) {
		PrescriptionTable prescriptionTable = PrescriptionTable.getInstance(null);
		return prescriptionTable.subsetByRegulation(regulationId);
	}

	public static PrescriptionTable clauseVersionSubset(int clauseVersionId, Connection conn) {
		PrescriptionTable prescriptionTable = PrescriptionTable.getInstance(conn);
		ArrayList<Integer> prescritions = ClauseVersionPrescriptions.getSubsetByClauseVersion(clauseVersionId, conn);
		return prescriptionTable.subsetByList(prescritions);
	}

	public static PrescriptionTable getSubset(ArrayList<Integer> aIds) {
		PrescriptionTable prescriptionTable = PrescriptionTable.getInstance(null);
		return prescriptionTable.subsetByList(aIds);
	}

	public static ArrayNode getJson(Integer pClauseVersionId, Connection conn, ObjectMapper mapper, boolean forInterview) {
		if (pClauseVersionId == null)
			pClauseVersionId = ClauseVersionTable.getActiveVersionId();
		if (mapper == null)
			 mapper = new ObjectMapper();
		ArrayNode result = mapper.createArrayNode();
		if (pClauseVersionId == null) {
			return result;
		}
		PreparedStatement ps1 = null;
		ResultSet rs1 = null;
		try {
			conn = ClsConfig.getDbConnection();
			String sSql = "SELECT " + PrescriptionRecord.FIELD_PRESCRIPTION_ID
					+ ", " + PrescriptionRecord.FIELD_REGULATION_ID
					+ ", " + PrescriptionRecord.FIELD_PRESCRIPTION_NAME
					+ ", " + PrescriptionRecord.FIELD_PRESCRIPTION_URL
					+ " FROM " + PrescriptionRecord.TABLE_PRESCRIPTIONS
					+ " WHERE " + PrescriptionRecord.FIELD_PRESCRIPTION_ID + " IN ("
						+ "	SELECT " + ClausePrescriptionRecord.FIELD_PRESCRIPTION_ID
						+ " FROM " + ClausePrescriptionRecord.TABLE_CLAUSE_PRESCRIPTIONS
						+ " UNION"
						+ " SELECT " + QuestionChoicePrescriptionRecord.FIELD_PRESCRIPTION_ID
						+ " FROM " + QuestionChoicePrescriptionRecord.TABLE_QUESTION_CHOICE_PRESCRIPTIONS
					+ ")"
					+ " ORDER BY " + PrescriptionRecord.FIELD_PRESCRIPTION_NAME;

			ps1 = conn.prepareStatement(sSql);
			rs1 = ps1.executeQuery();
			while (rs1.next()) {
				ObjectNode json = mapper.createObjectNode();
				json.put(PrescriptionRecord.FIELD_PRESCRIPTION_ID, rs1.getInt(PrescriptionRecord.FIELD_PRESCRIPTION_ID));
				json.put(PrescriptionRecord.FIELD_REGULATION_ID, rs1.getInt(PrescriptionRecord.FIELD_REGULATION_ID));
				json.put(PrescriptionRecord.FIELD_PRESCRIPTION_NAME, rs1.getString(PrescriptionRecord.FIELD_PRESCRIPTION_NAME));
				json.put(PrescriptionRecord.FIELD_PRESCRIPTION_URL, rs1.getString(PrescriptionRecord.FIELD_PRESCRIPTION_URL));
				result.add(json);
			}
		}
		catch (Exception oError) {
			oError.printStackTrace();
		}
		finally {
			SqlUtil.closeInstance(rs1, ps1, conn);
		}
		return result;
	}

	// Functions -----------------------------------------------------------------
	public static Date getReferHistoryDate(Connection conn) {
		return SysLastTableChangeAtTable.getLastUpdateDate(conn, PrescriptionRecord.TABLE_PRESCRIPTIONS);
	}
	  
	public static boolean needToRefreh(Connection conn) {
		return SysLastTableChangeAtTable.needToLoad(PrescriptionTable.refreshed, getReferHistoryDate(conn));
	}
	
	// ========================================================
	private Exception lastError;

	public void refreshWhenNeeded(Connection conn) {
		if (PrescriptionTable.needToRefreh(conn))
			this.refresh(conn);
	}

	private boolean refresh(Connection conn) {
		boolean result = false;
		String sSql = PrescriptionRecord.SQL_SELECT;
		boolean bCreateConnection = (conn == null);
		PreparedStatement ps1 = null;
		ResultSet rs1 = null;
		try {
			if (bCreateConnection)
				conn = ClsConfig.getDbConnection();
			ps1 = conn.prepareStatement(sSql);
			rs1 = ps1.executeQuery();
			this.clear();
			while (rs1.next()) {
				PrescriptionRecord prescriptionRecord = new PrescriptionRecord();
				prescriptionRecord.read(rs1);
				this.add(prescriptionRecord);
			}
			PrescriptionTable.refreshed = getReferHistoryDate(conn); // CommonUtils.getNow();
			result = true;
			System.out.println("PrescriptionTable refreshed");
		}
		catch (Exception oError) {
			this.lastError = oError;
			oError.printStackTrace();
		}
		finally {
			SqlUtil.closeInstance(rs1, ps1);
			if (bCreateConnection)
				SqlUtil.closeInstance(conn);
		}
		return result;
	}
	
	public PrescriptionTable subsetByRegulation(int regulationId) {
		PrescriptionTable prescriptionTable = new PrescriptionTable();
		for (PrescriptionRecord prescriptionRecord : this) {
			if (prescriptionRecord.isSameRegulationId(regulationId))
				prescriptionTable.add(prescriptionRecord);
		}
		return prescriptionTable;
	}
	
	public Date getRefreshed() {
		return refreshed;
	}
	
	public PrescriptionRecord findById(int id) {
		for (PrescriptionRecord prescriptionRecord : this) {
			if (prescriptionRecord.getId().intValue() == id)
				return prescriptionRecord;
		}
		return null;
	}

	public PrescriptionTable subsetByList(ArrayList<Integer> aIds) {
		PrescriptionTable result = new PrescriptionTable();
		for (Integer id : aIds) {
			PrescriptionRecord prescriptionRecord = this.findById(id);
			if (prescriptionRecord != null)
				result.add(prescriptionRecord);
		}
		return result;
	}

	public ArrayNode toJson(ObjectMapper mapper, boolean forInterview) {
		if (mapper == null)
			mapper = new ObjectMapper();
		ArrayNode json = mapper.createArrayNode();
		for (PrescriptionRecord prescriptionRecord : this) {
			json.add(prescriptionRecord.toJson(forInterview));
		}
		return json;
	}

	public Exception getLastError() {
		return lastError;
	}
	
}
