package gov.dod.cls.db;

import gov.dod.cls.bean.session.UserSession;
import gov.dod.cls.config.ClsConfig;
import gov.dod.cls.utils.Pagination;
import gov.dod.cls.utils.sql.SqlUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.codehaus.jackson.node.ObjectNode;

public final class RoleTable extends ArrayList<RoleRecord> {

	private static final long serialVersionUID = -2900451931663121777L;
	private static volatile RoleTable instance = null;

	public synchronized static RoleTable getInstance() {
		if (RoleTable.instance == null) {
			RoleTable.instance = new RoleTable();
			RoleTable.instance.load();
		} else if (RoleTable.instance.size() == 0) {
			RoleTable.instance.load();
		}
		return RoleTable.instance;
	}
	
	// ----------------------------------------------------------
	public static final String PREFIX_DO_ROLE = "role-";
	public static final String DO_ROLE_LIST = PREFIX_DO_ROLE + "list";

	public static void processRequest(String sDo, UserSession oUserSession, 
			HttpServletRequest req, HttpServletResponse resp) {
		switch (sDo) {
		case RoleTable.DO_ROLE_LIST:
			RoleTable.processList(req, resp);
		}
	}
	
	// ------------------------------------------------
	private void load() {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			conn = ClsConfig.getDbConnection();
			ps = conn.prepareStatement(RoleRecord.SQL_SELECT);
			rs = ps.executeQuery();
			this.clear();
			while (rs.next()) {
				RoleRecord roleRecord = new RoleRecord();
				roleRecord.read(rs);
				this.add(roleRecord);
			}
		}
		catch (Exception oError) {
			oError.printStackTrace();
		}
		finally {
			SqlUtil.closeInstance(rs, ps, conn);
		}
	}
	
	public static final String SQL_SELECT_ROLE_STATISTIC
		= "SELECT A." + RoleRecord.FIELD_ROLE_NAME
		+ ", COUNT(B." + UserRoleRecord.FIELD_USER_ID + ") AS UserCount"
		+ " FROM " + RoleRecord.TABLE_ROLES + " A"
		+ " LEFT OUTER JOIN " + UserRoleRecord.TABLE_USER_ROLES + " B"
		+ " ON (B." + UserRoleRecord.FIELD_ROLE_ID + "=A." + RoleRecord.FIELD_ROLE_ID + ")"
		+ " GROUP BY A." + RoleRecord.FIELD_ROLE_NAME
		+ " ORDER BY 1";

	private static void processList(HttpServletRequest req, HttpServletResponse resp) {
		Pagination pagination = new Pagination(req);
		pagination.addDataSource("Roles");
		Connection conn = null;
		PreparedStatement ps1 = null;
		ResultSet rs1 = null;
		try {
			conn = ClsConfig.getDbConnection();
			ps1 = conn.prepareStatement(SQL_SELECT_ROLE_STATISTIC);
			rs1 = ps1.executeQuery();
			while (rs1.next()) {
				//Integer id = rs1.getInt(UserRecord.FIELD_USER_ID);
				ObjectNode json = pagination.getMapper().createObjectNode();
				json.put("RoleName", rs1.getString(RoleRecord.FIELD_ROLE_NAME)); // RoleRecord.getLogicalName(id));
				json.put("UserCount", rs1.getInt("UserCount"));
					
				pagination.incCount();
				pagination.getArrayNode().add(json);
			}
			pagination.send(resp);
		}
		catch (Exception oError) {
			oError.printStackTrace();
		}
		finally {
			SqlUtil.closeInstance(rs1, ps1, conn);
		}
	}

}
