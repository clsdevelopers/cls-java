package gov.dod.cls.db;

import gov.dod.cls.utils.sql.SqlUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.node.ArrayNode;
import org.codehaus.jackson.node.ObjectNode;

public class DocumentClauseTable extends ArrayList<DocumentClauseRecord> {

	private static final long serialVersionUID = 3159684416414970330L;

	public static DocumentClauseTable loadByDocument(int docId, Connection conn) throws SQLException {
		DocumentClauseTable documentAnswerTable = new DocumentClauseTable();
		String sSql = DocumentClauseRecord.SQL_SELECT
				+ " WHERE " + DocumentClauseRecord.FIELD_DOCUMENT_ID + " = ?"
				+ " ORDER BY " + DocumentClauseRecord.FIELD_CLAUSE_ID;
		/*
		String sSql = DocumentClauseRecord.SQL_SELECT
				+ " JOIN " + ClauseRecord.TABLE_CLAUSES + " B"
				+ " ON (B." + ClauseRecord.FIELD_CLAUSE_ID + " = A." + DocumentClauseRecord.FIELD_CLAUSE_ID + ")"
				+ " WHERE A." + DocumentClauseRecord.FIELD_DOCUMENT_ID + " = ?"
				+ " ORDER BY B." + ClauseRecord.FIELD_CLAUSE_NAME; // DocumentClauseRecord.FIELD_CLAUSE_ID;
		 */
		PreparedStatement ps1 = null;
		ResultSet rs1 = null;
		try {
			ps1 = conn.prepareStatement(sSql);
			ps1.setInt(1, docId);
			rs1 = ps1.executeQuery();
			while (rs1.next()) {
				DocumentClauseRecord documentClauseRecord = new DocumentClauseRecord();
				documentClauseRecord.read(rs1);
				documentAnswerTable.add(documentClauseRecord);
			}
		}
		finally {
			SqlUtil.closeInstance(rs1, ps1);
		}
		return documentAnswerTable;
	}
	
	// CJ-593. For API, removed the Removed clauses.
	public static DocumentClauseTable loadByDocumentForApi(int docId, Connection conn) throws SQLException {
		DocumentClauseTable documentAnswerTable = new DocumentClauseTable();
		String sSql = DocumentClauseRecord.SQL_SELECT
				+ " WHERE " + DocumentClauseRecord.FIELD_DOCUMENT_ID + " = ?"
				+ " AND (" + DocumentClauseRecord.FIELD_OPTIONAL_USER_ACTION_CODE + " IS NULL"
				+ 	" OR " + DocumentClauseRecord.FIELD_OPTIONAL_USER_ACTION_CODE + " <> 'R' )" // " != 'R' "
				+ " ORDER BY " + DocumentClauseRecord.FIELD_CLAUSE_ID;

		PreparedStatement ps1 = null;
		ResultSet rs1 = null;
		try {
			ps1 = conn.prepareStatement(sSql);
			ps1.setInt(1, docId);
			rs1 = ps1.executeQuery();
			while (rs1.next()) {
				DocumentClauseRecord documentClauseRecord = new DocumentClauseRecord();
				documentClauseRecord.read(rs1);
				documentAnswerTable.add(documentClauseRecord);
			}
		}
		finally {
			SqlUtil.closeInstance(rs1, ps1);
		}
		return documentAnswerTable;
	}
	
	public static void deleteDocument(int docId, Connection conn) throws SQLException {
		PreparedStatement ps1 = null;
		try {
			String sSql = "DELETE FROM " + DocumentClauseRecord.TABLE_DOCUMENT_CLAUSES
					+ " WHERE " + DocumentClauseRecord.FIELD_DOCUMENT_ID + " = ?";
			ps1 = conn.prepareStatement(sSql);
			ps1.setInt(1, docId);
			ps1.execute();
		}
		finally {
			SqlUtil.closeInstance(ps1);
		}
	}

	// ===============================================
	public ArrayNode toJson(ObjectMapper mapper, boolean forInterview) {
		if (mapper == null)
			mapper = new ObjectMapper();
		ArrayNode json = mapper.createArrayNode();
		for (DocumentClauseRecord record : this) {
			json.add(record.toJson(forInterview));
		}
		return json;
	}
	
	public void populateJsonForApi(ObjectNode json, boolean bForVer1, ClauseTable clauseTable, DocumentFillInsTable documentFillInsTable) {
		ArrayNode arrayNode = json.arrayNode();
		for (DocumentClauseRecord record : this) {
			ObjectNode child = arrayNode.objectNode();
			record.populateJsonForApi(child, bForVer1, clauseTable, documentFillInsTable);
			arrayNode.add(child);
		}
		json.put(bForVer1 ? "clauses" : DocumentClauseRecord.TABLE_DOCUMENT_CLAUSES.toLowerCase(), arrayNode);
	}
	
	public void fromJson(ArrayNode pArrayNode) {
		for (JsonNode jsonNode : pArrayNode) {
			DocumentClauseRecord record = new DocumentClauseRecord();
			record.fromJson(jsonNode);
			this.add(record);
		}
	}
	
	public DocumentClauseRecord findByClauseId(int clauseId, boolean pRemove) {
		for (DocumentClauseRecord oRecord : this) {
			if (oRecord.getClauseId().intValue() == clauseId) {
				if (pRemove)
					this.remove(oRecord);
				return oRecord;
			}
		}
		return null;
	}
	
	public DocumentClauseRecord findByClauseId(int clauseId) {
		return this.findByClauseId(clauseId, false);
	}
	
	public void populateAwardFromSolicitaton(int awardDocumentId,
			ClauseTable awardClauseTable, DocumentFillInsTable awardDocumentFillInsTable,
			DocumentClauseTable solDocumentClauseTable, DocumentFillInsTable solDocumentFillInsTable, 
			ClauseTable solClauseTable, Connection conn) throws SQLException {
		PreparedStatement psInsertClause = null;
		PreparedStatement psInsertFillIns = null;
		
		try {
			ClauseSectionTable oClauseSectionTable = ClauseSectionTable.getInstance(conn);
			
			psInsertClause = DocumentClauseTable.createInertPrepareStatement(conn);
			psInsertClause.setInt(1, awardDocumentId);
			
			psInsertFillIns = DocumentFillInsTable.createInertPrepareStatement(conn);
			
			for (DocumentClauseRecord solDocumentClauseRecord : solDocumentClauseTable) {
				Integer solClauseId = solDocumentClauseRecord.getClauseId();
				ClauseRecord solClauseRecord = solClauseTable.findById(solClauseId);
				if (solClauseRecord == null)
					continue;
				
				if (oClauseSectionTable.isSolicitationSelection(solClauseRecord.getClauseSectionId()))
					continue;
				
				ClauseRecord awardClauseRecord = awardClauseTable.findByName(solClauseRecord.getClauseName());
				if (awardClauseRecord == null)
					continue;

				psInsertClause.setInt(2, awardClauseRecord.getId());
				psInsertClause.setBoolean(3, solDocumentClauseRecord.isFillinCompleted());
				SqlUtil.setParam(psInsertClause, 4, solDocumentClauseRecord.getOptionalUserActionCode(), java.sql.Types.VARCHAR);
				//psInsertClause.setBoolean(4, solDocumentClauseRecord.getIsRemovedByUser());
				psInsertClause.execute();
				
				DocumentFillInsTable solClauseFillIns = solDocumentFillInsTable.collectByClause(solClauseId);
				
				awardDocumentFillInsTable.populateFromPrevious(awardDocumentId, solClauseFillIns, psInsertFillIns,
						solClauseRecord.getClauseFillInTable(), awardClauseRecord.getClauseFillInTable()); // CJ-1061
			}
		} finally {
			SqlUtil.closeInstance(psInsertFillIns);
			SqlUtil.closeInstance(psInsertClause);
		}
	}
	
	public void populateDocFromTemplate(int awardDocumentId,
			ClauseTable awardClauseTable, DocumentFillInsTable awardDocumentFillInsTable,
			DocumentClauseTable solDocumentClauseTable, DocumentFillInsTable solDocumentFillInsTable, 
			ClauseTable solClauseTable, Connection conn) throws SQLException {
		PreparedStatement psInsertClause = null;
		PreparedStatement psInsertFillIns = null;
		
		try {
			psInsertClause = DocumentClauseTable.createInertPrepareStatement(conn);
			psInsertClause.setInt(1, awardDocumentId);
			
			psInsertFillIns = DocumentFillInsTable.createInertPrepareStatement(conn);
			
			for (DocumentClauseRecord solDocumentClauseRecord : solDocumentClauseTable) {
				Integer solClauseId = solDocumentClauseRecord.getClauseId();
				ClauseRecord solClauseRecord = solClauseTable.findById(solClauseId);
				if (solClauseRecord == null)
					continue;
				
				ClauseRecord awardClauseRecord = awardClauseTable.findByName(solClauseRecord.getClauseName());
				if (awardClauseRecord == null)
					continue;

				psInsertClause.setInt(2, awardClauseRecord.getId());
				psInsertClause.setBoolean(3, false);
				SqlUtil.setParam(psInsertClause, 4, solDocumentClauseRecord.getOptionalUserActionCode(), java.sql.Types.VARCHAR);
				psInsertClause.execute();
			}
		} finally {
			SqlUtil.closeInstance(psInsertFillIns);
			SqlUtil.closeInstance(psInsertClause);
		}
	}
	public static void upgradeClsVersion(int documentId,
			ClauseTable newClauseTable, // DocumentFillInsTable newDocumentFillInsTable,
			DocumentClauseTable origDocumentClauseTable, DocumentFillInsTable origDocumentFillInsTable, 
			ClauseTable origClauseTable, ArrayList<String> alRemovedClauseNames, Connection conn)
					throws SQLException 
	{
		PreparedStatement psUpdateClause = null;
		PreparedStatement psDeleteClause = null;
		PreparedStatement psUpdateFillIns = null;
		PreparedStatement psDeleteFillIns = null;
		try {
			psUpdateClause = DocumentClauseTable.createUpgradeClsVersionPrepareStatement(conn);
			// 1 " SET " + DocumentClauseRecord.FIELD_CLAUSE_ID + "= ?"
			// 2 ", " + DocumentClauseRecord.FIELD_IS_FILLIN_COMPLETED + "= ?"
			// 3 " WHERE " + DocumentClauseRecord.FIELD_DOCUMENT_ID + "= ?"
			// 4 " AND " + DocumentClauseRecord.FIELD_CLAUSE_ID + " = ?";

			psUpdateClause.setInt(3, documentId); // " WHERE " + DocumentClauseRecord.FIELD_DOCUMENT_ID + "= ?"
			
			psDeleteClause = DocumentClauseTable.createDeletePrepareStatement(conn);
			psDeleteClause.setInt(1, documentId); // " WHERE " + DocumentClauseRecord.FIELD_DOCUMENT_ID + "= ?"

			psUpdateFillIns = DocumentFillInsTable.createUpgradeClsVersionPrepareStatement(conn);
			// 1 " SET " + DocumentFillInsRecord.FIELD_CLAUSE_FILL_IN_ID + " = ?"
			// 2 " WHERE " + DocumentFillInsRecord.FIELD_DOCUMENT_FILL_IN_ID + " = ?";
			
			for (DocumentClauseRecord origDocumentClauseRecord : origDocumentClauseTable) {
				Integer origClauseId = origDocumentClauseRecord.getClauseId();
				ClauseRecord origClauseRecord = origClauseTable.findById(origClauseId);
				boolean bDelete = true;
				if (origClauseRecord != null) {
					ClauseRecord newClauseRecord = newClauseTable.findByName(origClauseRecord.getClauseName());
					if (newClauseRecord != null) {
						boolean bIsComplete = origDocumentClauseRecord.isFillinCompleted();
						if (bIsComplete && (!"D".equals(origDocumentClauseRecord.getOptionalUserActionCode()))) {
							if (newClauseRecord.hasSameFillInCodes(origClauseRecord) == false)
								bIsComplete = false;
						}
						Integer newClauseId = newClauseRecord.getId();
						psUpdateClause.setInt(1, newClauseId); // 1 " SET " + DocumentClauseRecord.FIELD_CLAUSE_ID
						psUpdateClause.setBoolean(2, bIsComplete); // 2 ", " + DocumentClauseRecord.FIELD_IS_FILLIN_COMPLETED + "= ?"
						psUpdateClause.setInt(4, origClauseId); // 4 " AND " + DocumentClauseRecord.FIELD_CLAUSE_ID + " = ?";
						psUpdateClause.execute();
						
						DocumentFillInsTable.upgradeClsVersion(origClauseRecord, newClauseRecord, origDocumentFillInsTable, psUpdateFillIns);
						bDelete = false;
					} else {
						alRemovedClauseNames.add(origClauseRecord.getClauseName());
					}
				}
				if (bDelete) {
					psDeleteClause.setInt(2, origClauseId); // " AND " + DocumentClauseRecord.FIELD_CLAUSE_ID + " = ?";
					psDeleteClause.execute();
				}
			}
			if (origDocumentFillInsTable.size() > 0) {
				psDeleteFillIns = DocumentFillInsTable.createDeletePrepareStatement(conn);
				origDocumentFillInsTable.deleteForClsVersion(psDeleteFillIns);
			}
		} finally {
			SqlUtil.closeInstance(psDeleteFillIns);
			SqlUtil.closeInstance(psUpdateFillIns);
			SqlUtil.closeInstance(psDeleteClause);
			SqlUtil.closeInstance(psUpdateClause);
		}
	}
	
	public static PreparedStatement createInertPrepareStatement(Connection conn) throws SQLException {
		String sSql
				= "INSERT INTO " + DocumentClauseRecord.TABLE_DOCUMENT_CLAUSES
				+ " (" + DocumentClauseRecord.FIELD_DOCUMENT_ID
				+ ", " + DocumentClauseRecord.FIELD_CLAUSE_ID
				+ ", " + DocumentClauseRecord.FIELD_IS_FILLIN_COMPLETED
				+ ", " + DocumentClauseRecord.FIELD_OPTIONAL_USER_ACTION_CODE // FIELD_IS_REMOVED_BY_USER
					+ ") VALUES (?, ?, ?, ?)";
		return conn.prepareStatement(sSql);
	}
	
	public static PreparedStatement createUpgradeClsVersionPrepareStatement(Connection conn) throws SQLException {
		String sSql
				= "UPDATE " + DocumentClauseRecord.TABLE_DOCUMENT_CLAUSES
				+ " SET " + DocumentClauseRecord.FIELD_CLAUSE_ID + "= ?"
				+ ", " + DocumentClauseRecord.FIELD_IS_FILLIN_COMPLETED + "= ?"
				+ " WHERE " + DocumentClauseRecord.FIELD_DOCUMENT_ID + "= ?"
				+ " AND " + DocumentClauseRecord.FIELD_CLAUSE_ID + " = ?";
		return conn.prepareStatement(sSql);
	}
	
	public static PreparedStatement createDeletePrepareStatement(Connection conn) throws SQLException {
		String sSql
				= "DELETE FROM " + DocumentClauseRecord.TABLE_DOCUMENT_CLAUSES
				+ " WHERE " + DocumentClauseRecord.FIELD_DOCUMENT_ID + "= ?"
				+ " AND " + DocumentClauseRecord.FIELD_CLAUSE_ID + " = ?";
		return conn.prepareStatement(sSql);
	}
	
	public void save(int documentId, DocumentClauseTable postTable, ClauseTable oClauseTable, 
			Connection conn, AuditEventRecord auditEventRecord)
			throws SQLException {
		PreparedStatement psInsert = null;
		PreparedStatement psUpdate = null;
		PreparedStatement psDelete = null;
		try {
			String sSql;
			DocumentClauseTable processed = new DocumentClauseTable();
			
			for (DocumentClauseRecord postRecord : postTable) {
				DocumentClauseRecord oRecord = this.findByClauseId(postRecord.getClauseId().intValue());
				if (oRecord == null) {
					if (oClauseTable.findById(postRecord.getClauseId()) != null) {
						if (psInsert == null) {
							psInsert = DocumentClauseTable.createInertPrepareStatement(conn);
						}
						psInsert.setInt(1, documentId);
						psInsert.setInt(2, postRecord.getClauseId());
						psInsert.setBoolean(3, postRecord.isFillinCompleted());
						SqlUtil.setParam(psInsert, 4, postRecord.getOptionalUserActionCode(), java.sql.Types.VARCHAR);
						// psInsert.setBoolean(4, postRecord.getIsRemovedByUser());
						psInsert.execute();
					} else {
						System.out.println("DocumentClauseTable.save() not ClauseId found error:"
								+ " DocumentId(" + documentId + ")"
								+ " ClauseId(" + postRecord.getClauseId() + ")"
								+ " IsFillinCompleted(" + postRecord.isFillinCompleted() + ")"
								+ " OptionalUserActionCode(" + postRecord.getOptionalUserActionCode() + ")"
								//+ " IsRemovedByUser(" + postRecord.getIsRemovedByUser() + ")"
								);
					}
				} else {
					if (psUpdate == null) {
						sSql = "UPDATE " + DocumentClauseRecord.TABLE_DOCUMENT_CLAUSES
							+ " SET " + DocumentClauseRecord.FIELD_IS_FILLIN_COMPLETED + " = ?"
							+ ", " + DocumentClauseRecord.FIELD_OPTIONAL_USER_ACTION_CODE + " = ?" // FIELD_IS_REMOVED_BY_USER
							+ " WHERE " + DocumentClauseRecord.FIELD_DOCUMENT_ID + " = ?"
							+ " AND " + DocumentClauseRecord.FIELD_CLAUSE_ID + " = ?";
						psUpdate = conn.prepareStatement(sSql);
					}
					if (oRecord.hasDifferentAnswer(postRecord)) {
						psUpdate.setBoolean(1, postRecord.isFillinCompleted());
						SqlUtil.setParam(psUpdate, 2, postRecord.getOptionalUserActionCode(), java.sql.Types.VARCHAR);
						//psUpdate.setBoolean(2, postRecord.getIsRemovedByUser());
						psUpdate.setInt(3, documentId);
						psUpdate.setInt(4, oRecord.getClauseId());
						psUpdate.execute();
					}
					processed.add(oRecord);
				}
			}
			
			for (DocumentClauseRecord oRecord : this) {
				if (!processed.contains(oRecord)) {
					if (psDelete == null) {
						sSql = "DELETE FROM " + DocumentClauseRecord.TABLE_DOCUMENT_CLAUSES
							+ " WHERE " + DocumentClauseRecord.FIELD_DOCUMENT_ID + " = ?"
							+ " AND " + DocumentClauseRecord.FIELD_CLAUSE_ID + " = ?";
						psDelete = conn.prepareStatement(sSql);
					}
					psDelete.setInt(1, documentId);
					psDelete.setInt(2, oRecord.getClauseId());
					psDelete.execute();
				}
			}
			
		} finally {
			SqlUtil.closeInstance(psInsert);
			SqlUtil.closeInstance(psUpdate);
			SqlUtil.closeInstance(psDelete);
		}
		
		logClauseAuditDetails (postTable, oClauseTable, auditEventRecord);
	}
	
	private void logClauseAuditDetails (DocumentClauseTable postTable, ClauseTable oClauseTable, AuditEventRecord auditEventRecord) {
		
		String sClauseLog = "";
		for (DocumentClauseRecord postRecord : postTable) {
			ClauseRecord oRecord = oClauseTable.findById(postRecord.getClauseId());
			if (oRecord != null) {
				if (oRecord.getClauseName() != null) {
					sClauseLog += oRecord.getClauseName() + "\r\n\t";
				}
			}
		}
		
		auditEventRecord.addAttribute("updated_clauses", sClauseLog);
	}
	
	// CJ-versioning
	// When the document is updated to reflect a new version, the answers must be updated also.
	public static void updateDocVersion(DocumentRecord docOrigRecord, DocumentRecord docUpdRecord, Connection conn) throws SQLException {
		PreparedStatement ps1 = null;
		String sSql = null;
		try {
			sSql = 
				"INSERT INTO " + DocumentClauseRecord.TABLE_DOCUMENT_CLAUSES +
				" (" + DocumentClauseRecord.FIELD_DOCUMENT_ID + ", " + 
					DocumentClauseRecord.FIELD_CLAUSE_ID  + ", " + 
					DocumentClauseRecord.FIELD_OPTIONAL_USER_ACTION_CODE  + ", " + // FIELD_IS_REMOVED_BY_USER 
					DocumentClauseRecord.FIELD_CREATED_AT  + ", " + 
					DocumentClauseRecord.FIELD_UPDATED_AT  + ", " + 
					DocumentClauseRecord.FIELD_IS_FILLIN_COMPLETED  + ") " + 
				" SELECT DISTINCT ?," +
					" C2." + ClauseRecord.FIELD_CLAUSE_ID + "," +
					" DC." + DocumentClauseRecord.FIELD_OPTIONAL_USER_ACTION_CODE + "," + // FIELD_IS_REMOVED_BY_USER
					" DC." + DocumentClauseRecord.FIELD_CREATED_AT + "," +
					" DC." + DocumentClauseRecord.FIELD_UPDATED_AT + "," +
					" DC." + DocumentClauseRecord.FIELD_IS_FILLIN_COMPLETED + 
				" FROM " + DocumentClauseRecord.TABLE_DOCUMENT_CLAUSES + "  DC" +
				" INNER JOIN " + ClauseRecord.TABLE_CLAUSES + " C1" +
					" ON DC." + DocumentClauseRecord.FIELD_CLAUSE_ID + " = C1." + ClauseRecord.FIELD_CLAUSE_ID +
					" AND C1." + ClauseRecord.FIELD_CLAUSE_VERSION_ID + " = ? " +
				" INNER JOIN " + ClauseRecord.TABLE_CLAUSES + " C2" +
					" ON C1." + ClauseRecord.FIELD_CLAUSE_NAME + " = C2." + ClauseRecord.FIELD_CLAUSE_NAME +
					" AND C2." + ClauseRecord.FIELD_CLAUSE_VERSION_ID + " = ? " +
				" WHERE DC." + DocumentClauseRecord.FIELD_DOCUMENT_ID + " = ?";
				
			ps1 = conn.prepareStatement(sSql);
			ps1.setInt(1, docUpdRecord.getId());
			ps1.setInt(2, docOrigRecord.getClauseVersionId());
			ps1.setInt(3, docUpdRecord.getClauseVersionId());
			ps1.setInt(4, docOrigRecord.getId());
			ps1.execute();
		}
		catch (Exception oError) {
			System.out.println("DocumentClauseTable.updateDocVersion(" + docOrigRecord.getId() + ", " + docUpdRecord.getId() + ") failed due to : " + oError.getMessage() + "\n" + sSql);
			throw oError;
		}
		finally {
			SqlUtil.closeInstance(ps1);
		}
	}
	
}
