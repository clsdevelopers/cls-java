package gov.dod.cls.db;

import gov.dod.cls.PageApi;
import gov.dod.cls.api.endpoints.ClausesEndpoint;
import gov.dod.cls.api.model.ApiDocumentQuestionAnswerItem;
import gov.dod.cls.bean.session.UserSession;
import gov.dod.cls.config.ClsConfig;
import gov.dod.cls.db.reports.DocGenerateMetaPdf;
import gov.dod.cls.db.reports.DocGenerateMetaWord;
import gov.dod.cls.model.JsonAble;
import gov.dod.cls.utils.CommonUtils;
import gov.dod.cls.utils.Pagination;
import gov.dod.cls.utils.sql.SqlUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.node.ArrayNode;
import org.codehaus.jackson.node.ObjectNode;

public class ClauseTable extends ArrayList<ClauseRecord> {
	
	private static final long serialVersionUID = -4431642528719867803L;

	// ----------------------------------------------------------
	public static final String PREFIX_DO_CLAUSE = "clause-";
	private static final String DO_CLAUSE_LIST = PREFIX_DO_CLAUSE + "list";
	public static final String DO_CLAUSE_DETAILS_GET = PREFIX_DO_CLAUSE + "detail-get";
	public static final String DO_CLAUSE_DETAILS_GET_PRINT = PREFIX_DO_CLAUSE + "detail-print";
	private static final String DO_CLAUSE_CURRENT_LIST = PREFIX_DO_CLAUSE + "current-list";
	private static final String DO_CLAUSE_PRINT = PREFIX_DO_CLAUSE + "print";

	public static void processRequest(String sDo, UserSession oUserSession, 
			HttpServletRequest req, HttpServletResponse resp) {
		
		// CJ-1301	- Check for Token
		Boolean hasToken = false;
		OauthInterviewRecord oAuth =  oUserSession.getOauthInterviewRecord();
		if (oAuth != null) {
			String sToken = oAuth.getInterviewToken();
			if ((sToken != null) && (sToken.length() > 0))
				hasToken = true;
		}
		
		
		if (!oUserSession.isSuperUser() && !hasToken  // CJ-680
			&& !sDo.equals (DO_CLAUSE_CURRENT_LIST) 
			&& !sDo.equals (DO_CLAUSE_PRINT)
			&& !sDo.equals (DO_CLAUSE_DETAILS_GET_PRINT)) { // Print clauses does not require admin.
			return;
		}
		
		
		switch (sDo) {
		case ClauseTable.DO_CLAUSE_LIST:
			ClauseTable.processList(req, resp);
			return;
		case ClauseTable.DO_CLAUSE_CURRENT_LIST:
			ClauseTable.processCurrentList(req, resp);
			return;
		case ClauseTable.DO_CLAUSE_PRINT:
			ClauseTable.printList(req, resp);
			return;
		case ClauseTable.DO_CLAUSE_DETAILS_GET_PRINT:
		case ClauseTable.DO_CLAUSE_DETAILS_GET:
			ClauseTable.getClauseDetails(sDo, oUserSession, req, resp);
			return;				
		}
	}
	
	// ----------------------------------------------------------
	private static final String SQL_LIST

		= "SELECT A." + ClauseRecord.FIELD_CLAUSE_ID
		+ ", A." + ClauseRecord.FIELD_CLAUSE_VERSION_ID
		+ ", C." + ClauseVersionRecord.FIELD_CLAUSE_VERSION_NAME
		+ ", A." + ClauseRecord.FIELD_CLAUSE_NAME
		+ ", A." + ClauseRecord.FIELD_INCLUSION_CD
		+ ", A." + ClauseRecord.FIELD_CLAUSE_TITLE
		+ ", A." + ClauseRecord.FIELD_CLAUSE_DATA
		+ ", A." + ClauseRecord.FIELD_CLAUSE_URL
		+ ", A." + ClauseRecord.FIELD_CLAUSE_SECTION_ID
		+ ", B." + ClauseSectionRecord.FIELD_CLAUSE_SECTION_CODE + " AS Section"
		+ ", A." + ClauseRecord.FIELD_EFFECTIVE_DATE
		+ ", A." + ClauseRecord.FIELD_REGULATION_ID
		+ ", A." + ClauseRecord.FIELD_CLAUSE_CONDITIONS
		+ ", A." + ClauseRecord.FIELD_CLAUSE_RULE
		+ ", A." + ClauseRecord.FIELD_ADDITIONAL_CONDITIONS
		+ ", A." + ClauseRecord.FIELD_IS_ACTIVE
		+ ", A." + ClauseRecord.FIELD_IS_EDITABLE
		+ ", A." + ClauseRecord.FIELD_EDITABLE_REMARKS
		+ ", A." + ClauseRecord.FIELD_IS_OPTIONAL
		+ ", A." + ClauseRecord.FIELD_PROVISON_OR_CLAUSE
		+ ", A." + ClauseRecord.FIELD_IS_BASIC_CLAUSE
		+ ", A." + ClauseRecord.FIELD_IS_DFARS_ACTIVITY
		+ ", A." + ClauseRecord.FIELD_CREATED_AT
		+ ", A." + ClauseRecord.FIELD_UPDATED_AT
		//+ ", A." + ClauseRecord.FIELD_ADDITIONAL_CONDITIONS
		// + ", CONCAT(B." + ClauseSectionRecord.FIELD_CLAUSE_SECTION_CODE + ", '. ', B." + ClauseSectionRecord.FIELD_CLAUSE_SECTION_HEADER + ") AS Section"
		+ " FROM " + ClauseRecord.TABLE_CLAUSES + " A"
		+ " LEFT OUTER JOIN " + ClauseSectionRecord.TABLE_CLAUSE_SECTIONS + " B"
		+ " ON (B." + ClauseSectionRecord.FIELD_CLAUSE_SECTION_ID + " = A." + ClauseRecord.FIELD_CLAUSE_SECTION_ID + ")"
		+ " JOIN " + ClauseVersionRecord.TABLE_CLAUSE_VERSIONS + " C"
		+ " ON (C." + ClauseVersionRecord.FIELD_CLAUSE_VERSION_ID + " = A." + ClauseRecord.FIELD_CLAUSE_VERSION_ID + ")"
		;
	
	
	// CJ-474, list the searched clause first
	private static final String SQL_LIST2

	= "SELECT A." + ClauseRecord.FIELD_CLAUSE_ID
	+ ", A." + ClauseRecord.FIELD_CLAUSE_VERSION_ID
	+ ", C." + ClauseVersionRecord.FIELD_CLAUSE_VERSION_NAME
	+ ", A." + ClauseRecord.FIELD_CLAUSE_NAME
	+ ", A." + ClauseRecord.FIELD_INCLUSION_CD
	+ ", A." + ClauseRecord.FIELD_CLAUSE_TITLE
	+ ", A." + ClauseRecord.FIELD_CLAUSE_DATA
	+ ", A." + ClauseRecord.FIELD_CLAUSE_URL
	+ ", A." + ClauseRecord.FIELD_CLAUSE_SECTION_ID
	+ ", B." + ClauseSectionRecord.FIELD_CLAUSE_SECTION_CODE + " AS Section"
	+ ", A." + ClauseRecord.FIELD_EFFECTIVE_DATE
	+ ", A." + ClauseRecord.FIELD_REGULATION_ID
	+ ", A." + ClauseRecord.FIELD_CLAUSE_CONDITIONS
	+ ", A." + ClauseRecord.FIELD_CLAUSE_RULE
	+ ", A." + ClauseRecord.FIELD_ADDITIONAL_CONDITIONS
	+ ", A." + ClauseRecord.FIELD_IS_ACTIVE
	+ ", A." + ClauseRecord.FIELD_IS_EDITABLE
	+ ", A." + ClauseRecord.FIELD_EDITABLE_REMARKS
	+ ", A." + ClauseRecord.FIELD_IS_OPTIONAL
	+ ", A." + ClauseRecord.FIELD_PROVISON_OR_CLAUSE
	+ ", A." + ClauseRecord.FIELD_IS_BASIC_CLAUSE
	+ ", A." + ClauseRecord.FIELD_IS_DFARS_ACTIVITY
	+ ", A." + ClauseRecord.FIELD_CREATED_AT
	+ ", A." + ClauseRecord.FIELD_UPDATED_AT
	+ ", CASE WHEN LOWER(A.clause_name) LIKE ? THEN 1 ELSE 0 END as 'name_match' "
	+ " FROM " + ClauseRecord.TABLE_CLAUSES + " A"
	+ " LEFT OUTER JOIN " + ClauseSectionRecord.TABLE_CLAUSE_SECTIONS + " B"
	+ " ON (B." + ClauseSectionRecord.FIELD_CLAUSE_SECTION_ID + " = A." + ClauseRecord.FIELD_CLAUSE_SECTION_ID + ")"
	+ " JOIN " + ClauseVersionRecord.TABLE_CLAUSE_VERSIONS + " C"
	+ " ON (C." + ClauseVersionRecord.FIELD_CLAUSE_VERSION_ID + " = A." + ClauseRecord.FIELD_CLAUSE_VERSION_ID + ")"
	;
	
	private static void processList(HttpServletRequest req, HttpServletResponse resp) {
		String query = req.getParameter("query");
		String clsVersion = req.getParameter("cls_version");
		String section = req.getParameter("section");
		String sort_field = req.getParameter("sort_field");
		String sort_order = req.getParameter("sort_order");

		Pagination pagination = new Pagination(req);
		try {
			ClauseTable.loadPagination(false, pagination, query, clsVersion, section, sort_order, sort_field, false, null);
			pagination.send(resp);
		}
		catch (Exception oError) {
			oError.printStackTrace();
		}
//		finally {
//			SqlUtil.closeInstance(rs1, ps1, conn);
//		}
	}
	
	// Printable Metadata always uses the latest/active version.
	private static void processCurrentList(HttpServletRequest req, HttpServletResponse resp) {
		
		String query = req.getParameter("query");
		String section = req.getParameter("section");
		String sort_field = req.getParameter("sort_field");
		String sort_order = req.getParameter("sort_order");

		Integer iVersion = ClauseVersionTable.getActiveVersionId();
		String clsVersion = null;
		if (iVersion!=null)
			clsVersion = iVersion.toString();
		
		Pagination pagination = new Pagination(req);
		try {
			ClauseTable.loadPagination(false, pagination, query, clsVersion, section, sort_order, sort_field, false, null);
			pagination.send(resp);
		}
		catch (Exception oError) {
			oError.printStackTrace();
		}
	}
	
	// Printable Metadata always uses the latest/active version.
	
	private static void printList(HttpServletRequest req, HttpServletResponse resp) {
		
		String docType = req.getParameter("docType");
		
		String clsList = req.getParameter("clause_ids");
		clsList = clsList.replaceAll("\\[", "");
		clsList = clsList.replaceAll("]", "");
		clsList = clsList.replaceAll("\"", "");
		clsList = clsList.replaceAll("-", ",");
		if ((clsList == null) || (clsList.length() == 0))
				return;
		String sWhere = " WHERE A." + ClauseRecord.FIELD_CLAUSE_ID + " IN (" + clsList.toString() + ")";
		
		
		ArrayList<ClauseRecord> clsArray = new ArrayList<ClauseRecord>();
		Connection conn = null;

		PreparedStatement ps1 = null;
		ResultSet rs1 = null;
		try {
			
			conn = ClsConfig.getDbConnection();
			ClausePrescriptionTable clausePrescriptions = ClausePrescriptionTable.getInstance(conn);
			ClauseFillInTable clauseFillInTable = ClauseFillInTable.getInstance(conn);
			
			String sSql = ClauseRecord.SQL_SELECT + sWhere;
			
			ps1 = conn.prepareStatement(sSql);
			rs1 = ps1.executeQuery();
			while (rs1.next()) {
				ClauseRecord clauseRecord = new ClauseRecord();
				clauseRecord.read(rs1);
				
				clauseRecord.setClausePrescriptionTable(clausePrescriptions.subsetByClauseId(clauseRecord.getId()));
				clauseRecord.setClauseFillInTable(clauseFillInTable.subsetByClauseId(clauseRecord.getId()));
				
				clsArray.add(clauseRecord);
			}
			
			if (docType.equals("word"))	// word doc
				DocGenerateMetaWord.downloadDoc (clsArray,req, resp);
			else // pdf
				DocGenerateMetaPdf.downloadDoc (clsArray,req, resp);
			
		}
		catch (Exception oError) {
			oError.printStackTrace();
		}
		finally {
			SqlUtil.closeInstance(rs1, ps1);
			SqlUtil.closeInstance(conn);
		}
	}
	
	public static void loadPaginationForApi(Pagination pagination, boolean pbApiVer1, String clsVersion, Connection conn) 
			throws SQLException {
		ClauseTable.loadPagination(true, pagination,
				null, // query
				clsVersion, // clsVersion, 
				null, // section, 
				null, // sort_order
				null, // sort_field
				pbApiVer1, conn);
	}

	public static void loadPagination(Pagination pagination, String query, String section,
			String sort_order, String sort_field, Connection conn) 
			throws SQLException {
		ClauseTable.loadPagination(false, pagination, null, null, null, null, null, false, conn);
	}
	
	public static void loadPagination(boolean forApi, Pagination pagination, String query, String clsVersion, String section,
			String sort_order, String sort_field, boolean pbApiVer1, Connection conn) 
			throws SQLException {
		pagination.addDataSource(ClauseRecord.TABLE_CLAUSES);
		boolean bCreateConnection = (conn == null);
		PreparedStatement ps1 = null;
		ResultSet rs1 = null;
		try {
			if (bCreateConnection)
				conn = ClsConfig.getDbConnection();
			String sSql = "SELECT COUNT(A." + ClauseRecord.FIELD_CLAUSE_ID + ")"
					+ " FROM " + ClauseRecord.TABLE_CLAUSES + " A";
			if (CommonUtils.isNotEmpty(clsVersion)) {
				sSql += " JOIN " + ClauseVersionRecord.TABLE_CLAUSE_VERSIONS + " C"
						+ " ON (C." + ClauseVersionRecord.FIELD_CLAUSE_VERSION_ID + " = A." + ClauseRecord.FIELD_CLAUSE_VERSION_ID + ")";
			}
			String operator = " WHERE ";
			if (CommonUtils.isNotEmpty(query)) {
				sSql += operator + "(LOWER(A." + ClauseRecord.FIELD_CLAUSE_NAME + ") LIKE ?" 
					+ " OR LOWER(A." + ClauseRecord.FIELD_CLAUSE_TITLE + ") LIKE ?"
					+ " OR LOWER(A." + ClauseRecord.FIELD_CLAUSE_CONDITIONS + ") LIKE ?)";
				operator = " AND ";
				query = "%" + query.toLowerCase() + "%";
			}
			if (CommonUtils.isNotEmpty(section)) {
				sSql += operator + "EXISTS (SELECT B." + ClauseSectionRecord.FIELD_CLAUSE_SECTION_ID
					+ " FROM " + ClauseSectionRecord.TABLE_CLAUSE_SECTIONS + " B"
					+ " WHERE (B." + ClauseSectionRecord.FIELD_CLAUSE_SECTION_ID + " = A." + ClauseRecord.FIELD_CLAUSE_SECTION_ID + ")"
					+ " AND B." + ClauseSectionRecord.FIELD_CLAUSE_SECTION_CODE + " = ?)";
				operator = " AND ";
			}
			if (CommonUtils.isNotEmpty(clsVersion)) {
				sSql += operator + "C." + ClauseVersionRecord.FIELD_CLAUSE_VERSION_ID + " = ?";
				operator = " AND ";
			}
			
			ps1 = conn.prepareStatement(sSql);
			int iParam = 1;
			if (CommonUtils.isNotEmpty(query)) {
				ps1.setString(iParam++, query);
				ps1.setString(iParam++, query);
				ps1.setString(iParam++, query);
			}
			if (CommonUtils.isNotEmpty(section))
				ps1.setString(iParam++, section);
			if (CommonUtils.isNotEmpty(clsVersion))
				ps1.setString(iParam++, clsVersion);
			rs1 = ps1.executeQuery();
			boolean isAcceptableRange = false;
			if (rs1.next())
				isAcceptableRange = pagination.isAcceptedRange(rs1.getInt(1));
			
			if (isAcceptableRange) {
				SqlUtil.closeInstance(rs1);
				rs1 = null;
				SqlUtil.closeInstance(ps1);
				ps1 = null;
				
				if (CommonUtils.isNotEmpty(query)) 
					sSql = SQL_LIST2;	// CJ-474, list the searched clause first
				else 
					sSql = SQL_LIST;
				operator = " WHERE ";
				if (CommonUtils.isNotEmpty(query)) {
					sSql += operator + "(LOWER(A." + ClauseRecord.FIELD_CLAUSE_NAME + ") LIKE ?" 
						+ " OR LOWER(A." + ClauseRecord.FIELD_CLAUSE_TITLE + ") LIKE ?"
						+ " OR LOWER(A." + ClauseRecord.FIELD_CLAUSE_CONDITIONS + ") LIKE ?)";
					operator = " AND ";
				}
				if (CommonUtils.isNotEmpty(section)) {
					sSql += operator + "B." + ClauseSectionRecord.FIELD_CLAUSE_SECTION_CODE + " = ?";
					operator = " AND ";
				}
				if (CommonUtils.isNotEmpty(clsVersion)) {
					sSql += operator + "C." + ClauseVersionRecord.FIELD_CLAUSE_VERSION_ID + " = ?";
				}
				String sAsc = ("desc".equals(sort_order) ? " DESC" : "");
				String orderField = (forApi ? ClauseRecord.FIELD_CLAUSE_ID : ClauseRecord.FIELD_CLAUSE_NAME);
				if ("clause_title".equals(sort_field))
					orderField = ClauseRecord.FIELD_CLAUSE_TITLE;
				// CJ-474, list the searched clause first
				if (CommonUtils.isNotEmpty(query))
					sSql += " ORDER BY name_match desc, " + orderField + sAsc;
				else 
					sSql += " ORDER BY " + orderField + sAsc;
				
				ps1 = conn.prepareStatement(sSql + pagination.sqlLimit());
				iParam = 1;
				if (CommonUtils.isNotEmpty(query)) {
					ps1.setString(iParam++, query); // CJ-474, list the searched clause first
					ps1.setString(iParam++, query);
					ps1.setString(iParam++, query);
					ps1.setString(iParam++, query);
				}
				if (CommonUtils.isNotEmpty(section))
					ps1.setString(iParam++, section);
				if (CommonUtils.isNotEmpty(clsVersion))
					ps1.setString(iParam++, clsVersion);
				
				rs1 = ps1.executeQuery();
				while (rs1.next()) {
					ObjectNode json = ClauseTable.genJson(forApi, pbApiVer1, false, rs1, pagination.getMapper());
					pagination.getArrayNode().add(json);
					pagination.incCount();
				}
			}
		}
		finally {
			SqlUtil.closeInstance(rs1, ps1);
			if (bCreateConnection)
				SqlUtil.closeInstance(conn);
		}
	}
	
	private static ObjectNode genJson(boolean forApi, boolean pbApiVer1, boolean includeClauseData, ResultSet rs1, ObjectMapper mapper) throws SQLException {
		ObjectNode json = mapper.createObjectNode();
		Integer id = rs1.getInt(ClauseRecord.FIELD_CLAUSE_ID);
		if (forApi && pbApiVer1) {
			json.put("id", id);
			json.put("clause_name", rs1.getString(ClauseRecord.FIELD_CLAUSE_NAME));
			json.put("effective_date", CommonUtils.toYearMonthDay(rs1.getDate(ClauseRecord.FIELD_EFFECTIVE_DATE)));
			json.put("section", rs1.getString("Section"));
			json.put("inclusion", rs1.getString(ClauseRecord.FIELD_INCLUSION_CD));
			json.put("active", rs1.getBoolean(ClauseRecord.FIELD_IS_ACTIVE));
			json.put("start_date", (String)null);
			json.put("end_date", (String)null);
			json.put("prescription_text", (String)null);
			json.put("clause_text", ""); // rs1.getString(ClauseRecord.FIELD_CLAUSE_DATA)
            json.put("url", rs1.getString(ClauseRecord.FIELD_CLAUSE_URL));
            json.put(ClauseVersionRecord.FIELD_CLAUSE_VERSION_NAME, rs1.getString(ClauseVersionRecord.FIELD_CLAUSE_VERSION_NAME));
			json.put(ClauseRecord.FIELD_CLAUSE_VERSION_ID, rs1.getInt(ClauseRecord.FIELD_CLAUSE_VERSION_ID));
			json.put(ClauseRecord.FIELD_IS_EDITABLE, rs1.getBoolean(ClauseRecord.FIELD_IS_EDITABLE));
			json.put(ClauseRecord.FIELD_EDITABLE_REMARKS, rs1.getString(ClauseRecord.FIELD_EDITABLE_REMARKS));
			json.put(ClauseRecord.FIELD_PROVISON_OR_CLAUSE, rs1.getString(ClauseRecord.FIELD_PROVISON_OR_CLAUSE));
			json.put(ClauseRecord.FIELD_IS_BASIC_CLAUSE, rs1.getBoolean(ClauseRecord.FIELD_IS_BASIC_CLAUSE));

			// CJ-320
			json.put(ClauseRecord.FIELD_CLAUSE_TITLE, ClauseRecord.removeDotAtEnd(rs1.getString(ClauseRecord.FIELD_CLAUSE_TITLE))); // CJ-815
			json.put(ClauseRecord.FIELD_CLAUSE_SECTION_ID, rs1.getInt(ClauseRecord.FIELD_CLAUSE_SECTION_ID));
			json.put(ClauseRecord.FIELD_REGULATION_ID, rs1.getInt(ClauseRecord.FIELD_REGULATION_ID));
			json.put(ClauseRecord.FIELD_CLAUSE_CONDITIONS, rs1.getString(ClauseRecord.FIELD_CLAUSE_CONDITIONS));
			json.put(ClauseRecord.FIELD_CLAUSE_RULE, rs1.getString(ClauseRecord.FIELD_CLAUSE_RULE));
			json.put(ClauseRecord.FIELD_ADDITIONAL_CONDITIONS, rs1.getString(ClauseRecord.FIELD_ADDITIONAL_CONDITIONS));
			json.put(ClauseRecord.FIELD_IS_OPTIONAL, rs1.getBoolean(ClauseRecord.FIELD_IS_OPTIONAL));
			json.put(ClauseRecord.FIELD_IS_DFARS_ACTIVITY, rs1.getBoolean(ClauseRecord.FIELD_IS_DFARS_ACTIVITY));
			json.put(ClauseRecord.FIELD_CREATED_AT, CommonUtils.toZulu(rs1.getDate(ClauseRecord.FIELD_CREATED_AT)));
			json.put(ClauseRecord.FIELD_UPDATED_AT, CommonUtils.toZulu(rs1.getDate(ClauseRecord.FIELD_UPDATED_AT)));
		} else {
			if (forApi) {
				json.put("id", id); // ClauseRecord.FIELD_CLAUSE_ID
				json.put("section", rs1.getString("Section"));
				json.put("url", ClausesEndpoint.getUrl(pbApiVer1, id));
				json.put(ClauseRecord.FIELD_EFFECTIVE_DATE, CommonUtils.toMonthYear(rs1.getDate(ClauseRecord.FIELD_EFFECTIVE_DATE)));
			} else {
				json.put(ClauseRecord.FIELD_CLAUSE_ID, id);
				json.put("Section", rs1.getString("Section"));
				json.put(ClauseRecord.FIELD_EFFECTIVE_DATE, CommonUtils.toJsonDate(rs1.getDate(ClauseRecord.FIELD_EFFECTIVE_DATE)));
			}
			json.put(ClauseRecord.FIELD_CLAUSE_NAME, rs1.getString(ClauseRecord.FIELD_CLAUSE_NAME));
			json.put(ClauseRecord.FIELD_INCLUSION_CD, rs1.getString(ClauseRecord.FIELD_INCLUSION_CD));
			json.put(ClauseRecord.FIELD_IS_ACTIVE, rs1.getBoolean(ClauseRecord.FIELD_IS_ACTIVE));
			json.put(ClauseRecord.FIELD_PROVISON_OR_CLAUSE, rs1.getString(ClauseRecord.FIELD_PROVISON_OR_CLAUSE));
			json.put(ClauseRecord.FIELD_CLAUSE_URL, rs1.getString(ClauseRecord.FIELD_CLAUSE_URL));
			if (includeClauseData)
				json.put(ClauseRecord.FIELD_CLAUSE_DATA, rs1.getString(ClauseRecord.FIELD_CLAUSE_DATA));
			if (forApi)
				json.put(ClauseRecord.FIELD_CLAUSE_TITLE, ClauseRecord.removeDotAtEnd(rs1.getString(ClauseRecord.FIELD_CLAUSE_TITLE))); // CJ-815
			else
				json.put(ClauseRecord.FIELD_CLAUSE_TITLE, rs1.getString(ClauseRecord.FIELD_CLAUSE_TITLE));
			json.put(ClauseRecord.FIELD_CLAUSE_CONDITIONS, rs1.getString(ClauseRecord.FIELD_CLAUSE_CONDITIONS));
			json.put(ClauseRecord.FIELD_CLAUSE_RULE, rs1.getString(ClauseRecord.FIELD_CLAUSE_RULE));
            json.put(ClauseVersionRecord.FIELD_CLAUSE_VERSION_NAME, rs1.getString(ClauseVersionRecord.FIELD_CLAUSE_VERSION_NAME));

			// CJ-320
			json.put(ClauseRecord.FIELD_CLAUSE_VERSION_ID, rs1.getInt(ClauseRecord.FIELD_CLAUSE_VERSION_ID));
			json.put(ClauseRecord.FIELD_CLAUSE_SECTION_ID, rs1.getInt(ClauseRecord.FIELD_CLAUSE_SECTION_ID));
			json.put(ClauseRecord.FIELD_REGULATION_ID, rs1.getInt(ClauseRecord.FIELD_REGULATION_ID));
			json.put(ClauseRecord.FIELD_ADDITIONAL_CONDITIONS, rs1.getString(ClauseRecord.FIELD_ADDITIONAL_CONDITIONS));
			json.put(ClauseRecord.FIELD_IS_EDITABLE, rs1.getBoolean(ClauseRecord.FIELD_IS_EDITABLE));
			json.put(ClauseRecord.FIELD_EDITABLE_REMARKS, rs1.getString(ClauseRecord.FIELD_EDITABLE_REMARKS));
			json.put(ClauseRecord.FIELD_IS_OPTIONAL, rs1.getBoolean(ClauseRecord.FIELD_IS_OPTIONAL));
			json.put(ClauseRecord.FIELD_IS_DFARS_ACTIVITY, rs1.getBoolean(ClauseRecord.FIELD_IS_DFARS_ACTIVITY));
			json.put(ClauseRecord.FIELD_IS_BASIC_CLAUSE, rs1.getBoolean(ClauseRecord.FIELD_IS_BASIC_CLAUSE));
			if (forApi) {
				json.put(ClauseRecord.FIELD_CREATED_AT, CommonUtils.toZulu(rs1.getDate(ClauseRecord.FIELD_CREATED_AT)));
				json.put(ClauseRecord.FIELD_UPDATED_AT, CommonUtils.toZulu(rs1.getDate(ClauseRecord.FIELD_UPDATED_AT)));
			} else {
				json.put(ClauseRecord.FIELD_CREATED_AT, CommonUtils.toJsonDate(rs1.getDate(ClauseRecord.FIELD_CREATED_AT)));
				json.put(ClauseRecord.FIELD_UPDATED_AT, CommonUtils.toJsonDate(rs1.getDate(ClauseRecord.FIELD_UPDATED_AT)));
			}
		}
		return json;
	}
	
	public static ClauseTable getByClauseVersion(int clauseVersionId, Connection conn, boolean forDownload) { // forDownload for CJ-264

		if (ClauseTable.instance == null) {
			if (ClauseVersionTable.getActiveVersionId() == clauseVersionId) {
				ClauseTable.instance = getByClauseVersion(clauseVersionId, false, conn, forDownload);
			}
			else {
				return ClauseTable.getByClauseVersion(clauseVersionId, false, conn, forDownload);
			}
		}
		else
		{
			if (ClauseTable.instance.getClauseVersionId() != clauseVersionId)
				return ClauseTable.getByClauseVersion(clauseVersionId, false, conn, forDownload);
			if (ClauseTable.needToRefreh(conn)) 
				ClauseTable.instance = ClauseTable.getByClauseVersion(clauseVersionId, false, conn, forDownload);
			
		}

		return ClauseTable.instance;
	}
	
	public static ClauseTable getByClauseVersion(int clauseVersionId, boolean bLinkedAward, Connection conn, boolean forDownload) { // forDownload for CJ-264
		ClauseTable clauseTable = new ClauseTable();
		clauseTable.loadByClauseVersion(clauseVersionId, bLinkedAward, conn, forDownload);
		return clauseTable;
	}
	
	public static ClauseRecord getRecord(int id) throws SQLException {
		ClauseRecord result = null;
		Connection conn = null;
		PreparedStatement ps1 = null;
		ResultSet rs1 = null;
		try {
			conn = ClsConfig.getDbConnection();
			String sSql = ClauseRecord.SQL_SELECT
					+ " WHERE " + ClauseRecord.FIELD_CLAUSE_ID + " = ?";
			ps1 = conn.prepareStatement(sSql);
			ps1.setInt(1, id);
			rs1 = ps1.executeQuery();
			if (rs1.next()) {
				result = new ClauseRecord();
				result.read(rs1);
			}
		}
		finally {
			SqlUtil.closeInstance(rs1, ps1, conn);
		}
		return result;
	}

	public static ObjectNode getRecordJson(int id, boolean forApi, boolean pbApiVer1, Connection conn) {
		ObjectNode json = null;
		boolean bCreateConnection = (conn == null);
		PreparedStatement ps1 = null;
		ResultSet rs1 = null;
		try {
			if (bCreateConnection)
				conn = ClsConfig.getDbConnection();
			
			ClauseFillInTable clauseFillInTable = null;
			if (pbApiVer1 == false)
				clauseFillInTable = ClauseFillInTable.getInstance(conn);
			String sSql = SQL_LIST
					+ " WHERE " + ClauseRecord.FIELD_CLAUSE_ID + " = ?";
			ps1 = conn.prepareStatement(sSql);
			ps1.setInt(1, id);
			rs1 = ps1.executeQuery();
			if (rs1.next()) {
				ObjectMapper mapper = new ObjectMapper();
				json = mapper.createObjectNode();
				ObjectNode clauseNode = ClauseTable.genJson(forApi, pbApiVer1, true, rs1, mapper);
				if (pbApiVer1 == false) {
					ClauseFillInTable subset = clauseFillInTable.subsetByClauseId(id);
					if ((subset != null) && (subset.size() > 0)) {
						ArrayNode arrayNode = json.arrayNode();
						for (ClauseFillInRecord oRecord : subset) {
							ObjectNode fillinNode = arrayNode.objectNode();
							oRecord.populateJsonForApi(fillinNode, null);
							arrayNode.add(fillinNode);
						}
						clauseNode.put(ClauseFillInRecord.TABLE_CLAUSE_FILL_INS.toLowerCase(), arrayNode);
					}
				}
				json.put("clause", clauseNode);
			}
		} catch (Exception oError) {
			oError.printStackTrace();
		}
		finally {
			SqlUtil.closeInstance(rs1, ps1);
			if (bCreateConnection)
				SqlUtil.closeInstance(conn);
		}
		return json;
	}
	
	public static ObjectNode getRecordContentJson(int id) {
		ObjectNode result = null;
		try {
			ClauseRecord oRecord = ClauseTable.getRecord(id);
			if (oRecord != null) {
				ObjectMapper mapper = new ObjectMapper();
				result = mapper.createObjectNode();
				result.put(ClauseRecord.FIELD_CLAUSE_DATA, oRecord.getClauseData());
			}
		} catch (Exception oError) {
			oError.printStackTrace();
		}
		return result;
	}
	
	private static void getClauseDetails(String sDo, UserSession oUserSession, 
			HttpServletRequest req, HttpServletResponse resp) {
		Integer id = CommonUtils.toInteger(req.getParameter("id"));
		String errorMessage = null;
		ClauseRecord clauseRecord = null;
		if (id != null) {
			try {
				clauseRecord = ClauseRecord.getRecord(id);
				if (clauseRecord == null)
					errorMessage = "The requested record is not found.";
			} catch (Exception oError) {
				errorMessage = "Unable to retrieve record. Error: " + oError.getMessage();
			}
		} else
			errorMessage = "Unauthorized request";
		String response;
		if (errorMessage == null) {
			ObjectNode objectNode = (ObjectNode)clauseRecord.toJson();
			response = JsonAble.jsonResult(JsonAble.STATUS_SUCCESS, objectNode, null);
		} else
			response = JsonAble.jsonResult(JsonAble.STATUS_FAIL, null, errorMessage);
		PageApi.send(resp, JsonAble.JSON_RESPONSE_FORMAT, response);
	}

	// ========================================================
	private volatile static ClauseTable instance = null;
	
	public synchronized static ClauseTable getInstance(Integer pClauseVersionId, Connection conn) {

		if (ClauseTable.instance == null) {
			ClauseTable.instance = new ClauseTable();
			ClauseTable.instance.refresh(pClauseVersionId, conn);
		}
		else
		{
			ClauseTable.instance.refreshWhenNeeded(pClauseVersionId, conn);
		}
		return ClauseTable.instance;
	}
	
	private void refresh(Integer pClauseVersionId, Connection conn) {
		
		ClauseVersionTable clauseVersionTable = ClauseVersionTable.getInstance(conn);
		ClauseVersionRecord clauseVersionRecord = clauseVersionTable.findActive();
		if (clauseVersionRecord == null) {
			if (pClauseVersionId != null)
				ClauseTable.instance.loadByClauseVersion(pClauseVersionId, conn);
		} else {
			int iNewclauseVersionId = clauseVersionRecord.getId().intValue();
			if (pClauseVersionId == null) {
				ClauseTable.instance = ClauseTable.instance;
				if (!ClauseTable.instance.isSameVersion(clauseVersionRecord, conn))
					ClauseTable.instance.loadByClauseVersion(iNewclauseVersionId, conn);
			} else {
				if (pClauseVersionId.intValue() == iNewclauseVersionId) {
					ClauseTable.instance = ClauseTable.instance;
					if (ClauseTable.instance.clauseVersionId == -1)
						ClauseTable.instance.loadByClauseVersion(iNewclauseVersionId, conn);
				} else {
					ClauseTable.instance.loadByClauseVersion(pClauseVersionId, conn);
				}
			}
		}
		
	}
	
	public static ArrayNode getJson(Integer pClauseVersionId, boolean forInterview, ObjectMapper mapper) {
		ClauseTable clauseSectionTable = ClauseTable.getInstance(pClauseVersionId, null);
		return clauseSectionTable.toJson(mapper, forInterview);
	}
	
	// public static 

	// =============================================================================
	private int clauseVersionId = -1;
	private static Date refreshed;
	private Exception lastError;
	private ClauseSectionTable clauseSectionTable = null;
	
	private boolean isSameVersion(ClauseVersionRecord clauseVersionRecord, Connection conn) {
		if (clauseVersionRecord == null)
			return false;
		int iNewclauseVersionId = clauseVersionRecord.getId().intValue();
		if ((this.clauseVersionId == -1) || (this.clauseVersionId != iNewclauseVersionId)) {
			return false;
		} else
			return true;
	}
	
	public boolean loadByClauseVersion(int clauseVersionId, Connection conn) {
		return loadByClauseVersion(clauseVersionId, false, conn, false);
	}
	
	// @ SuppressWarnings("resource")
	public boolean loadByClauseVersion(int clauseVersionId, boolean bLinkedAward, Connection conn, boolean forDownload) { // forDownload for CJ-264
		boolean result = false;
		
		this.clauseVersionId = clauseVersionId;
		
		String sSql = ClauseRecord.SQL_SELECT
				+ " WHERE A." + ClauseRecord.FIELD_CLAUSE_VERSION_ID + " = ?"
				+ " AND A." + ClauseRecord.FIELD_IS_ACTIVE + " = 1 "
				+ ClauseRecord.SQL_ORDER_BY_DEFAULT;
		
		boolean bCreateConnection = (conn == null);
		PreparedStatement ps1 = null;
		ResultSet rs1 = null;
		try {
			if (bCreateConnection)
				conn = ClsConfig.getDbConnection();
			
			ClauseSectionTable oClauseSectionTable = ClauseSectionTable.getInstance(conn);

			ClausePrescriptionTable clausePrescriptions = ClausePrescriptionTable.getInstance(conn);
			ClauseFillInTable clauseFillInTable = ClauseFillInTable.getInstance(conn);
			
			ps1 = conn.prepareStatement(sSql);
			ps1.setInt(1, clauseVersionId);
			rs1 = ps1.executeQuery();
			this.clear();
			while (rs1.next()) {
				ClauseRecord clauseRecord = new ClauseRecord();
				clauseRecord.read(rs1);
				if (bLinkedAward) {
					if (oClauseSectionTable.isSolicitationSelection(clauseRecord.getClauseSectionId()))
						continue;
				}
				int iClauseId = clauseRecord.getId().intValue();
				clauseRecord.setClausePrescriptionTable(clausePrescriptions.subsetByClauseId(iClauseId));
				clauseRecord.setClauseFillInTable(clauseFillInTable.subsetByClauseId(iClauseId));
				this.add(clauseRecord);
			}
			
			// Collections.sort(this, ClauseSorterDefault); // CJ-746: Replaced by ClauseRecord.SQL_ORDER_BY_DEFAULT
			
			this.refreshed = getReferHistoryDate(conn); // CommonUtils.getNow();
			result = true;
			System.out.println("ClauseTable loaded for version(" + clauseVersionId + ") at " + CommonUtils.getNow().toString());
		}
		catch (Exception oError) {
			this.lastError = oError;
			oError.printStackTrace();
		}
		finally {
			SqlUtil.closeInstance(rs1, ps1);
			if (bCreateConnection)
				SqlUtil.closeInstance(conn);
		}	
		return result;
	}
	
	public ClauseRecord findById(int clauseId) {
		for (ClauseRecord clauseRecord : this) {
			if (clauseRecord.getId().intValue() == clauseId)
				return clauseRecord;
		}
		return null;
	}
	
	public ClauseFillInRecord findFillInById(int fillInId) {
		ClauseFillInRecord result;
		for (ClauseRecord clauseRecord : this) {
			result = clauseRecord.findFillInById(fillInId);
			if (result != null)
				return result;
		}
		return null;
	}

	public ClauseRecord findByName(String clauseName) {
		for (ClauseRecord clauseRecord : this) {
			if (clauseRecord.getClauseName().equalsIgnoreCase(clauseName))
				return clauseRecord;
		}
		return null;
	}

	public ClauseRecord findByName(String clauseName, ClauseTable oClausesToAdvoid) {
		for (ClauseRecord clauseRecord : this) {
			if (clauseRecord.getClauseName().equalsIgnoreCase(clauseName)) {
				if (!oClausesToAdvoid.contains(clauseRecord))
					return clauseRecord;
			}
		}
		return null;
	}

	public ClauseTable subsetBySection(int sectionId, boolean forDownload) {
		ClauseTable result = new ClauseTable();
		for (ClauseRecord clauseRecord : this) {
			if (clauseRecord.isSameSection(sectionId))
				result.add(clauseRecord);
		}
		
		/*
		if (forDownload)
			Collections.sort(result, ClauseSorterForDownload);
		else
			Collections.sort(result, ClauseSorterDefault);
		*/
		
		return result;
	}
	
	/* CJ-746 Removed
	public void sortByClauseName() { // CJ-545
		Collections.sort(this, ClauseSorterByName);
	}

	// ======================================================================================
    // Comparator for sorting the list by selected clause fields
	// Note: SQL Order By sort would not work, because of special 
	// sorting for the ClauseName field.
    public static Comparator<ClauseRecord> ClauseSorterDefault = new Comparator<ClauseRecord>() {

		public int compare(ClauseRecord c1, ClauseRecord c2) {
			String section1 = c1.getSectionCode();
			String section2 = c2.getSectionCode();
			
			// Reference or Full
			String ref1 = c1.getInclusionName();
			String ref2 = c2.getInclusionName();
			
			// FAR or DFAR
			String reg1 = c1.getRegulationName();
			String reg2 = c2.getRegulationName();
				   
			String cName1 = c1.getClauseName();
			String cName2 = c2.getClauseName();
	
			// New Sort Order - Section, FAR, Reference, Name
			int result = section1.compareTo(section2);	// sort by Section Code
			if (result == 0)
				result = reg2.compareTo(reg1);	// sort by FAR, then DFAR
			if (result == 0)
				result = ref2.compareTo(ref1);	// sort by Reference or Full
			if (result == 0)
			   result = ClauseTable.compareClauseName(cName1, cName2); // compareComponents
			   //result = cName1.compareTo(cName2);	// sort by component name
		   
			return result;
		}
	};

	// ======================================================================================
    // Sort Order - Section, Clause Sorter, Reference/FullText, Name
    public static Comparator<ClauseRecord> ClauseSorterForDownload = new Comparator<ClauseRecord>() {

		public int compare(ClauseRecord c1, ClauseRecord c2) {
			int result = c1.getSectionCode().compareTo(c2.getSectionCode()); // by Section Code
			if (result == 0) {
				result = c1.getClauseTypeSorter() - c2.getClauseTypeSorter(); // by clause sorter
				if (result == 0) {
					result = c2.getInclusion().compareTo(c1.getInclusion()); // by Reference or Full
					if (result == 0) {
					   result = ClauseTable.compareClauseName(c1.getClauseName(), c2.getClauseName()); // compareComponents
					   //result = c1.getClauseName().compareTo(c2.getClauseName());	// sort by component name
					}
				}
			}

			return result;
		}
	};

	// ======================================================================================
	public static Comparator<ClauseRecord> ClauseSorterByName = new Comparator<ClauseRecord>() { // CJ-545

		public int compare(ClauseRecord c1, ClauseRecord c2) {
			// New Sort Order - Section, FAR, Reference, Name
			return ClauseTable.compareClauseName(c1.getClauseName(), c2.getClauseName());
	   }
		
	};
	*/

	// Sorting the clauses require extra logic,
	// since the string formats use a variable length 
	// number format.

	// Ex.:
	// 52.204.4	
	// 52.204.19
	public static int compareClauseName(String s1, String s2) {
		
		// CJ-1337, Replace Alternate string with numeric for easy comparison.
		s1 = replaceAlternateNumber(s1);
		s2 = replaceAlternateNumber(s2);
		
		String digit1 = s1.replaceAll("[^0-9.-]", "");
		String digit2 = s2.replaceAll("[^0-9.-]", "");
		
		digit1 = digit1.replaceAll("-", ".");
		digit2 = digit2.replaceAll("-", ".");
		
		String[] elements1 = digit1.split(Pattern.quote("."));
		String[] elements2 = digit2.split(Pattern.quote("."));
		
		int maxLength = elements1.length;
		
		if (elements2.length < maxLength)
			maxLength = elements2.length;
		
		if (maxLength == 0)
			return 0;
		
		int iReturn = 0;	// default, greater than
		
		for (int index = 0; index < maxLength; index++) {
			if (elements1[index].equals(elements2[index])){
				continue;
			}
			
			Integer Num1 = 0;
			Integer Num2 = 0;
			Boolean isNumber = true;
			try 
			{
				Num1 = Integer.parseInt (elements1[index]);
				Num2 = Integer.parseInt (elements2[index]);
			}
			catch (Exception e)
			{
				isNumber = false;
			}
			
			// Compare integer values
			if (isNumber) {					
				if (Num1 < Num2){
					iReturn = -1;
					break;
				}
				else if (Num1 > Num2){
					iReturn = 1;
					break;
				}	
			}
			// compare string values.
			else{
				iReturn = elements1[index].compareTo(elements2[index]);
				break;
			}
		}
		
		return iReturn;
	}
	
	// CJ-1337, Replace Alternate string with numeric for easy comparison.
	private static String replaceAlternateNumber (String s) {
		
		// Case doesn't matter, only digits are compared.
		s = s.toUpperCase();
		
		if (s.indexOf(" ALTERNATE V") > 0)
			s = s.replaceAll(" ALTERNATE V", ".5");
		else if (s.indexOf(" ALTERNATE IV") > 0)
			s = s.replaceAll(" ALTERNATE IV", ".4");
		else if (s.indexOf(" ALTERNATE III") > 0)
			s = s.replaceAll(" ALTERNATE III", ".3");
		else if (s.indexOf(" ALTERNATE II") > 0)
			s = s.replaceAll(" ALTERNATE II", ".2");
		else if (s.indexOf(" ALTERNATE I") > 0)
			s = s.replaceAll(" ALTERNATE I", ".1");
		//else if (s.indexOf(" ") > 0)
		//	s = s.replaceFirst(" ", ".0 ").trim();
		
		return s;
	}

	// ------------------------------------------------------------------
	public ArrayList<Integer> getSectionIds() {
		ArrayList<Integer> result = new ArrayList<Integer>();
		for (ClauseRecord clauseRecord : this) {
			Integer clauseSectionId = clauseRecord.getClauseSectionId();
			if ((clauseSectionId != null) && (!result.contains(clauseSectionId)))
				result.add(clauseSectionId);
		}
		return result;
	}

	public int getClauseVersionId() {
		return clauseVersionId;
	}

	public ClauseSectionTable getClauseSections() {
		if (this.clauseSectionTable == null) {
			ArrayList<Integer> aSections = this.getSectionIds();
			this.clauseSectionTable = ClauseSectionTable.getSubset(aSections);
		}
		return this.clauseSectionTable;
	}
	
	public ArrayList<ClauseRecord> collectBySection(Integer sectionId) {
		ArrayList<ClauseRecord> result = new ArrayList<ClauseRecord>();
		for (ClauseRecord clauseRecord : this) {
			Integer clauseSectionId = clauseRecord.getClauseSectionId();
			if (sectionId.equals(clauseSectionId))
				result.add(clauseRecord);
		}
		return result;
	}

	public ArrayNode toJson(ObjectMapper mapper, boolean forInterview) {
		if (mapper == null)
			mapper = new ObjectMapper();
		ArrayNode json = mapper.createArrayNode();
		for (ClauseRecord clauseRecord : this) {
			json.add(clauseRecord.toJson(forInterview));
		}
		return json;
	}

	public Date getRefreshed() {
		return refreshed;
	}

	public Exception getLastError() {
		return lastError;
	}
	
	// Refresh cache methods.
	public void refreshWhenNeeded(Integer pClauseVersionId, Connection conn) {
		if (ClauseTable.needToRefreh(conn))
			this.refresh(pClauseVersionId, conn);
	}
	
	public static boolean needToRefreh(Connection conn) {
		return SysLastTableChangeAtTable.needToLoad(ClauseTable.refreshed, getReferHistoryDate(conn));
	}
	
	public static Date getReferHistoryDate(Connection conn) {
		return SysLastTableChangeAtTable.getLastUpdateDate(conn, ClauseRecord.TABLE_CLAUSES);
	}
	
	/*
	// --------------------------------------
	public static void main(String[] args) {
		ClauseTable clauseTable = ClauseTable.getByClauseVersion(3, null);
		System.out.println(clauseTable.size());
	}
	*/
}
