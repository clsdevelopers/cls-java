package gov.dod.cls.db;

import gov.dod.cls.utils.CommonUtils;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

public class SysLastTableChangeAtRecord {

	public static final String TABLE_SYS_LAST_TABLE_CHANGE_AT = "Sys_Last_Table_Change_At";
	
	public static final String FIELD_TABLE_NAME = "Table_Name";
	public static final String FIELD_LAST_CHANGE_AT = "Last_Change_At";

	public static final String SQL_SELECT
		= "SELECT " + FIELD_TABLE_NAME
		+ ", " + FIELD_LAST_CHANGE_AT
		+ " FROM " + TABLE_SYS_LAST_TABLE_CHANGE_AT;

	// ======================================================
	private String name;
	private Date date;
	
	public void read(ResultSet rs) throws SQLException {
		this.name = rs.getString(FIELD_TABLE_NAME);
		this.date = CommonUtils.toDate(rs.getObject(FIELD_LAST_CHANGE_AT));
	}

	// ----------------------------------------------------------
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public boolean isSame(String psTableName) {
		return this.name.equals(psTableName);
	}

	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}

}
