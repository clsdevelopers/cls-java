package gov.dod.cls.db;

import gov.dod.cls.utils.CommonUtils;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

public class ClausePrescriptionRecord {

	public static final String TABLE_CLAUSE_PRESCRIPTIONS = "Clause_Prescriptions";
	
	public static final String FIELD_CLAUSE_PRESCRIPTION_ID = "clause_prescription_id";
	public static final String FIELD_CLAUSE_ID = "clause_id";
	public static final String FIELD_PRESCRIPTION_ID = "prescription_id";
	public static final String FIELD_CREATED_AT = "created_at";
	public static final String FIELD_UPDATED_AT = "updated_at";

	public static final String SQL_FROM
		= " FROM " + TABLE_CLAUSE_PRESCRIPTIONS + " A"
		+ " INNER JOIN " + PrescriptionRecord.TABLE_PRESCRIPTIONS + " B"
		+ " ON (B." + PrescriptionRecord.FIELD_PRESCRIPTION_ID + " = A." + FIELD_PRESCRIPTION_ID + ")";

	public static final String SQL_SELECT
		= "SELECT A." + FIELD_CLAUSE_PRESCRIPTION_ID
		+ ", A." + FIELD_CLAUSE_ID
		+ ", A." + FIELD_PRESCRIPTION_ID
		+ ", A." + FIELD_CREATED_AT
		+ ", A." + FIELD_UPDATED_AT
		+ SQL_FROM;
	
	public static final String SQL_ORDER_BY
		= " ORDER BY A." + FIELD_CLAUSE_ID + ", B." + PrescriptionRecord.FIELD_PRESCRIPTION_NAME;

	// ======================================================
	private Integer id;
	private Integer clauseId;
	private Integer prescriptionId;
	private Date createdAt;
	private Date updatedAt;
	
	public void read(ResultSet rs) throws SQLException {
		this.id = rs.getInt(FIELD_CLAUSE_PRESCRIPTION_ID); 
		this.clauseId = CommonUtils.toInteger(rs.getObject(FIELD_CLAUSE_ID));
		this.prescriptionId = CommonUtils.toInteger(rs.getObject(FIELD_PRESCRIPTION_ID));
		this.createdAt = CommonUtils.toDate(rs.getObject(FIELD_CREATED_AT));
		this.updatedAt = CommonUtils.toDate(rs.getObject(FIELD_UPDATED_AT));
	}

	// ----------------------------------------------------------

	public Integer getClauseId() {
		return clauseId;
	}
	public void setClauseId(Integer clauseId) {
		this.clauseId = clauseId;
	}

	public Integer getPrescriptionId() {
		return prescriptionId;
	}
	public void setPrescriptionId(Integer prescriptionId) {
		this.prescriptionId = prescriptionId;
	}

	public Date getCreatedAt() {
		return createdAt;
	}
	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public Date getUpdatedAt() {
		return updatedAt;
	}
	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	public Integer getId() {
		return id;
	}

}
