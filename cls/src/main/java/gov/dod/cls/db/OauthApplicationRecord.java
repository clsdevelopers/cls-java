package gov.dod.cls.db;

import gov.dod.cls.config.ClsConfig;
import gov.dod.cls.model.JsonAble;
import gov.dod.cls.utils.CommonUtils;
import gov.dod.cls.utils.sql.SqlInsert;
import gov.dod.cls.utils.sql.SqlUpdate;
import gov.dod.cls.utils.sql.SqlUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import org.codehaus.jackson.node.ObjectNode;

public class OauthApplicationRecord extends JsonAble {
	
	public static final String TABLE_OAUTH_APPLICATIONS = "OAuth_Applications";
	
	public static final String FIELD_APPLICATION_ID = "application_id";
	public static final String FIELD_APPLICATION_NAME = "application_name";
	public static final String FIELD_APPLICATION_UID= "application_uid";
	public static final String FIELD_APPLICATION_SECRET = "application_secret";
	public static final String FIELD_REDIRECT_URI = "redirect_uri";
	public static final String FIELD_ORG_ID = "org_id";
	public static final String FIELD_CREATED_AT = "created_at";
	public static final String FIELD_UPDATED_AT = "updated_at";
	
	public static final String SQL_SELECT
		= "SELECT A." + FIELD_APPLICATION_ID
		+ ", A." + FIELD_APPLICATION_NAME
		+ ", A." + FIELD_APPLICATION_UID
		+ ", A." + FIELD_APPLICATION_SECRET
		+ ", A." + FIELD_REDIRECT_URI
		+ ", A." + FIELD_ORG_ID
		+ ", B." + OrgRecord.FIELD_ORG_NAME
		+ ", B." + OrgRecord.FIELD_DEPT_ID
		+ ", C." + DeptRecord.FIELD_DEPT_NAME
		+ ", A." + FIELD_CREATED_AT
		+ ", A." + FIELD_UPDATED_AT
		+ " FROM " + TABLE_OAUTH_APPLICATIONS + " A"
		+ " INNER JOIN " + OrgRecord.TABLE_ORGS + " B ON (B." + OrgRecord.FIELD_ORG_ID + "=A." + FIELD_ORG_ID + ")"
		+ " INNER JOIN " + DeptRecord.TABLE_DEPTS + " C ON (C." + DeptRecord.FIELD_DEPT_ID + "=B." + OrgRecord.FIELD_DEPT_ID + ")";
	
	public static final String SQL_ORDER_BY
		= " ORDER BY C." + DeptRecord.FIELD_DEPT_NAME
		+ ", B." + OrgRecord.FIELD_ORG_NAME
		+ ", A." + FIELD_APPLICATION_NAME;
	
	public static OauthApplicationRecord getRecord(Integer applicationId, String applicationName, Integer orgId, Connection conn)
			throws SQLException {
		boolean bCreateConnection = (conn == null);
		OauthApplicationRecord result = null;
		PreparedStatement ps1 = null;
		ResultSet rs1 = null;
		try {
			String sSql = OauthApplicationRecord.SQL_SELECT;
			String sOperator = " WHERE ";
			if (applicationId != null) {
				sSql += sOperator + "A." + OauthApplicationRecord.FIELD_APPLICATION_ID + " = ?";
				sOperator = " AND ";
			}
			if (applicationName != null) {
				sSql += sOperator + "LOWER(A." + OauthApplicationRecord.FIELD_APPLICATION_NAME + ")=?";
				sOperator = " AND ";
				applicationName = applicationName.toLowerCase();
			}
			if (orgId != null)
				sSql += sOperator + "A." + OauthApplicationRecord.FIELD_ORG_ID + " = ?";
			if (bCreateConnection)
				conn = ClsConfig.getDbConnection();
			ps1 = conn.prepareStatement(sSql);
			int iParam = 1;
			if (applicationId != null)
				ps1.setInt(iParam++, applicationId);
			if (applicationName != null)
				ps1.setString(iParam++, applicationName);
			if (orgId != null)
				ps1.setInt(iParam, orgId);
			rs1 = ps1.executeQuery();
			if (rs1.next()) {
				result = new OauthApplicationRecord();
				result.read(rs1);
			}
		}
		finally {
			SqlUtil.closeInstance(rs1, ps1);
			if (bCreateConnection)
				SqlUtil.closeInstance(conn);
		}
		return result;
	}
	
	// CJ-1410
	public static Integer getOrgId (Integer appId, Connection conn) {

		Integer iResult=null;
		
		if (appId == null)
			return iResult;

		try {
			OauthApplicationRecord aRec = 
					OauthApplicationRecord.getRecord(appId, null, null, conn);
			if (aRec != null)
				iResult = aRec.getOrgId();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return iResult;
	}
	// ===============================================================
	private Integer applicationId;
	private String applicationName;
	private String applicationUid;
	private String applicationSecret;
	private String redirectUri = "";
	private Integer orgId;
	private Date createdAt;
	private Date updatedAt;

	private String orgName;
	private Integer deptId;
	private String deptName;

	public void read(ResultSet rs) throws SQLException {
		this.applicationId = rs.getInt(FIELD_APPLICATION_ID); 
		this.applicationName = rs.getString(FIELD_APPLICATION_NAME);
		this.applicationUid = rs.getString(FIELD_APPLICATION_UID);
		this.applicationSecret = rs.getString(FIELD_APPLICATION_SECRET);
		this.redirectUri = rs.getString(FIELD_REDIRECT_URI);
		this.orgId = rs.getInt(FIELD_ORG_ID); 
		this.orgName = rs.getString(OrgRecord.FIELD_ORG_NAME);
		this.deptId = rs.getInt(OrgRecord.FIELD_DEPT_ID); 
		this.deptName = rs.getString(DeptRecord.FIELD_DEPT_NAME);
		this.createdAt = CommonUtils.toDate(rs.getObject(FIELD_CREATED_AT));
		this.updatedAt = CommonUtils.toDate(rs.getObject(FIELD_UPDATED_AT));
	}

	@Override
	protected void populateJson(ObjectNode json, boolean forInterview) {
		json.put(FIELD_APPLICATION_ID, this.applicationId);
		json.put(FIELD_APPLICATION_NAME, this.applicationName);
		json.put(FIELD_APPLICATION_UID, this.applicationUid);
		json.put(FIELD_APPLICATION_SECRET, this.applicationSecret);
		json.put(FIELD_REDIRECT_URI, this.redirectUri);
		json.put(FIELD_ORG_ID, this.orgId);
		json.put(OrgRecord.FIELD_ORG_NAME, this.orgName);
		json.put(OrgRecord.FIELD_DEPT_ID, this.deptId);
		json.put(DeptRecord.FIELD_DEPT_NAME, this.deptName);
		json.put(FIELD_CREATED_AT, CommonUtils.toJsonDate(this.createdAt));
		json.put(FIELD_UPDATED_AT, CommonUtils.toJsonDate(this.updatedAt));
	}

	public void populateJsonForApi(boolean bForVer1, ObjectNode json) {
		json.put("created_at", CommonUtils.toZulu(this.createdAt));
		json.put("id", this.applicationId);
		json.put("name", this.applicationName);
		json.put("org_id", this.orgId);
		json.put("redirect_uri", (String)null);
		json.put("secret", this.applicationSecret);
		json.put("uid", this.applicationUid);
		json.put("updated_at", CommonUtils.toZulu(this.updatedAt));
	}

	public void insert(Connection conn, Integer userId) throws Exception {
		AuditEventRecord auditEventRecord = new AuditEventRecord("oauthapplication_created", null, userId);
		SqlInsert sqlInsert = new SqlInsert(null, TABLE_OAUTH_APPLICATIONS);
		sqlInsert.addField(FIELD_APPLICATION_NAME, this.applicationName, auditEventRecord);
		sqlInsert.addField(FIELD_APPLICATION_UID, this.applicationUid, auditEventRecord);
		sqlInsert.addField(FIELD_APPLICATION_SECRET, this.applicationSecret, auditEventRecord);
		sqlInsert.addField(FIELD_REDIRECT_URI, this.redirectUri, auditEventRecord);
		sqlInsert.addField(FIELD_ORG_ID, this.orgId, auditEventRecord);
		sqlInsert.execute(conn);
		auditEventRecord.saveNew(conn);
	}
	
	public boolean update(OauthApplicationRecord postRecord, Connection conn, Integer userId) throws Exception {
		boolean bSaved = false;
		AuditEventRecord auditEventRecord = new AuditEventRecord("oauthapplication_updated", null, userId);
		SqlUpdate sqlUpdate = new SqlUpdate(null, TABLE_OAUTH_APPLICATIONS);
		if (CommonUtils.isNotSame(this.applicationName, postRecord.applicationName))
			sqlUpdate.addString(FIELD_APPLICATION_NAME, postRecord.applicationName, auditEventRecord);
		if (CommonUtils.isNotSame(this.redirectUri, postRecord.redirectUri))
			sqlUpdate.addString(FIELD_REDIRECT_URI, postRecord.redirectUri, auditEventRecord);
		if (CommonUtils.isNotSame(this.orgId, postRecord.orgId))
			sqlUpdate.addString(FIELD_ORG_ID, postRecord.orgId, auditEventRecord);
		if (sqlUpdate.size() > 0) {
			sqlUpdate.addSet(FIELD_UPDATED_AT, CommonUtils.getNow(), java.sql.Types.DATE, auditEventRecord);
			sqlUpdate.addCriteria(FIELD_APPLICATION_ID, this.applicationId, java.sql.Types.INTEGER, auditEventRecord);
			sqlUpdate.execute(conn);
			auditEventRecord.saveNew(conn);
			bSaved = true;
		}
		return bSaved;
	}
	
	public void delete(Connection conn) throws SQLException {
		PreparedStatement ps1 = null;
		try {
			String sSql = "DELETE FROM " + TABLE_OAUTH_APPLICATIONS
					+ " WHERE " + FIELD_APPLICATION_ID + " = ?";
			ps1 = conn.prepareStatement(sSql);
			ps1.setInt(1, this.applicationId);
			ps1.execute();
		}
		finally {
			SqlUtil.closeInstance(ps1);
		}
	}
	
	// ------------------------------------------------------
	public String getApplicationName() {
		return applicationName;
	}

	public void setApplicationName(String applicationName) {
		this.applicationName = applicationName;
	}

	public String getApplicationUid() {
		return applicationUid;
	}

	public void setApplicationUid(String applicationUid) {
		this.applicationUid = applicationUid;
	}

	public String getApplicationSecret() {
		return applicationSecret;
	}

	public void setApplicationSecret(String applicationSecret) {
		this.applicationSecret = applicationSecret;
	}

	public String getRedirectUri() {
		return redirectUri;
	}

	public void setRedirectUri(String redirectUri) {
		this.redirectUri = redirectUri;
	}

	public Integer getOrgId() {
		return orgId;
	}

	public void setOrgId(Integer orgId) {
		this.orgId = orgId;
	}

	public Integer getDeptId() {
		return deptId;
	}

	public void setDeptId(Integer deptId) {
		this.deptId = deptId;
	}

	public Integer getApplicationId() {
		return applicationId;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public Date getUpdatedAt() {
		return updatedAt;
	}

	public String getOrgName() {
		return orgName;
	}

	public String getDeptName() {
		return deptName;
	};

}
