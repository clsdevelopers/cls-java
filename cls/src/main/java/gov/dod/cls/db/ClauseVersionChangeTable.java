package gov.dod.cls.db;

import gov.dod.cls.PageApi;
import gov.dod.cls.bean.ClsDocument;
import gov.dod.cls.bean.session.UserSession;
import gov.dod.cls.config.ClsConfig;
import gov.dod.cls.model.JsonAble;
import gov.dod.cls.utils.CommonUtils;
import gov.dod.cls.utils.sql.SqlUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.node.ArrayNode;
import org.codehaus.jackson.node.ObjectNode;

public class ClauseVersionChangeTable extends ArrayList<ClauseVersionChangeRecord> {

	private static final long serialVersionUID = 6087484134563239467L;

	// ----------------------------------------------------------
	public static final String PREFIX_DO_CLAUSE_VERSION_CHANGE = "clauseversionchange-";
	public static final String DO_CLAUSE_VERSION_CHANGE_LIST = PREFIX_DO_CLAUSE_VERSION_CHANGE + "list";
	public static final String DO_CLAUSE_VERSION_CHANGE_UPD = PREFIX_DO_CLAUSE_VERSION_CHANGE + "upd";
	public static final String DO_CLAUSE_VERSION_CHANGE_FINAL = PREFIX_DO_CLAUSE_VERSION_CHANGE + "final";

	public static void processRequest(String sDo, UserSession oUserSession, 
			HttpServletRequest req, HttpServletResponse resp) {
		
		switch (sDo) {
		case ClauseVersionChangeTable.DO_CLAUSE_VERSION_CHANGE_LIST:
			ClauseVersionChangeTable.processList(req, resp);
			return;
		case ClauseVersionChangeTable.DO_CLAUSE_VERSION_CHANGE_UPD:
			ClauseVersionChangeTable.processUpdate(req, resp, oUserSession);
			return;
		case ClauseVersionChangeTable.DO_CLAUSE_VERSION_CHANGE_FINAL:
			ClauseVersionChangeTable.processFinalization(req, resp, oUserSession);
			return;
		}
	}
	
	public static void processList(HttpServletRequest req, HttpServletResponse resp) {
		PreparedStatement ps1 = null;
		ResultSet rs1 = null;
		Connection conn = null;
		ObjectMapper mapper = new ObjectMapper();
		ObjectNode jsonTable = mapper.createObjectNode();
		
		Boolean bChgsFound = false;

		Integer docId = CommonUtils.toInteger(req.getParameter("id"));
		if (docId == null)
			return;
		
		try {
			conn = ClsConfig.getDbConnection();
			
			// Retrieve the version of the document.
			DocumentRecord docOrigRecord = DocumentTable.find (docId, conn);
			
			// Retrieve the latest version published.
			ClauseVersionTable.getInstance(conn);
			Integer clsVersionId = ClauseVersionTable.getActiveVersionId();
			
			if (clsVersionId == docOrigRecord.getClauseVersionId())
			{
				jsonTable.put("newId", docOrigRecord.getId());
				PageApi.send(resp, JsonAble.JSON_RESPONSE_FORMAT, JsonAble.jsonResult(JsonAble.STATUS_FAIL, jsonTable, "No update required."));	
				return;
			}
			
			ArrayNode arrayNode = mapper.createArrayNode();
			ClauseVersionChangeTable.populateJsonForQuestionChanges(false, arrayNode, clsVersionId, conn);
	
			ClauseVersionChangeTable.populateJsonForClauseChanges(false, arrayNode, clsVersionId, conn);
			bChgsFound = arrayNode.size() > 0;
			
			// Setup the data to be returned to the dashboard page.
			jsonTable.put("changeItems", arrayNode);
			jsonTable.put("count", arrayNode.size());
			jsonTable.put("docName", docOrigRecord.getAcquisitionTitle());
			
			ClauseVersionRecord oClauseVersionRecord = ClauseVersionTable.getRecord(clsVersionId);
			jsonTable.put("facNumber", oClauseVersionRecord.getFacNumber());
			jsonTable.put("dacNumber", oClauseVersionRecord.getDacNumber());
			jsonTable.put("dfarsNumber", oClauseVersionRecord.getDFarsNumber());
			
			jsonTable.put("facDate", CommonUtils.toString(oClauseVersionRecord.getFacDate()));
			jsonTable.put("dacDate", CommonUtils.toString(oClauseVersionRecord.getDacDate()));
			jsonTable.put("dfarsDate", CommonUtils.toString(oClauseVersionRecord.getDFarsDate()));
			
			// Send back response.
			String response = new String();				
			if (bChgsFound)
				response = JsonAble.jsonResult(JsonAble.STATUS_SUCCESS, jsonTable, "Change details");
			else
				response = JsonAble.jsonResult(JsonAble.STATUS_SUCCESS, jsonTable, "No change details available"); // JsonAble.STATUS_FAIL, null, "");
			
			PageApi.send(resp, JsonAble.JSON_RESPONSE_FORMAT, response);
		}
		catch (Exception oError) {
			oError.printStackTrace();
			PageApi.send(resp, JsonAble.JSON_RESPONSE_FORMAT, JsonAble.jsonResult(JsonAble.STATUS_FAIL, null,
					"Unknown error occurred.\nPlease contact system administrator for this instance."));	
		}
		finally {
			SqlUtil.closeInstance(rs1, ps1, conn);
		}
	}
	
	public static boolean populateJsonForApi(ObjectNode jsonRow, int clsVersionId, Connection conn) 
			throws SQLException {
		ArrayNode arrayNodeQestions = jsonRow.arrayNode();
		ClauseVersionChangeTable.populateJsonForQuestionChanges(true, arrayNodeQestions, clsVersionId, conn);
		jsonRow.put("question_change_count", arrayNodeQestions.size());
		jsonRow.put("question_changes", arrayNodeQestions);

		ArrayNode arrayNodeClauses = jsonRow.arrayNode();
		ClauseVersionChangeTable.populateJsonForClauseChanges(true, arrayNodeClauses, clsVersionId, conn);
		jsonRow.put("clause_change_count", arrayNodeClauses.size());
		jsonRow.put("clause_changes", arrayNodeClauses);

		return (arrayNodeQestions.size() > 0) || (arrayNodeClauses.size() > 0);
	}
	
	public static void populateJsonForQuestionChanges(boolean pbForApi, ArrayNode arrayNode, int clsVersionId, Connection conn) 
			throws SQLException {
		PreparedStatement ps1 = null;
		ResultSet rs1 = null;
		try {
			// Retrieve a list of question changes.
			String sSql = 
				"SELECT QGR." + QuestionGroupRefRecord.FIELD_QUESTION_GROUP + ", " + 
					" QGR." + QuestionGroupRefRecord.FIELD_QUESTION_GROUP_NAME + ", " + 
					" Q." + QuestionRecord.FIELD_QUESTION_CODE  + ", " +
					" Q." + QuestionRecord.FIELD_QUESTION_TEXT  + ", " + 
					" CVC." + ClauseVersionChangeRecord.FIELD_IS_QUESTION_OR_CLAUSE + ", " + 
					" CVC." + ClauseVersionChangeRecord.FIELD_CHANGE_CODE + ", " + 
					" CVC." + ClauseVersionChangeRecord.FIELD_CLAUSE_VER_CHANGE_ID + ", " +
					" CVC." + ClauseVersionChangeRecord.FIELD_CHANGE_BRIEF +
				" FROM " + ClauseVersionChangeRecord.TABLE_CLAUSE_VERSION_CHANGES + " CVC " + 
				" INNER JOIN " + QuestionRecord.TABLE_QUESTIONS + " Q " +
					" ON (Q." + QuestionRecord.FIELD_QUESTION_CODE + " = CVC." + ClauseVersionChangeRecord.FIELD_QUESTION_OR_CLAUSE_CODE +
					" AND Q." + ClauseRecord.FIELD_CLAUSE_VERSION_ID + " = CVC." + ClauseVersionChangeRecord.FIELD_CLAUSE_VERSION_ID + ")" +
				" INNER JOIN " + QuestionGroupRefRecord.TABLE_QUESTION_GROUP_REF + " QGR " +
					" ON QGR." + QuestionGroupRefRecord.FIELD_QUESTION_GROUP + " = Q." + QuestionRecord.FIELD_QUESTION_GROUP +
				" WHERE CVC." + ClauseVersionChangeRecord.FIELD_CLAUSE_VERSION_ID + " = ? " +
					" AND CVC." + ClauseVersionChangeRecord.FIELD_IS_QUESTION_OR_CLAUSE + " = 'Q' " +
				" ORDER BY QGR." + QuestionGroupRefRecord.FIELD_QUESTION_GROUP + 
					", Q." + QuestionRecord.FIELD_QUESTION_CODE;
			 
			ps1 = conn.prepareStatement(sSql);
			ps1.setInt(1, clsVersionId);
			rs1 = ps1.executeQuery();
			
			// Save the list of changes.
			//ArrayNode arrayNode = mapper.createArrayNode();	
			while (rs1.next()) {			
				// bChgsFound = true;
				String group = rs1.getString(QuestionGroupRefRecord.FIELD_QUESTION_GROUP);
				String groupName  = rs1.getString(QuestionGroupRefRecord.FIELD_QUESTION_GROUP_NAME);
				String title  = rs1.getString(QuestionRecord.FIELD_QUESTION_CODE);
				String text  = rs1.getString(QuestionRecord.FIELD_QUESTION_TEXT);
				String isQuestionOrClause  = rs1.getString(ClauseVersionChangeRecord.FIELD_IS_QUESTION_OR_CLAUSE);
				String chgCode =  rs1.getString(ClauseVersionChangeRecord.FIELD_CHANGE_CODE);
				Integer id = rs1.getInt(ClauseVersionChangeRecord.FIELD_CLAUSE_VER_CHANGE_ID);
				String changeBrief = rs1.getString(ClauseVersionChangeRecord.FIELD_CHANGE_BRIEF);
				
				ObjectNode jsonRow = arrayNode.objectNode(); // mapper.createObjectNode();
				if (pbForApi) {
					//String changeBrief = rs1.getString(ClauseVersionChangeRecord.FIELD_CHANGE_BRIEF);
					jsonRow.put("id", id);
					jsonRow.put(QuestionGroupRefRecord.FIELD_QUESTION_GROUP, group);
					jsonRow.put(QuestionGroupRefRecord.FIELD_QUESTION_GROUP_NAME, groupName);
					jsonRow.put(QuestionRecord.FIELD_QUESTION_CODE, title);
					jsonRow.put(QuestionRecord.FIELD_QUESTION_TEXT, text);
					// jsonRow.put(ClauseVersionChangeRecord.FIELD_IS_QUESTION_OR_CLAUSE, isQuestionOrClause);
					jsonRow.put(ClauseVersionChangeRecord.FIELD_CHANGE_CODE, chgCode);
					jsonRow.put(ClauseVersionChangeRecord.FIELD_CHANGE_BRIEF, changeBrief);
				} else {
					ClauseVersionChangeTable.loadClsContent(jsonRow, group, groupName, title, text, isQuestionOrClause, chgCode, changeBrief, id);
				}
				arrayNode.add(jsonRow);
			}
		}
		finally {
			SqlUtil.closeInstance(rs1, ps1);
		}
	}
	
	public static void populateJsonForClauseChanges(boolean pbForApi, ArrayNode arrayNode, int clsVersionId, Connection conn) 
			throws SQLException {
		PreparedStatement ps1 = null;
		ResultSet rs1 = null;
		try {
			String sSql = 
				"SELECT CS." + ClauseSectionRecord.FIELD_CLAUSE_SECTION_CODE + ", " + 
					" CS." + ClauseSectionRecord.FIELD_CLAUSE_SECTION_HEADER + ", " + 
					" C." + ClauseRecord.FIELD_CLAUSE_NAME  + ", " + 	
					" C." + ClauseRecord.FIELD_CLAUSE_TITLE  + ", " + 
					" CVC." + ClauseVersionChangeRecord.FIELD_IS_QUESTION_OR_CLAUSE + ", " + 
					" CVC." + ClauseVersionChangeRecord.FIELD_CHANGE_CODE + ", " + 
					" CVC." + ClauseVersionChangeRecord.FIELD_CLAUSE_VER_CHANGE_ID + ", " +
					" CVC." + ClauseVersionChangeRecord.FIELD_CHANGE_BRIEF +
				" FROM " + ClauseVersionChangeRecord.TABLE_CLAUSE_VERSION_CHANGES + " CVC " + 
				" INNER JOIN " + ClauseRecord.TABLE_CLAUSES + " C " +
					" ON (C." + ClauseRecord.FIELD_CLAUSE_NAME + " = CVC." + ClauseVersionChangeRecord.FIELD_QUESTION_OR_CLAUSE_CODE +
					" AND C." + ClauseRecord.FIELD_CLAUSE_VERSION_ID + " = CVC." + ClauseVersionChangeRecord.FIELD_CLAUSE_VERSION_ID + ")" +
				" INNER JOIN " + ClauseSectionRecord.TABLE_CLAUSE_SECTIONS + " CS " +
					" ON CS." + ClauseSectionRecord.FIELD_CLAUSE_SECTION_ID + " = C." + ClauseRecord.FIELD_CLAUSE_SECTION_ID +
				" WHERE CVC." + ClauseVersionChangeRecord.FIELD_CLAUSE_VERSION_ID + " = ? " +
					" AND CVC." + ClauseVersionChangeRecord.FIELD_IS_QUESTION_OR_CLAUSE + " = 'C' " +
				" ORDER BY CS." + ClauseSectionRecord.FIELD_CLAUSE_SECTION_CODE + 
					", C." + ClauseRecord.FIELD_CLAUSE_NAME;
			 
			ps1 = conn.prepareStatement(sSql);
			ps1.setInt(1, clsVersionId);
			rs1 = ps1.executeQuery();
			
			// Save the list of changes.
			//ArrayNode arrayNode = mapper.createArrayNode();	
			while (rs1.next()) {			
				// bChgsFound = true;
				String group = rs1.getString(ClauseSectionRecord.FIELD_CLAUSE_SECTION_CODE);
				String groupName  = rs1.getString(ClauseSectionRecord.FIELD_CLAUSE_SECTION_HEADER);
				String title  = rs1.getString(ClauseRecord.FIELD_CLAUSE_NAME);
				String text  = rs1.getString(ClauseRecord.FIELD_CLAUSE_TITLE);
				String isQuestionOrClause  = rs1.getString(ClauseVersionChangeRecord.FIELD_IS_QUESTION_OR_CLAUSE);
				String chgCode  = rs1.getString(ClauseVersionChangeRecord.FIELD_CHANGE_CODE);
				Integer id = rs1.getInt(ClauseVersionChangeRecord.FIELD_CLAUSE_VER_CHANGE_ID);
				String changeBrief = rs1.getString(ClauseVersionChangeRecord.FIELD_CHANGE_BRIEF);
				
				ObjectNode jsonRow = arrayNode.objectNode(); // mapper.createObjectNode();
				if (pbForApi) {
					//String changeBrief = rs1.getString(ClauseVersionChangeRecord.FIELD_CHANGE_BRIEF);
					jsonRow.put("id", id);
					jsonRow.put(ClauseSectionRecord.FIELD_CLAUSE_SECTION_CODE, group);
					jsonRow.put(ClauseSectionRecord.FIELD_CLAUSE_SECTION_HEADER, groupName);
					jsonRow.put(ClauseRecord.FIELD_CLAUSE_NAME, title);
					jsonRow.put(ClauseRecord.FIELD_CLAUSE_TITLE, text);
					// jsonRow.put(ClauseVersionChangeRecord.FIELD_IS_QUESTION_OR_CLAUSE, isQuestionOrClause);
					jsonRow.put(ClauseVersionChangeRecord.FIELD_CHANGE_CODE, chgCode);
					jsonRow.put(ClauseVersionChangeRecord.FIELD_CHANGE_BRIEF, changeBrief);
				} else {
					loadClsContent(jsonRow, group, groupName, title, text, isQuestionOrClause, chgCode, changeBrief, id);
				}
				arrayNode.add(jsonRow);
			}		
		}
		finally {
			SqlUtil.closeInstance(rs1, ps1);
		}
	}

	public static boolean populateCombinedChangesJsonForApi(ObjectNode jsonRow, int clsVersionId, Connection conn) 
			throws SQLException {
		ArrayNode arrayNodeQestions = jsonRow.arrayNode();
		ClauseVersionChangeTable.populateJsonForCombinedQuestionChanges(true, arrayNodeQestions, clsVersionId, conn);
		jsonRow.put("question_change_count", arrayNodeQestions.size());
		jsonRow.put("question_changes", arrayNodeQestions);

		ArrayNode arrayNodeClauses = jsonRow.arrayNode();
		ClauseVersionChangeTable.populateJsonForCombinedClauseChanges(true, arrayNodeClauses, clsVersionId, conn);
		jsonRow.put("clause_change_count", arrayNodeClauses.size());
		jsonRow.put("clause_changes", arrayNodeClauses);

		return (arrayNodeQestions.size() > 0) || (arrayNodeClauses.size() > 0);
	}
	
	public static void populateJsonForCombinedQuestionChanges(boolean pbForApi, ArrayNode arrayNode, int clsVersionId, Connection conn) 
			throws SQLException {
		PreparedStatement ps1 = null;
		ResultSet rs1 = null;
		try {
			// Retrieve a list of question changes.
			String sSql = 
				"SELECT QGR." + QuestionGroupRefRecord.FIELD_QUESTION_GROUP + ", " + 
					" QGR." + QuestionGroupRefRecord.FIELD_QUESTION_GROUP_NAME + ", " + 
					" Q." + QuestionRecord.FIELD_QUESTION_CODE  + ", " +
					" Q." + QuestionRecord.FIELD_QUESTION_TEXT  + ", " + 
					" CVC." + ClauseVersionChangeRecord.FIELD_IS_QUESTION_OR_CLAUSE + ", " + 
					" GROUP_CONCAT(CVC." + ClauseVersionChangeRecord.FIELD_CHANGE_CODE + " ORDER BY CVC." + ClauseVersionChangeRecord.FIELD_CLAUSE_VER_CHANGE_ID + " ASC SEPARATOR '|'  )  as `ChangeCodes`, " + 
					" GROUP_CONCAT(CVC." + ClauseVersionChangeRecord.FIELD_CHANGE_BRIEF + " ORDER BY CVC." + ClauseVersionChangeRecord.FIELD_CLAUSE_VER_CHANGE_ID + " ASC SEPARATOR '|'  )  as `ChangeBriefs` " + 
				" FROM " + ClauseVersionChangeRecord.TABLE_CLAUSE_VERSION_CHANGES + " CVC " + 
				" INNER JOIN " + QuestionRecord.TABLE_QUESTIONS + " Q " +
					" ON (Q." + QuestionRecord.FIELD_QUESTION_CODE + " = CVC." + ClauseVersionChangeRecord.FIELD_QUESTION_OR_CLAUSE_CODE +
					" AND Q." + ClauseRecord.FIELD_CLAUSE_VERSION_ID + " = CVC." + ClauseVersionChangeRecord.FIELD_CLAUSE_VERSION_ID + ")" +
				" INNER JOIN " + QuestionGroupRefRecord.TABLE_QUESTION_GROUP_REF + " QGR " +
					" ON QGR." + QuestionGroupRefRecord.FIELD_QUESTION_GROUP + " = Q." + QuestionRecord.FIELD_QUESTION_GROUP +
				" WHERE CVC." + ClauseVersionChangeRecord.FIELD_CLAUSE_VERSION_ID + " >= ? " +
					" AND CVC." + ClauseVersionChangeRecord.FIELD_IS_QUESTION_OR_CLAUSE + " = 'Q' " +
				" GROUP BY QGR." + QuestionGroupRefRecord.FIELD_QUESTION_GROUP + 
					", Q." + QuestionRecord.FIELD_QUESTION_CODE;
			 
			ps1 = conn.prepareStatement(sSql);
			ps1.setInt(1, clsVersionId);
			rs1 = ps1.executeQuery();
			
			// Save the list of changes.
			
			while (rs1.next()) {			
				String group = rs1.getString(QuestionGroupRefRecord.FIELD_QUESTION_GROUP);
				String groupName  = rs1.getString(QuestionGroupRefRecord.FIELD_QUESTION_GROUP_NAME);
				String title  = rs1.getString(QuestionRecord.FIELD_QUESTION_CODE);
				String text  = rs1.getString(QuestionRecord.FIELD_QUESTION_TEXT);
				//String isQuestionOrClause  = rs1.getString(ClauseVersionChangeRecord.FIELD_IS_QUESTION_OR_CLAUSE);
				String chgCode = rs1.getString("ChangeCodes"); 
				String changeBrief = rs1.getString("ChangeBriefs"); 
				
				String sChanges = mergeChanges (chgCode, changeBrief); 
				
				ObjectNode jsonRow = arrayNode.objectNode(); // mapper.createObjectNode();
				if (pbForApi) {
					
					//jsonRow.put("id", id);
					jsonRow.put(QuestionGroupRefRecord.FIELD_QUESTION_GROUP, group);
					jsonRow.put(QuestionGroupRefRecord.FIELD_QUESTION_GROUP_NAME, groupName);
					jsonRow.put(QuestionRecord.FIELD_QUESTION_CODE, title);
					jsonRow.put(QuestionRecord.FIELD_QUESTION_TEXT, text);
					// jsonRow.put(ClauseVersionChangeRecord.FIELD_IS_QUESTION_OR_CLAUSE, isQuestionOrClause);
					//jsonRow.put(ClauseVersionChangeRecord.FIELD_CHANGE_CODE, chgCode);
					//jsonRow.put(ClauseVersionChangeRecord.FIELD_CHANGE_BRIEF, changeBrief);
					jsonRow.put("changes", sChanges);
				}
				arrayNode.add(jsonRow);
			}
			
		}
		finally {
			SqlUtil.closeInstance(rs1, ps1);
		}
	}
	public static void populateJsonForCombinedClauseChanges(boolean pbForApi, ArrayNode arrayNode, int clsVersionId, Connection conn) 
			throws SQLException {
		PreparedStatement ps1 = null;
		ResultSet rs1 = null;
		try {
			String sSql = 
				"SELECT CS." + ClauseSectionRecord.FIELD_CLAUSE_SECTION_CODE + ", " + 
					" CS." + ClauseSectionRecord.FIELD_CLAUSE_SECTION_HEADER + ", " + 
					" C." + ClauseRecord.FIELD_CLAUSE_NAME  + ", " + 	
					" C." + ClauseRecord.FIELD_CLAUSE_TITLE  + ", " + 
					" CVC." + ClauseVersionChangeRecord.FIELD_IS_QUESTION_OR_CLAUSE + ", " + 
					" GROUP_CONCAT(CVC." + ClauseVersionChangeRecord.FIELD_CHANGE_CODE + " ORDER BY CVC." + ClauseVersionChangeRecord.FIELD_CLAUSE_VER_CHANGE_ID + " ASC SEPARATOR '|'  )  as `ChangeCodes`, " + 
					" GROUP_CONCAT(CVC." + ClauseVersionChangeRecord.FIELD_CHANGE_BRIEF + " ORDER BY CVC." + ClauseVersionChangeRecord.FIELD_CLAUSE_VER_CHANGE_ID + " ASC SEPARATOR '|'  )  as `ChangeBriefs` " + 
				" FROM " + ClauseVersionChangeRecord.TABLE_CLAUSE_VERSION_CHANGES + " CVC " + 
				" INNER JOIN " + ClauseRecord.TABLE_CLAUSES + " C " +
					" ON (C." + ClauseRecord.FIELD_CLAUSE_NAME + " = CVC." + ClauseVersionChangeRecord.FIELD_QUESTION_OR_CLAUSE_CODE +
					" AND C." + ClauseRecord.FIELD_CLAUSE_VERSION_ID + " = CVC." + ClauseVersionChangeRecord.FIELD_CLAUSE_VERSION_ID + ")" +
				" INNER JOIN " + ClauseSectionRecord.TABLE_CLAUSE_SECTIONS + " CS " +
					" ON CS." + ClauseSectionRecord.FIELD_CLAUSE_SECTION_ID + " = C." + ClauseRecord.FIELD_CLAUSE_SECTION_ID +
				" WHERE CVC." + ClauseVersionChangeRecord.FIELD_CLAUSE_VERSION_ID + " >= ? " +
					" AND CVC." + ClauseVersionChangeRecord.FIELD_IS_QUESTION_OR_CLAUSE + " = 'C' " +
				" GROUP BY CS." + ClauseSectionRecord.FIELD_CLAUSE_SECTION_CODE + 
					", C." + ClauseRecord.FIELD_CLAUSE_NAME;
			 
			ps1 = conn.prepareStatement(sSql);
			ps1.setInt(1, clsVersionId);
			rs1 = ps1.executeQuery();
			
			// Save the list of changes.
			//ArrayNode arrayNode = mapper.createArrayNode();	
			while (rs1.next()) {			
				// bChgsFound = true;
				String group = rs1.getString(ClauseSectionRecord.FIELD_CLAUSE_SECTION_CODE);
				String groupName  = rs1.getString(ClauseSectionRecord.FIELD_CLAUSE_SECTION_HEADER);
				String title  = rs1.getString(ClauseRecord.FIELD_CLAUSE_NAME);
				String text  = rs1.getString(ClauseRecord.FIELD_CLAUSE_TITLE);
				//String isQuestionOrClause  = rs1.getString(ClauseVersionChangeRecord.FIELD_IS_QUESTION_OR_CLAUSE);
				//Integer id = rs1.getInt(ClauseVersionChangeRecord.FIELD_CLAUSE_VER_CHANGE_ID);
				
				String chgCode = rs1.getString("ChangeCodes"); 
				String changeBrief = rs1.getString("ChangeBriefs"); 
				
				String sChanges = mergeChanges (chgCode, changeBrief); 
				
				ObjectNode jsonRow = arrayNode.objectNode(); // mapper.createObjectNode();
				if (pbForApi) {
					//jsonRow.put("id", id);
					jsonRow.put(ClauseSectionRecord.FIELD_CLAUSE_SECTION_CODE, group);
					jsonRow.put(ClauseSectionRecord.FIELD_CLAUSE_SECTION_HEADER, groupName);
					jsonRow.put(ClauseRecord.FIELD_CLAUSE_NAME, title);
					jsonRow.put(ClauseRecord.FIELD_CLAUSE_TITLE, text);
					// jsonRow.put(ClauseVersionChangeRecord.FIELD_IS_QUESTION_OR_CLAUSE, isQuestionOrClause);
					//jsonRow.put(ClauseVersionChangeRecord.FIELD_CHANGE_CODE, chgCode);
					//jsonRow.put(ClauseVersionChangeRecord.FIELD_CHANGE_BRIEF, changeBrief);
					jsonRow.put("changes", sChanges);
				} 
				arrayNode.add(jsonRow);
			}		
		}
		finally {
			SqlUtil.closeInstance(rs1, ps1);
		}
	}
	
	public static void processUpdate(HttpServletRequest req, HttpServletResponse resp, UserSession oUserSession) {
		Connection conn = null;

		ObjectMapper mapper = new ObjectMapper();
		ObjectNode jsonTable = mapper.createObjectNode();
		
		Integer origDocId = CommonUtils.toInteger(req.getParameter("id"));
		if (origDocId == null) {
			PageApi.send(resp, JsonAble.JSON_RESPONSE_FORMAT, JsonAble.jsonResult(JsonAble.STATUS_FAIL, null, "Document id is missing."));
			return;
		}
		
		try {
			jsonTable.put("newId", origDocId);
			conn = ClsConfig.getDbConnection();
		
			ClsDocument clsDocument = new ClsDocument();
			if (clsDocument.loadDocument(origDocId.intValue(), conn, false) == false) {
				PageApi.send(resp, JsonAble.JSON_RESPONSE_FORMAT, JsonAble.jsonResult(JsonAble.STATUS_FAIL, jsonTable, "Invalid document id."));	
				return;
			}
			
			ArrayList<String> alChanges = new ArrayList<String>();
			
			if (clsDocument.upgradeClsVersion(alChanges, 
					(oUserSession == null ? null : oUserSession.getUserId()), 
					(oUserSession == null ? null : oUserSession.getApplicationId()), conn)) {
				PageApi.send(resp, JsonAble.JSON_RESPONSE_FORMAT, JsonAble.jsonResult(JsonAble.STATUS_SUCCESS, jsonTable, "Update successful."));			
			} else {
				PageApi.send(resp, JsonAble.JSON_RESPONSE_FORMAT, JsonAble.jsonResult(JsonAble.STATUS_FAIL, jsonTable, alChanges.get(0)));	
			}
		}
		catch (Exception oError) {
			oError.printStackTrace();
			PageApi.send(resp, JsonAble.JSON_RESPONSE_FORMAT, JsonAble.jsonResult(JsonAble.STATUS_FAIL, null, "Unable to process due to unexpected internal error."));
		}
		finally {
			SqlUtil.closeInstance(conn);
		}
	}
	
	/* Replaced with above
	public static void processUpdate(HttpServletRequest req, HttpServletResponse resp, UserSession oUserSession) {
		Connection conn = null;

		ObjectMapper mapper = new ObjectMapper();
		ObjectNode jsonTable = mapper.createObjectNode();
		
		DocumentRecord docOrigRecord = null;
		DocumentRecord docUpdRecord = null;
		// ClsDocument clsDocument = new ClsDocument();

		Integer origDocId = CommonUtils.toInteger(req.getParameter("id"));
		if (origDocId == null)
			return;
		
		try {
			conn = ClsConfig.getDbConnection();
		
			// Retrieve the document.
			docOrigRecord = DocumentTable.find (origDocId, conn);
			
			// Retrieve the latest version published.
			ClauseVersionTable.getInstance(conn);
			Integer activeClsVersionId = ClauseVersionTable.getActiveVersionId();

			if (activeClsVersionId != docOrigRecord.getClauseVersionId())
				docUpdRecord = DocumentTable.updateDocVersion(oUserSession.getUserId(), docOrigRecord, activeClsVersionId, conn) ;

			if (docUpdRecord == null)
			{
				jsonTable.put("newId", origDocId);
				PageApi.send(resp, JsonAble.JSON_RESPONSE_FORMAT, JsonAble.jsonResult(JsonAble.STATUS_FAIL, jsonTable, "No update required."));	
				return;
			}
			
			DocumentAnswerTable.updateDocVersion(docOrigRecord, docUpdRecord, conn);
			DocumentClauseTable.updateDocVersion(docOrigRecord, docUpdRecord, conn);
			DocumentFillInsTable.updateDocVersion(docOrigRecord, docUpdRecord, conn);

			// Delete the original document
			docOrigRecord.deleteDocument(oUserSession, null);
			//if (clsDocument.loadDocument(docOrigRecord.getId(), null)) 
			//	clsDocument.deleteDocument(oUserSession, null);
			
			// Send back response.
			jsonTable.put("newId", docUpdRecord.getId());
			PageApi.send(resp, JsonAble.JSON_RESPONSE_FORMAT, JsonAble.jsonResult(JsonAble.STATUS_SUCCESS, jsonTable, "Update successful."));			
		}
		catch (Exception oError) {
			oError.printStackTrace();
			PageApi.send(resp, JsonAble.JSON_RESPONSE_FORMAT, JsonAble.jsonResult(JsonAble.STATUS_FAIL, null, "Unable to process due to unexpected internal error."));
		}
		finally {
			SqlUtil.closeInstance(conn);
		}
	}
	*/
	
	public static void processFinalization(HttpServletRequest req, HttpServletResponse resp, UserSession oUserSession) {
		
		PreparedStatement ps1 = null;
		ResultSet rs1 = null;
		Connection conn = null;

		DocumentRecord docOrigRecord = null;

		Integer docId = CommonUtils.toInteger(req.getParameter("id"));
		if (docId == null)
			return;
		
		try {
			conn = ClsConfig.getDbConnection();
		
			// Retrieve the document.
			docOrigRecord = DocumentTable.find (docId, conn);
			
			// Finalize the record.
			DocumentTable.updateDocStatus(docOrigRecord, "F", conn) ;

			// Send back response.
			PageApi.send(resp, JsonAble.JSON_RESPONSE_FORMAT, JsonAble.jsonResult(JsonAble.STATUS_SUCCESS, null, "The document has been finalized."));			
			
		}
		catch (Exception oError) {
			oError.printStackTrace();
		}
		finally {
			SqlUtil.closeInstance(rs1, ps1, conn);
		}
	}

	private static void loadClsContent(ObjectNode json, String group, String groupName, String title, String text, String isQuestionOrClause, String chgCode, String changeBrief, Integer id) {

		json.put("group", group);
		json.put("groupName", groupName);
		json.put("title", title);
		json.put("text", text);
		json.put(ClauseVersionChangeRecord.FIELD_IS_QUESTION_OR_CLAUSE, isQuestionOrClause);
		json.put(ClauseVersionChangeRecord.FIELD_CHANGE_CODE, chgCode);
		json.put(ClauseVersionChangeRecord.FIELD_CHANGE_BRIEF, changeBrief);
		json.put(ClauseVersionChangeRecord.FIELD_CLAUSE_VER_CHANGE_ID, id);
	}
	
	public static ClauseVersionChangeTable load(int clauseVersionId, Integer piPreviousClauseVerionId, Connection conn) 
			throws SQLException {
		ClauseVersionChangeTable result = new ClauseVersionChangeTable();
		
		String sSql = ClauseVersionChangeRecord.SQL_SELECT
				+ " WHERE " + ClauseVersionChangeRecord.FIELD_CLAUSE_VERSION_ID + " = ?";
		if (piPreviousClauseVerionId != null)
			sSql += " AND " + ClauseVersionChangeRecord.FIELD_PREVIOUS_CLAUSE_VERSION_ID + " = ?";
		sSql += " ORDER BY " + ClauseVersionChangeRecord.FIELD_QUESTION_OR_CLAUSE_CODE;
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = conn.prepareStatement(sSql);
			ps.setInt(1, clauseVersionId);
			if (piPreviousClauseVerionId != null)
				ps.setInt(2, piPreviousClauseVerionId);
			rs = ps.executeQuery();
			while (rs.next()) {
				ClauseVersionChangeRecord oRecord = new ClauseVersionChangeRecord();
				oRecord.read(rs);
				result.add(oRecord);
			}
		}
		finally {
			SqlUtil.closeInstance(rs, ps);
		}
		return result;
	}

	public ClauseVersionChangeRecord findByCode(String psCode) {
		for (ClauseVersionChangeRecord oRecord : this) {
			if (oRecord.getQuestionOrClauseCode().equals(psCode))
				return oRecord;
		}
		return null;
	}
	
	private static String mergeChanges (String sCodes, String sBriefs) {
		String sResult = "";
		
		String[] codeList = sCodes.split("\\|");
		String[] briefList = sBriefs.split("\\|");
		Integer chgCnt = codeList.length;
		
		for (Integer i = 0; i< chgCnt; i++) {
			String sNewCode = "[" + codeList[i].trim() + ": " + briefList[i].trim() + "], ";
			Integer iPosCode = sResult.indexOf(sNewCode);
			Integer iPosBracket = sResult.lastIndexOf("[");

			if ((iPosCode < 0) || (iPosCode < iPosBracket))
				sResult += sNewCode;
		}

		StringBuilder b = new StringBuilder(sResult);
		b.replace(sResult.lastIndexOf("], "), sResult.lastIndexOf("], ") + 3, "]" );
		sResult = b.toString();

		return sResult;
	}
}
