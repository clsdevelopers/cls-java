package gov.dod.cls.db;

import gov.dod.cls.model.JsonAble;
import gov.dod.cls.utils.CommonUtils;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import org.codehaus.jackson.node.ObjectNode;

public class QuestionGroupRefRecord extends JsonAble {

	public static final String TABLE_QUESTION_GROUP_REF = "Question_Group_Ref";
	
	public static final String FIELD_QUESTION_GROUP = "question_group";
	public static final String FIELD_QUESTION_GROUP_NAME = "question_group_name";
	public static final String FIELD_CREATED_AT = "created_at";
	public static final String FIELD_UPDATED_AT = "updated_at";
	
	public static final String SQL_SELECT
		= "SELECT " + FIELD_QUESTION_GROUP
		+ ", " + FIELD_QUESTION_GROUP_NAME
		+ ", " + FIELD_CREATED_AT
		+ ", " + FIELD_UPDATED_AT
		+ " FROM " + TABLE_QUESTION_GROUP_REF
		+ " ORDER BY " + FIELD_QUESTION_GROUP;

	// ------------------------------------------------------------
	private String code; 
	private String name; 
	private Date createdAt; 
	private Date updatedAt;

	public void read(ResultSet rs) throws SQLException {
		this.code = rs.getString(FIELD_QUESTION_GROUP); 
		this.name = rs.getString(FIELD_QUESTION_GROUP_NAME); 
		this.createdAt = rs.getTimestamp(FIELD_CREATED_AT);
		this.updatedAt = rs.getTimestamp(FIELD_UPDATED_AT); 
	}
	
	@Override
	protected void populateJson(ObjectNode json, boolean forInterview) {
		json.put(FIELD_QUESTION_GROUP, this.code);
		json.put(FIELD_QUESTION_GROUP_NAME, this.name);
		if (!forInterview) {
			json.put(FIELD_CREATED_AT, CommonUtils.toJsonDate(this.createdAt));
			json.put(FIELD_UPDATED_AT, CommonUtils.toJsonDate(this.updatedAt));
		}
	};

	// ------------------------------------------------------------
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Date getCreatedAt() {
		return createdAt;
	}
	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}
	public Date getUpdatedAt() {
		return updatedAt;
	}
	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}
	
}
