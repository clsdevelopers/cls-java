package gov.dod.cls.db;

import gov.dod.cls.PageApi;
import gov.dod.cls.bean.session.UserSession;
import gov.dod.cls.config.ClsConfig;
import gov.dod.cls.model.JsonAble;
import gov.dod.cls.utils.CommonUtils;
import gov.dod.cls.utils.Pagination;
import gov.dod.cls.utils.sql.SqlUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.node.ArrayNode;
import org.codehaus.jackson.node.ObjectNode;

public class ClauseSectionTable extends ArrayList<ClauseSectionRecord> {
	
	private static final long serialVersionUID = 2881221181419582906L;

	// ----------------------------------------------------------
	public static final String PREFIX_DO_CLAUSE_SECTION = "clausesection-";
	private static final String DO_DOCUMENT_LIST = PREFIX_DO_CLAUSE_SECTION + "list";
	public static final String DO_SECTION_DETAILS_GET = PREFIX_DO_CLAUSE_SECTION + "detail-get";

	public static void processRequest(String sDo, UserSession oUserSession, 
			HttpServletRequest req, HttpServletResponse resp) {
		if (!oUserSession.isSuperUser()) { // CJ-680
			return;
		}
		
		switch (sDo) {
		case ClauseSectionTable.DO_DOCUMENT_LIST:
			ClauseSectionTable.processList(req, resp);
			return;
		case ClauseSectionTable.DO_SECTION_DETAILS_GET:
			ClauseSectionTable.getSectionDetails(sDo, oUserSession, req, resp);
			return;	
		}
	}
	
	// ----------------------------------------------------------
	public static final String SQL_LIST
		= "SELECT A." + ClauseSectionRecord.FIELD_CLAUSE_SECTION_ID
		+ ", A." + ClauseSectionRecord.FIELD_CLAUSE_SECTION_CODE
		+ ", A." + ClauseSectionRecord.FIELD_CLAUSE_SECTION_HEADER
		+ " FROM " + ClauseSectionRecord.TABLE_CLAUSE_SECTIONS + " A";
		// + " ORDER BY 2";

	private static void processList(HttpServletRequest req, HttpServletResponse resp) {
		String query = req.getParameter("query");		
		String sort_field = req.getParameter("sort_field");
		String sort_order = req.getParameter("sort_order");
		
		Pagination pagination = new Pagination(req);
		pagination.addDataSource(ClauseSectionRecord.TABLE_CLAUSE_SECTIONS);
		Connection conn = null;
		PreparedStatement ps1 = null;
		ResultSet rs1 = null;
		try {
			conn = ClsConfig.getDbConnection();
			String sSql = "SELECT COUNT(A." + ClauseSectionRecord.FIELD_CLAUSE_SECTION_ID + ") FROM " + ClauseSectionRecord.TABLE_CLAUSE_SECTIONS + " A";
			
			String operator = " WHERE ";
			if (CommonUtils.isNotEmpty(query)) {
				sSql += operator + "(LOWER(A." + ClauseSectionRecord.FIELD_CLAUSE_SECTION_CODE+ ") LIKE ?" 
					+ " OR LOWER(A." + ClauseSectionRecord.FIELD_CLAUSE_SECTION_HEADER + ") LIKE ?"
					+ ")";
				operator = " AND ";
				query = "%" + query.toLowerCase() + "%";
			}			
			ps1 = conn.prepareStatement(sSql);			
			int iParam = 1;
			if (CommonUtils.isNotEmpty(query)) {
				ps1.setString(iParam++, query);
				ps1.setString(iParam++, query);
			}			
			rs1 = ps1.executeQuery();
			boolean isAcceptableRange = false;
			if (rs1.next())
				isAcceptableRange = pagination.isAcceptedRange(rs1.getInt(1));
			
			if (isAcceptableRange) {
				SqlUtil.closeInstance(rs1);
				rs1 = null;
				SqlUtil.closeInstance(ps1);
				ps1 = null;
				
				sSql = SQL_LIST;
				operator = " WHERE ";
				if (CommonUtils.isNotEmpty(query)) {
					sSql += operator + "(LOWER(A." + ClauseSectionRecord.FIELD_CLAUSE_SECTION_CODE+ ") LIKE ?" 
							+ " OR LOWER(A." + ClauseSectionRecord.FIELD_CLAUSE_SECTION_HEADER + ") LIKE ?"
							+ ")";
						operator = " AND ";
				}
				String sAsc = ("desc".equals(sort_order) ? " DESC" : "");
				String orderField = ClauseSectionRecord.FIELD_CLAUSE_SECTION_CODE;
				if ("section_header".equals(sort_field))
					orderField = ClauseSectionRecord.FIELD_CLAUSE_SECTION_HEADER;
				sSql += " ORDER BY " + orderField + sAsc;

				ps1 = conn.prepareStatement(sSql + pagination.sqlLimit());
				iParam = 1;
				if (CommonUtils.isNotEmpty(query)) {
					ps1.setString(iParam++, query);
					ps1.setString(iParam++, query);
				}
								
				rs1 = ps1.executeQuery();
				while (rs1.next()) {
					ObjectNode json = pagination.getMapper().createObjectNode();
					json.put(ClauseSectionRecord.FIELD_CLAUSE_SECTION_ID, rs1.getInt(ClauseSectionRecord.FIELD_CLAUSE_SECTION_ID));
					json.put(ClauseSectionRecord.FIELD_CLAUSE_SECTION_CODE, rs1.getString(ClauseSectionRecord.FIELD_CLAUSE_SECTION_CODE));
					json.put(ClauseSectionRecord.FIELD_CLAUSE_SECTION_HEADER, rs1.getString(ClauseSectionRecord.FIELD_CLAUSE_SECTION_HEADER));
						
					pagination.incCount();
					pagination.getArrayNode().add(json);
				}
			}
			pagination.send(resp);
		}
		catch (Exception oError) {
			oError.printStackTrace();
		}
		finally {
			SqlUtil.closeInstance(rs1, ps1, conn);
		}
	}
	
	private static void getSectionDetails(String sDo, UserSession oUserSession, 
			HttpServletRequest req, HttpServletResponse resp) {
		Integer id = CommonUtils.toInteger(req.getParameter("id"));
		String errorMessage = null;
		ClauseSectionRecord clauseSectionRecord = null;
		if (id != null) {
			try {
				clauseSectionRecord = ClauseSectionRecord.getRecord(id);
				if (clauseSectionRecord == null)
					errorMessage = "The requested record is not found.";
			} catch (Exception oError) {
				errorMessage = "Unable to retrieve record. Error: " + oError.getMessage();
			}
		} else
			errorMessage = "Unauthorized request";
		String response;
		if (errorMessage == null) {
			ObjectNode objectNode = (ObjectNode)clauseSectionRecord.toJson();
			response = JsonAble.jsonResult(JsonAble.STATUS_SUCCESS, objectNode, null);
		} else
			response = JsonAble.jsonResult(JsonAble.STATUS_FAIL, null, errorMessage);
		PageApi.send(resp, JsonAble.JSON_RESPONSE_FORMAT, response);
	}

	// ========================================================
	private volatile static ClauseSectionTable instance = null;
	private volatile static Date refreshed = null;
	
	public synchronized static ClauseSectionTable getInstance(Connection conn) {
		if (ClauseSectionTable.instance == null) {
			ClauseSectionTable.instance = new ClauseSectionTable();
			ClauseSectionTable.instance.refresh(conn);
		} else {
			ClauseSectionTable.instance.refreshWhenNeeded(conn);
		}
		return ClauseSectionTable.instance;
	}
	
	public static ClauseSectionTable getSubset(ArrayList<Integer> aIds) {
		ClauseSectionTable clauseSectionTable = ClauseSectionTable.getInstance(null);
		return clauseSectionTable.subsetByList(aIds);
	}

	public static ArrayNode getJson(ObjectMapper mapper, boolean forInterview) {
		ClauseSectionTable clauseSectionTable = ClauseSectionTable.getInstance(null);
		return clauseSectionTable.toJson(mapper, forInterview);
	}

	// Functions -----------------------------------------------------------------
	public static Date getReferHistoryDate(Connection conn) {
		return SysLastTableChangeAtTable.getLastUpdateDate(conn, ClauseSectionRecord.TABLE_CLAUSE_SECTIONS);
	}
	  
	// ========================================================
	private ArrayList<Integer> solicitationSections = null;
	private Exception lastError;

	public void refreshWhenNeeded(Connection conn) {
		if (SysLastTableChangeAtTable.needToLoad(ClauseSectionTable.refreshed, getReferHistoryDate(conn)))
			this.refresh(conn);
	}

	private boolean refresh(Connection conn) {
		boolean result = false;
		String sSql = ClauseSectionRecord.SQL_SELECT
				+ " ORDER BY " + ClauseSectionRecord.FIELD_CLAUSE_SECTION_CODE;	
		boolean bCreateConnection = (conn == null);
		PreparedStatement ps1 = null;
		ResultSet rs1 = null;
		try {
			if (bCreateConnection)
				conn = ClsConfig.getDbConnection();
			ps1 = conn.prepareStatement(sSql);
			rs1 = ps1.executeQuery();
			this.clear();
			while (rs1.next()) {
				ClauseSectionRecord clauseSectionRecord = new ClauseSectionRecord();
				clauseSectionRecord.read(rs1);
				this.add(clauseSectionRecord);
			}
			ClauseSectionTable.refreshed = getReferHistoryDate(conn); // CommonUtils.getNow();
			result = true;
			System.out.println("ClauseSectionTable refreshed");
		}
		catch (Exception oError) {
			this.lastError = oError;
			oError.printStackTrace();
		}
		finally {
			SqlUtil.closeInstance(rs1, ps1);
			if (bCreateConnection)
				SqlUtil.closeInstance(conn);
		}
		return result;
	}
	
	public ClauseSectionTable subsetByList(ArrayList<Integer> aIds) {
		ClauseSectionTable result = new ClauseSectionTable();
		for (Integer id : aIds) {
			ClauseSectionRecord clauseSectionRecord = this.findById(id);
			if (clauseSectionRecord != null)
				result.add(clauseSectionRecord);
		}
		return result;
	}

	public Date getRefreshed() {
		return refreshed;
	}
	
	public ClauseSectionRecord findById(int id) {
		for (ClauseSectionRecord clauseSectionRecord : this) {
			if (clauseSectionRecord.getId().intValue() == id)
				return clauseSectionRecord;
		}
		return null;
	}

	public ArrayNode toJson(ObjectMapper mapper, boolean forInterview) {
		if (mapper == null)
			mapper = new ObjectMapper();
		ArrayNode json = mapper.createArrayNode();
		for (ClauseSectionRecord clauseSectionRecord : this) {
			json.add(clauseSectionRecord.toJson(forInterview));
		}
		return json;
	}

	public Exception getLastError() {
		return lastError;
	}
	
	public boolean isSolicitationSelection(Integer piSectionId) {
		if (this.solicitationSections == null) {
			this.solicitationSections = new ArrayList<Integer>();
			for (ClauseSectionRecord clauseSectionRecord : this) {
				String sCode = clauseSectionRecord.getName();
				if ("K".equals(sCode) || "L".equals(sCode) || "M".equals(sCode))
					this.solicitationSections.add(clauseSectionRecord.getId());
			}
		}
		return this.solicitationSections.contains(piSectionId);
	}
	
}
