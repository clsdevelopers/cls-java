package gov.dod.cls.db;

import gov.dod.cls.config.ClsConfig;
import gov.dod.cls.model.JsonAble;
import gov.dod.cls.utils.CommonUtils;
import gov.dod.cls.utils.sql.SqlUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.node.ArrayNode;
import org.codehaus.jackson.node.ObjectNode;

public class ClauseSectionRecord extends JsonAble {

	public static final String TABLE_CLAUSE_SECTIONS = "Clause_Sections";
	
	public static final String FIELD_CLAUSE_SECTION_ID = "clause_section_id"; 
	public static final String FIELD_CLAUSE_SECTION_CODE = "clause_section_code";
	public static final String FIELD_CLAUSE_SECTION_HEADER = "clause_section_header";
	public static final String FIELD_CREATED_AT = "created_at";
	public static final String FIELD_UPDATED_AT = "updated_at";
	
	public static final String SQL_SELECT
		= "SELECT " + FIELD_CLAUSE_SECTION_ID
		+ ", " + FIELD_CLAUSE_SECTION_CODE
		+ ", " + FIELD_CLAUSE_SECTION_HEADER
		+ ", " + FIELD_CREATED_AT
		+ ", " + FIELD_UPDATED_AT
		+ " FROM " + TABLE_CLAUSE_SECTIONS;
		// + " ORDER BY " + FIELD_CLAUSE_SECTION_CODE;
	
	public static final String SQL_CLAUSES_SELECT
		= "SELECT A." + ClauseRecord.FIELD_CLAUSE_ID 
		+ ", A." + ClauseRecord.FIELD_CLAUSE_NAME
		+ ", A." + ClauseRecord.FIELD_CLAUSE_TITLE
		+ " FROM " + ClauseRecord.TABLE_CLAUSES + " A"
		+ " INNER JOIN " + TABLE_CLAUSE_SECTIONS + " B"
		+ " ON (B." + FIELD_CLAUSE_SECTION_ID + " = A." + ClauseRecord.FIELD_CLAUSE_SECTION_ID + ") "
		+ " WHERE A." + ClauseRecord.FIELD_CLAUSE_SECTION_ID + " = ?"
		+ " ORDER BY A." + ClauseRecord.FIELD_CLAUSE_NAME;

	// ------------------------------------------------------------
	private Integer id; 
	private String name; 
	private String header; 
	private Date createdAt; 
	private Date updatedAt;
	
	private static ArrayNode arrayNodeClauses;

	public void read(ResultSet rs) throws SQLException {
		this.id = rs.getInt(FIELD_CLAUSE_SECTION_ID); 
		this.name = rs.getString(FIELD_CLAUSE_SECTION_CODE); 
		this.header = rs.getString(FIELD_CLAUSE_SECTION_HEADER); 
		this.createdAt = rs.getTimestamp(FIELD_CREATED_AT);
		this.updatedAt = rs.getTimestamp(FIELD_UPDATED_AT); 
	}
	
	@Override
	protected void populateJson(ObjectNode json, boolean forInterview) {
		json.put(FIELD_CLAUSE_SECTION_ID, this.id);
		json.put(FIELD_CLAUSE_SECTION_CODE, this.name);
		json.put(FIELD_CLAUSE_SECTION_HEADER, this.header);
		if (!forInterview) {
			json.put(FIELD_CREATED_AT, CommonUtils.toJsonDate(this.createdAt));
			json.put(FIELD_UPDATED_AT, CommonUtils.toJsonDate(this.updatedAt));
			json.put(ClauseRecord.TABLE_CLAUSES, arrayNodeClauses);
		}
	};
	
	public static ClauseSectionRecord getRecord(Integer id) {
		if (id == null) 
			return null;
		
		Connection conn = null;
		try {
			conn = ClsConfig.getDbConnection();
			return ClauseSectionRecord.getRecord(conn, id, null);
		}
		catch (Exception oError) {
			oError.printStackTrace();
		}
		finally {
			SqlUtil.closeInstance(conn);
		}
		return null;
	}
	
	public static ClauseSectionRecord getRecord(Connection conn, Integer sectionId, ClauseSectionRecord pClauseSectionRecord) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			String sSql = SQL_SELECT;
			String operator = " WHERE ";
			if (sectionId != null) {
				sSql += operator + " " + FIELD_CLAUSE_SECTION_ID + " =?";
				// sSql += operator + " A." + FIELD_CLAUSE_SECTION_ID + "=?";
				// operator = " AND ";
			}
			ps = conn.prepareStatement(sSql);
			int iParam = 1;
			if (sectionId != null)
				ps.setInt(iParam++, sectionId);
			rs = ps.executeQuery();
			if (rs.next()) {
				if (pClauseSectionRecord == null)
					pClauseSectionRecord = new ClauseSectionRecord();
				pClauseSectionRecord.read(rs);
			}
			//---------------
			SqlUtil.closeInstance(rs, ps);
			rs = null;
			ps = null;
			ps = conn.prepareStatement(SQL_CLAUSES_SELECT);
			ObjectMapper mapper = new ObjectMapper();
			ObjectNode json = mapper.createObjectNode();
			arrayNodeClauses = json.arrayNode();
			ps.setInt(1, sectionId);
			rs = ps.executeQuery();
			while (rs.next()) {
				ObjectNode elementNode = json.objectNode();
				loadClause(rs, elementNode);
				arrayNodeClauses.add(elementNode);				
			}			
		}
		catch (Exception oError) {
			oError.printStackTrace();
		}
		finally {
			SqlUtil.closeInstance(rs, ps);
		}
		return pClauseSectionRecord;
	}
		
	private static void loadClause(ResultSet rs, ObjectNode elementNode) throws SQLException {	
		elementNode.put(ClauseRecord.FIELD_CLAUSE_ID, rs.getInt(ClauseRecord.FIELD_CLAUSE_ID));
		elementNode.put(ClauseRecord.FIELD_CLAUSE_NAME, rs.getString(ClauseRecord.FIELD_CLAUSE_NAME));
		elementNode.put(ClauseRecord.FIELD_CLAUSE_TITLE, rs.getString(ClauseRecord.FIELD_CLAUSE_TITLE));
	}
	
	// ------------------------------------------------------------
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getHeader() {
		return header;
	}
	public void setHeader(String header) {
		this.header = header;
	}
	public Date getCreatedAt() {
		return createdAt;
	}
	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}
	public Date getUpdatedAt() {
		return updatedAt;
	}
	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}
	public Integer getId() {
		return id;
	}
	
	public String getFullName() {
		if ("N".equals(this.name))
			return "No Section";
		return "Section " + this.name + " - " + this.header;
	}
	
}
