package gov.dod.cls.db;

import gov.dod.cls.bean.session.UserSession;
import gov.dod.cls.config.ClsConfig;
import gov.dod.cls.utils.CommonUtils;
import gov.dod.cls.utils.Pagination;
import gov.dod.cls.utils.sql.SqlUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.codehaus.jackson.node.ArrayNode;
import org.codehaus.jackson.node.ObjectNode;

public class AuditEventTable {
	
	// ----------------------------------------------------------
	public static final String PREFIX_DO_AUDIT_EVENT = "auditEvent-";
	private static final String DO_AUDIT_EVENT_LIST = PREFIX_DO_AUDIT_EVENT + "list";

	public static void processRequest(String sDo, UserSession oUserSession, 
			HttpServletRequest req, HttpServletResponse resp) {
		if (!oUserSession.isSuperUser()) { // CJ-680
			return;
		}
		
		switch (sDo) {
		case AuditEventTable.DO_AUDIT_EVENT_LIST:
			AuditEventTable.processList(req, resp);
		}
	}
	
	// ----------------------------------------------------------
	public static final String TAG_USER_LOGS = "UserLogs";
	
	// CJ-1410, Add User/Application Name, Document Name to list
	private static String getSqlStatement(boolean forOneUser) {
		return "SELECT A." + AuditEventRecord.FIELD_AUDIT_EVENT_ID
				+ ", A." + AuditEventRecord.FIELD_ACTION
				+ ", A." + AuditEventRecord.FIELD_INITIATED_USER_ID
				+ ", A." + AuditEventRecord.FIELD_REFERENCE_DOCUMENT_NUMBER
				+ ", A." + AuditEventRecord.FIELD_REFERENCE_USER_NAME
				//+ ", CONCAT(B." + UserRecord.FIELD_FIRST_NAME + ", ' ', B." + UserRecord.FIELD_LAST_NAME + ") AS InitiatedUserName"
				//+ ", IFNULL (CONCAT(B." + UserRecord.FIELD_FIRST_NAME + ", ' ', B." + UserRecord.FIELD_LAST_NAME + "),  OA."+ OauthApplicationRecord.FIELD_APPLICATION_NAME +" ) AS InitiatedUserName"
				+ ", A." + AuditEventRecord.FIELD_ADDITIONAL_DATA
				+ ", A." + AuditEventRecord.FIELD_CREATED_AT
				+ ", A." + AuditEventRecord.FIELD_UPDATED_AT
				+ " FROM " + AuditEventRecord.TABLE_AUDIT_EVENTS + " A"
				+ " LEFT OUTER JOIN " + UserRecord.TABLE_USERS + " B"
				+ " ON (B." + UserRecord.FIELD_USER_ID + " = A." + AuditEventRecord.FIELD_INITIATED_USER_ID + ")"
				//+ " LEFT OUTER JOIN " + OauthApplicationRecord.TABLE_OAUTH_APPLICATIONS + " OA "
				//+ " ON (OA." + OauthApplicationRecord.FIELD_APPLICATION_ID + " = A." + AuditEventRecord.FIELD_REFERENCE_APPLICATION_ID + ")"
				+ (forOneUser
					? " WHERE A." + AuditEventRecord.FIELD_USER_ID + "=?"
					: "")
				+ " ORDER BY A." + AuditEventRecord.FIELD_UPDATED_AT + " DESC"
				+ ", A." + AuditEventRecord.FIELD_CREATED_AT + " DESC";
	}
	
	// used to query Audit Events page with filters.
	private static String getSqlFilterStatement(String sActionType, String sQuery, String sDate) {
		
		String operator = " WHERE ";
		String sSqlFilter = "";
		if (CommonUtils.isNotEmpty(sActionType)) {
			sSqlFilter += operator + AuditEventRecord.FIELD_ACTION + " = ?";
			operator = " AND ";
		}
		
		if (CommonUtils.isNotEmpty(sQuery)) {
			sSqlFilter += operator + " (" + AuditEventRecord.FIELD_REFERENCE_USER_NAME + " LIKE ?" 
					+ " OR " + AuditEventRecord.FIELD_REFERENCE_DOCUMENT_NUMBER + " LIKE ?) " ;
			operator = " AND ";
		}
		
		if (CommonUtils.isNotEmpty(sDate)) {
			sSqlFilter += operator + " DATE(A." + AuditEventRecord.FIELD_CREATED_AT + ") = ?" ;
			operator = " AND ";
		}
		
		return "SELECT A." + AuditEventRecord.FIELD_AUDIT_EVENT_ID
				+ ", A." + AuditEventRecord.FIELD_ACTION
				+ ", A." + AuditEventRecord.FIELD_INITIATED_USER_ID
				+ ", A." + AuditEventRecord.FIELD_REFERENCE_DOCUMENT_NUMBER
				+ ", A." + AuditEventRecord.FIELD_REFERENCE_USER_NAME
				//+ ", CONCAT(B." + UserRecord.FIELD_FIRST_NAME + ", ' ', B." + UserRecord.FIELD_LAST_NAME + ") AS InitiatedUserName"
				//+ ", IFNULL (CONCAT(B." + UserRecord.FIELD_FIRST_NAME + ", ' ', B." + UserRecord.FIELD_LAST_NAME + "),  OA."+ OauthApplicationRecord.FIELD_APPLICATION_NAME +" ) AS InitiatedUserName"
				+ ", A." + AuditEventRecord.FIELD_ADDITIONAL_DATA
				+ ", A." + AuditEventRecord.FIELD_CREATED_AT
				+ ", A." + AuditEventRecord.FIELD_UPDATED_AT
				+ " FROM " + AuditEventRecord.TABLE_AUDIT_EVENTS + " A"
				+ " LEFT OUTER JOIN " + UserRecord.TABLE_USERS + " B"
				+ " ON (B." + UserRecord.FIELD_USER_ID + " = A." + AuditEventRecord.FIELD_INITIATED_USER_ID + ")"
				//+ " LEFT OUTER JOIN " + OauthApplicationRecord.TABLE_OAUTH_APPLICATIONS + " OA "
				//+ " ON (OA." + OauthApplicationRecord.FIELD_APPLICATION_ID + " = A." + AuditEventRecord.FIELD_REFERENCE_APPLICATION_ID + ")"
				+ sSqlFilter
				+ " ORDER BY A." + AuditEventRecord.FIELD_UPDATED_AT + " DESC"
				+ ", A." + AuditEventRecord.FIELD_CREATED_AT + " DESC";
	}
	
	private static String getSqlCountStatement(String sActionType, String sQuery, String sDate) {
		
		String operator = " WHERE ";
		String sSqlFilter = "";
		if (CommonUtils.isNotEmpty(sActionType)) {
			sSqlFilter += operator + AuditEventRecord.FIELD_ACTION + " = ?";
			operator = " AND ";
		}
		
		if (CommonUtils.isNotEmpty(sQuery)) {
			sSqlFilter += operator + " (" + AuditEventRecord.FIELD_REFERENCE_USER_NAME + " LIKE ?" 
					+ " OR " + AuditEventRecord.FIELD_REFERENCE_DOCUMENT_NUMBER + " LIKE ?) " ;
			operator = " AND ";
		}
		
		if (CommonUtils.isNotEmpty(sDate)) {
			sSqlFilter += operator + " DATE(" + AuditEventRecord.FIELD_CREATED_AT + ") = ?" ;
			operator = " AND ";
		}
		
		return "SELECT COUNT(" + AuditEventRecord.FIELD_AUDIT_EVENT_ID + ") FROM " + AuditEventRecord.TABLE_AUDIT_EVENTS +
				sSqlFilter;
	}
	
	public static void addJson(Integer userId, ObjectNode json) throws SQLException {
		Connection conn = null;
		try {
			conn = ClsConfig.getDbConnection();
			AuditEventTable.addJson(userId, json, conn); 
		}
		finally {
			SqlUtil.closeInstance(conn);
		}
	}
	
	private static void load(ResultSet rs, ObjectNode elementNode) throws SQLException {
		elementNode.put(AuditEventRecord.FIELD_AUDIT_EVENT_ID, rs.getInt(AuditEventRecord.FIELD_AUDIT_EVENT_ID));
		elementNode.put(AuditEventRecord.FIELD_ACTION, rs.getString(AuditEventRecord.FIELD_ACTION));
		elementNode.put(AuditEventRecord.FIELD_INITIATED_USER_ID, rs.getInt(AuditEventRecord.FIELD_INITIATED_USER_ID));
		elementNode.put(AuditEventRecord.FIELD_REFERENCE_USER_NAME, rs.getString(AuditEventRecord.FIELD_REFERENCE_USER_NAME));
		elementNode.put(AuditEventRecord.FIELD_REFERENCE_DOCUMENT_NUMBER, rs.getString(AuditEventRecord.FIELD_REFERENCE_DOCUMENT_NUMBER));
		elementNode.put(AuditEventRecord.FIELD_ADDITIONAL_DATA, rs.getString(AuditEventRecord.FIELD_ADDITIONAL_DATA));
		elementNode.put(AuditEventRecord.FIELD_CREATED_AT, CommonUtils.toJsonDate(rs.getTimestamp(AuditEventRecord.FIELD_CREATED_AT)));
		elementNode.put(AuditEventRecord.FIELD_UPDATED_AT, CommonUtils.toJsonDate(rs.getTimestamp(AuditEventRecord.FIELD_UPDATED_AT)));
	}
	
	public static void addJson(Integer userId, ObjectNode json, Connection conn) {
		ArrayNode arrayNode = json.arrayNode();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = conn.prepareStatement(getSqlStatement(userId != null));
			if (userId != null)
				ps.setInt(1, userId);
			rs = ps.executeQuery();
			while (rs.next()) {
				ObjectNode elementNode = json.objectNode();
				load(rs, elementNode);
				arrayNode.add(elementNode);
			}
		}
		catch (Exception oError) {
			oError.printStackTrace();
		}
		finally {
			SqlUtil.closeInstance(rs, ps);
		}
		json.put(TAG_USER_LOGS, arrayNode);
	}
	
	@SuppressWarnings("resource")
	private static void processList(HttpServletRequest req, HttpServletResponse resp) {
		String action_type = req.getParameter("action_type");
		String query_str = req.getParameter("query");
		String audit_date = req.getParameter("audit_date");

		Pagination pagination = new Pagination(req);
		pagination.addDataSource(AuditEventRecord.TABLE_AUDIT_EVENTS);
		Connection conn = null;
		PreparedStatement ps1 = null;
		ResultSet rs1 = null;
		try {
			conn = ClsConfig.getDbConnection();
			String sSql = getSqlCountStatement  (action_type, query_str, audit_date);
			
			ps1 = conn.prepareStatement(sSql);
			
			// CJ-1410 - add filters.
			int iParam = 1;
			iParam = addStringParam(ps1, iParam, action_type);
			iParam = addStringParam(ps1, iParam, query_str);
			iParam = addStringParam(ps1, iParam, query_str);
			iParam = addStringParam(ps1, iParam, audit_date);

			rs1 = ps1.executeQuery();
			boolean isAcceptableRange = false;
			if (rs1.next())
				isAcceptableRange = pagination.isAcceptedRange(rs1.getInt(1));
			
			if (isAcceptableRange) {
				rs1 = SqlUtil.closeInstance(rs1);
				ps1 = SqlUtil.closeInstance(ps1);

				ps1 = conn.prepareStatement(getSqlFilterStatement(action_type, query_str, audit_date) + pagination.sqlLimit());
				
				// CJ-1410 - add filters.
				iParam = 1;
				iParam = addStringParam(ps1, iParam, action_type);
				iParam = addStringParam(ps1, iParam, query_str);
				iParam = addStringParam(ps1, iParam, query_str);
				iParam = addStringParam(ps1, iParam, audit_date);
				
				rs1 = ps1.executeQuery();
				while (rs1.next()) {
					ObjectNode json = pagination.getMapper().createObjectNode();
					load(rs1, json);
					pagination.incCount();
					pagination.getArrayNode().add(json);
				}
			}
			pagination.send(resp);
		}
		catch (Exception oError) {
			oError.printStackTrace();
		}
		finally {
			SqlUtil.closeInstance(rs1, ps1, conn);
		}
	}
	
	private static int addStringParam (PreparedStatement ps, int iparam, String sFilter) throws SQLException {
		int iParam = iparam;
		if (CommonUtils.isNotEmpty(sFilter)) {
			ps.setString(iParam++, sFilter);
		}
		return iParam;
	}
	
	public static void deleteDocument(int docId, Connection conn) throws SQLException {
		PreparedStatement ps1 = null;
		try {
			String sSql = "DELETE FROM " + AuditEventRecord.TABLE_AUDIT_EVENTS
					+ " WHERE " + AuditEventRecord.FIELD_DOCUMENT_ID + " = ?";
			ps1 = conn.prepareStatement(sSql);
			ps1.setInt(1, docId);
			ps1.execute();
		}
		finally {
			SqlUtil.closeInstance(ps1);
		}
	}

	public static void deleteApplication(int applicationId, Connection conn) throws SQLException {
		PreparedStatement ps1 = null;
		try {
			String sSql = "DELETE FROM " + AuditEventRecord.TABLE_AUDIT_EVENTS
					+ " WHERE " + AuditEventRecord.FIELD_APPLICATION_ID + " = ?";
			ps1 = conn.prepareStatement(sSql);
			ps1.setInt(1, applicationId);
			ps1.execute();
		}
		finally {
			SqlUtil.closeInstance(ps1);
		}
	}

}
