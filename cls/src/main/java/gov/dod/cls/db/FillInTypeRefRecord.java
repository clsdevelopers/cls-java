package gov.dod.cls.db;

import gov.dod.cls.model.JsonAble;

public class FillInTypeRefRecord extends JsonAble {

	public static final String TYPE_CD_AMOUNT = "$"; //	Amount
	public static final String TYPE_CD_CHECKBOX = "C"; //	Checkbox
	public static final String TYPE_CD_DATE = "D"; // ate
	public static final String TYPE_CD_EMAIL = "E"; // Email
	public static final String TYPE_CD_MEMO = "M"; // Memo
	public static final String TYPE_CD_NUMBER = "N"; // Number
	public static final String TYPE_CD_RADIO = "R"; // Radio
	public static final String TYPE_CD_STRING = "S"; // String
	public static final String TYPE_CD_URL = "U"; // Url	

	public static final String TABLE_FILL_INS_TYPE_REF = "Fill_In_Type_Ref";
	
	public static final String FIELD_FILL_IN_TYPE = "fill_in_type";
	public static final String FIELD_FILL_IN_TYPE_NAME = "fill_in_type_name";
	public static final String FIELD_CREATED_AT = "created_at"; 
	public static final String FIELD_UPDATED_AT = "updated_at"; 

}
