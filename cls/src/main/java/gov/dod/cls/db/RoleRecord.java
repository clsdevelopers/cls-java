package gov.dod.cls.db;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

public final class RoleRecord {
	
	public interface ROLE_ID
	{
		int SYSTEM_ADMINISTRATOR = 1;
		int AGENCY_ADMINISTRATOR = 2;
		int AGENCY_REVIEW  = 3;
		int REVIEW = 4;
		int STANDARD_USER = 5;
	}
	public final static String TABLE_ROLES = "Roles";
	
	public final static String FIELD_ROLE_ID = "Role_Id";
	public final static String FIELD_ROLE_NAME = "Role_Name";
	public final static String FIELD_CREATED_AT = "created_at";
	public final static String FIELD_UPDATED_AT = "updated_at";

	public final static String ROLE_SYSTEM_ADMINTRATOR = "System Administrator"; // "superuser";
	public final static String ROLE_AGENCY_ADMINISTRATOR = "Agency Administrator"; // "sub_super_user";
	public final static String ROLE_AGENCY_REVIEWER = "Agency Reviewer"; // "agency_reviewer";
	public final static String ROLE_REVIEWER = "Reviewer"; // "reviewer";
	public final static String ROLE_REGULAR_USER = "Standard User"; // "regular_user";
	
	
	
	public final static String SQL_SELECT
		= "SELECT " + FIELD_ROLE_ID
		+ ", " + FIELD_ROLE_NAME
		+ ", " + FIELD_CREATED_AT
		+ ", " + FIELD_UPDATED_AT
		+ " FROM " + TABLE_ROLES
		+ " ORDER BY 1";
	
	public static String getLogicalName(int id) {
		switch (id) {
		case ROLE_ID.SYSTEM_ADMINISTRATOR: return "System Administrator";
		case ROLE_ID.AGENCY_ADMINISTRATOR: return "Agency Administrator";
		case ROLE_ID.AGENCY_REVIEW: return "Agency Reviewer";
		case ROLE_ID.REVIEW: return "Reviewer";
		case ROLE_ID.STANDARD_USER: return "Standard User";
		default: return null;
		}
	}

	// --------------------------------------------------------------
	private Integer id;
	private String name;
	private Date createdAt;
	private Date updatedAt;
	
	public void read(ResultSet rs) throws SQLException {
		this.id = rs.getInt(FIELD_ROLE_ID);
		this.name = rs.getString(FIELD_ROLE_NAME);
		this.createdAt = rs.getTimestamp(FIELD_CREATED_AT);
		this.updatedAt = rs.getTimestamp(FIELD_UPDATED_AT);
	}
	
	public Integer getId() {
		return id;
	}
	public String getName() {
		return name;
	}
	
	/*
	public String getDisplayName() {
		if (this.id != null)
			return getLogicalName(this.id);
		return null;
	}
	*/
	
	public Date getCreatedAt() {
		return createdAt;
	}
	public Date getUpdatedAt() {
		return updatedAt;
	}
}

