package gov.dod.cls.db;

import gov.dod.cls.utils.CommonUtils;
import gov.dod.cls.utils.sql.SqlUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.node.ArrayNode;
import org.codehaus.jackson.node.ObjectNode;

public class DocumentAnswerTable extends ArrayList<DocumentAnswerRecord> {

	private static final long serialVersionUID = -6824442839738339034L;

	public static DocumentAnswerTable loadByDocument(int docId, Connection conn) throws SQLException {
		DocumentAnswerTable documentAnswerTable = new DocumentAnswerTable();
		String sSql = DocumentAnswerRecord.SQL_SELECT
				+ " WHERE " + DocumentAnswerRecord.FIELD_DOCUMENT_ID + " = ?"
				+ " ORDER BY " + DocumentAnswerRecord.FIELD_QUESTION_ID;
		PreparedStatement ps1 = null;
		ResultSet rs1 = null;
		try {
			ps1 = conn.prepareStatement(sSql);
			ps1.setInt(1, docId);
			rs1 = ps1.executeQuery();
			while (rs1.next()) {
				DocumentAnswerRecord documentAnswerRecord = new DocumentAnswerRecord();
				documentAnswerRecord.read(rs1);
				documentAnswerTable.add(documentAnswerRecord);
			}
		}
		finally {
			SqlUtil.closeInstance(rs1, ps1);
		}
		return documentAnswerTable;

	}

	public ArrayNode toJson(ObjectMapper mapper, boolean forInterview) {
		if (mapper == null)
			mapper = new ObjectMapper();
		ArrayNode json = mapper.createArrayNode();
		for (DocumentAnswerRecord record : this) {
			json.add(record.toJson(forInterview));
		}
		return json;
	}
	
	public void fromJson(ArrayNode pArrayNode) {
		for (JsonNode jsonNode : pArrayNode) {
			DocumentAnswerRecord record = new DocumentAnswerRecord();
			record.fromJson(jsonNode);
			this.add(record);
		}
	}
	
	public void populateJsonForApi(ObjectNode json, boolean bForVer1, QuestionTable questionTable) {
		ArrayNode arrayNode = json.arrayNode();
		for (DocumentAnswerRecord record : this) {
			ObjectNode child = arrayNode.objectNode();
			record.populateJsonForApi(child, bForVer1, questionTable);
			arrayNode.add(child);
		}
		json.put(DocumentAnswerRecord.TABLE_DOCUMENT_ANSWERS.toLowerCase(), arrayNode);
	}
	
	public DocumentAnswerRecord findByQuestionId(int questionId) {
		for (DocumentAnswerRecord oRecord : this) {
			if (oRecord.getQuestionId().intValue() == questionId)
				return oRecord;
		}
		return null;
	}
	
	public static PreparedStatement createInertPrepareStatement(Connection conn) throws SQLException {
		String sSql
				= "INSERT INTO " + DocumentAnswerRecord.TABLE_DOCUMENT_ANSWERS
				+ " (" + DocumentAnswerRecord.FIELD_DOCUMENT_ID
				+ ", " + DocumentAnswerRecord.FIELD_QUESTION_ID
				+ ", " + DocumentAnswerRecord.FIELD_QUESTION_ANSWER
					+ ") VALUES (?, ?, ?)";
		return conn.prepareStatement(sSql);
	}
	
	public static PreparedStatement createUpdatePrepareStatement(Connection conn) throws SQLException {
		String sSql = "UPDATE " + DocumentAnswerRecord.TABLE_DOCUMENT_ANSWERS
			+ " SET " + DocumentAnswerRecord.FIELD_QUESTION_ANSWER + " = ?"
			+ " WHERE " + DocumentAnswerRecord.FIELD_DOCUMENT_ANSWER_ID + " = ?";
		return conn.prepareStatement(sSql);
	}
	
	public static PreparedStatement createAnswerDeletePrepareStatement(Connection conn) throws SQLException {
		String sSql = "DELETE FROM " + DocumentAnswerRecord.TABLE_DOCUMENT_ANSWERS
			+ " WHERE " + DocumentAnswerRecord.FIELD_DOCUMENT_ANSWER_ID + " = ?";
		return conn.prepareStatement(sSql);
	}
	
	public static PreparedStatement createUpgradeClsVesionPrepareStatement(Connection conn) throws SQLException {
		String sSql
				= "UPDATE " + DocumentAnswerRecord.TABLE_DOCUMENT_ANSWERS
				+ " SET " + DocumentAnswerRecord.FIELD_QUESTION_ID + " = ?"
				+ " WHERE " + DocumentAnswerRecord.FIELD_DOCUMENT_ANSWER_ID + " = ?";
		return conn.prepareStatement(sSql);
	}
	
	public static PreparedStatement createDeletePrepareStatement(Connection conn) throws SQLException {
		String sSql
				= "DELETE FROM " + DocumentAnswerRecord.TABLE_DOCUMENT_ANSWERS
				+ " WHERE " + DocumentAnswerRecord.FIELD_DOCUMENT_ID + " = ?"
				+ " AND " + DocumentAnswerRecord.FIELD_QUESTION_ID + " = ?";
		return conn.prepareStatement(sSql);
	}
	
	public void populateAwardFromSolicitaton(int awardDocumentId, QuestionTable awardQuestionTable, 
			DocumentAnswerTable solicitationAnswerTable, QuestionTable solicitationQuestionTable,
			Connection conn) throws SQLException {
		PreparedStatement psInsert = null;
		try {
			psInsert = DocumentAnswerTable.createInertPrepareStatement(conn);
			psInsert.setInt(1, awardDocumentId);
			for (DocumentAnswerRecord oSolicitationAnswerRecord : solicitationAnswerTable) {
				Integer solicitationQuestionId = oSolicitationAnswerRecord.getQuestionId();
				QuestionRecord solicitationQuestionRecord = solicitationQuestionTable.findById(solicitationQuestionId);
				if (solicitationQuestionRecord == null)
					continue;
				QuestionRecord awardQuestionRecord = awardQuestionTable.findByCode(solicitationQuestionRecord.getQuestionCode());
				if (awardQuestionRecord == null)
					continue;
				String sAnswer = oSolicitationAnswerRecord.getAnswer();
				if (QuestionRecord.CODE_DOCUMENT_TYPE.equals(awardQuestionRecord.getQuestionCode())) {
					sAnswer = QuestionRecord.DOCUMENT_TYPE_AWARD;
				} else {
					if (awardQuestionRecord.isSimpleSolicitationQuestion())
						continue;
				}
				psInsert.setInt(2, awardQuestionRecord.getId());
				psInsert.setString(3, sAnswer);
				psInsert.execute();
			}
		} finally {
			SqlUtil.closeInstance(psInsert);
		}
	}
	
	public void populateDocFromTemplate(int awardDocumentId, QuestionTable templateQuestionTable, 
			DocumentAnswerTable solicitationAnswerTable, QuestionTable solicitationQuestionTable,
			Connection conn) throws SQLException {
		PreparedStatement psInsert = null;
		try {
			psInsert = DocumentAnswerTable.createInertPrepareStatement(conn);
			psInsert.setInt(1, awardDocumentId);
			for (DocumentAnswerRecord oSolicitationAnswerRecord : solicitationAnswerTable) {
				Integer solicitationQuestionId = oSolicitationAnswerRecord.getQuestionId();
				QuestionRecord solicitationQuestionRecord = solicitationQuestionTable.findById(solicitationQuestionId);
				if (solicitationQuestionRecord == null)
					continue;
				QuestionRecord templateQuestionRecord = templateQuestionTable.findByCode(solicitationQuestionRecord.getQuestionCode());
				if (templateQuestionRecord == null)
					continue;
				String sAnswer = oSolicitationAnswerRecord.getAnswer();

				psInsert.setInt(2, templateQuestionRecord.getId());
				psInsert.setString(3, sAnswer);
				psInsert.execute();
			}
		} finally {
			SqlUtil.closeInstance(psInsert);
		}
	}
	
	public static void upgradeClsVersion(int documentId, QuestionTable newQuestionTable, 
			DocumentAnswerTable origAnswerTable, QuestionTable origQuestionTable,
			ArrayList<String> alRemovedQuestionCodes, Connection conn) throws SQLException {
		PreparedStatement psUpdate = null;
		PreparedStatement psDelete = null;
		try {
			psUpdate = DocumentAnswerTable.createUpgradeClsVesionPrepareStatement(conn);
			psDelete = DocumentAnswerTable.createDeletePrepareStatement(conn);
			psDelete.setInt(1, documentId); // " WHERE " + DocumentAnswerRecord.FIELD_DOCUMENT_ID + " = ?"
			for (DocumentAnswerRecord origAnswerRecord : origAnswerTable) {
				boolean bDelete = true;
				Integer origQuestionId = origAnswerRecord.getQuestionId();
				QuestionRecord origQuestionRecord = origQuestionTable.findById(origQuestionId);
				if (origQuestionRecord != null) {
					QuestionRecord newQuestionRecord = newQuestionTable.findByCode(origQuestionRecord.getQuestionCode());
					if (newQuestionRecord != null) {
						psUpdate.setInt(1, newQuestionRecord.getId()); //" SET " + DocumentAnswerRecord.FIELD_QUESTION_ID + " = ?"
						psUpdate.setInt(2, origAnswerRecord.getId()); // " WHERE " + DocumentAnswerRecord.FIELD_DOCUMENT_ANSWER_ID + " = ?";
						psUpdate.execute();
						bDelete = false;
					} else {
						alRemovedQuestionCodes.add(origQuestionRecord.getQuestionCode());
					}
				}
				if (bDelete) {
					psDelete.setInt(2, origAnswerRecord.getId()); // " AND " + DocumentAnswerRecord.FIELD_QUESTION_ID + " = ?"
					psDelete.execute();
				}
			}
		} finally {
			SqlUtil.closeInstance(psUpdate);
			SqlUtil.closeInstance(psDelete);
		}
	}
	
	public void save(int documentId, DocumentAnswerTable postTable, QuestionTable oQuestionTable,
			Connection conn, AuditEventRecord auditEventRecord)
			throws SQLException {
		PreparedStatement psInsert = null;
		PreparedStatement psUpdate = null;
		PreparedStatement psDelete = null;
		try {
			// String sSql;
			DocumentAnswerTable processed = new DocumentAnswerTable();
			
			for (DocumentAnswerRecord postRecord : postTable) {
				DocumentAnswerRecord oRecord = this.findByQuestionId(postRecord.getQuestionId().intValue());
				if (oRecord == null) {
					if (oQuestionTable.findById(postRecord.getQuestionId()) != null) {
						if (psInsert == null) {
							psInsert = DocumentAnswerTable.createInertPrepareStatement(conn);
						}
						psInsert.setInt(1, documentId);
						psInsert.setInt(2, postRecord.getQuestionId());
						if (postRecord.getAnswer() == null)
							psInsert.setNull(3, java.sql.Types.VARCHAR);
						else
							psInsert.setString(3, postRecord.getAnswer());
						psInsert.execute();
					} else {
						System.out.println("DocumentAnswerTable.save() not QuestionId found error:"
								+ " DocumentId(" + documentId + ")"
								+ " QuestionId(" + postRecord.getQuestionId() + ")"
								+ " Answer(" + postRecord.getAnswer() + ")");
					}
				} else {
					if (CommonUtils.isNotSame(oRecord.getAnswer(), postRecord.getAnswer())) {
						if (psUpdate == null) {
							psUpdate = DocumentAnswerTable.createUpdatePrepareStatement(conn);
							/*
							sSql = "UPDATE " + DocumentAnswerRecord.TABLE_DOCUMENT_ANSWERS
								+ " SET " + DocumentAnswerRecord.FIELD_QUESTION_ANSWER + " = ?"
								+ " WHERE " + DocumentAnswerRecord.FIELD_DOCUMENT_ANSWER_ID + " = ?";
							psUpdate = conn.prepareStatement(sSql);
							*/
						}
						if (postRecord.getAnswer() == null)
							psUpdate.setNull(1, java.sql.Types.VARCHAR);
						else
							psUpdate.setString(1, postRecord.getAnswer());
						psUpdate.setInt(2, oRecord.getId());
						psUpdate.execute();
					}
					processed.add(oRecord);
				}
			}
			
			for (DocumentAnswerRecord oRecord : this) {
				if (!processed.contains(oRecord)) {
					if (psDelete == null) {
						psDelete = DocumentAnswerTable.createAnswerDeletePrepareStatement(conn);
						/*
						sSql = "DELETE FROM " + DocumentAnswerRecord.TABLE_DOCUMENT_ANSWERS
							+ " WHERE " + DocumentAnswerRecord.FIELD_DOCUMENT_ANSWER_ID + " = ?";
						psDelete = conn.prepareStatement(sSql);
						*/
					}
					psDelete.setInt(1, oRecord.getId());
					psDelete.execute();
				}
			}
			
		} finally {
			SqlUtil.closeInstance(psInsert);
			SqlUtil.closeInstance(psUpdate);
			SqlUtil.closeInstance(psDelete);
		}
	}
	
	public static void deleteDocument(int docId, Connection conn) throws SQLException {
		PreparedStatement ps1 = null;
		try {
			String sSql = "DELETE FROM " + DocumentAnswerRecord.TABLE_DOCUMENT_ANSWERS
					+ " WHERE " + DocumentAnswerRecord.FIELD_DOCUMENT_ID + " = ?";
			ps1 = conn.prepareStatement(sSql);
			ps1.setInt(1, docId);
			ps1.execute();
		}
		finally {
			SqlUtil.closeInstance(ps1);
		}
	}
	
	// CJ-versioning
	// When the document is updated to reflect a new version, the answers must be updated also.
	public static void updateDocVersion(DocumentRecord docOrigRecord, DocumentRecord docUpdRecord, Connection conn) throws SQLException {
		PreparedStatement ps1 = null;
		String sSql = ""; 
		try {
			sSql = 
				"INSERT INTO " + DocumentAnswerRecord.TABLE_DOCUMENT_ANSWERS +
					" (" + DocumentAnswerRecord.FIELD_DOCUMENT_ID + ", " + 
						DocumentAnswerRecord.FIELD_QUESTION_ID  + ", " + 
						DocumentAnswerRecord.FIELD_QUESTION_ANSWER  + ", " + 
						DocumentAnswerRecord.FIELD_CREATED_AT  + ", " + 
						DocumentAnswerRecord.FIELD_UPDATED_AT  + ") " + 
				" SELECT ?," +
					" Q2." + QuestionRecord.FIELD_QUESTION_ID + "," +
					" A." + DocumentAnswerRecord.FIELD_QUESTION_ANSWER + "," +
					" A." + DocumentAnswerRecord.FIELD_CREATED_AT + "," +
					" A." + DocumentAnswerRecord.FIELD_UPDATED_AT + 
				" FROM " + QuestionRecord.TABLE_QUESTIONS + " Q1" +
				" INNER JOIN " + QuestionRecord.TABLE_QUESTIONS + " Q2" +
					" ON Q1." + QuestionRecord.FIELD_QUESTION_CODE + " = Q2." + QuestionRecord.FIELD_QUESTION_CODE +
					" AND Q1." + QuestionRecord.FIELD_QUESTION_TYPE + " = Q2." + QuestionRecord.FIELD_QUESTION_TYPE +
					" AND Q1." + QuestionRecord.FIELD_QUESTION_GROUP + " = Q2." + QuestionRecord.FIELD_QUESTION_GROUP +
					" AND Q1." + QuestionRecord.FIELD_CLAUSE_VERSION_ID + " = ?" +
					" AND Q2." + QuestionRecord.FIELD_CLAUSE_VERSION_ID + " = ?" +
				" INNER JOIN " + DocumentAnswerRecord.TABLE_DOCUMENT_ANSWERS + " A" +
					" ON A." + DocumentAnswerRecord.FIELD_QUESTION_ID + " = Q1." + QuestionRecord.FIELD_QUESTION_ID + 
				" WHERE A." + DocumentAnswerRecord.FIELD_DOCUMENT_ID + " = ?" +
				" ORDER BY Q1." + QuestionRecord.FIELD_QUESTION_CODE ;
			
			ps1 = conn.prepareStatement(sSql);
			ps1.setInt(1, docUpdRecord.getId());
			ps1.setInt(2, docOrigRecord.getClauseVersionId());
			ps1.setInt(3, docUpdRecord.getClauseVersionId());
			ps1.setInt(4, docOrigRecord.getId());
			ps1.execute();
		}
		catch (Exception oError) {
			System.out.println("DocumentAnswerTable.updateDocVersion(" + docOrigRecord.getId() + ", " + docUpdRecord.getId() + ") failed due to : " + oError.getMessage() + "\n" + sSql);
			throw oError;
		}
		finally {
			SqlUtil.closeInstance(ps1);
		}
	}
}
