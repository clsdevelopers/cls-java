package gov.dod.cls.db;

import gov.dod.cls.bean.session.UserSession;
import gov.dod.cls.config.ClsConfig;
import gov.dod.cls.utils.CommonUtils;
import gov.dod.cls.utils.Pagination;
import gov.dod.cls.utils.sql.SqlUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.node.ArrayNode;
import org.codehaus.jackson.node.ObjectNode;

public class RegulationTable extends ArrayList<RegulationRecord> {
	
	private static final long serialVersionUID = -3263933849494514126L;

	// ----------------------------------------------------------
	public static final String PREFIX_DO_REGULATION = "regulations-";
	public static final String DO_REGULATION_LIST = PREFIX_DO_REGULATION + "list";

	public static void processRequest(String sDo, UserSession oUserSession, 
			HttpServletRequest req, HttpServletResponse resp) {
		if (!oUserSession.isSuperUser()) { // CJ-680
			return;
		}
		
		switch (sDo) {
		case RegulationTable.DO_REGULATION_LIST:
			RegulationTable.processList(req, resp);
		}
	}
	
	// ----------------------------------------------------------
	public static final String SQL_LIST
		= "SELECT A." + RegulationRecord.FIELD_REGULATION_ID
		+ ", A." + RegulationRecord.FIELD_REGULATION_NAME
		+ ", A." + RegulationRecord.FIELD_REGULATION_TITLE
		+ ", A." + RegulationRecord.FIELD_REGULATION_URL
		//+ ", COUNT(B." + RegulationRecord.FIELD_REGULATION_ID + ") AS ClauseCount"
		//+ ", COUNT(C." + OrgRegulationRecord.FIELD_REGULATION_ID + ") AS AgencyCount"
		+ " FROM " + RegulationRecord.TABLE_REGULATIONS + " A";
		//+ " LEFT OUTER JOIN " + ClauseRecord.TABLE_CLAUSES + " B"
		//+ " ON (B." + ClauseRecord.FIELD_REGULATION_ID + " = A." + RegulationRecord.FIELD_REGULATION_ID + ")"
		//+ " LEFT OUTER JOIN " + OrgRegulationRecord.TABLE_ORG_REGULATIONS + " C"
		//+ " ON (C." + OrgRegulationRecord.FIELD_REGULATION_REGULATION_ID + " = A." + RegulationRecord.FIELD_REGULATION_ID + ")"
		//+ " GROUP BY A." + RegulationRecord.FIELD_REGULATION_ID + ", A." + RegulationRecord.FIELD_REGULATION_NAME
		//+ ", A." + RegulationRecord.FIELD_REGULATION_TITLE + ", A." + RegulationRecord.FIELD_REGULATION_URL
		// + " ORDER BY 2";

	@SuppressWarnings("resource")
	private static void processList(HttpServletRequest req, HttpServletResponse resp) {
		String query = req.getParameter("query");		
		String sort_field = req.getParameter("sort_field");
		String sort_order = req.getParameter("sort_order");
		
		Pagination pagination = new Pagination(req);
		pagination.addDataSource(RegulationRecord.TABLE_REGULATIONS);
		Connection conn = null;
		PreparedStatement ps1 = null;
		ResultSet rs1 = null;
		try {
			conn = ClsConfig.getDbConnection();
			String sSql = "SELECT COUNT(A." + RegulationRecord.FIELD_REGULATION_ID + ") FROM " + RegulationRecord.TABLE_REGULATIONS + " A";
			String operator = " WHERE ";
			if (CommonUtils.isNotEmpty(query)) {
				sSql += operator + "(LOWER(A." + RegulationRecord.FIELD_REGULATION_NAME + ") LIKE ?" 
					+ " OR LOWER(A." + RegulationRecord.FIELD_REGULATION_TITLE + ") LIKE ?"
					+ ")";
				operator = " AND ";
				query = "%" + query.toLowerCase() + "%";
			}			
			
			ps1 = conn.prepareStatement(sSql);
			int iParam = 1;
			if (CommonUtils.isNotEmpty(query)) {
				ps1.setString(iParam++, query);
				ps1.setString(iParam++, query);
			}	
			rs1 = ps1.executeQuery();
			boolean isAcceptableRange = false;
			if (rs1.next())
				isAcceptableRange = pagination.isAcceptedRange(rs1.getInt(1));
			
			if (isAcceptableRange) {
				rs1 = SqlUtil.closeInstance(rs1);
				ps1 = SqlUtil.closeInstance(ps1);
				
				sSql = "SELECT regulation_id, count(*)"
					+ " FROM " + ClauseRecord.TABLE_CLAUSES
					+ " WHERE regulation_id is not null"
					+ " GROUP BY regulation_id";
				ps1 = conn.prepareStatement(sSql);
				rs1 = ps1.executeQuery();
				HashMap<Integer, Integer> oClauseCounts = new HashMap<Integer, Integer>(); 
				while (rs1.next()) {
					oClauseCounts.put(rs1.getInt(1), rs1.getInt(2));
				}
				rs1 = SqlUtil.closeInstance(rs1);
				ps1 = SqlUtil.closeInstance(ps1);
				
				sSql = "SELECT regulation_id, COUNT(DISTINCT org_id)"
					+ " FROM " + OrgRegulationRecord.TABLE_ORG_REGULATIONS
					+ " WHERE regulation_id is not null"
					+ " GROUP BY regulation_id";
				ps1 = conn.prepareStatement(sSql);
				rs1 = ps1.executeQuery();
				HashMap<Integer, Integer> oOrgCounts = new HashMap<Integer, Integer>(); 
				while (rs1.next()) {
					oOrgCounts.put(rs1.getInt(1), rs1.getInt(2));
				}
				rs1 = SqlUtil.closeInstance(rs1);
				rs1 = null;
				ps1 = SqlUtil.closeInstance(ps1);
				ps1 = null;
					
				// sSql = SQL_LIST + pagination.sqlLimit();
				// ps1 = conn.prepareStatement(sSql);
				sSql = SQL_LIST;
				operator = " WHERE ";
				if (CommonUtils.isNotEmpty(query)) {
					sSql += operator + "(LOWER(A." + RegulationRecord.FIELD_REGULATION_NAME + ") LIKE ?" 
							+ " OR LOWER(A." + RegulationRecord.FIELD_REGULATION_TITLE + ") LIKE ?"
							+ ")";
						operator = " AND ";
				}
				String sAsc = ("desc".equals(sort_order) ? " DESC" : "");
				String orderField = RegulationRecord.FIELD_REGULATION_NAME;
				if ("regulation_title".equals(sort_field))
					orderField = RegulationRecord.FIELD_REGULATION_TITLE;
				sSql += " ORDER BY " + orderField + sAsc;

				ps1 = conn.prepareStatement(sSql + pagination.sqlLimit());
				iParam = 1;
				if (CommonUtils.isNotEmpty(query)) {
					ps1.setString(iParam++, query);
					ps1.setString(iParam++, query);
				}
				
				rs1 = ps1.executeQuery();
				while (rs1.next()) {
					ObjectNode json = pagination.getMapper().createObjectNode();
					int id = rs1.getInt(RegulationRecord.FIELD_REGULATION_ID);
					json.put(RegulationRecord.FIELD_REGULATION_ID, id);
					json.put(RegulationRecord.FIELD_REGULATION_NAME, rs1.getString(RegulationRecord.FIELD_REGULATION_NAME));
					json.put(RegulationRecord.FIELD_REGULATION_TITLE, rs1.getString(RegulationRecord.FIELD_REGULATION_TITLE));
					json.put(RegulationRecord.FIELD_REGULATION_URL, rs1.getString(RegulationRecord.FIELD_REGULATION_URL));
					
					Integer iClauseCount = new Integer(0);
					if (oClauseCounts.containsKey(id))
						iClauseCount = oClauseCounts.get(id);
					json.put("ClauseCount", iClauseCount);
					
					Integer iOrgCount = new Integer(0);
					if (oOrgCounts.containsKey(id))
						iOrgCount = oOrgCounts.get(id);
					json.put("AgencyCount", iOrgCount);

					pagination.incCount();
					pagination.getArrayNode().add(json);
				}
			}
			pagination.send(resp);
		}
		catch (Exception oError) {
			oError.printStackTrace();
		}
		finally {
			SqlUtil.closeInstance(rs1, ps1, conn);
		}
	}

	// ========================================================
	private volatile static RegulationTable instance = null;
	private volatile static Date refreshed = null;
	
	public synchronized static RegulationTable getInstance(Connection conn) {
		if (RegulationTable.instance == null) {
			RegulationTable.instance = new RegulationTable();
			RegulationTable.instance.refresh(conn);
		} else {
			RegulationTable.instance.refreshWhenNeeded(conn);
		}
		return RegulationTable.instance;
	}
	
	public static RegulationRecord getRecord(int id) {
		RegulationTable regulationTable = RegulationTable.getInstance(null);
		return regulationTable.findById(id);
	}

	public static RegulationTable getSubset(ArrayList<Integer> aIds) {
		RegulationTable regulationTable = RegulationTable.getInstance(null);
		return regulationTable.subsetByList(aIds);
	}
	
	public static ArrayNode getJson(ObjectMapper mapper, Connection conn, boolean forInterview) {
		RegulationTable regulationTable = RegulationTable.getInstance(conn);
		return regulationTable.toJson(mapper, forInterview);
	}

	// Functions -----------------------------------------------------------------
	public static Date getReferHistoryDate(Connection conn) {
		return SysLastTableChangeAtTable.getLastUpdateDate(conn, RegulationRecord.TABLE_REGULATIONS);
	}
	  
	public static boolean needToRefreh(Connection conn) {
		return SysLastTableChangeAtTable.needToLoad(RegulationTable.refreshed, getReferHistoryDate(conn));
	}
	
	// ---------------------------------------------------------------
	private Exception lastError;

	public void refreshWhenNeeded(Connection conn) {
		if (ClausePrescriptionTable.needToRefreh(conn))
			this.refresh(conn);
	}

	private boolean refresh(Connection conn) {
		boolean result = false;
		String sSql = RegulationRecord.SQL_SELECT;
		boolean bCreateConnection = (conn == null);
		PreparedStatement ps1 = null;
		ResultSet rs1 = null;
		try {
			if (bCreateConnection)
				conn = ClsConfig.getDbConnection();
			ps1 = conn.prepareStatement(sSql);
			rs1 = ps1.executeQuery();
			this.clear();
			while (rs1.next()) {
				RegulationRecord regulationRecord = new RegulationRecord();
				regulationRecord.read(rs1);
				this.add(regulationRecord);
			}
			//PrescriptionTable.getInstance(conn);
			RegulationTable.refreshed = getReferHistoryDate(conn); // CommonUtils.getNow();
			result = true;
			System.out.println("RegulationTable refreshed");
		}
		catch (Exception oError) {
			this.lastError = oError;
			oError.printStackTrace();
		}
		finally {
			SqlUtil.closeInstance(rs1, ps1);
			if (bCreateConnection)
				SqlUtil.closeInstance(conn);
		}
		return result;
	}
	
	public Date getRefreshed() {
		return RegulationTable.refreshed;
	}
	
	public RegulationRecord findById(int id) {
		for (RegulationRecord regulationRecord : this) {
			if (regulationRecord.getId().intValue() == id)
				return regulationRecord;
		}
		return null;
	}
	
	public RegulationTable subsetByList(ArrayList<Integer> aIds) {
		RegulationTable result = new RegulationTable();
		for (Integer id : aIds) {
			RegulationRecord regulationRecord = this.findById(id);
			if (regulationRecord != null)
				result.add(regulationRecord);
		}
		return result;
	}
	
	public ArrayNode toJson(ObjectMapper mapper, boolean forInterview) {
		if (mapper == null)
			mapper = new ObjectMapper();
		ArrayNode json = mapper.createArrayNode();
		for (RegulationRecord regulationRecord : this) {
			json.add(regulationRecord.toJson(forInterview));
		}
		return json;
	}

	public Exception getLastError() {
		return lastError;
	}
	
}
