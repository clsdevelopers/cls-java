package gov.dod.cls.db;

import gov.dod.cls.config.ClsConfig;
import gov.dod.cls.utils.CommonUtils;
import gov.dod.cls.utils.sql.SqlUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class OauthAccessTable {

	public static int countActiveSession(Connection conn, int applicationId) throws SQLException {
		int result = 0;
		boolean bCreateConnection = (conn == null);
		PreparedStatement ps1 = null;
		ResultSet rs1 = null;
		try {
			java.sql.Timestamp timestamp = new java.sql.Timestamp(CommonUtils.getNow().getTime());
			String sSql = "SELECT COUNT(*)"
					+ " FROM " + OauthAccessRecord.TABLE_OAUTH_ACCESS
					+ " WHERE " + OauthAccessRecord.FIELD_APPLICATION_ID + " = ?"
					+ " AND " + OauthAccessRecord.FIELD_EXPIRES_AT + " > ?";
			if (bCreateConnection)
				conn = ClsConfig.getDbConnection();
			ps1 = conn.prepareStatement(sSql);
			ps1.setInt(1, applicationId);
			ps1.setTimestamp(2, timestamp);
			rs1 = ps1.executeQuery();
			if (rs1.next()) {
				result = rs1.getInt(1);
			}
		}
		finally {
			SqlUtil.closeInstance(rs1, ps1);
			if (bCreateConnection)
				SqlUtil.closeInstance(conn);
		}
		return result;
	}
	
	public static void deleteApplication(Connection conn, int applicationId) throws SQLException {
		boolean bCreateConnection = (conn == null);
		PreparedStatement ps1 = null;
		try {
			String sSql = "DELETE FROM " + OauthAccessRecord.TABLE_OAUTH_ACCESS
					+ " WHERE " + OauthAccessRecord.FIELD_APPLICATION_ID + " = ?";
			if (bCreateConnection)
				conn = ClsConfig.getDbConnection();
			ps1 = conn.prepareStatement(sSql);
			ps1.setInt(1, applicationId);
			ps1.execute();
		}
		finally {
			SqlUtil.closeInstance(ps1);
			if (bCreateConnection)
				SqlUtil.closeInstance(conn);
		}
	}
	
}
