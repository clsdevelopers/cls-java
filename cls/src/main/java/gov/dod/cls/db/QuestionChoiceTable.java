package gov.dod.cls.db;

import gov.dod.cls.config.ClsConfig;
import gov.dod.cls.utils.sql.SqlUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.node.ArrayNode;

public class QuestionChoiceTable extends ArrayList<QuestionChoiceRecord> {

	private static final long serialVersionUID = -3398672348605342089L;
	
	private volatile static QuestionChoiceTable instance = null;
	private volatile static Date refreshed = null;
	
	public synchronized static QuestionChoiceTable getInstance(Connection conn) {
		if (QuestionChoiceTable.instance == null) {
			QuestionChoiceTable.instance = new QuestionChoiceTable();
			QuestionChoiceTable.instance.refresh(conn);
		} else {
			QuestionChoiceTable.instance.refreshWhenNeeded(conn);
		}
		return QuestionChoiceTable.instance;
	}
	
	// Functions -----------------------------------------------------------------
	public static Date getReferHistoryDate(Connection conn) {
		return SysLastTableChangeAtTable.getLastUpdateDate(conn, QuestionChoiceRecord.TABLE_QUESTION_CHOICES);
	}
	  
	public static boolean needToRefreh(Connection conn) {
		return SysLastTableChangeAtTable.needToLoad(QuestionChoiceTable.refreshed, getReferHistoryDate(conn));
	}
	
	// ---------------------------------------------------------------
	private Exception lastError;

	public void refreshWhenNeeded(Connection conn) {
		if (ClausePrescriptionTable.needToRefreh(conn))
			this.refresh(conn);
	}

	private boolean refresh(Connection conn) {
		boolean result = false;
		boolean bCreateConnection = (conn == null);
		PreparedStatement ps1 = null;
		ResultSet rs1 = null;
		try {
			if (bCreateConnection)
				conn = ClsConfig.getDbConnection();
			String sSql = QuestionChoiceRecord.SQL_SELECT
					+ " ORDER BY " + QuestionChoiceRecord.FIELD_QUESTION_ID
					+ ", " + QuestionChoiceRecord.FIELD_QUESTION_CHOCE_ID;
			ps1 = conn.prepareStatement(sSql);
			rs1 = ps1.executeQuery();
			this.clear();
			while (rs1.next()) {
				QuestionChoiceRecord record = new QuestionChoiceRecord();
				record.read(rs1);
				
				this.add(record);
			}
			QuestionChoiceTable.refreshed = getReferHistoryDate(conn); // CommonUtils.getNow();
			result = true;
			System.out.println("QuestionChoiceTable refreshed");
		}
		catch (Exception oError) {
			this.lastError = oError;
			oError.printStackTrace();
		}
		finally {
			SqlUtil.closeInstance(rs1, ps1);
			if (bCreateConnection)
				SqlUtil.closeInstance(conn);
		}
		return result;
	}
	
	public QuestionChoiceTable subset(int questionId) {
		QuestionChoiceTable result = new QuestionChoiceTable();
		boolean found = false;
		for (QuestionChoiceRecord record : this) {
			if (record.getQuestionId().intValue() == questionId) {
				result.add(record);
				found = true;
			} else if (found)
				break;
		}
		return result;
	}
	
	public void populate(QuestionChoicePrescriptionTable choicePrescriptionTable) {
		for (QuestionChoiceRecord record : this) {
			record.setQuestionChoicePrescriptionTable( choicePrescriptionTable.subset(record.getId().intValue()));
		}
	}

	public QuestionChoiceRecord findByText(String psText) {
		for (QuestionChoiceRecord record : this) {
			if (record.getChoiceText().equals(psText))
				return record;
		}
		return null;
	}

	public ArrayNode toJson(ObjectMapper mapper, boolean forInterview) {
		if (mapper == null)
			mapper = new ObjectMapper();
		ArrayNode json = mapper.createArrayNode();
		for (QuestionChoiceRecord record : this) {
			json.add(record.toJson(forInterview));
		}
		return json;
	}
	
	public Exception getLastError() {
		return lastError;
	}

}
