package gov.dod.cls.db;

import gov.dod.cls.utils.sql.SqlUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class OrgRegulationTable {

	public static final String SQL_SELECT_REGUL_ID_FOR_ORG
		= "SELECT " + OrgRegulationRecord.FIELD_REGULATION_ID
		+ " FROM " + OrgRegulationRecord.TABLE_ORG_REGULATIONS
		+ " WHERE " + OrgRegulationRecord.FIELD_ORG_ID + " = ?";
	
	public static final String SQL_DELETE_FOR_ORG
		= "DELETE FROM " + OrgRegulationRecord.TABLE_ORG_REGULATIONS
		+ " WHERE " + OrgRegulationRecord.FIELD_ORG_ID + " = ?"
		+ " AND " + OrgRegulationRecord.FIELD_REGULATION_ID + " = ?";
	
	public static final String SQL_INSERT
		= "INSERT INTO " + OrgRegulationRecord.TABLE_ORG_REGULATIONS
		+ " (" + OrgRegulationRecord.FIELD_ORG_ID
		+ ", " + OrgRegulationRecord.FIELD_REGULATION_ID
		+ ", " + OrgRegulationRecord.FIELD_CREATED_AT
		+ ", " + OrgRegulationRecord.FIELD_UPDATED_AT
		+ ") VALUES (?, ?, ?, ?)";
	
	public static boolean adminUpdate(Connection conn, Integer orgId, ArrayList<Integer> newRegulIds, AuditEventRecord auditEventRecord) 
			throws SQLException {
		ArrayList<Integer> currentRegulIds = OrgRegulationTable.getRegulIds(conn, orgId);
		
		if ((newRegulIds.size() == 0) && (currentRegulIds.size() == 0))
			return false;
		
		String added = "";
		String removed = "";
		java.sql.Date dNow = SqlUtil.getNow();
		PreparedStatement psInsert = null;
		PreparedStatement psDelete = null;
		
		try {
			for (Integer newRegul : newRegulIds) {
				if (newRegul == null)
					continue;
				int iIndex = currentRegulIds.indexOf(newRegul);
				if (iIndex >= 0) {
					currentRegulIds.remove(iIndex);
				} else {
					if (psInsert == null)
						psInsert = conn.prepareStatement(SQL_INSERT);
					psInsert.setInt(1, orgId);
					psInsert.setInt(2, newRegul);
					psInsert.setDate(3, dNow);
					psInsert.setDate(4, dNow);
					psInsert.execute();
					if (added.equals(""))
						added = RegulationRecord.getLogicalName(newRegul);
					else
						added += ", " + RegulationRecord.getLogicalName(newRegul);
				}
			}
			for (Integer deleteRegul : currentRegulIds) {
				if (psDelete == null) {
					psDelete = conn.prepareStatement(SQL_DELETE_FOR_ORG);
				}
				psDelete.setInt(1, orgId);
				psDelete.setInt(2, deleteRegul);
				psDelete.execute();
				removed += (removed.equals("") ? "" : ", ") + RegulationRecord.getLogicalName(deleteRegul);
			}
		}
		finally {
			SqlUtil.closeInstance(psInsert);
			SqlUtil.closeInstance(psDelete);
		}
		
		boolean changed = false;
		if (!added.equals("")) {
			auditEventRecord.addAttribute("added_regulations", added);
			changed = true;
		}
		if (!removed.equals("")) {
			auditEventRecord.addAttribute("removed_regulations", removed);
			changed = true;
		}
		return changed;
	}
	
	public static ArrayList<Integer> getRegulIds(Connection conn, Integer orgId)
			throws SQLException {
		ArrayList<Integer> currentRegulIds = new ArrayList<Integer>();
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = conn.prepareStatement(SQL_SELECT_REGUL_ID_FOR_ORG);
			ps.setInt(1, orgId);
			rs = ps.executeQuery();
			while (rs.next()) {
				currentRegulIds.add(rs.getInt(1));
			}
		}
		//catch (Exception oError) {
		//	oError.printStackTrace();
		//}
		finally {
			SqlUtil.closeInstance(rs, ps);
		}
		
		return currentRegulIds;
	}
	
	public static void adminRemoveUser(Connection conn, Integer orgId) throws SQLException {
		PreparedStatement ps = null;
		try {
			String sSql = "DELETE FROM " + OrgRegulationRecord.TABLE_ORG_REGULATIONS
					+ " WHERE " + OrgRegulationRecord.FIELD_ORG_ID + " = ?";
			ps = conn.prepareStatement(sSql);
			ps.setInt(1, orgId);
			ps.execute();
		}
		finally {
			SqlUtil.closeInstance(ps);
		}
	}
		
}
