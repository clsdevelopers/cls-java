package gov.dod.cls.db;

import gov.dod.cls.utils.sql.SqlUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class UserRoleTable {
	
	public static final String SQL_SELECT_ROLE_ID_FOR_USER
		= "SELECT " + UserRoleRecord.FIELD_ROLE_ID
		+ " FROM " + UserRoleRecord.TABLE_USER_ROLES
		+ " WHERE " + UserRoleRecord.FIELD_USER_ID + " = ?";

	public static final String SQL_DELETE_FOR_USER
		= "DELETE FROM " + UserRoleRecord.TABLE_USER_ROLES
		+ " WHERE " + UserRoleRecord.FIELD_USER_ID + " = ?"
		+ " AND " + UserRoleRecord.FIELD_ROLE_ID + " = ?";

	public static final String SQL_INSERT
		= "INSERT INTO " + UserRoleRecord.TABLE_USER_ROLES
		+ " (" + UserRoleRecord.FIELD_USER_ID
		+ ", " + UserRoleRecord.FIELD_ROLE_ID
		+ ", " + UserRoleRecord.FIELD_CREATED_AT
		+ ", " + UserRoleRecord.FIELD_UPDATED_AT
		+ ") VALUES (?, ?, ?, ?)";

	public static boolean adminUpdate(Connection conn, Integer userId, ArrayList<Integer> newRoleIds, AuditEventRecord auditEventRecord) 
			throws SQLException {
		ArrayList<Integer> currentRoleIds = UserRoleTable.getRoleIds(conn, userId);
		
		if ((newRoleIds.size() == 0) && (currentRoleIds.size() == 0))
			return false;
		
		String added = "";
		String removed = "";
		java.sql.Date dNow = SqlUtil.getNow();
		PreparedStatement psInsert = null;
		PreparedStatement psDelete = null;
		
		try {
			for (Integer newRole : newRoleIds) {
				if (newRole == null)
					continue;
				int iIndex = currentRoleIds.indexOf(newRole);
				if (iIndex >= 0) {
					currentRoleIds.remove(iIndex);
				} else {
					if (psInsert == null)
						psInsert = conn.prepareStatement(SQL_INSERT);
					psInsert.setInt(1, userId);
					psInsert.setInt(2, newRole);
					psInsert.setDate(3, dNow);
					psInsert.setDate(4, dNow);
					psInsert.execute();
					if (added.equals(""))
						added = RoleRecord.getLogicalName(newRole);
					else
						added += ", " + RoleRecord.getLogicalName(newRole);
				}
			}
			for (Integer deleteRole : currentRoleIds) {
				if (psDelete == null) {
					psDelete = conn.prepareStatement(SQL_DELETE_FOR_USER);
				}
				psDelete.setInt(1, userId);
				psDelete.setInt(2, deleteRole);
				psDelete.execute();
				removed += (removed.equals("") ? "" : ", ") + RoleRecord.getLogicalName(deleteRole);
			}
		}
		finally {
			SqlUtil.closeInstance(psInsert);
			SqlUtil.closeInstance(psDelete);
		}
		
		boolean changed = false;
		if (!added.equals("")) {
			auditEventRecord.addAttribute("added_roles", added);
			changed = true;
		}
		if (!removed.equals("")) {
			auditEventRecord.addAttribute("removed_roles", removed);
			changed = true;
		}
		return changed;
	}

	public static ArrayList<Integer> getRoleIds(Connection conn, Integer userId)
			throws SQLException {
		ArrayList<Integer> currentRoleIds = new ArrayList<Integer>();
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = conn.prepareStatement(SQL_SELECT_ROLE_ID_FOR_USER);
			ps.setInt(1, userId);
			rs = ps.executeQuery();
			while (rs.next()) {
				currentRoleIds.add(rs.getInt(1));
			}
		}
		//catch (Exception oError) {
		//	oError.printStackTrace();
		//}
		finally {
			SqlUtil.closeInstance(rs, ps);
		}
		
		return currentRoleIds;
	}
	
	public static void adminRemoveUser(Connection conn, Integer userId) throws SQLException {
		PreparedStatement ps = null;
		try {
			String sSql = "DELETE FROM " + UserRoleRecord.TABLE_USER_ROLES
					+ " WHERE " + UserRoleRecord.FIELD_USER_ID + " = ?";
			ps = conn.prepareStatement(sSql);
			ps.setInt(1, userId);
			ps.execute();
		}
		finally {
			SqlUtil.closeInstance(ps);
		}
	}

}
