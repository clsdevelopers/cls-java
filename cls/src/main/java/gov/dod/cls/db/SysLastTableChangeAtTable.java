package gov.dod.cls.db;

import gov.dod.cls.config.ClsConfig;
import gov.dod.cls.utils.CommonUtils;
import gov.dod.cls.utils.sql.SqlUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

public class SysLastTableChangeAtTable extends ArrayList<SysLastTableChangeAtRecord> {

	private static final long serialVersionUID = -4601372837997000503L;

	// Static --------------------------------------------------------------------
	private static SysLastTableChangeAtTable instance = null;
	private static long INTERVAL = 1000 * 30; // 30 seconds
	
	private static final String SQL_LAST_UPDATES
		= "SELECT '" + ClauseVersionRecord.TABLE_CLAUSE_VERSIONS.toUpperCase(Locale.ENGLISH) + "' table_name, MAX(Updated_At) Last_Change_At FROM " + ClauseVersionRecord.TABLE_CLAUSE_VERSIONS + "\n"
		+ "UNION\n"
		+ "SELECT '" + QuestionRecord.TABLE_QUESTIONS.toUpperCase(Locale.ENGLISH) + "' table_name, MAX(Updated_At) Last_Change_At FROM " + QuestionRecord.TABLE_QUESTIONS + "\n"
		+ "UNION\n"
		+ "SELECT '" + QuestionConditionRecord.TABLE_QUESTION_CONDITIONS.toUpperCase(Locale.ENGLISH) + "' table_name, MAX(Updated_At) Last_Change_At FROM " + QuestionConditionRecord.TABLE_QUESTION_CONDITIONS + "\n"
		+ "UNION\n"
		+ "SELECT '" + QuestionChoiceRecord.TABLE_QUESTION_CHOICES.toUpperCase(Locale.ENGLISH) + "' table_name, MAX(Updated_At) Last_Change_At FROM " + QuestionChoiceRecord.TABLE_QUESTION_CHOICES + "\n"
		+ "UNION\n"
		+ "SELECT '" + QuestionChoicePrescriptionRecord.TABLE_QUESTION_CHOICE_PRESCRIPTIONS.toUpperCase(Locale.ENGLISH) + "' table_name, MAX(Updated_At) Last_Change_At FROM " + QuestionChoicePrescriptionRecord.TABLE_QUESTION_CHOICE_PRESCRIPTIONS + "\n"
		+ "UNION\n"
		+ "SELECT '" + QuestionGroupRefRecord.TABLE_QUESTION_GROUP_REF.toUpperCase(Locale.ENGLISH) + "' table_name, MAX(Updated_At) Last_Change_At FROM " + QuestionGroupRefRecord.TABLE_QUESTION_GROUP_REF + "\n"
		+ "UNION\n"
		+ "SELECT '" + QuestionTypeRefRecord.TABLE_QUESTION_TYPE_REF.toUpperCase(Locale.ENGLISH) + "' table_name, MAX(Updated_At) Last_Change_At FROM " + QuestionTypeRefRecord.TABLE_QUESTION_TYPE_REF + "\n"
		+ "UNION\n"
		+ "SELECT '" + PrescriptionRecord.TABLE_PRESCRIPTIONS.toUpperCase(Locale.ENGLISH) + "' table_name, MAX(Updated_At) Last_Change_At FROM " + PrescriptionRecord.TABLE_PRESCRIPTIONS + "\n"
		+ "UNION\n"
		+ "SELECT '" + RegulationRecord.TABLE_REGULATIONS.toUpperCase(Locale.ENGLISH) + "' table_name, MAX(Updated_At) Last_Change_At FROM " + RegulationRecord.TABLE_REGULATIONS + "\n"
		+ "UNION\n"
		+ "SELECT '" + ClauseSectionRecord.TABLE_CLAUSE_SECTIONS.toUpperCase(Locale.ENGLISH) + "' table_name, MAX(Updated_At) Last_Change_At FROM " + ClauseSectionRecord.TABLE_CLAUSE_SECTIONS + "\n"
		+ "UNION\n"
		+ "SELECT '" + ClausePrescriptionRecord.TABLE_CLAUSE_PRESCRIPTIONS.toUpperCase(Locale.ENGLISH) + "' table_name, MAX(Updated_At) Last_Change_At FROM " + ClausePrescriptionRecord.TABLE_CLAUSE_PRESCRIPTIONS + "\n"
		+ "UNION\n"
		+ "SELECT '" + ClauseFillInRecord.TABLE_CLAUSE_FILL_INS.toUpperCase(Locale.ENGLISH) + "' table_name, MAX(Updated_At) Last_Change_At FROM " + ClauseFillInRecord.TABLE_CLAUSE_FILL_INS + "\n"
		+ "UNION\n"
		+ "SELECT '" + ClauseRecord.TABLE_CLAUSES.toUpperCase(Locale.ENGLISH) + "' table_name, MAX(Updated_At) Last_Change_At FROM " + ClauseRecord.TABLE_CLAUSES + "";

	public static synchronized SysLastTableChangeAtTable getSysLastTableChangeAtTable(Connection conn) {
		if (SysLastTableChangeAtTable.instance == null) {
		  SysLastTableChangeAtTable.instance = new SysLastTableChangeAtTable();
		  if (conn != null)
		    SysLastTableChangeAtTable.instance.load(conn);
		} else {
		  SysLastTableChangeAtTable.instance.refreshWhenNeeded(conn);
		}
		return SysLastTableChangeAtTable.instance;
	}

	public static Date getLastUpdateDate(Connection conn, String psTableName) {
	  SysLastTableChangeAtTable oSysLastTableChangeAtTable =
			SysLastTableChangeAtTable.getSysLastTableChangeAtTable(conn);
		return oSysLastTableChangeAtTable.getLastUpdateDate(psTableName);
	}
	
	public static boolean needToLoad(Date pOldDate, Date pNewDate) {
		if (pOldDate == null) {
			return (pNewDate != null);
		} else {
			if (pNewDate == null)
				return false;
			else
				return pNewDate.after(pOldDate);
		}
	}

	// Private Properties --------------------------------------------------------
	private Date _LastLoaded = null;
	private Date _LastChecked = null;
	
	// Constructor ---------------------------------------------------------------
	public SysLastTableChangeAtTable() {
		super();
	}
	
	public void refreshWhenNeeded(Connection conn) {
		if (this._LastLoaded == null) {
			this.load(conn);
		} else {
			Date dNow = CommonUtils.getNow();
			long diff = dNow.getTime() - this._LastLoaded.getTime();
			if (diff > SysLastTableChangeAtTable.INTERVAL) {
				this.load(conn);
			} else
				this._LastChecked = CommonUtils.getNow();
		}
	}
	
	public void load(Connection conn) {
		boolean bCreateConnection = (conn == null);
		PreparedStatement ps1 = null;
		ResultSet rs1 = null;
		try {
			if (bCreateConnection)
				conn = ClsConfig.getDbConnection();
			ps1 = conn.prepareStatement(SysLastTableChangeAtTable.SQL_LAST_UPDATES); // SysLastTableChangeAtRecord.SQL_SELECT
			rs1 = ps1.executeQuery();
			this.clear();
			while (rs1.next()) {
				SysLastTableChangeAtRecord oRecord = new SysLastTableChangeAtRecord();
				oRecord.read(rs1);
				this.add(oRecord);
			}
			this._LastLoaded = CommonUtils.getNow();
			this._LastChecked = CommonUtils.getNow();
		}
    	catch (Exception oIgnore) {
    		oIgnore.printStackTrace();
    	}
		finally {
			SqlUtil.closeInstance(rs1, ps1);
			if (bCreateConnection)
				SqlUtil.closeInstance(conn);
		}
	}
	
	public Date getLastUpdateDate(String psTableName) {
		if (psTableName == null)
			return null;
		psTableName = psTableName.toUpperCase(Locale.ENGLISH);
		for (SysLastTableChangeAtRecord oRecord : this) {
			if (oRecord.isSame(psTableName))
				return oRecord.getDate();
		}
		return null;
	}

}
