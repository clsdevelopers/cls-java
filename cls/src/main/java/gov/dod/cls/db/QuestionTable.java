package gov.dod.cls.db;

import gov.dod.cls.PageApi;
import gov.dod.cls.bean.session.UserSession;
import gov.dod.cls.config.ClsConfig;
import gov.dod.cls.model.JsonAble;
import gov.dod.cls.utils.CommonUtils;
import gov.dod.cls.utils.Pagination;
import gov.dod.cls.utils.sql.SqlUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.node.ArrayNode;
import org.codehaus.jackson.node.ObjectNode;

public class QuestionTable extends ArrayList<QuestionRecord> {
	
	private static final long serialVersionUID = -8571942314563941569L;

	// ----------------------------------------------------------
	public static final String PREFIX_DO_QUESTION = "question-";
	
	public static final String DO_QUESTION_LIST = PREFIX_DO_QUESTION + "list";
	public static final String DO_QUESTION_DETAILS_GET = PREFIX_DO_QUESTION + "detail-get";

	public static void processRequest(String sDo, UserSession oUserSession, 
			HttpServletRequest req, HttpServletResponse resp) {
		if (!oUserSession.isSuperUser()) { // CJ-680
			return;
		}
		
		switch (sDo) {
		case QuestionTable.DO_QUESTION_LIST:
			QuestionTable.processList(req, resp);
			return;
		case QuestionTable.DO_QUESTION_DETAILS_GET:
			QuestionTable.getQuestionDetails(sDo, oUserSession, req, resp);
			return;			
		}
	}
	
	// ----------------------------------------------------------
	private static final String SQL_LIST
		= "SELECT A." + QuestionRecord.FIELD_QUESTION_ID 
		+ ", A." + QuestionRecord.FIELD_QUESTION_CODE
		+ ", A." + QuestionRecord.FIELD_QUESTION_TYPE 
		+ ", A." + QuestionRecord.FIELD_QUESTION_TEXT
		+ ", D." + QuestionGroupRefRecord.FIELD_QUESTION_GROUP_NAME
		+ ", C." + QuestionConditionRecord.FIELD_QUESTION_CONDITION_SEQUENCE 
		+ ", E." + ClauseVersionRecord.FIELD_CLAUSE_VERSION_NAME
		+ " FROM " + QuestionRecord.TABLE_QUESTIONS + " A" 
		+ " INNER JOIN " + QuestionConditionRecord.TABLE_QUESTION_CONDITIONS + " C"  
		+ " ON (C." + QuestionConditionRecord.FIELD_QUESTION_ID + " = A." + QuestionRecord.FIELD_QUESTION_ID + ")"		
		+ " LEFT OUTER JOIN " + QuestionGroupRefRecord.TABLE_QUESTION_GROUP_REF + " D"  
		+ " ON (D." + QuestionGroupRefRecord.FIELD_QUESTION_GROUP + " = A." + QuestionRecord.FIELD_QUESTION_GROUP + ")"
		+ " JOIN " + ClauseVersionRecord.TABLE_CLAUSE_VERSIONS + " E"
		+ " ON (E." + ClauseVersionRecord.FIELD_CLAUSE_VERSION_ID + " = A." + QuestionRecord.FIELD_CLAUSE_VERSION_ID + ")"
		;
		// + " FROM " + QuestionRecord.TABLE_QUESTIONS + " A";

	private static void processList(HttpServletRequest req, HttpServletResponse resp) {
		String query = req.getParameter("query");
		String question_type = req.getParameter("question_type");
		String clsVersion = req.getParameter("cls_version");
		String sort_field = req.getParameter("sort_field");
		String sort_order = req.getParameter("sort_order");

		Pagination pagination = new Pagination(req);
		pagination.addDataSource(QuestionRecord.TABLE_QUESTIONS);
		Connection conn = null;
		PreparedStatement ps1 = null;
		ResultSet rs1 = null;
		PreparedStatement ps2 = null;
		ResultSet rs2 = null;
		try {
			conn = ClsConfig.getDbConnection();
			String sSql = "SELECT COUNT(A." + QuestionRecord.FIELD_QUESTION_ID + ")"
					+ " FROM " + QuestionRecord.TABLE_QUESTIONS + " A";
			if (CommonUtils.isNotEmpty(clsVersion)) {
				sSql += " JOIN " + ClauseVersionRecord.TABLE_CLAUSE_VERSIONS + " E"
						+ " ON (E." + ClauseVersionRecord.FIELD_CLAUSE_VERSION_ID + " = A." + QuestionRecord.FIELD_CLAUSE_VERSION_ID + ")";
			}
			String operator = " WHERE ";
			if (CommonUtils.isNotEmpty(query)) {
				sSql += operator + "(LOWER(A." + QuestionRecord.FIELD_QUESTION_CODE+ ") LIKE ?" 
					+ " OR LOWER(A." + QuestionRecord.FIELD_QUESTION_TEXT + ") LIKE ?"
					+ " OR EXISTS (SELECT B." + QuestionChoiceRecord.FIELD_QUESTION_CHOCE_ID
					+ " FROM " + QuestionChoiceRecord.TABLE_QUESTION_CHOICES + " B"
					+ " WHERE B." + QuestionChoiceRecord.FIELD_QUESTION_ID + " = A." + QuestionRecord.FIELD_QUESTION_ID
					+ " AND LOWER(B." + QuestionChoiceRecord.FIELD_CHOICE_TEXT + ") = ?)"
					+ ")";
				operator = " AND ";
				query = "%" + query.toLowerCase() + "%";
			}
			if (CommonUtils.isNotEmpty(question_type)) {
				sSql += operator + "A." + QuestionRecord.FIELD_QUESTION_TYPE + " = ?";
				operator = " AND ";
			}
			if (CommonUtils.isNotEmpty(clsVersion)) {
				sSql += operator + "E." + ClauseVersionRecord.FIELD_CLAUSE_VERSION_ID + " = ?";
				operator = " AND ";
			}
			
			ps1 = conn.prepareStatement(sSql);
			int iParam = 1;
			if (CommonUtils.isNotEmpty(query)) {
				ps1.setString(iParam++, query);
				ps1.setString(iParam++, query);
				ps1.setString(iParam++, query);
			}
			if (CommonUtils.isNotEmpty(question_type)) {
				ps1.setString(iParam++, question_type);
			}
			if (CommonUtils.isNotEmpty(clsVersion)) {
				ps1.setString(iParam++, clsVersion);
			}
			rs1 = ps1.executeQuery();
			boolean isAcceptableRange = false;
			if (rs1.next())
				isAcceptableRange = pagination.isAcceptedRange(rs1.getInt(1));
			
			if (isAcceptableRange) {
				SqlUtil.closeInstance(rs1);
				rs1 = null;
				SqlUtil.closeInstance(ps1);
				ps1 = null;
				
				sSql = "SELECT " + QuestionChoiceRecord.FIELD_CHOICE_TEXT
					+ " FROM " + QuestionChoiceRecord.TABLE_QUESTION_CHOICES
					+ " WHERE " + QuestionChoiceRecord.FIELD_QUESTION_ID + " = ?"
					+ " ORDER BY " + QuestionChoiceRecord.FIELD_QUESTION_CHOCE_ID;
				ps2 = conn.prepareStatement(sSql);
				
				sSql = SQL_LIST;
				operator = " WHERE ";
				if (CommonUtils.isNotEmpty(query)) {
					sSql += operator + "(LOWER(A." + QuestionRecord.FIELD_QUESTION_CODE+ ") LIKE ?" 
						+ " OR LOWER(A." + QuestionRecord.FIELD_QUESTION_TEXT + ") LIKE ?"
						+ " OR EXISTS (SELECT B." + QuestionChoiceRecord.FIELD_QUESTION_CHOCE_ID
						+ " FROM " + QuestionChoiceRecord.TABLE_QUESTION_CHOICES + " B"
						+ " WHERE B." + QuestionChoiceRecord.FIELD_QUESTION_ID + " = A." + QuestionRecord.FIELD_QUESTION_ID
						+ " AND LOWER(B." + QuestionChoiceRecord.FIELD_CHOICE_TEXT + ") = ?)"
						+ ")";
					operator = " AND ";
				}
				if (CommonUtils.isNotEmpty(question_type)) {
					sSql += operator + "A." + QuestionRecord.FIELD_QUESTION_TYPE + " = ?";
					operator = " AND ";
				}
				if (CommonUtils.isNotEmpty(clsVersion)) {
					sSql += operator + "E." + ClauseVersionRecord.FIELD_CLAUSE_VERSION_ID + " = ?";
					operator = " AND ";
				}
				
				String sAsc = ("desc".equals(sort_order) ? " DESC" : "");
				// String orderField = QuestionRecord.FIELD_QUESTION_ID;
				String orderField = QuestionConditionRecord.FIELD_QUESTION_CONDITION_SEQUENCE;
				if ("question_type".equals(sort_field))
					orderField = QuestionRecord.FIELD_QUESTION_TYPE;
				else if ("question_name".equals(sort_field))
					orderField = QuestionRecord.FIELD_QUESTION_CODE;
				sSql += " ORDER BY " + orderField + sAsc;

				ps1 = conn.prepareStatement(sSql + pagination.sqlLimit());
				iParam = 1;
				if (CommonUtils.isNotEmpty(query)) {
					ps1.setString(iParam++, query);
					ps1.setString(iParam++, query);
					ps1.setString(iParam++, query);
				}
				if (CommonUtils.isNotEmpty(question_type)) {
					ps1.setString(iParam++, question_type);
				}
				if (CommonUtils.isNotEmpty(clsVersion)) {
					ps1.setString(iParam++, clsVersion);
				}
				
				rs1 = ps1.executeQuery();
				while (rs1.next()) {
					ObjectNode json = pagination.getMapper().createObjectNode();
					Integer id = rs1.getInt(QuestionRecord.FIELD_QUESTION_ID);
					json.put(QuestionRecord.FIELD_QUESTION_ID , id);
					json.put(QuestionConditionRecord.FIELD_QUESTION_CONDITION_SEQUENCE, rs1.getInt(QuestionConditionRecord.FIELD_QUESTION_CONDITION_SEQUENCE));
					json.put(QuestionRecord.FIELD_QUESTION_CODE, rs1.getString(QuestionRecord.FIELD_QUESTION_CODE));
					json.put(QuestionRecord.FIELD_QUESTION_TYPE, rs1.getString(QuestionRecord.FIELD_QUESTION_TYPE));
					json.put(QuestionRecord.FIELD_QUESTION_TEXT, rs1.getString(QuestionRecord.FIELD_QUESTION_TEXT));
		            json.put(ClauseVersionRecord.FIELD_CLAUSE_VERSION_NAME, rs1.getString(ClauseVersionRecord.FIELD_CLAUSE_VERSION_NAME));
		            json.put(QuestionGroupRefRecord.FIELD_QUESTION_GROUP_NAME , rs1.getString(QuestionGroupRefRecord.FIELD_QUESTION_GROUP_NAME ));
					
					ArrayNode arrayNode = json.arrayNode();
					ps2.setInt(1, id);
					rs2 = ps2.executeQuery();
					while (rs2.next()) {
						arrayNode.add(rs2.getString(QuestionChoiceRecord.FIELD_CHOICE_TEXT));
					}
					rs2.close();
					rs2 = null;
					json.put(QuestionChoiceRecord.TABLE_QUESTION_CHOICES, arrayNode);
						
					pagination.incCount();
					pagination.getArrayNode().add(json);
				}
			}
			pagination.send(resp);
		}
		catch (Exception oError) {
			oError.printStackTrace();
		}
		finally {
			SqlUtil.closeInstance(rs2, ps2);
			SqlUtil.closeInstance(rs1, ps1, conn);
		}
	}
	
	private static void getQuestionDetails(String sDo, UserSession oUserSession, 
			HttpServletRequest req, HttpServletResponse resp) {
		Integer id = CommonUtils.toInteger(req.getParameter("id"));
		String errorMessage = null;
		QuestionRecord questionRecord = null;
		if (id != null) {
			try {
				questionRecord = QuestionRecord.getRecord(id);
				if (questionRecord == null)
					errorMessage = "The requested record is not found.";
			} catch (Exception oError) {
				errorMessage = "Unable to retrieve record. Error: " + oError.getMessage();
			}
		} else
			errorMessage = "Unauthorized request";
		String response;
		if (errorMessage == null) {
			ObjectNode objectNode = (ObjectNode)questionRecord.toJson();
			response = JsonAble.jsonResult(JsonAble.STATUS_SUCCESS, objectNode, null);
		} else
			response = JsonAble.jsonResult(JsonAble.STATUS_FAIL, null, errorMessage);
		PageApi.send(resp, JsonAble.JSON_RESPONSE_FORMAT, response);
	}	

	// ========================================================
	private volatile static QuestionTable instance = null;
	private volatile static Date refreshed = null;
	
	public synchronized static QuestionTable getInstance(Connection conn) {
		if (QuestionTable.instance == null) {
			QuestionTable.instance = new QuestionTable();
			QuestionTable.instance.refresh(conn);
		} else {
			QuestionTable.instance.refreshWhenNeeded(conn);
		}
		return QuestionTable.instance;
	}
	
	public static QuestionTable getSubsetByClauseVersion(int clauseVersionId, Connection conn) {
		return QuestionTable.getSubsetByClauseVersion(clauseVersionId, false, conn);
		/*
		QuestionTable questionTable = QuestionTable.getInstance(conn);
		return questionTable.subsetByClauseVersion(clauseVersionId);
		*/
	}
	
	public static QuestionTable getSubsetByClauseVersion(int clauseVersionId, boolean bIsLinkedAward, Connection conn) {
		QuestionTable questionTable = QuestionTable.getInstance(conn);
		return questionTable.subsetByClauseVersion(clauseVersionId, bIsLinkedAward);
	}
	
	public static ArrayNode getJson(Integer clauseVersionId, Connection conn, ObjectMapper mapper, boolean forInterview) {
		if (clauseVersionId == null) {
			clauseVersionId = ClauseVersionTable.getActiveVersionId();
			if (clauseVersionId == null) {
				QuestionTable emptyQuestions = new QuestionTable();
				return emptyQuestions.toJson(mapper, forInterview);
			}
		}
		QuestionTable questionTable = QuestionTable.getSubsetByClauseVersion(clauseVersionId, conn);
		return questionTable.toJson(mapper, forInterview);
	}

	// Functions -----------------------------------------------------------------
	public static Date getReferHistoryDate(Connection conn) {
		return SysLastTableChangeAtTable.getLastUpdateDate(conn, QuestionRecord.TABLE_QUESTIONS);
	}
	  
	public static boolean needToRefreh(Connection conn) {
		return SysLastTableChangeAtTable.needToLoad(QuestionTable.refreshed, getReferHistoryDate(conn))
				|| QuestionChoiceTable.needToRefreh(conn)
				|| ClausePrescriptionTable.needToRefreh(conn);
	}
	
	// ========================================================
	private Exception lastError = null;

	public void refreshWhenNeeded(Connection conn) {
		if (QuestionTable.needToRefreh(conn))
			this.refresh(conn);
	}

	private boolean refresh(Connection conn) {
		boolean result = false;
		String sSql = QuestionRecord.SQL_SELECT
				+ " JOIN " + QuestionConditionRecord.TABLE_QUESTION_CONDITIONS + " E ON (E." + QuestionConditionRecord.FIELD_QUESTION_ID + " = A." + QuestionRecord.FIELD_QUESTION_ID + ")"
				+ " WHERE A." + QuestionRecord.FIELD_IS_ACTIVE + " = 1"
				+ " ORDER BY A." + QuestionRecord.FIELD_CLAUSE_VERSION_ID + ", E." + QuestionConditionRecord.FIELD_QUESTION_CONDITION_SEQUENCE;
		/*
		String sSql = QuestionRecord.SQL_SELECT
				+ " WHERE A." + QuestionRecord.FIELD_IS_ACTIVE + " = 1"
				+ " ORDER BY A." + QuestionRecord.FIELD_CLAUSE_VERSION_ID + ", A." + QuestionRecord.FIELD_QUESTION_CODE;
		*/
		boolean bCreateConnection = (conn == null); 
		PreparedStatement ps1 = null;
		ResultSet rs1 = null;
		PreparedStatement ps2 = null;
		ResultSet rs2 = null;
		try {
			if (bCreateConnection)
				conn = ClsConfig.getDbConnection();
			QuestionChoiceTable questionChoiceTable = QuestionChoiceTable.getInstance(conn);
			QuestionChoicePrescriptionTable choicePrescriptionTable = QuestionChoicePrescriptionTable.getInstance(conn);
			//questionChoiceTable.load(conn);
			//choicePrescriptionTable.load(conn);
			ps1 = conn.prepareStatement(sSql);
			rs1 = ps1.executeQuery();
			this.clear();
			while (rs1.next()) {
				QuestionRecord questionRecord = new QuestionRecord();
				questionRecord.read(rs1);
				QuestionChoiceTable questionChoices = questionChoiceTable.subset(questionRecord.getId().intValue());
				questionChoices.populate(choicePrescriptionTable);
				questionRecord.setQuestionChoiceTable(questionChoices);
				this.add(questionRecord);
			}
			QuestionTable.refreshed = getReferHistoryDate(conn); // CommonUtils.getNow();
			result = true;
			System.out.println("QuestionTable refreshed");
		}
		catch (Exception oError) {
			this.lastError = oError;
			oError.printStackTrace();
		}
		finally {
			SqlUtil.closeInstance(rs1, ps1);
			SqlUtil.closeInstance(rs2, ps2);
			if (bCreateConnection)
				SqlUtil.closeInstance(conn);
		}
		return result;
	}
	
	public QuestionTable subsetByClauseVersion(int clauseVersionId) {
		return subsetByClauseVersion(clauseVersionId, false);
		/*
		QuestionTable questionTable = new QuestionTable();
		for (QuestionRecord questionRecord : this) {
			if (questionRecord.getClauseVersionId().intValue() == clauseVersionId)
				questionTable.add(questionRecord);
		}
		return questionTable;
		*/
	}
	
	public QuestionTable subsetByClauseVersion(int clauseVersionId, boolean bIsLinkedAward) {
		QuestionTable questionTable = new QuestionTable();
		for (QuestionRecord questionRecord : this) {
			if (questionRecord.getClauseVersionId().intValue() == clauseVersionId) {
				if (!(bIsLinkedAward && questionRecord.isSimpleSolicitationQuestion()))
					questionTable.add(questionRecord);
			}
		}
		return questionTable;
	}

	public QuestionRecord findById(int questionId) {
		for (QuestionRecord questionRecord : this) {
			if (questionRecord.getId().intValue() == questionId)
				return questionRecord;
		}
		return null;
	}

	public QuestionRecord findByCode(String questionCode) {
		for (QuestionRecord questionRecord : this) {
			if (questionRecord.getQuestionCode().equals(questionCode))
				return questionRecord;
		}
		return null;
	}

	public ArrayNode toJson(ObjectMapper mapper, boolean forInterview) {
		if (mapper == null)
			mapper = new ObjectMapper();
		ArrayNode json = mapper.createArrayNode();
		for (QuestionRecord questionRecord : this) {
			if (questionRecord.isActive())
				json.add(questionRecord.toJson(forInterview));
		}
		return json;
	}

	public ArrayNode toJsonForVersionApi(ObjectMapper mapper, QuestionGroupRefTable questionGroupRefTable, QuestionConditionTable questionConditionTable) {
		if (mapper == null)
			mapper = new ObjectMapper();
		ArrayNode json = mapper.createArrayNode();
		for (QuestionRecord questionRecord : this) {
			QuestionConditionRecord questionConditionRecord = questionConditionTable.findByQuestion(questionRecord);
			if (questionRecord.isActive() && (questionConditionRecord != null) &&
					questionConditionRecord.isBaseQuestion()) // CJ-581
				json.add(questionRecord.toJsonForVersionApi(mapper, questionGroupRefTable, questionConditionRecord));
		}
		return json;
	}

	public Date getRefreshed() {
		return refreshed;
	}

	public Exception getLastError() {
		return lastError;
	}
	
}
