package gov.dod.cls.db;

import gov.dod.cls.config.ClsConfig;
import gov.dod.cls.model.JsonAble;
import gov.dod.cls.utils.ClsConstants;
import gov.dod.cls.utils.CommonUtils;
import gov.dod.cls.utils.sql.SqlUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.node.ArrayNode;
import org.codehaus.jackson.node.ObjectNode;

public class ClauseRecord extends JsonAble {

	public static final String TABLE_CLAUSES = "Clauses";
	
	public static final String FIELD_CLAUSE_ID = "clause_id"; 
	public static final String FIELD_CLAUSE_VERSION_ID = "clause_version_id";
	public static final String FIELD_CLAUSE_NAME = "clause_name";
	public static final String FIELD_INCLUSION_CD = "inclusion_cd";
	public static final String FIELD_CLAUSE_TITLE = "clause_title";
	public static final String FIELD_CLAUSE_DATA = "clause_data";
	public static final String FIELD_CLAUSE_URL = "clause_url";
	public static final String FIELD_CLAUSE_SECTION_ID = "clause_section_id";
	public static final String FIELD_EFFECTIVE_DATE = "effective_date";
	public static final String FIELD_REGULATION_ID = "regulation_id";
	
	public static final String FIELD_CLAUSE_CONDITIONS = "clause_conditions";
	public static final String FIELD_CLAUSE_RULE = "clause_rule";
	//public static final String FIELD_CONDITION_TO_CLAUSE = "condition_to_clause";
	public static final String FIELD_ADDITIONAL_CONDITIONS = "additional_conditions";

	public static final String FIELD_IS_ACTIVE = "is_active";
	public static final String FIELD_IS_EDITABLE = "is_editable";
	public static final String FIELD_EDITABLE_REMARKS = "editable_remarks";
	public static final String FIELD_IS_OPTIONAL = "is_optional";
	public static final String FIELD_PROVISON_OR_CLAUSE = "provision_or_clause";

	public static final String FIELD_IS_BASIC_CLAUSE = "is_basic_clause";
	public static final String FIELD_COMMERCIAL_STATUS = "commercial_status"; // CJ-476

	public static final String FIELD_OPTIONAL_STATUS = "optional_status";	// CJ-802 
	public static final String FIELD_OPTIONAL_CONDITIONS = "optional_conditions";	// CJ-802 
	
	public static final String FIELD_IS_DFARS_ACTIVITY = "is_dfars_activity";
	
	public static final String FIELD_CREATED_AT = "created_at";
	public static final String FIELD_UPDATED_AT = "updated_at";
	
	public static final String FAR_PREFIX = "52."; // CJ-264
	public static final String DFAR_PREFIX = "252."; // CJ-264
	
	public static final String FAR_CLAUSE = "FAR"; // CJ-264
	public static final String DFAR_CLAUSE = "DFAR"; // CJ-264
	public static final String LOCAL_CLAUSE = "LOCAL"; // CJ-264
	
	public static final String END_OF_CLAUSE = "(End of Clause)";
	public static final String END_OF_PROVISION = "(End of Provision)";

	public static final String SQL_SELECT
		= "SELECT A." + FIELD_CLAUSE_ID 
		+ ", A." + FIELD_CLAUSE_NAME
		+ ", A." + FIELD_INCLUSION_CD
		+ ", C." + InclusionRefRecord.FIELD_INCLUSION_NAME
		+ ", A." + FIELD_IS_ACTIVE
		+ ", A." + FIELD_CLAUSE_CONDITIONS
		+ ", A." + FIELD_CLAUSE_RULE
		+ ", A." + FIELD_CLAUSE_TITLE
		// + ", A." + FIELD_CONDITION_TO_CLAUSE
		+ ", A." + FIELD_ADDITIONAL_CONDITIONS
		+ ", A." + FIELD_CREATED_AT
		+ ", A." + FIELD_UPDATED_AT
		+ ", A." + FIELD_CLAUSE_DATA
		+ ", A." + FIELD_CLAUSE_URL
		+ ", A." + FIELD_CLAUSE_SECTION_ID
		+ ", B." + ClauseSectionRecord.FIELD_CLAUSE_SECTION_CODE
		+ ", B." + ClauseSectionRecord.FIELD_CLAUSE_SECTION_HEADER
		+ ", A." + FIELD_IS_EDITABLE
		+ ", A." + FIELD_EDITABLE_REMARKS // "editable_remarks";
		+ ", A." + FIELD_IS_OPTIONAL
		+ ", A." + FIELD_CLAUSE_VERSION_ID
		+ ", E." + ClauseVersionRecord.FIELD_CLAUSE_VERSION_NAME
		+ ", A." + FIELD_EFFECTIVE_DATE
		+ ", A." + FIELD_REGULATION_ID
		+ ", D." + RegulationRecord.FIELD_REGULATION_NAME
		+ ", A." + FIELD_PROVISON_OR_CLAUSE
		+ ", A." + FIELD_IS_BASIC_CLAUSE
		+ ", A." + FIELD_COMMERCIAL_STATUS // CJ-476
		+ ", A." + FIELD_OPTIONAL_STATUS // CJ-802
		+ ", A." + FIELD_OPTIONAL_CONDITIONS // CJ-802
		+ ", A." + FIELD_IS_DFARS_ACTIVITY 
		+ " FROM " + TABLE_CLAUSES + " A"
		+ " INNER JOIN " + ClauseSectionRecord.TABLE_CLAUSE_SECTIONS + " B"
		+ " ON (B." + ClauseSectionRecord.FIELD_CLAUSE_SECTION_ID + " = A." + FIELD_CLAUSE_SECTION_ID + ") "
		+ " INNER JOIN " + InclusionRefRecord.TABLE_INCLUSION_REF + " C"
		+ " ON (C." + InclusionRefRecord.FIELD_INCLUSION_CD + " = A." + FIELD_INCLUSION_CD + ") "
		+ " INNER JOIN " + RegulationRecord.TABLE_REGULATIONS + " D"
		+ " ON (D." + RegulationRecord.FIELD_REGULATION_ID + " = A." + FIELD_REGULATION_ID + ") "
		+ " INNER JOIN " + ClauseVersionRecord.TABLE_CLAUSE_VERSIONS + " E"
		+ " ON (E." + ClauseVersionRecord.FIELD_CLAUSE_VERSION_ID + " = A." + FIELD_CLAUSE_VERSION_ID + ") ";
	
	public static final String SQL_ORDER_BY_DEFAULT
		= " ORDER BY A." + FIELD_CLAUSE_VERSION_ID
		+ ", B." + ClauseSectionRecord.FIELD_CLAUSE_SECTION_CODE
		+ ", A." + FIELD_INCLUSION_CD + " DESC"
		+ ", SUBSTRING_INDEX(A." + FIELD_CLAUSE_NAME + ", '.', 1) DESC"
		+ ", LPAD(SUBSTRING(SUBSTRING_INDEX(A." + FIELD_CLAUSE_NAME + ", '-', 1), INSTR(A." + FIELD_CLAUSE_NAME + ", '.') + 1), 4, '0')"
		+ ", LPAD(SUBSTRING(SUBSTRING_INDEX(A." + FIELD_CLAUSE_NAME + ", ' ', 1), INSTR(A." + FIELD_CLAUSE_NAME + ", '-') + 1), 5, '0')"
		+ ", A." + FIELD_CLAUSE_NAME;
	/* CJ-746: Replaced with above
	public static final String SQL_ORDER_BY
		= " ORDER BY A." + FIELD_CLAUSE_VERSION_ID + ", B." + ClauseSectionRecord.FIELD_CLAUSE_SECTION_CODE
		+ ", A." + FIELD_CLAUSE_NAME;
	*/

	public static final String SQL_PRESCRIPTION_SELECT
		= "SELECT B." + PrescriptionRecord.FIELD_PRESCRIPTION_NAME
		+ ", B." + PrescriptionRecord.FIELD_PRESCRIPTION_URL
		+ ", B." + PrescriptionRecord.FIELD_PRESCRIPTION_ID
		+ " FROM " + ClausePrescriptionRecord.TABLE_CLAUSE_PRESCRIPTIONS + " A"		
		+ " INNER JOIN " + PrescriptionRecord.TABLE_PRESCRIPTIONS + " B"  
		+ " ON (B." + PrescriptionRecord.FIELD_PRESCRIPTION_ID + " = A." + ClausePrescriptionRecord.FIELD_PRESCRIPTION_ID + ")"	
		+ " WHERE A." + ClausePrescriptionRecord.FIELD_CLAUSE_ID + " = ?"
		+ " ORDER BY B." + PrescriptionRecord.FIELD_PRESCRIPTION_NAME;
	
	public static final String SQL_FILL_INS_SELECT
		= "SELECT A." + ClauseFillInRecord.FIELD_CLAUSE_ID
		+ ", A." + ClauseFillInRecord.FIELD_FILL_IN_CODE
		+ ", A." + ClauseFillInRecord.FIELD_FILL_IN_TYPE
		+ ", A." + ClauseFillInRecord.FIELD_FILL_IN_MAX_SIZE
		+ ", A." + ClauseFillInRecord.FIELD_FILL_IN_GROUP_NUMBER
		+ ", A." + ClauseFillInRecord.FIELD_FILL_IN_PLACEHOLDER
		+ ", A." + ClauseFillInRecord.FIELD_FILL_IN_DEFAULT_DATA
		+ ", A." + ClauseFillInRecord.FIELD_FILL_IN_DISPLAY_ROWS
		+ ", A." + ClauseFillInRecord.FIELD_FILL_IN_FOR_TABLE
		+ ", A." + ClauseFillInRecord.FIELD_FILL_IN_HEADING
		+ ", B." + FillInTypeRefRecord.FIELD_FILL_IN_TYPE_NAME
		+ " FROM " + ClauseFillInRecord.TABLE_CLAUSE_FILL_INS + " A"
		+ " INNER JOIN " + FillInTypeRefRecord.TABLE_FILL_INS_TYPE_REF + " B"
		+ " ON (B." + FillInTypeRefRecord.FIELD_FILL_IN_TYPE + " = A." + ClauseFillInRecord.FIELD_FILL_IN_TYPE + ")"
		+ " WHERE A." + ClauseFillInRecord.FIELD_CLAUSE_ID + " = ?"
		+ " ORDER BY A." + ClauseFillInRecord.FIELD_CLAUSE_FILL_IN_ID;
	

	// ---------------------------------------------------------------------
	private Integer id;
	private String clauseName;
	private String inclusion;
	private boolean active;
	private String clauseConditions;
	private String clauseRule;
	private String clauseTitle;
	//private String conditionToClause;
	private String additionalConditions;
	private String clauseData;
	private String url;
	private Integer clauseSectionId;
	private boolean editable;
	private String editableRemarks; // FIELD_EDITABLE_REMARKS
	private boolean optional;
	private String provisonOrClause;
	private Integer clauseVersionId;
	private Date effectiveDate;
	private Integer regulationId;
	private boolean isBasicClause;
	private String commercialStatus; // CJ-476
	private String optionalStatus; // CJ-802
	private String optionalConditions; // CJ-802

	private Date createdAt;
	private Date updatedAt;
	
	private String inclusionName;
	private String clauseSectionCode;
	private String clauseSectionHeader;
	private String clauseVersionName;
	private String regulationName;
	private boolean isDfarsActivity;
	
	private static ArrayNode arrayNodePres;
	private static ArrayNode arrayNodeFillIns;
	
	//private ArrayList<Integer> prescriptionIds = null;
	private ClauseFillInTable clauseFillInTable = null;
	private ClausePrescriptionTable clausePrescriptionTable = null;
	
	public void read(ResultSet rs) throws SQLException {
		this.id = rs.getInt(FIELD_CLAUSE_ID); 
		this.clauseName = rs.getString(FIELD_CLAUSE_NAME);
		this.inclusion = rs.getString(FIELD_INCLUSION_CD);
		this.active = rs.getBoolean(FIELD_IS_ACTIVE);
		this.clauseConditions = rs.getString(FIELD_CLAUSE_CONDITIONS);
		this.clauseRule = rs.getString(FIELD_CLAUSE_RULE);
		this.clauseTitle = rs.getString(FIELD_CLAUSE_TITLE);
		// this.conditionToClause = rs.getString(FIELD_CONDITION_TO_CLAUSE);
		this.additionalConditions = rs.getString(FIELD_ADDITIONAL_CONDITIONS);
		this.clauseData = rs.getString(FIELD_CLAUSE_DATA);
		this.url = rs.getString(FIELD_CLAUSE_URL);
		this.clauseSectionId = CommonUtils.toInteger(rs.getObject(FIELD_CLAUSE_SECTION_ID));
		this.editable = rs.getBoolean(FIELD_IS_EDITABLE);
		this.editableRemarks = rs.getString(FIELD_EDITABLE_REMARKS);
		this.optional = rs.getBoolean(FIELD_IS_OPTIONAL);
		this.provisonOrClause = rs.getString(FIELD_PROVISON_OR_CLAUSE);
		this.isBasicClause = rs.getBoolean(FIELD_IS_BASIC_CLAUSE);
		this.commercialStatus = rs.getString(FIELD_COMMERCIAL_STATUS); // CJ-476
		this.optionalStatus = rs.getString(FIELD_OPTIONAL_STATUS); // CJ-802
		this.optionalConditions = rs.getString(FIELD_OPTIONAL_CONDITIONS); // CJ-808
		this.isDfarsActivity = rs.getBoolean(FIELD_IS_DFARS_ACTIVITY);

		this.clauseVersionId = CommonUtils.toInteger(rs.getObject(FIELD_CLAUSE_VERSION_ID));
		this.effectiveDate = CommonUtils.toDate(rs.getObject(FIELD_EFFECTIVE_DATE));
		this.regulationId = CommonUtils.toInteger(rs.getObject(FIELD_REGULATION_ID));
		this.createdAt = CommonUtils.toDate(rs.getObject(FIELD_CREATED_AT));
		this.updatedAt = CommonUtils.toDate(rs.getObject(FIELD_UPDATED_AT));
		
		this.inclusionName = rs.getString("Inclusion_Name");
		this.clauseSectionCode = rs.getString(ClauseSectionRecord.FIELD_CLAUSE_SECTION_CODE);
		this.clauseSectionHeader = rs.getString(ClauseSectionRecord.FIELD_CLAUSE_SECTION_HEADER);
		this.clauseVersionName = rs.getString(ClauseVersionRecord.FIELD_CLAUSE_VERSION_NAME);
		this.regulationName = rs.getString(RegulationRecord.FIELD_REGULATION_NAME);
		
	}
	
	@Override
	protected void populateJson(ObjectNode json, boolean forInterview) {
		json.put(FIELD_CLAUSE_ID, this.id);
		json.put(FIELD_CLAUSE_NAME, this.clauseName);
		json.put(FIELD_INCLUSION_CD, this.inclusion);
		json.put(FIELD_CLAUSE_CONDITIONS, this.clauseConditions);
		json.put(FIELD_CLAUSE_RULE, this.clauseRule);
		
		if (forInterview)
			json.put(FIELD_CLAUSE_TITLE, this.getClauseTitleText()); // CJ-815
		else
			json.put(FIELD_CLAUSE_TITLE, this.clauseTitle);
		
		json.put(FIELD_CLAUSE_URL, this.url);
		json.put(FIELD_ADDITIONAL_CONDITIONS, this.additionalConditions);
		json.put(FIELD_CLAUSE_SECTION_ID, this.clauseSectionId);
		json.put(FIELD_IS_EDITABLE, this.editable);
		json.put(FIELD_EDITABLE_REMARKS, this.editableRemarks);
		json.put(FIELD_IS_OPTIONAL, this.optional);
		json.put(FIELD_EFFECTIVE_DATE, CommonUtils.toJsonDate(this.effectiveDate));
		json.put(FIELD_REGULATION_ID, this.regulationId);
		json.put(FIELD_COMMERCIAL_STATUS, this.commercialStatus); // CJ-476
		json.put(FIELD_OPTIONAL_STATUS, this.optionalStatus); // CJ-802
		json.put(FIELD_OPTIONAL_CONDITIONS, this.optionalConditions); // CJ-802
		json.put(FIELD_IS_BASIC_CLAUSE, this.isBasicClause); // CJ-193
		json.put(FIELD_IS_DFARS_ACTIVITY, this.isDfarsActivity);

		if (forInterview) {
			ArrayNode arrayNode = null;
			if (clausePrescriptionTable != null) {
				arrayNode = json.arrayNode();
				for (ClausePrescriptionRecord prescriptionRecord : this.clausePrescriptionTable) {
					arrayNode.add(prescriptionRecord.getPrescriptionId().intValue());
				}
			}
			json.put("prescriptionIds", arrayNode);

			json.put("clauseFillIns", (clauseFillInTable == null) ? null : clauseFillInTable.toJson(null, forInterview));
		} else {
			json.put(FIELD_CLAUSE_DATA, this.clauseData);
			json.put(FIELD_CLAUSE_VERSION_ID, this.clauseVersionId);
			json.put(FIELD_IS_ACTIVE, this.active);
			json.put(FIELD_PROVISON_OR_CLAUSE, this.provisonOrClause);
			
			json.put(FIELD_CREATED_AT, CommonUtils.toJsonDate(this.createdAt));
			json.put(FIELD_UPDATED_AT, CommonUtils.toJsonDate(this.updatedAt));
			
			json.put("Inclusion_Name", this.inclusionName);
			json.put(ClauseSectionRecord.FIELD_CLAUSE_SECTION_CODE, this.clauseSectionCode);
			json.put(ClauseSectionRecord.FIELD_CLAUSE_SECTION_HEADER, this.clauseSectionHeader);
			json.put(ClauseVersionRecord.FIELD_CLAUSE_VERSION_NAME, this.clauseVersionName);
			json.put(RegulationRecord.FIELD_REGULATION_NAME, this.regulationName);
			json.put(PrescriptionRecord.TABLE_PRESCRIPTIONS, arrayNodePres);
			json.put(ClauseFillInRecord.TABLE_CLAUSE_FILL_INS, arrayNodeFillIns);
		}
	};
	
	public void populateJsonForApi(ObjectNode json, boolean bForVer1, DocumentFillInsTable documentFillInsTable) {
		if (bForVer1) {
			json.put("id", this.id);
			json.put("clause_name", this.clauseName);
			json.put("effective_date", CommonUtils.toYearMonthDay(this.effectiveDate));
			json.put("inclusion", this.inclusion);
			json.put("active", this.active);
			json.put("start_date", (String)null);
			json.put("end_date", (String)null);
			json.put("prescription_text", (String)null);
			json.put("clause_text", (String)null);
			json.put("url", this.url);
			}
		else {
			json.put(FIELD_CLAUSE_ID, this.id);
			json.put(FIELD_CLAUSE_NAME, this.clauseName);
			json.put(FIELD_INCLUSION_CD, this.inclusion);
			json.put(FIELD_CLAUSE_CONDITIONS, this.clauseConditions);
			json.put(FIELD_CLAUSE_RULE, this.clauseRule);
			json.put(FIELD_CLAUSE_TITLE, this.getClauseTitleText()); // CJ-815
			json.put(FIELD_CLAUSE_URL, this.url);
			json.put(FIELD_ADDITIONAL_CONDITIONS, this.additionalConditions);
			json.put(FIELD_CLAUSE_SECTION_ID, this.clauseSectionId);
			json.put(FIELD_IS_EDITABLE, this.editable);
			json.put(FIELD_IS_OPTIONAL, this.optional);
			json.put(FIELD_EDITABLE_REMARKS, this.editableRemarks);
			json.put(FIELD_EFFECTIVE_DATE, CommonUtils.toMonthYear(this.effectiveDate));
			json.put(FIELD_REGULATION_ID, this.regulationId);
			json.put(FIELD_CLAUSE_DATA, this.clauseData);
			json.put(FIELD_CLAUSE_VERSION_ID, this.clauseVersionId);
			json.put(FIELD_IS_ACTIVE, this.active);
			json.put(FIELD_IS_BASIC_CLAUSE, this.isBasicClause);
			json.put(FIELD_CREATED_AT, CommonUtils.toZulu(this.createdAt));
			json.put(FIELD_UPDATED_AT, CommonUtils.toZulu(this.updatedAt));
			
			if ((this.clauseFillInTable != null) && (this.clauseFillInTable.size() > 0)) {
				ArrayNode arrayNode = json.arrayNode();
				for (ClauseFillInRecord oRecord : this.clauseFillInTable) {
					ObjectNode childNode = arrayNode.objectNode();
					oRecord.populateJsonForApi(childNode, documentFillInsTable);
					arrayNode.add(childNode);
				}
				json.put(ClauseFillInRecord.TABLE_CLAUSE_FILL_INS.toLowerCase(), arrayNode);
			}
		}
		json.put(FIELD_COMMERCIAL_STATUS, this.commercialStatus); // CJ-476
		json.put(FIELD_OPTIONAL_STATUS, this.optionalStatus); // CJ-802
		json.put(FIELD_OPTIONAL_CONDITIONS, this.optionalConditions); // CJ-802
		json.put(FIELD_IS_DFARS_ACTIVITY, this.isDfarsActivity);
	}
	
	public PrescriptionTable getPrescriptionTable() {
		ArrayList<Integer> prescriptionIds = new ArrayList<Integer>(); 
		if (clausePrescriptionTable != null) {
			for (ClausePrescriptionRecord prescriptionRecord : this.clausePrescriptionTable) {
				prescriptionIds.add(prescriptionRecord.getPrescriptionId());
			}
		}
		return PrescriptionTable.getSubset(prescriptionIds);
	}

	public String getClauseData(DocumentFillInsTable aFillIns, ClsConstants.OutputType outputType) { // CJ-1105
		String result = this.clauseData; // + "\n(End of Clause)";
		if (this.clauseFillInTable == null)
			return result;
		
		return this.clauseFillInTable.resolveFillIns(result, aFillIns, outputType);
	}
	
	public String getClauseDataWFillins(DocumentFillInsTable aFillIns, String fullText, 
			ClsConstants.OutputType outputType) { // CJ-1105
		String result = fullText; // + "\n(End of Clause)";
		if (this.clauseFillInTable == null)
			return result;
		
		return this.clauseFillInTable.resolveFillIns(result, aFillIns, outputType);
	}
	
	public static ClauseRecord getRecord(Integer id) {
		if (id == null) 
			return null;
		
		Connection conn = null;
		try {
			conn = ClsConfig.getDbConnection();
			return ClauseRecord.getRecord(conn, id);
		}
		catch (Exception oError) {
			oError.printStackTrace();
		}
		finally {
			SqlUtil.closeInstance(conn);
		}
		return null;
	}
	
	public static ClauseRecord getRecord(Connection conn, Integer clauseId) {
		ClauseRecord pClauseRecord = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			String sSql = SQL_SELECT;
			String operator = " WHERE ";
			if (clauseId != null) {
				// sSql += operator + " " + FIELD_QUESTION_ID + "=?";
				sSql += operator + " A." + FIELD_CLAUSE_ID + "=?";
				// operator = " AND ";
			}
			ps = conn.prepareStatement(sSql);
			int iParam = 1;
			if (clauseId != null)
				ps.setInt(iParam++, clauseId);
			rs = ps.executeQuery();
			if (rs.next()) {
				//if (pClauseRecord == null)
					pClauseRecord = new ClauseRecord();
				pClauseRecord.read(rs);
			}
			//---------------
			SqlUtil.closeInstance(rs, ps);
			rs.close();
			rs = null;			
			ps = conn.prepareStatement(SQL_PRESCRIPTION_SELECT);
			ObjectMapper mapper = new ObjectMapper();
			ObjectNode json = mapper.createObjectNode();
			arrayNodePres = json.arrayNode();
			ps.setInt(1, clauseId);
			rs = ps.executeQuery();
			while (rs.next()) {
				ObjectNode elementNode = json.objectNode();
				load(rs, elementNode);
				arrayNodePres.add(elementNode);
			}			
			//---------------
			SqlUtil.closeInstance(rs, ps);
			rs.close();
			rs = null;			
			ps = conn.prepareStatement(SQL_FILL_INS_SELECT);
			mapper = new ObjectMapper();
			json = mapper.createObjectNode();
			arrayNodeFillIns = json.arrayNode();
			ps.setInt(1, clauseId);
			rs = ps.executeQuery();
			while (rs.next()) {
				ObjectNode elementNode = json.objectNode();
				loadFillIns(rs, elementNode);
				arrayNodeFillIns.add(elementNode);
			}
		}
		catch (Exception oError) {
			oError.printStackTrace();
		}
		finally {
			SqlUtil.closeInstance(rs, ps);
		}
		return pClauseRecord;
	}
	
	
	private static void load(ResultSet rs, ObjectNode elementNode) throws SQLException {		
		elementNode.put(PrescriptionRecord.FIELD_PRESCRIPTION_NAME, rs.getString(PrescriptionRecord.FIELD_PRESCRIPTION_NAME));
		elementNode.put(PrescriptionRecord.FIELD_PRESCRIPTION_ID, rs.getString(PrescriptionRecord.FIELD_PRESCRIPTION_ID));
		elementNode.put(PrescriptionRecord.FIELD_PRESCRIPTION_URL, rs.getString(PrescriptionRecord.FIELD_PRESCRIPTION_URL));
	}
	
	private static void loadFillIns(ResultSet rs, ObjectNode elementNode) throws SQLException {		
		elementNode.put(ClauseFillInRecord.FIELD_CLAUSE_ID, rs.getInt(ClauseFillInRecord.FIELD_CLAUSE_ID));
		elementNode.put(ClauseFillInRecord.FIELD_FILL_IN_CODE, rs.getString(ClauseFillInRecord.FIELD_FILL_IN_CODE));
		elementNode.put(ClauseFillInRecord.FIELD_FILL_IN_TYPE, rs.getString(ClauseFillInRecord.FIELD_FILL_IN_TYPE));
		elementNode.put(FillInTypeRefRecord.FIELD_FILL_IN_TYPE_NAME, rs.getString(FillInTypeRefRecord.FIELD_FILL_IN_TYPE_NAME));
		elementNode.put(ClauseFillInRecord.FIELD_FILL_IN_MAX_SIZE, rs.getInt(ClauseFillInRecord.FIELD_FILL_IN_MAX_SIZE));
		elementNode.put(ClauseFillInRecord.FIELD_FILL_IN_GROUP_NUMBER, rs.getInt(ClauseFillInRecord.FIELD_FILL_IN_GROUP_NUMBER));
		elementNode.put(ClauseFillInRecord.FIELD_FILL_IN_PLACEHOLDER, rs.getString(ClauseFillInRecord.FIELD_FILL_IN_PLACEHOLDER));
		elementNode.put(ClauseFillInRecord.FIELD_FILL_IN_DEFAULT_DATA, rs.getString(ClauseFillInRecord.FIELD_FILL_IN_DEFAULT_DATA));
		elementNode.put(ClauseFillInRecord.FIELD_FILL_IN_DISPLAY_ROWS, CommonUtils.toInteger(rs.getObject(ClauseFillInRecord.FIELD_FILL_IN_DISPLAY_ROWS)));
		elementNode.put(ClauseFillInRecord.FIELD_FILL_IN_FOR_TABLE, rs.getBoolean(ClauseFillInRecord.FIELD_FILL_IN_FOR_TABLE));		
		elementNode.put(ClauseFillInRecord.FIELD_FILL_IN_HEADING, rs.getString(ClauseFillInRecord.FIELD_FILL_IN_HEADING));
	}
	
	// ----------------------------------------------------------------------
	public String getHeading(DocumentClauseRecord documentClauseRecord, boolean forApi) {
		if (forApi)
			return "<strong>" + this.getClauseName() + "</strong>\n"
					+ "<h4>" + this.getClauseTitleText() // CJ-815 changed from getClauseTitle
					//+ (this.isBasicClause? "-BASIC": "") 
					+ "\n<small>" // CJ-193
					+ "(" + this.getEffectiveMonthYear() + ")"
					//+ (documentClauseRecord.getIsRemovedByUser() ? " (Full text modified)" : "")
					+ "</small></h4>\n";
		else
			return this.getClauseName() + " " + this.getClauseTitleText() // CJ-815 changed from getClauseTitle // Removed &nbsp;
					// + (this.isBasicClause ? "-BASIC": "") // CJ-193
					+ " (" + this.getEffectiveMonthYear() + ")"
					// + (documentClauseRecord.getIsRemovedByUser() ? " (Full text modified)" : "")
					;
	}
	
	public String getClauseName() {
		return clauseName;
	}
	public void setClauseName(String clauseName) {
		this.clauseName = clauseName;
	}

	public boolean isFarClause() { // CJ-264
		return this.clauseName.startsWith(FAR_PREFIX); 
	}
	
	public boolean isDfarClause() { // CJ-264
		return this.clauseName.startsWith(DFAR_PREFIX); 
	}
	
	public boolean isLocalClause() { // CJ-264
		return !(this.isFarClause() || this.isDfarClause()); 
	}
	
	public int getClauseTypeSorter() { // CJ-264
		if (this.isFarClause())
			return 0;
		if (this.isDfarClause())
			return 1;
		return 2;
	}
	
	public String getInclusion() {
		return inclusion;
	}
	public void setInclusion(String inclusion) {
		this.inclusion = inclusion;
	}
	public boolean isFullText() {
		return InclusionRefRecord.INCLUDE_CD_FULL_TEXT.equals(this.inclusion);
	}
	public boolean isReference() {
		return InclusionRefRecord.INCLUDE_CD_REFERENCE.equals(this.inclusion);
	}
	
	public boolean isActive() {
		return active;
	}
	public void setActive(boolean active) {
		this.active = active;
	}
	
	public String getClauseConditions() {
		return clauseConditions;
	}
	public void setClauseConditions(String clauseConditions) {
		this.clauseConditions = clauseConditions;
	}
	
	public String getClauseRule() {
		return clauseRule;
	}
	public void setClauseRule(String clauseRule) {
		this.clauseRule = clauseRule;
	}
	
	public String getClauseTitle() {
		return clauseTitle;
	}
	
	public static String removeDotAtEnd(String clauseTitle) { // CJ-815
		if ((clauseTitle != null) && clauseTitle.endsWith(".")) {
			return clauseTitle.substring(0, clauseTitle.length() - 1);
		}
		return clauseTitle;
	}

	public String getClauseTitleText() { // CJ-815
		return removeDotAtEnd(this.clauseTitle);
	}
	
	// CJ-1174
	public String getClauseTitleNoBasic() { // CJ-1168
		String clauseTitle = removeDotAtEnd(this.clauseTitle);
		
		if (clauseTitle != null) {
			clauseTitle = clauseTitle.trim();
			if (clauseTitle.toUpperCase().endsWith("- BASIC")) 
				clauseTitle = clauseTitle.substring(0, clauseTitle.length() - "- BASIC".length());
			else if (clauseTitle.toUpperCase().endsWith("-BASIC")) 
				clauseTitle = clauseTitle.substring(0, clauseTitle.length() - "-BASIC".length());
			clauseTitle = clauseTitle.trim();
		}
		
		return clauseTitle;
	}
	
	public void setClauseTitle(String clauseTitle) {
		this.clauseTitle = clauseTitle;
	}
	
	public String getAdditionalConditions() {
		return additionalConditions;
	}
	public void setAdditionalConditions(String additionalConditions) {
		this.additionalConditions = additionalConditions;
	}
	
	public Date getCreatedAt() {
		return createdAt;
	}
	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}
	
	public Date getUpdatedAt() {
		return updatedAt;
	}
	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}
	
	public String getClauseData() {
		return clauseData;
	}
	public void setClauseData(String clauseData) {
		this.clauseData = clauseData;
	}
	
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	
	public Integer getClauseSectionId() {
		return clauseSectionId;
	}
	public boolean isSameSection(int sectionId) {
		if (this.clauseSectionId == null)
			return false;
		return (this.clauseSectionId.intValue() == sectionId);
	}
	public void setClauseSectionId(Integer clauseSectionId) {
		this.clauseSectionId = clauseSectionId;
	}
	
	public boolean isEditable() {
		return editable;
	}
	public void setEditable(boolean editable) {
		this.editable = editable;
	}
	
	public String getEditableRemarks() {
		return editableRemarks;
	}

	public void setEditableRemarks(String editableRemarks) {
		this.editableRemarks = editableRemarks;
	}

	public boolean isOptional() {
		return optional;
	}
	public void setOptional(boolean optional) {
		this.optional = optional;
	}
	
	public String getProvisonOrClause() {
		return provisonOrClause;
	}
	public void setProvisonOrClause(String provisonOrClause) {
		this.provisonOrClause = provisonOrClause;
	}
	
	public String getEndOfProvisonOrClauseText() { // CJ-815
		if ("P".equalsIgnoreCase(this.provisonOrClause))
			return ClauseRecord.END_OF_PROVISION;
		else
			return ClauseRecord.END_OF_CLAUSE;
	}

	public Integer getClauseVersionId() {
		return clauseVersionId;
	}
	public String getClauseVersionName() {
		return clauseVersionName;
	}
	
	public void setClauseVersionId(Integer clauseVersionId) {
		this.clauseVersionId = clauseVersionId;
	}
	
	public Date getEffectiveDate() {
		return effectiveDate;
	}
	public String getEffectiveMonthYear() {
		return CommonUtils.toMonthYear(this.effectiveDate).toUpperCase(Locale.ENGLISH);
	}
	public void setEffectiveDate(Date effectiveDate) {
		this.effectiveDate = effectiveDate;
	}
	
	public Integer getRegulationId() {
		return regulationId;
	}
	public RegulationRecord getRegulationRecord() {
		if (this.regulationId == null)
			return null;
		return RegulationTable.getRecord(this.regulationId);
	}
	public void setRegulationId(Integer regulationId) {
		this.regulationId = regulationId;
	}

	public String getRegulationName() {
		return regulationName;
	}
	
	public String getInclusionName() {
		return inclusionName;
	}
	
	public String getSectionCode() {
		return clauseSectionCode;
	}
	
	public String getSectionHeader() {
		return clauseSectionHeader;
	}
	
	public boolean isBasicClause() {
		return isBasicClause;
	}
	public void setBasicClause(boolean isBasicClause) {
		this.isBasicClause = isBasicClause;
	}
	
	public String getCommercialStatus() {
		return this.commercialStatus;
	}

	public void setCommercialStatus(String commercialStatus) {
		this.commercialStatus = commercialStatus;
	}

	public String getOptionalStatus() {
		return this.optionalStatus;
	}

	public void setOptionalStatus(String optionalStatus) {
		this.optionalStatus = optionalStatus;
	}
	
	public String getOptionalConditions() {
		return this.optionalConditions;
	}

	// CJ-1072, track the spreadsheet tab for the clauses.
	public void setisDfarsActivity(boolean isDfarsActivity) {
		this.isDfarsActivity = isDfarsActivity;
	}
	
	public boolean getIsDfarsActivity() {
		return this.isDfarsActivity;
	}
	
	public void setOptionalConditions(String optionalConditions) {
		this.optionalConditions = optionalConditions;
	}
	
	public Integer getId() { return id; }

	public ClauseFillInTable getClauseFillInTable() {
		return clauseFillInTable;
	}
	
	public ClauseFillInRecord findFillInById(int fillInId) {
		if (this.clauseFillInTable == null)
			return null;
		return this.clauseFillInTable.findById(fillInId);
	}

	public void setClauseFillInTable(ClauseFillInTable clauseFillInTable) {
		this.clauseFillInTable = clauseFillInTable;
	}
	
	public boolean hasSameFillInCodes(ClauseRecord otherClauseRecord) {
		if ((otherClauseRecord == null) || (this.clauseFillInTable == null))
			return false;
		return this.clauseFillInTable.hasSameFillInCodes(otherClauseRecord.clauseFillInTable);
	}
	
	public String getFooterText() {
		return this.getEndOfProvisonOrClauseText();
		/*
		if(provisonOrClause == null)
			return null;
		return "P".equals(provisonOrClause) ? END_OF_PROVISION : END_OF_CLAUSE;
		*/
	}

	public ClausePrescriptionTable getClausePrescriptionTable() { return clausePrescriptionTable; }
	public void setClausePrescriptionTable(ClausePrescriptionTable clausePrescriptionTable) { this.clausePrescriptionTable = clausePrescriptionTable; }

	//public ArrayList<Integer> getPrescriptionIds() { return prescriptionIds; }
	//public void setPrescriptionIds(ArrayList<Integer> prescriptionIds) { this.prescriptionIds = prescriptionIds; }

}
