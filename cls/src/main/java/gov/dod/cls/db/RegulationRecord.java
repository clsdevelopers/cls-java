package gov.dod.cls.db;

import gov.dod.cls.model.JsonAble;
import gov.dod.cls.utils.CommonUtils;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import org.codehaus.jackson.node.ObjectNode;

public class RegulationRecord extends JsonAble {

	public static final String TABLE_REGULATIONS = "Regulations";
	
	public static final String FIELD_REGULATION_ID = "regulation_id";
	public static final String FIELD_REGULATION_NAME = "regulation_name";
	public static final String FIELD_REGULATION_TITLE = "regulation_title";
	public static final String FIELD_REGULATION_URL = "regulation_url";
	public static final String FIELD_CREATED_AT = "created_at";
	public static final String FIELD_UPDATED_AT = "updated_at";
	
	public final static String REGUL_FAR = "FAR";
	public final static String REGUL_DFARS = "DFARS";
	public final static String REGUL_AFFARS = "AFFARS";
	public final static String REGUL_AFARS = "AFARS";
	public final static String REGUL_NMCARS = "NMCARS";
	public final static String REGUL_USSOCOM = "USSOCOM";
	public final static String REGUL_USTRANSCOM = "USTRANSCOM";
	public final static String REGUL_DARS = "DARS";
	
	public static final String SQL_SELECT
		= "SELECT " + FIELD_REGULATION_ID
		+ ", " + FIELD_REGULATION_NAME
		+ ", " + FIELD_REGULATION_TITLE
		+ ", " + FIELD_REGULATION_URL
		+ ", " + FIELD_CREATED_AT
		+ ", " + FIELD_UPDATED_AT
		+ " FROM " + TABLE_REGULATIONS
		+ " ORDER BY " + FIELD_REGULATION_NAME;
	
	public static String getLogicalName(int id) {
		switch (id) {
		case 1: return "FAR";
		case 2: return "DFARS";
		case 3: return "AFFARS";
		case 4: return "AFARS";
		case 5: return "NMCARS";
		case 6: return "USSOCOM";
		case 7: return "USTRANSCOM";
		case 8: return "DARS";		
		default: return null;
		}
	}
	
	// ================================================================
	private Integer id;
	private String name;
	private String title;
	private String url;
	private Date createdAt;
	private Date updatedAt;
	
	public void read(ResultSet rs) throws SQLException {
		this.id = rs.getInt(FIELD_REGULATION_ID); 
		this.name = rs.getString(FIELD_REGULATION_NAME);
		this.title = rs.getString(FIELD_REGULATION_TITLE);
		this.url = rs.getString(FIELD_REGULATION_URL);
		this.createdAt = CommonUtils.toDate(rs.getObject(FIELD_CREATED_AT));
		this.updatedAt = CommonUtils.toDate(rs.getObject(FIELD_UPDATED_AT));
	}
	
	@Override
	protected void populateJson(ObjectNode json, boolean forInterview) {
		json.put(FIELD_REGULATION_ID, this.id);
		json.put(FIELD_REGULATION_NAME, this.name);
		json.put(FIELD_REGULATION_TITLE, this.title);
		json.put(FIELD_REGULATION_URL, this.url);
		if (!forInterview) {
			json.put(FIELD_CREATED_AT, CommonUtils.toJsonDate(this.createdAt));
			json.put(FIELD_UPDATED_AT, CommonUtils.toJsonDate(this.updatedAt));
		}
	};

	// ----------------------------------------------------------------
	public Integer getId() {
		return id;
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public Date getCreatedAt() {
		return createdAt;
	}
	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}
	public Date getUpdatedAt() {
		return updatedAt;
	}
	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}
	
	public PrescriptionTable getPrescription() {
		return PrescriptionTable.regulationSubset(this.id);
	}
	
}
