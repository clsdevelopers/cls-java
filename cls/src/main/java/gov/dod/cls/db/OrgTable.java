package gov.dod.cls.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.node.ArrayNode;
import org.codehaus.jackson.node.ObjectNode;

import gov.dod.cls.PageApi;
import gov.dod.cls.bean.session.UserSession;
import gov.dod.cls.config.ClsConfig;
import gov.dod.cls.model.JsonAble;
import gov.dod.cls.utils.CommonUtils;
import gov.dod.cls.utils.Pagination;
import gov.dod.cls.utils.sql.SqlUtil;

public class OrgTable {

	public static final String PREFIX_DO_ORG = "org-";
	
	public static final String DO_ORG_LIST = PREFIX_DO_ORG + "list";
	public static final String DO_ORG_DETAILS_GET = PREFIX_DO_ORG + "detail-get";
	public static final String DO_ORG_DETAILS_EDIT = PREFIX_DO_ORG + "detail-edit";
	public static final String DO_ORG_DETAILS_NEW = PREFIX_DO_ORG + "detail-new";
	public static final String DO_ORG_REMOVE = PREFIX_DO_ORG + "remove";
	public static final String DO_ORG_REFERENCES = PREFIX_DO_ORG + "references";

	public static void processRequest(String sDo, UserSession oUserSession, 
			HttpServletRequest req, HttpServletResponse resp) {

		if (OrgTable.DO_ORG_REFERENCES.equals(sDo)) {
			String response;
			try {
				String page = req.getParameter("pg");
				response = JsonAble.jsonResult(JsonAble.STATUS_SUCCESS, OrgTable.referenceJson(oUserSession, page), null); 
			} catch (Exception oError) {
				response = JsonAble.jsonResult(JsonAble.STATUS_ERROR, null, oError.getMessage()); 
			}
			PageApi.send(resp, JsonAble.JSON_RESPONSE_FORMAT, response);
			return;
		}
		
		if (!oUserSession.isSuperUser()) { // CJ-680
			return;
		}
		
		switch (sDo) {
		case OrgTable.DO_ORG_LIST:
			OrgTable.processList(req, resp);
			return;
		case OrgTable.DO_ORG_DETAILS_GET:
			OrgTable.getOrgDetails(sDo, oUserSession, req, resp);
			return;
		case OrgTable.DO_ORG_DETAILS_EDIT:
			OrgTable.saveOrgDetails(sDo, oUserSession, req, resp);
			return;
		case OrgTable.DO_ORG_DETAILS_NEW:
			OrgTable.saveOrgDetails(sDo, oUserSession, req, resp);
			return;
		case OrgTable.DO_ORG_REMOVE:
			//OrgTable.deactiveUser(oUserSession, req, resp);
			return;
		}
	}
	
	// ----------------------------------------------------------
	private static void processList(HttpServletRequest req, HttpServletResponse resp) {
		String query = req.getParameter("query");
		String sort_field = req.getParameter("sort_field");
		String sort_order = req.getParameter("sort_order");
		
		Pagination pagination = new Pagination(req);
		pagination.addDataSource("Users");
		
		String sSelect
			= "SELECT A." + OrgRecord.FIELD_ORG_ID + ", D." + DeptRecord.FIELD_DEPT_NAME + ", A." + OrgRecord.FIELD_ORG_NAME // + ", A." + OrgRecord.FIELD_ABBREVIATION
			+ ", COUNT(B." + UserRecord.FIELD_USER_ID + ") AS UserCount"
			+ ", COUNT(C." + OrgRegulationRecord.FIELD_ORG_ID + ") AS RegulationCount";
		String sFromWhere
			= " FROM " + OrgRecord.TABLE_ORGS + " A"
			+ " LEFT OUTER JOIN " + UserRecord.TABLE_USERS + " B ON (B.Org_Id = A." + OrgRecord.FIELD_ORG_ID + ")"
			+ " LEFT OUTER JOIN " + OrgRegulationRecord.TABLE_ORG_REGULATIONS + " C ON (C." + OrgRegulationRecord.FIELD_ORG_ID + " = A." + OrgRecord.FIELD_ORG_ID + ")"
			+ " LEFT OUTER JOIN " + DeptRecord.TABLE_DEPTS + " D ON (D." + DeptRecord.FIELD_DEPT_ID + " = A." + OrgRecord.FIELD_DEPT_ID + ")";
		String operator = " WHERE ";
		String condition = null;
		if (CommonUtils.isNotEmpty(query)) {
			pagination.addDataSource("query=" + query);
			condition = "(LOWER(D." + DeptRecord.FIELD_DEPT_NAME + ") LIKE ?"
						+ " OR LOWER(A." + OrgRecord.FIELD_ORG_NAME + ") LIKE ?)";
						//+ " OR LOWER(A." + OrgRecord.FIELD_ABBREVIATION + ") LIKE ?)";
			sFromWhere += operator + condition;
			operator = " AND ";
			query = "%" + query.toLowerCase() + "%";
		}
		
		String sAsc = ("desc".equals(sort_order) ? " DESC" : "");
		String sGroupBy = " GROUP BY 1, 2, 3"; // , 4";
		//+ "ORDER BY A.dept, A.name";
		String sOrderBy = " ORDER BY ";
		switch (sort_field) {
		case "name":
			sOrderBy += "A." + OrgRecord.FIELD_ORG_NAME + sAsc + ", D." + DeptRecord.FIELD_DEPT_NAME + sAsc;
			break;
		//case "abbreviation":
			//	sOrderBy += "A.abbreviation" + sAsc + ", B." + DeptRecord.FIELD_DEPT_NAME + sAsc + ", A.name" + sAsc;
			//	break;
		default:
			sOrderBy += "D." + DeptRecord.FIELD_DEPT_NAME + sAsc + ", A." + OrgRecord.FIELD_ORG_NAME + sAsc;
			break;
		}
		
		String sSql;
		Connection conn = null;
		PreparedStatement ps1 = null;
		ResultSet rs1 = null;
		try {
			conn = ClsConfig.getDbConnection();
			sSql = "SELECT COUNT(A." + OrgRecord.FIELD_ORG_ID + ") " // FROM " + OrgRecord.TABLE_ORGS + " A"
					+ sFromWhere;
				//+ ((condition == null) ? "" : " WHERE " + condition);
			ps1 = conn.prepareStatement(sSql);
			int iParam = 1;
			if (CommonUtils.isNotEmpty(query)) {
				ps1.setString(iParam++, query);
				ps1.setString(iParam++, query);
				//ps1.setString(iParam++, query);
			}
			rs1 = ps1.executeQuery();
			boolean isAcceptableRange = false;
			if (rs1.next())
				isAcceptableRange = pagination.isAcceptedRange(rs1.getInt(1));
			
			if (isAcceptableRange) {
				SqlUtil.closeInstance(rs1);
				rs1 = null;
				SqlUtil.closeInstance(ps1);
				ps1 = null;

				sSql = sSelect + sFromWhere + sGroupBy + sOrderBy + pagination.sqlLimit();
				ps1 = conn.prepareStatement(sSql);
				iParam = 1;
				if (CommonUtils.isNotEmpty(query)) {
					ps1.setString(iParam++, query);
					ps1.setString(iParam++, query);
					//ps1.setString(iParam++, query);
				}
				rs1 = ps1.executeQuery();

				while (rs1.next()) {
					ObjectNode json = pagination.getMapper().createObjectNode();
					json.put(OrgRecord.FIELD_ORG_ID, rs1.getInt(OrgRecord.FIELD_ORG_ID));
					json.put(DeptRecord.FIELD_DEPT_NAME, rs1.getString(DeptRecord.FIELD_DEPT_NAME));
					json.put(OrgRecord.FIELD_ORG_NAME, rs1.getString(OrgRecord.FIELD_ORG_NAME));
					//json.put(OrgRecord.FIELD_ABBREVIATION, rs1.getString(OrgRecord.FIELD_ABBREVIATION));
					json.put("UserCount", rs1.getInt("UserCount"));
					json.put("RegulationCount", rs1.getInt("RegulationCount"));
					
					pagination.incCount();
					pagination.getArrayNode().add(json);
				}
			}
			pagination.send(resp);
		}
		catch (Exception oError) {
			oError.printStackTrace();
		}
		finally {
			SqlUtil.closeInstance(rs1, ps1, conn);
		}
	}
	
	// --------------------------------------------------------------------------------------
	public static boolean getReference(String version, Boolean full, Pagination pagination)
			throws SQLException {
		pagination.addDataSource("Orgs");
		
		if (full == null)
			full = false;
		
		String sSql;
		Connection conn = null;
		PreparedStatement ps1 = null;
		ResultSet rs1 = null;
		try {
			conn = ClsConfig.getDbConnection();
			sSql = "SELECT COUNT(" + OrgRecord.FIELD_ORG_ID + ")"
				+ " FROM " + OrgRecord.TABLE_ORGS;
			ps1 = conn.prepareStatement(sSql);
			rs1 = ps1.executeQuery();
			boolean isAcceptableRange = false;
			if (rs1.next())
				isAcceptableRange = pagination.isAcceptedRange(rs1.getInt(1));
			
			if (isAcceptableRange) {
				SqlUtil.closeInstance(rs1);
				rs1 = null;
				SqlUtil.closeInstance(ps1);
				ps1 = null;

				sSql = "SELECT A." + OrgRecord.FIELD_ORG_ID
						+ ", A." + OrgRecord.FIELD_ORG_NAME;
				if (full)
					sSql += ", B." + DeptRecord.FIELD_DEPT_NAME
						+ ", A." + OrgRecord.FIELD_ORG_DESCRIPTION
						+ ", A." + OrgRecord.FIELD_ORG_SOURCE_ID
						+ ", A." + OrgRecord.FIELD_ORG_SOURCE_PARENT_ID
						+ ", A." + OrgRecord.FIELD_ORG_SOURCE_LEVEL
						+ ", A." + OrgRecord.FIELD_CREATED_AT
						+ ", A." + OrgRecord.FIELD_UPDATED_AT;
				
				sSql += " FROM " + OrgRecord.TABLE_ORGS + " A"
					+ " LEFT OUTER JOIN " + DeptRecord.TABLE_DEPTS + " B ON (B.Dept_Id = A." + OrgRecord.FIELD_DEPT_ID + ")"
					+ " ORDER BY 2" + pagination.sqlLimit();
				ps1 = conn.prepareStatement(sSql);
				rs1 = ps1.executeQuery();
				while (rs1.next()) {
					ObjectNode json = pagination.getMapper().createObjectNode();
					json.put(OrgRecord.FIELD_ORG_ID, rs1.getInt(OrgRecord.FIELD_ORG_ID));
					json.put(OrgRecord.FIELD_ORG_NAME, rs1.getString(OrgRecord.FIELD_ORG_NAME));
					if (full) {
						//json.put(OrgRecord.FIELD_ABBREVIATION, rs1.getString(OrgRecord.FIELD_ABBREVIATION));
						json.put(DeptRecord.FIELD_DEPT_NAME, rs1.getString(DeptRecord.FIELD_DEPT_NAME));
						json.put(OrgRecord.FIELD_ORG_DESCRIPTION, rs1.getString(OrgRecord.FIELD_ORG_DESCRIPTION));
						json.put(OrgRecord.FIELD_ORG_SOURCE_ID, rs1.getInt(OrgRecord.FIELD_ORG_SOURCE_ID));
						json.put(OrgRecord.FIELD_ORG_SOURCE_PARENT_ID, rs1.getInt(OrgRecord.FIELD_ORG_SOURCE_PARENT_ID));
						json.put(OrgRecord.FIELD_ORG_SOURCE_LEVEL, rs1.getInt(OrgRecord.FIELD_ORG_SOURCE_LEVEL));
						json.put(OrgRecord.FIELD_CREATED_AT, CommonUtils.toJsonDate(rs1.getTimestamp(OrgRecord.FIELD_CREATED_AT)));
						json.put(OrgRecord.FIELD_UPDATED_AT, CommonUtils.toJsonDate(rs1.getTimestamp(OrgRecord.FIELD_UPDATED_AT)));
					}
					pagination.incCount();
					pagination.getArrayNode().add(json);
				}
			}
		}
		finally {
			SqlUtil.closeInstance(rs1, ps1, conn);
		}
		return true;
	}
	
	private static void getOrgDetails(String sDo, UserSession oUserSession, 
			HttpServletRequest req, HttpServletResponse resp) {
		Integer id = CommonUtils.toInteger(req.getParameter("id"));
		String errorMessage = null;
		OrgRecord orgRecord = null;
		if (id != null) {
			orgRecord = OrgRecord.getRecord(id, null);
			if (orgRecord == null)
				errorMessage = "Invalid request";
		} else
			errorMessage = "Unauthorized request";
		String response;
		if ((orgRecord != null)) {
			ObjectNode objectNode = (ObjectNode)orgRecord.toJson();
			try {
				// AuditEventTable.addJson(id, objectNode);
				AuditEventTable.addJson(oUserSession.getUserId(), objectNode);   // 4/5/2015 IC
			} catch (Exception oIgnore) {
				oIgnore.printStackTrace();
			}
			response = JsonAble.jsonResult(JsonAble.STATUS_SUCCESS, objectNode, null);
		} else
			response = JsonAble.jsonResult(JsonAble.STATUS_FAIL, null, errorMessage);
		PageApi.send(resp, JsonAble.JSON_RESPONSE_FORMAT, response);
	}

	private static void saveOrgDetails(String sDo, UserSession oUserSession, 
			HttpServletRequest req, HttpServletResponse resp) {
		String errorMessage = null;
		OrgRecord orgRecord = null;
		Integer id = null;
		if (!DO_ORG_DETAILS_NEW.equals(sDo)) {
			id = CommonUtils.toInteger(req.getParameter("id"));
			if (id != null) {
				orgRecord = OrgRecord.getRecord(id, null);
				if (orgRecord == null)
					errorMessage = "Invalid request";
			} else
				errorMessage = "Unauthorized request";

			if (errorMessage != null) {
				PageApi.send(resp, JsonAble.JSON_RESPONSE_FORMAT, 
						JsonAble.jsonResult(JsonAble.STATUS_FAIL, null, errorMessage));
				return;
			}
		}

		String[] newRegulIds = null;
		String newDeptId = req.getParameter("dept_id");
		String newOrgName = req.getParameter("org_name");
		String newOrgDesc = req.getParameter("org_description");
		String sRegulIds = req.getParameter("org_regul_ids"); 
		newRegulIds = sRegulIds.split(",");

		ArrayList<Integer> newReguls = new ArrayList<Integer>();
		for (int iRegul = 0; iRegul < newRegulIds.length; iRegul++) {
			Integer newRegul = CommonUtils.toInteger(newRegulIds[iRegul]);
			if (newRegul != null)
				newReguls.add(newRegul);
		}
		
		Connection conn = null;
		try {
			conn = ClsConfig.getDbConnection();
			if (orgRecord != null) {
				if (!orgRecord.adminUpdate(conn, newOrgName, newOrgDesc,  
						CommonUtils.toInteger(newDeptId), 
						newReguls, oUserSession.getUserProfile())) {
					errorMessage = "No changes detected";
				} else {
					oUserSession.setMessage("Updated organization record of " + newOrgName);
				}
			} else {
				orgRecord = OrgRecord.getRecord(conn, null, newOrgName, null);
				if (orgRecord != null) {
					errorMessage = "Existing organization";
				} else {
					orgRecord = new OrgRecord();
					orgRecord.adminCreate(conn, newOrgName, newOrgDesc, 
							CommonUtils.toInteger(newDeptId), 
							newReguls, oUserSession.getUserProfile());
					oUserSession.setMessage("Created organization record of " + newOrgName);
				}
			}
		}
		catch (Exception oError) {
			errorMessage = "Unable to save: " + oError.getMessage();
		}
		finally {
			SqlUtil.closeInstance(conn);
		}

		String response;
		if (errorMessage == null) {
			response = JsonAble.jsonResult(JsonAble.STATUS_SUCCESS, null, null);
		} else
			response = JsonAble.jsonResult(JsonAble.STATUS_FAIL, null, errorMessage);
		PageApi.send(resp, JsonAble.JSON_RESPONSE_FORMAT, response);
	}
	
	public static ArrayNode toJson(ObjectMapper mapper, Connection conn, Integer iFitlerOrgId) throws SQLException {
		if (mapper == null)
			mapper = new ObjectMapper();
		ArrayNode result = mapper.createArrayNode();
		boolean bCreateConnection =  (conn == null);
		PreparedStatement ps1 = null;
		ResultSet rs1 = null;
		try {
			if (bCreateConnection)
				conn = ClsConfig.getDbConnection();
			String sSql = "SELECT " + OrgRecord.FIELD_ORG_ID
					+ ", " + OrgRecord.FIELD_ORG_NAME
					+ ", " + OrgRecord.FIELD_DEPT_ID
					+ " FROM " + OrgRecord.TABLE_ORGS
					+ ((iFitlerOrgId == null) ? ""
							: " WHERE " + OrgRecord.FIELD_ORG_ID + " = " + iFitlerOrgId) // CJ-680
					+ " ORDER BY " + OrgRecord.FIELD_DEPT_ID + ", " + OrgRecord.FIELD_ORG_NAME;
			ps1 = conn.prepareStatement(sSql);
			rs1 = ps1.executeQuery();			
			while (rs1.next()) {
				ObjectNode json = mapper.createObjectNode();
				json.put(OrgRecord.FIELD_DEPT_ID, rs1.getInt(OrgRecord.FIELD_DEPT_ID));
				json.put(OrgRecord.FIELD_ORG_ID, rs1.getInt(OrgRecord.FIELD_ORG_ID));
				json.put(OrgRecord.FIELD_ORG_NAME, rs1.getString(OrgRecord.FIELD_ORG_NAME));
				result.add(json);
			}
		}
		finally {
			SqlUtil.closeInstance(rs1, ps1);
			if (bCreateConnection)
				SqlUtil.closeInstance(conn);
		}
		return result;
	}
	
	private static ObjectNode referenceJson(UserSession oUserSession, String page) throws SQLException {
		ObjectMapper mapper = new ObjectMapper();
		ObjectNode result = mapper.createObjectNode();
		
		Connection conn = null;
		try {
			conn = ClsConfig.getDbConnection();
			Integer iOrgId = null; // CJ-680
			if ((oUserSession != null) && ("application-details".equals(page) || "user_details".equals(page))) {
				if (!oUserSession.isSuperUser())
					iOrgId = oUserSession.getOrgId();
			}
			result.put(OrgRecord.TABLE_ORGS, OrgTable.toJson(mapper, conn, iOrgId));
			result.put(DeptRecord.TABLE_DEPTS, DeptTable.toJson(mapper, conn, iOrgId));
		}
		finally {
			SqlUtil.closeInstance(conn);
		}
		
		return result;
	}


}
