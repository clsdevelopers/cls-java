package gov.dod.cls.db;

import gov.dod.cls.api.endpoints.DocumentEndpoint;
import gov.dod.cls.bean.session.UserSession;
import gov.dod.cls.config.ClsConfig;
import gov.dod.cls.model.JsonAble;
import gov.dod.cls.utils.CommonUtils;
import gov.dod.cls.utils.sql.SqlUpdate;
import gov.dod.cls.utils.sql.SqlUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Date;

import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.node.ArrayNode;
import org.codehaus.jackson.node.ObjectNode;

public class DocumentRecord extends JsonAble {
	
	public static final String TABLE_DOCUMENTS = "Documents";
	
	public static final String FIELD_DOCUMENT_ID = "document_id";
	public static final String FIELD_DOCUMENT_TYPE = "document_type";
	public static final String FIELD_ACQUISITION_TITLE = "acquisition_title";
	public static final String FIELD_DOCUMENT_NUMBER = "document_number";
	public static final String FIELD_USER_ID = "user_id";
	public static final String FIELD_DOCUMENT_STATUS = "doc_status_cd";
	public static final String FIELD_CONTRACT_NUMBER = "contract_number";
	public static final String FIELD_ORDER_NUMBER = "order_number";
	public static final String FIELD_SOLICITATION_ID = "solicitation_id";
	public static final String FIELD_FULL_NAME = "document_full_name";
	//public static final String FIELD_IS_MANUAL_ORDERS = "is_manual_orders"; // CJ-1005
	public static final String FIELD_CLAUSE_VERSION_ID = "clause_version_id";
	public static final String FIELD_APPLICATION_ID = "application_id";
	public static final String FIELD_LAST_QUESTION_CODE = "last_question_code";
	public static final String FIELD_CREATED_AT = "created_at";
	public static final String FIELD_UPDATED_AT = "updated_at";
	public static final String FIELD_DOCUMENT_DATE = "document_date"; // CJ-1312
	
	public static final String TAG_IS_LINKED_AWARD = "isLinkedAward";
	
	public static final String TAG_LINKED_AWARD_DOCUMENT_ID = "linked_award_document_id";

	public static final String SQL_SELECT
		= "SELECT " + FIELD_DOCUMENT_ID
		+ ", " + FIELD_DOCUMENT_TYPE
		+ ", " + FIELD_ACQUISITION_TITLE
		+ ", " + FIELD_DOCUMENT_NUMBER
		+ ", " + FIELD_CLAUSE_VERSION_ID		
		+ ", " + FIELD_USER_ID
		+ ", " + FIELD_DOCUMENT_STATUS
		+ ", " + FIELD_CREATED_AT
		+ ", " + FIELD_UPDATED_AT
		+ ", " + FIELD_APPLICATION_ID
		+ ", " + FIELD_CONTRACT_NUMBER
		+ ", " + FIELD_ORDER_NUMBER
		+ ", " + FIELD_SOLICITATION_ID
		+ ", " + FIELD_FULL_NAME
		//+ ", " + FIELD_IS_MANUAL_ORDERS // CJ-1005
		+ ", " + FIELD_CLAUSE_VERSION_ID
		+ ", " + FIELD_LAST_QUESTION_CODE
		+ ", " + FIELD_DOCUMENT_DATE // CJ-1312
		+ " FROM " + TABLE_DOCUMENTS;
	
	// ----------------------------------------------------------------------
	private Integer id = null;
	private String documentType; 
	private String acquisitionTitle; 
	private String documentNumber; 
	private Integer userId; 
	private String documentStatus; 
	private Date createdAt; 
	private Date updatedAt; 
	private Integer applicationId; 
	private String contractNumber; 
	private String orderNumber; 
	private Integer solicitationId; 
	private String fullName;
	private Boolean isManualOrders;
	private Integer clauseVersionId = null;
	private String lastQuestionCode = null;
	private Date documentDate = null; // CJ-1312

	protected DocumentAnswerTable documentAnswerTable = null;
	protected DocumentFillInsTable documentFillInsTable = null;
	protected DocumentClauseTable documentClauseTable = null;
	
	public void read(ResultSet rs) throws SQLException {
		this.id = rs.getInt(FIELD_DOCUMENT_ID); 
		this.documentType = rs.getString(FIELD_DOCUMENT_TYPE); 
		this.acquisitionTitle = rs.getString(FIELD_ACQUISITION_TITLE); 
		this.documentNumber = rs.getString(FIELD_DOCUMENT_NUMBER); 
		this.userId = CommonUtils.toInteger(rs.getObject(FIELD_USER_ID));
		this.documentStatus = rs.getString(FIELD_DOCUMENT_STATUS); 
		this.applicationId = CommonUtils.toInteger(rs.getObject(FIELD_APPLICATION_ID));
		this.contractNumber = rs.getString(FIELD_CONTRACT_NUMBER); 
		this.orderNumber = rs.getString(FIELD_ORDER_NUMBER); 
		this.solicitationId = CommonUtils.toInteger(rs.getObject(FIELD_SOLICITATION_ID));
		this.fullName = rs.getString(FIELD_FULL_NAME);
		//this.isManualOrders = rs.getBoolean(FIELD_IS_MANUAL_ORDERS); // CJ-1005
		this.clauseVersionId = CommonUtils.toInteger(rs.getObject(FIELD_CLAUSE_VERSION_ID));
		this.lastQuestionCode = rs.getString(FIELD_LAST_QUESTION_CODE);
		this.createdAt = rs.getTimestamp(FIELD_CREATED_AT);
		this.updatedAt = rs.getTimestamp(FIELD_UPDATED_AT); 
		this.documentDate = CommonUtils.toDate(rs.getObject(FIELD_DOCUMENT_DATE)); // CJ-1312
	}
	
	public static void populateJsonForApi(ObjectNode json, boolean bVer1, ResultSet rs, boolean forDetails) 
			throws SQLException {
		Integer id = rs.getInt(FIELD_DOCUMENT_ID);
		if (bVer1) {
			String sDocStatusCd = rs.getString(FIELD_DOCUMENT_STATUS);
			json.put("id", id);
			json.put("url", DocumentEndpoint.getUrl(bVer1, id));
			json.put("document_type", rs.getString(FIELD_DOCUMENT_TYPE));
			//json.put("is_manual_orders", DocTypeRefRecord.DOC_TYPE_CD_ORDER.equals(rs.getString(FIELD_DOCUMENT_TYPE))); // CJ-320
			json.put("acquisition_title", rs.getString(FIELD_ACQUISITION_TITLE));
			json.put("document_number", rs.getString(FIELD_DOCUMENT_NUMBER));
			//json.put("interview_complete", DocStatusRefRecord.CD_FINALIZED.equals(sDocStatusCd));
			json.put("doc_status_cd", sDocStatusCd);
			json.put("order_number", rs.getString(FIELD_ORDER_NUMBER));
			json.put("contract_number", rs.getString(FIELD_CONTRACT_NUMBER));
			json.put("created_at", CommonUtils.toZulu(rs.getTimestamp(FIELD_CREATED_AT)));
			json.put("updated_at", CommonUtils.toZulu(rs.getTimestamp(FIELD_UPDATED_AT)));
			json.put("user", (String)null);
			json.put(FIELD_CLAUSE_VERSION_ID, CommonUtils.toInteger(rs.getObject(FIELD_CLAUSE_VERSION_ID)));
			json.put(FIELD_SOLICITATION_ID, CommonUtils.toInteger(rs.getObject(FIELD_SOLICITATION_ID)));
		} else {
			json.put(FIELD_DOCUMENT_ID, id);
			json.put("url", DocumentEndpoint.getUrl(bVer1, id));
			json.put(FIELD_DOCUMENT_TYPE, rs.getString(FIELD_DOCUMENT_TYPE));
			json.put(FIELD_ACQUISITION_TITLE, rs.getString(FIELD_ACQUISITION_TITLE));
			json.put(FIELD_DOCUMENT_NUMBER, rs.getString(FIELD_DOCUMENT_NUMBER));
			json.put(FIELD_DOCUMENT_STATUS, rs.getString(FIELD_DOCUMENT_STATUS));
			//json.put(FIELD_APPLICATION_ID, this.applicationId);
			json.put(FIELD_CONTRACT_NUMBER, rs.getString(FIELD_CONTRACT_NUMBER));
			json.put(FIELD_ORDER_NUMBER, rs.getString(FIELD_ORDER_NUMBER));
			json.put(FIELD_FULL_NAME, rs.getString(FIELD_FULL_NAME));
			json.put(FIELD_SOLICITATION_ID, CommonUtils.toInteger(rs.getObject(FIELD_SOLICITATION_ID)));
			json.put("clause_version", ClauseVersionTable.getName(CommonUtils.toInteger(rs.getObject(FIELD_CLAUSE_VERSION_ID))));
			//json.put(FIELD_CLAUSE_VERSION_ID, CommonUtils.toInteger(rs.getObject(FIELD_CLAUSE_VERSION_ID)));
			json.put(FIELD_CREATED_AT, CommonUtils.toZulu(rs.getTimestamp(FIELD_CREATED_AT)));
			json.put(FIELD_UPDATED_AT, CommonUtils.toZulu(rs.getTimestamp(FIELD_UPDATED_AT)));
		}
		json.put(FIELD_DOCUMENT_DATE, CommonUtils.toYearMonthDay(rs.getObject(FIELD_DOCUMENT_DATE)));
	}
	
	public void populateJsonForApi(ObjectNode json, boolean bVer1, Connection conn) throws SQLException {
		if (bVer1) {
			json.put("id", this.id);
			json.put("url", DocumentEndpoint.getUrl(bVer1, this.id));
			json.put("document_type", this.documentType);
			//json.put("is_manual_orders", DocTypeRefRecord.DOC_TYPE_CD_ORDER.equals(this.documentType)); // CJ-320
			json.put("acquisition_title", this.acquisitionTitle);
			json.put("document_number", this.documentNumber);
			//json.put("interview_complete", this.getInterviewComplete());
			json.put("doc_status_cd", this.documentStatus);
			json.put("order_number", this.orderNumber);
			json.put("contract_number", this.contractNumber);
			json.put("created_at", CommonUtils.toZulu(this.createdAt));
			json.put("updated_at", CommonUtils.toZulu(this.updatedAt));
			json.put("user", (String)null);
			json.put(FIELD_CLAUSE_VERSION_ID, this.clauseVersionId);
		} else {
			json.put(FIELD_DOCUMENT_ID, this.id);
			json.put(FIELD_DOCUMENT_TYPE, this.documentType);
			json.put(FIELD_ACQUISITION_TITLE, this.acquisitionTitle);
			json.put(FIELD_DOCUMENT_NUMBER, this.documentNumber);
			json.put(FIELD_DOCUMENT_STATUS, this.documentStatus);
			//json.put(FIELD_APPLICATION_ID, this.applicationId);
			json.put(FIELD_CONTRACT_NUMBER, this.contractNumber);
			json.put(FIELD_ORDER_NUMBER, this.orderNumber);
			json.put(FIELD_FULL_NAME, this.fullName);
			json.put(FIELD_CLAUSE_VERSION_ID, this.clauseVersionId);
			json.put("clause_version", ClauseVersionTable.getName(this.clauseVersionId));
			json.put(FIELD_CREATED_AT, CommonUtils.toZulu(this.createdAt));
			json.put(FIELD_UPDATED_AT, CommonUtils.toZulu(this.updatedAt));
		}
		json.put(FIELD_DOCUMENT_DATE, CommonUtils.toYearMonthDay(this.documentDate)); // CJ-1312
		json.put(FIELD_SOLICITATION_ID, this.solicitationId);

		// CJ-401
		ArrayNode anLinkedAwardDocId = json.arrayNode();
		if (DocTypeRefRecord.DOC_TYPE_CD_SOLICITATION.equals(this.documentType)) {
			PreparedStatement ps2 = null;
			ResultSet rs2 = null;
			try {
				String sSql = "SELECT " + DocumentRecord.FIELD_DOCUMENT_ID
						+ " FROM " + DocumentRecord.TABLE_DOCUMENTS
						+ " WHERE " + DocumentRecord.FIELD_SOLICITATION_ID + " = ?"
						+ " ORDER BY 1";
				ps2 = conn.prepareStatement(sSql);

				ps2.setInt(1, this.id);
				rs2 = ps2.executeQuery();
				while (rs2.next()) {
					Integer linkedAwardId = rs2.getInt(DocumentRecord.FIELD_DOCUMENT_ID);
					anLinkedAwardDocId.add(linkedAwardId);
				}
			}
			finally {
				SqlUtil.closeInstance(rs2, ps2);
			}
		}
		json.put(DocumentRecord.TAG_LINKED_AWARD_DOCUMENT_ID, anLinkedAwardDocId);
	}
	
	@Override
	protected void populateJson(ObjectNode json, boolean forInterview) {
		json.put(FIELD_DOCUMENT_ID, this.id);
		json.put(FIELD_DOCUMENT_TYPE, this.documentType);
		json.put(FIELD_ACQUISITION_TITLE, this.acquisitionTitle);
		json.put(FIELD_DOCUMENT_NUMBER, this.documentNumber);
		json.put(FIELD_DOCUMENT_STATUS, this.documentStatus);
		json.put(FIELD_APPLICATION_ID, this.applicationId);
		json.put(FIELD_CONTRACT_NUMBER, this.contractNumber);
		json.put(FIELD_ORDER_NUMBER, this.orderNumber);
		json.put(FIELD_FULL_NAME, this.fullName);
		json.put(FIELD_CLAUSE_VERSION_ID, this.clauseVersionId);
		json.put("clause_version", ClauseVersionTable.getName(this.clauseVersionId));		
		json.put(FIELD_UPDATED_AT, CommonUtils.toJsonDate(this.updatedAt));
		json.put(FIELD_SOLICITATION_ID, this.solicitationId);
		json.put(TAG_IS_LINKED_AWARD, this.isLinkedAward());
		//json.put(FIELD_IS_MANUAL_ORDERS, this.isManualOrders); // CJ-1005
		if (forInterview) {
			json.put(FIELD_LAST_QUESTION_CODE, this.lastQuestionCode);
			json.put("VersionName", ClauseVersionTable.getName(this.clauseVersionId));
			json.put(forInterview ? DocumentAnswerRecord.TABLE_DOCUMENT_ANSWERS : DocumentAnswerRecord.TABLE_DOCUMENT_ANSWERS.toLowerCase(),
					(this.documentAnswerTable != null ? this.documentAnswerTable.toJson(null, true) : null));
			json.put(forInterview ? DocumentFillInsRecord.TABLE_DOCUMENT_FILL_INS : DocumentFillInsRecord.TABLE_DOCUMENT_FILL_INS.toLowerCase(),
					(this.documentFillInsTable != null ? this.documentFillInsTable.toJson(null, true) : null));
			json.put(forInterview ? DocumentClauseRecord.TABLE_DOCUMENT_CLAUSES : DocumentClauseRecord.TABLE_DOCUMENT_CLAUSES.toLowerCase(),
					(this.documentClauseTable != null ? this.documentClauseTable.toJson(null, true) : null));
		} else {
			json.put(FIELD_USER_ID, this.userId);
			json.put(FIELD_CREATED_AT, CommonUtils.toJsonDate(this.createdAt));
		}
	};
	
	public ObjectNode getJsonForQuestionApi(boolean bForVer1, QuestionTable questionTable, Connection conn) {
		ObjectMapper mapper = new ObjectMapper();
		ObjectNode result = mapper.createObjectNode();
		result.put(FIELD_DOCUMENT_ID, this.id);
		result.put(FIELD_CLAUSE_VERSION_ID, this.clauseVersionId);
		result.put(FIELD_CREATED_AT, CommonUtils.toZulu(this.createdAt));
		result.put(FIELD_UPDATED_AT, CommonUtils.toZulu(this.updatedAt));
		if (this.documentAnswerTable != null) {
			this.documentAnswerTable.populateJsonForApi(result, bForVer1, questionTable);
		} else {
			result.put(DocumentAnswerRecord.TABLE_DOCUMENT_ANSWERS.toLowerCase(),
					(this.documentAnswerTable != null ? this.documentAnswerTable.toJson(null, bForVer1) : null));
		}
		return result;
	}
	
	protected void fromJson(JsonNode pJsonNode) {
		this.id = CommonUtils.asInteger(pJsonNode, FIELD_DOCUMENT_ID);
		this.documentType = CommonUtils.asString(pJsonNode, FIELD_DOCUMENT_TYPE);
		this.acquisitionTitle = CommonUtils.asString(pJsonNode, FIELD_ACQUISITION_TITLE); 
		this.documentNumber = CommonUtils.asString(pJsonNode, FIELD_DOCUMENT_NUMBER); 
		//this.userId = CommonUtils.asInteger(pJsonNode, FIELD_USER_ID);
		this.documentStatus = CommonUtils.asString(pJsonNode, FIELD_DOCUMENT_STATUS); 
		this.applicationId = CommonUtils.asInteger(pJsonNode, FIELD_APPLICATION_ID);
		this.contractNumber = CommonUtils.asString(pJsonNode, FIELD_CONTRACT_NUMBER); 
		this.orderNumber = CommonUtils.asString(pJsonNode, FIELD_ORDER_NUMBER); 
		//this.solicitationId = CommonUtils.asInteger(pJsonNode, FIELD_SOLICITATION_ID);
		this.fullName = CommonUtils.asString(pJsonNode, FIELD_FULL_NAME); 
		//this.isManualOrders = CommonUtils.asBoolean(pJsonNode, FIELD_IS_MANUAL_ORDERS); // CJ-1005
		this.clauseVersionId = CommonUtils.asInteger(pJsonNode, FIELD_CLAUSE_VERSION_ID);
		this.lastQuestionCode = CommonUtils.asString(pJsonNode, FIELD_LAST_QUESTION_CODE);
		
		ArrayNode arrayNode = (ArrayNode)pJsonNode.get(DocumentAnswerRecord.TABLE_DOCUMENT_ANSWERS);
		this.documentAnswerTable = new DocumentAnswerTable();
		this.documentAnswerTable.fromJson(arrayNode);

		arrayNode = (ArrayNode)pJsonNode.get(DocumentClauseRecord.TABLE_DOCUMENT_CLAUSES);
		this.documentClauseTable = new DocumentClauseTable();
		this.documentClauseTable.fromJson(arrayNode);

		arrayNode = (ArrayNode)pJsonNode.get(DocumentFillInsRecord.TABLE_DOCUMENT_FILL_INS);
		this.documentFillInsTable = new DocumentFillInsTable();
		this.documentFillInsTable.fromJson(arrayNode);
	}
	
	public void prepareForClsVersionUpgrade(DocumentRecord oTarget, Integer newClauseVersionId, AuditEventRecord auditEventRecord, Connection conn) throws SQLException {
		oTarget.id = this.id;
		oTarget.documentType = this.documentType; 
		oTarget.acquisitionTitle = this.acquisitionTitle; 
		oTarget.documentNumber = this.documentNumber; 
		oTarget.userId = this.userId; 
		oTarget.documentStatus = this.documentStatus; 
		oTarget.createdAt = this.createdAt; 
		oTarget.updatedAt = this.updatedAt; 
		oTarget.applicationId = this.applicationId; 
		oTarget.contractNumber = this.contractNumber; 
		oTarget.orderNumber = this.orderNumber; 
		oTarget.solicitationId = this.solicitationId; 
		oTarget.fullName = this.fullName;
		oTarget.isManualOrders = this.isManualOrders;
		oTarget.clauseVersionId = newClauseVersionId; // this.clauseVersionId;
		oTarget.lastQuestionCode = null; // CJ-1169 changed from this.lastQuestionCode;

		oTarget.documentAnswerTable = new DocumentAnswerTable();
		oTarget.documentFillInsTable = new DocumentFillInsTable();
		oTarget.documentClauseTable = new DocumentClauseTable();
		
		String sSql = "UPDATE " + TABLE_DOCUMENTS
				+ " SET " + FIELD_CLAUSE_VERSION_ID + " = ?"
				+ ", " + FIELD_LAST_QUESTION_CODE + " = NULL"
				+ ", " + FIELD_DOCUMENT_STATUS + " = '" + DocStatusRefRecord.CD_IN_PROGRESS + "'"
				+ " WHERE " + FIELD_DOCUMENT_ID + " = ?";
		PreparedStatement psUpdate = null;
		try {
			psUpdate = conn.prepareStatement(sSql);
			psUpdate.setInt(1, newClauseVersionId);
			psUpdate.setInt(2, this.id);
			psUpdate.execute();
			auditEventRecord.addAttribute(FIELD_CLAUSE_VERSION_ID, newClauseVersionId);
		} finally {
			SqlUtil.closeInstance(psUpdate);
		}
	}

	protected void save(DocumentRecord postDocument, QuestionTable oQuestionTable, 
			ClauseTable oClauseTable, UserSession oUserSession, Connection conn) 
					throws Exception {
		//Connection conn = null;
		boolean bCreateConnection = (conn == null); 
		try {
			if (bCreateConnection)
				conn = ClsConfig.getDbConnection();
			
			String sAuditAction = AuditEventRecord.ACTION_DOCUMENT_UPDATED;
			if ("F".equals(postDocument.getDocumentStatus()))
				sAuditAction = AuditEventRecord.ACTION_DOCUMENT_FINALIZED;
			
			AuditEventRecord auditEventRecord = new AuditEventRecord(sAuditAction, oUserSession.getUserId(), oUserSession.getUserId());
			auditEventRecord.addAttribute (AuditEventRecord.FIELD_APPLICATION_ID, oUserSession.getApplicationId());
			
			SqlUpdate sqlUpdate = new SqlUpdate(null, TABLE_DOCUMENTS);
			sqlUpdate.addInteger(FIELD_DOCUMENT_ID, this.id, auditEventRecord);
			if (CommonUtils.isNotSame(this.acquisitionTitle, postDocument.acquisitionTitle))
				sqlUpdate.addString(FIELD_ACQUISITION_TITLE, postDocument.acquisitionTitle, auditEventRecord);
			if (CommonUtils.isNotSame(this.documentNumber, postDocument.documentNumber))
				sqlUpdate.addString(FIELD_DOCUMENT_NUMBER, postDocument.documentNumber, auditEventRecord);
			if (CommonUtils.isNotSame(this.documentStatus, postDocument.documentStatus))
				sqlUpdate.addString(FIELD_DOCUMENT_STATUS, postDocument.documentStatus, auditEventRecord);
			if (CommonUtils.isNotSame(this.contractNumber, postDocument.contractNumber))
				sqlUpdate.addString(FIELD_CONTRACT_NUMBER, postDocument.contractNumber, auditEventRecord);
			if (CommonUtils.isNotSame(this.orderNumber, postDocument.orderNumber))
				sqlUpdate.addString(FIELD_ORDER_NUMBER, postDocument.orderNumber, auditEventRecord);
			if (CommonUtils.isNotSame(this.fullName, postDocument.fullName))
				sqlUpdate.addString(FIELD_FULL_NAME, postDocument.fullName, auditEventRecord);

			//if (CommonUtils.isNotSame(this.isManualOrders, postDocument.isManualOrders)) // CJ-1005
			//	sqlUpdate.addBoolean(FIELD_IS_MANUAL_ORDERS, postDocument.isManualOrders, auditEventRecord);
			
			if (CommonUtils.isNotSame(this.clauseVersionId, postDocument.clauseVersionId))
				sqlUpdate.addString(FIELD_CLAUSE_VERSION_ID, postDocument.clauseVersionId, auditEventRecord);
			
			if (CommonUtils.isNotSame(this.lastQuestionCode, postDocument.lastQuestionCode))
				sqlUpdate.addString(FIELD_LAST_QUESTION_CODE, postDocument.lastQuestionCode);
				//sqlUpdate.addString(FIELD_LAST_QUESTION_CODE, postDocument.lastQuestionCode, auditEventRecord);

			this.updatedAt = CommonUtils.getNow();
			sqlUpdate.addDate(FIELD_UPDATED_AT, this.updatedAt, auditEventRecord);
			
			sqlUpdate.addCriteria(FIELD_DOCUMENT_ID, this.id, java.sql.Types.INTEGER);
			sqlUpdate.execute(conn);
			
			if (this.documentAnswerTable == null)
				this.documentAnswerTable = new DocumentAnswerTable();
			this.documentAnswerTable.save(this.id.intValue(), postDocument.documentAnswerTable, oQuestionTable, conn, auditEventRecord);
			
			if (this.documentClauseTable == null)
				this.documentClauseTable = new DocumentClauseTable();
			this.documentClauseTable.save(this.id.intValue(), postDocument.documentClauseTable, oClauseTable, conn, auditEventRecord);
			
			if (this.documentFillInsTable == null)
				this.documentFillInsTable = new DocumentFillInsTable();
			this.documentFillInsTable.save(this.id.intValue(), postDocument.documentFillInsTable, oClauseTable, 
					postDocument.documentClauseTable, conn, auditEventRecord); // CJ-745

			DocumentTable.get(this, this.id, conn);
			
			try {
				auditEventRecord.saveNew(conn);
			}
			catch (Exception oIgnore) {
				oIgnore.printStackTrace();
			}
		}
		finally {
			if (bCreateConnection)
				SqlUtil.closeInstance(conn);
		}
	}
	
	public void saveUpdatedByApi(AuditEventRecord auditEventRecord, Connection conn) throws Exception {
		SqlUpdate sqlUpdate = new SqlUpdate(null, TABLE_DOCUMENTS);
		this.updatedAt = CommonUtils.getNow();
		sqlUpdate.addDate(FIELD_UPDATED_AT, this.updatedAt); // , auditEventRecord
		
		sqlUpdate.addCriteria(FIELD_DOCUMENT_ID, this.id, java.sql.Types.INTEGER);
		sqlUpdate.execute(conn);
		auditEventRecord.saveNew(conn);
	}

	public boolean deleteDocument(UserSession oUserSession, Connection conn) {
		boolean result = false;
		boolean bCreateConnection = (conn == null);
		PreparedStatement ps1 = null;
		String sSql = null;
		try {
			int docId = this.id.intValue();
			if (bCreateConnection)
				conn = ClsConfig.getDbConnection();
			AuditEventTable.deleteDocument(docId, conn);
			DocumentAnswerTable.deleteDocument(docId, conn);
			DocumentFillInsTable.deleteDocument(docId, conn);
			DocumentClauseTable.deleteDocument(docId, conn);
			
			sSql = "DELETE FROM " + DocumentRecord.TABLE_DOCUMENTS
					+ " WHERE " + DocumentRecord.FIELD_DOCUMENT_ID + " = ?";
			ps1 = conn.prepareStatement(sSql);
			ps1.setInt(1, docId);
			ps1.execute();

			AuditEventRecord auditEventRecord = new AuditEventRecord(AuditEventRecord.ACTION_DOCUMENT_DESTROYED, null, (oUserSession == null) ? null : oUserSession.getUserId());
			auditEventRecord.addAttribute(FIELD_DOCUMENT_ID, this.id);
			auditEventRecord.addAttribute(FIELD_DOCUMENT_TYPE, this.documentType);
			auditEventRecord.addAttribute(FIELD_ACQUISITION_TITLE, this.acquisitionTitle);
			auditEventRecord.addAttribute(FIELD_DOCUMENT_NUMBER, this.documentNumber);
			auditEventRecord.addAttribute(FIELD_USER_ID, this.userId);
			auditEventRecord.addAttribute(FIELD_DOCUMENT_STATUS, this.documentStatus);
			auditEventRecord.addAttribute(FIELD_APPLICATION_ID, this.applicationId);
			auditEventRecord.addAttribute(FIELD_CONTRACT_NUMBER, this.contractNumber);
			auditEventRecord.addAttribute(FIELD_ORDER_NUMBER, this.orderNumber);
			auditEventRecord.addAttribute(FIELD_FULL_NAME, this.fullName);
			//auditEventRecord.addAttribute(FIELD_IS_MANUAL_ORDERS, this.isManualOrders); // CJ-1005
			auditEventRecord.addAttribute(FIELD_CLAUSE_VERSION_ID, this.clauseVersionId);
			auditEventRecord.addAttribute(FIELD_SOLICITATION_ID, this.solicitationId);
			auditEventRecord.addAttribute(FIELD_CREATED_AT, this.createdAt);
			auditEventRecord.addAttribute(FIELD_UPDATED_AT, this.updatedAt);
			auditEventRecord.setReferenceDocumentNumber(this.documentNumber);
			auditEventRecord.saveNew(conn);
			
			result = true;
		}
		catch (Exception oError) {
			oError.printStackTrace();
			System.out.println("DocumentRecord.deleteDocument() for " + this.id + ") failed due to : " + oError.getMessage() + "\n" + sSql);
		}
		finally {
			SqlUtil.closeInstance(ps1);
			if (bCreateConnection)
				SqlUtil.closeInstance(conn);
		}
		return result;
	}
	
	public boolean copyDocument(Integer origId, Integer newId, Integer origClauseVersionId, Integer newClauseVersionId, Connection conn) {
		boolean result = false;
		boolean bCreateConnection = (conn == null);
		PreparedStatement ps1 = null;
		String sSql = null;
		try {
			if (bCreateConnection)
				conn = ClsConfig.getDbConnection();
					
			// Copy question answers
			sSql = "INSERT INTO " + DocumentAnswerRecord.TABLE_DOCUMENT_ANSWERS
					+ " (" + DocumentAnswerRecord.FIELD_DOCUMENT_ID 
					+	", "  + DocumentAnswerRecord.FIELD_QUESTION_ID 
					+	", " + DocumentAnswerRecord.FIELD_QUESTION_ANSWER 
					+	", " + DocumentAnswerRecord.FIELD_CREATED_AT
					+	", " + DocumentAnswerRecord.FIELD_UPDATED_AT + ") "
					+ " SELECT " + newId.toString() 
					+	", " + DocumentAnswerRecord.FIELD_QUESTION_ID 
					+	", " + DocumentAnswerRecord.FIELD_QUESTION_ANSWER 
					+	", " + DocumentAnswerRecord.FIELD_CREATED_AT
					+	", " + DocumentAnswerRecord.FIELD_UPDATED_AT
					+ 	" FROM  " + DocumentAnswerRecord.TABLE_DOCUMENT_ANSWERS
					+ " WHERE " + DocumentRecord.FIELD_DOCUMENT_ID + " = ? "
					+ " AND "  + DocumentAnswerRecord.FIELD_QUESTION_ID + " NOT IN "
					+ 	" (SELECT "  + DocumentAnswerRecord.FIELD_QUESTION_ID 
					+ 	" FROM " + DocumentAnswerRecord.TABLE_DOCUMENT_ANSWERS 
					+ 	" WHERE " + DocumentRecord.FIELD_DOCUMENT_ID + " = ? )";
					
			
			ps1 = conn.prepareStatement(sSql);
			ps1.setInt(1, origId);
			ps1.setInt(2, newId);
			ps1.execute();
			ps1.close();

			// CJ-1237, Don't copy the Fillins and editables answers, per Larry
			/*
			// Copy fillin answers
			sSql = "INSERT INTO " + DocumentFillInsRecord.TABLE_DOCUMENT_FILL_INS
					+ " (" + DocumentFillInsRecord.FIELD_DOCUMENT_ID 
					+	", "  + DocumentFillInsRecord.FIELD_CLAUSE_FILL_IN_ID 
					+	", " + DocumentFillInsRecord.FIELD_DOCUMENT_FILL_IN_ANSWER 
					+	", " + DocumentFillInsRecord.FIELD_CREATED_AT
					+	", " + DocumentFillInsRecord.FIELD_UPDATED_AT
					+	", " + DocumentFillInsRecord.FIELD_FULL_TEXT_MODIFIED + ") "
					+ " SELECT " + newId.toString() 
					+	", "  + DocumentFillInsRecord.FIELD_CLAUSE_FILL_IN_ID 
					+	", " + DocumentFillInsRecord.FIELD_DOCUMENT_FILL_IN_ANSWER 
					+	", " + DocumentFillInsRecord.FIELD_CREATED_AT
					+	", " + DocumentFillInsRecord.FIELD_UPDATED_AT + ", 0"
					+ 	" FROM  " + DocumentFillInsRecord.TABLE_DOCUMENT_FILL_INS
					+ " WHERE " + DocumentRecord.FIELD_DOCUMENT_ID + " = ?";
					
			
			ps1 = conn.prepareStatement(sSql);
			ps1.setInt(1, origId);
			ps1.execute();
			ps1.close();
			*/
			
			// copy clauses
			sSql = "INSERT INTO " + DocumentClauseRecord.TABLE_DOCUMENT_CLAUSES
					+ " (" + DocumentClauseRecord.FIELD_DOCUMENT_ID 
					+	", "  + DocumentClauseRecord.FIELD_CLAUSE_ID 
					+	", " + DocumentClauseRecord.FIELD_IS_FILLIN_COMPLETED 
					+	", " + DocumentClauseRecord.FIELD_CREATED_AT
					+	", " + DocumentClauseRecord.FIELD_UPDATED_AT + ") "
					+ " SELECT " + newId.toString() 
					+	", "  + DocumentClauseRecord.FIELD_CLAUSE_ID 
					+	", 0 " // + DocumentClauseRecord.FIELD_IS_FILLIN_COMPLETED 
					+	", " + DocumentClauseRecord.FIELD_CREATED_AT
					+	", " + DocumentClauseRecord.FIELD_UPDATED_AT
					+ 	" FROM  " + DocumentClauseRecord.TABLE_DOCUMENT_CLAUSES
					+ " WHERE " + DocumentRecord.FIELD_DOCUMENT_ID + " = ?"
					+ " AND "  + DocumentClauseRecord.FIELD_CLAUSE_ID + " NOT IN "
					+ 	" (SELECT "  + DocumentClauseRecord.FIELD_CLAUSE_ID 
					+ 	" FROM " + DocumentClauseRecord.TABLE_DOCUMENT_CLAUSES 
					+ 	" WHERE " + DocumentRecord.FIELD_DOCUMENT_ID + " = ? )";
			
			ps1 = conn.prepareStatement(sSql);
			ps1.setInt(1, origId);
			ps1.setInt(2, newId);
			ps1.execute();
			ps1.close();
			
			// Set the version equal to the original document. This match the answers.
			sSql = "UPDATE " + DocumentRecord.TABLE_DOCUMENTS
					+ " SET " + DocumentRecord.FIELD_CLAUSE_VERSION_ID + " = ? "
					+ " WHERE " + DocumentRecord.FIELD_DOCUMENT_ID +  " = ? ";
			ps1 = conn.prepareStatement(sSql);
			ps1.setInt(1, origClauseVersionId);
			ps1.setInt(2, newId);
			ps1.execute();
			ps1.close();
			
			if (origClauseVersionId != newClauseVersionId) {
				
			}
			
			result = true;
		}
		catch (Exception oError) {
			oError.printStackTrace();
			System.out.println("DocumentRecord.copyDocument() for " + this.id + ") failed due to : " + oError.getMessage() + "\n" + sSql);
		}
		finally {
			SqlUtil.closeInstance(ps1);
			if (bCreateConnection)
				SqlUtil.closeInstance(conn);
		}
		return result;
	}
	
	public boolean changeStatusToIncomplete(Connection conn) throws SQLException { // CJ-1023
		if (!DocStatusRefRecord.CD_IN_PROGRESS.equals(this.documentStatus)) {
			String sSql = "UPDATE " + TABLE_DOCUMENTS
					+ " SET " + FIELD_DOCUMENT_STATUS + " = ?"
					+ ", " + FIELD_UPDATED_AT + " = ?" 
					+ " WHERE " + FIELD_DOCUMENT_ID + " = ?";
			PreparedStatement statement = null;
			try {
				Date dNow = SqlUtil.getNow();

				statement = conn.prepareStatement(sSql);
				statement.setString(1, DocStatusRefRecord.CD_IN_PROGRESS);
				statement.setTimestamp(2, new Timestamp(dNow.getTime()));
				statement.setInt(3, this.id);
				statement.execute();
				
				this.documentStatus = DocStatusRefRecord.CD_IN_PROGRESS;
				this.updatedAt = dNow;
				return true;
			} finally {
				SqlUtil.closeInstance(statement);
			}
		} else
			return false;
	}
	
	// ----------------------------------------------------------------------
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getDocumentType() {
		return documentType;
	}
	public String getDocumentTypeName() {
		return ("A".equals(this.documentType) ? "Award" : "Solicitation");
	}
	public void setDocumentType(String documentType) {
		this.documentType = documentType;
	}
	public String getAcquisitionTitle() {
		return acquisitionTitle;
	}
	public void setAcquisitionTitle(String acquisitionTitle) {
		this.acquisitionTitle = acquisitionTitle;
	}
	public String getDocumentNumber() {
		return documentNumber;
	}
	public void setDocumentNumber(String documentNumber) {
		this.documentNumber = documentNumber;
	}
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	public String getDocumentStatus() {
		return documentStatus;
	}
	public void setDocumentStatus(String documentStatus) {
		this.documentStatus = documentStatus;
	}
	
	public boolean getInterviewComplete() {
		return DocStatusRefRecord.CD_FINALIZED.equals(this.documentStatus);
	}
	
	public Date getCreatedAt() {
		return createdAt;
	}
	public String getCreatedAtString() {
		return CommonUtils.toString(this.createdAt);
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}
	
	public Date getUpdatedAt() {
		return updatedAt;
	}
	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	public Integer getApplicationId() {
		return applicationId;
	}
	public void setApplicationId(Integer applicationId) {
		this.applicationId = applicationId;
	}
	
	public String getContractNumber() {
		return contractNumber;
	}
	public void setContractNumber(String contractNumber) {
		this.contractNumber = contractNumber;
	}
	
	public String getOrderNumber() {
		return orderNumber;
	}
	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}
	
	public Integer getSolicitationId() {
		return solicitationId;
	}
	public void setSolicitationId(Integer solicitationId) {
		this.solicitationId = solicitationId;
	}
	
	public boolean isLinkedAward() {
		if (this.solicitationId == null)
			return false;
		return (DocTypeRefRecord.DOC_TYPE_CD_AWARD.equals(this.documentType));
	}
	
	public String getFullName() {
		return fullName;
	}
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	
	public Boolean getIsManualOrders() {
		return isManualOrders;
	}
	public void setIsManualOrders(Boolean isManualOrders) {
		this.isManualOrders = isManualOrders;
	}

	public Integer getClauseVersionId() {
		return clauseVersionId;
	}
	public void setClauseVersionId(Integer clauseVersionId) {
		this.clauseVersionId = clauseVersionId;
	}

	public DocumentAnswerTable getAnswers() {
		return documentAnswerTable;
	}

	public DocumentFillInsTable getFillIns() {
		return documentFillInsTable;
	}

	public DocumentClauseTable getDocumentClauseTable() {
		return documentClauseTable;
	}
	public void setDocumentClauseTable(DocumentClauseTable documentClauseTable) {
		this.documentClauseTable = documentClauseTable;
	}

	public Date getDocumentDate() { // CJ-1312
		return documentDate;
	}
	public void setDocumentDate(Date documentDate) { // CJ-1312
		this.documentDate = documentDate;
	}

}
