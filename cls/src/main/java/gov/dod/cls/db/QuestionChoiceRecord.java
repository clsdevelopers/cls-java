package gov.dod.cls.db;

import gov.dod.cls.model.JsonAble;
import gov.dod.cls.utils.CommonUtils;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import org.codehaus.jackson.node.ObjectNode;

public class QuestionChoiceRecord extends JsonAble {

	public static final String TABLE_QUESTION_CHOICES = "Question_Choices";
	
	public static final String FIELD_QUESTION_CHOCE_ID= "question_choce_id";
	public static final String FIELD_QUESTION_ID = "question_id";
	public static final String FIELD_CHOICE_TEXT = "choice_text";
	public static final String FIELD_PROMPT_AFTER_SELECTION = "prompt_after_selection";
	public static final String FIELD_CREATED_AT = "created_at";
	public static final String FIELD_UPDATED_AT = "updated_at";
	
	public static final String SQL_SELECT
		= "SELECT " + FIELD_QUESTION_CHOCE_ID
		+ ", " + FIELD_QUESTION_ID
		+ ", " + FIELD_CHOICE_TEXT
		+ ", " + FIELD_PROMPT_AFTER_SELECTION
		+ ", " + FIELD_CREATED_AT
		+ ", " + FIELD_UPDATED_AT
		+ " FROM " + TABLE_QUESTION_CHOICES;

	public static final String SQL_ORDER_BY = " ORDER BY " + FIELD_QUESTION_ID + ", " + FIELD_QUESTION_CHOCE_ID;

	// ----------------------------------------------------
	private Integer id; 
	private Integer questionId; 
	private String choiceText; 
	private String promptAfterSelection;
	private Date createdAt; 
	private Date updatedAt;
	
	private QuestionChoicePrescriptionTable questionChoicePrescriptionTable = null;
	
	public void read(ResultSet rs) throws SQLException {
		this.id = rs.getInt(FIELD_QUESTION_CHOCE_ID); 
		this.questionId = CommonUtils.toInteger(rs.getObject(FIELD_QUESTION_ID));
		this.choiceText = rs.getString(FIELD_CHOICE_TEXT); 
		this.promptAfterSelection = rs.getString(FIELD_PROMPT_AFTER_SELECTION);
		this.createdAt = CommonUtils.toDateTime(rs, FIELD_CREATED_AT);
		this.updatedAt = CommonUtils.toDateTime(rs, FIELD_UPDATED_AT);
	}
	
	@Override
	protected void populateJson(ObjectNode json, boolean forInterview) {
		json.put(FIELD_QUESTION_CHOCE_ID, this.id);
		json.put(FIELD_CHOICE_TEXT, this.choiceText);
		json.put(FIELD_PROMPT_AFTER_SELECTION, this.promptAfterSelection);
		if (forInterview) {
			json.put("prescriptionIds", (questionChoicePrescriptionTable == null) ? null : questionChoicePrescriptionTable.toJson(null, forInterview));
		} else {
			json.put(FIELD_QUESTION_ID, this.questionId);
			json.put(FIELD_CREATED_AT, CommonUtils.toJsonDate(this.createdAt));
			json.put(FIELD_UPDATED_AT, CommonUtils.toJsonDate(this.updatedAt));
		}
	}

	// -------------------------------------------------------------------
	public Integer getQuestionId() {
		return questionId;
	}
	public void setQuestionId(Integer questionId) {
		this.questionId = questionId;
	}

	public String getChoiceText() {
		return choiceText;
	}
	public void setChoiceText(String choiceText) {
		this.choiceText = choiceText;
	}

	public String getPromptAfterSelection() {
		return promptAfterSelection;
	}
	public void setPromptAfterSelection(String promptAfterSelection) {
		this.promptAfterSelection = promptAfterSelection;
	}

	public Date getCreatedAt() {
		return createdAt;
	}
	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public Date getUpdatedAt() {
		return updatedAt;
	}
	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	public Integer getId() {
		return id;
	}

	public QuestionChoicePrescriptionTable getQuestionChoicePrescriptionTable() {
		return questionChoicePrescriptionTable;
	}

	public void setQuestionChoicePrescriptionTable(
			QuestionChoicePrescriptionTable questionChoicePrescriptionTable) {
		this.questionChoicePrescriptionTable = questionChoicePrescriptionTable;
	}


}
