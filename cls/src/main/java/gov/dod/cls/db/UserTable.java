package gov.dod.cls.db;

import gov.dod.cls.PageApi;
import gov.dod.cls.bean.Oauth;
import gov.dod.cls.bean.session.UserSession;
import gov.dod.cls.config.ClsConfig;
import gov.dod.cls.db.utils.UserRoles;
import gov.dod.cls.exception.InvalidDataException;
import gov.dod.cls.mail.SendMail;
import gov.dod.cls.model.JsonAble;
import gov.dod.cls.reference.ClsPages;
import gov.dod.cls.utils.CommonUtils;
import gov.dod.cls.utils.Pagination;
import gov.dod.cls.utils.sql.SqlUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.codehaus.jackson.node.ObjectNode;
import org.springframework.util.StringUtils;

public class UserTable extends ArrayList<UserRecord> {

	private static final long serialVersionUID = -6972826749737467386L;

	public static final String PREFIX_DO_USER = "user-";
	
	public static final String DO_USER_LIST = PREFIX_DO_USER + "list";
	public static final String DO_USER_DETAILS_GET = PREFIX_DO_USER + "detail-get";
	public static final String DO_USER_DETAILS_EDIT = PREFIX_DO_USER + "detail-edit";
	public static final String DO_USER_DETAILS_NEW = PREFIX_DO_USER + "detail-new";
	public static final String DO_USER_REMOVE = PREFIX_DO_USER + "remove";
	public static final String DO_USER_RESET_PASSWORD = PREFIX_DO_USER + "reset-password";
	public static final String DO_USER_DETAILS_RESET_PASSWORD = PREFIX_DO_USER + "detail-resetpassword";
	public static final String DO_USER_FORGET_PASSWORD = PREFIX_DO_USER + "forgot-password";
	
	public static void processRequest(String sDo, UserSession oUserSession, 
			HttpServletRequest req, HttpServletResponse resp) {
		switch (sDo) {
		case UserTable.DO_USER_RESET_PASSWORD:
			UserTable.resetUserPassword(oUserSession, req, resp, true);
			return;	
		case UserTable.DO_USER_DETAILS_RESET_PASSWORD:
			UserTable.getUserForResetPassword(req, resp);
			return;	
		case UserTable.DO_USER_FORGET_PASSWORD:
			UserTable.resetUserPassword(null, req, resp, true);
			return;	
		}

		if (!(oUserSession.isSuperUser() || oUserSession.isSubsuperUser())) { // CJ-680
			return;
		}
		
		switch (sDo) {
		case UserTable.DO_USER_LIST:
			UserTable.processList(sDo, oUserSession, req, resp);
			return;
		case UserTable.DO_USER_DETAILS_GET:
			UserTable.getUserDetails(sDo, oUserSession, req, resp);
			return;
		case UserTable.DO_USER_DETAILS_EDIT:
			UserTable.saveUserDetails(sDo, oUserSession, req, resp);
			return;
		case UserTable.DO_USER_DETAILS_NEW:
			UserTable.saveUserDetails(sDo, oUserSession, req, resp);
			return;
		case UserTable.DO_USER_REMOVE:
			UserTable.deactiveUser(oUserSession, req, resp);
			return;
		}
	}

	// ----------------------------------------------------------
	public static UserRecord login(String email, String password, String currentLogonIp) {
		
		if (CommonUtils.isEmpty(email))
			return null;
		UserRecord usersRecord = UserRecord.getRecord(null, email, null);
		if (usersRecord != null) {
			if ((!usersRecord.isPasswordSame(password)) || (!usersRecord.isActiveUser())) {  
				usersRecord = null;
			} else
				usersRecord.updateCurrentLogin(currentLogonIp);
		}
		return usersRecord;
		
		/*		
		if (CommonUtils.isEmpty(email))
			return null;
		UserRecord usersRecord = UserRecord.getRecord(null, email);
		if (usersRecord != null) {
			if (!usersRecord.isPasswordSame(password)) {
				usersRecord = null;
			} else
				usersRecord.updateCurrentLogin(currentLogonIp);
		}
		return usersRecord;
		*/
	}
	
	// CJ-1373, Add 'Email account not registered'
	// ----------------------------------------------------------
	public static boolean validateEmail(String email) {
		
		if (CommonUtils.isEmpty(email))
			return false;
		UserRecord usersRecord = UserRecord.getRecord(null, email, null);
		if (usersRecord == null) 
			return false;
		
		return true;
	}
	
	public static UserRecord cacLogin(String cacHash, String currentLogonIp) {
		
		if (CommonUtils.isEmpty(cacHash))
			return null;
		UserRecord usersRecord = UserRecord.getRecordByCac(null, cacHash);
		if (usersRecord != null) {
			if (!usersRecord.isActiveUser()) { // CJ-732
				usersRecord = null;
			} else
				usersRecord.updateCurrentLogin(currentLogonIp);
		}
		return usersRecord;
		
		/*		
		if (CommonUtils.isEmpty(email))
			return null;
		UserRecord usersRecord = UserRecord.getRecord(null, email);
		if (usersRecord != null) {
			if (!usersRecord.isPasswordSame(password)) {
				usersRecord = null;
			} else
				usersRecord.updateCurrentLogin(currentLogonIp);
		}
		return usersRecord;
		*/
	}
	
	private static void processList(String sDo, UserSession oUserSession, 
			HttpServletRequest req, HttpServletResponse resp) {
		String query = req.getParameter("query");
		String role_id = req.getParameter("role_id");
		String sort_field = req.getParameter("sort_field");
		String sort_order = req.getParameter("sort_order");
		Integer iRoleId = null;
		
		Pagination pagination = new Pagination(req);
		pagination.addDataSource("Users");
		
		String sSelect
			= "SELECT A." + UserRecord.FIELD_USER_ID
			+ ", A." + UserRecord.FIELD_EMAIL
			+ ", A." + UserRecord.FIELD_FIRST_NAME
			+ ", A." + UserRecord.FIELD_LAST_NAME
			+ ", A." + UserRecord.FIELD_IS_ACTIVE      
			+ ", B." + OrgRecord.FIELD_ORG_NAME + " AS OrgName"
			+ ", C." + DeptRecord.FIELD_DEPT_NAME;
		String sFromWhere =
			" FROM " + UserRecord.TABLE_USERS + " A"
			+ " LEFT OUTER JOIN " + OrgRecord.TABLE_ORGS + " B"
			+ " ON (B." + OrgRecord.FIELD_ORG_ID + "=A." + UserRecord.FIELD_ORG_ID + ")"
		    + " INNER JOIN " + DeptRecord.TABLE_DEPTS + " C ON (C." + DeptRecord.FIELD_DEPT_ID + "=B." + OrgRecord.FIELD_DEPT_ID + ")";
		String operator = " WHERE ";

		if (!oUserSession.isSuperUser()) { // CJ-680
			String sOrgFilter;
			if (oUserSession.isSubsuperUser()) {
				sOrgFilter = "A." + UserRecord.FIELD_ORG_ID + " = " + oUserSession.getOrgId() + " ";
			} else {
				sOrgFilter = "A." + UserRecord.FIELD_USER_ID + " = " + oUserSession.getUserId() + " ";
			}
			sFromWhere += operator + sOrgFilter; 
			operator = " AND ";
		}
		
		if (CommonUtils.isNotEmpty(query)) {
			pagination.addDataSource("query=" + query);
			sFromWhere += operator
				+ "(LOWER(A." + UserRecord.FIELD_FIRST_NAME + ") LIKE ?"
				+ " OR LOWER(A." + UserRecord.FIELD_LAST_NAME + ") LIKE ?"
				+ " OR LOWER(A." + UserRecord.FIELD_EMAIL + ") LIKE ?"
				+ " OR LOWER(B." + OrgRecord.FIELD_ORG_NAME + ") LIKE ?)";
			operator = " AND ";
			query = "%" + query.toLowerCase() + "%";
		}
		if (CommonUtils.isNotEmpty(role_id))
			iRoleId = CommonUtils.toInteger(role_id);
		if (iRoleId != null) {
			pagination.addDataSource("role_id=" + role_id);
			sFromWhere += operator + "EXISTS ("
					+ "SELECT " + UserRoleRecord.TABLE_USER_ROLES + "." + UserRoleRecord.FIELD_USER_ID
					+ " FROM " + UserRoleRecord.TABLE_USER_ROLES
					+ " WHERE " + UserRoleRecord.TABLE_USER_ROLES + "." + UserRoleRecord.FIELD_USER_ID
					+ " = A." + UserRecord.FIELD_USER_ID
					+ " AND " + UserRoleRecord.TABLE_USER_ROLES + "." + UserRoleRecord.FIELD_ROLE_ID + "=?"
					+ ")";
		}
		
		String sAsc = ("desc".equals(sort_order) ? " DESC" : "");
		String sOrderBy = " ORDER BY " + ("first_name".equals(sort_field)
				? " 2" + sAsc + ", 1" + sAsc
				: " 1" + sAsc + ", 2" + sAsc);
		
		String sSql;
		Connection conn = null;
		PreparedStatement ps1 = null;
		PreparedStatement ps2 = null;
		ResultSet rs1 = null;
		ResultSet rs2 = null;
		try {
			conn = ClsConfig.getDbConnection();
			sSql = "SELECT COUNT(A." + UserRecord.FIELD_USER_ID + ")" + sFromWhere;
			ps1 = conn.prepareStatement(sSql);
			int iParam = 1;
			if (CommonUtils.isNotEmpty(query)) {
				ps1.setString(iParam++, query);
				ps1.setString(iParam++, query);
				ps1.setString(iParam++, query);
				ps1.setString(iParam++, query);
			}
			if (iRoleId != null)
				ps1.setInt(iParam++, iRoleId);
			rs1 = ps1.executeQuery();
			boolean isAcceptableRange = false;
			if (rs1.next())
				isAcceptableRange = pagination.isAcceptedRange(rs1.getInt(1));
			
			if (isAcceptableRange) {
				SqlUtil.closeInstance(rs1);
				rs1 = null;
				SqlUtil.closeInstance(ps1);
				ps1 = null;

				ps2 = conn.prepareStatement(UserRoles.SQL_SELECT);
				
				sSql = sSelect + sFromWhere + sOrderBy + pagination.sqlLimit();
				ps1 = conn.prepareStatement(sSql);
				iParam = 1;
				if (CommonUtils.isNotEmpty(query)) {
					ps1.setString(iParam++, query);
					ps1.setString(iParam++, query);
					ps1.setString(iParam++, query);
					ps1.setString(iParam++, query);
				}
				if (iRoleId != null)
					ps1.setInt(iParam++, iRoleId);
				rs1 = ps1.executeQuery();

				while (rs1.next()) {
					ObjectNode json = pagination.getMapper().createObjectNode();
					Integer id = rs1.getInt(UserRecord.FIELD_USER_ID);
					json.put(UserRecord.FIELD_USER_ID, id);
					json.put(UserRecord.FIELD_EMAIL, rs1.getString(UserRecord.FIELD_EMAIL));
					json.put(UserRecord.FIELD_FIRST_NAME, rs1.getString(UserRecord.FIELD_FIRST_NAME));
					json.put(UserRecord.FIELD_LAST_NAME, rs1.getString(UserRecord.FIELD_LAST_NAME));
					json.put(UserRecord.FIELD_IS_ACTIVE, rs1.getBoolean(UserRecord.FIELD_IS_ACTIVE));					
					json.put("OrgName", rs1.getString("OrgName"));
					json.put("DeptName", rs1.getString(DeptRecord.FIELD_DEPT_NAME));
					
					String roles = null;
					ps2.setInt(1, id);
					rs2 = ps2.executeQuery();
					while (rs2.next()) {
						if (roles == null)
							roles = rs2.getString(1);
						else
							roles += ", " + rs2.getString(1);
					}
					rs2.close();
					rs2 = null;
					json.put(UserRoles.TAG_ROLES, roles);
					
					pagination.incCount();
					pagination.getArrayNode().add(json);
				}
			}
			pagination.send(resp);
		}
		catch (Exception oError) {
			oError.printStackTrace();
		}
		finally {
			SqlUtil.closeInstance(rs2, ps2, null);
			SqlUtil.closeInstance(rs1, ps1, conn);
		}
	}
	
	private static void getUserDetails(String sDo, UserSession oUserSession, 
			HttpServletRequest req, HttpServletResponse resp) {
		Integer id = CommonUtils.toInteger(req.getParameter("id"));
		String errorMessage = null;
		UserRecord userRecord = null;
		if (id != null) {
			Integer orgId = oUserSession.isSuperUser() ? null : oUserSession.getOrgId(); // CJ-680
			userRecord = UserRecord.getRecord(id, null, orgId);
			if (userRecord == null)
				errorMessage = "Invalid request";
		} else
			errorMessage = "Unauthorized request";
		String response;
		if ((userRecord != null)) {
			ObjectNode objectNode = (ObjectNode)userRecord.toJson();
			try {
				AuditEventTable.addJson(id, objectNode);
			} catch (Exception oIgnore) {
				oIgnore.printStackTrace();
			}
			response = JsonAble.jsonResult(JsonAble.STATUS_SUCCESS, objectNode, null);
		} else
			response = JsonAble.jsonResult(JsonAble.STATUS_FAIL, null, errorMessage);
		PageApi.send(resp, JsonAble.JSON_RESPONSE_FORMAT, response);
	}

	private static void saveUserDetails(String sDo, UserSession oUserSession, 
			HttpServletRequest req, HttpServletResponse resp) {
		String errorMessage = null;
		UserRecord userRecord = null;
		Integer id = null;
		if (!DO_USER_DETAILS_NEW.equals(sDo)) {
			id = CommonUtils.toInteger(req.getParameter("id"));
			if (id != null) {
				Integer orgId = oUserSession.isSuperUser() ? null : oUserSession.getOrgId(); // CJ-680
				userRecord = UserRecord.getRecord(id, null, orgId);
				if (userRecord == null)
					errorMessage = "Invalid request";
			} else
				errorMessage = "Unauthorized request";

			if (errorMessage != null) {
				PageApi.send(resp, JsonAble.JSON_RESPONSE_FORMAT, 
						JsonAble.jsonResult(JsonAble.STATUS_FAIL, null, errorMessage));
				return;
			}
		}
		try
		{
			validateUserData(req, StringUtils.endsWithIgnoreCase(DO_USER_DETAILS_NEW,sDo));
		}
		catch(InvalidDataException e)
		{
			errorMessage = e.getMessage();

			PageApi.send(resp, JsonAble.JSON_RESPONSE_FORMAT, 
					JsonAble.jsonResult(JsonAble.STATUS_FAIL, null, errorMessage));
			return;
			
		}
		String[] newRoleIds = null;
		String newEmail = req.getParameter("email");
		String newFirstName = req.getParameter("first_name");
		String newLastName = req.getParameter("last_name");
		String newOrgId = req.getParameter("org_id");
		String newPassword = req.getParameter("password");
		String newIsActive = req.getParameter("is_active");
		
		boolean bnewIsActive;
		if (newIsActive.equals("true"))
			bnewIsActive = true;
		else 
			bnewIsActive = false;

		String sRoleIds = req.getParameter("user_role_ids"); // req.getParameterValues("user_role_ids");
		newRoleIds = sRoleIds.split(",");

		ArrayList<Integer> newRoles = new ArrayList<Integer>();
		for (int iRole = 0; iRole < newRoleIds.length; iRole++) {
			Integer newRole = CommonUtils.toInteger(newRoleIds[iRole]);
			if (newRole != null)
				newRoles.add(newRole);
		}
		
		Connection conn = null;
		try {
			conn = ClsConfig.getDbConnection();
			if (userRecord != null) {
				if (!userRecord.adminUpdate(conn, newEmail, newFirstName, newLastName, 
						CommonUtils.toInteger(newOrgId), 
						newPassword, newRoles, oUserSession.getUserProfile(), bnewIsActive)) {
					errorMessage = "No changes detected";
				} else {
					oUserSession.setMessage("Updated user record of " + newEmail);
				}
			} else {
				userRecord = UserRecord.getRecord(conn, null, newEmail, null);
				if (userRecord != null) {
					errorMessage = "Existing account";
				} else {
					userRecord = new UserRecord();
					userRecord.adminCreate(conn, newEmail, newFirstName, newLastName, 
							CommonUtils.toInteger(newOrgId), 
							newPassword, newRoles, oUserSession.getUserProfile(), bnewIsActive);
					oUserSession.setMessage("Created user record of " + newEmail);
				}
			}
		}
		catch (Exception oError) {
			errorMessage = "Unable to save: " + oError.getMessage();
		}
		finally {
			SqlUtil.closeInstance(conn);
		}

		String response;
		if (errorMessage == null) {
			response = JsonAble.jsonResult(JsonAble.STATUS_SUCCESS, null, null);
		} else
			response = JsonAble.jsonResult(JsonAble.STATUS_FAIL, null, errorMessage);
		PageApi.send(resp, JsonAble.JSON_RESPONSE_FORMAT, response);
	}
	
	
	
	
	public static void removeUser(UserSession oUserSession, 
			HttpServletRequest req, HttpServletResponse resp) {
		String errorMessage = null;
		UserRecord userRecord = null;
		Integer id = CommonUtils.toInteger(req.getParameter("id"));
		if (id != null) {
			Integer orgId = oUserSession.isSuperUser() ? null : oUserSession.getOrgId(); // CJ-680
			userRecord = UserRecord.getRecord(id, null, orgId);
			if (userRecord == null)
				errorMessage = "Invalid request";
		} else
			errorMessage = "Unauthorized request";

		if (errorMessage != null) {
			PageApi.send(resp, JsonAble.JSON_RESPONSE_FORMAT, 
					JsonAble.jsonResult(JsonAble.STATUS_FAIL, null, errorMessage));
			return;
		}

		Connection conn = null;
		try {
			conn = ClsConfig.getDbConnection();
			userRecord.adminRemove(conn, oUserSession.getUserProfile());
			//oUserSession.setMessage("Updated user record of " + newEmail);
		}
		catch (Exception oError) {
			errorMessage = "Unable to save: " + oError.getMessage();
		}
		finally {
			SqlUtil.closeInstance(conn);
		}

		String response;
		if (errorMessage == null) {
			response = JsonAble.jsonResult(JsonAble.STATUS_SUCCESS, null, null);
		} else
			response = JsonAble.jsonResult(JsonAble.STATUS_FAIL, null, errorMessage);
		PageApi.send(resp, JsonAble.JSON_RESPONSE_FORMAT, response);
	}
	
	private static void deactiveUser(UserSession oUserSession, 
			HttpServletRequest req, HttpServletResponse resp) {
		String errorMessage = null;
		UserRecord userRecord = null;
		Integer id = CommonUtils.toInteger(req.getParameter("id"));
		if (id != null) {
			Integer orgId = oUserSession.isSuperUser() ? null : oUserSession.getOrgId(); // CJ-680
			userRecord = UserRecord.getRecord(id, null, orgId);
			if (userRecord == null)
				errorMessage = "Invalid request";
		} else
			errorMessage = "Unauthorized request";

		if (errorMessage != null) {
			PageApi.send(resp, JsonAble.JSON_RESPONSE_FORMAT, 
					JsonAble.jsonResult(JsonAble.STATUS_FAIL, null, errorMessage));
			return;
		}

		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs1 = null;
		try {
			conn = ClsConfig.getDbConnection();

			String sSql1 = "SELECT " + UserRecord.FIELD_IS_ACTIVE + " FROM " + UserRecord.TABLE_USERS 
					       + " WHERE " + UserRecord.FIELD_USER_ID + " =?";
			ps = conn.prepareStatement(sSql1);
			ps.setInt(1, id);
			rs1 = ps.executeQuery();
			rs1.next(); 
			boolean bOldIsActive = rs1.getBoolean(UserRecord.FIELD_IS_ACTIVE);
			boolean bnewIsActive;
			if (bOldIsActive)
				bnewIsActive = false;
			else
				bnewIsActive = true;
			SqlUtil.closeInstance(ps);
			SqlUtil.closeInstance(rs1);
			
			String sSql = "UPDATE " + UserRecord.TABLE_USERS
					+ " SET " + UserRecord.FIELD_IS_ACTIVE + " =?"
					+ " WHERE " + UserRecord.FIELD_USER_ID + " =?";
			ps = conn.prepareStatement(sSql);
			ps.setBoolean(1, bnewIsActive);
			ps.setInt(2, id);
			ps.execute();
			// userRecord.adminRemove(conn, oUserSession.getUserProfile());
			//oUserSession.setMessage("Updated user record of " + newEmail);
		}
		catch (Exception oError) {
			errorMessage = "Unable to save: " + oError.getMessage();
		}
		finally {
			SqlUtil.closeInstance(conn);
			SqlUtil.closeInstance(null, ps, conn);
		}

		String response;
		if (errorMessage == null) {
			response = JsonAble.jsonResult(JsonAble.STATUS_SUCCESS, null, null);
		} else
			response = JsonAble.jsonResult(JsonAble.STATUS_FAIL, null, errorMessage);
		PageApi.send(resp, JsonAble.JSON_RESPONSE_FORMAT, response);
	}
	
	private static void resetUserPassword(UserSession oUserSession, 
			HttpServletRequest req, HttpServletResponse resp, boolean pIsJasonFormat) {
		String errorMessage = null;
		UserRecord userRecord = null;
		//Integer id = CommonUtils.toInteger(req.getParameter("id"));
		String email = req.getParameter("email");
		email = email.toLowerCase();
		String first_name = req.getParameter("first_name");
		String last_name = req.getParameter("last_name");
		
		if (email != null) {
			Integer orgId = null;
			if ((oUserSession != null) && (!oUserSession.isSuperUser()))
				orgId = oUserSession.getOrgId(); // CJ-680
			userRecord = UserRecord.getRecord(null, email, orgId);
			if (userRecord == null)
				errorMessage = "Email cannot be found.";
		} else
			errorMessage = "Unauthorized request";

		if (errorMessage != null)  {
			if (pIsJasonFormat) {
				PageApi.send(resp, JsonAble.JSON_RESPONSE_FORMAT, 
						JsonAble.jsonResult(JsonAble.STATUS_FAIL, null, errorMessage));
				return;
			} else {
				String sParam = "?message=" + errorMessage;
				PageApi.sendRedirect(req, resp, ClsPages.PAGE_FORGETPASSWORD + sParam);
				return;
			}
		}

		Connection conn = null;
		PreparedStatement ps = null;
		//ResultSet rs1 = null;
		try {
			conn = ClsConfig.getDbConnection();

			java.sql.Timestamp timestamp = new java.sql.Timestamp(CommonUtils.getNow().getTime());
			String sToken = Oauth.genCode();
						
			String sSql = "UPDATE " + UserRecord.TABLE_USERS
					+ " SET " + UserRecord.FIELD_RESET_PASSWORD_TOKEN + " =?"
					+ ", " + UserRecord.FIELD_RESET_PASSWORD_SENT_AT + " =?"
					+ " WHERE LOWER(" + UserRecord.FIELD_EMAIL + ")=?";
			ps = conn.prepareStatement(sSql);
			ps.setString(1, sToken);
			ps.setTimestamp(2, timestamp);
			ps.setString(3, email);
			ps.execute();
			
			// -----------------------
			
			SendMail sendMail = SendMail.getInstance();
			
			sendMail.setMailHost(sendMail.getMailHost());
			sendMail.setMailFrom(sendMail.getMailFrom());
			sendMail.setMailSSL(sendMail.isMailSSL());
			sendMail.setMailPort(sendMail.getMailPort());  //25, 587
			sendMail.setMailUser(sendMail.getMailUser());
			sendMail.setMailPassword(sendMail.getMailPassword());
									
			//String sURL = req.getContextPath();
			ClsConfig clsConfig = ClsConfig.getInstance();
			// clsConfig.getServerPath();
			// System.out.println("Server Path:" + clsConfig.getServerPath());
			// System.out.println("getContextPath: " + sURL);
					
			// String sResetUrl = "http://localhost:8080/cls/resetpswd.html?token=" + sToken;
			if (first_name == null)
				first_name = "";
			if (last_name == null)
				last_name = "";
			String sResetUrl = clsConfig.getClsUrl() + "resetpassword.jsp?token=" + sToken;
			String sMailContent = "<h2>Ready to reset your CLS password?</h2>Hello " + first_name + " " + last_name + ", <br><br>"+
					"Click the following link to reset your CLS password: <br> <strong>" + sResetUrl + "</strong>";

			//String[] toRecipients = {"ilchan.chang@hgs-worldwide.com"}; 
			String[] toRecipients = {email};
			sendMail.send(toRecipients, null, null, "Password Reset for CLS", sMailContent, true);
			// -----------------------
			
		}
		catch (Exception oError) {
			errorMessage = "Unable to save: " + oError.getMessage();
		}
		finally {
			SqlUtil.closeInstance(conn);
			SqlUtil.closeInstance(null, ps, conn);
		}

		if (pIsJasonFormat) {
			String response;
			if (errorMessage == null) {
				response = JsonAble.jsonResult(JsonAble.STATUS_SUCCESS, null, null);
			} else
				response = JsonAble.jsonResult(JsonAble.STATUS_FAIL, null, errorMessage);
			PageApi.send(resp, JsonAble.JSON_RESPONSE_FORMAT, response);
		} else {
			if (errorMessage == null) {				
				// this.sessionMessage = "Sending an email successully.";
				// this.messagePage = ClsPages.PAGE_RESETPASSWORD;
				PageApi.sendRedirect(null, req, resp, ClsPages.PAGE_LOGIN);
			} else {
				//this.sessionMessage = null;
				String sParam = "?message=" + errorMessage;
				PageApi.sendRedirect(req, resp, ClsPages.PAGE_FORGETPASSWORD + sParam);
			}			
		}
	}	
		
	private static void getUserForResetPassword( 
			HttpServletRequest req, HttpServletResponse resp) {
		String errorMessage = null;
		//UserRecord userRecord = null;
		String sToken = req.getParameter("token");
		
		if (sToken == null) 
			errorMessage = "Unauthorized request";

		if (errorMessage != null) {
			PageApi.send(resp, JsonAble.JSON_RESPONSE_FORMAT, 
					JsonAble.jsonResult(JsonAble.STATUS_FAIL, null, errorMessage));
			return;
		}		
		Pagination pagination = new Pagination(req);

		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs1 = null;
		try {
			conn = ClsConfig.getDbConnection();

			String sSql = "SELECT " + UserRecord.FIELD_USER_ID 
					+ ", " + UserRecord.FIELD_EMAIL
					+ ", " + UserRecord.FIELD_FIRST_NAME
					+ ", " + UserRecord.FIELD_LAST_NAME
					+ ", " + UserRecord.FIELD_RESET_PASSWORD_TOKEN
					+ ", " + UserRecord.FIELD_RESET_PASSWORD_SENT_AT
				+ " FROM " + UserRecord.TABLE_USERS
				+ " WHERE " + UserRecord.FIELD_RESET_PASSWORD_TOKEN + " =?";
			
			ps = conn.prepareStatement(sSql);
			ps.setString(1, sToken);
			
			rs1 = ps.executeQuery();
			while (rs1.next()) {
				ObjectNode json = pagination.getMapper().createObjectNode();
				// int iUserId = rs1.getInt(UserRecord.FIELD_USER_ID);
				json.put(UserRecord.FIELD_USER_ID, rs1.getInt(UserRecord.FIELD_USER_ID));
				json.put(UserRecord.FIELD_EMAIL, rs1.getString(UserRecord.FIELD_EMAIL));
				json.put(UserRecord.FIELD_FIRST_NAME, rs1.getString(UserRecord.FIELD_FIRST_NAME));
				json.put(UserRecord.FIELD_LAST_NAME, rs1.getString(UserRecord.FIELD_LAST_NAME));
				json.put(UserRecord.FIELD_RESET_PASSWORD_TOKEN, rs1.getString(UserRecord.FIELD_RESET_PASSWORD_TOKEN));
				json.put(UserRecord.FIELD_RESET_PASSWORD_SENT_AT, CommonUtils.toJsonDate(rs1.getTimestamp(UserRecord.FIELD_RESET_PASSWORD_SENT_AT)));
				
				java.sql.Timestamp timestampNow = new java.sql.Timestamp(CommonUtils.getNow().getTime());
				java.sql.Timestamp timestampSent = rs1.getTimestamp(UserRecord.FIELD_RESET_PASSWORD_SENT_AT);
				
				long hourDiff = compareTwoTimeStamps(timestampNow, timestampSent);
				boolean bTokenExpired = false;
				
				ClsConfig clsConfig = ClsConfig.getInstance();

				int iExpireHourLimit = Integer.parseInt(clsConfig.getExpireHours());
				if (iExpireHourLimit <= 0)
					iExpireHourLimit = 24;				
				if (hourDiff > iExpireHourLimit)
					bTokenExpired = true;
				json.put("token_expired", bTokenExpired);
				if (bTokenExpired)
					removeToken(rs1.getInt(UserRecord.FIELD_USER_ID));
				
				pagination.incCount();
				pagination.getArrayNode().add(json);
			}
			pagination.send(resp);
						
		}
		catch (Exception oError) {
			errorMessage = "Unable to save: " + oError.getMessage();
		}
		finally {
			SqlUtil.closeInstance(conn);
			SqlUtil.closeInstance(null, ps, conn);
		}		
	}
	
	public static long compareTwoTimeStamps(java.sql.Timestamp currentTime, java.sql.Timestamp oldTime) {
		long milliseconds1 = oldTime.getTime();
		long milliseconds2 = currentTime.getTime();
	
		long diff = milliseconds2 - milliseconds1;
		//long diffSeconds = diff / 1000;
		//long diffMinutes = diff / (60 * 1000);
		long diffHours = diff / (60 * 60 * 1000);
		//long diffDays = diff / (24 * 60 * 60 * 1000);
	
		return diffHours;
	}
	
	private static void removeToken(Integer pUserId) {
		//String errorMessage = null;
		Connection conn = null;
		PreparedStatement ps = null;
		try {
			conn = ClsConfig.getDbConnection();	
			
			String sSql = "UPDATE " + UserRecord.TABLE_USERS
					+ " SET " + UserRecord.FIELD_RESET_PASSWORD_TOKEN + " =?"
					+ ", " + UserRecord.FIELD_RESET_PASSWORD_SENT_AT + " =?"
					+ " WHERE " + UserRecord.FIELD_USER_ID + " =?";
			ps = conn.prepareStatement(sSql);
			ps.setString(1, null);
			ps.setTimestamp(2, null);
			ps.setInt(3, pUserId);
			ps.execute();
		}
		catch (Exception oError) {
			System.out.println("Unable to save: " + oError.getMessage());
			//errorMessage = "Unable to save: " + oError.getMessage();
		}
		finally {
			SqlUtil.closeInstance(conn);
			SqlUtil.closeInstance(null, ps, conn);
		}
	}	
	
	private static void validateUserData(HttpServletRequest req, boolean newUser) throws InvalidDataException
	{
		String email = req.getParameter("email");
		validateXSS(email, 1);
		String firstName = req.getParameter("first_name");
		validateXSS(firstName, 2);
		String lastName = req.getParameter("last_name");
		validateXSS(lastName, 3);
		String orgId = req.getParameter("org_id");
		//password is optional for edit user
		String password = req.getParameter("password");
		String active = req.getParameter("is_active");
		if (CommonUtils.isEmpty(email))
		{
			throw new InvalidDataException("Email Address is empty");
		}
		if (!CommonUtils.isValidEmmail(email))
		{
			throw new InvalidDataException("Email address is not valid");
		}
	   
	    if (CommonUtils.isEmpty(firstName))
	    {
	    	throw new InvalidDataException("First name is empty");
	    }
	    if (CommonUtils.isEmpty(lastName))
	    {
	    	throw new InvalidDataException("Last name is empty");
	    }	
	    
	    if (newUser &&  CommonUtils.isEmpty(password))
	    {
	    	throw new InvalidDataException("Password is empty");
	    }
	    if (CommonUtils.isEmpty(orgId))
		{
	    	throw new InvalidDataException("Agency is empty");
		}
	    if (CommonUtils.isEmpty(active))
	  	{
	  	    throw new InvalidDataException("Is active is empty");
	  	}
	}
	//CJ-1441
	private static void validateXSS(String input, int type) throws InvalidDataException
	{
		boolean check = true;
		String errorMsg = "";
		
		 if (input.indexOf('<') > -1){
			 	check = false;
			 	errorMsg = "cannot containt character: <";
		    }
		 else if (input.indexOf('>') > -1){
		    	check = false;
		    	errorMsg = "cannot containt character: >";
		    }
		 else if (input.indexOf('/') > -1){
		    	check = false;
			 	errorMsg = "cannot containt character: /";
		    }
		 else if (input.indexOf('(') > -1){
		    	check = false;
		    	errorMsg = "cannot containt character: (";
		    }
		 else if (input.indexOf(')') > -1){
		    	check = false;
		    	errorMsg = "cannot containt character: )";
		    }
		 else if (input.indexOf('{') > -1){
		    	check = false;
		    	errorMsg = "cannot containt character: {";
		    }
		 else if (input.indexOf('}') > -1){
		    	check = false;
		    	errorMsg = "cannot containt character: }";
		    	
		    }
		 
		if (check == false){
		    	switch (type) {
		    	case 1: 
		    		errorMsg = "Email " + errorMsg;
		    		break;
		    	case 2:
		    		errorMsg = "First Name " + errorMsg;
		    		break;
		    	case 3:	
		    		errorMsg = "Last Name " + errorMsg;			    	
		    		break;
		    	}
		    	throw new InvalidDataException(errorMsg);
		    }
		    
	}

	
}
