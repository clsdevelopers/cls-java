package gov.dod.cls.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.node.ArrayNode;
import org.codehaus.jackson.node.ObjectNode;

import gov.dod.cls.bean.session.UserSession;
import gov.dod.cls.config.ClsConfig;
import gov.dod.cls.utils.Pagination;
import gov.dod.cls.utils.sql.SqlUtil;

public class DeptTable {
	public static final String PREFIX_DO_DEPT = "dept-";
	
	public static final String DO_DEPT_LIST = PREFIX_DO_DEPT + "list";

	public static void processRequest(String sDo, UserSession oUserSession, 
			HttpServletRequest req, HttpServletResponse resp) {
		switch (sDo) {
		case DeptTable.DO_DEPT_LIST:
			DeptTable.processList(req, resp);
		}
	}
	
	private static void processList(HttpServletRequest req, HttpServletResponse resp) {
		
		Pagination pagination = new Pagination(req);
		String sSql = "SELECT " + DeptRecord.FIELD_DEPT_ID + ", " + DeptRecord.FIELD_DEPT_NAME + 
				         " FROM " + DeptRecord.TABLE_DEPTS + " ORDER BY " + DeptRecord.FIELD_DEPT_NAME;
		
		Connection conn = null;
		PreparedStatement ps1 = null;
		ResultSet rs1 = null;
		try {
			conn = ClsConfig.getDbConnection();
			ps1 = conn.prepareStatement(sSql);
			rs1 = ps1.executeQuery();			
			while (rs1.next()) {
				ObjectNode json = pagination.getMapper().createObjectNode();
				// ObjectMapper mapper = new ObjectMapper();
				// ObjectNode json = mapper.createObjectNode();
				json.put(DeptRecord.FIELD_DEPT_ID, rs1.getInt(DeptRecord.FIELD_DEPT_ID));
				json.put(DeptRecord.FIELD_DEPT_NAME, rs1.getString(DeptRecord.FIELD_DEPT_NAME));
				
				pagination.incCount();
				pagination.getArrayNode().add(json);
			}
			pagination.send(resp);
		}
		catch (Exception oError) {
			oError.printStackTrace();
		}
		finally {
			SqlUtil.closeInstance(rs1, ps1, conn);
		}
	}
	
	public static ArrayNode toJson(ObjectMapper mapper, Connection conn, Integer iFitlerOrgId) throws SQLException {
		if (mapper == null)
			mapper = new ObjectMapper();
		ArrayNode result = mapper.createArrayNode();
		boolean bCreateConnection =  (conn == null);
		PreparedStatement ps1 = null;
		ResultSet rs1 = null;
		try {
			if (bCreateConnection)
				conn = ClsConfig.getDbConnection();
			String sSql = "SELECT " + DeptRecord.FIELD_DEPT_ID + ", " + DeptRecord.FIELD_DEPT_NAME + 
			         " FROM " + DeptRecord.TABLE_DEPTS;
			if (iFitlerOrgId != null) { // CJ-680
				sSql += " WHERE " + DeptRecord.FIELD_DEPT_ID + " IN "
						+ " (SELECT " + OrgRecord.FIELD_DEPT_ID +
						" FROM " +  OrgRecord.TABLE_ORGS +
						" WHERE " + OrgRecord.FIELD_ORG_ID + " = " +  iFitlerOrgId + ")";
			}
			sSql += " ORDER BY " + DeptRecord.FIELD_DEPT_NAME;
			ps1 = conn.prepareStatement(sSql);
			rs1 = ps1.executeQuery();			
			while (rs1.next()) {
				ObjectNode json = mapper.createObjectNode();
				json.put(DeptRecord.FIELD_DEPT_ID, rs1.getInt(DeptRecord.FIELD_DEPT_ID));
				json.put(DeptRecord.FIELD_DEPT_NAME, rs1.getString(DeptRecord.FIELD_DEPT_NAME));
				result.add(json);
			}
		}
		finally {
			SqlUtil.closeInstance(rs1, ps1);
			if (bCreateConnection)
				SqlUtil.closeInstance(conn);
		}
		return result;
	}

}
