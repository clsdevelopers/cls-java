package gov.dod.cls.bean;

import gov.dod.cls.bean.session.UserSession;
import gov.dod.cls.db.DocumentTable;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class Dashboard {

	// ----------------------------------------------------------
	public static final String PREFIX_DO_DASHBOARD = "dashboard-";

	private static final String DO_DOCUMENT_LIST = PREFIX_DO_DASHBOARD + "doc-list";

	public static void processRequest(String sDo, UserSession oUserSession, 
			HttpServletRequest req, HttpServletResponse resp) {

		switch (sDo) {
		case Dashboard.DO_DOCUMENT_LIST:
			String interview_status = req.getParameter("interview_status");
			Integer userId, orgId;
			String query = req.getParameter("query");
			String sortField = req.getParameter("sort_field");
			String sortOrder = req.getParameter("sort_order");
			
			userId = null;
			orgId = null;
			if (!(oUserSession.isSuperUser() || oUserSession.isReviewer())) {
				if (oUserSession.isSubsuperUser() || oUserSession.isAgencyReviewer())
					orgId = oUserSession.getOrgId();
				else
					userId = oUserSession.getUserId();
			}
			/* CJ-680 Replaced following with above
			if (oUserSession.isSuperUser() || oUserSession.isSubsuperUser())
				userId = null;
			else
				userId = oUserSession.getUserId();
			*/
			
			DocumentTable.processDashboardList(req, resp, interview_status, userId, orgId, query, sortField, sortOrder);
		}
	}
	
}
