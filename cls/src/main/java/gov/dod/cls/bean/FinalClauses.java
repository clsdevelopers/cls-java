package gov.dod.cls.bean;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.node.ArrayNode;
import org.codehaus.jackson.node.ObjectNode;

import gov.dod.cls.PageApi;
import gov.dod.cls.bean.session.UserSession;
import gov.dod.cls.config.ClsConfig;
import gov.dod.cls.db.AuditEventRecord;
import gov.dod.cls.db.ClauseFillInRecord;
import gov.dod.cls.db.ClauseFillInTable;
import gov.dod.cls.db.ClausePrescriptionRecord;
import gov.dod.cls.db.ClauseRecord;
import gov.dod.cls.db.ClauseSectionRecord;
import gov.dod.cls.db.ClauseSectionTable;
import gov.dod.cls.db.ClauseTable;
import gov.dod.cls.db.DocumentClauseRecord;
import gov.dod.cls.db.DocumentClauseTable;
import gov.dod.cls.db.DocumentFillInsTable;
import gov.dod.cls.db.DocumentFillInsRecord;
import gov.dod.cls.db.DocumentRecord;
import gov.dod.cls.db.DocumentTable;
import gov.dod.cls.db.PrescriptionRecord;
import gov.dod.cls.db.QuestionGroupRefRecord;
import gov.dod.cls.db.RegulationRecord;
import gov.dod.cls.model.JsonAble;
import gov.dod.cls.utils.CommonUtils;
import gov.dod.cls.utils.Pagination;
import gov.dod.cls.utils.sql.SqlUtil;


public class FinalClauses  {

	public static final String PREFIX_DO_FINAL_CLAUSE = "finalclauses-";

	public static final String DO_ACTIVE = PREFIX_DO_FINAL_CLAUSE + "active";
	public static final String DO_SAVE = PREFIX_DO_FINAL_CLAUSE + "save";
	public static final String DO_KEYWORD = PREFIX_DO_FINAL_CLAUSE + "keyword";
	private static final Logger logger = Logger.getLogger(FinalClauses.class);
	// ----------------------------------------------------------------------
	
	private Integer docId = null;
	protected DocumentFillInsTable documentFillInsTable = null;
	protected DocumentClauseTable documentClauseTable = null;
	
	//private QuestionTable questionTable = null;
	private ClauseTable clauseTable = null;
	private Exception lastError = null;
	
	// ----------------------------------------------------------------------
	
	public static void processRequest(String sDo, UserSession oUserSession, 
			HttpServletRequest req, HttpServletResponse resp) {
		

		Integer docId = CommonUtils.toInteger(req.getParameter("id"));
		if (docId == null)
		{
			String response = JsonAble.jsonResult(JsonAble.STATUS_FAIL, null, "Invalid Document Id: " + docId); 
			PageApi.send(resp, JsonAble.JSON_RESPONSE_FORMAT, response);
			return;
		}
		String response;
		
		switch (sDo) {

		case FinalClauses.DO_ACTIVE:
			FinalClauses.processActiveList(oUserSession, req, resp, docId);
			return;
			
		case FinalClauses.DO_KEYWORD:
			String sKey = req.getParameter("searchStr");
			response = FinalClauses.processKeywordList(oUserSession, req, resp, docId, sKey);
			PageApi.send(resp, JsonAble.JSON_RESPONSE_FORMAT, response);
			return;
			
		case FinalClauses.DO_SAVE:
			String data = req.getParameter("data");			
			String mode = req.getParameter("mode");
			logger.info("MODE=" + mode + " SAVE DATA = " + data);
			if (CommonUtils.isEmpty(data)) {
				response = JsonAble.jsonResult(JsonAble.STATUS_FAIL, null, "no data", oUserSession); // CJ-649
				PageApi.send(resp, JsonAble.JSON_RESPONSE_FORMAT, response);
				return;
			}
			try {
				FinalClauses origClauses = new FinalClauses();
				if (!origClauses.loadDocumentClauses(docId, null)) {
					response = JsonAble.jsonResult(JsonAble.STATUS_FAIL, null, "Invalid Document Id: " + docId, oUserSession); 
					PageApi.send(resp, JsonAble.JSON_RESPONSE_FORMAT, response);
					return;
				}
				
				FinalClauses updClauses = new FinalClauses();
				updClauses.docId = docId;
				
				updClauses.readPost(data);
				Connection conn = null;
				try {
					conn = ClsConfig.getDbConnection();
					origClauses.loadReferences(conn);
					origClauses.save(updClauses, origClauses.clauseTable, mode, oUserSession, conn);
				}
				finally {
					SqlUtil.closeInstance(conn);
				}
				if ("1".equals(mode))
					oUserSession.setSessionMessage("Document saved");
				ObjectMapper mapper = new ObjectMapper();
				ObjectNode json = mapper.createObjectNode();
				json.put(DocumentRecord.FIELD_UPDATED_AT, CommonUtils.toJsonDate(CommonUtils.getNow()));
				response = JsonAble.jsonResult(JsonAble.STATUS_SUCCESS, json, "The selection has been saved successfully", oUserSession);
				PageApi.send(resp, JsonAble.JSON_RESPONSE_FORMAT, response);
			}
			catch (JsonParseException oJsonError) {
				oJsonError.printStackTrace();
				response = JsonAble.jsonResult(JsonAble.STATUS_FAIL, null, oJsonError.getMessage(), oUserSession); 
				PageApi.send(resp, JsonAble.JSON_RESPONSE_FORMAT, response);
			}
			catch (IOException ioError) {
				ioError.printStackTrace();
				response = JsonAble.jsonResult(JsonAble.STATUS_FAIL, null, ioError.getMessage(), oUserSession); 
				PageApi.send(resp, JsonAble.JSON_RESPONSE_FORMAT, response);
			}
			catch (Exception ioError) {
				ioError.printStackTrace();
				response = JsonAble.jsonResult(JsonAble.STATUS_FAIL, null, ioError.getMessage(), oUserSession); 
				PageApi.send(resp, JsonAble.JSON_RESPONSE_FORMAT, response);
			}
			return;			
		}

	}
	
	
	
	// ----------------------------------------------------------------------
	
	private static void processActiveList(UserSession oUserSession, HttpServletRequest req, HttpServletResponse resp, Integer docId) {
		
		
		PreparedStatement ps1 = null;
		ResultSet rs1 = null;
		Connection conn = null;

		if (docId == null)
			return;
		
		Pagination pagination = new Pagination(req);
		
		try {
			conn = ClsConfig.getDbConnection();
			
			ClauseFillInTable  cfiTable = ClauseFillInTable.getInstance(conn);
			DocumentFillInsTable dfiTable = DocumentFillInsTable.loadByDocument(docId, conn);
			
			// Retrieve a list of question changes.
			String sSql = 
			 
			"SELECT DISTINCT DC." + DocumentClauseRecord.FIELD_CLAUSE_ID + ", " +
				"DC." + DocumentClauseRecord.FIELD_IS_FILLIN_COMPLETED + ", " +
				"DC." + DocumentClauseRecord.FIELD_OPTIONAL_USER_ACTION_CODE + ", " +
				"C." + ClauseRecord.FIELD_CLAUSE_NAME + ", " +
				"C." + ClauseRecord.FIELD_INCLUSION_CD + ", " +
				"C." + ClauseRecord.FIELD_CLAUSE_TITLE + ", " +
				"C." + ClauseRecord.FIELD_EFFECTIVE_DATE + ", " +
				"C." + ClauseRecord.FIELD_IS_EDITABLE + ", " +
				"C." + ClauseRecord.FIELD_IS_OPTIONAL + ", " +
				"C." + ClauseRecord.FIELD_CLAUSE_URL + ", " +
				"R." + RegulationRecord.FIELD_REGULATION_TITLE + ", " +
				"R." + RegulationRecord.FIELD_REGULATION_URL + ", " +
				"R." + RegulationRecord.FIELD_REGULATION_NAME + ", " +
				"CS." + ClauseSectionRecord.FIELD_CLAUSE_SECTION_CODE + ", " +
				"CS." + ClauseSectionRecord.FIELD_CLAUSE_SECTION_HEADER + " " +
			" FROM " + DocumentClauseRecord.TABLE_DOCUMENT_CLAUSES + " DC " + 
			" INNER JOIN " + ClauseRecord.TABLE_CLAUSES + " C " +
				" ON C." + ClauseRecord.FIELD_CLAUSE_ID + 
					" = DC." + DocumentClauseRecord.FIELD_CLAUSE_ID +
			" INNER JOIN " + ClauseSectionRecord.TABLE_CLAUSE_SECTIONS + " CS " +
				" ON CS." + ClauseSectionRecord.FIELD_CLAUSE_SECTION_ID + 
					" = C." + ClauseRecord.FIELD_CLAUSE_SECTION_ID +
			" INNER JOIN " + RegulationRecord.TABLE_REGULATIONS + " R " +
				" ON R." + RegulationRecord.FIELD_REGULATION_ID + 
					" = C." + ClauseRecord.FIELD_REGULATION_ID +					
			" WHERE DC." + DocumentClauseRecord.FIELD_DOCUMENT_ID + " = ? " ;
			
			ps1 = conn.prepareStatement(sSql);
			ps1.setInt(1, docId);
			rs1 = ps1.executeQuery();
			
			while (rs1.next()) {
				
				ObjectNode json = pagination.getMapper().createObjectNode();
				loadClsContent (json, rs1);
				
				Integer clauseId = rs1.getInt(DocumentClauseRecord.FIELD_CLAUSE_ID);
				
				// Add combine prescriptions
				String sPrescription = getPrescriptions (clauseId, conn);
				json.put(PrescriptionRecord.FIELD_PRESCRIPTION_NAME, sPrescription);
				
				// Add fill in clauses.
				ClauseFillInTable cfiClause = cfiTable.subsetByClauseId(clauseId);
				json.put(ClauseFillInRecord.TABLE_CLAUSE_FILL_INS, cfiClause.toJson(pagination.getMapper(), false));
				
				json.put("Clause_FillIns_Answers", loadFillInAnswerContent(pagination.getMapper(), cfiClause, dfiTable));
				
				// Add fill in answers.
				//DocumentFillInsTable dfiAnswer = dfiTable.collectByClause(clauseId);
				//json.put(DocumentFillInsRecord.TABLE_DOCUMENT_FILL_INS, dfiAnswer.toJson(pagination.getMapper(), false));
				
				pagination.getArrayNode().add(json);
				pagination.incCount();
			}
			
			pagination.send(resp);
		}
		catch (Exception oError) {
			oError.printStackTrace();
		}
		finally {
			SqlUtil.closeInstance(rs1, ps1, conn);
		}
		
	}	

	private static String processKeywordList(UserSession oUserSession, HttpServletRequest req, HttpServletResponse resp, 
			Integer docId, String sKey) {
				
		ObjectMapper mapper = new ObjectMapper();
		ObjectNode objectNode = mapper.createObjectNode();
		ArrayNode arrayNode = mapper.createArrayNode();
		
		PreparedStatement ps1 = null;
		ResultSet rs1 = null;
		Connection conn = null;

		String response = null;
		
		try {
			conn = ClsConfig.getDbConnection();
			
			DocumentRecord docOrigRecord = DocumentTable.find (docId, conn);
			
			
			// Retrieve a list of question changes.
			String sSql = 
			 
			"SELECT * FROM " + ClauseRecord.TABLE_CLAUSES +			
			" WHERE (" + ClauseRecord.FIELD_CLAUSE_NAME + " LIKE ? " + 
			" OR " + ClauseRecord.FIELD_CLAUSE_TITLE + " LIKE ? " + 
			" OR " + ClauseRecord.FIELD_CLAUSE_DATA + " LIKE ? ) " +
			" AND " + ClauseRecord.FIELD_CLAUSE_VERSION_ID + " = ?  ";
			
			ps1 = conn.prepareStatement(sSql);
			sKey = "%" + sKey + "%";
			ps1.setString(1, sKey);
			ps1.setString(2, sKey);
			ps1.setString(3, sKey);
			ps1.setInt(4, docOrigRecord.getClauseVersionId());
			rs1 = ps1.executeQuery();
			
			while (rs1.next()) {
				
				ObjectNode jsonRow = arrayNode.objectNode(); // mapper.createObjectNode();
				jsonRow.put("id", rs1.getInt(ClauseRecord.FIELD_CLAUSE_ID));
				jsonRow.put("name", rs1.getString(ClauseRecord.FIELD_CLAUSE_NAME));
				arrayNode.add(jsonRow);
				
			}
			
			objectNode.put("clauseItems", arrayNode);
			
			response = JsonAble.jsonResult(JsonAble.STATUS_SUCCESS, objectNode, "Data retreived", oUserSession); 
			
		}
		catch (Exception oError) {
			oError.printStackTrace();
			response = JsonAble.jsonResult(JsonAble.STATUS_FAIL, null, "Error retreiving data", oUserSession); 
		}
		finally {
			SqlUtil.closeInstance(rs1, ps1, conn);
		}
		
		return response;
		
	}	

	// Load the FillIns and applicable answers.
	public static ArrayNode loadFillInAnswerContent(ObjectMapper mapper, ClauseFillInTable cfiTable, DocumentFillInsTable dfiTable) {
		if (mapper == null)
			mapper = new ObjectMapper();
		
		ArrayNode json = mapper.createArrayNode();
		for (ClauseFillInRecord clauseFillInRecord : cfiTable) {
			
			ObjectNode jsonRow = clauseFillInRecord.toJson(mapper);
			
			DocumentFillInsRecord dfi = dfiTable.findByClauseFillInId(clauseFillInRecord.getId());
			if (dfi != null){
				jsonRow.put("has_Answer", 1);
				jsonRow.put(DocumentFillInsRecord.FIELD_FULL_TEXT_MODIFIED, dfi.getFullTtextModified());
				jsonRow.put(DocumentFillInsRecord.FIELD_DOCUMENT_FILL_IN_ANSWER, dfi.getAnswer());
			}
			else
				jsonRow.put("has_Answer", 0);
			json.add(jsonRow);
		}
		return json;
	}
	
	private static void loadClsContent(ObjectNode json, ResultSet rs) throws SQLException {
				
		json.put(DocumentClauseRecord.FIELD_CLAUSE_ID, rs.getInt(DocumentClauseRecord.FIELD_CLAUSE_ID));
		json.put(DocumentClauseRecord.FIELD_IS_FILLIN_COMPLETED, rs.getInt(DocumentClauseRecord.FIELD_IS_FILLIN_COMPLETED));
		json.put(DocumentClauseRecord.FIELD_OPTIONAL_USER_ACTION_CODE, rs.getString(DocumentClauseRecord.FIELD_OPTIONAL_USER_ACTION_CODE));
		json.put(ClauseRecord.FIELD_CLAUSE_NAME, rs.getString(ClauseRecord.FIELD_CLAUSE_NAME));
		json.put(ClauseRecord.FIELD_INCLUSION_CD, rs.getString(ClauseRecord.FIELD_INCLUSION_CD));
		json.put(ClauseRecord.FIELD_CLAUSE_TITLE, rs.getString(ClauseRecord.FIELD_CLAUSE_TITLE));
		json.put(ClauseRecord.FIELD_EFFECTIVE_DATE, CommonUtils.toJsonDate(rs.getTimestamp(ClauseRecord.FIELD_EFFECTIVE_DATE)));
		json.put(ClauseRecord.FIELD_IS_EDITABLE, rs.getInt(ClauseRecord.FIELD_IS_EDITABLE));
		json.put(ClauseRecord.FIELD_IS_OPTIONAL, rs.getInt(ClauseRecord.FIELD_IS_OPTIONAL));
		json.put(ClauseRecord.FIELD_CLAUSE_URL, rs.getString(ClauseRecord.FIELD_CLAUSE_URL));
		json.put(RegulationRecord.FIELD_REGULATION_TITLE, rs.getString(RegulationRecord.FIELD_REGULATION_TITLE));
		json.put(RegulationRecord.FIELD_REGULATION_URL, rs.getString(RegulationRecord.FIELD_REGULATION_URL));
		json.put(RegulationRecord.FIELD_REGULATION_NAME, rs.getString(RegulationRecord.FIELD_REGULATION_NAME));
		json.put(ClauseSectionRecord.FIELD_CLAUSE_SECTION_CODE, rs.getString(ClauseSectionRecord.FIELD_CLAUSE_SECTION_CODE));
		json.put(ClauseSectionRecord.FIELD_CLAUSE_SECTION_HEADER, rs.getString(ClauseSectionRecord.FIELD_CLAUSE_SECTION_HEADER));
		
	}
	
	
	private static String getPrescriptions(int clauseId, Connection conn) throws SQLException {
		
		PreparedStatement ps1 = null;
		ResultSet rs1 = null;
		String sResult = "";
		
		try {
			String sSql = "SELECT " + PrescriptionRecord.FIELD_PRESCRIPTION_NAME + 
					" FROM " + ClausePrescriptionRecord.TABLE_CLAUSE_PRESCRIPTIONS + " CP " +
					" INNER JOIN " + PrescriptionRecord.TABLE_PRESCRIPTIONS + " P " +
					" ON P."  + PrescriptionRecord.FIELD_PRESCRIPTION_ID + 
						" = CP." + ClausePrescriptionRecord.FIELD_PRESCRIPTION_ID +
					" WHERE CP." + ClausePrescriptionRecord.FIELD_CLAUSE_ID + " = ?";
			ps1 = conn.prepareStatement(sSql);
			ps1.setInt(1, clauseId);
			rs1 = ps1.executeQuery();
			while (rs1.next()){
				sResult += ((sResult.length() > 0) ? "; " : "") + rs1.getString(PrescriptionRecord.FIELD_PRESCRIPTION_NAME);
			}
		}
		finally {
			SqlUtil.closeInstance(rs1, ps1);
		}
		
		return sResult;
	}
	
	public boolean loadDocumentClauses(int docId, Connection conn) {
		boolean result = false;
		this.lastError = null;
		this.docId = docId;
		boolean bCreateConnection = (conn == null);
		try {
			if (bCreateConnection)
				conn = ClsConfig.getDbConnection();
			// Retrieve the version of the document.
			DocumentRecord docOrigRecord = DocumentTable.find (this.docId, conn);
			
			if (docOrigRecord != null) {
				
				this.documentFillInsTable = DocumentFillInsTable.loadByDocument(this.docId, conn);
				this.documentClauseTable = DocumentClauseTable.loadByDocument(this.docId, conn);
	
				//this.loadReferences(conn);
				
				result = true;
			}
		}
		catch (Exception oError) {
			this.lastError = oError;
			oError.printStackTrace();
		}
		finally {
			if (bCreateConnection)
				SqlUtil.closeInstance(conn);
		}
		return result;
	}
	private void loadReferences(Connection conn) throws SQLException {
		
		// Retrieve the version of the document.
		DocumentRecord docOrigRecord = DocumentTable.find (this.docId, conn);
		int clauseVersionId = docOrigRecord.getClauseVersionId();

		ClauseSectionTable.getInstance(conn);
		//this.questionTable = QuestionTable.getSubsetByClauseVersion(clauseVersionId, conn); // bIsLinkedAward, 
		this.clauseTable = ClauseTable.getByClauseVersion(clauseVersionId, conn, false); // bIsLinkedAward, 
	}
	
	protected void save(FinalClauses updClause, ClauseTable oClauseTable, String mode,
			UserSession oUserSession, Connection conn) throws Exception {

		boolean bCreateConnection = (conn == null); 
		try {
			if (bCreateConnection)
				conn = ClsConfig.getDbConnection();
			
			String sAuditAction = AuditEventRecord.ACTION_DOCUMENT_UPDATED;
			if ("3".equals(mode))
				sAuditAction = AuditEventRecord.ACTION_DOCUMENT_FINALIZED;
			AuditEventRecord auditEventRecord = new AuditEventRecord(sAuditAction, oUserSession.getUserId(), oUserSession.getUserId());
			auditEventRecord.addAttribute (AuditEventRecord.FIELD_APPLICATION_ID, oUserSession.getApplicationId());
			auditEventRecord.addAttribute (AuditEventRecord.FIELD_DOCUMENT_ID, this.docId);
			
			if (this.documentClauseTable == null)
				this.documentClauseTable = new DocumentClauseTable();
			this.documentClauseTable.save(this.docId.intValue(), updClause.documentClauseTable, oClauseTable, conn, auditEventRecord);
			
			if (this.documentFillInsTable == null)
				this.documentFillInsTable = new DocumentFillInsTable();
			this.documentFillInsTable.save(this.docId.intValue(), updClause.documentFillInsTable, oClauseTable, updClause.documentClauseTable, conn, auditEventRecord);

			DocumentRecord docRecord = null;
			docRecord = DocumentTable.get(docRecord, this.docId, conn);
			
			if ("3".equals(mode))
				DocumentTable.updateDocStatus (docRecord, "F", conn);
			else	
				DocumentTable.updateDocStatus (docRecord, "I", conn);
		
			docRecord.saveUpdatedByApi (auditEventRecord, conn); // CJ-361
			
			// CJ-1410, saveUpdatedByApi already saves audit record.
			/*
			try {
				auditEventRecord.saveNew(conn);
			}
			catch (Exception oIgnore) {
				oIgnore.printStackTrace();
			}
			*/
		}
		finally {
			if (bCreateConnection)
				SqlUtil.closeInstance(conn);
		}
	}
	
	protected void fromJson(JsonNode pJsonNode)
	{
		//this.id = CommonUtils.asInteger(pJsonNode, DocumentClauseRecord.FIELD_DOCUMENT_ID);

		ArrayNode arrayNode = (ArrayNode)pJsonNode.get(DocumentClauseRecord.TABLE_DOCUMENT_CLAUSES);
		this.documentClauseTable = new DocumentClauseTable();
		this.documentClauseTable.fromJson(arrayNode);
		
		arrayNode = (ArrayNode)pJsonNode.get(DocumentFillInsRecord.TABLE_DOCUMENT_FILL_INS);
		this.documentFillInsTable = new DocumentFillInsTable();
		this.documentFillInsTable.fromJson(arrayNode);
	}
	
	private JsonNode postToJson(String postedJsonString) throws JsonParseException, IOException {
		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(org.codehaus.jackson.map.SerializationConfig.Feature.INDENT_OUTPUT, true);
		JsonFactory factory = mapper.getJsonFactory();
		JsonParser jp = factory.createJsonParser(postedJsonString);
		return mapper.readTree(jp);
	}

	private boolean readPost(String postedJsonString) throws JsonParseException, IOException {
		boolean result = false;
		
		JsonNode jsonNode = this.postToJson(postedJsonString);
		this.fromJson(jsonNode);

		return result;
	}

	public Exception getLastError() {
		return lastError;
	}

	public void setLastError(Exception lastError) {
		this.lastError = lastError;
	}
}
