package gov.dod.cls.bean.utils;

import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Locale;
import java.util.Calendar;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Element;

import gov.dod.cls.PageApi;
import gov.dod.cls.bean.ClsDocument;
import gov.dod.cls.db.ClauseRecord;
import gov.dod.cls.db.ClauseSectionRecord;
import gov.dod.cls.db.ClauseSectionTable;
import gov.dod.cls.db.ClauseTable;
import gov.dod.cls.db.DocumentClauseRecord;
import gov.dod.cls.db.DocumentClauseTable;
import gov.dod.cls.db.DocumentFillInsRecord;
import gov.dod.cls.db.DocumentFillInsTable;
import gov.dod.cls.db.RegulationRecord;
import gov.dod.cls.db.RegulationTable;
import gov.dod.cls.utils.ClsConstants;
import gov.dod.cls.utils.CommonUtils;

public class DocGenerateXml {
	
	private static final String ALTERNATE_UPPER = "ALTERNATE";
	private static final String DEVIATION_UPPER = "DEVIATION";

	public static void download(ClsDocument clsDocument, HttpServletRequest req, HttpServletResponse resp) {
		DocGenerateXml docGenerateXml = new DocGenerateXml(clsDocument);
		try {
			String plaintext = req.getParameter("pt");
			resp.setContentType("application/force-download");
			resp.setHeader("Content-Transfer-Encoding", "binary");
			PageApi.send(resp, "attachment; filename=\"" + clsDocument.genFileName("pds") + "\"",
					"application/octet-stream", // "Application/xml", // 
					docGenerateXml.generate("1".equals(plaintext))); // "text/xml"
		} catch (Exception oError) {
			oError.printStackTrace();
			PageApi.send(resp, "text/plain", oError.getMessage());
		}
	}
	
	private ClsDocument clsDocument;
	private boolean textOnly;
	
	public DocGenerateXml(ClsDocument clsDocument) {
		this.clsDocument = clsDocument;
		RegulationTable.getInstance(null);
	}
	
	public String generate(boolean textOnly) throws ParserConfigurationException, TransformerException {
		this.textOnly = textOnly;
		
		DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
		
		// root elements
		org.w3c.dom.Document doc = docBuilder.newDocument();
		org.w3c.dom.Element eProcurementDocument = doc.createElement("ProcurementDocument");
		doc.appendChild(eProcurementDocument);
		
		/*
		eProcurementDocument.appendChild(this.createElement(doc, "SchemaVersionUsed", "2.3"));
		eProcurementDocument.appendChild(this.createElement(doc, "ProcurementInstrumentForm", "SF 26"));
		
		// OriginatorDetails elements
		Element eOriginatorDetails = doc.createElement("OriginatorDetails");
		eOriginatorDetails.appendChild(this.createElement(doc, "DITPRNumber", "3722"));
		eOriginatorDetails.appendChild(this.createElement(doc, "InternalDocumentNumber", this.clsDocument.getId().toString())); // "SPRDL114C00310000000000"
		eOriginatorDetails.appendChild(this.createElement(doc, "SystemAdministratorDoDAAC", "W90X80"));
		eProcurementDocument.appendChild(eOriginatorDetails);
		*/
		 
		// AwardInstrument elements
		Element eAwardInstrument = doc.createElement("AwardInstrument");
		eProcurementDocument.appendChild(eAwardInstrument);
		 
		// ProcurementInstrumentHeader elements
		Element eProcurementInstrumentHeader = doc.createElement("ProcurementInstrumentHeader");
		eAwardInstrument.appendChild(eProcurementInstrumentHeader);
		 
		// ClauseInformation elements
		Element eClauseInformation = doc.createElement("ClauseInformation");
		eClauseInformation.appendChild(this.createElement(doc, "RegulationURL", "http://www.ecfr.gov/")); // CJ-779
		eClauseInformation.appendChild(this.createElement(doc, "RegulationURL", "http://www.acq.osd.mil/")); // CJ-779
		eProcurementInstrumentHeader.appendChild(eClauseInformation);
		 
		DocumentClauseTable documentClauseTable = this.clsDocument.getDocumentClauseTable();
		DocumentFillInsTable docFillinsTable = this.clsDocument.getFillIns();
		ClauseSectionTable clauseSectionTable = ClauseSectionTable.getInstance(null);

		// ArrayList<DocumentClauseRecord> clauses = new ArrayList<DocumentClauseRecord>(); 
		ArrayList<DocumentClauseRecord> farClausesByReference = new ArrayList<DocumentClauseRecord>(); 
		ArrayList<DocumentClauseRecord> farClausesByText = new ArrayList<DocumentClauseRecord>(); 

		ArrayList<DocumentClauseRecord> dfarClausesByReference = new ArrayList<DocumentClauseRecord>(); 
		ArrayList<DocumentClauseRecord> dfarClausesByText = new ArrayList<DocumentClauseRecord>(); 
		
		ArrayList<DocumentClauseRecord> localClausesByReference = new ArrayList<DocumentClauseRecord>(); 
		ArrayList<DocumentClauseRecord> localClausesByText = new ArrayList<DocumentClauseRecord>(); 

		ArrayList<DocumentClauseRecord> clausesByReference; 
		ArrayList<DocumentClauseRecord> clausesByText; 

		for (ClauseSectionRecord clauseSectionRecord : clauseSectionTable) {
			ClauseTable clauseTable = clsDocument.getClauses().subsetBySection(clauseSectionRecord.getId(), true);
			//clauseTable.sortByClauseName(); // CJ-746 removed
			for (ClauseRecord clauseRecord : clauseTable) {
				DocumentClauseRecord documentClauseRecord = documentClauseTable.findByClauseId(clauseRecord.getId(), true);
				if ((documentClauseRecord != null) && (!documentClauseRecord.isRemoved())) {
					documentClauseRecord.setClauseRecord(clauseRecord);
					if (clauseRecord.isFarClause()) {
						clausesByReference = farClausesByReference; 
						clausesByText = farClausesByText;
					} else if (clauseRecord.isDfarClause()) {
						clausesByReference = dfarClausesByReference; 
						clausesByText = dfarClausesByText;
					} else {
						clausesByReference = localClausesByReference; 
						clausesByText = localClausesByText;
					} 
					//if (clauseRecord.isFullText())
					if (documentClauseRecord.isEditFullText(docFillinsTable))
						clausesByText.add(documentClauseRecord);
					else
						clausesByReference.add(documentClauseRecord);					// clauses.add(documentClauseRecord);
				}
			}

			// CJ-1206, Sort the clauses
			farClausesByReference = sortClauses(farClausesByReference);
			farClausesByText = sortClauses(farClausesByText);
			dfarClausesByReference = sortClauses(dfarClausesByReference);
			dfarClausesByText = sortClauses(dfarClausesByText);
			localClausesByReference = sortClauses(localClausesByReference);
			localClausesByText = sortClauses(localClausesByText);
			
			if (!farClausesByReference.isEmpty()) {
				// System.out.println("[" + clauseSectionRecord.getName() + " FAR Referene]");
				this.generatSection(doc, eClauseInformation, clauseSectionRecord, farClausesByReference, clauseTable, docFillinsTable);
				farClausesByReference.clear();
			}
			if (!farClausesByText.isEmpty()) {
				// System.out.println("[" + clauseSectionRecord.getName() + " FAR Text]");
				this.generatSection(doc, eClauseInformation, clauseSectionRecord, farClausesByText, clauseTable, docFillinsTable);
				farClausesByText.clear();
			}
			if (!dfarClausesByReference.isEmpty()) {
				// System.out.println("[" + clauseSectionRecord.getName() + " DFAR Reference]");
				this.generatSection(doc, eClauseInformation, clauseSectionRecord, dfarClausesByReference, clauseTable, docFillinsTable);
				dfarClausesByReference.clear();
			}
			if (!dfarClausesByText.isEmpty()) {
				// System.out.println("[" + clauseSectionRecord.getName() + " DFAR Text]");
				this.generatSection(doc, eClauseInformation, clauseSectionRecord, dfarClausesByText, clauseTable, docFillinsTable);
				dfarClausesByText.clear();
			}
			if (!localClausesByReference.isEmpty()) {
				// System.out.println("[" + clauseSectionRecord.getName() + " Local FAR Reference]");
				this.generatSection(doc, eClauseInformation, clauseSectionRecord, localClausesByReference, clauseTable, docFillinsTable);
				localClausesByReference.clear();
			}
			if (!localClausesByText.isEmpty()) {
				//System.out.println("[" + clauseSectionRecord.getName() + " Local FAR Text]");
				this.generatSection(doc, eClauseInformation, clauseSectionRecord, localClausesByText, clauseTable, docFillinsTable);
				localClausesByText.clear();
			}
		}
		
		doc.setXmlStandalone(true);
		DOMSource domSource = new DOMSource(doc);
		StringWriter stringWriter = new StringWriter();
		StreamResult streamResult = new StreamResult(stringWriter);
		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		Transformer transformer = transformerFactory.newTransformer();
		transformerFactory.setFeature(XMLConstants.FEATURE_SECURE_PROCESSING, true);
		transformer.transform(domSource, streamResult);
		
		String result = stringWriter.toString();
		
		if (this.textOnly) 
			result = stripSpecialCharacters (result); // CJ-834
		
		return result;
	}
	
	private void generatSection(org.w3c.dom.Document doc,
			Element eClauseInformation,
			ClauseSectionRecord clauseSectionRecord,
			ArrayList<DocumentClauseRecord> clauses,
			ClauseTable clauseTable,
			DocumentFillInsTable docFillinsTable) 
	{
		ArrayList<DocumentClauseRecord> regularClauses = new ArrayList<DocumentClauseRecord>();
		ArrayList<DocumentClauseRecord> deviationClauses = new ArrayList<DocumentClauseRecord>();
		int iRecord = 0;
		while (iRecord < clauses.size()) {
			DocumentClauseRecord documentClauseRecord = clauses.get(iRecord++);
			ClauseRecord clauseRecord = documentClauseRecord.getClauseRecord();
			String sClauseName = clauseRecord.getClauseName();
			String sClauseNameUpper = sClauseName.toUpperCase(Locale.ENGLISH);

			if (sClauseNameUpper.contains(DEVIATION_UPPER))
				deviationClauses.add(documentClauseRecord);
			else
				regularClauses.add(documentClauseRecord);

			//if (!clauseRecord.isBasicClause()) // CJ-1168, For PDS Compliance -Full text DFAR basic & alternate should appear once in XML output
			{
				String sNamePrefix = sClauseNameUpper.split(" ")[0] + " ";
				while (iRecord < clauses.size()) {
					DocumentClauseRecord altDocumentClauseRecord = clauses.get(iRecord);
					ClauseRecord altClauseRecord = altDocumentClauseRecord.getClauseRecord();
					String altClauseName = altClauseRecord.getClauseName().toUpperCase(Locale.ENGLISH);
					if (altClauseName.startsWith(sNamePrefix)) {
						if (altClauseName.contains(DEVIATION_UPPER))
							deviationClauses.add(altDocumentClauseRecord);
						else
							regularClauses.add(altDocumentClauseRecord);
						iRecord++;
					} else
						break;
				}
			}

			if (!regularClauses.isEmpty()) {
				this.processClauses(doc, eClauseInformation, clauseSectionRecord, clauses, clauseTable, docFillinsTable, documentClauseRecord, regularClauses, false);
				regularClauses.clear();
			}
			if (!deviationClauses.isEmpty()) {
				this.processClauses(doc, eClauseInformation, clauseSectionRecord, clauses, clauseTable, docFillinsTable, documentClauseRecord, deviationClauses, true);
				deviationClauses.clear();
			}
		}
	}
	
	private void processClauses(org.w3c.dom.Document doc,
			Element eClauseInformation,
			ClauseSectionRecord clauseSectionRecord,
			ArrayList<DocumentClauseRecord> clauses,
			ClauseTable clauseTable,
			DocumentFillInsTable docFillinsTable,
			DocumentClauseRecord documentClauseRecord,
			ArrayList<DocumentClauseRecord> altClauses,
			boolean pbDeviation) 
	{
		ClauseRecord clauseRecord = altClauses.get(0).getClauseRecord();
		altClauses.remove(0);

		String sClauseName = clauseRecord.getClauseName();
		String sClauseNameUpper = sClauseName.toUpperCase(Locale.ENGLISH);
		String sNamePrefix = sClauseNameUpper.split(" ")[0];
		
		boolean isBasic = clauseRecord.isBasicClause();
		boolean skipFirstAltText = false;

		if (sClauseNameUpper.contains(ALTERNATE_UPPER)) {
			altClauses.add(documentClauseRecord);
			skipFirstAltText = true; // CJ-750
			String sMainClauseName;
			if (pbDeviation) {
				String[] nameParts = sClauseNameUpper.split(DEVIATION_UPPER);
				sMainClauseName = sNamePrefix + " (" + DEVIATION_UPPER + nameParts[1]; 
			} else {
				sMainClauseName = sNamePrefix;
			}
			ClauseRecord mainClauseRecord = clauseTable.findByName(sMainClauseName);
			if (mainClauseRecord == null) {
				System.out.println("Unable to locate a missing main clause, " + sMainClauseName + ", of " + clauseRecord.getClauseName());
			} else {
				DocumentClauseRecord mainDocumentClauseRecord = null;
				for (DocumentClauseRecord tempDocumentClauseRecord : clauses) {
					if (mainClauseRecord == tempDocumentClauseRecord.getClauseRecord()) {
						mainDocumentClauseRecord = tempDocumentClauseRecord;
						break;
					}
				}
				if (mainDocumentClauseRecord == null) {
					System.out.println("A main clause, " + sMainClauseName + ", was not selected");
				} else {
					System.out.println("A main clause, " + sMainClauseName + ", was selected but not in this batch");
				}
				clauseRecord = mainClauseRecord;
				skipFirstAltText = false;
			}
		}
		/*
		else if (sClauseNameUpper.contains(DEVIATION_UPPER)) {
			altClauses.add(documentClauseRecord);
			skipFirstAltText = true;
		}
		*/
		
		//eClauseInformation.appendChild(this.createElement(doc, "RegulationUrl", clauseRecord.getUrl()));
		Element eClauseDetails = doc.createElement("ClauseDetails");

		RegulationRecord regulationRecord = clauseRecord.getRegulationRecord();
		eClauseDetails.appendChild(this.createElement(doc, "RegulationOrSupplement",
				(regulationRecord != null ? regulationRecord.getName() : null)));
		eClauseDetails.appendChild(this.createElement(doc, "ClauseNumber", sNamePrefix));
		eClauseDetails.appendChild(this.createElement(doc, "ClauseTitle", clauseRecord.getClauseTitleNoBasic())); // CJ-1174 changed from clauseRecord.getClauseTitle()
		
		Calendar lastEffectiveDate = Calendar.getInstance(); // CJ-779
		if (clauseRecord.getEffectiveDate() == null)
			lastEffectiveDate.setTimeInMillis(0);
		else
			lastEffectiveDate.setTimeInMillis(clauseRecord.getEffectiveDate().getTime());
		Calendar effectiveDate = Calendar.getInstance();
		
		altClauses = sortClauses(altClauses);	// CJ-1337, Sort Alternates
		
		ArrayList<Element> aVariations = new ArrayList<Element>(); // Per Judy
		if (altClauses.size() > 0) {
			for (DocumentClauseRecord altDocumentClauseRecord : altClauses) {
				
				generateDocVariationTag (altDocumentClauseRecord.getClauseRecord(), effectiveDate, lastEffectiveDate, doc,  aVariations) ;
			}
		}
		// CJ-1154, Deviation clauses were not showing the <ClauseVariation> tag.
		else if (pbDeviation)
		{
			generateDocVariationTag (clauseRecord, effectiveDate, lastEffectiveDate, doc,  aVariations) ;
		}
		
		
		eClauseDetails.appendChild(this.createElement(doc, "ClauseEffectiveDate",
				CommonUtils.toYearMonth(lastEffectiveDate.getTime()))); // CJ-779
		for (Element element : aVariations) { // Per Judy
			eClauseDetails.appendChild(element);
		}

		if (documentClauseRecord.isEditFullText(docFillinsTable)) { // CJ-576
		// if (clauseRecord.isFullText()) { // ("F".equals(clauseRecord.getInclusion())) {
			Element eClauseText = doc.createElement("ClauseText");
			DocumentFillInsTable aFillIns = this.clsDocument.getFillIns().collectByClause(clauseRecord.getId()); // .intValue()
			// CJ-533, support editables with fillins
			String sClauseText = "";
			if (clauseRecord.isEditable() 
					&& (clauseRecord.getEditableRemarks() != null)
					&& ((clauseRecord.getEditableRemarks().equals("ALL")) 
					|| (clauseRecord.getEditableRemarks().startsWith("ALL")))) {
				
				String sFillInName = clauseRecord.getClauseData();
				sFillInName = sFillInName.replace("{{", "");
				sFillInName = sFillInName.replace("}}", "");
				DocumentFillInsRecord oDocFillIn = aFillIns.findByClauseFillInCode(sFillInName);
				
				if (oDocFillIn != null)
					sClauseText = clauseRecord.getClauseDataWFillins(aFillIns, oDocFillIn.getAnswer(), ClsConstants.OutputType.XML);
				else {
					sClauseText = clauseRecord.getClauseData(aFillIns, ClsConstants.OutputType.XML);
					sClauseText = clauseRecord.getClauseDataWFillins(aFillIns, sClauseText, ClsConstants.OutputType.XML);
				}
			}
			// CJ-516
			else if (clauseRecord.isEditable() 
					&& (clauseRecord.getEditableRemarks() != null)
					&& (clauseRecord.getEditableRemarks().startsWith("PARENT"))) {
				
				String sContent = clauseRecord.getClauseData();

				Integer	pos1 = sContent.indexOf("{{", 0);
				Integer	pos2 = sContent.indexOf("}}", 0);
				String sFillInName = "";
					
				if ((pos1 >= 0) &&  (pos2 >= 0)) {
					sFillInName = sContent.substring(pos1+2, pos2);
					DocumentFillInsRecord oDocFillIn = aFillIns.findByClauseFillInCode(sFillInName);
					
					if (oDocFillIn != null) 
						sContent = sContent.replace ("{{" + sFillInName + "}}", oDocFillIn.getAnswer());
					else
						sContent = clauseRecord.getClauseDataWFillins(aFillIns, sContent, ClsConstants.OutputType.XML);
				}
				sClauseText = clauseRecord.getClauseDataWFillins(aFillIns, sContent, ClsConstants.OutputType.XML);
			}
			else
				sClauseText = clauseRecord.getClauseData(aFillIns, ClsConstants.OutputType.XML);
			
			sClauseText += (this.textOnly ? "\n" : "") + clauseRecord.getFooterText(); // CJ-817
			
			//add full text for alt clauses
			if ((altClauses.size() > 0)) { // && (isBasic == false)) { // CJ-1168, For PDS Compliance -Full text DFAR basic & alternate should appear once in XML output
				for (DocumentClauseRecord altDocumentClauseRecord : altClauses) {
					if (skipFirstAltText) {
						skipFirstAltText = false;
						continue;
					}
					ClauseRecord altClauseRecord = altDocumentClauseRecord.getClauseRecord();
					DocumentFillInsTable altFillIns = this.clsDocument.getFillIns().collectByClause(altClauseRecord.getId()); // .intValue()
					// CJ-1336
					String sFillInName = clauseRecord.getClauseData();
					sFillInName = sFillInName.replace("{{", "");
					sFillInName = sFillInName.replace("}}", "");
					DocumentFillInsRecord oDocFillIn = altFillIns.findByClauseFillInCode(sFillInName);
					
					String altClauseText = "";
					if (oDocFillIn != null)
						altClauseText = altClauseRecord.getClauseDataWFillins(altFillIns, oDocFillIn.getAnswer(), ClsConstants.OutputType.XML);
					else {
						altClauseText = altClauseRecord.getClauseData(altFillIns, ClsConstants.OutputType.XML);
						altClauseText = altClauseRecord.getClauseDataWFillins(altFillIns, altClauseText, ClsConstants.OutputType.XML);
					} 
					// String altClauseText = altClauseRecord.getClauseData(altFillIns, ClsConstants.OutputType.XML)
					// + (this.textOnly ? "\n" : "") + altClauseRecord.getFooterText(); // CJ-817
					// end CJ-1336
					altClauseText = altClauseText + (this.textOnly ? "\n" : "") + altClauseRecord.getFooterText(); // CJ-817
					sClauseText += altClauseText;
				}
			}
			//moved these three lines down to after alt clause text added
			Element eClauseFullText = doc.createElement("ClauseFullText");

			if (this.textOnly) {
				// sClauseText += "\n" + clauseRecord.getFooterText(); // CJ-817
				eClauseFullText.appendChild(doc.createTextNode(sClauseText));			
			}
			else {
				// sClauseText += clauseRecord.getFooterText(); // CJ-817					
				eClauseFullText.appendChild(doc.createCDATASection(sClauseText));	
			}
			eClauseText.appendChild(eClauseFullText);
			eClauseDetails.appendChild(eClauseText);
		}
		altClauses.clear();
		eClauseDetails.appendChild(this.createElement(doc, "Section", clauseSectionRecord.getName()));
		eClauseInformation.appendChild(eClauseDetails);
	}
	
	private void generateDocVariationTag 
		(ClauseRecord clauseRecord, Calendar effectiveDate, Calendar lastEffectiveDate,
			org.w3c.dom.Document doc, ArrayList<Element> aVariations) {
		ClauseRecord altClauseRecord = clauseRecord;
		String sAltClauseName = altClauseRecord.getClauseName();
		String[] variationParts = sAltClauseName.split(" ");
		String sVariationName = (variationParts.length > 2) ? variationParts[2] : "";
		if (sVariationName.endsWith(")"))
			sVariationName = sVariationName.substring(0, sVariationName.length() - 1);
		
		if (altClauseRecord.getEffectiveDate() != null) { // CJ-779
			effectiveDate.setTimeInMillis(altClauseRecord.getEffectiveDate().getTime());
			if (effectiveDate.after(lastEffectiveDate))
				lastEffectiveDate = effectiveDate;
		}

		// CJ-727
		String sClauseVariation = (sAltClauseName.toUpperCase(Locale.ENGLISH).contains(ALTERNATE_UPPER) ? "Alternate" : "Deviation");
		Element eAlternateOrDeviation = this.createElement(doc, "AlternateOrDeviation", sClauseVariation);

		Element eClauseVariation = doc.createElement("ClauseVariation"); // CJ-779
		
		eClauseVariation.appendChild(eAlternateOrDeviation);
		
		Element eVariationName = this.createElement(doc, "VariationName", sVariationName);
		eClauseVariation.appendChild(eVariationName);

		aVariations.add(eClauseVariation);
	}
	/* CJ-1033 replaced followings with aboves
	private void generatSection(org.w3c.dom.Document doc,
			Element eClauseInformation,
			ClauseSectionRecord clauseSectionRecord,
			ArrayList<DocumentClauseRecord> clauses,
			ClauseTable clauseTable,
			DocumentFillInsTable docFillinsTable) 
	{
		
		ArrayList<DocumentClauseRecord> altClauses = new ArrayList<DocumentClauseRecord>();
		int iRecord = 0;
		while (iRecord < clauses.size()) {
			DocumentClauseRecord documentClauseRecord = clauses.get(iRecord++);
			ClauseRecord clauseRecord = documentClauseRecord.getClauseRecord();
			String sClauseName = clauseRecord.getClauseName();
			String sClauseNameUpper = sClauseName.toUpperCase(Locale.ENGLISH);
			boolean isBasic = clauseRecord.isBasicClause();
			boolean skipFirstAltText = false;
			if (sClauseNameUpper.contains(ALTERNATE_UPPER)) {
				altClauses.add(documentClauseRecord);
				skipFirstAltText = true; // CJ-750
			}
			else if (sClauseNameUpper.contains(DEVIATION_UPPER)) {
				altClauses.add(documentClauseRecord);
				skipFirstAltText = true;
			}
			
			//clauseRecord = documentClauseRecord.getClauseRecord();
			String sNamePrefix = sClauseNameUpper.split(" ")[0]; // clauseRecord.getClauseName().toUpperCase().split(" ")[0];
			if (!isBasic) {
				if (skipFirstAltText && documentClauseRecord.isEditFullText(docFillinsTable)) { // CJ-576
				//if (skipFirstAltText && clauseRecord.isFullText()) { // CJ-750
					ClauseRecord mainClauseRecord = clauseTable.findByName(sNamePrefix);
					if (mainClauseRecord == null) {
						System.out.println("Unable to locate a missing main clause, " + sNamePrefix + ", of " + sClauseName);
					} else {
						DocumentClauseRecord mainDocumentClauseRecord = null;
						for (DocumentClauseRecord tempDocumentClauseRecord : clauses) {
							if (mainClauseRecord == tempDocumentClauseRecord.getClauseRecord()) {
								mainDocumentClauseRecord = tempDocumentClauseRecord;
								break;
							}
						}
						if (mainDocumentClauseRecord == null) {
							System.out.println("A main clause, " + sNamePrefix + ", was not selected");
						} else {
							System.out.println("A main clause, " + sNamePrefix + ", was selected but not in this batch");
						}
						clauseRecord = mainClauseRecord;
						skipFirstAltText = false;
					}
				}
				while (iRecord < clauses.size()) {
					DocumentClauseRecord altDocumentClauseRecord = clauses.get(iRecord);
					ClauseRecord altClauseRecord = altDocumentClauseRecord.getClauseRecord();
					String altClauseName = altClauseRecord.getClauseName().toUpperCase(Locale.ENGLISH);
					if (altClauseName.startsWith(sNamePrefix) &&
							(altClauseName.contains(ALTERNATE_UPPER) || altClauseName.contains(DEVIATION_UPPER))) {
						altClauses.add(altDocumentClauseRecord);
						iRecord++;
					} else
						break;
				}
			}
			
			//eClauseInformation.appendChild(this.createElement(doc, "RegulationUrl", clauseRecord.getUrl()));
			Element eClauseDetails = doc.createElement("ClauseDetails");

			RegulationRecord regulationRecord = clauseRecord.getRegulationRecord();
			eClauseDetails.appendChild(this.createElement(doc, "RegulationOrSupplement",
					(regulationRecord != null ? regulationRecord.getName() : null)));
			eClauseDetails.appendChild(this.createElement(doc, "ClauseNumber", sNamePrefix));
			eClauseDetails.appendChild(this.createElement(doc, "ClauseTitle", clauseRecord.getClauseTitleText())); // CJ-815 changed from clauseRecord.getClauseTitle()
			
			Calendar lastEffectiveDate = Calendar.getInstance(); // CJ-779
			if (clauseRecord.getEffectiveDate() == null)
				lastEffectiveDate.setTimeInMillis(0);
			else
				lastEffectiveDate.setTimeInMillis(clauseRecord.getEffectiveDate().getTime());
			Calendar effectiveDate = Calendar.getInstance();
			
			ArrayList<Element> aVariations = new ArrayList<Element>(); // Per Judy
			if (altClauses.size() > 0) {
				for (DocumentClauseRecord altDocumentClauseRecord : altClauses) {
					ClauseRecord altClauseRecord = altDocumentClauseRecord.getClauseRecord();
					String sAltClauseName = altClauseRecord.getClauseName();
					String[] variationParts = sAltClauseName.split(" ");
					String sVariationName = (variationParts.length > 2) ? variationParts[2] : "";
					if (sVariationName.endsWith(")"))
						sVariationName = sVariationName.substring(0, sVariationName.length() - 1);
					
					if (altClauseRecord.getEffectiveDate() != null) { // CJ-779
						effectiveDate.setTimeInMillis(altClauseRecord.getEffectiveDate().getTime());
						if (effectiveDate.after(lastEffectiveDate))
							lastEffectiveDate = effectiveDate;
					}

					// CJ-727
					String sClauseVariation = (sAltClauseName.toUpperCase(Locale.ENGLISH).contains(ALTERNATE_UPPER) ? "Alternate" : "Deviation");
					Element eAlternateOrDeviation = this.createElement(doc, "AlternateOrDeviation", sClauseVariation);

					Element eClauseVariation = doc.createElement("ClauseVariation"); // CJ-779
					//Element eClauseVariation = this.createElement(doc, "ClauseVariation", sClauseVariation);
					
					eClauseVariation.appendChild(eAlternateOrDeviation);
					
					Element eVariationName = this.createElement(doc, "VariationName", sVariationName);
					eClauseVariation.appendChild(eVariationName);

					aVariations.add(eClauseVariation);
					//eClauseDetails.appendChild(eClauseVariation); // Per Judy
				}
			}
			eClauseDetails.appendChild(this.createElement(doc, "ClauseEffectiveDate",
					CommonUtils.toYearMonth(lastEffectiveDate.getTime()))); // CJ-779
			for (Element element : aVariations) { // Per Judy
				eClauseDetails.appendChild(element);
			}

			if (documentClauseRecord.isEditFullText(docFillinsTable)) { // CJ-576
			// if (clauseRecord.isFullText()) { // ("F".equals(clauseRecord.getInclusion())) {
				Element eClauseText = doc.createElement("ClauseText");
				DocumentFillInsTable aFillIns = this.clsDocument.getFillIns().collectByClause(clauseRecord.getId()); // .intValue()
				// CJ-533, support editables with fillins
				String sClauseText = "";
				if (clauseRecord.isEditable() 
						&& (clauseRecord.getEditableRemarks() != null)
						&& ((clauseRecord.getEditableRemarks().equals("ALL")) 
						|| (clauseRecord.getEditableRemarks().startsWith("ALL")))) {
					
					String sFillInName = clauseRecord.getClauseData();
					sFillInName = sFillInName.replace("{{", "");
					sFillInName = sFillInName.replace("}}", "");
					DocumentFillInsRecord oDocFillIn = aFillIns.findByClauseFillInCode(sFillInName);
					
					if (oDocFillIn != null)
						sClauseText = clauseRecord.getClauseDataWFillins(aFillIns, oDocFillIn.getAnswer());
					else {
						sClauseText = clauseRecord.getClauseData(aFillIns);
						sClauseText = clauseRecord.getClauseDataWFillins(aFillIns, sClauseText);
					}
				}
				// CJ-516
				else if (clauseRecord.isEditable() 
						&& (clauseRecord.getEditableRemarks() != null)
						&& (clauseRecord.getEditableRemarks().startsWith("PARENT"))) {
					
					String sContent = clauseRecord.getClauseData();

					Integer	pos1 = sContent.indexOf("{{", 0);
					Integer	pos2 = sContent.indexOf("}}", 0);
					String sFillInName = "";
						
					if ((pos1 >= 0) &&  (pos2 >= 0)) {
						sFillInName = sContent.substring(pos1+2, pos2);
						DocumentFillInsRecord oDocFillIn = aFillIns.findByClauseFillInCode(sFillInName);
						
						if (oDocFillIn != null) 
							sContent = sContent.replace ("{{" + sFillInName + "}}", oDocFillIn.getAnswer());
						else
							sContent = clauseRecord.getClauseDataWFillins(aFillIns, sContent);
					}
					sClauseText = clauseRecord.getClauseDataWFillins(aFillIns, sContent);
				}
				else
					sClauseText = clauseRecord.getClauseData(aFillIns);
				
				sClauseText += (this.textOnly ? "\n" : "") + clauseRecord.getFooterText(); // CJ-817
				
				//add full text for alt clauses
				if ((altClauses.size() > 0) && (isBasic == false)) {
					for (DocumentClauseRecord altDocumentClauseRecord : altClauses) {
						if (skipFirstAltText) {
							skipFirstAltText = false;
							continue;
						}
						ClauseRecord altClauseRecord = altDocumentClauseRecord.getClauseRecord();
						DocumentFillInsTable altFillIns = this.clsDocument.getFillIns().collectByClause(altClauseRecord.getId()); // .intValue()
						String altClauseText = altClauseRecord.getClauseData(altFillIns)
								+ (this.textOnly ? "\n" : "") + altClauseRecord.getFooterText(); // CJ-817
						sClauseText += altClauseText;
					}
				}
				//moved these three lines down to after alt clause text added
				Element eClauseFullText = doc.createElement("ClauseFullText");

				if (this.textOnly) {
					// sClauseText += "\n" + clauseRecord.getFooterText(); // CJ-817
					eClauseFullText.appendChild(doc.createTextNode(sClauseText));			
				}
				else {
					// sClauseText += clauseRecord.getFooterText(); // CJ-817					
					eClauseFullText.appendChild(doc.createCDATASection(sClauseText));	
				}
				eClauseText.appendChild(eClauseFullText);
				eClauseDetails.appendChild(eClauseText);
			}
			altClauses.clear();
			eClauseDetails.appendChild(this.createElement(doc, "Section", clauseSectionRecord.getName()));
			eClauseInformation.appendChild(eClauseDetails);
		}
	}
	*/

	private String stripSpecialCharacters(String sInput) {
		String sOutput = sInput;
		/*
		sOutput = sOutput.replace("&", "&amp;");
		sOutput = sOutput.replace("<", "&lt;");
		sOutput = sOutput.replace(">", "&gt;");
		sOutput = sOutput.replace("\"", "&quot;");
		sOutput = sOutput.replace("'", "&apos;");
		*/
		sOutput = sOutput.replace("¢", "&cent;");
		sOutput = sOutput.replace("£", "&pound;");
		sOutput = sOutput.replace("½", "&frac12;");
		sOutput = sOutput.replace("&mdash;", "-"); // Fancy dash
		sOutput = sOutput.replace("&ldquo;", "&quot;"); // left quote
		sOutput = sOutput.replace("&rdquo;", "&quot;"); // right quote
		sOutput = sOutput.replace("“", "&quot;"); // left quote
		sOutput = sOutput.replace("”", "&quot;"); // right quote
		sOutput = sOutput.replace("&reg;", " "); // copyright
		
		sOutput = sOutput.replaceAll("[^\\x00-\\x7f]", ""); 
		
		return sOutput;	
	}
	
	private Element createElement(org.w3c.dom.Document doc, String tag, String value) {
		Element element = doc.createElement(tag);
		if ((value != null) && (!value.isEmpty())) {
			element.appendChild(doc.createTextNode(value));
		}
		return element;
	}
	
	private ArrayList<DocumentClauseRecord> sortClauses(ArrayList<DocumentClauseRecord> srcClauses) {
		
		ArrayList<DocumentClauseRecord> dstClauses = new ArrayList<DocumentClauseRecord>(); 

		for (DocumentClauseRecord srcRecord : srcClauses) {
			Integer ndx = 0; 
			Boolean bAdded = false;
			if (dstClauses.size() == 0) {
				dstClauses.add(0, srcRecord);
				continue;
			}
			for (DocumentClauseRecord destRecord : dstClauses) {

				if (ClauseTable.compareClauseName(srcRecord.getClauseName(), destRecord.getClauseName())<0) {
					dstClauses.add(ndx, srcRecord);
					bAdded = true;
					break;
				}
				ndx++;
			}
			if (!bAdded) {
				dstClauses.add(srcRecord);
			}
			
		}
		
		srcClauses.clear();
		return dstClauses;
	}
}
