package gov.dod.cls.bean.utils;

import java.awt.image.BufferedImage;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Locale;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.edit.PDPageContentStream;
import org.apache.pdfbox.pdmodel.font.PDFont;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.apache.pdfbox.pdmodel.graphics.xobject.PDPixelMap;
import org.apache.pdfbox.pdmodel.graphics.xobject.PDXObjectImage;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import gov.dod.cls.PageApi;
import gov.dod.cls.bean.ClsDocument;
import gov.dod.cls.db.ClauseRecord;
import gov.dod.cls.db.ClauseSectionRecord;
import gov.dod.cls.db.ClauseSectionTable;
import gov.dod.cls.db.ClauseTable;
import gov.dod.cls.db.DocumentClauseRecord;
import gov.dod.cls.db.DocumentClauseTable;
import gov.dod.cls.db.DocumentFillInsRecord;
import gov.dod.cls.db.DocumentFillInsTable;
import gov.dod.cls.db.RegulationTable;
import gov.dod.cls.utils.ClsConstants;
import gov.dod.cls.utils.CommonUtils;

public class DocGeneratePdf{
	
	public static final PDFont FONT =  PDType1Font.TIMES_ROMAN;
	public static final PDFont FONT_BOLD =  PDType1Font.TIMES_BOLD;
	public static final int FONT_SIZE = 10;
	public static final int MARGIN = 72;
	
	private ClsDocument clsDocument;
	private PDPageContentStream contentStream;
	private PDPage page;
	private PDDocument doc;	
	
	private float startY, startX, currentX, currentY, pageWidth, pageHeight, leading;

	public static void downloadPdf(ClsDocument clsDocument, HttpServletRequest req, HttpServletResponse resp) {
		DocGeneratePdf docGeneratePdf = new DocGeneratePdf(clsDocument);
		try {
			PageApi.send(resp, 
					"attachment; filename=" + clsDocument.genFileName("pdf"),
					"application/pdf",
					docGeneratePdf.createDoc());
		} catch (Exception oError) {
			oError.printStackTrace();
			PageApi.send(resp, "text/plain", oError.getMessage());
		}
	}
	
	public DocGeneratePdf(ClsDocument clsDocument) {
		this.clsDocument = clsDocument;
		RegulationTable.getInstance(null);
	}
	
	private void initializeDoc() throws IOException{
		doc = new PDDocument();
	    page = new PDPage();
	    doc.addPage(page);
	    contentStream = new PDPageContentStream(doc, page);
	    contentStream.beginText();
	    contentStream.setFont(FONT, FONT_SIZE);
	    PDRectangle mediabox = page.findMediaBox();
		pageWidth = mediabox.getWidth() - 2*MARGIN;
		leading = 1.5f * FONT_SIZE;
		startX = mediabox.getLowerLeftX() + MARGIN;
		startY = mediabox.getUpperRightY() - MARGIN;
		currentY = startY;
		currentX = startX;
	    contentStream.moveTextPositionByAmount(startX, startY);
	    pageHeight = mediabox.getHeight() - 2*MARGIN;
	}
	
	public PDDocument createDoc() throws Exception {
		initializeDoc();
	    
	    contentStream.setFont(FONT_BOLD, FONT_SIZE);
	    contentStream.drawString("Acquisition Title");
        contentStream.moveTextPositionByAmount(0, -leading);
        contentStream.drawString("Document Number");
        contentStream.moveTextPositionByAmount(0, -leading);
        contentStream.drawString("Document Type");
        contentStream.moveTextPositionByAmount(0, -leading);
        contentStream.drawString("Creation Date");
        contentStream.moveTextPositionByAmount(0, -leading);
        contentStream.drawString("Last Updated");
        contentStream.moveTextPositionByAmount(0, -leading);
        
        contentStream.setFont(FONT,FONT_SIZE);
        contentStream.moveTextPositionByAmount(200, leading*5);
        contentStream.drawString(this.clsDocument.getAcquisitionTitle());
        contentStream.moveTextPositionByAmount(0, -leading);
        contentStream.drawString(this.clsDocument.getDocumentNumber());
        contentStream.moveTextPositionByAmount(0, -leading);
        contentStream.drawString(this.clsDocument.getDocumentTypeName());
        contentStream.moveTextPositionByAmount(0, -leading);
        contentStream.drawString(CommonUtils.toDateTimeBrief(this.clsDocument.getCreatedAt()));
        contentStream.moveTextPositionByAmount(0, -leading);
        contentStream.drawString(CommonUtils.toDateTimeBrief(this.clsDocument.getUpdatedAt()));
        contentStream.moveTextPositionByAmount(0, -leading);

        contentStream.moveTextPositionByAmount(-200, -leading*3);
        currentY -= (leading*8);
        
		DocumentClauseTable documentClauseTable = this.clsDocument.getDocumentClauseTable();
		DocumentFillInsTable docFillinsTable = this.clsDocument.getFillIns();
		ClauseSectionTable clauseSectionTable = ClauseSectionTable.getInstance(null);

		ArrayList<DocumentClauseRecord> farClausesByReference = new ArrayList<DocumentClauseRecord>(); 
		ArrayList<DocumentClauseRecord> farClausesByText = new ArrayList<DocumentClauseRecord>(); 

		ArrayList<DocumentClauseRecord> dfarClausesByReference = new ArrayList<DocumentClauseRecord>(); 
		ArrayList<DocumentClauseRecord> dfarClausesByText = new ArrayList<DocumentClauseRecord>(); 
		
		ArrayList<DocumentClauseRecord> localClausesByReference = new ArrayList<DocumentClauseRecord>(); 
		ArrayList<DocumentClauseRecord> localClausesByText = new ArrayList<DocumentClauseRecord>(); 

		ArrayList<DocumentClauseRecord> clausesByReference; 
		ArrayList<DocumentClauseRecord> clausesByText; 

        for (ClauseSectionRecord clauseSectionRecord : clauseSectionTable) {
			ClauseTable clauseTable = clsDocument.getClauses().subsetBySection(clauseSectionRecord.getId(), true);
			for (ClauseRecord clauseRecord : clauseTable) {
				DocumentClauseRecord documentClauseRecord = documentClauseTable.findByClauseId(clauseRecord.getId(), true);
				if ((documentClauseRecord != null) && (!documentClauseRecord.isRemoved())) {
					documentClauseRecord.setClauseRecord(clauseRecord);

					if (clauseRecord.isFarClause()) {
						clausesByReference = farClausesByReference; 
						clausesByText = farClausesByText;
					} else if (clauseRecord.isDfarClause()) {
						clausesByReference = dfarClausesByReference; 
						clausesByText = dfarClausesByText;
					} else {
						clausesByReference = localClausesByReference; 
						clausesByText = localClausesByText;
					} 
					//if (clauseRecord.isFullText())
					if (documentClauseRecord.isEditFullText(docFillinsTable))
						clausesByText.add(documentClauseRecord);
					else
						clausesByReference.add(documentClauseRecord);
				}
			}

			// CJ-1206, Sort the clauses
			farClausesByReference = sortClauses(farClausesByReference);
			farClausesByText = sortClauses(farClausesByText);
			dfarClausesByReference = sortClauses(dfarClausesByReference);
			dfarClausesByText = sortClauses(dfarClausesByText);
			localClausesByReference = sortClauses(localClausesByReference);
			localClausesByText = sortClauses(localClausesByText);
			
			if (!(farClausesByText.isEmpty() && farClausesByReference.isEmpty() &&
					dfarClausesByText.isEmpty() && dfarClausesByReference.isEmpty() &&
					localClausesByText.isEmpty() && localClausesByReference.isEmpty() )) {
				
				this.writeLine(true, clauseSectionRecord.getFullName());
				
				this.generateSection(ClauseRecord.FAR_CLAUSE, farClausesByReference, farClausesByText);
				this.generateSection(ClauseRecord.DFAR_CLAUSE, dfarClausesByReference, dfarClausesByText);
				this.generateSection(ClauseRecord.LOCAL_CLAUSE, localClausesByReference, localClausesByText);
				this.writeLine(false, "");
				//this.generatSection(clauseSectionRecord, farClausesByReference, farClausesByText,
				//		dfarClausesByReference, dfarClausesByText, localClausesByReference, localClausesByText);

				farClausesByReference.clear();
				farClausesByText.clear();

				dfarClausesByReference.clear(); 
				dfarClausesByText.clear(); 
				
				localClausesByReference.clear(); 
				localClausesByText.clear(); 
			}
		}

	    contentStream.endText(); 
	    contentStream.close();
		
		return doc;
	}
	
	private void generateSection(String clauseType,
			ArrayList<DocumentClauseRecord> clausesByReference, ArrayList<DocumentClauseRecord> clausesByText) throws Exception {
		ClauseRecord clauseRecord;
		
        // this.writeLine(true, clauseSectionRecord.getFullName()); 

		if (!clausesByReference.isEmpty()) {
			this.writeLine(false, "");
	        this.writeLine(true, clauseType + " CLAUSES INCLUDED BY REFERENCE");
	        this.writeLine(false, "");
	        for (DocumentClauseRecord documentClauseRecord : clausesByReference) {
				clauseRecord = documentClauseRecord.getClauseRecord();
				
				// CJ-843, append effective date to the title
				String sTitleDate = clauseRecord.getClauseTitleText();
				if (clauseRecord.getEffectiveMonthYear().length() > 0)
					sTitleDate = sTitleDate + " (" + clauseRecord.getEffectiveMonthYear() + ")";
				
		        writeClauseReferenceLine(clauseRecord.getClauseName(),
		        		sTitleDate, // clauseRecord.getClauseTitleText(), 	// CJ-843
		        		""); //clauseRecord.getEffectiveMonthYear()); 		// CJ-843
			}
		}

		if (!clausesByText.isEmpty()) {
			this.writeLine(false, "");
			this.writeLine(false, "");
			this.writeLine(true, clauseType + " CLAUSES INCLUDED BY FULL TEXT");
			this.writeLine(false, "");
			ArrayList<DocumentClauseRecord> altClauses = new ArrayList<DocumentClauseRecord>(); 
			int iRecord = 0;
			while (iRecord < clausesByText.size()) {
				DocumentClauseRecord documentClauseRecord = clausesByText.get(iRecord++);
				clauseRecord = documentClauseRecord.getClauseRecord();
				String sNamePrefix = clauseRecord.getClauseName().toUpperCase(Locale.ENGLISH).split(" ")[0];
				
				boolean isBasicClause = clauseRecord.isBasicClause();
				String sClauseName = clauseRecord.getClauseName();
				if(sClauseName.toUpperCase(Locale.ENGLISH).contains("ALTERNATE")) //Alternate added with no non-alternate present
					isBasicClause = true;
				
				this.writeHeader(true, clauseRecord.getHeading(documentClauseRecord, false));
	        	
				DocumentFillInsTable aFillIns = this.clsDocument.getFillIns().collectByClause(clauseRecord.getId().intValue());
				// CJ-533, support editables with fillins
				String clauseText = "";
				if (clauseRecord.isEditable() 
						&& (clauseRecord.getEditableRemarks() != null)
						&& ((clauseRecord.getEditableRemarks().equals("ALL")) 
						|| (clauseRecord.getEditableRemarks().startsWith("ALL")))) {
					
					String sFillInName = clauseRecord.getClauseData();
					sFillInName = sFillInName.replace("{{", "");
					sFillInName = sFillInName.replace("}}", "");
					DocumentFillInsRecord oDocFillIn = aFillIns.findByClauseFillInCode(sFillInName);
					
					if (oDocFillIn != null)
						clauseText = clauseRecord.getClauseDataWFillins(aFillIns, oDocFillIn.getAnswer(), ClsConstants.OutputType.PDF);
					else {
						clauseText = clauseRecord.getClauseData(aFillIns, ClsConstants.OutputType.PDF);
						clauseText = clauseRecord.getClauseDataWFillins(aFillIns, clauseText, ClsConstants.OutputType.PDF);
					}
				}
				// CJ-516
				else if (clauseRecord.isEditable() 
						&& (clauseRecord.getEditableRemarks() != null)
						&& (clauseRecord.getEditableRemarks().startsWith("PARENT"))) {
					
					String sContent = clauseRecord.getClauseData();

					Integer	pos1 = sContent.indexOf("{{", 0);
					Integer	pos2 = sContent.indexOf("}}", 0);
					String sFillInName = "";
						
					if ((pos1 >= 0) &&  (pos2 >= 0)) {
						sFillInName = sContent.substring(pos1+2, pos2);
						DocumentFillInsRecord oDocFillIn = aFillIns.findByClauseFillInCode(sFillInName);
						
						if (oDocFillIn != null) 
							sContent = sContent.replace ("{{" + sFillInName + "}}", oDocFillIn.getAnswer());
						else
							sContent = clauseRecord.getClauseDataWFillins(aFillIns, sContent, ClsConstants.OutputType.PDF);
					}
					clauseText = clauseRecord.getClauseDataWFillins(aFillIns, sContent, ClsConstants.OutputType.PDF);
				}
				else
					clauseText = clauseRecord.getClauseData(aFillIns, ClsConstants.OutputType.PDF);
				clauseText = this.formatClauseData (clauseText); 
				
				this.writeClauseData(clauseText);

	        	this.writeLine(false, clauseRecord.getEndOfProvisonOrClauseText(), true); // CJ-817 ClauseRecord.END_OF_PROVISION, CJ-791
	        	this.writeLine(false, "");
				
				while (iRecord < clausesByText.size()) {
					DocumentClauseRecord altDocumentClauseRecord = clausesByText.get(iRecord);
					ClauseRecord altClauseRecord = altDocumentClauseRecord.getClauseRecord();
					String altClauseName = altClauseRecord.getClauseName().toUpperCase(Locale.ENGLISH);
					if (altClauseName.toUpperCase(Locale.ENGLISH).startsWith(sNamePrefix) && 
							(altClauseName.contains("ALTERNATE") || altClauseName.contains("DEVIATION"))) {
						altClauses.add(altDocumentClauseRecord);
						iRecord++;
					} else
						break;
				}
				//String sHeading = clauseRecord.getHeading(documentClauseRecord, false);
				
				for (DocumentClauseRecord altDocumentClauseRecord : altClauses) {
					ClauseRecord altClauseRecord = altDocumentClauseRecord.getClauseRecord();
					this.writeHeader(true, altClauseRecord.getHeading(altDocumentClauseRecord, false)); // CJ-791
					/*
					String altHeading =  altClauseRecord.getHeading(altDocumentClauseRecord, false);
					if(isBasicClause) {
						this.writeLine(true, altHeading);
						this.writeLine(true, "");
					}
					*/
					aFillIns = this.clsDocument.getFillIns().collectByClause(altClauseRecord.getId().intValue());
					// CJ-1336
					String sFillInName = clauseRecord.getClauseData();
					sFillInName = sFillInName.replace("{{", "");
					sFillInName = sFillInName.replace("}}", "");
					DocumentFillInsRecord oDocFillIn = aFillIns.findByClauseFillInCode(sFillInName);
					
					if (oDocFillIn != null)
						clauseText = altClauseRecord.getClauseDataWFillins(aFillIns, oDocFillIn.getAnswer(), ClsConstants.OutputType.PDF);
					else {
						clauseText = altClauseRecord.getClauseData(aFillIns, ClsConstants.OutputType.PDF);
						clauseText = altClauseRecord.getClauseDataWFillins(aFillIns, clauseText, ClsConstants.OutputType.PDF);
					} 
					// clauseText = altClauseRecord.getClauseData(aFillIns, ClsConstants.OutputType.PDF);
					// end CJ-1336
					
					this.writeClauseData(clauseText);
					//if (isBasicClause) { // CJ-817 removed condition
						this.writeLine(false, clauseRecord.getFooterText(), true);
						this.writeLine(false, "");
					//}
				}
				
				/*
		        if(!isBasicClause) {
		        	this.writeLine(false, clauseRecord.getFooterText());
		        	this.writeLine(false, "");
		        }
		        */
				altClauses.clear();
			}
			
		}
        this.writeLine(false, "");
	}
	
	// Editable clauses may include fonts, that throw off the PDF formatting.
	private String formatClauseData(String clauseText) throws Exception {
		
		ArrayList<String> paraTags = new ArrayList<String>();
		
		// Replace any paragraph tags with attributes. PDF won't display
		int pos = -1;
		while (true) {
			pos = clauseText.indexOf("<p ", pos + 1);
			if (pos < 0)
				break;
			int posEnd = clauseText.indexOf ("p>", pos); // CJ-1105 changed from ">"
			if (posEnd < 0)
				break;
			
			String sTagAttr = clauseText.substring(pos, posEnd + 2); // CJ-1105 changed from posEnd+1
			
			if (!paraTags.contains(sTagAttr))
				paraTags.add(sTagAttr);
		}
			
		for (String paraLine : paraTags){
			clauseText = clauseText.replace(paraLine, "<p>");
		}
		
		clauseText = clauseText.replace("<br/>", ClsConstants.HTML_BR);
		clauseText = clauseText.replace("<br>", ClsConstants.HTML_BR);
		/* // CJ-1105
		clauseText = clauseText.replace("<br />", "<p>");
		clauseText = clauseText.replace("<br/>", "<p>");
		clauseText = clauseText.replace("<br>", "<p>");
		*/
		
		// Additional string that PDF cannot handle.
		clauseText = clauseText.replace ("&squ;", "[<span class='spanData'>_____</span>]");
		clauseText = clauseText.replace ("&#x25a1;", "[<span class='spanData'>_____</span>]");
		
		clauseText = clauseText.replace ("&rdquor;", "\"");
		clauseText = clauseText.replace ("&rdquo;", "\"");
		clauseText = clauseText.replace ("&ldquo;", "\"");
		
		return clauseText;
	}
	
	private void writeClauseData(String clauseText) throws Exception {
		clauseText = clauseText.replaceAll("<table","<p><table");
		clauseText = clauseText.replaceAll("</table>","</p></table>"); // "/table>","</p></table>"
		clauseText = formatImgTags (clauseText); // CJ-1052, Check for embedded image tags.
		
        String[] clauseTextElements = clauseText.split("<p>");
        for (String sSection : clauseTextElements) {
        	if (sSection.endsWith("</p>")) { // CJ-1105
        		sSection = sSection.substring(0, sSection.length() - "</p>".length());
        	}
        	if (sSection.contains("<table")) {
        		drawTable(sSection);
	        	this.writeLine(false, "");
        	}
        	else if (sSection.contains("<img")) {
        		String sUrl = getImgSrc(sSection);
        		drawImage(sUrl);
	        	this.writeLine(false, "");
        	}
        	else if (sSection != "") {
        		String[] aH1 = sSection.split("<h1>");
    			for (String sParagraph : aH1) {
    				int iPos = sParagraph.indexOf("</h1>");
    				if (iPos > 0) {
    					String sH1Text = sParagraph.substring(0, iPos);
    					this.writeLine(true, sH1Text, true);
    					sParagraph = sParagraph.substring(iPos + "</h1>".length());
    				}
    				String[] aBreakedLines = sParagraph.split(ClsConstants.HTML_BR); // CJ-1105
    				//ArrayList<String> listBreaks = new ArrayList<String>(aBreakedLines.length);
    				for (String sBrLine : aBreakedLines) {
    					//listBreaks.add(sBrLine);
    					ArrayList<String> lines = getParagraphLines(Jsoup.parse(sBrLine).text(), pageWidth);
        	        	for (String line : lines) {
        	        		this.writeLine(false, line);
        	        	}
        	        	if (aBreakedLines.length > 0)
	        				currentY -= 0.5f * FONT_SIZE;
    				}
    				/*
    	        	ArrayList<String> lines = getParagraphLines(Jsoup.parse(sParagraph).text(), pageWidth);
    	        	for (String line : lines) {
    	        		this.writeLine(false, line);
    	        	}
    	        	*/
    			}
    			/*
	        	ArrayList<String> lines = getParagraphLines(Jsoup.parse(sSection).text(), pageWidth);
	        	for (String line : lines) {
	        		this.writeLine(false, line);
	        	}
	        	*/
	        	this.writeLine(false, "");
        	}
        }
	}
	
	//Takes a string and returns the lines based on allowed width
	private ArrayList<String> getParagraphLines(String text, float width) throws Exception{
		ArrayList<String> lines = new ArrayList<String>();
	    int lastSpace = -1;
	    while (text.length() > 0) {
	        int spaceIndex = text.indexOf(' ', lastSpace + 1);
	        if (spaceIndex < 0) {
	        	// CJ-511, if substring is longer than col length
	        	float fsize = FONT_SIZE * FONT.getStringWidth(text) / 1000;
	        	if (fsize < width)
	        		lines.add(text);
	        	// last substring broken on a space
	        	else if (text.indexOf(' ', lastSpace) >= 0) {
	        		lines.add (text.substring (0, lastSpace));
	        		lines.add (text.substring (lastSpace + 1));
	        	}
	        	// long string of characters, not broken by space.
	        	else {
	        		int iLines = (int) (fsize / width) + 1;
	        		int iCharsMax = (text.length() / iLines) -1;
	        		int pBeg, pEnd = 0;
	        		for (int i = 0; i<iLines; i++) {
	        			pBeg = i*iCharsMax;
	        			pEnd = pBeg + iCharsMax;
	        			lines.add(text.substring(pBeg, pEnd));
	        		}
	        		if (pEnd<=text.length())
	        			lines.add(text.substring(pEnd));
	        	}
	        		
	            text = "";
	        }
	        else {
	            String subString = text.substring(0, spaceIndex);
	            float size = FONT_SIZE * FONT.getStringWidth(subString) / 1000;
	            size += 5;	// CJ-511, add padding to the column text.
	            if (size > width) {
	                if (lastSpace < 0)
	                    lastSpace = spaceIndex;
	                subString = text.substring(0, lastSpace);
	                //lines.add(subString);
	                
		        	// CJ-511, if substring is longer than col length
		        	float fsize = FONT_SIZE * FONT.getStringWidth(subString) / 1000;
		        	if (fsize < width)
		        		lines.add(subString);
		        	else {
		        		int iLines = (int) (fsize / width) + 1;
		        		int iCharsMax = subString.length() / iLines;
		        		int pBeg, pEnd = 0;
		        		for (int i = 0; i<iLines; i++) {
		        			pBeg = i*iCharsMax;
		        			pEnd = pBeg + iCharsMax;
		        			lines.add(subString.substring(pBeg, pEnd));
		        		}
		        		if (pEnd<=subString.length())
		        			lastSpace -= subString.length() - pEnd;
		        			//lines.add(subString.substring(pEnd));
		        	}
		        	// end CJ-511
		        	
	                text = text.substring(lastSpace).trim();
	                lastSpace = -1;
	            }
	            else {
	                lastSpace = spaceIndex;
	            }
	        }
	    }
		return lines;
	}
	
	// CJ-511, For very large titles, we will need to breakup line
	private void writeHeader(boolean bold, String header) throws Exception{
		ArrayList<String> lines = getParagraphLines(Jsoup.parse(header).text(), pageWidth);
    	for(String line : lines){
    		this.writeLine(true, line);
    	}
    	//this.writeLine(false, "");
	}
	
	private void writeLine(boolean bold, String line) throws Exception{
		this.writeLine(bold, line, false);
	}
	
	private void writeLine(boolean bold, String line, boolean center) throws Exception{
		line = line.replaceAll("[^\\p{ASCII}]", ""); //strip all non-ASCII, find better solution later
		
		checkPageSpace(0);
		
		PDFont oPdfFont = (bold ? FONT_BOLD : FONT);
		contentStream.setFont(oPdfFont, FONT_SIZE);
		
    	if (center) { // CJ-800
    		float fTextWidth = oPdfFont.getStringWidth(line) / 1000 * FONT_SIZE;
    		// float fMargin = (page.getMediaBox().getWidth() - fTextWidth) / 2;
    		float fMargin = (pageWidth - fTextWidth) / 2;
    		
    		contentStream.moveTextPositionByAmount(fMargin, -leading);
        	contentStream.drawString(line);
        	contentStream.moveTextPositionByAmount(fMargin * -1, -leading);
    	} else {
    		if (line.contains(ClsConstants.HTML_BR)) { // CJ-1105
    			// http://stackoverflow.com/questions/7598099/how-to-insert-a-linefeed-with-pdfbox-drawstring
            	contentStream.drawString(line);
    		} else {
            	contentStream.drawString(line);
    		}
    		contentStream.moveTextPositionByAmount(0, -leading);
    	}
        currentY -= leading;
		contentStream.setFont(FONT, FONT_SIZE);
	}
	
	private void writeClauseReferenceLine(String number, String title, String effdate) throws Exception{
		number=number.replaceAll("[^\\p{ASCII}]", "");
		number=number.replaceAll("Alternate", "Alt");
		title=title.replaceAll("[^\\p{ASCII}]", "");
		effdate = effdate.replaceAll("[^\\p{ASCII}]", "");
		
		ArrayList<String> numberLines = getParagraphLines(number, pageWidth*0.2f);
		ArrayList<String> titleLines = getParagraphLines(title, pageWidth*0.7f); // CJ-843, increase col size for title+date
		
		int extraLinesNeeded = Math.max(numberLines.size(), titleLines.size())-1;
		
		checkPageSpace(extraLinesNeeded*leading);
		
		contentStream.moveTextPositionByAmount(0, -leading);
		
		int yDisplace = 0;
		for(String line : numberLines){
			contentStream.drawString(line);
			contentStream.moveTextPositionByAmount(0, -leading);
			yDisplace+=leading;
		}
		contentStream.moveTextPositionByAmount(0, yDisplace);

		yDisplace = 0;
		contentStream.moveTextPositionByAmount(pageWidth*0.25f, 0);
		for(String line : titleLines){
			contentStream.drawString(line);
			contentStream.moveTextPositionByAmount(0, -leading);
			yDisplace+=leading;
		}
		contentStream.moveTextPositionByAmount(-pageWidth*0.25f, yDisplace);
		
		/* // CJ-843
		contentStream.moveTextPositionByAmount(pageWidth*0.9f, 0);
		contentStream.drawString(effdate);
		contentStream.moveTextPositionByAmount(-pageWidth*0.9f, 0);
    	*/
		
		contentStream.moveTextPositionByAmount(0, -extraLinesNeeded*leading);
        currentY-=((extraLinesNeeded+1)*leading);
	}
	
	private void drawTable(String tableHTML) throws Exception{
		tableHTML = tableHTML.replace("<th", "<td");
		tableHTML = tableHTML.replace("</th", "</td");
		tableHTML = tableHTML.replaceAll("[^\\p{ASCII}]", "");
		ArrayList<ArrayList<String>> cells = new ArrayList<ArrayList<String>>();
		Document tableDoc = Jsoup.parse(tableHTML);
		tableDoc.select("span").unwrap();
		Elements rows = tableDoc.select("tr");
		int numCols = 0;
		
		//get table data from html
		for(Element row : rows){
			ArrayList<String> rowData = new ArrayList<String>();
			Elements rowNodes = row.children();
			
			if(rowNodes.size() > numCols)
				numCols = rowNodes.size();
			
			for(Element data : rowNodes){
				rowData.add(data.ownText());
			}
			cells.add(rowData);
		}
		float cellWidth = (1f/(numCols))*(pageWidth-(10*numCols));
		
		int totalLines = 0;
		ArrayList<Integer> rowLineLocations = new ArrayList<Integer>();
		
		//figure out space needed for table
		for(ArrayList<String> row : cells){ //each row
			int rowLinesNeeded = 0;
			for(String cellData : row){ // each cell
				ArrayList<String> lines = getParagraphLines(cellData,cellWidth);
				if(lines.size() > rowLinesNeeded)
					rowLinesNeeded = lines.size();
			}
			totalLines+=rowLinesNeeded;
			rowLineLocations.add(totalLines);
		}
		
		float ySpaceNeeded = (leading*totalLines);
		checkPageSpace(ySpaceNeeded);
		
		//draw table lines
		contentStream.endText();
		contentStream.drawLine(currentX, currentY, currentX+pageWidth, currentY);
		contentStream.drawLine(currentX, currentY, currentX, currentY-(leading*totalLines)-(0.25f*leading));
		
		for(int x = 1; x < numCols; x++){
			contentStream.drawLine(currentX+(pageWidth/numCols)*x, currentY,currentX+(pageWidth/numCols)*x, currentY-(leading*totalLines)-(0.25f*leading));
		}
		
		for(int y : rowLineLocations){
			contentStream.drawLine(currentX, currentY-(y*leading)-(0.25f*leading), currentX+pageWidth, currentY-(y*leading)-(0.25f*leading));
		}

		contentStream.drawLine(currentX+pageWidth, currentY, currentX+pageWidth, currentY-(leading*totalLines)-(0.25f*leading));

		contentStream.beginText();
		contentStream.moveTextPositionByAmount(currentX+(0.5f*leading), currentY-(leading));		
		
		//draw the table text
		for(ArrayList<String> row : cells){ //each row
			float xDisplace = 0;
			int rowLinesNeeded = 0;
			for(String cellData : row){ // each cell
				ArrayList<String> lines = getParagraphLines(cellData,cellWidth);
				if(lines.size() > rowLinesNeeded)
					rowLinesNeeded = lines.size();
				int yDisplace = 0;
				for(String cellLine : lines){ // each line in cell
					contentStream.drawString(cellLine);
					contentStream.moveTextPositionByAmount(0, -leading);
					yDisplace+=leading;
				}
				contentStream.moveTextPositionByAmount(cellWidth+10, yDisplace);
				xDisplace = xDisplace+cellWidth+10;
			}
			contentStream.moveTextPositionByAmount(-xDisplace, -(leading*(rowLinesNeeded)));
			currentY-=leading*rowLinesNeeded;
		}
		this.writeLine(false,"");
	}
	
	private void checkPageSpace(float ySpaceNeeded) throws IOException{
		if((currentY-ySpaceNeeded) < MARGIN){
    	    contentStream.endText(); 
    	    contentStream.close();
    	    page = new PDPage();
    	    doc.addPage(page);
    	    contentStream = new PDPageContentStream(doc, page);
    		contentStream.beginText();
    		contentStream.setFont(FONT, FONT_SIZE);
    		contentStream.moveTextPositionByAmount(startX, startY);
    		currentY=startY;
    		currentX=startX;
		}
	}
	
	private ArrayList<DocumentClauseRecord> sortClauses(ArrayList<DocumentClauseRecord> srcClauses) {
		
		ArrayList<DocumentClauseRecord> dstClauses = new ArrayList<DocumentClauseRecord>(); 

		for (DocumentClauseRecord srcRecord : srcClauses) {
			Integer ndx = 0; 
			Boolean bAdded = false;
			if (dstClauses.size() == 0) {
				dstClauses.add(0, srcRecord);
				continue;
			}
			for (DocumentClauseRecord destRecord : dstClauses) {

				if (ClauseTable.compareClauseName(srcRecord.getClauseName(), destRecord.getClauseName())<0) {
					dstClauses.add(ndx, srcRecord);
					bAdded = true;
					break;
				}
				ndx++;
			}
			if (!bAdded) {
				dstClauses.add(srcRecord);
			}
			
		}
		
		srcClauses.clear();
		return dstClauses;
	}
	
	
	// CJ-1052, Check for embedded image tags. Wrap them with <p> tags
	// Note: can't use Jsoup.parse(), which changes the outerHtml string returned.
	private String formatImgTags (String clauseTxt) {
		
		Integer iImgBegPos = clauseTxt.indexOf("<img ");
		while (iImgBegPos > 0)
		{
			Integer iImgEndPos = clauseTxt.indexOf("/>",iImgBegPos);
			if (iImgEndPos < 0)
				iImgBegPos = 0;
			else {
				String sOrig = clauseTxt.substring (iImgBegPos, iImgEndPos + "/>".length());
				String sReplace =  "<p>" + sOrig + "</p>";
				clauseTxt = clauseTxt.replace(sOrig, sReplace);
				iImgBegPos = clauseTxt.indexOf("<img ", iImgEndPos);
			}
		}
		return clauseTxt;
	}
	
	// Example tag:
	// <img src="http://www.ecfr.gov/graphics/er31mr14.000.gif" alt="eCFR graphic er31mr14.000.gif" />
	private String getImgSrc (String tag) {
		
		Document doc = Jsoup.parse(tag);
		Element link = doc.select("img").first();
		String srcRef = link.attr("src");
		return srcRef;
	}
	
	// CJ-1052, Check for embedded image tags. Draw image.
	// Example file names:
	// "https://3.bp.blogspot.com/-tJKBKMMFxls/VBgQZlBmRiI/AAAAAAAAQ1w/AMDO2UP-lf4/w1200-h630-p-nu/Canon%2BEOS%2B7D%2BMark%2BII%2BSample%2BJPEG%2BImage%2B01.jpg";
    // "http://www.ecfr.gov/graphics/er31mr14.000.gif";
	
	private boolean drawImage (String sFileImage) throws IOException {
		
		boolean bResult = true; 
	    
	    PDXObjectImage image = null;
	    
	    try {
	    	
	    	URL url = new URL(sFileImage);
	    	BufferedImage bufferImage;
		
			//bufferImage = ImageIO.read(new File(sFileImage)); -- use for files
	    	bufferImage = ImageIO.read(url);
			image = new PDPixelMap(doc, bufferImage);
		    
			// reduce size until entire pic fits on a single page.
		    float fWidth = image.getWidth();
		    float fHeight = image.getHeight();
		    while ((fWidth > pageWidth) || (fHeight > pageHeight)){
		    	fWidth = fWidth/2;
		    	fHeight = fHeight/2;
		    }

		    checkPageSpace(fHeight);
		    contentStream.endText();
		    
		    contentStream.drawXObject(image, MARGIN, currentY-fHeight, fWidth, fHeight);

		    currentY -= fHeight;
		    contentStream.beginText();
    		contentStream.setFont(FONT, FONT_SIZE);
    		contentStream.moveTextPositionByAmount(startX, currentY);
		    
	    } catch (FileNotFoundException e) {
	    	bResult = false;
			e.printStackTrace();
		} catch (IOException e) {
			bResult = false;
			e.printStackTrace();
		}

	    
	    return bResult;
	}

}