package gov.dod.cls.bean.utils;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.net.URL;
import java.util.ArrayList;
import java.util.Locale;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.pdfbox.pdmodel.graphics.xobject.PDPixelMap;
import org.apache.pdfbox.pdmodel.graphics.xobject.PDXObjectImage;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.util.Units;
import org.apache.poi.xwpf.usermodel.ParagraphAlignment;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.apache.poi.xwpf.usermodel.XWPFStyles;
import org.apache.poi.xwpf.usermodel.XWPFTable;
import org.apache.poi.xwpf.usermodel.XWPFTableCell;
import org.apache.poi.xwpf.usermodel.XWPFTableRow;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTFonts;

import gov.dod.cls.PageApi;
import gov.dod.cls.bean.ClsDocument;
import gov.dod.cls.db.ClauseRecord;
import gov.dod.cls.db.ClauseSectionRecord;
import gov.dod.cls.db.ClauseSectionTable;
import gov.dod.cls.db.ClauseTable;
import gov.dod.cls.db.DocumentClauseRecord;
import gov.dod.cls.db.DocumentClauseTable;
import gov.dod.cls.db.DocumentFillInsRecord;
import gov.dod.cls.db.DocumentFillInsTable;
import gov.dod.cls.db.RegulationTable;
import gov.dod.cls.utils.ClsConstants;
import gov.dod.cls.utils.CommonUtils;

public class DocGenerateDoc {
	
	public static final int 	SECTION_HEADER_FONT_SIZE = 16; // 14
	public static final int 	DEFAULT_FONT_SIZE = 12;
	public static final String 	SECTION_HEADER_CLAUSES_BY_REF = "CLAUSES INCORPORATED BY REFERENCE";
	public static final String 	SECTION_HEADER_CLAUSES_BY_FULL_TEXT = "CLAUSES INCORPORATED BY FULL TEXT";
	
	public static void downloadDoc(ClsDocument clsDocument, HttpServletRequest req, HttpServletResponse resp) {
		DocGenerateDoc docGenerateDoc = new DocGenerateDoc(clsDocument);
		try {
			PageApi.send(resp, 
					"attachment; filename=" + clsDocument.genFileName("docx"),
					"application/msword",
					docGenerateDoc.createDoc());
		} catch (Exception oError) {
			oError.printStackTrace();
			PageApi.send(resp, "text/plain", oError.getMessage());
		}
	}
	
	private ClsDocument clsDocument;
	
	public DocGenerateDoc(ClsDocument clsDocument) {
		this.clsDocument = clsDocument;
		RegulationTable.getInstance(null);
	}
	
	public static void insertParagraphIntoTable(XWPFTableCell cell, String text, boolean bold) {
		XWPFParagraph newPara = cell.addParagraph();
		XWPFRun run = newPara.createRun(); 
		run.setBold(bold);
		run.setText(text);
		run.setFontSize(DEFAULT_FONT_SIZE); // CJ-796
		newPara.setAlignment(ParagraphAlignment.LEFT); 
	}
	
	public XWPFDocument createDoc() throws Exception {
        XWPFDocument document = new XWPFDocument();
        
        CTFonts fonts = CTFonts.Factory.newInstance();
        fonts.setHAnsi("Times New Roman");

        XWPFStyles xWpfStyles = document.createStyles();
        xWpfStyles.setDefaultFonts(fonts);
        //xWpfStyles.
        
        XWPFTable table = document.createTable();
        XWPFTableRow nextRow = table.getRow(0);
        nextRow.addNewTableCell();
        DocGenerateDoc.insertParagraphIntoTable(nextRow.getCell(0), "Acquisition Title", true);
        DocGenerateDoc.insertParagraphIntoTable(nextRow.getCell(1), this.clsDocument.getAcquisitionTitle(), false);
        
        nextRow = table.createRow();  
        DocGenerateDoc.insertParagraphIntoTable(nextRow.getCell(0), "Document Number", true);
        DocGenerateDoc.insertParagraphIntoTable(nextRow.getCell(1), this.clsDocument.getDocumentNumber(), false);  
        
        nextRow = table.createRow();  
        DocGenerateDoc.insertParagraphIntoTable(nextRow.getCell(0), "Document Type", true);
        DocGenerateDoc.insertParagraphIntoTable(nextRow.getCell(1), this.clsDocument.getDocumentTypeName(), false);  
        
        nextRow = table.createRow();  
        DocGenerateDoc.insertParagraphIntoTable(nextRow.getCell(0), "Creation Date", true);
        DocGenerateDoc.insertParagraphIntoTable(nextRow.getCell(1), CommonUtils.toDateTimeBrief(this.clsDocument.getCreatedAt()), false);  
        
        nextRow = table.createRow();  
        DocGenerateDoc.insertParagraphIntoTable(nextRow.getCell(0), "Last Updated", true);
        DocGenerateDoc.insertParagraphIntoTable(nextRow.getCell(1), CommonUtils.toDateTimeBrief(this.clsDocument.getUpdatedAt()), false);  
        
        int[] cols = {3000, 10000};
        for (int i = 0; i < table.getNumberOfRows(); i++){
            XWPFTableRow row = table.getRow(i);
            int numCells = row.getTableCells().size();
            for (int j = 0; j < numCells; j++){
                XWPFTableCell cell = row.getCell(j);
                cell.getCTTc().addNewTcPr().addNewTcW().setW(BigInteger.valueOf(cols[j]));
                
            }
        }
        table.getCTTbl().getTblPr().unsetTblBorders();
        
        XWPFRun run = document.createParagraph().createRun();
        run.setFontSize(DEFAULT_FONT_SIZE); // CJ-796
        run.addBreak();
        
        DocumentClauseTable documentClauseTable = this.clsDocument.getDocumentClauseTable();
		DocumentFillInsTable docFillinsTable = this.clsDocument.getFillIns();
		ClauseSectionTable clauseSectionTable = ClauseSectionTable.getInstance(null);

		ArrayList<DocumentClauseRecord> farClausesByReference = new ArrayList<DocumentClauseRecord>(); 
		ArrayList<DocumentClauseRecord> farClausesByText = new ArrayList<DocumentClauseRecord>(); 

		ArrayList<DocumentClauseRecord> dfarClausesByReference = new ArrayList<DocumentClauseRecord>(); 
		ArrayList<DocumentClauseRecord> dfarClausesByText = new ArrayList<DocumentClauseRecord>(); 
		
		ArrayList<DocumentClauseRecord> localClausesByReference = new ArrayList<DocumentClauseRecord>(); 
		ArrayList<DocumentClauseRecord> localClausesByText = new ArrayList<DocumentClauseRecord>(); 

		ArrayList<DocumentClauseRecord> clausesByReference; 
		ArrayList<DocumentClauseRecord> clausesByText; 

		StringBuffer sb = new StringBuffer();
        for (ClauseSectionRecord clauseSectionRecord : clauseSectionTable) {
			ClauseTable clauseTable = clsDocument.getClauses().subsetBySection(clauseSectionRecord.getId(), true);
			for (ClauseRecord clauseRecord : clauseTable) {
				DocumentClauseRecord documentClauseRecord = documentClauseTable.findByClauseId(clauseRecord.getId(), true);
				//if (documentClauseRecord != null) {
				if ((documentClauseRecord != null) && (!documentClauseRecord.isRemoved())) {
					documentClauseRecord.setClauseRecord(clauseRecord);
					
					if (clauseRecord.isFarClause()) {
						clausesByReference = farClausesByReference; 
						clausesByText = farClausesByText;
					} else if (clauseRecord.isDfarClause()) {
						clausesByReference = dfarClausesByReference; 
						clausesByText = dfarClausesByText;
					} else {
						clausesByReference = localClausesByReference; 
						clausesByText = localClausesByText;
					} 
					//if (clauseRecord.isFullText())
					if (documentClauseRecord.isEditFullText(docFillinsTable))
						clausesByText.add(documentClauseRecord);
					else
						clausesByReference.add(documentClauseRecord);
					/*
					if ("F".equals(clauseRecord.getInclusion()))
						clausesByText.add(documentClauseRecord);
					else
						clausesByReference.add(documentClauseRecord);
					*/
				}
			}

			// CJ-1206, Sort the clauses
			farClausesByReference = sortClauses(farClausesByReference);
			farClausesByText = sortClauses(farClausesByText);
			dfarClausesByReference = sortClauses(dfarClausesByReference);
			dfarClausesByText = sortClauses(dfarClausesByText);
			localClausesByReference = sortClauses(localClausesByReference);
			localClausesByText = sortClauses(localClausesByText);
			
			if (!(farClausesByText.isEmpty() && farClausesByReference.isEmpty() &&
					dfarClausesByText.isEmpty() && dfarClausesByReference.isEmpty() &&
					localClausesByText.isEmpty() && localClausesByReference.isEmpty() )) {
			//	if ((!clausesByText.isEmpty()) || (!clausesByReference.isEmpty())) {
				
				XWPFParagraph paragraph = document.createParagraph();
		        run = paragraph.createRun();
		        run.setBold(true);
		        run.setText(clauseSectionRecord.getFullName());
		        run.setFontSize(SECTION_HEADER_FONT_SIZE);
		        run.addBreak();
		        
				this.generateSection(sb, ClauseRecord.FAR_CLAUSE, farClausesByReference, farClausesByText, document);
				this.generateSection(sb, ClauseRecord.DFAR_CLAUSE, dfarClausesByReference, dfarClausesByText, document);
				this.generateSection(sb, ClauseRecord.LOCAL_CLAUSE, localClausesByReference, localClausesByText, document);

				farClausesByReference.clear();
				farClausesByText.clear();

				dfarClausesByReference.clear(); 
				dfarClausesByText.clear(); 
				
				localClausesByReference.clear(); 
				localClausesByText.clear(); 
			}
		}
        
        return document;
    }
	
	private void generateSectionReferences(String clauseType, ArrayList<DocumentClauseRecord> clausesByReference,
			XWPFDocument document) {
		ClauseRecord clauseRecord;
		XWPFParagraph paragraph;
        XWPFRun run;
        
        paragraph = document.createParagraph();
        run = paragraph.createRun();
        run.setBold(true);
        run.setText(clauseType + " " + SECTION_HEADER_CLAUSES_BY_REF);
        run.setFontSize(DEFAULT_FONT_SIZE); // CJ-796
        run.addBreak();
        run.addBreak();
        
		XWPFTable table = document.createTable(clausesByReference.size(), 2); // CJ-843
		table.setWidth(1000);
        table.setCellMargins(0, 0, 0, 0);
       
        XWPFTableRow tableRow = table.getRow(0);   
        int rowNum = 0;
        for (DocumentClauseRecord documentClauseRecord : clausesByReference) {
        	tableRow = table.getRow(rowNum++);   
			clauseRecord = documentClauseRecord.getClauseRecord();
			
			// CJ-843, append effective date to the title
			String sTitleDate = clauseRecord.getClauseTitleText();
			if (clauseRecord.getEffectiveMonthYear().length() > 0)
				sTitleDate = sTitleDate + " (" + clauseRecord.getEffectiveMonthYear() + ")";
			
			tableRow.getCell(0).setText(clauseRecord.getClauseName());  
	        tableRow.getCell(1).setText(sTitleDate); // (clauseRecord.getClauseTitleText()); // CJ-843
	        // tableRow.getCell(2).setText(clauseRecord.getEffectiveMonthYear()); 			// CJ-843
		}
        
        int[] cols = {3000, 12000}; // CJ-843, 2200};
        for (int i = 0; i < table.getNumberOfRows(); i++){
            XWPFTableRow row = table.getRow(i);
            int numCells = row.getTableCells().size();
            for (int j = 0; j < numCells; j++){
                XWPFTableCell cell = row.getCell(j);
                cell.getCTTc().addNewTcPr().addNewTcW().setW(BigInteger.valueOf(cols[j]));
            }
        }
        table.getCTTbl().getTblPr().unsetTblBorders();
        
        paragraph = document.createParagraph();
        run = paragraph.createRun();
        run.setFontSize(DEFAULT_FONT_SIZE); // CJ-796
        run.addBreak();
	}
	
	private void generateSection(StringBuffer sb, String clauseType, // ClauseSectionRecord clauseSectionRecord,
			ArrayList<DocumentClauseRecord> clausesByReference, ArrayList<DocumentClauseRecord> clausesByText,
			XWPFDocument document) {
		ClauseRecord clauseRecord;
		XWPFParagraph paragraph;
        XWPFRun run;
        
		if (!clausesByReference.isEmpty()) {
			this.generateSectionReferences(clauseType, clausesByReference, document);
		}
		
		if (!clausesByText.isEmpty()) {
			paragraph = document.createParagraph();
	        run = paragraph.createRun();
	        run.setText(clauseType + " " + SECTION_HEADER_CLAUSES_BY_FULL_TEXT);
	        run.setBold(true);
	        run.setFontSize(DEFAULT_FONT_SIZE); // CJ-796
	        run.addBreak();
	        run.addBreak();

	        ArrayList<DocumentClauseRecord> altClauses = new ArrayList<DocumentClauseRecord>(); 
			int iRecord = 0;
			while (iRecord < clausesByText.size()) {
				DocumentClauseRecord documentClauseRecord = clausesByText.get(iRecord++);
				clauseRecord = documentClauseRecord.getClauseRecord();
				String baseClauseName = clauseRecord.getClauseName().toUpperCase(Locale.ENGLISH);
				String sNamePrefix = baseClauseName.split(" ")[0];
				while (iRecord < clausesByText.size()) {
					DocumentClauseRecord altDocumentClauseRecord = clausesByText.get(iRecord);
					ClauseRecord altClauseRecord = altDocumentClauseRecord.getClauseRecord();
					String altClauseName = altClauseRecord.getClauseName().toUpperCase(Locale.ENGLISH);
					if (altClauseName.toUpperCase(Locale.ENGLISH).startsWith(sNamePrefix) && 
							(altClauseName.contains("ALTERNATE") || altClauseName.contains("DEVIATION"))) {
						altClauses.add(altDocumentClauseRecord);
						iRecord++;
					} else
						break;
				}
				
				String sHeading = clauseRecord.getHeading(documentClauseRecord, false);
				DocumentFillInsTable aFillIns = this.clsDocument.getFillIns().collectByClause(clauseRecord.getId().intValue());
				// CJ-533, support editables with fillins
				String clauseText = "";
				if (clauseRecord.isEditable() 
						&& (clauseRecord.getEditableRemarks() != null)
						&& ((clauseRecord.getEditableRemarks().equals("ALL")) 
						|| (clauseRecord.getEditableRemarks().startsWith("ALL")))) {
					String sFillInName = clauseRecord.getClauseData();
					sFillInName = sFillInName.replace("{{", "");
					sFillInName = sFillInName.replace("}}", "");
					DocumentFillInsRecord oDocFillIn = aFillIns.findByClauseFillInCode(sFillInName);
					
					if (oDocFillIn != null) {
						clauseText = clauseRecord.getClauseDataWFillins(aFillIns, oDocFillIn.getAnswer(), ClsConstants.OutputType.DOC);
					} else {
						clauseText = clauseRecord.getClauseData(aFillIns, ClsConstants.OutputType.DOC);
						clauseText = clauseRecord.getClauseDataWFillins(aFillIns, clauseText, ClsConstants.OutputType.DOC);
					}
				}
				// CJ-516
				else if (clauseRecord.isEditable() 
						&& (clauseRecord.getEditableRemarks() != null)
						&& (clauseRecord.getEditableRemarks().startsWith("PARENT"))) {
					String sContent = clauseRecord.getClauseData();

					Integer	pos1 = sContent.indexOf("{{", 0);
					Integer	pos2 = sContent.indexOf("}}", 0);
					String sFillInName = "";
						
					if ((pos1 >= 0) &&  (pos2 >= 0)) {
						sFillInName = sContent.substring(pos1+2, pos2);
						DocumentFillInsRecord oDocFillIn = aFillIns.findByClauseFillInCode(sFillInName);
						
						if (oDocFillIn != null) 
							sContent = sContent.replace ("{{" + sFillInName + "}}", oDocFillIn.getAnswer());
						else
							sContent = clauseRecord.getClauseDataWFillins(aFillIns, sContent, ClsConstants.OutputType.DOC);
					}
					clauseText = clauseRecord.getClauseDataWFillins(aFillIns, sContent, ClsConstants.OutputType.DOC);
				}
				else
					clauseText = clauseRecord.getClauseData(aFillIns, ClsConstants.OutputType.DOC);
				
				clauseText = clauseText.replace("<br/>", ClsConstants.HTML_BR);
				clauseText = clauseText.replace("<br>", ClsConstants.HTML_BR);
				/* // CJ-1105
				clauseText = clauseText.replace("<br />", "<p>");
				clauseText = clauseText.replace("<br/>", "<p>");
				clauseText = clauseText.replace("<br>", "<p>");
				*/
				
				clauseText = formatImgTags(clauseText);	// CJ-1052
				
				paragraph = document.createParagraph();
		        run = paragraph.createRun();
		        run.setText(Jsoup.parse(sHeading).text());
		        run.setBold(true);
		        run.setFontSize(DEFAULT_FONT_SIZE); // CJ-796
		        
		        //this.addClauseText(clauseText, document, run);
		        clauseText = clauseText.replace("<table", "<p><table").replaceAll("</table>","</table></p>");
		        String[] clauseTextElements = clauseText.split("<p>");
		        for (String element : clauseTextElements) {
		        	if (element.contains("<table")) {
		        		try {
							drawTable( document, element);
					        run.addBreak();
						} catch (Exception e) {
							e.printStackTrace();
						}
		        	}
		        	else if (element.contains("<img")) {
		        		String sUrl = getImgSrc(element);
		        		try {
							drawImage(document, sUrl);
						} catch (IOException e) {
							e.printStackTrace();
						}
		        		run.addBreak();
		        	}
		        	else if (!element.isEmpty()) {
		        		String[] aH1 = element.split("<h1>");
		    			for (String sSentence : aH1) {
		    				int iPos = sSentence.indexOf("</h1>");
		    				if ((iPos == 0) || (iPos < 0)) {
		    					if (iPos == 0) {
		    						sSentence = sSentence.substring("</h1>".length());
		    					}
		    				} else {
		    					String sH1Text = sSentence.substring(0, iPos);
		    					sSentence = sSentence.substring(iPos + "</h1>".length());

		    					paragraph = document.createParagraph();
		    			        paragraph.setAlignment(ParagraphAlignment.CENTER);
		    			        run = paragraph.createRun();
		    			        run.setFontSize(DEFAULT_FONT_SIZE);
		    			        run.setBold(true);
		    	        		run.setText(Jsoup.parse(sH1Text).text());
		    	    	        run.addBreak();
		    				}
		    				if (!sSentence.isEmpty()) {
		    					paragraph = document.createParagraph();
		    			        run = paragraph.createRun();
		    			        run.setFontSize(DEFAULT_FONT_SIZE); // CJ-796
		    			        if (sSentence.contains(ClsConstants.HTML_BR)) { // CJ-1105
		    			        	String[] aBreaks = sSentence.split(ClsConstants.HTML_BR);
		    			        	for (int iLine = 0; iLine < aBreaks.length; iLine++) {
		    			        		String sBreak = aBreaks[iLine];
			    			        	run.setText(Jsoup.parse(sBreak).text());
			    			        	if (iLine < (aBreaks.length - 1))
			    			        		run.addCarriageReturn();
		    			        	}
		    			        } else {
		    			        	run.setText(Jsoup.parse(sSentence).text());
		    			        }
		    			        run.addBreak();
		    				}
		    			}
//						paragraph = document.createParagraph();
//				        run = paragraph.createRun();
//				        run.setFontSize(DEFAULT_FONT_SIZE); // CJ-796
//		        		run.setText(Jsoup.parse(element).text());
//				        run.addBreak();
		        	}
		        }
		        
				paragraph = document.createParagraph();
		        paragraph.setAlignment(ParagraphAlignment.CENTER);
		        run = paragraph.createRun(); // CJ-791
		        run.setText(clauseRecord.getEndOfProvisonOrClauseText()); // CJ-817 ClauseRecord.END_OF_PROVISION, CJ-791
		        run.setFontSize(DEFAULT_FONT_SIZE); // CJ-796
		        //run.setText(baseClauseName.contains("ALTERNATE") ? ClauseRecord.END_OF_PROVISION : ClauseRecord.END_OF_CLAUSE); // CJ-791
		        run.addBreak();
		        
		        // CJ-728, Nest the Alternate clauses under the basic clauses.		        
				for (DocumentClauseRecord altDocumentClauseRecord : altClauses) {
					ClauseRecord altClauseRecord = altDocumentClauseRecord.getClauseRecord();

					// CJ-791
					sHeading = altClauseRecord.getHeading(altDocumentClauseRecord, false);
					paragraph = document.createParagraph();
			        run = paragraph.createRun();
			        run.setFontSize(DEFAULT_FONT_SIZE); // CJ-796
			        run.setText(Jsoup.parse(sHeading).text());
			        run.setBold(true);
					
					aFillIns = this.clsDocument.getFillIns().collectByClause(altClauseRecord.getId().intValue());
					
					// CJ-1336
					String sFillInName = clauseRecord.getClauseData();
					sFillInName = sFillInName.replace("{{", "");
					sFillInName = sFillInName.replace("}}", "");
					DocumentFillInsRecord oDocFillIn = aFillIns.findByClauseFillInCode(sFillInName);
					
					if (oDocFillIn != null)
						clauseText = altClauseRecord.getClauseDataWFillins(aFillIns, oDocFillIn.getAnswer(), ClsConstants.OutputType.DOC);
					else {
						clauseText = altClauseRecord.getClauseData(aFillIns, ClsConstants.OutputType.DOC);
						clauseText = altClauseRecord.getClauseDataWFillins(aFillIns, clauseText, ClsConstants.OutputType.DOC);
					} 
					// clauseText = altClauseRecord.getClauseData(aFillIns, ClsConstants.OutputType.DOC);
					// end CJ-1336
					
					
			        // this.addClauseText(clauseText, document, run);
					clauseText = clauseText.replace("<table", "<p><table").replaceAll("</table>","</p></table>");
			        clauseTextElements = clauseText.split("<p>");
			        for (String element : clauseTextElements) {
			        	if (element.contains("<table")) {
			        		try {
								drawTable( document, element);
						        run.addBreak();
							} catch (Exception e) {
								e.printStackTrace();
							}
			        	}
			        	else if (!element.isEmpty()) {
			        		String[] aH1 = element.split("<h1>");
			    			for (String sSentence : aH1) {
			    				int iPos = sSentence.indexOf("</h1>");
			    				if ((iPos == 0) || (iPos < 0)) {
			    					if (iPos == 0) {
			    						sSentence = sSentence.substring("</h1>".length());
			    					}
			    				} else {
			    					String sH1Text = sSentence.substring(0, iPos);
			    					sSentence = sSentence.substring(iPos + "</h1>".length());

			    					paragraph = document.createParagraph();
			    			        paragraph.setAlignment(ParagraphAlignment.CENTER);
			    			        run = paragraph.createRun();
			    			        run.setFontSize(DEFAULT_FONT_SIZE);
			    			        run.setBold(true);
			    	        		run.setText(Jsoup.parse(sH1Text).text());
			    	    	        run.addBreak();
			    				}
			    				if (!sSentence.isEmpty()) {
			    					paragraph = document.createParagraph();
			    			        run = paragraph.createRun();
			    			        run.setFontSize(DEFAULT_FONT_SIZE); // CJ-796
			    	        		run.setText(Jsoup.parse(sSentence).text());
							        run.addBreak();
			    				}
			    			}
//							paragraph = document.createParagraph();
//					        run = paragraph.createRun();
//					        run.setFontSize(DEFAULT_FONT_SIZE); // CJ-796
//			        		run.setText(Jsoup.parse(element).text());
			        	}
			        }
			        run.addBreak();

					paragraph = document.createParagraph();
			        paragraph.setAlignment(ParagraphAlignment.CENTER);
			        run = paragraph.createRun(); // CJ-817
			        run.setText(clauseRecord.getEndOfProvisonOrClauseText());
			        run.setFontSize(DEFAULT_FONT_SIZE);
			        run.addBreak();
				}
				
				// CJ-791 removed paragraph = document.createParagraph();
		        // run = paragraph.createRun();
				// run.setText(ClauseRecord.END_OF_CLAUSE);
		        run.addBreak();
		        run.addBreak();
				altClauses.clear();
			}
		}
	}
	
	private void drawTable(XWPFDocument document, String element) throws Exception {
		// strip trailing data.
		int iEnd = element.indexOf("</table></p>");
		if (iEnd > 0) {
			iEnd = iEnd + "</table>".length();
			element = element.substring(0, iEnd);
		}
		
		// get the size of the table.
		Integer iCols = getTableColCnt (element);
		Integer iRows = getTableRowCnt(element);
		
		XWPFTable table2 = document.createTable(iRows, iCols);
		table2.setWidth(1000);
        table2.setCellMargins(0, 10, 0, 10);
        
        
		String tableElement = element;
		tableElement = tableElement.replace("<th", "<td");
		String[] rowElements = tableElement.split("<tr>");
		int iRow = -1; 
        for (String eRow : rowElements) {
        	if (iRow < 0) {
        		iRow++;
        		continue;
        	}
        		
        	int iCol = -1;
        	
        	String[] colElements = eRow.split("<td");
        	for (String eCol : colElements) {
	        	if (iCol < 0) {
	        		iCol++;
	        		continue;
	        	}
        		// remove the html tags.
        		eCol = "<td" + eCol;
        		String noHTMLString = eCol.replaceAll("\\<.*?>","");
        		
		        XWPFParagraph paragraph = table2.getRow(iRow).getCell(iCol).getParagraphs().get(0);
		        XWPFRun run = paragraph.createRun();
		        run.setFontSize(DEFAULT_FONT_SIZE); // CJ-796
		        run.setText(Jsoup.parse(noHTMLString).text());
		        
        		iCol++;
        	}
        	
        	iRow++;
        }
        XWPFRun run = document.createParagraph().createRun();
        run.setFontSize(DEFAULT_FONT_SIZE); // CJ-796
        run.addBreak();
	}
	
	private int getTableColCnt (String element) {
		// get column size
		Integer iCols = 0;
		Integer iPos = 0;
		while (true) {
			iPos = element.indexOf("<th", iPos);
			if (iPos < 0)
				break;
			else{
				iPos++;
				iCols++;
			}
		}
		
		return iCols;
	}
	
	private int getTableRowCnt (String element) {
		Integer iRows = 0;
		Integer iPos = 0;
		while (true) {
			iPos = element.indexOf("<tr", iPos+1);
			if (iPos < 0)
				break;
			else
				iRows++;
		}
		
		return iRows;
	}
	
	private ArrayList<DocumentClauseRecord> sortClauses(ArrayList<DocumentClauseRecord> srcClauses) {
		
		ArrayList<DocumentClauseRecord> dstClauses = new ArrayList<DocumentClauseRecord>(); 

		for (DocumentClauseRecord srcRecord : srcClauses) {
			Integer ndx = 0; 
			Boolean bAdded = false;
			if (dstClauses.size() == 0) {
				dstClauses.add(0, srcRecord);
				continue;
			}
			for (DocumentClauseRecord destRecord : dstClauses) {

				if (ClauseTable.compareClauseName(srcRecord.getClauseName(), destRecord.getClauseName())<0) {
					dstClauses.add(ndx, srcRecord);
					bAdded = true;
					break;
				}
				ndx++;
			}
			if (!bAdded) {
				dstClauses.add(srcRecord);
			}
			
		}
		
		srcClauses.clear();
		return dstClauses;
	}
	
	// CJ-1052, Check for embedded image tags. Wrap them with <p> tags
	// Note: can't use Jsoup.parse(), which changes the outerHtml string returned.
	private String formatImgTags (String clauseTxt) {
		
		Integer iImgBegPos = clauseTxt.indexOf("<img ");
		while (iImgBegPos > 0)
		{
			Integer iImgEndPos = clauseTxt.indexOf("/>",iImgBegPos);
			if (iImgEndPos < 0)
				iImgBegPos = 0;
			else {
				String sOrig = clauseTxt.substring (iImgBegPos, iImgEndPos + "/>".length());
				String sReplace =  "<p>" + sOrig + "</p>";
				clauseTxt = clauseTxt.replace(sOrig, sReplace);
				iImgBegPos = clauseTxt.indexOf("<img ", iImgEndPos);
			}
		}
		return clauseTxt;
	}
	
	// Example tag:
	// <img src="http://www.ecfr.gov/graphics/er31mr14.000.gif" alt="eCFR graphic er31mr14.000.gif" />
	private String getImgSrc (String tag) {
		
		Document doc = Jsoup.parse(tag);
		Element link = doc.select("img").first();
		String srcRef = link.attr("src");
		return srcRef;
	}
	
	private boolean drawImage (XWPFDocument document, String sFileImage) throws IOException {
			    
		boolean bResult = true; 
			
		String picFormat = "";
		Integer picType = -1;
		
		if ((sFileImage.toLowerCase().endsWith(".jpg")) || (sFileImage.toLowerCase().endsWith(".jpeg")))
		{
			picFormat = "jpg";
			picType = org.apache.poi.xwpf.usermodel.Document.PICTURE_TYPE_JPEG;
		}
		else if (sFileImage.toLowerCase().endsWith(".gif")) 
		{
			picFormat = "gif";
			picType = org.apache.poi.xwpf.usermodel.Document.PICTURE_TYPE_GIF;
		}
		else if (sFileImage.toLowerCase().endsWith(".png")) 
		{
			picFormat = "png";
			picType = org.apache.poi.xwpf.usermodel.Document.PICTURE_TYPE_PNG;
		}
		else if (sFileImage.toLowerCase().endsWith(".bmp")) 
		{
			picFormat = "bmp";
			picType = org.apache.poi.xwpf.usermodel.Document.PICTURE_TYPE_BMP;
		}
		/* NOT TESTED
		else if ((sFileImage.toLowerCase().endsWith(".tif")) || (sFileImage.toLowerCase().endsWith(".tiff")))
		{
			picFormat = "tif";
			picType = org.apache.poi.xwpf.usermodel.Document.PICTURE_TYPE_TIFF;
		}
		*/
		else
			return false;
		
		try {
			
	    	URL url = new URL(sFileImage);
	    	BufferedImage bufferImage = ImageIO.read(url);
	    	ByteArrayOutputStream bufferOutStream = new ByteArrayOutputStream();
	    	ImageIO.write(bufferImage, picFormat, bufferOutStream);
	    	InputStream pictureData = new ByteArrayInputStream(bufferOutStream.toByteArray());
	    	
	    	// Note: Just pass the width and height units. Word does the rest.
			document.createParagraph().createRun().addPicture
				(pictureData, picType, "pic", Units.toEMU(300), Units.toEMU(300));
		    
	    } catch (InvalidFormatException e) {
	    	bResult = false;
			e.printStackTrace();
		} catch (IOException e) {
			bResult = false;
			e.printStackTrace();
		}
	    
		return bResult;
	}

}