package gov.dod.cls.bean;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.node.ArrayNode;
import org.codehaus.jackson.node.ObjectNode;

import gov.dod.cls.PageApi;
import gov.dod.cls.bean.session.UserSession;
import gov.dod.cls.bean.utils.DocGenerateDoc;
import gov.dod.cls.bean.utils.DocGeneratePdf;
import gov.dod.cls.bean.utils.DocGenerateXml;
import gov.dod.cls.bean.utils.DocShowHtml;
import gov.dod.cls.config.ClsConfig;
import gov.dod.cls.db.AuditEventRecord;
import gov.dod.cls.db.ClauseRecord;
import gov.dod.cls.db.ClauseSectionRecord;
import gov.dod.cls.db.ClauseSectionTable;
import gov.dod.cls.db.ClauseTable;
import gov.dod.cls.db.ClauseVersionChangeTable;
import gov.dod.cls.db.ClauseVersionRecord;
import gov.dod.cls.db.ClauseVersionTable;
import gov.dod.cls.db.DocStatusRefRecord;
import gov.dod.cls.db.DocTypeRefRecord;
import gov.dod.cls.db.DocumentAnswerTable;
import gov.dod.cls.db.DocumentClauseTable;
import gov.dod.cls.db.DocumentFillInsTable;
import gov.dod.cls.db.DocumentRecord;
import gov.dod.cls.db.DocumentTable;
import gov.dod.cls.db.OauthInterviewRecord;
import gov.dod.cls.db.PrescriptionRecord;
import gov.dod.cls.db.PrescriptionTable;
import gov.dod.cls.db.QuestionConditionRecord;
import gov.dod.cls.db.QuestionConditionTable;
import gov.dod.cls.db.QuestionGroupRefRecord;
import gov.dod.cls.db.QuestionGroupRefTable;
import gov.dod.cls.db.QuestionRecord;
import gov.dod.cls.db.QuestionTable;
import gov.dod.cls.db.RegulationRecord;
import gov.dod.cls.db.RegulationTable;
import gov.dod.cls.exception.NotAllowedException;
import gov.dod.cls.exception.ObjectNotFoundException;
import gov.dod.cls.model.JsonAble;
import gov.dod.cls.utils.ClsConstants;
import gov.dod.cls.utils.CommonUtils;
import gov.dod.cls.utils.sql.SqlUtil;

public class ClsDocument extends DocumentRecord {

	public static final String PREFIX_DO_DOCUMENT = "doc-";

	public static final String DO_CREATE = PREFIX_DO_DOCUMENT + "create";
	public static final String DO_REOPEN = PREFIX_DO_DOCUMENT + "reopen";
	public static final String DO_CREATE_LINKED_AWARD = PREFIX_DO_DOCUMENT + "createLinkedAward";
	public static final String DO_GET_DOC_TO_LINK_AWARD = PREFIX_DO_DOCUMENT + "getDocToLinkAward";
	
	public static final String DO_DOWNLOAD = PREFIX_DO_DOCUMENT + "download";
	
	public static final String DO_REF_PREFIX = PREFIX_DO_DOCUMENT + "ref-";
	public static final String DO_REF_REGULATIONS = DO_REF_PREFIX + "regulations";
	public static final String DO_REF_CLAUSE_CONDITION = DO_REF_PREFIX + "clause_condition";
	public static final String DO_REF_PRESCRIPTION = DO_REF_PREFIX + "prescription";
	public static final String DO_REF_CLAUSE_SECTION = DO_REF_PREFIX + "clause_section";
	public static final String DO_REF_CLAUSE = DO_REF_PREFIX + "clause";
	
	public static final String DO_REF_QUESTION_CONDITION = DO_REF_PREFIX + "question_condition";
	public static final String DO_REF_QUESTION = DO_REF_PREFIX + "question";

	public static final String DO_REF_QUESTION_GROUP = DO_REF_PREFIX + "question_group";
	
	public static final String DO_REF_SETS = DO_REF_PREFIX + "sets";

	public static final String DO_INTERVIEW = PREFIX_DO_DOCUMENT + "interview";
	public static final String DO_TRANSCRIPT = PREFIX_DO_DOCUMENT + "transcript"; // CJ-1223
	public static final String DO_POST = PREFIX_DO_DOCUMENT + "post";
	public static final String DO_ALIVE = PREFIX_DO_DOCUMENT + "alive";
	public static final String DO_DELETE = PREFIX_DO_DOCUMENT + "delete";

	public static final String DO_CLAUSE_CONTENT = PREFIX_DO_DOCUMENT + "clause-content";
	
	public static final String DO_GET_DOC_TO_COPY = PREFIX_DO_DOCUMENT + "getDocToCopy";
	public static final String DO_CREATE_COPY = PREFIX_DO_DOCUMENT + "createcopy";
	
	private static final Logger logger = Logger.getLogger(ClsDocument.class);
	
	public static void processRequest(String sDo, UserSession oUserSession, 
			HttpServletRequest req, HttpServletResponse resp) {
		switch (sDo) {
		case DO_ALIVE:
			PageApi.send(resp, ClsConstants.RESPONSE_FORMAT_TEXT, JsonAble.jsonResult(JsonAble.STATUS_SUCCESS, null, null, oUserSession));
			return;
		case DO_CLAUSE_CONTENT:
			Integer id = CommonUtils.toInteger(req.getParameter(ClauseRecord.FIELD_CLAUSE_ID));
			logDebug(oUserSession, "Clause", id, "posted Content Request.", null);
			if (id != null) {
				ObjectNode objectNode = ClauseTable.getRecordContentJson(id.intValue());
				String response;
				if (objectNode != null) {
					response = JsonAble.jsonResult(JsonAble.STATUS_SUCCESS, objectNode, null, oUserSession); // CJ-573
					logDebug(oUserSession, "Clause", id, "content returned successfully.", null);
				}
				else {
					response = JsonAble.jsonResult(JsonAble.STATUS_FAIL, null, "Not found", oUserSession); // CJ-573
					logInfo(oUserSession, "Clause", id, "content NOT found.", null);
				}
				PageApi.send(resp, JsonAble.JSON_RESPONSE_FORMAT, response);
			}
			return;
		/*
		case DO_REF_CLAUSE:
			Pagination.send(ClauseRecord.TABLE_CLAUSES, req, resp, ClauseTable.getJson(null, true, null));
			return;
		case DO_REF_CLAUSE_SECTION:
			Pagination.send(PrescriptionRecord.TABLE_PRESCRIPTIONS, req, resp, ClauseSectionTable.getJson(null, true));
			return;
		case DO_REF_REGULATIONS:
			Pagination.send(RegulationRecord.TABLE_REGULATIONS, req, resp, RegulationTable.getJson(null, null, true));
			return;
		case DO_REF_PRESCRIPTION:
			Pagination.send(PrescriptionRecord.TABLE_PRESCRIPTIONS, req, resp, PrescriptionTable.getJson(null, null, null, true));
			return;
		case DO_REF_QUESTION_CONDITION:
			Pagination.send(QuestionConditionRecord.TABLE_QUESTION_CONDITIONS, req, resp, QuestionConditionTable.getJson(null, null,null, true));
			return;
		case DO_REF_QUESTION:
			Pagination.send(QuestionRecord.TABLE_QUESTIONS, req, resp, QuestionTable.getJson(null, null, null, true));
			return;
		case DO_REF_QUESTION_GROUP:
			Pagination.send(QuestionGroupRefRecord.TABLE_QUESTION_GROUP_REF, req, resp, QuestionGroupRefTable.getJson(null, true));
			return;
		case DO_REF_SETS:
			ClsDocument clsDocument = new ClsDocument();
			String response = JsonAble.jsonResult(JsonAble.STATUS_SUCCESS, clsDocument.getInterviewJson(), null); 
			PageApi.send(resp, JsonAble.JSON_RESPONSE_FORMAT, response);
			return;
		*/
		case ClsDocument.DO_CREATE:
			ClsDocument.createDocument(oUserSession, req, resp);
			return;
		case ClsDocument.DO_CREATE_LINKED_AWARD:
			ClsDocument.createLinkedAward(oUserSession, req, resp);
			return;
		case ClsDocument.DO_REOPEN:
			ClsDocument.reopenDocument(oUserSession, req, resp);
			return;
		}

		Integer docId = CommonUtils.toInteger(req.getParameter("id"));
		if (docId == null)
			return;

		Connection conn = null;
		try {
			conn = ClsConfig.getDbConnection();
			ClsDocument clsDocument = null;
			if (DocumentTable.hasUserDocument(oUserSession, docId, conn)) // CJ-264
				clsDocument = new ClsDocument();
			if ((clsDocument == null) || (!clsDocument.loadDocument(docId, conn, ClsDocument.DO_DOWNLOAD.equals(sDo)))) {
				String response = JsonAble.jsonResult(JsonAble.STATUS_FAIL, null, "Invalid Document Id: " + docId); 
				PageApi.send(resp, JsonAble.JSON_RESPONSE_FORMAT, response);
				logInfo(oUserSession, "Document", docId, "Invalid Document Id.", null);
				return;
			}
	
			String response;
			switch (sDo) {
			case DO_INTERVIEW:
			case DO_TRANSCRIPT: // CJ-1223
				logDebug(oUserSession, "Document", docId, "posted " +  sDo, null);
				try {
					response = clsDocument.genInterviewJson(oUserSession, sDo); // JsonAble.jsonResult(JsonAble.STATUS_SUCCESS, clsDocument.getInterviewJson(), null); 
					PageApi.send(resp, JsonAble.JSON_RESPONSE_FORMAT, response);
					return;
				}
				catch (Exception ioError) {
					ioError.printStackTrace();
					logger.info("Problem gen interview json ", ioError);
					response = JsonAble.jsonResult(JsonAble.STATUS_FAIL, null, ioError.getMessage()); 
					PageApi.send(resp, JsonAble.JSON_RESPONSE_FORMAT, response);
					return;
				}

			case ClsDocument.DO_DOWNLOAD:
				String format = req.getParameter("format");
				logDebug(oUserSession, "Document", docId, "posted Download.", format);
				
				switch (format) {
				case "html": 
					DocShowHtml.downloadHtml(clsDocument, req, resp);
					break;
				case "xml":
					DocGenerateXml.download(clsDocument, req, resp);
					break;
				case "docx": 
					// CJ-549; This docx cannot be opened in a Windows environment.
					DocGenerateDoc.downloadDoc(clsDocument, req, resp);
					break;
				case "pdf": 
					  DocGeneratePdf.downloadPdf(clsDocument, req, resp);
					break;
				default:
					PageApi.send(resp, null, "Not-implemented format: " + format);
				}
				return;
			case DO_POST:
				logDebug(oUserSession, "Document", docId, "posted Save.", null);
				
				String data = req.getParameter("data");
				String mode = req.getParameter("mode");
				if (CommonUtils.isEmpty(data)) {
					response = JsonAble.jsonResult(JsonAble.STATUS_FAIL, null, "no data"); 
					PageApi.send(resp, JsonAble.JSON_RESPONSE_FORMAT, response);
					return;
				}
				try {
					ClsDocument postDocument = new ClsDocument();
					postDocument.readPost(data);
					clsDocument.loadReferences(conn, false, null);
					clsDocument.save(postDocument, clsDocument.questionTable, clsDocument.clauseTable, oUserSession, conn);
					if ("1".equals(mode))
						oUserSession.setSessionMessage("Document saved");
					ObjectMapper mapper = new ObjectMapper();
					ObjectNode json = mapper.createObjectNode();
					json.put(FIELD_UPDATED_AT, CommonUtils.toJsonDate(clsDocument.getUpdatedAt()));
					logInfo(oUserSession, "Document", docId, "Saved successfully.", null);
					response = JsonAble.jsonResult(JsonAble.STATUS_SUCCESS, json, "The document has been saved successfully", oUserSession);
					PageApi.send(resp, JsonAble.JSON_RESPONSE_FORMAT, response);
				}
				catch (JsonParseException oJsonError) {
					oJsonError.printStackTrace();
					logInfo(oUserSession, "Document", docId, "Save failed.", oJsonError.getMessage());
					response = JsonAble.jsonResult(JsonAble.STATUS_FAIL, null, oJsonError.getMessage()); 
					PageApi.send(resp, JsonAble.JSON_RESPONSE_FORMAT, response);
				}
				catch (IOException ioError) {
					ioError.printStackTrace();
					logInfo(oUserSession, "Document", docId, "Save failed.", ioError.getMessage());
					response = JsonAble.jsonResult(JsonAble.STATUS_FAIL, null, ioError.getMessage()); 
					PageApi.send(resp, JsonAble.JSON_RESPONSE_FORMAT, response);
				}
				catch (Exception ioError) {
					ioError.printStackTrace();
					logInfo(oUserSession, "Document", docId, "Save failed.", ioError.getMessage());
					response = JsonAble.jsonResult(JsonAble.STATUS_FAIL, null, ioError.getMessage()); 
					PageApi.send(resp, JsonAble.JSON_RESPONSE_FORMAT, response);
				}
				return;
			case DO_DELETE:
				logDebug(oUserSession, "Document", docId, "posted Delete.", null);
				
				if (clsDocument.deleteDocument(oUserSession, null)) {
					response = JsonAble.jsonResult(JsonAble.STATUS_SUCCESS, null, "Document deleted");
					logInfo(oUserSession, "Document", docId, "Document deleted.", null);
				} else {
					response = JsonAble.jsonResult(JsonAble.STATUS_FAIL, null, "Unable to delete the document"); 
					logInfo(oUserSession, "Document", docId, "Unable to delete the document.", null);
				}
				PageApi.send(resp, JsonAble.JSON_RESPONSE_FORMAT, response);
				return;
			case DO_GET_DOC_TO_LINK_AWARD:
				response = clsDocument.genDocToLinkAwardResponse(conn);
				PageApi.send(resp, JsonAble.JSON_RESPONSE_FORMAT, response);
				return;
			case DO_GET_DOC_TO_COPY:
				response = clsDocument.genDocToCopyResponse(conn);
				PageApi.send(resp, JsonAble.JSON_RESPONSE_FORMAT, response);
				return;	
			case DO_CREATE_COPY:		
				response = ClsDocument.createCopyDocument (oUserSession,  req,  resp);
				PageApi.send(resp, JsonAble.JSON_RESPONSE_FORMAT, response);
				return;
			}
		} finally {
			SqlUtil.closeInstance(conn);
		}
	}
		
	private static void createDocument(UserSession oUserSession, HttpServletRequest req, HttpServletResponse resp) {
		String docType = req.getParameter("doc_type");
		String acquisitionTitle = req.getParameter("acquisition_title");
		String documentNumber = req.getParameter("document_number");
		String errorMessage = null;
		Integer docId = null;
		if (CommonUtils.isEmpty(docType))
			errorMessage = "Document type is empty";
		else if (CommonUtils.isEmpty(acquisitionTitle))
			errorMessage = "Acquisition title is empty";
		else if (CommonUtils.isEmpty(documentNumber))
			errorMessage = "Document number is empty";
		else if(acquisitionTitle.indexOf("<") >-1)
			errorMessage ="Title cannot contain special character: <";
		else if(acquisitionTitle.indexOf(">") >-1)
			errorMessage ="Title cannot contain special character: >";
		else if(acquisitionTitle.indexOf(";") >-1)
			errorMessage ="Title cannot contain special character: ;";
		else if(acquisitionTitle.indexOf(":") >-1)
			errorMessage ="Title cannot contain special character: :";
		else if(acquisitionTitle.indexOf("@") >-1)
			errorMessage ="Title cannot contain special character: @";
		else if(acquisitionTitle.indexOf("#") >-1)
			errorMessage ="Title cannot contain special character: #";
		else if(documentNumber.indexOf("<") >-1)
			errorMessage ="Document Number cannot contain special character: <";
		else if(documentNumber.indexOf(">") >-1)
			errorMessage ="Document Number cannot contain special character: >";
		else if(documentNumber.indexOf(";") >-1)
			errorMessage ="Document Number cannot contain special character: ;";
		else if(documentNumber.indexOf(":") >-1)
			errorMessage ="Document Number cannot contain special character: :";
		else if(documentNumber.indexOf("@") >-1)
			errorMessage ="Document Number cannot contain special character: @";
		else if(documentNumber.indexOf("#") >-1)
			errorMessage ="Document Number cannot contain special character: #";
		
		
		else {
			Connection conn = null;
			try {
				conn = ClsConfig.getDbConnection();
				DocumentRecord documentRecord = DocumentTable.findbyDocNumber(oUserSession.getUserId().intValue(), documentNumber, conn);
				if (documentRecord != null) {
					errorMessage = "Document number already exists";
				} else {
					ClauseVersionRecord clauseVersionRecord = ClauseVersionTable.getInstance(conn).findActive();
					if (clauseVersionRecord == null) {
						errorMessage = "There is not active Clause version available to create documents";
					} else {
						documentRecord = DocumentTable.create(oUserSession.getUserId().intValue(), docType,
								acquisitionTitle, documentNumber,clauseVersionRecord.getId().intValue(), conn);
						if (documentRecord != null) {
							docId = documentRecord.getId();
						} else {
							errorMessage = "Unable to create document due to unknown server error";
						}
					}
				}
			} catch (Exception oError) {
				oError.printStackTrace();
				errorMessage = "Unable to create document due to server internal error";
			} finally {
				SqlUtil.closeInstance(conn);
			}
		}
		String jsonResult;
		if (errorMessage == null) {
			oUserSession.setSessionMessage("Document prepared.");
			jsonResult = JsonAble.jsonResult(JsonAble.STATUS_SUCCESS, null, (docId == null) ? null : docId.toString());
		} else {
			jsonResult = JsonAble.jsonResult(JsonAble.STATUS_FAIL, null, errorMessage);
		}
		PageApi.send(resp, JsonAble.JSON_RESPONSE_FORMAT, jsonResult);
	}
	
	private static String createCopyDocument
		(UserSession oUserSession, HttpServletRequest req, HttpServletResponse resp) {
		
		Integer origId = CommonUtils.toInteger(req.getParameter("id"));
		
		String acquisitionTitle = req.getParameter("acquisition_title");
		String documentNumber = req.getParameter("document_number");
		
		Integer newId = null;
		String jsonResult;
		//CJ-1441
		if(acquisitionTitle.indexOf("<") >-1)
			return JsonAble.jsonResult(JsonAble.STATUS_FAIL, null,"Title cannot contain special character: <");
		else if(acquisitionTitle.indexOf(">") >-1)
			return JsonAble.jsonResult(JsonAble.STATUS_FAIL, null,"Title cannot contain special character: >");
		else if(acquisitionTitle.indexOf(";") >-1)
			return JsonAble.jsonResult(JsonAble.STATUS_FAIL, null,"Title cannot contain special character: ;");
		else if(acquisitionTitle.indexOf(":") >-1)
			return JsonAble.jsonResult(JsonAble.STATUS_FAIL, null,"Title cannot contain special character: :");
		else if(acquisitionTitle.indexOf("@") >-1)
			return JsonAble.jsonResult(JsonAble.STATUS_FAIL, null,"Title cannot contain special character: @");
		else if(acquisitionTitle.indexOf("#") >-1)
			return JsonAble.jsonResult(JsonAble.STATUS_FAIL, null,"Title cannot contain special character: #");
		else if(documentNumber.indexOf("<") >-1)
			return JsonAble.jsonResult(JsonAble.STATUS_FAIL, null,"Document Number cannot contain special character: <");
		else if(documentNumber.indexOf(">") >-1)
			return JsonAble.jsonResult(JsonAble.STATUS_FAIL, null,"Document Number cannot contain special character: >");
		else if(documentNumber.indexOf(";") >-1)
			return JsonAble.jsonResult(JsonAble.STATUS_FAIL, null,"Document Number cannot contain special character: ;");
		else if(documentNumber.indexOf(":") >-1)
			return JsonAble.jsonResult(JsonAble.STATUS_FAIL, null,"Document Number cannot contain special character: :");
		else if(documentNumber.indexOf("@") >-1)
			return JsonAble.jsonResult(JsonAble.STATUS_FAIL, null,"Document Number cannot contain special character: @");
		else if(documentNumber.indexOf("#") >-1)
			return JsonAble.jsonResult(JsonAble.STATUS_FAIL, null,"Document Number cannot contain special character: #");


		Connection conn = null;
		try {
			conn = ClsConfig.getDbConnection();

			DocumentRecord documentRecord = DocumentTable.findbyDocNumber(oUserSession.getUserId().intValue(), documentNumber, conn);
			if (documentRecord != null) {
				return JsonAble.jsonResult(JsonAble.STATUS_FAIL, null, "Document number already exists"); 
			} 
			
			documentRecord = DocumentTable.find(origId, conn);
			if (documentRecord == null) {
				jsonResult = JsonAble.jsonResult(JsonAble.STATUS_FAIL, null, "Source document not found.");	
				return jsonResult;
			}
			
			String docType = documentRecord.getDocumentType();
			Integer origClauseVersionId = documentRecord.getClauseVersionId();
			
			ClauseVersionRecord clauseVersionRecord = ClauseVersionTable.getInstance(conn).findActive();
			if (clauseVersionRecord == null) {
				jsonResult = JsonAble.jsonResult(JsonAble.STATUS_FAIL, null, "There is not active Clause version available to create document");						
				return jsonResult;
			} 
			
			documentRecord = DocumentTable.create(oUserSession.getUserId().intValue(), docType,
						acquisitionTitle, documentNumber, clauseVersionRecord.getId().intValue(), conn);
			if (documentRecord == null) {
				jsonResult = JsonAble.jsonResult(JsonAble.STATUS_FAIL, null, "Unable to create document due to unknown server error");	
				return jsonResult;
			}
			
			newId = documentRecord.getId();
			Integer newClauseVersionId = documentRecord.getClauseVersionId();
			
			if (documentRecord.copyDocument( origId, newId, origClauseVersionId, newClauseVersionId, conn) == false)
			{
				jsonResult = JsonAble.jsonResult(JsonAble.STATUS_FAIL, null, "Unable to copy document answers due to unknown server error");	
				return jsonResult;
			}
			
			// check if we need to upgrade the version
			if ( origClauseVersionId < newClauseVersionId ) {
				
				ClsDocument clsDocument = new ClsDocument();
				if (clsDocument.loadDocument(newId, conn, false) == false) {
					jsonResult = JsonAble.jsonResult(JsonAble.STATUS_FAIL, null, "Unable to upgrade document version");	
					return jsonResult;
				}
				
				ArrayList<String> alChanges = new ArrayList<String>();
				if (clsDocument.upgradeClsVersion(alChanges, (oUserSession == null ? null : oUserSession.getUserId()), (oUserSession == null ? null : oUserSession.getApplicationId()), conn) == false) {
					jsonResult = JsonAble.jsonResult(JsonAble.STATUS_FAIL, null, "Unable to upgrade document version");						
					return jsonResult;
				}
			}
						
		} catch (Exception oError) {
			oError.printStackTrace();
		} finally {
			SqlUtil.closeInstance(conn);
		}
		
		oUserSession.setSessionMessage("Document copied successfully.");
		jsonResult = JsonAble.jsonResult(JsonAble.STATUS_SUCCESS, null, (newId == null) ? null : newId.toString());

		return jsonResult;
		
	}
	private static void createLinkedAward(UserSession oUserSession, HttpServletRequest req, HttpServletResponse resp) {
		String sSolicitationDocId = req.getParameter("doc_id");
		String acquisitionTitle = req.getParameter("acquisition_title");
		String documentNumber = req.getParameter("document_number");
		String clauseVersionId = req.getParameter("clause_version_id");
		
		String errorMessage = null;
		Integer awardDocId = null;
		
		Integer solicitationDocId = CommonUtils.toInteger(sSolicitationDocId);
		
		if (CommonUtils.isEmpty(sSolicitationDocId))
			errorMessage = "Solicitation document is empty";
		else if (solicitationDocId == null)
			errorMessage = "Solicitation document id is invalid";
		else if (CommonUtils.isEmpty(acquisitionTitle))
			errorMessage = "Acquisition title is empty";
		else if (CommonUtils.isEmpty(documentNumber))
			errorMessage = "Document number is empty";
		else if(acquisitionTitle.indexOf("<") >-1)
			errorMessage ="Title cannot contain special character: <";
		else if(acquisitionTitle.indexOf(">") >-1)
			errorMessage ="Title cannot contain special character: >";
		else if(acquisitionTitle.indexOf(";") >-1)
			errorMessage ="Title cannot contain special character: ;";
		else if(acquisitionTitle.indexOf(":") >-1)
			errorMessage ="Title cannot contain special character: :";
		else if(acquisitionTitle.indexOf("@") >-1)
			errorMessage ="Title cannot contain special character: @";
		else if(acquisitionTitle.indexOf("#") >-1)
			errorMessage ="Title cannot contain special character: #";
		else if(documentNumber.indexOf("<") >-1)
			errorMessage ="Document Number cannot contain special character: <";
		else if(documentNumber.indexOf(">") >-1)
			errorMessage ="Document Number cannot contain special character: >";
		else if(documentNumber.indexOf(";") >-1)
			errorMessage ="Document Number cannot contain special character: ;";
		else if(documentNumber.indexOf(":") >-1)
			errorMessage ="Document Number cannot contain special character: :";
		else if(documentNumber.indexOf("@") >-1)
			errorMessage ="Document Number cannot contain special character: @";
		else if(documentNumber.indexOf("#") >-1)
			errorMessage ="Document Number cannot contain special character: #";
		else {
			Connection conn = null;
			try {
				conn = ClsConfig.getDbConnection();
				ClsDocument solClsDocument = new ClsDocument();
				ClsDocument awardClsDocument = new ClsDocument();
				DocumentRecord documentRecord = DocumentTable.find(oUserSession.getUserId().intValue(), 
						DocTypeRefRecord.DOC_TYPE_CD_AWARD, acquisitionTitle, documentNumber, conn);
				if (documentRecord != null) {
					errorMessage = "Already exists";
				} else {
					if (!solClsDocument.loadDocument(solicitationDocId, conn, false)) {
						errorMessage = "Unknown Solicitation document(" + solicitationDocId + ")";
					} else {
						ClauseVersionTable oClauseVersionTable = ClauseVersionTable.getInstance(conn); 
						ClauseVersionRecord clauseVersionRecord = null;
						int iSolClsVerId = solClsDocument.getClauseVersionId().intValue();
						if (CommonUtils.isNotEmpty(clauseVersionId)) {
							Integer iAwardClsVerId = CommonUtils.toInteger(clauseVersionId);
							if (iAwardClsVerId == null)
								errorMessage = "Invalid CLS version requested";
							else if (iAwardClsVerId.intValue() < iSolClsVerId)
								errorMessage = "Requested CLS version is older than the solicitation document' CLS version";
							else
								clauseVersionRecord = oClauseVersionTable.findById(iAwardClsVerId.intValue());
						} else {
							clauseVersionRecord = oClauseVersionTable.findById(iSolClsVerId);
						}
						if (clauseVersionRecord == null) {
							errorMessage = "There is no valid CLS version available to create documents";
						} else {
							documentRecord = DocumentTable.create(oUserSession.getUserId().intValue(), DocTypeRefRecord.DOC_TYPE_CD_AWARD,
									acquisitionTitle, documentNumber, solicitationDocId, clauseVersionRecord.getId().intValue(), conn);
							if (documentRecord != null) {
								awardDocId = documentRecord.getId();
								if (!awardClsDocument.loadDocument(awardDocId, null, false)) {
									errorMessage = "Unknown error loading newly created Award document(" + awardDocId + ")";
								}
							} else {
								errorMessage = "Unable to create document due to unknown server error";
							}
						}
					}
				}
				if (CommonUtils.isEmpty(errorMessage)) {
					awardClsDocument.populateAwardFromSolicitation(solClsDocument, conn);
				}
			} catch (Exception oError) {
				oError.printStackTrace();
				errorMessage = "Unable to create document due to server internal error";
			} finally {
				SqlUtil.closeInstance(conn);
			}
		}
		String jsonResult;
		if (errorMessage == null) {
			oUserSession.setSessionMessage("Document prepared.");
			jsonResult = JsonAble.jsonResult(JsonAble.STATUS_SUCCESS, null, (awardDocId == null) ? null : awardDocId.toString());
		} else {
			jsonResult = JsonAble.jsonResult(JsonAble.STATUS_FAIL, null, errorMessage);
		}
		PageApi.send(resp, JsonAble.JSON_RESPONSE_FORMAT, jsonResult);
	}
	
	
	private static void reopenDocument(UserSession oUserSession, HttpServletRequest req, HttpServletResponse resp) 
	{
		Integer docId = CommonUtils.toInteger(req.getParameter("id"));
		
		Connection conn = null;
		try 
		{
			conn = ClsConfig.getDbConnection();
			conn.setAutoCommit(false);
			DocumentRecord docRecord = validateReopenAction(oUserSession, docId, conn);
			if (CommonUtils.isSame(DocStatusRefRecord.CD_FINALIZED, docRecord.getDocumentStatus()))
			{
				DocumentTable.updateDocStatus(docRecord, DocStatusRefRecord.CD_IN_PROGRESS, conn);
				addReopenEvent(oUserSession, docId, docRecord, conn);
			}
			conn.commit();
			String response = JsonAble.jsonResult(JsonAble.STATUS_SUCCESS, null, "Document reopen"); 
			PageApi.send(resp, JsonAble.JSON_RESPONSE_FORMAT, response);			
		}
		catch(Exception e)
		{
			
			SqlUtil.rollback(conn);
			String response = JsonAble.jsonResult(JsonAble.STATUS_FAIL, null, e.getMessage()); 
			PageApi.send(resp, JsonAble.JSON_RESPONSE_FORMAT, response);
		}
		finally
		{
			SqlUtil.closeInstance(conn);
		}
	}

	private static void addResumeEvent(UserSession oUserSession, Integer docId, DocumentRecord docRecord,
			Connection conn) throws Exception
	{
		AuditEventRecord auditEventRecord = new AuditEventRecord(AuditEventRecord.ACTION_DOCUMENT_RESUME, oUserSession.getUserId(), null);
		auditEventRecord.addAttribute(DocumentRecord.FIELD_DOCUMENT_ID, docId);
		auditEventRecord.addAttribute(DocumentRecord.FIELD_DOCUMENT_STATUS, docRecord.getDocumentStatus());
		auditEventRecord.addAttribute (AuditEventRecord.FIELD_APPLICATION_ID, oUserSession.getApplicationId());
		auditEventRecord.saveNew(conn);
	}
	
	private static void addReopenEvent(UserSession oUserSession, Integer docId, DocumentRecord docRecord,
			Connection conn) throws Exception
	{
		AuditEventRecord auditEventRecord = new AuditEventRecord(AuditEventRecord.ACTION_DOCUMENT_REOPEN, oUserSession.getUserId(), null);
		auditEventRecord.addAttribute(DocumentRecord.FIELD_DOCUMENT_ID, docId);
		auditEventRecord.addAttribute(DocumentRecord.FIELD_DOCUMENT_STATUS, docRecord.getDocumentStatus());
		auditEventRecord.addAttribute (AuditEventRecord.FIELD_APPLICATION_ID, oUserSession.getApplicationId());
		auditEventRecord.saveNew(conn);
	}
		
	private static DocumentRecord validateReopenAction(UserSession oUserSession, Integer docId, Connection conn)
			throws Exception
	{
				
		if (docId == null)
		{
			throw new IllegalArgumentException("The doc id is required");
		}
		if (!DocumentTable.hasUserDocument(oUserSession, docId, conn))				
		{
			throw new ObjectNotFoundException("Invalid Document Id: " + docId);
		}
		DocumentRecord docRecord = DocumentTable.get(null, docId, conn);
		String docStatus = docRecord.getDocumentStatus();
		if (CommonUtils.isNotSame(DocStatusRefRecord.CD_FINALIZED, docStatus))
		{
//			throw new NotAllowedException("The document id :" + docId + " has already open");
			return docRecord;
		}
		String docType = docRecord.getDocumentType();
		if (CommonUtils.isSame(DocTypeRefRecord.DOC_TYPE_CD_ORDER, docType)
		   || CommonUtils.isSame(DocTypeRefRecord.DOC_TYPE_CD_AWARD, docType))
		{
			return docRecord;
		}
		if (DocumentTable.hasLinkedAward(docRecord, conn))
		{
			throw new NotAllowedException("A linked award has been created with this solicitation. User cannot reopen this document.");
		}
		return docRecord;
	}
	/*
	// API ----------------------------------------------------------
	public static ObjectNode getRecordApiJson(int docId) {
		ClsDocument clsDocument = new ClsDocument();
		if (!clsDocument.loadDocument(docId, null)) {
			return null;
		}
		return clsDocument.getApiJson(false);
	}
	*/
	
	public static void checkCache(Connection conn) {
		RegulationTable.getInstance(conn);
		ClauseTable.getInstance(null, conn);
		QuestionTable.getInstance(conn);
	}

	// instance ----------------------------------------------------------
	private Integer docId;
	private QuestionTable questionTable = null;
	private ClauseTable clauseTable = null;
	private Exception lastError = null;
	
	public boolean loadDocument(int docId, Connection conn, boolean forDownload) { // forDownload for CJ-264
		boolean result = false;
		this.lastError = null;
		this.docId = docId;
		boolean bCreateConnection = (conn == null);
		try {
			if (bCreateConnection)
				conn = ClsConfig.getDbConnection();
			DocumentTable.get(this, this.docId, conn);
			if (this.getClauseVersionId() != null) {
				this.documentAnswerTable = DocumentAnswerTable.loadByDocument(this.docId, conn);
				this.documentFillInsTable = DocumentFillInsTable.loadByDocument(this.docId, conn);
				this.documentClauseTable = DocumentClauseTable.loadByDocument(this.docId, conn);
	
				this.loadReferences(conn, forDownload, null);
				/*
				int clauseVersionId = this.getClauseVersionId();
				//boolean bIsLinkedAward = this.isLinkedAward();
				ClauseSectionTable.getInstance(conn);
				this.questionTable = QuestionTable.getSubsetByClauseVersion(clauseVersionId, conn); // bIsLinkedAward, 
				this.clauseTable = ClauseTable.getByClauseVersion(clauseVersionId, conn); // bIsLinkedAward,
				*/ 
				
				result = true;
			}
		}
		catch (Exception oError) {
			this.lastError = oError;
			oError.printStackTrace();
		}
		finally {
			if (bCreateConnection)
				SqlUtil.closeInstance(conn);
		}
		return result;
	}
	
	public boolean loadDocumentForApi(int docId, Connection conn, boolean forDownload, HashMap<Integer, ClauseTable> mapClauseTable) {
		boolean result = false;
		this.lastError = null;
		this.docId = docId;
		boolean bCreateConnection = (conn == null);
		try {
			if (bCreateConnection)
				conn = ClsConfig.getDbConnection();
			DocumentTable.get(this, this.docId, conn);
			if (this.getClauseVersionId() != null) {
				this.documentAnswerTable = DocumentAnswerTable.loadByDocument(this.docId, conn);
				this.documentFillInsTable = DocumentFillInsTable.loadByDocumentForApi(this.docId, conn);
				this.documentClauseTable = DocumentClauseTable.loadByDocumentForApi(this.docId, conn);
	
				this.loadReferences(conn, forDownload, mapClauseTable);
				
				result = true;
			}
		}
		catch (Exception oError) {
			this.lastError = oError;
			oError.printStackTrace();
		}
		finally {
			if (bCreateConnection)
				SqlUtil.closeInstance(conn);
		}
		return result;
	}
	
//	private void loadReferences(Connection conn, boolean forDownload) { // forDownload for CJ-264
//=======
	public void reloadAnswers(Connection conn) throws SQLException {
		this.documentAnswerTable = DocumentAnswerTable.loadByDocument(this.docId, conn);
	}
	
	private void loadReferences(Connection conn, boolean forDownload, HashMap<Integer, ClauseTable> mapClauseTable) { // forDownload for CJ-264
// >>>>>>> Implemented Indicators
		Integer clauseVersionId = this.getClauseVersionId();
		if (this.questionTable == null)
			this.questionTable = QuestionTable.getSubsetByClauseVersion(clauseVersionId, conn); 
		if (this.clauseTable == null) {
			ClauseSectionTable.getInstance(conn);
			if (mapClauseTable == null) {
				this.clauseTable = ClauseTable.getByClauseVersion(clauseVersionId, conn, forDownload);
			} else {
				if (mapClauseTable.containsKey(clauseVersionId)) {
					this.clauseTable = mapClauseTable.get(clauseVersionId);
				} else {
					this.clauseTable = ClauseTable.getByClauseVersion(clauseVersionId, conn, forDownload);
					mapClauseTable.put(clauseVersionId, this.clauseTable);
				}
			}
		}
	}
	
	public void populateAwardFromSolicitation(ClsDocument solClsDocument, Connection conn) 
			throws SQLException {
		this.documentAnswerTable.populateAwardFromSolicitaton(this.docId, this.questionTable,
				solClsDocument.documentAnswerTable, solClsDocument.questionTable, conn);
		this.documentClauseTable.populateAwardFromSolicitaton(this.docId, 
				this.clauseTable, this.documentFillInsTable, 
				solClsDocument.documentClauseTable, solClsDocument.documentFillInsTable,
				solClsDocument.clauseTable, conn);
	}
	
	// CJ-1244, API. Copy Solicitation strips out all solicitation questions and answers
	public void populateDocFromTemplate(ClsDocument solClsDocument, Connection conn) 
			throws SQLException {
		this.documentAnswerTable.populateDocFromTemplate(this.docId, this.questionTable,
				solClsDocument.documentAnswerTable, solClsDocument.questionTable, conn);
		this.documentClauseTable.populateDocFromTemplate(this.docId, 
				this.clauseTable, this.documentFillInsTable, 
				solClsDocument.documentClauseTable, solClsDocument.documentFillInsTable,
				solClsDocument.clauseTable, conn);
	}
	
	public boolean upgradeClsVersion(ArrayList<String> alChanges, Integer userId, Integer applicationId, Connection conn) throws Exception {
		ClauseVersionTable.getInstance(conn);
		Integer activeClsVersionId = ClauseVersionTable.getActiveVersionId();
		if (activeClsVersionId == null) {
			alChanges.add("No active CLS version found.");
			return false;
		}
		if (activeClsVersionId.equals(this.getClauseVersionId())) {
			alChanges.add("CLS version is up-to-date.");
			return false;
		}
		
		AuditEventRecord auditEventRecord = new AuditEventRecord(AuditEventRecord.ACTION_DOCUMENT_VERSION_UPDATED, userId, null);
		ClsDocument newClsDocument = new ClsDocument();
		this.prepareForClsVersionUpgrade(newClsDocument, activeClsVersionId, auditEventRecord, conn);

		QuestionTable newQuestionTable = QuestionTable.getSubsetByClauseVersion(activeClsVersionId, conn); 
		ClauseTable newClauseTable = ClauseTable.getByClauseVersion(activeClsVersionId, conn, false); 
		
		// CJ-1410, log more audit details for version upgrade.  
		auditEventRecord.addAttribute(DocumentRecord.FIELD_DOCUMENT_ID, this.docId);
		if (userId != null) {
			auditEventRecord.addAttribute(AuditEventRecord.FIELD_USER_ID, userId);
		}
		else if (applicationId != null) {
			auditEventRecord.addAttribute(AuditEventRecord.FIELD_APPLICATION_ID, applicationId);
		}
		
		ArrayList<String> alRemovedQuestionCodes = new ArrayList<String>();
		DocumentAnswerTable.upgradeClsVersion(this.docId, newQuestionTable,
				this.documentAnswerTable, this.questionTable, alRemovedQuestionCodes, conn);
		if (alRemovedQuestionCodes.size() > 0) {
			String sLog = "";
			for (String sItem : alRemovedQuestionCodes) {
				sLog += (sLog.isEmpty() ? "" : ", ") + sItem;
			}
			auditEventRecord.addAttribute("Removed Questions", sLog);
			alChanges.add("Removed Questions: " + sLog);
		}
		
		ArrayList<String> alRemovedClauseNames = new ArrayList<String>();
		DocumentClauseTable.upgradeClsVersion(this.docId, 
				newClauseTable, // newClsDocument.documentFillInsTable, 
				this.documentClauseTable, this.documentFillInsTable,
				this.clauseTable, alRemovedClauseNames, conn);
		if (alRemovedClauseNames.size() > 0) {
			String sLog = "";
			for (String sItem : alRemovedClauseNames) {
				sLog += (sLog.isEmpty() ? "" : ", ") + sItem;
			}
			auditEventRecord.addAttribute("Removed Clauses", sLog);
			alChanges.add("Removed Clauses: " + sLog);
		}

		auditEventRecord.saveNew(conn);
		
		return true;
	}
	
	protected boolean prepareReferences(Connection conn) {
		if (this.getClauseVersionId() == null) {
			this.setClauseVersionId(ClauseVersionTable.getActiveVersionId());
			if (this.getClauseVersionId() == null) {
				return false;
			}
		}
		boolean bCreateConnection = (conn == null);
		try {
			if (bCreateConnection)
				conn = ClsConfig.getDbConnection();
			int clauseVersionId = this.getClauseVersionId().intValue();
			if (this.clauseTable == null) {
				ClauseSectionTable.getInstance(conn);
				this.clauseTable = ClauseTable.getByClauseVersion(clauseVersionId, conn, false);
			}
			if (this.questionTable == null)
				this.questionTable = QuestionTable.getSubsetByClauseVersion(clauseVersionId, conn);
		}
		finally {
			if (bCreateConnection)
				SqlUtil.closeInstance(conn);
		}
		return true;
	}
	
	private String genDocToLinkAwardResponse(Connection conn) {
		if (!DocTypeRefRecord.DOC_TYPE_CD_SOLICITATION.equals(this.getDocumentType()))
			return JsonAble.jsonResult(JsonAble.STATUS_FAIL, null, "Not a solicitation document"); 
		if (!DocStatusRefRecord.CD_FINALIZED.equals(this.getDocumentStatus()))
			return JsonAble.jsonResult(JsonAble.STATUS_FAIL, null, "Not finalyzed solicitation document");

		ClauseVersionTable.getInstance(conn);
		
		ObjectMapper mapper = new ObjectMapper();
		ObjectNode objectNode = mapper.createObjectNode();
		ObjectNode nodeDocument = this.toJson(true);
		//int clauseVersionId = this.getClauseVersionId();
		//ClauseVersionRecord oDocClauseVersionRecord = ClauseVersionTable.getRecord(clauseVersionId);
		//nodeDocument.put(ClauseVersionRecord.FIELD_CLAUSE_VERSION_NAME, oDocClauseVersionRecord.getName());
		objectNode.put(DocumentRecord.TABLE_DOCUMENTS, nodeDocument);
		
		ClauseVersionRecord oActiveClauseVersionRecord = ClauseVersionTable.getActive();
		ObjectNode nodeClsVersion = oActiveClauseVersionRecord.genJson(false, mapper);
		objectNode.put(ClauseVersionRecord.TABLE_CLAUSE_VERSIONS, nodeClsVersion);
		
		String sMessage = null;
		Integer activeClsVersionId = oActiveClauseVersionRecord.getId();
		if (!activeClsVersionId.equals(this.getClauseVersionId())) {
			ArrayNode arrayNode = mapper.createArrayNode();
			try {
				ClauseVersionChangeTable.populateJsonForQuestionChanges(false, arrayNode, activeClsVersionId, conn);
				ClauseVersionChangeTable.populateJsonForClauseChanges(false, arrayNode, activeClsVersionId, conn);
			} catch (Exception oError) {
				oError.printStackTrace();
				sMessage = "Unable to list all changes";
			}
			objectNode.put("changeItems", arrayNode);
		}
		
		return JsonAble.jsonResult(JsonAble.STATUS_SUCCESS, objectNode, sMessage);
	}
	
	private String genDocToCopyResponse(Connection conn) {
		if ((!DocTypeRefRecord.DOC_TYPE_CD_SOLICITATION.equals(this.getDocumentType()))
		&& (!DocTypeRefRecord.DOC_TYPE_CD_AWARD.equals(this.getDocumentType())))
			return JsonAble.jsonResult(JsonAble.STATUS_FAIL, null, "Not a solicitation/award document"); 
		if (!DocStatusRefRecord.CD_FINALIZED.equals(this.getDocumentStatus()))
			return JsonAble.jsonResult(JsonAble.STATUS_FAIL, null, "Not a finalized solicitation/award document");
		
		ClauseVersionTable.getInstance(conn);
		
		String sMessage = null;		
		
		ObjectMapper mapper = new ObjectMapper();
		ObjectNode objectNode = mapper.createObjectNode();
		ObjectNode nodeDocument = this.toJson(true);
		objectNode.put(DocumentRecord.TABLE_DOCUMENTS, nodeDocument);
		
		/*
		ClauseVersionRecord oActiveClauseVersionRecord = ClauseVersionTable.getActive();
		ObjectNode nodeClsVersion = oActiveClauseVersionRecord.genJson(false, mapper);
		objectNode.put(ClauseVersionRecord.TABLE_CLAUSE_VERSIONS, nodeClsVersion);
		
		String sMessage = null;
		Integer activeClsVersionId = oActiveClauseVersionRecord.getId();
		if (!activeClsVersionId.equals(this.getClauseVersionId())) {
			ArrayNode arrayNode = mapper.createArrayNode();
			try {
				ClauseVersionChangeTable.populateJsonForQuestionChanges(false, arrayNode, activeClsVersionId, conn);
				ClauseVersionChangeTable.populateJsonForClauseChanges(false, arrayNode, activeClsVersionId, conn);
			} catch (Exception oError) {
				oError.printStackTrace();
				sMessage = "Unable to list all changes";
			}
			objectNode.put("changeItems", arrayNode);
		}
		*/
		
		return JsonAble.jsonResult(JsonAble.STATUS_SUCCESS, objectNode, sMessage);
	}
	
	private boolean upgradeClsVerOption(Integer currentClauseVersionId)
	{
		int clauseVersionId = this.getClauseVersionId();
		if (currentClauseVersionId == null)
		{
			return false;
		}
		if (currentClauseVersionId.intValue() == clauseVersionId)
		{
			return false;
		}
	
		return !this.isLinkedAward();
	}
	
	
	private String genInterviewJson(UserSession oUserSession, String sDo) throws Exception { // CJ-573
		String result = null; // JsonAble.jsonResult(JsonAble.STATUS_SUCCESS, clsDocument.getInterviewJson(), null); 
		Connection conn = null;
		try {
			conn = ClsConfig.getDbConnection();
		//	validateReopenAction(oUserSession, this.docId, conn);
			
			ObjectMapper mapper = new ObjectMapper();
			ObjectNode objectNode = mapper.createObjectNode();		
		
			ClauseVersionTable.getInstance(conn);
			Integer clsVersionId = ClauseVersionTable.getActiveVersionId();		
			
			boolean bUpgradeClsVerOption = DO_INTERVIEW.equals(sDo) && upgradeClsVerOption(clsVersionId);
			
			String origDocStatus = this.getDocumentStatus();
			
			/* Displaying versioning pop-up			
			boolean bUpgradeClsVerOption = true;
			if (this.getInterviewComplete() || (clsVersionId.intValue() == clauseVersionId)) {
				bUpgradeClsVerOption = false;
			} else {
				if (this.isLinkedAward()) {
					try {
						DocumentRecord documentRecord = DocumentTable.get(this.getSolicitationId(), conn);
						if (documentRecord != null) {
							if (clsVersionId.intValue() == documentRecord.getClauseVersionId())
								bUpgradeClsVerOption = false;
						}
					} catch (Exception oError) {
						oError.printStackTrace();
						bUpgradeClsVerOption = false;
					}
				} 
			}
			*/
			if (bUpgradeClsVerOption == false) { // if (this.getInterviewComplete() || (clsVersionId.intValue() == clauseVersionId)) {
				this.populageInterviewJson(conn, mapper, objectNode, sDo); // CJ-1223
				result = JsonAble.jsonResult(JsonAble.STATUS_SUCCESS, objectNode, null, oUserSession); // CJ-573
				
				// CJ-1410, add document resume/reopen audits
				if (this.getId() != null) {
					DocumentRecord docRecord = DocumentTable.get(this.getId(), conn);
					if (docRecord != null) {
						if (origDocStatus.equals ("F"))
							addReopenEvent(oUserSession, docId, docRecord, conn);
						else
							addResumeEvent(oUserSession, docId, docRecord, conn);
					}
				}
			} else {
				ArrayNode arrayNode = mapper.createArrayNode();
				String sMessage = null;
				try {
					ClauseVersionChangeTable.populateJsonForQuestionChanges(false, arrayNode, clsVersionId, conn);
					ClauseVersionChangeTable.populateJsonForClauseChanges(false, arrayNode, clsVersionId, conn);
				} catch (Exception oError) {
					oError.printStackTrace();
					sMessage = "Unable to list all changes";
				}
				objectNode.put(DocumentRecord.TABLE_DOCUMENTS, this.toJson(true));
				objectNode.put("changeItems", arrayNode);
				objectNode.put("count", arrayNode.size());
				objectNode.put("docName", this.getAcquisitionTitle());
				result = JsonAble.jsonResult("UpgradeClsVersion", objectNode, sMessage, oUserSession); // CJ-573
			}
		}
		finally {
			SqlUtil.closeInstance(conn);
		}
		return result;
	}
	
	private ObjectNode populageInterviewJson(Connection conn, ObjectMapper mapper, ObjectNode result, String sDo) throws SQLException {
		if (result == null) {
			//ObjectMapper mapper = new ObjectMapper();
			result = mapper.createObjectNode();
		}
		
		//Connection conn = null;
		boolean bConnectionCreated = (conn == null);
		try {
			if (bConnectionCreated)
				conn = ClsConfig.getDbConnection();
			if (this.prepareReferences(conn)) {
				if (DO_INTERVIEW.equals(sDo)) // CJ-1223
					this.changeStatusToIncomplete(conn); // CJ-1023
				int clauseVersionId = this.getClauseVersionId();
				result.put(RegulationRecord.TABLE_REGULATIONS, RegulationTable.getJson(mapper, conn, true));
				result.put(PrescriptionRecord.TABLE_PRESCRIPTIONS, PrescriptionTable.getJson(clauseVersionId, conn, mapper, true));
				
				if (DO_TRANSCRIPT.equals(sDo)) { // CJ-1223 added condition
					result.put(ClauseSectionRecord.TABLE_CLAUSE_SECTIONS, mapper.createArrayNode());
					result.put(ClauseRecord.TABLE_CLAUSES, mapper.createArrayNode());
				} else {
					result.put(ClauseSectionRecord.TABLE_CLAUSE_SECTIONS, ClauseSectionTable.getJson(mapper, true));
					result.put(ClauseRecord.TABLE_CLAUSES, this.clauseTable.toJson(mapper, true)); // ClauseTable.getJson(clauseVersionId, true, mapper));
				}
				result.put(QuestionConditionRecord.TABLE_QUESTION_CONDITIONS, QuestionConditionTable.getJson(clauseVersionId, conn, mapper, true));
				result.put(QuestionRecord.TABLE_QUESTIONS, this.questionTable.toJson(mapper, true)); // QuestionTable.getJson(clauseVersionId, conn, mapper, true));
				result.put(QuestionGroupRefRecord.TABLE_QUESTION_GROUP_REF, QuestionGroupRefTable.getJson(null, true));
				result.put(DocumentRecord.TABLE_DOCUMENTS, this.toJson(true));
				result.put(ClauseVersionRecord.TABLE_CLAUSE_VERSIONS, ClauseVersionTable.getJson(clauseVersionId, mapper, conn));
			}
		}
		finally {
			if (bConnectionCreated)
				SqlUtil.closeInstance(conn);
		}
		
		return result;
	}
	
	public ObjectNode getApiJson(boolean bForVer1, Connection conn) throws SQLException {
		ObjectMapper mapper = new ObjectMapper();
		ObjectNode result = mapper.createObjectNode();
		
		boolean bCreateConnection = (conn == null);
		try {
			if (bCreateConnection)
				conn = ClsConfig.getDbConnection();
			if (this.prepareReferences(conn)) {
				ObjectNode jsonRecord = mapper.createObjectNode(); 
				this.populateJsonForApi(jsonRecord, bForVer1, conn);
				this.populateJsonForApiVer1(jsonRecord, bForVer1);
				//this.documentClauseTable.populateJsonForApi(jsonRecord, bForVer1, this.clauseTable, this.documentFillInsTable);
				//this.documentAnswerTable.populateJsonForApi(jsonRecord, bForVer1, this.questionTable);
				result.put("document", jsonRecord);
			}
		}
		finally {
			if (bCreateConnection)
				SqlUtil.closeInstance(conn);
		}
		
		return result;
	}
	
	public void populateJsonForApiVer1(ObjectNode jsonRecord, boolean bForVer1) {
		this.documentClauseTable.populateJsonForApi(jsonRecord, bForVer1, this.clauseTable, this.documentFillInsTable);
		this.documentAnswerTable.populateJsonForApi(jsonRecord, bForVer1, this.questionTable);
		
		if (bForVer1) {
			ObjectNode fillinNode = jsonRecord.objectNode();
			fillinNode.put("document_id", this.docId);
			this.documentFillInsTable.populateJsonForApi(fillinNode, bForVer1);
			jsonRecord.put("document_fill_ins", fillinNode);
		}
	}
	
	private JsonNode postToJson(String postedJsonString) throws JsonParseException, IOException {
		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(org.codehaus.jackson.map.SerializationConfig.Feature.INDENT_OUTPUT, true);
		JsonFactory factory = mapper.getJsonFactory();
		JsonParser jp = factory.createJsonParser(postedJsonString);
		return mapper.readTree(jp);
	}

	private boolean readPost(String postedJsonString) throws JsonParseException, IOException {
		boolean result = false;
		
		JsonNode jsonNode = this.postToJson(postedJsonString);
		this.fromJson(jsonNode);

		return result;
	}

	private static void logDebug (UserSession oUserSession, String prefix, Integer id, String message, String error) {
		
		String email = "";
		
		if ((oUserSession != null) && (oUserSession.getEmail() != null)) email = oUserSession.getEmail();
		if (prefix == null) prefix = "";
		if (id == null) id = 0;
		if (message == null) message = "";
		if (error == null) error = "";			
		 
		logger.debug(email + ": " + prefix + " [" + id.toString() + "] " + message + " " + error);
	}
	
	private static void logInfo (UserSession oUserSession, String prefix, Integer id, String message, String error) {

		String email = "";
		
		if ((oUserSession != null) && (oUserSession.getEmail() != null)) email = oUserSession.getEmail();
		if (prefix == null) prefix = "";
		if (id == null) id = 0;
		if (message == null) message = "";
		if (error == null) error = "";			
		 
		logger.info(email + ": " + prefix + " [" + id.toString() + "] " + message + " " + error);

	}

	public String genFileName(String extension) {
		return "cls_" + ("S".equals(this.getDocumentType()) ? "sol" : "awd")
				+ "_" + this.getDocumentNumber() + "." + this.getId() //  + ")"
				+ ((extension == null) ? "" : "." + extension);
	}
	
	// --------------------------------------------------------
	public Integer getDocId() {
		return docId;
	}

	public QuestionTable getQuestions() {
		return questionTable;
	}

	public ClauseTable getClauses() {
		return clauseTable;
	}

	public Exception getLastError() {
		return lastError;
	}
	
}
