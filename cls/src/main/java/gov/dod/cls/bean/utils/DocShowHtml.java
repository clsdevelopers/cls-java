package gov.dod.cls.bean.utils;

import java.util.ArrayList;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import gov.dod.cls.PageApi;
import gov.dod.cls.bean.ClsDocument;
import gov.dod.cls.db.ClauseRecord;
import gov.dod.cls.db.ClauseSectionRecord;
import gov.dod.cls.db.ClauseSectionTable;
import gov.dod.cls.db.ClauseTable;
import gov.dod.cls.db.DocumentClauseRecord;
import gov.dod.cls.db.DocumentClauseTable;
import gov.dod.cls.db.DocumentFillInsRecord;
import gov.dod.cls.db.DocumentFillInsTable;
import gov.dod.cls.utils.ClsConstants;
import gov.dod.cls.utils.CommonUtils;

public class DocShowHtml {

	private ClsDocument clsDocument;
	private boolean forApi = false;
	
	private String tagSection;
	private String tagInclusion;
	
	public static void downloadDoc(ClsDocument clsDocument, HttpServletRequest req, HttpServletResponse resp) {
		DocShowHtml docShowHtml = new DocShowHtml(clsDocument);
		PageApi.send(resp, "attachment; filename=\"" + clsDocument.genFileName("docx") + "\"",
				"application/msword", // "text/html",
				stripSpecialChars(docShowHtml.genHtml()));
	}
	
	public static void downloadHtml(ClsDocument clsDocument, HttpServletRequest req, HttpServletResponse resp) {
		DocShowHtml docShowHtml = new DocShowHtml(clsDocument);
		resp.setContentType("application/force-download");
		resp.setHeader("Content-Transfer-Encoding", "binary");
		PageApi.send(resp, "attachment; filename=\"" + clsDocument.genFileName("html") + "\"",
				"application/octet-stream", // "text/html",
				docShowHtml.genHtml());
	}
	
	public DocShowHtml(ClsDocument clsDocument) {
		this.clsDocument = clsDocument;
	}
	
	public String genHtml() {
		return this.genHtml(false);
	}
	
	private void setForApi(boolean forApi) {
		this.forApi = forApi;
		this.tagSection = (forApi ? "" : "h2");
		this.tagInclusion = (forApi ? "h4" : "h3");
	}
	
	public String genHtml(boolean forApi) {
		this.setForApi(forApi);
		
		StringBuffer sb = new StringBuffer();
		
		if (this.forApi == false)
			this.generateHeader(sb);

		DocumentClauseTable documentClauseTable = this.clsDocument.getDocumentClauseTable();
		DocumentFillInsTable docFillinsTable = this.clsDocument.getFillIns();
		
		ClauseSectionTable clauseSectionTable = ClauseSectionTable.getInstance(null);

		ArrayList<DocumentClauseRecord> farClausesByReference = new ArrayList<DocumentClauseRecord>(); 
		ArrayList<DocumentClauseRecord> farClausesByText = new ArrayList<DocumentClauseRecord>(); 

		ArrayList<DocumentClauseRecord> dfarClausesByReference = new ArrayList<DocumentClauseRecord>(); 
		ArrayList<DocumentClauseRecord> dfarClausesByText = new ArrayList<DocumentClauseRecord>(); 
		
		ArrayList<DocumentClauseRecord> localClausesByReference = new ArrayList<DocumentClauseRecord>(); 
		ArrayList<DocumentClauseRecord> localClausesByText = new ArrayList<DocumentClauseRecord>(); 

		ArrayList<DocumentClauseRecord> clausesByReference; 
		ArrayList<DocumentClauseRecord> clausesByText; 

		for (ClauseSectionRecord clauseSectionRecord : clauseSectionTable) {
			ClauseTable clauseTable = clsDocument.getClauses().subsetBySection(clauseSectionRecord.getId(), true);
			for (ClauseRecord clauseRecord : clauseTable) {
				DocumentClauseRecord documentClauseRecord = documentClauseTable.findByClauseId(clauseRecord.getId(), true);
				if ((documentClauseRecord != null) && (!documentClauseRecord.isRemoved())) {
					documentClauseRecord.setClauseRecord(clauseRecord);

					if (clauseRecord.isFarClause()) {
						clausesByReference = farClausesByReference; 
						clausesByText = farClausesByText;
					} else if (clauseRecord.isDfarClause()) {
						clausesByReference = dfarClausesByReference; 
						clausesByText = dfarClausesByText;
					} else {
						clausesByReference = localClausesByReference; 
						clausesByText = localClausesByText;
					} 
					//if (clauseRecord.isFullText())
					if (documentClauseRecord.isEditFullText(docFillinsTable))
						clausesByText.add(documentClauseRecord);
					else
						clausesByReference.add(documentClauseRecord);
					/*
					if ("F".equals(clauseRecord.getInclusion()))
						farClausesByText.add(documentClauseRecord);
					else
						farClausesByReference.add(documentClauseRecord);
					*/
				}
			}
			// CJ-1206, Sort the clauses
			farClausesByReference = sortClauses(farClausesByReference);
			farClausesByText = sortClauses(farClausesByText);
			dfarClausesByReference = sortClauses(dfarClausesByReference);
			dfarClausesByText = sortClauses(dfarClausesByText);
			localClausesByReference = sortClauses(localClausesByReference);
			localClausesByText = sortClauses(localClausesByText);
			
			if (!(farClausesByText.isEmpty() && farClausesByReference.isEmpty() &&
					dfarClausesByText.isEmpty() && dfarClausesByReference.isEmpty() &&
					localClausesByText.isEmpty() && localClausesByReference.isEmpty() )) {
			//	if ((!farClausesByText.isEmpty()) || (!farClausesByReference.isEmpty())) {
				if (this.forApi == false)
					sb.append("<p class='margin-top-50'>\n");
				this.generateSection(sb, clauseSectionRecord, farClausesByReference, farClausesByText,
						dfarClausesByReference, dfarClausesByText, localClausesByReference, localClausesByText);
				if (this.forApi == false)
					sb.append("</p\n");

				farClausesByReference.clear();
				farClausesByText.clear();

				dfarClausesByReference.clear(); 
				dfarClausesByText.clear(); 
				
				localClausesByReference.clear(); 
				localClausesByText.clear(); 
			}
		}
		
		if (this.forApi == false)
			this.generateFooter(sb);
		
		return sb.toString();
	}
	
	private void addPageBreak(StringBuffer sb) {
		if (this.forApi == false)
			sb.append("<div class='page-break'></div>\n");
	}
	
	private void generateSection(StringBuffer sb, ClauseSectionRecord clauseSectionRecord,
			ArrayList<DocumentClauseRecord> farClausesByReference, ArrayList<DocumentClauseRecord> farClausesByText,
			ArrayList<DocumentClauseRecord> dfarClausesByReference, ArrayList<DocumentClauseRecord> dfarClausesByText,
			ArrayList<DocumentClauseRecord> localClausesByReference, ArrayList<DocumentClauseRecord> localClausesByText) {
		// ClauseRecord clauseRecord;
		this.addPageBreak(sb);
		if (this.tagSection.isEmpty())
			sb.append(clauseSectionRecord.getFullName() + "\n");
		else
			sb.append("<" + this.tagSection + ">" + clauseSectionRecord.getFullName() + "</" + this.tagSection + ">\n");
		
		this.generateSection(sb, ClauseRecord.FAR_CLAUSE, farClausesByReference, farClausesByText);
		this.generateSection(sb, ClauseRecord.DFAR_CLAUSE, dfarClausesByReference, dfarClausesByText);
		this.generateSection(sb, ClauseRecord.LOCAL_CLAUSE, localClausesByReference, localClausesByText);
	}

	private void generateSection(StringBuffer sb, String clauseType,
			ArrayList<DocumentClauseRecord> clausesByReference, ArrayList<DocumentClauseRecord> clausesByText) {
		ClauseRecord clauseRecord;
		
		if (!clausesByReference.isEmpty()) {
			sb.append("<" + this.tagInclusion + ((this.forApi == true) ? " class='margin-top-20'" : "")
					+ ">" + clauseType + " CLAUSES INCORPORATED BY REFERENCE</" + this.tagInclusion + ">\n");
			sb.append("<table class='" + ((this.forApi == true) ? "margin-top-20" : "tableClausesByReference") + "'>\n");
			for (DocumentClauseRecord documentClauseRecord : clausesByReference) {
				clauseRecord = documentClauseRecord.getClauseRecord();
				// CJ-843, append effective date to the title
				String sTitleDate = clauseRecord.getClauseTitleText();
				if (clauseRecord.getEffectiveMonthYear().length() > 0)
					sTitleDate = sTitleDate + " (" + clauseRecord.getEffectiveMonthYear() + ")";
				
				sb.append("<tr>");
				sb.append("<td>" + clauseRecord.getClauseName() + "</td>");
				sb.append("<td>" + sTitleDate + "</td>"); // CJ-843
				//sb.append("<td>" + clauseRecord.getClauseTitleText() + "</td>"); // CJ-843
				//sb.append("<td>" + clauseRecord.getEffectiveMonthYear() + "</td>"); // CJ-843
				sb.append("</tr>\n");
			}
			sb.append("</table>\n");
			// sb.append("<p>&nbsp;</p>\n");
		}
		
		if (!clausesByText.isEmpty()) {
			sb.append("<" + this.tagInclusion + ((this.forApi == true) ? " class='margin-top-20'" : "")
					+ ">" + clauseType + " CLAUSES INCORPORATED BY FULL TEXT</" + this.tagInclusion + ">\n");
			ArrayList<DocumentClauseRecord> altClauses = new ArrayList<DocumentClauseRecord>(); 
			int iRecord = 0;
			while (iRecord < clausesByText.size()) {
				DocumentClauseRecord documentClauseRecord = clausesByText.get(iRecord++);
				clauseRecord = documentClauseRecord.getClauseRecord();
				
				//boolean isBasicClause = clauseRecord.isBasicClause();
				String sNamePrefix = clauseRecord.getClauseName().toUpperCase(Locale.ENGLISH).split(" ")[0];
				//String sClauseName = clauseRecord.getClauseName().toUpperCase(Locale.ENGLISH);;
				//if (sClauseName.contains("ALTERNATE")) //Alternate added with no non-alternate present
				//	isBasicClause = true;
				
				while (iRecord < clausesByText.size()) {
					DocumentClauseRecord altDocumentClauseRecord = clausesByText.get(iRecord);
					ClauseRecord altClauseRecord = altDocumentClauseRecord.getClauseRecord();
					String altClauseName = altClauseRecord.getClauseName().toUpperCase(Locale.ENGLISH);
					if (altClauseName.toUpperCase(Locale.ENGLISH).startsWith(sNamePrefix) && 
							(altClauseName.contains("ALTERNATE") || altClauseName.contains("DEVIATION"))) {
						altClauses.add(altDocumentClauseRecord);
						iRecord++;
					} else
						break;
				}
				String sHeading = clauseRecord.getHeading(documentClauseRecord, this.forApi);
				DocumentFillInsTable aFillIns = this.clsDocument.getFillIns().collectByClause(clauseRecord.getId().intValue());
				// CJ-533, support editables with fillins
				String clauseText = "";
				if (clauseRecord.isEditable() 
						&& (clauseRecord.getEditableRemarks() != null)
						&& ((clauseRecord.getEditableRemarks().equals("ALL")) 
						|| (clauseRecord.getEditableRemarks().startsWith("ALL")))) {
					
					String sFillInName = clauseRecord.getClauseData();
					sFillInName = sFillInName.replace("{{", "");
					sFillInName = sFillInName.replace("}}", "");
					DocumentFillInsRecord oDocFillIn = aFillIns.findByClauseFillInCode(sFillInName);
					
					if (oDocFillIn != null)
						clauseText = clauseRecord.getClauseDataWFillins(aFillIns, oDocFillIn.getAnswer(), ClsConstants.OutputType.HTML);
					else {
						clauseText = clauseRecord.getClauseData(aFillIns, ClsConstants.OutputType.HTML);
						clauseText = clauseRecord.getClauseDataWFillins(aFillIns, clauseText, ClsConstants.OutputType.HTML);
					}
				}
				// CJ-516
				else if (clauseRecord.isEditable() 
						&& (clauseRecord.getEditableRemarks() != null)
						&& (clauseRecord.getEditableRemarks().startsWith("PARENT"))) {
					
					String sContent = clauseRecord.getClauseData();

					Integer	pos1 = sContent.indexOf("{{", 0);
					Integer	pos2 = sContent.indexOf("}}", 0);
					String sFillInName = "";
						
					if ((pos1 >= 0) &&  (pos2 >= 0)) {
						sFillInName = sContent.substring(pos1+2, pos2);
						DocumentFillInsRecord oDocFillIn = aFillIns.findByClauseFillInCode(sFillInName);
						
						if (oDocFillIn != null) 
							sContent = sContent.replace ("{{" + sFillInName + "}}", oDocFillIn.getAnswer());
						else
							sContent = clauseRecord.getClauseDataWFillins(aFillIns, sContent, ClsConstants.OutputType.HTML);
					}
					clauseText = clauseRecord.getClauseDataWFillins(aFillIns, sContent, ClsConstants.OutputType.HTML);
				}
				else
					clauseText = clauseRecord.getClauseData(aFillIns, ClsConstants.OutputType.HTML);

				clauseText = clauseText.replaceAll("<h1>", "<h4>").replaceAll("</h1>", "</h4>"); // CJ-800
				
				// CJ-791
				if (this.forApi) {
					sb.append("<div class='margin-top-30'>");
					sb.append(sHeading);
					sb.append("<p>\n");
					sb.append(clauseText);
					//sb.append(clauseRecord.getFooterText());
					sb.append("</p>\n");
				} else {
					sb.append("<p><b>" + sHeading + "</b></p>");
					sb.append("<div class='divClauseText'>\n");
					sb.append(clauseText);
					//sb.append("<p>" + clauseRecord.getFooterText() + "</p>\n");
				}
		        sb.append("<p>" + clauseRecord.getEndOfProvisonOrClauseText() + "</p>"); // CJ-817 ClauseRecord.END_OF_PROVISION
	        	// sb.append(sClauseName.contains("ALTERNATE") ? ClauseRecord.END_OF_PROVISION : ClauseRecord.END_OF_CLAUSE); // CJ-791
				sb.append("</div>\n");
				
				for (DocumentClauseRecord altDocumentClauseRecord : altClauses) {
					ClauseRecord altClauseRecord = altDocumentClauseRecord.getClauseRecord();
					sHeading = altClauseRecord.getHeading(altDocumentClauseRecord, this.forApi);
					//if (isBasicClause)
					//	clauseText += "<p><b>" + altClauseRecord.getHeading(altDocumentClauseRecord, this.forApi)
					//	+ "</b></p>";
					aFillIns = this.clsDocument.getFillIns().collectByClause(altClauseRecord.getId().intValue());
					
					// CJ-1336
					String sFillInName = clauseRecord.getClauseData();
					sFillInName = sFillInName.replace("{{", "");
					sFillInName = sFillInName.replace("}}", "");
					DocumentFillInsRecord oDocFillIn = aFillIns.findByClauseFillInCode(sFillInName);
					
					if (oDocFillIn != null)
						clauseText = altClauseRecord.getClauseDataWFillins(aFillIns, oDocFillIn.getAnswer(), ClsConstants.OutputType.HTML);
					else {
						clauseText = altClauseRecord.getClauseData(aFillIns, ClsConstants.OutputType.HTML);
						clauseText = altClauseRecord.getClauseDataWFillins(aFillIns, clauseText, ClsConstants.OutputType.HTML);
					} 
					// clauseText = altClauseRecord.getClauseData(aFillIns, ClsConstants.OutputType.HTML);
					// end CJ-1336
					
					clauseText = clauseText.replaceAll("<h1>", "<h4>").replaceAll("</h1>", "</h4>"); // CJ-800
					// CJ-791
					if (this.forApi) {
						sb.append("<div class='margin-top-30'>");
						sb.append(sHeading);
						sb.append("<p>\n");
						sb.append(clauseText);
						sb.append("</p>\n");
					} else {
						sb.append("<p><b>" + sHeading + "</b></p>");
						sb.append("<div class='divClauseText'>\n");
						sb.append(clauseText);
					}
			        sb.append("<p>" + altClauseRecord.getEndOfProvisonOrClauseText() + "</p>"); // CJ-817
					sb.append("</div>\n");
				}
				altClauses.clear();
			}

			if (this.forApi == false)
				sb.append("<p>&nbsp;</p>\n");
		}
	}

	private void generateHeader(StringBuffer sb) {
		sb.append("<!doctype html>\n");
		sb.append("<html>\n");
		sb.append("<head>\n");
		sb.append("<meta http-equiv='Cache-Control' content='no-cache, no-store, must-revalidate' />\n");
		sb.append("<meta http-equiv='Pragma' content='no-cache' />\n");
		sb.append("<meta http-equiv='Expires' content='0' />\n");
		sb.append("<meta charset='utf-8'>\n");
		sb.append("<title>Document #" + this.clsDocument.getDocumentNumber() + "</title>\n");
		sb.append("<style>\n");
		sb.append(	".spanData {font-family:\"Courier New\", Consolas, \"Andale Mono\", \"Lucida Console\", \"Lucida Sans Typewriter\", Monaco, monospace;font-weight:bold;}\n");
		sb.append(	".clsData {font-family:Consolas, \"Andale Mono\", \"Lucida Console\", \"Lucida Sans Typewriter\", Monaco, \"Courier New\", monospace;font-weight:bold;}\n");
		sb.append(	"@media all { .page-break { display: none; } }\n");
		sb.append(	"@media print { .page-break	{ display: block; page-break-before: always; } }\n");
		sb.append(	".hd1 { display: none; }\n"); //sb.append(".hd1 { font-size: 110%; font-weight: bold;}\n");
		sb.append(	".tableDocInfo { border: #d0d0d0 solid 1px; border-collapse: collapse; }\n");
		sb.append(	".tableDocInfo th, .tableDocInfo td { padding: 3px; border: #d0d0d0 solid 1px; }\n");
		sb.append(	".tableDocInfo th { text-align:right; }\n");
		sb.append(	".tableEditClause { border: #d0d0d0 solid 1px; border-collapse: collapse; }\n");
		sb.append(	".tableEditClause th, .tableEditClause td { padding: 3px; border: #d0d0d0 solid 1px; }\n");
		sb.append(	"h4 { text-align:center; }\n");
		sb.append("</style>\n");
		sb.append("</head>\n");

		sb.append("<body>\n");
		
		sb.append("<table class='tableDocInfo'>\n");
		sb.append(	"<tr>");
		sb.append(		"<th>Acquisition Title</th>");
		sb.append(		"<td>" + this.clsDocument.getAcquisitionTitle() + "</td>");
		sb.append(	"</tr>\n");
		sb.append(	"<tr>");
		sb.append(		"<th>Document Number</th>");
		sb.append(		"<td>" + this.clsDocument.getDocumentNumber() + "</td>");
		sb.append(	"</tr>\n");
		sb.append(	"<tr>");
		sb.append(		"<th>Document Type</th>");
		sb.append(		"<td>" + this.clsDocument.getDocumentTypeName() + "</td>");
		sb.append(	"</tr>\n");
		sb.append(	"<tr>");
		sb.append(		"<th>Creation Date</th>");
		sb.append(		"<td>" +  CommonUtils.toDateTimeBrief(this.clsDocument.getCreatedAt()) + "</td>");
		sb.append(	"</tr>\n");
		sb.append(	"<tr>");
		sb.append(		"<th>Last Updated</th>");
		sb.append(		"<td>" + CommonUtils.toDateTimeBrief(this.clsDocument.getUpdatedAt()) + "</td>");
		sb.append(	"</tr>\n");
		sb.append("</table>");
		//sb.append("<p><strong>Document Information:</strong></p>\n");
	}
	
	private void generateFooter(StringBuffer sb) {
		sb.append("</body>\n");
		sb.append("</html>\n");
	}
	
	// IE / MS Word cannot handle some special characters/sequences.
	private static String stripSpecialChars(String html) {
		
		if (html == null)
			return html;
		
		html = html.replace ("&squ;", "[<span class='spanData'>_____</span>]");
		html = html.replace ("&#x25a1;", "[<span class='spanData'>_____</span>]");
		
		//html = html.replace ("&rdquor;", "\"");
		html = html.replace ("&rdquor;", "&rdquo;");
		
		return html;
	}
	
	/*
	public static void main(String[] args) {
		String text = "(a) {{Any}} supplies and services to be furnished under this contract shall be ordered by issuance of delivery orders or task orders by the individuals or activities designated in the Schedule. Such orders may be issued from {{start_date}} through {{end_date}} [insert dates].\n"
				+ "(b) All delivery orders or task orders are subject to the terms and conditions of this contract. In the event of conflict between a delivery order or task order and this contract, the contract shall control.\n"
				+ "(c) If mailed, a delivery order or task order is considered “issued” when the Government deposits the order in the mail. Orders may be issued orally, by facsimile, or by electronic commerce methods only if authorized in the Schedule.";
		
		Pattern pattern = Pattern.compile("\\{\\{(.+?)\\}\\}"); // "an\\s+(.*?)\\s+for"
		Matcher matcher = pattern.matcher(text);
	    // check all occurance
	    while (matcher.find()) {
	      System.out.print("Start index: " + matcher.start());
	      System.out.print(" End index: " + matcher.end() + " ");
	      System.out.println(matcher.group());
	    }
	}
	*/

	private ArrayList<DocumentClauseRecord> sortClauses(ArrayList<DocumentClauseRecord> srcClauses) {
		
		ArrayList<DocumentClauseRecord> dstClauses = new ArrayList<DocumentClauseRecord>(); 

		for (DocumentClauseRecord srcRecord : srcClauses) {
			Integer ndx = 0; 
			Boolean bAdded = false;
			if (dstClauses.size() == 0) {
				dstClauses.add(0, srcRecord);
				continue;
			}
			for (DocumentClauseRecord destRecord : dstClauses) {

				if (ClauseTable.compareClauseName(srcRecord.getClauseName(), destRecord.getClauseName())<0) {
					dstClauses.add(ndx, srcRecord);
					bAdded = true;
					break;
				}
				ndx++;
			}
			if (!bAdded) {
				dstClauses.add(srcRecord);
			}
			
		}
		
		srcClauses.clear();
		return dstClauses;
	}
}