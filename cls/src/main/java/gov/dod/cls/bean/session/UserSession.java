package gov.dod.cls.bean.session;
import org.apache.log4j.Logger;

import java.net.URLEncoder;
import java.security.cert.X509Certificate;
import java.sql.Connection;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.node.ObjectNode;

import gov.dod.cls.PageApi;
import gov.dod.cls.bean.ClsDocument;
import gov.dod.cls.config.ClsConfig;
import gov.dod.cls.db.OauthApplicationTable;
import gov.dod.cls.db.OauthInterviewRecord;
import gov.dod.cls.db.UserRecord;
import gov.dod.cls.db.UserTable;
import gov.dod.cls.exception.InvalidDataException;
import gov.dod.cls.exception.UnrecoverableException;
import gov.dod.cls.filter.SessionInCookieFilter;
import gov.dod.cls.model.JsonAble;
import gov.dod.cls.reference.ClsPages;
import gov.dod.cls.utils.CommonUtils;
import gov.dod.cls.utils.URLEncodeUtils;
import gov.dod.cls.utils.sql.SqlUtil;

public class UserSession extends JsonAble implements java.io.Serializable {

	private static final long serialVersionUID = 3492227366948617901L;

	private static final Logger logger = Logger.getLogger(UserSession.class.getName());
	
	public static final String PREFIX_DO_SESSION = "session-";

	public final static String DO_SESSION_LOGIN = PREFIX_DO_SESSION + "login"; // "session-login"
	public final static String DO_SESSION_LOGOUT = PREFIX_DO_SESSION + "logout"; // "session-logout"
	public final static String DO_SESSION_GET_LOGIN = PREFIX_DO_SESSION + "getLogin"; // "session-getLogin"
	public final static String DO_SESSION_SETTINGS_UPDATE = PREFIX_DO_SESSION + "settings-update";
	public final static String DO_SESSION_CHANGE_PASSWORD = PREFIX_DO_SESSION + "change_password";
	public final static String DO_SESSION_CHANGE_PASSWORD2 = PREFIX_DO_SESSION + "change_password_2"; // CJ-700
	public final static String DO_SESSION_REGISTER = PREFIX_DO_SESSION + "register";
	public final static String DO_SESSION_RESET_PASSWORD = PREFIX_DO_SESSION + "reset-password";
	public final static String DO_SESSION_CONTINUE_SESSION = PREFIX_DO_SESSION + "continueSession"; // CJ-634
	
	public final static String CAC_HEADER = "AJP_CUSTOM_SSL_CLIENT_CERT";
	
	private Date sessionStarted = null;
	private Date lastLoginTried = null;
	private int loginTried = 0;
	private UserRecord userRecord = null;
	private OauthInterviewRecord oauthInterviewRecord = null;
	private String sessionMessage = null;
	private String messagePage = null;

	//private HttpSession session = null; // CJ-573
	private Integer iMaxInactiveInterval = null;
	private Long lLastAccessedTime = null;
	
	public UserSession(Date sessionStarted) {
		super();
		this.sessionStarted = sessionStarted;
	}

	public boolean isVerified() {
		return (this.userRecord != null) || (this.oauthInterviewRecord != null);
	}

	public boolean isSuperUser() {
		return (this.userRecord != null) && this.userRecord.getRoles().isSuperUser();
	}
	
	public boolean isSubsuperUser() {
		return (this.userRecord != null) && this.userRecord.getRoles().isSubsuperUser();
	}
	
	public boolean isAgencyReviewer() {
		return (this.userRecord != null) && this.userRecord.getRoles().isAgencyReviewer();
	}
	
	public boolean isReviewer() {
		return (this.userRecord != null) && this.userRecord.getRoles().isReviewer();
	}
	
	public boolean isRegularUser() {
		return (this.userRecord != null) && this.userRecord.getRoles().isRegularUser();
	}
	
	public Integer getOrgId() {
		return (this.userRecord == null) ? null : this.userRecord.getOrgId();
	}

	public Date getSessionStarted() { return sessionStarted; }

	public Date getLastLoginTried() { return lastLoginTried; }

	public int getLoginTried() { return loginTried; }

	public UserRecord getUserProfile() { return userRecord; }
	
	public Integer getUserId() {
		return (this.userRecord == null) ? null : userRecord.getId();
	}
	
	public Integer getApplicationId() {
		return (this.oauthInterviewRecord == null) ? null : this.oauthInterviewRecord.getApplicationId();
	}
	
	public String getEmail() {
		return (this.userRecord == null) ? null : userRecord.getEmail();
	}
	
	public Integer getiMaxInactiveInterval() { return iMaxInactiveInterval; }
	public void setiMaxInactiveInterval(Integer iMaxInactiveInterval) { this.iMaxInactiveInterval = iMaxInactiveInterval; }

	public Long getlLastAccessedTime() { return lLastAccessedTime; }
	public void setlLastAccessedTime(Long lLastAccessedTime) { this.lLastAccessedTime = lLastAccessedTime; }

	//public HttpSession getSession() { return session; }
	public void setSession(HttpSession session) {
		this.iMaxInactiveInterval = session.getMaxInactiveInterval();
		this.lLastAccessedTime = session.getLastAccessedTime();
		//this.session = session;
	}

	public String getSessionMessage() { return sessionMessage; }
	public void setSessionMessage(String sessionMessage) {
		this.sessionMessage = sessionMessage;
		this.messagePage = null;
	}

	public String getMessagePage() { return messagePage; }
	public void setMessagePage(String messagePage) {
		this.messagePage = messagePage;
		this.sessionMessage = null;
	}
	
	public static String getSessionMessage(HttpServletRequest request) {
		HttpSession session = request.getSession(false);
		if (session == null)
			return "";
		UserSession oUserSession = (UserSession)session.getAttribute(UserSession.class.getName());
		if (oUserSession == null)
			return "";
		if (oUserSession.sessionMessage == null)
			return "";
		String result = oUserSession.sessionMessage;
		oUserSession.setMessagePage(null);
		return result;
	}

	public OauthInterviewRecord getOauthInterviewRecord() { return oauthInterviewRecord; }
	//CJ-1441
	private boolean checker(String input){
		boolean check = true;
		String errorMsg = "";
		
		 if (input.indexOf('<') > -1){
			 	check = false;
		    }
		 else if (input.indexOf('>') > -1){
		    	check = false;
		    }
		 else if (input.indexOf('/') > -1){
		    	check = false;
		    }
		 else if (input.indexOf('(') > -1){
		    	check = false;
		    }
		 else if (input.indexOf(')') > -1){
		    	check = false;
		    }
		 else if (input.indexOf('{') > -1){
		    	check = false;
		    }
		 else if (input.indexOf('}') > -1){
		    	check = false;
		    }
		 return check;
		}

public void accountSettingUpdate(HttpServletRequest req, HttpServletResponse resp) {
			if (this.userRecord == null)
				return;
			//CJ-1441
			boolean check = true;
			boolean check1 = true;
			boolean check2 = true;
			String email = req.getParameter("email");
			check = checker(email);
			String first_name = req.getParameter("first_name");
			check1 = checker(first_name);
			String last_name = req.getParameter("last_name");
			check2 = checker(last_name);
			String time_zone = req.getParameter("time_zone");
			
			//CJ-1441
			if (check == false || check1 == false || check2 == false){
				this.sessionMessage = "Unable to process special character: < > / ( ) { } ";
				PageApi.sendRedirect(null, req, resp, ClsPages.PAGE_ACCOUNT_DASHBOARD);
			    }
			else {
				
				if (this.userRecord.accountSettingUpdate(email, first_name, last_name, time_zone)) {
					this.sessionMessage = "Account settings changed successfully!";
					PageApi.sendRedirect(null, req, resp, ClsPages.afterLogon(null, this));
				} else {
					if (this.userRecord.getLastError() != null)
						this.sessionMessage = "Unable to save.\n" + this.userRecord.getLastError().getMessage();
					else
						this.sessionMessage = "Nothing is changed.";
					PageApi.sendRedirect(null, req, resp, ClsPages.PAGE_ACCOUNT_SETTINGS);
				}
			}
		}
	
	public void accountChangePasssword(HttpServletRequest req, HttpServletResponse resp) {
		if (this.userRecord == null)
			return;
		String current_password = req.getParameter("current_password");
		String new_password = req.getParameter("new_password");
		if (!this.userRecord.isPasswordSame(current_password)) {
			this.sessionMessage += "Invalid current password.";
			PageApi.sendRedirect(null, req, resp, ClsPages.PAGE_ACCOUNT_CHANGE_PASSWORD);
			return;
		}
		if (!UserRecord.isValidPassword(new_password)) {
			this.sessionMessage += "Invalid new password.";
			PageApi.sendRedirect(null, req, resp, ClsPages.PAGE_ACCOUNT_CHANGE_PASSWORD);
			return;
		}
		if (this.userRecord.changePassword(new_password)) {
			this.sessionMessage = "Changing password successfully!";
			HttpSession session = req.getSession(false);
			SessionInCookieFilter.update(session, req, resp);
			PageApi.sendRedirect(null, req, resp, ClsPages.afterChangePassword(null, this));
		} else {
			this.sessionMessage = "Unable to save.";
			if (this.userRecord.getLastError() != null)
				this.sessionMessage += "\n" + this.userRecord.getLastError().getMessage();
			PageApi.sendRedirect(null, req, resp, ClsPages.PAGE_ACCOUNT_CHANGE_PASSWORD);
		}
	}
	
	// CJ-700, the redirect and session message did not work for invalid passwords.
	// The user never received an error message on the screen.
	public void accountChangePasssword_v2(HttpServletRequest req, HttpServletResponse resp) {
		if (this.userRecord == null)
			return;
		String current_password = req.getParameter("current_password");
		String new_password = req.getParameter("new_password");
		String response = null;

		if (!this.userRecord.isPasswordSame(current_password)) {
			response = JsonAble.jsonResult(JsonAble.STATUS_FAIL, null, "Invalid current password.");
			PageApi.send(resp, JsonAble.JSON_RESPONSE_FORMAT, response);
			return;
		}
		if (!UserRecord.isValidPassword(new_password)) {
			response = JsonAble.jsonResult(JsonAble.STATUS_FAIL, null, "Invalid new password.");
			PageApi.send(resp, JsonAble.JSON_RESPONSE_FORMAT, response);
			return;
		}
		if (this.userRecord.changePassword(new_password)) {
			HttpSession session = req.getSession(false);
			SessionInCookieFilter.update(session, req, resp);
			response = JsonAble.jsonResult(JsonAble.STATUS_SUCCESS, null, "Changing password successfully!");
			PageApi.send(resp, JsonAble.JSON_RESPONSE_FORMAT, response);
		} else {
			response = JsonAble.jsonResult(JsonAble.STATUS_FAIL, null, "Unable to save.");
			PageApi.send(resp, JsonAble.JSON_RESPONSE_FORMAT, response);
		}
	}
	
	public void accountAssignCac(HttpServletRequest req, HttpServletResponse resp) {
		String sMessage = null;
		boolean bSuccess = false;
		ObjectMapper mapper = new ObjectMapper();
		ObjectNode json = mapper.createObjectNode();

		try {
			if (this.userRecord == null) {
				sMessage = "No session";
			} else {
				String cacHash = null;
				String rawCert = req.getHeader(CAC_HEADER);
				if (rawCert == null)
					System.out.println("Failed request (" + this.userRecord.getEmail() + ") for accountAssignCac()::getHeader().");
				
				if (rawCert != null) {
					X509Certificate cert = CommonUtils.getX509Cert(rawCert);
					if (cert == null)
						System.out.println("Failed request (" + this.userRecord.getEmail() + ") for accountAssignCac()::getX509Cert().");

					cacHash = CommonUtils.getCacHashFromCert(cert);
					
					if (cacHash == null)
						System.out.println("Failed request (" + this.userRecord.getEmail() + ") for accountAssignCac()::getCacHashFromCert().");
				}
				if (cacHash != null && this.userRecord.assignCac(cacHash)){
					sMessage = "CAC registered successfully!";
					HttpSession session = req.getSession(false);
					SessionInCookieFilter.update(session, req, resp);
					bSuccess = true;
				} else {
					sMessage = "Unable to assign CAC card. No CAC found";
					if (this.userRecord.getLastError() != null)
						sMessage += "\n" + this.userRecord.getLastError().getMessage();
				}
				JsonAble.addSession(json, this);
			}
		} catch (Exception oError) {
			System.out.println("UserSession.accountAssignCac() Error: " + oError.getMessage());
			sMessage = "Unable to assign CAC card due to an internal error.";
		}

		json.put(JsonAble.TAG_STATUS, (bSuccess ? JsonAble.STATUS_SUCCESS : JsonAble.STATUS_FAIL)); 
		json.put(JsonAble.TAG_MESSAGE, sMessage);
		json.put(JsonAble.TAG_DATA, (String)null);
		PageApi.send(resp, JsonAble.JSON_RESPONSE_FORMAT, json.toString());
	}
	
	/* Replaced following with above for CJ-659
	public void accountAssignCacOld(HttpServletRequest req, HttpServletResponse resp) {
		if (this.userRecord == null)
			return;
		
		String rawCert = req.getHeader(CAC_HEADER);
		
		X509Certificate cert = CommonUtils.getX509Cert(rawCert);

		String cacHash = CommonUtils.getCacHashFromCert(cert);
		
		if(cacHash != null && this.userRecord.assignCac(cacHash)){
			this.sessionMessage = "CAC registered successfully!";
			HttpSession session = req.getSession(false);
			SessionInCookieFilter.update(session, resp);
			PageApi.sendRedirect(null, req, resp, ClsPages.afterChangePassword(null, this));
		} else {
			this.sessionMessage = "Unable to save. No CAC found";
			if (this.userRecord.getLastError() != null)
				this.sessionMessage += "\n" + this.userRecord.getLastError().getMessage();
			PageApi.sendRedirect(null, req, resp, ClsPages.PAGE_ACCOUNT_SETTINGS);
		}
	}
	*/
	private void validateData(HttpServletRequest req) throws InvalidDataException
	{
		String email = req.getParameter("email");
		String firstName = req.getParameter("first_name");
		String lastName = req.getParameter("last_name");
		String password = req.getParameter("password"); // p
		String dept_id = req.getParameter("dept_id"); // 
		String orgId = req.getParameter("org_id"); // agency
		if (CommonUtils.isEmpty(email))
		{
			throw new InvalidDataException("Email Address is empty");
		}
		if (!CommonUtils.isValidEmmail(email))
		{
			throw new InvalidDataException("Email address is not valid");
		}
	    if (CommonUtils.isEmpty(password))
	    {
	    	throw new InvalidDataException("Password is empty");
	    }
	    if (CommonUtils.isEmpty(firstName))
	    {
	    	throw new InvalidDataException("First name is empty");
	    }
	    if (CommonUtils.isEmpty(lastName))
	    {
	    	throw new InvalidDataException("Last name is empty");
	    }
	    if (CommonUtils.isEmpty(orgId))
		{
	    	throw new InvalidDataException("Agency is empty");
		}
	    
	}
	
	
	private void register(HttpServletRequest req, HttpServletResponse resp) {
		Locale.setDefault(Locale.ENGLISH);
		
		String email = req.getParameter("email");
		String firstName = req.getParameter("first_name");
		String lastName = req.getParameter("last_name");
		String password = req.getParameter("password"); // p
		String dept_id = req.getParameter("dept_id"); // 
		String org_id = req.getParameter("org_id"); // agency
		String remoteIP = PageApi.getRemoteIp(req);
		String errorMessage = null;
		Connection conn = null;
		try
		{
		    conn = ClsConfig.getDbConnection();		
			if (conn == null)
			{
				throw new UnrecoverableException("The JDBC Connection can not be established");
			}
			validateData(req);
			this.userRecord = registerNewUser(email, firstName, lastName, password, org_id, remoteIP, conn); 
		}
		catch (InvalidDataException e)
		{
			logger.debug("Invalid a new user data.", e);
			errorMessage = e.getMessage();
		}
		catch (UnrecoverableException e)
		{
			logger.error("Problem to register a new user.", e);
			errorMessage = e.getMessage();
		}
		
		finally 
		{
			SqlUtil.closeInstance(conn);
		}	
		

		if (errorMessage == null) {
			this.sessionMessage = "Registered successully.";
			this.messagePage = ClsPages.PAGE_ACCOUNT_DASHBOARD;
			PageApi.sendRedirect(null, req, resp, ClsPages.PAGE_ACCOUNT_DASHBOARD);
			/*
			this.messagePage = ClsPages.PAGE_ACCOUNT_DASHBOARD;
			String html = "<html><head></head><body>"
					+ "<script>window.location.replace('" + ClsPages.PAGE_ACCOUNT_DASHBOARD + "');</script>"
					+ "</body>";
			HttpSession session = req.getSession(false);
			SessionInCookieFilter.update(session, resp);
			PageApi.send(resp, null, null, html);
			*/
		} else {
			this.sessionMessage = null;			
			String sParam = "?message=" + URLEncodeUtils.encodeURIComponent(errorMessage)
				+ "&email=" + URLEncodeUtils.encodeURIComponent(email)
				+ "&first_name=" + URLEncodeUtils.encodeURIComponent(firstName)
				+ "&last_name=" + URLEncodeUtils.encodeURIComponent(lastName)
				+ "&dept_id=" + dept_id 
				+ "&org_id=" + org_id;
			logger.debug("sParam" + sParam);				
			
			PageApi.sendRedirect(req, resp, ClsPages.PAGE_REGISTER + sParam);
			
		}
		/*
		String jsonResult;
		if (errorMessage == null) {
			this.sessionMessage = "Registered successully.";
			this.messagePage = ClsPages.PAGE_ACCOUNT_DASHBOARD;
			jsonResult = JsonAble.jsonResult(JsonAble.STATUS_SUCCESS, null, ClsPages.afterLogon(req, this));
		} else {
			jsonResult = JsonAble.jsonResult(JsonAble.STATUS_FAIL, null, errorMessage);
		}
		PageApi.send(resp, JsonAble.JSON_RESPONSE_FORMAT, jsonResult);
		*/
	}

	private UserRecord registerNewUser(String email, String firstName, String lastName, String password, String org_id,
			String remoteIP, Connection conn) throws UnrecoverableException, InvalidDataException
	{
		
		try
		{
			UserRecord userRecord = UserRecord.getRecord(conn, null, email, null);
			if (userRecord != null)
			{
				throw new InvalidDataException("Already registered");
			}
			userRecord = UserRecord.register(email, password, firstName, lastName, org_id, conn, remoteIP);
			if (userRecord == null)
			{
				throw new UnrecoverableException("Unable to register due to unknown server error");				
			}
			return userRecord;

		} 
		catch (InvalidDataException e)
		{
			throw e;
		}
		catch (Exception oError)
		{
			logger.error("Problem to register a new user.", oError);
			oError.printStackTrace();
			
			throw new UnrecoverableException("Unable to register due to server internal error");
		}
	}
		
	private void resetPassword(HttpServletRequest req, HttpServletResponse resp) {
		String user_id = req.getParameter("Id"); //
		//String email = req.getParameter("email");
		String password = req.getParameter("password"); //
		String token = req.getParameter("token");
		boolean bUpdated = false;
		boolean bUserRecord = false;
 
		String errorMessage = null;
		if (CommonUtils.isEmpty(user_id))
			errorMessage = "User Id is empty";
		else if (CommonUtils.isEmpty(password))
			errorMessage = "Password is empty";
		else {
			Connection conn = null;
			try {
				conn = ClsConfig.getDbConnection();
				bUserRecord = UserRecord.checkRecord(conn, token);
				if (!bUserRecord) {
					errorMessage = "Unable to reset password due to unknown user or invalid token";
				} else {
					bUpdated = UserRecord.resetPassword(user_id, password, conn, PageApi.getRemoteIp(req));
					if (bUpdated == false) {
						errorMessage = "Unable to reset password due to unknown server error";
					}
				}
			} catch (Exception oError) {
				oError.printStackTrace();
				errorMessage = "Unable to reset password due to server internal error";
			} finally {
				SqlUtil.closeInstance(conn);
			}
		}
		
		if (errorMessage == null) {
			this.sessionMessage = "Reset password successully.";
			this.messagePage = ClsPages.PAGE_RESETPASSWORD;
			PageApi.sendRedirect(null, req, resp, ClsPages.PAGE_ACCOUNT_DASHBOARD);
		} else {
			this.sessionMessage = null;
			String sParam = "?message=" + errorMessage;
			PageApi.sendRedirect(req, resp, ClsPages.PAGE_RESETPASSWORD + sParam);
		}
	}
	
	public boolean login(HttpServletRequest req, String currentLogonIp) {
		String email = req.getParameter("id"); 
		String password = req.getParameter("p");
		
		if (this.userRecord != null) {
			this.userRecord = null;
			this.loginTried = 0;
		}
		this.loginTried++;
		this.lastLoginTried = CommonUtils.getNow();

		this.userRecord = UserTable.login(email, password, currentLogonIp);
		if (this.userRecord != null) {
			this.sessionMessage = "Logon successful!";
			return true;
		}
		return false;
	}

	public String jsonLogin(HttpServletRequest req, String currentLogonIp) {
		this.login(req, currentLogonIp);
		return this.getJson(null);
	}
	
	@Override
	protected void populateJson(ObjectNode json, ObjectMapper mapper) {
		boolean result = (this.userRecord != null) || (this.oauthInterviewRecord != null);
		json.put(JsonAble.TAG_STATUS, (result ? JsonAble.STATUS_SUCCESS : JsonAble.STATUS_FAIL));
		if (result) {
			if (this.userRecord != null)
				json.put(JsonAble.TAG_DATA, this.userRecord.toJson(mapper));
			else {
				ObjectNode recordNode = json.objectNode();
				this.oauthInterviewRecord.fillJson(recordNode, false);
				json.put("token", recordNode);
			}
		}
	}
	
	public Long getLastAccessedTime() { // CJ-573
		return this.lLastAccessedTime;
		/*
		if (this.session != null)
			return this.session.getLastAccessedTime();
		return null;
		*/
	}
	
	public Integer getMaxInactiveInterval() { // CJ-573
		return this.iMaxInactiveInterval;
		/*
		if (this.session != null)
			return this.session.getMaxInactiveInterval();
		return null;
		*/
	}
	
	public Date getExpiresAt() { // CJ-573
		if (this.oauthInterviewRecord != null) {
			return this.oauthInterviewRecord.getExpiresAt();
		}
		if (this.iMaxInactiveInterval != null) { // if (this.session != null) {
			Calendar calendar = Calendar.getInstance();
			// Dataload sync problem - Expire time was often in the past.
			calendar.setTimeInMillis(calendar.getTimeInMillis()); //(this.session.getLastAccessedTime());			
			// Dataload sync problem - Session timeout always occurred as soon as you logged in. 
			calendar.add(Calendar.SECOND, this.iMaxInactiveInterval);
			// calendar.add(Calendar.SECOND, this.session.getMaxInactiveInterval() + 30);
			return calendar.getTime();
		}
		return null;
	}
	
	public boolean validateToken(String token) {
		if (CommonUtils.isEmpty(token)) {
			this.oauthInterviewRecord = null;
			return false;
		}
		boolean result = false;
		Connection conn = null;
		try {
			if (this.oauthInterviewRecord != null) {
				if (!this.oauthInterviewRecord.getInterviewToken().equals(token))
					this.oauthInterviewRecord = null;
			}
			if (this.oauthInterviewRecord == null) {
				conn = ClsConfig.getDbConnection();
				this.oauthInterviewRecord = OauthInterviewRecord.getRecord(conn, token);
			}
			if (this.oauthInterviewRecord != null) {
				if (conn == null)
					conn = ClsConfig.getDbConnection();
				if (this.oauthInterviewRecord.isExpired(conn, true)) {
					this.oauthInterviewRecord = null;
				} else {
					this.oauthInterviewRecord.updateLaunchedAt(conn);
					String orgName = OauthApplicationTable.getOrgName(conn, this.oauthInterviewRecord.getApplicationId().intValue());
					this.oauthInterviewRecord.setOrgName(orgName);
					result = true;
				}
			}
			if (result == false) {
				System.out.println("Invalid token: " + token);
			}
		} catch (Exception oError) {
			oError.printStackTrace();
		} finally {
			SqlUtil.closeInstance(conn);
		}
		return result;
	}
	
	public String getJson(HttpServletRequest req) {
		ObjectMapper mapper = new ObjectMapper();
		ObjectNode json = mapper.createObjectNode();
		
		String redirect = null;

		boolean result = (this.userRecord != null);

		String token = req.getParameter(ClsPages.PARAM_TOKEN);
		if (CommonUtils.isEmpty(token)) {
			this.oauthInterviewRecord = null;
			if (result && (req != null)) {
				String path = req.getParameter("pg");
				if (!ClsPages.canAcessPage(path, this)) {
					redirect = ClsPages.afterLogon(req, this);
				}
			}
		} else {
			result = this.validateToken(token);
		}
		
		json.put(JsonAble.TAG_STATUS, (result ? JsonAble.STATUS_SUCCESS : JsonAble.STATUS_FAIL));
		if (result) {
			ObjectNode oData;
			if (this.oauthInterviewRecord != null) {
				oData = mapper.createObjectNode();
				this.oauthInterviewRecord.fillJson(oData, false);
				json.put("token", oData);
			} else {
				oData = userRecord.toJson(mapper);
				json.put(JsonAble.TAG_DATA, (redirect == null) ? oData : null);
			}
		}
		json.put(JsonAble.TAG_MESSAGE, (redirect == null) ? "" : redirect); // this.sessionMessage
		JsonAble.addSession(json, this); // CJ-573
		//this.sessionMessage = null;
		return json.toString();
	}
	
	private void continueSession(HttpServletRequest req, HttpServletResponse resp, HttpSession session) { // CJ-634
		ObjectMapper mapper = new ObjectMapper();
		ObjectNode json = mapper.createObjectNode();

		String sStatus = JsonAble.STATUS_SUCCESS;
		
		String token = req.getParameter(ClsPages.PARAM_TOKEN);
		if (CommonUtils.isNotEmpty(token)) {
			Connection conn = null;
			try {
				conn = ClsConfig.getDbConnection();
				this.oauthInterviewRecord = OauthInterviewRecord.getRecord(conn, token);
				if (this.oauthInterviewRecord != null) {
					this.oauthInterviewRecord.extendExpiration(conn);
				} else {
					System.out.println("Invalid token: " + token);
					sStatus = JsonAble.STATUS_FAIL;
				}
			} catch (Exception oError) {
				oError.printStackTrace();
				sStatus = JsonAble.STATUS_FAIL;
			} finally {
				SqlUtil.closeInstance(conn);
			}
		}
		else if (this.userRecord == null)
			sStatus = JsonAble.STATUS_FAIL;

		json.put(JsonAble.TAG_STATUS, sStatus);
		JsonAble.addSession(json, this);
		SessionInCookieFilter.update(session, req, resp); // this.session
		PageApi.send(resp, JsonAble.JSON_RESPONSE_FORMAT, json.toString());
	}
	
	public void logout() {
		this.userRecord = null;
		this.loginTried = 0;
		this.lastLoginTried = null;
	}
	
	public void submitCacLogin(HttpServletRequest req, HttpServletResponse resp) {
		
		String rawCert = req.getHeader(CAC_HEADER);
		
		X509Certificate cert = CommonUtils.getX509Cert(rawCert);
		
		//if (cert == null)
		//	System.out.println("Failed request for submitCacLogin()::getX509Cert().");

		String cacHash = CommonUtils.getCacHashFromCert(cert);
		
		//if (cacHash == null)
		//	System.out.println("Failed request for submitCacLogin()::getCacHashFromCert().");
		
		if (this.userRecord != null) {
			this.userRecord = null;
			this.loginTried = 0;
		}
		this.loginTried++;
		this.lastLoginTried = CommonUtils.getNow();
		String currentLogonIp = PageApi.getRemoteIp(req);
		if(cacHash != null)
			this.userRecord = UserTable.cacLogin(cacHash, currentLogonIp);
		
		//if (this.userRecord == null)
		//	System.out.println("Failed request for submitCacLogin()::cacLogin().");
		
		if (this.userRecord != null) {
			this.messagePage = ClsPages.PAGE_ACCOUNT_DASHBOARD;
			this.sessionMessage = "Logon successful!";
			PageApi.sendRedirect(null, req, resp, ClsPages.PAGE_ACCOUNT_DASHBOARD);
		} else {
			this.sessionMessage = null;
			String sParam = "?message=Unable to login. No CAC detected.";
			PageApi.sendRedirect(req, resp, ClsPages.PAGE_LOGIN + sParam);
		}
		
	}
	
	public void submitLogin(HttpServletRequest req, HttpServletResponse resp, HttpSession session) {
		String email = req.getParameter("email"); 
		String password = req.getParameter("password");
		if (this.userRecord != null) {
			this.userRecord = null;
			this.loginTried = 0;
		}
		
		this.loginTried++;
		this.lastLoginTried = CommonUtils.getNow();
		String currentLogonIp = PageApi.getRemoteIp(req);
		this.userRecord = UserTable.login(email, password, currentLogonIp);
		if (this.userRecord != null) {
			this.messagePage = ClsPages.PAGE_ACCOUNT_DASHBOARD;
			this.sessionMessage = "Logon successful!";
			
			PageApi.sendRedirect(session, req, resp, ClsPages.PAGE_ACCOUNT_DASHBOARD); // this.session
			/*
			String html = "<html><head></head><body>"
					+ "<script>window.location.replace('" + ClsPages.PAGE_ACCOUNT_DASHBOARD + "');</script>"
					+ "</body>";
			HttpSession session = req.getSession(false);
			SessionInCookieFilter.update(session, resp);
			PageApi.send(resp, null, null, html);
			*/
		} else {
			this.sessionMessage = null;
			String sParam = "?message=Unable to login&email=" + email;
			
			// CJ-1373, Add 'Email account not registered'
			if (!UserTable.validateEmail(email))
				sParam = "?message=Unable to login. Email account not registered.&email=" + email;
			
			PageApi.sendRedirect(session, req, resp, ClsPages.PAGE_LOGIN + sParam); // this.session
		}
	}
	
	public void serverLogin(HttpServletRequest req) {
		String email = req.getParameter("email"); 
		String password = req.getParameter("password");
		if (this.userRecord != null) {
			this.userRecord = null;
			this.loginTried = 0;
		}
		
		this.loginTried++;
		this.lastLoginTried = CommonUtils.getNow();
		String currentLogonIp = PageApi.getRemoteIp(req);
		this.userRecord = UserTable.login(email, password, currentLogonIp);
	}

	// ------------------------------------------------------------------------------
	public void processRequest(String sDo, HttpServletRequest req, HttpServletResponse resp, HttpSession session) {
		switch (sDo) {
		case DO_SESSION_GET_LOGIN:
			PageApi.send(resp, JsonAble.JSON_RESPONSE_FORMAT, this.getJson(req));
			return;
		case DO_SESSION_LOGOUT:
			this.logout();
			PageApi.send(resp, JsonAble.JSON_RESPONSE_FORMAT, 
					JsonAble.jsonResult(JsonAble.STATUS_SUCCESS, null, ClsPages.afterLogout(req, this), this));
			return;
		case DO_SESSION_LOGIN:
			String currentLogonIp = PageApi.getRemoteIp(req);
			boolean bSuccess = this.login(req, currentLogonIp);
			String response = JsonAble.jsonResult((bSuccess ? JsonAble.STATUS_SUCCESS : JsonAble.STATUS_FAIL),
					null, (bSuccess ? ClsPages.afterLogon(req, this) : null), this);
			PageApi.send(resp, JsonAble.JSON_RESPONSE_FORMAT, response);
			return;
		case DO_SESSION_REGISTER:
			this.register(req, resp);
			return;
		case DO_SESSION_RESET_PASSWORD:
			this.resetPassword(req, resp);
			return;	
		case DO_SESSION_CONTINUE_SESSION: // CJ-634
			this.continueSession(req, resp, session);
			return;
		}
			
		if (this.isVerified() == false) {
			this.setMessage("Invalid access");
			PageApi.sendRedirect(req, resp, "");
			return;
		}
		
		switch (sDo) {
		case DO_SESSION_SETTINGS_UPDATE:
			this.accountSettingUpdate(req, resp);
			return;
	
		case DO_SESSION_CHANGE_PASSWORD:
			this.accountChangePasssword(req, resp);
			return;
		
			// CJ-700; change pwd, no redirect.
		case DO_SESSION_CHANGE_PASSWORD2:
			this.accountChangePasssword_v2(req, resp);
			return;	
			
		}
	}
	
	public void initialize(HttpServletRequest req, HttpServletResponse resp) {
		Exception error = null;
		Connection conn = null;
		try {
			ClsConfig clsConfig = ClsConfig.getInstance();
			/*
			if (clsConfig.getServerPath() == null) {
				String sPg = req.getParameter("pg");
				int iPos = sPg.lastIndexOf("index.html");
				if (iPos >= 0)
					sPg = sPg.substring(0, iPos);
				clsConfig.setServerPath(sPg);
			}
			*/
			conn = clsConfig.getConnection();
			ClsDocument.checkCache(conn);
		} catch (Exception oError) {
			error = oError;
			oError.printStackTrace();
		} finally {
			SqlUtil.closeInstance(conn);
		}
		String response = null;
		if (error == null) {
			response = JsonAble.jsonResult(JsonAble.STATUS_SUCCESS, null, "ready");
		} else {
			response = JsonAble.jsonResult(JsonAble.STATUS_FAIL, null, "not ready");
		}
		PageApi.send(resp, JsonAble.JSON_RESPONSE_FORMAT, response);
	}
	
}
