package gov.dod.cls.bean;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import gov.dod.cls.PageApi;
import gov.dod.cls.api.utils.ApiResponseError;
import gov.dod.cls.bean.session.UserSession;
import gov.dod.cls.config.ClsConfig;
import gov.dod.cls.db.ClauseVersionRecord;
import gov.dod.cls.db.DocumentRecord;
import gov.dod.cls.db.OauthAccessRecord;
import gov.dod.cls.db.OauthApplicationRecord;
import gov.dod.cls.model.JsonAble;
import gov.dod.cls.utils.CommonUtils;
import gov.dod.cls.utils.sql.SqlUtil;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.node.ObjectNode;

// chrome-extension://hgmloofddffdnphfgcellkdfbfbjeloo/RestClient.html#RequestPlace:default
// Sample Authorization: 4d59e050-9156-4e1d-bf38-21c4e39dc6fc de0c0bc2-13c6-4c21-8f4f-359da67cda46

public class Oauth {

	// API =============================================================================================
	public static final String VERSION_1 = "v1";
	public static final String VERSION_2 = "v2";
	public static final String TOKEN_TYPE = "bearer";
	public static final Integer EXPIRES_IN = 7200; // Oauth.EXPIRES_IN
	
	public static Response checkParameters(String version, Object[] params, ApiResponseError apiResponseError) {
		if (CommonUtils.isEmpty(version)) {
			return apiResponseError.sendBadRequest("Missing Required Version Parameter", null);
		}
		if ((!Oauth.VERSION_2.equals(version)) && (!Oauth.VERSION_1.equals(version))) {
			return apiResponseError.sendBadRequest("Invalid version", null);
		}
		if (params != null) {
			for (Object param : params) {
				if ((param == null) || param.toString().isEmpty())
					return apiResponseError.sendBadRequest("Missing Required Parameter(s)", null);
			}
		}
		return null;
	}

	// Reference =============================================================================================
	public static final String PREFIX_DO_OAUTH = "oauth-";

	public static final String DO_NEW_APP_CODES = PREFIX_DO_OAUTH + "new_app_codes";
	//public static final String DO_APP_POST = PREFIX_DO_OAUTH + "app_post";
	
	
	public static void processRequest(String sDo, UserSession oUserSession, 
			HttpServletRequest req, HttpServletResponse resp) {
		switch (sDo) {
		case DO_NEW_APP_CODES:
			String response = JsonAble.jsonResult(JsonAble.STATUS_SUCCESS, Oauth.genNewCodes(), null); 
			PageApi.send(resp, JsonAble.JSON_RESPONSE_FORMAT, response);
			return;
		/*
		case DO_APP_POST:
			PageApi.send(resp, JsonAble.JSON_RESPONSE_FORMAT, Oauth.postApp(oUserSession, req, resp));
			return;
		*/
		}
	}
	
	private static ObjectNode genNewCodes() {
		ObjectMapper mapper = new ObjectMapper();
		ObjectNode result = mapper.createObjectNode();
		result.put(OauthApplicationRecord.FIELD_APPLICATION_UID, Oauth.genCode());
		result.put(OauthApplicationRecord.FIELD_APPLICATION_SECRET, Oauth.genCode());
		return result;
	}
	
	public static String genCode() {
		UUID idOne = UUID.randomUUID();
		return Oauth.asciiToHex(String.valueOf(idOne));
	}
	
	public static String asciiToHex(String asciiValue)
	{
	    char[] chars = asciiValue.toCharArray();
	    StringBuffer hex = new StringBuffer();
	    for (int i = 0; i < chars.length; i++)
	    {
	    	char ch = chars[i];
	    	if (ch != '-')
	    		hex.append(Integer.toHexString((int) ch));
	    }
	    return hex.toString();
	}
	
	// Instance ======================================================================================
	private String accessToken = null;
	private OauthAccessRecord accessRecord = null;
	private OauthApplicationRecord applicatonRecord = null;
	private String lastError = null;
	
	public Oauth(HttpHeaders httpHeaders) {
		super();
		this.checkAuthorization(httpHeaders);
	}
	
	private void clear() {
		this.accessRecord = null;
		this.accessToken = null;
		this.lastError = null;
	}
	
	public boolean isAuthorized(Connection conn) throws SQLException {
		if (CommonUtils.isEmpty(this.accessToken))
			return false;
		
		boolean bCreateConnection = (conn == null);
		PreparedStatement ps1 = null;
		ResultSet rs1 = null;
		try {
			if (bCreateConnection)
				conn = ClsConfig.getDbConnection();
			String sSql = OauthAccessRecord.SQL_SELECT
					+ " WHERE " + OauthAccessRecord.FIELD_ACCESS_TOKEN + " = ?";
			ps1 = conn.prepareStatement(sSql);
			ps1.setString(1, this.accessToken);
			rs1 = ps1.executeQuery();
			if (rs1.next()) {
				OauthAccessRecord oRecord = new OauthAccessRecord();
				oRecord.read(rs1);
				if (oRecord.isExpired(conn, true)) {
					this.lastError = "Expired";
				} else {
					this.accessRecord = oRecord;
					this.applicatonRecord = OauthApplicationRecord.getRecord(oRecord.getApplicationId(), null, null, conn);
				}
			} else {
				this.lastError = "Invalid Token";
			}
		}
		/*
		catch (Exception oError) {
			oError.printStackTrace();
			this.lastError = "Application Error: " + oError.getMessage();
		}
		*/
		finally {
			SqlUtil.closeInstance(rs1, ps1);
			if (bCreateConnection)
				SqlUtil.closeInstance(conn);
		}
		return (this.accessRecord != null);
	}

	public boolean isNotAuthorized(Connection conn) throws SQLException {
		return (!this.isAuthorized(conn));
	}
	
	public int getApplicatonId() {
		if (this.accessRecord == null)
			return -1;
		return this.accessRecord.getApplicationId().intValue();
	}

	public int getOauthAccessId() {
		if (this.accessRecord == null)
			return -1;
		return this.accessRecord.getOauthAccessId().intValue();
	}

	private boolean checkAuthorization(HttpHeaders httpHeaders) {
		this.clear();

		if (httpHeaders == null) {
			this.lastError = "No HttpHeaders";
			return false;
		}
		
		List<String> authorizations = httpHeaders.getRequestHeader("Authorization");
		if ((authorizations == null) || (authorizations.size() < 1)) {
			this.lastError = "No Authorization Header";
			return false;
		}
		
		String[] authParams = authorizations.get(0).split(" ");
		if (authParams.length != 2) {
			this.lastError = "Authorization header has wrong parameters";
			return false;
		}
		
		if (!Oauth.TOKEN_TYPE.equalsIgnoreCase(authParams[0])) {
			this.lastError = "Invalid token type";
			return false;
		}
		this.accessToken = authParams[1];
		
		return true;
	}
	
	
	private ObjectNode applicationNode = null;
	public void populateApplicationJsonForApi(boolean bForVer1, ObjectNode json) {
		if ((this.applicationNode == null) && (this.applicatonRecord != null)) {
			this.applicationNode = json.objectNode();
			this.applicatonRecord.populateJsonForApi(bForVer1, this.applicationNode);
		}
		json.put("application", this.applicationNode);
	}

	public boolean isClsVersionSupported(int clsVersionId, Connection conn) throws SQLException { // CJ-582
		if (this.applicatonRecord == null)
			return false;
		PreparedStatement ps1 = null;
		ResultSet rs1 = null;
		String sSql = "SELECT A." + ClauseVersionRecord.FIELD_CLAUSE_VERSION_ID + "\n"
				+ "FROM " + ClauseVersionRecord.TABLE_CLAUSE_VERSIONS + " A\n"
				+ "WHERE A." + ClauseVersionRecord.FIELD_CLAUSE_VERSION_ID + " = ?\n"
				+ "AND (A." + ClauseVersionRecord.FIELD_IS_ACTIVE + " = 1\n"
				+ 	"OR EXISTS (\n"
				+ 		"SELECT B." + DocumentRecord.FIELD_CLAUSE_VERSION_ID + "\n"
				+ 		"FROM " + DocumentRecord.TABLE_DOCUMENTS + " B\n"
				+ 		"WHERE B." + DocumentRecord.FIELD_APPLICATION_ID + " = ?\n"
				+ 		"AND B." + DocumentRecord.FIELD_CLAUSE_VERSION_ID + " = A." + ClauseVersionRecord.FIELD_CLAUSE_VERSION_ID + "\n"
				+ 	")\n"
				+ ")";
		try {
			ps1 = conn.prepareStatement(sSql);
			ps1.setInt(1, clsVersionId);
			ps1.setInt(2, this.applicatonRecord.getApplicationId());
			rs1 = ps1.executeQuery();
			return (rs1.next());
		} finally {
			SqlUtil.closeInstance(rs1, ps1);
		}
	}
	
	public ArrayList<Integer> getSupportedClsVersions(Connection conn) throws SQLException { // CJ-582
		if (this.applicatonRecord == null)
			return null;
		ArrayList<Integer> result = new ArrayList<Integer>();
		PreparedStatement ps1 = null;
		ResultSet rs1 = null;
		String sSql = "SELECT A." + ClauseVersionRecord.FIELD_CLAUSE_VERSION_ID + "\n"
				+ "FROM " + ClauseVersionRecord.TABLE_CLAUSE_VERSIONS + " A\n"
				+ "WHERE A." + ClauseVersionRecord.FIELD_IS_ACTIVE + " = 1\n"
				+ "OR EXISTS (\n"
				+ 	"SELECT B." + DocumentRecord.FIELD_CLAUSE_VERSION_ID + "\n"
				+ 	"FROM " + DocumentRecord.TABLE_DOCUMENTS + " B\n"
				+ 	"WHERE B." + DocumentRecord.FIELD_APPLICATION_ID + " = ?\n"
				+ 	"AND B." + DocumentRecord.FIELD_CLAUSE_VERSION_ID + " = A." + ClauseVersionRecord.FIELD_CLAUSE_VERSION_ID + "\n"
				+ ")\n"
				+ "ORDER BY 1";
		try {
			ps1 = conn.prepareStatement(sSql);
			ps1.setInt(1, this.applicatonRecord.getApplicationId());
			rs1 = ps1.executeQuery();
			while (rs1.next()) {
				result.add(rs1.getInt(1));
			}
		} finally {
			SqlUtil.closeInstance(rs1, ps1);
		}
		return result;
	}
	
	public String getAccessToken() { return this.accessToken; }
	public OauthAccessRecord getAccessRecord() { return this.accessRecord; }
	public String getLastError() { return this.lastError; }
	
}
