package gov.dod.cls;

import gov.dod.cls.bean.session.UserSession;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.jasypt.commons.CommonUtils;

@WebServlet("/cac")
public class CacApi extends HttpServlet {

	private static final long serialVersionUID = 3977693003706341508L;
	public final static String PARAM_DO = "do";
	
	@Override
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
	        throws ServletException, IOException {
		this.doProcess(false, req, resp);
	}

	// ----------------------------------------------------------------------------
	@Override
	public void doPost(HttpServletRequest req, HttpServletResponse resp)
	        throws ServletException, IOException {
		this.doProcess(true, req, resp);
	}

	public void doProcess(boolean isPost, HttpServletRequest req, HttpServletResponse resp)
	        throws ServletException, IOException 
	{

		String sDo = req.getParameter(PARAM_DO);
		if (CommonUtils.isEmpty(sDo))
			return;
		
		HttpSession session = req.getSession(true);
		UserSession oUserSession = PageApi.getUserSession(session);

		try {
			if ("login".equals(sDo)) {
				oUserSession.submitCacLogin(req, resp);
				return;
			}
			
			if ("assign".equals(sDo)) {
				oUserSession.accountAssignCac(req, resp);
				return;
			}
			
			
		} finally {
			//SessionInCookieFilter.update(session, (HttpServletResponse) resp);
			//session.setAttribute(UserSession.class.getName(), null);
		}
	}
	
}
