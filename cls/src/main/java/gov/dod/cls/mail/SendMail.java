package gov.dod.cls.mail;

import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import gov.dod.cls.config.ContextHelper;

public class SendMail {

	public synchronized static SendMail getInstance() {
		SendMail sendMail = ContextHelper.getInstance().getSpringBean(SendMail.class, "sendMail");
		return sendMail;
	}
	
	// http://www.mkyong.com/java/javamail-api-sending-email-via-gmail-smtp-example/
    private String mailFrom = ""; // Sender's email ID needs to be mentioned
    private String mailHost = ""; // "hgs-worldwide.com"; //
    private boolean mailSSL = false;
    private String mailPort = null;
    private String mailUser = null;
    private String mailPassword = null;
    
    public boolean send(String[] toRecipients, String[] ccRecipients, String[] bccRecipients, 
    		String subject, String content, boolean isHtmlContent ) {
    	boolean result = false;
    	Properties properties = System.getProperties();

        properties.setProperty("mail.smtp.host", this.mailHost); // Setup mail server
        if ((this.mailPort != null) && (!this.mailPort.isEmpty()))
        	if (this.mailSSL)
            	properties.setProperty("mail.smtp.socketFactory.port", this.mailPort);
        	else
        		properties.setProperty("mail.smtp.port", this.mailPort);

        if (this.mailSSL)
        	properties.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        else
        	properties.put("mail.smtp.starttls.enable", "true");

        javax.mail.Authenticator authenticator = null;
        
        if ((this.mailUser != null) && (!this.mailUser.isEmpty())) {
        	properties.put("mail.smtp.auth", "true");
        	properties.setProperty("mail.user", this.mailUser);
	        if ((this.mailPassword != null) && (!this.mailPassword.isEmpty()))
	        	properties.setProperty("mail.password", this.mailPassword);
	        authenticator = new javax.mail.Authenticator() {
				protected PasswordAuthentication getPasswordAuthentication() {
					return new PasswordAuthentication(mailUser, mailPassword);
				}
			};
        }
        Session session = Session.getDefaultInstance(properties, authenticator); // Get the default Session object.

        try {
        	MimeMessage message = new MimeMessage(session); // Create a default MimeMessage object.
        	message.setFrom(new InternetAddress(this.mailFrom)); // Set From: header field of the header.

        	// Set To: header field of the header.
        	for (String recipient : toRecipients) {
        		message.addRecipient(Message.RecipientType.TO, new InternetAddress(recipient));
        	}
        	if (ccRecipients != null)
        		for (String recipient : ccRecipients) {
        			boolean bNotFound = true;
        			for (int index = 0; index < toRecipients.length; index++) {
        				if (recipient.equalsIgnoreCase(toRecipients[index])) {
        					bNotFound = false;
        					break;
        				}
        			}
        			if (bNotFound)
        				message.addRecipient(Message.RecipientType.CC, new InternetAddress(recipient));
            	}
        	if (bccRecipients != null)
        		for (String recipient : bccRecipients) {
        			boolean bNotFound = true;
        			for (int index = 0; index < toRecipients.length; index++) {
        				if (recipient.equalsIgnoreCase(toRecipients[index])) {
        					bNotFound = false;
        					break;
        				}
        			}
        			if (bNotFound)
        				message.addRecipient(Message.RecipientType.BCC, new InternetAddress(recipient));
            	}
        	
        	message.setSubject(subject); // Set Subject: header field
        	if (isHtmlContent)
        		message.setText(content, "utf-8", "html");
        	else
        		message.setText(content);

        	Transport.send(message); // Send message
        	result = true;
        	System.out.println("Sent message successfully....");
        }
        catch (MessagingException mex) {
        	System.out.println("Sent message failed: " + mex.getMessage());
        	mex.printStackTrace();
        }
        return result;
    }

    public String getMailFrom() {
		return this.mailFrom;
	}
	public void setMailFrom(String from) {
		this.mailFrom = from;
	}
	
	public String getMailHost() {
		return this.mailHost;
	}
	public void setMailHost(String host) {
		this.mailHost = host;
	}
    
	public boolean isMailSSL() {
		return this.mailSSL;
	}
	public void setMailSSL(boolean mailSSL) {
		this.mailSSL = mailSSL;
	}

	public String getMailPort() {
		return this.mailPort;
	}
	public void setMailPort(String port) {
		this.mailPort = port;
	}
    
	public String getMailUser() {
		return this.mailUser;
	}
	public void setMailUser(String user) {
		this.mailUser = user;
	}
    
	public String getMailPassword() {
		return this.mailPassword;
	}
	public void setMailPassword(String password) {
		this.mailPassword = password;
	}
    
	// =======================================================
	public static boolean sendMail(String[] toRecipients, String[] ccRecipients, String[] bccRecipients, 
			String subject, String content, boolean isHtmlContent ) {
		SendMail sendMail = SendMail.getInstance();
		return sendMail.send(toRecipients, ccRecipients, bccRecipients, subject, content, isHtmlContent);
	}
	
	public static boolean sendMail(String toRecipient, String subject, String content, boolean isHtmlContent ) {
		String[] toRecipients = {toRecipient}; 
		return sendMail(toRecipients, null, null, subject, content, isHtmlContent);
	}

	public static boolean sendMail(String toRecipient, String[] bccRecipients, 
			String subject, String content, boolean isHtmlContent ) {
		String[] toRecipients = {toRecipient}; 
		return sendMail(toRecipients, null, bccRecipients, subject, content, isHtmlContent);
	}

	// =======================================================
	public static void main(String[] args) {
		SendMail sendMail = new SendMail();
		sendMail.setMailHost("");
		sendMail.setMailSSL(false);
		sendMail.setMailPort("");
		sendMail.setMailUser("");
		sendMail.setMailPassword("");

		String[] toRecipients = {"ilchan.chang@hickoryground.com"}; 
		sendMail.send(toRecipients, null, null, "Test from CCA", "<h2>Testing</h2> from SendMail class from <strong>cyberopscourse@gmail.com</strong>", true);
	}
}
