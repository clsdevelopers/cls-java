// var _oUserInfo = new UserInfo();

var _token = getUrlParameter('token');

if ((_token == null) || (_token == '')) {
	alert('You can not reset password because of invalid token!');
	// goDashboard();
	goHome();
}

displayUserDetails = function(data) {
	var aRecord = data.data; // jQuery.parseJSON(data.data);
	var oRec = aRecord[0];
	if ((oRec == null) || (oRec == '')) {
		alert('You can not reset password because of invalid token or the user does not exist!');
		goHome();
	} else {
		if (oRec.token_expired) {
			alert('You can not reset password because token was expired!');
			goHome();
		} else {
			$('#user_id').val(oRec.User_Id);
			$('#user_email').val(oRec.email);
			$('#user_first_name').val(oRec.first_name);
			$('#user_last_name').val(oRec.last_name);
			$('#user_token').val(oRec.reset_password_token);
			$('#reset_password_sent_at').val(longToDateTimeStr(oRec.reset_password_sent_at));
		}
	}
}

validateForm = function(form) {
	var oNewPassword = $('#password');
	var sNesPassword = oNewPassword.val();
	var error = detectPasswordError(sNesPassword);
	if (error) {
		oNewPassword.focus();
		displaySessionMessage(error);
		return false;
	}
	var oNewPasswordAgain = $('#password_again');
	var sNewPasswordAgain = oNewPasswordAgain.val();
	if (sNewPasswordAgain != sNesPassword) {
		oNewPasswordAgain.focus();
		displaySessionMessage("'Confirm password' value is not matching");
		return false;
	}
	
	return true;
}

retrieveUserInfo = function() {
	if (!_token)
		return;
	var page = getPagePath();
	var data = {'do': 'user-detail-resetpassword', token: _token};
	var instance = this;
	$.ajax({
		url: getAppPath() + 'page',
		data: data,
		cache: false,
		dataType: 'json',
		success: function(data) {
			if (data) {
				displayUserDetails(data);
			} else {
				alert('Unable to get User data - Token might be invalid.');
				//goDashboard();
			}
		},
		error: function(object, text, error) {
			alert(error);
			//goDashboard();
		}
	});
}

function resetPassword() {
	if (!validateForm())
		return false;

	return true;
}	

displayBottomFooter();
retrieveUserInfo();

$( document ).ready(function() {
	$('#password').focus();
});