var OPR_DO_NOT_INCLUDE = 1;
var OPR_INCLUDE = 2;
var OPR_REPLACE_WITH = 3;

var SHOW_AC_NO_ERROR = false;
var SHOW_AC_ERROR = true;

var GREATER_THAN_OR_EQUAL_TO = 'GREATER THAN OR EQUAL TO ';
var LESS_THAN_OR_EQUAL_TO = 'LESS THAN OR EQUAL TO ';
var GREATER_THAN = 'GREATER THAN ';
var LESS_THAN = 'LESS THAN ';
var EQUAL_TO = 'EQUAL TO ';

if (!String.prototype.startsWith) {
	String.prototype.startsWith = function(searchString, position) {
		position = position || 0;
		return this.lastIndexOf(searchString, position) === position;
	};
}

// ===============================================================================================
AdditionalConditionItem = function() {
	this.rule = null;
	this.question = null;
	this.answer = null;
	this.notAnswer = false;
	
	this.oprType = null;
	this.andClauses = null;
	this.orClauses = null;

	this.oprClauses = null;
	
	this.addAndClause = function addAndClause(oClause) {
		if (this.andClauses == null)
			this.andClauses = new Array();
		this.andClauses.push(oClause);
	}

	this.addAndClauseSet = function addAndClauseSet(aClauses) {
		for (var iItem = 0; iItem < aClauses.length; iItem++) {
			this.addAndClause(aClauses[iItem]);
		}
	}

	this.addOrClause = function addOrClause(oClause) {
		if (this.orClauses == null)
			this.orClauses = new Array();
		this.orClauses.push(oClause);
	}

	this.addOrClauseSet = function addOrClauseSet(aClauses) {
		for (var iItem = 0; iItem < aClauses.length; iItem++) {
			this.addOrClause(aClauses[iItem]);
		}
	}

	this.evaluate = function evaluate(pClause, oFinalCheckSet, oPrescriptionChoices) {
		var okToGo;
		var aSelectedClauses = oPrescriptionChoices.clauses;
		if (this.rule) {
			if (pClause.oEvaluation == null)
				return false;
			var evalRule = this.rule;
			okToGo = pClause.oEvaluation.evalRule(aSelectedClauses, evalRule);
			if ((okToGo == null) || (okToGo == false))
				return false;
		}
		if (this.question) {
			okToGo = this.question.hasValue(this.answer);
			if (okToGo == null)
				return (this.notAnswer ? true : false); // null;
			if (this.notAnswer == true)
				return !okToGo;
			return okToGo;
			/*
			var aAnswerValues = this.question.getAnswerValues();
			if (! aAnswerValues)
				return false;
			okToGo = (aAnswerValues.indexOf(this.answer) >= 0);
			if (this.notAnswer)
				okToGo = !okToGo;
			if (okToGo == false)
				return false;
			*/
		}

		var oClause;
		if (this.andClauses) {
			for (var item = 0; item < this.andClauses.length; item++) {
				oClause = this.andClauses[item];
				if (aSelectedClauses.indexOf(oClause) < 0)
					return false;
				if (oClause.isApplicable(oPrescriptionChoices, false) == false)
					return false;
			}
		}

		okToGo = false;
		if (this.orClauses) {
			for (var item = 0; item < this.orClauses.length; item++) {
				oClause = this.orClauses[item];
				if (aSelectedClauses.indexOf(oClause) >= 0) {
					if (oClause.isApplicable(oPrescriptionChoices, false))
						okToGo = true;
				}
			}
			if (okToGo == false)
				return false;
		}
		if (this.oprClauses) {
			var aApplied = new Array();
			switch (this.oprType) {
				case OPR_DO_NOT_INCLUDE:
					for (var item = 0; item < this.oprClauses.length; item++) {
						oClause = this.oprClauses[item];
						if (oPrescriptionChoices.hasClause(oClause)) {
							//oClause.oprAdtnlCond = OPR_DO_NOT_INCLUDE;	// CJ-180
							oPrescriptionChoices.updateCrossoverClause(oClause, false);
							oFinalCheckSet.addItem(oClause, false);
							oClause.show(false, oPrescriptionChoices);
						}
					}
					break;
				case OPR_INCLUDE:
					for (var item = 0; item < this.oprClauses.length; item++) {
						oClause = this.oprClauses[item];
						if (oPrescriptionChoices.hasClause(oClause) == false) {
							//oClause.oprAdtnlCond = OPR_INCLUDE;	// CJ-180
							oPrescriptionChoices.updateCrossoverClause(oClause, true);
							oFinalCheckSet.addItem(oClause, true);
							oClause.show(true, oPrescriptionChoices);
						}
					}
					break;
				case OPR_REPLACE_WITH:
					for (var item = 0; item < this.oprClauses.length; item++) {
						oClause = this.oprClauses[item];
						if (oPrescriptionChoices.hasClause(oClause) == false) {
							oPrescriptionChoices.updateCrossoverClause(oClause, true);
							oFinalCheckSet.addItem(oClause, true);
							oClause.show(true, oPrescriptionChoices);
						}
					}
					
					break;
			}
		}
	};
	
	this.debugHtml = function debugHtml() {
		var sCondition = '';
		if (this.rule) {
			sCondition = this.rule;
		}
		if (this.question) {
			sCondition += (sCondition ? ' AND ' : '') + '[' + this.question + '] IS: ' + (this.notAnswer ? 'NOT ' : '') + '"' + this.answer + '"';
		}
		var sConditionClauses = '';
		if (this.andClauses) {
			sConditionClauses = '(AND) ';
			for (var item = 0; item < this.andClauses.length; item++) {
				sConditionClauses += (item > 0 ? ', ' : '') + this.andClauses[item].name;
			}
		}
		if (this.orClauses) {
			sConditionClauses += ' (OR) ';
			for (var item = 0; item < this.orClauses.length; item++) {
				sConditionClauses += (item > 0 ? ', ' : '') + this.orClauses[item].name;
			}
		}
		
		var sOperator;
		switch (this.oprType) {
			case OPR_DO_NOT_INCLUDE:
				sOperator = 'DO NOT INCLUDE';
				break;
			case OPR_INCLUDE:
				sOperator = 'INCLUDE';
				break;
			case OPR_REPLACE_WITH:
				sOperator = 'REPLACE WITH';
				break;
			 default:
				sOperator = '[null]';
		}
		
		var sOprClauses = '';
		if (this.oprClauses) {
			for (var item = 0; item < this.oprClauses.length; item++) {
				sOprClauses += (item > 0 ? ', ' : '') + this.oprClauses[item].name;
			}
		}
		var sResult = 'IF ';
		if (sConditionClauses)
			sResult += sConditionClauses + ' ADDED';
		if (sCondition)
			if (sConditionClauses)
				sResult += ' AND ' + sCondition;
			else
				sResult += sCondition;
		sResult += ', ' + sOperator + ' ' + sOprClauses;
		return sResult;
	};

};

// ===============================================================================================
AdditionalConditionEval = function() {
	this.conditions = new Array();
	this.isExpressionValid = false;
	this.clause;
	
	this.parseConditions = function parseConditions(oClauseItem, questionSet, clauseSet) {
		this.clause = oClauseItem;
		var pCondition = oClauseItem.additionalConditions;
		var parseError = '', sError;
		
		var aParts, aClauses, iPos, sOperations;
		var aIf = pCondition.toUpperCase().replace(/\n/g, ' ').split('IF ');
		for (var iIf = 0; iIf < aIf.length; iIf++) {
			var oAdtnlCondItem = null;
			var sExpression = aIf[iIf].trim();
			if (sExpression) {
//				sExpression = sExpression.replace('IS ', '');
				aParts = sExpression.split('ADDED');
				if (aParts.length > 1) {
					oAdtnlCondItem = new AdditionalConditionItem();
					//if ('52.225-3' == oClauseItem.name) // ('252.232-7004' == oClauseItem.name) || ('52.232-36' == oClauseItem.name))
					//	logOnConsole(oClauseItem.name + '[' + iIf + '] "' + sExpression + '"');
					for (var iPart = 0; iPart < aParts.length - 1; iPart++) {
						var sCondition = aParts[iPart].trim();
						if (sCondition) {
							aClauses = sCondition.split(' OR ');
							if (aClauses.length > 1) {
								oAdtnlCondItem.addOrClauseSet(this.resolveClauseSet(aClauses, clauseSet));
							} else {
								iPos = sCondition.indexOf('AND ');
								if (iPos >= 0) {
									if (iPos == 0) {
										oAdtnlCondItem.addAndClause(oClauseItem);
										sOperations = sCondition.substring('AND '.length + iPos).trim();
										iPos = sOperations.indexOf(' IS:');
										if (iPos > 0) {
											logOnConsole(oClauseItem.name + '[' + iIf + '][' + iPart + '] "' + sCondition + '"');
										} else {
											var aOr = sOperations.split(',');
											if (aOr.length > 1) {
												oAdtnlCondItem.addOrClauseSet(this.resolveClauseSet(aOr, clauseSet)); // aOr);
											} else {
												logOnConsole(oClauseItem.name + '[' + iIf + '][' + iPart + '] "' + sCondition + '"');
											}
										}
									} else {
										/*
										var aAnd = sCondition.split(' AND ');
										for (var iAnd = 0; iAnd < aAnd.length; iAnd++) {
											var sItem = aAnd[iAnd];
											if (sItem) {
											}
										}
										*/
										parseError += (parseError ? '<br />\n' : '') + '[' + iIf + '][' + iPart + '] Unknown: "' + sCondition + '"';
										//logOnConsole(oClauseItem.name + '[' + iIf + '][' + iPart + '] "' + sCondition + '"');
									}
								} else {
									iPos = sCondition.indexOf(' IS:');
									if (iPos > 0) {
										//logOnConsole(oClauseItem.name + '[' + iIf + '][' + iPart + '] IS: "' + sCondition + '"');
										parseError += (parseError ? '<br />\n' : '') + oClauseItem.name + '[' + iIf + '][' + iPart + '] IS: "' + sCondition + '"';
									} else {
										var aOr = sCondition.split(',');
										if (aOr.length > 1)
											oAdtnlCondItem.addOrClauseSet(this.resolveClauseSet(aOr, clauseSet)); // aOr);
										else
											oAdtnlCondItem.addAndClauseSet(this.resolveClauseSet(aOr, clauseSet)); // aOr);
									}
								}
							}
						} else {
							oAdtnlCondItem.addAndClause(oClauseItem);
						}
					}
					var sRemainedCondition = null;
					sOperations = aParts[aParts.length - 1].trim();
					iPos = sOperations.indexOf('DO NOT INCLUDE ');
					if (iPos >= 0) {
						sRemainedCondition = sOperations.substring(0, iPos).trim();
						var sCaluses = sOperations.substring('DO NOT INCLUDE '.length + iPos).trim();
						this.addDoNotInclude(sCaluses, questionSet, clauseSet, oAdtnlCondItem);
					} else {
						iPos = sOperations.indexOf('INCLUDE ');
						if (iPos >= 0) {
							sRemainedCondition = sOperations.substring(0, iPos).trim();
							var sCaluses = sOperations.substring('INCLUDE '.length + iPos).trim();
							this.addInclude(sCaluses, clauseSet, oAdtnlCondItem);
						} else {
							iPos = sOperations.indexOf('REPLACE WITH ');
							if (iPos >= 0) {
								sRemainedCondition = sOperations.substring(0, iPos).trim();
								var sCaluses = sOperations.substring('REPLACE WITH '.length + iPos).trim();
								this.addReplaceWidth(sCaluses, questionSet, clauseSet, oAdtnlCondItem);
							} else {
								parseError += (parseError ? '<br />\n' : '') + '[' + iIf + '] ADDED: "' + aParts[aParts.length - 1].trim() + '"';
							}
						}
					}
					if (sRemainedCondition && (sRemainedCondition != ',')) {
						var aQuestion = sRemainedCondition.split(' IS:');
						if (aQuestion.length == 2) {
							if (aQuestion[0].startsWith('AND '))
								aQuestion[0] = aQuestion[0].substring('AND '.length).trim();
							else
								aQuestion[0] = aQuestion[0].trim();
							sError = this.assignQuestion(oAdtnlCondItem, aQuestion[0], aQuestion[1], questionSet);
							if (sError)
								parseError += (parseError ? '<br />\n' : '') + '[' + iIf + '] ' + sError;
							//oAdtnlCondItem.question = aQuestion[0];
							//oAdtnlCondItem.answer = aQuestion[1].trim();
						} else {
							parseError += (parseError ? '<br />\n' : '') + '[' + iIf + '] sRemainedCondition: "' + sRemainedCondition + '"';
						}
					}
					if (this.conditions.indexOf(oAdtnlCondItem) < 0)
						this.conditions.push(oAdtnlCondItem);
				} else {
					//if ('252.219-7003 ALTERNATE I' == oClauseItem.name) { // ('252.232-7004' == oClauseItem.name) || ('52.232-36' == oClauseItem.name)) {
					//	logOnConsole(oClauseItem.name + '[' + iIf + '] "' + sExpression + '"');
					//}
					aParts = sExpression.split(' IS:');
					if (aParts.length > 1) {
						sError = this.parseQuestion(aParts, questionSet, clauseSet);
						if (sError)
							parseError += (parseError ? '<br />\n' : '') + '[' + iIf + '] ' + sError;
					} else {
						aParts = sExpression.split(' TRUE,');
						if (aParts.length > 1) {
							sError = this.parseRule(aParts, questionSet, clauseSet);
						} else
							parseError += (parseError ? '<br />\n' : '') + '[' + iIf + '] no ADDED: "' + sExpression + '"';

					}
				}
			}
		}
		
		if (!parseError) {
			this.isExpressionValid = true;
			if (SHOW_AC_NO_ERROR == true) {
				//logOnConsole('\n<br /><strong>Clause</strong> (id: ' + oClauseItem.id + ', Name: ' + oClauseItem.name + '): NO ERROR');
				logOnConsole('\n<br /><strong>[AC] Clause</strong> (id: ' + oClauseItem.id + ', Name: ' + oClauseItem.name + '): NO ERROR<br />\n'
					+ this.debugHtml());
			}
		} else 
		if (SHOW_AC_ERROR == true) {
			parseError += "\n<pre>" + oClauseItem.additionalConditions + "</pre>\n";
			logOnConsole('\n<br /><strong>[AC] Clause</strong> (id: ' + oClauseItem.id + ', Name: ' + oClauseItem.name + '): ' +
					parseError);
		}
		//return;
	};

	this.parseRule = function parseRule(pParts, questionSet, clauseSet) {
		var sError = null;
		
		var sRule = pParts[0].trim();
		if (!sRule)
			return 'Rule is not defined';

		if (pParts.length < 2)
			return 'Rule expression has less than 2 parts';

		var oAdtnlCondItem = null;
		var sOperations = pParts[1].trim();
		var iPos = sOperations.indexOf('DO NOT INCLUDE ');
		if (iPos >= 0) {
			var sCaluses = sOperations.substring('DO NOT INCLUDE '.length + iPos).trim();
			oAdtnlCondItem = this.addDoNotInclude(sCaluses, questionSet, clauseSet);
		} else {
			iPos = sOperations.indexOf('INCLUDE ');
			if (iPos >= 0) {
				var sCaluses = sOperations.substring('INCLUDE '.length + iPos).trim();
				oAdtnlCondItem = this.addInclude(sCaluses, clauseSet);
			}
		}
		
		if (oAdtnlCondItem != null) {
			oAdtnlCondItem.rule = sRule;
			return ''; // this.assignQuestion(oAdtnlCondItem, sQustion, sAnswer, questionSet);
		}
		
		sError = 'UNKNOWN OPERATION of ' + pParts;
		
		return sError;
	};
	
	this.parseQuestion = function parseQuestion(pParts, questionSet, clauseSet) {
		var sError = null;
		
		var sQustion = pParts[0].trim();
		if (!sQustion)
			return 'Question is not defined';

		if (pParts.length < 2)
			return 'Question expression has less than 2 parts';

		var aParts2 = pParts[1].split(',');
		if (aParts2.length != 2)
			return 'Question expression does not match with 1 character';

		var sAnswer = aParts2[0].trim();
		
		var oAdtnlCondItem = null;
		var sOperations = aParts2[1].trim();
		var iPos = sOperations.indexOf('DO NOT INCLUDE ');
		if (iPos >= 0) {
			var sCaluses = sOperations.substring('DO NOT INCLUDE '.length + iPos).trim();
			oAdtnlCondItem = this.addDoNotInclude(sCaluses, questionSet, clauseSet);
		} else {
			iPos = sOperations.indexOf('INCLUDE ');
			if (iPos >= 0) {
				var sCaluses = sOperations.substring('INCLUDE '.length + iPos).trim();
				oAdtnlCondItem = this.addInclude(sCaluses, clauseSet);
			}
		}
		
		if (oAdtnlCondItem != null) {
			aParts2 = sQustion.split(' TRUE AND ');
			if (aParts2.length == 2) {
				oAdtnlCondItem.rule = aParts2[0].trim();
				sQustion = aParts2[1].trim();
			}
			//oAdtnlCondItem.question = sQustion;
			//oAdtnlCondItem.answer = sAnswer.trim();
			return this.assignQuestion(oAdtnlCondItem, sQustion, sAnswer, questionSet);
		}
		
		sError = 'UNKNOWN OPERATION of ' + pParts;
		
		return sError;
	};
	
	this.assignQuestion = function assignQuestion(oAdtnlCondItem, sQustion, sAnswer, questionSet) {
		var oQuestion = questionSet.findByName('[' + sQustion + ']');
		if (oQuestion) {
			sAnswer = sAnswer.trim();
			if (sAnswer.charAt(sAnswer.length - 1) == ',')
				sAnswer = sAnswer.substring(0, sAnswer.length - 1).trim();
			var bFound = false;
			var bNot = false;
			var sNotAnswer = sAnswer;
			if (sNotAnswer.startsWith('NOT '))
				sNotAnswer = sNotAnswer.substring('NOT '.length).trim();
			var debug = 'Value not found: "' + sAnswer + '" in [' + sQustion + '] question.';
			
			if (oQuestion.hasChoice(sAnswer, true)) // Hierarchy
				bFound = true;
			else if (oQuestion.hasChoice(sNotAnswer, true)) { // Hierarchy
				sAnswer = sNotAnswer;
				bNot = true;
				bFound = true;
			}
			else if (oQuestion.isNumberType()) {
				if (sAnswer)
					sAnswer = sAnswer.replace(/,/g, '');
				if (! isNumber(sAnswer)) {
					debug = '[' + sQustion + '] Value must be a number but found: [' + sAnswer + ']';
				} else {
					bFound = true;
				}
			}
			if (bFound == false) {
				debug += ' Available values are <ul>';
				var choices = oQuestion.choices.elements;
				for (var c = 0; c < choices.length; c++) {
					var questionChoiceItem = choices[c];
					if (sAnswer == questionChoiceItem.choice) {
						bFound = true;
						break;
					}
					else if (sNotAnswer == questionChoiceItem.choice) {
						bFound = true;
						bNot = true;
						break;
					}
					debug += '<li>"' + questionChoiceItem.choice + '"</li>';
				}
				debug += '</ul>';
			}
			
			/*
			var choices = oQuestion.choices.elements;
			for (var c = 0; c < choices.length; c++) {
				var questionChoiceItem = choices[c];
				if (sAnswer == questionChoiceItem.choice) {
					bFound = true;
					break;
				}
				else if (sNotAnswer == questionChoiceItem.choice) {
					bFound = true;
					bNot = true;
					break;
				}
				debug += '<li>"' + questionChoiceItem.choice + '"</li>';
			}
			debug += '</ul>';
			*/
			if (bFound) {
				oAdtnlCondItem.question = oQuestion;
				oAdtnlCondItem.answer = sAnswer.trim();
				oAdtnlCondItem.notAnswer = bNot;
				return null;
			} else {
				return debug;
			}

		} else {
			return 'Unknown Question [' + sQustion + ']';
		}
	};

	// ------------------------------------------
	
	this.addDoNotInclude = function addDoNotInclude(pClauses, questionSet, clauseSet, oAdtnlCondItem) {
		var aClauses = this.parseClauses(pClauses, clauseSet);

		if (!oAdtnlCondItem)
			oAdtnlCondItem = new AdditionalConditionItem();
		oAdtnlCondItem.oprType = OPR_DO_NOT_INCLUDE;
		oAdtnlCondItem.oprClauses = aClauses;
		
		// Code to add directly to the clause evaluation class.
		for (var i=0; i<oAdtnlCondItem.oprClauses.length; i++) { 
			//logOnConsole ("AdditionalConditionItem: If " + this.clause.name + " OPR_DO_NOT_INCLUDE for " + oAdtnlCondItem.oprClauses[i].name);
			
			// Ex. If 252.232-7004 OPR_DO_NOT_INCLUDE for 52.232-16 Alternate I
			
			var oClause = oAdtnlCondItem.oprClauses[i]; 
			
			var oAdditionalRules = new AdditionalRules(questionSet, clauseSet);
			
			// Replace any evals that point back to Include clause.
			
			// Ex. If 252.232-7004 OPR_DO_NOT_INCLUDE for 52.232-16 Alternate I
			// Clause:  252.232-7004 
			// Condition: (A || B) && C && D 
			// [D] 52.232-16 ALTERNATE I: INCLUDED ==> same arg being excluded.
			
			oAdditionalRules.replaceRecursiveClauseArgs (this.clause, oClause);
			
			// Ex. 52.232-16 Alternate I
			// Modified Condition = (A || (B && D)) && C && E && F && Z0 
			// New Eval = [Z0] 252.232-7004:  (NOT) INCLUDED
			oAdditionalRules.appendDoNotInclArgument(oClause, this.clause);
			
			if ((oClause.oprType == null) || (oClause.oprType != OPR_REPLACE_WITH))	// CJ-1366, clause may already be replaced.
				oClause.oprType = OPR_DO_NOT_INCLUDE; // CJ-1157
		}
		
		this.conditions.push(oAdtnlCondItem);
		return oAdtnlCondItem;
	}
	
	this.addInclude = function addInclude(pClauses, clauseSet, oAdtnlCondItem) {
		
		var aClauses = this.parseClauses(pClauses, clauseSet);

		if (!oAdtnlCondItem)
			oAdtnlCondItem = new AdditionalConditionItem();
		oAdtnlCondItem.oprType = OPR_INCLUDE;
		oAdtnlCondItem.oprClauses = aClauses;
		
		// Code to add directly to the clause evaluation class.
		for (var i=0; i<oAdtnlCondItem.oprClauses.length; i++) { 
			//logOnConsole ("AdditionalConditionItem: If " + this.clause.name + " OPR_INCLUDE for " + oAdtnlCondItem.oprClauses[i].name);
			
			var oClause = oAdtnlCondItem.oprClauses[i]; 
			var sRule = oClause.rule.replace(/[^a-zA-Z 0-9]+/g, '');
			

			// Add numbers for the Add Cond.
			// Note: conditions will break, if a number is in first position
			for (var i=48; i<58; i++) { 
				var code = 'Z' + String.fromCharCode(i);
				if (sRule.indexOf (code) < 0)
					break;
			}
			
			oClause.rule = oClause.rule + " OR " + code;
			oClause.conditions = oClause.conditions + "\n(" + code + ") " + this.clause.name + " IS: INCLUDED";
			
			// add the condition
			if (oClause.oEvaluation != null) {
				
				var oEval = new ClauseEvalItem();
				
				oEval.code = code;
				oEval.name = this.clause.name;
				oEval.not = false;
				oEval.oClause = this.clause;
				oEval.oprType = 1;
				oEval.regulationId = this.clause.regulationId;
				oEval.value = "INCLUDED";
				
				oClause.oEvaluation.rule = oClause.oEvaluation.rule + " || " + code;
				oClause.oEvaluation.conditions.push(oEval);
			}
			
		}
		
		this.conditions.push(oAdtnlCondItem);
		return oAdtnlCondItem;
	};
	/*
	this.addReplaceWidth = function addReplaceWidth(pClauses, questionSet, clauseSet, oAdtnlCondItem) {
		var aClauses = this.parseClauses(pClauses, clauseSet);

		if (!oAdtnlCondItem)
			oAdtnlCondItem = new AdditionalConditionItem();
		oAdtnlCondItem.oprType = OPR_REPLACE_WITH;
		oAdtnlCondItem.oprClauses = aClauses;
		
		if (oAdtnlCondItem.andClauses.length > 0) { 
			
			//logOnConsole ("AdditionalConditionItem: If " + oAdtnlCondItem.andClauses[0].name + " OPR_REPLACE_WITH with " + this.clause.name );
			
			var oClauseCondition = new ClauseAddCondition();
			oClauseCondition.oprType = 3;	// CJ-180
			oClauseCondition.newClause = this.clause;
			oClauseCondition.newClause.oprAdtnlParentId = oAdtnlCondItem.andClauses[0].id;
			oClauseCondition.newClause.oprAdtnlCond = 3;
			
			oAdtnlCondItem.andClauses[0].arrAdtnlCond.push (oClauseCondition);
			
		}

		this.conditions.push(oAdtnlCondItem);
		return oAdtnlCondItem;
	};
	*/
	
	this.addReplaceWidth = function addReplaceWidth(pClauses, questionSet, clauseSet, oAdtnlCondItem) {
		var aClauses = this.parseClauses(pClauses, clauseSet);

		if (!oAdtnlCondItem)
			oAdtnlCondItem = new AdditionalConditionItem();
		oAdtnlCondItem.oprType = OPR_REPLACE_WITH;
		oAdtnlCondItem.oprClauses = aClauses;
		
		if (oAdtnlCondItem.andClauses.length > 0) { 
			
			//logOnConsole ("AdditionalConditionItem: If " + oAdtnlCondItem.andClauses[0].name + " OPR_REPLACE_WITH with " + this.clause.name );
			
			var oClause = oAdtnlCondItem.andClauses[0];
			
			var oAdditionalRules = new AdditionalRules(questionSet, clauseSet);
			oAdditionalRules.appendReplaceArgument(this.clause, oClause);
			
			oClause.oprType = OPR_REPLACE_WITH;
		}

		this.conditions.push(oAdtnlCondItem);
		return oAdtnlCondItem;
	};
	
	// ----------------------------------------------------------------
	this.resolveClauseSet = function resolveClauseSet(aClauses, clauseSet) {
		var aResult = new Array();
		for (var item = 0; item < aClauses.length; item++) {
			var sClause = aClauses[item].trim();
			if (sClause) {
				var oClause = clauseSet.findByName(sClause);
				if (oClause)
					aResult.push(oClause);
				else
					logOnConsole('\n<br /><strong>[AC] Clause</strong> (id: ' + this.clause.id + ', Name: ' + this.clause.name + '): ' +
						' unknown clause "' + sClause + '"');
			}
		}
		return aResult;
	};

	this.resolveClause = function resolveClause(sClause, clauseSet) {
		var oClause = clauseSet.findByName(sClause);
		if (! oClause)
			logOnConsole('\n<br /><strong>[AC] Clause</strong> (id: ' + this.clause.id + ', Name: ' + this.clause.name + '): ' +
						' unknown clause "' + sClause + '"');
		return oClause;
	};

	// ----------------------------------------------------------------
	this.parseClauses = function parseClauses(pClauses, clauseSet) {
		pClauses = pClauses.replace(/DFARS CLAUSE /g, ' ').replace(/DFAR /g, ' ').replace(/FAR /g, ' ');
		var aItems = pClauses.trim().split(',');
		var aResult = new Array();
		for (var item = 0; item < aItems.length; item++) {
			var sClause = aItems[item].trim();
			if (sClause) {
				var oClause = clauseSet.findByName(sClause);
				if (oClause)
					aResult.push(oClause);
				else
					logOnConsole('\n<br /><strong>[AC] Clause</strong> (id: ' + this.clause.id + ', Name: ' + this.clause.name + '): ' +
						' unknown clause "' + sClause + '"');
			}
		}
		return aResult;
	};
	
	this.evaluate = function evaluate(oFinalCheckSet, oPrescriptionChoices) {
		if (this.isExpressionValid == false)
			return;
		for (var iEval = 0; iEval < this.conditions.length; iEval++) {
			var oAdtnlCondItem = this.conditions[iEval];
			oAdtnlCondItem.evaluate(this.clause, oFinalCheckSet, oPrescriptionChoices);
		}
	};
		
	// ----------------------------------------------------------------
	this.debugHtml = function debugHtml() {
		var html = '';
		for (var item = 0; item < this.conditions.length; item++) {
			if (item == 0)
				html = this.conditions[item].debugHtml();
			else
				html += '<hr/>' + this.conditions[item].debugHtml();
		}
		return html;
	};
	
};

// -----------------------------------------------------------

function RuleItem() {

	this.origCode;
	this.origCondition;
	this.newCode;
	this.newCondition;
	
	this.oEval = new ClauseEvalItem();
}

function AdditionalRules(questionSet, clauseSet) {
	
	this.questionSet = questionSet;
	this.clauseSet = clauseSet;
	
	this.replacementConditionList = new Array(); // conditions, rule
	this.originalConditionList = new Array();
	this.replacementEval = null;
	
	// TODO: Support every letter in the Alpha from Z-A.
	this.getNextArgument = function getNextArgument(sRule) { 
		// Add numbers for the Add Cond.
		// Note: conditions will break, if the number is in first position
		
		sRule = sRule.replace(/[^a-zA-Z 0-9]+/g, '');
		
		var bFound = false;
		var code;
		for (var j=90; j>64; j--) { 
			for (var i=48; i<58; i++) { 
				var code = String.fromCharCode(j) + String.fromCharCode(i);
				if (sRule.indexOf (code) < 0) { 
					bFound = true;
					break;
				}
			}
			
			if (bFound)
				break;
		}
			
		return code;
	}
	
	this.isClauseACondition = function isClauseACondition(mainClause, argClause) { 
		
		var bFound = false;
		this.replacementEval = null;
		
		var sRule = mainClause.originalRule;
		sRule = sRule.replace(/OR/g, '||').replace(/AND/g, '&&');
		
		for (var j=0; j < mainClause.oEvaluation.conditions.length; j++){
			var oEval = mainClause.oEvaluation.conditions[j];
			if ((oEval.name.toUpperCase() == argClause.name.toUpperCase()) && (sRule.indexOf(oEval.code) >= 0)){ 
				//logOnConsole ("Uh oh! Need to replace condition: " + argClause.name);
				this.replacementEval = oEval;
				bFound = true;
				break;
			}
		}
		
		return bFound;
	}
	
	this.parseConditions = function parseConditions(argClause, replacementConditions) {	
		
		// var parseError = '';
		
		var conditions = argClause.originalConditions.replace(/\n/g, ' ');

		var sLine, iPos;
		var aLines = new Array();
		var aTemp = conditions.split(' (');
		for (var iLine = 0; iLine < aTemp.length; iLine++) {
			sLine = aTemp[iLine];
			if (iLine == 0) {
				aLines.push(sLine);
			} else {
				iPos = sLine.indexOf(')');
				var c = sLine.charAt(0);
				if ((iPos < 0) || (iPos > 2) || (c < 'A') || (c > 'Z')) {
					var index = aLines.length - 1;
					aLines[index] = aLines[index] + ' (' + sLine;
				} else 
					aLines.push(sLine);
			}
		}

		var bError = false;
		for (var iLine = 0; iLine < aLines.length; iLine++) {
			sLine = aLines[iLine].trim();
			if (sLine) {
				iPos = sLine.indexOf(')');
				if ((iPos > 3)) {
					// Parser error
					logOnConsole ("Additional Conditional Parser Error:" + argClause.name);
					bError = true;
					break;
				} else {
					var code = sLine.substring (0, iPos);
					code = code.replace(/[^a-zA-Z 0-9]+/g, '');
					
					if (sLine.charAt(0) != '(')
						sLine = '(' + sLine;
					
					oItem = new RuleItem();
					oItem.origCode = code;
					oItem.origCondition = sLine;
					
					oItem.oEval.setRegulationId (argClause.regulationId);
					oItem.oEval.parseLine(oItem.origCondition, questionSet, clauseSet);
					
					//this.populateEval(oItem.oEval, argClause, oItem.origCode, oItem.origCondition) ;
					replacementConditions.push (oItem);
					
					/*
					logOnConsole ("Eval conditions found: " + oItem.origCode + ", "+ oItem.origCondition);
					logOnConsole ("oEval.code: " + oItem.oEval.code);
					logOnConsole ("oEval.name: " + oItem.oEval.name);
					logOnConsole ("oEval.not: " + oItem.oEval.not);
					logOnConsole ("oEval.regulationId: " + oItem.oEval.regulationId);
					logOnConsole ("oEval.oprType: " + oItem.oEval.oprType);
					logOnConsole ("oEval.value: " + oItem.oEval.value);
					*/
					
				}
			}
		}
		
		if (bError)
			this.replacementConditionList.length = 0;
	}
	
	// Original Rule: (A OR (B AND D)) AND C AND E AND F 
	// Map: A->Z0;  B->Z1; etc. 
	
	this.mapConditions = function mapConditions(mainClause, argClause) {
		
		var sRule = mainClause.rule;
		
		for (var i=0; i<this.replacementConditionList.length; i++) {
			
			var code = this.getNextArgument (sRule);
			sRule = sRule + " AND " + code;
			
			var sOrigCode = '(' + this.replacementConditionList[i].origCode + ')';
			var sNewCode = '(' + code + ')';
			
			this.replacementConditionList[i].newCode = code;
			this.replacementConditionList[i].newCondition = this.replacementConditionList[i].origCondition.replace (sOrigCode, sNewCode);
		}
	}
	
	// To prevent rule/condition conflicts, create unique arguments to generate to build a replacement string.
	
	// Original Rule: (A OR (B AND D)) AND C AND E AND F 
	// Replacement rule = (Z0 OR (Z1 AND Z3)) AND Z2 AND Z4 AND Z5 
	
	this.getReplacementRule = function getReplacementRule (argClause) {
		
		var sRule = argClause.originalRule;
		sRule = sRule.replace(/OR/g, '||').replace(/AND/g, '&&');
		
		for (var i=0; i<this.replacementConditionList.length; i++) {
			var code = this.replacementConditionList[i].origCode;
			// replace 2-char codes first.
			if (code.length > 1) {
				while (sRule.indexOf (code) >= 0)
					sRule = sRule.replace (code, this.replacementConditionList[i].newCode);
			}
		}
		
		for (var i=0; i<this.replacementConditionList.length; i++) {
			var code = this.replacementConditionList[i].origCode;
			// replace 1-char codes last.
			if (code.length == 1) {
				while (sRule.indexOf (code) >= 0)
					sRule = sRule.replace (code, this.replacementConditionList[i].newCode);
			}
		}
		
		while (sRule.indexOf ('||') >= 0)
			sRule = sRule.replace('||', 'OR');
		while (sRule.indexOf ('&&') >= 0)
			sRule = sRule.replace('&&', 'AND');
		
		return sRule;
	}
	
	// Replace the given code, with the given replaceString
	// OriginalRule: (A OR B) AND C AND ((D AND !F) OR (E AND F)) AND G 
	// Replacement rule: (Z0 OR Z1)  
	// ModifiedRule rule: (A OR B) AND (Z0 OR Z1) OR D OR E) AND F AND G
			
	this.getModifiedRule = function getModifiedRule (mainClause, sReplacementCode, sReplacementRule) {
		
		var sRule = mainClause.rule;
		sRule = sRule.replace(/OR/g, '||').replace(/AND/g, '&&');
		
		sRule = sRule.replace(sReplacementCode, '(' + sReplacementRule + ')');
			
		while (sRule.indexOf ('||') >= 0)
			sRule = sRule.replace('||', 'OR');
		while (sRule.indexOf ('&&') >= 0)
			sRule = sRule.replace('&&', 'AND');
		
		return sRule;
	}

	// Remove the condition string.
	// Orig: (A) DOCUMENT TYPE IS: SOLICITATION (B) DOCUMENT TYPE IS: AWARD (C) 52.219-9 IS: INCLUDED 
	// Modified: (A) DOCUMENT TYPE IS: SOLICITATION (B) DOCUMENT TYPE IS: AWARD  
	
	this.getModifiedCondition = function getModifiedCondition (sConditions, code) {
		
		var sRule = "";
		for (var i=0; i<this.originalConditionList.length; i++) {
			var oCond = this.originalConditionList[i];
			if (oCond.origCode == code)
				sRule = oCond.origCondition;
		}

		sConditions = sConditions.replace (sRule, "");
		return sConditions;
	}
	
	// Replace the given code, with the given replaceString
	// OriginalRule: (A OR B) AND C AND ((D AND !F) OR (E AND F)) AND G 
	// Replacement rule: (Z0 OR Z1)  
	// ModifiedRule rule: (A OR B) AND (Z0 OR Z1) OR D OR E) AND F AND G
			
	this.getModifiedEvalRule = function getModifiedEvalRule (mainClause, sReplacementCode, sReplacementRule) {
		
		var sRule = mainClause.rule;
		sRule = sRule.replace(/OR/g, '||').replace(/AND/g, '&&');
		
		sRule = sRule.replace(sReplacementCode, '(' + sReplacementRule + ')');
		
		sRule = sRule.replace(/OR/g, '||').replace(/AND/g, '&&');
		
		return sRule;
	}
	
	this.dumpConditions = function dumpConditions(ConditionList) { 
		for (var i=0; i<ConditionList.length; i++) {
			var oCond = ConditionList[i];
			logOnConsole ("dumpConditions[origCode]: " + oCond.origCode + ", [origCondition]: " + oCond.origCondition);
			logOnConsole ("dumpConditions[newCode]: " + oCond.newCode + ", [newCondition]: " + oCond.newCondition);
		}
	}
	
	this.replaceRecursiveClauseArgs = function replaceRecursiveClauseArgs(mainClause, argClause) { 
		
		if ((!this.isClauseACondition(mainClause, argClause)) || (!this.replacementEval))
			return;
		
		this.parseConditions (mainClause, this.originalConditionList);
		this.parseConditions (argClause, this.replacementConditionList);
		if (this.replacementConditionList.length == 0)
			return;
		
		this.mapConditions(mainClause, argClause);
		var sReplacementRule = this.getReplacementRule (argClause);

		var sModifiedRule = this.getModifiedRule (mainClause, this.replacementEval.code, sReplacementRule);
		var sModifiedCondition = this.getModifiedCondition (mainClause.conditions, this.replacementEval.code);
		var sModifiedEvalRule = this.getModifiedEvalRule (mainClause, this.replacementEval.code, sReplacementRule);
		
		if (mainClause.oEvaluation != null) {
			for (var i=0; i< this.replacementConditionList.length; i++) {
				var oCond = this.replacementConditionList[i];
				
				oCond.oEval.code = oCond.newCode;
				
				mainClause.oEvaluation.conditions.push(oCond.oEval);
			}
			mainClause.oEvaluation.rule = sModifiedEvalRule;
		}
		
		mainClause.rule = sModifiedRule;
		mainClause.conditions = sModifiedCondition;
		
		// Logging
		/*logOnConsole ("mainClause.originalRule: " + mainClause.originalRule);
		logOnConsole ("argClause.originalRule: " + argClause.originalRule);
		logOnConsole ("Replacement rule = " + sReplacementRule);
		logOnConsole ("ModifiedRule rule = " + sModifiedRule);
		logOnConsole ("evalReplacementCode: " + this.replacementEval.code);
		logOnConsole ("mainClause.conditions: " + mainClause.conditions);
		logOnConsole ("sModifiedCondition: " + sModifiedCondition);
		logOnConsole ("mainClause.oEvaluation.rule: " + mainClause.oEvaluation.rule);
		logOnConsole ("sModifiedEvalRule: " + sModifiedEvalRule);
		*/
		
		
		//this.dumpConditions (this.replacementConditionList);
	}
	
	this.appendDoNotInclArgument = function appendDoNotInclArgument(mainClause, argClause) { 
		
		var code = this.getNextArgument (mainClause.rule);
		
		mainClause.rule = mainClause.rule + " AND " + code;
		mainClause.conditions = mainClause.conditions + "\n(" + code + ") " + argClause.name + " IS: NOT INCLUDED";
		
		//logOnConsole (mainClause.conditions);
		
		// add the condition
		if (mainClause.oEvaluation != null) {
			
			var oEval = new ClauseEvalItem();
			
			oEval.code = code;
			oEval.name = argClause.name;
			oEval.not = true;
			oEval.mainClause = argClause;
			oEval.oClause = argClause;
			oEval.oprType = 1;
			oEval.regulationId = argClause.regulationId;
			oEval.value = "INCLUDED";
			
			mainClause.oEvaluation.rule = mainClause.oEvaluation.rule + " && " + code;
			mainClause.oEvaluation.conditions.push(oEval);
			
			//logOnConsole ("do not include: extra condition");
			//logOnConsole (mainClause.oEvaluation.rule);
		}
	}
	
	// Replace the given code, with the given replaceString
	// OriginalRule: (A OR B) AND C 
	// ModifiedRule rule: ((A OR B) AND C) || Z0 
	
	this.appendReplaceArgument = function appendReplaceArgument(mainClause, argClause) { 
		
		var code = this.getNextArgument (mainClause.rule);
		
		mainClause.rule = "(" + mainClause.rule + ") OR " + code;
		mainClause.conditions = mainClause.conditions + "\n(" + code + ") " + argClause.name + " IS: INCLUDED";
		
		//logOnConsole (mainClause.conditions);
		
		// add the condition
		if (mainClause.oEvaluation != null) {
			
			var oEval = new ClauseEvalItem();
			
			oEval.code = code;
			oEval.name = argClause.name;
			oEval.not = false;
			oEval.mainClause = argClause;
			oEval.oClause = argClause;
			oEval.oprType = 1;
			oEval.regulationId = argClause.regulationId;
			oEval.value = "INCLUDED";
			
			mainClause.oEvaluation.rule = "(" + mainClause.oEvaluation.rule + ") || " + code;
			mainClause.oEvaluation.conditions.push(oEval);
			
			//logOnConsole ("replace: extra condition");
			//logOnConsole (mainClause.oEvaluation.rule);
		}
	}
}