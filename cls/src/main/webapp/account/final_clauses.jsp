<!DOCTYPE html>
<html lang='en'>
<head>
	<meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
	<meta http-equiv="Pragma" content="no-cache" />
	<meta http-equiv="Expires" content="0" />
	<meta charset='utf-8'>
	<meta content='IE=Edge,chrome=1' http-equiv='X-UA-Compatible'>
	<meta content='width=device-width, initial-scale=1.0' name='viewport'>
	<title>Working Clause Sheet</title>
    <link href="../assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="../assets/cleditor1_4_5/jquery.cleditor.css" rel="stylesheet">

    <link href="../assets/datatables/css/jquery.dataTables.css" rel="stylesheet">
    <link href="../assets/datatables/css/fixedHeader.dataTables.min.css" rel="stylesheet">
    <!-- <link href="../assets/datatables/css/dataTables.bootstrap.css" rel="stylesheet"> -->

	<link href="../assets/application.css" media="all" rel="stylesheet" type="text/css" />
	<link href="interview.css" media="all" rel="stylesheet" type="text/css" />
	<link href="final_clauses.css" media="all" rel="stylesheet" type="text/css" />
	
	<!-- Le HTML5 shim, for IE6-8 support of HTML elements -->
	<!--[if lt IE 9]>
	<script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.6.1/html5shiv.js" type="text/javascript"></script>
	<![endif]-->
	
	<style type="text/css">
		/*
		.maxHeightCell {
			height:38px;
			overflow:hidden;
			word-wrap:break-word;
			vertical-align: middle;
		}
		*/

		.dataTables_wrapper .dataTables_paginate .paginate_button {
		  color: white !important;
		  border: 1px solid #1f6d9a;
		  background-color: #1f6d9a; /* #337ab7 #0e537b #1f6d9a */
		}
		
		.dataTables_wrapper .dataTables_paginate .paginate_button.current, 
		.dataTables_wrapper .dataTables_paginate .paginate_button.current:hover {
		  color: black !important;
		  outline: none;
		  font-weight: bold;
		  background-color: #bce1f6;
		  background: #bce1f6;
		
		  /* background: -webkit-gradient(linear, left top, left bottom, color-stop(0%, #337ab7), color-stop(100%, #0c0c0c)); */
		  /* Chrome,Safari4+ */
		  /* background: -webkit-linear-gradient(top, #337ab7 0%, #0c0c0c 100%); */
		  /* Chrome10+,Safari5.1+ */
		  /* background: -moz-linear-gradient(top, #337ab7 0%, #0c0c0c 100%); */
		  /* FF3.6+ */
		  /* background: -ms-linear-gradient(top, #337ab7 0%, #0c0c0c 100%); */
		  /* IE10+ */
		  /* background: -o-linear-gradient(top, #337ab7 0%, #0c0c0c 100%); */
		  /* Opera 11.10+ */
		  /* background: linear-gradient(to bottom, #337ab7 0%, #0c0c0c 100%); */
		  /* W3C */
		}
		
		.dataTables_wrapper .dataTables_paginate .paginate_button:hover {
		  color: black !important;
		  outline: none;
		  font-weight: normal;
		  background-color: #bce1f6;
		  background: #bce1f6;
		  /* background: -webkit-gradient(linear, left top, left bottom, color-stop(0%, #337ab7), color-stop(100%, #0c0c0c)); */
		  /* Chrome,Safari4+ */
		  /* background: -webkit-linear-gradient(top, #337ab7 0%, #0c0c0c 100%); */
		  /* Chrome10+,Safari5.1+ */
		  /* background: -moz-linear-gradient(top, #337ab7 0%, #0c0c0c 100%); */
		  /* FF3.6+ */
		  /* background: -ms-linear-gradient(top, #337ab7 0%, #0c0c0c 100%); */
		  /* IE10+ */
		  /* background: -o-linear-gradient(top, #337ab7 0%, #0c0c0c 100%); */
		  /* Opera 11.10+ */
		  /* background: linear-gradient(to bottom, #337ab7 0%, #0c0c0c 100%); */
		  /* W3C */
		  box-shadow: inset 0 0 3px #111;
		}
		
		.dataTables_wrapper .dataTables_paginate .paginate_button {
			min-width: initial; /* 1.5em; */
			padding: 1px 7px; /* 5px 10px; */
			/* font-size: x-small; */
			margin-left: 2px;
			margin-right: 2px;
			margin-bottom: 3px;
		}
		.dataTables_wrapper .dataTables_info {
		  padding-top: 5px;
		}
	</style>
	
</head>
<body style="width: 100%;height: 100%;clip: auto;position: absolute;overflow: hidden;">

<div id="divTopBanner" style="display:none;"></div>

<div data-alerts="alerts" data-titles="{'warning': '<em>Warning!</em>'}" data-ids="myid" data-fade="5000"></div>
 
<div class='container-fluid containerMarginTopNormal' id="divContainer" style="max-height:90%"> 

	<div class='page-header' id="pageHeader">

		<div class='panel panel-primary' id='collapsedHeader' style='border: 0; padding-top:0; padding-bottom:0;margin-top:0px;margin-bottom:0;'> 
		<div class='panel-heading QPanelHeading' >							
		<div class='row' style="white-space: nowrap;margin-left:5px;">ACQUISITION PROFILE
		    <a href="#" onclick="expandHeader();" style="float:right;color:white;margin-right:20px;" >
		    <span id="expand_link" ><img src="../assets/images/menu_collapse.png" alt="Menu collapse." /></span></a>
		</div>
		</div>
		</div>
		
		<table id='expandedHeader' style="width:100%; margin-top:10px; display:block;">
			<tr>
				<td valign="top" style="padding-right:20px;"> <!--  width="50%"  -->
					<table style="width:100%;">
					<tr>
						<td><!--  width="45%" -->
							<span style="white-space: nowrap;"><span class="docLabel">Acquisition Title:</span> <span id="acquisition_title"></span></span>
						</td>
						<td rowspan="2" valign="top" style="padding-left:15px;padding-right:5px">
							<span class="docLabel">Last Updated:</span> <span id="spanUpdatedAt" style="white-space: nowrap;"></span>
						</td>
					</tr>
					<tr>
						<td>
							<span style="white-space: nowrap;"><span class="docLabel">Document Number:</span> <span id="document_number"></span></span><br/>
						</td>
					</tr>
					<tr>
						<td>
							<span style="white-space: nowrap;"><span class="docLabel">Document Type:</span> <span id="spanDocType"></span></span>
						</td>
						<td><font class="text-warning"><span id="spanAutoSaveDisplay"></span></font></td>
					</tr>
					</table>
				</td>
				<td valign="top" id="tdFixMessage"> <!-- width="50%"  -->
					<!-- <strong>Clause Version: </strong><span id="spanVersionName"></span><br /> -->
					<div id="divFixMessage" style="border-style:solid;border-width:1px;border-color:grey;padding:5px;text-align:left;magin-top:-5px;font-size:smaller;">
						<span id="spanFixedMessage">A complete and accurate set of clauses will not be available until all questions in all applicable sections have been answered and the session has been validated.</span>
					</div>
				</td>
			</tr>
		</table>
	</div>
	
	<div class="panel panel-default" id="mainPanel" style="margin-top:0;padding:0;margin-bottom:24px;">
		<div class="panel-body" id="mainPanelBody" style="margin-top:0;padding:0;">

			<div role='tabpanel' > <!-- style="padding-top:10px;"  -->
				<ul class='nav nav-tabs' role='tablist'>
					<li class='active' id='selectedClauses-tab' role='presentation'>
						<a href="#selectedClauses" data-toggle="tab" role="tab">Clauses</a>
					</li>
					<li  id='addClauses-tab' role='presentation'>
						<a href="#addClauses" data-toggle="tab" role="tab">Add</a>
					</li>
					<li  id='removeClauses-tab' role='presentation'>
						<a href="#removeClauses" data-toggle="tab" role="tab">Removed</a>
					</li>
				</ul>
			</div>
		
			<div class='tab-content' id='finalclause_content' >
				<div class='tab-pane active' id='selectedClauses' role='tabpanel' >
					<table class="table table-hover table-striped" id="tableSelectedClauses"><!-- document-table table display -->
						<thead>
							<tr>
							<th style="max-width:125px;"><a href="#" onclick="javascript:onActvClausesSortClick('section', 0)">Section</a>
									<span id="spanACSC0"></span>
								</th>
								<th style="max-width:125px;"><a href="#" onclick="javascript:onActvClausesSortClick('clause_num', 1)">Clause #</a>
									<span id="spanACSC1"></span>
								</th>
								<th style="max-width:250px;"><a href="#" onclick="javascript:onActvClausesSortClick('title', 2)">Clause Title</a>
									<span id="spanACSC2"></span>
								</th>
								<th style="max-width:125px;"><a href="#" onclick="javascript:onActvClausesSortClick('prescription', 3)">Prescription</a>
									<span id="spanACSC3"></span>
								</th>
								<th><a href="#" onclick="javascript:onActvClausesSortClick('regulation', 4)" title="Regulation">Regs</a>
									<span id="spanACSC4"></span>
								</th>
								<th><a href="#" onclick="javascript:onActvClausesSortClick('clause_date', 5)">Date</a>
									<span id="spanACSC5"></span>
								</th>
								<th><a href="#" onclick="javascript:onActvClausesSortClick('type', 6)">Type</a>
									<span id="spanACSC6 "></span>
								</th>
								<th><a href="#" onclick="javascript:onActvClausesSortClick('required', 7)" title="Is Required?">Req.</a>
									<span id="spanACSC7 "></span>
								</th>
								<th><a href="#" onclick="javascript:onActvClausesSortClick('edit', 8)" title="Is Editable?">Edit</a>
									<span id="spanACSC8 "></span>
								</th>
								<th><a href="#" onclick="javascript:onActvClausesSortClick('notes', 9)">Notes</a>
									<span id="spanACSC9 "></span>
								</th>
								<th>
									<button id="btnFilter" type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#filterModal">Filter</button>
								</th>
								<th width="14">&#10003;<span class='hide'>Remove</span></th>
							</tr>
						</thead>
					</table>
				</div>
		
				<div class='tab-pane' id='addClauses' role='tabpanel' style='overflow-y:auto;' >
					<div class='container-fluid margin-left-0 well' style="padding-left:0px;padding-top:10px;padding-bottom:10px;margin:0;">
					<form accept-charset="UTF-8" action="" class="form-inline" method="get" onsubmit="onSearchClickFC(); return false;">
						<div class='col-md-8 margin-left-0'>
							<input class="form-control" id="query" name="query" placeholder="Clause Number, Title, and ECFR Text" title="Clause Number" type="text" />
							<input class="btn btn-primary" name="commit" type="button" value="Search" onclick="onSearchClickFC()" />
							<button type="button" class="btn btn-primary" id="btnFCSearchClear" style="margin-right:16px;" >Clear</button>
						</div>
					</form>
					</div>
					<table id="tableAddClauses" class='table table-hover table-striped'>
						<thead>
							<tr>
								<th>
									<a href="#" onclick="javascript:onAddClausesSortClick('section', 0)">Section</a> 
									<span id="spanICSC0"></span> 
								</th>
								<th width="14">&#10003;<span class='hide'>Select</span></th>
								<th>
									<a href="#" onclick="javascript:onAddClausesSortClick('clause_num', 1)">Clause #</a> 
									<span id="spanICSC1"></span> 
								</th>
								<th>
									<a href="#" onclick="javascript:onAddClausesSortClick('title', 2)">Clause Title</a> 
									<span id="spanICSC2"></span> 
								</th>
								<th>
									<a href="#" onclick="javascript:onAddClausesSortClick('prescription', 3)">Prescription</a> 
									<span id="spanICSC3"></span> 
								</th>
								<th>
									<a href="#" onclick="javascript:onAddClausesSortClick('regulation', 4)" title="Regulation">Regs</a> 
									<span id="spanICSC4"></span> 
								</th>
								<th>
									<a href="#" onclick="javascript:onAddClausesSortClick('clause_date', 5)">Date</a> 
									<span id="spanICSC5"></span> 
								</th>
								<th>
									<a href="#" onclick="javascript:onAddClausesSortClick('type', 6)">Type</a> 
									<span id="spanICSC6"></span> 
								</th>
								<th>
									<a href="#" onclick="javascript:onAddClausesSortClick('required', 7)" title="Is Required?">Req.</a> 
									<span id="spanICSC7"></span> 
								</th>
								<th>
									<a href="#" onclick="javascript:onAddClausesSortClick('edit', 8)" title="Is Editable?">Edit</a> 
									<span id="spanICSC8"></span> 
								</th>
								<th>View Clause</th>
							</tr>
						</thead>
						<tbody>
						</tbody>
					</table>
				</div>
				
				<div class='tab-pane' id='removeClauses' role='tabpanel' style='overflow-y:auto;' >
					<table id="tableRemoveClauses" class='document-table table table-hover table-striped' >
						<thead>
							<tr>
								<th><a href="#" onclick="javascript:onRemoveClausesSortClick('section', 0)">Section</a>
									<span id="spanRCSC0"></span>
								</th>					
								<th style="max-width:125px;"><a href="#" onclick="javascript:onRemoveClausesSortClick('clause_num', 1)">Clause #</a>
									<span id="spanRCSC1"></span>
								</th>
								<th style="max-width:250px;"><a href="#" onclick="javascript:onRemoveClausesSortClick('title', 2)">Clause Title</a>
									<span id="spanRCSC2"></span>
								</th>
								<th style="max-width:125px;"><a href="#" onclick="javascript:onRemoveClausesSortClick('prescription', 3)">Prescription</a>
									<span id="spanRCSC3"></span>
								</th>
								<th><a href="#" onclick="javascript:onRemoveClausesSortClick('regulation', 4)" title="Regulation">Regs</a>
									<span id="spanRCSC4"></span>
								</th>
								<th><a href="#" onclick="javascript:onRemoveClausesSortClick('clause_date', 5)">Date</a>
									<span id="spanRCSC5"></span>
								</th>
								<th><a href="#" onclick="javascript:onRemoveClausesSortClick('type', 6)">Type</a>
									<span id="spanRCSC6 "></span>
								</th>
								<th><a href="#" onclick="javascript:onRemoveClausesSortClick('required', 7)" title="Is Required?">Req.</a>
									<span id="spanRCSC7 "></span>
								</th>
								<th><a href="#" onclick="javascript:onRemoveClausesSortClick('edit', 8)" title="Is Editable?">Edit</a>
									<span id="spanRCSC8 "></span>
								</th>
								<th><a href="#" onclick="javascript:onRemoveClausesSortClick('notes', 9)">Notes</a>
									<span id="spanRCSC9 "></span>
								</th>						
								<th>View Clause</th>	
							</tr>
						</thead>
						<tbody>
						
						</tbody>
					</table>
		
				</div>
				
			</div>

		</div>
		<div class="panel-footer" id="mainPanelFooter">
			<table style="width:100%;"><tr>
				<td width="60%">
					<div id='pageButtons' style='display:none;'> <!-- padding-top:15px;height:35px; -->
						<button type="button" class="btn btn-sm btn-default" id="btnFCCancel" style="margin-right:16px;" >Cancel</button>
						<button type="button" class="btn btn-sm btn-default" id="btnFCSaveContinue" style="margin-right:16px;" >Save</button>
						<button type="button" class="btn btn-sm btn-primary" id="btnFCSaveExit" style="margin-right:16px;" >Save and Return</button>
						<button type="button" class="btn btn-sm btn-success" id="btnFCFinalExit">Finalize and Exit</button> 
					</div>
				</td>
				<td width="40%" align="right">
				Show
				<select id="selItemsPerPage" onclick="selItemsPerPage_onClick(this)" title="items per page" disabled>
				  <option value="10">10</option>
				  <option value="25">25</option>
				  <option value="50">50</option>
				  <option value="100">100</option>
				</select> 
				entries
				</td>
			</tr></table>
		</div>
	</div>

</div>

<!-- Modal -->
<div class="modal fade" id="pleaseWaitDialogFC" role="dialog" data-backdrop="static" data-keyboard="false" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h1>Processing...</h1>
				This may take 1-2 minutes
			</div>
			<div class="modal-body" style="text-align: center;">
				<i class="fa fa-circle-o-notch fa-spin fa-5x"></i>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="saveWaitDialog" data-keyboard="false" role="dialog" data-backdrop="static" aria-hidden="true">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header">
				<h4 style="text-align: center;">Save...&nbsp;&nbsp;   <i class="fa fa-circle-o-notch fa-spin fa-2x"></i></h4>		
			</div>
			
		</div>
	</div>
</div>

<div class="modal fade" id="fcModal" tabindex="-1" role="dialog" aria-labelledby="fcModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-xlg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="fcModelClose"><span aria-hidden="true">&times;</span></button>
        <div class="modal-title" id="fcModalLabel">Modal title</div>
      </div>
      <div class="modal-body" id="fcModalBody">
      </div>
      <div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal" id="fcModelClose">Close</button>
			<button type="button" class="btn btn-primary" id="fcModelSave">In-Progress</button>
			<button type="button" class="btn btn-success" id="fcModelComplete">Complete</button>
      </div>
    </div>
  </div>
</div>


<div class="modal fade" id="fcEditModal" tabindex="-1" role="dialog" aria-labelledby="fcEditModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-xlg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="onEditClose_FC();"><span aria-hidden="true">&times;</span></button>
        <div class="modal-title" id="fcEditModalLabel">Modal title</div>
      </div>   
      <div class="modal-header" id="fcEditModalBanner">
        <h5 class="modal-title">Warning: Remove all or none of the items marked [System Field].</h5>
      </div>
      <!--  
      <label id="fcEditModalBanner" style="padding-left:20px; padding-right:20px;">
      	Warning: Do not edit or remove items marked 
      <span style="background-color: rgb(204, 204, 204);">[System Field]</span>.</label> 
      -->
      <div class="modal-body" id="fcEditModalBody">
      </div>
      <div class="modal-footer">
      	<div class="row">
      		<div class='col-md-8' id="fcModelRemarks"></div>
      		<div class='col-md-3' style="white-space:nowrap;">
		        <button type="button" class="btn btn-default" id="fcEditModelClose">Close</button>
		        <button type="button" class="btn btn-primary" id="fcEditModelSave">Save</button>		        
		   </div>
		</div>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="verFCChangesModal" tabindex="-1" role="dialog" aria-labelledby="verFCChangesModalLabel" aria-hidden="false"
	 data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog modal-xlg">
    <div class="modal-content">
      <div class="modal-header">
        <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>  -->
        <div class="modal-title" id="verFCChangesModalLabel">CLS Version Changes</div>
      </div>
      <div class="modal-body" id="verFCChangesBody">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success" id="bnFCVerChangesUpdate">Update</button>
      </div>
    </div>
  </div>
</div>

<!-- Modal -->
<div class="modal fade" id="filterModal" role="dialog" tabindex="-1" aria-labelledby="fcFilterModalLabel">
    <div class="modal-dialog modal-sm">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <div class="modal-title" id="fcFilterModalLabel">Filter by</div>
        </div>
        <div class="modal-body">
        	<fieldset>
        	<legend class="hide">Filter Cause By</legend>
			<input type="radio" id="rbFilterClauseDataAll" name="filterClauseData" value="All" checked onclick="onFilterClauseData(0)"> <label for="rbFilterClauseDataAll"> All</label><br/>
			<input type="radio" id="rbFilterClauseDataECFR" name="filterClauseData" value="eCFR View" onclick="onFilterClauseData(1)"> <label for="rbFilterClauseDataECFR"> eCFR View</label><br/>
			<input type="radio" id="rbFilterClauseDataFillIn" name="filterClauseData" value="Fill-ins" onclick="onFilterClauseData(2)"> <label for="rbFilterClauseDataFillIn"> Update Fill-ins</label>
        	</fieldset>
        </div>
        <!-- 
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
         -->
      </div>
    </div>
</div>

<div id = "alert_placeholder"></div>
<div id="divSessionTimeout"></div>

<div id="divBottomFooter" style="height:25px;"></div>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="../assets/jquery-1.11.2.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="../assets/bootstrap/js/bootstrap.min.js"></script>
<script src="../assets/bootstrap/js/jquery.bsAlerts.min.js"></script>
<script src="../assets/cleditor1_4_5/jquery.cleditor.js"></script>

<!-- CJ-377- -->
<!-- <script src="../assets/StickyTableHeaders/jquery.stickytableheaders.js"></script> -->
<script src="../assets/datatables/js/jquery.dataTables.js"></script>
<script src="../assets/datatables/js/dataTables.fixedHeader.min.js"></script>
<!-- <script src="../assets/datatables/js/dataTables.bootstrap.js"></script> -->

<script src="../assets/session.js" type="text/javascript"></script>

<script src="interview-q-eval.js" type="text/javascript"></script>
<script src="interview-c-eval.js" type="text/javascript"></script>
<script src="inerview-ac-eval.js" type="text/javascript"></script>
<script src="interview-utils.js" type="text/javascript"></script>
<script src="interview-items.js" type="text/javascript"></script>
<script src="interview-f-check.js" type="text/javascript"></script>
<!--  <script src="interview.js" type="text/javascript"></script> -->
<script src="showClsVersionUpgrade.js" type="text/javascript"></script>

<script src="final_clauses.js" type="text/javascript"></script>

</body>
</html>
