<!DOCTYPE html>
<html lang='en'>
<head>
	<meta charset='utf-8'>
	<meta content='IE=Edge,chrome=1' http-equiv='X-UA-Compatible'>
	<meta content='width=device-width, initial-scale=1.0' name='viewport'>
	<title>Metadata - Clauses - Print</title>
	<!-- Le HTML5 shim, for IE6-8 support of HTML elements -->
	<!--[if lt IE 9]>
	<script src="../..//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.6.1/html5shiv.js" type="text/javascript"></script>
	<![endif]-->
    <link href="../assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="../assets/bootstrapvalidator/css/bootstrapValidator.min.css" rel="stylesheet">
	<link href="../assets/application.css" media="all" rel="stylesheet" type="text/css" />
	<link href="../assets/cls.css" media="all" rel="stylesheet" type="text/css" />
	<!-- = favicon_link_tag 'favicon.ico', :rel => 'shortcut icon' -->
</head>
<body>
<div id="divTopBanner" style="min-height:89px;"></div>

<div data-alerts="alerts" data-titles="{'warning': '<em>Warning!</em>'}" data-ids="myid" data-fade="3000"></div>

<div class='container-fluid margin-top-30'>
	<div class='container-fluid'> </div>
	<div class='container-fluid'>
		<h1>Print Metadata</h1>
		<ul class='nav nav-tabs'>
			<li class='active'><a href="clauses.html">Clauses</a></li>
		</ul>

		<div class='tab-content'>
			<div class='tab-pane active fixed-div'>
				<br>
				<div class='container-fluid margin-left-0 well'>
					<form accept-charset="UTF-8" action="" method="get" onsubmit="retrieveList(1); return false;">
						<div class='col-md-5 margin-left-0'>
							<label class="hide" for="query">Search by clause name, title, data sequence or condition</label>
							<input class="form-control" id="query" name="query" placeholder="Search by clause name, title, data sequence or condition" type="text" 
							onkeydown = "if (event.keyCode == 13) { $('#SearchBttn').click(); return false; }  " />
						</div>
						<div class='col-md-1'>
							<label class="hide" for="section">Search Clause Section</label>
							<select class="form-control" id="section" name="section" title="Search Clause Section">
							<option value=""></option>
							<option value="A">A</option>
							<option value="B">B</option>
							<option value="C">C</option>
							<option value="D">D</option>
							<option value="E">E</option>
							<option value="F">F</option>
							<option value="G">G</option>
							<option value="H">H</option>
							<option value="I">I</option>
							<option value="J">J</option>
							<option value="K">K</option>
							<option value="L">L</option>
							<option value="M">M</option>
							<option value="N">N</option></select>
						</div>
						<div class='col-md-4 dashed-border'>
							<div>
								<fieldset>
								<legend class="hide">Sort by name or title</legend>
								<div class='col-md-3 padding-top-10' style="white-space: nowrap;">									
									<input checked="checked" id="sort_field_clause_name" name="sort_field" type="radio" value="clause_name" title="Sort by Name" />
									<label for="sort_field_clause_name">Name</label>
								</div>
								<div class='col-md-3 padding-top-10' style="white-space: nowrap;">
									<input id="sort_field_clause_title" name="sort_field" type="radio" value="clause_title" title="Sort by Title" />
									<label for="sort_field_clause_title">Title</label>
								</div>
								<div class='col-md-6'>
								    <label class="hide" for="sort_order">Sort order</label>
									<select class="form-control" id="sort_order" name="sort_order" title="Sort order">
									<option value="asc">Ascending</option>
									<option value="desc">Descending</option></select>
								</div>
								</fieldset>
							</div>
						</div>
						<div class='col-md-1'>
							<input class="btn btn-primary" name="commit" type="button" id="SearchBttn" value="Search" onclick="retrieveList(1)" />
						</div>
						<div class='col-md-1 dropdown'>
						<button class="btn btn-default btn-md" data-toggle="dropdown">&nbsp;Print &nbsp; 
						<span class="caret"></span>
						</button>
						<ul class="dropdown-menu" role="menu">
						<li><a href="javascript:void(printList('pdf'));">PDF</a></li>
						<li><a href="javascript:void(printList('word'));">WORD</a></li>
						</ul>
						</div>
					</form>
				</div>

				<!--  <a href="clauses_details.html?mode=new" class="btn btn-default">New Clause</a>  -->
				<table id="tableContent" class='table table-striped'>
					<thead>
						<tr>
							<th></th>
							<th>Clause name</th>
							<th>Clause title</th>
							<th>CLS Version</th>
							<th>Section</th>
							<th>P/C</th>
							<th>Inclusion</th>
							<th>Conditions</th>
							<th>Rule</th>
							<!--  <th colspan='2'>Actions</th>  -->
						</tr>
					</thead>
					<tbody>
					</tbody>
				</table>
				<ul id="ulPagination" class="pagination pagination"></ul>
			</div>
		</div>
	</div>
</div>

<div id="divBottomFooter"></div>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script type="text/javascript" src="../assets/jquery-1.11.2.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script type="text/javascript" src="../assets/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="../assets/bootstrap/js/jquery.bsAlerts.min.js"></script>
<script type="text/javascript" src="../assets/bootstrapvalidator/js/bootstrapValidator.js"></script>
<script type="text/javascript" src="../assets/session.js" type="text/javascript"></script>
<script type="text/javascript" src="clauses_print.js" type="text/javascript"></script>

</body>
</html>
