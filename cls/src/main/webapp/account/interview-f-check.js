var ADDED_CLAUSE = 1;
var REMOVED_CLAUSE = 2;
var NOT_COMPLETED = 3;

//====================================================================================
FinalCheckItem = function(oClause, pMode) {
	this.clause = oClause;
	this.mode = pMode;
	this.removedClauses = new Array();

	this.addRemovedClause = function addRemovedClause(oClause) {
		this.removedClauses.push(oClause);
	};
	
	this.isRemovedClause = function isRemovedClause() {
		return (this.mode == REMOVED_CLAUSE);
	};

	this.isEditClause = function isEditClause() {
		return this.clause.isEditClause();
	};
	
	this.isAddedClause = function isAddedClause() {
		return (this.mode == ADDED_CLAUSE);
	};
	
	this.isNotCompletedClause = function isNotCompletedClause() {
		return (this.mode == NOT_COMPLETED);
	};
	
	this.genHtml = function genHtml() {
		var divClass, btnClass;
		var bEditClause = this.isEditClause();
		if (bEditClause && (this.mode != REMOVED_CLAUSE)) {
			if (this.clause.isCompleted) {
				divClass = 'bg-success';
				btnClass = 'btn-primary';
			} else {
				divClass = 'bg-danger';
				btnClass = 'btn-danger';
			}
		} else {
			divClass = '';
			btnClass = 'btn-primary';
		}
		
		var html = '<div id="' + this.getFormId() + '" class="divClause ' + divClass + '">';
		html += this.clause.getLinkHeader() // sMode + 
			+ 'by ' + ((this.clause.inclusion == 'F') ? 'Full Text' : 'Reference');
		if (this.clause.inclusion == 'F') { // (this.clause.content) {
			html += ' <a id="' + this.getEditLinkId() + '" href="javascript:void(popClause('
				+ this.clause.id + ', ' + bEditClause + ', true))" class="btn btn-xs ' + btnClass + '">'
				+ (bEditClause ? 'Update Fill-ins' : 'View Content') + '</a>';
		}
		// html += '<div style="padding-left:15px">' // 508 Compliance = px removed
		html += '<div style="padding-left:1%">' + this.clause.title + ' (' + longToMonthYear(this.clause.effectiveDate) + ')';
		if (this.removedClauses.length > 0) {
			var sRemovedHtml = '';
			for (var index = 0; index < this.removedClauses.length; index++) {
				var oClause = this.removedClauses[index];
				sRemovedHtml += (sRemovedHtml ? ', ' : '') + oClause.getLinkHeader();
			}
			html += '<br /><strong>Replaced Clauses: </strong>' + sRemovedHtml;
		}
		return html + '</div></div>';
	};
	
	this.getFormId = function getFormId() { return 'fc-c-' + this.clause.id; };
	this.getEditLinkId = function getEditLinkId() { return 'fc-e-' + this.clause.id; };
	
	this.clauseUpdated = function clauseUpdated() {
		this.clause.updateCompleteColor(this.getFormId(), this.getEditLinkId());
	};

};

// ====================================================================================
FinalCheckSet = function() {
	this.elements = new Array();
	
	this.createItem = function createItem(oClause, bAdded) {
		var oFinalCheckItem = this.checkDuplicates(oClause, bAdded);
		if (oFinalCheckItem == null) {
			var bAdd = true;
			oFinalCheckItem = new FinalCheckItem(oClause, bAdded);
			var sAdjustedName = this.adjustClauseNumber(oClause.name);
			for (var iEle = 0; iEle < this.elements.length; iEle++) {
				var oRecord = this.elements[iEle];
				if (this.adjustClauseNumber(oRecord.clause.name) > sAdjustedName) {
					this.elements.splice(iEle, 0, oFinalCheckItem);
					bAdd = false;
					break;
				}
			}
			if (bAdd)
				this.elements.push(oFinalCheckItem);
		}
		return oFinalCheckItem;
	};
	
	// CJ-358, 
	// The Interview_Items.FinalCheckSet() / AdditionalConditions logic
	// can combine to add a clause, then remove it. This makes it
	// appear that an item is being removed, when it wasn't previously added.
	this.createRemoveItem = function createRemoveItem (oClause) {
		
		if (oClause.isVisible == false)
			return null;
		
		var oFinalCheckItem = this.checkDuplicates(oClause, ADDED_CLAUSE);
		if (oFinalCheckItem == null) {
			oFinalCheckItem = new FinalCheckItem(oClause, REMOVED_CLAUSE);
			var sAdjustedName = this.adjustClauseNumber(oClause.name);
			for (var iEle = 0; iEle < this.elements.length; iEle++) {
				var oRecord = this.elements[iEle];
				if (this.adjustClauseNumber(oRecord.clause.name) > sAdjustedName) {
					break;
				}
			}
			this.elements.push(oFinalCheckItem);
			return oFinalCheckItem;
		}
		else {
			var pos = this.elements.indexOf(oFinalCheckItem);
			this.elements.splice(pos, 1);
			return null;
		}
			
	};
	
	this.adjustClauseNumber = function adjustClauseNumber(name) {
		var aItems = name.split('.');
		var result = '';
		for (var iItem = 0; iItem < aItems.length; iItem++) {
			var sItem = aItems[iItem];
			if (sItem.length < 3)
				sItem = ' ' + sItem;
			result += sItem;
		}
		return result;
	} 
	
	this.checkDuplicates = function checkDuplicates(oClause, bAdded) {
		var result = null;
		var iLength = this.elements.length;
		for (var iEle = iLength - 1; iEle >= 0 ; iEle--) {
			var oFinalCheckItem = this.elements[iEle];
			if (oFinalCheckItem.clause == oClause) {
				if (oFinalCheckItem.mode == bAdded)
					result = oFinalCheckItem;
				else
					this.elements.splice(iEle, 1);
			}
		}
		return result;
	}; 
	
	this.addedItem = function addedItem(oClause) {
		return this.createItem(oClause, ADDED_CLAUSE);
	};
	
	this.removedItem = function addedItem(oClause) {
		return this.createItem(oClause, REMOVED_CLAUSE);
	};
	
	this.addItem = function addItem(oClause, bAdded) {
		if (bAdded)
			return this.createItem(oClause, ADDED_CLAUSE);
		else
			return this.createRemoveItem(oClause);
		// return this.createItem(oClause, (bAdded ? ADDED_CLAUSE : REMOVED_CLAUSE));
	};
	
	this.hasItems = function hasItems() {
		return (this.elements.length > 0);
	};
	
	this.addNotCompletedClauses = function addNotCompletedClauses(oClause) {
		return this.createItem(oClause, NOT_COMPLETED);
	};
	
	this.getCountMessage = function getCountMessage() {
		var iRemovedClauses = 0, iAddedRefClauses = 0, iFillinClauses = 0, iIncompletedClauses = 0;
		for (var iEle = 0; iEle < this.elements.length; iEle++) {
			var oFinalCheckItem = this.elements[iEle];
			if (oFinalCheckItem.isRemovedClause()) {
				iRemovedClauses++;
			}
			else if (oFinalCheckItem.isNotCompletedClause()) {
				iIncompletedClauses++;
			}
			else if (oFinalCheckItem.isEditClause()) {
				iFillinClauses++;
			}
			else {
				iAddedRefClauses++;
			}
		}
		var result = '';
		if (iAddedRefClauses > 0)
			result = 'Added: ' + iAddedRefClauses;
		if (iRemovedClauses > 0)
			result += (result == '' ? '' : ', ') + 'Removed: ' + iRemovedClauses;
		if (iIncompletedClauses > 0)
			result += (result == '' ? '' : ', ') + 'Incompleted: ' + iIncompletedClauses;
		if (result == '')
			result = 'All clear';
		return result;
	} 
	
	this.genHtml = function genHtml() {
		var sRemovedClauses = '', sAddedRefClauses = '', sFillinClauses = '', sIncompletedClauses = '';
		var iRemovedClauses = 0, iAddedRefClauses = 0, iFillinClauses = 0, iIncompletedClauses = 0;
		for (var iEle = 0; iEle < this.elements.length; iEle++) {
			var oFinalCheckItem = this.elements[iEle];
			if (oFinalCheckItem.isRemovedClause()) {
				sRemovedClauses += oFinalCheckItem.genHtml();
				iRemovedClauses++;
			}
			else if (oFinalCheckItem.isNotCompletedClause()) {
				sIncompletedClauses += oFinalCheckItem.genHtml();
				iIncompletedClauses++;
			}
			else if (oFinalCheckItem.isEditClause()) {
				sFillinClauses += oFinalCheckItem.genHtml();
				iFillinClauses++;
			}
			else {
				sAddedRefClauses += oFinalCheckItem.genHtml();
				iAddedRefClauses++;
			}
		}
		var html = this.genGroupHtml('ifc-f', 'Added Fill-in Clauses (' + iFillinClauses + ')', sFillinClauses);
		html += this.genGroupHtml('ifc-i', 'Uncompleted Fill-in Clauses (' + iIncompletedClauses + ')', sIncompletedClauses);
		html += this.genGroupHtml('ifc-r', 'Added Reference and Text Clauses (' + iAddedRefClauses + ')', sAddedRefClauses);
		html += this.genGroupHtml('ifc-m', 'Removed Clauses (' + iRemovedClauses + ')', sRemovedClauses);
		return html;
	};
	
	this.genGroupHtml = function genGroupHtml(sGroupId, sTitle, sContent) {
		if (!sContent)
			return '';
		var html =
			'<div class="panel-group" role="tablist" aria-multiselectable="true">' +
				'<div class="panel panel-default collapsible-panel">' // panel-primary
				    + '<div class="panel-heading collapsible-panel-heading">'
					    + '<h4 class="panel-title">'
					    	// + '<table class="tblChoices"><tr>'
					    	// + '<th><i class="fa fa-bell-o"></i></th><td>' // Removed for CJ-546
						    + '<a data-toggle="collapse" href="#' + sGroupId + '">' // data-parent="#' + parentId + '" 
						    + sTitle
						    + '<span class="pull-right clickable"><i class="glyphicon glyphicon-chevron-down"></i></span>'
						    + '</a>'
						    // + '</td></tr></table>'
					    + '</h4>'
				    + '</div>'
				    + '<div id="' + sGroupId + '" class="panel-collapsed collapse in">'
					    + '<div class="panel-body" style="padding:0;">'
					    	+ sContent
					    + '</div>'
				    + '</div>'
			    + '</div>'
		    + '</div>';
		return html;
	};
	
	this.clauseUpdated = function clauseUpdated(oClause) {
		for (var iEle = 0; iEle < this.elements.length; iEle++) {
			var oFinalCheckItem = this.elements[iEle];
			if (oFinalCheckItem.clause == oClause) {
				oFinalCheckItem.clauseUpdated();
			}
		}
	} 
	
};
