var _oUserInfo = new UserInfo();

onPageBtnClick = function(page) {
	location = page;
}

onLoginResponse = function(success, userInfo) {
	if (success) {
		//displayTopBanner(userInfo);
		//$('#spanUserName').html(_oUserInfo.fullName());
		//$('#spanUserOrgName').html(_oUserInfo.orgName);
		$('#user_email').val(userInfo.email);
		$('#user_first_name').val(userInfo.first_name);
		$('#user_last_name').val(userInfo.last_name);
		$('#user_time_zone').val(userInfo.time_zone);
		$("#navSettings").addClass("active");
		displaySessionMessage(userInfo.sessionMessage);
	} else
		goHome();
}

_oUserInfo.getLogon(true, onLoginResponse);