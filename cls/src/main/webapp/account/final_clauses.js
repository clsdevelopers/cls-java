var _oUserInfoFC = new UserInfo();
//_oUserInfoFC.onSessionTimeoutCallback = onSessionTimedOut; // CJ-573 // CJ-634 removed
var _interview = new Interview();
var _docId = getUrlParameter("doc_id");
var _theFcToken = getUrlParameter("token");
var _isFcToken = false;
var _isOrderDoc = false;

var _tableHeight = null;
var _prevSelRowOnSelectedTable = null;
var _prevSelRowOnAddTable = null;

var _titleBannerHeight = 115;

var TAB_ENUM = {
    selected: 0,
    add: 1,
    removed: 2
}

//CJ-1356
var autoSavingInterval;
//CJ-1356 Unsaved Changes
//var unsavedChanges = false;
//CJ-1356
var pendingSave = false;

var _activeClauseTab = TAB_ENUM.selected;

var FILTER_CLAUSE_DATA_ALL = 0;
var FILTER_CLAUSE_DATA_ECFR_VIEW = 1;
var FILTER_CLAUSE_DATA_FILL_IN = 2;

var _filterClauseData = FILTER_CLAUSE_DATA_ALL;


var _pageManager = new FinalClausePageManager();

var TABLE_LANGUAGE = {
	"info": "_START_ to _END_ of _TOTAL_",
	"sInfoEmpty": "0 to 0 of 0" // CJ-1276
};

var TABLE_DOM = 'rt<"bottom"ip><"clear">'; //  // CJ-1274
//'dom': 'rt<"clear"><"bottom container-fluid"<"navRow"<"navDiv"p><"navDiv"i>>>'
//'dom': 'rt<"clear"><"bottom container-fluid"<"navRow"<"navDiv"p><"navDiv"i<"navDiv"l>>>>'
//'dom': 'rt<"clear"><"bottom container-fluid"<"row"<"col-md-4 col-md-offset-3"p><"col-md-2"l><"col-md-3"i>>>'

//-----------------------------------------------------------------
function disableAllOptionsForExpiredSession() { // CJ-634 added
	$("#btnFCCancel").prop("disabled", true);
	$("#btnFCSaveContinue").prop("disabled", true);
	$("#btnFCSaveExit").prop("disabled", true);
	$("#btnFCFinalExit").prop("disabled", true);
	$('#spanAutoSaveDisplay').html('&nbsp;&nbsp; This session has timed-out.');
}

// -----------------------------------------------------------------
// Login Methods
//-----------------------------------------------------------------
onLoginResponseFC = function(success, userInfo) {
	if (success) {
		_oUserInfoFC = userInfo; // CJ-311
		
		if (userInfo.canAccessAdminOptions() == true) {
			$('#divContainer').removeClass('containerMarginTopNormal').addClass('containerMarginTopAdmin');
		}
		if (userInfo.isToken == true) {
			_isFcToken = true;
			_docId = userInfo.documentId;
		}
		
		if (_interview == null)	
			_interview = new Interview();
		_interview.retrieve(_docId, onFCLoaded, userInfo);

	} else
		goHome();
}

$('#pleaseWaitDialogFC').modal();

_oUserInfoFC.getLogon(true, onLoginResponseFC, null, _theFcToken, 'divSessionTimeout'); // CJ-634 added 'divSessionTimeout' parameter

//-----------------------------------------------------------------
//Page Setup Methods
//-----------------------------------------------------------------

function onFCLoaded(pInterview, isSuccess, pUpgradeClsVersionData) {
	if (!pInterview.ready) {
		alert('Not ready');
		return;
	}
	_interview = pInterview;		// CJ-311
	
	// load header
	var oDoc = pInterview.doc;
	if (oDoc.id) {
		$('#acquisition_title').html(oDoc.title);
		$('#document_number').html(oDoc.number);
		$('#spanDocType').html(getDocumentTypeName(oDoc.type) + (oDoc.isLinkedAward ? " (Linked)" : ""));
	
		$('#spanUpdatedAt').html(longToDateTimeStr(oDoc.updatedAt, _oUserInfoFC.user_timezone_offset));
		
		// CJ-477, Add FAC and DAC number to banner.
		var spanMsg = 'A complete and accurate set of recommended clauses will not be available until all questions in all applicable sections have been answered and the session has been validated.';
		
		if ((pInterview.fac_number != null) && (pInterview.dac_number != null) && (pInterview.dfars_number != null))
		{
			var additionalMsg = 'CLS has been updated to include changes through FAC ' + pInterview.fac_number +  (pInterview.fac_date ? ' (' + pInterview.fac_date + ')' : '') +
			', DAC ' + pInterview.dac_number +  (pInterview.dac_date ? ' (' + pInterview.dac_date + ')' : '') +
			', and DFARS ' + pInterview.dfars_number + (pInterview.dfars_date ? ' (' + pInterview.dfars_date + ')' : '') + '.';
			//var additionalMsg = 'CLS has been updated to include changes through FAC ' + pInterview.fac_number + ', DAC ' + pInterview.dac_number + ', and DFARS ' + pInterview.dfars_number + '.';
			if (oDoc.type != 'O')
			{
				spanMsg = additionalMsg + ' ' + spanMsg;
			}
			else
			{
				spanMsg = additionalMsg;
			}
		}
		$('#spanFixedMessage').html(spanMsg);
		
	}
	// Need to know if this is an order before we load the page.
	if (oDoc.type == "O") 
		_isOrderDoc = true;
	
	_pageManager.loadPage();
	//_pageManager.retrieve(_docId);	// load selected tab
	
	$('#tableMain').show();
	
	if (oDoc.type == "O") {
		
		$("#btnFCFinalExit").show();
		$('#btnFCSaveExit').hide();
		$('#btnFCCancel').hide();
	}
	else {
		$('#btnFCFinalExit').hide();
	}
		
	$('#pleaseWaitDialogFC').modal('hide');
	
	// Does not look like server logic supports Orders, specifically Clauses, for version upgrade.
	if (pUpgradeClsVersionData) {
		$("#bnFCVerChangesUpdate").click( function() {
			upgradeDocVersion(_docId, oDoc.type, 'verFCChangesModal', onClsVersionUpgraded);
		});
				 
		showVersionChangesDialog(pUpgradeClsVersionData, _docId, 
				'verFCChangesModal', 'verFCChangesModalLabel', 'verFCChangesBody', 
				'bnFCVerChangesCancel', 'bnFCVerChangesUpdate', false, oDoc.type);
		return;
			
	}
	//CJ-1356
	//Initiates Auto-Save
	this.autoSavingInterval = window.setTimeout(function() {this.autoSaveTimeout()}, 600000); //10 min
}

onClsVersionUpgraded = function() {
	showLoadingProgress(0);
	$('#pleaseWaitDialogFC').modal('show');
	_interview.retrieve(_docId, onFCLoaded, _oUserInfoFC);
}

//-----------------------------------
//Auto-Save CJ - 1356
//----------------------------------
this.autoSaveTimeout = function autoSaveTimeout() {
	if (this.autoSavingInterval != null) {
			window.clearTimeout(this.autoSavingInterval);
			this.autoSavingInterval = null;
			logOnConsole('clearAutoSaveTimeoutInstance()');
			this.autoSavingInterval = window.setTimeout(function() {this.autoSaveTimeout()}, 600000); //10 min
			
			//this.unsavedChanges == true &&
			if (this.pendingSave == false)
			{
				this.onSaveChanges(4);	
			}
	}
}


//-----------------------------------------------------------------
//Page Sort Methods
//-----------------------------------------------------------------


toggleSortIcons = function(prefixId, pIndex, sortAscencing) {
	var oSortIcons = $("[id^='" + prefixId + "']");
	for (var iIcon = 0; iIcon < oSortIcons.length; iIcon++) {
		var oIcon = oSortIcons[iIcon];
		$(oIcon).html(''); // remove current symbol

		if (iIcon == pIndex) {
			//$(oIcon).html(sortAscencing ? '&#8711;' : '&#8710;'); // Asc or Desc Arrows
			$(oIcon).html(sortAscencing ? '<img src="../assets/images/sort_asc.png" alt="sort ascending" />' : '<img src="../assets/images/sort_desc.png" alt="sort cending" />'); // Asc or Desc Arrows
		} 
	}
}

//-----------------------------------------------------------------
// Sorting Events
//-----------------------------------------------------------------
onActvClausesSortClick = function(field, pIndex) {
	if (_pageManager.selectedClauseSet.sortIndex == pIndex)
		_pageManager.selectedClauseSet.sortAsc = !_pageManager.selectedClauseSet.sortAsc;
	else {
		_pageManager.selectedClauseSet.sortIndex = pIndex;
		_pageManager.selectedClauseSet.sortAsc = true;
	}
	toggleSortIcons('spanACSC', pIndex, _pageManager.selectedClauseSet.sortAsc);

	_pageManager.selectedClauseSet.loadClauseTable(); // loadSelectedClauseTable();
	//var htmlRows = _pageManager.selectedClauseSet.genHtmlRows(_filterClauseData);
	//$("#tableSelectedClauses tbody").html(htmlRows);
}

onAddClausesSortClick = function(field, pIndex) {

	if (_pageManager.addClauseSet.sortIndex == pIndex)
		_pageManager.addClauseSet.sortAsc = !_pageManager.addClauseSet.sortAsc;
	else {
		_pageManager.addClauseSet.sortIndex = pIndex;
		_pageManager.addClauseSet.sortAsc = true;
	}
	toggleSortIcons('spanICSC', pIndex, _pageManager.addClauseSet.sortAsc);

	_pageManager.addClauseSet.loadClauseTable();
	
}

onRemoveClausesSortClick = function(field, pIndex) {
	if (_pageManager.removeClauseSet.sortIndex == pIndex)
		_pageManager.removeClauseSet.sortAsc = !_pageManager.removeClauseSet.sortAsc;
	else {
		_pageManager.removeClauseSet.sortIndex = pIndex;
		_pageManager.removeClauseSet.sortAsc = true;
	}
	toggleSortIcons('spanRCSC', pIndex, _pageManager.removeClauseSet.sortAsc);

	_pageManager.removeClauseSet.loadClauseTable();
}

onAddClausesSearchSortClick = function(field, pIndex) {
	_pageManager.addClauseSet.sortIndex = pIndex;
	_pageManager.addClauseSet.sortAsc = true;
	
	toggleSortIcons('spanICSC', pIndex, _pageManager.addClauseSet.sortAsc);

	_pageManager.addClauseSet.loadClauseTable();
}

//-----------------------------------------------------------------
// Page Button Methods
//-----------------------------------------------------------------

$("#btnFCCancel").click(function() {
	onSaveChanges(0);
});

$("#btnFCSaveContinue").click(function() {
	onSaveChanges(1);
});

$("#btnFCSaveExit").click(function() {
	onSaveChanges(2);
});

$("#btnFCFinalExit").click(function() {
	onSaveChanges(3);
});

onSaveChanges = function(iMode) {	
pendingSave=true;//CJ-1356	

	window.onbeforeunload = null;		// Prevents the 'session has been modified' message. 
	if (iMode == 0){
		//location = 'interview.jsp?doc_id=' + _docId;
		location = 'interview.jsp?' + (_isFcToken ? 'token=' + _theFcToken : 'doc_id=' + _docId);
		return;
	}
	if (iMode == 3)	{ // Orders, Finalize and Exit
		if (!this._pageManager.validateOrders())
			return;
	}
	
	//_oFinalCheckSet = null;
	
	var pMode = iMode;
	var instance = this;
	var jsonDoc = this._pageManager.createJson();
	var oData = {
		id: _docId,
		mode: iMode,
		data: JSON.stringify(jsonDoc)
	};
	
	if(iMode == 1 || iMode == 4)//CJ-1409
	$('#saveWaitDialog').modal('show');
	
	$.ajax({
		url: getAppPath() + 'page?do=finalclauses-save&id=' + _docId,
		type: 'POST',
		data: oData,
		cache: false,
		dataType: 'JSON',
		success: function(data) {
			$('#pleaseWaitDialogFC').modal('hide');
			$('#saveWaitDialog').modal('hide'); //CJ-1409
			if ('success' == data.status) {
				_oUserInfoFC.loadSessionJson(data, true); // CJ-573
				// CJ-361
				if (data.data.updated_at)
					$('#spanUpdatedAt').html(longToDateTimeStr(data.data.updated_at, _oUserInfoFC.user_timezone_offset));
				if (iMode == 1){
					/* User clicks Save
					   Logic: The pending save is true as the user wants to save a document
					          UnsavedChange is false as the user is about to save a document
					*/
					alert(data.message);
				}
				else if (iMode == 2){
					//location = 'interview.jsp?doc_id=' + _docId + (_isFcToken ? '&token=' + _theFcToken : '');
					location = 'interview.jsp?' + (_isFcToken ? 'token=' + _theFcToken : 'doc_id=' + _docId);
				}
				else if (iMode == 3){ // Finalize and Exit to the Dashboard.
					if (_isFcToken)
						alert('The document has been finalized. Please close this window.');
					else
						location = 'dashboard.jsp';
				}
				//CJ-1356
				else if(iMode == 4){
					
					/*
					 Logic:  If the user has made changes and a save is not pending
					 		Then continue with the autoSave
					 */
					alert(data.message);
					/* Text Document Auto-Save, appears once 
					automatic save occurs, text disappears in 5 seconds */
					$('#spanAutoSaveDisplay').html('&nbsp;&nbsp; Document Auto-Saved'); 
					setTimeout(function() {   
						  $("#spanAutoSaveDisplay").fadeOut();
						}, 5000);
					document.getElementById("spanAutoSaveDisplay").style.display='';
					
					
				}
				//unsavedChanges=false;
				pendingSave=false;
				
				
			}
			
			 else {
				if (message == 'no session') { // CJ-573
					displaySessonEnded(true);
				} else
					alert(data.message);
			}
			
			
		},
		
		
		error: function(object, text, error) {
			$('#pleaseWaitDialogFC').modal('hide');
			$('#saveWaitDialog').modal('hide'); //CJ-1409
			// CJ-1382, reset flags (todo) and show server error dialog
			myAlert("Connection Lost!", "Please check your network connection and try again.");
			
			//goHome();
		}
	});

	
};

onAddClauseClick = function(poCheckBox, clauseId) {
	
	//this.unsavedChanges = true; //CJ-1356
	
	_pageManager.onToggleClause(clauseId, poCheckBox.checked);
	_oUserInfoFC.timelyRefreshTimeout(); // CJ-696
}

onRemoveClauseClick = function(clauseId, poLink) {	
	
	//this.unsavedChanges = true; //CJ-1356
	
	_pageManager.onRemoveClause(clauseId, poLink);
	_oUserInfoFC.timelyRefreshTimeout(); // CJ-696
}

$("#btnFCSearchClear").click(function() {
	$('#query').val("");
	//_pageManager.showSearchClauses ("", true);
	_pageManager.showAllClauses();
	_oUserInfoFC.timelyRefreshTimeout(); // CJ-696
});

onSearchClickFC = function() {
	var param = $('#query').val();
	if (param == null)
		param = "";
	if (param == "")
		return;
	
	onSearch(param);
	_oUserInfoFC.timelyRefreshTimeout(); // CJ-696
}

onSearch = function(sKeyword) {
	var instance = this;
	var jsonDoc = this._pageManager.createJson();
	var oData = {
		id: _docId,
		searchStr: sKeyword,
		data: JSON.stringify(jsonDoc)
	};
	$.ajax({
		url: getAppPath() + 'page?do=finalclauses-keyword',
		type: 'POST',
		data: oData,
		cache: false,
		dataType: 'JSON',
		success: function(data) {
			if ('success' == data.status) {
				_pageManager.showSearchClauses (data.data.clauseItems);
			} else {
				if (message == 'no session') { // CJ-573
					displaySessonEnded(true);
				} else
					alert(data.message);
			}
		},
		error: function(object, text, error) {
			alert ("Error while searching results. Try again.");
			//goHome();
		}
	});
	
};

//-----------------------------------------------------------------
// Clause Popup Methods
//-----------------------------------------------------------------
showClauseContent_FC = function(oClause, edit) {
	// Don't allow edit, if the clause has not been added
	var tempClause = _pageManager.selectedClauseSet.findById(oClause.id);
	if (tempClause) {
		if (!oClause.hasFillin())
			edit = false;
	}
	else
		edit = false;
	
	var sCotent = oClause.getContent(edit);
	$('#fcModalLabel').html(oClause.getModalTitle());
	$('#fcModalBody').html(sCotent);
	if (edit) {
		$('#fcModelSave').show();
		$('#fcModelComplete').show();
	}
	else {
		$('#fcModelSave').hide();
		$('#fcModelComplete').hide();
	}
	$('#fcModal').modal();
}

popClause_FC = function(clauseId, pbHasFillin, pbActiveTab) {
	var edit = pbActiveTab;
	//if (_oFinalCheckSet != null) {
	//	$('#' + _completeModal).modal('toggle');
	//}
	var oClause = _pageManager.selectedClauseSet.findById(clauseId);
	if (oClause == null)
		oClause = _pageManager.removeClauseSet.findById(clauseId);
	if (oClause == null)
		oClause = _pageManager.addClauseSet.findById(clauseId);
	
	_clause = oClause;
	if (oClause.content == null) {
		$.ajax({
			url: getAppPath() + 'page?do=doc-clause-content&clause_id=' + clauseId,
			cache: false,
			dataType: 'json',
			success: function(data) {
				try {
					if ('success' == data.status) {
						oClause.content = data.data.clause_data;
						_oUserInfoFC.loadSessionJson(data, true); // CJ-573
						showClauseContent_FC(oClause, edit);
					} else if ('no session' == data.message) {  
						displaySessonEnded(true); // CJ-573
						//alert('You have been logged out for being inactivity, please refresh the page and log back into CLS to continue...');
						//goHome();	
					} else
						alert('Unable to retrieve clause data.')
				}
				catch (oError) {
					alert("JavaScript Error: " + oError);
				}
			},
			error: function(object, text, error) {
				logAjaxErrorOnConsole(object, text, error);
				alert("AJAX Error: " + object.responseText);
			}
		});
	} else {
		showClauseContent_FC(oClause, edit);
		_oUserInfoFC.timelyRefreshTimeout(); // CJ-696
	}
}

$("#fcModelClose").click(function() {
	$('#fcModal').modal('hide');
});

$("#fcModelSave").click(function() {
	onClauseSave_FC(false);
});

$("#fcModelComplete").click(function() {
	onClauseSave_FC(true);
});

onClauseSave_FC = function(isCompleted) {
	_pageManager.onClauseSave (_clause, isCompleted);
	$('#fcModal').modal('hide');
	_oUserInfoFC.timelyRefreshTimeout(); // CJ-696
}

//-----------------------------------------------------------------
//Clause Editable Methods
//-----------------------------------------------------------------
showEditContent_FC = function(oClause, edit) {	
	// Don't allow edit, if the clause has not been added
	var tempClause = _pageManager.selectedClauseSet.findById(oClause.id);
	if (!tempClause) {
		edit = false;
	}
	
	var sRemarks = (oClause.editableRemarks ? oClause.editableRemarks : '');
	
	sRemarks = sRemarks.replace ('PARENT', '');	// CJ-561
	if (sRemarks == 'ALL')
		sRemarks = '';
	$('#fcModelRemarks').html(sRemarks);

	if (((oClause.editableRemarks == 'ALL') || (oClause.editableRemarks.indexOf ('ALL') == 0))
			&& (oClause.pClauseFillInSet.elements.length > 1))
		$('#fcEditModalBanner').show();
	else
		$('#fcEditModalBanner').hide();
	
	var sContent = oClause.getEditContent();

	$('#fcEditModalLabel').html(oClause.getModalTitle());
	$('#fcEditModalBody').html(sContent);

	// CJ-583, enable all controls in the edit window.
	//$( ".editinput-class" ).prop( "disabled", false ); //Disable
	
	$('#fcEditModelSave').show();
	$('#fcEditModelClose').show();

	//$('#fcEditModal').show();	// CJ-556
	$('#fcEditModal').modal('show');
}

popEditClause_FC = function(clauseId, pbActiveTab) {
	var edit = pbActiveTab;

	var oClause = _pageManager.selectedClauseSet.findById(clauseId);
	if (oClause == null)
		oClause = _pageManager.removeClauseSet.findById(clauseId);
	if (oClause == null)
		oClause = _pageManager.addClauseSet.findById(clauseId);
	
	_clause = oClause;
	if (oClause.content == null) {
		$.ajax({
			url: getAppPath() + 'page?do=doc-clause-content&clause_id=' + clauseId,
			cache: false,
			dataType: 'json',
			success: function(data) {
				try {
					if ('success' == data.status) {
						oClause.content = data.data.clause_data;
						_oUserInfoFC.loadSessionJson(data, true); // CJ-573
						showEditContent_FC(oClause, false);
					} else if ('no session' == data.message) {  
						displaySessonEnded(true); // CJ-573
					} else
						alert('Unable to retrieve clause data.')
				}
				catch (oError) {
					alert("JavaScript Error: " + oError);
				}
			},
			error: function(object, text, error) {
				logAjaxErrorOnConsole(object, text, error);
				alert("AJAX Error: " + object.responseText);
			}
		});
	} else {
		showEditContent_FC(oClause, false);
		_oUserInfoFC.timelyRefreshTimeout(); // CJ-696
	}
}

onEditClose_FC = function() {
	$('#fcEditModal').modal('hide');
	_oUserInfoFC.timelyRefreshTimeout(); // CJ-696
}

$("#fcEditModelClose").click(function() {
	$('#fcEditModal').modal('hide');
	_oUserInfoFC.timelyRefreshTimeout(); // CJ-696
});

$("#fcEditModelSave").click(function() {
	var oSuccessMessage = _pageManager.onEditClauseSave (_clause);
	if (oSuccessMessage == '0')
		$('#fcEditModal').modal('hide');
	else
		alert (oSuccessMessage);
	_oUserInfoFC.timelyRefreshTimeout(); // CJ-696
});

//-----------------------------------------------------------------
// FinalClauseSet Methods
//-----------------------------------------------------------------
function FinalClausePageManager() {
	
	this.selectedClauseSet = new FinalClauseSet(TAB_ENUM.selected);
	this.addClauseSet = new FinalClauseSet(TAB_ENUM.add);
	this.removeClauseSet = new FinalClauseSet(TAB_ENUM.removed);

	this.loadPage = function loadPage(){
		
		_pageManager = this;	// CJ-311
		
		for (var iData = 0; iData < _interview.clauses.elements.length; iData++) {
			var oClauseCopy = new FinalClauseItem();
			oClauseCopy.copyClauseItem (_interview.clauses.elements[iData]);
			
			var iPos = this.addClauseSet.findIndexById(oClauseCopy.id);
			if (iPos < 0)	// no duplicates.
				if (oClauseCopy.isRestrictedForAward == false) // CJ-606
					this.addClauseSet.elements.push(oClauseCopy);
			
			if (oClauseCopy.isDocumentClause == true) {
				if (oClauseCopy.isRemoved() == true) { 
					iPos = this.removeClauseSet.findIndexById(oClauseCopy.id);
					if (iPos < 0)
						this.removeClauseSet.elements.push(oClauseCopy);
				}
				else {
					
					iPos = this.selectedClauseSet.findIndexById(oClauseCopy.id);
					if (iPos < 0)
						this.selectedClauseSet.elements.push(oClauseCopy);
				}
			}
		}
		
		// redisplay the data
		_pageManager.removeClauseSet.loadClauseTable();
		_pageManager.addClauseSet.loadClauseTable();
		_pageManager.selectedClauseSet.loadClauseTable();
		
		$('#pageButtons').show();
	}
	
	this.createJson = function createJson() {
	    var oDocFillInSet = new DocFillInSet();
	    var oDocClauseSet = new DocClauseSet();
	    
	    this.selectedClauseSet.saveAnswers (oDocClauseSet, oDocFillInSet) ;
	    this.removeClauseSet.saveAnswers (oDocClauseSet, oDocFillInSet) ;
	    
	    var result = {
			"document_id": this.id,
			"Document_Clauses": oDocClauseSet.createJson(),
			"Document_Fill_Ins": oDocFillInSet.createJson()
	    }
		return result;
	};
	
	// This method only applies to selected clause set.
	this.onToggleClause = function onToggleClause(clauseId, bAddItem){
		
		var iAddPos = this.addClauseSet.findIndexById(clauseId);
		if (iAddPos < 0)
			return;

		if ((this.addClauseSet.elements[iAddPos].isDocumentClause == true) && (!this.addClauseSet.elements[iAddPos].isRemoved())) 
			bAddItem = false;
		else
			bAddItem = true;
		
		if (bAddItem) {
			
			this.addClauseSet.elements[iAddPos].optionalUserActionCode = 'A';
						
			// Copy to the Active clause tab.
			var oClauseCopy = this.addClauseSet.findById(clauseId);
			oClauseCopy.isDocumentClause = true;	
			this.selectedClauseSet.elements.push(oClauseCopy);

			// remove from Remove tab, if necessary
			var iPos = this.removeClauseSet.findIndexById(clauseId);
			if (iPos >= 0) {
				if (this.removeClauseSet.elements.length == 1)
					this.removeClauseSet.elements.length = 0;
				else
					this.removeClauseSet.elements.splice(iPos, 1);
			}
			
			// update the item in the table
			//var htmlRow = this.addClauseSet.genAddRow(this.addClauseSet.elements[iAddPos]);       
			//$("#dtr-" + this.addClauseSet.elements[iAddPos].id).html(htmlRow);
		}
		else {
			this.addClauseSet.elements[iAddPos].optionalUserActionCode = 'R';
			
			// Copy to the Remove clause tab.
			var oClauseCopy = this.addClauseSet.findById(clauseId);
			oClauseCopy.isDocumentClause = true;	
			this.removeClauseSet.elements.push(oClauseCopy);
			
			// remove from Active tab
			var iPos = this.selectedClauseSet.findIndexById(clauseId);
			if (iPos >= 0) {
				this.selectedClauseSet.elements.splice(iPos, 1);
			}
			
			// update the item in the table
			//var htmlRow = this.addClauseSet.genAddRow(this.addClauseSet.elements[iAddPos]); 
			//$("#dtr-" + this.addClauseSet.elements[iAddPos].id).html(htmlRow);
		}
		// update the item in the table
		
		$('#selMarker-' + clauseId).html(genSelectClauseBtnContent(bAddItem)).attr("aria-label", (bAddItem ? "Clause Selected" : "Select Clause")  );
		//_pageManager.addClauseSet.displayTable.row('.selected').invalidate().draw( true );
		
		// redisplay the data
		_pageManager.removeClauseSet.loadClauseTable();
		
		// Update the Add Tab row.
		//$("#tableAddClauses tr[id=tr-" + clauseId + "]").find("#notes").html('Added By User');
		//$('#ck-' + clauseId).attr('checked', false); // Unchecks it
		
		_pageManager.selectedClauseSet.loadClauseTable();
	};
	

	
	
	this.onRemoveClause = function onRemoveClause(clauseId, poLink){
		
		var oClause = this.selectedClauseSet.findById(clauseId);
		if (oClause == null)
			return;
		
		// copy to remove tab
		oClause.optionalUserActionCode = 'R';
		this.removeClauseSet.elements.push(oClause);
		
		// remove from selected tab
		var iPos = this.selectedClauseSet.findIndexById(clauseId);
		if (iPos >= 0)
			this.selectedClauseSet.elements.splice(iPos, 1);
		
		// update the Add clause tab.
		iPos = this.addClauseSet.findIndexById(clauseId);
		if (iPos >= 0)
			this.addClauseSet.elements[iPos].optionalUserActionCode = 'R';
			
		// redisplay the data
		_pageManager.removeClauseSet.loadClauseTable();
		_pageManager.addClauseSet.loadClauseTable();
		
		// Update the Add Tab row to reflect the removed record.
		var oTr = $(poLink).closest('tr');
		_pageManager.selectedClauseSet.displayTable.$('tr.selected').removeClass('selected');
		$(oTr).addClass('selected');
		
		var oSelected = _pageManager.selectedClauseSet.displayTable.row('.selected');
		if (oSelected && (oSelected.length > 0)) {
			oSelected.remove().draw( true );
		} else {
			alert('Unable to locate the record.')
		}
		_prevSelRowOnSelectedTable = null;
		_pageManager.selectedClauseSet.displayTable.$('tr.selected').removeClass('selected');
		
		/*
		var htmlRow = this.addClauseSet.genAddRow(this.addClauseSet.elements[iPos]);       
		$("#dtr-" + this.addClauseSet.elements[iPos].id).html(htmlRow);
		_pageManager.selectedClauseSet.loadClauseTable();
		*/
	};
	
	// This method only applies to selected clause set.
	// CJ-625, Clear search string
	this.showAllClauses = function showAllClauses(){
		var oAddClauseItem;
		for (var iData = 0; iData < this.addClauseSet.elements.length; iData++) {
			oAddClauseItem = this.addClauseSet.elements[iData];
			this.addClauseSet.elements[iData].showSearchItem = true;
		}
		
		_pageManager.addClauseSet.loadClauseTable();
		//var htmlRows = this.addClauseSet.genHtmlRows();
		//$("#divTableAddClauses").html(htmlRows);
	};
	
	// This method only applies to selected clause set.
	// CJ-625, Db call to compare string against clause name, title, and text.
	this.showSearchClauses = function showSearchClauses(applicableClauses) {
		for (var iData = 0; iData < this.addClauseSet.elements.length; iData++) {
			var oAddClauseItem = this.addClauseSet.elements[iData];
			this.addClauseSet.elements[iData].showSearchItem = false;
		}
		
		for (var iResults = 0; iResults < applicableClauses.length; iResults++) {
			for (var iData = 0; iData < this.addClauseSet.elements.length; iData++) {
				var oAddClauseItem = this.addClauseSet.elements[iData];
				
				if (oAddClauseItem.clause_name == applicableClauses[iResults].name)
					this.addClauseSet.elements[iData].showSearchItem = true;
			}		
		}
		
		_pageManager.addClauseSet.loadClauseTable();
		//var htmlRows = this.addClauseSet.genHtmlRows();
		//$("#divTableAddClauses").html(htmlRows);
	};
	
	// An update to a clause on one tab, should be reflected in all tabs.
	this.onClauseSave = function onClauseSave(selClause, isCompleted){		
		
		var oClause = _pageManager.selectedClauseSet.findById(selClause.id);
		if (oClause != null) {
			oClause.onClauseSave(selClause, isCompleted);
			
			// CJ-1142
			if (isCompleted) {
				oClause.isCompleted = oClause.pClauseFillInSet.anyFillinsAnswered();
				oClause.is_fillin_completed = oClause.isCompleted;	
			}
			
			var sClass = oClause.genHtmlBtnStyle(true);
			var oLink = $('#updFillin-' + selClause.id);
			oLink.removeClass('btn-danger');
			oLink.removeClass('btn-success');
			oLink.addClass(sClass);

			_pageManager.selectedClauseSet.displayTable.row('.selected').invalidate().draw( false );
			//_pageManager.selectedClauseSet.loadClauseTable();
		}
		
		oClause = _pageManager.removeClauseSet.findById(selClause.id);
		if (oClause != null) {
			oClause.onClauseSave(selClause, isCompleted);
			_pageManager.removeClauseSet.loadClauseTable();
		}
	}
	
	// CJ-408
	// An update to a clause on one tab, should be reflected in all tabs.
	this.onEditClauseSave = function onEditClauseSave(selClause, isCompleted) {
		var bResult = true;
		
		var oClause = _pageManager.selectedClauseSet.findById(selClause.id);
		if (oClause != null) {
			bResult = oClause.onEditClauseSave(selClause, isCompleted);
			//var htmlRows = this.selectedClauseSet.genHtmlRows();
			//$("#tableActiveClauses tbody").html(htmlRows);
			
			
			var iPos = this.selectedClauseSet.findIndexById(oClause.id);
			var htmlRow = this.selectedClauseSet.elements[iPos].genEditableLink(TAB_ENUM.selected);       
			//$("#edrow-" + this.selectedClauseSet.elements[iPos].id).html(htmlRow);
			$("#el-" + this.selectedClauseSet.elements[iPos].id).html(htmlRow);
			 

		}
		return bResult;
	}
	
	this.validateOrders = function validateOrders(){
		var pProgress = new Progress();
		
		if (this.selectedClauseSet.elements.length <= 0) {
			alert('No clauses selected')
			return false;
		}
		
		this.selectedClauseSet.getProgress (pProgress);
		
		var msg = "";
		// CJ-1171, Don't show message for no counts.
		if (pProgress.isClauseCompleted() == false) {
			msg = pProgress.getClauseInCompleteMessage() + '\n';
			msg += 'Are you sure you want to complete this document without updating fill-ins?';
		
			var r = confirm(msg);
			if (r != true) {
				return false;
			}
		}
		
		return true;
	}
}

//-----------------------------------------------------------------
function onSelRowOnSelectedDataTable() {
	$('#tableSelectedClauses tbody').on( 'click', 'tr', function () {
		 if ( $(this).hasClass('selected') ) {
			 if (_prevSelRowOnSelectedTable != this) {
				 _prevSelRowOnSelectedTable = null;
				 $(this).removeClass('selected');
			 }
		 }
		 else {
			 _pageManager.selectedClauseSet.displayTable.$('tr.selected').removeClass('selected');
			 $(this).addClass('selected');
			 _prevSelRowOnSelectedTable = this;
		 }
	} );
}

function onSelRowOnAddDataTable() {
	$('#tableAddClauses tbody').on( 'click', 'tr', function () {
		 if ( $(this).hasClass('selected') ) {
			 if (_prevSelRowOnAddTable != this) {
				 _prevSelRowOnAddTable = null;
				 $(this).removeClass('selected');
			 }
		 }
		 else {
			 _pageManager.addClauseSet.displayTable.$('tr.selected').removeClass('selected');
			 $(this).addClass('selected');
			 _prevSelRowOnAddTable = this;
		 }
	} );
}

function onSelRowOnRemovedDataTable() {
	$('#tableRemoveClauses tbody').on( 'click', 'tr', function () {
		 if ( $(this).hasClass('selected') ) {
			 $(this).removeClass('selected');
		 }
		 else {
			 _pageManager.removeClauseSet.displayTable.$('tr.selected').removeClass('selected');
			 $(this).addClass('selected');
		 }
	} );
}

/*
function formatCell(data) {
	return data;
	// return '<div class="maxHeightCell">' + data + '</div>';
}
*/

//-----------------------------------------------------------------
function FinalClauseSet(pActiveTab) {
	this.elements = new Array();
	
	this.sortIndex = 0;
	this.sortAsc = true;
	this.tabId = pActiveTab; // TAB_ENUM.selected;
	
	this.needToDisplay = true;
	this.displayTable = null;
	this.displayClauses = [];
	
	this.loadJson = function loadJson(json) {
		this.elements.length = 0;
		for (var iData = 0; iData < json.length; iData++) {
			var oClauseItem = new FinalClauseItem();
			
			if (this.tabId == TAB_ENUM.selected) {	// Active Clauses
				if (json[iData].optional_user_action_code == null || json[iData].optional_user_action_code == 'A') {
					oClauseItem.loadJson(json[iData]);
					this.elements.push(oClauseItem);
				}
			} 
			else if (this.tabId == TAB_ENUM.add){	// Add Clauses
				oClauseItem.loadJson(json[iData]);
				this.elements.push(oClauseItem);		
			}
			else if (this.tabId == TAB_ENUM.removed){	// Removed Clauses
				if (json[iData].optional_user_action_code != null && json[iData].optional_user_action_code == 'R') {
					oClauseItem.loadJson(json[iData]);
					this.elements.push(oClauseItem);
				}			
			}
		}
		return true;
	};
	
	// CJ-852, separate counts for regular and editable fillins
	this.getProgress = function getProgress(oProgress) {
		var iTotal = 0, iAnswered = 0;
		var iRegularFillinTotal = 0; iRegularFillinAnswered = 0;
		var iEditFillinTotal = 0; iEditFillinAnswered = 0;
		var oClause, bApplicable;
		for (var item = 0; item < this.elements.length; item++) {
			oClause = this.elements[item];
			
			if (!oClause.hasEntry())
				continue;			
			
			var bHasRegFillin = oClause.hasRegularFillin();
			var bAnsRegFillin = bHasRegFillin && oClause.is_fillin_completed;
			
			var bHasEditFillin = oClause.is_editable && oClause.hasEditFillin();
			var bAnsEditFillin = bHasEditFillin && oClause.isEditFillinCompleted();
			
			var bAnswered = (!bHasRegFillin || bAnsRegFillin)
							&& (!bHasEditFillin || bAnsEditFillin);
			
			if (bHasRegFillin) 
				iRegularFillinTotal++;
			if (bHasEditFillin)
				iEditFillinTotal++;
			if (bAnsRegFillin)
				iRegularFillinAnswered++;
			if (bAnsEditFillin)
				iEditFillinAnswered++;
			
			iTotal++;
			if (bAnswered)
				iAnswered++;
		}
		oProgress.totalClausesForInput = iTotal;
		oProgress.answeredClausesForInput = iAnswered;
		
		oProgress.totalFillInClauses = iRegularFillinTotal;
		oProgress.answeredFillinClauses = iRegularFillinAnswered;
		
		oProgress.totalEditFillInClauses = iEditFillinTotal;
		oProgress.answeredEditFillinClauses = iEditFillinAnswered;
	}

	// This code mimics PrescriptionChoices.saveAnswers(), 
	// to integrate with the Interview-Items classes.
	this.saveAnswers = function saveAnswers(oDocClauseSet, oDocFillInSet) {
		for (var iEle = 0; iEle < this.elements.length; iEle++) {
			var oClauseItem = this.elements[iEle];
			
			var oDocClauseItem = new DocClauseItem();
			oDocClauseItem.clauseId = oClauseItem.id;
			oDocClauseItem.isCompleted = (oClauseItem.is_fillin_completed == 1) ? true : false;
			//oDocClauseItem.isRemovedByUser = oClauseItem.isRemovedByUser;
			oDocClauseItem.optionalUserActionCode = oClauseItem.optionalUserActionCode;
			
			// CJ-628, System added, required clauses cannot be removed also.
			if (!_isOrderDoc &&  (oClauseItem.optionalUserActionCode == 'R') && (oClauseItem.is_optional == false)) {  
				var bApplicable = oClauseItem.pInterviewClause.isApplicable(_interview.prescriptionChoices, true, true);
				if (bApplicable)
					oDocClauseItem.optionalUserActionCode = null;
			}
			
			oDocClauseSet.add(oDocClauseItem);
			
			oClauseItem.saveAnswers(oDocFillInSet);
		}
	};
	
	this.findById = function findById(id) {
		for (var iEle = 0; iEle < this.elements.length; iEle++) {
			var oClauseItem = this.elements[iEle];
			if (oClauseItem.id == id)
				return oClauseItem;
		}
		return null;
	};
	
	this.findIndexById = function findIndexById(id) {
		for (var iEle = 0; iEle < this.elements.length; iEle++) {
			var oClauseItem = this.elements[iEle];
			if (oClauseItem.id == id)
				return iEle;
		}
		return -1;
	};
	
	this.getClausesToDisplay = function getClausesToDisplay(piFilterClauseData) {
		var result = new Array();
		var sortedClauses = this.sortElements();
		var oRec;
		for (var iData = 0; iData < sortedClauses.length; iData++) {
			oRec = sortedClauses[iData];
			
			if ((this.tabId == TAB_ENUM.add) && (oRec.showSearchItem == false))
				continue;

			if (piFilterClauseData == FILTER_CLAUSE_DATA_ECFR_VIEW) {
				if (oRec.hasFillin())
					continue;
			} else if (piFilterClauseData == FILTER_CLAUSE_DATA_FILL_IN) {
				if (!oRec.hasFillin())
					continue;
			}

			result.push(oRec);
		}
		return result;
	}
	
	this.loadClauseTable = function loadClauseTable() {
		var measureTime = new MeasureTime(); // CJ-1170
		
		this.displayClauses = this.getClausesToDisplay((TAB_ENUM.selected == this.tabId) ? _filterClauseData : FILTER_CLAUSE_DATA_ALL);
		measureTime.log('loadClauseTable.getClausesToDisplay()');
		this.needToDisplay = true;
		if (_activeClauseTab == this.tabId) {
			measureTime.restart();
			this.displayClauseTable();
			measureTime.log('loadClauseTable.displayClauseTable()');
		}
	}
	
	this.displayClauseTable = function displayClauseTable() {
		switch (this.tabId) {
	    case TAB_ENUM.selected:
	    	this.displaySelectedClauseTable();
	        break;
	    case TAB_ENUM.add:
	    	this.displayAddClauseTable();
	        break;
	    default:
	    	this.displayRemovedClauseTable();
	    	break;
		}
	}
	
	this.displaySelectedClauseTable = function displaySelectedClauseTable() {
		var iPageLen =  10;  // CJ-1090
		var firstTime = (this.displayTable == null);
		if (!firstTime) {
			iPageLen =  this.displayTable.page.len();  // CJ-1090
			this.displayTable.destroy();
			this.displayTable = null;
		}

		if (_tableHeight == null)
			_tableHeight = getWindowHeight();
		
		this.displayTable =
			$('#tableSelectedClauses').DataTable( {
		        data: this.displayClauses,
		        'aoColumns': [
	                { 'data': 'clause_section_code', 'render': function ( data, type, row ) {
	                	return '<a href="#" data-toggle="tooltip" data-placement="right" title="' + 
	                		row.getSectionHeader() + '">Section ' + data + '</a>';
	                } },
	                { 'data': 'clause_name' },
	                { 'data': 'title' },
	                { 'data': 'prescription_name' },
	                { 'data': 'regulation_name' },
	                { 'data': 'effective_date', 'render': function ( data, type, row ) { 
	                	return longToMonthYear(data);
	                } },
	                { 'data': 'inclusion_cd', 'render': function ( data, type, row ) { 
	                	return ((data == 'R') ? 'Reference' : 'Full Text');
	                } },
	                { 'data': 'is_optional', 'sClass': 'center', 'render': function ( data, type, row ) { 
	                	return ((data == false) ? 'Yes' : 'No');
	                } }, // No
	                { 'data': 'is_editable', 'sClass': 'center', 'render': function ( data, type, row ) { 
                		return ((data == 1) ? row.genEditableLink(TAB_ENUM.selected) : 'No'); 
                	} }, // No 
	                { 'data': 'optionalUserActionCode', 'render': function ( data, type, row ) {
	            		return row.genNotes();
	                } },
	                { 'orderable': false, 'render': function ( data, type, row ) {
	                	return row.genHtmlFillinBttn(true); 
	                } },
	                { 'data': 'id', 'sClass': 'center', 'render': function ( data, type, row ) {
	                	return row.genHtmlRemoveBttn(TAB_ENUM.selected); 
	                } }
	            ],
	            //fixedHeader: true,
	            scrollY: _tableHeight - 50, // 49
		        scrollCollapse: true,
	            searching: false,
	            autoWidth: false,
	            ordering: false,
		        paging: true,
		        pageLength: iPageLen, // CJ-1090
		        deferRender: true, // CJ-1170
		        language: TABLE_LANGUAGE,
		        /* language: {
		        	"info": "_START_ to _END_ of _TOTAL_",
		        	"sInfoEmpty": "0 to 0 of 0" // CJ-1276
		        }, // CJ-1274
		        */
		        'dom': TABLE_DOM, // 'rt<"bottom"ip><"clear">'
		    } );
		 $('[data-toggle="tooltip"]').tooltip();
		 this.needToDisplay = false;
		 if (firstTime)
			 onSelRowOnSelectedDataTable();
		 $('#selItemsPerPage').removeAttr('disabled');
	}
	
	this.displayAddClauseTable = function displayAddClauseTable() {
		var iPageLen =  10;  // CJ-1090
		var firstTime = (this.displayTable == null);
		if (!firstTime) {
			iPageLen =  this.displayTable.page.len();  // CJ-1090
			this.displayTable.destroy();
			this.displayTable = null;
		}

		if (_tableHeight == null)
			_tableHeight = getWindowHeight();
		
		this.displayTable =
			$('#tableAddClauses').DataTable( {
		        data: this.displayClauses,
		        "aoColumns": [
	                { "data": "clause_section_code", "render": function ( data, type, row ) {
	                	return '<a href="#" data-toggle="tooltip" data-placement="right" title="' + 
	                		row.getSectionHeader() + '">Section ' + data + '</a>';
	                } },
	                { "sClass": "center", "render": function ( data, type, row ) { 
	                	return row.genHtmlCheckSelection(TAB_ENUM.add);
	                } },
	                { "data": "clause_name" },
	                { "data": "title" },
	                { "data": "prescription_name" },
	                { "data": "regulation_name" },
	                { "data": "effective_date", "render": function ( data, type, row ) { 
	                	return longToMonthYear(data);
	                } },
	                { "data": "inclusion_cd", "render": function ( data, type, row ) { 
	                	return ((data == "R") ? 'Reference' : 'Full Text');
	                } },
	                { "data": "is_optional", "sClass": "center", "render": function ( data, type, row ) { 
	                	return ((data == false) ? 'Yes' : 'No');
	                } }, // No
	                { "data": "is_editable", "sClass": "center", "render": function ( data, type, row ) { 
	                	return ((data == 1) ? row.genEditableLink(TAB_ENUM.add) : 'No');
	                } }, // No 
	                { "orderable": false, "render": function ( data, type, row ) {
	                	return row.genHtmlFillinBttn(false); 
	                } }
	            ],
	            //fixedHeader: true,
	            scrollY: _tableHeight - 110, // 149 // CJ-1090 86
		        scrollCollapse: true,
	            searching: false,
	            autoWidth: true,
	            ordering: false,
		        paging: true,
		        pageLength: iPageLen, // CJ-1090
		        deferRender: true, // CJ-1170
		        language: TABLE_LANGUAGE,
		        /* language: {
		        	"info": "_START_ to _END_ of _TOTAL_",
		        	"sInfoEmpty": "0 to 0 of 0" // CJ-1276
		        }, // CJ-1274
		        */
		        'dom': TABLE_DOM // 'rt<"bottom"ip><"clear">'
		    } );
		 $('[data-toggle="tooltip"]').tooltip();
		 this.needToDisplay = false;
		 if (firstTime)
			 onSelRowOnAddDataTable();
	}

	this.displayRemovedClauseTable = function displayRemovedClauseTable() {
		var iPageLen = 10;  // CJ-1090
		var firstTime = (this.displayTable == null);
		if (!firstTime) {
			iPageLen = this.displayTable.page.len();  // CJ-1090
			this.displayTable.destroy();
			this.displayTable = null;
		}

		if (_tableHeight == null)
			_tableHeight = getWindowHeight();
		
		this.displayTable =
			$('#tableRemoveClauses').DataTable( {
		        data: this.displayClauses,
		        "aoColumns": [
	                { "data": "clause_section_code", "render": function ( data, type, row ) {
	                	return '<a href="#" data-toggle="tooltip" data-placement="right" title="' +
	                		row.getSectionHeader() + '">Section ' + data + '</a>';
	                } },
	                { "data": "clause_name" },
	                { "data": "title" },
	                { "data": "prescription_name" },
	                { "data": "regulation_name" },
	                { "data": "effective_date", "render": function ( data, type, row ) { 
	                	return longToMonthYear(data);
	                } },
	                { "data": "inclusion_cd", "render": function ( data, type, row ) {
	                	return ((data == "R") ? 'Reference' : 'Full Text');
	                } },
	                { "data": "is_optional", "sClass": "center", "render": function ( data, type, row ) {
	                	return ((data == false) ? 'Yes' : 'No');
	                } }, // No
	                { "data": "is_editable", "sClass": "center", "render": function ( data, type, row ) { 
	                	return ((data == 1) ? row.genEditableLink(TAB_ENUM.removed) : 'No'); // No 
	                } },
	                { "data": "optionalUserActionCode", "render": function ( data, type, row ) {
	            		return row.genNotes();
	                } },
	                { "orderable": false, "render": function ( data, type, row ) {
	                	return row.genHtmlFillinBttn(false); 
	                } }
	            ],
	            //fixedHeader: true,
	            scrollY: _tableHeight - 50, // 49 // CJ-1090 50
		        // scroller: { rowHeight: 57 },
		        scrollCollapse: true,
	            searching: false,
	            autoWidth: false,
	            ordering: false,
		        paging: true,
		        pageLength: iPageLen, // CJ-1090
		        deferRender: true, // CJ-1170
		        language: TABLE_LANGUAGE,
		        /* language: {
		        	"info": "_START_ to _END_ of _TOTAL_",
		        	"sInfoEmpty": "0 to 0 of 0" // CJ-1276
		        }, // CJ-1274
		        */
		        'dom': TABLE_DOM // 'rt<"bottom"ip><"clear">'
		    } );
		 $('[data-toggle="tooltip"]').tooltip();
		 this.needToDisplay = false;
		 if (firstTime)
			 onSelRowOnRemovedDataTable();
	}

	this.sortElements = function sortElements() {
		var iSortCol = this.sortIndex;
		var sortedClauses = new Array();
			
		for (var index = 0; index < this.elements.length; index++) {
			var oRec = this.elements[index];
		
			var aSortedClause = new FinalClauseItem();			
			aSortedClause = oRec;

			if (sortedClauses.length == 0){
				sortedClauses.push(aSortedClause);
			}
			else{
				var oClauseSort;
				for (var sortIndex = 0; sortIndex < sortedClauses.length; sortIndex++) {
					oClauseSort = sortedClauses[sortIndex];
					if (this.sortAsc == true)	{	 // A to Z // Changed for CJ-264
						if (iSortCol == 0) {
							if (compareClauseForSort(oClauseSort, oRec))
								continue;
						}
						else if (iSortCol == 1) {
							if (oClauseSort.pv_clause_name.compare (oRec.pv_clause_name) < 0)
								continue;
						}
						else if (iSortCol == 2) {
							if (oClauseSort.title < oRec.title)
								continue;
						}
						else if (iSortCol == 3) {
							if (oClauseSort.pv_prescription_name.compare (oRec.pv_prescription_name) < 0)
								continue;
						}
						else if (iSortCol == 4) {
							if (oClauseSort.regulation_name < oRec.regulation_name)
								continue;
						}
						else if (iSortCol == 5) {
							if (oClauseSort.effective_date < oRec.effective_date)
								continue;
						}
						else if (iSortCol == 6) {
							if (oClauseSort.inclusion_cd < oRec.inclusion_cd)
								continue;
						}
						else if (iSortCol == 7) {
							if (oClauseSort.is_optional < oRec.is_optional)
								continue;
						}
						else if (iSortCol == 8) {
							if (oClauseSort.is_editable < oRec.is_editable)
								continue;
						}
						else if (iSortCol == 9) {
							if (oClauseSort.genNotes() < oRec.genNotes())
								continue;
						}
						break;
					}
					else {
						if (iSortCol == 0) {
							if (!compareClauseForSort(oClauseSort, oRec))
								continue;
						}
						else if (iSortCol == 1) {
							if (oClauseSort.pv_clause_name.compare (oRec.pv_clause_name) > 0)
								continue;
						}
						else if (iSortCol == 2) {
							if (compareStrForSort(oClauseSort.title, oRec.title) > 0)
								continue;
						}
						else if (iSortCol == 3) {
							if (oClauseSort.pv_prescription_name.compare (oRec.pv_prescription_name) > 0)
								continue;
						}
						else if (iSortCol == 4) {
							if (oClauseSort.regulation_name > oRec.regulation_name)
								continue;
						}
						else if (iSortCol == 5) {
							if (oClauseSort.effective_date > oRec.effective_date)
								continue;
						}
						else if (iSortCol == 6) {
							if (oClauseSort.inclusion_cd > oRec.inclusion_cd)
								continue;
						}
						else if (iSortCol == 7) {
							if (oClauseSort.is_optional > oRec.is_optional)
								continue;
						}
						else if (iSortCol == 8) {
							if (oClauseSort.is_editable > oRec.is_editable)
								continue;
						}
						else if (iSortCol == 9) {
							if (oClauseSort.genNotes() > oRec.genNotes())
								continue;
						}
						break;
					}
				}
				sortedClauses.splice(sortIndex, 0, aSortedClause);
			}
		}
		
		return sortedClauses;
	}
}

//-----------------------------------------------------------------
//Items per Page event
//-----------------------------------------------------------------
function selItemsPerPage_onClick(poSelect) { // CJ-1092
	var oDisplayTable = null;
	switch (_activeClauseTab) {
	case TAB_ENUM.selected:
		oDisplayTable = _pageManager.selectedClauseSet.displayTable;
		break;
	case TAB_ENUM.add:
		oDisplayTable = _pageManager.addClauseSet.displayTable;
		break;
	default:
	 	oDisplayTable = _pageManager.removeClauseSet.displayTable;
	 	break;
	}
	var iItemsPerPage = $(poSelect).val();
	if (oDisplayTable.page.len() != iItemsPerPage) {
		oDisplayTable.page.len(iItemsPerPage).draw();;
	}
}

function getClauseTypeSortIndex(clauseName) { // CJ-264
	var aParts = clauseName.split('.');
	if (aParts.length > 0) {
		if ('52' == aParts[0])
			return 0;
		if ('252' == aParts[0])
			return 1;
	}
	return 2;
}

function compareClauseForSort(oClauseSort, oRec) { // CJ-264
	if (oClauseSort.clause_section_code < oRec.clause_section_code)
		return true;
	if (oClauseSort.clause_section_code > oRec.clause_section_code)
		return false;
	
	var iClauseTypeOrder = getClauseTypeSortIndex(oClauseSort.clause_name)
	var iRecTypeOrder = getClauseTypeSortIndex(oRec.clause_name); 
	if (iClauseTypeOrder < iRecTypeOrder)
		return true;
	if (iClauseTypeOrder > iRecTypeOrder)
		return false;
	
	if (oClauseSort.inclusion_cd > oRec.inclusion_cd) // 'F'/'R'
		return true;
	if (oClauseSort.inclusion_cd < oRec.inclusion_cd) // 'F'/'R'
		return false;

	if (oClauseSort.pv_clause_name.compare (oRec.pv_clause_name) < 0)
		return true;

	return false;
}

function genSelectedGlyphIcon(selected) {
	if (selected) 
		//return '<i class="glyphicon glyphicon-check" />';
		return '<font size="4">&#9745;</font>';	// html, checked.
	else 
		//return '<i class="glyphicon glyphicon-unchecked" />';
		return '<font size="4">&#9744;</font>';	// html, unchecked.
}

function genSelectClauseBtnContent(selected)
{
	return genSelectedGlyphIcon(selected) + '<span class="hide">' + (selected ? 'Clause Selected' : 'Select Clause') + '</span>';
};


function FinalClauseItem() {
	
	this.id;
	this.clause_name;
	this.pv_clause_name = new ParagrahpValue();			// used for sorting.
	this.prescription_name;
	this.pv_prescription_name = new ParagrahpValue();	// used for sorting.

	this.clause_section_code;
	this.clause_section_header;
	this.effective_date;
	this.inclusion_cd;
	this.is_optional;  
	this.isRestrictedForAward = false; // CJ-606
	this.is_editable;
	this.is_fillin_completed;
	this.optionalUserActionCode;
	this.url;
	this.title;
	this.isBasicClause; // CJ-193 for is_basic_clause
	this.editableRemarks;
	
	this.regulation_name;
	this.regulation_title;
	this.regulation_url;
	
	this.showSearchItem = true;
	this.isDocumentClause = false;
	
	this.pClauseFillInSet = new ClauseFillInSet();	// Interview-items class.
	
	this.section_header_addtab = null;
	
	this.pInterviewClause = null;
	
	this.loadJson = function loadJson(json) {
		this.id = json.clause_id;
		this.clause_name = json.clause_name;
		this.is_fillin_completed = json.is_fillin_completed;
		this.optionalUserActionCode = json.optional_user_action_code;
		
		this.inclusion_cd = json.inclusion_cd;
		this.clause_section_code = json.clause_section_code;
		this.clause_section_header = json.clause_section_header;
		this.effective_date = json.effective_date;
		this.is_editable = json.is_editable;
		this.is_optional = json.is_optional;
		this.prescription_name = json.prescription_name;
		this.url = json.clause_url;
		this.title = json.clause_title;
		this.isBasicClause = json.is_basic_clause; // CJ-193
		this.editableRemarks = json.editable_remarks;
		
		this.regulation_name = json.regulation_name;
		this.regulation_title  = json.regulation_title;
		this.regulation_url  = json.regulation_url;
		
		this.pv_clause_name.parse (this.clause_name);		
		this.pv_prescription_name.parse (this.prescription_name);

		this.loadFillInSet (json.Clause_FillIns_Answers);
	};
	
	// Load the ClauseFillInSet, using data service calls on this page.
	this.loadFillInSet = function loadFillInSet(json){
		this.pClauseFillInSet.elements.length = 0;
		for (var iData = 0; iData < json.length; iData++) {
			var oClauseFillInItem = this.loadFillInItem(json[iData]);
			this.pClauseFillInSet.elements.push(oClauseFillInItem);
		}
	}
	
	// Load the ClauseFillInItem, using data service calls on this page.
	this.loadFillInItem = function loadFillInItem(json){
		var oClauseFillInItem = new ClauseFillInItem();
		oClauseFillInItem.id = json.clause_fill_in_id;
		oClauseFillInItem.code = json.fill_in_code;
		oClauseFillInItem.type = json.fill_in_type;
		oClauseFillInItem.maxSize = json.fill_in_max_size;
		oClauseFillInItem.groupNumber = json.fill_in_group_number;
		oClauseFillInItem.placeholder = json.fill_in_placeholder;
		oClauseFillInItem.defaultData = json.fill_in_default_data;
		oClauseFillInItem.displayRows = json.fill_in_display_rows;
		oClauseFillInItem.forTable = json.fill_in_for_table;
		oClauseFillInItem.heading = json.fill_in_heading;
		
		if (json.has_Answer == 1) {
			oClauseFillInItem.fullTtextModified = json.full_text_modified;	
			oClauseFillInItem.answer = json.document_fill_in_answer ;			
		}		
		
		return oClauseFillInItem;
	}
	
	// copy the clause items from interview, into the FinalClauseItem
	this.copyClauseItem = function copyClauseItem(oClause){
		
		this.pInterviewClause = oClause;
		
		this.id = oClause.id;
		this.clause_name = oClause.name;
		
		this.effective_date = oClause.effectiveDate;
		this.inclusion_cd = oClause.inclusion;
		this.is_optional = oClause.getClauseOption(); // optional; // CJ-476 

		this.isRestrictedForAward = oClause.isRestrictedForAward(); // CJ-606
		
		this.is_editable = oClause.editable;
		this.is_fillin_completed = oClause.isCompleted;
		this.optionalUserActionCode = oClause.optionalUserActionCode;
		this.url = oClause.url;
		this.title = oClause.title;
		this.isBasicClause = oClause.isBasicClause; // CJ-193
		this.editableRemarks = oClause.editableRemarks;
		this.showSearchItem = true;
		
		// Get Section data
		this.clause_section_code = oClause.getSectionCode();
		this.clause_section_header = "";
		var secItem = _interview.sections.findById(oClause.sectionId);
		if (secItem) {
			this.clause_section_header = secItem.header;
		}
		
		// Get Regulation data
		this.regulationId = oClause.regulationId;
		var rItem = _interview.regulations.findById(this.regulationId);
		if (rItem){
			this.regulation_name = rItem.name;
			this.regulation_title = rItem.title;
			this.regulation_url = rItem.url;
		}
		
		// Get Prescription data
		var sPrescription = "";
		for (var index = 0; index < oClause.prescriptionIds.length; index++) {
			var oRegulation = _interview.prescriptions.findById(oClause.prescriptionIds[index]);
			if (oRegulation) {
				sPrescription += (sPrescription ? '; ' : '') + oRegulation.name;
			}
		}
		this.prescription_name = sPrescription;
		
		
		// Add Fillins
		this.pClauseFillInSet.elements.length = 0;
		for (var iData = 0; iData < oClause.clauseFillIns.elements.length; iData++) {
			var oClauseFillInItem = new ClauseFillInItem();
			oClauseFillInItem = oClause.clauseFillIns.elements[iData];
			this.pClauseFillInSet.elements.push(oClauseFillInItem);
		}
		this.pClauseFillInSet.hasEditChildFillins = oClause.clauseFillIns.hasEditChildFillins; // CJ-561
		this.pClauseFillInSet.bFillinsReplaced = oClause.clauseFillIns.bFillinsReplaced;
		
		// Is this clause already included in document clause cart.
		this.isDocumentClause = (_interview.doc.clauses.findByClauseId(this.id) == null) ? false : true;
		
		// Special sorting elements
		this.pv_clause_name.parse (this.clause_name);	
		this.pv_prescription_name.parse (this.prescription_name);
		
		// CJ-598, Database is now populating this Optional field.
		// Since the database is not yet populating the Optional and Editable fields, do it here.
		//if ((this.isDocumentClause == true) && this.getOptionalCode() == 'S')
		//	this.is_optional = 0;
		//else 
		//	this.is_optional = 1;
		
		// CJ-408, Setup conditional edits.
		if (oClause.oEditCondition != null) {
			this.is_editable = oClause.oEditCondition.evaluate();
			//this.editableRemarks = "";
		}
		
		// CJ-628, System added clauses cannot be removed also.
		//if (!_isOrderDoc && (this.optionalUserActionCode != null) && (this.optionalUserActionCode == 'R') && (this.is_optional == 0)) { 
		//	this.optionalUserActionCode = null;
		//}
	}
	
	this.saveAnswers = function saveAnswers(oDocFillInSet) {
		this.pClauseFillInSet.saveAnswers(oDocFillInSet);
	};
	
	this.getOptionalCode = function getOptionalCode(){
		
		if (this.optionalUserActionCode == null)
			return "S";
		
		else 
			return this.optionalUserActionCode;
	}
	
	this.isRemoved = function isRemoved(){
		var b = false;
		
		if ((this.optionalUserActionCode != null)
		&& (this.optionalUserActionCode == 'R')) 
			b = true;
		
		return b;
	}
	
	this.hasFillin = function hasFillin() {
		return (this.pClauseFillInSet.hasFillinEntry());	// CJ-408,
		//return (this.pClauseFillInSet.elements.length > 0);
	};
	
	this.hasEntry = function hasEntry() {
		return (this.pClauseFillInSet.hasEntry());
	};
	
	this.hasRegularFillin = function hasRegularFillin() {
		return (this.pClauseFillInSet.hasFillinEntry());
	};
	
	this.hasEditFillin = function hasEditFillin() {
		return (this.pClauseFillInSet.hasEditFillinEntry());
	};
	
	this.isEditFillinCompleted = function isEditFillinCompleted() {
		return (this.pClauseFillInSet.isEditFillinCompleted());
	};
	
	this.hasFillinsRemoved = function hasFillinsRemoved() {
		return (this.pClauseFillInSet.bFillinsReplaced);
	};
	
	this.genNotes = function genNotes(){
		var note;
		if (this.optionalUserActionCode == null)
			note = 'System Added'; // 'Added by System';
		else if (this.optionalUserActionCode == 'A')
			note = 'User Added'; // 'Added by User';
		else if (this.optionalUserActionCode == 'R')
			note = 'User Removed';		
		return note;
	}
	
	this.genHtmlBtnStyle = function genHtmlBtnStyle(pbActiveTab) {
		var btnClass;
		if (this.hasFillin() && pbActiveTab) {
			if (this.hasFillinsRemoved()) {
				btnClass = 'btn-primary';
			}
			else if (this.is_fillin_completed) { 
				btnClass = 'btn-success';
			} else {
				btnClass = 'btn-danger';
			}
		} else {
			btnClass = 'btn-primary';
		}
		
		return btnClass;
	}
	
	this.getSectionHeader = function getSectionHeader() {
		return this.clause_section_header.replace ("/", " ").replace ("/", " ");
	} 
	
	// Add description to the first occurrence of a new Section
	this.genHtmlSectionHeader = function genHtmlSectionHeader(sortIndex, previousHeader) {
		
		var sSection = this.clause_section_code;
		
		if (sortIndex != 0)
			return sSection;
		
		if (previousHeader!=null && previousHeader== this.clause_section_header)
			return sSection;

		// Otherwise, append description
		var clause_section_header = this.clause_section_header;
		clause_section_header = clause_section_header.replace ("/", " ");
		clause_section_header = clause_section_header.replace ("/", " ");
		
		sSection = 
			'<b> Section ' + this.clause_section_code + '.</b><br/>'+ 
			'<span style="font-size: x-small">' + clause_section_header + '</span>';

		return sSection;
	}
	
	// On the Add tab, preselect added clauses.
	this.genHtmlCheckSelection = function genHtmlCheckSelection(activeTab) {
		
		var sSelectedCheckField = "";
		
		if (activeTab != TAB_ENUM.add)
			return sSelectedCheckField;
		
		// Don't display checkboxes for items already added by the system (Document Clause Carts).
		if ((this.isDocumentClause == true) && (!this.isRemoved()) && (this.optionalUserActionCode == null))
			return '<div id="checker-' + this.id + '"></div>'; // class="col-md-1" 
		
		// New Code
		var bChecked = (this.isDocumentClause == true) && (!this.isRemoved());
		var sCheckVal = genSelectClauseBtnContent(bChecked);
		/*
		if ((this.isDocumentClause == true) && (!this.isRemoved())) 
			sCheckVal= '<i class="glyphicon glyphicon-check" />';
		else 
			sCheckVal = '<i class="glyphicon glyphicon-unchecked" />';
		*/
			
		sSelectedCheckField = 
			//'<div id="checker-' + this.id + '" >' + // class="clearfix col-md-1"  
			'<button aria-label="' + (bChecked ? 'Clause Selected' : 'Select Clause') + '" class="btn btn-link" id="selMarker-' + this.id + '" onclick="javascript:onAddClauseClick(this, ' + this.id + ')" >'
			 + sCheckVal +  '</button>';
			
		return sSelectedCheckField;
	}
	
	this.genHtmlFillinBttn = function genHtmlFillinBttn(pbActiveTab) {
		var sBttnText; // = ((this.hasFillin() & pbActiveTab) ? 'Update Fill-ins' : 'View Content');
		if (this.hasFillin() && !this.hasFillinsRemoved()) {
			sBttnText = (pbActiveTab ? 'Update Fill-ins' : 'View Fill-in Content');
		} else
			sBttnText = 'eCFR View';  // 'View Content'; // CJ-203
		
		var sFillinBttn = 
			'<button id="updFillin-' + this.id + '" ' +
				'onclick="javascript:popClause_FC(' + this.id + ', ' + this.hasFillin() + ', ' + pbActiveTab + ')" ' +
				'class="btn btn-xs ' + this.genHtmlBtnStyle(pbActiveTab) + '">' + 
				'<span style="font-size: x-small">' + sBttnText + '</span>' +
			'</button>';
		
		return sFillinBttn;
	}
	
	// Add X mark for the optional clauses		
	this.genHtmlRemoveBttn = function genHtmlRemoveBttn(activeTab) {
		
		var sRemoveBttn = "";
		
		if (activeTab != TAB_ENUM.selected)
			return sRemoveBttn;
			
		// Required, and clause not added by user.
		if ((this.is_optional == 0) && (this.getOptionalCode() != 'A'))
			return sRemoveBttn;
		
		sRemoveBttn = 
			'<button class="btn btn-link" onclick="javascript:onRemoveClauseClick(' + this.id + ',this)" ' +
			'aria-label="Remove ' + this.clause_name + '"> '+
			//'<i class="glyphicon glyphicon-remove"></i>' +
			'<font size="4">&#10006;</font>';
			'<span class="hide">Remove ' + this.clause_name + '</span>' +
			'</button>';
		
		return sRemoveBttn;
	}
	
	// CJ-408
	this.genEditableLink = function genEditableLink(activeTab) {
		if (activeTab != TAB_ENUM.selected)
			return 'Yes';
		
		var sStyle= "";
		if (this.hasEditFillin()) {
			if (this.isEditFillinCompleted())
				sStyle = 'style="color:green" ';
			else
				sStyle = 'style="color:red" ';
		}
		
		var sEditable = 
			'<a id="el-' + this.id + '" ' + sStyle + 
			'data-toggle="tooltip" data-placement="bottom" data-container="body" title="Edit Clause Text" ' +
			'href="#" onclick="javascript:popEditClause_FC(' + this.id + ', ' + true + ')" >Yes</a>';
		return sEditable;
	}

	this.getModalTitle = function getModalTitle() {
		var title = 
			'<b>' + this.regulation_name + ' ' + this.clause_name + '</b> ' + // CJ-203
			this.title +
			// (this.isBasicClause ? "-BASIC" : "") + // CJ-193 // CJ-965 Remove Basic
			' (' + longToMonthYear(this.effective_date) + ')';
		return title;
	};
	
	this.getContent = function getContent(edit) {
		return this.pClauseFillInSet.genInputContent(this, (! edit));
	};
	
	// CJ-408
	this.getEditContent = function getEditContent() {
		var sContent = this.pClauseFillInSet.genEditContent(this);
		// var sContent = this.pClauseFillInSet.genInputContent (this, false);
		return sContent;
	};
	
	this.onClauseSave = function onClauseSave(selClause, isCompleted) {
		// CJ-844
		if (isCompleted)
			isCompleted = this.pClauseFillInSet.anyFillinsAnswered();
		
		this.isCompleted = isCompleted;
		this.is_fillin_completed = isCompleted;
		this.pClauseFillInSet.onClauseSave(isCompleted, selClause, 1);
		// CJ-583, debug only
		if (this.pClauseFillInSet.elements[0]) { 
			logOnConsole ("onClauseSave");
			logOnConsole	(this.pClauseFillInSet.elements[0].answer ? this.pClauseFillInSet.elements[0].answer : 
				this.pClauseFillInSet.elements[0].default_data);
		}
		this.updateCompleteColor();
	};
	
	// CJ-408
	this.onEditClauseSave = function onEditClauseSave(selClause, isCompleted) {
		
		var oSuccessMessage = this.pClauseFillInSet.validateContent();
		
		if (oSuccessMessage == '0') {
			if (!(typeof isCompleted === "undefined")) { // CJ-782 check if isCompleted is passed
				this.isCompleted = isCompleted;
				this.is_fillin_completed = isCompleted;
			}
			if (this.hasFillinsRemoved()) { 
				isCompleted = true;
				this.isCompleted = true;
				this.is_fillin_completed = true;
				
				var sClass = selClause.genHtmlBtnStyle(true);
				var oLink = $('#updFillin-' + selClause.id);
				oLink.html('eCFR View');
				oLink.removeClass('btn-danger');
				oLink.removeClass('btn-success');
				oLink.addClass(sClass);
				
			}
			
			this.pClauseFillInSet.onClauseSave(isCompleted, selClause, 2);
			
			// CJ-583, debug only
			if (this.pClauseFillInSet.elements[0]) { 
				logOnConsole ("onEditClauseSave");
				logOnConsole (this.pClauseFillInSet.elements[0].answer ? this.pClauseFillInSet.elements[0].answer : 
					this.pClauseFillInSet.elements[0].default_data);
			}
			
			// this.updateCompleteColor();
		}
		return oSuccessMessage;
	};
	
	this.updateCompleteColor = function updateCompleteColor() {
		var oBtn = $('#' + this.id);
		if (this.isCompleted) {
			oBtn.removeClass('btn-danger');
			oBtn.addClass('btn-success');
		} else {
			oBtn.removeClass('btn-success');
			oBtn.addClass('btn-danger');
		}
	} 
	
	// CJ-583
	this.getFullEditId = function getFullEditId() {
		var id = this.pClauseFillInSet.getFullEditId();
		return id;
	};
}

// Sorting the clause and prescriptions will require extra logic,
// since the string formats use a variable length number format.

// Ex.:
// 9.409	
// 14.201-6(b)(1)

function ParagrahpValue() {
	this.elements;
	
	// split the parts of the string into an array.
	this.parse = function parse(sValue) {
		
		// will filter the string down to just alphanumeric values, separated by periods
		var newVal = sValue.replace(/[^a-z0-9\s]/gi, '.');
		this.elements = newVal.split(".");
	}
	
	// Returns a integer indicating the relation between the strings:
	//	0	They compare equal
	// -1	The value of the first string is lower than the compared string, 
	//	1	The value of the first string is greater than the compared string
	this.compare = function (curValue){
		var oValue = this.elements;
		var pValue = curValue.elements;
		return compareStrForSort(oValue, pValue);
	}
}

function compareStrForSort(oValue, pValue) {
	var maxLength = oValue.length;
	if (pValue.length < maxLength)
		maxLength = pValue.length;
	
	if (maxLength == 0)
		return 0;
	
	var iReturn = 0;	// default, greater than
	
	for (var index = 0; index < maxLength; index++) {
		if (oValue[index] == pValue[index]){
			continue;
		}
		
		// Compare integer values
		if (isNumeric(oValue[index]) && isNumeric(pValue[index])) {
			var oNum = parseInt (oValue[index]);
			var pNum = parseInt (pValue[index]);
			
			if (oNum < pNum){
				iReturn = -1;
				break;
			}
			else if (oNum > pNum){
				iReturn = 1;
				break;
			}	
		}
		// compare string values.
		else{
			
			if (oValue[index] < pValue[index]){
				iReturn = -1;
				break;
			}
			else if (oValue[index] > pValue[index]){
				iReturn = 1;
				break;
			}	
		}
	}
	
	return iReturn;
}

onFilterClauseData = function(piMode) {
	
	//CJ-1428 Bug-Fix: This message will not allow the page to filter eCFR View and Update Fill-ins 
	//alert(data.message);
	/* Text Document Auto-Save, appears once 
	automatic save occurs, text disappears in 5 seconds */
	/*
	$('#spanAutoSaveDisplay').html('&nbsp;&nbsp; Fill In being edited'); 
	setTimeout(function() {   
		  $("#spanAutoSaveDisplay").fadeOut();
		}, 10000);
	document.getElementById("spanAutoSaveDisplay").style.display='';
	*/
	
	_filterClauseData = piMode;
	
	
	var filterClauseDataSelection = '';
	if (_filterClauseData == FILTER_CLAUSE_DATA_ECFR_VIEW) {
		filterClauseDataSelection = 'Filter eCFR';
		//filterClauseDataSelection = ': eCFR View';
	} else if (_filterClauseData == FILTER_CLAUSE_DATA_FILL_IN) {
		filterClauseDataSelection = 'Filter Fill-ins';
		//filterClauseDataSelection = ': Update Fill-ins';
	} else
		filterClauseDataSelection = 'Filter';
	$('#spanFilterClauseDataSelection').html(filterClauseDataSelection);
	$('#btnFilter').html(filterClauseDataSelection);

	_pageManager.selectedClauseSet.loadClauseTable(); // loadSelectedClauseTable();
	//var htmlRows = _pageManager.selectedClauseSet.genHtmlRows(_filterClauseData);
	// $("#tableSelectedClauses tbody").html(htmlRows);
	$('#filterModal').modal('hide');
}

// Copied from Interview.js, for integration with Interview.js
var confirmOnInterviewExit = function (e) 
{
    // If we haven't been passed the event get the window.event
    e = e || window.event;

    var message = 'This session has been modified. If you exit without saving, all changes will be lost.';

    // For IE6-8 and Firefox prior to version 4
    if (e) 
    {
        e.returnValue = message;
    }

    // For Chrome, Safari, IE8+ and Opera 12+
    return message;
};
window.onbeforeunload = confirmOnInterviewExit;

//-----------------------------------------------------------------
// Tab selection event
//-----------------------------------------------------------------
$('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
	//e.target // newly activated tab
	//e.relatedTarget // previous active tab
	var target = $(e.target).attr("href") // activated tab
	
	var toRefresh = false;
	var oDisplayTable = null; // CJ-1092
	if ('#addClauses' == target) {
		_activeClauseTab = TAB_ENUM.add;
		toRefresh = _pageManager.addClauseSet.needToDisplay;
    	oDisplayTable = _pageManager.addClauseSet.displayTable;
	}
	else if ('#removeClauses' == target) {
		_activeClauseTab = TAB_ENUM.removed;
		toRefresh = _pageManager.removeClauseSet.needToDisplay;
    	oDisplayTable = _pageManager.removeClauseSet.displayTable;
	}
	else {
		_activeClauseTab = TAB_ENUM.selected;
		toRefresh = _pageManager.selectedClauseSet.needToDisplay;
    	oDisplayTable = _pageManager.selectedClauseSet.displayTable;
	}

	try {
		var iItemsPerPage = (oDisplayTable ? oDisplayTable.page.len() : 10); // CJ-1092
		$('#selItemsPerPage').val(iItemsPerPage);
	}
	catch(err) {
	}

	if (toRefresh)
		adjustHeights();
});

//-----------------------------------------------------------------
// Windows resize events
//-----------------------------------------------------------------
function getWindowHeight() {
	var clsBannerHeight = 100;
	var admBannerHeight = 35;
	var headerHeight = _titleBannerHeight; //85;
	var saveButtonHeight = 35;//50;
	var footerHeight = 60;
	
	var iHeight =  $(window).innerHeight();
	
	if ((_theFcToken != null) && (_theFcToken != ""))		// valid token? No admin banner.
		iHeight = iHeight - clsBannerHeight;
	else if (_oUserInfoFC.canAccessAdminOptions() == true) 
		iHeight = iHeight - clsBannerHeight - admBannerHeight;
	else
		iHeight = iHeight - clsBannerHeight;
	
	iHeight = iHeight - headerHeight - footerHeight - saveButtonHeight - 20;
	
	var iTdFixMessage = $('#tdFixMessage').height() - 60; // CJ-1090
	if (iTdFixMessage > 0)
		iHeight -= iTdFixMessage; 
	
	_tableHeight = iHeight;
	return iHeight;
}

function adjustHeights() {
	try {
		$("body").css("cursor", "progress");
		var iHeight = getWindowHeight();
		if ($(window).height() < 400) {
			iHeight = 100;
		}
	
		switch (_activeClauseTab) {
	    case TAB_ENUM.selected:
	    	_pageManager.selectedClauseSet.displaySelectedClauseTable();
	        break;
	    case TAB_ENUM.add:
	    	_pageManager.addClauseSet.displayAddClauseTable();
	        break;
	    default:
	    	_pageManager.removeClauseSet.displayRemovedClauseTable();
	    	break;
		}
	}
	finally {
		$("body").css("cursor", "default");
	}
}

function expandHeader() {
	
   var e = document.getElementById('expandedHeader');
   if (e.style.display == 'none') { // expand header, and switch indicator to collapse
	   e.style.display = 'block';
	   $('#expand_link').html('<img src="../assets/images/menu_collapse.png" alt="menu collapse" />');
	   _titleBannerHeight = 115;
   }
   else { 							// collapse header, and switch indicator to expand
	   e.style.display = 'none'; 
	   $('#expand_link').html('<img src="../assets/images/menu_expand.png" alt="menu expand" />');
	   _titleBannerHeight = 48;
   }
   adjustHeights();
   //window.dispatchEvent(new Event('resize'));
}

$(function(){
	document.body.style['overflow'] = 'auto';
	//adjustHeights();
});

$(window).resize(function(){
	adjustHeights();

	switch (_activeClauseTab) {
    case TAB_ENUM.selected:
    	_pageManager.addClauseSet.needToDisplay = true;
    	_pageManager.removeClauseSet.needToDisplay = true;
        break;
    case TAB_ENUM.add:
    	_pageManager.selectedClauseSet.needToDisplay = true;
    	_pageManager.removeClauseSet.needToDisplay = true;
        break;
    default:
    	_pageManager.selectedClauseSet.needToDisplay = true;
		_pageManager.addClauseSet.needToDisplay = true;
    	break;
	}
});

//-----------------------------------------------------------------
//Methods to handle Paste events for the memo fields. 
//All data must be prepared for CLS standards
//-----------------------------------------------------------------

//CJ-915, flag convert pasted content to plain text.
var _pastedElem = null;
function handlepaste(el, e) {	
	_pastedElem = el;
	setTimeout(processPaste, 20);
}

function processPaste() {

	
	if (!_pastedElem)
		return;
	
	editRemoveAttributes(_pastedElem);
	
	_pastedElem = null;

}

//For MS Word, the Comments are NOT presented as element nodes.
//Thus they cannot be deleted using the DOM object.
//NOTE: This method will prevent the UNDO.
function editContents(sHtml) {
	
	// Replace any comments also. - RegExp not working.
	//sHtml = sHtml.replace(new RegExp('<!--(.*?)-->', 'g'), '');
	//sHtml = sHtml.replace(new RegExp('<!--[\s\S]*?-->', 'g'), '');
	
	var bComments = true;
	while (bComments) {
		var posBeg = sHtml.indexOf ('<!--');
		var posEnd = sHtml.indexOf ('-->');
		if ((posBeg < 0) || (posEnd < 0)) { 
			bComments = false;
			continue;
		}
		
		var res = sHtml.substring(posBeg, posEnd + 3);	// 3 = length of '-->'
		if (res) sHtml = sHtml.replace (res, "");
	}
	
	// Paste often includes newlines, which CLS does not support.
	sHtml = sHtml.replace(new RegExp('\r', 'g'), '');
	sHtml = sHtml.replace(new RegExp('\n', 'g'), ' ');
	
	// Chrome browser adds bullet to li items.
	sHtml = sHtml.replace(new RegExp('\u2022', 'g'), '');
	
	//if (sHtml == "<br>")
	//	sHtml = "";
	
	return sHtml.trim();
}

//CJ-915, Recursively strip all attributes and any tags that CLS does not support.
function editRemoveAttributes(elem) {

	for(var i=0; i<elem.children.length; i++) {
		
		// Traverse child nodes first.
		if (elem.children[i].hasChildNodes())
			editRemoveAttributes (elem.children[i]);
	
		// If this is a system field tag, ignore.
		if ((elem.children[i].id != null) && (elem.children[i].id.indexOf('sf-') == 0))
			continue;
		
		// Strip newlines, comments
		elem.children[i].innerHTML = editContents(elem.children[i].innerHTML);
		
		// Remove nested duplicate names -- Ex. <div><div><div>1</div></div></div>.
		if ((elem.children[i].parentNode.nodeName == elem.children[i].nodeName) && 
				(elem.children[i].parentNode.childNodes.length == 1)) {
			elem.children[i].parentNode.removeChild (elem.children[i]);
			i--;
			continue;
		}
		
		// Remove empty elements;
		if ((editContents(elem.children[i].textContent)== "") && (elem.children[i].innerHTML== "") &&
			(elem.children[i].tagName != "BR") && (elem.children[i].tagName != "HR")) {
			elem.children[i].parentNode.removeChild (elem.children[i]);
			i--;
			continue;
		}
		
		// Strip non-supported Tags.
		if (!isSupportedTag(elem.children[i].tagName, 'all')) {
			
			// if node has text, save to text node
			if (elem.children[i].innerHTML!= "") { 
				var newNode = document.createElement('span');
				newNode.innerHTML = elem.children[i].innerHTML;

				var parent = elem.children[i].parentNode;
				parent.insertBefore(newNode, elem.children[i]);
				i++;
			}
			elem.children[i].parentNode.removeChild (elem.children[i]);
			i--;
			continue;
		}

		// Strip all attributes from supported tags.
		var attributes = elem.children[i].attributes;
		var j = attributes.length;
		while( j-- )
			elem.children[i].removeAttributeNode(attributes[j]);

		// Table can have embedded standard HTML elements, like <p>. 
		// CLS does not support these elements within a table eleement.
		if (elem.children[i].tagName == "TABLE") {
			
			editTableTags(elem.children[i]);
			
			var headerRow = elem.children[i].getElementsByTagName("tr")[0]; 
			editTableHeader (headerRow);
			
		}
	}
}
//CJ-915, Recursively strip non-supported table tags.
function editTableTags(elem) {

	for(var i=0; i<elem.children.length; i++) {
		
		// Traverse child nodes first.
		if (elem.children[i].hasChildNodes())
			editTableTags (elem.children[i]);
	
		// If this is a system field tag, ignore.
		if ((elem.children[i].id != null) && (elem.children[i].id.indexOf('sf-') == 0))
			continue;
		
		// Strip newlines, comments
		elem.children[i].innerHTML = editContents(elem.children[i].innerHTML);
		
		if (!isSupportedTag(elem.children[i].tagName, 'table')) {
			
			// If node has text, save to text node
			if (elem.children[i].innerHTML!= "") { 
				var newNode = document.createElement('span');
				newNode.innerHTML = elem.children[i].innerHTML;
				
				var parent = elem.children[i].parentNode;
				parent.insertBefore(newNode, elem.children[i]);
				i++;
			}
			elem.children[i].parentNode.removeChild (elem.children[i]);
			i--;
			continue;
		}
	}
}

//CJ-915, Replace <td> in first table row with <th>. This is a CLS requirement.
function editTableHeader(elem) {

	for(var i=0; i<elem.children.length; i++) {
		
		if (elem.children[i].hasChildNodes())
			editTableHeader (elem.children[i]);
	
		// If this is a system field tag, ignore.
		if ((elem.children[i].id != null) && (elem.children[i].id.indexOf('sf-') == 0))
			continue;
		
		if (elem.children[i].tagName == "TD") 
		{ 
			var newNode = document.createElement('th');
			newNode.innerHTML = elem.children[i].innerHTML;
			
			// Make the font normal, since <th> will bold it
			var att = document.createAttribute("style");
			att.value = "font-weight: normal"; 
			newNode.setAttributeNode(att);  
			
			var parent = elem.children[i].parentNode;
			parent.insertBefore(newNode, elem.children[i]);
			i++;
		
			elem.children[i].parentNode.removeChild (elem.children[i]);
			i--;
			continue;
		}
	}
}
	
//CJ-915, If the tag is supported, then return true.
function isSupportedTag(tagName, tagType) {
		
	if (tagType == 'all') { 
		switch (tagName) { 
			case "A":
			case "B":
			case "BR":
			case "DIV":
			case "EM":
			case "H1": case "H2": case "H3": case "H4": case "H5": case "H6":
			case "HR":
			case "I":
			case "LI":
			case "P":
			case "SPAN":
			case "STRONG":
			case "SUB":	case "SUP":
			case "TABLE": case "TBODY": case "TR": case "TH": case "TD": case "TFOOT": case "THEAD":
			case "U":
			case "UL":
				return true;
		}
	}
	else if (tagType == 'table') {
		switch (tagName) { 
		case "A":
		case "B":
		case "EM":
		case "H1": case "H2": case "H3": case "H4": case "H5": case "H6":
		case "I":
		case "SPAN":
		case "STRONG":
		case "SUB":	case "SUP":
		case "TABLE": case "TBODY": case "TR": case "TH": case "TD": case "TFOOT": case "THEAD":
			return true;
		}
	}
		
	return false;
}

