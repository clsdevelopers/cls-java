onAssignCacCard = function(poButton) {
	$(poButton).prop("disabled", true);
	
	// CAC testing... send to PageAPI  because CacApi is not picking up.
	//var urlToHandler ='/cls/page?do=assignCAC';
	//var urlToHandler ='/cls/cac?do=assign';
	var urlToHandler = getAppPath() + 'cac?do=assign';

	//urlToHandler = '../cac?do=assign';
	$.ajax({
        url: urlToHandler,
		cache: false,
		dataType: 'json',
        contentType: 'application/json',
        success: function (data) {
    		if ("No session" != data.message) {
    			if (!(typeof _oUserInfo === "undefined")) {
    				_oUserInfo.loadSessionJson(data, true);
    			}
        		$(poButton).prop("disabled", false);
    		}
        	//if ('success' == data.status) {
    		//}
            alert(data.message);
        },
        error: function (data, status, jqXHR) {
        	logAjaxErrorOnConsole(data, status, jqXHR);
            $(poButton).prop("disabled", false);
            alert('There was an error.');
        }
    }); // end $.ajax
}

getAppPath = function() {
	var aPath = window.location.pathname.split('/');
	if (aPath.indexOf('cls') >= 0)
		return '/' + aPath[1] + '/';
	else
		return '/';
}