var PANEL_GROUP_HEADER_PREFIX = 'ph-';
var QUESTION_GROUP_CHVRON_PREFIX = 'qgi-';
var PANEL_GROUP_BODY_PREFIX = 'pb-';

onChgPanelShown = function(id) {
	$('#' + id).collapse("toggle");
}

showVersionChangesDialog = function(oData, docId, psModalId, psModalLabel, psModalBody, psModalBtnCancel, psModalBtnUpdate, pbCreateLinkedAward, docType) {
	if (!psModalId)
		psModalId = "verChangesModal";
	if (!psModalLabel)
		psModalLabel = "verChangesModalLabel";
	if (!psModalBody)
		psModalBody = "verChangesBody";
	
	if (!psModalBtnCancel)
		psModalBtnCancel = "bnVerChangesCancel";
	
	if (!psModalBtnUpdate)
		psModalBtnUpdate = "bnVerChangesUpdate";
	
	// _resumeDocId = docId;
	
	var sPromptText = 'CLS has updated its clause selection criteria based on recent changes issued by the DARC (FACs and DACs), as listed below. Please select \'update\' to include the applicable changes in your document.';
	if ((oData.facNumber) &&  (oData.dacNumber) &&  (oData.dfarsNumber))
		sPromptText = 'CLS has been updated to reflect the change(s) published for FAR and DFARS through FAC ' + oData.facNumber + ', DAC ' + oData.dacNumber + ', and DFARS ' + oData.dfarsNumber + '. Please select \'update\' to include the applicable changes in your document.';
	//	if (pbCreateLinkedAward)
//		sPromptText = 'CLS version changes are listed below.';
//	else
//		sPromptText = 'This document, ' + oData.docName +', will be updated to reflect the most recent version of interview criteria.' +
//		'  Changes are listed below.';
	$('#' + psModalLabel).html(sPromptText);

	var groupName = null;
	var groupHtml;
	
	var hideQuestions = false;
	if (docType && ('O' == docType))
		hideQuestions = true;

	var firstRow = true;
	var htmlRows = ''; 
	for (var iRow = 0; iRow < oData.changeItems.length; iRow++) {
		var oRec = oData.changeItems[iRow];
		if ((oRec.Is_Question_Or_Clause == 'Q') && hideQuestions)
			continue;

		if (firstRow || (oData.changeItems[iRow-1].groupName != oData.changeItems[iRow].groupName))
			parentNode = (firstRow ? '<div class="panel-group" id="accrdnChgList">' : '</div></div></div>')
				+ '<div class="panel panel-default collapsible-panel">'
				+ '<div class="panel-heading">'
				+ '<h4 class="panel-title">'
				+ '<a data-toggle="collapse" data-parent="#accrdnChgList" href="#" onclick="onChgPanelShown(\''+ PANEL_GROUP_HEADER_PREFIX + oRec.Clause_Ver_Change_Id+ '\')" class="bold">'
				+ '<span class="pull-right" id="' + QUESTION_GROUP_CHVRON_PREFIX + oRec.Clause_Ver_Change_Id + '"><i class="glyphicon glyphicon-chevron-down"></i></span>'
				+  ((oRec.Is_Question_Or_Clause == 'C') ? 'Section ' + oRec.group + ' - ' + oRec.groupName : oRec.groupName) + '</a></h4></div> '
				+ '<div id="' + PANEL_GROUP_HEADER_PREFIX + oRec.Clause_Ver_Change_Id +'" class="panel-collapse collapse' + ((iRow == 0) ? ' in' : '') + '">'
				+ '<div id="' + PANEL_GROUP_BODY_PREFIX + oRec.Clause_Ver_Change_Id +'" class="panel-body" style="padding:1%;" >' ;
		else
			parentNode = '';
		
		firstRow = false;

		var sChangeType = (oRec.Is_Question_Or_Clause == 'C') ? 'Clause  ' : 'Question ';
		var html = parentNode
			+ '<p><b>' + oRec.title + '</b>'
			+ '<br/>' + oRec.text + '<br/>'
			+ '<i>' + sChangeType + ' ' +  oRec.Change_Code + ': ' + oRec.Change_Brief + '</i>'
			+ '</p>';
		
		htmlRows += html;
	}

	htmlRows += '</div>';
	
	// html = htmlRows;
	$('#' + psModalBody).html(htmlRows); // html
	
	//$('#' + psModalBtnCancel).show();
	//$('#' + psModalBtnUpdate).show();
	
	// $('#' + psModalId).show();
	$('#' + psModalId).modal('show');
}

upgradeDocVersion = function (id, docType, psModalId, poCallBack) {
	if (!psModalId)
		psModalId = "verChangesModal";
	var page = getPagePath();
	var data = {
		'do': 'clauseversionchange-upd',
		'id': id
	};
	var instance = this;
	$.ajax({
		url: getAppPath() + 'page',
		data: data,
		cache: false,
		dataType: 'json',
		xhrFields: { withCredentials: true },
		success: function(data) {
			if ((data)  && ('success' == data.status))
			{
				$('#' + psModalId).modal('hide');
				if (poCallBack) {
					poCallBack();
				} else {
					id = data.data.newId; 
					if ((docType) && (docType == 'O')) { 
						console.log ('Upgrade: Redirect to final_clauses.jsp?doc_id=' + id);
						$(location).prop('href','final_clauses.jsp?doc_id=' + id);	
					}
					else
						$(location).prop('href','interview.jsp?doc_id=' + id);
				}
			}
			else {
				alert("Version could not be updated.")
			}
		},
		error: function(object, text, error) {
			alert(error);
		}
	});  
}
