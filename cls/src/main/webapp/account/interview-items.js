var ANSWER_STATE_UNKNOWN = 0;
var ANSWER_STATE_REQUIRED = 1;
var ANSWER_STATE_NOT_REQUIRED = 2;
var ANSWER_STATE_ANSWERED = 3;

var SYMBOL_ANSWERED = '&#9998;'; // bell icon
var SYMBOL_UNKNOWN = '&#9998;'; // bell icon 

var SYMBOL2_ANSWERED = '&#10003;'; // check mark icon 
var SYMBOL2_UNKNOWN = '<b>&#9744;</b>'; // square

var SYMBOL3_ANSWERED = '&#10003;' ; // check mark icon 
var SYMBOL3_UNKNOWN = '<font size="4">&#9900;</font>' ; // circle

var CHVRON_DOWN = '<img src="../assets/images/menu_expand.png" alt="Menu expand." />';
var CHVRON_UP = '<img src="../assets/images/menu_collapse.png" alt="Menu collapse." />';

var BTN_COMPLETE_ID_PREFIX = 'btnComplete-';

var QUESTION_GROUP_ID_PREFIX = 'qga-';
var QUESTION_GROUP_CHVRON_PREFIX = 'qgi-';
var QUESTION_CHOICE_PREFIX = "qci-";
var CLAUSE_EDIT_LINK_PREFIX = 'dc-e-';
var CLAUSE_ORDER_CB_PREFIX = 'dc-x-';

var ID_FAR = 1;
var ID_DFAR = 2;

var DIVIDER = '\t';

// CJ-476
var QUESTION_CODE_PROCEDURE = '[PROCEDURES]';
var COMMERCIAL_PROCEDURE = 'COMMERCIAL PROCEDURES (FAR PART 12)';
var QUESTION_CODE_SUPPLIES_OR_SERVICES = '[SUPPLIES OR SERVICES]';
var SERVICES = 'SERVICES';
var CLAUSE_OPTION_REQUIRED_CD = 'R';
var CLAUSE_OPTION_OPTONAL_CD = 'O';
var CLAUSE_OPTION_NOT_APPLICABLE_CD = 'N';

var FAR_PREFIX = "52."; // CJ-264
var DFAR_PREFIX = "252."; // CJ-264

// CJ-606
var QUESTION_CODE_DOCUMENT_TYPE = '[DOCUMENT TYPE]';
var AWARD = 'AWARD';

var _isToken = false;
var _Token = "";
var _isFinalMode = false;
var _oFinalCheckSet = null;
var _completeModal = "completeModal";
var _activeGroupCode = null;
var _activeQuestionPanelId= null; 
var _resetQuestionPanelId = null;
var _isLinkedAward = false;
var _isAward = false; // CJ-606
var _isManualOrders = false;
var FIRST_QUESTION_ID = 36826;

var _maxQuestionPanelHeight = 0; //CJ-638

var SAVE_ON_DEMAND = 0;
var SAVE_ON_EXIT = 1;
var SAVE_ON_FINALIZE = 2;
var SAVE_ON_TIMEOUT = 3;
var SAVE_ON_WORKSHEET = 4;
var SAVE_ON_RESET = 5;

function clearFinalCheck() {
	_oFinalCheckSet = null;
}

function onFillinModalClosed() {
	if (_oFinalCheckSet != null) {
		$('#' + _completeModal).modal('toggle');
	}
}

getSymbolCode = function(answered) {
	return (answered ? SYMBOL_ANSWERED : SYMBOL_UNKNOWN);
}

getSymbolCode2 = function(answered) {
	return (answered ? SYMBOL2_ANSWERED : SYMBOL2_UNKNOWN);
}

getSymbolCode3 = function(answered) {
	return (answered ? SYMBOL3_ANSWERED : SYMBOL3_UNKNOWN);
}

getChvronCode = function(expanded) {
	return (expanded ? CHVRON_UP : CHVRON_DOWN);
}

changeChvron = function(containerId, expanded) {
	var oSymbol = $('#' + containerId);
	var sCurrentSymbol = oSymbol.html();
	var sNewSymbol = getChvronCode(expanded);
	if (sNewSymbol != sCurrentSymbol) {
		oSymbol.html(sNewSymbol);
		return true;
	} else
		return false;
}

changeSymbol = function(containerId, answered, iSymbol) {
	var oSymbol = $('#' + containerId);
	var sCurrentSymbol = oSymbol.html();
	var sNewSymbol;
	if (2 == iSymbol) {
		sNewSymbol = getSymbolCode2(answered);
	} else if (3 == iSymbol) {
		sNewSymbol = getSymbolCode3(answered);
	} else
		sNewSymbol = getSymbolCode(answered);
	if (sNewSymbol != sCurrentSymbol) {
		oSymbol.html(sNewSymbol);
		return true;
	} else
		return false;
}

var MONTHS = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];	

longToMonthYear = function(value) {
	var date = toDate(value)
	if (date) {
		date.setDate(date.getDate() + 1);
		return MONTHS[date.getMonth()] + ' ' + date.getFullYear();
		// return date.toLocaleDateString('en-us', {month: "short", year: "numeric" });
	} else
		return '';
}

var TABLE_PRESCRIPTION = 'tablePrescription';

//------------------------------------------------
function isValidNumber(value) {
	var regex = /^(?=.)^\$?(([1-9][0-9]{0,2}(,[0-9]{3})*)|[0-9]+)?(\.[0-9]{1,2})?$/;
	return regex.test(value);
}

function isNumeric(n) {
	return !isNaN(parseFloat(n)) && isFinite(n);
}

genLink = function(url) {
	return '<a href="' + url + '">' + url + '</a>'; 
}

onBtnGroupQuestionClick = function (pGroupCode, pQuestionId) {
	_activeQuestionPanelId = pQuestionId; 
	_interview.qGroup.onGroupQuestionClick(pGroupCode, pQuestionId);
	//logOnConsole('onBtnGroupQuestionClick(' + pGroupCode + ', ' + pQuestionId + ')');
}

toggleGroupLabels = function(pGroupdId, bExpand) {
	var iconId = QUESTION_GROUP_CHVRON_PREFIX + pGroupdId;
	changeChvron(iconId, bExpand);
	var oParent = $('#' + iconId).parents();
	if (oParent.length > 0) {
		var oLink = oParent[0];
		if (bExpand)
			$(oLink).addClass('bold');
		else
			$(oLink).removeClass('bold');
	}
}


adjustQuestionChoicesHeight = function(activeQuestionDiv)
{
	var heading = activeQuestionDiv.find(".panel-heading");
	var padding = 80;
	var headingHeight =  heading.height();		
	activeQuestionDiv.find(".panel-body").css("height", _maxQuestionPanelHeight-headingHeight-padding);
	
}

//On the final page for a group of questions, the user
//will be presented with a button "Next Tab." To support
//this option, both the left and center panels must be modified.
onNextQuestionTabClick = function(accordianId, skipToQuestionId) {
	
	var id = accordianId.substr(QUESTION_GROUP_ID_PREFIX.length); // 'qga-'
	
	// CJ-362, use Navigation class to jump to first question in new panel.
	_interview.skipToQuestionPanel(skipToQuestionId); // CJ-362
	
	//$('#' + QUESTION_GROUP_CHVRON_PREFIX + id).click(); // this displays the Tab panel.
	//$('#' + QUESTION_GROUP_ID_PREFIX + id).trigger("shown.bs.collapse"); // this displays the question.
	
}

onQuestonGroupShown = function(e) {
	//logOnConsole('onQuestonGroupShown(' + e.currentTarget.id + ')');
	var id = e.currentTarget.id.substr(QUESTION_GROUP_ID_PREFIX.length); // 'qga-'

	toggleGroupLabels(_activeGroupCode, false); // changeChvron(QUESTION_GROUP_CHVRON_PREFIX + _activeGroupCode, false);
	toggleGroupLabels(id, true); // changeChvron(QUESTION_GROUP_CHVRON_PREFIX + id, true);

	$('[id^="panel-' + _activeGroupCode + '"]').hide();
	$('#panel-' + id).show();
	
	_activeGroupCode = id;
	_interview.SetActivePanel (id);
}

showClauseContent = function(oClause, edit) {
	$('#myModalLabel').html(oClause.getModalTitle());
	$('#myModalBody').html(oClause.getContent(edit));
	
	if (edit) {
		$('#myModelSave').show();
		$('#myModelComplete').show();
		var sRemarks = (oClause.editableRemarks ? oClause.editableRemarks : '');
		if (sRemarks == 'ALL')
			sRemarks = '';
		$('#myModelRemarks').html(sRemarks);
	}
	else {
		$('#myModelSave').hide();
		$('#myModelComplete').hide();
		$('#myModelRemarks').html('');
	}
	$('#myModal').modal();
}

popClause = function(clauseId, edit, fromComplete) {
	if (fromComplete) {
		//if (_oFinalCheckSet != null)
			$('#' + _completeModal).modal('hide'); // toggle
	} else
		_oFinalCheckSet = null;
	var oClause = _interview.clauses.findById(clauseId);
	
	if (_interview.doc.type != 'O') 
		edit = false;
	
	_clause = oClause;
	if (oClause.content == null) {
		$.ajax({
			url: getAppPath() + 'page?do=doc-clause-content&clause_id=' + clauseId,
			cache: false,
			dataType: 'json',
			success: function(data) {
				try {
					if ('success' == data.status) {
						if (_oUserInfo)
							_oUserInfo.loadSessionJson(data, true); // CJ-573
						oClause.content = data.data.clause_data;
						showClauseContent(oClause, edit);
					} else if ('no session' == data.message) {
						displaySessonEnded(true); // CJ-573
						//alert('You have been logged out for being inactivity, please refresh the page and log back into CLS to continue...');
						//goHome();
					} else
						alert('Unable to retrieve cluase data.')
				}
				catch (oError) {
					alert("JavaScript Error: " + oError);
				}
			},
			error: function(object, text, error) {
				logAjaxErrorOnConsole(object, text, error);
				alert("AJAX Error: " + object.responseText);
			}
		});
	} else {
		showClauseContent(oClause, edit);
		if (_oUserInfo) // CJ-696
			_oUserInfo.timelyRefreshTimeout();
	}
}

$("#myModelSave").click(function() {
	onClauseSave(false);
});

$("#myModelComplete").click(function() {
	onClauseSave(true);
});

onClauseSave = function(isCompleted) {
	_clause.onClauseSave(isCompleted);
	$('#myModal').modal('hide');
	if (_oFinalCheckSet != null) {
		_oFinalCheckSet.clauseUpdated(_clause);
		$('#' + _completeModal).modal('toggle');
	}
	if (_oUserInfo) // CJ-696
		_oUserInfo.timelyRefreshTimeout();
}

// Rules events ----------------------------------------------
 
//Populate and show dialog w. generic content, using Transcript dialog.
showDetailedContent = function(html) {
	
	$('#completeModalBody').html(html);
	
	$('#completeModal').modal();
}

//Reset events ----------------------------------------------

$("#bnResetYes").click(function() {
	_interview.onResetBtnClick();
});

$("#bnResetNo").click(function() {
	$('#resetModal').modal('hide');
});

$("#bnResetClose").click(function() {
	$('#resetModal').modal('hide');
});

$("#btnReset").click(function() {
	
	// The reset dialog combines the elements for all reset activities,
	// Warning, Processing, Success.
	
	// Show the elements for Reset - Warning.
	$('#resetModalLabel').html('Warning');
	$('#resetModalLabel').show();
	$('#resetBody').html('All answer choices selected will be removed and reset to the default value. Are you sure you want to reset your interview?');
	$('#resetBody').show();
	$('#bnResetNo').show();
	$('#bnResetYes').show();
	$('#bnResetClose').hide();
	$('#resetFooter').show();
	$('#resetHeader').hide();
	$('#resetProcessHeader').hide();
	$('#resetProcessMessage').hide();
	$('#resetProcessSpinner').hide();
	$('#resetModal').modal('show');
});

// Order selection events -------------------------------------
onOrderCheckboxClick = function(poCheckBox, psClauseId, pParam) {
	var value = (pParam == 3) ? false : poCheckBox.checked;
	_interview.onOrderCheckboxChanged(psClauseId, value, pParam);	
}

// Populate and show Transcript dialog

showTranscriptContent = function(html) {
	
	// CJ-1384, Add header info to the transcript pop-up.
	var tabs ='&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' ;
	var hdrHtml = 
		tabs + 'ACQUISITION NAME: ' +  _interview.doc.title.toUpperCase() + '<br/>' + 
		tabs + 'DOCUMENT NUMBER: ' + _interview.doc.number.toUpperCase() + '<br/>' + 
		tabs + 'DOCUMENT TYPE: ' + getDocumentTypeName(_interview.doc.type).toUpperCase() + '<br/>';
	$('#transcriptModalTitle').html(hdrHtml);
	
	$('#transcriptBody').html(html);//(oClause.getContent(edit));
	$('#bnComplete').show();
	
	$('#transcriptModal').modal();
}

$("#bnTranscriptComplete").click(function() {
	$('#transcriptModal').modal('hide');
});

// end populate and show Transcript dialog


onSkipToLink = function(finalPageId, skipToQuestionId)
{	
	//_interview.onSkipToQuestion(finalPageId, skipToQuestionId);
	_interview.skipToQuestionPanel(skipToQuestionId);
}


onGoToLink = function(skipToQuestionId)
{	
	$('#transcriptModal').modal('hide');
	
	//_interview.onGoToQuestion(skipToQuestionId);
	_interview.skipToQuestionPanel(skipToQuestionId);
}

function Progress() {
	this.totalQuestions = 0;
	this.answeredQuestions = 0;

	this.totalClausesForInput = 0;
	this.answeredClausesForInput = 0;
	
	this.totalFillInClauses = 0;
	this.answeredFillinClauses = 0;
	
	this.totalEditFillInClauses = 0;
	this.answeredEditFillinClauses = 0;
	
	this.display = function display() {
		var iTotal = this.totalQuestions //+ this.totalClausesForInput;  
		var iAnswers = this.answeredQuestions //+ this.answeredClausesForInput;  
		var percent = Math.floor(iAnswers * 100 / iTotal);
		$('#divBarProgress').css('width', percent + '%').attr('aria-valuenow', percent); // .progress-bar 
	};
	
	this.isQuestionCompleted = function isQuestionCompleted() {
		return (this.answeredQuestions >= this.totalQuestions); 
	};
	
	this.getQuestionInCompleteMessage = function getQuestionInCompleteMessage() {
		var remained = (this.totalQuestions - this.answeredQuestions);
		//return 'There are unanswered ' + remained
		//		+ ' question' + (remained > 1 ? 's' : '') + ' from total ' + this.totalQuestions + ' questions.';  
		
		return 'There are ' + remained + ' unanswered questions. Please answer all questions before clicking Complete and Exit.';
	};
	
	this.isClauseCompleted = function isClauseCompleted() {
		return (this.answeredClausesForInput >= this.totalClausesForInput); 
	};
	
	this.getClauseInCompleteMessage = function getClauseInCompleteMessage() {

		var remainingFillins = (this.totalFillInClauses - this.answeredFillinClauses);
		var remainingEdits = (this.totalEditFillInClauses - this.answeredEditFillinClauses);
		
		var sFillinMsg = "";
		var sEditMessage = "";
		if (remainingFillins > 0) {
			sFillinMsg = 'There ' +  (remainingFillins > 1 ? 'are ' : 'is ') + remainingFillins 
			+ ' unanswered clause fill-in' + (remainingFillins > 1 ? 's' : '') +
			(remainingEdits > 0 ? ' and ' : '.');
		}
		if (remainingEdits > 0) {
			if (remainingFillins == 0)
				sEditMessage = 'There ' +  (remainingEdits > 1 ? 'are ' : 'is ') + remainingEdits 
			    + ' unedited clause' + (remainingEdits > 1 ? 's' : '')  + '.';
			else
				sEditMessage = remainingEdits + ' unedited clause' + (remainingEdits > 1 ? 's' : '')  + '.';
		}
		
		return sFillinMsg + sEditMessage;
	};
}

//------------------------------------------------
function ChangeEventItems() {
	this.questions = new Array();
	this.clauses = new Array();
	
	this.init = function init(oQuestionItem) {
		this.questions.length = 0;
		this.clauses.length = 0;
		if (oQuestionItem)
			this.questions.push(oQuestionItem);
	};
	
	this.hasQuestion = function hasQuestion(oQuestionItem) {
		/*
		for (var iIndex = 0; iIndex < this.questions.length; iIndex++) {
			if (this.questions[iIndex].containQuestion(oQuestionItem))
				return true;
		}
		return false;
		*/
		return (this.questions.indexOf(oQuestionItem) >= 0); 
	};
	
	this.hasClause = function hasClause(oClauseItem) {
		return (this.clauses.indexOf(oClauseItem) >= 0); 
	};
	
	this.addQuestion = function addQuestion(oQuestionItem) {
		this.questions.push(oQuestionItem);
	};
	
	this.addClause = function addClause(oClauseItem) {
		if (this.clauses.indexOf(oClauseItem) < 0)
			this.clauses.push(oClauseItem);
	};
};

//------------------------------------------------
var _interview = null;
var _clause = null;
var _progress = new Progress();
var _changeEventItems = new ChangeEventItems();
var gSatEval = new SatEval();

// ------------------------------------------------
function RegulationItem() {
	this.id;
	this.name;
	this.title;
	this.url;

	this.getNameLink = function getNameLink() {
		return '<a href="' + this.url + '" title="' + this.title.replace(/"/g, '&quot;') + '" target="_blank">' + this.name + '</a>'; 
	};

	this.loadJson = function loadJson(json) {
	    this.id = json.regulation_id;
	    this.name = json.regulation_name;
	    this.title = json.regulation_title;
	    this.url = json.regulation_url;
	};
};

function RegulationSet() {
	this.elements = new Array();

	this.retrieve = function retrieve() {
		var instance = this;
		$.ajax({
			url: getAppPath() + 'page?do=doc-ref-regulations',
			cache: false,
			dataType: 'json',
			success: function(data) {
				if (!instance.loadJson(data.data)) {
					goHome();
				}
			},
			error: function(object, text, error) {
				goHome();
			}
		});
	};

	this.loadJson = function loadJson(json) {
		this.elements.length = 0;
		var oRegulationItem;
		try {
			for (var iData = 0; iData < json.length; iData++) {
				oRegulationItem = new RegulationItem();
				oRegulationItem.loadJson(json[iData]);
				this.elements.push(oRegulationItem);
			}
		} catch (error) {
			logOnConsole(error);
		}
		return true;
	};

	this.findById = function findById(id) {
		var oRegulationItem;
		for (var iEle = 0; iEle < this.elements.length; iEle++) {
			oRegulationItem = this.elements[iEle];
			if (oRegulationItem.id == id)
				return oRegulationItem;
		}
		return null;
	};
	
	this.getNameLink = function getNameLink(id) {
		var oItem = this.findById(id);
		if (oItem)
			return oItem.getNameLink() + ' ';
		else
			return id;
	};
	
	this.getName = function getName(id) {
		var oItem = this.findById(id);
		if (oItem)
			return oItem.name + ' ';
		else
			return id;
	};
};

// -------------------------------------------------------
function PrescriptionItem() {
	this.id;
	this.name;
	this.regulationId;
	this.url;
	//this.conntent;
	
	this.clauses = null;
	
	this.getNameLink = function getNameLink() {
		if (this.url)
			return '<a href="' + this.url + '" target="_blank">' + this.name + '</a>'; 
		return this.name;
	};
	
	this.loadJson = function loadJson(json) {
	    this.id = json.prescription_id;
	    this.name = json.prescription_name;
	    this.regulationId = json.regulation_id;
	    this.url = json.prescription_url;
	    //this.content = json.prescription_content;
	};
	
	this.getFormIdPrefix = function getFormIdPrefix() {
		return 'pi-' + this.id + '-';
	};
	
	this.getClauses = function getClauses() {
		if (this.clauses == null) {
			this.clauses = _interview.clauses.getClausesByPrescription(this.id);
		}
		return this.clauses;
	};
	
	this.containClause = function containClause(oClause) {
		if (oClause == null)
			return false;
		var aClauses = this.getClauses();
		return (aClauses.indexOf(oClause) >= 0);
	};
	
	this.genHtmlRows = function genHtmlRows() {
		var aClauses = this.getClauses();
		var nameTdAttributes = '';
		if (aClauses.length > 1)
			nameTdAttributes = ' rowspan="' + aClauses.length + '" valign="top"';
		var html = '<tr id="' + this.getFormIdPrefix() + '0"><td' + nameTdAttributes + '><strong>' + this.getNameLink() + '</strong></td>';
		if (aClauses.length < 1) {
			html += '<td>&nbsp;</td><td>&nbsp;</td></tr>';
		} else {
			for (var index = 0; index < aClauses.length; index++) {
				if (index > 0)
					html += '<tr id="' + this.getFormIdPrefix() + index + '">'; // <td>' + this.getNameLink() + '</td>';
				var oClause = aClauses[index];
				html += '<td>' + oClause.getSectionCode() + '.</td>'
					+ '<td>' + oClause.getModalTitle() + '.</td></tr>';
			}
		}
		return html;
	};
	
	this.getFormIds = function getFormIds() {
		var result = new Array();
		var formIdPrefix = this.getFormIdPrefix();
		var aClauses = this.getClauses();
		if (aClauses.length < 1)
			result.push(formIdPrefix + '0');
		else {
			for (var index = 0; index < aClauses.length; index++) {
				result.push(formIdPrefix + index);
			}
		}
		return result;
	}
}

function PrescriptionSet() {
	this.elements = new Array();
	this.retrieve = function retrieve() {
		var instance = this;
		$.ajax({
			url: getAppPath() + 'page?do=doc-ref-prescription',
			cache: false,
			dataType: 'json',
			success: function(data) {
				if (!instance.loadJson(data.data)) {
					goHome();
				}
			},
			error: function(object, text, error) {
				goHome();
			}
		});
	};
	this.loadJson = function loadJson(json) {
		this.elements.length = 0;
		var oPrescriptionItem;
		for (var iData = 0; iData < json.length; iData++) {
			oPrescriptionItem = new PrescriptionItem();
			oPrescriptionItem.loadJson(json[iData]);
			this.elements.push(oPrescriptionItem);
		}
		return true;
	};
	this.findById = function findById(id) {
		var oPrescriptionItem;
		for (var iEle = 0; iEle < this.elements.length; iEle++) {
			oPrescriptionItem = this.elements[iEle];
			if (oPrescriptionItem.id == id)
				return oPrescriptionItem;
		}
		return null;
	}
}

//-------------------------------------------------------
function ClauseSectionItem() {
	this.id;
	this.name;
	this.header;
	this.aClauses = new Array();

	this.loadJson = function loadJson(json) {
	    this.id = json.clause_section_id;
	    this.name = json.clause_section_code;
	    this.header = json.clause_section_header;
	};
	
	this.registerClause = function registerClause(oClause) {
		this.aClauses.push(oClause);
	};
	
	this.getFormId = function getFormId() {
		return 'cs-' + this.id;
	} 

	this.getCollapseId = function getCollapseId() {
		return 'sectCollapse' + this.id;
	} 

	this.genHtml = function genHtml() {
		if (this.aClauses.length <= 0)
			return '';
	
		var sClauseHtml = '';
		var sFarClausesByReference = '', sFarClausesByText = '';
		var sDfarClausesByReference = '', sDfarClausesByText = ''; 
		var sLocalClausesByReference = '', sLocalClausesByText = ''; 

		var bAny = false, sHtml;
		var oClause, bApplicable;
		for (var index = 0; index < this.aClauses.length; index++) {
			oClause = this.aClauses[index]; // unsorted
			bApplicable = _interview.prescriptionChoices.isApplicableClause(oClause);
			if (bApplicable)
				bAny = true;
			sClauseHtml = oClause.genHtml(bApplicable);
			if (oClause.isFarClause()) {
				if (oClause.isFullText())
					sFarClausesByText += sClauseHtml;
				else
					sFarClausesByReference += sClauseHtml;
			} else if (oClause.isDfarClause()) {
				if (oClause.isFullText())
					sDfarClausesByText += sClauseHtml;
				else
					sDfarClausesByReference += sClauseHtml;
			} else {
				if (oClause.isFullText())
					sLocalClausesByText += sClauseHtml;
				else
					sLocalClausesByReference += sClauseHtml;
			}
		}

		var idCollapse = this.getCollapseId();
		var html =
			'<div id="' + this.getFormId() + '" ' + (bAny ? '' : ' style="display:none"')
				+ ' class="panel-group" role="tablist" aria-multiselectable="true">' +
				'<div class="panel panel-default collapsible-panel">' // panel-primary
				    + '<div class="panel-heading collapsible-panel-heading">'
					    + '<div class="panel-title" style="font-size:.95em;">'
					    	+ '<table class="tblChoices"><tr>'
					    	+ '<td>' //+ '<td><i class="fa fa-bell-o"></i></td><td>'
						    // TODO - replace the toggle with the menu_collapse.png"
						    + '<a data-toggle="collapse" href="#' + idCollapse + '">' // data-parent="#' + parentId + '" 
						    + 'Section ' + this.name + ' - ' + this.header.replace(/\//g, " / ")
						    + '<span class="pull-right"><img src="../assets/images/menu_expand.png" alt="Menu expand." /></span>' //  clickable
						    + '</a>'
						    + '</td></tr></table>'
					    + '</div>'
				    + '</div>'
				    + '<div id="' + idCollapse + '" class="panel-collapsed collapse in">'
					    + '<div class="panel-body" style="padding:0;font-size:.95em;">'
					    	//+ sClauseHtml
					    	+ sFarClausesByReference + sFarClausesByText
							+ sDfarClausesByReference + sDfarClausesByText
							+ sLocalClausesByReference + sLocalClausesByText
					    + '</div>'
				    + '</div>'
			    + '</div>'
		    + '</div>';
		return html;
	};
	
	this.setupEvents = function setupEvents() {
		var idCollapse = this.getCollapseId();
		$('#' + idCollapse).on('shown.bs.collapse', function (e) { onCollapseShown(e); });
		$('#' + idCollapse).on('hidden.bs.collapse', function (e) { onCollapseHidden(e); });
	};
	
	this.clauseChanged = function clauseChanged(visible, poClause, oPrescriptionChoices) {
		if (visible) {
			$('#' + this.getFormId()).show();
		} else {
			var bFound = false;
			for (var index = 0; index < this.aClauses.length; index++) {
				//var oClause = this.aClauses[index];
				if (oPrescriptionChoices.isApplicableClause(this.aClauses[index])) {
					bFound = true;
					break;
				}
			}
			if (bFound == false)
				$('#' + this.getFormId()).hide();
		}
	};
	
	this.isSolicitationSelection = function isSolicitationSelection() {
		return ("K" == this.name) || ("L" == this.name) || ("M" == this.name);
	}
}

function ClauseSectionSet() {
	this.elements = new Array();

	this.retrieve = function retrieve() {
		var instance = this;
		$.ajax({
			url: getAppPath() + 'page?do=doc-ref-clause_section',
			cache: false,
			dataType: 'json',
			success: function(data) {
				if (!instance.loadJson(data.data)) {
					goHome();
				}
			},
			error: function(object, text, error) {
				goHome();
			}
		});
	};

	this.resolveObjectReferences = function resolveObjectReferences(oClause) {
		var oSection = this.findById(oClause.sectionId);
		if (oSection != null)
			oSection.registerClause(oClause);
		return oSection;
	};

	this.loadJson = function loadJson(json) {
		this.elements.length = 0;
		var oClauseSectionItem;
		for (var iData = 0; iData < json.length; iData++) {
			oClauseSectionItem = new ClauseSectionItem();
			oClauseSectionItem.loadJson(json[iData]);
			this.elements.push(oClauseSectionItem);
		}
		return true;
	};

	this.findById = function findById(id) {
		var oClauseSectionItem;
		for (var iEle = 0; iEle < this.elements.length; iEle++) {
			oClauseSectionItem = this.elements[iEle];
			if (oClauseSectionItem.id == id)
				return oClauseSectionItem;
		}
		return null;
	};

	this.findByName = function findByName(name) {
		var oClauseSectionItem;
		for (var iEle = 0; iEle < this.elements.length; iEle++) {
			oClauseSectionItem = this.elements[iEle];
			if (oClauseSectionItem.name.toUpperCase() == name)
				return oClauseItem;
		}
		return null;
	};

	this.getNameById = function getNameById(id) {
		var oItem = this.findById(id);
		return (oItem ? oItem.name : '');
	};
	
	this.genHtml = function genHtml() {
		var html = '';
		for (var iEle = 0; iEle < this.elements.length; iEle++) {
			//var oClauseSectionItem = this.elements[iEle];
			html += this.elements[iEle].genHtml();
		}
		return html;
	};
	
	this.setupEvents = function setupEvents() {
		for (var iEle = 0; iEle < this.elements.length; iEle++) {
			// var oClauseSectionItem = this.elements[iEle];
			this.elements[iEle].setupEvents();
		}
	};
}

//-------------------------------------------------------
function ClauseFillInItem() {
	this.id;
	this.code;
	this.type;
	this.maxSize;
	this.groupNumber;
	this.placeholder;
	this.defaultData;
	this.displayRows;
	this.forTable
	this.heading;

	this.answer = null;
	this.fullTtextModified = false;
	
	// CJ-561 - partial editable with fillins
	this.isEditParentFillin = false;
	this.isEditChildFillin = false;

	this.loadJson = function loadJson(json) {
	    this.id = json.clause_fill_in_id;
	    this.code = json.fill_in_code;
	    this.type = json.fill_in_type;
	    this.maxSize = json.fill_in_max_size;
	    this.groupNumber = json.fill_in_group_number;
		this.placeholder = json.fill_in_placeholder;
		this.defaultData = json.fill_in_default_data;
		this.displayRows = json.fill_in_display_rows;
		this.forTable = json.fill_in_for_table;
		this.heading = json.fill_in_heading;
	};
	
	this.loadAnswers = function loadAnswers(oDocFillInItem) {
		this.answer = oDocFillInItem.answer;
		this.fullTtextModified = oDocFillInItem.fullTtextModified;
	}

	this.saveAnswers = function saveAnswers(oDocFillInSet) {
		if (this.answer) {
			var oDocFillInItem = new DocFillInItem();
			oDocFillInItem.clauseFillInId = this.id;
			oDocFillInItem.answer = this.answer;
			oDocFillInItem.fullTtextModified = this.fullTtextModified;
			oDocFillInSet.add(oDocFillInItem);
		}
	}
	
	this.getFormId = function getFormId() {
		return 'c-f-i-' + this.id;
	};
	
	this.hasAnswer = function hasAnswer() {
		if ((this.type == 'C') || (this.type == 'R'))
			return true;
		return (this.answer != null) && (this.answer != '');
	};
	
	this.genInput = function genInput(forTable, cellValue, pbReadOnly) {
		
		if (!pbReadOnly)	// CJ-408 - show fillin only
			pbReadOnly = (this.defaultData != null);
		
		var sEditor;
		var sValue = '';
		if (forTable)
			sValue = (cellValue ? cellValue : '');
		else
			sValue = ((this.answer != null) ? this.answer : this.defaultData);
		if (this.type == 'M') {
			if (pbReadOnly) {
				return sValue;
			}
			var iMaxLength = (this.maxSize ? this.maxSize : 255);
			sEditor = '<textarea rows="' + (this.displayRows ? this.displayRows : 7)
				+ '" cols="80" style="width:100%"'
				+ ' name="' + this.getFormId() + '"'
				+ (forTable ? ' aria-labelledby="' + this.getFormId() + '"' : ' id="' + this.getFormId() + '"')
				//+ (this.placeholder ? ' placeholder="' + this.placeholder.replace(/"/g, '&quot;') + '"' : '')
				+ ' title="' + (this.placeholder ?  this.placeholder.replace(/"/g, '&quot;')  : 'Fill in textarea') + '" '
				+ ' maxlength="' + iMaxLength + '" >'
				+ (sValue ? sValue : '')
				+ '</textarea>';
			if (!forTable)
			{
				sEditor += '<label class="hide" for="' + this.getFormId() + '">'
					+ (this.placeholder ? this.placeholder.replace(/"/g, '&quot;')  : 'Fill in  textarea')
					+ '</label>';
				
				// CJ-1304, Placeholders for text fields
				if (this.placeholder)
					sEditor += '&nbsp;[<i>' + this.placeholder.replace(/"/g, '&quot;') + '</i>]';
			}
			// sEditor += '<script>$("#' + this.getFormId() + '").cleditor();</script>';
			var iHeight = 28;
			if (this.displayRows) {
				iHeight += this.displayRows * 15; 
			} else
				iHeight = 250;
//			sEditor += '<script>$(document).ready(function(){'
//				+ '$("#' + this.getFormId() + '").cleditor({'
//					+ 'height: ' + iHeight + ',' // 400
//					+ 'docType: \'<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">\''
//				+ '});'
//				+ 'window.setTimeout(function() {'
//					+ 'var editor = $("#' + this.getFormId() + '").cleditor()[0];'
//					+ 'editor.blurred(function(e){editor.updateTextArea(); });'
//					//+ 'editor.updateFrame();editor.updateTextArea();'
//					+ 'editor.refresh();'
//					//`+ 'alert("shown ' + this.getFormId() + '");'
//				+ '}, 500);'
//				+ '});</script>';
			
		} else 
		if ((this.type == 'C') || (this.type == 'R')) {
			if (pbReadOnly) {
				if (sValue)
					return "(<span class='spanData'>X</span>)";
				else
					return "(<span class='spanData'>_____</span>)";
			}
			
			sEditor = "";
			
				// CJ-1304, Placeholders for selection fields.
				// The parser will set the displayRows = 1, if placeholder needs a break line. 
			if (this.placeholder) { 
				sEditor += //'<label style="font-weight: normal;" >'
					 '[<i>' + this.placeholder.replace(/"/g, '&quot;')  + '</i>]&nbsp;' 
					//+ '</label>';
				if (this.displayRows == 1)
					sEditor +=	'<br>';
			}
			
			sEditor += 
				'<input type="' + ((this.type == 'C') ? 'checkbox' : 'radio') + '"'
				+ ' name="' + this.getFormId() + '"'
				+ ' id="' + this.getFormId() + '"'
				+ ' value="' + (sValue ? sValue : '1') + '"'
				+ ' arial-label="' + (this.placeholder ? this.placeholder.replace(/"/g, '&quot;')  : 'Make Selection ' + ((this.type == 'C') ? 'checkbox' : 'radio')) + '" '
				+ (sValue ? ' checked' : '')
			    + '>' 
			    + '<label class="hide" for="' + this.getFormId() + '">'
				+ (this.placeholder ? this.placeholder.replace(/"/g, '&quot;')  : 'Make Selection ' +  ((this.type == 'C') ? 'checkbox' : 'radio'))
				+ '</label>';

		} else {
			if (pbReadOnly) {
				if (sValue)
					return sValue;
				else
					return "(<span class='spanData'>_____</span>)";
			}
			var iMaxLength = (this.maxSize ? this.maxSize : 80);
			var iSize = (iMaxLength > 10 ? Math.floor(iMaxLength * 4 / 5) : iMaxLength);
			if (iSize > 60)
				iSize = 60;
			var sMax = '', sWidth = '';
			var sType;
			if ((this.type == '$') || (this.type == 'N')) {
				sType = 'number';
				if (iMaxLength > 12)
					iMaxLength = 12;
				var iMax = Math.pow(10, iMaxLength) - 1;
				sMax = ' max="' + iMax + '"';
				var iWidth = 10 * iMaxLength + 20;
				sWidth = ' style="width:' + iWidth + 'px"';
			}
			else if (this.type == 'D')
				sType = 'date';
			else if (this.type == 'E')
				sType = 'email';
			else if (this.type == 'U')
				sType = 'url';
			else{
				sType = 'text';
				if (forTable)
				{
					if (iSize > 25)
						iSize = 25;
					
				}
				else
				{
					iSize = this.getInputSize(iMaxLength, this.placeholder);
				}
			}
			
			sEditor = '<input type="' + sType + '" name="' + this.getFormId() + '"'
			+ (forTable ? ' aria-labelledby="' + this.getFormId() + '"' : ' id="' + this.getFormId() + '"')
			+ ' maxlength="' + iMaxLength + '" size="' + iSize + '"'
			+ sMax + sWidth
			+ (sValue ? (' value="' + sValue.replace(/"/g, '&quot;') + '"') : '')
			//+ (this.placeholder ? ' placeholder="' + this.placeholder.replace(/"/g, '&quot;') + '"' : '')
			+ ' title="' + (this.placeholder ? this.placeholder.replace(/"/g, '&quot;')  : 'Fill in ' +  sType) + '" '
			+ ' >';
			if (!forTable)
			{
				// CJ-1304, Placeholders for text fields
				if (this.placeholder)
					sEditor += '&nbsp;[<i>' + this.placeholder.replace(/"/g, '&quot;') + '</i>]';

				// 508 compliance - always include and hide label.
				sEditor += '<label class="hide" for="' + this.getFormId() + '">'
				+ (this.placeholder ? this.placeholder.replace(/"/g, '&quot;')  : 'Fill in ' +  sType)
				+ '</label>';
				
			}
		}
		return sEditor;
	};

	this.getInputSize = function(iMaxLength, sPlaceHolder)
	{		
		if (iMaxLength <= 100)
		{
			return 25;
		}
		if (iMaxLength <= 200)
		{
			return 50;
		}
		return 75;
	};
	
	// CJ-408 - fillin item
	this.genEditInput = function genEditInput(forTable, cellValue) {

		var pbReadOnly = (this.defaultData == null);	// CJ-408 - show editable only

		var sEditor;
		var sValue = '';
		if (forTable)
			sValue = (cellValue ? cellValue : '');
		else
			sValue = ((this.answer != null) ? this.answer : this.defaultData);
		if (this.type == 'M') {
			if (pbReadOnly) {
				return sValue;
			}
			/*
			var iMaxLength = (this.maxSize ? this.maxSize : 255);
			sEditor = '<textarea rows="' + (this.displayRows ? this.displayRows : 7)
				+ '" cols="80" style="width:100%"'
				+ ' name="' + this.getFormId() + '"'
				+ (forTable ? '' : ' id="' + this.getFormId() + '"')
				+ (this.placeholder ? ' placeholder="' + this.placeholder.replace(/"/g, '&quot;') + '"' : '')
				+ ' maxlength="' + iMaxLength + '" >'
				+ (sValue ? sValue : '')
				+ '</textarea>';
			// sEditor += '<script>$("#' + this.getFormId() + '").cleditor();</script>';
			var iHeight = 28;
			if (this.displayRows) {
				iHeight += this.displayRows * 15;
			} else
				iHeight = 250;
			sEditor += '<script>$(document).ready(function(){'
				+ '$("#' + this.getFormId() + '").cleditor({'
					+ 'height: ' + iHeight + ',' // 400
					+ 'docType: \'<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">\''
				+ '});'
				// Idea = if IE continues to lock, then add button above text area.
				//+ '$("#fcEditModelClose").click(function() {'
				//+ ' alert ("hi");'
				//+ '});'
				+ 'window.setTimeout(function() {'
					+ 'var editor = $("#' + this.getFormId() + '").cleditor()[0];'
					// + 'editor.updateFrame();editor.updateTextArea();'
					+ 'editor.refresh();'
					//`+ 'alert("shown ' + this.getFormId() + '");'
				+ '}, 500);'
				+ '});</script>';
				
			*/
			sEditor = '<div id="bdr-' + this.getFormId() + '" style="width:100%;background-color:#B7CEEC;padding-bottom:3px;">&nbsp;Click inside the box to edit content.</div>'
				+ '<div class="modal-body" style="border:2px solid #B7CEEC;" contenteditable="true" id="' + this.getFormId() + '" onpaste="handlepaste(this, event)">'
				+ sValue + ' </div>' ;
			
		} else
		if ((this.type == 'C') || (this.type == 'R')) {
			if (pbReadOnly) {
				if (sValue)
					return "(<span class='spanData'>X</span>)";
				else
					return "(<span class='spanData'>_____</span>)";
			}
			sEditor = '<input type="' + ((this.type == 'C') ? 'checkbox' : 'radio') + '"'
				+ ' name="' + this.getFormId() + '"'
				+ ' id="' + this.getFormId() + '"'
				+ ' value="' + (sValue ? sValue : '1') + '"'
				+ ' arial-label="' + (this.placeholder ? this.placeholder.replace(/"/g, '&quot;')  : 'Make Selection ' + ((this.type == 'C') ? 'checkbox' : 'radio')) + '" '
				+ (sValue ? ' checked' : '')
				+ '>' 
			    + '<label class="hide" for="' + this.getFormId() + '">'
				+ (this.placeholder ? this.placeholder.replace(/"/g, '&quot;')  : 'Make Selection ' +  ((this.type == 'C') ? 'checkbox' : 'radio'))
				+ '</label>';
			
			sEditor += '  class="editinput-class" >';
		} else {
			if (pbReadOnly) {
				if (sValue)
					return sValue;
				else
					return "(<span class='spanData'>_____</span>)";
			}
			var iMaxLength = (this.maxSize ? this.maxSize : 80);
			var iSize = (iMaxLength > 10 ? Math.floor(iMaxLength * 4 / 5) : iMaxLength);
			if (iSize > 60)
				iSize = 60;
			else if (iSize < 25) iSize = 25; // CJ-583, try to force IE clear icon by changing size of text field.
			var sMax = '', sWidth = '';
			var sType;
			// if ((this.type == '$') || (this.type == 'N')) {
			if (this.type == 'N') {
				sType = 'text';
				if (iMaxLength > 12)
					iMaxLength = 12;
				var iMax = Math.pow(10, iMaxLength) - 1;
				//sMax = ' max="' + iMax + '"';  CJ-583, removed. Not enforcing anything since type=text.
				var iWidth = 10 * iMaxLength + 20;
				//sWidth = ' style="width:' + iWidth + 'px"';  CJ-583, removed. Size determines the width.
			}
			else if (this.type == '$')
				sType = 'text';
			else if (this.type == 'D')
				sType = 'date';
			else if (this.type == 'E')
				sType = 'email';
			else if (this.type == 'U')
				sType = 'url';
			else{
				sType = 'text';
				if (forTable)
				{
					if (iSize > 25)
						iSize = 25;
					
				}
				else
				{
					iSize = this.getInputSize(iMaxLength);
				}
			}

			sEditor = '<input type="' + sType + '" name="' + this.getFormId() + '"'
			+ (forTable ? ' aria-labelledby="' + this.getFormId() + '"' : ' id="' + this.getFormId() + '"')
			+ ' maxlength="' + iMaxLength + '" size="' + iSize + '"'
			+ sMax + sWidth
			+ (sValue ? (' value="' + sValue.replace(/"/g, '&quot;') + '"') : '')
			//+ (this.placeholder ? ' placeholder="' + this.placeholder.replace(/"/g, '&quot;') + '"' : '')
			+ ' title="' + (this.placeholder ? this.placeholder.replace(/"/g, '&quot;')  : 'Fill in ' +  sType) + '" '
			+ ' class="editinput-class" >';
			if (!forTable)
			{
				// CJ-1304, Placeholders for text fields
				if (this.placeholder)
					sEditor += '&nbsp;[<i>' + this.placeholder.replace(/"/g, '&quot;') + '</i>]';
				
					sEditor += '<label  class="hide" for="' + this.getFormId() + '">'
					+ (this.placeholder ? this.placeholder.replace(/"/g, '&quot;')  : 'Fill in ' +  sType)
					+ '</label>';
			}
		}
		return sEditor;
	};
}

// -------------------------------------------------------------------------------
function ClauseFillInSet() {
	this.elements = new Array();
	
	this.bFullEditAndFillin = false;
	this.sFullFillinName;
	this.FullFillinId;
	this.hasEditChildFillins = false;
	this.bFillinsReplaced = false;
	
	this.loadJson = function loadJson(json) {
		this.elements.length = 0;
		var oClauseFillInItem;
		for (var iData = 0; iData < json.length; iData++) {
			oClauseFillInItem = new ClauseFillInItem();
			oClauseFillInItem.loadJson(json[iData]);
			this.elements.push(oClauseFillInItem);
		}
		
		this.initEditChildren(); // CJ-561 - partial editable with fillins
		
		return true;
	};
	
	// CJ-561 - identify editable with fillins
	this.initEditChildren = function initEditChildren() {
		var bParentFound = false;
		var childElements = new Array();
		var oFillIn;
		
		for (var iEle = 0; iEle < this.elements.length; iEle++) {
			oFillIn = this.elements[iEle];

			if ((oFillIn.defaultData != null) 
			&& (oFillIn.defaultData.indexOf ("{{") >= 0) && (oFillIn.defaultData.indexOf ("}}") >= 0)) { 

				this.hasEditChildFillins = true;
				oFillIn.isEditParentFillin = true;
			
				var pos1 = 0;
				var pos2 = 0;
				//logOnConsole ("sFillIn parent = " + oFillIn.code);
				
				while (1) { 
					pos1 = oFillIn.defaultData.indexOf ("{{", pos2);
					pos2 = oFillIn.defaultData.indexOf ("}}", pos2);
					
					if ((pos1 < 0) || (pos2 < 0))
						break;
					var sFillIn = oFillIn.defaultData.substring (pos1+2, pos2);
					childElements.push (sFillIn);
					//logOnConsole ("sFillIn child = " + sFillIn);
					
					bParentFound = true;
					pos2 = pos2 + 1;
				}
			}
		}
		
		if (bParentFound) {
			for (var iEle = 0; iEle < this.elements.length; iEle++) {
				oFillIn = this.elements[iEle];
				for (var iChildEle = 0; iChildEle < childElements.length; iChildEle++) { 
					if (oFillIn.code == childElements[iChildEle])	{ // if code names match
						oFillIn.isEditChildFillin = true;
						//logOnConsole ("sFillIn child = " + oFillIn.code);
					}
				}
			}
		}
	}
	
	this.loadAnswers = function loadAnswers(oDocFillInSet) {
		if (oDocFillInSet.isEmpty())
			return;
		var oClauseFillInItem, oDocFillInItem;
		for (var iEle = 0; iEle < this.elements.length; iEle++) {
			oClauseFillInItem = this.elements[iEle];
			oDocFillInItem = oDocFillInSet.findByClauseFillInId(oClauseFillInItem.id);
			if (oDocFillInItem != null) { 
				oClauseFillInItem.loadAnswers(oDocFillInItem);
				
				// CJ-914, Mark, if all system fields have been removed.
				if ((oClauseFillInItem.defaultData != null) 
					&& (oClauseFillInItem.defaultData.indexOf ("{{") >= 0) && (oClauseFillInItem.defaultData.indexOf ("}}") >= 0)
					&& (oDocFillInItem.answer != null)
					&& (oDocFillInItem.answer.indexOf ("{{") < 0) && (oDocFillInItem.answer.indexOf ("}}") < 0)) {
						this.bFillinsReplaced = true;
				}
			}
		}
	};
	
	this.saveAnswers = function saveAnswers(oDocFillInSet) {
		for (var iEle = 0; iEle < this.elements.length; iEle++) {
			//var oClauseFillInItem = this.elements[iEle];
			this.elements[iEle].saveAnswers(oDocFillInSet);
			//var oDocFillInItem = findByClauseFillInId(oClauseFillInItem.id);
			//if (oDocFillInItem != null)
			//	oClauseFillInItem.loadAnswers(oDocFillInItem);
		}
	};

	this.clear = function clear() {
		this.elements.length = 0;
		
		this.bFullEditAndFillin = false;
		this.sFullFillinName = null;
		this.FullFillinId = 0;
	};
	
	this.findById = function findById(id) {
		var oClauseFillInItem;
		for (var iEle = 0; iEle < this.elements.length; iEle++) {
			oClauseFillInItem = this.elements[iEle];
			if (oClauseFillInItem.id == id)
				return oClauseFillInItem;
		}
		return null;
	};
	
	this.hasEntry = function hasEntry() {
		return (this.elements.length > 0);
	};
	
	// CJ-408
	this.hasFillinEntry = function hasFillinEntry() {

		var b = false;
		for (var iEle = 0; iEle < this.elements.length; iEle++) {
			var oClauseFillInItem = this.elements[iEle];
			if (oClauseFillInItem.defaultData == null)
				b = true;
		}
		return b;
	};

	this.hasEditFillinEntry = function hasEditFillinEntry() {

		var b = false;
		for (var iEle = 0; iEle < this.elements.length; iEle++) {
			var oClauseFillInItem = this.elements[iEle];
			if (oClauseFillInItem.defaultData != null)
				b = true;
		}
		return b;
	};
	
	this.isEditFillinCompleted = function isEditFillinCompleted() {

		var b = false;
		for (var iEle = 0; iEle < this.elements.length; iEle++) {
			var oClauseFillInItem = this.elements[iEle];
			if ((oClauseFillInItem.defaultData != null) && (oClauseFillInItem.answer != null))
				b = true;
		}
		return b;
	};
	
	/*
	this.hasAllAnswered = function hasAllAnswered(oClause) {
		for (var iEle = 0; iEle < this.elements.length; iEle++) {
			var oClauseFillInItem = this.elements[iEle];
			if (oClauseFillInItem.hasAnswer() == false)
				return false;
		}
		return true;
	};
	
	this.hasAnyAnswer = function hasAnyAnswer(oClause) {
		for (var iEle = 0; iEle < this.elements.length; iEle++) {
			var oClauseFillInItem = this.elements[iEle];
			if (oClauseFillInItem.hasAnswer())
				return true;
		}
		return false;
	};
	*/
	
	this.genTable = function genTable(aColumns, pbReadOnly) {
		var baseFillIn = aColumns[0];
		var aValues = new Array(aColumns.length);
		var iPrevRows = 0;
		var sHeaderTr = '<thead><tr>';
		var sFunc = 'onAddClauseRow' + baseFillIn.id + '()';
		var sScript = '<script>\nfunction ' + sFunc + ' {\nvar sRow = \'<tr>';
		for (var item = 0; item < aColumns.length; item++) {
			var oFillIn = aColumns[item];
			sHeaderTr += '<th id="' + oFillIn.getFormId() + '">' + (oFillIn.heading ? oFillIn.heading : '') + '</th>'; 
			var value = oFillIn.answer;
			if (value == null)
				aValues[item] = new Array();
			else {
				var aAnswers = value.split(DIVIDER);
				aValues[item] = aAnswers;
				if (iPrevRows < aAnswers.length)
					iPrevRows = aAnswers.length
			}
			sScript += '<td>' + oFillIn.genInput(true, '', pbReadOnly) + '</td>';
		}
		sScript += '</tr>\';\n$("#' + baseFillIn.getFormId() + ' tbody").append(sRow);\n}\n</script>\n';
		sHeaderTr += '</tr></thead>';
		var rows = (baseFillIn.displayRows ? baseFillIn.displayRows : 5);
		if (iPrevRows > rows)
			rows = iPrevRows; 

		// CJ-1304, Placeholders for tables will be used as instruction,
		// if only the first placeholder value is set.
		var sInstructions = "";
		var bUseInstructions = false;
		if (aColumns.length > 0) {
			for (var item = 0; item < aColumns.length; item++) {
				if (item == 0 && aColumns[item].placeholder)
					bUseInstructions = true;
				else if (item > 0 && aColumns[item].placeholder)
					bUseInstructions = false;
			}
		}
		if (bUseInstructions) {
			sInstructions = '<label style="font-weight: normal;">[<i>' + aColumns[0].placeholder + '</i>]</label><br>';
		}
		
		var html = sInstructions + '<table class="tableEditClause" id="' + baseFillIn.getFormId() + '">' + sHeaderTr + '<tbody>';
		for (var iRow = 0; iRow < rows; iRow++) {
			html += '<tr>';
			for (var item = 0; item < aColumns.length; item++) {
				var oFillIn = aColumns[item];
				var sValue = null;
				var aAnswers = aValues[item];
				if (aAnswers) {
					if (aAnswers.length > iRow)
						sValue = aAnswers[iRow];
				}
				var sEditor = oFillIn.genInput(true, sValue, pbReadOnly);
				html += '<td>' + sEditor + '</td>';
			}
			html += '</tr>';
		}
		html += '</tbody>'
			+ (pbReadOnly ? "" :
				'<tfoot><tr><td colspan="' + aColumns.length + '">'
			+ '<button type="button" class="btn btn-default" onclick="' + sFunc + '">Add Row</button>' + sScript
			+ '</td></tr></tfoot>')
			+ '</table>';
		
		return html;
	}
	
	this.genInputContent = function genInputContent(oClause, pbReadOnly) {
		
		var modifiedContent = oClause.content;
		var aColumns = new Array();
		var aTables = new Array();
		var sFirstTableColumnName = null;
		var oFillIn;
		
		// check for Full Edits, with additional Fillins.
		this.initFullEditVariables(oClause);
		if (this.bFullEditAndFillin) { 

			for (var item = 0; item < this.elements.length; item++) {
				oFillIn = this.elements[item];
				if ((oFillIn.code == this.sFullFillinName) && (oFillIn.defaultData != null )) {
					modifiedContent = oFillIn.defaultData;
					if (oFillIn.answer != null)
						modifiedContent = oFillIn.answer;
				} 
			}
		}		
		// CJ-561
		else if (this.hasEditChildFillins) {
			for (var item = 0; item < this.elements.length; item++) {
				oFillIn = this.elements[item];
				if ((oFillIn.isEditParentFillin) && (oFillIn.defaultData != null )) {
					if (oFillIn.answer != null)
						modifiedContent = modifiedContent.replace('{{' + oFillIn.code + '}}',  oFillIn.answer);
					else
						modifiedContent = modifiedContent.replace('{{' + oFillIn.code + '}}',  oFillIn.defaultData);
				} 
			}
		}
		
		for (var item = 0; item < this.elements.length; item++) {
			oFillIn = this.elements[item];
			if (this.bFullEditAndFillin && (oFillIn.code == this.sFullFillinName))
				 continue;
			else if (!this.bFullEditAndFillin && oFillIn.isEditParentFillin)	// CJ-561
				continue;
			
			if (oFillIn.forTable == true) {
				var iGroup = (oFillIn.groupNumber ? oFillIn.groupNumber : -1);
				var oTable = null;
				for (var iTable = 0; iTable < aTables.length; iTable++) {
					var oColumns = aTables[iTable];
					var oColumnFillIn = oColumns[0];
					var iColumnGroup = (oColumnFillIn.groupNumber ? oColumnFillIn.groupNumber : -1);
					if (iColumnGroup == iGroup) {
						oTable = oColumns;
						break;
					}
				}
				if (oTable == null) {
					oTable = new Array();
					aTables.push(oTable);
				}
				oTable.push(oFillIn);
				//if (sFirstTableColumnName == null)
				//	sFirstTableColumnName = oFillIn.code; 
				//aColumns.push(oFillIn);
			} else {
				var sEditor, sFillInName;
				/*
				if (sFirstTableColumnName != null) {
					sFillInName = sFirstTableColumnName;
					sEditor = this.genTable(aColumns);
					modifiedContent = modifiedContent.replace('{{' + sFillInName + '}}', sEditor);
					for (var iCol = 1; iCol < aColumns.length; iCol++) {
						oFillIn = aColumns[iCol];
						modifiedContent = modifiedContent.replace('{{' + oFillIn.code + '}}', '');
					}
					sFirstTableColumnName = null;
					aColumns = new Array();
				}
				*/
				sFillInName = oFillIn.code;
				sEditor = oFillIn.genInput(false, null, pbReadOnly);
				modifiedContent = modifiedContent.replace('{{' + sFillInName + '}}', sEditor);
			}
		}
		for (var iTable = 0; iTable < aTables.length; iTable++) {
			aColumns = aTables[iTable];
			sEditor = this.genTable(aColumns, pbReadOnly);
			oFillIn = aColumns[0];
			modifiedContent = modifiedContent.replace('{{' + oFillIn.code + '}}', sEditor);
			for (var iCol = 1; iCol < aColumns.length; iCol++) {
				oFillIn = aColumns[iCol];
				modifiedContent = modifiedContent.replace('{{' + oFillIn.code + '}}', '');
			}
		}
		/*
		if (sFirstTableColumnName != null) {
			sEditor = this.genTable(aColumns, pbReadOnly);
			oFillIn = aColumns[0];
			modifiedContent = modifiedContent.replace('{{' + oFillIn.code + '}}', sEditor);
			for (var iCol = 1; iCol < aColumns.length; iCol++) {
				oFillIn = aColumns[iCol];
				modifiedContent = modifiedContent.replace('{{' + oFillIn.code + '}}', '');
			}
		}
		*/
		return modifiedContent;
	};

	// CJ-408
	this.genEditContent = function genEditContent(oClause) {
		var modifiedContent = oClause.content;
		var aColumns = new Array();
		var aTables = new Array();
		var sFirstTableColumnName = null;
		var oFillIn;
		
		// check for Full Edits, with additional Fillins.
		this.initFullEditVariables(oClause);
		if (this.bFullEditAndFillin == true)
			return this.contentInsertSystemFields(oClause);
		// CJ-561, partial edits with fillins
		else if (this.hasEditChildFillins == true)	
			return this.contentInsertSystemFieldsPartial(oClause);

		for (var item = 0; item < this.elements.length; item++) {
			oFillIn = this.elements[item];
			if (oFillIn.forTable == true) {
				var iGroup = (oFillIn.groupNumber ? oFillIn.groupNumber : -1);
				var oTable = null;
				for (var iTable = 0; iTable < aTables.length; iTable++) {
					var oColumns = aTables[iTable];
					var oColumnFillIn = oColumns[0];
					var iColumnGroup = (oColumnFillIn.groupNumber ? oColumnFillIn.groupNumber : -1);
					if (iColumnGroup == iGroup) {
						oTable = oColumns;
						break;
					}
				}
				if (oTable == null) {
					oTable = new Array();
					aTables.push(oTable);
				}
				oTable.push(oFillIn);

			} else {
				var sEditor, sFillInName;
				sFillInName = oFillIn.code;
				sEditor = oFillIn.genEditInput(false, null);
				modifiedContent = modifiedContent.replace('{{' + sFillInName + '}}', sEditor);
			}
		}
		for (var iTable = 0; iTable < aTables.length; iTable++) {
			aColumns = aTables[iTable];
			oFillIn = aColumns[0];
			modifiedContent = modifiedContent.replace('{{' + oFillIn.code + '}}', '');
			for (var iCol = 1; iCol < aColumns.length; iCol++) {
				oFillIn = aColumns[iCol];
				modifiedContent = modifiedContent.replace('{{' + oFillIn.code + '}}', '');
			}
		}

		return modifiedContent;
	};

	// CJ-408, validate edit content.
	// No reason fill in couldn't use this function also.
	this.validateContent = function validateContent() {

		var oFillIn;
		var oValidateInput = new ValidateInput();
		var oSuccessMessage = '0';

		//this.initFullEditVariables(oClause);
		
		for (var item = 0; item < this.elements.length; item++) {
			oFillIn = this.elements[item];

			if ((oFillIn.defaultData == null ) || (oFillIn.forTable == true)) {
				continue;
			}
			else if (this.bFullEditAndFillin && (oFillIn.code == this.sFullFillinName)) {
				//var answer = $('#' + oFillIn.getFormId()).val();
				var answer = $('#' + oFillIn.getFormId()).html();
				var otherFillIns = new Array();
				for (var item2 = 0; item2 < this.elements.length; item2++) {
					if (this.elements[item2].code != this.sFullFillinName)
						otherFillIns.push(this.elements[item2].code);
				}
				
				if (oValidateInput.areFillinsRemoved (answer, otherFillIns)) {
					this.bFillinsReplaced = true;
				}
				
				if (!oValidateInput.validateFullEdit (answer, otherFillIns)) {
					oSuccessMessage = oValidateInput.lastErrorMessage;
					break;
				}
			}
			// CJ-561
			else if (!this.bFullEditAndFillin && oFillIn.isEditParentFillin) {
				var answer = $('#' + oFillIn.getFormId()).html();
				var otherFillIns = new Array();
				for (var item2 = 0; item2 < this.elements.length; item2++) {
					if (this.elements[item2].code != oFillIn.code)
						otherFillIns.push(this.elements[item2].code);
				}
					
				if (oValidateInput.areFillinsRemoved (answer, otherFillIns)) {
					this.bFillinsReplaced = true;
				}
				
				if (!oValidateInput.validateFullEdit (answer, otherFillIns)) {
					oSuccessMessage = oValidateInput.lastErrorMessage;
					break;
				}
			}
			else {
				var answer = $('#' + oFillIn.getFormId()).val();
				if (!oValidateInput.check (oFillIn.code, oFillIn.type, answer)) {
					oSuccessMessage = oValidateInput.lastErrorMessage;
					break;
				}
			}
		}

		
		return oSuccessMessage;
	};

	// CJ-844 check if any fillins hava an answer.
	this.anyFillinsAnswered = function anyFillinsAnswered() {
		var hasAnyAnswer = false;
		
		for (var item = 0; item < this.elements.length; item++) {
			var oFillIn = this.elements[item];
			if (oFillIn.defaultData != null )
				continue;
			
			if (oFillIn.forTable == true) {
				var formInputs = $('input[name=' + oFillIn.getFormId() + ']');
				var sValues = '';
				for (var iInput = formInputs.length - 1; iInput >= 0; iInput--) {
					var oInput = formInputs[iInput];
					var value = oInput.value;
					if (sValues == '') {
						value = value.trim();
						if (value)
							sValues = value;
					} else
						sValues = value + DIVIDER + sValues;
				}
				oFillIn.answer = sValues;
			} else {
				// for checkboxes, we need to check for "checked" attribute
				if (oFillIn.type == 'C') {
					oFillIn.answer = ($('#' + oFillIn.getFormId()).prop('checked') ? '1' : '');
				} 
				else if (oFillIn.type == 'M') { 
					// CJ-1142, Textarea fillins ansers must be retreived using .val();
					//oFillIn.answer = $('#' + oFillIn.getFormId()).html();		
					
					var fillInElement = $('#' + oFillIn.getFormId());
					/* fillInElement.prop("tagName") undefined
					var fillInTagName = fillInElement.prop("tagName").toLowerCase(); // undefined error
					if (fillInTagName == 'input' || fillInTagName == 'textarea') 	
					{
						oFillIn.answer = fillInElement.val();	
					}
					else
					{
						oFillIn.answer = fillInElement.html();	
					}
					*/ 
					// assume textarea, because this entire method is for regular fillins.
					oFillIn.answer = fillInElement.val();						
					if (!oFillIn.answer)
						oFillIn.answer = fillInElement.html();	
					
				}				
				else
					oFillIn.answer = $('#' + oFillIn.getFormId()).val();

				if (oFillIn.answer)
					oFillIn.answer = oFillIn.answer.trim();
			}
			
			if (oFillIn.answer)
				hasAnyAnswer = true;
		}
		
		return hasAnyAnswer;
	};
	
	
	this.onClauseSave = function onClauseSave(isCompleted, oClause, iFullEditMode) {
		var hasAnyAnswer = false;
		
		for (var item = 0; item < this.elements.length; item++) {
			var oFillIn = this.elements[item];
			//logOnConsole ("code = " + oFillIn.code);	// CJ-583, debug only
			// CJ-583
			if ((iFullEditMode == 1) // only save answers for non-full fill-ins
				&& (this.sFullFillinName == oFillIn.code))	
				continue;
			if ((iFullEditMode == 2) // only save answers for full fill-in
					&& (oClause.editableRemarks != null) 
					&& ((oClause.editableRemarks == 'ALL') || (oClause.editableRemarks.indexOf ('ALL') == 0)) 
					&& (this.sFullFillinName!= null) && (this.sFullFillinName != oFillIn.code))
				continue;
			
			if ((iFullEditMode == 1) && (oFillIn.defaultData != null ))	// only save fillin fields.
				continue;
			if ((iFullEditMode == 2) && (oFillIn.defaultData == null ))	// only save edit fields.
				continue;
			
			if (oFillIn.forTable == true) {
				var formInputs = $('input[name=' + oFillIn.getFormId() + ']');
				var sValues = '';
				for (var iInput = formInputs.length - 1; iInput >= 0; iInput--) {
					var oInput = formInputs[iInput];
					var value = oInput.value;
					if (sValues == '') {
						value = value.trim();
						if (value)
							sValues = value;
					} else
						sValues = value + DIVIDER + sValues;
				}
				oFillIn.answer = sValues;
			} else {
				// for checkboxes, we need to check for "checked" attribute
				if (oFillIn.type == 'C') {
					oFillIn.answer = ($('#' + oFillIn.getFormId()).prop('checked') ? '1' : '');
				} 
				else if (oFillIn.type == 'M')
				{
					var fillInElement = $('#' + oFillIn.getFormId());
					/*
					var fillInTagName =fillInElement.prop("tagName").toLowerCase();
					
					if (fillInTagName == 'input' || fillInTagName == 'textarea') 
					{
						oFillIn.answer = fillInElement.val();						
					}
					else
					{
						oFillIn.answer = fillInElement.html();	
					}
					*/
					
					if (oFillIn.defaultData == null ) { // regular fillin, assume textarea
						oFillIn.answer = fillInElement.val();						
						if (!oFillIn.answer)
							oFillIn.answer = fillInElement.html();	
					}
					else {
						oFillIn.answer = fillInElement.html();
						if (!oFillIn.answer) 
							oFillIn.answer = fillInElement.val();	
					}
					
				}					
				else
					oFillIn.answer = $('#' + oFillIn.getFormId()).val();

				if (oFillIn.answer)
					oFillIn.answer = oFillIn.answer.trim();
				
				// CJ-658, Delete all text reverts to default
				//if ((!oFillIn.answer) && (oFillIn.type == 'M') && (oFillIn.defaultData))
				// CJ-745, Add for textboxes too.
				if ((!oFillIn.answer) && (oFillIn.defaultData))
					oFillIn.answer = " ";
				
				if (oFillIn.answer && (oFillIn.type == 'M'))
					oFillIn.fullTtextModified = true;
			}
			// CJ-583, debug only
			//if (oFillIn.answer)
			//logOnConsole ("answer = " + oFillIn.answer);
			
			if (oFillIn.answer)
				hasAnyAnswer = true;
			_interview.doc.unsavedChanges = true;
			window.onbeforeunload = confirmOnInterviewExit;
		}
		this.contentRemoveSystemFields (oClause);
		
		return hasAnyAnswer;
	};
	
	// Set flags for Full Edits, with additional Fillins. 
	this.initFullEditVariables = function initFullEditVariables(oClause) {
		
		if (this.sFullFillinName)
			return;
		
		if ((oClause) && (oClause.editableRemarks != null) 
				&& ((oClause.editableRemarks == 'ALL') || (oClause.editableRemarks.indexOf ('ALL') == 0)) 
				&&  (this.elements.length > 1))
		{
			this.bFullEditAndFillin = true;
			
			this.sFullFillinName =  oClause.content;
			this.sFullFillinName = this.sFullFillinName.replace ("{{", "");
			this.sFullFillinName = this.sFullFillinName.replace ("}}", "");
			
			for (var item = 0; item < this.elements.length; item++) {
				var oFillIn = this.elements[item];
				if (oFillIn.code == this.sFullFillinName)
					this.FullFillinId = oFillIn.id;
			}
		}
	}
	
	// Called when Edit content is prepared for display.
	// Return the content with the System Field labels.
	this.contentInsertSystemFields = function contentInsertSystemFields(oClause) {

		var modifiedContent;
		this.initFullEditVariables(oClause);
		
		if (!this.bFullEditAndFillin)
			return modifiedContent;
		
		for (var item = 0; item < this.elements.length; item++) {
			oFillIn = this.elements[item];
			if ((oFillIn.code == this.sFullFillinName) && (oFillIn.defaultData != null )) {
				modifiedContent = oFillIn.genEditInput(false, null); // oFillIn.defaultData;
			} 
		}
		
		if (modifiedContent != oClause.content) {
			for (var item = 0; item < this.elements.length; item++) {
				oFillIn = this.elements[item];
				if (oFillIn.code != this.sFullFillinName) {
					var sFillInName = oFillIn.code;
					modifiedContent = modifiedContent.replace('{{' + sFillInName + '}}', '<span id="sf-' + sFillInName +  '" style="background-color: rgb(204, 204, 204);">[System Field]</span>');
				} 
			}
		}
	
		return modifiedContent;
	}	

	// CJ-561
	// Called when Partial Edit content is prepared for display.
	// Return the content with the System Field labels.
	this.contentInsertSystemFieldsPartial = function contentInsertSystemFieldsPartial(oClause) {

		var modifiedContent = oClause.content;
		
		if (this.hasEditChildFillins == false)	
			return modifiedContent;
		
		for (var item = 0; item < this.elements.length; item++) {
			oFillIn = this.elements[item];
			if (oFillIn.isEditChildFillin == false) {
				var sFillInName = oFillIn.code;
				modifiedContent = modifiedContent.replace('{{' + sFillInName + '}}', oFillIn.genEditInput(false, null)); // oFillIn.defaultData;
			} 
		}
		
		if (modifiedContent != oClause.content) {
			for (var item = 0; item < this.elements.length; item++) {
				oFillIn = this.elements[item];
				if (oFillIn.isEditChildFillin == true) {
					var sFillInName = oFillIn.code;
					modifiedContent = modifiedContent.replace('{{' + sFillInName + '}}', '<span id="sf-' + sFillInName +  '" style="background-color: rgb(204, 204, 204);">[System Field]</span>');
				} 
			}
		}
	
		logOnConsole ("modifiedContent: " + modifiedContent);
		return modifiedContent;
	}	
	// If fully editable clause with Fillins, then remove the "System Field" labels
	this.contentRemoveSystemFields = function contentRemoveSystemFields(oClause) {
		
		this.initFullEditVariables(oClause);
			
		if (!this.bFullEditAndFillin)
			return this.contentRemoveSystemFieldsPartial(oClause);
	
		var modifiedContent;
		
		// first, retreive the answer for the fully editable fillin
		for (var item = 0; item < this.elements.length; item++) {
			oFillIn = this.elements[item];
			if (oFillIn.code == this.sFullFillinName) {
				if (oFillIn.answer != null) {
					modifiedContent = oFillIn.answer;
				}
			}
		}
		
		if (!modifiedContent) 
			return;
		
		// if the edit clause was modified, then replace the 'system field' labels with orignal fillin names.
		var oValidateInput = new ValidateInput();
		for (var item = 0; item < this.elements.length; item++) {
			oFillIn = this.elements[item];
			if (oFillIn.code != this.sFullFillinName) {
				var sFillInName = oFillIn.code;
				//var sFillInSpan = this.contentRetreiveSystemFields(modifiedContent, sFillInName);
				var sFillInSpan = oValidateInput.contentRetreiveSystemFields (modifiedContent, sFillInName);
				modifiedContent = modifiedContent.replace(sFillInSpan, '{{' + sFillInName + '}}');
			}
		}
		
		// now save the content, including the edits and orignal fillin names, back to the answer for the full Fill in.
		for (var item = 0; item < this.elements.length; item++) {
			oFillIn = this.elements[item];
			if (oFillIn.code == this.sFullFillinName) {
				oFillIn.answer = modifiedContent;
			}
		}
	}
	
	// If fully editable clause with Fillins, then remove the "System Field" labels
	this.contentRemoveSystemFieldsPartial = function contentRemoveSystemFieldsPartial(oClause) {
		
		if (this.hasEditChildFillins == false)	
			return;
		
		// if the edit clause was modified, then replace the 'system field' labels with orignal fillin names.
		var oValidateInput = new ValidateInput();
		for (var item = 0; item < this.elements.length; item++) {
			if ((this.elements[item].isEditParentFillin == false) 
				|| (this.elements[item].answer == null))
				continue;
			
			// save the answer with System fields.
			oFillIn = this.elements[item];	
			var modifiedAnswer = oFillIn.answer;
			logOnConsole ("");
			logOnConsole ("modifiedAnswer: " + modifiedAnswer);

			for (var item2 = 0; item2 < this.elements.length; item2++) {
				if (this.elements[item2].isEditChildFillin == false) 
					continue;
					
				oChildFillIn = this.elements[item2];
				var sFillInSpan = oValidateInput.contentRetreiveSystemFields (modifiedAnswer, oChildFillIn.code);
				logOnConsole ("span name: " + sFillInSpan);
				
				modifiedAnswer =  modifiedAnswer.replace(sFillInSpan, '{{' + oChildFillIn.code + '}}');
				oFillIn.answer = modifiedAnswer;
				logOnConsole ("new answer: " + modifiedAnswer);
				logOnConsole ("");
			}
		}
		
	}
	
	// CJ-583
	this.getFullEditId = function getFullEditId() {
		return this.FullFillinId;
	}
}

// -------------------------------------------------------
function ClauseItem() {
	this.id;
	this.name;
	this.inclusion;
	this.title;
	this.isBasicClause; // CJ-193 for is_basic_clause
	this.content = null;
	this.url;
	this.sectionId;
	this.oSection;
	this.effectiveDate;
	this.regulationId;
	this.conditions;
	this.rule;
	this.additionalConditions = null; // this.conditionToClause;
	this.editable; // boolean
	this.editableRemarks;
	this.oEditCondition;
	this.optional; // boolean
	this.commercialStatus = null; // CJ-476
	this.isDfarsActivity;
	this.oEvaluation = null;
	this.oAdtnlCond = null; // new
	
	this.originalRule;
	this.originalConditions;
	this.oprType = null;
	
	this.optionalUserActionCode = null; // null, A (Added), R (Removed)
	
	// this.isRemovedByUser = false;
	this.isCompleted = false;
	this.isVisible = false;

	this.prescriptionIds = new Array();
	this.clauseFillIns = new ClauseFillInSet();

	this.oOptionCondition = null; // CJ-802
	
	/*
	this.compareClauseForSort = function compareClauseForSort(oRec) { // CJ-746
		var iClauseTypeOrder = getClauseTypeSortIndex(this.name)
		var iRecTypeOrder = getClauseTypeSortIndex(oRec.name); 
		if (iClauseTypeOrder < iRecTypeOrder)
			return true;
		if (iClauseTypeOrder > iRecTypeOrder)
			return false;
		
		if (this.inclusion > oRec.inclusion) // 'F'/'R'
			return true;
		if (this.inclusion < oRec.inclusion) // 'F'/'R'
			return false;

		return false;
	};
	*/
	
	this.getContent = function getContent(edit) {
		if (! edit)
			// return this.content;
			return this.clauseFillIns.genInputContent(this, true) ;
		
		return this.clauseFillIns.genInputContent(this);
	};
	
	this.updateCompleteColor = function updateCompleteColor(sFormId, sEditLinkId) {
		var oDiv = $('#' + sFormId);
		var oBtn = $('#' + sEditLinkId);
		if (this.isCompleted) {
			oDiv.removeClass('bg-danger');
			oDiv.addClass('bg-success');
			oBtn.removeClass('btn-danger');
			oBtn.addClass('btn-success');
		} else {
			oDiv.removeClass('bg-success');
			oDiv.addClass('bg-danger');
			oBtn.removeClass('btn-success');
			oBtn.addClass('btn-danger');
		}
	} 
	
	this.onClauseSave = function onClauseSave(isCompleted) {
		this.isCompleted = isCompleted;
		//var hasAnyAnswer = isCompleted;
		this.clauseFillIns.onClauseSave(isCompleted);
		this.updateCompleteColor(this.getFormId(), this.getEditLinkId());
		/*
		var oDiv = $('#' + this.getFormId());
		var oBtn = $('#' + this.getEditLinkId());
		if (this.isCompleted) {
			oDiv.removeClass('bg-danger');
			oDiv.addClass('bg-success');
			oBtn.removeClass('btn-danger');
			oBtn.addClass('btn-success');
		} else {
			oDiv.removeClass('bg-success');
			oDiv.addClass('bg-danger');
			oBtn.removeClass('btn-success');
			oBtn.addClass('btn-danger');
		}
		*/
		_interview.adjustProgressBar(2);
	};
	
	this.isRestrictedForAward = function isRestrictedForAward() { // CJ-606
		return ((_isLinkedAward || _interview.isAward) &&
				this.oSection && this.oSection.isSolicitationSelection());
	} 
	
	this.isAllowed = function isAllowed() {
		if (this.isRestrictedForAward()) // if (_isLinkedAward && this.oSection && this.oSection.isSolicitationSelection()) // CJ-606
			return false;
		else if (_interview.isCommercialProcedure && ('DOES NOT APPLY AT ALL' == this.commercialStatus)) // CJ-476
			return false;
		else
			return true;
	} 
	
	this.isEditClause = function isEditClause() {
		return (this.inclusion == 'F') && this.hasFillin();
	};
	
	this.hasFillin = function hasFillin() {
		return (this.clauseFillIns.hasEntry());
	};
	
	this.hasRegularFillin = function hasRegularFillin() {
		return (this.clauseFillIns.hasFillinEntry());
	};
	
	this.hasEditFillin = function hasEditFillin() {
		return (this.clauseFillIns.hasEditFillinEntry());
	};
	
	this.isEditFillinCompleted = function isEditFillinCompleted() {
		return (this.clauseFillIns.isEditFillinCompleted());
	};
	
	this.isCommercialAlwaysRequired = function isCommercialAlwaysRequired() { // CJ-476
		if (this.isRestrictedForAward()) // CJ-606
			return false;

		if (this.commercialStatus && _interview.isCommercialProcedure) {
			if ('R (ALWAYS)' == this.commercialStatus) {
				return true;
			}
		}
		return false;
	}
	
	this.getCommercialProcedureOption = function getCommercialProcedureOption(bApplicable) { // CJ-476
		/*
		 * R (ALWAYS): - required regardless of rule - these clauses should appear in the clause cart under a commercial procedure selection
		 * O (RULE APPLIES) - optional when rule satisfied 
		 * R (RULE APPLIES) - Required when rules are satisfied...as normal
		 * R (SERVICES-RULES APPLIES) - required when rule satisfied for services - as long as commercial procurement involving services 
		 */
		if (this.commercialStatus && _interview.isCommercialProcedure && (this.isRestrictedForAward() == false)) { // CJ-606
			if ('R (ALWAYS)' == this.commercialStatus)
				return CLAUSE_OPTION_REQUIRED_CD;
	
			// var bApplicable = this.isApplicable(_interview.prescriptionChoices, false, false);
			if ('R (RULE APPLIES)' == this.commercialStatus) {
				if (bApplicable)
					return CLAUSE_OPTION_REQUIRED_CD;
				else
					return CLAUSE_OPTION_OPTONAL_CD;
			}
			
			if ('R (SERVICES-RULE APPLIES)' == this.commercialStatus) {
				if (bApplicable && _interview.isSevice)
					return CLAUSE_OPTION_REQUIRED_CD;
				else
					return CLAUSE_OPTION_OPTONAL_CD;
			}
			
			if ('O (RULE APPLIES)' == this.commercialStatus) {
				return CLAUSE_OPTION_OPTONAL_CD;
			}
			
			if ('DOES NOT APPLY AT ALL' == this.commercialStatus)
				return CLAUSE_OPTION_NOT_APPLICABLE_CD;
		}
		
		return CLAUSE_OPTION_OPTONAL_CD;
	}
	
	// CJ-802, Thanks to the success of the commercial procedure clause options,
	// we now need to make a clause optional per answer choices.
	this.getConditionalOption = function getConditionalOption() {
			if ('R (RULE APPLIES)' == this.oOptionCondition.status) {				
					return CLAUSE_OPTION_REQUIRED_CD;
			}
			else if ('O (RULE APPLIES)' == this.oOptionCondition.status) {
				return CLAUSE_OPTION_OPTONAL_CD;
			}
			else return (this.optional);
	}
	
	this.getClauseOption = function getClauseOption() { // CJ-476
		if (this.commercialStatus && _interview.isCommercialProcedure) {
			var bApplicable = this.isApplicable(_interview.prescriptionChoices, false, false);
			var sCommercialOption = this.getCommercialProcedureOption(bApplicable);
			if (CLAUSE_OPTION_REQUIRED_CD == sCommercialOption)
				return false;
			return true;
		} 
		// CJ-802
		else if ((this.oOptionCondition) && (this.oOptionCondition.isApplicable())) {
			return this.getConditionalOption();
		}
		else 
			return (this.optional);		
	}
	
	/*
	this.hasAllAnswered = function hasAllAnswered() {
		return this.clauseFillIns.hasAllAnswered(this);
	}
	this.hasAnyAnswer = function hasAnyAnswer() {
		return this.isCompleted; // this.clauseFillIns.hasAnyAnswer(this);
	}
	*/
	
	this.getModalTitle = function getModalTitle() {
		return this.getLinkHeader() + this.title +
				// (this.isBasicClause ? "-BASIC" : "") + // CJ-193 // CJ-965 Remove Basic
				' (' + longToMonthYear(this.effectiveDate) + ')';
	};
	
	this.getClauseLink = function getClauseLink() {
		return '<a href="' + this.url + '" target="_blank" title="Visit official site"><strong>' +  this.name + '</strong></a> ';
	};
	
	this.getLinkHeader = function getLinkHeader() {
		return '<b>' + _interview.regulations.getName(this.regulationId) + this.name + '</b> ';  
	};
	
	this.genHtml = function genHtml(bSelected) {
		var bEditClause = this.clauseFillIns.hasFillinEntry();
		//var bSelected = _interview.prescriptionChoices.isApplicableClause(this);
		var divClass, btnClass;
		if (bEditClause) {
			if (this.isCompleted) { // this.clauseFillIns.hasAnyAnswer(this)) { // hasAllAnswered(this)
				divClass = 'bg-success';
				btnClass = 'btn-primary';
			} else {
				divClass = 'bg-danger';
				btnClass = 'btn-danger';
			}
		} else {
			divClass = '';
			btnClass = 'btn-primary';
		}
	
		var sButtonName = 'eCFR View'; 
		if (bEditClause) {
			if (_interview.doc.type == 'O')
				sButtonName = 'Update Fill-ins';
			else
				sButtonName = 'View Fill-ins';
		}
		
		this.isVisible = bSelected;
		var html = '<div id="' + this.getFormId() + '" class="divClause ' + divClass
			+ '"' + (bSelected ? 'style="font-size:1em;"' : ' style="display:none"') + '>'
			+ this.getLinkHeader()
			+ 'by ' + ((this.inclusion == 'F') ? 'Full Text' : 'Reference')
			+ ' &nbsp;<button id="' + this.getEditLinkId() + '" onclick="javascript:popClause('
				+ this.id + ', ' + bEditClause + ')" class="btn btn-xs ' + btnClass + '">'
				+ '<span style="font-size: x-small">' + sButtonName + '</span></button>' 
			+ '<span class="pull-right ordersRemoveClause nodisplay"><button class="btn btn-link" onclick="javascript:onOrderCheckboxClick(null,' + this.id + ',3)"><i class="fa fa-times"></i><span class="hide">Discard this clause</span></button></span>'
			+ '<div style="padding-left:15px; font-size:.85em;">' + this.title + ' (' + longToMonthYear(this.effectiveDate) + ')'; 
		if (this.prescriptionIds.length > 0) {
			var sRegulationHtml = '', oRegulation;
			for (var index = 0; index < this.prescriptionIds.length; index++) {
				oRegulation = _interview.prescriptions.findById(this.prescriptionIds[index]);
				if (oRegulation) {
					sRegulationHtml += (sRegulationHtml ? ', ' : '') + oRegulation.getNameLink();
				}
			}
			if (sRegulationHtml)
				html += '<br /><strong>Prescriptions: </strong>' + sRegulationHtml;
			if (this.optionalUserActionCode == 'A')
				html += '<br /><i>(Manually Selected Clause)</i>';
		}
		return html + '</div></div>';
	};
	
	this.isFar = function isFar() {
		return (ID_FAR == this.regulationId);
	} 
	
	this.isDFar = function isDFar() {
		return (ID_DFAR == this.regulationId);
	} 
	
	/* CJ-1005
	this.genOrderSelDivHtml = function genOrderSelDivHtml(bSelected, pParam) {
		var sId = this.getOrderCheckboxId(pParam);
		var sCheckBox = '<input type="checkbox" id="' + sId + '" value="' + this.id + '"' + (bSelected ? ' checked' : '')
			+ ' onclick="onOrderCheckboxClick(this, ' + this.id + ',' + pParam + ')"'
			+ '>';
		var html = '<div>'
			// + '<div class="order_sel_clause_xb">' + sCheckBox + '</div>'
			// + '<div class="order_sel_clause_name">' + sLabel + '</div>'
			+ '<label for="' + sId + '">' + sCheckBox + ' '
			+ (pParam ? '<span class="mono">' + this.oSection.name + '</span>. ' + this.getLinkHeader() : this.getClauseLink())
			+ ' ' + this.title + '</label>'
			+ '</div>';
		return html;
	};
	*/
	
	this.show = function show(bVisible, oPrescriptionChoices) {
		
		// never show Replaced clauses
		if (this.oprType == OPR_REPLACE_WITH) // 3
			bVisible = false;
		
		if (this.isVisible != bVisible) {
			if (bVisible)
				$('#' + this.getFormId()).show();
			else
				$('#' + this.getFormId()).hide();
			this.isVisible = bVisible;
			this.oSection.clauseChanged(bVisible, this, oPrescriptionChoices);
		}
	}

	this.getLink = function getLink() {
		return genLink(this.url);
	};
	
	this.getSectionCode = function getSectionCode() {
		return _interview.sections.getNameById(this.sectionId);
	};

	this.resolveObjectReferences = function resolveObjectReferences(aSections, aQuestions, aClauseSet, oDocFillInSet) {
		this.oSection = aSections.resolveObjectReferences(this);
		/*
		if (this.conditions) {
			this.oEvaluation = new ClauseEvaluation();
			this.oEvaluation.parseConditions(this, aQuestions, aClauseSet)
		}
		*/
		this.clauseFillIns.loadAnswers(oDocFillInSet);
	};
	
	this.prepareEvals = function prepareEvals(aQuestions, aClauseSet) {
		if (this.conditions) {
			this.oEvaluation = new ClauseEvaluation();
			this.oEvaluation.parseConditions(this, aQuestions, aClauseSet)
		}
		if (this.additionalConditions) {
			this.oAdtnlCond = new AdditionalConditionEval();
			this.oAdtnlCond.parseConditions(this, aQuestions, aClauseSet)
		}
	};
	
	this.saveAnswers = function saveAnswers(oDocFillInSet) {
		this.clauseFillIns.saveAnswers(oDocFillInSet);
	};
	
	this.cacheApplicable = null;
	this.isApplicable = function isApplicable(oPrescriptionChoices, clearCache, forAnswerEval) { // aSelectedClauses
		// Don't apply removed Clause records.

		if ((!forAnswerEval) && (this.optionalUserActionCode != null)) {
			// CJ-628, System added clauses cannot be removed also.
			if (this.optionalUserActionCode == 'R') // && (this.optional == 1))
				return false;
			if ((this.oprType == OPR_REPLACE_WITH) && (this.optionalUserActionCode == 'A')) { // 3 If user ADDS a replaced clause, then only show the replacement.
				logConsole(this.name + " is not applicable because of OPR_REPLACE_WITH & (this.optionalUserActionCode == 'A')"); 
				return false;
			} 
			// CJ-1251, System should display manually added clauses, that are also marked "DO NOT INCLUDE"
			if ((this.oprType == OPR_DO_NOT_INCLUDE) && (this.optionalUserActionCode == 'A')) { 
				logConsole(this.name + " is applicable because of DO_NOT_INCLUDE & (this.optionalUserActionCode == 'A')"); 
				return true;
			}
			// CJ-1157, Make sure no additional condition before returning true;
			if ((this.oprType == null) || (this.oprType == 0))
					return true;
		} 
		
		if (_isManualOrders)
			return true;
		
		if (this.oEvaluation) {
			if (clearCache || (this.cacheApplicable == null)) { 
				this.cacheApplicable = this.oEvaluation.isApplicable(oPrescriptionChoices.clauses);
				//oPrescriptionChoices.updateCrossoverApplicable (this, this.cacheApplicable);					
			}		
			if ((!forAnswerEval) && (this.oprType == OPR_REPLACE_WITH)) { // 3
				return false;
			} 
			return this.cacheApplicable;
		}
		//else if (this.prescriptionIds.length > 0) 
		//	return oPrescriptionChoices.hasOtherClause(this, null);
		else
			return false;
	};
	
	this.hasQuestionCodes = function hasQuestionCodes(aCodes, clearCache) {
		if (this.oEvaluation != null) {
			var result = this.oEvaluation.hasQuestionCodes(aCodes);
			if (result && clearCache)
				this.cacheApplicable = null;
			return result;
		}
		return false;
	};
	
	this.isChangeEffected = function isChangeEffected() {
		if (this.oEvaluation != null) {
			return this.oEvaluation.isChangeEffected(_changeEventItems);
		}
		return false;
	}

	this.finalCheck = function finalCheck(oFinalCheckSet, oPrescriptionChoices) {
		if (_isManualOrders == true) {
			/* Removed for CJ-546
			if (oPrescriptionChoices.hasClause(this) == true) {
				if (this.isEditClause())
					if (this.isCompleted == false)
						oFinalCheckSet.addNotCompletedClauses(this);
			}
			*/
		} else {	
			var bPrevious = this.isVisible;
			var bNow = bPrevious;
			bNow = this.isApplicable(oPrescriptionChoices, true); // oPrescriptionChoices.clauses,
			
			if (oPrescriptionChoices.hasClause(this) == false) {
				if (bNow != bPrevious)
					oPrescriptionChoices.updateCrossoverClause(this, bNow);
			}
			
			if (bNow != bPrevious) {
				oFinalCheckSet.addItem(this, bNow);
				this.show(bNow, oPrescriptionChoices);
			}
			/* Removed for CJ-546
			else if (bNow && this.isEditClause() && (this.isCompleted == false))
				oFinalCheckSet.addNotCompletedClauses(this);
			*/
		}
	};
	
	this.evalAdditionalCondition = function evalAdditionalCondition(oFinalCheckSet, oPrescriptionChoices) {
		if (this.oAdtnlCond != null)
			this.oAdtnlCond.evaluate(oFinalCheckSet, oPrescriptionChoices);
	};
	
	this.onQuestionAnswerChanged = function onQuestionAnswerChanged(oPrescriptionChoices) {
		//if (this.isChangeEffected()) {
			//_changeEventItems.addClause(this);
		
		var bPrevious = this.isVisible;
		var bNow = this.isApplicable(oPrescriptionChoices, true); // oPrescriptionChoices.clauses
		
		if (oPrescriptionChoices.hasClause(this) == false) {
			if (bNow != bPrevious)
				oPrescriptionChoices.updateCrossoverClause(this, bNow);
		} else {
			if (bNow != bPrevious)
				oPrescriptionChoices.updateCrossoverClause(this, bNow);
		}
		
		if (bNow != bPrevious) {
			this.show(bNow, oPrescriptionChoices);
			//this.showCrossoverApplicable (bNow, oPrescriptionChoices);
		}
		//}
	};
	
	this.loadJson = function loadJson(json) {
		this.id = json.clause_id;
		this.name = json.clause_name;
		this.inclusion = json.inclusion_cd;
		this.title = json.clause_title;
		this.isBasicClause = json.is_basic_clause; // CJ-193 
		// this.content = json.clause_data;
		this.url = json.clause_url;
		this.sectionId = json.clause_section_id;
		this.effectiveDate = json.effective_date;
		this.regulationId = json.regulation_id;
		this.conditions = json.clause_conditions;
		this.originalConditions = this.conditions;
		this.rule = json.clause_rule;
		this.originalRule = this.rule;
		this.additionalConditions = json.additional_conditions; // this.conditionToClause = json.condition_to_clause;
		this.editable = json.is_editable;
		this.editableRemarks = json.editable_remarks;
		this.optional = json.is_optional;
		this.commercialStatus = json.commercial_status; // CJ-476
		this.isDfarsActivity = json.is_dfars_activity;

		// CJ-802 - Check if the optional flag is conditonal.
		if ((json.optional_status != null) && (json.optional_conditions != null)){
			this.oOptionCondition = new AttributeConditions
				(this.name, json.optional_conditions, json.optional_status);
		}
		
		
		this.checkEditCondition(this.editableRemarks);
		
		this.prescriptionIds.length = 0;
		if (json.prescriptionIds) {
			var aIds = json.prescriptionIds;
			for (var index = 0; index < aIds.length; index++)
				this.prescriptionIds.push(aIds[index]);
		}
		
		this.clauseFillIns.clear();
		if (json.clauseFillIns) {
			this.clauseFillIns.loadJson(json.clauseFillIns);
		}
		if (!this.hasFillin())
			this.isCompleted = true;

	};
	
	this.hasPrescription = function hasPrescription(prescriptionId) {
		return (this.prescriptionIds.indexOf(prescriptionId) >= 0);
	};
	
	this.getFormId = function getFormId() { return 'dc-' + this.id; };
	this.getEditLinkId = function getEditLinkId() { return CLAUSE_EDIT_LINK_PREFIX + this.id; }; // 'dc-e-'
	this.getOrderCheckboxId = function getOrderCheckboxId(pParam) { return CLAUSE_ORDER_CB_PREFIX + this.id + pParam; }; // 'dc-x-'
	
	this.checkEditCondition = function checkEditCondition(sRemarks) {
		
		if ((this.editable == false) || (sRemarks == null))
			return;
		
		if (sRemarks.indexOf ("ALL - ") > -1) { 
			//sRemarks = sRemarks.replace ("ALL - ", ""); System Fields need the ALL - 
			
			this.oEditCondition = new EditCondition();
			this.oEditCondition.initialize(sRemarks);
			
			if (!this.oEditCondition.isValid())
				this.oEditCondition = null;
		}	
	}
	
	// CJ-802
	this.checkOptionCondition = function checkOptionCondition() {
		
		if (this.oOptionCondition == null)
			return;
					
		this.oOptionCondition.resolve();
		
		if (!this.oOptionCondition.isValid())
			this.oOptionCondition = null;
		
	}
	
	this.isFarClause = function isFarClause() { // CJ-746
		return this.name.startsWith(FAR_PREFIX); // startsWith() defined in session.js
	};

	this.isDfarClause = function isDfarClause() { // CJ-746
		return this.name.startsWith(DFAR_PREFIX); // startsWith() defined in session.js
	};
	
	this.isLocalClause = function isLocalClause() { // CJ-746
		return !(this.isFarClause() || this.isDfarClause()); 
	};
	
	this.isFullText = function isFullText() { // CJ-746
		return ('F' == this.inclusion); 
	};

};

//-------------------------------------------------------

function ClauseSet() {
	this.elements = new Array();

	this.retrieve = function retrieve() {
		var instance = this;
		$.ajax({
			url: getAppPath() + 'page?do=doc-ref-clause',
			cache: false,
			dataType: 'json',
			success: function(data) {
				if (!instance.loadJson(data.data)) {
					goHome();
				}
			},
			error: function(object, text, error) {
				goHome();
			}
		});
	};
	
	this.loadJson = function loadJson(json) {
		this.elements.length = 0;
		var oClauseItem;
		for (var iData = 0; iData < json.length; iData++) {
			oClauseItem = new ClauseItem();
			oClauseItem.loadJson(json[iData]);
			this.elements.push(oClauseItem);
		}
		return true;
	};
	
	this.resolveObjectReferences = function resolveObjectReferences(aSections, aQuestions, oDocClauseSet, oDocFillInSet) {
		var oClauseItem, oDocClauseItem;
		for (var iEle = 0; iEle < this.elements.length; iEle++) {
			oClauseItem = this.elements[iEle];
			oClauseItem.resolveObjectReferences(aSections, aQuestions, this, oDocFillInSet);
			oDocClauseItem = oDocClauseSet.findByClauseId(oClauseItem.id);
			if (oDocClauseItem != null) {
				oClauseItem.isCompleted = oDocClauseItem.isCompleted;
				oClauseItem.optionalUserActionCode = oDocClauseItem.optionalUserActionCode;
				//oClauseItem.isRemovedByUser = oDocClauseItem.isRemovedByUser;
				// CJ-358
				oClauseItem.isVisible = true;
				oClauseItem.cacheApplicable = true;

			}
		}
	};
	
	this.prepareEvals = function prepareEvals(aQuestions) {
		for (var iEle = 0; iEle < this.elements.length; iEle++) {
			// var oClauseItem = this.elements[iEle];
			this.elements[iEle].prepareEvals(aQuestions, this);
		}
	};

	// CJ-802
	this.checkOptionCondition = function checkOptionCondition() {
		for (var iEle = 0; iEle < this.elements.length; iEle++) {
			// var oClauseItem = this.elements[iEle];
			this.elements[iEle].checkOptionCondition();
		}
	};
	
	this.findById = function findById(id) {
		var oClauseItem;
		for (var iEle = 0; iEle < this.elements.length; iEle++) {
			oClauseItem = this.elements[iEle];
			if (oClauseItem.id == id)
				return oClauseItem;
		}
		return null;
	};
	
	this.findByName = function findByName(name) {
		name = name.toUpperCase();
		var oClauseItem;
		for (var iEle = 0; iEle < this.elements.length; iEle++) {
			oClauseItem = this.elements[iEle];
			if (oClauseItem.name.toUpperCase() == name)
				return oClauseItem;
		}
		return null;
	};
	
	this.getClausesByPrescription = function getClausesByPrescription(prescriptionId) {
		var result = new Array();
		var oClauseItem;
		for (var iEle = 0; iEle < this.elements.length; iEle++) {
			oClauseItem = this.elements[iEle];
			if (oClauseItem.hasPrescription(prescriptionId))
				result.push(oClauseItem);
		}
		return result;
	};
	
	this.finalCheck = function finalCheck(oFinalCheckSet, oPrescriptionChoices) {
		var iEle;
		//var oClauseItem;
		for (iEle = 0; iEle < this.elements.length; iEle++) {
			//oClauseItem = this.elements[iEle];
			this.elements[iEle].finalCheck(oFinalCheckSet, oPrescriptionChoices);
		}
		if (_isManualOrders == false) {
			for (iEle = this.elements.length - 1; iEle >= 0; iEle--) {
				//oClauseItem = this.elements[iEle];
				this.elements[iEle].finalCheck(oFinalCheckSet, oPrescriptionChoices);
			}
			for (iEle = this.elements.length - 1; iEle >= 0; iEle--) {
				// oClauseItem = this.elements[iEle];
				this.elements[iEle].evalAdditionalCondition(oFinalCheckSet, oPrescriptionChoices);
			}
		}
		_interview.doc.unsavedChanges = true;
		window.onbeforeunload = confirmOnInterviewExit;
	};
	
	this.onQuestionAnswerChanged = function onQuestionAnswerChanged(oPrescriptionChoices) {
		var iEle;
		for (iEle = 0; iEle < this.elements.length; iEle++) {
			//var oClauseItem = this.elements[iEle];
			//if (_changeEventItems.hasClause(oClauseItem) == false)
			this.elements[iEle].onQuestionAnswerChanged(oPrescriptionChoices);
		}
		_interview.doc.unsavedChanges = true;
		window.onbeforeunload = confirmOnInterviewExit;
		/*
		for (iEle = this.elements.length - 1; iEle >= 0; iEle--) {
			var oClauseItem = this.elements[iEle];
			//if (_changeEventItems.hasClause(oClauseItem) == false)
				oClauseItem.onQuestionAnswerChanged(oPrescriptionChoices);
		}
		*/
	};
	
	/* CJ-1005
	this.genOrderSelOptions = function genOrderSelOptions(pFarTableId, pDfarTableId, poSelected) {
		var aSorted = new Array();
		var aSortedFars = new Array();
		var aSortedDfars = new Array();
		var iEle, sFars = '', sDfars = '';
		var oFarSection = null, oDfarSection = null;
		var oClauseItem;
		for (iEle = 0; iEle < this.elements.length; iEle++) {
			oClauseItem = this.elements[iEle];
			if (oClauseItem.isAllowed() == false)
				continue;
			//aSorted.push(oClauseItem);
			var bSelected = false;
			if (poSelected)
				bSelected = poSelected.indexOf(oClauseItem) >= 0;
			var sItem = oClauseItem.genOrderSelDivHtml(bSelected, 0);
			if (oClauseItem.isDFar()) { // 2: DFARS
				if ((oDfarSection == null) || (oDfarSection != oClauseItem.oSection)) {
					oDfarSection = oClauseItem.oSection;
					sDfars += '<div class="ordersSection">Section ' + oDfarSection.name + ' - ' + oDfarSection.header.replace(/\//g, " / ") + '</div>';
				}
				sDfars += sItem;
				aSortedDfars.push(oClauseItem);
			} else {
				if ((oFarSection == null) || (oFarSection != oClauseItem.oSection)) {
					oFarSection = oClauseItem.oSection;
					sFars += '<div class="ordersSection">Section ' + oFarSection.name + ' - ' + oFarSection.header.replace(/\//g, " / ") + '</div>';
				}
				sFars += sItem;
				aSortedFars.push(oClauseItem);
			}
		}
		$('#' + pFarTableId).html(sFars);
		$('#' + pDfarTableId).html(sDfars);
		sFars = '', sDfars = '';

		aSortedFars.sort(function(a, b) {
			if (a.name < b.name)
				return -1;
			if (a.name > b.name)
				return 1;
			return 0; 
		});
		
		aSortedDfars.sort(function(a, b) {
			if (a.name < b.name)
				return -1;
			if (a.name > b.name)
				return 1;
			return 0; 
		});
		
		aSorted = aSortedFars.concat(aSortedDfars);
		
		return aSorted;
	};
	*/
}

// QuestionConditionItem =============================================================================================
function QuestionConditionItem() {
	//this.id;
	this.sequence;
	this.questionId;
	this.originalCondition;
	this.conditionToQuestion;
	this.parentId; // Hierarchy
	this.defaultBaseline; // CJ-584
	
	this.question = null;
	this.oEvaluation = null;

	this.loadJson = function loadJson(json) {
		//this.id = json.question_condition_id;
	    this.sequence = json.question_condition_sequence;
	    this.questionId = json.question_id;
	    this.originalCondition = json.original_condition_text;
	    this.conditionToQuestion = json.condition_to_question;
	    this.parentId = json.parent_question_id;
		this.defaultBaseline = json.default_baseline; // CJ-584
		//if ((this.questionId == 36541) || (this.questionId == 36548))
		//	this.defaultBaseline = true;
	};
	
	this.resolveQuestion = function resolveQuestion(questions) {
		this.question = questions.findById(this.questionId);
		if (this.question) {
			this.question.conditionItem = this;
			if (this.parentId) {
				var oParentQuestion = questions.findById(this.parentId);
				if (oParentQuestion) {
					oParentQuestion.children.push(this.question);
					this.question.parent = oParentQuestion; 
				}
			}
		}
	
		if (this.conditionToQuestion) {
			this.oEvaluation = new QuestionEvaluation();
			this.oEvaluation.parseStatement(this.conditionToQuestion, questions, this.question);
		}
		
		return this.question;
	};
	
	this.cacheApplicable = null;
	this.isApplicable = function isApplicable() {
		if (this.oEvaluation) {
			if (this.cacheApplicable == null) {
				this.cacheApplicable = this.oEvaluation.isApplicable();
			}
			return this.cacheApplicable;
		} else
			return true;
	};
	
	this.hasCodes = function hasCodes(aCodes) {
		if (this.oEvaluation != null) {
			return this.oEvaluation.hasCodes(aCodes);
		}
		return false;
	};
	
	this.isChangeEffected = function isChangeEffected() {
		if (this.oEvaluation == null)
			return false;
		return this.oEvaluation.isChangeEffected(_changeEventItems);
	};
	
	this.checkAnswers = function checkAnswers() {
		if (this.originalCondition && this.question) {
			// Evaluates NOT ... TOGETHER expression only
			var aValues = null;
			var bNot = false;
			var sTest = 'SELECT ONE OR MORE BUT ';
			var iPos = this.originalCondition.indexOf(sTest);
			if (iPos >= 0) {
				var sEval = this.originalCondition.substr(iPos + sTest.length);
				sTest = "NOT ";
				iPos = sEval.indexOf(sTest);
				if (iPos == 0) {
					bNot = true;
					sEval = sEval.substr(sTest.length);
				}
				sTest = " TOGETHER";
				iPos = sEval.indexOf(sTest);
				if (iPos > 0) {
					var sItem = sEval.substr(iPos);
					if (sItem == sTest) {
						sEval = sEval.substr(0, iPos);
						aValues = sEval.split(' AND ');
					}
				}
			}
			if (aValues) {
				var aAnswers = this.question.getAnswerValues();
				if ((aAnswers == null) || (aAnswers.length == 0)) {
					alert(this.originalCondition);
					return false;
				}
				var iMatched = 0;
				for (var iItem = 0; iItem < aValues.length; iItem++) {
					sTest = aValues[iItem];
					iPos = sTest.indexOf('"');
					if (iPos == 0)
						sTest = sTest.substr(1, sTest.length - 2);
					iPos = aAnswers.indexOf(sTest);
					if (iPos >= 0) {
						iMatched++;
					}
				}
				if (iMatched == aValues.length) {
					alert(this.originalCondition);
					return false;
				}
			}
		}
		return true;
	} 
};

//-------------------------------------------------------
function QuestionConditionSet() {
	this.elements = new Array();

	this.retrieve = function retrieve() {
		var instance = this;
		$.ajax({
			url: getAppPath() + 'page?do=doc-ref-question_condition',
			cache: false,
			dataType: 'json',
			success: function(data) {
				if (!instance.loadJson(data.data)) {
					goHome();
				}
			},
			error: function(object, text, error) {
				goHome();
			}
		});
	};

	this.loadJson = function loadJson(json) {
		this.elements.length = 0;
		var oQuestionConditionItem;
		for (var iData = 0; iData < json.length; iData++) {
			oQuestionConditionItem = new QuestionConditionItem();
			oQuestionConditionItem.loadJson(json[iData]);
			this.elements.push(oQuestionConditionItem);
		}
		return true;
	};
	
	this.findById = function findById(id) {
		var oQuestionConditionItem;
		for (var iEle = 0; iEle < this.elements.length; iEle++) {
			oQuestionConditionItem = this.elements[iEle];
			if (oQuestionConditionItem.id == id)
				return oQuestionConditionItem;
		}
		return null;
	};
	
	this.loadAnswers = function loadAnswers(oDocAnswerSet) {
		if (oDocAnswerSet.isEmpty())
			return;
		var oQuestionConditionItem, oQuestionItem, oDocAnswerItem;
		for (var iEle = 0; iEle < this.elements.length; iEle++) {
			oQuestionConditionItem = this.elements[iEle];
			oQuestionItem = oQuestionConditionItem.question;
			if (oQuestionItem) {
				oDocAnswerItem = oDocAnswerSet.findByQuestionId(oQuestionItem.id);
				if (oDocAnswerItem != null) {
					oQuestionItem.loadAnswers(oDocAnswerItem);
				}
			}
		}
	};	

	// ???????????????????????????????????????????????????????????????????????????????????????????????????
	this.saveAnswers = function saveAnswers(oDocAnswerSet) {
		var oQuestionItem, sAnswer, oDocAnswerItem;
		for (var iEle = 0; iEle < this.elements.length; iEle++) {
			// var oQuestionConditionItem = this.elements[iEle];
			oQuestionItem = this.elements[iEle].question;
			if (oQuestionItem) {
				sAnswer = oQuestionItem.getAnswerToSave();
				if ((sAnswer != null) && sAnswer) {
					oDocAnswerItem = new DocAnswerItem();
					oDocAnswerItem.questionId = oQuestionItem.id;
					oDocAnswerItem.answer = sAnswer;
					oDocAnswerSet.add(oDocAnswerItem);
				}
			}
		}
	};
	
	this.setToManualOrders = function setToManualOrders() {
		for (var iEle = 0; iEle < this.elements.length; iEle++) {
			//var oQuestionConditionItem = this.elements[iEle];
			var oQuestionItem = this.elements[iEle].question;
			if (oQuestionItem) {
				oQuestionItem.setToManualOrders();
			}
		}
	};

}

// ===========================================================================================================================
genChoiceEvent = function(inputType, oQuestion, oBaseQuestion, choiceId) {
 	var sSubEvent = ('N' == inputType) ? 'onblur' : 'onclick';
	var sEvent = ' ' + sSubEvent;
	return sEvent + '="onChoiceChanged(this, \'' + inputType + '\', ' + oQuestion.id + ', ' + oBaseQuestion.id + (choiceId ? ', ' + choiceId : '') + ')"'; 
}

onChoiceChanged = function (oInput, inputType, questionId, baseQuestionId, choiceId) {
	//var sMsg = 'onChoiceChanged("' + inputType + '", ' + questionId + ', ' + baseQuestionId + (choiceId ? ', ' + choiceId : '') + ')';
	//alert(sMsg);
	var value;
	if ('N' == inputType) {
		value = oInput.value.trim();
		if (value.length <= 0)
			return;
		if (!isValidNumber(value)) {
			//alert("Invalid number");
			return;
		}
		/*
		try { // IE8 issue
			if (!oInput.checkValidity())
				return;
		} catch (error) { }
		value = oInput.value;
		*/
	} else
		value = oInput.checked;
	_interview.doc.unsavedChanges = true;
	_interview.onInputChanged(questionId, baseQuestionId, choiceId, value);
	window.onbeforeunload = confirmOnInterviewExit;
	if (_oUserInfo) // CJ-696
		_oUserInfo.timelyRefreshTimeout();
}

// PrescriptionChoices =======================================================================================================
function PrescriptionChoices() {
	this.questionChoiceItems = new Array();
	this.prescriptions = new Array();
	this.clauses = new Array();
	this.crossoverClauses = new Array();
	this.needToDisplay = false;
	this.oTablePrescription = null;
	
	this.saveAnswers = function saveAnswers(oDocClauseSet, oDocFillInSet) {
		for (var item = 0; item < this.clauses.length; item++) {
			var oClause = this.clauses[item];
			var bApplicable = (_isManualOrders || oClause.isApplicable(this, false)); // this.clauses
			if ((bApplicable) 
				//  When saving, include removed records so that they aren't deleted.
				|| ((oClause.optionalUserActionCode != null) && (oClause.optionalUserActionCode == 'R')))
			{			
				var oDocClauseItem = new DocClauseItem();
				oDocClauseItem.clauseId = oClause.id;
				oDocClauseItem.isCompleted = oClause.isCompleted;
				oDocClauseItem.optionalUserActionCode = oClause.optionalUserActionCode;
				// oDocClauseItem.isRemovedByUser = oClause.isRemovedByUser;
				oDocClauseSet.add(oDocClauseItem);
				if (oClause.optionalUserActionCode != 'R') { // CJ-745
					oClause.saveAnswers(oDocFillInSet);
				}
			}
		}
	};
	
	this.loadAnswers = function loadAnswers(oDocClauseSet, oClauseSet) {
		var sLogNotFoundIds = '';
		var oItems = oDocClauseSet.elements;
		var clauseId, oClause;
		for (var iClause = 0; iClause < oItems.length; iClause++) {
			//var oDocClauseItem = oItems[iClause];
			clauseId = oItems[iClause].clauseId;
			oClause = oClauseSet.findById(clauseId);
			if (oClause) {
				if (this.clauses.indexOf(oClause) < 0) {
					this.clauses.push(oClause);
					if (this.crossoverClauses.indexOf(oClause) < 0)
						this.crossoverClauses.push(oClause);
					oClause.show(true, this);
				}
			} else {
				sLogNotFoundIds += (sLogNotFoundIds ? ', ' : '') + clauseId;
			}
		}
		if (sLogNotFoundIds) {
			logOnConsole('PrescriptionChoices.loadAnswers(): invalid Clause Ids: ' + sLogNotFoundIds);
		}
	};
	
	this.postChangeEvent = function postChangeEvent() {
		var oClause;
		for (var item = 0; item < this.clauses.length; item++) {
			oClause = this.clauses[item];
			if (oClause.isChangeEffected()) {
				var bApplicable = oClause.isApplicable(this, true); // this.clauses
				oClause.show(bApplicable, this);
				//oClause.showCrossoverApplicable (bApplicable, this);
				_changeEventItems.addClause(oClause);
			}
		}
	};
	
	/* CJ-1005
	this.preapreOrdersInterface = function preapreOrdersInterface() {
		if (_isManualOrders == false) {
			var aOldClauses = this.clauses.slice(0);
			this.clauses = new Array();
			var oClause, bApplicable;
			for (var item = 0; item < aOldClauses.length; item++) {
				oClause = aOldClauses[item];
				bApplicable = oClause.isApplicable(this, false); // this.clauses
				if (bApplicable)
					this.clauses.push(oClause);
			}
		}
		return this.clauses;
	};
	*/
	
	this.setOrderClause = function setOrderClause(oClause, bSelect) {
		var iPos = this.clauses.indexOf(oClause);
		if (bSelect) {
			if (iPos < 0)
				this.clauses.push(oClause);
		} else {
			if (iPos >= 0)
				this.clauses.splice(iPos, 1);
		}
		oClause.show(bSelect, this);
	}
	
	this.clauseAdded = function clauseAdded(oClause, bInitialize) {
		
		// CJ-358 - on page load, don't overwrite the cache.
		var bClearCache = bInitialize || (oClause.cacheApplicable == false); // if true, page load or clear all items	
		if (oClause.cacheApplicable == null)								// if null, then this is a removed clause
			oClause.cacheApplicable = false;
		var bApplicable = oClause.isApplicable(this, bClearCache); // (this, true);	// this.clauses

		if (!bInitialize)
			oClause.show(bApplicable, this);
		_changeEventItems.addClause(oClause);
		return bApplicable;
		//if (oClause.isApplicable(this.clauses, false))
		//	_interview.adjustProgressBar();
	};
	
	this.clauseRemoved = function clauseRemoved(oClause) {
		var bApplicable = oClause.isApplicable(this, true); // this.clauses 
		oClause.cacheApplicable = null;
		oClause.show(false, this);
		_changeEventItems.addClause(oClause);
		return bApplicable;
		//if (oClause.isApplicable(this.clauses, false))
		//	_interview.adjustProgressBar();
	};
	
	this.updateCrossoverClause = function updateCrossoverClause(oClause, bAdd) {
		var iPosClause = this.clauses.indexOf(oClause);
		var iPosCross = this.crossoverClauses.indexOf(oClause);
		if (bAdd) {
			if (iPosClause < 0)
				this.clauses.push(oClause);
			if (iPosCross < 0) {
				this.crossoverClauses.push(oClause);
				return true;
			} else {
				return false;
			}
		} else {
			if (iPosClause >= 0)
				this.clauses.splice(iPosClause, 1);
			if (iPosCross < 0) {
				return false;
			} else {
				this.crossoverClauses.splice(iPosCross, 1);
				return true;
			}
		}
	}
	/*
	this.updateCrossoverApplicable = function updateCrossoverApplicable(oClause, bApplicable) {
			
		var aCondition;
		for (var index = 0; index < oClause.arrAdtnlCond.length; index++) {
			aCondition = oClause.arrAdtnlCond[index];
			//Removed, AddCond no longer require these checks.
			//if (aCondition.newClause.oprAdtnlCond == 1) { // Do not Include, 
			//	if (bApplicable == true)				// if the parent clause is applicable.
			//		aCondition.newClause.cacheApplicable = false;
			//}
			//else 
				aCondition.newClause.cacheApplicable = bApplicable;
		}
	}
	*/
	this.isApplicableClause = function isApplicableClause(oClause) {
		if (this.clauses.indexOf(oClause) >= 0) {
			return oClause.isApplicable(this, false); // this.clauses
		} else
			return false;
	};
	
	this.hasClause = function hasClause(oClause) {
		return (this.clauses.indexOf(oClause) >= 0);
	};
	
	this.prescriptionAdded = function prescriptionAdded(oPrescription, bInitialize) {
		var bApplicableChanged = false;
		var html = oPrescription.genHtmlRows();
		$('#' + TABLE_PRESCRIPTION + ' tbody').append(html);
		var clauses = oPrescription.getClauses();
		var oClause;
		for (var index = 0; index < clauses.length; index++) {
			oClause = clauses[index];
			if (this.clauses.indexOf(oClause) <= 0) {
				this.clauses.push(oClause);
				bApplicableChanged |= this.clauseAdded(oClause, bInitialize);
			}
		}
		return bApplicableChanged;
	};
	
	this.prescriptionRemoved = function prescriptionRemoved(oPrescription) {
		var bApplicableChanged = false;
		var aFormIds = oPrescription.getFormIds();
		for (var index = 0; index < aFormIds.length; index++) {
			$('#' + aFormIds[index]).remove();
		}
		var clauses = oPrescription.getClauses();
		for (var index = 0; index < clauses.length; index++) {
			var oClause = clauses[index];
			if (!this.hasOtherClause(oClause, oPrescription)) {
				var iPos = this.clauses.indexOf(oClause);
				if (iPos >= 0) {
					this.clauses.splice(iPos, 1);
					bApplicableChanged |= this.clauseRemoved(oClause);
				}
			}
		}
		return bApplicableChanged;
	};
	
	this.hasOtherClause = function hasOtherClause(oClause, excludePrescription) {
		/* Removed prescription link
		for (var item = 0; item < this.prescriptions.length; item++) {
			var oPrescription = this.prescriptions[item];
			if (oPrescription != excludePrescription) {
				if (oPrescription.containClause(oClause))
					return true;
			}
		}
		*/
		return false;
	};
	
	this.addChoiceItem = function addChoiceItem(questionChoiceItem, oQuestion, bInitialize) {
		if ((questionChoiceItem.aPrescriptions == null) || (this.questionChoiceItems.indexOf(questionChoiceItem) >= 0))
			return false;
		
		var bApplicableChanged = false;
		var aPrescriptions = questionChoiceItem.aPrescriptions;
		var added = false;
		var oPrescription;
		for (var item = 0; item < aPrescriptions.length; item++) {
			oPrescription = aPrescriptions[item];
			if (oPrescription != null) {
				if (this.prescriptions.indexOf(oPrescription) < 0) {
					this.prescriptions.push(oPrescription);
					bApplicableChanged |= this.prescriptionAdded(oPrescription, bInitialize);
					added = true;
				}
			}
		}
		if (added)
			this.questionChoiceItems.push(questionChoiceItem);
		return bApplicableChanged;
	};
	
	this.removeChoiceItem = function removeChoiceItem(questionChoiceItem, oQuestion) {
		if (questionChoiceItem.aPrescriptions == null)
			return;
		
		var index = this.questionChoiceItems.indexOf(questionChoiceItem);
		if (index < 0)
			return;

		var bApplicableChanged = false;
		/* Removed prescription link
		var aPrescriptions = questionChoiceItem.aPrescriptions;
		for (var item = 0; item < aPrescriptions.length; item++) {
			var oPrescription = aPrescriptions[item];
			if (oPrescription != null) {
				if (this.hasOtherPrescription(oPrescription, questionChoiceItem)) {
					continue;
				}
				var iPos = this.prescriptions.indexOf(oPrescription);
				if (iPos >= 0) {
					this.prescriptions.splice(iPos, 1);
					bApplicableChanged |= this.prescriptionRemoved(oPrescription);
				}
			}
		}
		*/
		this.questionChoiceItems.splice(index, 1);
		return bApplicableChanged;
	};
	
	this.hasOtherPrescription = function hasOtherPrescription(oPrescription, excludeQuestionChoiceItem) {
		/* Removed prescription link
		var aChoiceItems = this.questionChoiceItems;
		for (var item = 0; item < aChoiceItems.length; item++) {
			var oChoiceItem = aChoiceItems[item];
			if (oChoiceItem != excludeQuestionChoiceItem) {
				if (oChoiceItem.containPrescription(oPrescription))
					return true;
			}
		}
		*/
		return false;
	};
	
	this.onInputChanged = function onInputChanged(questionChoiceItem, value, oQuestion, bInitialize) {
		var bApplicableChanged;
		if (value)
			bApplicableChanged = this.addChoiceItem(questionChoiceItem, oQuestion, bInitialize);
		else
			bApplicableChanged = this.removeChoiceItem(questionChoiceItem, oQuestion);

		//var aCodes = [oQuestion.name];
		var oClause, bApplicable;
		for (var item = 0; item < this.clauses.length; item++) {
			oClause = this.clauses[item];
			if (oClause.isChangeEffected()) { // if (oClause.hasQuestionCodes(aCodes, false)) {
				bApplicable = oClause.isApplicable(this, bInitialize); // (this, true); // CJ-358
				if (bApplicable) {
					_changeEventItems.addClause(oClause);
					bApplicableChanged = true;
				}
				//bApplicableChanged |= bApplicable; 
				if (!bInitialize)
					oClause.show(bApplicable, this);
			}
		}
		if (bApplicableChanged && (!bInitialize))
			_interview.adjustProgressBar(2);
	}
	
	this.genHtml = function genHtml() {
		var html = '<table id="' + TABLE_PRESCRIPTION + '">'
			+ '<thead><tr><th>Prescription</th><th colspan="2">Clause</th></tr></thead>'
			+ '<tbody>';
		for (var item = 0; item < this.prescriptions.length; item++) {
			//var oPrescription = this.prescriptions[item];
			html += this.prescriptions[item].genHtmlRows();
		}
		html += '</tbody></table>';
		return html;
	};
	
	// CJ-1171, rewrite for clarity.
	this.getProgress = function getProgress(oProgress) {
		var iTotal = 0, iAnswered = 0;
		var iRegularFillinTotal = 0; iRegularFillinAnswered = 0;
		var iEditFillinTotal = 0; iEditFillinAnswered = 0;
		var oClause, bApplicable;
		for (var item = 0; item < this.clauses.length; item++) {
			oClause = this.clauses[item];
			if (oClause.hasFillin()) {
				bApplicable = oClause.isApplicable(this, false); // this.clauses
				if (bApplicable) {
					
					var bHasRegFillin = oClause.hasRegularFillin();
					var bAnsRegFillin = bHasRegFillin && oClause.isCompleted;
					
					var bHasEditFillin = oClause.editable && oClause.hasEditFillin();
					var bAnsEditFillin = bHasEditFillin && oClause.isEditFillinCompleted();
					
					// CJ-1171, Check conditional editables.
					if (bHasEditFillin && !bAnsEditFillin && oClause.oEditCondition 
						&& !oClause.oEditCondition.evaluate(oClause.editableRemarks)) 
						bHasEditFillin = false;
							
					var bAnswered = (!bHasRegFillin || bAnsRegFillin)
									&& (!bHasEditFillin || bAnsEditFillin);
					
					if (bHasRegFillin) 
						iRegularFillinTotal++;
					if (bHasEditFillin)
						iEditFillinTotal++;
					if (bAnsRegFillin)
						iRegularFillinAnswered++;
					if (bAnsEditFillin)
						iEditFillinAnswered++;
					
					if ((bHasRegFillin) || (bHasEditFillin)) { 
						iTotal++;
						
						if (bAnswered)
							iAnswered++;
					}
						
				}
			}
		}
		oProgress.totalClausesForInput = iTotal;
		oProgress.answeredClausesForInput = iAnswered;
		
		oProgress.totalFillInClauses = iRegularFillinTotal;
		oProgress.answeredFillinClauses = iRegularFillinAnswered;
		
		oProgress.totalEditFillInClauses = iEditFillinTotal;
		oProgress.answeredEditFillinClauses = iEditFillinAnswered;
	};
	/*
	this.getProgress = function getProgress(oProgress) {
		var iTotal = 0, iAnswered = 0;
		var iRegularFillinTotal = 0; iRegularFillinAnswered = 0;
		var iEditFillinTotal = 0; iEditFillinAnswered = 0;
		var oClause, bApplicable;
		for (var item = 0; item < this.clauses.length; item++) {
			oClause = this.clauses[item];
			if (oClause.hasFillin()) {
				bApplicable = oClause.isApplicable(this, false); // this.clauses
				if (bApplicable) {
					iTotal++;
					if (oClause.isCompleted) // hasAnyAnswer() hasAllAnswered()
						iAnswered++;
		
					// Count Regular fillins
					if (oClause.hasRegularFillin()) {
						iRegularFillinTotal++;
						if (oClause.isCompleted)
							iRegularFillinAnswered++;
					}
					if (!oClause.editable)
					{
						// CJ-1139, Don't mark as answered if incomplete fillins
						if (!oClause.hasRegularFillin()) 
							iAnswered++;
						continue;
					}
					// Count Edit fillins
					if (oClause.hasEditFillin()) { 
						
						// CJ-1171, Check conditional editables.
						if (!oClause.isEditFillinCompleted()) {
							if (oClause.oEditCondition 
								&& !oClause.oEditCondition.evaluate(oClause.editableRemarks)) {
								iTotal--;
								continue;
							}		
						}
						
						iEditFillinTotal++;
						
						if (oClause.isEditFillinCompleted())
							iEditFillinAnswered++;
						if ((!oClause.isCompleted) && oClause.isEditFillinCompleted())
							iAnswered++;
					}
				}
			}
		}
		oProgress.totalClausesForInput = iTotal;
		oProgress.answeredClausesForInput = iAnswered;
		
		oProgress.totalFillInClauses = iRegularFillinTotal;
		oProgress.answeredFillinClauses = iRegularFillinAnswered;
		
		oProgress.totalEditFillInClauses = iEditFillinTotal;
		oProgress.answeredEditFillinClauses = iEditFillinAnswered;
	};
	*/
	this.findClauseById = function findClauseById(id) {

		var oClause = null;
		for (var index = 0; index < this.clauses.length; index++) {
			oClause = this.clauses[index];
			if (oClause.id == id)
				return oClause;
		}
		return null;
	};
}

// QuestionChoiceItem ==================================================================================================
function QuestionChoiceItem() {
	this.id;
	this.choice;
	this.answer = null;
	this.previousAnswer = null; // CJ-1112
	this.promptAfterSelection = null; // CJ-547
	this.prescriptionIds = new Array();
	this.aPrescriptions = null;
	this.questionItem = null;

	this.loadJson = function loadJson(json) {
		this.id = json.question_choce_id;
		this.choice = json.choice_text;
		this.promptAfterSelection = json.prompt_after_selection; 
		this.prescriptionIds.length = 0;
		if (json.prescriptionIds) {
			var value;
			for (var index = 0; index < json.prescriptionIds.length; index++) {
				value = json.prescriptionIds[index];
				this.prescriptionIds.push(value.prescription_id);
			}
		}
		this.aPrescriptions = new Array(this.prescriptionIds.length);
	};
	
	this.resolveObjectReferences = function resolveObjectReferences(questionItem, prescriptions) {
		this.questionItem = questionItem;
		for (var item = 0; item < this.prescriptionIds.length; item++) {
			this.aPrescriptions[item] = prescriptions.findById(this.prescriptionIds[item]);
		}
	};
	
	this.genTranscriptHtml = function genTranscriptHtml(oQuestion) {
		var html = this.choice;
		var sPresc = this.getPrescriptionCodes();
		if (sPresc)
			html += ' <span class="normal">[' + sPresc + ']';
		return html;
	}
	
	this.getFormId = function getFormId() {
		return QUESTION_CHOICE_PREFIX + this.id;
	};
	
	this.hasPrescription = function hasPrescription(id) {
		return (this.prescriptionIds.indexOf(id) >= 0);
	};

	this.containPrescription = function containPrescription(oPrescription) {
		return (this.aPrescriptions.indexOf(oPrescription) >= 0);
	};

	this.getPrescriptionCodes = function getPrescriptionCodes() {
		var sPrescription = '';
		if (this.aPrescriptions) {
			var oPres;
			for (var item = 0; item < this.aPrescriptions.length; item++) {
				oPres = this.aPrescriptions[item];
				if (oPres)
					sPrescription += ((item == 0) ? '' : ', ') + oPres.name; 
			}
		}
		return sPrescription;
	}
	
	this.genTooltip = function genTooltip() {
		if (!this.aPrescriptions)
			return '';
		
		var sPrescription = '';
		var oPres;
		for (var item = 0; item < this.aPrescriptions.length; item++) {
			oPres = this.aPrescriptions[item];
			if (oPres) {
				sPrescription += '<li role="presentation"><a role="menuitem" '; // tabindex="-1"'; 508 compliance issue.
				if (oPres.url)
					sPrescription += ' href="' + oPres.url + '" target="_blank" title="Visit the site">' + oPres.name + '</a></li>';
				else
					sPrescription += '>' + oPres.name + '</a></li>';
			}
		}
		if (sPrescription == '')
			return '';
		
		// Note: <button aria-label="Prescription(s)"> presents a 508 compliance issue.
		// Jaws and VoiceOver read Title using modern browsers.
		// http://www.powermapper.com/tests/screen-readers/labelling/button-title-img-null-alt/
		
		var sId = 'qcim-' + this.id; 
		var sResult = ' <div class="dropdown" style="display:inline-block;margin-left:10px;">'
	        // + '<button class="btn btn-default btn-xs dropdown-toggle" aria-label="Prescription(s)" title="Prescription(s)" id="'
			+ '<button class="btn btn-default btn-xs dropdown-toggle"  title="Prescription(s)" id="'
	        + sId + '" data-toggle="dropdown"><img src="../assets/images/information.png" alt="Information" /> ' // <i class="fa fa-info-circle"></i>' // fa fa-flag-checkered
	        + ' <span class="caret"></span><span class="hide">Prescription(s)</span></button>'
	        + '<ul class="dropdown-menu" role="menu" aria-labelledby="' + sId + '">'
	        + sPrescription
	        + '</ul>'
	        + '</div>';

		return sResult;
	};
	
	this.genRadio = function genRadio(oQuestion, baseQuestion) {
		var value = this.choice.replace(/"/g, '&quot;');
		var sTooltip = '';
		var sItem = '<label><input type="radio" name="' + oQuestion.getIdPrefix()
			+ '" id="' + this.getFormId() + '"'
			+ ' value="' + value + '"'
			+ (this.answer ? ' checked ' : '')
			+ genChoiceEvent('1', oQuestion, baseQuestion, this.id)
			+ '> '
			+ this.choice
			+ '</label>'
			+ this.genTooltip()
			;
		return sItem;
	};
	
	this.genRadioRow = function genRadioRow(oQuestion, baseQuestion, bReadOnly) {
		var sFormId = this.getFormId();
		var sItem = '<tr>'
			+ '<td width="5%"><input type="radio" name="' + oQuestion.getIdPrefix()
			+ '" id="' + sFormId + '"'
			+ ' value="' + this.choice.replace(/"/g, '&quot;') + '"'
			+ (this.answer ? ' checked ' : '')
			+ genChoiceEvent('1', oQuestion, baseQuestion, this.id)
			+ (bReadOnly ? ' disabled="disabled"' : '')
			+ '></td>'
			+ '<td><label for="' + sFormId + '">' + this.choice
			+ '</label>'
			+ this.genTooltip()
			+ '</td>'
			+ '</tr>';
		return sItem;
	};
	
	this.genCheckbox = function genCheckbox(oQuestion, baseQuestion) {
		var value = this.choice.replace(/"/g, '&quot;'); 
		var sItem = '<label><input type="checkbox" name="' + this.getFormId()
			+ '" id="' + this.getFormId() + '"'
			+ ' value="' + value + '"'
			+ (this.answer ? ' checked ' : '')
			+ genChoiceEvent('M', oQuestion, baseQuestion, this.id)
			+ '> '
			+ this.choice
			+ '</label>'
			+ this.genTooltip()
			;
		return sItem;
	};
	
	this.genCheckboxRow = function genCheckboxRow(oQuestion, baseQuestion) {
		var sFormId = this.getFormId();
		var sItem = '<tr>'
			+ '<td><input type="checkbox" name="' + sFormId
			+ '" id="' + sFormId + '"'
			+ ' value="' + this.choice.replace(/"/g, '&quot;') + '"'
			+ (this.answer ? ' checked ' : '')
			+ genChoiceEvent('M', oQuestion, baseQuestion, this.id)
			+ '></td>'
			+ '<td><label for="' + sFormId + '">' + this.choice
			+ '</label>'
			+ this.genTooltip()
			+ '</td>'
			+ '</tr>';
		return sItem;
	};
	
	// Bev - 508 Compliance
	this.genDivCheckboxRow = function genDivCheckboxRow(oQuestion, baseQuestion) {
			var sFormId = this.getFormId();
			var sItem = '<div class="divChoice">'
				+ '<input type="checkbox" name="' + sFormId
				+ '" id="' + sFormId + '"'
				+ ' value="' + this.choice.replace(/"/g, '&quot;') + '"'
				+ (this.answer ? ' checked ' : '')
				+ genChoiceEvent('M', oQuestion, baseQuestion, this.id)
				+ '>'
				+ '<label for="' + sFormId + '">' + this.choice + '</label>'
				+ this.genTooltip()
				+ '</div>';
			return sItem;
	};
	
	this.genDivRadioRow = function genDivRadioRow(oQuestion, baseQuestion, bReadOnly) {
		var sFormId = this.getFormId();		
		var sItem = '<div class="divChoice">'
			+ '<input type="radio" name="' + oQuestion.getIdPrefix()
			+ '" id="' + sFormId + '"'
			+ ' value="' + this.choice.replace(/"/g, '&quot;') + '"'
			+ (this.answer ? ' checked ' : '')
			+ genChoiceEvent('1', oQuestion, baseQuestion, this.id)
			+ (bReadOnly ? ' disabled="disabled"' : '')
			+ '>'
			+ '<label for="' + sFormId + '">' + this.choice + '</label>'
			+ this.genTooltip()
			+ '</div>';
		return sItem;
	};

}

// QuestionChoiceSet --------------------------------------------------------------------
function QuestionChoiceSet() {
	this.elements = new Array();

	this.loadJson = function loadJson(json) {
		this.elements.length = 0;
		var oQuestionChoiceItem;
		for (var iData = 0; iData < json.length; iData++) {
			oQuestionChoiceItem = new QuestionChoiceItem();
			oQuestionChoiceItem.loadJson(json[iData]);
			this.elements.push(oQuestionChoiceItem);
		}
		return true;
	};
	
	this.resolveObjectReferences = function resolveObjectReferences(questionItem, prescriptions) {
		for (var iEle = 0; iEle < this.elements.length; iEle++) {
			// var oQuestionChoiceItem = this.elements[iEle];
			this.elements[iEle].resolveObjectReferences(questionItem, prescriptions);
		}
	};
	
	this.clear = function clear() {
		this.elements.length = 0;
	}

	this.hasPrescription = function hasPrescription(id) {
		for (var iEle = 0; iEle < this.elements.length; iEle++) {
			// var oQuestionChoiceItem = this.elements[iEle];
			if (this.elements[iEle].hasPrescription(id))
				return true;
		}
		return false;
	}
	
	this.getPromptAfterSelect = function getPromptAfterSelect() {
		var result = '';
		var oItem;
		for (var iEle = 0; iEle < this.elements.length; iEle++) {
			oItem = this.elements[iEle];
			if (oItem.answer && oItem.promptAfterSelection) {
				if (result)
					result += '\n' + oItem.promptAfterSelection;
				else
					result = oItem.promptAfterSelection;
			}
		}
		return result;
	} 
	
	this.findById = function findById(id) {
		var oQuestionChoiceItem;
		for (var iEle = 0; iEle < this.elements.length; iEle++) {
			oQuestionChoiceItem = this.elements[iEle];
			if (oQuestionChoiceItem.id == id)
				return oQuestionChoiceItem;
		}
		return null;
	}
	
	this.getPrescriptionDivId = function getPrescriptionDivId(prefix) {
		return prefix + '-prescriptions';
	};

	this.genRadioHtml = function genRadioHtml(oQuestion, baseQuestion, bReadOnly) {
		var result = "";
		
		// 508 Compliance
		// The calling function already adds a div and a fieldset.
		// So, table and fieldset SHOULD not be added.
		
		//var result = '<fieldset><legend class="hide">' 		
		//	+ (oQuestion.text ? oQuestion.text.replace(/'/g, '&quot;') : oQuestion.code)
		//	+ '</legend>';
	    //result += '<table class="tblChoices">';	// 508 Compliance - table not needed.	
		for (var item = 0; item < this.elements.length; item++) {
			//var oItem = this.elements[item];
			result += this.elements[item].genDivRadioRow(oQuestion, baseQuestion, bReadOnly);
		}
		//result += '</table></fieldset>';

		return result;
	}


	// Bev - 508 Compliance
	this.genInputHtml = function genInputHtml(oQuestion, baseQuestion) {
		var inputType = oQuestion.type;
		var prefix = oQuestion.getIdPrefix();
		var result = "";
		var bReadOnly = false;
		if (_isLinkedAward && (oQuestion == baseQuestion) && (baseQuestion.name == '[DOCUMENT TYPE]'))
			bReadOnly = (oQuestion.answer ? true : false); // true;
		if ("M" == inputType) {
			for (var item = 0; item < this.elements.length; item++) {
				result += this.elements[item].genDivCheckboxRow(oQuestion, baseQuestion);
			}
			result += '<p class="help-block">Choose One or More Values</p>';
		} else 
		if ("B" == inputType) {
			if (this.elements.length >= 1) {
				result = this.genRadioHtml(oQuestion, baseQuestion, bReadOnly);
			} else {
				result = '<fieldset><legend class="hide">' 
					+ (oQuestion.text ? oQuestion.text.replace(/'/g, '&quot;') : oQuestion.code)
					+ '</legend>';
				result += '<div class="divChoice"><label><input type="radio" name="' + prefix + '" value="YES" id="' + prefix + '_0"'
					+ (('YES' == oQuestion.answer) ? ' checked' : '') + '> YES</label>'
					+ '</div>'
					+ '<div class="divChoice"><label><input type="radio" name="' + prefix + '" value="NO" id="' + prefix + '_1"'
					+ (('NO' == oQuestion.answer) ? ' checked' : '') + '> NO</label>'
					+ '</div>';
				result += '</fieldset>';
			}
			result += '<p class="help-block">Choose One Value</p>';
		} else 
		if ("1" == inputType) {
			result = this.genRadioHtml(oQuestion, baseQuestion, bReadOnly);
			result += '<p class="help-block">Choose One Value</p>';
		} else 
		if ("N" == inputType) {
			var sValue = '';
			if (oQuestion.answer)
				sValue = ' value="' + oQuestion.answer + '" ';
			result = '<input type="text" maxlength="12" ' // number
				+ sValue
				+ genChoiceEvent('N', oQuestion, baseQuestion)
				+ ' name="' + QUESTION_CHOICE_PREFIX + prefix + '" id="' + QUESTION_CHOICE_PREFIX + prefix + '">';
			if (this.elements.length == 1) { // CJ-735
				result += this.elements[0].genTooltip();
			}
			result += '<label for="' + QUESTION_CHOICE_PREFIX + prefix + '" class="help-block">Enter Number Value</label>';
		} else 
		{ 
			result = 'Input Type: ' + inputType + '<br />Element: ' + this.elements.length;
		}
		//result += '<div id="' + this.getPrescriptionDivId(prefix) + '"></div>';

		return result;
	};
	
	this.getInputIds = function getInputIds(oQuestion) {
		var result = '';
		var inputType = oQuestion.type;
		if (("M" == inputType) || ("1" == inputType) || (("B" == inputType) && (this.elements.length >= 1))) {
			for (var item = 0; item < this.elements.length; item++) {
				//var oItem = this.elements[item];
				result += ((item == 0) ? '' : ',') + this.elements[item].getFormId();
			}
		} else {
			var prefix = oQuestion.getIdPrefix();
			if ("N" == inputType)
				result = QUESTION_CHOICE_PREFIX + prefix;
			else
				result = prefix + '_0,' + prefix + '_1';
		}
		return result;
	}
	
	this.onInputChanged = function onInputChanged(oQuestion, choiceId, value) {
		this.clearPreviousAnswers(oQuestion); // CJ-1112

		var inputType = oQuestion.type;
		if ("M" == inputType) {
			var oSelItem = this.findById(choiceId);
			if (oSelItem) {
				oSelItem.answer = value;
				if (QUESTION_CODE_PROCEDURE == oQuestion.name) { // CJ-476
					if (COMMERCIAL_PROCEDURE == oSelItem.choice)
						_interview.isCommercialProcedure = (oSelItem.answer) ? true : false;	
				}
				else if (QUESTION_CODE_SUPPLIES_OR_SERVICES == oQuestion.name) {
					if (SERVICES == oSelItem.choice)
						_interview.isSevice = (oSelItem.answer) ? true : false;	
				}
				_interview.prescriptionChoices.onInputChanged(oSelItem, value, oQuestion);
				if (this.isExcludedOption(oSelItem.choice)){
					for (var item = 0; item < this.elements.length; item++) {
						var oItem = this.elements[item];
						if (oItem != oSelItem) {
							if (oSelItem.answer == true){
								oItem.answer = false;
								$('#' + oItem.getFormId()).attr('checked', false);
								_interview.prescriptionChoices.onInputChanged(oItem, oItem.answer, oQuestion);
							}
						}
					}
				} else {
					for (var item = 0; item < this.elements.length; item++) {
						var oItem = this.elements[item];
						if ((oItem != oSelItem) && (this.isExcludedOption(oItem.choice))) {
							if (oSelItem.answer == true){
								oItem.answer = false;
								$('#' + oItem.getFormId()).attr('checked', false);
								_interview.prescriptionChoices.onInputChanged(oItem, oItem.answer, oQuestion);
							}
						}
					}
				}
				return oSelItem;
			}
		} else
		if (("1" == inputType) || (("B" == inputType) && (this.elements.length >= 1))) {
			var oSelItem = this.findById(choiceId);
			if (oSelItem) {
				oSelItem.answer = true;
				if (QUESTION_CODE_DOCUMENT_TYPE == oQuestion.name) { // CJ-606
					_interview.isAward = (AWARD == oSelItem.choice);
				}
				_interview.prescriptionChoices.onInputChanged(oSelItem, oSelItem.answer, oQuestion);
				for (var item = 0; item < this.elements.length; item++) {
					var oItem = this.elements[item];
					if (oItem != oSelItem) {
						if ((oItem.answer == null) || (oItem.answer == true)) {
							oItem.answer = false;
							_interview.prescriptionChoices.onInputChanged(oItem, oItem.answer, oQuestion);
						}
					}
				}
				return oSelItem;
			}
		} else {
			oQuestion.answer = value;
			//return null;
		}
		return null;
	};
	
	this.restorePreviousAnswers = function(oQuestion) { // CJ-1112
		var changed = false;
		var oItem;
		for (var item = 0; item < this.elements.length; item++) {
			oItem = this.elements[item];
			if (oItem.previousAnswer != null) {
				if (oItem.anwer == null) {
					oItem.anwer = oItem.previousAnswer;
					changed = true;
				}
				oItem.previousAnswer = null;
			}
		}
		if (oQuestion.previousAnswer != null) {
			if (oQuestion.previousAnswer != null) {
				oQuestion.answer = oQuestion.previousAnswer;
				changed = true;
			}
			oQuestion.previousAnswer = null;
		}
		return changed;
	}
	
	this.clearPreviousAnswers = function(oQuestion) { // CJ-1112
		var oItem;
		for (var item = 0; item < this.elements.length; item++) {
			oItem = this.elements[item];
			oItem.previousAnswer = null;
		}
		oQuestion.previousAnswer = null;
	}

	this.isExcludedOption = function isExcludedOption (oChoice) {
		if ((oChoice.indexOf('NONE OF THE ABOVE') == 0)
				|| (oChoice == 'NONE APPLY') 
				|| (oChoice == 'NONE OF THE LISTED SOURCES') 
				|| (oChoice == 'NO WAIVERS APPROVED')   
				|| (oChoice == 'NO EXEMPTION APPLIES') 
				|| (oChoice == 'NO EXCEPTIONS APPLY')
				|| (oChoice == 'NONE OF THE ABOVE SUBCATEGORIES')
				|| (oChoice == "VARIATION IN QUANITY DOES NOT APPLY" )
				|| (oChoice == "ALTHOUGH NOT LISTED ABOVE, OTHER SERVICE TYPES ARE BEING PROCURED" )
				|| (oChoice == "ACQUISITION OF A SUPPLY, BUT NOT ANY OF THE ABOVE LISTED ITEMS")) {
			return true;
		}else {
			return false;
		}
	};
	
	this.genTranscriptHtml = function genTranscriptHtml(oQuestion, pLabel, bFinalPage, baseFinalPageId, bNoLink) {
		var inputType = oQuestion.type;
		var bAddAnswerLink = false;
		var html = '';
		
		var bDisplayLink = true;
		if (typeof bNoLink != 'undefined')
			bDisplayLink = !bNoLink;
		
		if (("M" == inputType) || ("1" == inputType) || (("B" == inputType) && (this.elements.length >= 1))) {
			for (var item = 0; item < this.elements.length; item++) {
				var oItem = this.elements[item];
				
				var startEvent = '', endEvent = '';
				if (bDisplayLink) {
					if (bFinalPage)	
						startEvent = '<A HREF="#" onclick="javascript:onSkipToLink(\'' + baseFinalPageId + '\', \'' + oItem.questionItem.getDivId() + '\');">';
					else
						startEvent = '<A HREF="#" onclick="javascript:onGoToLink(\'' + oItem.questionItem.getDivId() + '\');">';
					endEvent = '</a>';
				}
				if (oItem.answer) {
					if (html == '') {
						if (pLabel)
							html += '<li>' + startEvent + pLabel + endEvent + '<ul>' ;
						else
							bAddLinkToAnswer = true;
					}
					if (bAddLinkToAnswer){
						bAddLinkToAnswer = false;	
						html += '<li>' + startEvent + '<strong>' + oItem.choice + '</strong>' + endEvent + '</li>' ;
					}
					else
						html += '<li><strong>' + oItem.choice + '</strong></li>';
				}
			}
			if (html !== '') {
				if (pLabel)
					html += '</ul></li>';
			}
		} else {
			var sAnswer = '';
			var startEvent = '', endEvent = '';
			if (bDisplayLink) {
				if (bFinalPage)	
					startEvent = '<A HREF="#" onclick="javascript:onSkipToLink(\'' + baseFinalPageId + '\', \'' + oQuestion.getDivId() + '\');">';
				else
					startEvent = '<A HREF="#" onclick="javascript:onGoToLink(\'' + oQuestion.getDivId() + '\');">';
				endEvent = '</a>';
			}
				
			if (oQuestion.answer != null) {
				if ("B" == inputType)
					sAnswer = (oQuestion.answer) ? 'Yes' : 'No';
				else
					sAnswer = oQuestion.answer;
				sAnswer = '<strong>' + sAnswer + '</strong>';
				html = '<li>' + startEvent + (pLabel ? pLabel + endEvent + '<br />' + sAnswer : sAnswer + endEvent ) + '</li>';
			}
		}
		return html;
	};

	
	this.hasAnswer = function hasAnswer(oQuestion) {
		var result = false;
		var inputType = oQuestion.type;
		if (("M" == inputType) || ("1" == inputType) || (("B" == inputType) && (this.elements.length >= 1))) {
			for (var item = 0; item < this.elements.length; item++) {
				//var oItem = this.elements[item];
				if (this.elements[item].answer) {
					result = true;
					break;
				}
			}
		} else {
			if (oQuestion.answer != null)
				result = true;
		}
		return result;
	};
	
	this.getAnswerValues = function getAnswerValues(oQuestion) {
		var result = null;
		var inputType = oQuestion.type;
		if (("M" == inputType) || ("1" == inputType) || (("B" == inputType) && (this.elements.length >= 1))) {
			var oItem;
			for (var item = 0; item < this.elements.length; item++) {
				oItem = this.elements[item];
				if (oItem.answer) {
					if (result == null)
						result = new Array();
					result.push(oItem.choice);
				}
			}
		} else {
			if (oQuestion.answer != null)
				result = [oQuestion.answer];
		}
		return result;
	};
	
	this.hasValue = function hasValue(poTarget, oQuestion) {
		var inputType = oQuestion.type;
		if (("M" == inputType) || ("1" == inputType) || (("B" == inputType) && (this.elements.length >= 1))) {
			var bHasAnswer = false;
			var bBoolean = ("M" != inputType);
			var oItem;
			for (var item = 0; item < this.elements.length; item++) {
				oItem = this.elements[item];
				if (oItem.answer != null) {
					if (oItem.answer) {
						bHasAnswer = true;
						if ((true == poTarget) || (oItem.choice == poTarget))
							return true;
					}
					else if (bBoolean)
						bHasAnswer = true;
				}
			}
			if (bHasAnswer)
				return false;
			return null;
		} else {
			if (oQuestion.answer == null)
				return null;
			else if (true == poTarget)
				return true;
			else
				return oQuestion.answer == poTarget;
		}
	}
	
	this.loadAnswers = function loadAnswers(oQuestion, oDocAnswerItem) {
		if (!oDocAnswerItem.answer)
			return false;
		var aValues = oDocAnswerItem.answer.split(DIVIDER);
		if (aValues.length < 1)
			return false;
		var bFound = false;
		var inputType = oQuestion.type;
		//oDocAnswerItem.answerClauses = new DocAnswerClauseSet();
		if (("M" == inputType) || ("1" == inputType) || (("B" == inputType) && (this.elements.length >= 1))) {
			var oItem;
			for (var item = 0; item < this.elements.length; item++) {
				oItem = this.elements[item];
				if (aValues.indexOf(oItem.choice) >= 0) {
					oItem.answer = true;
					if (QUESTION_CODE_PROCEDURE == oQuestion.name) { // CJ-476
						if (COMMERCIAL_PROCEDURE == oItem.choice)
							_interview.isCommercialProcedure = true;	
					}
					else if (QUESTION_CODE_SUPPLIES_OR_SERVICES == oQuestion.name) {
						if (SERVICES == oItem.choice)
							_interview.isSevice = true;	
					}
					else if (QUESTION_CODE_DOCUMENT_TYPE == oQuestion.name) {
						_interview.isAward = (AWARD == oItem.choice);	
					}
					//$('#' + oItem.getFormId()).attr('checked', true); //$('#' + oItem.getFormId() + ' :checked').removeAttr('checked');
					_interview.prescriptionChoices.onInputChanged(oItem, oItem.answer, oQuestion);
					bFound = true;
				}
			}
		} else {
			oQuestion.answer = aValues[0];
			//$('#' + oQuestion.getIdPrefix()).val(oQuestion.answer);
			bFound = true;
		}
		return bFound;
	};
	
	this.clearAnswers = function clearAnswers(oQuestion) {
		var bResult = false;
		var inputType = oQuestion.type;
		if (("M" == inputType) || ("1" == inputType) || (("B" == inputType) && (this.elements.length >= 1))) {
			var oItem, oInputs;
			for (var item = 0; item < this.elements.length; item++) {
				oItem = this.elements[item];
				oItem.previousAnswer = oItem.answer; // CJ-1112
				if (oItem.answer == true) {
					oItem.answer = false;
					_interview.prescriptionChoices.onInputChanged(oItem, true, oQuestion, true);
					oInputs = $('#' + oItem.getFormId());
					if (oInputs.length > 0)
						oInputs.removeAttr('checked'); // $('#' + oItem.getFormId() + ' :checked').removeAttr('checked');
					bResult = true;
				}
			}
		} else {
			oQuestion.previousAnswer = oQuestion.answer; // CJ-1112
			if (oQuestion.answer != null) {
				oQuestion.answer = null;
				bResult = true;
				$('#' + oQuestion.getIdPrefix()).val('');
			}
		}
		return bResult;
	};
	
	this.refreshControls = function refreshControls(oQuestion) { // CJ-584
		var inputType = oQuestion.type;
		if (("M" == inputType) || ("1" == inputType) || (("B" == inputType) && (this.elements.length >= 1))) {
			var oItem, oInputs;
			for (var item = 0; item < this.elements.length; item++) {
				oItem = this.elements[item];
				if (oItem.answer == true)
					$('#' + oItem.getFormId()).prop('checked', true);
				else
					$('#' + oItem.getFormId()).prop('checked', false);
					//$('#' + oItem.getFormId()).removeProp('checked');// CJ-785, Once prop is removed, oInput.checked returns null always.
			}
		} else
			$('#' + oQuestion.getIdPrefix()).val((oQuestion.answer != null) ? oQuestion.answer :'');
	};
	
	this.setToManualOrders = function setToManualOrders() {
		for (var item = 0; item < this.elements.length; item++) {
			//var oItem = this.elements[item];
			this.elements[item].answer = null;
		}
	};
}

//==================================================================================================
genBaseQuestionNextEvent = function(oBaseQuestion) {
	return ' onclick="onBaseQuestionNextEvent(' + oBaseQuestion.id + ')"'; 
}

onBaseQuestionNextEvent = function(baseQuestionId) {
	var oQustion = _interview.getFocusedQuestion(baseQuestionId);
	if (oQustion == null) {
		_interview.onMoveQuestion(true, baseQuestionId, baseQuestionId, true);
	} else {
		var aInputIds = oQustion.choices.getInputIds(oQustion);		
		onNextQuestionEvent(oQustion.type, oQustion.id, baseQuestionId, aInputIds);
	}
}

//genNextQuestionEvent = function(inputType, oQuestion, oBaseQuestion, inputId) {
//	return ' onclick="onNextQuestionEvent(\'' + inputType + '\', ' + oQuestion.id + ', ' + oBaseQuestion.id + ', \'' + inputId + '\')"'; 
//}

onNextQuestionEvent = function (pInputType, questionId, baseQuestionId, pInputIds) {
	//var sMsg = 'onNextQuestionEvent("' + pInputType + '", ' + questionId + ', ' + baseQuestionId + (pInputIds ? ', ' + pInputIds : '') + ')';
	//alert(sMsg);
	if (_oUserInfo) // CJ-696
		_oUserInfo.timelyRefreshTimeout();
	if (!pInputIds) {
		alert('pInputIds is missing');
		return;
	}
	var value;
	if ('N' == pInputType) {
		var oInput = $('#' + pInputIds);
		oInput = $('#' + pInputIds);
		if (oInput) {
			value = $('#' + pInputIds).val().trim();
			if (!value) {
				alert('The value must be a decimal or integer.');
				oInput.focus();
				return;
			}
			if (!isValidNumber(value)) { // if (!isNumeric(value)) {
				alert('Invalid number');
				oInput.focus();
				return;
			}
		} else {
			alert('Unknown pInputIds: "' + pInputIds + '"');
			return;
		}
	} else {
		var answered = false;
		var aInputs = pInputIds.split(',');
		var oInput;
		for (var index = 0; index < aInputs.length; index++) {
			// Added a secondary check, because sometimes checked is not dependable... 
			// esp, after radio button selection.
			var checker = $('#' + aInputs[index]).is(':checked');
			oInput = $('#' + aInputs[index]);
			if (oInput && (oInput.length == 1)) {
				if (oInput[0].checked || checker) {
					answered = true;
					break;
				}
			} else {
				alert('Unknown pInputIds: "' + aInputs[index] + '"');
				return;
			}
		}
		if (!answered) {
			alert('Missing selection');
			$('#' + aInputs[0]);
			return;
		}
	}
	_interview.onMoveQuestion(true, questionId, baseQuestionId, 0);
}

// ==================================================================================================
//genPreviousQuestionEvent = function(oQuestion, oBaseQuestion, isFinal) {
//	return ' onclick="onPreviousQuestionEvent(' + oQuestion.id + ', ' + oBaseQuestion.id + ', ' + (isFinal ? '1' : '0') + ')"'; 
//}
//onPreviousQuestionEvent = function (questionId, baseQuestionId, isFinal) {
//	_interview.onMoveQuestion(false, questionId, baseQuestionId, isFinal);
//}

genBaseQuestionPreviousEvent = function(oBaseQuestion) {
	return ' onclick="onBaseQuestionPreviousEvent(' + oBaseQuestion.id + ')"'; 
}
onBaseQuestionPreviousEvent = function(baseQuestionId) {
	var oQustion = _interview.getFocusedQuestion(baseQuestionId);
	_interview.onMoveQuestion(false, (oQustion == null) ? baseQuestionId : oQustion.id, baseQuestionId, (oQustion == null));
}

// QuestionItem ==================================================================================================
function QuestionItem() {
    this.id;
    this.name;
    this.type;
    this.text;
    this.group;

	this.choices = new QuestionChoiceSet();
	this.children = new Array(); // Hierarchy
	this.parent = null;
	
	this.prevousQuestion = null;
	this.nextQuestion = null;
	this.conditionItem = null;
	this.qGroupItem = null;
	
	this.answer = null;
	this.previousAnswer = null; // CJ-1112
	this.state = ANSWER_STATE_UNKNOWN;
	this.aInstanceArray = null;
	this.baseSubGroupId = null; 
	this.baseGroupId = null;
	this.focusedQuestion = null;
	
	this.loadJson = function loadJson(json) {
	    this.id = json.question_id;
	    this.name = json.question_code;
	    this.type = json.question_type;
	    this.text = json.question_text;
	    this.group = json.question_group;
	    //this.active = json.active;
	    
	    if (json.choices)
	    	this.choices.loadJson(json.choices);
	    else
	    	this.choices.clear();
	};
	
	this.resolveObjectReferences = function resolveObjectReferences(prescriptions) {
		this.choices.resolveObjectReferences(this, prescriptions);
		if (this.nextQuestion)
			this.nextQuestion.resolveObjectReferences(prescriptions);
	};
	
	this.loadAnswers = function loadAnswers(oDocAnswerItem) {
		if (this.choices.loadAnswers(this, oDocAnswerItem)) {
			// display transcript?
		}
	};

	this.hasPrescription = function hasPrescription(id) {
		return this.choices.hasPrescription(id);
	};
	
	this.hasChoice = function hasChoice(target, isTest) { // Hierarchy
		var c;
		var choices = this.choices.elements;
		for (c = 0; c < choices.length; c++) {
			//var questionChoiceItem = choices[c];
			if (target == choices[c].choice)
				return true;
		}
		for (c = 0; c < this.children.length; c++) {
			//var oChild = this.children[c];
			if (this.children[c].hasChoice(target, isTest))
				return true;
		}
		return false;
	}
	
	this.containQuestion = function containQuestion(oQuestionItem) { // Hierarchy
		if (this == oQuestionItem)
			return true;
		for (c = 0; c < this.children.length; c++) {
			if (this.children[c].containQuestion(oQuestionItem))
				return true;
		}
		return false;
	}
	
	this.getDivId = function getDivId() { return 'qd-' + this.id; };
	this.getBasicDivId = function getBasicDivId() { return 'qdb-' + this.id; };
	this.getBasicBtnId = function getBasicBtnId() { return 'qbt-' + this.id; };
	this.getIdPrefix = function getIdPrefix() { return 'qi-' + this.id; };
	this.getFinalPageId = function getFinalPageId() { return 'qf-' + this.id; };
	this.getFinalPageUlId = function getFinalPageUlId() { return 'qfu-' + this.id; };
	this.getCollapseId = function getCollapseId() { return 'qli-c-' + this.id; };
	this.getSymbolId = function getSymbolId() { return 'qli-s-' + this.id; };
	this.getBtnSymbolId = function getBtnSymbolId() { return 'qbts-' + this.id; };
	this.getTranscriptId = function getTranscriptId() { return 'qli-i-' + this.id; };
	this.getTransValueId = function getTransValueId() { return 'qli-v-' + this.id; };

	this.getBaseNextBtnId = function getBaseNextBtnId() { return 'qnbn-' + this.id; };
	this.getBasePreviousBtnId = function getBasePreviousBtnId() { return 'qnbp-' + this.id; };
	
	this.buildInstanceArray = function buildInstanceArray() {
		if (this.aInstanceArray == null) {
			this.aInstanceArray = new Array();
			var instance = this;
			while (instance) {
				this.aInstanceArray.push(instance);
				instance = instance.nextQuestion;
			}
		}
		return this.aInstanceArray;
	};
	
	this.genQuestionButtons = function genQuestionButtons(oGroup, bActive) {
		var sTitle = formatDisplayText(this.text); // this.text.capitalize(true);
		var sDisplay = '';
		if (!bActive) { // CJ-584
			if (!this.conditionItem.isApplicable())
				sDisplay = ' style="display:none;"';
		}
		var html = '<button ' + sDisplay
				+ ' onclick="onBtnGroupQuestionClick(\'' + oGroup.code + '\', ' + this.id + ')"'
				+ ' type="button" class="btn btn-default' + (bActive ? ' active' : '') + '"'
				+ ' title="' + sTitle + '"'
				+ ' id="' + this.getBasicBtnId() + '"'
				+ ' >'
				+ '<span id="' + this.getBtnSymbolId() + '">'
				+ getSymbolCode3(this.hasAllAnswered(true))
				+ '</span> '
				+ sTitle
				+ '</button>';
		return html;
	};
	
	this.showBasicButton = function showBasicButton(visible) { // CJ-584
		if (visible)
			$('#' + this.getBasicBtnId()).show();
		else
			$('#' + this.getBasicBtnId()).hide();
	};
	
	this.showConditionalBase = function showConditionalBase() { // CJ-584
		if (this.conditionItem.defaultBaseline) { // CJ-726
			this.conditionItem.cacheApplicable == null;
			var bApplicable = this.conditionItem.isApplicable();
			if (bApplicable) { // CJ-584
				$('#' + this.getFinalPageId()).hide();
				var oChild; // buildInstanceArray()
				var aQuestions = this.buildInstanceArray();
				if (aQuestions.length > 1) {
					for (var c = 1; c < aQuestions.length; c++) {
						oChild = aQuestions[c];
						oChild.choices.refreshControls(oChild);
						$('#' + oChild.getDivId()).hide();
					}
				}
				/* CJ-726
				for (var c = 0; c < this.children.length; c++) {
					oChild = this.children[c];
					oChild.choices.refreshControls(oChild);
					$('#' + oChild.getDivId()).hide();
				}
				*/
				this.choices.refreshControls(this);
				$('#' + this.getDivId()).show();
				this.focusedQuestion = this;
				// $('#' + this.getBasicDivId()).show();
			} else {
				$('#' + this.getDivId()).hide();
				this.focusedQuestion = null;
				// $('#' + this.getBasicDivId()).hide();
			}
			this.showBasicButton(bApplicable);
			return bApplicable;
		} else
			return true;
	};
	
	// This method adds a Next button to the Transcript page, to proceed
	// from one group to another.
	this.genNextQGroupButtons = function genNextQGroupButtons(oGroup, bIsNewTab) {
		var sTitle = 'Next';
		var sRedirect = ' onclick="onBtnGroupQuestionClick(\'' + oGroup.code + '\', ' + this.id + ')"';
		
		if (bIsNewTab)
			sRedirect = ' onclick="onNextQuestionTabClick(\'' + oGroup.getAccordionId() + '\', ' + oGroup.baseGroupId + ')"';	// CJ-362, not sure this is called.

		var html = '<button '
				+ sRedirect
				+ ' type="button" class="btn btn-primary btn-xs"'
				+ ' title="' + sTitle + '"'
				+ ' id="' + this.getBasicBtnId() + '"'
				+ ' >'
				+ sTitle
				+ '</button>';
		
		return html;
	};	

	/*
	this.genButtons = function genButtons(oBaseQuestion, isFinal, nextQuestionGroup) {
		var html = '';
		if (isFinal || (this != oBaseQuestion)) {
			var event = genPreviousQuestionEvent(this, oBaseQuestion, isFinal);
			html = '<button' + event + ' class="btn btn-primary btn-sm" type="button" style="margin-right:16px;">Previous</button>';
		}
		if (!isFinal) {
			var pInputIds = this.choices.getInputIds(this);
			var event = genNextQuestionEvent(this.type, this, oBaseQuestion, pInputIds);
			html += '<button' + event + ' class="btn btn-primary btn-sm" type="button">Next</button>';
		}
		// Add a Next button to the Transcript page, 
		// which proceeds to the next sub-group of questions.
		else
		{
			if (nextQuestionGroup != null) {
				var bIsNewTab = false;
				if (oBaseQuestion.group != nextQuestionGroup.group)
					bIsNewTab = true;
				// Add button for Next group of questions
				html += nextQuestionGroup.genNextQGroupButtons(nextQuestionGroup.qGroupItem, bIsNewTab);
			}
		}
		
		return html;
	}
	*/
	
	this.genConditionTooltip = function genConditionTooltip() {
		if ((!this.conditionItem) || (!this.conditionItem.originalCondition))
			return '';
		return ' title="'
			+ this.conditionItem.originalCondition.replace(/"/g, '&quot;')
			+ '"';
	};
	
	this.genFinalPageHtml = function genFinalPageHtml(baseQuestion, pShow, nextQuestionGroup) {
		//var showStyle = ' style="font-size: 1em;overflow-y:auto;height:' + (_maxQuestionPanelHeight-100) + 'px;"';
		//var hideStyle = ' style="display:none"';
		
		var html = '<div id="' + this.getFinalPageId() + '"' + (pShow ? 'style="font-size: 1em"' : ' style="display:none"') + '>'
			+ '<fieldset><legend><span style="font-size: .65em; font-weight: bold;display: block;">TRANSCRIPT</span></legend>'
			//+ '<fieldset><legend><h4>TRANSCRIPT</h4></legend>'
			+ '<ul id="' + this.getFinalPageUlId() + '">'
			+ this.getTranscriptContent(true)
			+ '</ul>'
			+ '</fieldset>'
			// + this.genButtons(baseQuestion, true, nextQuestionGroup)
			+ '</div>';
		return html;
	};
	
	this.genQuestionHtml = function genQuestionHtml(baseQuestion, pShow, iMaxChoices) {
		var sStyle = 'font-size: .85em; width: 100%;';
		var iDiff = iMaxChoices - this.choices.elements.length;
		// add display and height to the style for parent div; remove padding-bottom here.
		//if (iDiff > 0)
		//	sStyle = 'padding-bottom:' + (20 * iDiff) + 'px;';
		if (pShow == false)
			sStyle += 'display:none;';
		var bLegend = (this != baseQuestion);
		var html = '<div id="' + this.getDivId() + '"'
			+ (sStyle ? ' style="' + sStyle + '"' : '') // + (pShow ? '' : ' style="display:none"')
			+ '>'
			+ '<fieldset><legend class="hide">' 
			+ (this.text ? this.text.replace(/'/g, '&quot;') : this.code)
			+ '</legend>'
			+ (bLegend ? 
					'<div style="width=100%;border-bottom: 1px solid #aaa;">' + 
					'<span' + this.genConditionTooltip() + ' style="font-size: 1em;display: block;" >' + (this.text ? this.text : '') + '</span>' + 
					'</div><br/>' : '')
			+ this.choices.genInputHtml(this, baseQuestion)
			+ '</fieldset></div>';
		return html;
	};
	
	this.genHtml = function genHtml(parentId, isFirst, nextQuestionGroup, poLastQuestions) { // CJ-1074
		//var idCollapse = this.getCollapseId();
		var panalFocusedQuestion = poLastQuestions.findByBaseQuestionId(this.id); // CJ-1074
		
		var aQuestions = this.buildInstanceArray();

		var allAnswered = true;
		var questionHtml = '';

		var iMaxChoices = 1;
		var oQuestion, iLength, bApplicable, bShow, bHasAnswer;
		for (var index = 0; index < aQuestions.length; index++) {
			oQuestion = aQuestions[index];
			iLength = oQuestion.choices.elements.length;
			if (iLength > iMaxChoices)
				iMaxChoices = iLength;
		}
		this.focusedQuestion = null;
		for (var index = 0; index < aQuestions.length; index++) {
			oQuestion = aQuestions[index];
			bApplicable = oQuestion.conditionItem.isApplicable();
			bShow = false;
			if (bApplicable || (index == 0)) { // CJ-584
				if ((panalFocusedQuestion != null) && (oQuestion.id == panalFocusedQuestion.focusedQuestionId)) {
					allAnswered = false;
					bShow = true;
					this.focusedQuestion = oQuestion;
				} else {
					bHasAnswer = oQuestion.choices.hasAnswer(this);
					if (bHasAnswer == false) {
						if (allAnswered == true) {
							allAnswered = false;
							bShow = true;
							if (this.focusedQuestion == null)
								this.focusedQuestion = oQuestion;
						}
					}
				}
			}
			questionHtml += oQuestion.genQuestionHtml(this, bShow, iMaxChoices);
		}
		questionHtml += this.genFinalPageHtml(this, allAnswered, nextQuestionGroup);
		var sStyle = (isFirst ? '' : ' style="display:none"');
		
		var sButtons
			= '<button type="button" id="' + this.getBaseNextBtnId() + '" class="btn btn-xs btn-primary" style="margin-right:5px;font-size:.75em;"'
				+ genBaseQuestionPreviousEvent(this) + ' title="Previous question">Previous</button>'
	    	+ '<button type="button" id="' + this.getBasePreviousBtnId() + '" class="btn btn-xs btn-primary" style="font-size:.75em;"'
	    		+ genBaseQuestionNextEvent(this) + ' title="Next question">Next</button>';
		
		// Bev - 508 Compliance.
		var html
			= '<div id="' + this.getBasicDivId() + '"' + sStyle + '>' // class="panel-group" role="tablist" aria-multiselectable="true" 
				//+ '<div class="panel panel-default" >' // CJ-498. removed. This caused a double scroll bar.
					
				// title heading panel
														// CJ-498. Added border and color to style, to mimic panel-default
					+ '<div class="panel-heading" style="width:100%;border: 1px solid #dddddd;background-color: #f5f5f5;">' 
					    + '<div class="panel-title" style="width:100%;color:#19597f;">'
					    	+ '<div class="row" >'
						    	+ '<div class="QTitleSymbol" id="' + this.getSymbolId() + '">' + getSymbolCode3(allAnswered) + ' </div>'
						    	+ '<div class="QTitleText">' + this.text + '</div>'
							    + '<div class="QTitleBttns" align="right"> ' + sButtons + '</div>'
					+ '</div></div></div>'	// row-title-heading
					
					// body panel
				    + '<div class="panel-body QuestionBody">' // CJ-1198: changed from '<div class="panel-body" style="background-color:white;overflow-y:auto;height:' + (_maxQuestionPanelHeight-100) + 'px;">'
				    	+ questionHtml
				    + '</div>'
			   // + '</div>'
		    + '</div>';
		return html;
	};

	this.toggle = function toggle(pQuestionId) {
		var button = $('#' + this.getBasicBtnId());
		if (this.id == pQuestionId) {
			button.addClass('active');
			
			var divQuestion = $('#' + this.getBasicDivId());
			divQuestion.show();
			adjustQuestionChoicesHeight(divQuestion);
		} else {
			button.removeClass('active');
			$('#' + this.getBasicDivId()).hide();
		}
	};
	
	this.getTranscriptTree = function getTranscriptTree(bNoLink) {
		var html = '<li id="' + this.getTranscriptId() + '-head">'
			// + '<span><i class="fa ' + (this.nextQuestion ? ' fa-minus-square-o' : 'fa-leaf') + '"></i> '  // Removed leaf or minus-square
		    + '<span> ' + (this.text ? this.text : '') + '</span> '
			+ '<ul class="valueUl" id="' + this.getTransValueId() + '">'
			+ this.getTranscriptContent(false, bNoLink)
			+ '</ul>';
		return html + '</li>';
	};
	
	this.putTranscriptValue = function putTranscriptValue() {
		var values = this.getTranscriptContent(false); // this.choices.genTranscriptHtml(this);
		var oUl = $('#' + this.getTransValueId());
		if (oUl) {
			oUl.empty();
			oUl.append(values);
		}
	}
	
	// ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
	this.onInputChanged = function onInputChanged(questionId, choiceId, value) {
		var aQuestions = this.buildInstanceArray();
		var oCurrent = null;
		var iCurrent = -1;
		for (var index = 0; index < aQuestions.length; index++) {
			oCurrent = aQuestions[index];
			if (oCurrent.id == questionId) {
				iCurrent = index;
				break;
			}
		}
		if (iCurrent == -1) {
			alert('Unable to locate a question page');
			return;
		}
		_changeEventItems.init(oCurrent);
		var oSelItem = oCurrent.choices.onInputChanged(oCurrent, choiceId, value);
		this.putTranscriptValue();
		
		//var aCodes = [oCurrent.name];
		this.onQuestionAnswerChanged(++iCurrent); // aCodes, ++iCurrent);
		_interview.qGroup.onQuestionAnswerChanged(this.qGroupItem, this); // , aCodes
		_interview.clauses.onQuestionAnswerChanged(_interview.prescriptionChoices);
		
		this.hasAllAnswered();
	};
	
	this.onQuestionAnswerChanged = function onQuestionAnswerChanged(iStartPos) { // (aCodes, iStartPos)
		var bChanged = false;
		var aQuestions = this.buildInstanceArray();
		var oQuestion = null;
		if (!iStartPos)
			iStartPos = 0;
		for (var index = iStartPos; index < aQuestions.length; index++) {
			oQuestion = aQuestions[index];
			if (oQuestion.conditionItem.isChangeEffected()) { // if (oQuestion.conditionItem.hasCodes(aCodes)) {
				oQuestion.state = ANSWER_STATE_UNKNOWN;
				oQuestion.conditionItem.cacheApplicable = null;
				if (oQuestion.conditionItem.isApplicable()) { // CJ-1112
					if (oQuestion.choices.restorePreviousAnswers(oQuestion)) {
						bChanged = true;
						_changeEventItems.addQuestion(oQuestion);
					}
				} else {
					if (oQuestion.choices.clearAnswers(oQuestion)) {
						//oQuestion.putTranscriptValue();
						bChanged = true;
						_changeEventItems.addQuestion(oQuestion);
					}
				}
			}
		}
		if (bChanged) {
			this.putTranscriptValue();
			this.hasAllAnswered(false); // CJ-1112
		}
		return bChanged;
	};
	
	this.getTranscriptContent = function getTranscriptContent(bFinalPage, bNoLink) {
		var sLabel;
		var values = ''; // this.choices.genTranscriptHtml(this);
		var instance = this;
		var baseFinalPageId = instance.getFinalPageId();
		var transcript;
		while (instance) {
			if (instance.conditionItem.isApplicable()) {
				if (instance == this)
					sLabel = '';
				else
					sLabel = instance.text; // '<strong>' + instance.text + '</strong>';
				transcript = instance.choices.genTranscriptHtml(instance, sLabel, bFinalPage, baseFinalPageId, bNoLink);
				if (transcript) {
					values += transcript;
				}
			}
			instance = instance.nextQuestion;
		}
		return values;
	} 
	
	this.showFinalPage = function showFinalPage() {
		var values = this.getTranscriptContent(true); // this.choices.genTranscriptHtml(this);
		$('#' + this.getFinalPageUlId()).html(values);
		$('#' + this.getFinalPageId()).scrollTop();
		$('#' + this.getFinalPageId()).show();
		
		_activeQuestionPanelId =  this.getFinalPageId(); 
		
		_interview.onSaveBtnClick(3);  // auto-saving
	}
	
	this.onMoveQuestion = function onMoveQuestion(forward, questionId, baseQuestionId, isFinal) {
		var aQuestions = this.buildInstanceArray();
		var oCurrent = null;
		var iCurrent = -1;
		if (isFinal) {
			iCurrent = aQuestions.length;
		} else {
			for (var index = 0; index < aQuestions.length; index++) {
				oCurrent = aQuestions[index];
				if (oCurrent.id == questionId) {
					iCurrent = index;
					break;
				}
			}
			if (iCurrent == -1) {
				alert('Unable to locate a question page');
				return;
			}
			if (forward && oCurrent) {
				var sPrompt = oCurrent.choices.getPromptAfterSelect();
				if (sPrompt) {
					$(document).trigger("add-alerts", 
						    [{ 'message': sPrompt, 'priority': 'info' }]
						);
					//alert(sPrompt);
				}
			}
		}
		if (forward) {
			if (isFinal) {
				if (this.qGroupItem == null) {
					alert('There is no more follow-up question');
				} else {
					this.qGroupItem.onMoveBaseQuestion(forward, this);
				}
				return;
			} else {
				var oCurrentQuestion = aQuestions[iCurrent];
				if (oCurrentQuestion.conditionItem && (oCurrentQuestion.conditionItem.checkAnswers() == false)) {
					return;
				}
			}
			var oQuestion = null;
			var iNextIndex = iCurrent;
			while (++iNextIndex < aQuestions.length) {
				oQuestion = aQuestions[iNextIndex];
				if (oQuestion.conditionItem.isApplicable())
					break;
				
			}
			$('#' + oCurrent.getDivId()).hide();
			if (iNextIndex >= aQuestions.length) {
				this.showFinalPage();
				this.focusedQuestion = null; 
				$("#" + oCurrent.getBaseNextBtnId()).show();
			}
			else {
				_activeQuestionPanelId =  oQuestion.getDivId(); 
				$('#' + oQuestion.getDivId()).scrollTop();
				$('#' + oQuestion.getDivId()).show();	
				if (_interview.isFirstQuestion(oQuestion))
				{
					$('#qnbn-' + baseQuestionId).hide();					
				}
				else
				{					
					$('#qnbn-' + baseQuestionId).show();
				}
				/* Bev removed CJ-611
				if(oQuestion.getBasicDivId()=='qdb-' + firstQuestionId) {
					if(oQuestion.getDivId()=='qd-' + firstQuestionId) {
						$('#qnbn-' + firstQuestionId).hide();
						$('#qnbp-'+ firstQuestionId).show();
					}
				}
				else {
					$('#qnbn-' + baseQuestionId).show();
					$("#qnbp-" + baseQuestionId).show();
				}
				*/
				this.focusedQuestion = oQuestion; 
			}
		} else {
			if (iCurrent <= 0) {
				if (this.qGroupItem == null) {
					alert('There is no more question');
				} else {
					this.qGroupItem.onMoveBaseQuestion(forward, this);
				}
				return;
			}
			var oQuestion = null;
			var iPrevousIndex = iCurrent;
			while (--iPrevousIndex >= 0) {
				oQuestion = aQuestions[iPrevousIndex];
				if (oQuestion.conditionItem.isApplicable())
					break;
			}
			if (isFinal) {
				$('#' + this.getFinalPageId()).hide();
			} else {
				$('#' + oCurrent.getDivId()).hide();
			}
			_activeQuestionPanelId =  oQuestion.getDivId(); 
			$('#' + oQuestion.getDivId()).scrollTop();
			$('#' + oQuestion.getDivId()).show();
			if (_interview.isFirstQuestion(oQuestion))
			{
				$('#qnbn-' + baseQuestionId).hide();
			}
			else
			{
				$('#qnbn-' + baseQuestionId).show();
			}
			/* Bev removed, CJ-611
			// CJ-611
			if ((oQuestion.group == "A") && (oQuestion.baseGroupId == oQuestion.id)) { 
				
				var firstQuestionId = oQuestion.id;
				if(oQuestion.getDivId()=='qd-' + firstQuestionId) 
					$('#qnbn-' + firstQuestionId).hide();
				else
					$('#qnbn-' + firstQuestionId).show();
			}
			*/
			this.focusedQuestion = oQuestion; 
		}
	};
	
	this.showResumePrompt = function(isTranscriptPage) { // CJ-824
		var sPrompt = '';
		var oQuestion;
		if (isTranscriptPage) {
			var aQuestions = this.buildInstanceArray();
			oQuestion = aQuestions[aQuestions.length - 1];
		} else {
			oQuestion = this.prevousQuestion;
		}
		while (oQuestion != null) {
			if (oQuestion.conditionItem.isApplicable()) { // if (oQuestion.hasAnswer()) {
				sPrompt = oQuestion.choices.getPromptAfterSelect();
				oQuestion = null;
			} else {
				oQuestion = oQuestion.prevousQuestion;
			}
		}
		
		if (sPrompt) {
			$(document).trigger("add-alerts", 
				    [{ 'message': sPrompt, 'priority': 'info' }]
				);
		}
	}
	
	this.findQuestionById = function findQuestionById(questionId) {
		var oQuestion = null;
		var aQuestions = this.buildInstanceArray();
		for (var index = 0; index < aQuestions.length; index++) {
			oQuestion = aQuestions[index];
			if (oQuestion.id == questionId) {
				return oQuestion;
			}
		}
		return null;
	}
	
	this.hasAnswer = function hasAnswer() {
		var result;
		if (this.conditionItem.isApplicable())  {
			result = this.choices.hasAnswer(this);
			if (result) {
				this.state = ANSWER_STATE_ANSWERED;
				if (this.nextQuestion)
					result &= this.nextQuestion.hasAnswer();
			} else
				this.state = ANSWER_STATE_UNKNOWN;
			return result;
		} else {
			this.state = ANSWER_STATE_NOT_REQUIRED;
			result = true;
			if (this.nextQuestion)
				result &= this.nextQuestion.hasAnswer();
		}
		return result;
	};
	
	this.getAnswerValues = function getAnswerValues() {
		return this.choices.getAnswerValues(this);
	};
	
	this.hasParentAnswer = function hasParentAnswer() {
		if (this.choices.hasAnswer(this) == true)
			return true;
		if (this.parent != null) {
			return this.parent.hasParentAnswer();
		}
		return false;
	};
	
	this.getFirstQuestion = function getFirstQuestion() {
		if (this.prevousQuestion != null)
			return this.prevousQuestion.getFirstQuestion();
		else
			return this;
	};
	
	this.hasValue = function hasValue(poTarget) {
		var result = this.choices.hasValue(poTarget, this);
		if (result == null) {
			if (this.parent != null) {
				if (this.parent.hasParentAnswer())
					return false;
			}
			var oFirstQuestion = this.getFirstQuestion();
			if (oFirstQuestion != this) {
				if (oFirstQuestion.choices.hasAnswer(this))
					return false;
			}
			return null;
		}
		if (result == true)
			return result;

		var bAnyValue = true;
		var oChildQuestion;
		for (var iChild = 0; iChild < this.children.length; iChild++) {
			oChildQuestion = this.children[iChild];
			result = oChildQuestion.hasValue(poTarget);
			if (true == result)
				return true;
			if (result != null)
				bAnyValue = true;
		}
		if (bAnyValue)
			return false;
		return null;
	};
	
	this.getNumberAnswer = function getNumberAnswer() {
		if ("N" == this.type)
			return removeDollarFormat(this.answer); // CJ-1313
		var bAnyValue = false;
		var result;
		for (var iChild = 0; iChild < this.children.length; iChild++) {
			result = this.children[iChild].getNumberAnswer();
			if (result != null)
				return result;
		}
		return null;
	}
	
	this.getAnswerToSave = function getAnswerToSave() {
		var aValues = this.getAnswerValues();
		if (aValues && aValues.length > 0) {
			var result = '';
			for (var item = 0; item < aValues.length; item++) {
				if (item == 0)
					result = aValues[item];
				else
					result += DIVIDER + aValues[item];
			}
			return result;
		} else
			return null;
	}
	
	this.hasAllAnswered = function hasAllAnswered(statusOnly) {
		var result = this.choices.hasAnswer(this);
		if (result) {
			if (this.nextQuestion)
				result &= this.nextQuestion.hasAnswer();
		}
		if (statusOnly)
			return result;
		this.state = (result ? ANSWER_STATE_ANSWERED : ANSWER_STATE_UNKNOWN);
		changeSymbol(this.getBtnSymbolId(), result, 3);
		var symbolChanged = changeSymbol(this.getSymbolId(), result, 3);
		if (symbolChanged && this.qGroupItem)
			this.qGroupItem.onAnswerStatusChanged(this);
		return result;
	};
	
	this.isNumberType = function isNumberType() {
		return ("N" == this.type);
	};
	
	this.setToManualOrders = function setToManualOrders() {
		this.answer = null;
		this.choices.setToManualOrders();
	};
	
	// only for base question
	this.onSkipToGroup = function(targetQuestionItem) { // CJ-951
		//$('#' + this.getBasicBtnId()).addClass('active'); // Left Pane
		//$('#' + this.getBasicDivId()).show();			// Middle Pane
		var oQuestion;
		for (var iQuestion = 0; iQuestion < this.aInstanceArray.length; iQuestion++) {
			oQuestion = this.aInstanceArray[iQuestion];
			if (oQuestion == targetQuestionItem) {
				_activeQuestionPanelId = oQuestion.getDivId();
				$('#' + _activeQuestionPanelId).show();
			} else {
				$('#' + oQuestion.getDivId()).hide();
			}
		}
		if (targetQuestionItem == null) {
			_activeQuestionPanelId = this.getFinalPageId(); 
			$('#' + _activeQuestionPanelId).show();
		} else {
			$('#' + this.getFinalPageId()).hide();
		}
		
		$('#' + _activeQuestionPanelId).scrollTop();
		
		// set focus
		// CJ-362, check for final page id. If true, set to null.
		this.focusedQuestion = targetQuestionItem;
	};
	
};

function QuestionSet() {
	this.elements = new Array();
	this.retrieve = function retrieve() {
		var instance = this;
		$.ajax({
			url: getAppPath() + 'page?do=doc-ref-question',
			cache: false,
			dataType: 'json',
			success: function(data) {
				if (!instance.loadJson(data.data)) {
					goHome();
				}
			},
			error: function(object, text, error) {
				goHome();
			}
		});
	};
	
	this.loadJson = function loadJson(json) {
		this.elements.length = 0;
		var oQuestionItem;
		for (var iData = 0; iData < json.length; iData++) {
			oQuestionItem = new QuestionItem();
			oQuestionItem.loadJson(json[iData]);
			this.elements.push(oQuestionItem);
		}
		return true;
	};
	
	this.findById = function findById(id) {
		var oQuestionItem;
		for (var iEle = 0; iEle < this.elements.length; iEle++) {
			oQuestionItem = this.elements[iEle];
			if (oQuestionItem.id == id)
				return oQuestionItem;
		}
		return null;
	};
	
	this.findByName = function findByName(name) {
		var oQuestionItem;
		for (var iEle = 0; iEle < this.elements.length; iEle++) {
			oQuestionItem = this.elements[iEle];
			if (oQuestionItem.name == name)
				return oQuestionItem;
		}
		return null;
	};
	
	
	this.findByGroup = function findByGroup(sGroup) {
		var oQuestionItem;
		for (var iEle = 0; iEle < this.elements.length; iEle++) {
			oQuestionItem = this.elements[iEle];
			if (oQuestionItem.group == sGroup)
				return oQuestionItem;
		}
		return null;
	};
	
	
	// Retrieve a group of subGroupIds, per GroupId
	this.getSubGroupIds = function getSubGroupIds(GroupId) {

	    var grpArray  = new Array();
	    var oQuestionItem, sActiveQuestionPanelId;
		for (var iEle = 0; iEle < this.elements.length; iEle++) {
			oQuestionItem = this.elements[iEle];
			if (oQuestionItem.baseGroupId == GroupId)
			{
				sActiveQuestionPanelId = oQuestionItem.baseSubGroupId; //'#qd-' + oQuestionItem.baseSubGroupId;
				if ($.inArray(sActiveQuestionPanelId, grpArray) == -1)
					grpArray.push(sActiveQuestionPanelId);
			}
		}
		return grpArray;
	};
		
	this.onInputChanged = function onInputChanged(questionId, baseQuestionId, choiceId, value) {
		var oQuestionItem = this.findById(baseQuestionId);
		if (oQuestionItem) {
			oQuestionItem.onInputChanged(questionId, choiceId, value);
		} else {
			alert('baseQuestionId(' + baseQuestionId + ') is not found');
		}
	};
	
	this.onMoveQuestion = function onMoveQuestion(forward, questionId, baseQuestionId, isFinal) {
		var oQuestionItem = this.findById(baseQuestionId);
		if (oQuestionItem) {
			oQuestionItem.onMoveQuestion(forward, questionId, baseQuestionId, isFinal);
		} else {
			alert('baseQuestionId(' + baseQuestionId + ') is not found');
		}
	};

	this.SetActivePanel = function SetActivePanel(code) {
		var id = _activeQuestionPanelId.toString();
		if (id.indexOf("-") >= 0)
			id = _activeQuestionPanelId.substr(("qd-").length); // qd- or qf-, doesn't matter. same length.
		
		var activeQuestion = this.findById (id);
		
		// if the Accordian panel is already active, then don't disturb settings
		if (code != activeQuestion.group) {
			var oQuestion = this.findByGroup (code);
			_activeQuestionPanelId = oQuestion.baseGroupId;			
		}
		if (_oUserInfo) // CJ-696
			_oUserInfo.timelyRefreshTimeout();
	};
	
}

// QGroupItem ==================================================================================================
function QGroupItem() {
    this.code;
    this.name;
    this.baseGroupId = null;
    this.hasConditionalBaseQuestions = false;
    
    this.questions = new Array();

	this.loadJson = function loadJson(json) {
	    this.code = json.question_group;
	    this.name = json.question_group_name;
	};
	
	this.getDivAccordianId = function getDivAccordianId() { // CJ-584
		return 'accordion' + this.code;
	} 
	
	this.getDivQGroupId = function getDivQGroupId() { // CJ-584
		return 'qg-' + this.code;
	} 
	
	this.getQuestionCountId = function getQuestionCountId() { // CJ-584
		return 'qgqc-' + this.code;
	} 
	
	this.genHtml = function genHtml(nextQuestionTab, poLastQuestions) { // mlee // CJ-1074
		if (this.questions.length == 0)
			return '';

		var id = this.getDivAccordianId(); // 'accordion' + this.code;
		var html = '<div id="' + id + '">'; // class="panel-group"
		var oQuestion, nextQuestionGroup;
		for (var iQuestion = 0; iQuestion < this.questions.length; iQuestion++) {
			oQuestion = this.questions[iQuestion];
			// Pass a pointer to the next sub group. Transcript page will use it.
			nextQuestionGroup = null;
			if (iQuestion < (this.questions.length - 1))
				nextQuestionGroup = this.questions[iQuestion+1]
			else
				if ((nextQuestionTab != null) && (nextQuestionTab.questions.length > 0))
					nextQuestionGroup = nextQuestionTab.questions[0];
			
			html += oQuestion.genHtml(id, (iQuestion == 0), nextQuestionGroup, poLastQuestions); // mlee
		}
		return html + '</div>';
	};
	
	this.genQuestionButtons = function genQuestionButtons() {
		var html = '<div class="btn-group-vertical" role="group" aria-label="..." >';
		var oQuestion, activeFlag = null;
		for (var iQuestion = 0; iQuestion < this.questions.length; iQuestion++) {
			oQuestion = this.questions[iQuestion];
			if (this.hasConditionalBaseQuestions) {
				if (activeFlag == null) {
					if (oQuestion.conditionItem.isApplicable()) {
						activeFlag == true;
					}
				} else
					activeFlag == false;
			} else {
				activeFlag == (iQuestion == 0);
			}
			html += oQuestion.genQuestionButtons(this, activeFlag); // genHtml(id, true); // (iQuestion == 0));
		}
		return html + '</div>';
	};
	
	this.getQuestionCount = function getQuestionCount() {
		var iQuestionCount = this.questions.length;
		if (this.hasConditionalBaseQuestions) {
			var oQuestion;
			for (var iQuestion = 0; iQuestion < this.questions.length; iQuestion++) {
				oQuestion = this.questions[iQuestion];
				oQuestion.conditionItem.cacheApplicable = null;
				if (!oQuestion.conditionItem.isApplicable()) {
					iQuestionCount--;
				}
			}
		}
		return iQuestionCount;
	} 
	
	this.genAccordion = function genAccordion(bActive) {
		var iQuestionCount = this.getQuestionCount();
		var sName = this.name.replace(/"/g, '&quot;');
		var sDisplay = (iQuestionCount > 0) ? 'style="font-size:.85em;"' : ' style="display:none;"'
		var html
			= '<div id="' + this.getDivQGroupId() + '" class="panel panel-default collapsible-panel"' + sDisplay + '>' // CJ-584
				+ '<div class="panel-heading">' //  collapsible-panel-heading
					+ '<div class="panel-title">'
						+ '<div class="divChoices">'
						+ '<span id="' + this.getSymbolId() + '" style="color:black; padding-right:10px;">' + getSymbolCode2(this.isAllAnswered()) + '</span>'
						+ '<a data-toggle="collapse" data-parent="#accrdnQGroup" href="#' + this.getAccordionId() + '"'
						+ (bActive ? ' class="bold"' : '') + '>'
							+ sName 
						+ '<span class="pull-right" id="' + this.getAccordChvenId() + '"><img src="../assets/images/menu_expand.png" alt="Menu expand." /></span>'
						+ '</a>'
						+ '</div>'
					+ '</div>'
				+ '</div>'
				+ '<div id="' + this.getAccordionId() + '" class="panel-collapse collapse' + (bActive ? ' in' : '') + '">'
					+ '<div class="panel-body" style="padding:3px;padding-left:10px;font-size:1.15em;">'
						+ this.genQuestionButtons()
					+ '</div>'
				+ '</div>'
			+ '</div>';
		return html;
	};
	
	this.onGroupQuestionClick = function onGroupQuestionClick(pQuestionId) {
		for (var iQuestion = 0; iQuestion < this.questions.length; iQuestion++) {
			//var oQuestion = this.questions[iQuestion];
			this.questions[iQuestion].toggle(pQuestionId);
		}
		if (_oUserInfo) // CJ-696
			_oUserInfo.timelyRefreshTimeout();
	};
	
	this.getAccordionId = function getAccordionId() {
		return QUESTION_GROUP_ID_PREFIX + this.code;
	};
	
	this.getAccordChvenId = function getAccordChvenId() {
		return QUESTION_GROUP_CHVRON_PREFIX + this.code;
	};
	
	this.getTranscriptId = function getTranscriptId() {
		return 'tli' + this.code;
	};
	
	this.getTranscriptTree = function getTranscriptTree(bNoLink) {
		if (! this.hasApplicableQuestions()) // if (this.questions.length <= 0) // CJ-584
			return '';
		var html = '<li id="' + this.getTranscriptId() + '">'
			// + '<span><i class="fa fa-minus-square-o"></i> '  // Removed the minus-square
			+ '<span> ' + this.name.toUpperCase() + '</span>';
		html += '<ul>';
		var oQuestion;
		for (var iQuestion = 0; iQuestion < this.questions.length; iQuestion++) {
			oQuestion = this.questions[iQuestion];
			if (oQuestion.conditionItem.isApplicable()) // CJ-591
				html += oQuestion.getTranscriptTree(bNoLink);
		}
		return html + '</ul></li>';
	};
	
	this.getSymbolId = function getSymbolId() {
		return 'qg-s-' + this.code;
	};
	
	this.isAllAnswered = function isAllAnswered() {
		var allAnswered = true;
		if (this.hasConditionalBaseQuestions) { // CJ-653
			var oQuestion;
			for (var iQuestion = 0; iQuestion < this.questions.length; iQuestion++) {
				oQuestion = this.questions[iQuestion];
				if (oQuestion.conditionItem.isApplicable()) {
					if (!oQuestion.hasAllAnswered(true)) {
						allAnswered = false;
						break;
					}
				}
			}
		} else {
			for (var iQuestion = 0; iQuestion < this.questions.length; iQuestion++) {
				if (!this.questions[iQuestion].hasAllAnswered(true)) { // }(oQuestion.state != ANSWER_STATE_ANSWERED) {
					allAnswered = false;
					break;
				}
			}
		}
		return allAnswered;
	};
	
	this.countAnswered = function countAnswered() {
		var result = 0;
		var oQuestion;
		for (var iQuestion = 0; iQuestion < this.questions.length; iQuestion++) {
			var oQuestion = this.questions[iQuestion];
			if (oQuestion.conditionItem.isApplicable() && oQuestion.hasAllAnswered(true)) {
				++result;
			}
		}
		return result;
	};
	
	this.onAnswerStatusChanged = function onAnswerStatusChanged(oQuestion) {
		var allAnswered = this.isAllAnswered();
		changeSymbol(this.getSymbolId(), allAnswered, 2);
		_interview.adjustProgressBar(1); // _interview.qGroup.adjustProgressBar();
	};
	
	this.onQuestionAnswerChanged = function onQuestionAnswerChanged(iQuestion) { // , aCodes
		var bAdjustForConditional = (iQuestion == 0);
		var bChanged = false;
		while (iQuestion < this.questions.length) {
			//var oQuestion = this.questions[iQuestion];
			bChanged |= this.questions[iQuestion].onQuestionAnswerChanged(0); // aCodes
			iQuestion++;
		}
		if (bChanged) {
			this.onAnswerStatusChanged();
		}
		if (bAdjustForConditional == true)	// CJ-588
			this.adjustForConditionalBaseQuestions(); // CJ-584
		return bChanged;
	};
	
	this.adjustForConditionalBaseQuestions = function adjustForConditionalBaseQuestions() {
		if (!this.hasConditionalBaseQuestions)
			return;
		var oBaseQuestion, iApplicable = 0;
		var bFirstBaseQuestion = true;
		var allAnswered = true; // CJ-653
		for (var iBaseQuestion = 0; iBaseQuestion < this.questions.length; iBaseQuestion++) {
			oBaseQuestion = this.questions[iBaseQuestion];
			if (oBaseQuestion.showConditionalBase()) {
				iApplicable++;
				if (bFirstBaseQuestion == true) {
					this.baseGroupId = oBaseQuestion.id; // CJ-729 
					oBaseQuestion.toggle(oBaseQuestion.id);
					bFirstBaseQuestion = false;
				} else {
					oBaseQuestion.toggle(-1);
					$('#' + oBaseQuestion.getBasicDivId()).hide();
				}
				allAnswered = allAnswered && oBaseQuestion.hasAllAnswered(false); // CJ-653
			} else {
				oBaseQuestion.toggle(-1);
				$('#' + oBaseQuestion.getBasicDivId()).hide();
			}
		}
		// CJ-637, Remove count from Accordian Panel name
		//$('#' + this.getQuestionCountId()).html('' + iApplicable);
		if (iApplicable > 0) {
			changeSymbol(this.getSymbolId(), allAnswered, 2); // CJ-653
			$('#' + this.getDivQGroupId()).show();
		} else
			$('#' + this.getDivQGroupId()).hide();
	} 
	
	this.onMoveBaseQuestion = function onMoveBaseQuestion(forward, poBaseQuestion) {
		var iPos = this.questions.indexOf(poBaseQuestion);
		if (iPos < 0) {
			alert("Unable to locate rquested base question: " + poBaseQuestion.name);
			return;
		}
		var oNewQuestion = null, oNextQuestion; // CJ-584
		if (forward) {
			while ((this.questions.length > ++iPos) && (oNewQuestion == null)) {
				oNextQuestion = this.questions[iPos];
				if (oNextQuestion.conditionItem.isApplicable())
					oNewQuestion = oNextQuestion;
			}
			//if (this.questions.length > ++iPos)
			//	oNewQuestion = this.questions[iPos];
		} else {
			while ((--iPos >= 0) && (oNewQuestion == null)) {
				oNextQuestion = this.questions[iPos];
				if (oNextQuestion.conditionItem.isApplicable())
					oNewQuestion = oNextQuestion;
			}
			//if (--iPos >= 0)
			//	oNewQuestion = this.questions[iPos]; 
		}
		if (oNewQuestion != null) {
			poBaseQuestion.toggle(oNewQuestion.id);
			oNewQuestion.toggle(oNewQuestion.id);
			if (oNewQuestion.focusedQuestion == null)
			{
				//Last Question: Transcript is shown
				$('#qnbn-' + poBaseQuestion.id).show();				
			}
			else
			{
				
				if (_interview.isFirstQuestion(this.focusedQuestion))
				{		
					$('#' + this.focusedQuestion.getBaseNextBtnId()).hide();
				}
				else
				{				
					$('#' + this.focusedQuestion.getBaseNextBtnId()).show();
				}
			}
			// CJ-759 Wrong _activeQuestionPanelId was causing the blank transcript page.
			// _activeQuestionPanelId =  oNewQuestion.getFinalPageId();
			var sActiveQuestionPanelTmpId = 'qd-' + oNewQuestion.id;
			var qdStyle = $('#' + sActiveQuestionPanelTmpId).attr('style');
			if ((qdStyle != null) && (qdStyle.indexOf("display:none;") >= 0))
				sActiveQuestionPanelTmpId = 'qf-' + oNewQuestion.id;
			
			_activeQuestionPanelId = sActiveQuestionPanelTmpId;
			
		} else {
			_interview.qGroup.onMoveQuestionGroup(forward, this);
		}
	};
	
	this.hasApplicableQuestions = function hasApplicableQuestions() {
		if (this.questions.length == 0)
			return false;
		if (!this.hasConditionalBaseQuestions)
			return true;
		for (var iQuestion = 0; iQuestion < this.questions.length; iQuestion++) {
			if (this.questions[iQuestion].conditionItem.isApplicable()) {
				return true;
			}
		}
		return false;
	};
	
	this.getHtmlIncompletedQuestions = function getHtmlIncompletedQuestions() {
		if (this.questions.length == 0)
			return '';

		var html = '';
		var oQuestion;

		for (var iQuestion = 0; iQuestion < this.questions.length; iQuestion++) {
			oQuestion = this.questions[iQuestion];
			if (this.hasConditionalBaseQuestions) {
				if (!oQuestion.conditionItem.isApplicable())
					oQuestion = null;
			}
			if ((oQuestion != null) && (!oQuestion.hasAllAnswered(true))) {
				if (html == '') {
					html = '<li>' //  id="' + this.getTranscriptId() + '"
						+ '<strong>' + this.name.toUpperCase() + '</strong>'
						+ '<ul>';
				}
				html += '<li><a href="#" onclick="javascript:onClickQuestionModal(\'' + oQuestion.getFinalPageId() + '\', \'' + oQuestion.focusedQuestion.id + '\')">' + oQuestion.text + '</a></li>'; // oQuestion.getTranscriptTree();
			}
		}

		if (html != '')
			html += '</ul></li>'
		return html;
	};
	

	this.getFirstAppliableQuestionItem = function() { // CJ-951
		if (!this.hasApplicableQuestions()) {
			return null;
		}
		if (!this.hasConditionalBaseQuestions) {
			return this.questions[0];
		}
		var oQuestion;
		for (var iQuestion = 0; iQuestion < this.questions.length; iQuestion++) {
			oQuestion = this.questions[iQuestion];
			if (oQuestion.conditionItem.isApplicable()) {
				return oQuestion;
			}
		}
		return null;
	};
	
	this.getLastAppliableQuestionItem = function getLastAppliableQuestionItem() {
		if (!this.hasApplicableQuestions()) {
			return null;
		}
		if (!this.hasConditionalBaseQuestions) {
			return this.questions[this.questions.length - 1];
		}
		var oQuestion;
		for (var iQuestion = this.questions.length - 1; iQuestion >= 0; iQuestion--) {
			oQuestion = this.questions[iQuestion];
			if (oQuestion.conditionItem.isApplicable()) {
				return oQuestion;
			}
		}
		return null; // CJ-951 this.questions[0];
	};
	
	this.onSkipToGroup = function(targetQuestionItem) { // CJ-951
		if (_activeGroupCode != null) {
			toggleGroupLabels(_activeGroupCode, false); 
			$('[id^="panel-' + _activeGroupCode + '"]').hide();
		}

		$('#' + QUESTION_GROUP_CHVRON_PREFIX + this.code).click(); // this displays the Tab panel.
		toggleGroupLabels(this.code, true); 
		$('#panel-' + this.code).show();

		_activeGroupCode = this.code;

		var oQuestion;
		for (var iQuestion = 0; iQuestion < this.questions.length; iQuestion++) {
			oQuestion = this.questions[iQuestion];
			if (oQuestion == targetQuestionItem) {
				$('#' + oQuestion.getBasicBtnId()).addClass('active'); 	// Left Pane				
				var questionDiv = $('#' + oQuestion.getBasicDivId()); // Middle Pane
				questionDiv.show();	
				adjustQuestionChoicesHeight(questionDiv);
			} else {
				$('#' + oQuestion.getBasicBtnId()).removeClass('active'); 	// Left Pane
				$('#' + oQuestion.getBasicDivId()).hide();					// Middle Pane
			}
		}
	};

};

// ------------------------------------------------------------------------
function QGroupSet() {
	this.elements = new Array();

	this.retrieve = function retrieve() {
		var instance = this;
		$.ajax({
			url: getAppPath() + 'page?do=doc-ref-question_group',
			cache: false,
			dataType: 'json',
			success: function(data) {
				if (!instance.loadJson(data.data)) {
					goHome();
				}
			},
			error: function(object, text, error) {
				goHome();
			}
		});
	};

	this.loadJson = function loadJson(json) {
		this.elements.length = 0;
		var oQGroupItem;
		for (var iData = 0; iData < json.length; iData++) {
			oQGroupItem = new QGroupItem();
			oQGroupItem.loadJson(json[iData]);
			this.elements.push(oQGroupItem);
		}
		return true;
	};

	this.resolveObjectReferences = function resolveObjectReferences(qConditions, questions, prescriptions) {
		var oPreviousQuestion = null;
		var aConditions = qConditions.elements;
		var oCondition, oQuestion, sGroup, qGroupItem;
		for (var iCond = 0; iCond < aConditions.length; iCond++) {
			oCondition = aConditions[iCond];
			oQuestion = oCondition.resolveQuestion(questions);
			if (!oQuestion) {
				alert(oCondition.sequence + ' does not have corresponding Question item');
				continue;
			}
			sGroup = oQuestion.group;
			if (!sGroup) {
				oQuestion.group = '0';
				sGroup = '0';
			}
			qGroupItem = this.findByCode(sGroup);
			if (!qGroupItem) {
				alert(sGroup + ' group is not found');
				continue;
			}
			oQuestion.qGroupItem = qGroupItem;
			if (oCondition.conditionToQuestion) {
				if (oCondition.defaultBaseline == false) {
					if ((oQuestion.name == '[TRANSPORTATION REQUIREMENTS 5]') || (oQuestion.name == '[SHIPPING STATEMENTS]'))
						oCondition.defaultBaseline = true;
				}
				if ((oCondition.defaultBaseline == false) && oPreviousQuestion && (oQuestion.group == oPreviousQuestion.group)) { // CJ-584
					oQuestion.baseSubGroupId = oPreviousQuestion.baseSubGroupId; 
					oQuestion.baseGroupId = qGroupItem.baseGroupId;
					
					oPreviousQuestion.nextQuestion = oQuestion;
					oQuestion.prevousQuestion = oPreviousQuestion; 
				} else { // CJ-584
					if (qGroupItem.baseGroupId == null)
						qGroupItem.baseGroupId = oQuestion.id;
					oQuestion.baseGroupId = qGroupItem.baseGroupId;
					oQuestion.baseSubGroupId = oQuestion.id; 
					
					qGroupItem.questions.push(oQuestion);
					qGroupItem.hasConditionalBaseQuestions = true;
					// alert('Unbounded Question found: "' + oCondition.originalCondition + '"');
				}
			} else {
				if (qGroupItem.baseGroupId == null)
					qGroupItem.baseGroupId = oQuestion.id;
				oQuestion.baseGroupId = qGroupItem.baseGroupId;
				oQuestion.baseSubGroupId = oQuestion.id; 
				
				qGroupItem.questions.push(oQuestion);
			}
			oPreviousQuestion = oQuestion;
			oQuestion.resolveObjectReferences(prescriptions);
		}
	};
	
	this.onGroupQuestionClick = function onGroupQuestionClick(pGroupCode, pQuestionId) {
		var oQGroupItem = this.findByCode(pGroupCode);
		if (oQGroupItem) {
			oQGroupItem.onGroupQuestionClick(pQuestionId);
		} else {
			logOnConsole('QGroupSet().onGroupQuestionClick(' + pGroupCode + ', ' + pQuestionId + '): code not found');
		}
	};
	
	this.onMoveQuestionGroup = function onMoveQuestionGroup(forward, oQGroupItem) {
		var iPos = this.elements.indexOf(oQGroupItem);
		if (iPos < 0) {
			alert('onMoveQuestionGroup() Error: Unable to locate requested question groupd item');
			return;
		}
		var oTargetQGroupItem = null, oNextQGroupItem; // CJ-584
		if (forward) {
			while ((this.elements.length > ++iPos) && (oTargetQGroupItem == null)) {
				oNextQGroupItem = this.elements[iPos];
				if (oNextQGroupItem.hasApplicableQuestions())
					oTargetQGroupItem = oNextQGroupItem;
			}
		} else {
			while ((--iPos >= 0) && (oTargetQGroupItem == null)) {
				oNextQGroupItem = this.elements[iPos];
				if (oNextQGroupItem.hasApplicableQuestions())
					oTargetQGroupItem = oNextQGroupItem;
			}
		}
		/*
		if (forward) {
			if (this.elements.length <= ++iPos) {
				alert('No more next questions');
				return;
			}
		} else {
			if (--iPos < 0) {
				alert('No more previous questions');
				return;
			}
		}
		oTargetQGroupItem = this.elements[iPos];
		// If an accordian group does not have any questions (ie. Transportation), then skip
		while ((oTargetQGroupItem.questions.length == 0) && (this.elements.length > (iPos + 1))) {
			//iPos = iPos + 1;
			oTargetQGroupItem = this.elements[++iPos];
		}
		*/
		
		var nextQuestionId = this.getNextQuestionId(forward, oTargetQGroupItem);
		 
		if (nextQuestionId != null) {
			// CJ-362, 
			// NOTE: if there is a concern about Previous click going to first subgroup of questions,
			// then change oTargetQGroupItem.baseGroupId to whichever group the BAs decide.
			// onNextQuestionTabClick(oTargetQGroupItem.getAccordionId(), oTargetQGroupItem.baseGroupId); 
			// CJ-652: the previous needs to change to the last subgroup of the question
			onNextQuestionTabClick(oTargetQGroupItem.getAccordionId(), nextQuestionId);		
		 
		} else {
			var sPrompt = (forward ? 'You have reached the end of the interview' : 'No more previous questions');
			$(document).trigger("add-alerts", [{ 'message': sPrompt, 'priority': 'warning' }] );
		}
	};
	
	this.findByCode = function findByCode(code) {
		var oItem;
		for (var iEle = 0; iEle < this.elements.length; iEle++) {
			oItem = this.elements[iEle];
			if (oItem.code == code)
				return oItem;
		}
		return null;
	};
	
	this.getTranscriptTree = function getTranscriptTree(bNoLink) {
		var html = '';
		for (var iEle = 0; iEle < this.elements.length; iEle++) {
			//var oItem = this.elements[iEle];
			html += this.elements[iEle].getTranscriptTree(bNoLink);
		}
		return html;
	};
	
	this.getProgress = function getProgress(oProgress) {
		var iTotal = 0, iAnswered = 0, oItem;
		for (var iEle = 0; iEle < this.elements.length; iEle++) {
			oItem = this.elements[iEle];
			iTotal += oItem.getQuestionCount(); // questions.length; // CJ-584
			iAnswered += oItem.countAnswered();
		}
		oProgress.totalQuestions = iTotal;
		oProgress.answeredQuestions = iAnswered;
	};
	
	this.onQuestionAnswerChanged = function onQuestionAnswerChanged(oGroup, oQuestion) { // , aCodes
		var iGroup = 0;
		var iQuestion = 0;
		while (iGroup < this.elements.length) {
			if (this.elements[iGroup] == oGroup) {
				iQuestion = oGroup.questions.indexOf(oQuestion);
				break;
			} else
				iGroup++;
		}
		if (iQuestion < 0) {
			logOnConsole('QGroupSet.onQuestionAnswerChanged: Unable to locate question');
			return;
		}
		var bChanged = oGroup.onQuestionAnswerChanged(++iQuestion); // , aCodes
		while (++iGroup < this.elements.length) {
			//oGroup = this.elements[iGroup];
			bChanged |= this.elements[iGroup].onQuestionAnswerChanged(0); // , aCodes
		}
		if (bChanged)
			_interview.adjustProgressBar(1); // this.adjustProgressBar();
	}
	
	this.getHtmlIncompletedQuestions = function getHtmlIncompletedQuestions() {
		var html = '<ul>';
		for (var iGroup = 0; iGroup < this.elements.length; iGroup++) {
			html += this.elements[iGroup].getHtmlIncompletedQuestions();
		}
		return html + '</ul>';
	};
	
	this.getFirstAppliableQuestionItem = function() {
		var oItem= null; 
		var firstAppliciableQuestion = null;
		for (var iEle = 0; iEle < this.elements.length; iEle++) {
			oItem = this.elements[iEle];
			firstAppliciableQuestion = oItem.getFirstAppliableQuestionItem();
			if (firstAppliciableQuestion != null)
			{
				return firstAppliciableQuestion
			}
		}
		return null;
	};
	
	this.getNextQuestionId = function(forward, oNextQGroupItem) {
		if (!oNextQGroupItem) {
			return null;
		}
		var targetQuestion;
		
		if (forward) {
			targetQuestion = oNextQGroupItem.getFirstAppliableQuestionItem();
			//return oNextQGroupItem.getFirstAppliableQuestionItem().baseSubGroupId; // CJ-951 oNextQGroupItem.baseGroupId;
		} else {
			targetQuestion = oNextQGroupItem.getLastAppliableQuestionItem();
			//return oNextQGroupItem.getLastAppliableQuestionItem().baseSubGroupId;
		}
		if (targetQuestion == null) {
			return null;
		} else {
			return targetQuestion.baseSubGroupId;
		}
	};

this.getNextGroupItem = function (forward, currentGroupItem)
	{
		var oTargetQGroupItem = null, oNextQGroupItem;
		
		var iPos = this.elements.indexOf(currentGroupItem);
		if(forward)
		{
			while ((this.elements.length > ++iPos) && (oTargetQGroupItem == null)) {
				oNextQGroupItem = this.elements[iPos];
				if (oNextQGroupItem.hasApplicableQuestions())
					oTargetQGroupItem = oNextQGroupItem;
			}
		}
		else
		{
			while ((--iPos >= 0) && (oTargetQGroupItem == null)) {
				oNextQGroupItem = this.elements[iPos];
				if (oNextQGroupItem.hasApplicableQuestions())
				{
					oTargetQGroupItem = oNextQGroupItem;					
				}		
			
			}
		}
		return oTargetQGroupItem;
	};
}

// DocClauseItem ==================================================================================================
function DocClauseItem() {
	this.clauseId;
	this.isCompleted = false;
	this.optionalUserActionCode = null; // null, A (Added), R (Removed)
	// this.isRemovedByUser = false;
	
	this.loadJson = function loadJson(json) {
	    this.clauseId = json.clause_id;
	    this.isCompleted = json.is_fillin_completed;
		this.optionalUserActionCode = json.optional_user_action_code;
	    // this.isRemovedByUser = json.is_removed_by_user;
	};

	this.createJson = function createJson() {
		var result = {
			"clause_id": this.clauseId,
			"is_fillin_completed": this.isCompleted,
			"optional_user_action_code": this.optionalUserActionCode 
			// "is_removed_by_user": this.isRemovedByUser
	    }
		return result;
	};

}

//----------------------------------------------------------------
function DocClauseSet() {
	this.elements = new Array();

	this.loadJson = function loadJson(json) {
		this.elements.length = 0;
		if (json) {
			var oItem;
			for (var iData = 0; iData < json.length; iData++) {
				oItem = new DocClauseItem();
				oItem.loadJson(json[iData]);
				this.elements.push(oItem);
			}
		}
	};
	
	this.isEmpty = function isEmpty() {
		return (this.elements.length < 1); 
	};
	
	this.findByClauseId = function findByClauseId(id) {
		var oItem;
		for (var item = 0; item < this.elements.length; item++) {
			oItem = this.elements[item];
			if (oItem.clauseId == id)
				return oItem;
		}
		return null;
	};
	

	this.add = function add(oDocClauseItem) {
		this.elements.push(oDocClauseItem);
	};

	this.createJson = function createJson() {
		var result = new Array();
		var oItem;
		for (var item = 0; item < this.elements.length; item++) {
			oItem = this.elements[item];
			result.push(oItem.createJson());
		}
		return result;
	};
	
}

// DocAnswerItem ==================================================================================================
function DocAnswerItem() {
	this.id;
	this.questionId;
	this.answer;
	
	this.loadJson = function loadJson(json) {
	    this.id = json.document_answer_id;
	    this.questionId = json.question_id;
	    this.answer = json.question_answer;
	};

	this.createJson = function createJson() {
		var result = {
			"document_answer_id": this.id,
			"question_id": this.questionId,
			"question_answer": this.answer
	    }
		return result;
	};
	
}

// ----------------------------------------------------------------
function DocAnswerSet() {
	this.elements = new Array();

	this.loadJson = function loadJson(json) {
		this.elements.length = 0;
		if (json) {
			var oItem;
			for (var iData = 0; iData < json.length; iData++) {
				oItem = new DocAnswerItem();
				oItem.loadJson(json[iData]);
				this.elements.push(oItem);
			}
		}
	};
	
	this.isEmpty = function isEmpty() {
		return (this.elements.length < 1); 
	};
	
	this.findByQuestionId = function findByQuestionId(id) {
		var oItem;
		for (var item = 0; item < this.elements.length; item++) {
			oItem = this.elements[item];
			if (oItem.questionId == id)
				return oItem;
		}
		return null;
	};

	this.add = function add(oDocAnswerItem) {
		this.elements.push(oDocAnswerItem);
	};

	this.createJson = function createJson() {
		var result = new Array();
		var oItem;
		for (var item = 0; item < this.elements.length; item++) {
			oItem = this.elements[item];
			result.push(oItem.createJson());
		}
		return result;
	};
	
}

// DocAnswerItem ==================================================================================================
function DocFillInItem() {
	this.id;
	this.clauseFillInId;
	this.answer;
	this.fullTtextModified = false;
	
	this.loadJson = function loadJson(json) {
	    this.id = json.document_fill_in_id;
	    this.clauseFillInId = json.clause_fill_in_id;
	    this.answer = json.document_fill_in_answer;
	    this.fullTtextModified = json.full_text_modified;
	};

	this.createJson = function createJson() {
		var result = {
			"document_fill_in_id": this.id,
			"clause_fill_in_id": this.clauseFillInId,
			"document_fill_in_answer": this.answer,
			"full_text_modified": this.fullTtextModified
	    }
		return result;
	};

}

function DocFillInSet() {
	this.elements = new Array();

	this.loadJson = function loadJson(json) {
		this.elements.length = 0;
		if (json) {
			var oItem;
			for (var iData = 0; iData < json.length; iData++) {
				oItem = new DocFillInItem();
				oItem.loadJson(json[iData]);
				this.elements.push(oItem);
			}
		}
	};
	
	this.isEmpty = function isEmpty() {
		return (this.elements.length < 1); 
	};
	
	this.findByClauseFillInId = function findByClauseFillInId(id) {
		var oIteml
		for (var item = 0; item < this.elements.length; item++) {
			var oItem = this.elements[item];
			if (oItem.clauseFillInId == id)
				return oItem;
		}
		return null;
	};

	this.add = function add(oDocFillInItem) {
		this.elements.push(oDocFillInItem);
	};

	this.createJson = function createJson() {
		var result = new Array();
		for (var item = 0; item < this.elements.length; item++) {
			//var oItem = this.elements[item];
			result.push(this.elements[item].createJson());
		}
		return result;
	};
	
}

// Document ==================================================================================================
function Document() {
    this.id;
    this.type;
    this.title;
    this.number;
    this.isFinalized;
    this.contractNumber;
    this.orderNumber;
    this.fullName;
    this.versionName;
    this.clauseVersionId;
    this.updatedAt;
    this.isLinkedAward;
    this.unsavedChanged = false;

    this.lastQuestions = new LastQuestions(); // CJ-1074 at interview-utils.js
    this.lastQuestionCode;
    
    this.answers = new DocAnswerSet();
    this.fillins = new DocFillInSet();
    this.clauses = new DocClauseSet();
    
	this.loadJson = function loadJson(json) {
	    this.id = json.document_id;
	    this.type = json.document_type;
	    this.title = json.acquisition_title;
	    this.number = json.document_number;
	    //this.isCompleted = json.is_interview_completed;
	    this.isFinalized =  (json.doc_status_cd == 'F') ? 1 : 0; 
	    this.contractNumber = json.contract_number;
	    this.orderNumber = json.order_number;
	    this.fullName = json.document_full_name;
	    this.clauseVersionId = json.clause_version_id;

	    this.lastQuestions.fromString(json.last_question_code); // CJ-1074
	    this.lastQuestionCode = this.lastQuestions.activeQuestionPanelId;
	    //this.lastQuestionCode = json.last_question_code;
	    
	    // _isManualOrders = json.is_manual_orders; // CJ-1005
	    
	    this.versionName = json.VersionName;
	    this.updatedAt = json.updated_at;
	    this.isLinkedAward = json.isLinkedAward;
	    _isLinkedAward = this.isLinkedAward; 
	    
	    this.answers = new DocAnswerSet();
	    this.fillins = new DocFillInSet();
	    this.clauses = new DocClauseSet();

	    if (_isManualOrders == false)
	    	this.answers.loadJson(json.Document_Answers);
	    this.fillins.loadJson(json.Document_Fill_Ins);
	    this.clauses.loadJson(json.Document_Clauses);
	};
	
	this.createJson = function createJson(qConditions, prescriptionChoices, qGroup) {
	    var oDocAnswerSet = new DocAnswerSet();
	    var oDocClauseSet = new DocClauseSet();
	    var oDocFillInSet = new DocFillInSet();

	    if (_isManualOrders == false)
	    	qConditions.saveAnswers(oDocAnswerSet);
	    prescriptionChoices.saveAnswers(oDocClauseSet, oDocFillInSet);
	    
	    var aDocAnswerSet = oDocAnswerSet.createJson();
	    if ((_isManualOrders == false) && (aDocAnswerSet.length == 0)) {
	    	logOnConsole('Empty DocAnswerSet');
	    }
	    
	    var sLastQuestions = this.lastQuestions.toString(qGroup, (_resetQuestionPanelId ? _resetQuestionPanelId : _activeQuestionPanelId)); // CJ-1074
		
	    var result = {
			"document_id": this.id,
			"document_type": this.type,
			"acquisition_title": this.title,
			"document_number": this.number,
			//"is_interview_completed": this.isCompleted,
			"doc_status_cd" : (this.isFinalized) ? "F" : "I",			
			"contract_number": this.contractNumber,
			"order_number": this.orderNumber,
			"document_full_name": this.fullName,
			//"is_manual_orders": _isManualOrders, // CJ-1005
			"clause_version_id": this.clauseVersionId,
			"last_question_code": sLastQuestions, // CJ-1074 (_resetQuestionPanelId ? _resetQuestionPanelId : (_activeQuestionPanelId ? _activeQuestionPanelId.toString() : null)), 
			"Document_Answers": aDocAnswerSet,
			"Document_Clauses": oDocClauseSet.createJson(),
			"Document_Fill_Ins": oDocFillInSet.createJson()
	    }
		return result;
	};
	
	// CJ-1400, Reset means clearing all answers, clauses, fillins.
	this.createResetJson = function createJson(qConditions, prescriptionChoices, qGroup) {
	    		
		var oDocAnswerSet = new Array();
	    var oDocClauseSet = new Array();
	    var oDocFillInSet = new Array();
	    
	    var result = {
			"document_id": this.id,
			"document_type": this.type,
			"acquisition_title": this.title,
			"document_number": this.number,
			"doc_status_cd" : "I",			
			"contract_number": this.contractNumber,
			"order_number": this.orderNumber,
			"document_full_name": this.fullName,
			"clause_version_id": this.clauseVersionId,
			"last_question_code": null,  
			"Document_Answers": oDocAnswerSet,
			"Document_Clauses": oDocClauseSet,
			"Document_Fill_Ins": oDocFillInSet
	    }
		return result;
	};
};

var _debugStart = null, _debugEnd = null;
//var _loadingStep = 0;
//var _oDivBarLoading = $('#divBarLoading');
//var _oSpanLoadingPercent = document.getElementById('spanLoadingPercent');

function showLoadingProgress(step, log) {
	//_loadingStep += step;
	//var percent = Math.round((_loadingStep / 27) * 100);
	//_oSpanLoadingPercent.innerHTML = percent + "%";
	//_oSpanLoadingPercent.style.display="visible";
	//$('#divBarLoading').css('width', percent + '%').attr('aria-valuenow', percent); // .progress-bar
	// jQuery('#divBarLoading').animate({width: percent + '%'});
	//if (log && (!(typeof console === "undefined" || typeof logOnConsole === "undefined"))) {
	//	logOnConsole(formatTime(new Date()) + log);
	//}
	//$('#divBarLoading').css('width', percent + '%').attr('aria-valuenow', percent); // .progress-bar
	//doSleep(1);
}

// Interview ==================================================================================================
function Interview() {
	this.regulations = new RegulationSet();
	this.prescriptions = new PrescriptionSet();
	this.sections = new ClauseSectionSet();
	this.clauses = new ClauseSet();
	this.qConditions = new QuestionConditionSet();
	this.questions = new QuestionSet();
	this.qGroup = new QGroupSet();
	this.prescriptionChoices = new PrescriptionChoices();
	this.doc = new Document();
	this.isCommercialProcedure = false; // CJ-476
	this.isSevice = false; // CJ-476
	this.isAward = false; // CJ-606
	this.userInfo;
	
	this.ready = false;
	this.lastClick = new Date().getTime();
	this.pendingSave = false;
	
	this.fac_number;
	this.dac_number;
	this.dfars_number;
	this.fac_date;
	this.dac_date;
	this.dfars_date;
	
	this.sat_conditions;
	//CJ-1465 SV
	this.sat_750k_rule;
	this.sat_300k_rule;
	this.sat_1million_rule;
	
	this.autoSavingInterval;
	
	this.retrieve = function retrieve(docId, onCompleteCallBack, userInfo, forTranscript) {
		this.userInfo = userInfo;
		if (userInfo.isToken) {
			_isToken = true;
			_Token = userInfo.token;
		}
		
		_interview = this;
		showLoadingProgress(1, "Interview().retrieve() Start");
		_debugStart = new Date();
		
		var sDoParam;
		if (forTranscript) // CJ-1223
			sDoParam = "doc-transcript";
		else
			sDoParam = "doc-interview";

		$.ajax({
			url: getAppPath() + 'page?do=' + sDoParam + '&id=' + docId,
			cache: false,
			dataType: 'json',
			success: function(data) {
				try {
					if (data.status == 'success') {
						_interview.loadJson(data.data);
						if (onCompleteCallBack) {
							showLoadingProgress(1, "onCompleteCallBack() Start");
							window.onbeforeunload = null;
							onCompleteCallBack(_interview, true);
						}
					} else if (data.status == 'UpgradeClsVersion') {
						_interview.doc.loadJson(data.data.Documents);
						_interview.ready = true;
						if (onCompleteCallBack)
							onCompleteCallBack(_interview, true, data.data);
					} else if ('no session' == data.message) {
						displaySessonEnded(true); // CJ-573
						//alert('You have been logged out for being inactivity, please refresh the page and log back into CLS to continue...');
						//goHome();						
					} else {
						alert('Requested document was not found');
						goDashboard();
					}
					showLoadingProgress(1, "onCompleteCallBack() End");
				}
				catch (oError) {
					alert("JavaScript Error: " + oError);
				}
			},
			error: function(object, text, error) {
				logAjaxErrorOnConsole(object, text, error);
				alert("AJAX Error: " + object.responseText);
			}
		});
	};
	
	this.loadJson = function loadJson(json) {
		showLoadingProgress(1, "this.regulations.loadJson()");
		this.regulations.loadJson(json.Regulations);
		showLoadingProgress(1, "this.prescriptions.loadJson()");
		this.prescriptions.loadJson(json.Prescriptions);
		showLoadingProgress(1, "this.sections.loadJson()");
		this.sections.loadJson(json.Clause_Sections);
		showLoadingProgress(1, "this.clauses.loadJson()");
		this.clauses.loadJson(json.Clauses);

		showLoadingProgress(1, "addSatDefintionClauses()");
		// CJ-1360, only one version record with SAT
		for (var iEle = 0; iEle < json.Clause_Versions.length; iEle++) {	
			this.sat_conditions = json.Clause_Versions[iEle].sat_conditions;
			this.sat_750k_rule = json.Clause_Versions[iEle].sat_750k_rule;
			this.sat_300k_rule = json.Clause_Versions[iEle].sat_300k_rule;
			this.sat_1million_rule = json.Clause_Versions[iEle].sat_1million_rule;
		}
		addSatDefintionClauses(this.clauses, this.sat_conditions, this.sat_300k_rule, this.sat_750k_rule, this.sat_1million_rule);
		
		showLoadingProgress(1, "this.qConditions.loadJson()");
		this.qConditions.loadJson(json.Question_Conditions);
		showLoadingProgress(1, "this.questions.loadJson()");
		this.questions.loadJson(json.Questions);
		showLoadingProgress(1, "this.qGroup.loadJson()");
		this.qGroup.loadJson(json.Question_Group_Ref);
		showLoadingProgress(1, "this.doc.loadJson()");
		this.doc.loadJson(json.Documents);

		showLoadingProgress(1, "this.qGroup.resolveObjectReferences()");
		this.qGroup.resolveObjectReferences(this.qConditions, this.questions, this.prescriptions);
		showLoadingProgress(1, "this.clauses.resolveObjectReferences()");
		this.clauses.resolveObjectReferences(this.sections, this.questions, this.doc.clauses, this.doc.fillins);
		
		showLoadingProgress(1, "this.clauses.prepareEvals()");
		this.clauses.prepareEvals(this.questions);

		if (_isManualOrders == false) {
			showLoadingProgress(1, "this.qConditions.loadAnswers()");
			this.qConditions.loadAnswers(this.doc.answers);
		}
		showLoadingProgress(1, "this.prescriptionChoices.loadAnswers()");
		this.prescriptionChoices.loadAnswers(this.doc.clauses, this.clauses);
		
		// CJ-477, only one version record with FAC and DAC
		for (var iEle = 0; iEle < json.Clause_Versions.length; iEle++) {
		    this.fac_number = json.Clause_Versions[iEle].fac_number;
		    this.dac_number = json.Clause_Versions[iEle].dac_number;	
		    this.dfars_number = json.Clause_Versions[iEle].dfars_number;	
			this.fac_date = json.Clause_Versions[iEle].fac_date;	
			this.dac_date = json.Clause_Versions[iEle].dac_date;	
			this.dfars_date = json.Clause_Versions[iEle].dfars_date;	
		}

		showLoadingProgress(1, "gSatEval.prepareEval()");
		gSatEval.prepareEval(this.clauses, this.regulations, this.prescriptionChoices);
		
		this.clauses.checkOptionCondition();	// CJ-802
		
		// CJ-180, Run Rules at the start.
		_oFinalCheckSet = new FinalCheckSet();
		this.clauses.finalCheck(_oFinalCheckSet, this.prescriptionChoices);
		this.clauses.finalCheck(_oFinalCheckSet, this.prescriptionChoices);	// run twice, since the first check may have additional impacts. 

		this.ready = true;
	};
	
	this.onInputChanged = function onInputChanged(questionId, baseQuestionId, choiceId, value) {
		this.questions.onInputChanged(questionId, baseQuestionId, choiceId, value);
		this.prescriptionChoices.postChangeEvent();
		if (_oUserInfo) // CJ-696
			_oUserInfo.timelyRefreshTimeout();
		//this.onClick(); // Removed for CJ-696
	};
	
	this.onOrderCheckboxChanged = function onOrderCheckboxChanged(psClauseId, value, pParam) {
		var oClause = this.clauses.findById(psClauseId);
		if (oClause) {
			this.prescriptionChoices.setOrderClause(oClause, value);
			if (pParam == 3) {
				var sId = oClause.getOrderCheckboxId(0);
				$('#' + sId).attr('checked', value);
				sId = oClause.getOrderCheckboxId(1);
				$('#' + sId).attr('checked', value);
			} else {
				var sId = oClause.getOrderCheckboxId(pParam == 1 ? 0 : 1);
				$('#' + sId).attr('checked', value);
			}
			_interview.doc.unsavedChanges = true;
			window.onbeforeunload = confirmOnInterviewExit;
		} else {
			logOnConsole('Interview.onOrderCheckboxChanged: invalid clause id of "' + psClauseId + '"');
		}
	};
	
	this.onMoveQuestion = function onMoveQuestion(forward, questionId, baseQuestionId, isFinal) {
		this.questions.onMoveQuestion(forward, questionId, baseQuestionId, isFinal);
		if (_oUserInfo) // CJ-696
			_oUserInfo.timelyRefreshTimeout();
	};

	this.getFocusedQuestion = function getFocusedQuestion(baseQuestionId) {
		var oBaseQuestion = this.questions.findById(baseQuestionId);
		if (oBaseQuestion == null)
			return null;
		return oBaseQuestion.focusedQuestion;
	}
	
	
	this.SetActivePanel = function SetActivePanel(code) {
		this.questions.SetActivePanel (code);
	};
	
	this.setToManualOrders = function setToManualOrders() {
		_isManualOrders = true;
		this.qConditions.setToManualOrders();
	};
	
	this.adjustHeight = function(maxHeight) { // CJ-1198
		/*
		$('div[id^="content"]').each(function () {
			$(this).height(maxHeight);
		    console.log('adjustHeight: ' + this.id);
		});
		*/
		maxHeight = maxHeight- 45;
		_maxQuestionPanelHeight = maxHeight;
		$('.QGroupBody').css('height', maxHeight);
		$('.QuestionBody').css('height', maxHeight - 100);
	}
	
	this.constructScreen = function constructScreen(idGroupButtons, idContent, idUlTranscript, idPrescrptionContainer, idSectionContent, maxHeight) {
		// CJ-1401
		_activeQuestionPanelId = null;
		//_resetQuestionPanelId = null;

		var oIdSectionContent = $('#' + idSectionContent);
		var html;
		
		this.adjustHeight(maxHeight); // CJ-1198
		_maxQuestionPanelHeight = maxHeight; // CJ-638
		if (_isManualOrders == false) {
			//var groupHtmlButtons = '';
			var groupHtmlContent = '';
			var groupAccordian = '<div class="panel-group" id="accrdnQGroup">';
			var aGroups = this.qGroup.elements;

			showLoadingProgress(1, "this.constructScreen() #1 collects");
			var oGroup;
			for (var iGroup = 0; iGroup < aGroups.length; iGroup++) {
			//for (var iGroup = 0; iGroup < 1; iGroup++) { // 508 Testing
				oGroup = aGroups[iGroup];
	
				// Pass pointer to the next Tab, for use on the Transcript page.
				var nextQuestionTab = null;
				var iNextGroup = iGroup;
				while (iNextGroup<aGroups.length-1){
					nextQuestionTab = aGroups[iNextGroup + 1];
					if (nextQuestionTab.questions.length == 0)
						iNextGroup++;
					else
						break;
				}
				
				html = oGroup.genHtml(nextQuestionTab, this.doc.lastQuestions); // mlee
				if (html) {
					if (iGroup == 0)
						_activeGroupCode = oGroup.code;
					
					var sName = oGroup.name.replace(/"/g, '&quot;');
					groupHtmlContent += '<div id="panel-' + oGroup.code + '" class="panel panel-primary" style="border: 0;' + ((iGroup == 0) ? '' : 'display:none;') + '">'
						//+ '<div class="panel-heading"><i class="fa fa-tasks"></i> ' + sName.toUpperCase() + '</div>'
						+ '<div class="panel-heading QPanelHeading" > ' + sName.toUpperCase() + '</div>'
						// add display and height to the style; remove max-height.
						+ '<div id="content' + oGroup.code + '" class="panel-body QGroupBody">' // CJ-1198 changed from '<div id="content' + oGroup.code + '" class="panel-body" style="display:block;height:' + maxHeight + 'px;padding-left:0px;padding-right:5px;">'
						+ html
						+ '</div>'
						+ '</div>';
	
					groupAccordian += oGroup.genAccordion(iGroup == 0);
				}
				
				/*
				// begin
				if (iGroup == 0) { 
					html = oGroup.genHtml(nextQuestionTab, this.doc.lastQuestions); // mlee
				
					_activeGroupCode = oGroup.code;
					
					var sName = oGroup.name.replace(/"/g, '&quot;');
					groupHtmlContent += '<div id="panel-' + oGroup.code + '" class="panel panel-primary" style="border: 0;' + ((iGroup == 0) ? '' : 'display:none;') + '">'
						//+ '<div class="panel-heading"><i class="fa fa-tasks"></i> ' + sName.toUpperCase() + '</div>'
						+ '<div class="panel-heading QPanelHeading" > ' + sName.toUpperCase() + '</div>'
						// add display and height to the style; remove max-height.
						+ '<div id="content' + oGroup.code + '" class="panel-body QGroupBody">' // CJ-1198 changed from '<div id="content' + oGroup.code + '" class="panel-body" style="display:block;height:' + maxHeight + 'px;padding-left:0px;padding-right:5px;">'
						+ html
						+ '</div>'
						+ '</div>';
				}
				groupAccordian += oGroup.genAccordion(iGroup == 0);
				// end
				*/
			}
			groupAccordian += '</div>';
			
			var oDivQuestionGroup = $('#divQuestionGroup');
			var oIdContent = $('#' + idContent);
			
			showLoadingProgress(1, "this.constructScreen() #1 $('#divQuestionGroup').html(groupAccordian)");
			oDivQuestionGroup.html(groupAccordian); // $(oDivQuestionGroup)
			
			showLoadingProgress(1, "this.constructScreen() #1 $('#' + idContent).html(groupHtmlContent)");
			//$('#' + idGroupButtons).html(groupHtmlButtons);
			oIdContent.html(groupHtmlContent); // $(oIdContent)
			//prepareCollapsibleGlyphicon();
	
			showLoadingProgress(1, "this.constructScreen() #2 onQuestonGroupShown(e)");
			var oGroup, sGroupCode;
			for (var iGroup = 0; iGroup < aGroups.length; iGroup++) {
				oGroup = aGroups[iGroup];
				sGroupCode = oGroup.code;
				$('#' + oGroup.getAccordionId()).on('shown.bs.collapse', function (e) {
					onQuestonGroupShown(e);
					//onBtnGroupClick(oGroup.code);
				});
			}
			
			showLoadingProgress(1, "this.constructScreen() #3 resumeLastQuestionPanel()");
			// CJ-160
			_activeQuestionPanelId = this.initActivePanelId();
			if (_resetQuestionPanelId) { 
				this.skipToQuestionPanel (_resetQuestionPanelId);
				_resetQuestionPanelId = null;
			}
			else
				this.resumeLastQuestionPanel (_activeQuestionPanelId);
		}
		
		// Sections is where the Document Clause Cart information is being collected.
		showLoadingProgress(1, "this.constructScreen() #4 this.sections.genHtml()");
		html = this.sections.genHtml();
		showLoadingProgress(1, "this.constructScreen() #5 $('#' + idSectionContent).html(this.sections.genHtml())");
		oIdSectionContent.html(html); // $(oIdSectionContent)

		showLoadingProgress(1, "this.constructScreen() #6 sections.setupEvents()");
		this.sections.setupEvents();
		
		showLoadingProgress(1, "this.constructScreen() #7 adjustProgressBar()");
		this.adjustProgressBar(0); 

		// CJ-180, Run Rules at the start.
		//_oFinalCheckSet = new FinalCheckSet();
		//this.clauses.finalCheck(_oFinalCheckSet, this.prescriptionChoices);
		
		_debugEnd = new Date();
		var oDuration = _debugEnd - _debugStart;
		logOnConsole('Duration: ' + oDuration + " msec");
	};
	
	/* CJ-1005
	this.genOrderSelOptions = function genOrderSelOptions(pFarTableId, pDfarTableId) {
		var oSelectedClauses = this.prescriptionChoices.preapreOrdersInterface(); // clauses;
		return this.clauses.genOrderSelOptions(pFarTableId, pDfarTableId, oSelectedClauses);
	};
	*/
	
	this.adjustProgressBar = function adjustProgressBar(iArea) {
		if ((iArea == 0) || (iArea == 1))
			this.qGroup.getProgress(_progress);
		if ((iArea == 0) || (iArea == 2))
			this.prescriptionChoices.getProgress(_progress);
		_progress.display();
	};
	
	this.onRunRules = function onRunRules() {
		_oFinalCheckSet = new FinalCheckSet();
		this.clauses.finalCheck(_oFinalCheckSet, this.prescriptionChoices);
		
		var html = _oFinalCheckSet.genHtml();
		showDetailedContent (html);
		
		// update the clause cart
		this.prescriptionChoices.postChangeEvent();

		if (_oUserInfo) // CJ-696
			_oUserInfo.timelyRefreshTimeout();
	};
	
	
	// Add a dialog to display Transcript content
	this.onTranscriptBtnClick = function onTranscriptBtnClick(bPrintView, bNoLink) { // CJ-1223 bNoLink
		var html = this.qGroup.getTranscriptTree(bPrintView ? true : bNoLink);
		if (bPrintView)
		{
			// CJ-1384, Add header info to the transcript pop-up.
			var prHtml = '<span>ACQUISITION NAME: ' +  _interview.doc.title.toUpperCase() + '</span><br/>' + 
			'<span>DOCUMENT NUMBER: ' + _interview.doc.number.toUpperCase() + '</span><br/>' + 
			'<span>DOCUMENT TYPE: ' + getDocumentTypeName(_interview.doc.type).toUpperCase() + '</span><hr>'+ html;

			_transcriptWindow.document.write(prHtml);
		}
		else
			showTranscriptContent (html);
		if (_oUserInfo) // CJ-696
			_oUserInfo.timelyRefreshTimeout();
	}
	
	// CJ-1400, if the reset succeeds, remove saved answers from main array.
	this.resetAnswers = function resetAnswers() {
		// CJ-1401
		this.prescriptionChoices = new PrescriptionChoices();
		this.isCommercialProcedure = false;
		this.isSevice = false;
		this.isAward = false;
		
		// Clear question answers.
		var oQuestion;
		for (var iEle = 0; iEle < this.questions.elements.length; iEle++) {
			oQuestion = this.questions.elements[iEle];
			oQuestion.answer = null;
			for (var iEle2 = 0; iEle2 < oQuestion.choices.elements.length; iEle2++) {
				 oQuestion.choices.elements[iEle2].answer = null;
			}
		}
		// Set added clauses back to system
		for (var iEle = 0; iEle < this.prescriptionChoices.clauses.length; iEle++)  {
			this.prescriptionChoices.clauses[iEle].optionalUserActionCode = null;		
		}
		this.prescriptionChoices.clauses.length = 0;
	}
	
	// Add a dialog to display Transcript content
	this.onResetBtnClick = function onResetBtnClick() {

		// The reset dialog combines the elements for all reset activities,
		// Warning, Processing, Success.
		
		// Show the elements for Reset - Processing...
		$('#resetModalLabel').hide();
		$('#resetBody').hide();
		$('#bnResetNo').hide();
		$('#bnResetYes').hide();
		$('#bnResetClose').hide();
		$('#resetFooter').hide();
		$('#resetHeader').show();
		$('#resetProcessHeader').show();
		$('#resetProcessMessage').show();
		$('#resetProcessSpinner').show();
		
		this.doc.lastQuestionCode = null;
		_resetQuestionPanelId = this.initActivePanelId();
		_resetQuestionPanelId = _resetQuestionPanelId.replace("qf-", "qd-");

		
		this.saveDocument(SAVE_ON_RESET);
		
		if (_oUserInfo) // CJ-696
			_oUserInfo.timelyRefreshTimeout();
	}
	
	this.autoSaveTimeout = function autoSaveTimeout() {
		if (this.autoSavingInterval != null) {
			window.clearTimeout(this.autoSavingInterval);
			this.autoSavingInterval = null;
			logOnConsole('clearAutoSaveTimeoutInstance()');
			
			this.autoSavingInterval = window.setTimeout(function() {_interview.autoSaveTimeout()}, 600000);
			
			if (_interview.pendingSave == false && _interview.doc.unsavedChanges == true)
				this.saveDocument(SAVE_ON_TIMEOUT);
		}
	}
	
	this.onSaveBtnClick = function onSaveBtnClick(pMode) {

		if (SAVE_ON_TIMEOUT != pMode)
			window.onbeforeunload = null;
		
		_oFinalCheckSet = null;
		if (SAVE_ON_FINALIZE == pMode) { // Finalize and Exit	
			if ((this.doc.type == 'O') || _isManualOrders) { // Cross-over check for none Order type
				this.prescriptionChoices.getProgress(_progress);
			} else {
				_oFinalCheckSet = new FinalCheckSet();
				this.clauses.finalCheck(_oFinalCheckSet, this.prescriptionChoices);
				if (_oFinalCheckSet.hasItems()) {
					if (reviewCrossitemsForComplete(this, _progress, _oFinalCheckSet) == false)
						_oFinalCheckSet = null;
					return;
				}
			}
		}
		if (validateBeforeSave(pMode, this, _progress) == false)
			return;
		
		if (_interview.pendingSave == false && _interview.doc.unsavedChanges == true) {
			this.saveDocument(pMode);
			return;
		}
		
		
		if (SAVE_ON_EXIT == pMode || SAVE_ON_FINALIZE == pMode || SAVE_ON_WORKSHEET == pMode) {	
			if (_interview.pendingSave == true) { //catch edge case where a save or finalize and exit is attempted during a pending save request

				// CJ-617, Breakout message for API 
				if (SAVE_ON_WORKSHEET == pMode) {	// Clause worksheet
					if (_token && this.doc.isFinalized)	// If API, display message.
						alert('Your interview is finalized and cannot be modified. If modifications need to be made, please reopen your interview. Click OK to proceed.');
					else
						this.saveDocument(pMode);
				}
				else  
					alert('The system has automatically saved your document. Click OK to proceed.');
			}
			else
				this.saveDocument(pMode);
		}
	};
	this.saveDocument = function saveDocument(pMode) {
		
		//logOnConsole ("saveDocument = " + pMode);
		
		_oFinalCheckSet = null;
		_interview.pendingSave = true;
		_interview.doc.unsavedChanges = false;
		if (SAVE_ON_FINALIZE == pMode)
			this.doc.isFinalized = true;
		else 
			this.doc.isFinalized = false;
				
		var instance = this;
		var jsonDoc = null;
		// CJ-1400
		if (SAVE_ON_RESET == pMode)
			jsonDoc = this.doc.createResetJson(this.qConditions, this.prescriptionChoices, this.qGroup); 
		else
			jsonDoc = this.doc.createJson(this.qConditions, this.prescriptionChoices, this.qGroup); 
		
		var oData = {
			id: this.doc.id,
			mode: pMode,
			data: JSON.stringify(jsonDoc)
		};
				
		//logOnConsole ("saveContent = " + oData.data);
		
		if (this.showSaveWaitModal(pMode))
		{
			$('#saveWaitDialog').modal('show');
		}
		$.ajax({
			url: getAppPath() + 'page?do=doc-post',
			type: 'POST',
			data: oData,
			cache: false,
			dataType: 'JSON',
			success: function(data) {
				$('#saveWaitDialog').modal('hide');
				$('#pleaseWaitDialog').modal('hide');
				if ('success' == data.status) {
					if ((!_token) && ((SAVE_ON_EXIT == pMode) || (SAVE_ON_FINALIZE == pMode))) {
						location = 'dashboard.jsp';
					}
					else if (SAVE_ON_WORKSHEET == pMode) {
						location = 'final_clauses.jsp?' + (_isToken ? 'token=' + _Token : 'doc_id=' + oData.id);
					}
					else if (SAVE_ON_RESET == pMode) {	// reset
						window.onbeforeunload = null;
						_interview.resetAnswers();
						onDocumentReset(instance);
					}
					else {
						instance.doc.updatedAt = data.data.updated_at;
						onDocumentSaved(instance, data.message, pMode, data); // CJ-573
					}
				} else {
					onDocumentSaved(instance, data.message, pMode); // CJ-573 // alert(data.message);
				}
			},
			error: function(object, text, error) {
				$('#saveWaitDialog').modal('hide');
				$('#pleaseWaitDialog').modal('hide');
				
				// CJ-1382, close reset dialog.
				if (SAVE_ON_RESET == pMode) {	// reset
					$('#resetModal').modal('hide');
				}
				
				// CJ-1382, reset flags and show server error dialog
				_interview.pendingSave = false;
				_interview.doc.unsavedChanges = true;
				myAlert("Connection Lost!", "Please check your network connection and try again.");
				
				//goHome();
			}
		});
	};
	
	/* Removed for CJ-696
	this.onClick = function onClick() {
		var iNow = new Date().getTime();
		if (((iNow - this.lastClick) / 1000) > 60) {
			this.lastClick = iNow;
			$.ajax({
				url: getAppPath() + 'page?do=doc-alive',
				cache: false,
				success: function(data) { }
			});
		}
	};
	*/
	
	this.showSaveWaitModal = function showSaveWaitModal(pMode)
	{
		var showWaitModal = true;
		if ((!_token) && ((SAVE_ON_EXIT == pMode) || (SAVE_ON_FINALIZE == pMode))) {
			showWaitModal = false;
		}
		else if (SAVE_ON_WORKSHEET == pMode) {
			showWaitModal = false;
		}
		else if (SAVE_ON_RESET == pMode) {	
			showWaitModal = false;
		}
		return 	showWaitModal;
		
	}
	
	// Set the active panel name, when the page is first loaded.
	this.initActivePanelId = function initActivePanelId () {
		var lastQuestionPanel = new NavigationItem();
		lastQuestionPanel.init(this.doc.lastQuestionCode);
		
		return lastQuestionPanel.name;	
	};
	
	this.resumeLastQuestionPanel = function resumeLastQuestionPanel (newPanelId) {
		var ns = new NavigationSet();
		ns.resumeLastQuestionPanel (newPanelId);
	};
	
	this.skipToQuestionPanel = function skipToQuestionPanel (newPanelId) {
		var ns = new NavigationSet();
		ns.skipToQuestionPanel (_activeQuestionPanelId, newPanelId);
		if (_oUserInfo) // CJ-696
			_oUserInfo.timelyRefreshTimeout();
	};
	
	this.disableAllEntriesForTimeout = function disableAllEntriesForTimeout() {
		$('[id^=' + QUESTION_CHOICE_PREFIX + ']').attr("disabled", true);
		$('[id^=' + CLAUSE_EDIT_LINK_PREFIX + ']').attr("disabled", true);
		$('[id^=' + CLAUSE_ORDER_CB_PREFIX + ']').attr("disabled", true);
	};
	
	this.isFirstQuestion = function isFirstQuestion(questionItem)
	{
		var firstAppliableQuestion = _interview.qGroup.getFirstAppliableQuestionItem();
		if ((firstAppliableQuestion == null) || (questionItem == null))
		{
			//SHould not be happen.		
			return false;
		}
		return (firstAppliableQuestion.id == questionItem.id);


	}
	
}

//NavigationItem ==================================================================================================
// Most navigation can be handle by the main classes, 
// since the questions are linked by next and previous.

// The Transcript summary is a little different, 
// because the navigation can be initiated from anywhere
// on the screen.

function NavigationSet() {
	
	this.skipToQuestionPanel = function skipToQuestionPanel (curPanelId,newPanelId) {
		var defaultPanel = new NavigationItem();
		var newPanel = new NavigationItem();
		
		defaultPanel.init (curPanelId);
		newPanel.init (newPanelId);
		
		this.showQuestionPanel (defaultPanel, newPanel) ;
		if (_interview.isFirstQuestion(newPanel.questionItem))
		{
			$("#" + newPanel.baseItem.getBaseNextBtnId()).hide();
		}
		// Bev removed CJ-611
		//this.disableFirstPrevBttn (newPanel); // CJ-611

		if (_oUserInfo) // CJ-696
			_oUserInfo.timelyRefreshTimeout();
	};
	
	this.resumeLastQuestionPanel = function resumeLastQuestionPanel (newPanelId) {
		var defaultPanel = new NavigationItem();
		var newPanel = new NavigationItem();
		
		defaultPanel.init (null);
		newPanel.init (newPanelId);
		
		if (newPanel.baseItem && (newPanel.name == newPanel.baseItem.getFinalPageId())) // CJ-1169
			newPanel.baseItem.focusedQuestion = null;
		
		if (newPanel.baseItem && // CJ-1406
			newPanel.baseItem.focusedQuestion && newPanel.questionItem && // CJ-1169
			(newPanel.baseItem.focusedQuestion.id != newPanel.questionItem.id))
		{
			newPanel.baseItem.focusedQuestion = newPanel.questionItem;
		}
		
		if (newPanel.baseItem) // CJ-1406
			this.showQuestionPanel (defaultPanel, newPanel, true) ;
		
		// Bev removed CJ-611
		//this.disableFirstPrevBttn (newPanel); // CJ-611
		
	}
	
	this.showQuestionPanel = function showQuestionPanel (defaultPanel, newPanel, resumeLastQuestion) {
		var defaultQuestion = defaultPanel.questionItem;
		var newQuestion = newPanel.questionItem;
		
		// Different Tabs
		if (defaultQuestion.baseGroupId != newQuestion.baseGroupId) { 		
			this.onSkipToGroup(defaultPanel, newPanel);
		}
		// Different subgroups
		else if (defaultQuestion.baseSubGroupId != newQuestion.baseSubGroupId) { 				
			this.onSkipToSubGroup(defaultPanel, newPanel);
			this.onSkipToQuestion(newPanel);
		}
		// Different questions
		else if (defaultQuestion.id != newQuestion.id) { 			
			this.onSkipToQuestion(newPanel);	
		}
		// First page vs Summary
		else if (defaultPanel.name != newPanel.name) { 			
			this.onSkipToQuestion(newPanel);	
		}
		else {
			this.showQuestion(newPanel);
			
			if (resumeLastQuestion) { // CJ-824
				
				var oQuestion = defaultQuestion.focusedQuestion;
				if (oQuestion)
				{
					oQuestion.showResumePrompt(false);		
					if (_interview.isFirstQuestion(newPanel.baseItem.focusedQuestion))
					{
						$("#" + newPanel.baseItem.getBaseNextBtnId()).hide();
					}
				}
				else
				{
					defaultQuestion.showResumePrompt(true);							
				}
					
			}
			return;
		}
		if (resumeLastQuestion) { // CJ-824
			var oQuestion = newQuestion.focusedQuestion;
			if (oQuestion)
			{
				oQuestion.showResumePrompt(false);	
				if (_interview.isFirstQuestion(newPanel.baseItem.focusedQuestion))
				{					
					$("#" + newPanel.baseItem.getBaseNextBtnId()).hide();
				}
			}
			else
			{
				newQuestion.showResumePrompt(true);
			}
		}
	};
	
	this.onSkipToGroup = function onSkipToGroup(currentSubGroup, newSubGroup) {
		var newGroupCode = newSubGroup.baseItem.group;
		_activeQuestionPanelId = newSubGroup.name; // set before click event, to avoid race condition.
		
		newSubGroup.questionItem.qGroupItem.onSkipToGroup(newSubGroup.baseItem); // CJ-951
		/*
		$('#' + QUESTION_GROUP_CHVRON_PREFIX + newGroupCode).click(); // this displays the Tab panel.
		toggleGroupLabels(_activeGroupCode, false); 
		toggleGroupLabels(newGroupCode, true); 
		$('[id^="panel-' + _activeGroupCode + '"]').hide();
		$('#panel-' + newGroupCode).show();
		_activeGroupCode = newGroupCode;
		
		var lPanelButtons = newSubGroup.getSubGroupIds();
		
		// deactivate all questions and final page
		var sPanelButton;
		for (var iQuestion = 0; iQuestion < lPanelButtons.length; iQuestion++) {
			sPanelButton = lPanelButtons[iQuestion];
			$('#qbt-' + sPanelButton).removeClass('active'); 	// Left Pane
			$('#qdb-' + sPanelButton).hide();					// Middle Pane
		}
		*/

		var targetQuestionItem;
		if ((newSubGroup.questionItem) && (newSubGroup.questionItem.getFinalPageId() == newSubGroup.name))
			targetQuestionItem = null;
		else
			targetQuestionItem = newSubGroup.questionItem;
		newSubGroup.baseItem.onSkipToGroup(targetQuestionItem); // CJ-951
		/*
		// activate and show new subgroup
		var baseQuestion = newSubGroup.baseItem;
		$('#' + baseQuestion.getBasicBtnId()).addClass('active'); // Left Pane
		$('#' + baseQuestion.getBasicDivId()).show();			// Middle Pane
		// deactivate all questions and final page
		var oQuestion;
		for (var iQuestion = 0; iQuestion < baseQuestion.aInstanceArray.length; iQuestion++) {
			oQuestion = baseQuestion.aInstanceArray[iQuestion];
			$('#' + oQuestion.getDivId()).hide();
			if (iQuestion == 0)
				$('#' + oQuestion.getFinalPageId()).hide();
		}
		
		// ... and activate new question	
		$('#' + _activeQuestionPanelId).scrollTop();
		$('#' + _activeQuestionPanelId).show();	
		
		// set focus
		// CJ-362, check for final page id. If true, set to null.
		baseQuestion.focusedQuestion = targetQuestionItem;
//		if ((newSubGroup.questionItem) && (newSubGroup.questionItem.getFinalPageId() == newSubGroup.name))
//			baseQuestion.focusedQuestion = null;
//		else
//			baseQuestion.focusedQuestion = newSubGroup.questionItem;
		*/
	}
	
	this.onSkipToSubGroup = function onSkipToSubGroup(currentSubGroup, newSubGroup) {
		this.showSubGroup(currentSubGroup.baseItem, newSubGroup.baseItem);
	}
	
	this.showSubGroup = function showSubGroup(currentSubGroup, newSubGroup) {
		// deactivate and hide current subgroup
		$('#' + currentSubGroup.getBasicBtnId()).removeClass('active');	// Left Pane
		$('#' + currentSubGroup.getBasicDivId()).hide();				// Middle Pane
		
		// activate and show new subgroup
		$('#' + newSubGroup.getBasicBtnId()).addClass('active'); 		// Left Pane
		$('#' + newSubGroup.getBasicDivId()).show();					// Middle Pane
	}
	
	this.onSkipToQuestion = function onSkipToQuestion(newSubGroup) {
		this.showQuestion(newSubGroup);
			
		// save current question focus
		_activeQuestionPanelId = newSubGroup.name; 
		
		// CJ-1155, the focusQuestion for a transcript summary should be null.
		if (_activeQuestionPanelId.indexOf ('qf-') >= 0)
			newSubGroup.baseItem.focusedQuestion = null;
		else
			newSubGroup.baseItem.focusedQuestion = newSubGroup.questionItem;
	};
	
	this.showQuestion = function showQuestion(newSubGroup) {
		var currentPanel = newSubGroup.baseName;
		var newPanel = newSubGroup.name;
		var questionDivId = newSubGroup.baseItem.getBasicDivId();
		$('#' + currentPanel).hide();
		$('#' + newPanel).scrollTop();
		$('#' + newPanel).show();
		var questionDiv = $('#' + questionDivId);	
		adjustQuestionChoicesHeight(questionDiv);
	};	
	
	/* Bev removed CJ-611
	 * 
	// CJ-611, Disable the Previous Button for very first question in the interview.
	this.disableFirstPrevBttn = function disableFirstPrevBttn (newPanel) {
	
		if ((newPanel.questionItem.group == "A") 
			&& (newPanel.questionItem.baseGroupId == newPanel.questionItem.id)
			&& (newPanel.name =='qd-' + newPanel.questionItem.id)) 
				$('#qnbn-' + newPanel.questionItem.id).hide();
		else
			$('#qnbn-' + newPanel.questionItem.baseGroupId).show();
	}
	*/
}

//NavigationItem -------------------------------------------------------------------------

function NavigationItem() {

	// this.qGroup = pqGroup; // pointer to qGroup elements in the interview class. 
	this.qGroup = _interview.qGroup; // pointer to qGroup elements in the interview class.
	
	this.questionGroup = null;
	this.questionItem = null;
	this.name = null;

	this.baseItem = null;
	this.baseName = null;

	this.init = function init (panelId) {
		
		if (panelId == null) { 									// panel name not set
			this.getDefaultActivePanel();
			this.name = this.getActivePanelName ();
		}
		else {					
			var id = panelId.toString().replace(/[^0-9\s]/gi, '');
			this.getActivePanel(id);
			
			if (panelId.toString().indexOf("-") >= 0) 						// full panel name already present
				this.name = panelId;
			
			else 												// only panel id is available.
				this.name = this.getActivePanelName ();
		}
		
		this.baseName = this.getBasePanelName();
	}
	
	// when the document is first started,
	// then default the active tab to the first question
	this.getDefaultActivePanel = function getDefaultActivePanel () {
		var oGroup, oGroupQuestion;
		for (var iGroup = 0; iGroup < this.qGroup.elements.length; iGroup++) {
			oGroup = this.qGroup.elements[iGroup];
			
			for (var iItem = 0; iItem < oGroup.questions.length; iItem++) {
				// default is first question item.
				if (this.questionItem == null) {
					oGroupQuestion = oGroup.questions[iItem];
					if (oGroupQuestion.conditionItem.isApplicable()) { // CJ-951
						this.questionGroup = oGroup;
						this.questionItem = oGroup.questions[iItem];
						this.baseItem = oGroup.questions[0];
					}
				}
			}
			
			if (this.questionItem != null)
				break;
		}
	};
	
	this.getActivePanel = function getActivePanel (id) {
		id = id.toString().replace(/[^0-9\s]/gi, '');	// strip any decoration from id.
		var oGroup, oGroupItem;
		for (var iGroup = 0; iGroup < this.qGroup.elements.length; iGroup++) {
			oGroup = this.qGroup.elements[iGroup];
			
			for (var iItem = 0; iItem < oGroup.questions.length; iItem++) {
				oGroupItem = oGroup.questions[iItem];
			
				if (this.questionItem == null) { 
					this.questionItem = oGroupItem.findQuestionById(id);
					
					if (this.questionItem)
						this.baseItem = 
							oGroupItem.findQuestionById(this.questionItem.baseSubGroupId);
				}
				else { 
					break;
				}
			}

			if (this.questionItem != null) {
				this.questionGroup = oGroup;
				break;
			}
		}
	};
	
	this.getActivePanelName = function getActivePanelName () {
		var panelId = "";
		
		if (this.questionItem == null) 
			return "";
		
		// If the id matches the subgroupid, then must decide whether to show first page or transcript page.
		if (this.questionItem.baseSubGroupId == this.questionItem.id) {
			var focusQuestion = _interview.getFocusedQuestion (this.questionItem.baseSubGroupId);
			
			if (focusQuestion == null)
				panelId = 'qf-' + this.questionItem.baseSubGroupId;
			else
				panelId = 'qd-' + this.questionItem.id;
		}
		else
			panelId = 'qd-' + this.questionItem.id;
		
		return panelId;
	};
	
	this.getBasePanelName = function getBasePanelName () {
		if (this.baseItem == null) 
			return "";
		
		var panelId = "";
		
		// must decide whether to show first page or transcript page.
		var focusQuestion = _interview.getFocusedQuestion (this.baseItem.id);
		
		if (focusQuestion == null)
			panelId = 'qf-' + this.baseItem.id;
		else
			//panelId = 'qd-' + this.baseItem.id;
			panelId = 'qd-' + focusQuestion.id;
		
		return panelId;
	};
	
	// Retrieve a group of subGroupIds, per GroupId
	this.getSubGroupIds = function getSubGroupIds() {
		var GroupId = this.questionItem.baseGroupId;
		
	    var grpArray  = new Array();
	    var oGroup, oGroupItem, sActiveQuestionPanelId;
	    for (var iGroup = 0; iGroup < this.qGroup.elements.length; iGroup++) {
			oGroup = this.qGroup.elements[iGroup];
			
			for (var iItem = 0; iItem < oGroup.questions.length; iItem++) {
				oGroupItem = oGroup.questions[iItem];
				
				if (oGroupItem.baseGroupId == GroupId) {
					sActiveQuestionPanelId = oGroupItem.baseSubGroupId; 
					if ($.inArray(sActiveQuestionPanelId, grpArray) == -1)
						grpArray.push(sActiveQuestionPanelId);
				}
			}
	    }
	    
	    return grpArray;
	};

};

//============================================================================

function ValidateInput() {

	this.lastErrorMessage;

	this.check = function check(code, type, answer) {
		this.lastErrorMessage = "";
		bSuccess = true;

		if (type == '$')
			bSuccess = this.validateAmount(answer);
		if (!bSuccess) return bSuccess;
		
		if (type == 'N')
			bSuccess = this.validateNumber(answer);
		if (!bSuccess) return bSuccess;
		
		bSuccess = this.specialValidation(code, answer);
		if (!bSuccess) return bSuccess;
			
		return bSuccess;
	}

	// the HTML editor may modify the SYSTEM FIELD span tag to include different fonts, backgrounds, etc.
	this.contentRetreiveSystemFields = function contentRetreiveSystemFields(modifiedContent, sFillInName) {
	
		var sFillInSpan = '<span id="sf-' + sFillInName +  '" style="background-color: rgb(204, 204, 204);">[System Field]</span>';
		
		if (modifiedContent.indexOf (sFillInSpan) < 0) { 
			var iStartPos = modifiedContent.indexOf('<span id="sf-' + sFillInName +  '"'); 
			if (iStartPos > -1) { 
				var iEndPos = modifiedContent.indexOf('</span>', iStartPos) + 7;
				if (iEndPos > iStartPos)
					sFillInSpan = modifiedContent.substring(iStartPos, iEndPos);
			}
		}
		
		// CJ-657 - Validation Errors, per reformatted span in IE9
		// <span style=\"background-color: rgb(204, 204, 204);\" id=\"sf-textbox_52.212-2[0]\">[System Field]</span>
		if (modifiedContent.indexOf (sFillInSpan) < 0) { 
			var sId = 'id="sf-' + sFillInName +  '"';
			var iStartPos = modifiedContent.indexOf(sId); 
			if (iStartPos > -1) {
				var str1 = modifiedContent.substring(1, iStartPos + sId.length);
				var pos1 = str1.lastIndexOf("<span ");
				var sSpan = str1.substring(pos1);
				iStartPos = modifiedContent.indexOf(sSpan);
			}
			if (iStartPos > -1) { 
				var iEndPos = modifiedContent.indexOf('</span>', iStartPos) + 7;
				if (iEndPos > iStartPos)
					sFillInSpan = modifiedContent.substring(iStartPos, iEndPos);
			}
		}
		
		return sFillInSpan;
	}
	
	// CJ-914, Check if all fillins have been removed.
	this.areFillinsRemoved = function areFillinsRemoved (answer, otherFillIns) {

		var iMissingFields = 0;
		for (var iItem = 0; iItem < otherFillIns.length; iItem++) {
			var sFillInName = otherFillIns[iItem];
			
			var sFillInSpan = this.contentRetreiveSystemFields (answer, sFillInName);
			if ((answer.indexOf(sFillInSpan) < 0) 
			||  (sFillInSpan.indexOf('[System Field]') < 0)) {
				//(answer.indexOf('<span id="sf-' + sFillInName +  '"') < 0) 
				
				iMissingFields++;
			}
		} 
		
		if (iMissingFields == otherFillIns.length)	
			return true;
		
		return false;
	}
	
	this.validateFullEdit = function validateFullEdit (answer, otherFillIns) {
		var bValid = true;
		var iMissingFields = 0;
		for (var iItem = 0; iItem < otherFillIns.length; iItem++) {
			var sFillInName = otherFillIns[iItem];
			
			var sFillInSpan = this.contentRetreiveSystemFields (answer, sFillInName);
			if ((answer.indexOf(sFillInSpan) < 0) 
			||  (sFillInSpan.indexOf('[System Field]') < 0)) {
				//(answer.indexOf('<span id="sf-' + sFillInName +  '"') < 0) 
				
				bValid = false;
				iMissingFields++;
			}
		} 
		
		// CJ-914. It's valid to replace all system fields.
		if (iMissingFields == otherFillIns.length)
			bValid = true;
		
		if (!bValid) 
			this.lastErrorMessage = "System field has been removed or edited. Changes cannot be saved.";
		
		return bValid;
	}
	
	this.validateAmount = function validateAmount(answer) {

		var bValid = true;

		// no answer? return
		if (answer == null)
			return bValid;

		// valid characters?
		if (answer.match(/^[0-9,.]*$/)) {

			// valid currency format?
			var tmp = answer.replace (/,/g, "");
			if (answer.indexOf ('.') > -1)
				tmp = this.formatMoney (tmp, 2);
			else
				tmp = this.formatMoney (tmp, 0);
			if (tmp != answer)
				bValid = false;
		}
		else
			bValid = false;

		if (!bValid)
			this.lastErrorMessage = "Dollar amount entered is not in a correct format.";

		return bValid;
	}

	// CJ-408 - fillin item - validate currency
	this.formatMoney = function formatMoney(n, c){

		var c = isNaN(c = Math.abs(c)) ? 2 : c,
		    d = ".",
		    t = ",",
		    s = n < 0 ? "-" : "",
		    i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "",
		    j = (j = i.length) > 3 ? j % 3 : 0;
		   return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
	};
	
	this.validateNumber = function validateNumber(answer) {

		var bValid = true;

		// no answer? return
		if (answer == null)
			return bValid;

		if (answer.length == 0) {
			this.lastErrorMessage = "A blank value is not a valid entry.";
			return false;
		}
		
		// valid characters?
		if (/^[0-9]+$/.test(answer)) {
			bValid = true;
		}
		else
			bValid = false;

		if (!bValid)
			this.lastErrorMessage = "At least one answer is not a valid number.";

		return bValid;
	}
	
	this.specialValidation = function specialValidation(code, answer) {

		var bValid = true;

		// no answer? return
		if (answer == null)
			return bValid;

		if (code.indexOf ('next_x_days_52.232-20') >= 0)
		{
			var iAnswer = parseInt(answer);
			if ((iAnswer < 30) || (iAnswer > 90))
				bValid = false;
		}
		else if (code.indexOf ('cost_percent_52.232-20') >= 0)
		{
			var iAnswer = parseInt(answer);
			if ((iAnswer < 75) || (iAnswer > 85))
				bValid = false;
		}

		if (!bValid)
			this.lastErrorMessage = "At least one answer is not valid.";

		return bValid;
	}
}

//============================================================================
//CJ-408
function EditCondition() {
	
	this.questionCode;
	this.questionChoice;
	
	this.initialize = function initialize(sRemarks) {
		if (sRemarks.toUpperCase().indexOf ('COST REIMBURSEMENT') > -1){
			this.questionCode = '[CONTRACT TYPE]';
			this.questionChoice = 'COST REIMBURSEMENT';
		}
		else if (sRemarks.toUpperCase().indexOf ('ONLY IF USING NEGOTIATION PROCEDURE') > -1){
			this.questionCode = '[PROCEDURES]';
			this.questionChoice = 'NEGOTIATION (FAR PART 15)';
		}
		else if (sRemarks.toUpperCase().indexOf ('COMMERCIAL ITEMS ONLY') > -1){
			this.questionCode = '[COMMERCIAL ITEMS]';
			this.questionChoice = 'THE SUPPLIES ARE COMMERCIAL ITEMS';
		}
		else if (sRemarks.toUpperCase().indexOf ('WHEN THIS CLAUSE IS INCLUDED IN INVITATIONS FOR BIDS') > -1){
			this.questionCode = '[SOLICITATION TYPE]';
			this.questionChoice = 'INVITATION FOR BIDS';
		}
		
	}
	
	this.isValid = function isValid() {
		if ((this.questionCode == null) || (this.questionChoice == null))
			return false;
		else
			return true;
	}

	this.evaluate = function evaluate(sRemarks) {
		
		var bValid = false;
		
		var oQuestion;
		var oQuestionChoice;
		
		// look for a match...
		for (var iEle = 0; iEle < _interview.questions.elements.length; iEle++) {
			oQuestion = _interview.questions.elements[iEle];
			if (oQuestion.name == this.questionCode) {		
				for (var iEle2 = 0; iEle2 < oQuestion.choices.elements.length; iEle2++) {
					oQuestionChoice = oQuestion.choices.elements[iEle2];
					if (oQuestionChoice.choice == this.questionChoice){
						break;
					}
				}
				break;
			}
		}
		
		// did we find a match?
		if ((oQuestion && oQuestion.name == this.questionCode)
		&& (oQuestionChoice && (oQuestionChoice.choice == this.questionChoice) 
		// and is is selected?
		&& oQuestionChoice.answer)) {
			bValid = true;
		}
		
		return bValid;
	}
}

//============================================================================
// CJ-802
// Class to support specific question / choice / answer evaluation.
// Often different flags or attributes depend on a specifc answer choice.

function AttributeConditions(sName, sCondition, sStatus) {
	
	this.conditionList = new Array();
	
	this.name = sName;
	this.condition = sCondition;
	this.status = sStatus;
	
	this.isExpressionValid = true;
	
	this.resolve = function resolve() {
		
		if (this.condition == null) { 
			this.isExpressionValid = false;
			return;
		}
		
		var aConditions = this.condition.split( "\n" );
		
		for (var i = 0; i<aConditions.length; i++) { 
						
			var oAttributeChoice = new AttributeChoice();		
			if (oAttributeChoice.initialize(aConditions[i])) {
				this.conditionList.push(oAttributeChoice);
				//logOnConsole('AttributeCondition added for ' + this.name + 
				//		"\n[Question Code] = " + oAttributeChoice.questionCode + 
				//		"\n[Question Choice] = " + oAttributeChoice.questionChoice );
			}
			else { 
				this.isExpressionValid = false;
				break;
			}
		}
	}
	
	this.isValid = function isValid() {
		return this.isExpressionValid;
	}

	// check if all conditions are satisfied.
	this.isApplicable = function isApplicable() {
		
		for (var i = 0; i<this.conditionList.length; i++) { 
			var oCondition = this.conditionList[i];
			if (!oCondition.isApplicable())
				return false;
		}
		
		return true;
	}
	
}

//============================================================================
//CJ-802
function AttributeChoice() {

	this.condition;
	
	this.questionCode;
	this.questionChoice;
	this.not = false;
	
	this.isValid;
	
	this.initialize = function initialize(sCondition) {
		
		this.isValid = true;
		this.condition = sCondition;
		
		this.parse();
		if (!this.isValid) {
			logOnConsole('AttributeCondition for ' + this.name + ' encountered error parsing: ' +  this.condition);
			return false;
		}
		
		this.validate();
		if (!this.isValid) {
			logOnConsole('AttributeCondition for ' + this.name + ' encountered error validating: ' +  this.condition);
			return false;
		}
		
		return true;
	}
	
	this.parse = function parse() {
		
		if (this.condition.indexOf (':') < 0)
		{ 
			this.isValid = false;
			return;
		}
		
		var sCondition = this.condition;
		
		// check for NOT operand
		if (sCondition.indexOf (': NOT') > 0){
			this.not = true;
			sCondition = sCondition.replace (": NOT", ":");
		}
			
		sCondition = sCondition.replace ("IS:", ":");
		var aDictionary = sCondition.split( ":" );
		
		this.questionCode = '[' + aDictionary[0].trim() + ']';
		this.questionChoice = aDictionary[1].trim();
		
	}
	
	this.validate = function validate() {
		
		var bValid = false;
		
		var oQuestion;
		var oQuestionChoice;
		
		// look for a match...
		for (var iEle = 0; iEle < _interview.questions.elements.length; iEle++) {
			oQuestion = _interview.questions.elements[iEle];
			if (oQuestion.name == this.questionCode) {		
				for (var iEle2 = 0; iEle2 < oQuestion.choices.elements.length; iEle2++) {
					oQuestionChoice = oQuestion.choices.elements[iEle2];
					if (oQuestionChoice.choice == this.questionChoice){
						break;
					}
				}
				break;
			}
		}
		
		// did we find a match?
		if ((oQuestion && oQuestion.name == this.questionCode)
		&& (oQuestionChoice && (oQuestionChoice.choice == this.questionChoice))) {
			bValid = true;
		}
		
		this.isValid = bValid;
	}

	this.isApplicable = function isApplicable() {
		
		var bValid = false;
		
		var oQuestion;
		var oQuestionChoice;
		
		// look for a match...
		for (var iEle = 0; iEle < _interview.questions.elements.length; iEle++) {
			oQuestion = _interview.questions.elements[iEle];
			if (oQuestion.name == this.questionCode) {		
				for (var iEle2 = 0; iEle2 < oQuestion.choices.elements.length; iEle2++) {
					oQuestionChoice = oQuestion.choices.elements[iEle2];
					if (oQuestionChoice.choice == this.questionChoice){
						break;
					}
				}
				break;
			}
		}
		
		// did we find a match?
		if ((oQuestion && oQuestion.name == this.questionCode)
		&& (oQuestionChoice && (oQuestionChoice.choice == this.questionChoice))) {  
			// and is it selected?
			if (this.not) {   	// NOT added - PROCEDURES IS: NOT COMMERCIAL PROCEDURES
				if (!oQuestionChoice.answer)
					bValid = true;
			}
			else { 				// INAPPROPRIATE CONTINGENCY IS: YES
				if (oQuestionChoice.answer)		
					bValid = true;
			}
		}
		
		return bValid;
	}
}