var _oUserInfo = new UserInfo();
var _interview = new Interview();
var _docId = getUrlParameter("doc_id");
var _token = getUrlParameter("token");
var _isReset = getUrlParameter("resetmode");
var _autoSavingInterval = null;
var _titleBannerHeight = 100;

function disableAllOptionsForExpiredSession() { // CJ-634 added
	if (_autoSavingInterval != null) {
		window.clearTimeout(_autoSavingInterval);
		_autoSavingInterval = null;
	}
	$("#btnSaveContinue").prop("disabled", true);
	$("#btnSaveExit").prop("disabled", true);
	$("#btnCompleteExit").prop("disabled", true);
	$("#btnTranscript").prop("disabled", true);
	$("#btnFillins").prop("disabled", true);
	$("#btnRunRules").prop("disabled", true);
	$("#btnOrdersSaveContinue").prop("disabled", true);
	$("#btnOrdersCompleteExit").prop("disabled", true);
	_interview.disableAllEntriesForTimeout();
	$('#inputOrdersSearchClause').prop("disabled", true);
	$('#divOrderSearchResult').html('');
	
	$('#spanAutoSaveDisplay').html('&nbsp;&nbsp; This session has timed-out.');
	$('#spanAutoSaveDisplay').show();
}

$('#AcquisitionProfile').attr('href', 'interview.jsp?doc_id=' + _docId);

changeOtherChvron = function(id, expanded) {
	var oItem = $('#' + id).parents('.collapsible-panel').find('i');
	if (expanded)
		oItem.removeClass('glyphicon-chevron-up').addClass('glyphicon-chevron-down');
	else
		oItem.removeClass('glyphicon-chevron-down').addClass('glyphicon-chevron-up');
}

onCollapseShown = function(e) {
	changeOtherChvron(e.currentTarget.id, true);
}

onCollapseHidden = function(e) {
	changeOtherChvron(e.currentTarget.id, false);
}

onDocumentReset = function(pInterview) {

	_isReset = 1;
	pInterview.pendingSave = false;
	
	onLoaded(pInterview, true, null);

	// The reset dialog combines the elements for all reset activities,
	// Warning, Processing, Success.
	
	// Show the elements for Reset - Success!	
	$('#resetProcessHeader').hide();
	$('#resetProcessMessage').hide();
	$('#resetProcessSpinner').hide();
	$('#resetHeader').hide();
	
	$('#resetModalLabel').html('Success!');
	$('#resetBody').html('All answers have been reset.');

	$('#resetFooter').show();
	$('#bnResetYes').hide();
	$('#bnResetNo').hide();
	$('#bnResetClose').show();
	$('#resetModalLabel').show();
	$('#resetBody').show();
	
}
		
onDocumentSaved = function(pInterview, message, pMode, jsonData) { // added jsonData for CJ-573
	if (message == 'no session') { // CJ-573
		displaySessonEnded(true);
		return;
	}
	var oDoc = pInterview.doc;
	if ((jsonData) 		// CJ-573, 
	&&  (pMode != 3)) 	// CJ-713, Don't reset the session timeout for auto save
		_oUserInfo.loadSessionJson(jsonData, true); 
	$('#spanUpdatedAt').html(longToDateTimeStr(oDoc.updatedAt, _oUserInfo.user_timezone_offset));
	if (pMode == 2) {
		alert(message + '. Please close this window.');
	}
	else {
		if (0 == pMode)
			alert(message);
		if (3 == pMode) {
			$('#spanAutoSaveDisplay').html('&nbsp;&nbsp; Document Auto-Saved'); 
			setTimeout(function() {   
				  $("#spanAutoSaveDisplay").fadeOut();
				}, 5000);
			document.getElementById("spanAutoSaveDisplay").style.display='';
		}
		_interview.pendingSave = false;
//		window.clearInterval(_autoSavingInterval);	moved to _interview.onSaveBtnClick()
//		_autoSavingInterval = window.setInterval(function() {_interview.onSaveBtnClick(3)}, 900000);    // 15 min	
	}	
}

function onFinalCheckCancelClick() {
	clearFinalCheck();
	$('#completeModal').modal('hide');
}

function onFinalCheckSubmitClick() {
	clearFinalCheck();
	$('#completeModal').modal('hide');
	_interview.saveDocument(2);
	//alert('onFinalCheckSubmitClick()');
}

function onFillinCancelClick() {
	onFillinModalClosed();
}

reviewCrossitemsForComplete = function(pInterview, pProgress, oFinalCheckSet) {
	var html;
	if ((pProgress.isQuestionCompleted() == false)) { // CJ-579 // CJ-1005 (_isManualOrders == false) && 
		html = pInterview.qGroup.getHtmlIncompletedQuestions();
		$('#completeTitle').html('You have unanswered questions. Please review and answer incomplete questions below before finalizing this interview.');
		$('#completeModelComplete').hide();
		//alert(pProgress.getQuestionInCompleteMessage());
		//return false;
	} else {
		html = oFinalCheckSet.genHtml();
		$('#completeTitle').html('Review clause changes (due to Crossover rules) and complete the document interview');
		$('#completeModelComplete').show();
	}

	$('#completeModalBody').html(html);
	$('#completeModal').modal();
	//prepareCollapsibleGlyphicon();
	return true;
};

onClickQuestionModal = function(finalPageId, skipToQuestionId) {
	clearFinalCheck();
	$('#completeModal').modal('hide');
	_interview.skipToQuestionPanel(skipToQuestionId);
}

validateBeforeSave = function(pMode, pInterview, pProgress) {
	if (pMode == 2) {
		var msg = '';
		/* CJ-1005
		if (_isManualOrders == true) {
			if (pInterview.prescriptionChoices.clauses.length <= 0) {
				alert('No clauses selected')
				return false;
			}
		} else {
		*/
		if (pProgress.isQuestionCompleted() == false) {
			html = pInterview.qGroup.getHtmlIncompletedQuestions();
			$('#completeTitle').html('You have unanswered questions. Please review and answer incomplete questions below before finalizing this interview.');
			$('#completeModelComplete').hide();
			$('#completeModalBody').html(html);
			$('#completeModal').modal();
			//alert(pProgress.getQuestionInCompleteMessage());
			return false;
		}
		//}
		
		// CJ-1171, Don't show message for no counts.
		if (pProgress.isClauseCompleted() == false) {
			msg = pProgress.getClauseInCompleteMessage() + '\n';
			msg += 'Are you sure you want to complete this document without updating fill-ins?';
			
			var r = confirm(msg);
			if (r != true) {
				return false;
			}
		}
		// CJ-1171, Don't show Session Modified message.
		window.onbeforeunload = null;
	}
	return true;
}

applyTreeInterface = function() {
	$(function () {
	    $('.tree li:has(ul)').addClass('parent_li').find(' > span').attr('title', 'Collapse this branch');
	    $('.tree li.parent_li > span').on('click', function (e) {
	        var children = $(this).parent('li.parent_li').find(' > ul > li');
	        if (children.is(":visible")) {
	            children.hide('fast');
	            $(this).attr('title', 'Expand this branch').find(' > i').addClass('fa-plus-square-o').removeClass('fa-minus-square-o');
	        } else {
	            children.show('fast');
	            $(this).attr('title', 'Collapse this branch').find(' > i').addClass('fa-minus-square-o').removeClass('fa-plus-square-o');
	        }
	        e.stopPropagation();
	    });
	});
}

function expandHeader() {
	
   var e = document.getElementById('expandedHeader');
   if (e.style.display == 'none') { // expand header, and switch indicator to collapse
	   e.style.display = 'block';
	   $('#expand_link').html('<img src="../assets/images/menu_collapse.png" alt="Menu collapse."  />');
	   _titleBannerHeight = 100;
   }
   else {							// collapse header, and switch indicator to expand
	   e.style.display = 'none'; 
	   $('#expand_link').html('<img src="../assets/images/menu_expand.png" alt="Menu expand." />');
	   _titleBannerHeight = 15;
   }
   adjustHeights();
   //window.dispatchEvent(new Event('resize'));
}

function expandQGroup() {
	
	   var expandQGroup = document.getElementById('tdExpandedQGroups');
	   var collapsedQGroup = document.getElementById('tdCollapsedQGroups');

	   if (expandQGroup.style.display == 'none') { // expand header, and switch indicator to collapse
		   expandQGroup.style.display = 'block';
		   collapsedQGroup.style.display = 'none';
	   }
	   else {							// collapse header, and switch indicator to expand
		   expandQGroup.style.display = 'none'; 
		   collapsedQGroup.style.display = 'block';
	   }
	   adjustWidths();
	   adjustHeights();
	}

function expandCGroup() {
	
	   var expandTableCol = document.getElementById('tdExpandedClauses');   
	   var collapsedTableCol = document.getElementById('tdCollapsedClauses');

	   if (expandTableCol.style.display == 'none') { // expand header, and switch indicator to collapse
		   expandTableCol.style.display = 'block';
		   collapsedTableCol.style.display = 'none';
		   
		   $('#btnFillins_Hrz').hide();
		   
	   }
	   else {							// collapse header, and switch indicator to expand
		   expandTableCol.style.display = 'none'; 
		   collapsedTableCol.style.display = 'block';
		   
		   $('#btnFillins_Hrz').show();
	   }
	   adjustWidths();
	   adjustHeights();
	}

function adjustWidths() {
	
	   var questionPanel = document.getElementById('tdExpandedQGroups');
	   var contentPanel = document.getElementById('tdContent');
	   var contentDiv = document.getElementById('divContent');
	   var clausePanel = document.getElementById('tdExpandedClauses');
	   
	   var iWidth = $(window).innerWidth ();
	   
	   // all three panels displayed
	   if ((questionPanel.style.display == 'block')
			   && (clausePanel.style.display == 'block')) {
		   contentPanel.style.width = '45%';
		   contentPanel.style.minWidth = '45%';
	   }
	   // only middle pane displayed.
	   if ((questionPanel.style.display == 'none')
		   && (clausePanel.style.display == 'none')) {
		   contentPanel.style.width = '100%';
		   contentPanel.style.minWidth = '450px';
	   }
	   else if (questionPanel.style.display == 'none')  { 
		   contentPanel.style.width = '70%';
		   contentPanel.style.minWidth = '70%';
	   }
	   else if (clausePanel.style.display == 'none')  { 
		   contentPanel.style.width = '70%';
		   contentPanel.style.minWidth = '70%';	   
	   }
	   
}

function onLoaded(pInterview, isSuccess, pUpgradeClsVersionData) {
	if (isSuccess == false) {
		alert('Requested document was not found');
		goDashboard();
		return;
	}
	if (!pInterview.ready) {
		alert('Not ready');
		return;
	}
	var oDoc = pInterview.doc;
	if (oDoc.id) {
		//$('#acquisition_title_collapsed').html(oDoc.title);
		$('#acquisition_title').html(oDoc.title);
		$('#document_number').html(oDoc.number);
		$('#spanDocType').html(getDocumentTypeName(oDoc.type) + (oDoc.isLinkedAward ? " (Linked)" : ""));
		//$('#spanVersionName').html(oDoc.versionName);
		$('#spanUpdatedAt').html(longToDateTimeStr(oDoc.updatedAt, _oUserInfo.user_timezone_offset));
		
		// CJ-477, Add FAC and DAC number to banner.
		var spanMsg = 'A complete and accurate set of recommended clauses will not be available until all questions in all applicable sections have been answered and the session has been validated.';
		if ((pInterview.fac_number != null) && (pInterview.dac_number != null) && (pInterview.dfars_number != null))
			spanMsg = 'CLS has been updated to include changes through FAC ' + pInterview.fac_number + (pInterview.fac_date ? ' (' + pInterview.fac_date + ')' : '') +
			', DAC ' + pInterview.dac_number + (pInterview.dac_date ? ' (' + pInterview.dac_date + ')' : '') +
			', and DFARS ' + pInterview.dfars_number + (pInterview.dfars_date ? ' (' + pInterview.dfars_date + ')' : '') +
			'. ' + spanMsg;
		$('#spanFixedMessage').html(spanMsg);
	}
	$('#pleaseWaitDialog').modal('hide');
	if (pUpgradeClsVersionData) {
		$("#bnVerChangesUpdate").click( function() {
			upgradeDocVersion(_docId, oDoc.type, 'verChangesModal', onClsVersionUpgraded);
		});
				 
		showVersionChangesDialog(pUpgradeClsVersionData, _docId, null, null, null, null, null, false, oDoc.type);
		return;
	}
	// CJ-155
	var iHeight = getWindowHeight() ; // CJ-1198: changed from - 45; 60; smaller Save buttons
	/* CJ-1198: removed
	if ($(window).height() < 400) {
		iHeight = 200;
	}
	*/
	
	pInterview.constructScreen('divGroupButtons', 'divContent', 'ulTranscript', 'divContainerPrescription', 'divContentSection', 
			iHeight-20); // CJ-498. Added -20 for the initial button display (Save/Finalize/etc.).
	
	pInterview.autoSavingInterval = window.setTimeout(function() {pInterview.autoSaveTimeout()}, 600000);    // 10 min
}

onClsVersionUpgraded = function() {
	showLoadingProgress(0);
	$('#pleaseWaitDialog').modal('show');
	_interview.retrieve(_docId, onLoaded, _oUserInfo);
}

onLoginResponse = function(success, userInfo) {
	if (success) {
		if (userInfo.canAccessAdminOptions() == true) {
			$('#divContainer').removeClass('containerMarginTopNormal').addClass('containerMarginTopAdmin');
		}
		if (userInfo.isToken == true) {
			_docId = userInfo.documentId;
			$('#btnSaveExit').hide();
			$('#btnCompleteExit').html('Finalize'); // CJ-612 changed from Complete
		}
		if (_interview == null)	{ 
			_interview = new Interview();
		}
		_interview.retrieve(_docId, onLoaded, userInfo);
	} else
		goHome();
}

$('#pleaseWaitDialog').modal();
_oUserInfo.getLogon(true, onLoginResponse, null, _token, 'divSessionTimeout'); // CJ-634 added 'divSessionTimeout' parameter 

getWindowHeight = function() {
	
	var clsBannerHeight = 90;
	var admBannerHeight = 35;
	var headerHeight = _titleBannerHeight //85;
	var footerHeight = 60; 
	
	var iHeight =  $(window).innerHeight ();
	
	if ((_token != null) && (_token != ""))		// valid token? No admin banner.
		iHeight = iHeight - clsBannerHeight;
	else //if (_oUserInfo.canAccessAdminOptions() == true) 
		iHeight = iHeight - clsBannerHeight - admBannerHeight;
	//else
	//	iHeight = iHeight - clsBannerHeight;
	iHeight -= (headerHeight + footerHeight);
	if (!isChrome()) { // CJ-760
		iHeight -= 20;
	}
	
	// iHeight = iHeight - headerHeight;
	// iHeight = iHeight - footerHeight;
	
	var bodyScroll = false; // CJ-1198
	if ($(window).height() < 600) { // CJ-1198: changed from 400
		bodyScroll = true;
		//document.body.style['overflow'] = 'auto';
		iHeight = 450; // CJ-1198: changed from 250
		$('#divContentFooter').height(60);
	} else {
		$('#divContentFooter').height(30);
	}
	if ($(window).width() < 1024) {
		bodyScroll = true;
		if (!isChrome()) { // CJ-760
			iHeight -= 10;
			// logOnConsole('!isChrome(): ' + iHeight);
		}
	}
	if (bodyScroll) {
		document.body.style['overflow'] = 'auto'; 
	}
	else
		document.body.style['overflow'] = 'hidden';
	

	return iHeight;
}

function adjustHeights() { // CJ-1198
	//logOnConsole('#divContentFooter height: ' + $('#divContentFooter').height());
	var iHeight = getWindowHeight();
	/*
	if ($(window).height() < 600) { // CJ-1198: changed from 400
		document.body.style['overflow'] = 'auto';
		iHeight = 450; // CJ-1198: changed from 250 
	}
	*/
	$('#QuestionGroupPanel, #divContentSection, #divContent').css('max-height', iHeight);
	$('#SearchClausesPanel').css('max-height', iHeight);
	_interview.adjustHeight(iHeight);
}

// CJ-155, resize the panels programmatically to fit the current screen size
$(function(){
	adjustHeights();
	/* CJ-1198: replaced followings with above
	var iHeight = getWindowHeight();
	
	if ($(window).height() < 400) {
		document.body.style['overflow'] = 'auto';
		iHeight = 250;
	}

	$('#QuestionGroupPanel, #divContentSection, #divContent').css('max-height', iHeight);
	$('#SearchClausesPanel').css('max-height', iHeight);
	*/
});

$(window).resize(function() { // CJ-1198
	adjustHeights();
});

var confirmOnInterviewExit = function (e) 
{
    // If we haven't been passed the event get the window.event
    e = e || window.event;

    var message = 'This session has been modified. If you exit without saving, all changes will be lost.';

    // For IE6-8 and Firefox prior to version 4
    if (e) 
    {
        e.returnValue = message;
    }

    // For Chrome, Safari, IE8+ and Opera 12+
    return message;
};
window.onbeforeunload = confirmOnInterviewExit;

// =================================================================
var _sortedClauseSet;
var _sPreviousOrderSearch = '';

/*
if (_token)
	$('#divOrdersOption').show();
else
	$('#divOrdersOption').hide();
*/

/* CJ-1005
function onOrderSearchChange(poInput) {
	var sValue = poInput.value;
	var oDivOrderSearchResult = $('#divOrderSearchResult');
	if ((sValue.length > 0) && (sValue != ' ')) {
		if (_sPreviousOrderSearch != sValue) {
			var oSelectedClauses = _interview.prescriptionChoices.clauses;
			var sHtml = '';
			var sSearch = sValue.replace(/[.*+?^${}()|[\]\\]/g, "\\$&");
			var oSubstringRegex = new RegExp(sSearch, 'i'); // regex used to determine if a string contains the substring
			// iterate through the pool of ClauseItem set and for any string that
			// contains the substring `q`, add it to the `matches` array
			$.each(_sortedClauseSet, function(i, oClauseItem) {
				var sItem = oClauseItem.name + '  ' + oClauseItem.title;
				if (oSubstringRegex.test(sItem)) {
					var bSelected = oSelectedClauses.indexOf(oClauseItem) >= 0;
					var sItem = oClauseItem.genOrderSelDivHtml(bSelected, 1);
					sHtml += sItem;
				}
			});
			_sPreviousOrderSearch = sValue;
			$(oDivOrderSearchResult).html(sHtml);
		}
	} else {
		$(oDivOrderSearchResult).html('');
	}
}

function onRunOrders() {
	_sortedClauseSet = _interview.genOrderSelOptions('divOrderFar', 'divOrderDfar');
	_isManualOrders = true;
	
	$('.ordersRemoveClause').removeClass('nodisplay'); // toggleClass
	
	$('#tdExpandedQGroups').hide();
	$('#tdContent').hide();
	$('#spanFixedMessage').html('CLS does not validate custom clause selection');
	$('#barProgress').hide();
	//$('#btnRunOrders').hide();
	$('#btnFillins').hide();	// CJ-157
	//$('#labelOrders').show(); // CJ-1005
	//$('#tdOrders').show(); // CJ-1005
	//$('#divOrdersClauseOptions').show(); // CJ-1005
	$('#btnRunRules').hide();
	$(document).ready(function(){
		// $('.combobox').combobox();
	});
}
*/

$('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
	if (e.target.innerText == "Manual Search")
		$('#inputOrdersSearchClause').focus();
	// e.target // // newly activated tab
	// e.relatedTarget // previous active tab
})

/* CJ-1005
function divOrdersScrolToTop() {
	var iTop = $("#ulOrdersTabs").offset().top; 
	//alert(iTop); // 280
	$('#SearchClausesPanel').animate({
		scrollTop: 0 // iTop
	}, 500);
}
*/