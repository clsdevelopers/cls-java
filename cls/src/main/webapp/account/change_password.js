var _oUserInfo = new UserInfo();

onPageBtnClick = function(page) {
	location = page;
}

onLoginResponse = function(success, userInfo) {
	if (success) {
		$("#navSettings").addClass("active");
		displaySessionMessage(userInfo.sessionMessage);
	} else
		goHome();
}

_oUserInfo.getLogon(true, onLoginResponse);


$("#btnChgPassword").click(function() {
	
	var oCurrentPassword = $('#current_password');
	var sCurrentPassword = oCurrentPassword.val();
	
	var oNewPassword = $('#new_password');
	var sNewPassword = oNewPassword.val();
	error = detectPasswordError(sNewPassword);
	if (error) {
		oNewPassword.focus();
		displaySessionMessage(error);
		return;
	}
	var oNewPasswordAgain = $('#new_password_again');
	var sNewPasswordAgain = oNewPasswordAgain.val();
	if (sNewPasswordAgain != sNewPassword) {
		oNewPasswordAgain.focus();
		displaySessionMessage("'New password again' value is not matching");
		return;
	}

	changePassword (sCurrentPassword, sNewPassword);
});

//CJ-700, validate current password.
function changePassword(cpwd, npwd) {
	
	var oData = {current_password: cpwd, new_password: npwd};
	$.ajax({      
		url: getAppPath() + 'page?do=session-change_password_2',
		type: 'POST',
		data: oData,
		cache: false,
		dataType: 'json',
		xhrFields: { withCredentials: true },
		success: function(data) 
		{
			if (data && ('success' == data.status)) {
				location.href = "settings.html";
			} else {
				if (data && data.message)
					displaySessionMessage(data.message);
				else
					displaySessionMessage("New password failed.");
			}
		},
		error: function(object, text, error) 
		{
			displaySessionMessage("New password failed.");
		}
	});
	
	
}