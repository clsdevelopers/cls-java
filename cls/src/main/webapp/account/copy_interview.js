var _oUserInfo = new UserInfo();
var _doc_id = getUrlParameter("doc_id");
var _document_number = getUrlParameter("document_number");
var _acquisition_title = decodeURIComponent(getUrlParameter("acquisition_title"));
var _docClsVersionId = null;
var _activeClsVersionId = null;
//S. 7-14-2016 document_type, CJ-1356
var _docType = getUrlParameter("document_type");
//
var _jsonData = null;

$('#solocitation_number').val(_document_number);
$('#solocitation_title').val(_acquisition_title);


//Sava 7/18/2016 CJ-1359 Changes

if ('S' == _docType) {
	_docType = "S";
	$("#legendTitle").html('New Solicitation Template');
	$("h1").html('New Solicitation Template');
	$('#award1').html('Source Solicitation Title');
	$('#award2').html('Source Solicitation Document Number');
	document.getElementById('document_number').placeholder ="Solicitation document number";
	document.getElementById("acquisition_title").placeholder = "Acquisition title of the solicitation template";
	$('#btnCreate').val('Copy Solicitation Document');
} 
else if ('A' == _docType){
	$("#legendTitle").html('New Award Template');
	$("h1").html('New Award Template');
	$('#award1').html('Source Award Title');
	$('#award2').html('Source Award Document Number');
	document.getElementById('document_number').placeholder ="Award document number";
	document.getElementById("acquisition_title").placeholder = "Acquisition title of the award template";
	$('#btnCreate').val('Copy Award Document');
}


prepareInterviewTemplate = function() {
	
	var oData = { id: _doc_id };
	$.ajax({      
		url: getAppPath() + 'page?do=doc-getDocToCopy',
		type: 'GET',
		data: oData,
		cache: false,
		dataType: 'json',
		success: function(data) 
		{
			if (data && ('success' == data.status)) {
				displayDocumentInfo(data.data);
			} else {
				alert(data.message ? data.message : "Unable to retrieve document.");
				location = 'dashboard.jsp';
			}
		},
		error: function(object, text, error) 
		{
			alert(error);
		}
	});
}

displayDocumentInfo = function(json) {
	_jsonData = json;
	var oDocument = json.Documents;
	var oClsVersion = json.Clause_Versions;
	
	$('#solocitation_number').val(oDocument.document_number);
	$('#solocitation_title').val(oDocument.acquisition_title);
	
	/*
	_docClsVersionId = oDocument.clause_version_id;
	var iDocClsVersionName = oDocument.clause_version;
	var sHtml = '<input type="radio" name="cxClsVersion" id="cxClsVersionCurrent" value="' + _docClsVersionId + '" checked> '
			+ iDocClsVersionName ;
	$('#labelClsVerCurrent').html(sHtml);
	
	_activeClsVersionId = oClsVersion.id;
	var iActiveClsVersionName = oClsVersion.clause_version_name;
	sHtml = '<input type="radio" name="cxClsVersion" id="cxClsVersionNew" value="' + _activeClsVersionId + '"> Upgrade to '
			+ iActiveClsVersionName
			+ '<br /><button type="button" class="btn btn-default" onclick="onShowChange()" style="margin-left:30px;">Show Changes</button>';
	$('#labelClsVerNew').html(sHtml);
	
	if (_docClsVersionId == _activeClsVersionId) {
		$('#devClsVerNew').hide();
	} else {
		$('#devClsVerNew').show();
	}
	*/
}

onShowChange = function() {
	showVersionChangesDialog(_jsonData, null, null, null, null, null, null, true);
}

closeVerChangesModal = function() {
	$('#verChangesModal').modal('hide');
}

onSubmit = function() {
	var oAcquisitionTitle = $('#acquisition_title');
	var oData = {	id: _doc_id,
					acquisition_title: $('#acquisition_title').val(),
					document_number: $('#document_number').val()
				};
	$.ajax({      
		url: getAppPath() + 'page?do=doc-createcopy',
		type: 'POST',
		data: oData,
		cache: false,
		dataType: 'json',
		success: function(data) 
		{
			if (data && ('success' == data.status)) {
				location.href = 'interview.jsp?doc_id=' + data.message;
			} else {
				displaySessionMessage(data.message ? data.message : "Unable to create.");
				oAcquisitionTitle.focus();
			}
		},
		error: function(object, text, error) 
		{
			alert(error);
		}
	});
	return false;
}

onLoginResponse = function(success, userInfo) {
	if (success) {
		displaySessionMessage(userInfo.sessionMessage);
		prepareInterviewTemplate();
	} else
		goHome();
}

if (!_doc_id) {
	location = 'dashboard.jsp';
} else {
	_oUserInfo.getLogon(true, onLoginResponse);	
}
