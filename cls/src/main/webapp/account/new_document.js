var _oUserInfo = new UserInfo();
var _docType = getUrlParameter("type");

changeLabelsForType = function() {
	
	if ('S' == _docType) {
		_docType = "S";
		docTypeName = 'solicitation';
		$("#legendTitle").html('New Solicitation');
		$("h1").html('New Solicitation');
		$('#labelDocumentNumber').html('Solicitation Document Number');
		$('#AcquisitionTitle').html('Aquisition Title');
		document.getElementById("acquisition_title").placeholder = "Aquisition title of the solicitation";
		document.getElementById("document_number").placeholder = "Solicitation document number";
		$('#btnCreate').val('Create New Solicitation');
	}
	else if ('O' == _docType) {
		_docType = "O";
		$("#legendTitle").html('New Order');
		$("h1").html('New Order');
		$('#labelDocumentNumber').html('Order Document Number');
		$('#AcquisitionTitle').html('Aquisition Title');
		document.getElementById("acquisition_title").placeholder = "Acquisition title of the order";
		document.getElementById("document_number").placeholder = "Order document number";
		$('#btnCreate').val('Create New Order');
	}
	
	var helpTextTemplate = $('#acquisition_title_help').html();
	var helpText = helpTextTemplate.replace(/docType/g, getDocumentTypeName(_docType).toLowerCase());
	$('#acquisition_title_help').html(helpText);
}

changeLabelsForType();

onSubmit = function() {
	var oAcquisitionTitle = $('#acquisition_title');
	var oData = {	doc_type: _docType,
					acquisition_title: $('#acquisition_title').val(),
					document_number: $('#document_number').val()
				};
	$.ajax({      
		url: getAppPath() + 'page?do=doc-create',
		type: 'POST',
		data: oData,
		cache: false,
		dataType: 'json',
		success: function(data) 
		{
			if (data && ('success' == data.status)) {
				if (_docType == "O")
					location = 'final_clauses.jsp?doc_id=' + data.message;
				else
					location = 'interview.jsp?doc_id=' + data.message;
			} else {
				displaySessionMessage(data.message ? data.message : "Unable to create.");
				oAcquisitionTitle.focus();
			}
		},
		error: function(object, text, error) 
		{
			alert(error);
		}
	});
	return false;
}

onLoginResponse = function(success, userInfo) {
	if (success) {
		displaySessionMessage(userInfo.sessionMessage);
	} else
		goHome();
}

_oUserInfo.getLogon(true, onLoginResponse);