var PAREN_LEFT = '(';
var PAREN_RIGHT = ')';

var BRAC_LEFT = '[';
var BRAC_RIGHT = ']';

var OPR_IS_NOT_NULL = 'IS NOT NULL';
var OPR_IS_NULL = 'IS NULL';
var OPR_AND = 'AND';
var OPR_OR = 'OR';
var OPERATOR_AND = ' ' + OPR_AND + ' ';
var OPERATOR_OR = ' ' + OPR_OR + ' ';

var PAREN_AND_CHECK = PAREN_RIGHT + OPERATOR_AND;
var PAREN_OR_CHECK = PAREN_RIGHT + OPERATOR_OR;

var OPERATOR_AND_CHECK = BRAC_RIGHT + OPERATOR_AND;
var OPERATOR_OR_CHECK = BRAC_RIGHT + OPERATOR_OR;

var OPERATOR_NULL_OR_CHECK = ' NULL' + OPERATOR_OR;
var OPERATOR_NULL_AND_CHECK = ' NULL' + OPERATOR_AND;

var OPR_IS_NOT_NULL_CHECK = BRAC_RIGHT + ' ' + OPR_IS_NOT_NULL;
var OPR_IS_NULL_CHECK = BRAC_RIGHT + ' ' + OPR_IS_NULL;
var OPR_EQUAL_CHECK = '=';
var OPR_NOT_EQUAL_CHECK = '<>';

var OPR_TYPE_NONE = -1;
var OPR_TYPE_EQUAL = 0;
var OPR_TYPE_NOT_EQUAL = 1;
var OPR_TYPE_AND = 2;
var OPR_TYPE_OR = 3;
var OPR_TYPE_IS_NULL = 4;
var OPR_TYPE_IS_NOT_NULL = 5;

// CJ-1184
var OPR_IS_CHECK = 'IS';
var IS_GREATER_THAN_OR_EQUAL_TO = 'IS GREATER THAN OR EQUAL TO ', OPR_TYPE_GREATER_THAN_OR_EQUAL_TO = 6;
var IS_LESS_THAN_OR_EQUAL_TO = 'IS LESS THAN OR EQUAL TO ', OPR_TYPE_LESS_THAN_OR_EQUAL_TO = 7; 
var IS_GREATER_THAN = 'IS GREATER THAN ', OPR_TYPE_GREATER_THAN = 8; 
var IS_LESS_THAN = 'IS LESS THAN ', OPR_TYPE_LESS_THAN = 9; 
var IS_EQUAL_TO = 'IS EQUAL TO ', OPR_TYPE_EQUAL_TO = 10; 

var SHOW_NO_ERROR = false;
var SHOW_ERROR = true;

/*
SUBSISTENCE = [FOOD/TEXTILES]
OR PERISHABLE SUBSISTENCE = [FOOD/TEXTILES]
OR SUBSISTENCE FOR BOTH GOVERNMENT USE AND RESALE IN THE SAME SCHEDULE = [FOOD/TEXTILES]

FIXED PRICE WITH ECONOMIC PRICE ADJUSTMENT -- ACTUAL COSTS = [FIXED PRICE]
OR FIXED PRICE WITH ECONOMIC PRICE ADJUSTMENT -- COST INDICIES = [FIXED PRICE]
OR FIXED PRICE WITH ECONOMIC PRICE ADJUSTMENT - ESTABLISHED PRICES = [FIXED PRICE]
*/

if (!String.prototype.includes) {
  String.prototype.includes = function(search, start) {
    'use strict';
    if (typeof start !== 'number') {
      start = 0;
    }
    
    if (start + search.length > this.length) {
      return false;
    } else {
      return this.indexOf(search, start) !== -1;
    }
  };
}

if (!String.prototype.endsWith) {
	  String.prototype.endsWith = function(searchString, position) {
	      var subjectString = this.toString();
	      if (typeof position !== 'number' || !isFinite(position) || Math.floor(position) !== position || position > subjectString.length) {
	        position = subjectString.length;
	      }
	      position -= searchString.length;
	      var lastIndex = subjectString.indexOf(searchString, position);
	      return lastIndex !== -1 && lastIndex === position;
	  };
}

function convertToInt(value, prefix) {
	var sValue = value.substring(prefix.length).trim();
	// CJ-1363: case of $550,000[]
	var sNumber = sValue.replace(new RegExp(',', 'g'), '');
	if (sNumber.startsWith('$'))
		sNumber = sNumber.substring(1);
	if (sNumber.endsWith('[]'))
		sNumber = sNumber.substring(0, sNumber.length - 2);
	//if (sValue != sNumber)
	//	alert(sValue + ' -> ' + sNumber);
	return parseInt(sNumber);
}

function QuestionEvalItem(parentOperator) {
	this.parentOperator = parentOperator;
	this.code;
	this.operatorType;
	this.value = null;
	this.oQuestion = null;
	this.openParen = false;
	this.closeParen = false;
	
	this.parseExpression = function parseExpression(expression, questionSet, pQuestion) {
		expression = expression.trim();
		if (!expression)
			return 'Empty';
		
		if (pQuestion.name == '[ACTIVE FEDERAL CONTRACTS]') {
			var debug = '';
		}
		
		var aSplit;
		var res = expression.slice(0, 1);
		if (res == PAREN_LEFT) { // '('
			this.openParen = true;
			expression = expression.substr(1);
			res = expression.slice(0, 1);
		}
		if (expression.substr(expression.length - 1) == PAREN_RIGHT) { // ')'
			this.closeParen = true;
			expression = expression.substr(0, expression.length - 1);
		}
		
		if (((res == BRAC_LEFT) || (res == '"')) && expression.includes(' IS ')) {
			
			var bContinue = true;
			if (res == BRAC_LEFT) {
				aSplit = expression.split(BRAC_RIGHT);
				if (aSplit.length != 2)
					return 'Missing name';
				this.code = aSplit[0] + BRAC_RIGHT;
			} else { // CJ-1363
				var altExpression = expression.substring(1);
				aSplit = altExpression.split('"');
				if (aSplit.length != 2)
					return 'Missing name';
				// CJ-1370, Check for false positive.
				// Some choices include 'IS' keyword.
				// i.e. ""PRICE IS A SELECTION FACTOR" = [EVALUATION FACTORS]"
				if (aSplit[0].includes(' IS '))
					bContinue = false;
				else
					this.code = BRAC_LEFT + aSplit[0] + BRAC_RIGHT;
			}
			if (bContinue) { // CJ-1370, May need to skip this section
				this.oQuestion = questionSet.findByName(this.code);
				if (this.oQuestion == null) {
					return this.code + ' is not found';
				}
				var sRightSide = aSplit[1].trim();
				if (OPR_IS_NOT_NULL == sRightSide) {
					this.operatorType = OPR_TYPE_IS_NOT_NULL;
					return '';
				}
				if (OPR_TYPE_IS_NULL == sRightSide) {
					this.operatorType = OPR_TYPE_IS_NULL;
					return '';
				}
				if (sRightSide.startsWith(IS_GREATER_THAN_OR_EQUAL_TO)) {
					this.operatorType = OPR_TYPE_GREATER_THAN_OR_EQUAL_TO;
					this.value = convertToInt(sRightSide, IS_GREATER_THAN_OR_EQUAL_TO);
					return '';
				}
				if (sRightSide.startsWith(IS_LESS_THAN_OR_EQUAL_TO)) {
					this.operatorType = OPR_TYPE_LESS_THAN_OR_EQUAL_TO;
					this.value = convertToInt(sRightSide, IS_LESS_THAN_OR_EQUAL_TO);
					return '';
				}
				if (sRightSide.startsWith(IS_GREATER_THAN)) {
					this.operatorType = OPR_TYPE_GREATER_THAN;
					this.value = convertToInt(sRightSide, IS_GREATER_THAN);
					return '';
				}
				if (sRightSide.startsWith(IS_LESS_THAN)) {
					this.operatorType = OPR_TYPE_LESS_THAN;
					this.value = convertToInt(sRightSide, IS_LESS_THAN);
					return '';
				}
				if (sRightSide.startsWith(IS_EQUAL_TO)) {
					this.operatorType = OPR_TYPE_EQUAL_TO;
					this.value = convertToInt(sRightSide, IS_EQUAL_TO);
					return '';
				}
				
				return 'Unknown format';
			}
		}
		
		aSplit = expression.split(OPR_EQUAL_CHECK);
		if (aSplit.length == 2) {
			this.code = aSplit[1].trim();
			this.value = aSplit[0].trim();
			this.operatorType = OPR_TYPE_EQUAL;
		} else {
			aSplit = expression.split(OPR_NOT_EQUAL_CHECK);
			if (aSplit.length < 2)
				aSplit = expression.split('!=');
			if (aSplit.length == 2) {
				this.code = aSplit[1].trim();
				this.value = aSplit[0].trim();
				this.operatorType = OPR_TYPE_NOT_EQUAL;
			} else
				return false;
		}

		this.oQuestion = questionSet.findByName(this.code);
		if (this.oQuestion == null) {
			return this.code + ' is not found';
		}
		if (pQuestion == this.oQuestion) {
			return this.code + ' is self-referencing';
		}
		if (this.value != null) {
			if (this.value.substr(0, 1) == '"') {
				if (this.value.substr(this.value.length - 1, 1) == '"') {
					this.value = this.value.substring(1, this.value.length - 1).trim();
				}
			}
			if (this.oQuestion.hasChoice(this.value, true)) // Hierarchy
				return '';
			var choices = this.oQuestion.choices.elements;
			var debug = 'Value not found: "' + this.value + '" in ' + this.oQuestion.name + ' choices. Available values: <ul>';
			for (var c = 0; c < choices.length; c++) {
				var questionChoiceItem = choices[c];
				if (this.value == questionChoiceItem.choice)
					return '';
				debug += '<li>"' + questionChoiceItem.choice + '"</li>';
			}
			debug += '</ul>';
			return debug;
		}
		return '';
	};
	
	this.evaluate = function evaluate() {
		if (this.oQuestion == null)
			return false;
		//if (this.code == "[CONTRACT VALUE]") {
		//	var debug = "";
		//}
		var result;
		if ((this.operatorType == OPR_TYPE_IS_NOT_NULL) || (this.operatorType == OPR_TYPE_IS_NULL)) {
			result = this.oQuestion.hasValue(true);
			if (this.operatorType == OPR_TYPE_IS_NOT_NULL) {
				return (true == result);
			} else {
				return (true != result);
			}
		}
		result = this.oQuestion.hasValue(this.value);
		if (this.operatorType == OPR_TYPE_NOT_EQUAL) {
			if (result == null) // CJ-1313
				return false;
			return (true != result);
		}

		// CJ-1184
		if ((this.operatorType != OPR_TYPE_GREATER_THAN_OR_EQUAL_TO) &&
			(this.operatorType != OPR_TYPE_LESS_THAN_OR_EQUAL_TO) &&
			(this.operatorType != OPR_TYPE_GREATER_THAN) &&
			(this.operatorType != OPR_TYPE_LESS_THAN) &&
			(this.operatorType != OPR_TYPE_EQUAL_TO)) {
			
			return (true == result);
		}

		var sAnswerValue = this.oQuestion.getNumberAnswer();
		if ((sAnswerValue == null) || (sAnswerValue == '') || (!isNumber(sAnswerValue)))
			return false;

		var iAnswerValue = parseInt(sAnswerValue);
		if (this.operatorType == OPR_TYPE_GREATER_THAN_OR_EQUAL_TO)
			return (iAnswerValue >= this.value);
		if (this.operatorType == OPR_TYPE_LESS_THAN_OR_EQUAL_TO)
			return (iAnswerValue <= this.value);
		if (this.operatorType == OPR_TYPE_GREATER_THAN)
			return (iAnswerValue > this.value);
		if (this.operatorType == OPR_TYPE_LESS_THAN)
			return (iAnswerValue < this.value);
		if (this.operatorType == OPR_TYPE_EQUAL_TO)
			return (iAnswerValue == this.value);
		
		return true;
	};
	
	this.hasCodes = function hasCodes(aCodes) {
		for (var index = 0; index < aCodes.length; index++) {
			if (aCodes[index] == this.code)
				return true;
		}
		return false;
	};
	
	this.isChangeEffected = function isChangeEffected(oChangeEventItems) {
		return oChangeEventItems.hasQuestion(this.oQuestion);
	};
	
	this.htmlExpression = function htmlExpression() {
		var html = (this.parentOperator == OPR_TYPE_NONE) ? ''
			: ((this.parentOperator == OPR_TYPE_AND) ? OPERATOR_AND : OPERATOR_OR);
		html += this.code;
		switch (this.operatorType) {
			case OPR_TYPE_IS_NOT_NULL:
				html += ' ' + OPR_IS_NOT_NULL;
				break;
			case OPR_TYPE_IS_NULL:
				html += ' ' + OPR_IS_NULL;
				break;
			default:
				html += ' ' + OPR_EQUAL_CHECK + ' ' + this.value;
				break;
		}
		return html;
	};
};

// ---------------------------------------------------------------------------
function QuestionEvaluation() {
	this.conditions;
	this.isExpressionValid = false;
	this.operatorType;
	
	this.parseStatement = function parseStatement(conditionToQuestion, questionSet, oQuestion) {
		if (!conditionToQuestion)
			return 'Empty';
		this.conditions = new Array();
		
		//if ('[OUTSIDE U.S.]' == oQuestion.name) {
		//	var debug = '';
		//}
		
		var parenOperatorType = OPR_TYPE_NONE;

		var aParenExpressions = conditionToQuestion.split(PAREN_OR_CHECK);
		if (aParenExpressions.length > 1) {
			parenOperatorType = OPR_TYPE_OR;
		} else {
			aParenExpressions = conditionToQuestion.split(PAREN_AND_CHECK );
			if (aParenExpressions.length > 1 )
				parenOperatorType = OPR_TYPE_AND;
		}
		var aExpressions = new Array();
		var aParens = new Array();
		for (var item = 0; item < aParenExpressions.length; item++) {
			if (parenOperatorType != OPR_TYPE_NONE) {
				if (item < aParenExpressions.length - 1)
					aParenExpressions[item] += PAREN_RIGHT;
			}
			var aSubExpr = this.splitConditions(aParenExpressions[item]);
			for (var iSub = 0; iSub < aSubExpr.length; iSub++) {
				if (iSub == 0) {
					aParens.push((item == 0) ? OPR_TYPE_NONE : parenOperatorType);
				} else {
					aParens.push(this.operatorType);
				}
				var sSubExpr = aSubExpr[iSub];
				iPos = sSubExpr.indexOf(' AND "');
				if (iPos > 0) {
					//console.log(sSubExpr);
					var sFirstExpr = sSubExpr.substr(0, iPos);
					aExpressions.push(sFirstExpr);
					this.operatorType = OPR_TYPE_AND;
					aParens.push(this.operatorType);
					sSubExpr = sSubExpr.substr(iPos + ' AND '.length); 
				} else {
					iPos = sSubExpr.indexOf(' OR "');
					if (iPos > 0) {
						var sFirstExpr = sSubExpr.substr(0, iPos);
						aExpressions.push(sFirstExpr);
						this.operatorType = OPR_TYPE_OR;
						aParens.push(this.operatorType);
						sSubExpr = sSubExpr.substr(iPos + ' OR '.length); 
					}
				}
				aExpressions.push(sSubExpr);
			}
		}

		var errors = '';
		for (var item = 0; item < aExpressions.length; item++) {
			var questionConditionItem = new QuestionEvalItem(aParens[item]);
			//var questionConditionItem = new QuestionEvalItem((item == 0) ? OPR_TYPE_NONE : this.operatorType);
			var error = questionConditionItem.parseExpression(aExpressions[item], questionSet, oQuestion);
			if (!error)
				this.conditions.push(questionConditionItem);
			else {
				if (errors != '')
					errors += '<br />';
				errors += error;
			}
		}

		if (errors == '') {
			this.isExpressionValid = true;
			if (SHOW_NO_ERROR == true)
				logOnConsole((oQuestion ? '\n<br /><strong>Question</strong> (id: ' + oQuestion.id + ', Name: ' + oQuestion.name + '): ' : '') + ' NO ERROR');
		} else {
			if (SHOW_ERROR == true)
				logOnConsole((oQuestion ? '\n<br /><strong>Question</strong> (id: ' + oQuestion.id + ', Name: ' + oQuestion.name + '): ' : '') +
						errors);
		}
		return errors;
	}
	
	this.splitConditions = function splitConditions(pConditionExpession) {
		this.operatorType = OPR_TYPE_NONE;
		var aExpressions = pConditionExpession.split(OPERATOR_OR_CHECK);
		if (aExpressions.length > 1)
			this.operatorType = OPR_TYPE_OR;
		else {
			aExpressions = pConditionExpession.split(OPERATOR_AND_CHECK);
			if (aExpressions.length > 1 )
				this.operatorType = OPR_TYPE_AND;
		}
		if (aExpressions.length > 1) {
			for (var item = 0; item < aExpressions.length - 1; item++) {
				aExpressions[item] += BRAC_RIGHT;
			}
		} else {
			aExpressions = pConditionExpession.split(OPERATOR_NULL_OR_CHECK);
			if (aExpressions.length > 1) {
				this.operatorType = OPR_TYPE_OR;
				for (var item = 0; item < aExpressions.length - 1; item++) {
					if (item < (aExpressions.length - 1))
						aExpressions[item] += ' NULL';
				}
			} else {
				aExpressions = pConditionExpession.split(OPERATOR_NULL_AND_CHECK);
				if (aExpressions.length > 1) {
					this.operatorType = OPR_TYPE_AND;
					for (var item = 0; item < aExpressions.length - 1; item++) {
						if (item < (aExpressions.length - 1))
							aExpressions[item] += ' NULL';
					}
				}
			}
		}
		return aExpressions;
	};
	
	this.isApplicable = function isApplicable() {
		if (this.isExpressionValid == false)
			return false;
		
		var aLastResults = new Array();
		var aLastOperators = new Array();
		var result = true;
		var isFirstSub = true;
		for (var item = 0; item < this.conditions.length; item++) {
			var oItem = this.conditions[item];
			if (oItem.openParen) {
				aLastResults.push(result);
				aLastOperators.push(oItem.parentOperator);
				isFirstSub = true;
				result = true;
			}
			var bAnswer = oItem.evaluate();
			if (isFirstSub)
				result = bAnswer;
			else {
				if (oItem.parentOperator == OPR_TYPE_OR)
					result = result || bAnswer;
				else
					result = result && bAnswer;
			}
			if (oItem.closeParen && (aLastResults.length > 0)) {
				var lastResult = aLastResults.pop();
				var lastParen = aLastOperators.pop();
				if (OPR_TYPE_AND == lastParen)
					result = result && bAnswer;
				else if (OPR_TYPE_OR == lastParen) // CJ-1313
					result = result || bAnswer;
			}
			isFirstSub = false;
		}
		return result;
	}
	
	this.hasCodes = function hasCodes(aCodes) {
		if (this.isExpressionValid == true)
			for (var item = 0; item < this.conditions.length; item++) {
				var oItem = this.conditions[item];
				if (oItem.hasCodes(aCodes))
					return true;
			}
		return false;
	}
	
	this.isChangeEffected = function isChangeEffected(oChangeEventItems) {
		if (this.isExpressionValid == true)
			for (var item = 0; item < this.conditions.length; item++) {
				var oItem = this.conditions[item];
				if (oItem.isChangeEffected(oChangeEventItems))
					return true;
			}
		return false;
	};
	
	this.htmlExpression = function htmlExpression() {
		var html = '';
		for (var item = 0; item < this.conditions.length; item++) {
			var oItem = this.conditions[item];
			if (item > 0)
				html += '<br />';
			html += oItem.htmlExpression();
		}
		return html;
	}
	
}
