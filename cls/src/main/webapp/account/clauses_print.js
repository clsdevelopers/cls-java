var _oUserInfo = new UserInfo();
var _oMetaData = new MetaData(10, true, 'onLimitChange');
var _selectedItems = new Array();
var _maxSelections = 5;
var _theToken = getUrlParameter("token");
var _isToken = false;

onLimitChange = function(oSelect) {
	var sLimit = oSelect.options[oSelect.selectedIndex].value;
	var iLimit = parseInt(sLimit);
	if (_oMetaData.limit != iLimit) {
		_oMetaData.limit = iLimit;
		retrieveList(1);
	}
}

viewRecordMsg = function() {
	alert('will be implemented');
}

adjustConditionText = function(OriginalConditions) {
	
	// Programmatically split the condition list field,
	// building an HTML string that can be displayed. 
	var htmlConditions = "";

	if (OriginalConditions == null)
		return '';
	
	var aCondition = OriginalConditions.split("(");
	for (var iCond = 0; iCond < aCondition.length; iCond++) {
	
		if (aCondition[iCond].length > 2) {
			if ((aCondition[iCond][1] == ')') 
			&& (/[A-Z]/.test(aCondition[iCond][0]))){
				if (htmlConditions.length == 0)
					htmlConditions += '(' + aCondition[iCond];
				else
					htmlConditions += '<br />(' + aCondition[iCond];
			} 								
			else
				htmlConditions += '(' + aCondition[iCond];
			}													
		else {													
			htmlConditions +=  aCondition[iCond] ;				
		}							
	}
	
	
	if (htmlConditions) {
		//var result = htmConditions.replace(new RegExp('\\n?\n','g'), '<br />'); // htmConditions.split("\n").join("<br />"); // htmConditions.replace(new RegExp('\r?\n','g'), '<br />'); // htmConditions.replace(/(?:\r\n|\r|\n)/g, '<br/>');
		return htmlConditions.replace(/\\n/g, "<br />");
	} else
		return '';
}



function formatClauseTitle(pTitle, pUrl) {
	if (pUrl) {
		return '<a href="' + pUrl + '" title="Visit the site" target="_blank">' + pTitle + '</a>';
	} else
		return pTitle;
}

retrieveList = function(offset, userInfo) {
	var page = getPagePath();
	var data =
		{'do': 'clause-current-list',
		query: $('#query').val(),
		section: $('#section').val(),
		sort_field: $("input:radio[name ='sort_field']:checked").val(), // $('#sort_field').val(),
		sort_order: $('#sort_order').val(),
		limit: _oMetaData.limit, offset: offset};
	var instance = this;
	$.ajax({
		url: getAppPath() + 'page',
		data: data,
		cache: false,
		dataType: 'json',
		success: function(data) {
			var htmlRows = '';
			_oMetaData.loadByJson(data.meta);
			if (data.data) {
				var aRecords = data.data;
				for (var iRow = 0; iRow < aRecords.length; iRow++) {
					var oRec = aRecords[iRow];
					var oConditions = oRec.clause_conditions;
								
					var sDetail = '<td class="nowrap"><a href="../admin/metadata/clauses_details.html?id=' + oRec.clause_id + '">' + oRec.clause_name + '</a></td>' ;
					if (_oUserInfo.isToken)
						sDetail = '<td class="nowrap">' + oRec.clause_name + '</a></td>';
						//sDetail = '<td class="nowrap"><a href="clause_detail.jsp?token=' + _theToken + '">' + oRec.clause_name + '</a></td>';
					else if (!_oUserInfo.isSuperUser)
						sDetail = '<td class="nowrap"><a href="clause_detail.jsp?id=' + oRec.clause_id + '">' + oRec.clause_name + '</a></td>';
			
					  
					htmlRows += '<tr>' +
						'<td><input type="checkbox" id="check_' + oRec.clause_id + '" onclick="selectItem('+ oRec.clause_id +')" ' + isChecked(oRec.clause_id) + '/></td>' +
						// '<td><a href="clauses_details.html?id=' + oRec.clause_id + '">' + oRec.clause_id + '</a></td>' +
						sDetail + 
						'<td>' + formatClauseTitle(oRec.clause_title, oRec.clause_url) + '</td>' +
						'<td>' + oRec.clause_version_name + '</td>' +
						'<td class="nowrap">' + (oRec.Section ? oRec.Section : '') + '</td>' +
						'<td>' + oRec.provision_or_clause + '</td>' +
						'<td>' + oRec.inclusion_cd + '</td>' +
						'<td style="width:35%;">' + adjustConditionText(oRec.clause_conditions) + '</td>' +						
						'<td style="width:10%;">' +  oRec.clause_rule + '</td>' +
						'</tr>';
				}
			}
			$("#tableContent tbody").html(htmlRows);
			_oMetaData.loadNavigations('retrieveList', 'ulPagination');
		},
		error: function(object, text, error) {
			alert(error);
		}
	});
};

isChecked = function(id) {
	var sSelected = '';
	for (var item = 0; item < _selectedItems.length; item++) {
		if (id == _selectedItems[item])
			sSelected = 'checked';
	}
	return sSelected;
}

selectItem = function(id) {

	var poCheckBox = $('#check_' + id);
	var bSelected = poCheckBox.prop('checked');

	if (bSelected && _selectedItems.length == 5) {
		poCheckBox.prop('checked', false);
		alert ('Max limit of ' + _maxSelections + ' selections has been reached. Print current items, then select additional items.');
		return;
	}
	
	// if not selected, remove from array
	if (!bSelected) {
		var oItem;
		for (var item = 0; item < _selectedItems.length; item++) {
			oItem = _selectedItems[item];
			if (oItem == id) { 
				_selectedItems.splice(item, 1);
				break;
			}
		}
	}
	else // add to array
	{ 
		_selectedItems.push (id);
	}
}

deselectAllItems = function() {

	for (var item = 0; item < _selectedItems.length; item++) {
		
		var poCheckBox = $('#check_' + _selectedItems[item]);
		poCheckBox.prop('checked', false);		
	}
	
	_selectedItems.length = 0; // clear the selected list after print.
}

printList = function(docType) {

	if (_selectedItems.length == 0) { 
		alert ("No clauses have been selected.")
		return;
	}
	
	var idStr = '';
	for (var item = 0; item < _selectedItems.length; item++) {
		idStr += ((idStr.length > 0) ? '-' : '') + _selectedItems[item];
	}
	var linkPath = getAppPath() + 'page?do=clause-print'; // &clause_ids=' + idStr;
	
    // Build a form
    var form = $('<form></form>').attr('action', linkPath).attr('method', 'post');
    // Add the one key/value
    form.append($("<input></input>").attr('type', 'hidden').attr('name', 'clause_ids').attr('value', idStr));
    form.append($("<input></input>").attr('type', 'hidden').attr('name', 'docType').attr('value', docType));
    //send request
    form.appendTo('body').submit().remove();
    
    deselectAllItems();
	
};


onLoginResponse = function(success, userInfo) {
	if (success) {
		/*
		if ((!userInfo.isSuperUser) && (!userInfo.isToken)){
			alert('Not authorized');
			goDashboard();
			return;
		}
		*/
		fillClsLatestVersion("clsVersion");
		retrieveList(0, userInfo);
	}
	displaySessionMessage(_oUserInfo.sessionMessage);
}

function fillClsLatestVersion(psClsVersionSelectId) {
	var page = getPagePath();
	var data = {'do': 'clauseversion-references-print'};
	$.ajax({
		url: getAppPath() + 'page',
		data: data,
		cache: false,
		dataType: 'json',
		success: function(data) {
			if (data.data) {
				var aRecords = data.data;
				var html = '<option value=""></option>';
				for (var iData = 0; iData < aRecords.length; iData++) {
					var oRec = aRecords[iData];
					html += '<option value="' + oRec.clause_version_id + '">' + oRec.clause_version_name + '</option>';
				}
				$('#' + psClsVersionSelectId).html(html);
			}
		},
		error: function(object, text, error) {
			alert(error);
		}
	});
};
_oUserInfo.getLogon(true, onLoginResponse, null, _theToken);
