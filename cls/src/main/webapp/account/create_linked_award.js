var _oUserInfo = new UserInfo();
var _doc_id = getUrlParameter("doc_id");
var _document_number = getUrlParameter("document_number");
var _acquisition_title = decodeURIComponent(getUrlParameter("acquisition_title"));
var _docClsVersionId = null;
var _activeClsVersionId = null;
var _jsonData = null;

$('#solocitation_number').val(_document_number);
$('#solocitation_title').val(_acquisition_title);

prepareLinkedAward = function() {

	var oData = { id: _doc_id };
	$.ajax({      
		url: getAppPath() + 'page?do=doc-getDocToLinkAward',
		type: 'GET',
		data: oData,
		cache: false,
		dataType: 'json',
		success: function(data) 
		{
			if (data && ('success' == data.status)) {
				displayDocumentInfo(data.data);
			} else {
				alert(data.message ? data.message : "Unable to retrieve document.");
				location = 'dashboard.jsp';
			}
		},
		error: function(object, text, error) 
		{
			alert(error);
		}
	});
}

displayDocumentInfo = function(json) {
	_jsonData = json;
	var oDocument = json.Documents;
	var oClsVersion = json.Clause_Versions;
	
	$('#solocitation_number').val(oDocument.document_number);
	$('#solocitation_title').val(oDocument.acquisition_title);
	
	_docClsVersionId = oDocument.clause_version_id;
	var iDocClsVersionName = oDocument.clause_version;
	var sHtml = '<input type="radio" name="cxClsVersion" id="cxClsVersionCurrent" value="' + _docClsVersionId + '" checked> '
			+ iDocClsVersionName ;
	$('#labelClsVerCurrent').html(sHtml);
	
	_activeClsVersionId = oClsVersion.id;
	var iActiveClsVersionName = oClsVersion.clause_version_name;
	sHtml = '<input type="radio" name="cxClsVersion" id="cxClsVersionNew" value="' + _activeClsVersionId + '"> Upgrade to '
			+ iActiveClsVersionName
			+ '<br /><button type="button" class="btn btn-default" onclick="onShowChange()" style="margin-left:1%;">Show Changes</button>';
	$('#labelClsVerNew').html(sHtml);
	
	if (_docClsVersionId == _activeClsVersionId) {
		$('#devClsVerNew').hide();
	} else {
		$('#devClsVerNew').show();
	}
}

onShowChange = function() {
	showVersionChangesDialog(_jsonData, null, null, null, null, null, null, true);
}

closeVerChangesModal = function() {
	$('#verChangesModal').modal('hide');
}

onSubmit = function() {
	
	$('#btnCancel').prop("className", "btn btn-default disabled");

	var oAcquisitionTitle = $('#acquisition_title');
	var iClsVersionId;
	if (document.getElementById('cxClsVersionCurrent').checked)
		iClsVersionId = _docClsVersionId;
	else
		iClsVersionId = _activeClsVersionId;
		
	var oData = {	doc_id: _doc_id,
					acquisition_title: $('#acquisition_title').val(),
					document_number: $('#document_number').val(),
					clause_version_id: iClsVersionId
				};
	$.ajax({      
		url: getAppPath() + 'page?do=doc-createLinkedAward',
		type: 'POST',
		data: oData,
		cache: false,
		dataType: 'json',
		success: function(data) 
		{
			if (data && ('success' == data.status)) {
				location = 'interview.jsp?doc_id=' + data.message;
			} else {
				$('#btnCancel').prop("className", "btn btn-default");
				displaySessionMessage(data.message ? data.message : "Unable to create.");
				oAcquisitionTitle.focus();
			}
		},
		error: function(object, text, error) 
		{
			$('#btnCancel').prop("className", "btn btn-default");
			alert(error);
		}
	});
	return false;
}

onLoginResponse = function(success, userInfo) {
	if (success) {
		displaySessionMessage(userInfo.sessionMessage);
		prepareLinkedAward();
	} else
		goHome();
}

if (!_doc_id) {
	location = 'dashboard.jsp';
} else {
	_oUserInfo.getLogon(true, onLoginResponse);	
}
