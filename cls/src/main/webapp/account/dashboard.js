var LIMIT = 10;

var _oUserInfo = new UserInfo();
var _interview = new Interview();
var _inProgressMetaData = new MetaData(LIMIT, true, 'onInProgressLimitChange');
var _inProgressSortField = 'created_at';
var _inProgressSortAscencing = false;
var _finalizeMetaData = new MetaData(LIMIT, true, 'onFinalizeLimitChange');
var _finalizeSortField = 'created_at';
var _finalizeSortAscencing = false;
var _resumeDocId = 0;
var _resumeDocType = null;
var _resumeDocStatusCd  = null;
var _transcriptWindow = null;

getDocTypeName = function(document_type) {
	switch (document_type) {
	case "S": return "Solicitation";
	case "A": return "Award";
	case "O": return "Order";
	}
	return document_type;
}

toggleSortIcons = function(prefixId, pIndex, sortAscencing) {
	var oSortIcons = $("[id^='" + prefixId + "']");
	for (var iIcon = 0; iIcon < oSortIcons.length; iIcon++) {
		var oIcon = oSortIcons[iIcon];
		$(oIcon).removeClass('fa-sort-alpha-asc'); // fa-sort-asc
		$(oIcon).removeClass('fa-sort-alpha-desc'); // fa-sort-desc
		if (iIcon == pIndex) {
			$(oIcon).addClass(sortAscencing ? 'fa-sort-alpha-asc' : 'fa-sort-alpha-desc'); // 'fa-sort-asc' : 'fa-sort-desc');
		} else {
			$(oIcon).addClass('fa-sort');
		}
	}
}

onInProgressLimitChange = function(oSelect) {
	var sLimit = oSelect.options[oSelect.selectedIndex].value;
	var iLimit = parseInt(sLimit);
	if (_inProgressMetaData.limit != iLimit) {
		_inProgressMetaData.limit = iLimit;
		retrieveInProgressList(1);
	}
}

onInProgressSortClick = function(field, pIndex) {
	if (_inProgressSortField == field)
		_inProgressSortAscencing = !_inProgressSortAscencing;
	else {
		_inProgressSortField = field;
		_inProgressSortAscencing = true;
	}
	toggleSortIcons('spanSIC', pIndex, _inProgressSortAscencing);

	retrieveInProgressList(1);
}

retrieveInProgressList = function(offset) {
	var page = getPagePath();
	var data = {
		'do': 'dashboard-doc-list', 
		interview_complete: 0,
		interview_status: 'I',
		query: $('#query').val(),
		sort_field: _inProgressSortField,
		sort_order: (_inProgressSortAscencing ? "asc" : "desc"),
		//limit: _inProgressMetaData.limit, offset: offset
	};
	var instance = this;
	$.ajax({
		url: getAppPath() + 'page',
		data: data,
		cache: false,
		dataType: 'json',
		xhrFields: { withCredentials: true },
		success: function(data) {
			var htmlRows = '';
			_inProgressMetaData.loadByJson(data.meta);
			if (data.data) {
				_inProgressData = data.data;
				displayInProgressDataTables();
			}

		},
		error: function(object, text, error) {
			alert(error);
		}
	});
};

var _inProgressTable = null;
var _inProgressData = null;

displayInProgressDataTables = function() {
	if (_inProgressData == null)
		return;
	
	var iPageLen =  (_inProgressTable == null) ? 10 : _inProgressTable.page.len();
	var firstTime = (_inProgressTable == null);
	if (!firstTime) {
		_inProgressTable.destroy();
		_inProgressTable = null;
	}
	
	var iHeight = calcEachTableHeight(1470, 1258);
	logConsole("InProgressTable Height = " + iHeight);
	
	_inProgressTable =
		$('#tableInProgress').DataTable( {
	        data: _inProgressData,
	        "aoColumns": [
                { "data": "created_at", "render": function ( data, type, row ) {
                	return longToDateStr(data);
                } },
                // { "data": "acquisition_title" },
                { "data": "acquisition_title", "render": function ( data, type, row ) {
                	return wordwrap(row.acquisition_title);
                } },
                //{ "data": "document_number" },
                { "data": "document_number", "render": function ( data, type, row ) {
                	return wordwrap(row.document_number);
                } },
                { "data": "clause_version" },
                //{ "data": "email" },
                { "data": "email", "render": function ( data, type, row ) {
                	return  wrapEmail(data, row.email);
                } },
                { "data": "document_type", "render": function ( data, type, row ) { 
                	return getDocTypeName(data);
                } },
                { "data": "updated_at", "render": function ( data, type, row ) {
                	//return  longToDateTimeStr(data, _oUserInfo.user_timezone_offset);
                	return  wrapLongToDateTimeStr(data, _oUserInfo.user_timezone_offset);
                } },
                { "data": "document_id", "render": function ( data, type, row ) {
                	var bLinkedAward = (row.solicitation_id != null && (row.document_type == 'A'));
                	return getViewHtml(row.document_id, row.acquisition_title, 'I', row.document_type, bLinkedAward);
                } },
                { "data": "document_id", "render": function ( data, type, row ) {
					
                	var bLinkedAward = (row.solicitation_id != null && (row.document_type == 'A'));
                	var actionDropdown = "";
                	
					var reopenLink = getResumeLink (row.document_id, row.acquisition_title, 'I', row.document_type, bLinkedAward);
					var removeLink = getDeleteLink (row.document_id, row.acquisition_title, 'I', row.document_type, bLinkedAward);
					
					actionDropdown = 
						/* Pretty, but presented issues with scrolling last few rows.
						 * 
						'<div class="dropdown" ><div>' + //  style="width:200px"  class="floater"
						'<a href="javascript:void(0);" class="btn btn-default btn-xs" data-toggle="dropdown">&nbsp;Action &nbsp;' +
						'<span class="caret"></span></a>' +
						'<ul class="dropdown-menu  pull-right" role="menu" style="min-width: 80px;">' +
						*/
						'<div class="dropdown">' +
						'<div class="floater">' +
						'<button type="button" class="btn btn-xs btn-default" data-toggle="dropdown">Action <span class="caret"></span></button>' +
						'<ul class="dropdown-menu  pull-right" role="menu" style="min-width: 80px;">' + 
						
						((reopenLink.length>0) ?'<li>' + reopenLink + '</li>': '') +
						((removeLink.length>0) ?'<li>' + removeLink + '</li>': '') +
						'</ul></div></div>';
					
                	return actionDropdown;
                	
                	
                } }
                /*{ "data": "document_id", "render": function ( data, type, row ) {
                	var bLinkedAward = (row.solicitation_id != null && (row.document_type == 'A'));
                	return getDocOptionHtml(row.document_id, row.acquisition_title, 'I', row.document_type, bLinkedAward, false);
                } }*/
            ],
            scrollY: iHeight,
	        scrollCollapse: false, // true
            searching: false,
            autoWidth: false,
            ordering: false,
	        paging: true,
	        pageLength: iPageLen,
	        deferRender: true,
	        language: {
	        	"info": "_START_ to _END_ of _TOTAL_",
	        	"sInfoEmpty": "0 to 0 of 0"
	        },
	        //'dom': 'rt<"bottom"ip><"clear">'
	        "dom": 'rt<"clear"><"bottom container-fluid"<"row"<"col-md-3"i><"col-md-2"><"col-md-7"p>>>'
		    //"dom": 'rt<"clear"><"bottom container-fluid"<"row"<"col-md-3"i><"col-md-2"l><"col-md-7"p>>>'
        	//"dom": 'rt<"clear"><"bottom container-fluid"<"row"<"col-md-4 col-md-offset-2"p><"col-md-4"i><"col-md-2"l>>>'
	    } );
	 //$('[data-toggle="tooltip"]').tooltip();
	$('#divItemsPerPageInProgress').show();
}

function selItemsPerPageInProgress_onClick(poSelect) {
	var iItemsPerPage = $(poSelect).val();
	if (_inProgressTable.page.len() != iItemsPerPage) {
		_inProgressTable.page.len(iItemsPerPage).draw();;
	}
}

onFinalizeLimitChange = function(oSelect) {
	var sLimit = oSelect.options[oSelect.selectedIndex].value;
	var iLimit = parseInt(sLimit);
	if (_finalizeMetaData.limit != iLimit) {
		_finalizeMetaData.limit = iLimit;
		retrieveFinalizeList(1);
	}
}

onFinalizeSortClick = function(field, pIndex) {
	if (_finalizeSortField == field)
		_finalizeSortAscencing = !_finalizeSortAscencing;
	else {
		_finalizeSortField = field;
		_finalizeSortAscencing = true;
	}
	toggleSortIcons('spanCIC', pIndex, _finalizeSortAscencing);
	retrieveFinalizeList(1);
}

retrieveFinalizeList = function(offset) {
	var bShowTable = (offset >= 0);
	if (offset < 1)
		offset = 1;
	var page = getPagePath();
	var data = {
		'do': 'dashboard-doc-list', 
		interview_complete: 1,
		interview_status: 'F',
		query: $('#query').val(),
		sort_field: _finalizeSortField,
		sort_order: (_finalizeSortAscencing ? "asc" : "desc"),
		//limit: _finalizeMetaData.limit, offset: offset
	};
	var instance = this;
	$.ajax({
		url: getAppPath() + 'page',
		data: data,
		cache: false,
		dataType: 'json',
		xhrFields: { withCredentials: true },
		success: function(data) {
			_finalyzedData = data.data;
			if (bShowTable)
				displayFinalyzedDataTables();
		},
		error: function(object, text, error) {
			alert(error);
		}
	});
};

//------------------------------------------------------------------------------------------

function format ( d ) {
    return 'The child row can contain any data you wish, including links, images, inner tables etc.';
}

// Array to track the ids of the details displayed rows
var _detailRows = [];

var _finalyzedTable = null;
var _finalyzedData = null;

// calcEachTableHeight(1579, 1320)
function calcEachTableHeight(firstWrapWidth, secondWrapWidth) {
	if (_tableHeight == null)
		_tableHeight = calcDefaultTableHeight();
	var iHeight = _tableHeight; 
	var iWindowWidth =  $(window).innerWidth();
	if (iWindowWidth < 1579) {
		if (iWindowWidth < 1320) {
			iHeight -= 50;
		} else {
			iHeight -= 25;
		}
	}
	
	if (iHeight < 150)
		iHeight = 150;

	return iHeight;
}

function adjustFinalyzedTableHeight() {
	if (_finalyzedTable) {
		var iHeight = calcEachTableHeight(1579, 1320);
		$(_finalyzedTable).children('.dataTables_scroll').children('.dataTables_scrollBody').css('height', iHeight + "px");
		//var oSettings = _finalyzedTable.settings();
		//oSettings.oScroll.sY = iHeight + "px";
		_finalyzedTable.draw();
	}
}

displayFinalyzedDataTables = function() {
	if (_finalyzedData == null)
		return;
	
	var iPageLen =  (_finalyzedTable == null) ? 10 : _finalyzedTable.page.len();
	var firstTime = (_finalyzedTable == null);
	if (!firstTime) {
		_finalyzedTable.destroy();
		_finalyzedTable = null;
	}

	if (_tableHeight == null)
		_tableHeight = calcDefaultTableHeight();
	
	var iHeight = calcEachTableHeight(1579, 1320); 
	logConsole("InProgressTable Height = " + iHeight);
	
	_finalyzedTable =
		$('#tableFinalize').DataTable( {
	        data: _finalyzedData,
	        "columns": [
                { "data": "created_at", "render": function ( data, type, row ) {
                	return longToDateStr(data);
                } },
                { "data": "acquisition_title", "render": function ( data, type, row ) {
                	return wordwrap(row.acquisition_title);
                } },
                { "data": "document_number", "render": function ( data, type, row ) {
                	return wordwrap(row.document_number);
                } },
                { "data": "clause_version" },
                //{ "data": "email" },
                { "data": "email", "render": function ( data, type, row ) {
                	return  wrapEmail(data, row.email);
                } },
                { "data": "document_type", "render": function ( data, type, row ) { 
                	return getDocTypeName(data);
                } },
                { "data": "updated_at", "render": function ( data, type, row ) {
                	//return  longToDateTimeStr(data, _oUserInfo.user_timezone_offset);
                	return  wrapLongToDateTimeStr(data, _oUserInfo.user_timezone_offset);
                } },
                { "data": "document_id", "render": function ( data, type, row ) {
                	var bLinkedAward = (row.solicitation_id != null && (row.document_type == 'A'));
                	return getViewHtml(row.document_id, row.acquisition_title, 'F', row.document_type, (row.LinkedDocuments.length > 0));
                } },
                { "data": "document_id", "render": function ( data, type, row ) {
                	var createAwardOption = '', linkedDocBlock = '', actionDropdown = '', copyDoc='';
					
					if ('S' == row.document_type) {
						createAwardOption = '<a href="create_linked_award.html?doc_id=' + row.document_id
							+ '&document_number=' +  row.document_number
							+ '&acquisition_title=' + row.acquisition_title
								+ '" >Create Linked Award</a>';
					}
					if  (('S' == row.document_type) || ('A' == row.document_type)) {
						copyDoc = '<a href="copy_interview.html?doc_id=' + row.document_id
							+ '&document_number=' +  row.document_number
							+ '&acquisition_title=' + row.acquisition_title +  
							//Sava CJ-1359 row.document_type added
							'&document_type='+row.document_type + '" >Create Template</a>';
					}
					
					var reopenLink = getResumeLink (row.document_id, row.acquisition_title, 'F', row.document_type, (row.LinkedDocuments.length > 0));
					var removeLink = getDeleteLink (row.document_id, row.acquisition_title, 'F', row.document_type, (row.LinkedDocuments.length > 0));
					actionDropdown = 
						
						/* Pretty, but presented issues with scrolling last few rows.
						 * 
						'<div class="dropdown" ><div>' + //  style="width:200px"  class="floater"
						'<a href="javascript:void(0);" class="btn btn-default btn-xs" data-toggle="dropdown">&nbsp;Action &nbsp;' +
						'<span class="caret"></span></a>' +
						'<ul class="dropdown-menu pull-right" role="menu">' +
						*/
						
						'<div class="dropdown">' +
						'<div class="floater">' +
						'<button type="button" class="btn btn-xs btn-default" data-toggle="dropdown">Action <span class="caret"></span></button>' +
						'<ul class="dropdown-menu  pull-right" role="menu" ' + 
						((createAwardOption.length>0) || (copyDoc.length>0) ? '' : ' style="min-width: 80px;"') + // smaller width for resume/open
						'>' + 
						
						((reopenLink.length>0) ?'<li>' + reopenLink + '</li>': '') +
						((createAwardOption.length>0) ?'<li>' + createAwardOption + '</li>' : '') +
						((copyDoc.length>0) ?'<li>' + copyDoc + '</li>': '') +
						((removeLink.length>0) ?'<li>' + removeLink + '</li>': '') +
						'</ul></div></div>';
					
                	return actionDropdown;
                } },
                {   "data": "document_id",
                    "render": function ( data, type, row ) {
                    	var oLinkedDocuments = row.LinkedDocuments;
                    	if (oLinkedDocuments.length < 1)
                    		return '';
                    	var html
                    		= '<button class="btn btn-xs btn-link" onclick="javascript:toggleDetails(this)" id="linkAw' + row.document_id + '" title="Show linked awards">'
                    		+ getLinkedAwardBtnContent(false)
                    		+ '</button>';
                    	return html;
                    }
                },
            ],
            scrollY: iHeight,
	        scrollCollapse: false, // true
            searching: false,
            autoWidth: false,
            ordering: false,
	        paging: true,
	        pageLength: iPageLen,
	        deferRender: true,
	        language: {
	        	"info": "_START_ to _END_ of _TOTAL_",
	        	"sInfoEmpty": "0 to 0 of 0"
	        },
	        //'dom': 'rt<"bottom"ip><"clear">'
	        "dom": 'rt<"clear"><"bottom container-fluid"<"row"<"col-md-3"i><"col-md-2"><"col-md-7"p>>>'
		    //"dom": 'rt<"clear"><"bottom container-fluid"<"row"<"col-md-3"i><"col-md-2"l><"col-md-7"p>>>'
        	//"dom": 'rt<"clear"><"bottom container-fluid"<"row"<"col-md-4 col-md-offset-2"p><"col-md-4"i><"col-md-2"l>>>'
	    } );
	 //$('[data-toggle="tooltip"]').tooltip();
	$('#divItemsPerPageFinalize').show();
}

function selItemsPerPageFinalize_onClick(poSelect) {
	var iItemsPerPage = $(poSelect).val();
	if (_finalyzedTable.page.len() != iItemsPerPage) {
		_finalyzedTable.page.len(iItemsPerPage).draw();;
	}
}

// ------------------------------------------------------------------------------------------

getDocOptionHtml = function(id, acquisition_title, docStatusCd, docType, bLinkedAward, bForLinkBlock) {
	var linkView = getAppPath() + 'page?do=doc-download&id=' + id + '&format=';
	var linkViewXmlPlainText = getAppPath() + 'page?do=doc-download&pt=1&id=' + id + '&format=xml';
	
	// Show Resume button 
	// 1. if this is InProgress tab or
	// 2. it this is Finalize tab and Award has not been linked to the document.
	var htmlResume = "";
	if ((docStatusCd == 'I') || (!bLinkedAward)) {	
		htmlResume = getResumeLink (id, acquisition_title, docStatusCd, docType, bLinkedAward);
	}						
	
	// Show Delete button 
	// 1. if this is InProgress tab or
	// 2. it this is Finalize tab and Award has not been linked to the document.
	var htmlDelete = "";
	if ((docStatusCd == 'I') || (!bLinkedAward)) {	
		
		// linked document 'blocks' appear on the Finalize Tab.
		// Use status = 'F' for the table to refresh properly.
		var docStatusCdTmp = docStatusCd;
		var bLinkedAwardTmp = bLinkedAward;
		if (bForLinkBlock) { 
			docStatusCdTmp = 'F';
			bLinkedAwardTmp = false;
		}
		
		htmlDelete = 
			getDeleteLink (id, acquisition_title, docStatusCdTmp, docType, bLinkedAwardTmp);
	}
	
	var actionDropdown = 
		'<div class="dropdown" ><div>' +
		'<a href="javascript:void(0);" class="btn btn-default btn-xs" data-toggle="dropdown">&nbsp;Action &nbsp;' +
		'<span class="caret"></span></a>' +
		'<ul class="dropdown-menu  pull-right" role="menu" style="min-width: 80px;">' +
		((htmlResume.length>0) ?'<li>' + htmlResume + '</li>': '') +
		((htmlDelete.length>0) ?'<li>' + htmlDelete + '</li>': '') +
		'</ul></div></div>';
	
	var html =
		/*
		'<div class="dropdown" ><div>' + 
		'<a href="javascript:void(0);" class="btn btn-default btn-xs" data-toggle="dropdown">&nbsp;View &nbsp;' +
		'<span class="caret"></span></a>' +
		'<ul class="dropdown-menu pull-left" role="menu">' +
		*/

		'<div><div class="dropdown">' + //  style="width:225px" style="white-space: nowrap;" width:225px
		'<div class="floater">' +
		'<button type="button" class="btn btn-xs btn-default" data-toggle="dropdown">View <span class="caret"></span></button>' +
		'<ul class="dropdown-menu" role="menu">' +

					'<li><a href="' + linkView + 'html">HTML</a></li>' + //  target="_blank"
					'<li><a href="' + linkView + 'pdf">PDF</a></li>' +
					'<li><a href="' + linkView + 'docx">WORD</a></li>' +
					'<li><a href="' + linkView + 'xml">XML</a></li>' +
					'<li><a href="' + linkViewXmlPlainText + '">XML (Plain Text)</a></li>' +
					'<li><a href="#" onclick="javascript:onTranscriptMenuClick('+id+');">TRANSCRIPT</a></li>' +
				'</ul>' +
			'</div>' +
		'</div></div>' +
			
		'</td><td>' + actionDropdown; 

	return html;
}


getViewHtml = function(id, acquisition_title, docStatusCd, docType, bLinkedAward) {
	var linkView = getAppPath() + 'page?do=doc-download&id=' + id + '&format=';
	var linkViewXmlPlainText = getAppPath() + 'page?do=doc-download&pt=1&id=' + id + '&format=xml';
	
	var html =
		/*
		'<div class="dropdown floater" ><div>' + 
		'<a href="javascript:void(0);" class="btn btn-default btn-xs" data-toggle="dropdown">&nbsp;View &nbsp;' +
		'<span class="caret"></span></a>' +
		'<ul class="dropdown-menu pull-left" role="menu">' +
		*/
		// No bug, just re-aligned coordinates with Action dropdown.
		
		'<div class="dropdown">' +
		'<div class="floater">' +
		'<button type="button" class="btn btn-xs btn-default" data-toggle="dropdown">View <span class="caret"></span></button>' +
		'<ul class="dropdown-menu" role="menu">' +
		
					'<li><a href="' + linkView + 'html">HTML</a></li>' + //  target="_blank"
					'<li><a href="' + linkView + 'pdf">PDF</a></li>' +
					'<li><a href="' + linkView + 'docx">WORD</a></li>' +
					'<li><a href="' + linkView + 'xml">XML</a></li>' +
					'<li><a href="' + linkViewXmlPlainText + '">XML (Plain Text)</a></li>' +
					'<li><a href="#" onclick="javascript:onTranscriptMenuClick('+id+');">TRANSCRIPT</a></li>' +
				'</ul>' +
			'</div>'
		+ '</div>'; // KH

	return html;
}


getResumeLink = function(id, acquisition_title, docStatusCd, docType, bLinkedAward) {
	
	// Show Resume button 
	// 1. if this is InProgress tab or
	// 2. it this is Finalize tab and Award has not been linked to the document.
	var htmlResume = "";
	if ((docStatusCd == 'I') || (!bLinkedAward)) {	
		htmlResume = 
			'<a href="#" onclick="javascript:resumeDocument(' + id + ',' + bLinkedAward + ', \'' + docType + '\', \'' + docStatusCd + '\')" >' + ((docStatusCd == 'I') ? 'Resume' : 'Reopen') + '</a> ' ;
	}						

	return htmlResume;
}

getDeleteLink = function(id, acquisition_title, docStatusCd, docType, bLinkedAward) {

	
	// Show Delete button 
	// 1. if this is InProgress tab or
	// 2. it this is Finalize tab and Award has not been linked to the document.
	var htmlDelete = "";
	if ((docStatusCd == 'I') || (!bLinkedAward)) {	
		htmlDelete = 
			'<a href="#" onclick="javascript:removeDocument(' + id + ',\'' + docStatusCd + '\',\'' + acquisition_title.replace(/'/g, '&quot;') + '\')" >Delete</a>'; 
	}
							

	return htmlDelete;
}

onTranscriptMenuClick = function(id) {
	_interview.retrieve(id, onTranscriptMenuCallBack, _oUserInfo, true); // CJ-1223
}

onTranscriptMenuCallBack = function() {
	_interview.onTranscriptBtnClick(false, true); // CJ-1223
}

toggleDetails = function(pButton) {
	var tr = $(pButton).closest('tr');
    var row = _finalyzedTable.row( tr );
    var idx = $.inArray( tr.attr('id'), _detailRows );

    var isUp;
    if ( row.child.isShown() ) {
        // tr.removeClass( 'details' );
        row.child.hide();

        // Remove from the 'open' array
        _detailRows.splice( idx, 1 );
        isUp = false;
        $(pButton).prop('title', 'Show linked awards');
    }
    else {
    	// tr.addClass( 'details' );
        // row.child( format( row.data() ) ).show();
        var oRec = row.data();
        var sChild = getLinkedDocBlock(oRec, oRec.document_id);
        row.child( sChild ).show();

        // Add to the 'open' array
        if ( idx === -1 ) {
            _detailRows.push( tr.attr('id') );
        }
        isUp = true;
        $(pButton).prop('title', 'Hide linked awards');
    }
    $(pButton).html(getLinkedAwardBtnContent(isUp));
}


getLinkedAwardBtnContent = function(isUp) {
	var oLinkContent;
	if (isUp)
		oLinkContent = '<i class="fa fa-chevron-up fa-3"></i>'; // fa-caret-square-o-up		
	else
		oLinkContent = '<i class="fa fa-chevron-down fa-3"></i>'; // fa-caret-square-o-down
	return oLinkContent + '<span class="hide">Link Detail</span>';
}

getLinkedDocLink = function(pRec, pRow) {
	var oLinkedDocuments = pRec.LinkedDocuments;
	if (oLinkedDocuments.length < 1)
		return '';

	return '<button class="btn btn-xs btn-link" onclick="javascript:toggleDetails(' + pRow + ')" id="linkAw' + pRow + '">'
		+ getLinkedAwardBtnContent(false)
		+ '</button>';
}

getLinkedDocBlock = function(pRec, pRow) {
	var oLinked = pRec.LinkedDocuments;
	if (oLinked.length < 1)
		return '';
	var htmlRows = '<table class="table-hover" style="width:100%">'; // '<table class="table table-hover table-striped" style="width:100%;">';
	for (var iLink = 0; iLink < oLinked.length; iLink++) {
		var oRec = oLinked[iLink];
		htmlRows += '<tr>' + //  id="trAw' + pRow + "_" + iLink + '" style="display: none !important;"
			'<td width="7.30%"><i class="fa fa-angle-right"></i> ' + longToDateStr(oRec.created_at) + '</td>' + //  class="dashboard_doc_date"
			'<td width="14.66%">' + oRec.acquisition_title + '</td>' + //  class="dashboard_doc_title"
			'<td width="13.76%">' + oRec.document_number + '</td>' + //  class="dashboard_doc_number"
			'<td width="4.39%">' + oRec.clause_version + '</td>' + //  class="dashboard_doc_version" 			
			'<td width="17.25%">' + (oRec.email ? oRec.email : '') + '</td>' + //  class="dashboard_created_by"
			'<td width="5.88%">' + getDocTypeName(oRec.document_type) + '</td>' +
			'<td width="12.60%">' + longToDateTimeStr(oRec.updated_at, _oUserInfo.user_timezone_offset) + '</td>' + //  class="dashboard_doc_date"
			'<td>' + getDocOptionHtml(oRec.document_id, oRec.acquisition_title, oRec.doc_status_cd, oRec.document_type, true, true) + '</td>' + // col-sm-3 //  class="dashboard_doc_btn"
			'<td><em>(' + (oRec.doc_status_cd == 'F' ? 'Finalize' : 'In Progress') + ')</em></td>' + //  class="dashboard_doc_btn"
			//'<td class="dashboard_linked-award"></td>' +
			'</tr>';
	}
	return htmlRows + "</table>"; // 
}

isReopen = function(docType, docStatusCd, bLinkedAward) {
	if (docStatusCd == 'I')	{
		return false;
	}
	if (docType == 'O' || docType == 'A') {
		return true;
	}
	//it must be S
	return !bLinkedAward;
}

resumeDocument = function (id, bLinkedAward, docType, docStatusCd) {
	if (bLinkedAward) {  		
		resumeDocumentRedirectTo(id, docType);
		return;
	}
	var page = getPagePath();
	var data = {
		'do': 'clauseversionchange-list',
		'id': id
	};
	var instance = this;
	$.ajax({
		url: getAppPath() + 'page',
		data: data,
		cache: false,
		dataType: 'json',
		xhrFields: { withCredentials: true },
		success: function(data) {
			if (data) {
				if ('success' == data.status) {
					showVersionChangesContent(data.data, id, docType, docStatusCd);
				} else if (data.data) {						
					resumeDocumentRedirectTo(id, docType);
				} else if ('no session' == data.message) { 
					displaySessonEnded(true); // CJ-573
				} else {
					alert(data.message);
				}
			} 
		},
		error: function(object, text, error) {
			alert(error);
		}
	});
}

resumeDocumentRedirectTo = function(id, docType) {
	if (docType == "O")
		location.href = 'final_clauses.jsp?doc_id=' + id;
	else
		location.href = 'interview.jsp?doc_id=' + id; // $(location).prop('href','interview.jsp?doc_id=' + id); 
}

$("#bnVerChangesUpdate").click( function()
{
	upgradeDocVersion(_resumeDocId, _resumeDocType);
//	sendReopenRequest(_resumeDocId, _resumeDocType, upgradeDocVersion);	
});
 
$("#bnVerChangesCancel").click( function()
{
	$('#verChangesModal').modal('hide'); // $('#verChangesModal').hide();	
});
    
showVersionChangesContent = function(oData, docId, docType, docStatusCd) {
	_resumeDocId = docId;
	_resumeDocType = docType;
	_resumeDocStatusCd = docStatusCd;
	showVersionChangesDialog(oData, docId);
}

removeDocument = function(id, docStatusCd, acquisition_title) {
	if (confirm('Are you sure you want to delete "' + acquisition_title + '" document?') == false) {
		return;
	}
	var page = getPagePath();
	var data = {
		'do': 'doc-delete',
		'id': id
	};
	var instance = this;
	$.ajax({
		url: getAppPath() + 'page',
		data: data,
		cache: false,
		dataType: 'json',
		xhrFields: { withCredentials: true },
		success: function(data) {
			if (data) {
				if ('success' == data.status) {
					if (docStatusCd == 'I')
						retrieveInProgressList(1);
					else
						retrieveFinalizeList(1);
				} else if ('no session' == data.message) {
					alert('You have been logged out for being inactivity, please refresh the page and log back into CLS to continue...');
					goHome();
				}	
				alert(data.message);
			} else
				alert("Unable to process");
		},
		error: function(object, text, error) {
			logAjaxErrorOnConsole(object, text, error);
			alert(error);
		}
	});
}

toggleOptionsForUser = function() {
	$('#spanUserName').html(_oUserInfo.fullName());
	// $('#spanUserOrgName').html(_oUserInfo.orgName);
	if (_oUserInfo.canAccessAdminOptions()) {
		var oDivContainer = $('#divContainer');
		oDivContainer.removeClass('margin-top-15');
		oDivContainer.addClass('margin-top-30');
	}
}

onSearchClick = function() {
	retrieveInProgressList(1);
	retrieveFinalizeList(1);
}

// CJ-674 - Transcript link needs this defined.
var confirmOnInterviewExit = function (e) {
    // If we haven't been passed the event get the window.event
    e = e || window.event;
    var message = 'This session has been modified. If you exit without saving, all changes will be lost.';
    // For IE6-8 and Firefox prior to version 4
    if (e) {
        e.returnValue = message;
    }
    // For Chrome, Safari, IE8+ and Opera 12+
    return message;
};

onLoginResponse = function(success, userInfo) {
	if (success) {
		toggleOptionsForUser();
		retrieveInProgressList(1);
		retrieveFinalizeList(-1);
		// onSearchClick();
		$("#navDashboard").addClass("active");
	}
	displaySessionMessage(_oUserInfo.sessionMessage);
}

_oUserInfo.getLogon(true, onLoginResponse, null, null, 'divSessionTimeout'); // CJ-634 added 'divSessionTimeout' parameter

newDocument = function( docType) {
	location.href = 'new_document.html?type=' + docType;
}

//-----------------------------------------------------------------
//Windows resize events
//-----------------------------------------------------------------
var TAB_ENUM = {
    inCompleted: 0,
    finalyzed: 1
}

var _activeClauseTab = TAB_ENUM.inCompleted;
var _tableHeight = null;

function calcDefaultTableHeight() {
	var divTopBannerHeight = $('#divTopNavBar').height();
	var odivAdminOptionsHeight = $('#divAdminOptions').height();
	if (odivAdminOptionsHeight != null)
		divTopBannerHeight -= odivAdminOptionsHeight;
	var tableOptionsHeight = $('#tableOptions').height();
	var divTabPanelHeight = $('#divTabPanel').height();

	var divBottomFooterHeight = 24;
	// var divItemsPerPageHeight = 0; // 32;
	var extraSpacing = 60 + 90; // 90

	var iWindowHeight =  $(window).innerHeight();
	
	_tableHeight = iWindowHeight - divTopBannerHeight - tableOptionsHeight - divTabPanelHeight - divBottomFooterHeight - extraSpacing; // divItemsPerPageHeight 
	
	return _tableHeight;
}

/*
$(function(){
	// document.body.style['overflow'] = 'auto';
	// adjustHeights();
});
*/

$('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
	var target = $(e.target).attr("href") // activated tab
	
	if ('#inprogress' == target) {
		_activeClauseTab = TAB_ENUM.inCompleted;
		if (_windowSizeChanged)
			displayInProgressDataTables();
	}
	else { // if ('#finalize' == target) 
		_activeClauseTab = TAB_ENUM.finalyzed;
		if (_windowSizeChanged || (_finalyzedTable == null))
			displayFinalyzedDataTables();
	}
	_windowSizeChanged = false;
});

var _windowSizeChanged = false;
var resizeRunTime;
var resizeTimeout = false;
var resizeDelta = 200;

$(window).resize(function() {
    resizeRunTime = new Date();
    if (resizeTimeout === false) {
        resizeTimeout = true;
        setTimeout(resizeWinEnd, resizeDelta);
    }
});

function resizeWinEnd() {
    if (new Date() - resizeRunTime < resizeDelta) {
        setTimeout(resizeWinEnd, resizeDelta);
    } else {
        resizeTimeout = false;
    	_windowSizeChanged = true;
        calcDefaultTableHeight();
    	if (_activeClauseTab == TAB_ENUM.inCompleted) {
    		if (_inProgressTable != null) {
        		displayInProgressDataTables();
    		}
    	} else {
    		if (_finalyzedTable != null) {
    			displayFinalyzedDataTables();
    		}
    	}
    	/*
        var iHeight, sTableId, oTable;
    	if (_activeClauseTab == TAB_ENUM.inCompleted) {
    		if (_inProgressTable == null) {
        		displayInProgressDataTables();
        		return;
    		}
    		oTable = _inProgressTable;
    		iHeight = calcEachTableHeight(1470, 1258);
    		sTableId = 'tableInProgress';
    	} else {
    		if (_finalyzedTable == null) {
    			displayFinalyzedDataTables();
        		return;
    		}
    		oTable = _finalyzedTable;
    		iHeight = calcEachTableHeight(1579, 1320);
    		sTableId = 'tableFinalize'; 
    	}
    	var oScrollBody = $('#' + sTableId + '_wrapper .dataTables_scrollBody');
    	oScrollBody.css('max-height', iHeight + "px");
    	oTable.draw(false);
    	*/
    }               
}



function wrapEmail(value, email) {
	var result = email
	if ((result!= null) && (result.indexOf("@") > 0))
		result = result.replace("@", "<br>@");
	return result;
}

function wrapLongToDateTimeStr(value, timeOffset) {
	var result = longToDateTimeStr(value, timeOffset);
	result = result.replace(" ", "<br>");
	return result;
}


// Some sort of wrapping is required for long title / document numbers.
// If the strings are too long, then it will misalign the header.
// Usually this only happens when a tester adds a long string....

function wordwrap( str, width) {
	 
	if ((str.indexOf (" ") < 0)
	&& (str.indexOf ("-") < 0))
		str = '<div style="width: 100px;word-break:break-all;" >' + str + '</div>';
	//else
	//	str = '<div style="width: 100px;" >' + str + '</div>';
	
	
	return str;
}

