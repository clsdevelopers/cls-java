<!DOCTYPE html>
<html lang='en'>
<head>
	<meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
	<meta http-equiv="Pragma" content="no-cache" />
	<meta http-equiv="Expires" content="0" />
	<meta charset='utf-8'>
	<meta content='IE=Edge,chrome=1' http-equiv='X-UA-Compatible'>
	<meta content='width=device-width, initial-scale=1.0' name='viewport'>
	<title>Interview</title>
    <link href="../assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="../assets/cleditor1_4_5/jquery.cleditor.css" rel="stylesheet">
	<link href="../assets/application.css" media="all" rel="stylesheet" type="text/css" />
	<link href="interview.css" media="all" rel="stylesheet" type="text/css" />
	<!-- Le HTML5 shim, for IE6-8 support of HTML elements -->
	<!--[if lt IE 9]>
	<script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.6.1/html5shiv.js" type="text/javascript"></script>
	<![endif]-->

</head>
<body style="width: 100%;height: 100%;clip: auto;position: absolute;overflow: hidden;">

<div id="divTopBanner" style="display:none;"></div>

<div data-alerts="alerts" data-titles="{'warning': '<em>Warning!</em>'}" data-ids="myid" data-fade="10000" style="padding-top:10px;"></div>
 
<div class='container-fluid containerMarginTopNormal' id="divContainer" style="max-height:90%; padding-left:0; padding-right:0;">

	<div class='page-header' id='page-header-id' style='width:100%;'>

		<div class='panel panel-primary' id='collapsedHeader' style='border: 0; padding-top:0; padding-bottom:0;margin-top:-10px;margin-bottom:0;'> 
		<div class='panel-heading QPanelHeading' >							
		<div class='row' style="white-space: nowrap;margin-left:5px;">ACQUISITION PROFILE
		    <a href="#" onclick="expandHeader();" style="float:right;color:white;margin-right:20px;" >
		    <span id="expand_link"> <img src="../assets/images/menu_collapse.png" alt="Menu collapse." /></span></a>
		</div>
		</div>
		</div>
	
		<table id='expandedHeader' style="width:100%; margin-top:10px; display:block;">
			<tr>
				<td valign="top" style="padding-left:10px; padding-right:20px;" width="55%">
					<table style="width:100%;">
					<tr>
						<td width="45%">
							<span style="white-space: nowrap;"><span class="docLabel">Acquisition Title:</span> <span id="acquisition_title"></span></span>
						</td>
						<td width="45%">
							<span style="white-space: nowrap;"><span class="docLabel">Last Updated:</span> <span id="spanUpdatedAt"></span></span>
						</td>
					</tr>
					<tr>
						<td width="45%">
							<span style="white-space: nowrap;"><span class="docLabel">Document Number:</span> <span id="document_number"></span></span><br/>
						</td>
						<td>
							<font class="text-warning"> <span id="spanAutoSaveDisplay"></span></font>
						</td>
					</tr>
					<tr>
						<td width="45%">
							<span style="white-space: nowrap;"><span class="docLabel">Document Type:</span> <span id="spanDocType"></span></span>
						</td>
						<td>
						<div class="progress" id="barProgress" style="margin-top:10px;margin-bottom:0px;">
						  <div class="progress-bar progress-bar-success" id="divBarProgress" role="progressbar" aria-valuenow="100"
						  aria-valuemin="0" aria-valuemax="100" style="width:100%">Progress</div>
						</div>  
						</td>
					</tr>
					</table>
				</td>
				<td width="45%" valign="top" style="padding-right:10px;">
					<div style="border-style:solid;border-width:1px;padding:5px;text-align:left;margin-top:-5px;margin-right:10px;font-size: smaller">
						<span id="spanFixedMessage">A complete and accurate set of clauses will not be available until all questions in all applicable sections have been answered and the session has been validated.</span>
					</div>
				</td>
			</tr>
		</table>
	</div>
	
	<table id="tableMain">
		<tr>
			<td id="tdQGroup" style="width:370px;min-width:250px">
				<div class="panel panel-primary" style="border: 0;">
					<div class="panel-heading">QUESTION GROUPS</div>
					<div class="panel-body" style="padding-left:0px;padding-right:5px;scroll:auto;" id="QuestionGroupPanel">
						<div id="divQuestionGroup"></div>
					</div>
				</div>
			</td>
			<td class="tdContents" id="tdContent" style="min-width:450px">
				<div id="divContent"></div>
				<div id="divContentFooter">
					<button type="button" class="btn btn-sm btn-default" id="btnSaveContinue" onclick="_interview.onSaveBtnClick(0);">Save</button>
					<button type="button" class="btn btn-sm btn-primary" id="btnSaveExit" onclick="_interview.onSaveBtnClick(1);">Save and Exit</button>
					<button type="button" class="btn btn-sm btn-success" id="btnCompleteExit" onclick="_interview.onSaveBtnClick(2);">Finalize and Exit</button>
					<button type="button" class="btn btn-sm btn-primary" id="btnTranscript" onclick="_interview.onTranscriptBtnClick();">Transcript</button>
					<button type="button" class="btn btn-sm btn-danger" id="btnReset" title="The reset button resets all answers back to default values" >Reset</button>
				</div>
			</td>
			<td id="tdSolication" style="width:450px;min-width:300px;">
				<div id="divSolication" class="panel panel-primary" style="border: 0;">
					<div class="panel-heading" style="position: relative"> 
						CLAUSE CART
						<button type="button" class="btn btn-default btn-sm" id="btnFillins" style="position:absolute; right:5px; top:6px;" onclick="_interview.onSaveBtnClick(4);">Clause Worksheet</button>
					</div>
					<div id="divContentSection" class="panel-body" style=";scroll:auto;padding-left:0px;padding-right:5px;"></div>
				</div>
			</td>
		</tr>
	</table>
</div>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-xlg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="onFillinCancelClick()"><span aria-hidden="true">&times;</span></button>
        <div class="modal-title" id="myModalLabel">Modal title</div>
      </div>
      <div class="modal-body" id="myModalBody">
      </div>
      <div class="modal-footer">
      	<div class="row">
      		<div class='col-md-8' id="myModelRemarks"></div>
      		<div class='col-md-3' style="white-space:nowrap;">
		        <button type="button" class="btn btn-default" data-dismiss="modal" onclick="onFillinCancelClick()">Close</button>
		        <button type="button" class="btn btn-primary" id="myModelSave">In-Progress</button>
		        <button type="button" class="btn btn-success" id="myModelComplete">Complete</button>
      		</div>
      	</div>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="completeModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="onFinalCheckCancelClick()"><span aria-hidden="true">&times;</span></button>
        <div class="modal-title" id="completeTitle">Review clause changes (due to Crossover rules) and complete the document interview</div>
      </div>
      <div class="modal-body" id="completeModalBody">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal" title="Cancel this document interview completion" onclick="onFinalCheckCancelClick()">Close</button>
        <!-- <button type="button" class="btn btn-success" id="completeModelComplete" title="Submit and complete this document interview" onclick="onFinalCheckSubmitClick()">Submit and Complete</button> CJ-611 --> 
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="pleaseWaitDialog" role="dialog" data-backdrop="static" data-keyboard="false" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h1>Processing...</h1>
				This may take 1-2 minutes...
			</div>
			<div class="modal-body" style="text-align: center;">
				<i class="fa fa-circle-o-notch fa-spin fa-5x"></i>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="saveWaitDialog" data-keyboard="false" role="dialog" data-backdrop="static" aria-hidden="true">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header">
				<h4 style="text-align: center;">Save...&nbsp;&nbsp;   <i class="fa fa-circle-o-notch fa-spin fa-2x"></i></h4>		
			</div>
			
		</div>
	</div>
</div>


<div class="modal fade" id="transcriptModal" tabindex="-1" role="dialog" aria-labelledby="transcriptModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-xlg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <div class="modal-title" id="transcriptModalLabel">
        	<span>
	        	TRANSCRIPT
		        <button type="button" class="btn btn-default pull-right" id="bnTranscriptPrintView" onclick="transcriptPrintView();" style="margin-right:10px; padding-bottom:5px">Printable View</button>    
	        </span><br/>
	        <span id="transcriptModalTitle" style="font-size: small; font-color: black"></span>
        </div>
      </div>
      <div class="modal-body" id="transcriptBody">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" id="bnTranscriptComplete">Close</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="verChangesModal" tabindex="-1" role="dialog" aria-labelledby="verChangesModalLabel" aria-hidden="false"
	 data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog modal-xlg">
    <div class="modal-content">
      <div class="modal-header">
        <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>  -->
        <div class="modal-title" id="verChangesModalLabel">CLS Version Changes</div>
      </div>
      <div class="modal-body" id="verChangesBody">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success" id="bnVerChangesUpdate">Update</button>
      </div>
    </div>
  </div>
</div>

<!-- The reset dialog combines elements for three dialogs into one. -->
<div class="modal fade" id="resetModal" tabindex="-1" role="dialog" aria-labelledby="resetModalLabel" aria-hidden="false"
	 data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <div class="modal-title">
        	<span id="resetModalLabel">Warning</span>
		</div>
		<div id="resetHeader">
		<h1><span id="resetProcessHeader">Processing...</span></h1>
		<span id="resetProcessMessage">This may take 1-2 minutes...</span>
		</div>
      </div>
      <div class="modal-body">
      	<span id="resetBody">All answer choices selected will be removed and reset to the default value. Are you sure you want to reset your interview?</span>
		<span id="resetProcessSpinner" style="margin:auto; display:table;"><i class="fa fa-circle-o-notch fa-spin fa-5x"></i></span>
      </div>
      <div class="modal-footer" id="resetFooter">
        <button type="button" class="btn btn-primary" id="bnResetYes">Yes</button>
        <button type="button" class="btn btn-default" id="bnResetNo">No</button>
        <button type="button" class="btn btn-default" id="bnResetClose">Close</button>      
      </div>
    </div>
  </div>
</div>

<div id = "alert_placeholder"></div>
<div id="divSessionTimeout"></div>

<div id="divBottomFooter"></div>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="../assets/jquery-1.11.2.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="../assets/bootstrap/js/bootstrap.min.js"></script>
<script src="../assets/bootstrap/js/jquery.bsAlerts.min.js"></script>
<script src="../assets/cleditor1_4_5/jquery.cleditor.js"></script>

<script src="../assets/session.js" type="text/javascript"></script>

<script src="interview-q-eval.js" type="text/javascript"></script>
<script src="interview-c-eval.js" type="text/javascript"></script>
<script src="inerview-ac-eval.js" type="text/javascript"></script>
<script src="interview-utils.js" type="text/javascript"></script>
<script src="interview-items.js" type="text/javascript"></script>
<script src="interview-f-check.js" type="text/javascript"></script>

<script src="showClsVersionUpgrade.js" type="text/javascript"></script>
<script src="interview.js" type="text/javascript"></script>

</body>
</html>
