<!DOCTYPE html>
<%@page import="gov.dod.cls.bean.session.UserSession"%>
<html lang='en'>
<head>
	<meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
	<meta http-equiv="Pragma" content="no-cache" />
	<meta http-equiv="Expires" content="0" />
	<meta charset='utf-8'>
	<meta content='IE=Edge,chrome=1' http-equiv='X-UA-Compatible'>
	<meta content='width=device-width, initial-scale=1.0' name='viewport'>
	<title>Dashboard</title>
    <link href="../assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <link href="../assets/datatables/css/jquery.dataTables.css" rel="stylesheet">

	<link href="../assets/application.css" media="all" rel="stylesheet" type="text/css" />
	<style>
		.tableOptions {
			width:100%;
		}
		td.tableOptions {
			virtical-align: top;
		}

/* line 92, ../../app/assets/stylesheets/cls.css.scss */
.dashboard_create_btn {
  padding-right: 30px;
  padding-bottom: 12px; /* 20px; */
}

/* line 97, ../../app/assets/stylesheets/cls.css.scss */
.dashboard_doc_id {
  padding-right: 12px;
}

/* line 101, ../../app/assets/stylesheets/cls.css.scss */
.dashboard_doc_awards {
  padding-left: 4px;
  padding-right: 4px;
}

/* line 106, ../../app/assets/stylesheets/cls.css.scss */
.dashboard_doc_date {
  padding-left: 4px;
  padding-right: 16px;
}

/* line 111, ../../app/assets/stylesheets/cls.css.scss */
.dashboard_doc_desc {
  width: 50%;
  padding-left: 4px;
  padding-right: 20px;
}

/* line 117, ../../app/assets/stylesheets/cls.css.scss */
.dashboard_doc_bool {
  padding-left: 4px;
  padding-right: 4px;
}

/* line 122, ../../app/assets/stylesheets/cls.css.scss */
.dashboard_doc_complete {
  padding-left: 4px;
  padding-right: 4px;
}

/* line 127, ../../app/assets/stylesheets/cls.css.scss */
.dashboard_doc_actions {
  text-align: center;
}

/* line 131, ../../app/assets/stylesheets/cls.css.scss */
.dashboard_doc_btn {
  padding-left: 20px;
  padding-top: 4px;
  padding-bottom: 4px;
}

.dashboard_linked-award {
{
  padding-left: 4px;
  padding-top: 0px;
  padding-bottom: 4px;
}

th.dataTables_scollHead, a.dataTables_scollHead {
	white-space: nowrap;
}

	</style>
</head>

<body>

<div id="divTopBanner" style="min-height:52px;"></div>

<div data-alerts="alerts" data-titles="{'warning': '<em>Warning!</em>'}" data-ids="myid" data-fade="3000"></div>
 
<div class='container-fluid margin-top-15' id="divContainer">
	<table class="tableOptions" id="tableOptions">
		<tr>
			<td class='dashboard_create_btn'>
				<button type="button" class="btn btn-primary" onclick="newDocument('S');">New Solicitation</button>
			</td>
			<td class='dashboard_create_btn'>
				<button type="button" class="btn btn-primary" onclick="newDocument('A');">New Award</button>
			</td>
			<td class='dashboard_create_btn'>
				<button type="button" class="btn btn-primary" onclick="newDocument('O');">Order under an Existing Contract</button>
			</td>
			<td width="80%" class='dashboard_create_btn'>
				<!-- <div class='container-fluid margin-left-0 well' style="padding:10px;margin-top:0;margin-bottom:0;"> -->
					<form accept-charset="UTF-8" action="" class="form-inline" method="get" onsubmit="onSearchClick(); return false;" style="margin:0;padding:0;"> <!--   -->
						<div class="form-group" style="margin:0;padding:0;"><!-- <div class='12 margin-left-0'> -->
							<input class="form-control" id="query" name="query" placeholder="Title, Number, or Email" aria-label="Title, Number or Email" type="text" />
							<button type="button" class="btn btn-primary" onclick="onSearchClick();">Search</button>
						</div>
					</form>
				<!-- </div> -->
			</td>
		</tr>
	</table>
	<div role='tabpanel' id="divTabPanel">
		<ul class='nav nav-tabs' role='tablist'>
			<li class='active' id='inprogress-tab' role='presentation'>
				<a href="#inprogress" data-toggle="tab" role="tab">In Progress</a>
			</li>
			<li id='finalize-tab' role='presentation'>
				<a href="#finalize" data-toggle="tab" role="tab">Finalized</a>
			</li>			
		</ul>
	</div>
	<div class='tab-content' id='divTabContent'>
		<div class='tab-pane active' id='inprogress' role='tabpanel'>
			<table id="tableInProgress" class='table table-hover table-striped'> <!-- document-table table table-hover table-striped -->
				<thead>
					<tr>
						<th><a href="#" onclick="javascript:onInProgressSortClick('created_at', 0)">Create<br>Date</a>
							<span id="spanSIC0" class='i fa fa-sort'></span>
						</th>
						<th><a href="#" onclick="javascript:onInProgressSortClick('acquisition_title', 1)">Title</a>
							<span id="spanSIC1" class='i fa fa-sort'></span>
						</th>
						<th><a href="#" onclick="javascript:onInProgressSortClick('document_number', 2)">Number</a>
							<span id="spanSIC2" class='i fa fa-sort'></span>
						</th>
						<th><a href="#" onclick="javascript:onInProgressSortClick('clause_version_id', 3)">Version</a>
							<span id="spanSIC3" class='i fa fa-sort'></span>
						</th>						
						<th><a href="#" onclick="javascript:onInProgressSortClick('email', 4)">Email</a>
							<span id="spanSIC4" class='i fa fa-sort'></span>
						</th>
						<th><a href="#" onclick="javascript:onInProgressSortClick('document_type', 5)">Type</a>
							<span id="spanSIC5" class='i fa fa-sort'></span>
						</th>
						<th><a href="#" onclick="javascript:onInProgressSortClick('updated_at', 6)">Last<br>Updated</a>
							<span id="spanSIC6" class='i fa fa-sort'></span>
						</th>
						<th><span>View</span></th>
						<th><span>Actions</span></th>
					</tr>
				</thead>
				<tbody>
				</tbody>
			</table>
			<div id="divItemsPerPageInProgress" style="display:none;text-align:right;padding-right:18px;margin-top:3px;">
				Show
				<select id="selItemsPerPageInProgress" onclick="selItemsPerPageInProgress_onClick(this)" title="items per page">
				  <option value="10" selected>10</option>
				  <option value="25">25</option>
				  <option value="50">50</option>
				  <option value="100">100</option>
				</select> 
				entries
			</div>
		</div>
		
<!-- Add a Finalize Tab -->
		<div class='tab-pane' id='finalize' role='tabpanel'>
			<table id="tableFinalize" class='table table-hover table-striped'> <!-- document-table table table-hover table-striped -->
				<thead>
					<tr>
						<th><a href="#" onclick="javascript:onFinalizeSortClick('created_at', 0)">Create<br>Date</a>
							<span id="spanCIC0" class='i fa fa-sort'></span>
						</th>
						<th><a href="#" onclick="javascript:onFinalizeSortClick('acquisition_title', 1)">Title</a>
							<span id="spanCIC1" class='i fa fa-sort'></span>
						</th>
						<th><a href="#" onclick="javascript:onFinalizeSortClick('document_number', 2)">Number</a>
							<span id="spanCIC2" class='i fa fa-sort'></span>
						</th>
						<th><a href="#" onclick="javascript:onFinalizeSortClick('clause_version_id', 3)">Version</a>
							<span id="spanSIC3" class='i fa fa-sort'></span>
						</th>
						<th><a href="#" onclick="javascript:onFinalizeSortClick('email', 4)">Email</a>
							<span id="spanCIC4" class='i fa fa-sort'></span>
						</th>
						<th><a href="#" onclick="javascript:onFinalizeSortClick('document_type', 5)">Type</a>
							<span id="spanCIC5" class='i fa fa-sort'></span>
						</th>
						<th><a href="#" onclick="javascript:onFinalizeSortClick('updated_at', 6)">Last<br>Updated</a>
							<span id="spanCIC6" class='i fa fa-sort'></span>
						</th>
						<th><span>View</span></th>
						<th><span>Actions</span></th>
						<th><span id="colLinkAward">&nbsp;&nbsp;</span></th>
					</tr>
				</thead>
				<tbody>
				</tbody>
			</table>
			<div id="divItemsPerPageFinalize" style="display:none;text-align:right;padding-right:18px;margin-top:3px;">
				Show
				<select id="selItemsPerPageFinalize" onclick="selItemsPerPageFinalize_onClick(this)" title="items per page">
				  <option value="10" selected>10</option>
				  <option value="25">25</option>
				  <option value="50">50</option>
				  <option value="100">100</option>
				</select> 
				entries
			</div>
		</div>
<!-- End Finalize Tab -->
	</div>
</div>

<div class="modal fade" id="verChangesModal" tabindex="-1" role="dialog" aria-labelledby="verChangesModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-xlg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="verChangesModalLabel">Transcript</h4>
      </div>
      <div class="modal-body" id="verChangesBody">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" id="bnVerChangesCancel">Cancel</button>
        <button type="button" class="btn btn-success" id="bnVerChangesUpdate">Update</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="transcriptModal" tabindex="-1" role="dialog" aria-labelledby="transcriptModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-xlg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="transcriptModalLabel">
        	<span>
	        	TRANSCRIPT
	        	<button type="button" class="btn btn-default pull-right" id="bnTranscriptPrintView" onclick="transcriptPrintView();" style="margin-right:10px;">Printable View</button>
        	</span><br/>
	        <span id="transcriptModalTitle" style="font-size: small; font-color: black"></span>
        	
        </h4>
      </div>
      <div class="modal-body" id="transcriptBody">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" id="bnTranscriptComplete">Close</button>
      </div>
    </div>
  </div>
</div>

<div id="divSessionTimeout"></div>

<div id="divBottomFooter"></div>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="../assets/jquery-1.11.2.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="../assets/bootstrap/js/bootstrap.min.js"></script>
<script src="../assets/bootstrap/js/jquery.bsAlerts.min.js"></script>

<script src="../assets/datatables/js/jquery.dataTables.js"></script>

<script src="../assets/session.js" type="text/javascript"></script>

<script>
setSessionMessage("<%= UserSession.getSessionMessage(request) %>");
</script>
<script src="interview-f-check.js" type="text/javascript"></script>
<script src="interview-q-eval.js" type="text/javascript"></script>
<script src="interview-c-eval.js" type="text/javascript"></script>
<script src="inerview-ac-eval.js" type="text/javascript"></script>
<script src="interview-utils.js" type="text/javascript"></script>
<script src="interview-items.js" type="text/javascript"></script>

<script src="showClsVersionUpgrade.js" type="text/javascript"></script>
<script src="dashboard.js" type="text/javascript"></script>

</body>
</html>
