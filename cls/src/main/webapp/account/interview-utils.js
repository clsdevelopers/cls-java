formatDisplayText = function(text) {
	var words = text.split(' ');
	if(words.length < 1)
		return null;
	
	var inParen = false;
	var sCurrentWord = words[0];
	var sFormattedText = sCurrentWord.slice(0, 1).toUpperCase() + sCurrentWord.slice(1).toLowerCase() + ' ';

	for(var iPos = 1; iPos < words.length; iPos++ ) {
		sCurrentWord = words[iPos];
        if(sCurrentWord.charAt(0) == '(')
            inParen = true;
		if(inParen == true)
			sFormattedText += sCurrentWord.toLowerCase() + ' ';
		else
			sFormattedText += sCurrentWord.slice(0, 1).toUpperCase() + sCurrentWord.slice(1).toLowerCase() + ' ';
        if(sCurrentWord.slice(-1) == ')')
            inParen = false;
	}
	sFormattedText = sFormattedText.replace(/ FAR /i, ' FAR ');
	sFormattedText = sFormattedText.replace(/ SPS /i, ' SPS ');
	sFormattedText = sFormattedText.replace(/ACRNS/i, 'ACRNS');
	sFormattedText = sFormattedText.replace(/\(CAR\)/i, '(CAR)');
	sFormattedText = sFormattedText.replace(/\(CAS\)/i, '(CAS)');
	sFormattedText = sFormattedText.replace(/DUNS/i, 'DUNS');
	sFormattedText = sFormattedText.replace(/STANDARD PROCUREMENT SYSTEM/i, 'Standard Procurement System');
	sFormattedText = sFormattedText.replace(/CONTRACT ACTION REPORT/i, 'Contract Action Report');
	sFormattedText = sFormattedText.replace(/CONTRACT ACCOUNTING SYSTEM/i, 'Contract Accounting System');
	return sFormattedText.trim();
}

var FAR = 'FAR';
var DFAR = 'DFAR;'

function SatEval() {
	this.far300k = new ClauseItem();
	this.farMillion = new ClauseItem();
	//SAT 750k CJ-1465
	this.far750k = new ClauseItem();
	this.dfar750k = new ClauseItem();
	
	this.dfar300k = new ClauseItem();
	this.dfarMillion = new ClauseItem();
	this.prescriptionChoices = new PrescriptionChoices();
	this.regulationSet = new RegulationSet();
	
	this.farSat;
	this.dfarSat;
	
	this.prepareEval = function prepareEval(clauseSet, regulationSet, prescriptionChoices) {	
		this.far300k = clauseSet.findByName('SAT_FAR_300000');
		this.farMillion = clauseSet.findByName('SAT_FAR_1000000');
		this.dfar300k = clauseSet.findByName('SAT_DFAR_300000');
		this.dfarMillion = clauseSet.findByName('SAT_DFAR_1000000');
		//SAT 750K CJ-1465
		this.far750k = clauseSet.findByName('SAT_FAR_750000');
		this.dfar750k = clauseSet.findByName('SAT_DFAR_750000');
		
		this.prescriptionChoices = prescriptionChoices;
		this.regulationSet = regulationSet;
		this.updateSat();
		
		//to be removed after sat value clauses added to parser
		//SAT 750k CJ-1465 
		var satDefClauseIds = [this.far300k.id, this.far750k.id, this.farMillion.id, this.dfar300k.id, this.dfar750k.id, this.dfarMillion.id];
		for(iEle = 0; iEle < clauseSet.elements.length; iEle++) {
			var currentClause = clauseSet.elements[iEle];
			if(satDefClauseIds.indexOf(currentClause.id) > -1)
				clauseSet.elements.splice(iEle--,1);
		}
		//end of temp fix
	};
	
	this.updateSat = function updateSat() {
		this.farSat = 150000;
		if (this.far300k && this.far300k.isApplicable(this.prescriptionChoices, true))
			this.farSat = 300000;
		//SAT 750k CJ-1465
		if (this.far750k && this.far750k.isApplicable(this.prescriptionChoices, true))
			this.farSat = 750000;
		
		if (this.farMillion && this.farMillion.isApplicable(this.prescriptionChoices, true))
			this.farSat = 1500000;
		
		this.dfarSat = 150000;
		if (this.dfar300k && this.dfar300k.isApplicable(this.prescriptionChoices, true))
			this.dfarSat = 300000;
		//Sat 750k CJ-1465
		if (this.dfar750k && this.dfar750k.isApplicable(this.prescriptionChoices, true))
			this.dfarSat = 750000;
		if (this.dfarMillion && this.dfarMillion.isApplicable(this.prescriptionChoices, true))
			this.dfarSat = 1500000;
		};
	
	this.getSat = function getSat(regulationId) {
		this.updateSat(); //we can instead updateSat() every time a choice changes if we ensure sat value clauses evaluate first
		var regulationName = this.regulationSet.findById(regulationId).name;
		if (regulationName == FAR)
			return this.farSat;
		if (regulationName = DFAR)
			return this.dfarSat;
		return 150000;
	};
}

//sat definition clauses ===============================================================================
// CJ-1360, SAT values are added to the db from spreadsheet. DB values are passed into this method.
addSatDefintionClauses = function(clauseSet, sat_conditions, sat_300k_rule, sat_750k_rule, sat_1million_rule) {
	var conditions = '(A) PURPOSE OF PROCUREMENT IS: FOR USE IN CONTINGENCY OPERATIONS ' 
					+'(B) PURPOSE OF PROCUREMENT IS: FOR USE IN DEFENDING AGAINST OR RECOVERY FROM NUCLEAR, BIOLOGICAL, CHEMICAL, OR RADIOLOGICAL ATTACK '
					+'(C) PLACE OF ISSUANCE IS: SOLICITATION/AWARD ISSUED IN THE UNITED STATES (50 STATES PLUS DISTRICT OF COLUMBIA) '
					+'(D) PLACE OF PERFORMANCE IS: UNITED STATES (50 STATES PLUS DISTRICT OF COLUMBIA) '
					+'(E) PURPOSE OF PROCUREMENT IS: FOR USE IN HUMANITARIAN OR PEACEKEEPING OPERATIONS '
					+'(F) PLACE OF ISSUANCE IS: NOT SOLICITATION/AWARD ISSUED IN THE UNITED STATES (50 STATES PLUS DISTRICT OF COLUMBIA) '
					+'(G) PLACE OF PERFORMANCE IS: NOT UNITED STATES (50 STATES PLUS DISTRICT OF COLUMBIA)';
	
	// CJ-1360, Assign defaults. Read SAT criteria from db, if provided.
	var sat_conditions_def = conditions;
	var sat_300k_rule_def = '((A OR B) AND C AND D) OR (!C AND !D AND E)';
	//CJ-1465
	var sat_750k_rule_def = '(A OR B) AND C AND D';
	var sat_1million_rule_def = '(A OR B) AND !C AND !D';
	
	
	if ((sat_conditions != null) && (sat_conditions.length > 0)
	&& (sat_300k_rule != null) && (sat_300k_rule.length > 0)
	&& (sat_750k_rule != null) && (sat_750k_rule.length > 0)
	&& (sat_1million_rule != null) && (sat_1million_rule.length > 0)) {
		
		sat_conditions_def = sat_conditions;
		sat_300k_rule_def = sat_300k_rule;
		// CJ-1465
		sat_750k_rule_def = sat_750k_rule;
		sat_1million_rule_def = sat_1million_rule;
	}
	
	var far300k = new ClauseItem();
	//setupSatClause(far300k, 1000000, 'SAT_FAR_300000', conditions, '((A OR B) AND C AND D) OR (!C AND !D AND E)');
	setupSatClause(far300k, 1000000, 'SAT_FAR_300000', sat_conditions_def, sat_300k_rule_def);
	clauseSet.elements.push(far300k);
	
	var far750k = new ClauseItem();
	setupSatClause(far750k, 750001, 'SAT_FAR_750000', sat_conditions_def, sat_750k_rule_def);
	clauseSet.elements.push(far750k);
	
	var farMillion = new ClauseItem();
	//setupSatClause(farMillion, 1000001, 'SAT_FAR_1000000', conditions, '(A OR B) AND !C AND !D');
	setupSatClause(farMillion, 1500001, 'SAT_FAR_1000000', sat_conditions_def, sat_1million_rule);
	clauseSet.elements.push(farMillion);
	
	var dfar300k = new ClauseItem();
	//setupSatClause(dfar300k, 1000002, 'SAT_DFAR_300000', conditions, '((A OR B) AND C AND D) OR (!C AND !D AND E)');
	setupSatClause(dfar300k, 1000002, 'SAT_DFAR_300000', sat_conditions_def, sat_300k_rule_def);
	clauseSet.elements.push(dfar300k);
	
	var dfar750k = new ClauseItem();
	setupSatClause(dfar750k, 750002, 'SAT_DFAR_750000', sat_conditions_def, sat_750k_rule_def);
	clauseSet.elements.push(dfar750k);
	
	var dfarMillion = new ClauseItem();
	//setupSatClause(dfarMillion, 1000004, 'SAT_DFAR_1000000', conditions, '(A OR B) AND !C AND !D');
	setupSatClause(dfarMillion, 1500004, 'SAT_DFAR_1000000', sat_conditions_def, sat_1million_rule);
	clauseSet.elements.push(dfarMillion);
}

setupSatClause = function(satClause, id, name, conditions, rule) {
	satClause.id = id;
	satClause.name = name;
	satClause.inclusion = 'R';
	satClause.title = 'N/A';
	satClause.url = 'N/A';
	satClause.sectionId = 1;
	satClause.oSection = 1;
	satClause.effectiveDate = 'N/A';
	satClause.regulationId = 1;
	satClause.conditions =  conditions
	satClause.rule = rule;
	satClause.editable = 'N/A';
	satClause.optional = 'N/A';
	return satClause;

}

transcriptPrintView = function() {
	_transcriptWindow = window.open();
	_interview.onTranscriptBtnClick(true);
}

PanalFocusedQuestion = function(pBaseQuestionId, pFocusedQuestionId) { // CJ-1074
	this.baseQuestionId = pBaseQuestionId;
	this.focusedQuestionId = pFocusedQuestionId;
	//this.isVisible = pIsVisible;
}

LastQuestions = function() { // CJ-1074
	this.panelQuestions = null;
	this.activeQuestionPanelId = null;
	
	this.toString = function(poQGroupSet, poActiveQuestionPanelId) {
		var result = (poActiveQuestionPanelId ? poActiveQuestionPanelId.toString() : '') + ';';
		var oQGroup, oQuestion, nextQuestionGroup;
		var oQGroupElements = poQGroupSet.elements;
		//var bVisible;
		for (var iEle = 0; iEle < oQGroupElements.length; iEle++) {
			oQGroup = oQGroupElements[iEle];
			for (var iQuestion = 0; iQuestion < oQGroup.questions.length; iQuestion++) {
				oQuestion = oQGroup.questions[iQuestion];
				//bVisible = $('#' + oQuestion.getBasicDivId()).is(':visible');
				result += oQuestion.id 
					+ '\t' + (oQuestion.focusedQuestion ? oQuestion.focusedQuestion.id : '')
					//+ '\t' + (bVisible ? '1' : '0')
					+ '\n';  
			}
		}
		return result;
	}
	
	this.parseInt = function(psValue) {
		if (psValue) {
			return parseInt(psValue);
		} else
			return null;
	}
	
	this.fromString = function(psSaved) {
		this.activeQuestionPanelId = null; // CJ-1406, re-init activepanel to null after version upgrade.
		if (!psSaved)
			return;
		var aParts = psSaved.split(';');
		this.activeQuestionPanelId = aParts[0];
		if (aParts.length > 1) {
			this.panelQuestions = new Array();
			var aItems = aParts[1].split('\n');
			var sItem, aRecord, panelQuestionId, activeQuestionPanelId;
			//var bVisible;
			for (var iItem = 0; iItem < aItems.length; iItem++) {
				sItem = aItems[iItem];
				if (sItem) {
					aRecord = sItem.split('\t');
					panelQuestionId = this.parseInt(aRecord[0]);
					if (panelQuestionId) {
						//bVisible = false;
						if ((aRecord.length > 0) && aRecord[1]) {
							activeQuestionPanelId = this.parseInt(aRecord[1]);
							//if ((aRecord.length > 1) && aRecord[2])
							//	bVisible = '1' == aRecord[2];
						}
						else
							activeQuestionPanelId = null;
						
						this.panelQuestions.push(new PanalFocusedQuestion(panelQuestionId, activeQuestionPanelId)); // , bVisible
					}
				}
			}
		}
	}
	
	this.findByBaseQuestionId = function(questionId) {
		if (this.panelQuestions) {
			var oItem;
			for (var iItem = 0; iItem < this.panelQuestions.length; iItem++) {
				oItem = this.panelQuestions[iItem];
				if (oItem.baseQuestionId == questionId)
					return oItem;
			}
		}
		return null;
	}
	
}

function removeDollarFormat(value) { // CJ-1313
	if (value) {
		value = value.replace(/,/g, '');
		value = value.replace('$', '');
	}
	return value;
}