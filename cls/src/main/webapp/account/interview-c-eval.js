var OPR_IS = 0;
var OPR_INCLUDED = 1;
var OPR_LESS_THAN_OR_EQUAL_TO = 2;
var OPR_GREATER_THAN = 3;
var OPR_LESS_THAN = 4;
var OPR_GREATER_THAN_OR_EQUAL_TO = 5;
var OPR_EQUAL_TO = 6;

var GREATER_THAN_OR_EQUAL_TO = 'GREATER THAN OR EQUAL TO ';
var LESS_THAN_OR_EQUAL_TO = 'LESS THAN OR EQUAL TO ';
var GREATER_THAN = 'GREATER THAN ';
var LESS_THAN = 'LESS THAN ';
var EQUAL_TO = 'EQUAL TO ';

var SAT = 'SAT';

var SHOW_C_NO_ERROR = false;
var SHOW_C_ERROR = true;

function ClauseEvalItem() {
	this.code;
	this.name;
	this.value;
	this.regulationId;
	this.not = false;
	this.oprType = OPR_IS;
	this.oQuestion = null;
	this.oValueQuestion = null;
	this.oClause = null;

	this.setRegulationId = function setRegulationId(regulationId) {
		this.regulationId = regulationId;
	}

	this.parseLine = function parseLine(conditionLine, questionSet, clauseSet) {
		var error = '';
		conditionLine = conditionLine.trim();
		if (!conditionLine)
			return 'Empty';
		var iPos = conditionLine.indexOf(')');
		if (iPos < 2)
			return 'No identifier';
		this.code = conditionLine.substring(1, iPos);
		var rest = conditionLine.substring(iPos + 1).trim();
		var aValues = rest.split(' IS:');
		if (aValues.length != 2) {
			if (aValues.length > 2)
				return '(' + this.code + ') More than one " IS:" found';
			aValues = rest.split(' IS :');
			if (aValues.length != 2) {
				error = '(' + this.code + ') Not " IS:" syntax';
				aValues = rest.split(':');
				if (aValues.length != 2) {
					return '(' + this.code + ') No " IS:" found';
				}
			}
		}
		this.name = aValues[0].trim();
		this.value = aValues[1].trim();
		if (this.value.substr(0, 4) == 'NOT ') {
			this.value = this.value.substr(4);
			this.not = true;
		}

		if (this.value.substr(0, GREATER_THAN_OR_EQUAL_TO.length) == GREATER_THAN_OR_EQUAL_TO) {
			this.value = this.value.substr(GREATER_THAN_OR_EQUAL_TO.length);
			this.oprType = OPR_GREATER_THAN_OR_EQUAL_TO;
		} else
		if (this.value.substr(0, LESS_THAN_OR_EQUAL_TO.length) == LESS_THAN_OR_EQUAL_TO) {
			this.value = this.value.substr(LESS_THAN_OR_EQUAL_TO.length);
			this.oprType = OPR_LESS_THAN_OR_EQUAL_TO;
		} else
		if (this.value.substr(0, GREATER_THAN.length) == GREATER_THAN) {
			this.value = this.value.substr(GREATER_THAN.length);
			this.oprType = OPR_GREATER_THAN;
		} else
		if (this.value.substr(0, LESS_THAN.length) == LESS_THAN) {
			this.value = this.value.substr(LESS_THAN.length);
			this.oprType = OPR_LESS_THAN;
		} else
		if (this.value.substr(0, EQUAL_TO.length) == EQUAL_TO) {
			this.value = this.value.substr(EQUAL_TO.length);
			this.oprType = OPR_EQUAL_TO;
		} else
		if ('INCLUDED' == this.value)
			this.oprType = OPR_INCLUDED;

		if (this.oprType == OPR_INCLUDED) {
			if (this.name) {
				this.name = this.name.toUpperCase();
				this.oClause = clauseSet.findByName(this.name);
				if (this.oClause == null) {
					error += ((error == '') ? '' : '<br />') + '(' + this.code + ') Clause name not found: {' + this.name + '}';
				}
			} else {
				error += ((error == '') ? '' : '<br />') + '(' + this.code + ') Clause name is empty';
			}
		} else {
			this.oQuestion = questionSet.findByName('[' + this.name + ']');
			if (this.oQuestion == null) {
				error += ((error == '') ? '' : '; ')
					+ '(' + this.code + ') [' + this.name + ']'
					+ ' Question Name not found';
			} else
			if ((this.oprType == OPR_LESS_THAN_OR_EQUAL_TO) ||
				(this.oprType == OPR_GREATER_THAN_OR_EQUAL_TO) ||
				(this.oprType == OPR_GREATER_THAN) ||
				(this.oprType == OPR_LESS_THAN) ||
				(this.oprType == OPR_EQUAL_TO)) {
				if (this.value)
					this.value = this.value.replace(/,/g, '');
				if (! isNumber(this.value)) {
					this.oValueQuestion = questionSet.findByName('[' + this.value + ']');
					if (this.oValueQuestion == null && this.value != SAT) {
						error += ((error == '') ? '' : '; ')
							+ '(' + this.code + ') Value Question Name not found: [' + this.value + ']';
					}
				} else {
					this.value = parseInt(this.value);
				}
			} else
			if (this.value) {
				var found = false;
				var notValue = 'NOT ' + this.value;
				var debug = '(' + this.code + ') Value not found: "' + this.value + '" in ' + this.oQuestion.name + ' choices.';
				if (this.oQuestion.hasChoice(this.value, true)) // Hierarchy
					found = true;
				else if ((this.not == true) && this.oQuestion.hasChoice(notValue, true)) { // Hierarchy
					this.value = notValue;
					this.not = false;
					found = true;
				}
				else if (this.oQuestion.isNumberType()) {
					if (this.value)
						this.value = this.value.replace(/,/g, '');
					if (! isNumber(this.value)) {
						error += ((error == '') ? '' : '; ')
							+ '(' + this.code + ') Value must be a number but found: [' + this.value + ']';
					} else {
						this.oprType == OPR_EQUAL_TO;
						this.value = parseInt(this.value);
						found = true;
					}
				}
				if (found == false) {
					debug += ' Available values: <ul>';
					var choices = this.oQuestion.choices.elements;
					for (var c = 0; c < choices.length; c++) {
						var questionChoiceItem = choices[c];
						if (questionChoiceItem.choice == this.value) { // if (questionChoiceItem.choice.startsWith(this.value)) {
							found = true;
							break;
						} else if (this.not == true) {
							if (questionChoiceItem.choice == notValue) {
								this.value = notValue;
								this.not = false;
								found = true;
								break;
							}
						}
						debug += '<li>"' + questionChoiceItem.choice + '"</li>';
					}
					debug += '</ul>';
				}
				if (! found) {
					error += ((error == '') ? '' : '<br />') + debug;
				}
			}
		}
		return error;
	};

	this.evaluateAnswer = function evaluateAnswer(aSelectedClauses) {
		var result = false;
		if (this.oprType == OPR_INCLUDED) {
			// Possible problem:
			// When clauses are evaluated for answer, they don't
			// always have aSelectedClauses (oPrescriptionChoices) defined
			// prior to arriving at this point. 
			// In fact, we may need two things here:
			// 1. clearCache = true
			// 2. always use _interview.clauses
			
			if (aSelectedClauses && this.oClause) {
				if (aSelectedClauses.indexOf(this.oClause) >= 0)
					result = this.oClause.isApplicable(aSelectedClauses, null, true);
			}
			return ((this.not == true) ? (! result) : result);
		}
		if (this.oQuestion == null)
			return null; // false;

		if (this.oprType == OPR_IS) {
			result = this.oQuestion.hasValue(this.value);
			if (result == null)
				return null;
			if (this.not == true)
				return !result;
			return result;
		}

		var sAnswerValue = this.oQuestion.getNumberAnswer();
		if (sAnswerValue == null)
			return null;
		var iAnswerValue = null;
		var iTargetValue = null;
		if (isNumber(sAnswerValue)) {
			iAnswerValue = parseInt(sAnswerValue);
			if(this.value == SAT)
				iTargetValue = gSatEval.getSat(this.regulationId);
			else if (this.oValueQuestion !== null) {
				var aTargetValues = this.oValueQuestion.getAnswerValues();
				if (aTargetValues && (aTargetValues.length > 0)) {
					var sTargetValue = aTargetValues[0];
					if (isNumber(sTargetValue)) {
						iTargetValue = parseInt(sTargetValue);
					}
				}
			} else if (!isNaN(this.value))
				iTargetValue = this.value;
		}
		if ((iAnswerValue != null) && (iTargetValue != null)) {
			if (this.oprType == OPR_LESS_THAN_OR_EQUAL_TO)
				result = iAnswerValue <= iTargetValue;
			else if (this.oprType == OPR_GREATER_THAN_OR_EQUAL_TO)
				result = iAnswerValue >= iTargetValue;
			else if (this.oprType == OPR_GREATER_THAN)
				result = iAnswerValue > iTargetValue;
			else if (this.oprType == OPR_LESS_THAN)
				result = iAnswerValue < iTargetValue;
			else if (this.oprType == OPR_EQUAL_TO)
				result = iAnswerValue == iTargetValue;
		}
		if (this.not == true)
			result = !result;

		return result;
	};

	this.hasQuestionCodes = function hasQuestionCodes(aCodes) {
		if (this.oprType == OPR_INCLUDED)
			return false;
		for (var index = 0; index < aCodes.length; index++) {
			if (aCodes[index] == this.code)
				return true;
			else {
				if ((this.oprType == OPR_LESS_THAN_OR_EQUAL_TO) ||
					(this.oprType == OPR_GREATER_THAN_OR_EQUAL_TO) ||
					(this.oprType == OPR_GREATER_THAN) ||
					(this.oprType == OPR_LESS_THAN)) {
					if (! isNumber(this.value)) {
						if (aCodes[index] == this.value)
							return true;
					}
				}
			}
		}
		return false;
	};

	this.isChangeEffected = function isChangeEffected(oChangeEventItems) {
		if (this.oprType == OPR_INCLUDED) {
			if (this.oClause) {
				return oChangeEventItems.hasClause(this.oClause);
			}
			return false;
		}

		if (oChangeEventItems.hasQuestion(this.oQuestion))
			return true;

		if (this.oValueQuestion != null)
			if (oChangeEventItems.hasQuestion(this.oValueQuestion))
				return true;

		return false;
	};

	this.debugHtml = function debugHtml() {
		return '(' + (this.code ? '' : '<span class="red">') + this.code + (this.code ? '' : '</span>') + ')'
			+ ' [' + (this.name ? '' : '<span class="red">') + this.name + (this.name ? '' : '</span>') + '] '
			+ (this.not ? 'NOT ' : '')
			+ (this.oprType == OPR_LESS_THAN_OR_EQUAL_TO ? LESS_THAN_OR_EQUAL_TO : '')
			+ (this.oprType == OPR_GREATER_THAN_OR_EQUAL_TO ? GREATER_THAN_OR_EQUAL_TO : '')
			+ (this.oprType == OPR_GREATER_THAN ? GREATER_THAN : '')
			+ (this.oprType == OPR_LESS_THAN ? LESS_THAN : '')
			+ '"' + (this.value ? '' : '<span class="red">') + this.value + (this.value ? '' : '</span>') + '"';
	};

}

var QUESTON_REQUIRING_ACTIVITY = '[REQUIRING ACTIVITY]';
var _qRequiringActivity = null;
var CIVILIAN = 'CIVILIAN';

// --------------------------------------------------------------
function ClauseEvaluation() {
	this.conditions = new Array();
	this.rule = null; // 12859	(A || B) && C IF THIS CLAUSE APPLIES, USE IT INSTEAD OF FAR 52.232-23
	this.isExpressionValid = false;
	this.clause;
	this.isDFAR = false;

	this.parseError = null;

	this.parseConditions = function parseConditions(oClauseItem, questionSet, clauseSet) {	
		this.clause = oClauseItem;
		//this.isDFAR = (this.clause.regulationId == 2); // 2: DFARS
		this.isDFAR = this.clause.isDfarsActivity; // CJ-1072
		if (_qRequiringActivity == null)
			_qRequiringActivity = questionSet.findByName(QUESTON_REQUIRING_ACTIVITY);
		
		var parseError = '';
		this.conditions = new Array(); // conditions, rule
		var conditions = oClauseItem.conditions.replace(/\n/g, ' ');

		var sLine, iPos;
		var aLines = new Array();
		var aTemp = conditions.split(' (');
		for (var iLine = 0; iLine < aTemp.length; iLine++) {
			sLine = aTemp[iLine];
			if (iLine == 0) {
				aLines.push(sLine);
			} else {
				iPos = sLine.indexOf(')');
				var c = sLine.charAt(0);
				//if ((iPos < 0) || (iPos > 2) || !(/^[0-9A-Z]+$/.test(c))  ) {	// added numbers for additional conditions.
				if ((iPos < 0) || (iPos > 2) || (c < 'A') || (c > 'Z')) {
				//if ((sLine.length >= 3) && (')' != sLine.charAt(1)) && (')' != sLine.charAt(2))) {
					var index = aLines.length - 1;
					aLines[index] = aLines[index] + ' (' + sLine;
				} else 
					aLines.push(sLine);
			}
		}

		var oItem = null;
		for (var iLine = 0; iLine < aLines.length; iLine++) {
			sLine = aLines[iLine].trim();
			if (sLine) {
				iPos = sLine.indexOf(')');
				if ((iPos > 3) && (oItem != null)) {
					oItem.value += ' (' + sLine;
				} else {
					if (sLine.charAt(0) != '(')
						sLine = '(' + sLine;
					oItem = new ClauseEvalItem();
					oItem.setRegulationId(this.clause.regulationId);
					var error = oItem.parseLine(sLine, questionSet, clauseSet);
					if (error) {
						if (parseError)
							parseError += '<br />';
						parseError += error;
					}
					this.conditions.push(oItem);
				}
			}
		}

		if (oClauseItem.rule) {
			var rule = oClauseItem.rule;
			var iPos = rule.indexOf('ADDITIONAL INSTRUCTION');
			if (iPos > 0)
				rule = rule.substring(0, iPos);
			iPos = rule.indexOf('\n');
			if (iPos > 0)
				rule = rule.substring(0, iPos);
			iPos = rule.indexOf('\\n');
			if (iPos > 0)
				rule = rule.substring(0, iPos);
			iPos = rule.indexOf(' IF THIS ');
			if (iPos > 0)
				rule = rule.substring(0, iPos);
			this.rule = rule.replace(/OR/g, '||').replace(/AND/g, '&&');

			this.conditions.sort(this.itemSorting);
			var sTestRuleError = this.testRule();
			if (sTestRuleError)
				parseError += (parseError ? '<br />\n' : '') + sTestRuleError;
		} else {
			/*
			if (parseError)
				parseError += '<br />';
			parseError += 'Empty Rule';
			*/
		}

		if (parseError == '') {
			this.isExpressionValid = true;
			if (SHOW_C_NO_ERROR == true)
				logOnConsole('\n<br /><strong>Clause</strong> (id: ' + oClauseItem.id + ', Name: ' + oClauseItem.name + '): NO ERROR');
		} else {
			if (SHOW_C_ERROR == true)
				logOnConsole('\n<br /><strong>Clause</strong> (id: ' + oClauseItem.id + ', Name: ' + oClauseItem.name + '): '
						+ parseError
						+ '\n<pre>' + oClauseItem.conditions + '</pre>\n');
		}
		this.parseError = parseError;
		return parseError;
	};

	this.itemSorting = function itemSorting(a, b) {
		var sA = a.code;
		if (!sA)
			return -1;
		var sB = b.code;
		if (!sB)
			return -1;
		var iA = sA.length;
		var iB = sB.length;
		if (iA < iB)
			return 1;
		if (iB < iA)
			return -1;

		if (sA > sB)
			return 1;
		return -1;
	}

	this.isApplicable = function isApplicable(aSelectedClauses) {
		if (this.clause.isRestrictedForAward()) // CJ-606
			return false;
		
		if (this.clause.isCommercialAlwaysRequired()) // CJ-476
			return true;
		
		if (this.isExpressionValid == false)
			return false;

		if (this.rule) {
			var evalRule = this.rule;
			return this.evalRule(aSelectedClauses, evalRule);
		}
		else
			return false;
	};

	this.evalRule = function evalRule(aSelectedClauses, evalRule) {
		if (this.isExpressionValid == false)
			return false;
		
		if (this.clause.isAllowed() == false)
			return false;

		if (this.isDFAR && (_qRequiringActivity != null)) {
			var oCivilian = _qRequiringActivity.hasValue(CIVILIAN);
			if ((oCivilian != null) && (oCivilian == true))
				return false;
		}
		
		var bDebug = false;
		/*
		if ((this.clause.name == "52.219-9 (DEVIATION 2013-00014)") || (this.clause.name == "52.219-8") || (this.clause.name == "52.219-9")) { 
					bDebug = true;
					logOnConsole ("---");
					logOnConsole ("debugging  eValAnswer for " + this.clause.name);
					logOnConsole ("condition = " + evalRule);
		}
		*/
		
		var bBigOr = (evalRule.indexOf(') || (') > 0);
		var aCodes = new Array();
		try {
			for (var item = 0; item < this.conditions.length; item++) {
				var oItem = this.conditions[item];
				aCodes.push(oItem.code);
				var bValue = oItem.evaluateAnswer(aSelectedClauses);
				if (bDebug) {
					logOnConsole ("[ " + oItem.code + "] " + oItem.name + ": " + (oItem.not ? " (NOT) " : "") + oItem.value + " = " + bValue); 
				}
				if (bValue == null) {
					if (bBigOr)
						bValue = false;
					else {
						if (evalRule.indexOf('(' + oItem.code + ' || ') >= 0)
							bValue = false;
						else if (evalRule.indexOf(' || ' + oItem.code + ')') > 0)
							bValue = false;
						else if (evalRule.indexOf(' || ' + oItem.code + ' || ') > 0)
							bValue = false;
						else
							return false;
					}
				}
				var sValue = (bValue ? 'true' : 'false');
				var re = new RegExp(oItem.code, 'g');
				evalRule = evalRule.replace(re, sValue);
			}
			if (bDebug) {
				logOnConsole ("condition = " + evalRule);
			}
			var result = eval(evalRule);
			if (bDebug)
				logOnConsole ("final condition = " + result);
			if (result == true)
				return true;
			result = this.clause.getCommercialProcedureOption(false); // CJ-476
			return (CLAUSE_OPTION_REQUIRED_CD == result);
		}
		catch (error) {
			var log = '[' + this.clause.name + '] condition evaluation error: ' + error
				+ '<br />\nRule: ' + this.rule
				+ '<br />\nItems: ' + aCodes
				+ '<br />\nevalRule: ' + evalRule;
			logOnConsole(log);
			return false;
		}
	};

	this.hasQuestionCodes = function hasQuestionCodes(aCodes) {
		if (this.isExpressionValid == true)

			if (this.isDFAR && (_qRequiringActivity != null)) {
				for (var index = 0; index < aCodes.length; index++) {
					if (aCodes[index] == _qRequiringActivity)
						return true;
				}
			}
				
			for (var item = 0; item < this.conditions.length; item++) {
				var oItem = this.conditions[item];
				if (oItem.hasQuestionCodes(aCodes))
					return true;
			}
		return false;
	};

	this.isChangeEffected = function isChangeEffected(oChangeEventItems) {
		if (this.isExpressionValid == true)
			
			if (this.isDFAR && (_qRequiringActivity != null)) {
				if (oChangeEventItems.hasQuestion(_qRequiringActivity))
					return true;
			}
			
			for (var item = 0; item < this.conditions.length; item++) {
				var oItem = this.conditions[item];
				if (oItem.isChangeEffected(oChangeEventItems))
					return true;
			}
		return false;
	};

	this.testRule = function testRule() {
		if (!this.rule)
			return 'Empty';

		var aCodes = new Array();
		var evalRule = this.rule;
		var errorMessage = '';
		try {
			for (var item = 0; item < this.conditions.length; item++) {
				var oItem = this.conditions[item];
				aCodes.push(oItem.code);
				var sValue = 'true';
				var re = new RegExp(oItem.code, 'g');
				evalRule = evalRule.replace(re, sValue);
			}
			var result = eval(evalRule);
		}
		catch (error) {
			errorMessage = 'Clause Rule expession error - ' + error
				+ '<br />\nTested Rule: ' + this.rule
				+ '<br />\nItems: ' + aCodes;
		}
		return errorMessage;
	};

	this.debugConditionHtml = function debugConditionHtml() {
		var html = '';
		for (var item = 0; item < this.conditions.length; item++) {
			var oItem = this.conditions[item];
			if (item > 0)
				html += '<br/>';
			html += oItem.debugHtml();
		}
		return html;
	};

};
