var _oUserInfo = new UserInfo();

onPageBtnClick = function(page) {
	location = page;
}

onLoginResponse = function(success, userInfo) {
	if (success) {
		displaySessionMessage(userInfo.sessionMessage);
		$("#navSupport").addClass("active");
	} else
		goHome();
}

_oUserInfo.getLogon(true, onLoginResponse);
