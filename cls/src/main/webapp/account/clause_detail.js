var _oUserInfo = new UserInfo();

var _sMode = getUrlParameter('mode');
var _sId = getUrlParameter('id');
var _theToken = getUrlParameter("token");

function checkNull(sData) {
	if ((sData == null) || (sData === ""))
		return "&nbsp;";
	else
		return sData;
}

var MONTHS = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];	

longToMonthYear = function(value) {
	var date = toDate(value)
	if (date) {
		date.setDate(date.getDate() + 1);
		return MONTHS[date.getMonth()] + ' ' + date.getFullYear();
		// return date.toLocaleDateString('en-us', {month: "short", year: "numeric" });
	} else
		return '';
}

function formatClauseCondition(pCondition) {
	if (!pCondition)
		return pCondition;
	if (pCondition.indexOf('\n') > 0)
		return pCondition;
	var result = '';
	var aLines = pCondition.split('(');
	for (var iLine = 0; iLine < aLines.length; iLine++) {
		var line = aLines[iLine];
		if (line) {
			var iPos = line.indexOf(')');
			if (iPos > 2)
				result += '(' + line;
			else if (result.length == 0)
				result = '(' + line;
			else {
				var ch = line.charAt(0);
				if ((ch >= 'A') && (ch <= 'Z')) 
					result += '\n(' + line;
				else
					result += '(' + line;
			}
		}
	}
	return result;
}

displayClauseDetails = function(data) {
	var record = data.data; // jQuery.parseJSON(data.data);
	// $('#divEdit').remove();
	var htmlPres = "";
	var htmlFillins = "";
	var clauseUrl = "";

	var oPrescriptions = record.Prescriptions;
	if (oPrescriptions != null) {
		var sPresName = "";
		var sPresUrl = "";
		var sPresUId = "";
		for (var iPres = 0; iPres < oPrescriptions.length; iPres++) {
			sPresName = ((oPrescriptions[iPres].prescription_name == null)? "&nbsp;": oPrescriptions[iPres].prescription_name);
			// sPresUrl = ((oPrescriptions[iPres].prescription_url == null)? "&nbsp;": oPrescriptions[iPres].prescription_url); 
			sPresUId = ((oPrescriptions[iPres].prescription_id == null)? "&nbsp;": oPrescriptions[iPres].prescription_id);
			if ((sPresUId != "&nbsp;") && (sPresUId != ''))
				sPresName =  sPresName;
				//sPresName = '<a href="prescriptions_details.html?id=' + sPresUId + '">' + sPresName + '</a>';
			// if ((sPresUrl != "&nbsp;") && (sPresUrl != ""))
			//	sPresName = '<a href="' + sPresUrl + '" target="_blank">' + sPresName + '</a>';

			if (iPres == 0) {
				htmlPres = sPresName;
			} else {
				// htmlPres += '<br />' + sPresName;
				htmlPres += ', ' + sPresName;
			}	
		}
		htmlPres += '<br /><br />';
	}	
	
	var oFillIns = record.Clause_Fill_Ins;
	if (oFillIns != null) {
		for (var iFill = 0; iFill < oFillIns.length; iFill++) {
			sPresName = ((oFillIns[iFill].prescription_name == null)? "&nbsp;": oFillIns[iFill].prescription_name);
			sPresUrl = ((oFillIns[iFill].prescription_url == null)? "&nbsp;": oFillIns[iFill].prescription_url); 
			if ((sPresUrl != "&nbsp;") && (sPresUrl != ''))
				sPresName = '<a href="' + sPresUrl + '" target="_blank">' + sPresName + '</a>';

			if (iFill == 0) {
				htmlFillins = '<tr><th>Code</th>' +
					'<th>Type</th>' +
					'<th>Max Size</th>' +
					'<th>Group No</th>' +
					'<th>Place Holder</th>' +
					'<th>Display Rows</th>' +
					'<th>For Table</th>' +
					'<th>Heading</th>' +
					'<th>Default Data</th>';
			}	
			htmlFillins += '<tr>' +
				'<td>' + checkNull(oFillIns[iFill].fill_in_code) + '</td>' +
				'<td>' + checkNull(oFillIns[iFill].fill_in_type_name) + '</td>' +
				'<td>' + checkNull(oFillIns[iFill].fill_in_max_size) + '</td>' +
				'<td>' + checkNull(oFillIns[iFill].fill_in_group_number) + '</td>' +
				'<td>' + checkNull(oFillIns[iFill].fill_in_placeholder) + '</td>' +
				'<td>' + checkNull(oFillIns[iFill].fill_in_display_rows) + '</td>' +
				'<td>' + (oFillIns[iFill].fill_in_for_table ? 'true' : 'false') + '</td>' +
				'<td>' + checkNull(oFillIns[iFill].fill_in_heading) + '</td>' +
				'<td>' + checkNull(oFillIns[iFill].fill_in_default_data) + '</td>' +
				'</tr>';
		}
	}
	if ((record.clause_url != null) && (record.clause_url != ""))
		clauseUrl = '<a href="' + record.clause_url + '" target="_blank">' + record.clause_url + '</a>';
	else
		clauseUrl = "";
	
	$('#h3Title').html('View Clause');
		
	$('#tdClauseId').html(record.clause_id);
	$('#tdClauseName').html(record.clause_name);
	$('#tdClauseTitle').html(record.clause_title);
	$('#tdClauseSectionCode').html(record.clause_section_code + " - " + record.clause_section_header);
	// $('#tdClauseSectionHeader').html(record.clause_section_header);
	$('#tdInclusionCd').html(record.inclusion_cd + " - " + record.Inclusion_Name);
	// $('#tdInclusionName').html(record.Inclusion_Name);
	$('#tdQClauseData').html(record.clause_data);
	// $('#tdClauseURL').html(record.clause_url);
	$('#tdClauseURL').html(clauseUrl);
	$('#tdClauseVersionName').html(record.clause_version_name);
	$('#tdEffectiveDate').html(longToMonthYear(record.effective_date));
	$('#tdRegulation').html(record.regulation_name);

	var sProvisionOrClause = record.provision_or_clause;
	if ("P" == sProvisionOrClause)
		sProvisionOrClause = "Provision";
	else if ("C" == sProvisionOrClause)
		sProvisionOrClause = "Clause";

	$('#tdProvisionOrClause').html(sProvisionOrClause);
	$('#tdIsActive').html((record.is_active ? 'true' : 'false'));
	$('#tdIsEditable').html((record.is_editable ? 'true' : 'false'));
	$('#tdEditableRemarks').html(record.editable_remarks ? record.editable_remarks.replace(/\n/g, '<br />') : null);
	$('#tdCommercialStatus').html(record.commercial_status ? record.commercial_status.replace(/\n/g, '<br />') : 'N/A');
	$('#tdIsOptional').html((record.is_optional ? 'true' : 'false'));
	$('#tdIsBasicClause').html((record.is_basic_clause ? 'true' : 'false'));
	$('#tdCreatedAt').html(longToDateTimeStr(record.created_at));
	$('#tdUpdatedAt').html(longToDateTimeStr(record.updated_at));
	$('#tdClauseCondition').html(formatClauseCondition(record.clause_conditions));
	$('#tdClauseRule').html(record.clause_rule);
	$('#tdAdditionalConditions').html(record.additional_conditions);
	// $('#tdConditiontoClause').html(record.condition_to_clause);
		
	// $('#tablePrescription').html(htmlPres);
	$('#tdPrescription').html(htmlPres);
	$('#tableFillins').html(htmlFillins);
	$('#divView').show();
}

retrieveClauseDetails = function() {
	if (!_sId)
		return;
	var page = getPagePath();
	var data = {'do': 'clause-detail-print', id: _sId, mode: _sMode, token: _theToken};
	var instance = this;
	$.ajax({
		url: getAppPath() + 'page',
		data: data,
		cache: false,
		dataType: 'json',
		success: function(data) {
			var htmlRows = '';
			if (data) {
				// if ('edit' == _sMode)
				// editClauseDetails(data);
				//else
				displayClauseDetails(data);
			} else {
				alert('Unable to retrieve data.');
				//goDashboard();
			}
		},
		error: function(object, text, error) {
			alert(error);
			//goDashboard();
		}
	});
}

onLoginResponse = function(success, userInfo) {
	if (success) {
		retrieveClauseDetails();
		displaySessionMessage(userInfo.sessionMessage);
	} else
		goHome();
}

// _oUserInfo.getLogon(true, onLoginResponse);
_oUserInfo.getLogon(true, onLoginResponse, null, _theToken);