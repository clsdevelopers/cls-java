// var _oUserInfo = new UserInfo();

validateForm = function(form) {
	var oUserEmail = $('#user_email');
	if (oUserEmail.val() == "") {
		myAlert("Error", "A valid email address is required.");	
		return false;
	}
	var re = /\S+@\S+\.\S+/;
    if (!re.test(oUserEmail.val())){
    	myAlert("Error", "The email address has an invalid format.\n\n - Be sure to include an '@' in the address. \n - Also, make sure that the domain follows the '@'. For instance, '@google.mail'.");	
		return false;
	}
    
	return true;
}

function resetPassword() {
	if (!validateForm())
		return false;
	resetLogin();
}

displayBottomFooter();

function resetLogin() {
	
	$('#bnReset').prop("className", "btn btn-lg btn-primary btn-block disabled");
	
	var oEmail = $('#user_email');
	var oData = {email: oEmail.val(), pIsJasonFormat : true};
	$.ajax({      
		url: 'page?do=user-forgot-password',
		type: 'POST',
		data: oData,
		cache: false,
		dataType: 'json',
		xhrFields: { withCredentials: true },
		success: function(data) 
		{
			if (data && ('success' == data.status)) {
				myAlert("Success", "An email has been sent with instructions to reset your password.");
				$('#bnReset').prop("className", "btn btn-lg btn-primary btn-block");
				return true;
			} else {
				// data.message
				if (data && data.message)
					myAlert("Error", data.message);
				else
					myAlert("Error", "Unable to reset.");
				$('#user_email').focus();
				$('#bnReset').prop("className", "btn btn-lg btn-primary btn-block");
				return false;
			}
		},
		error: function(object, text, error) 
		{
			myAlert("Error", error);
			$('#bnReset').prop("className", "btn btn-lg btn-primary btn-block");
			return false;
		}
	});
	
	return true;
}

document.getElementById('user_email').onkeypress=function(e){
    if(e.keyCode==13){
    	resetPassword();
    }
}

//if (!(navigator.userAgent.match(/msie/i) || navigator.userAgent.match(/trident/i))) {
    $('#user_email').focus();
/*
} else {
	setTimeout(function(){
		if (!$("#user_email").is(":focus")) {
			$('#user_email').focus();
		}
	}, 5000);
*/
//}
    
    
