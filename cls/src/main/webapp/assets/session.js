var VERSION = '@VERSION@';

var _sessionMessage = null;
var _userInfoInstance = null;
var _tokenSessionExpiring = false; // CJ-1159

function getSessionMessage() {
	return _sessionMessage;
}
function setSessionMessage(msg) {
	_sessionMessage = msg;
}

var _consoleAvail = null;
function isConsoleAvail() {
	if (_consoleAvail == null) {
		_consoleAvail = (!(typeof console === "undefined" || typeof console.log === "undefined"));
	}
	return _consoleAvail;
}

logOnConsole = function(log) {
	if (isConsoleAvail()) {
		var sDate = (new Date()).toLocaleString() + ' '; // format("mm/dd/yyyy HH:MM:s:l ");
		console.log(sDate + log);
	}
}

function clearConsole() {
	if (isConsoleAvail()) {
		try {
			console.clear();
		} catch (error) {
		}
	}
}

function logConsole(log) {
	if (isConsoleAvail()) {
		console.log(log);
	}
}

function addZero(x,n) {
    if (x.toString().length < n) {
        x = "0" + x;
    }
    return x;
}

function formatTime(oDate) {
    var h = addZero(oDate.getHours(), 2);
    var m = addZero(oDate.getMinutes(), 2);
    var s = addZero(oDate.getSeconds(), 2);
    var ms = addZero(oDate.getMilliseconds(), 3);
    return h + ":" + m + ":" + s + ":" + ms + " ";
}

logOnConsole2 = function(log) {
	if (!(typeof console === "undefined" || typeof console.log === "undefined")) {
		console.log(formatTime(new Date()) + log);
	}
}

doSleep = function(timeInMilliseconds) {
	try
	{
	  java.lang.Thread.sleep(timeInMilliseconds);
	}
	catch (e)
	{
	  /*
	   * This will happen if the sleep is woken up - you might want to check
	   * if enough time has passed and sleep again if not - depending on how
	   * important the sleep time is to you.
	   */
	}
}

logAjaxErrorOnConsole = function(object, text, error) {
	var sLog = 'Ajax Error';
	if (object) {
		sLog += '\n  status: ' + object.statusText
			  + '\n  responseText: ' + object.responseText
	}
	sLog += '\n  text: ' + text
		  + '\n  error: ' + error
	logOnConsole(sLog);
}

if (typeof String.prototype.startsWith != 'function') {
	String.prototype.startsWith = function (str){
		return this.indexOf(str) === 0;
	};
}

String.prototype.capitalize = function(lower) {
	return (lower ? this.toLowerCase() : this).replace(/(?:^|\s)\S/g, function(a) { return a.toUpperCase(); });
};

String.prototype.toCamelCase = function() {
	
	var tmpName = this;
	if ((this.toUpperCase().indexOf ('MC') == 0) && (this.length > 2)) { 
	    tmpName = 'Mc' + this[2].toUpperCase() + this.slice(3);
	}
		
    return tmpName.replace(/^([a-z])|([\s-_'])(\w)/g, function(match, p1, p2, p3, offset) {
    	var returnValue;
        if (p2) returnValue = p2;
        if (p3) return (returnValue ? returnValue + p3.toUpperCase(): p3.toUpperCase());

        return p1.toUpperCase();
    });
};


function isNumber(n) {
  return !isNaN(parseFloat(n)) && isFinite(n);
}

UserInfo = function() {
	_userInfoInstance = this;
	
	this.id = null;
	this.email = null;
	this.first_name = null;
	this.last_name = null;
	this.current_logon_at = null;
	this.reset_password_sent_at = null;
	this.time_zone = null;
	this.user_timezone_offset = 0; // CJ-361
	this.org_id = null;
	this.orgName = null;

	this.isSuperUser = false;
	this.isSubsuperUser = false;
	this.isAgencyReviewer = false;
	this.isReviewer = false;
	this.isRegularUser = false;
	
	this.isToken = false;
	this.token = null;
	this.documentId = null;
	this.expiresAt = null;
	this.applicationId = null;

	this.sessionMessage = null;

	// CJ-573
	this.serverLastAccessedTime = null;
	this.serverMaxInactiveInterval = null;
	this.serverSessionTimeout = null;
	this.lastRefreshed = null;
	this.timeoutInstance = null;
	this.timeToExpire = null;
	this.serverTime = null;

	this.onSessionTimeoutCallback = null;
	this.inRefreshTimeCall = false; // CJ-696	
		
	this.isLogin = function isLogin() {
		return (this.id ? true : false);
	};
	this.clear = function clear() {
		this.id = null;
		this.email = null;
		this.first_name = null;
		this.last_name = null;
		this.current_logon_at = null;
		this.reset_password_sent_at = null;
		this.org_id = null;
		this.orgName = null;
		this.isSuperUser = false;
		this.isSubsuperUser = false;
		this.isAgencyReviewer = false;
		this.isReviewer = false;
		this.isRegularUser = false;
	};

	this.loadByJson = function loadByJson(data) {
		this.clear();
		if (data.message)
			this.sessionMessage = data.message;
		else
			this.sessionMessage = _sessionMessage;
		if ('success' == data.status) {
			var oData = data.data;
			if (oData) {
				this.id = oData.id;
				this.email = oData.email;
				this.first_name = oData.first_name;
				this.last_name = oData.last_name;
				this.current_logon_at = oData.current_logon_at;
				this.reset_password_sent_at = oData.reset_password_sent_at;
				this.time_zone = oData.time_zone;
				this.org_id = oData.org_id;
				this.orgName = oData.orgName;
				this.isSuperUser = oData.isSuperUser;
				this.isSubsuperUser = oData.isSubsuperUser;
				this.isAgencyReviewer = oData.isAgencyReviewer;
				this.isReviewer = oData.isReviewer;
				this.isRegularUser = oData.isRegularUser;
				this.user_timezone_offset = oData.user_timezone_offset;
				
			} else if (data.token) {
				oData = data.token;
				this.isToken = true;
				this.token = oData.launch_token;
				this.documentId = oData.document_id;
				this.expiresAt = oData.expires_at;
				this.applicationId = oData.application_id;
				this.orgName = oData.orgName;
			} else {
				location = data.message;
			}
			this.loadSessionJson(data, true);
			return true;
		} else {
			return false;
		}
	};
	
	this.getSessionDuration = function getSessionDuration(lDiff) { // CJ-573
		if (!lDiff)
			lDiff = this.serverSessionTimeout.getTime() - this.lastRefreshed.getTime(); // lastRefreshed

		lDiff = Math.ceil(lDiff / 1000);
		var seconds = Math.floor(lDiff % 60);
		lDiff = lDiff / 60; 
		var minutes = Math.floor(lDiff % 60); // ceil
		lDiff = lDiff / 60; 
		var hours = Math.floor(lDiff % 24);

		var sDuration = '';
		if (hours > 0)
			sDuration = hours + ' hour' + ((hours > 1) ? 's' : '');
		if (minutes > 0)
			sDuration += ' ' + minutes + ' minute' + ((minutes > 1) ? 's' : '');
		if (seconds > 0)
			sDuration += ' ' + seconds + ' second' + ((seconds > 1) ? 's' : '');
		
		return sDuration;
	}
	
	this.getTimeLeft = function getTimeLeft() { // CJ-634
		if (!this.timeToExpire)
			return '';

		var dNow = new Date();
		var lDiff = this.timeToExpire.getTime() - dNow.getTime();
		return lDiff;
	}
	
	this.isValidTimeLeft = function isValidTimeLeft(lDiff) {
		return (lDiff <= this.getNoticeTimeDelay());
	}
	
	this.loadSessionJson = function loadSessionJson(json, pbSetTimeout) { // CJ-573
		if (json.user_session) {
			var oData = json.user_session;
			if (!oData.sessionTimeout) { // CJ-649
				return false;
			}

			this.lastRefreshed = new Date();

			var lastSessionTimeout = this.serverSessionTimeout;

			this.serverLastAccessedTime = new Date(oData.lastAccessedTime);
			this.serverMaxInactiveInterval = oData.maxInactiveInterval;
			this.serverSessionTimeout = new Date(oData.sessionTimeout);
			this.serverTime = new Date(oData.serverTime);

			var iRemainedTime = this.serverSessionTimeout.getTime() - this.serverTime.getTime(); // lastRefreshed
			if (iRemainedTime < 0)
				iRemainedTime = 0;
			this.timeToExpire = new Date(this.lastRefreshed.getTime() + iRemainedTime);

			if (lastSessionTimeout == null) {
				logOnConsole('    lastAccessedTime: ' + this.serverLastAccessedTime);
				logOnConsole(' maxInactiveInterval: ' + this.serverMaxInactiveInterval);
			}
			//logOnConsole('serverSessionTimeout: ' + this.serverSessionTimeout);
			//logOnConsole('   WS sessionTimeout: ' + this.timeToExpire);
			logOnConsole('time remains in msec: ' + iRemainedTime);

			if (!_tokenSessionExpiring) { // CJ-1259
				if (pbSetTimeout && (this.onSessionTimeoutCallback != null)) {
					this.setTimeoutInstance(this.onSessionTimeoutCallback, (iRemainedTime - this.getNoticeTimeDelay()));
				} else if (!pbSetTimeout) {
					this.clearTimeoutInstance();
				}
	
				logOnConsole('session expires in ' + this.getSessionDuration(iRemainedTime));
			}
			return true;
		} else
			return false;
	}

	this.clearTimeoutInstance = function clearTimeoutInstance() {
		if (this.timeoutInstance != null) {
			window.clearTimeout(this.timeoutInstance);
			this.timeoutInstance = null;
			logOnConsole('clearTimeoutInstance()');
		}
	}
	
	this.setTimeoutInstance = function setTimeoutInstance(callback, timeDelay) {
		this.clearTimeoutInstance();
		if (!timeDelay) {
			timeDelay = this.getTimeLeft();
		}
		if (timeDelay <= 0)
			timeDelay = 100;
		this.timeoutInstance = window.setTimeout(callback, timeDelay);
		logOnConsole('setTimeoutInstance(' + timeDelay + ')');
	}
	
	this.getNoticeTimeDelay = function getNoticeTimeDelay() {
		var iNoticeTimeDelay = 1000 * 60 * 10; // s mins // Changed from 2 min to 10 min
		return iNoticeTimeDelay;
	}
	
	this.fullName = function getFullName() {
		return this.first_name.toCamelCase() + ' ' + this.last_name.toCamelCase();
	};
	
	this.getLogon = function getLogon(required, callback, noExtraOptions, token, psDivSessionTimeout) {
		if (psDivSessionTimeout)
			this.prepareSessionTimeout(psDivSessionTimeout, (token ? true : false));
		var instance = this;
		var page = getPagePath();
		var sUrl = getAppPath() + 'page?do=session-getLogin&pg=' + page + (token ? '&token=' + token : '');
		var instance = this;
		if (!noExtraOptions)
			noExtraOptions = false;
		$.ajax({
			url: sUrl,
			cache: false,
			dataType: 'json',
			xhrFields: { withCredentials: true },
			success: function(data) {
				if (instance.loadByJson(data)) {
					displayTopBanner(instance, noExtraOptions);
					displayBottomFooter(instance);
					callback(true, instance);
					if (instance.getCookieForTokenSessionExpiring()) { // CJ-1259
						_tokenSessionExpiring = true;
						_sessionTimeoutInterval = window.setInterval(onSessionTimeoutInterval, 1000);
					}
				} else {
					if (required)
						goHome();
					else { 
						callback(false, instance);
					}
				}
			},
			error: function(object, text, error) {
				logAjaxErrorOnConsole(object, text, error);
			}
		});
	};

	this.canAccessAdminOptions = function canAccessAdminOptions() {
		return (this.isSuperUser || this.isSubsuperUser);
	};
	
	this.prepareSessionTimeout = function prepareSessionTimeout(psDivSessionTimeout, isToken) {
		var html = '<div class="modal fade" data-keyboard="false" data-backdrop="static" id="sessionTimeoutModal" tabindex="-1" role="dialog" ' +
			' aria-hidden="true" labelledby="sessionTimeoutLabel"' +
			// (isToken ? ' aria-hidden="true"' : ' aria-hidden="false" data-keyboard="false" data-backdrop="static"') +
		 	'> ' +
		  '<div class="modal-dialog">' +
		    '<div class="modal-content">' +
		      '<div class="modal-header">' +		        
		        '<h2 class="modal-title" style="font-weight: bold;" id="sessionTimeoutLabel">Your session is about to expire</h2> ' +
		      '</div> ' +
		      '<div class="modal-body" id="sessionTimeoutBody"> ' +
		      	'For your security, this online session will end ' + (isToken ? '' : 'due to inactivity ') +
		      	'in <span id="sessionTimeoutLeft">2 minutes</span>. ' +
		      	(isToken ? 'Please exit your browser. Or, select the \'Continue\' button to interview until the session expires.'
		      			: ' If you want to extend your session, please select the \'Continue\' button. To end your session, click the \'Log Out\' button.') // CJ-677 // CJ-634
		      	 +
		      '</div>' +
		      '<div class="modal-footer">';
		
		/*
		if (psSaveDataProcedureName)
	        html +=
		        '<button type="button" class="btn btn-success" id="sessionTimeoutSaveData" style="margin-right:15px" onclick="' +
		        psSaveDataProcedureName + '()">Save</button>';
		*/
		if (!isToken)
	        html +=
		        // 508 Compliance - style="margin-right:15px;" -> "margin-right:1%;"
		        '<button type="button" class="btn btn-success" id="sessionTimeoutContinue" style="margin-right:1%;" onclick="continueSession()">Continue</button>' +
		        '<button type="button" class="btn btn-primary" id="sessionTimeoutLogout" onclick="closeSession()">Log Out</button>'
	        	;
		else // CJ-1159
			html += '<button type="button" class="btn btn-primary" id="sessionTokenTimeoutContinue" onclick="continueTokenSession()">Continue</button>';
        html +=
		        // '<button type="button" class="btn btn-primary" id="sessionTimeoutLogout" onclick="closeSession()">' + (isToken ? 'Close' : 'Log Out') + '</button>' +
		      '</div>' +
		    '</div>' +
		  '</div>' +
		'</div>';
		$('#' + psDivSessionTimeout).replaceWith(html);
		this.onSessionTimeoutCallback = onSessionTimeOut; // CJ-573
	}
	
	this.timelyRefreshTimeout = function timelyRefreshTimeout() { // CJ-696
		if (this.token) {
			// logOnConsole('timelyRefreshTimeout() this.token');
			return;
		}
		if (this.inRefreshTimeCall == true) {
			// logOnConsole('timelyRefreshTimeout() this.inRefreshTimeCall');
			return;
		}
		if (this.lastRefreshed != null) {
			var dNow = new Date();
			var iDiff = dNow - this.lastRefreshed;
			if (iDiff < (1000 * 10)) { // check if less than 10 seconds
				// logOnConsole('timelyRefreshTimeout() less than 10 seconds');
				return;
			}
		}
		this.refreshTimeout(null);
		// logOnConsole('timelyRefreshTimeout() called');
	};
	
	this.refreshTimeout = function refreshTimeout(callback) {
		this.inRefreshTimeCall = true; // CJ-696
		var instance = this;
		var page = getPagePath();
		var sUrl = getAppPath() + 'page?do=session-getLogin&pg=' + page + (this.token ? '&token=' + this.token : '');
		var instance = this;
		$.ajax({
			url: sUrl,
			cache: false,
			dataType: 'json',
			xhrFields: { withCredentials: true },
			success: function(data) {
				instance.inRefreshTimeCall = false; // CJ-696
				if ((data.status == 'success') && instance.loadSessionJson(data, true)) {
					//logOnConsole('refreshTimeout() success');
					if (callback)
						callback(true, instance);
				} else {
					//logOnConsole('refreshTimeout() failed');
					if (callback)
						callback(false, instance);
					else 
						goHome();
				}
			},
			error: function(object, text, error) {
				instance.inRefreshTimeCall = false; // CJ-696
				logAjaxErrorOnConsole(object, text, error);
				if (callback)
					callback(false, instance);
			}
		});
	}

	this.continueSession = function continueSession(callback) {
		var instance = this;
		var page = getPagePath();
		var sUrl = getAppPath() + 'page?do=session-continueSession&pg=' + page + (this.token ? '&token=' + this.token : '');
		//var sUrl = getAppPath() + 'page?do=session-getLogin&pg=' + page + (token ? '&token=' + token : '');
		var instance = this;
		$.ajax({
			url: sUrl,
			cache: false,
			dataType: 'json',
			xhrFields: { withCredentials: true },
			success: function(data) {
				if ((data.status == 'success') && instance.loadSessionJson(data, false)) {
					setTimeout(function() {
						instance.refreshTimeout(callback); 
					}, 100);
				} else {
					if (callback)
						callback(false, instance);
					else 
						goHome();
				}
			},
			error: function(object, text, error) {
				logAjaxErrorOnConsole(object, text, error);
				if (callback)
					callback(false, instance);
			}
		});
	}
	
	this.setCookieForTokenSessionExpiring = function() { // CJ-1259
		if (this.token && this.timeToExpire) {
		    var expires = "expires=" + this.timeToExpire.toUTCString();
		    document.cookie = this.token + "=" + (_tokenSessionExpiring ? "1" : "0") + "; " + expires;
		    return true;
		} else {
			return false;
		}
	}
	
	this.getCookieForTokenSessionExpiring = function() { // CJ-1259
		if (this.token) {
			var sName = this.token + "=";
		    var aItems = document.cookie.split(';');
		    for (var i = 0; i < aItems.length; i++) {
		        var sItem = aItems[i];
		        while (sItem.charAt(0) == ' ') {
		            sItem = sItem.substring(1);
		        }
		        if (sItem.indexOf(sName) == 0) {
		        	var sValue = sItem.substring(sName.length, sItem.length);
		            return (sValue == "1");
		        }
		    }
		}
	    return false;
	}
	
}

/* 
 * Session timeout related functions
 */
var _sessionTimeoutInterval = null;

function onSessionTimeOut() { // CJ-573
	if (!_tokenSessionExpiring) { // CJ-1259
		if (_userInfoInstance.token)
			$('#sessionTimeoutLogout').hide();
		else
			$('#sessionTimeoutLogout').html('Log Out'); // _userInfoInstance.token ? 'Close' : 'Log Out');
		$('#sessionTimeoutLabel').html('Your session is about to expire');
		
		var sText = 'For your security, this online session will end ' +
			(_userInfoInstance.token ? '' : 'due to inactivity ') +
			'in <span id="sessionTimeoutLeft">2 minutes</span>. ';
		if (_userInfoInstance.token) {
			sText += 'Please exit your browser. Or, select the \'Continue\' button to interview until the session expires.'; // CJ-1159 // CJ-634
			$('#sessionTimeoutContinue').hide();
		}
		else {
			sText += 'If you want to extend your session, please select the \'Continue\' button. To end your session, click the \'Log Out\' button.';
			$('#sessionTimeoutContinue').show();
		}
		$('#sessionTimeoutBody').html(sText);
		
		$('#sessionTimeoutLeft').html(_userInfoInstance.getSessionDuration(_userInfoInstance.getTimeLeft()));
		$('#sessionTimeoutModal').modal('show');
	}

	if (_sessionTimeoutInterval != null) {
		window.clearInterval(_sessionTimeoutInterval);
		_sessionTimeoutInterval = null;
	}
	_sessionTimeoutInterval = window.setInterval(onSessionTimeoutInterval, 1000); // CJ-1159 changed from this.onSessionTimeoutInterval

	if (!_tokenSessionExpiring) { // CJ-1259
		$("#sessionTimeoutModal").on('hidden.bs.modal', function () {
			if ((!_userInfoInstance.token) && (_sessionTimeoutInterval != null)) { // CJ-1159 added (!_userInfoInstance.token)
				window.clearInterval(_sessionTimeoutInterval);
				_sessionTimeoutInterval = null;
				_userInfoInstance.setTimeoutInstance(displaySessonEnded);
			}
		});
	}
}
//_userInfoInstance.onSessionTimeoutCallback = onSessionTimeOut; // CJ-573

function displaySessionExpired() {
	if (!(typeof disableAllOptionsForExpiredSession === "undefined")) {
		disableAllOptionsForExpiredSession();
	}
	
	$('#sessionTimeoutContinue').hide();
	$('#sessionTimeoutLabel').html('Your session has expired'); // CJ-677
	var sMessage = 'For your security, this online session has ended'; // CJ-677 // CJ-573 & CJ-634
	if (_userInfoInstance.token)
		sMessage += '. Please exit your browser.';
	else
		sMessage += ' due to inactivity. Select the \'Logout\' button to proceed.'; // CJ-677
	$('#sessionTimeoutBody').html(sMessage);

	if (_userInfoInstance.token) { // CJ-1159
		if (_sessionTimeoutInterval != null) {
			window.clearInterval(_sessionTimeoutInterval);
			_sessionTimeoutInterval = null;
		}
		$('#sessionTokenTimeoutContinue').hide();
		// _userInfoInstance.setTimeoutInstance(displaySessonEnded);
	}
	$('#sessionTimeoutModal').modal('show'); // CJ-1195
}

function onSessionTimeoutInterval() {
	var iTimeLeft = _userInfoInstance.getTimeLeft();
	if (iTimeLeft && (iTimeLeft > 0)) {
		if (_tokenSessionExpiring) { // CJ-1159
			var oSpanAutoSaveDisplay = $('#spanAutoSaveDisplay'); 
			oSpanAutoSaveDisplay.html("Session ends in " + _userInfoInstance.getSessionDuration(iTimeLeft));
			oSpanAutoSaveDisplay.show();
		} else {
			if (true) //  (_userInfoInstance.isValidTimeLeft(iTimeLeft))
				$('#sessionTimeoutLeft').html(_userInfoInstance.getSessionDuration(iTimeLeft));
			else {
				window.clearInterval(_sessionTimeoutInterval);
				_sessionTimeoutInterval = null;
			}
		}
	} else {
		if (_tokenSessionExpiring) { // CJ-1159
			$('#spanAutoSaveDisplay').html("");
		}
		displaySessonEnded(_tokenSessionExpiring); // false changed for CJ-1159
		/*
		window.clearInterval(_sessionTimeoutInterval);
		_sessionTimeoutInterval = null;
		displaySessionExpired();
		*/
	}
}

function displaySessonEnded(showModal) {
	if (_sessionTimeoutInterval != null) {
		window.clearInterval(_sessionTimeoutInterval);
		_sessionTimeoutInterval = null;
	}
	displaySessionExpired();
	if (showModal) {
		$('#sessionTimeoutModal').modal('show');
	}
}

function closeSession() {
	window.clearInterval(_sessionTimeoutInterval);
	_sessionTimeoutInterval = null;
	displaySessionExpired();
	window.onbeforeunload = null;
	if (_userInfoInstance.token)
		closeWindows(); // window.close(); // CJ-634
	else
		logoff();
}

function continueSession() {
	window.clearInterval(_sessionTimeoutInterval);
	_sessionTimeoutInterval = null;
	_userInfoInstance.continueSession(continueSessionResponse);
}

function continueTokenSession() { // CJ-1159
	_tokenSessionExpiring = true;
	_userInfoInstance.setCookieForTokenSessionExpiring(); // CJ-1259
	$('#sessionTimeoutModal').modal('hide');
	if (_sessionTimeoutInterval == null) {
		_sessionTimeoutInterval = window.setInterval(onSessionTimeoutInterval, 1000);
	}
}

function continueSessionResponse(pbSuccess) {
	if (pbSuccess) {
		$('#sessionTimeoutModal').modal('hide');
	} else {
		displaySessionExpired();
	}
}
// end of Session timeout related functions --------------------------------------------

getDocumentTypeName = function(docType) {
	if ('S' == docType)
		return 'Solicitation';
	if ('A' == docType)
		return 'Award';
	if ('O' == docType)
		return 'Order';
	return docType;
}

getAppPath = function() {
	var aPath = window.location.pathname.split('/');
	if (aPath.indexOf('cls') >= 0)
		return '/' + aPath[1] + '/';
	else
		return '/';
}

getPagePath = function() {
	var pathname = window.location.pathname;
	var aPath = window.location.pathname.split('/');
	var result = null;
	for (var pos = 2; pos < aPath.length; pos++) {
		if (result == null)
			result = aPath[pos];
		else
			result += '/' + aPath[pos];
	}
	return result;
}

goHome = function() {
	location = getAppPath();
}

goDashboard = function() {
	location = getAppPath() + '/account/dashboard.jsp';
}

logoff = function() {
	var sAppPath = getAppPath(); 
	$.ajax({
		url: sAppPath + 'page?do=session-logout',
		cache: false,
		dataType: 'json',
		xhrFields: { withCredentials: true },
		success: function(data) {
			if (data && ('success' == data.status)) {
				location = data.message;
			} else
				goHome();
		},
		error: function(object, text, error) {
			alert(error);
		}
	});
}

displaySessionMessage = function(sessionMessage) {
	if (sessionMessage)
		$(document).trigger("add-alerts", 
		    [{ 'message': sessionMessage, 'priority': 'warning' }]
		);
}

clearSessionMessage = function() {
	$(document).trigger("clear-alerts");
}

CustomFixLowercase = function(pOrgName) { 
	var aSpecialWord = ["Usda","U.s.", "U.n.", "Nasa"];

	// CJ-174, replace any acronyms in parenthesis 
	pOrgName = CapitalizeParenthesisData (pOrgName);
	
	var aPrepositions = [" And ", " Of ", " On ", " For ", " At ", " Near ", " In ", " Between ", " By ", " Except ", " From", " To ", " Under ", " The "];
	var sFixedOrgName = pOrgName;
	for (var index = 0; index < aSpecialWord.length; index++) {
		var sRegExp = new RegExp(aSpecialWord[index], 'g');
		sFixedOrgName = sFixedOrgName.replace(sRegExp, aSpecialWord[index].toUpperCase());
	}
	for (var index = 0; index < aPrepositions.length; index++) {
		var sRegExp = new RegExp(aPrepositions[index], 'g');
		sFixedOrgName = sFixedOrgName.replace(sRegExp, aPrepositions[index].toLowerCase());
	}
	return sFixedOrgName;
}

// CJ-174, find any acronyms and upper case them
CapitalizeParenthesisData = function(pOrgName) {
	var ipos1 = pOrgName.indexOf ("(");
	if (ipos1 < 0)
		return pOrgName;
	
	ipos2 = pOrgName.indexOf (")");
	
	var pTmpName = pOrgName.substring (ipos1+1, ipos2);
	
	ipos1 = pTmpName.indexOf (" ");
	if (ipos1 > 0)
	{
		// Just replace first letter
		// Ex, (air Force) => (Air Force)
		pTmpName = pTmpName.substring (0, 1);
		pOrgName = pOrgName.replace (('(' + pTmpName), ('(' + pTmpName.toUpperCase()));	
	}
	else
		pOrgName = pOrgName.replace (('(' + pTmpName + ')'), ('(' + pTmpName.toUpperCase() + ')'));

	
	// Run again, in case there are two parens in the name.
	// Ex, 'AIR NATIONAL GUARD UNITS (MOBILIZATION) (TITLE 5)'
	ipos1 = pOrgName.indexOf ("(", ipos2);
	if (ipos1 < 0)
		return pOrgName;
	
	ipos2 = pOrgName.indexOf (")", ipos1);
	
	pTmpName = pOrgName.substring (ipos1+1, ipos2);
	
	ipos1 = pTmpName.indexOf (" ");
	if (ipos1 > 0)
	{
		pTmpName = pTmpName.substring (0, 1);
		pOrgName = pOrgName.replace (('(' + pTmpName), ('(' + pTmpName.toUpperCase()));	
	}
	else
		pOrgName = pOrgName.replace (('(' + pTmpName + ')'), ('(' + pTmpName.toUpperCase() + ')'));
	
	return pOrgName;
	
}

displayTopBanner = function(userInfo, noExtraOptions) {
	var oDivTopBanner = $('#divTopBanner');
	if (!oDivTopBanner)
		return;

	var path = getAppPath();
	var html = ''; 
	var sOrgName;
	if (userInfo.orgName) {
		//sOrgName = userInfo.orgName.capitalize(true);
		//sOrgName = CustomFixLowercase(sOrgName);
		
		sOrgName = userInfo.orgName;
	} else {
		sOrgName = '<organization>';
	}
	if (userInfo.isToken == true) {
		noExtraOptions = true;
		html = 
			'<div class="navbar navbar-default navbar-fixed-top" role="navigation" id="divTopNavBar">' +
				'<div class="container-fluid">' +
					'<div class="navbar-header">' +
						'<span class="navbar-brand">Clause Logic Service</span>' +
					'</div>' +
					'<div class="navbar-collapse collapse">' +
						'<ul class="nav navbar-nav">' +
						'<li><a href="' + path + 'account/clauses_print.jsp?token=' + userInfo.token + '">Print Metadata</a></li>' + 
						'</ul>' +
						'<ul class="nav navbar-nav navbar-right">' +
							'<li class="nav navbar-text"><span id="spanUserOrgName">' + sOrgName + '</span></li>' +
							(userInfo.expiresAt
									? '<li class="nav navbar-text">Session ends at ' + longToDateTimeStr(userInfo.expiresAt) + '</li>'
									: '') +
						'</ul>' +
					'</div>' +
				'</div>' +
			'</div>';
	} else {
		
		html = 
			'<div class="navbar navbar-default navbar-fixed-top" role="navigation" id="divTopNavBar">' +
				'<div class="container-fluid">' +
					'<div class="navbar-header">' +
						'<button class="navbar-toggle" data-target=".navbar-collapse" data-toggle="collapse" type="button">' +
							'<span class="sr-only">Toggle navigation</span>' +
							'<span class="icon-bar"></span>' +
						'</button>' +
						'<span class="navbar-brand">Clause Logic Service</span>' +
					'</div>' +
					'<div class="navbar-collapse collapse">' +
						'<ul class="nav navbar-nav">' +
							'<li id="navDashboard"><a href="' + path + 'account/dashboard.jsp">Dashboard</a></li>' +
							'<li id="navSettings"><a href="' + path + 'account/settings.html">Settings</a></li>' +
							// '<li id="navSupport"><a href="' + path + 'account/support.html">Support</a></li>' +
							'<li><a href="' + path + 'account/clauses_print.jsp">Print Metadata</a></li>' + 
						'</ul>' +
						'<ul class="nav navbar-nav navbar-right">' +
							'<li class="nav navbar-text"><span id="spanUserName">' + userInfo.fullName() + '</span></li>' +
							'<li class="nav navbar-text"><span id="spanUserOrgName">' + sOrgName + '</span></li>' +
							'<li><a href="' + path + 'logout.jsp">Logoff</a></li>' + // '<li><a href="javascript:void(logoff())">Logoff</a></li>' +
						'</ul>' +
					'</div>' +
				'</div>' +
			'</div>';
	}
	if ((!noExtraOptions) && userInfo.canAccessAdminOptions()) {
		var sOptions =
			(userInfo.isSuperUser
				?	'<li><a href="' + path + 'admin/audit_events.html">Audit Events</a></li>' +
					// '<li><a href="' + path + 'admin/documents.html">Documents</a></li>' +
					'<li><a href="' + path + 'admin/metadata/questions.html">Metadata</a></li>'
				: '') +
			'<li><a href="' + path + 'oauth2/applications.html">Oauth Permissions</a></li>' +
			(userInfo.isSuperUser
				?	'<li><a href="' + path + 'admin/orgs.html">Organizations</a></li>' +
					'<li><a href="' + path + 'admin/reports.html">Reports</a></li>' +
					'<li><a href="' + path + 'admin/roles.html">Roles</a></li>'
				: '') +
			'<li><a href="' + path + 'admin/users.html">Users</a></li>';
			
		html += // (userInfo.isSuperUser || userInfo.isSubsuperUser)
			'<div class="container" id="divAdminOptions">' +
				'<div class="subnav subnav-fixed">' +
					'<ul class="nav nav-pills">' +
					sOptions
					'</ul>' +
				'</div>' +
			'</div>';
	}
	oDivTopBanner.replaceWith(html); // replaceWith(html);
	oDivTopBanner.show();
}

displayBottomFooter = function(userInfo) {
	var oDivBottomFooter = $('#divBottomFooter');
	if (!oDivBottomFooter)
		return;
	
	var pageLinks = '<a href="'+ getAppPath() + 'index.html" title="Go to the home page">Home</a>' // CJ-1300
		+ '<a href="'+ getAppPath() + 'sitemap.jsp">Sitemap</a>'
		+ '<a href="'+ getAppPath() + 'privacypolicy.jsp" title="Privacy Policy for Clause Logic Service">Privacy</a>';
	//pageLinks += '<span class="cls-footer-spacer"></span><a href="'+ getAppPath() + 'privacypolicy.jsp">Privacy</a>';
	
	if ((userInfo != null) && (userInfo.isToken == true))
		pageLinks = "";
	
	/*
	var content = '<div class="text-left" style="margin-left:2%;display:inline;">' + 
		//'<a href="'+ page + '">Sitemap</a></div>' + 
		pageLinks pageLinks/div>' + 
		'<div class="text-center" style="margin-left:5%;margin-right:2%;display:inline;">' + 
		'&copy; 2015 United States Government - All rights reserved.' +
		'<span class="cls-footer-spacer"></span> Clause Logic Service <em>(ver ' + VERSION + ')</em></div>';
	*/
	
	var content = '<table><tr>'
		+ '<td width="10%">' + pageLinks + '</td>'
		+ '<td width="30%" style="padding-left:20px;padding-right:10px;">'
			+ '&copy; 2016 United States Government - All rights reserved.'
		+ '</td>'
		+ '<td width="60%" style="text-align:right;padding-right:10px;">'
			+ 'Clause Logic Service <em>(ver ' + VERSION + ')</em>'
		+ '</td>'
		+ '</tr></table>';
	
	var html = 
		'<div class="margin-top-50">' +
			'<div class="footer navbar-fixed-bottom cls-footer">' +
				content +
			'</div>' +
		'</div>';
	oDivBottomFooter.replaceWith(html);
}

// ------------------------------------------
MetaData = function(initialLimit, useLimitCombo, pseventCallOnLimitChanged) {
	this.data_source = null;
	this.origin = null;
	this.security_descriptor = null;
	this.effective_date = null;
	this.product_security_descriptor = null;
	this.has_sipernet_breaches = null;
	this.has_sipernet_data = null;
	this.limit = (initialLimit ? initialLimit : 10);
	this.offset = 0;
	this.count = 0;
	this.total_count = 0;
	this.useLimitCombo = (useLimitCombo ? useLimitCombo : false);
	this.eventCallOnLimitChanged = pseventCallOnLimitChanged;
	
	this.clear = function clear() {
		this.data_source = null;
		this.origin = null;
		this.security_descriptor = null;
		this.effective_date = null;
		this.product_security_descriptor = null;
		this.has_sipernet_breaches = null;
		this.has_sipernet_data = null;
		this.limit = 0;
		this.offset = 0;
		this.count = 0;
		this.total_count = 0;
	};

	this.loadByJson = function loadByJson(data) {
		if (data) {
			this.data_source = data.data_source;
			this.origin = data.origin;
			this.security_descriptor = data.security_descriptor;
			this.effective_date = data.effective_date;
			this.product_security_descriptor = data.product_security_descriptor;
			this.has_sipernet_breaches = data.has_sipernet_breaches;
			this.has_sipernet_data = data.has_sipernet_data;
			this.limit = data.params.limit;
			this.offset = data.params.offset;
			this.count = data.count;
			this.total_count = data.total_count;
		} else {
			this.clear();
		}
	};
	
	this.canPrevious = function canPrevious() {
		if ((this.limit > 0) && (this.total_count > 0)) {
			return (this.offset > 1);
		}
		return false;
	};
	
	this.canNext = function canNext() {
		if (this.limit > 0) {
			return (this.total_count >= (this.offset + this.count));
		}
		return false;
	};
	
	this.getSelLimit = function getSelLimit() {
		if ((!this.useLimitCombo) || (!this.eventCallOnLimitChanged))
			return this.limit;
		var html = '<select aria-label="Select limit records per page" onclick="' + this.eventCallOnLimitChanged + '(this)">'
			+ '<option' + ((10 == this.limit) ? ' selected' : '') + '>10</option>'
			+ '<option' + ((25 == this.limit) ? ' selected' : '') + '>25</option>'
			+ '<option' + ((50 == this.limit) ? ' selected' : '') + '>50</option>'
			+ '<option' + ((100 == this.limit) ? ' selected' : '') + '>100</option>'
			+ '</select>';
		return html;
	};

	this.getPageLink = function getPageLink(psEvent, iOffset, psLabel) {
		var iPage = this.calcPageNo(iOffset)
		var rel = (this.offset > iOffset) ? 'prev' : 'next';
		var html = '<a rel="' + rel + '" href="javascript:void(' + psEvent + '(' + iOffset + '))">' +
			(psLabel ? psLabel : '' + iPage) + 
			'</a>';
		return html;
	};
	
	this.getLinkByPageNo = function getLinkByPageNo(psEvent, iPage, psLabel) {
		var iOffset = this.limit * (iPage - 1) + 1; 
		var rel = (this.offset > iOffset) ? 'prev' : 'next';
		var html = '<a rel="' + rel + '" href="javascript:void(' + psEvent + '(' + iOffset + '))">' +
			(psLabel ? psLabel : '' + iPage) + 
			'</a>';
		return html;
	};
	
	this.calcPageNo = function calcPageNo(iOffset) {
		var iPage = Math.floor(iOffset / this.limit);
		if ((iOffset % this.limit) > 0)
			iPage++;
		return iPage;
	};
	
	this.getPageArray = function getPageArray() {
		var aOffsets = new Array();
		if (this.canPrevious() || this.canNext()) {
			var iLast = this.calcPageNo(this.total_count);
			var iCurrent = this.calcPageNo(this.offset);
			var iStart = iCurrent - 4;
			if (iStart < 2)
				iStart = 2;
			var iEnd = iCurrent + (9 - (iCurrent - iStart ));
			if (iEnd >= iLast)
				iEnd = iLast - 1;
			
			aOffsets.push(1);
			if (iStart > 2)
				aOffsets.push(null);
			for (var iPage = iStart; iPage <= iEnd; iPage++) {
				aOffsets.push(iPage);
			}
			if (iEnd < (iLast - 1))
				aOffsets.push(null);
			aOffsets.push(iLast);
		}
		return aOffsets;
	}
	
	this.getLiNavigations = function getLiNavigations(psEvent) {
		if (! psEvent)
			psEvent = 'retrieveList';
		var iOffset = this.offset - this.limit;
		var bEnable = this.canPrevious();
		var html = '<li class="prev' + (bEnable ? '' : ' disabled') + '">' +
			(bEnable ? this.getPageLink(psEvent, iOffset, '&#8592; Previous')
					 : '<span>&#8592; Previous</span>') +
			'</li>';

		var aPages = this.getPageArray();
		var iCurrentPage = this.calcPageNo(this.offset);
		for (var index = 0; index < aPages.length; index++) {
			var iPage = aPages[index];
			if (iPage == null)
				html += '<li class="disabled"><span><i class="fa fa-ellipsis-h"></i></span></li>';
			else if (iPage == iCurrentPage)
				html += '<li class="active"><span>' + iPage + '</span></li>';
			else
				html += '<li>' + this.getLinkByPageNo(psEvent, iPage) + '</li>';
		}
		bEnable = this.canNext();
		iOffset = this.offset + this.limit;
		html += '<li class="next' + (bEnable ? '' : ' disabled') + '">' +
			(bEnable ? this.getPageLink(psEvent, iOffset, 'Next &#8594;')
					 : '<span>Next &#8594;</span>') +
			'</li>';
		html += '<li class="disabled"><span>Showing ' + this.offset + ' to '
			+ (this.offset + this.count - 1) + ' of '
			+ this.total_count + ' rows ' + this.getSelLimit() + ' records per page</span></li>';
		return html;
	};
	
	this.loadNavigations = function loadNavigations(psEvent, psUlId) {
		if (!psUlId)
			psUlId = 'ulPagination';
		var html = this.getLiNavigations(psEvent);
		$('#' + psUlId).html(html);
	};
	
};

function getUrlParameter(sParam)
{
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++) 
    {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam) 
        {
            return sParameterName[1];
        }
    }
};

function toDate(value) {
	if (value) {
		try {
		    var dDate = new Date(parseInt(value));
		    return dDate;
		}
		catch(err) {
		}
	}
	return null;
};

function toGMTDate(value) {
	if (value) {
		try {
		    var dDate = new Date(parseInt(value));
		    
		    var now = new Date();
		    var dDate2 = new Date(dDate.getTime() + now.getTimezoneOffset() * 60000);
		    	
		    return dDate2;
		}
		catch(err) {
		}
	}
	return null;
};

var localeDateTimeOptions = {
    weekday: "long", year: "numeric", month: "2-digit",
    day: "2-digit", hour: "2-digit", minute: "2-digit"
    /*weekday: "long", year: "numeric", month: "short",
    day: "numeric", hour: "2-digit", minute: "2-digit"*/
};

var localeDateOptions = {
    year: "numeric", month: "2-digit", day: "2-digit"
};

function getDateStr(date) {
	if (date) {
		//return date.toLocaleDateString();
		return date.toLocaleDateString('en-us', localeDateOptions);
	}
	else
		return '';
}

function longToDateStr(value) {
	var date = toDate(value);
	return (date ? getDateStr(date) : '');
}

function longToDateTimeStr(value, timeOffset) {
	
	// CJ-361
	if (!timeOffset)
		timeOffset = 0;
	
	// original method for existing calls.
	if (timeOffset == 0) {
		var date = toDate(value);
		return (date ? getDateStr(date) + ' ' + date.toLocaleTimeString() : '');
	}
	else {
		
		timeOffset = timeOffset * 60000;
		value = value + timeOffset; 
		
		var date = toGMTDate(value);
		
		var tmStamp = getTimeOffsetStr(date);
		return (date ? getDateStr(date) + ' ' + tmStamp : '');	
	}
}

//CJ-361, There is no clean way to retreive timestamp format: hh:mm:ss am.
function getTimeOffsetStr (theDate) { 
	
	if (!theDate)
		return '';
	
	var date_format = '12'; /* FORMAT CAN BE 12 hour (12) OR 24 hour (24)*/

	var d       = theDate;
	var hour    = d.getHours();  /* Returns the hour (from 0-23) */
	var minutes     = d.getMinutes();  /* Returns the minutes (from 0-59) */
	var seconds = d.getSeconds();
	var result  = hour;
	var ext     = '';
	
	if(date_format == '12'){
	    if(hour > 12){
	        ext = 'PM';
	        hour = (hour - 12);
	
	        if(hour < 10){
	            result = "0" + hour;
	        }else if(hour == 12){
	            hour = "00";
	            ext = 'AM';
	        }
	    }
	    else if(hour < 12){
	        result = ((hour < 10) ? "0" + hour : hour);
	        ext = 'AM';
	    }else if(hour == 12){
	        ext = 'PM';
	    }
	}
	
	if(minutes < 10){
	    minutes = "0" + minutes; 
	}
	
	if(seconds < 10){
		seconds = "0" + seconds; 
	}
	
	result = hour + ":" + minutes + ":" + seconds + ' ' + ext; 
	
	//console.log(result);
	
	return result;
}


//CJ-1441
function XSS(value){
	var patt = new RegExp("<");
	var patt1 = new RegExp(">");
	var patt2 = new RegExp("/");
	var patt3 = new RegExp(";");
	var patt4 = new RegExp(":");
	var patt5 = new RegExp("@");
	var patt6 = new RegExp("#");
	
	
	if(patt.test(value)){
		return -1;
	}
	else if(patt1.test(value)){
		return -1;
	}
	else if (patt2.test(value)){
		return -1;
	}
	else if (patt3.test(value)){
		return -1;
	}
	else if (patt4.test(value)){
		return -1;
	}
	else if (patt5.test(value)){
		return -1;
	}
	else if (patt6.test(value)){
		return -1;
	}
	
}

function validateEmail(value)
{
	var re=/^[-a-z0-9~!$%^&*_=+}{\'?]+(\.[-a-z0-9~!$%^&*_=+}{\'?]+)*@([a-z0-9_][-a-z0-9_]*(\.[-a-z0-9_]+)*\.(aero|arpa|biz|com|coop|edu|gov|info|int|mil|museum|name|net|org|pro|travel|mobi|[a-z][a-z])|([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}))(:[0-9]{1,5})?$/i;
	return re.test(value);
}

function valPassword(value) {
	var re = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$|~=[\]'"_#!%^&*:;(){}\\\/<>,?+@.-])[a-zA-Z0-9$|~=[\]'"_#!%^&*:;(){}\\\/<>,+?@.-]{8,20}$/;
	return re.test(value);
}

function detectPasswordError(value) {
	if (!valPassword(value)) {
		return "Password value must be 8-20 characters long, contain uppercase and lowercase letters, digit(s), special character(s), but no spaces";
	}
	return null;
}

function detectPasswordError2(value, confirmValue) {
	if (!valPassword(value)) {
		return "Password value must be 8-20 characters long, contain uppercase and lowercase letters, digit(s), special character(s), but no spaces";
	}
	else if (value != confirmValue) {
		return "Password and Confirm Password values do not match"; 
	}
		
	return null;
}

function toFixed (number, precision) {
	var multiplier = Math.pow(10, precision + 1),
	wholeNumber = Math.floor(number * multiplier);
	return Math.round(wholeNumber / 10) * 10 / multiplier;
}

Number.prototype.formatMoney = function(c, d, t){
	var n = this, 
		c = isNaN(c = Math.abs(c)) ? 2 : c, 
		d = d == undefined ? "." : d, 
		t = t == undefined ? "," : t, 
		s = n < 0 ? "-" : "", 
		i = parseInt(n = toFixed(Math.abs(+n || 0), c)) + "", 
		j = (j = i.length) > 3 ? j % 3 : 0;
	return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
};

function changeFavicon() {
	//<link rel="icon" type="image/png" href="assets/images/favicon.png" />
	var link = document.createElement('link');
	link.rel = 'icon';
	link.type = 'image/png';
	link.href = getAppPath() + '/assets/images/favicon.png';
	document.head.appendChild(link);
}

changeFavicon();

function fillClsVersionSelect(psClsVersionSelectId) {
	var page = getPagePath();
	var data = {'do': 'clauseversion-references'};
	$.ajax({
		url: getAppPath() + 'page',
		data: data,
		cache: false,
		dataType: 'json',
		success: function(data) {
			if (data.data) {
				var aRecords = data.data;
				var html = '<option value=""></option>';
				for (var iData = 0; iData < aRecords.length; iData++) {
					var oRec = aRecords[iData];
					html += '<option value="' + oRec.clause_version_id + '">' + oRec.clause_version_name + '</option>';
				}
				$('#' + psClsVersionSelectId).html(html);
			}
		},
		error: function(object, text, error) {
			alert(error);
		}
	});
};

function closeWindows() { // CJ-634	http://stackoverflow.com/questions/57854/how-can-i-close-a-browser-window-without-receiving-the-do-you-want-to-close-thi
	var browserName = navigator.appName;
	//var browserVer = parseInt(navigator.appVersion);
	if (browserName == "Microsoft Internet Explorer"){
		try {
			var ie7 = (document.all && !window.opera && window.XMLHttpRequest) ? true : false;  
			if (ie7)
			{	//This method is required to close a window without any prompt for IE7 & greater versions.
				window.open('', '_parent', '');
				window.close();
			}
			else
			{	// This method is required to close a window without any prompt for IE6
				this.focus();
				self.opener = this;
				self.close();
			}
		}
		catch (e) {
			window.close();
		}
	} else { // For NON-IE Browsers except Firefox which doesnt support Auto Close
		window.close();
	}
}

function MeasureTime() { // CJ-1170
	this.started = new Date();
	this.ended = null;
	this.log = function(label) {
		logOnConsole(label + ": " + this.duration());
	}
	this.duration = function() {
		if (this.ended == null)
			this.ended = new Date();
		var lDiff = this.ended - this.started;
		var mSeconds = Math.floor(lDiff % 1000);
		lDiff = lDiff / 1000;
		var seconds = Math.floor(lDiff % 60);
		lDiff = lDiff / 60; 
		var minutes = Math.floor(lDiff % 60); // ceil
		lDiff = lDiff / 60; 
		var hours = Math.floor(lDiff % 24);

		var sDuration = '';
		if (hours > 0)
			sDuration = hours + ':';
		if (minutes > 0)
			sDuration += this.pad(minutes, 2) + ':';
		if (seconds > 0)
			sDuration += this.pad(seconds, 2) + ':';
		sDuration += this.pad(mSeconds, 3);
		return sDuration;
	}
	this.pad = function(iValue, iSize) {
		var result = '' + iValue;
		while (result.length < iSize)
			result = '0' + result;
		return result;
	}
	this.restart = function() {
		this.started = new Date();
		this.ended = null;
	}
}

// https://support.microsoft.com/en-us/kb/167820
var _isChrome = null;
function isChrome() { // CJ-760
	if (_isChrome == null) {
		var ua = window.navigator.userAgent;
		var iPos = ua.indexOf ( "Chrome/" )
		_isChrome = (iPos > 0);
		logOnConsole('isChrome(): ' + _isChrome);
	}
	return _isChrome; 
}

//-------------- MyAlert methods -------------------
// CLS customized alert.
// Be sure to add to *.html file: <div id = "alert_placeholder"></div>

function myAlert(myTitle, myBody) {
	myBody = myBody.replace(new RegExp('\n', 'g'), '<br/>');

	var dlgClassName = "modal-dialog modal-sm";
	if (myBody.length > 50)
		dlgClassName = "modal-dialog modal-md";
	
	var html = '<div class="modal fade" id="myAlertModal" tabindex="-1" role="dialog" aria-labelledby="myAlertLabel" aria-hidden="true">'
	  + '<div id="myModalStyle" class="' + dlgClassName + '">'
	  + '<div class="modal-content">'
	  + '<div class="modal-header">'
	  + '<div class="modal-title"><h4>' + myTitle + '</h4></div>'
	  + '</div>'
	  + '<div class="modal-body"><span>' + myBody + '</span></div>'
	  + '<div class="modal-footer">'
	  + '<button type="button" class="btn btn-default" data-dismiss="modal" id="bnMyAlertClose">Close</button>'
	  + '</div>'
	  + '</div>'
	  + '</div>'
	  + '</div>';
	
	$('#alert_placeholder').html(html);
	$('#myAlertModal').modal('show');


}

$("#bnMyAlertClose").click(function() {
	$('#myAlertModal').modal('hide');
});
//-------------- MyAlert methods - End -------------------