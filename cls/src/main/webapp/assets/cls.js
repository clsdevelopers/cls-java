$(function(){

  $('[title]').tooltip();

  $.wysiwyg.rmFormat.enabled = true;

  iv_indicator_values = $('#iv_indicator_value_id').html();
  $('#iv_indicator_id').change(function(){
    indicator = $('#iv_indicator_id :selected').text();
    escaped_indicator = indicator.replace(/([#;&,.+*~\':"!^$[\]()=>|\/@])/g, '\\$1');
    options = $(iv_indicator_values).filter("optgroup[label='" + escaped_indicator + "']").html();
    if(options){
      $('#iv_indicator_value_id').html(options);
    }else{
      $('#iv_indicator_value_id').empty();
    }
  });

  ivg_indicator_values = $('#ivg_indicator_value_id').html();
  $('#ivg_indicator_id').change(function(){
    indicator = $('#ivg_indicator_id :selected').text();
    escaped_indicator = indicator.replace(/([#;&,.+*~\':"!^$[\]()=>|\/@])/g, '\\$1');
    options = $(ivg_indicator_values).filter("optgroup[label='" + escaped_indicator + "']").html();
    if(options){
      $('#ivg_indicator_value_id').html(options);
    }else{
      $('#ivg_indicator_value_id').empty();
    }
  });

  // $('#revision_effective_start_date').datepicker({
  //   //altField: '#revision_effective_start_date_actual',
  //   //altFormat: 'yy-mm-dd',
  //   changeYear: true,
  //   //dateFormat: "yy-mm-dd",
  //   yearRange: '1980:2050'
  // });

  // $('#revision_effective_end_date').datepicker({
  //   //altField: '#revision_effective_end_date_actual',
  //   //altFormat: 'yy-mm-dd',
  //   changeYear: true,
  //   //dateFormat: "yy-mm-dd",
  //   yearRange: '1980:2050'
  // });

});
