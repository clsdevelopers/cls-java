function notify() {
	$.ajax({
		url: 'page?do=index&pg=' + window.location,
		cache: false,
		dataType: 'json',
		success: function(data) {
			if (data) {
				if ('success' != data.status) {
					alert('Sever is not ready');
				}
			}
		},
		error: function(object, text, error) {}
	});
};

function browserCheck() {
    if (navigator.appName == 'Microsoft Internet Explorer') {
        var version = null;
    	var ua = navigator.userAgent;
        var re  = new RegExp("MSIE ([0-9]{1,}[\.0-9]{0,})");
        if (re.exec(ua) != null)
            version = parseFloat( RegExp.$1 );
        if(version && version < 9)
        	alert("CLS does not support Internet Explorer 8 and older. Please use Internet Explorer 9 and later when accessing CLS.");
    }    
}

browserCheck();
notify();
displayBottomFooter();
