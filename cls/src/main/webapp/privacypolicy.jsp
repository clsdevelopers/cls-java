<!DOCTYPE html>
<html>
<head>
	<title>Privacy Policy</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
	<link href="assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">
	<link href="assets/application.css" media="all" rel="stylesheet" type="text/css" />
	<!-- IE6-8 support of HTML5 elements -->
	<!--[if lt IE 9]>
	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
</head>
<body>
<div class='navbar navbar-default navbar-fixed-top' role='navigation'>
	<div class='container-fluid'>
		<div class='navbar-header'>
			<button class='navbar-toggle' data-target='.navbar-collapse' data-toggle='collapse' type='button'>
				<span class='sr-only'>Toggle navigation</span>
				<span class='icon-bar'></span>
				<span class='icon-bar'></span>
				<span class='icon-bar'></span>
			</button>
			<span class="navbar-brand" style="text-decoration:none">Clause Logic Service</span>
		</div>
		<div class='navbar-collapse collapse'> </div>
	</div>
</div>
<div class="container">
	<div class="row">
		<div id='container'>
			<h2 id="label-privacy">Privacy Policy for Clause Logic Service</h2>
			<hr style="height: 10px">

			<div>
			
			<p>Clause Logic Service (CLS) recognizes the importance of protecting the privacy of your personal information, and we have prepared this Privacy Policy to provide you with important information about our privacy practices. This Privacy Policy applies when you use the CLS web site.</p>
			
			<h3>Personal Data We Collect</h3>
			
			<p>CLS collects data to operate effectively and provide you the best experiences with our services. You provide some of this data directly, such as when you register with CLS or update your user profile information. We get some of it by recording how you interact with our services by, for example, using technologies like <a href="#cookieAnchor">cookies</a>, which store information on your device when you sign-in. We do not obtain data from third parties (including other companies).</p>
			
			<h3>How We Use Personal Data</h3>
			
			<p>CLS uses some data we collect to personalize your experience. As an example, within your user profile you may select a time zone. If selected, all dates and times listed throughout CLS will be specific to that time zone.</p>
			
			<p>CLS user data is also used to ensure system security. For instance, you must select a Department and Agency when registering. As a standard user, you will only see the documents that you create. The Agency Administrator will have access to CLS Solicitations that are created by all users in her Agency, including yours. The Administrator will not have access to Solicitations created by external Agencies.</p>
			
			<p>CLS does not share personal data with any third party.</p>
			
			<h3>How to Access &amp; Control Your Personal Data</h3>
			
			<p>You can view or edit some of your user profile information via the Settings menu item located at the top of every CLS page. However, to promote account privileges or change certain information, you will need to contact the CLS Administrator: 
			
			<br>
			<br>&nbsp;&nbsp;Chris Webster 
			<br>&nbsp;&nbsp;Chris.webster@dla.mil
			</p>
			
			</div>
			
			<a name="cookieAnchor">&nbsp;</a>
			<h2 id="label-cookies">Cookies</h2>
			<hr style="height: 10px">

			<div>
						
			<h3>How We Use Cookies</h3>
			
			<p>Cookies are small files that are placed on your computer when you visit a web site. Cookies may be used to store a unique identification number tied to your computer or device so that you can be recognized as the same user across one or more browsing sessions, and across one or more pages.</p>
			
			<p>Users that login to the site via email and password must enable cookies to access CLS.
			Cookies are not required to access CLS for CAC or Smart Card users. </p>
			
			<h3>How To Disable Cookies</h3>
			
			<p>Most browsers accept cookies automatically, but can be configured not to do so or to notify the user when a cookie is being sent. If you wish to disable cookies, refer to your browser help menu to learn how to disable cookies. If you disable browser cookies, it may interfere with the proper functioning of CLS.</p>
							
							
			</div>
		</div>
	</div>
</div>
<div class="margin-top-50">
	<div class="footer navbar-fixed-bottom cls-footer">
		<a href="index.html" title="Go to the home page">Home</a> 
		<a href="javascript:history.back();" title="Go to the previous page">Back</a>
	</div>
</div>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="assets/jquery-1.11.2.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src=".bootstrap/js/bootstrap.min.js"></script>

</body>
</html>
