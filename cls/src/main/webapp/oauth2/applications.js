var _oUserInfo = new UserInfo();
var _oMetaData = new MetaData();

//var LIMIT = 10;

retrieveList = function(offset) {
	var page = getPagePath();
	var data = {'do': 'oauthapp-list'};
	var instance = this;
	$.ajax({
		url: getAppPath() + 'page',
		data: data,
		cache: false,
		dataType: 'json',
		success: function(data) {
			_oMetaData.loadByJson(data.meta);
			var htmlRows = '';
			if (data.data) {
				var aRecords = data.data;
				for (var iRow = 0; iRow < aRecords.length; iRow++) {
					var oRec = aRecords[iRow];
					var canDelete = (oRec.UserCount + oRec.RegulationCount) < 1;
					htmlRows += '<tr>' +
						'<td>' + (oRec.Dept_Name ? oRec.Dept_Name : '') + '</td>' +
						'<td><a href="application-details.html?id=' + oRec.application_id + '">' + (oRec.Org_Name ? oRec.Org_Name : '') + '</a></td>' +
						'<td><a href="application-details.html?id=' + oRec.application_id + '">' + oRec.application_name + '</a></td>' +
						'<td>' + longToDateTimeStr(oRec.created_at, _oUserInfo.user_timezone_offset) + '</td>' +
						'<td>' + longToDateTimeStr(oRec.updated_at, _oUserInfo.user_timezone_offset) + '</td>' +
						// 508 Compliance - make links buttons.
						//'<td style="width:50px;"><a href="application-details.html?id=' + oRec.application_id + '&mode=edit" class="btn btn-default btn-xs">Edit</a></td>' +
						'<td><button onclick="location.href=\'application-details.html?id=' + oRec.application_id + '&mode=edit\'" class="btn btn-default btn-xs">Edit</button></td>' +
						'<td>' +
							'<button onclick="javascript:void(removeRecord(' + oRec.application_id + ',\'' + oRec.application_name + '\'))" class="btn btn-xs btn-danger" data-confirm="Are you sure?" data-method="delete" rel="nofollow">Delete</button>' +
							//'<a href="javascript:void(removeRecord(' + oRec.application_id + ',\'' + oRec.application_name + '\'))" class="btn btn-xs btn-danger" data-confirm="Are you sure?" data-method="delete" rel="nofollow">Delete</a>' +
						'</td>' +
						'</tr>';
				}
			}
			$("#tableList tbody").html(htmlRows);
			//_oMetaData.loadNavigations('retrieveList', 'ulPagination');
		},
		error: function(object, text, error) {
			alert(error);
		}
	});
};

removeRecord = function(id, appName) {
	if (!confirm('Are you sure you want to delete "' + appName + '" application?'))
		return;

	var data = {'do': 'oauthapp-delete', 'id': id};
	var instance = this;
	$.ajax({
		url: getAppPath() + 'page',
		data: data,
		cache: false,
		dataType: 'json',
		success: function(data) {
			if ('success' == data.status) {
				retrieveList(0);
				alert('"' + appName + '" application is deleted.')
			} else {
				alert(data.message);
			}
		},
		error: function(object, text, error) {
			alert(error);
		}
	});	
}

onLoginResponse = function(success, userInfo) {
	if (!userInfo.canAccessAdminOptions()) {
		alert('Not authorized');
		goDashboard();
		return;
	}
	if (success) {
		retrieveList(0);
	}
	displaySessionMessage(_oUserInfo.sessionMessage);
}

_oUserInfo.getLogon(true, onLoginResponse);
