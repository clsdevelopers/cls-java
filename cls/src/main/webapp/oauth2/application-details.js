var _oUserInfo = new UserInfo();
var _sMode = getUrlParameter('mode');
var _sId = getUrlParameter('id');
var isMacLike = navigator.platform.match(/(Mac|iPhone|iPod|iPad)/i)? true: false;

removeRecord = function(id, orgName) {
	if (confirm('Are you sure you want to delete ' + orgName + ' application?')) {
		alert('will be implemented');
	}
}

function loadDepts(json) {
	var html = '<option value="">Please select</option>';
	var oRec;
	var sDeptId = null;
	if (json.length == 1) {
		oRec = json[0];
		html += '<option value="' + oRec.Dept_Id + '" selected>' + oRec.Dept_Name + '</option>';
		sDeptId = oRec.Dept_Id;
	} else {
		for (var iData = 0; iData < json.length; iData++) {
			oRec = json[iData];
			html += '<option value="' + oRec.Dept_Id + '">' + oRec.Dept_Name + '</option>';
		}
	}
	$('#dept_id').html(html);
	if (sDeptId) {
		onDeptSelect(sDeptId);
	}
}

OrgItem = function(Dept_Id, Org_Id, Org_Name) {
	this.deptId = Dept_Id;
	this.orgId = Org_Id;
	this.orgName = Org_Name;
}

OrgSet = function() {
	this.elements = new Array();
	this.loadJson = function(json){
		for (var iData = 0; iData < json.length; iData++) {
			var oOrgItem = new OrgItem(json[iData].Dept_Id, json[iData].Org_Id, json[iData].Org_Name);
			this.elements.push(oOrgItem);
		}
	}
};

var _orgArray = new OrgSet();

function onDeptSelect(sId) {
	if (!sId)
		sId = $('#dept_id').val(); // oSelect.options[oSelect.selectedIndex].value;
	var html;
	if (sId) {
		html = '<option value="">Please select</option>';
		var aItems = _orgArray.elements;
		//var bFound = false;
		var oItem;
		var aOrg = new Array();
		for (var iData = 0; iData < aItems.length; iData++) {
			oItem = aItems[iData];
			if (oItem.deptId == sId) {
				aOrg.push(oItem);
				//html += '<option value="' + oItem.orgId + '">' + oItem.orgName + '</option>';
				//bFound = true;
			}
			//else if (bFound)
			//	break;
		}
		if (aOrg.length == 1) {
			oItem = aOrg[0];
			html += '<option value="' + oItem.orgId + '" selected>' + oItem.orgName + '</option>';
		} else {
			for (var iData = 0; iData < aOrg.length; iData++) {
				oItem = aOrg[iData];
				html += '<option value="' + oItem.orgId + '">' + oItem.orgName + '</option>';
			}
		}
	} else {
		html = '<option value="">Select a department above at first</option>';
	}
	$('#org_id').html(html);
}

submitForm = function() {
	var data = {
		'do': 'oauthapp-detail-edit',
		mode: _sMode,
		id: _sId,
		application_name: $('#application_name').val(),
		application_uid: $('#application_uid').val(),
		application_secret: $('#application_secret').val(),
		org_id: $('#org_id').val()
	};
	$.ajax({
		url: getAppPath() + 'page',
		data: data,
		cache: false,
		dataType: 'json',
		success: function(data) {
			if ('success' == data.status) {
				location = 'applications.html';
			} else {
				alert(data.message);
				$('#application_name').focus();
			}
		},
		error: function(object, text, error) {
			//alert(error);
			goDashboard();
		}
	});
}

function onBtnEditClick() {
	location = 'application-details.html?id=' + _sId + "&mode=edit"; 
}

displayDetails = function(json) {
	var data = json.data;
	$('#application_name').val(data.application_name),
	$('#application_uid').val(data.application_uid);
	$('#application_secret').val(data.application_secret);

	var orgId = data.org_id;
	var deptId = data.Dept_Id;
	$('#dept_id').val(deptId);
	onDeptSelect(deptId);
	$("#dept_id").on('change', function() {
		onDeptSelect();
	});
	$('#org_id').val(orgId);
	$('#created_at').val(longToDateTimeStr(data.created_at, _oUserInfo.user_timezone_offset));
	$('#divCreatedAt').show();
	$('#updated_at').val(longToDateTimeStr(data.updated_at, _oUserInfo.user_timezone_offset));
	$('#divLastUpdated').show();
	
	if ('edit' == _sMode) {
		$('#idTitle').html('Edit Application');
		$('#btnEdit').hide();
	} else {
		$('#idTitle').html('Application');
		$("#application_name").prop('disabled', true);
		$("#dept_id").prop('disabled', true);
		$("#org_id").prop('disabled', true);
		$('#btnSubmit').hide();
		$('#btnCancel').html('Back');
	}
}

retrieveRecord = function() {
	if (!_sId)
		return;
	var page = getPagePath();
	var data = {'do': 'oauthapp-detail-get', id: _sId, mode: _sMode};
	var instance = this;
	$.ajax({
		url: getAppPath() + 'page',
		data: data,
		cache: false,
		dataType: 'json',
		success: function(data) {
			var htmlRows = '';
			if (data) {
				displayDetails(data);
			} else {
				alert('Unable to retrieve data.');
				//goDashboard();
			}
		},
		error: function(object, text, error) {
			alert(error);
			//goDashboard();
		}
	});
}


$('#form').submit(function(e){
	submitForm();
	return false;
});

retrieveNewCodes = function() {
	var page = getPagePath();
	var data = {'do': 'oauth-new_app_codes'};
	var instance = this;
	$.ajax({
		url: getAppPath() + 'page',
		data: data,
		cache: false,
		dataType: 'json',
		success: function(data) {
			var htmlRows = '';
			if (data) {
				if ('success' == data.status) {
					//var application_uid = data.data.application_uid;
					$('#application_uid').val(data.data.application_uid);
					//var application_secret = data.data.application_secret;
					$('#application_secret').val(data.data.application_secret);
				} else {
					alert(data.message);
				}
			} else {
				alert('Unable to retrieve data.');
				//goDashboard();
			}
		},
		error: function(object, text, error) {
			alert(error);
			//goDashboard();
		}
	});
};

retrieveDeptOrg = function() {
	var page = getPagePath();
	var data = {'do': 'org-references', 'pg': 'application-details'};
	var instance = this;
	$.ajax({
		url: getAppPath() + 'page',
		data: data,
		cache: false,
		dataType: 'json',
		success: function(data) {
			var htmlRows = '';
			if (data) {
				if ('success' == data.status) {
					_orgArray.loadJson(data.data.Orgs);
					loadDepts(data.data.Depts);
					$("#dept_id").on('change', function() {
						if (isMacLike)
							setTimeout(function() {onDeptSelect();}, 50);
						else
							onDeptSelect();
					});
					if ('new' == _sMode) {
						$('#btnEdit').hide();
						retrieveNewCodes();
					} else {
						retrieveRecord();
					}
				} else {
					alert(data.message);
				}
			} else {
				alert('Unable to retrieve data.');
				//goDashboard();
			}
		},
		error: function(object, text, error) {
			alert(error);
			//goDashboard();
		}
	});
};

onLoginResponse = function(success, userInfo) {
	if (success) {
		retrieveDeptOrg();
	}
	displaySessionMessage(_oUserInfo.sessionMessage);
}

_oUserInfo.getLogon(true, onLoginResponse);

