var _oUserInfo = new UserInfo();
var _oMetaData = new MetaData();

var LIMIT = 10;

retrieveList = function(offset) {
	var page = getPagePath();
	var data = {'do': 'user-list', query: $('#query').val(), role_id: $('#role_id').val(),
			sort_field: $("input:radio[name ='sort_field']:checked").val(), // $('#sort_field').val(),
			sort_order: $('#sort_order').val(),
			limit: LIMIT, offset: offset};
	var instance = this;
	$.ajax({
		url: getAppPath() + 'page',
		data: data,
		cache: false,
		dataType: 'json',
		success: function(data) {
			var htmlRows = '';
			_oMetaData.loadByJson(data.meta);
			if (data.data) {
				var aRecords = data.data;
				for (var iRow = 0; iRow < aRecords.length; iRow++) {
					var oRec = aRecords[iRow];
					var sActive = (oRec.is_active ? "Deactivate": "Activate");
					var sIsActive = (oRec.is_active ? "Yes": "No");
					htmlRows += '<tr>' +
						'<td><a href="../admin/user_details.html?id=' + oRec.User_Id + '">' + oRec.email + '</a></td>' +
						//'<td>' + oRec.first_name + ' ' + oRec.last_name + '</td>' +
						'<td>' + oRec.first_name + '</td>' +
						'<td>' + oRec.last_name + '</td>' +
						'<td>' + (oRec.Roles == null ? "None": oRec.Roles ) + '</td>' +
						'<td>' + oRec.DeptName + '</td>' +						
						'<td>' + oRec.OrgName + '</td>' +
						'<td>' + sIsActive + '</td>' +   
						// 508 Compliance Changes.
						//'<td style="width:50px;"><a href="user_details.html?id=' + oRec.User_Id + '&mode=edit" class="btn btn-default btn-xs">Edit</a></td>' +
						//'<td style="width:64px;"><a href="javascript:void(deactiveUser(' + oRec.User_Id + ',\'' + oRec.first_name + '\',\'' + oRec.last_name + '\',' + oRec.is_active + '))" class="btn btn-xs btn-danger" data-confirm="Are you sure?" data-method="delete" rel="nofollow">' + sActive + '</a></td>' +
						//'<td style="width:64px;"><a href="javascript:void(resetUserPassword(' + oRec.User_Id + ',\'' + oRec.first_name + '\',\'' + oRec.last_name + '\',\'' + oRec.email + '\',' + oRec.is_active + '))" class="btn btn-xs btn-success" data-confirm="Are you sure?" data-method="delete" rel="nofollow">Reset Password</a></td>' +
						'<td><button onclick="location.href=\'user_details.html?id=' + oRec.User_Id + '&mode=edit\'" class="btn btn-default btn-xs">Edit</button></td>' +
						'<td><button onclick="javascript:void(deactiveUser(' + oRec.User_Id + ',\'' + oRec.first_name + '\',\'' + oRec.last_name + '\',' + oRec.is_active + '))" class="btn btn-xs btn-danger" data-confirm="Are you sure?" data-method="delete" rel="nofollow">' + sActive + '</button></td>' +
						'<td><button onclick="javascript:void(resetUserPassword(' + oRec.User_Id + ',\'' + oRec.first_name + '\',\'' + oRec.last_name + '\',\'' + oRec.email + '\',' + oRec.is_active + '))" class="btn btn-xs btn-success" data-confirm="Are you sure?" data-method="delete" rel="nofollow">Reset Password</button></td>' +
						'</tr>';
				}
			}
			$("#tableUsers tbody").html(htmlRows);
			_oMetaData.loadNavigations('retrieveList', 'ulPagination');
		},
		error: function(object, text, error) {
			alert(error);
		}
	});
};

removeUser = function(id, email) {
	if (confirm('Are you sure you want to delete ' + email + ' account?')) {
		var data = {'do': 'user-remove', id: id};
		$.ajax({
			url: getAppPath() + 'page',
			data: data,
			cache: false,
			dataType: 'json',
			success: function(data) {
				if ('success' == data.status) {
					retrieveList(1);
					alert(email + ' account is removed.');
				} else {
					alert(data.message);
				}
			},
			error: function(object, text, error) {
				//alert(error);
				goDashboard();
			}
		});		
	}
}

// deactiveUser = function(id, email, isactive) {
deactiveUser = function(id, first_name, last_name, isactive) {	
	var sActiveorDeactive = (isactive ? "deactivate" : "activate");
	if (confirm('Are you sure you want to ' + sActiveorDeactive + ' ' + first_name + ' ' + last_name + ' account?')) {
		var data = {'do': 'user-remove', id: id};
		$.ajax({
			url: getAppPath() + 'page',
			data: data,
			cache: false,
			dataType: 'json',
			success: function(data) {
				if ('success' == data.status) {
					retrieveList(1);
					alert(first_name + ' ' + last_name + ' account is ' + sActiveorDeactive + 'd.');
				} else {
					alert(data.message);
				}
			},
			error: function(object, text, error) {
				//alert(error);
				goDashboard();
			}
		});		
	}
}

resetUserPassword = function(id, first_name, last_name, email, isactive) {	
	if (confirm('Are you sure you want to reset password for ' + first_name + ' ' + last_name + ' account?')) {
		var data = {'do': 'user-reset-password', id: id, first_name: first_name, last_name: last_name, email: email};
		$.ajax({
			url: getAppPath() + 'page',
			data: data,
			cache: false,
			dataType: 'json',
			success: function(data) {
				if ('success' == data.status) {
					retrieveList(1);
					alert('An email will be sent to ' +  first_name + ' ' + last_name + ' for reset password.');
				} else {
					alert(data.message);
				}
			},
			error: function(object, text, error) {
				//alert(error);
				goDashboard();
			}
		});		
	}
}

toggleOptionsForUser = function() {
	$('#spanUserName').html(_oUserInfo.fullName());
	// $('#spanUserOrgName').html(_oUserInfo.orgName);
	retrieveList(0);
}

onLoginResponse = function(success, userInfo) {
	if (!userInfo.canAccessAdminOptions()) {
		alert('Not authorized');
		goDashboard();
		return;
	}
	if (success) {
		toggleOptionsForUser();
	}
	displaySessionMessage(_oUserInfo.sessionMessage);
}

_oUserInfo.getLogon(true, onLoginResponse);
