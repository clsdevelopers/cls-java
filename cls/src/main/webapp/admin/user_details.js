var _oUserInfo = new UserInfo();
var _sMode = getUrlParameter('mode');
var _sId = getUrlParameter('id');

/*
(function() {
	$(function() {
		return $('table.audit-event-log a.audit-event-details-link').click(function() {
			$(this).parents('tr').next().toggle();
			return false;
		});
	});
}).call(this);
*/

$(function(){
	var allRequiredCheckboxes = $(':checkbox[required]');
	var checkboxNames = [];

	for (var i = 0; i < allRequiredCheckboxes.length; ++i){
		var name = allRequiredCheckboxes[i].name;
		checkboxNames.push(name);
	}

	checkboxNames = checkboxNames.reduce(function(p, c) {
		if (p.indexOf(c) < 0) p.push(c);
			return p;
	}, []);

	for (var i in checkboxNames){
	    !function(){
	    	var name = checkboxNames[i];
	    	var checkboxes = $('input[name="' + name + '"]');
	    	checkboxes.change(function(){
	    		if(checkboxes.is(':checked')) {
	    			checkboxes.removeAttr('required');
	    		} else {
	    			checkboxes.attr('required', 'required');
	    		}
	    	});
	    }();
	}
});

resetCheckBoxesForLoaded = function() {
	var allRequiredCheckboxes = $(':checkbox[required]');
	var checkboxNames = [];

	for (var i = 0; i < allRequiredCheckboxes.length; ++i){
		var name = allRequiredCheckboxes[i].name;
		if (checkboxNames.indexOf(name) < 0)
			checkboxNames.push(name);
	}

	for (var i in checkboxNames) {
		var name = checkboxNames[i];
		var checkboxes = $('input[name="' + name + '"]');
		var checked = false;
		for (var i = 0; i < checkboxes.length; ++i){
			var checkbox = checkboxes[i];
			if (checkbox.checked) {
				checked = true;
				break;
			}
		}
		if (checked)
			checkboxes.removeAttr('required');
		else
			checkboxes.attr('required', 'required');
	}
}

function validateForm() {
	var errorMessage = null;
	var emailId = $('#user_email');
	var emailValue = emailId.val();
	if (!validateEmail(emailValue)){
		emailId.focus();
		displaySessionMessage("The email address is not valid");
		return false;
	}
	
	var oComp = document.getElementById('linkNewPassword');
	if (!oComp) {
		var oInput = $('#user_password');
		var sPswd = oInput.val();
		
		var oInput2 = $('#confirm_password');
		var sConfirmPswd = oInput2.val();
		
		errorMessage = detectPasswordError2(sPswd, sConfirmPswd);
		if (errorMessage) {
			displaySessionMessage(errorMessage);
			oInput.focus();
			return false;
		}
	}
	var sOrgId = $('#org_id').val();
	if (!sOrgId) {
		alert("'Org / Agency' must be selected.");
		return false;
	}
	return true;
}

function getSubmitData() {
	var roleCheckboxes= document.getElementsByName('user_role_ids');
	var roles = '';
	for (var i = 0; i < roleCheckboxes.length; i++) {
		if (roleCheckboxes[i].checked) {
			if (roles == '')
				roles = roleCheckboxes[i].value;
			else
				roles += ',' + roleCheckboxes[i].value;
		}
	}

	var object = document.getElementById("user_is_active_yes");
	var isactive = object.checked;
	
	var newPassword = null;
	// if (!$('#linkNewPassword'))  
	if ($('#linkNewPassword'))
		newPassword = $('#user_password').val();
	var sOrgId = $('#org_id').val();
	
	var data = {'do': 'user-detail-' + _sMode,
			id: _sId,
			email: $('#user_email').val(),
			first_name: $('#user_first_name').val(),
			last_name: $('#user_last_name').val(),
			// org_id: $('#user_org_id').val(),
			org_id: sOrgId,
			user_role_ids: roles,
			password: newPassword,
			is_active: isactive   
		};
	return data;
}

submitForm = function() {
	var data = getSubmitData();
	$.ajax({
		url: getAppPath() + 'page',
		data: data,
		cache: false,
		dataType: 'json',
		success: function(data) {
			if ('success' == data.status) {
				//alert('Saved.');
				location = 'users.html';
			} else {
				alert(data.message);
			}
		},
		error: function(object, text, error) {
			//alert(error);
			goDashboard();
		}
	});
}

toggleEventDetails = function(id) {
	$('#' + id).toggle();
}

getLogTableRows = function(userLogs) {
	var htmlRows = '';
	for (var iRec = 0; iRec < userLogs.length; iRec++) {
		var oRec = userLogs[iRec];
		var sTrId = "trLog" + oRec.audit_event_id;	
		htmlRows +=
			'<tr>' +
				'<td style="white-space:nowrap">' + longToDateTimeStr(oRec.updated_at) + '</td>' +
				'<td>' + oRec.action + '</td>' +
				'<td><button class="btn btn-link" onClick="toggleEventDetails(\'' + sTrId + '\')">Details</a></td>' +
			'</tr>' +
			'<tr id="' + sTrId + '" style="display:none">' + //  class="audit-event-additional-data"
				'<td colspan="3">' +
					'<table class="table">' +
						'<tr>' +
							'<td><strong>Initiated user</strong></td>' +
							'<td><a href="user_details.html?id=' + oRec.initiated_user_id + '">' + oRec.InitiatedUserName + '</a></td>' +
						'</tr>' +
						//'<tr>' +
						//	'<td><strong>User</strong></td>' +
						//	'<td><a href="../../admin/users/18.html">Bill  Chin</a></td>' +
						//'</tr>' +
					'</table>' +
					'<pre>' + oRec.additional_data + '</pre>' +
				'</td>' +
			'</tr>';
	}
	return htmlRows;
} 

displayUserDetails = function(data) {
	var record = data.data; // jQuery.parseJSON(data.data);
	var sIsActive = (record.is_active ? "Yes" : "No");
	$('#divEdit').remove();
	
	$('#h3Title').html('View User');
	$('#tdEmail').html(record.email);
	$('#tdName').html(record.first_name + ' ' + record.last_name);
	//$('#tdOrg').html(record.orgName);
	$('#tdOrg').html(record.orgName);	// CJ-754 Org_Name
	$('#tdDept').html(record.Dept_Name);
	
	$('#tdRoles').html(record.Roles);
	$('#tdIsActive').html(sIsActive);	
	
	$('#tdCurrentLogonAt').html(longToDateTimeStr(record.current_logon_at));
	$('#tdCurrentLogonIp').html(record.current_logon_ip);
	$('#tdLastLogonAt').html(longToDateTimeStr(record.last_logon_at));
	$('#tdLastLogonIp').html(record.last_logon_ip);
	$('#tdPasswordUpdatedAt').html(longToDateTimeStr(record.password_updated_at));
	
	var htmlRows = getLogTableRows(record.UserLogs);
	$("#tableAuditEventsForView tbody").html(htmlRows);
	$('#linkEdit').prop("href", 'user_details.html?id=' + _sId + '&mode=edit');
	$('#divView').show();
	
}

editUserDetails = function(data) {
	var record = data.data; // jQuery.parseJSON(data.data);
	$('#divView').remove();
	
	$('#h3Title').html('Edit User');
	//$('#form').prop("target", getAppPath() + '/page?do=user-detail-mod&id=' + _sId);
	$('#form').submit(function(e){
		if (validateForm())
			submitForm();
		return false;
	});
	$('#linkShow').prop("href", 'user_details.html?id=' + _sId);
	
	$('#user_email').val(record.email);
	$('#user_first_name').val(record.first_name);
	$('#user_last_name').val(record.last_name);
	
	var orgId = record.org_id;
	var deptId = record.Dept_Id;
	
	var deptName = record.Dept_Name;
	var orgName = record.orgName; // CJ-754 Org_Name
	
	 setTimeout(function() {$('#dept_id').val(deptId); }, 50);
	 // $('#dept_id').val(deptId);
	 setTimeout(function() {onDeptSelect(deptId);}, 50);
	 // onDeptSelect(deptId);

	$("#dept_id").on('change', function() {
		onDeptSelect();
	});	
	setTimeout(function() {$('#org_id').val(orgId); }, 50);
	// $('#org_id').val(orgId);
	
	if (record.isSuperUser) {
		$('#user_role_ids_0').prop( "checked", true );
	} else {
		if (!_oUserInfo.isSuperUser) {
			$('#divSysAdmin').hide();
		}
	}
	if (record.isSubsuperUser)
		$('#user_role_ids_1').prop( "checked", true );
	if (record.isAgencyReviewer)
		$('#user_role_ids_2').prop( "checked", true );
	if (record.isReviewer) {
		$('#user_role_ids_3').prop( "checked", true );
	} else {
		if (!_oUserInfo.isSuperUser) {
			$('#divReviewer').hide();
		}
	}
	if (record.isRegularUser)
		$('#user_role_ids_4').prop( "checked", true );
	
	if (record.is_active)  
		$('#user_is_active_yes').prop( "checked", true );
	else
		$('#user_is_active_no').prop( "checked", true );

	resetCheckBoxesForLoaded();
	
	var htmlRows = getLogTableRows(record.UserLogs);
	$("#tableAuditEventsForEdit tbody").html(htmlRows);
	$('#divAuditForEdit').show();
	$('#divEdit').show();
}

retrieveUserDetails = function() {
	if (!_sId)
		return;
	var page = getPagePath();
	var data = {'do': 'user-detail-get', id: _sId, mode: _sMode};
	var instance = this;
	$.ajax({
		url: getAppPath() + 'page',
		data: data,
		cache: false,
		dataType: 'json',
		success: function(data) {
			var htmlRows = '';
			if (data) {
				if ('edit' == _sMode)
					editUserDetails(data);
				else
					displayUserDetails(data);
			} else {
				alert('Unable to retrieve data.');
				//goDashboard();
			}
		},
		error: function(object, text, error) {
			alert(error);
			//goDashboard();
		}
	});
}

function loadDepts(json) {
	var html = '<option value="">Please select</option>';
	var oRec;
	var sDeptId = null;
	if (json.length == 1) {
		oRec = json[0];
		html += '<option value="' + oRec.Dept_Id + '" selected>' + oRec.Dept_Name + '</option>';
		sDeptId = oRec.Dept_Id;
	} else {
		for (var iData = 0; iData < json.length; iData++) {
			oRec = json[iData];
			html += '<option value="' + oRec.Dept_Id + '">' + oRec.Dept_Name + '</option>';
		}
	}
	$('#dept_id').html(html);
	if (sDeptId) {
		onDeptSelect(sDeptId);
	}
	/*
	var html = '<option value="">Please select</option>';
	for (var iData = 0; iData < json.length; iData++) {
		html += '<option value="' + json[iData].Dept_Id + '">' + json[iData].Dept_Name + '</option>';
	}
	$('#dept_id').html(html);
	*/
}

OrgItem = function(Dept_Id, Org_Id, Org_Name) {
	this.deptId = Dept_Id;
	this.orgId = Org_Id;
	this.orgName = Org_Name;
}

OrgSet = function() {
	this.elements = new Array();
	this.loadJson = function(json){
		for (var iData = 0; iData < json.length; iData++) {
			var oOrgItem = new OrgItem(json[iData].Dept_Id, json[iData].Org_Id, json[iData].Org_Name);
			this.elements.push(oOrgItem);
		}
	}
};

var _orgArray = new OrgSet();

function onDeptSelect(sId) {
	if (!sId)
		sId = $('#dept_id').val(); // oSelect.options[oSelect.selectedIndex].value;
	var html;
	if (sId) {
		html = '<option value="">Please select</option>';
		var aItems = _orgArray.elements;
		//var bFound = false;
		var oItem;
		var aOrg = new Array();
		for (var iData = 0; iData < aItems.length; iData++) {
			oItem = aItems[iData];
			if (oItem.deptId == sId) {
				aOrg.push(oItem);
			}
		}
		if (aOrg.length == 1) {
			oItem = aOrg[0];
			html += '<option value="' + oItem.orgId + '" selected>' + oItem.orgName + '</option>';
		} else {
			for (var iData = 0; iData < aOrg.length; iData++) {
				oItem = aOrg[iData];
				html += '<option value="' + oItem.orgId + '">' + oItem.orgName + '</option>';
			}
		}
		/*
		html = '<option value="">Please select</option>';
		var aItems = _orgArray.elements;
		var bFound = false;
		for (var iData = 0; iData < aItems.length; iData++) {
			var oItem = aItems[iData];
			if (oItem.deptId == sId) {
				html += '<option value="' + oItem.orgId + '">' + oItem.orgName + '</option>';
				bFound = true;
			} else if (bFound)
				break;
		}
		*/
	} else {
		html = '<option value="">Select a department above at first</option>';
	}
	$('#org_id').html(html);
}

retrieveDeptOrg = function() {
	var page = getPagePath();
	var data = {'do': 'org-references', 'pg': 'user_details'};
	var instance = this;
	$.ajax({
		url: getAppPath() + 'page',
		data: data,
		cache: false,
		dataType: 'json',
		success: function(data) {
			var htmlRows = '';
			if (data) {
				if ('success' == data.status) {
					_orgArray.loadJson(data.data.Orgs);
					loadDepts(data.data.Depts);
					if ('new' != _sMode) { // CJ-754
						retrieveUserDetails();
					}
					/*
					$("#dept_id").on('change', function() {
						onDeptSelect();
					});
					*/
				} else {
					alert(data.message);
				}
			} else {
				alert('Unable to retrieve data.');
				//goDashboard();
			}
		},
		error: function(object, text, error) {
			alert(error);
			//goDashboard();
		}
	});
};


onLoginResponse = function(success, userInfo) {
	if (success) {
		if (!userInfo.canAccessAdminOptions()) {
			alert('Not authorized');
			goDashboard();
			return;
		}
		retrieveDeptOrg();
		if ('new' == _sMode) {
			//$('#form').prop("target", getAppPath() + '/page?do=user-detail-new');
			$('#form').submit(function(e){
				if (validateForm())
					submitForm();
				return false;
			});
			$("#dept_id").on('change', function() {
				onDeptSelect();
			});	
			$('#divView').remove();
			//divAuditForEdit
			$('#h3Title').html('Create User');
			$('#btnSave').val('Create User');
			$('#spanLinkShow').hide();
			//linkNewPassword
			$('#user-password-field').removeClass('hide');
			$('#linkNewPassword').remove();
			$('#user_password').prop('required',true);
			$('#divEdit').show();
			if (!userInfo.isSuperUser) {
				$('#divSysAdmin').hide();
				$('#divReviewer').hide();
			}
		// } else {
			// CJ-754 retrieveUserDetails();
		}		
		displaySessionMessage(userInfo.sessionMessage);
	} else
		goHome();
}

_oUserInfo.getLogon(true, onLoginResponse);
