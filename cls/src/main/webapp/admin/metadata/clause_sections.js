var _oUserInfo = new UserInfo();
var _oMetaData = new MetaData(10, true, 'onLimitChange');

onLimitChange = function(oSelect) {
	var sLimit = oSelect.options[oSelect.selectedIndex].value;
	var iLimit = parseInt(sLimit);
	if (_oMetaData.limit != iLimit) {
		_oMetaData.limit = iLimit;
		retrieveList(1);
	}
}

viewRecordMsg = function() {
	alert('will be implemented');
}

retrieveList = function(offset) {
	var page = getPagePath();
	var sortOrder = $('#sort_order').val();
	var data =
		{'do': 'clausesection-list', 
		query: $('#query').val(),
		sort_field: $("input:radio[name ='sort_field']:checked").val(), 
		sort_order: sortOrder,			
		limit: _oMetaData.limit, offset: offset};
	var instance = this;
	$.ajax({
		url: getAppPath() + 'page',
		data: data,
		cache: false,
		dataType: 'json',
		success: function(data) {
			var htmlRows = '';
			_oMetaData.loadByJson(data.meta);
			if (data.data) {
				var aRecords = data.data;
				for (var iRow = 0; iRow < aRecords.length; iRow++) {
					var oRec = aRecords[iRow];
					htmlRows += '<tr>' +
						'<td><a href="clause_sections_details.html?id=' + oRec.clause_section_id + '">' + oRec.clause_section_id + '</a></td>' +
						'<td><a href="clause_sections_details.html?id=' + oRec.clause_section_id + '">' + oRec.clause_section_code + '</a></td>' +
						// '<td>' + oRec.clause_section_code + '</td>' +
						'<td>' + oRec.clause_section_header + '</td>' +
						// '<td style="width:50px;"><a href="clauses_details.html?id=' + oRec.clause_section_id + '&mode=edit" class="btn btn-default btn-xs">Edit</a></td>' +
						// '<td style="width:50px;"><a href="javascript:(viewRecordMsg())" class="btn btn-default btn-xs">Edit</a></td>' +
						// '<td style="width:64px;"><a href="javascript:void(removeItem(' + oRec.clause_section_id + ',\'' + oRec.clause_section_code + '\'))" class="btn btn-xs btn-danger" data-confirm="Are you sure?" data-method="delete" rel="nofollow">Delete</a></td>' +
						'</tr>';
				}
			}
			$("#tableContent tbody").html(htmlRows);
			_oMetaData.loadNavigations('retrieveList', 'ulPagination');
		},
		error: function(object, text, error) {
			alert(error);
		}
	});
};

removeItem = function(id, name) {
	if (confirm('Are you sure you want to delete ' + name + ' header?')) {
		alert('will be implemented');
	}
}

onLoginResponse = function(success, userInfo) {
	if (success) {
		if (!userInfo.isSuperUser) {
			alert('Not authorized');
			goDashboard();
			return;
		}
		retrieveList(0);
	}
	displaySessionMessage(_oUserInfo.sessionMessage);
}

_oUserInfo.getLogon(true, onLoginResponse);
