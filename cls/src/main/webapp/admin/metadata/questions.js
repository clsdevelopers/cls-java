var _oUserInfo = new UserInfo();
var _oMetaData = new MetaData(10, true, 'onLimitChange');

// var LIMIT = 10;

onLimitChange = function(oSelect) {
	var sLimit = oSelect.options[oSelect.selectedIndex].value;
	var iLimit = parseInt(sLimit);
	if (_oMetaData.limit != iLimit) {
		_oMetaData.limit = iLimit;
		retrieveList(1);
	}
}

viewRecordMsg = function() {
	alert('will be implemented');
}

getQuestionTypeName = function(code) {
	switch (code) {
	case 'B': return 'Boolean';
	case 'N': return 'Number';
	case 'D': return 'Date';
	case 'C': return 'Currency';
	case 'T': return 'Text';
	case '1': return 'Choose One';
	case 'M': return 'Choose Multiple';
	}
	return code;
}

retrieveList = function(offset) {
	var page = getPagePath();
	var sortOrder = $('#sort_order').val();
	var data =
		{'do': 'question-list',
		query: $('#query').val(),
		cls_version: $('#clsVersion').val(),
		question_type: $('#question_type').val(),
		sort_field: $("input:radio[name ='sort_field']:checked").val(), // $('#sort_field').val(),
		sort_order: sortOrder,
		limit: _oMetaData.limit, offset: offset};
	var instance = this;
	$.ajax({
		url: getAppPath() + 'page',
		data: data,
		cache: false,
		dataType: 'json',
		success: function(data) {
			var htmlRows = '';
			_oMetaData.loadByJson(data.meta);
			if (data.data) {
				var aRecords = data.data;
				for (var iRow = 0; iRow < aRecords.length; iRow++) {
					var htmlChoices = "";
					var oRec = aRecords[iRow];
					// var oQuestionChoices = oRec.question_choices;
					var oQuestionChoices = oRec.Question_Choices;
					for (var iChoice = 0; iChoice < oQuestionChoices.length; iChoice++) {
						if (iChoice == 0)
							htmlChoices = oQuestionChoices[0];
						else
							htmlChoices += '<br />' + oQuestionChoices[iChoice];
					}
					htmlRows += '<tr>' +
						// '<td>' + oRec.question_id + '</td>' +
						// '<td><a href="javascript:(viewRecordMsg())">' + oRec.question_code + '</a></td>' +
						'<td><a href="questions_details.html?id=' + oRec.question_id + '">' + oRec.question_condition_sequence + '</a></td>' +
						'<td><a href="questions_details.html?id=' + oRec.question_id + '">' + oRec.question_id + '</a></td>' +
						'<td><a href="questions_details.html?id=' + oRec.question_id + '">' + oRec.question_code + '</a></td>' +
						'<td>' + oRec.clause_version_name + '</td>' +
						// '<td>' + (oRec.question_group ? oRec.question_group : '') + '</td>' +
						'<td>' + (oRec.question_group_name ? oRec.question_group_name : '') + '</td>' +
						'<td>' + oRec.question_text + '</td>' +
						'<td>' + getQuestionTypeName(oRec.question_type) + '</td>' +
						'<td>' + htmlChoices + '</td>' +
						// '<td style="width:50px;"><a href="questions_details.html?id=' + oRec.question_id + '&mode=edit" class="btn btn-default btn-xs">Edit</a></td>' +
						// '<td style="width:50px;"><a href="javascript:(viewRecordMsg())" class="btn btn-default btn-xs">Edit</a></td>' +
						// '<td style="width:64px;"><a href="javascript:void(removeItem(' + oRec.question_id + ',\'' + oRec.question_name + '\'))" class="btn btn-xs btn-danger" data-confirm="Are you sure?" data-method="delete" rel="nofollow">Delete</a></td>' +
						'</tr>';
				}
			}
			$("#tableContent tbody").html(htmlRows);
			_oMetaData.loadNavigations('retrieveList', 'ulPagination');
		},
		error: function(object, text, error) {
			alert(error);
		}
	});
};

removeItem = function(id, question_name) {
	if (confirm('Are you sure you want to delete ' + question_name + ' account?')) {
		alert('will be implemented');
	}
}

onLoginResponse = function(success, userInfo) {
	if (success) {
		if (!userInfo.isSuperUser) {
			alert('Not authorized');
			goDashboard();
			return;
		}
		fillClsVersionSelect("clsVersion");
		retrieveList(0);
	}
	displaySessionMessage(_oUserInfo.sessionMessage);
}

_oUserInfo.getLogon(true, onLoginResponse);
