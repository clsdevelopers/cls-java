var _oUserInfo = new UserInfo();
var _oMetaData = new MetaData(10, true, 'onLimitChange');

// var LIMIT = 10;

onLimitChange = function(oSelect) {
	var sLimit = oSelect.options[oSelect.selectedIndex].value;
	var iLimit = parseInt(sLimit);
	if (_oMetaData.limit != iLimit) {
		_oMetaData.limit = iLimit;
		retrieveList(1);
	}
}

retrieveList = function(offset) {
	var page = getPagePath();
	var sortOrder = $('#sort_order').val();
	var data =
		{'do': 'regulations-list',
		query: $('#query').val(),
		sort_field: $("input:radio[name ='sort_field']:checked").val(), 
		sort_order: sortOrder,			
		limit: _oMetaData.limit, offset: offset};
	var instance = this;
	$.ajax({
		url: getAppPath() + 'page',
		data: data,
		cache: false,
		dataType: 'json',
		success: function(data) {
			var htmlRows = '';
			_oMetaData.loadByJson(data.meta);
			if (data.data) {
				var aRecords = data.data;
				for (var iRow = 0; iRow < aRecords.length; iRow++) {
					var oRec = aRecords[iRow];
					htmlRows += '<tr>' +
						'<td>' + oRec.regulation_name + '</td>' +
						'<td>' + oRec.regulation_title + '</td>' +
						'<td>' + (oRec.regulation_url ? '<a href="' + oRec.regulation_url + '" target="_blank">' + oRec.regulation_url + '</a>' : '') + '</td>' +
						'<td align="right">' + oRec.ClauseCount.formatMoney(0, '.', ',') + '</td>' +
						'<td align="right">' + oRec.AgencyCount.formatMoney(0, '.', ',') + '</td>' +
						'</tr>';
				}
			}
			$("#tableContent tbody").html(htmlRows);
			_oMetaData.loadNavigations('retrieveList', 'ulPagination');			
		},
		error: function(object, text, error) {
			alert(error);
		}
	});
};

onLoginResponse = function(success, userInfo) {
	if (success) {
		if (!userInfo.isSuperUser) {
			alert('Not authorized');
			goDashboard();
			return;
		}
		retrieveList(0);
	}
	displaySessionMessage(_oUserInfo.sessionMessage);
}

_oUserInfo.getLogon(true, onLoginResponse);
