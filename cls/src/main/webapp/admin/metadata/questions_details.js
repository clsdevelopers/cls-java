var _oUserInfo = new UserInfo();

var _sMode = getUrlParameter('mode');
var _sId = getUrlParameter('id');

displayQuestionDetails = function(data) {
	var record = data.data; // jQuery.parseJSON(data.data);
	// $('#divEdit').remove();
	var htmlChoices = "";
	var htmlCondition = "";
	var htmlCondition2 = "";
	var htmlPres = "";
	
	var oQuestionChoices = record.Question_Choices;
	if (oQuestionChoices != null) {
		for (var iChoice = 0; iChoice < oQuestionChoices.length; iChoice++) {
			if (iChoice == 0)
				htmlChoices = oQuestionChoices[0];
			else
				htmlChoices += '<br />' + oQuestionChoices[iChoice];
		}
	}
	
	var oPrescriptions = record.Prescriptions;
	if (oPrescriptions != null) {
		var prevChoiceText = "";
		var sChoiceText = "";
		var sPresName = "";
		var sPresUrl = "";
		var sPresUId = "";
		for (var iPres = 0; iPres < oPrescriptions.length; iPres++) {
			sPresName = ((oPrescriptions[iPres].prescription_name == null)? "&nbsp;": oPrescriptions[iPres].prescription_name);
			// sPresUrl = ((oPrescriptions[iPres].prescription_url == null)? "&nbsp;": oPrescriptions[iPres].prescription_url);
			sPresUId = ((oPrescriptions[iPres].prescription_id == null)? "&nbsp;": oPrescriptions[iPres].prescription_id);
			if ((sPresUId != "&nbsp;") && (sPresUId != ''))
				sPresName = '<a href="prescriptions_details.html?id=' + sPresUId + '">' + sPresName + '</a>';
			// if ((sPresUrl != "&nbsp;") && (sPresUrl != ''))
			// 		sPresName = '<a href="' + sPresUrl + '">' + sPresName + '</a>';
			if (iPres == 0) {
				htmlPres = '<tr><th>Choices</th>' +
					'<th> Prescriptions </th>';
				htmlPres += '<tr>' +
					'<td>' + oPrescriptions[0].choice_text + '</td>' +
					'<td>' + sPresName + '</td>' +
					'</tr>';
				prevChoiceText = oPrescriptions[0].choice_text;
			} else {
				var sChoiceText = ((oPrescriptions[iPres].choice_text == prevChoiceText)? "&nbsp;" : oPrescriptions[iPres].choice_text);
				htmlPres += '<tr>' +
				'<td>' + sChoiceText + '</td>' +
				'<td>' + sPresName + '</td>' +
				'</tr>';
				prevChoiceText = oPrescriptions[iPres].choice_text;
			}	
		}
	}	

	
	$('#h3Title').html('View Questions');
	$('#tdQuestionId').html(record.question_id);
	$('#tdQuestionCode').html(record.question_code);
	$('#tdQuestionGroup').html(record.question_group);
	$('#tdQuestionType').html(record.question_type);
	$('#tdQuestionGroupName').html(record.question_group_name);
	$('#tdQuestionTypeName').html(record.question_type_name);
	$('#tdQuestionText').html(record.question_text);
	//$('#tdClauseVersionId').html(record.clause_version_id);
	$('#tdClauseVersionName').html(record.clause_version_name);
	$('#tdIActive').html(record.is_active);
	$('#tdDefaultBaseline').html(record.default_baseline);
	$('#tdCreatedAt').html(longToDateTimeStr(record.created_at));
	$('#tdUpdatedAt').html(longToDateTimeStr(record.updated_at));
	// $('#tdChoices').html(htmlChoices);
	
	$('#tdConditionText').html(record.original_condition_text);
	$('#tdConditionToQuestion').html(record.condition_to_question);
	$('#tdSequence').html(record.question_condition_sequence);
	
	$('#tablePrescription').html(htmlPres);
	
	// var htmlRows = getLogTableRows(record.UserLogs);
	// var htmlRows = "";
	// $("#tableAuditEventsForView tbody").html(htmlRows);
	// $('#linkEdit').prop("href", 'org_details.html?id=' + _sId + '&mode=edit');
	$('#divView').show();
}

retrieveQuestionDetails = function() {
	if (!_sId)
		return;
	var page = getPagePath();
	var data = {'do': 'question-detail-get', id: _sId, mode: _sMode};
	var instance = this;
	$.ajax({
		url: getAppPath() + 'page',
		data: data,
		cache: false,
		dataType: 'json',
		success: function(data) {
			var htmlRows = '';
			if (data) {
				// if ('edit' == _sMode)
				// editQuestionDetails(data);
				//else
				displayQuestionDetails(data);
			} else {
				alert('Unable to retrieve data.');
				//goDashboard();
			}
		},
		error: function(object, text, error) {
			alert(error);
			//goDashboard();
		}
	});
}

onLoginResponse = function(success, userInfo) {
	if (success) {
		if (!userInfo.isSuperUser) {
			alert('Not authorized');
			goDashboard();
			return;
		}
		retrieveQuestionDetails();
		displaySessionMessage(userInfo.sessionMessage);
	} else
		goHome();
}

_oUserInfo.getLogon(true, onLoginResponse);