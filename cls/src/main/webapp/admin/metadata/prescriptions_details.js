var _oUserInfo = new UserInfo();

var _sMode = getUrlParameter('mode');
var _sId = getUrlParameter('id');

function checkNull(sData) {
	if ((sData == null) || (sData === ""))
		return "&nbsp;";
	else
		return sData;
}

displayPrescriptionDetails = function(data) {
	var record = data.data; // jQuery.parseJSON(data.data);
	var sUrl = "";
	var htmlClauses = "";
	var htmlQuestions = "";
	// $('#divEdit').remove();
	
	if ((record.prescription_url != null) && (record.prescription_url != ""))
		sUrl = '<a href="' + record.prescription_url + '" target="_blank">' + record.prescription_url + '</a>';
	else
		sUrl = "";	
	
	var oClauses = record.Clauses;
	if (oClauses != null) {
		for (var iFill = 0; iFill < oClauses.length; iFill++) {
			var sClauseId = ((oClauses[iFill].clause_id == null)? "&nbsp;": oClauses[iFill].clause_id);
			if ((sClauseId != "&nbsp;") && (sClauseId != ''))
				sClauseId = '<a href="clauses_details.html?id=' + sClauseId + '">' + sClauseId + '</a>';
			if (iFill == 0) {
				htmlClauses = '<tr><th>Clause ID</th>' +
					'<th>Clause Name</th>' +
					'<th>Clause Title</th>';				
			}	
			htmlClauses += '<tr>' +
				'<td>' + sClauseId + '</td>' +
				'<td>' + checkNull(oClauses[iFill].clause_name) + '</td>' +
				'<td>' + checkNull(oClauses[iFill].clause_title) + '</td>' +
				'</tr>';
		}
	}
	
	var oQuestions = record.Questions;
	if (oQuestions != null) {
		for (var iFill = 0; iFill < oQuestions.length; iFill++) {
			var sQuestionId = ((oQuestions[iFill].question_id == null)? "&nbsp;": oQuestions[iFill].question_id);
			if ((sQuestionId != "&nbsp;") && (sQuestionId != ''))
				sQuestionId = '<a href="questions_details.html?id=' + sQuestionId + '">' + sQuestionId + '</a>';
			if (iFill == 0) {
				htmlQuestions = '<tr><th>Question ID</th>' +
					'<th>Question Code</th>' +
					'<th>Question Text</th>' +
					'<th>Choice Text</th>';				
			}	
			htmlQuestions += '<tr>' +
				'<td>' + sQuestionId + '</td>' +
				'<td>' + checkNull(oQuestions[iFill].question_code) + '</td>' +
				'<td>' + checkNull(oQuestions[iFill].question_text) + '</td>' +
				'<td>' + checkNull(oQuestions[iFill].choice_text) + '</td>' +				
				'</tr>';
		}
	}
	
	$('#h3Title').html('View Prescription');
		
	$('#tdPrescriptionId').html(record.prescription_id);
	$('#tdPrescriptionName').html(record.prescription_name);
	$('#tdRegulation').html(record.regulation_name);	
	$('#tdURL').html(sUrl);
	$('#tdContent').html(record.prescription_content);	
	$('#tableClauses').html(htmlClauses);
	$('#tableQuestions').html(htmlQuestions);
	$('#tdCreatedAt').html(longToDateTimeStr(record.created_at));
	$('#tdUpdatedAt').html(longToDateTimeStr(record.updated_at));

	$('#divView').show();
}

retrievePrescriptionDetails = function() {
	if (!_sId)
		return;
	var page = getPagePath();
	var data = {'do': 'prescription-detail-get', id: _sId, mode: _sMode};
	var instance = this;
	$.ajax({
		url: getAppPath() + 'page',
		data: data,
		cache: false,
		dataType: 'json',
		success: function(data) {
			if (data) {
				// if ('edit' == _sMode)
				// editClauseDetails(data);
				//else
				displayPrescriptionDetails(data);
			} else {
				alert('Unable to retrieve data.');
				//goDashboard();
			}
		},
		error: function(object, text, error) {
			alert(error);
			//goDashboard();
		}
	});
}

onLoginResponse = function(success, userInfo) {
	if (success) {
		if (!userInfo.isSuperUser) {
			alert('Not authorized');
			goDashboard();
			return;
		}
		retrievePrescriptionDetails();
		displaySessionMessage(userInfo.sessionMessage);
	} else
		goHome();
}

_oUserInfo.getLogon(true, onLoginResponse);