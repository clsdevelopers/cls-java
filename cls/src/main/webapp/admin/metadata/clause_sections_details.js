var _oUserInfo = new UserInfo();

var _sMode = getUrlParameter('mode');
var _sId = getUrlParameter('id');

function checkNull(sData) {
	if ((sData == null) || (sData === ""))
		return "&nbsp;";
	else
		return sData;
}

displaySectionDetails = function(data) {
	var record = data.data; // jQuery.parseJSON(data.data);
	var htmlClauses = "";
	// $('#divEdit').remove();
	
	var oClauses = record.Clauses;
	if (oClauses != null) {
		for (var iFill = 0; iFill < oClauses.length; iFill++) {
			var sClauseId = ((oClauses[iFill].clause_id == null)? "&nbsp;": oClauses[iFill].clause_id);
			if ((sClauseId != "&nbsp;") && (sClauseId != ''))
				sClauseId = '<a href="clauses_details.html?id=' + sClauseId + '">' + sClauseId + '</a>';
			if (iFill == 0) {
				htmlClauses = '<tr><th>Clause ID</th>' +
					'<th>Clause Name</th>' +
					'<th>Clause Title</th>';				
			}	
			htmlClauses += '<tr>' +
				'<td>' + sClauseId + '</td>' +
				'<td>' + checkNull(oClauses[iFill].clause_name) + '</td>' +
				'<td>' + checkNull(oClauses[iFill].clause_title) + '</td>' +
				'</tr>';
		}
	}
	
	$('#h3Title').html('View Clause Section');
		
	$('#tdSectionId').html(record.clause_section_id);
	$('#tdSectionCode').html(record.clause_section_code);
	$('#tdSectionHeader').html(record.clause_section_header);
	$('#tableClauses').html(htmlClauses);
	$('#tdCreatedAt').html(longToDateTimeStr(record.created_at));
	$('#tdUpdatedAt').html(longToDateTimeStr(record.updated_at));

	$('#divView').show();
}

retrieveSectionDetails = function() {
	if (!_sId)
		return;
	var page = getPagePath();
	var data = {'do': 'clausesection-detail-get', id: _sId, mode: _sMode};
	var instance = this;
	$.ajax({
		url: getAppPath() + 'page',
		data: data,
		cache: false,
		dataType: 'json',
		success: function(data) {
			if (data) {
				// if ('edit' == _sMode)
				// editClauseDetails(data);
				//else
				displaySectionDetails(data);
			} else {
				alert('Unable to retrieve data.');
				//goDashboard();
			}
		},
		error: function(object, text, error) {
			alert(error);
			//goDashboard();
		}
	});
}

onLoginResponse = function(success, userInfo) {
	if (success) {
		if (!userInfo.isSuperUser) {
			alert('Not authorized');
			goDashboard();
			return;
		}
		retrieveSectionDetails();
		displaySessionMessage(userInfo.sessionMessage);
	} else
		goHome();
}

_oUserInfo.getLogon(true, onLoginResponse);