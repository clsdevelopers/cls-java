var _oUserInfo = new UserInfo();
var _oMetaData = new MetaData(10, true, 'onLimitChange');

onLimitChange = function(oSelect) {
	var sLimit = oSelect.options[oSelect.selectedIndex].value;
	var iLimit = parseInt(sLimit);
	if (_oMetaData.limit != iLimit) {
		_oMetaData.limit = iLimit;
		retrieveList(1);
	}
}

viewRecordMsg = function() {
	alert('will be implemented');
}

adjustConditionText = function(OriginalConditions) {
	
	// Programmatically split the condition list field,
	// building an HTML string that can be displayed. 
	var htmlConditions = "";

	if (OriginalConditions == null)
		return '';
	
	var aCondition = OriginalConditions.split("(");
	for (var iCond = 0; iCond < aCondition.length; iCond++) {
	
		if (aCondition[iCond].length > 2) {
			if ((aCondition[iCond][1] == ')') 
			&& (/[A-Z]/.test(aCondition[iCond][0]))){
				if (htmlConditions.length == 0)
					htmlConditions += '(' + aCondition[iCond];
				else
					htmlConditions += '<br />(' + aCondition[iCond];
			} 								
			else
				htmlConditions += '(' + aCondition[iCond];
			}													
		else {													
			htmlConditions +=  aCondition[iCond] ;				
		}							
	}
	
	
	if (htmlConditions) {
		//var result = htmConditions.replace(new RegExp('\\n?\n','g'), '<br />'); // htmConditions.split("\n").join("<br />"); // htmConditions.replace(new RegExp('\r?\n','g'), '<br />'); // htmConditions.replace(/(?:\r\n|\r|\n)/g, '<br/>');
		return htmlConditions.replace(/\\n/g, "<br />");
	} else
		return '';
}



function formatClauseTitle(pTitle, pUrl) {
	if (pUrl) {
		return '<a href="' + pUrl + '" title="Visit the site" target="_blank">' + pTitle + '</a>';
	} else
		return pTitle;
}

retrieveList = function(offset) {
	var page = getPagePath();
	var data =
		{'do': 'clause-list',
		query: $('#query').val(),
		cls_version: $('#clsVersion').val(),
		section: $('#section').val(),
		sort_field: $("input:radio[name ='sort_field']:checked").val(), // $('#sort_field').val(),
		sort_order: $('#sort_order').val(),
		limit: _oMetaData.limit, offset: offset};
	var instance = this;
	$.ajax({
		url: getAppPath() + 'page',
		data: data,
		cache: false,
		dataType: 'json',
		success: function(data) {
			var htmlRows = '';
			_oMetaData.loadByJson(data.meta);
			if (data.data) {
				var aRecords = data.data;
				for (var iRow = 0; iRow < aRecords.length; iRow++) {
					var oRec = aRecords[iRow];
					var oConditions = oRec.clause_conditions;
										
					/*
					var htmConditions = "";
					for (var iCondition = 0; iCondition < oConditions.length; iCondition++) {
						if (iCondition == 0)
							htmConditions = oConditions[0];
						else
							htmConditions += '<br />' + oConditions[iCondition];
					}
					*/
					htmlRows += '<tr>' +
						// '<td>' + oRec.Clause_Id + '</td>' +
						'<td><a href="clauses_details.html?id=' + oRec.clause_id + '">' + oRec.clause_id + '</a></td>' +
						'<td class="nowrap"><a href="clauses_details.html?id=' + oRec.clause_id + '">' + oRec.clause_name + '</a></td>' +
						// '<td class="nowrap"><a href="javascript:(viewRecordMsg())">' + oRec.clause_name + '</a></td>' +
						'<td>' + formatClauseTitle(oRec.clause_title, oRec.clause_url) + '</td>' +
						'<td>' + oRec.clause_version_name + '</td>' +
						'<td class="nowrap">' + (oRec.Section ? oRec.Section : '') + '</td>' +
						'<td>' + oRec.provision_or_clause + '</td>' +
						'<td>' + oRec.inclusion_cd + '</td>' +
						'<td style="width:35%;">' + adjustConditionText(oRec.clause_conditions) + '</td>' +						
						'<td style="width:10%;">' +  oRec.clause_rule + '</td>' +
						// '<td style="width:50px;"><a href="clauses_details.html?id=' + oRec.Clause_Id + '&mode=edit" class="btn btn-default btn-xs">Edit</a></td>' +
						// '<td style="width:50px;"><a href="javascript:(viewRecordMsg())" class="btn btn-default btn-xs">Edit</a></td>' +
						// '<td style="width:64px;"><a href="javascript:void(removeItem(' + oRec.Clause_Id + ',\'' + oRec.clause_name.replace(/'/g, '&quot;') + '\'))" class="btn btn-xs btn-danger" data-confirm="Are you sure?" data-method="delete" rel="nofollow">Delete</a></td>' +
						'</tr>';
				}
			}
			$("#tableContent tbody").html(htmlRows);
			_oMetaData.loadNavigations('retrieveList', 'ulPagination');
		},
		error: function(object, text, error) {
			alert(error);
		}
	});
};

removeItem = function(id, clause_name) {
	if (confirm('Are you sure you want to delete ' + clause_name + ' clause?')) {
		alert('will be implemented');
	}
}

onLoginResponse = function(success, userInfo) {
	if (success) {
		if (!userInfo.isSuperUser) {
			alert('Not authorized');
			goDashboard();
			return;
		}
		fillClsVersionSelect("clsVersion");
		retrieveList(0);
	}
	displaySessionMessage(_oUserInfo.sessionMessage);
}

_oUserInfo.getLogon(true, onLoginResponse);
