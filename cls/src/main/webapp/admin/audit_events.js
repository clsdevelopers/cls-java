var _oUserInfo = new UserInfo();
var _oMetaData = new MetaData(10, true, 'onLimitChange');

onLimitChange = function(oSelect) {
	var sLimit = oSelect.options[oSelect.selectedIndex].value;
	var iLimit = parseInt(sLimit);
	if (_oMetaData.limit != iLimit) {
		_oMetaData.limit = iLimit;
		retrieveList(1);
	}
}

toggleEventDetails = function(id) {
	$('#' + id).toggle();
}

getLogTableRows = function(userLogs) {
	var htmlRows = '';
	for (var iRec = 0; iRec < userLogs.length; iRec++) {
		var oRec = userLogs[iRec];
		var sTrId = "trLog" + oRec.audit_event_id;
		htmlRows +=
			'<tr>' +
				'<td style="white-space:nowrap">' + longToDateTimeStr(oRec.updated_at) + '</td>' +
				'<td>' + oRec.action + '</td>' +
				'<td>' + (oRec.ref_document_number ? oRec.ref_document_number : '&nbsp;') + '</td>' +
				'<td>' + (oRec.ref_user_name ? oRec.ref_user_name : '&nbsp;') + '</td>' +
				'<td><button class="btn btn-link audit-event-details-link" onclick="toggleEventDetails(\'' + sTrId + '\')" >Details</button></td>' +
			'</tr>' +
			'<tr id="' + sTrId + '" style="display:none">' + //  class="audit-event-additional-data"
				'<td colspan="3">' +
					'<table class="table">' +
						'<tr>' +
							'<td><strong>Initiated User</strong></td>' +
							'<td>' + (oRec.initiated_user_id ? '<a href="user_details.html?id=' + oRec.initiated_user_id + '">' + oRec.ref_user_name + '</a>' : oRec.ref_user_name) +'</td>' +
						'</tr>' +
						//'<tr>' +
						//	'<td><strong>User</strong></td>' +
						//	'<td><a href="../../admin/users/18.html">Bill  Chin</a></td>' +
						//'</tr>' +
					'</table>' +
					'<pre>' + oRec.additional_data + '</pre>' +
				'</td>' +
			'</tr>';
	}
	return htmlRows;
} 

retrieveList = function(offset, bClearValues) {
	var page = getPagePath();
	var data = {'do': 'auditEvent-list', 
			query: (bClearValues ? '' : $('#query').val()),
			action_type:  (bClearValues ? '' : $('#action_type').val()),
			audit_date:  (bClearValues ? '' : $('#audit_date').val()),
			limit: _oMetaData.limit, offset: offset};
	var instance = this;
	$.ajax({
		url: getAppPath() + 'page',
		data: data,
		cache: false,
		dataType: 'json',
		success: function(data) {
			var htmlRows = '';
			_oMetaData.loadByJson(data.meta);
			if (data.data) {
				var aRecords = data.data;
				htmlRows = getLogTableRows(aRecords);
			}
			$("#tableContent tbody").html(htmlRows);
			_oMetaData.loadNavigations('retrieveList', 'ulPagination');
		},
		error: function(object, text, error) {
			alert(error);
		}
	});
};

onLoginResponse = function(success, userInfo) {
	if (success) {
		if (!userInfo.isSuperUser) {
			alert('Not authorized');
			goDashboard();
			return;
		}
		retrieveList(0);
	}
	displaySessionMessage(_oUserInfo.sessionMessage);
}

_oUserInfo.getLogon(true, onLoginResponse);
