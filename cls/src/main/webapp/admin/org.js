var _oUserInfo = new UserInfo();
var _oMetaData = new MetaData();

var LIMIT = 10;

retrieveList = function(offset) {
	var page = getPagePath();
	var data = {'do': 'org-list',
			'query': $('#query').val(), 
			'sort_field': $("input:radio[name ='sort_field']:checked").val(),
			'sort_order': $('#sort_order').val(),
			'limit': LIMIT,
			'offset': offset};
	var instance = this;
	$.ajax({
		url: getAppPath() + 'page',
		data: data,
		cache: false,
		dataType: 'json',
		success: function(data) {
			_oMetaData.loadByJson(data.meta);
			var htmlRows = '';
			if (data.data) {
				var aRecords = data.data;
				for (var iRow = 0; iRow < aRecords.length; iRow++) {
					var oRec = aRecords[iRow];
					var canDelete = (oRec.UserCount + oRec.RegulationCount) < 1;
					htmlRows += '<tr>' +
						'<td valign="top">' + (oRec.Dept_Name ? oRec.Dept_Name : '') + '</td>' +
						'<td valign="top"><a href="org_details.html?id=' + oRec.Org_Id + '">' + oRec.Org_Name + '</a></td>' +
						// '<td valign="top" class="width-300">' + (oRec.Org_Description ? oRec.Org_Description : '') + '</td>' +
						'<td valign="top" align="right">' + oRec.UserCount + '</td>' +
						'<td valign="top" align="right">' + oRec.RegulationCount + '</td>' +
						//'<td valign="top" style="width:50px;"><a href="org_details.html?id=' + oRec.Org_Id + '&mode=edit" class="btn btn-default btn-xs">Edit</a></td>' +
						'<td valign="top"><button onclick="location.href=\'org_details.html?id=' + oRec.Org_Id + '&mode=edit\'" class="btn btn-default btn-xs">Edit</button></td>' +
						'<td valign="top">' +
						(canDelete ?
							'<a href="javascript:void(removeOrg(' + oRec.Org_Id + ',\'' + oRec.Org_Name + '\'))" class="btn btn-xs btn-danger" data-confirm="Are you sure?" data-method="delete" rel="nofollow">Delete</a>'
							: '')
						'</td>' +
						'</tr>';
				}
			}
			$("#tableOrg tbody").html(htmlRows);
			_oMetaData.loadNavigations('retrieveList', 'ulPagination');
		},
		error: function(object, text, error) {
			alert(error);
		}
	});
};

removeOrg = function(id, orgName) {
	if (confirm('Are you sure you want to delete ' + orgName + ' organization?')) {
		alert('will be implemented')
		/*
		var data = {'do': 'user-remove', id: id};
		$.ajax({
			url: getAppPath() + 'page',
			data: data,
			cache: false,
			dataType: 'json',
			success: function(data) {
				if ('success' == data.status) {
					retrieveList(1);
					alert(email + ' account is removed.');
				} else {
					alert(data.message);
				}
			},
			error: function(object, text, error) {
				//alert(error);
				goDashboard();
			}
		});
		*/
	}
}

onLoginResponse = function(success, userInfo) {
	if (success) {
		if (!userInfo.isSuperUser) {
			alert('Not authorized');
			goDashboard();
			return;
		}
		retrieveList(0);
	}
	displaySessionMessage(_oUserInfo.sessionMessage);
}

_oUserInfo.getLogon(true, onLoginResponse);
