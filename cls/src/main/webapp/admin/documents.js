var _oUserInfo = new UserInfo();
var _oMetaData = new MetaData();

var LIMIT = 0;

retrieveList = function(offset) {
	var page = getPagePath();
	var data = {'do': 'document-list'}; //, limit: LIMIT, offset: offset};
	var instance = this;
	$.ajax({
		url: getAppPath() + 'page',
		data: data,
		cache: false,
		dataType: 'json',
		success: function(data) {
			_oMetaData.loadByJson(data.meta);
			var htmlRows = '';
			if (data.data) {
				var aRecords = data.data;
				for (var iRow = 0; iRow < aRecords.length; iRow++) {
					var oRec = aRecords[iRow];
					htmlRows += '<tr>' +
						'<td>' + oRec.document_type + '</td>' +
						'<td><a href="document_details.html?id=' + oRec.id + '">' + oRec.acquisition_title + '</a></td>' +
						'<td>' + oRec.document_number + '</td>' +
						'<td>' + longToDateTimeStr(oRec.created_at) + '</td>' +
						'<td>' + (oRec.UserName ? oRec.UserName : '') + '</td>' +
						'<td>' + '' + '</td>' +
						'<td>' + oRec.interview_complete + '</td>' +
						'<td><a href="document_details.html?id=' + oRec.id + '&mode=edit" class="btn btn-default btn-xs">Edit</a></td>' +
						'<td>' +
							'<a href="javascript:void(removeDocument(' + oRec.id + ',\'' + oRec.acquisition_title.replace(/'/g, '&quot;') + '\'))" class="btn btn-xs btn-danger" data-confirm="Are you sure?" data-method="delete" rel="nofollow">Delete</a>' +
						'</td>' +
						'</tr>';
				}
			}
			$("#tableContent tbody").html(htmlRows);
			//_oMetaData.loadNavigations('retrieveList', 'ulPagination');
		},
		error: function(object, text, error) {
			alert(error);
		}
	});
};

removeDocument = function(id, acquisition_title) {
	if (confirm('Are you sure you want to delete "' + acquisition_title + '" document?')) {
		alert('will be implemented')
		/*
		var data = {'do': 'user-remove', id: id};
		$.ajax({
			url: getAppPath() + 'page',
			data: data,
			cache: false,
			dataType: 'json',
			success: function(data) {
				if ('success' == data.status) {
					retrieveList(1);
					alert(email + ' account is removed.');
				} else {
					alert(data.message);
				}
			},
			error: function(object, text, error) {
				//alert(error);
				goDashboard();
			}
		});
		*/
	}
}

onLoginResponse = function(success, userInfo) {
	if (success) {
		retrieveList(0);
	}
	displaySessionMessage(_oUserInfo.sessionMessage);
}

_oUserInfo.getLogon(true, onLoginResponse);
