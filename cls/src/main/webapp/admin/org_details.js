var _oUserInfo = new UserInfo();

var _sMode = getUrlParameter('mode');
var _sId = getUrlParameter('id');

$(function(){
	var allRequiredCheckboxes = $(':checkbox[required]');
	var checkboxNames = [];

	for (var i = 0; i < allRequiredCheckboxes.length; ++i){
		var name = allRequiredCheckboxes[i].name;
		checkboxNames.push(name);
	}

	checkboxNames = checkboxNames.reduce(function(p, c) {
		if (p.indexOf(c) < 0) p.push(c);
			return p;
	}, []);

	for (var i in checkboxNames){
	    !function(){
	    	var name = checkboxNames[i];
	    	var checkboxes = $('input[name="' + name + '"]');
	    	checkboxes.change(function(){
	    		if(checkboxes.is(':checked')) {
	    			checkboxes.removeAttr('required');
	    		} else {
	    			checkboxes.attr('required', 'required');
	    		}
	    	});
	    }();
	}
});

resetCheckBoxesForLoaded = function() {
	var allRequiredCheckboxes = $(':checkbox[required]');
	var checkboxNames = [];

	for (var i = 0; i < allRequiredCheckboxes.length; ++i){
		var name = allRequiredCheckboxes[i].name;
		if (checkboxNames.indexOf(name) < 0)
			checkboxNames.push(name);
	}

	for (var i in checkboxNames) {
		var name = checkboxNames[i];
		var checkboxes = $('input[name="' + name + '"]');
		var checked = false;
		for (var i = 0; i < checkboxes.length; ++i){
			var checkbox = checkboxes[i];
			if (checkbox.checked) {
				checked = true;
				break;
			}
		}
		if (checked)
			checkboxes.removeAttr('required');
		else
			checkboxes.attr('required', 'required');
	}
}

retrieveDeptList = function() {
	var page = getPagePath();
	var data = {'do': 'dept-list'};
	var instance = this;
	$.ajax({
		url: getAppPath() + 'page',
		data: data,
		cache: false,
		dataType: 'json',
		success: function(data) {
			// _oMetaData.loadByJson(data.meta);
			var htmlRows = '<option value=""></option>';
			if (data.data) {
				var aRecords = data.data;
				for (var iRow = 0; iRow < aRecords.length; iRow++) {
					var oRec = aRecords[iRow];
					// <option value="1">Commission on Civil Rights</option>
					htmlRows += '<option value="' + oRec.Dept_Id + '">' + oRec.Dept_Name + '</option>';
				}
			}
			$("#orgs_dept_id").html(htmlRows);
			// _oMetaData.loadNavigations('retrieveList', 'ulPagination');
		},
		error: function(object, text, error) {
			alert(error);
		}
	});
};

retrieveRegulList = function() {
	var page = getPagePath();
	var data =
		{'do': 'regulations-list'}; // ,limit: LIMIT, offset: offset};
	var instance = this;
	$.ajax({
		url: getAppPath() + 'page',
		data: data,
		cache: false,
		dataType: 'json',
		success: function(data) {
			var htmlRows = '';
			// _oMetaData.loadByJson(data.meta);
			if (data.data) {
				var aRecords = data.data;
				for (var iRow = 0; iRow < aRecords.length; iRow++) {
					var oRec = aRecords[iRow];					
					// <label><input type="checkbox" name="orgs_regulation_ids" required value="1" id="orgs_regulation_ids_0">System Administrator</label>
					//<br>					
					htmlRows += '<label><input type="checkbox" name="orgs_regulation_ids" required value="' + oRec.regulation_id + '" id="orgs_regulation_ids_' + oRec.regulation_id + '"> ' + oRec.regulation_name + '</label>' +
								'<br>';
				}
			}
			$("#orgs_regulation_checkboxes").html(htmlRows);
		},
		error: function(object, text, error) {
			alert(error);
		}
	});
};

getLogTableRows = function(userLogs) {
	var htmlRows = '';
	for (var iRec = 0; iRec < userLogs.length; iRec++) {
		var oRec = userLogs[iRec];
		var sTrId = "trLog" + oRec.audit_event_id;
		htmlRows +=
			'<tr>' +
				'<td style="white-space:nowrap">' + longToDateTimeStr(oRec.updated_at) + '</td>' +
				'<td>' + oRec.action + '</td>' +
				'<td><button class="btn btn-link" onClick="toggleEventDetails(\'' + sTrId + '\')">Details</button></td>' +
			'</tr>' +
			'<tr id="' + sTrId + '" style="display:none">' + //  class="audit-event-additional-data"
				'<td colspan="3">' +
					'<table class="table">' +
						'<tr>' +
							'<td><strong>Initiated user</strong></td>' +
							'<td><a href="user_details.html?id=' + oRec.initiated_user_id + '">' + oRec.InitiatedUserName + '</a></td>' +
						'</tr>' +
					'</table>' +
					'<pre>' + oRec.additional_data + '</pre>' +
				'</td>' +
			'</tr>';
	}
	return htmlRows;
} 


displayOrgDetails = function(data) {
	var record = data.data; // jQuery.parseJSON(data.data);
	$('#divEdit').remove();
	
	$('#h3Title').html('View Organization');
	$('#tdDepartment').html(record.deptName);
	$('#tdName').html(record.Org_Name);
	$('#tdDescription').html(record.Org_Description);
	$('#tdRegulations').html(record.Regulations);
	
	var htmlRows = getLogTableRows(record.UserLogs);
	// var htmlRows = "";
	$("#tableAuditEventsForView tbody").html(htmlRows);
	$('#linkEdit').prop("href", 'org_details.html?id=' + _sId + '&mode=edit');
	$('#divView').show();
}

editOrgDetails = function(data) {
	var record = data.data; // jQuery.parseJSON(data.data);
	$('#divView').remove();
	
	$('#h3Title').html('Edit Organization');
	//$('#form').prop("target", getAppPath() + '/page?do=user-detail-mod&id=' + _sId);
	$('#form').submit(function(e){
		if (validateForm())
			submitForm();
		return false;
	});
	$('#linkShow').prop("href", 'org_details.html?id=' + _sId);
	
	$('#orgs_dept_id').val(record.Dept_Id); 
	$('#orgs_name').val(record.Org_Name);
	$('#orgs_description').val(record.Org_Description);
	
	if (record.isREGUL_FAR)
		$('#orgs_regulation_ids_1').prop( "checked", true );
	if (record.isREGUL_DFARS)
		$('#orgs_regulation_ids_2').prop( "checked", true );
	if (record.isREGUL_AFFARS)
		$('#orgs_regulation_ids_3').prop( "checked", true );
	if (record.isREGUL_AFARS)
		$('#orgs_regulation_ids_4').prop( "checked", true );
	if (record.isREGUL_NMCARS)
		$('#orgs_regulation_ids_5').prop( "checked", true );
	if (record.isREGUL_USSOCOM)
		$('#orgs_regulation_ids_6').prop( "checked", true );
	if (record.isREGUL_USTRANSCOM)
		$('#orgs_regulation_ids_7').prop( "checked", true );
	if (record.isREGUL_DARS)
		$('#orgs_regulation_ids_8').prop( "checked", true );
	
	resetCheckBoxesForLoaded();
	
	var htmlRows = getLogTableRows(record.UserLogs);
	// var htmlRows = "";
	$("#tableAuditEventsForEdit tbody").html(htmlRows);
	$('#divAuditForEdit').show();
	$('#divEdit').show();
}

function getSubmitData() {
	
	var regulationCheckboxes= document.getElementsByName('orgs_regulation_ids');
	var regulations = '';
	for (var i = 0; i < regulationCheckboxes.length; i++) {
		if (regulationCheckboxes[i].checked) {
			if (regulations == '')
				regulations = regulationCheckboxes[i].value;
			else
				regulations += ',' + regulationCheckboxes[i].value;
		}
	}
	var data = {'do': 'org-detail-' + _sMode,
			id: _sId,
			org_regul_ids: regulations,
			dept_id: $('#orgs_dept_id').val(),
			org_name: $('#orgs_name').val(),
			org_description: $('#orgs_description').val()
		};
	return data;
	
}

function validateForm() {
	var errorMessage = null;
	return true;
}

submitForm = function() {
	var data = getSubmitData();
	$.ajax({
		url: getAppPath() + 'page',
		data: data,
		cache: false,
		dataType: 'json',
		success: function(data) {
			if ('success' == data.status) {
				//alert('Saved.');
				location = 'orgs.html';
			} else {
				alert(data.message);
			}
		},
		error: function(object, text, error) {
			//alert(error);
			goDashboard();
		}
	});
}

toggleEventDetails = function(id) {
	$('#' + id).toggle();
}

retrieveOrgDetails = function() {
	if (!_sId)
		return;
	var page = getPagePath();
	var data = {'do': 'org-detail-get', id: _sId, mode: _sMode};
	var instance = this;
	$.ajax({
		url: getAppPath() + 'page',
		data: data,
		cache: false,
		dataType: 'json',
		success: function(data) {
			var htmlRows = '';
			if (data) {
				if ('edit' == _sMode)
					editOrgDetails(data);
				else
					displayOrgDetails(data);
			} else {
				alert('Unable to retrieve data.');
				//goDashboard();
			}
		},
		error: function(object, text, error) {
			alert(error);
			//goDashboard();
		}
	});
}

onLoginResponse = function(success, userInfo) {
	if (success) {
		if (!userInfo.isSuperUser) {
			alert('Not authorized');
			goDashboard();
			return;
		}
		if ('new' == _sMode) {
			retrieveDeptList();
			// retrieveRegulList();
			//$('#form').prop("target", getAppPath() + '/page?do=user-detail-new');
			
			 $('#form').submit(function(e){
				if (validateForm())
					submitForm();
				return false;
			 });
			
			$('#divView').remove();
			$('#h3Title').html('Create Organization');
			$('#btnSave').val('Create Org');
			$('#spanLinkShow').hide();
			$('#divEdit').show();
		} else {
			if ('edit' == _sMode)
				retrieveDeptList();
			retrieveOrgDetails();
		}
		displaySessionMessage(userInfo.sessionMessage);
	} else
		goHome();
}

_oUserInfo.getLogon(true, onLoginResponse);
