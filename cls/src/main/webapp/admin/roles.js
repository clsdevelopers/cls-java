var _oUserInfo = new UserInfo();

retrieveList = function() {
	var page = getPagePath();
	var data = {'do': 'role-list'};
	var instance = this;
	$.ajax({
		url: getAppPath() + 'page',
		data: data,
		cache: false,
		dataType: 'json',
		success: function(data) {
			var htmlRows = '';
			if (data.data) {
				var aRecords = data.data;
				for (var iRow = 0; iRow < aRecords.length; iRow++) {
					var oRec = aRecords[iRow];
					htmlRows += '<tr>' +
						'<td>' + oRec.RoleName + '</td>' +
						'<td>' + oRec.UserCount + '</td>' +
						'</tr>';
				}
			}
			$("#tableRoles tbody").html(htmlRows);
		},
		error: function(object, text, error) {
			alert(error);
		}
	});
};

onLoginResponse = function(success, userInfo) {
	if (success) {
		if (!userInfo.isSuperUser) {
			alert('Not authorized');
			goDashboard();
			return;
		}
		retrieveList();
	}
	displaySessionMessage(_oUserInfo.sessionMessage);
}

_oUserInfo.getLogon(true, onLoginResponse);
