var _oUserInfo = new UserInfo();
var isMacLike = navigator.platform.match(/(Mac|iPhone|iPod|iPad)/i)? true: false;

checkRequire = function(formEleId, formLabel) {
	var oInput = $('#' + formEleId);
	if (oInput) {
		var sValue = oInput.val();
		var sTrim = sValue.trim();
		if (sValue != sTrim)
			oInput.val(sTrim);
		if (sTrim) {
			return true;
		} else {
			oInput.focus();
			displaySessionMessage("The " + formLabel + " must not be empty");
			return false;
		}
	} else {
		return true;
	}
}

validateForm = function(form) {
	var emailId = $('#email');
	var emailValue = emailId.val();
	if ((!emailValue) || (!validateEmail(emailValue))) {
		emailId.focus();
		displaySessionMessage("The email address is not valid");
		return false;
	}
	
	

	if ((!checkRequire("first_name", "First name")) || 
		(!checkRequire("last_name", "Last name"))) {
		return false;
	}
	//CJ-1441
	var oFirstName = $('#first_name');
	var sFirstName = oFirstName.val();
	var check = XSS(sFirstName);
	if(check == -1){
		displaySessionMessage("First Name cannot contain special characters: < > / ; : @ # ");
		return false;
	}
	//CJ-1441
	var oLastName = $('#last_name');
	var sLastName = oLastName.val();
	var check = XSS(sLastName);
	if(check == -1){
		displaySessionMessage("Last Name cannot contain special characters: < > / ; : @ # ");
		return false;
	}
	
	
	var oNewPassword = $('#password');
	var sNesPassword = oNewPassword.val();
	var error = detectPasswordError(sNesPassword);
	if (error || (!sNesPassword)) {
		oNewPassword.focus();
		displaySessionMessage(error);
		return false;
	}
	var oNewPasswordAgain = $('#password_again');
	var sNewPasswordAgain = oNewPasswordAgain.val();
	if (sNewPasswordAgain != sNesPassword) {
		oNewPasswordAgain.focus();
		displaySessionMessage("Password and Confirm Password values do not match");
		return false;
	}
	
	if ((!checkRequire("dept_id", "Department")) || 
		(!checkRequire("org_id", "Agency"))) {
		return false;
	}
	return true;
}

function loadDepts(json, deptId) {
	var html = '<option value="">Please select</option>';
	for (var iData = 0; iData < json.length; iData++) {
		var oRec = json[iData];
		html += '<option value="' + oRec.Dept_Id + '"' + ((oRec.Dept_Id == deptId) ? ' selected' : '')
			+ '>' + oRec.Dept_Name + '</option>';
	}
	$('#dept_id').html(html);
}

OrgItem = function(Dept_Id, Org_Id, Org_Name) {
	this.deptId = Dept_Id;
	this.orgId = Org_Id;
	this.orgName = Org_Name;
}

OrgSet = function() {
	this.elements = new Array();
	this.loadJson = function(json){
		for (var iData = 0; iData < json.length; iData++) {
			var oOrgItem = new OrgItem(json[iData].Dept_Id, json[iData].Org_Id, json[iData].Org_Name);
			this.elements.push(oOrgItem);
		}
	}
};

var _orgArray = new OrgSet();

function onDeptSelect(sId, orgId) {
	if (!sId)		
		sId = $('#dept_id').val(); // oSelect.options[oSelect.selectedIndex].value;
	var shtml;
	if (sId) {
		shtml = '<option value="">Please select</option>';
		var aItems = _orgArray.elements;
		var bFound = false;
		for (var iData = 0; iData < aItems.length; iData++) {
			var oItem = aItems[iData];
			if (oItem.deptId == sId) {
				shtml += '<option value="' + oItem.orgId + '"' + ((oItem.orgId == orgId) ? ' selected' : '')
				+ '>' + oItem.orgName + '</option>';
				bFound = true;
			} else if (bFound)
				break;
		}
	} else {
		shtml = '<option value="">Select a department above at first</option>';
	}
	$('#org_id').html(shtml);
}

retrieveDeptOrg = function() {
	var page = getPagePath();
	var data = {'do': 'org-references'};
	var instance = this;
	$.ajax({
		url: getAppPath() + 'page',
		data: data,
		cache: false,
		dataType: 'json',
		success: function(data) {
			var htmlRows = '';
			if (data) {
				if ('success' == data.status) {
					var deptId = getUrlParameter('dept_id');
					var orgId = getUrlParameter('org_id');
					_orgArray.loadJson(data.data.Orgs);
					loadDepts(data.data.Depts, deptId);
					if (orgId) {
						onDeptSelect(null, orgId);
					}
					$("#dept_id").on('change', function() {
						// onDeptSelect();
						if (isMacLike)
							setTimeout(function() {onDeptSelect();}, 50);
						else
							onDeptSelect();
					});
				} else {
					alert(data.message);
				}
			} else {
				alert('Unable to retrieve data.');
				//goDashboard();
			}
		},
		error: function(object, text, error) {
			alert(error);
			//goDashboard();
		}
	});
};


function register() {
	if (!validateForm())
		return false;

	return true;
	/*
	var oEmail = $('#email');
	var oData = {	email: oEmail.val(),
					p: $('#password').val(),
					first_name: $('#first_name').val(),
					last_name: $('#last_name').val(),
					agency: $('#org_id').val()
					// agency: $('#agency').val()
				};
	$.ajax({      
		url: 'page?do=session-register',
		type: 'POST',
		data: oData,
		cache: false,
		dataType: 'json',
		success: function(data) 
		{
			if (data && ('success' == data.status)) {
				location = data.message;
			} else {
				displaySessionMessage(data.message ? data.message : "Unable to register.");
				oEmail.focus();
			}
		},
		error: function(object, text, error) 
		{
			alert(error);
		}
	});
	return false;
	*/
}

retrieveDeptOrg();
displayBottomFooter();

var message = getUrlParameter('message');
if (message) {
	$('#email').val(decodeURIComponent(getUrlParameter('email')));
	$('#first_name').val(decodeURIComponent(getUrlParameter('first_name')));
	$('#last_name').val(decodeURIComponent(getUrlParameter('last_name')));
	alert(decodeURIComponent(message));
}
