<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
	<title>CLS - Sitemap</title>
	<meta content="CLS Sitemap" name="Description">
	<meta content="CLS,sitemap" name="keywords">
	<link href="assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">
	<link href="assets/application.css" media="all" rel="stylesheet" type="text/css" />
	<!-- IE6-8 support of HTML5 elements -->
	<!--[if lt IE 9]>
	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
</head>
<body>
<div class='navbar navbar-default navbar-fixed-top' role='navigation'>
	<div class='container-fluid'>
		<div class='navbar-header'>
			<button class='navbar-toggle' data-target='.navbar-collapse' data-toggle='collapse' type='button'>
				<span class='sr-only'>Toggle navigation</span>
				<span class='icon-bar'></span>
				<span class='icon-bar'></span>
				<span class='icon-bar'></span>
			</button>
			<span class="navbar-brand" style="text-decoration:none">Clause Logic Service</span>
		</div>
		<div class='navbar-collapse collapse'> </div>
	</div>
</div>
<div class="container">
	<div class="row">
		<div id='container'>
			<h1 id="label-about">About Clause Logic Service</h1>
			<hr style="height: 10px">

			<div>

				<h3 id="label-Account">Account</h3>
				
				<p>All users have access to the following account pages:</p>
				
				<h4 id="label-Settings">Register</h4>
				<h4 id="label-Settings">Logon</h4>
				<ul>
					<li>Email</li>
					<li>CAC</li>
					<li>Smart Card</li>
				</ul>
				<h4 id="label-Settings">Logoff</h4>
				
				<hr style="height: 10px">

				<h3 id="label-Users">Users</h3>

				<p>Users have access to the following pages:</p>

				<h4 id="label-Dashboard">Dashboard</h4>
				<ul>
					<li><h5 id="label-Solicitations">Solicitations</h5>
						<ul><li>Clause Work Sheet</li><li>Linked Award</li></ul>
					</li>
					<li><h5 id="label-Awards">Awards</h5>
						<ul><li>Clause Work Sheet</li></ul>
					</li>
					<li>Orders</li>
				</ul>
				
				<h4 id="label-Settings">Settings</h4>
				<ul>
					<li>Personal Information</li>
					<li>Change Password</li>
					<li>Assign CAC</li>
				</ul>
				
				<h4 id="label-Support">Support</h4>
				<ul>
					<li>Tutorial</li>
				</ul>
				
				<h4 id="label-PrintMetadata">Print Metadata</h4>
				<ul>
					<li>Clauses</li>
				</ul>
				
				<hr style="height: 10px">
				
				<h3 id="label-Admins">Administrators</h3>

				<p>Administrators have access to the following pages:</p>
				
				<h4 id="label-Audit">Audit Events</h4>
				<h4 id="label-Metadata">Metadata</h4>
				<ul>
					<li>Questions</li>
					<li>Clauses</li>
					<li>Sections</li>
					<li>Prescriptions</li>
					<li>Versions</li>
					<li>Regulations</li>
				</ul>
				<h4 id="label-Oauth">OAuth Permissions</h4>
				<h4 id="label-Orgs">Organizations</h4>
				<h4 id="label-Oauth">Reports</h4>
				<ul>
					<li>Database Statistics</li>
				</ul>
				<h4 id="label-Roles">Roles</h4>
				<h4 id="label-Users">Users</h4>

				<hr style="height: 10px">

				<h3 id="label-Ideas">IDEAS</h3>
				
				<p>IDEAS users have access to the following account pages:</p>
				
				<h4 id="label-API-Solicitations">Solicitations</h4>
				<h4 id="label-API-Awards">Awards</h4>
				<h4 id="label-API-LinkAwards">Linked Awards</h4>
				<h4 id="label-API-Orders">Orders</h4>
				<h4 id="label-API-PrintMetadata">Print Metadata</h4>
				
			</div>
		</div>
	</div>
</div>
<div class="margin-top-50">
	<div class="footer navbar-fixed-bottom cls-footer">
		<a href="index.html" title="Go to the home page">Home</a> 
		<a href="javascript:history.back();" title="Go to the previous page">Back</a>
	</div>
</div>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="assets/jquery-1.11.2.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src=".bootstrap/js/bootstrap.min.js"></script>

</body>
</html>
