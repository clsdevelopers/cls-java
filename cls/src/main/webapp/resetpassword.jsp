<!DOCTYPE html>
<html lang='en'>
<head>
	<meta charset='utf-8'>
	<meta content='IE=Edge,chrome=1' http-equiv='X-UA-Compatible'>
	<meta content='width=device-width, initial-scale=1.0' name='viewport'>
	<title>Reset Password Details</title>
	<!-- Le HTML5 shim, for IE6-8 support of HTML elements -->
	<!--[if lt IE 9]>
	<script src="../..//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.6.1/html5shiv.js" type="text/javascript"></script>
	<![endif]-->
    <link href="assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="assets/bootstrapvalidator/css/bootstrapValidator.min.css" rel="stylesheet">
	<link href="assets/application.css" media="all" rel="stylesheet" type="text/css" />
	<!-- = favicon_link_tag 'favicon.ico', :rel => 'shortcut icon' -->
</head>
<body>
<div class='navbar navbar-default navbar-fixed-top' role='navigation'>
	<div class='container-fluid'>
		<div class='navbar-header'>
			<button class='navbar-toggle' data-target='.navbar-collapse' data-toggle='collapse' type='button'>
				<span class='sr-only'>Toggle navigation</span>
				<span class='icon-bar'></span>
				<span class='icon-bar'></span>
				<span class='icon-bar'></span>
			</button>
			<a href="index.html" class="navbar-brand">Clause Logic Service</a>
		</div>
		<div class='navbar-collapse collapse'>
		</div>
	</div>
</div>

<div data-alerts="alerts" data-titles="{'warning': '<em>Warning!</em>'}" data-ids="myid" data-fade="0"></div>

<div class='container-fluid'> <!-- margin-top-15 -->
	<div class='container-fluid'> </div>

	<h3 id="h3Title">Reset Password</h3>

	<div class='container-fluid' id="divEdit">
		<form accept-charset="UTF-8" autocomplete="off" class="form-horizontal" id="form" 
			method="post" onsubmit="return resetPassword()" action="page?do=session-reset-password">
			<fieldset>
				<div class='form-group' style="display:none">
					<label class="col-md-4 control-label" for="user_id">ID</label>
					<div class='col-md-6'>
						<input class="form-control" id="user_id" name="Id" type="text" readonly />
					</div>
				</div>
				<div class='form-group' style="display:none">
					<label class="col-md-4 control-label" for="user_token">Token</label>
					<div class='col-md-6'>
						<input class="form-control" id="user_token" name="token" type="text" readonly />
					</div>
				</div>	
				<div class='form-group' style="display:none">
					<label class="col-md-4 control-label" for="reset_password_sent_at">Sent At</label>
					<div class='col-md-6'>
						<input class="form-control" id="reset_password_sent_at" name="sent_at" required type="text" readonly />
					</div>
				</div>								
				<div class='form-group'>
					<label class="col-md-4 control-label" for="user_email">Email</label>
					<div class='col-md-6'>
						<input class="form-control" id="user_email" name="email" required size="30" maxlength="80" type="email" readonly />
					</div>
				</div>
				<div class='form-group'>
					<label class="col-md-4 control-label" for="user_first_name">First name</label>
					<div class='col-md-6'>
						<input class="form-control" id="user_first_name" name="first_name" required size="30" maxlength="60" type="text" readonly />
					</div>
				</div>
				<div class='form-group'>
					<label class="col-md-4 control-label" for="user_last_name">Last name</label>
					<div class='col-md-6'>
						<input class="form-control" id="user_last_name" name="last_name" required size="30" maxlength="60" type="text" readonly />
					</div>
				</div>				
				<div class='form-group'>
					<label class="col-md-4 control-label" for="password">New password</label>
					<div class='col-md-6'>
						<input class="form-control" data-bv-identical-field="password"
							 data-bv-identical-message="The password and its confirm are not the same"
							data-bv-identical="true" id="password" name="password" placeholder="Password value must be 8-20 characters long, contain uppercase and lowercase letters, digit(s), special character(s), but no spaces"
							required="required" type="password" maxlength="20" />
					</div>	
				</div>
				<div class='form-group'>
					<label class="col-md-4 control-label" for="password_again">Confirm password</label>
					<div class='col-md-6'>
						<input class="form-control" id="password_again" name="password_again" placeholder="Password again"
							required="required" type="password" maxlength="20" />
					</div>
				</div>
			</fieldset>
			<div class='form-group'>
				<div class='col-sm-offset-4 col-sm-6'>
					<input class="btn btn-primary" name="commit" type="submit" id="btnSave" value="Reset Password" onclick="clearSessionMessage()"/>
					<a href="../cls/logon.html" class="btn btn-default" id="logon_with_email_form" role="button">Cancel</a>
				</div>
			</div>
		</form>

		<!--  <span id="spanLinkShow"><a id="linkShow" href="users_details.html?id=">Show</a> |</span> <a href="users.html">Back</a>  -->
	</div>

</div>

<div id="divBottomFooter"></div>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script type="text/javascript" src="assets/jquery-1.11.2.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script type="text/javascript" src="assets/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="assets/bootstrap/js/jquery.bsAlerts.min.js"></script>
<script type="text/javascript" src="assets/bootstrapvalidator/js/bootstrapValidator.js"></script>
<script type="text/javascript" src="assets/session.js" type="text/javascript"></script>
<script type="text/javascript" src="resetpassword.js" type="text/javascript"></script>
</body>
</html>
