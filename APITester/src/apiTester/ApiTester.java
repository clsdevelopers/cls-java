package apiTester;

import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;
import java.util.prefs.Preferences;

import javax.swing.JFileChooser;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JRadioButton;
import javax.swing.UIManager;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.text.BadLocationException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import http.RestRequest;
import ui.MainFrame;
import util.JsonUtils;
import util.Popup;

public class ApiTester {
	private HashMap<String, Object> vars;
	private TestSuite testSuite;
	private ApiCall curCall;
	private MainFrame window;
	String filePath;
	private int curCallIndex;
	private JFileChooser jfc = new JFileChooser();
	private static FileNameExtensionFilter FILTER = new FileNameExtensionFilter("JSON files", "json");
	
	
	public ApiTester() throws HeadlessException, IOException {
		jfc = new JFileChooser();
		jfc.setFileFilter(FILTER);
		vars = new HashMap<>();
		window = new MainFrame(this);
		//promptAndLoad();
		new ui.InitWindow(this);
	}
	
	private void popWindow(){
		window.setVisible(true);
		window.setVarMap(vars);
		window.setDefaultVarMap(testSuite.getPredefinedVariables());
	}
	
	public void initFromExistingFile(){
		if(promptForFile(false)){
			try {
				loadFromFile(filePath);
			} catch (IOException e) {
				Popup.popErrorWindow(writer ->{
					writer.println("An error occured while reading the file:");
					e.printStackTrace(writer);
				});
			}
			popWindow();
		}
	}
	
	public void initFromNewFile(){
		if(promptForFile(true)){
			if(!filePath.endsWith(".json")) filePath = filePath + ".json";
			createFromNewFile(filePath);
			popWindow();
		}
	}
	
	private void createFromNewFile(String path){
		JSONObject root = new JSONObject();
		root.put("name", "New Suite");
		root.put("variables", new JSONObject().put("url_prefix", "Put your URL prefix here"));
		JSONArray calls = new JSONArray();
		calls.put(new ApiCall("New Call", "GET").toJson());
		root.put("calls", calls);
		load(root);
	}
	
	public boolean promptForFile(boolean save){
		File startingFolder = null;
		String startingPath = Preferences.userNodeForPackage(this.getClass()).get("lastFolder", "");
		if(!startingPath.equals("")){
			startingFolder = new File(startingPath);
		}
		jfc.setCurrentDirectory(startingFolder);
		int retval;
		if(save){
			retval = jfc.showSaveDialog(null);
		} else {
			retval = jfc.showOpenDialog(null);
		}
		if(retval == JFileChooser.APPROVE_OPTION){
			filePath = jfc.getSelectedFile().getAbsolutePath();
			Preferences.userNodeForPackage(this.getClass()).put("lastFolder", jfc.getCurrentDirectory().getAbsolutePath());
		} else {
			return false;
		}
		return true;
	}
	
	private void loadFromFile(String path) throws IOException{
		FileInputStream fis = new FileInputStream(path);
		load(new JSONObject(new JSONTokener(fis)));
		fis.close();
	}
	
	private void load(JSONObject obj){
		setTestSuite(new TestSuite(obj));
	}
	
	private void setTestSuite(TestSuite newTestSuite) {
		this.testSuite = newTestSuite;
		resetVars();
		window.setTestSuite(testSuite);
		setCurCallFromIndex(0);
	}
	
	public void resetVars(){
		testSuite.getApiCalls().forEach(call -> {
			call.clearLastResponse();
		});
		vars.clear();
		vars.putAll(testSuite.getPredefinedVariables());
		window.refreshVarsTable();
	}
	
	public void runAll(){
		window.switchToCurrentVars();
		new Thread(new Runnable() {
			@Override
			public void run() {
				testSuite.getApiCalls().forEach(call -> {
					call.clearLastResponse();
				});
				for(int i = 0; i < testSuite.getApiCalls().size(); i++){
					setCurCallFromIndex(i);
					runCurrent();
				}
			}
		}).start();
	}

	private void runApiCall(ApiCall call) {
		System.out.println("\n\n******Running api call: "+call.getName());
		RestRequest request = new RestRequest(call);
		request.prepare(vars);
		int retval = request.send();
		System.out.println("Response code: "+retval);
		if(retval!=200){
			System.err.println("Error: API call "+call.getName()+" returned a non-ok response from the server: " + retval);
		} else {
			System.out.println(request.getResponse().toString(1));
			updateVarsFromResponse(request.getResponse(), call);
		}
	}

	private void updateVarsFromResponse(JSONObject response, ApiCall call) {
		for(Map.Entry<String, String> entry : call.getOutputParams().entrySet()){
			vars.put(entry.getKey(), JsonUtils.findValueFromPath(entry.getValue(), response));
		}
	}
	
	private void setCurCallFromIndex(int index){
		if(index < 0 || index >= testSuite.getApiCalls().size()) return;
		this.curCallIndex = index;
		curCall = testSuite.getApiCalls().get(index);
		window.setCallListIndex(index);
		window.setCall(curCall);
	}
	
	private void trySetNewBody(String body){
		JSONObject json;
		try{
			json = new JSONObject(body);
		} catch (JSONException e){
			window.setBodyParseStatus(false);
			return;
		}
		curCall.setBody(json);
		window.setBodyParseStatus(true);
		save();
	}

	public void runCurrent() {
		window.switchToCurrentVars();
		runApiCall(curCall);
		window.updateResponse(curCall);
	}
	
	public void save(){
		if(testSuite.unsavedChanges())
			saveAs(filePath);
	}
	
	public void saveAs(String path) {
		try {
			testSuite.saveToFile(path);
		} catch (Exception e) {
			Popup.popErrorWindow(writer ->{
				writer.println("An error occured while saving:");
				e.printStackTrace(writer);
			});
		}
		filePath = path;
	}

	public void addNewCall() {
		ApiCall newCall = new ApiCall("New Call", "GET");
		testSuite.getApiCalls().add(newCall);
		window.setTestSuite(testSuite);
	}

	public void moveCurrentCallUp() {
		if(curCallIndex <= 0) return;
		ApiCall moved = testSuite.getApiCalls().remove(curCallIndex);
		testSuite.getApiCalls().add(curCallIndex-1, moved);
		window.setTestSuite(testSuite);
		setCurCallFromIndex(curCallIndex-1);
	}
	
	public void moveCurrentCallDown(){
		if(curCallIndex >= (testSuite.getApiCalls().size() - 1)) return;
		ApiCall moved = testSuite.getApiCalls().remove(curCallIndex);
		testSuite.getApiCalls().add(curCallIndex+1, moved);
		window.setTestSuite(testSuite);
		setCurCallFromIndex(curCallIndex+1);
	}

	public void promptAndDeleteCurrentCall() {
		int retval = JOptionPane.showConfirmDialog(window, "Are you sure you want to delete this call?", "Confirmation", JOptionPane.YES_NO_OPTION);
		if(retval == JOptionPane.YES_OPTION){
			deleteCurrentCall();
		}
	}
	
	private void deleteCurrentCall(){
		testSuite.getApiCalls().remove(curCallIndex);
		window.setTestSuite(testSuite);
		if(curCallIndex >= testSuite.getApiCalls().size()){
			setCurCallFromIndex(curCallIndex-1);
		} else {
			setCurCallFromIndex(curCallIndex);
		}
	}
	
	public void promptAndRenameCurrentCall(){
		String newName = JOptionPane.showInputDialog("Enter a new name for the call:");
		if(newName != null){
			renameCurrentCall(newName);
		}
	}
	
	public String buildRunReport(){
		StringWriter writer = new StringWriter();
		buildRunReport(new PrintWriter(writer));
		writer.flush();
		return writer.toString();
	}
	
	public void buildRunReport(PrintWriter writer){
		writer.append("API Report\n\n");
		writer.append("Suite: "+testSuite.getSuiteName()+"\n");
		writer.append("Calls:\n");
		for(ApiCall call : testSuite.getApiCalls()){
			if(call.hasLastRun()){
				writer.append("\nCall: "+call.getName()+"\n");
				writer.append("\tType: "+call.getRequestType()+"\n");
				writer.append("\tAddress: "+call.getLastAddress()+"\n");
				writer.append("\tHeaders:\n");
				for(Map.Entry<String, String> entry : call.getLastHeaders().entrySet()){
					writer.append("\t\t").append(entry.getKey()).append("=").append(entry.getValue()).append("\n");
				}
				writer.append("\tBody:\n"+call.getLastBody().toString(1)+"\n");
				writer.append("\tResponse: "+call.getLastResponseCode()+"\n");
				writer.append(new JSONObject(call.getLastResponse()).toString(1)+"\n");
			}
		}
	}
	
	private void renameCurrentCall(String newName){
		curCall.setName(newName);
		window.setCall(curCall);
		window.setTestSuite(testSuite);
	}

	public void promptAndRenameSuite() {
		String newName = JOptionPane.showInputDialog("Enter a new name for the suite:");
		if(newName != null){
			renameSuite(newName);
		}
	}
	
	private void renameSuite(String newName){
		testSuite.setName(newName);
		window.setTestSuite(testSuite);
	}

	public static void main(String[] args) {
		System.setProperty("jsse.enableSNIExtension", "false");
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
			new ApiTester();
		} catch (Exception e) {
			System.err.println("Uncaught exception");
			e.printStackTrace();
		}
	}
	
	public class RequestTypeListener implements ActionListener{
		@Override
		public void actionPerformed(ActionEvent e) {
			JRadioButton source = (JRadioButton) e.getSource();
			if(source.isSelected()){
				curCall.setRequestType(source.getText());
				save();
			}
			
		}
	}
	
	public class CallListListener implements ListSelectionListener{
		@Override
		public void valueChanged(ListSelectionEvent e) {
			if(!e.getValueIsAdjusting()){
				@SuppressWarnings("unchecked")
				JList<String> source = (JList<String>) e.getSource();
				setCurCallFromIndex(source.getSelectedIndex());
				save();
			}
		}
	}
	
	public class UrlFieldDocListener implements DocumentListener{
		@Override
		public void changedUpdate(DocumentEvent arg0) {}

		@Override
		public void insertUpdate(DocumentEvent e) {
			try {
				curCall.setAddress(e.getDocument().getText(0, e.getDocument().getLength()));
				save();
			} catch (BadLocationException e1) {
				e1.printStackTrace();
			}
		}

		@Override
		public void removeUpdate(DocumentEvent e) {
			try {
				int length = e.getDocument().getLength();
				if(length == 0) return;//prevents extra saves
				curCall.setAddress(e.getDocument().getText(0, length));
				save();
			} catch (BadLocationException e1) {
				e1.printStackTrace();
			}
		}
	}
	
	public class BodyDocumentListener implements DocumentListener{
		@Override
		public void changedUpdate(DocumentEvent e) {}

		@Override
		public void insertUpdate(DocumentEvent e) {	
			try {
				trySetNewBody(e.getDocument().getText(0, e.getDocument().getLength()));
			} catch (BadLocationException e1) {
				e1.printStackTrace();
			}
		}

		@Override
		public void removeUpdate(DocumentEvent e) {
			try {
				trySetNewBody(e.getDocument().getText(0, e.getDocument().getLength()));
			} catch (BadLocationException e1) {
				e1.printStackTrace();
			}
		}
	}
	
	public class TableDataChangeListener implements TableModelListener{
		@Override
		public void tableChanged(TableModelEvent e) {
			save();
		}
		
	}
}
