package apiTester;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import util.JsonUtils;

public class ApiCall {
	private String name, address, requestType;
	Map<String, String> header;
	private Map<String, String> outputParams;
	private JSONObject body;
	private String lastResponse;
	private int lastResponseCode;
	private JSONObject lastBody;
	public JSONObject getLastBody() {
		return lastBody;
	}

	public Map<String, String> getLastHeaders() {
		return lastHeaders;
	}

	public String getLastAddress() {
		return lastAddress;
	}

	private Map<String, String> lastHeaders;
	private String lastAddress;
	private boolean hasLastRun = false;
	
	public ApiCall(File source) throws JSONException, IOException{
		loadFromJson(source);
	}
	
	public ApiCall(JSONObject root){
		readJson(root);
	}
	
	public ApiCall(String name, String requestType){
		setName(name);
		setRequestType(requestType);
		header = new HashMap<>();
		outputParams = new HashMap<>();
		body = new JSONObject();
		address = "";
	}
	
	private void loadFromJson(File file) throws JSONException, IOException{
		FileInputStream stream = new FileInputStream(file);
		JSONObject root = new JSONObject(new JSONTokener(stream));
		readJson(root);
		stream.close();
	}
	
	private void readJson(JSONObject root){
		name = root.getString("name");
		JSONObject request = root.getJSONObject("request");
		requestType = request.getString("type");
		address = request.getString("address");
		JSONObject headerObject = root.getJSONObject("header");
		header = JsonUtils.objectToStringMap(headerObject);
		body = root.getJSONObject("body");
		outputParams = JsonUtils.objectToStringMap(root.getJSONObject("output"));
		System.out.println("api call "+ name + " successfully loaded!");
	}
	
	public JSONObject toJson(){
		JSONObject root = new JSONObject();
		root.put("name", getName());
		JSONObject request = new JSONObject();
		request.put("type", getRequestType());
		request.put("address", getAddress());
		root.put("request", request);
		root.put("header", JsonUtils.mapToJson(getRawHeader()));
		root.put("body", body);
		root.put("output", JsonUtils.mapToJson(getOutputParams()));
		return root;
	}
	
	public Map<String, String> getRawHeader(){
		return header;
	}
	
	public Map<String, String> getOutputParams(){
		return outputParams;
	}
	
	public JSONObject getRawBody(){
		return body;
	}

	public String getName() {
		return name;
	}

	public String getAddress() {
		return address;
	}

	public String getRequestType() {
		return requestType;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setBody(JSONObject body) {
		this.body = body;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public void setRequestType(String newType){
		this.requestType = newType;
	}

	public String getLastResponse() {
		return lastResponse;
	}

	public void setLastResponse(String lastResponse) {
		this.lastResponse = lastResponse;
	}
	
	public void clearLastResponse(){
		setLastResponse(null);
		setLastResponseCode(-1);
		setLastBody(null);
		setLastAddress(null);
		hasLastRun = false;
	}

	public void setHasLastRun(boolean hasLastRun) {
		this.hasLastRun = hasLastRun;
	}

	public int getLastResponseCode() {
		return lastResponseCode;
	}

	public void setLastResponseCode(int lastResponseCode) {
		this.lastResponseCode = lastResponseCode;
	}

	public void setLastBody(JSONObject body) {
		this.lastBody = body;
	}

	public void setLastHeaders(Map<String, String> headers) {
		this.lastHeaders = headers;
	}

	public void setLastAddress(String address) {
		this.lastAddress = address;
	}

	public boolean hasLastRun() {
		return hasLastRun ;
	}
}
