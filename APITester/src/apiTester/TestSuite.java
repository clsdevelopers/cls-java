package apiTester;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;

import util.JsonUtils;

public class TestSuite {
	private static final String TAG_CALLS = "calls";
	private static final String TAG_VARIABLES = "variables";
	private static final String TAG_NAME = "name";
	private List<ApiCall> apiCalls;
	private Map<String, Object> predefinedVariables;
	private String suiteName;
	private JSONObject lastSave;
	
	public TestSuite(JSONObject root){
		loadFromJson(root);
	}
	
	private void loadFromJson(JSONObject root){
		suiteName = root.getString(TAG_NAME);
		predefinedVariables = JsonUtils.objectToMap(root.getJSONObject(TAG_VARIABLES));
		JSONArray calls = root.getJSONArray(TAG_CALLS);
		apiCalls = new ArrayList<ApiCall>(calls.length());
		for(int i = 0; i < calls.length(); i++){
			apiCalls.add(i, new ApiCall(calls.getJSONObject(i)));
		}
		lastSave = root;
		System.out.println("suite "+suiteName+" successfully loaded!");
	}
	
	public JSONObject toJson(){
		JSONObject root = new JSONObject();
		root.put(TAG_NAME, getSuiteName());
		root.put(TAG_VARIABLES, new JSONObject(getPredefinedVariables()));
		JSONArray array = new JSONArray();
		for(int i = 0; i < apiCalls.size(); i++){
			array.put(i, apiCalls.get(i).toJson());
		}
		root.put(TAG_CALLS, array);
		return root;
	}
	
	public boolean unsavedChanges(){
		if(lastSave == null) return true;
		JSONObject newSave = toJson();
		if(lastSave.similar(newSave)) 
			return false;
		else
			return true;
	}
	
	public void saveToFile(String path) throws FileNotFoundException{
		PrintWriter writer = new PrintWriter(path);
		lastSave = toJson();
		writer.print(getSaveString(lastSave));
		writer.flush();
		writer.close();
		System.out.println("Saved to "+path);
	}
	
	private String getSaveString(JSONObject root){
		return root.toString(4);
	}
	
	public List<ApiCall> getApiCalls() {
		return apiCalls;
	}
	public Map<String, Object> getPredefinedVariables() {
		return predefinedVariables;
	}
	public String getSuiteName() {
		return suiteName;
	}

	public void setName(String newName) {
		this.suiteName = newName;
		
	}
}
