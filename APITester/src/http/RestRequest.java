package http;

import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.json.JSONObject;
import org.json.JSONTokener;

import apiTester.ApiCall;
import util.JsonUtils;
import util.StringUtils;

public class RestRequest {
	private JSONObject response;
	private ApiCall apiCall;
	private Map<String, String> headers;
	private JSONObject body;
	private String address;
	
	public RestRequest(ApiCall call){
		this.apiCall = call;
	}
	
	public void prepare(HashMap<String, Object> vars){
		System.out.println("preparing REST request...");
		body = new JSONObject(apiCall.getRawBody().toString());
		headers = new HashMap<>(apiCall.getRawHeader());
		address = apiCall.getAddress();
		for(Map.Entry<String, Object> var : vars.entrySet()){
			System.out.println("translating "+var.getKey()+" -> " + var.getValue()+" ("+var.getValue().getClass().getSimpleName()+")");
			JsonUtils.replaceAll(var.getKey(), var.getValue(), body);
			address = address.replaceAll(StringUtils.quote(var.getKey()), var.getValue().toString());
			Set<String> headerKeys = headers.keySet();
			for(String key : headerKeys){
				if(headers.get(key) instanceof String) headers.put(key, ((String) headers.get(key)).replaceAll(StringUtils.quote(var.getKey()), var.getValue().toString()));
			}
		}
	}
	
	public int send(){
		
	    try {
	        URL url = new URL(address);

	        HttpURLConnection connection = (HttpURLConnection) url.openConnection();

	        connection.setDoInput(true);
	        connection.setDoOutput(true);
	        connection.setRequestMethod(apiCall.getRequestType());
	        for(Map.Entry<String, String> pair : headers.entrySet()){
	        	connection.setRequestProperty(pair.getKey(), pair.getValue());
	        }
	        
	        System.out.println("Sending "+apiCall.getRequestType()+" request to " + address);
	        if(!apiCall.getRequestType().equals("GET")){
	        	System.out.println(" with payload: \n"+body.toString(1));
	        	OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream(), "UTF-8");
	        	if(body!=null && !apiCall.getRequestType().equals("GET"))writer.write(body.toString());
	        	writer.close();
	        }
	        InputStream instream;
	        try{
	        	instream = connection.getInputStream();
	        } catch(Exception e){
	        	instream = connection.getErrorStream();
	        }
	        int responseCode = connection.getResponseCode();
	        
	        try{
	        	response = new JSONObject(new JSONTokener(instream));
	        } catch(Exception e){
	        	response = new JSONObject();
	        }
	        if(instream != null)instream.close();
	        connection.disconnect();
	        apiCall.setLastResponse(response.toString(1));
	        apiCall.setLastResponseCode(responseCode);
			apiCall.setLastBody(body);
			apiCall.setLastHeaders(headers);
			apiCall.setLastAddress(address);
			apiCall.setHasLastRun(true);
	        return responseCode;
	    } catch (Exception e) {
	            e.printStackTrace();
	    }
	    return -1;
	}
	
	public JSONObject getResponse() {
		return response;
	}
}
