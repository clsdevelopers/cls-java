package ui;

import java.awt.BorderLayout;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JToolBar;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JList;
import javax.swing.ListSelectionModel;
import javax.swing.JScrollPane;
import javax.swing.JLabel;
import javax.swing.border.TitledBorder;

import apiTester.ApiCall;
import apiTester.ApiTester;
import apiTester.ApiTester.RequestTypeListener;
import apiTester.TestSuite;

import javax.swing.border.EtchedBorder;
import javax.swing.JTextField;
import javax.swing.JRadioButton;
import javax.swing.AbstractButton;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.DefaultListModel;
import javax.swing.JTextArea;
import java.awt.Dimension;
import javax.swing.border.LineBorder;
import java.awt.Color;
import javax.swing.JButton;
import javax.swing.ImageIcon;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JSeparator;
import javax.swing.JTabbedPane;
import javax.swing.Box;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class MainFrame extends JFrame {
	
	private static final long serialVersionUID = 7887093875309202046L;
	private JPanel contentPane;
	private JTextField urlField;
	private final ButtonGroup requestTypeGroup = new ButtonGroup();
	private EditableMapBackedTablePanel<String, Object> varsTable;
	private JList<String> callList;
	private JLabel dynCurCallLabel;
	private JTextArea bodyTextArea;
	private EditableMapBackedTablePanel<String, String> headersPanel;
	private JLabel dynResponseCode;
	private JTextArea responseTextArea;
	private EditableMapBackedTablePanel<String, String> outputVarsTablePanel;
	private JLabel dynCurSuiteLabel;
	private EditableMapBackedTablePanel<String, Object> defVarsTable;
	private JTabbedPane varsTabPane;

	/**
	 * Create the frame.
	 */
	public MainFrame(ApiTester tester) {
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent arg0) {
				tester.save();
			}
		});
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1260, 859);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		JPanel northPanel = new JPanel();
		contentPane.add(northPanel, BorderLayout.NORTH);
		GridBagLayout gbl_northPanel = new GridBagLayout();
		gbl_northPanel.columnWidths = new int[]{579, 0};
		gbl_northPanel.rowHeights = new int[]{16, 88, -50, 0};
		gbl_northPanel.columnWeights = new double[]{1.0, Double.MIN_VALUE};
		gbl_northPanel.rowWeights = new double[]{0.0, 1.0, 0.0, Double.MIN_VALUE};
		northPanel.setLayout(gbl_northPanel);
		
		JToolBar toolBar = new JToolBar();
		toolBar.setFloatable(false);
		GridBagConstraints gbc_toolBar = new GridBagConstraints();
		gbc_toolBar.insets = new Insets(0, 0, 5, 0);
		gbc_toolBar.fill = GridBagConstraints.HORIZONTAL;
		gbc_toolBar.anchor = GridBagConstraints.NORTH;
		gbc_toolBar.gridx = 0;
		gbc_toolBar.gridy = 0;
		northPanel.add(toolBar, gbc_toolBar);
		
		JButton btnOpen = new JButton("Open Suite...");
		btnOpen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				tester.initFromExistingFile();
			}
		});
		btnOpen.setActionCommand("open");
		btnOpen.setToolTipText("Open");
		btnOpen.setIcon(new ImageIcon(MainFrame.class.getResource("/javax/swing/plaf/metal/icons/ocean/newFolder.gif")));
		toolBar.add(btnOpen);
		
		JButton btnRuncur = new JButton("Run Current");
		btnRuncur.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				tester.runCurrent();
			}
		});
		btnRuncur.setToolTipText("Run Current");
		btnRuncur.setIcon(new ImageIcon(MainFrame.class.getResource("/icons/singlestep.gif")));
		toolBar.add(btnRuncur);
		
		JButton btnRunall = new JButton("Run All");
		btnRunall.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				tester.runAll();
			}
		});
		btnRunall.setIcon(new ImageIcon(MainFrame.class.getResource("/icons/play.gif")));
		btnRunall.setToolTipText("Run all API calls in order");
		toolBar.add(btnRunall);
		
		JButton btnResetVars = new JButton("Reset Variables");
		btnResetVars.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				tester.resetVars();
			}
		});
		btnResetVars.setIcon(new ImageIcon(MainFrame.class.getResource("/icons/reset.gif")));
		btnResetVars.setToolTipText("Reset all variables to their default values");
		toolBar.add(btnResetVars);
		
		JButton btnGenerateRunReport = new JButton("Generate Run Report");
		btnGenerateRunReport.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				util.Popup.popMessageWindow(writer -> {
					tester.buildRunReport(writer);
				});
			}
		});
		btnGenerateRunReport.setIcon(new ImageIcon(MainFrame.class.getResource("/icons/import.gif")));
		toolBar.add(btnGenerateRunReport);
		
		JPanel topPanel = new JPanel();
		topPanel.setBorder(new EmptyBorder(0, 5, 0, 0));
		GridBagConstraints gbc_topPanel = new GridBagConstraints();
		gbc_topPanel.insets = new Insets(0, 0, 5, 0);
		gbc_topPanel.fill = GridBagConstraints.BOTH;
		gbc_topPanel.gridx = 0;
		gbc_topPanel.gridy = 1;
		northPanel.add(topPanel, gbc_topPanel);
		GridBagLayout gbl_topPanel = new GridBagLayout();
		gbl_topPanel.columnWidths = new int[]{0, 0, 0};
		gbl_topPanel.rowHeights = new int[]{120, 30, 0};
		gbl_topPanel.columnWeights = new double[]{1.0, 1.0, Double.MIN_VALUE};
		gbl_topPanel.rowWeights = new double[]{1.0, 0.0, Double.MIN_VALUE};
		topPanel.setLayout(gbl_topPanel);
		
		dynCurSuiteLabel = new JLabel("<html><h1>[Suite Name]</h1></html>");
		GridBagConstraints gbc_dynCurSuiteLabel = new GridBagConstraints();
		gbc_dynCurSuiteLabel.insets = new Insets(0, 0, 5, 5);
		gbc_dynCurSuiteLabel.gridx = 0;
		gbc_dynCurSuiteLabel.gridy = 0;
		topPanel.add(dynCurSuiteLabel, gbc_dynCurSuiteLabel);
		
		JPanel varsTablePanel = new JPanel();
		varsTablePanel.setBorder(new TitledBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null), "Variables", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		GridBagConstraints gbc_varsTablePanel = new GridBagConstraints();
		gbc_varsTablePanel.gridheight = 2;
		gbc_varsTablePanel.fill = GridBagConstraints.BOTH;
		gbc_varsTablePanel.gridx = 1;
		gbc_varsTablePanel.gridy = 0;
		topPanel.add(varsTablePanel, gbc_varsTablePanel);
		GridBagLayout gbl_varsTablePanel = new GridBagLayout();
		gbl_varsTablePanel.columnWidths = new int[]{532, 0};
		gbl_varsTablePanel.rowHeights = new int[]{119, 0};
		gbl_varsTablePanel.columnWeights = new double[]{1.0, Double.MIN_VALUE};
		gbl_varsTablePanel.rowWeights = new double[]{1.0, Double.MIN_VALUE};
		varsTablePanel.setLayout(gbl_varsTablePanel);
		
		varsTabPane = new JTabbedPane(JTabbedPane.TOP);
		GridBagConstraints gbc_varsTabPane = new GridBagConstraints();
		gbc_varsTabPane.fill = GridBagConstraints.BOTH;
		gbc_varsTabPane.gridx = 0;
		gbc_varsTabPane.gridy = 0;
		varsTablePanel.add(varsTabPane, gbc_varsTabPane);
		
		varsTable = new EditableMapBackedTablePanel<String, Object>();
		varsTabPane.addTab("Current", null, varsTable, null);
		varsTable.setPreferredSize(new Dimension(872, 100));
		varsTable.setModel(new MapBackedTableModel<String, Object>(null, "var", null, "Variable", "Value"));
		varsTable.getModel().addTableModelListener(tester.new TableDataChangeListener());
		
		defVarsTable = new EditableMapBackedTablePanel<>();
		varsTabPane.addTab("Default", null, defVarsTable, null);
		defVarsTable.setPreferredSize(varsTable.getPreferredSize());
		defVarsTable.setModel(new MapBackedTableModel<String, Object>(null, "var", null, "Variable", "Value"));
		defVarsTable.getModel().addTableModelListener(tester.new TableDataChangeListener());
		
		JButton btnRenameSuite = new JButton("Rename Suite");
		btnRenameSuite.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				tester.promptAndRenameSuite();
			}
		});
		GridBagConstraints gbc_btnRenameSuite = new GridBagConstraints();
		gbc_btnRenameSuite.insets = new Insets(0, 0, 0, 5);
		gbc_btnRenameSuite.gridx = 0;
		gbc_btnRenameSuite.gridy = 1;
		topPanel.add(btnRenameSuite, gbc_btnRenameSuite);
		
		JSeparator separator = new JSeparator();
		GridBagConstraints gbc_separator = new GridBagConstraints();
		gbc_separator.fill = GridBagConstraints.BOTH;
		gbc_separator.gridx = 0;
		gbc_separator.gridy = 2;
		northPanel.add(separator, gbc_separator);
		
		JPanel westPanel = new JPanel();
		westPanel.setBorder(new TitledBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null), "API Calls", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		contentPane.add(westPanel, BorderLayout.WEST);
		GridBagLayout gbl_westPanel = new GridBagLayout();
		gbl_westPanel.columnWidths = new int[]{162, 0};
		gbl_westPanel.rowHeights = new int[]{0, 0, 0};
		gbl_westPanel.columnWeights = new double[]{1.0, Double.MIN_VALUE};
		gbl_westPanel.rowWeights = new double[]{1.0, 0.0, Double.MIN_VALUE};
		westPanel.setLayout(gbl_westPanel);
		
		callList = new JList<String>();
		callList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		GridBagConstraints gbc_testList = new GridBagConstraints();
		gbc_testList.insets = new Insets(0, 0, 5, 0);
		gbc_testList.fill = GridBagConstraints.BOTH;
		gbc_testList.gridx = 0;
		gbc_testList.gridy = 0;
		westPanel.add(new JScrollPane(callList), gbc_testList);
		
		Box horizontalBox = Box.createHorizontalBox();
		GridBagConstraints gbc_horizontalBox = new GridBagConstraints();
		gbc_horizontalBox.gridx = 0;
		gbc_horizontalBox.gridy = 1;
		westPanel.add(horizontalBox, gbc_horizontalBox);
		
		JButton btnAddCall = new JButton("New");
		btnAddCall.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				tester.addNewCall();
			}
		});
		horizontalBox.add(btnAddCall);
		
		JButton btnMoveUp = new JButton("Move Up");
		btnMoveUp.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				tester.moveCurrentCallUp();
			}
		});
		horizontalBox.add(btnMoveUp);
		
		JButton btnMoveDown = new JButton("Move Down");
		btnMoveDown.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				tester.moveCurrentCallDown();
			}
		});
		horizontalBox.add(btnMoveDown);
		callList.addListSelectionListener(tester.new CallListListener());
		
		JPanel statusPanel = new JPanel();
		contentPane.add(statusPanel, BorderLayout.SOUTH);
		GridBagLayout gbl_statusPanel = new GridBagLayout();
		gbl_statusPanel.columnWidths = new int[]{0, 0};
		gbl_statusPanel.rowHeights = new int[]{0, 0};
		gbl_statusPanel.columnWeights = new double[]{0.0, Double.MIN_VALUE};
		gbl_statusPanel.rowWeights = new double[]{0.0, Double.MIN_VALUE};
		statusPanel.setLayout(gbl_statusPanel);
		
		JLabel dynStatusLabel = new JLabel("<Status>");
		GridBagConstraints gbc_dynStatusLabel = new GridBagConstraints();
		gbc_dynStatusLabel.gridx = 0;
		gbc_dynStatusLabel.gridy = 0;
		statusPanel.add(dynStatusLabel, gbc_dynStatusLabel);
		
		JPanel mainContentPanel = new JPanel();
		mainContentPanel.setBorder(new EmptyBorder(3, 6, 6, 6));
		contentPane.add(mainContentPanel, BorderLayout.CENTER);
		GridBagLayout gbl_mainContentPanel = new GridBagLayout();
		gbl_mainContentPanel.columnWidths = new int[]{66, 823, 0, 0};
		gbl_mainContentPanel.rowHeights = new int[]{0, 0, 0, 0, 97, 174, 154, 0, 0};
		gbl_mainContentPanel.columnWeights = new double[]{0.0, 1.0, 0.0, Double.MIN_VALUE};
		gbl_mainContentPanel.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 1.0, 1.0, 1.0, 1.0, Double.MIN_VALUE};
		mainContentPanel.setLayout(gbl_mainContentPanel);
		
		dynCurCallLabel = new JLabel("<html><h1>[Selected Call Name]</h1></html>");
		GridBagConstraints gbc_dynCurCallLabel = new GridBagConstraints();
		gbc_dynCurCallLabel.gridheight = 2;
		gbc_dynCurCallLabel.fill = GridBagConstraints.HORIZONTAL;
		gbc_dynCurCallLabel.insets = new Insets(0, 0, 5, 5);
		gbc_dynCurCallLabel.gridx = 1;
		gbc_dynCurCallLabel.gridy = 0;
		mainContentPanel.add(dynCurCallLabel, gbc_dynCurCallLabel);
		
		JButton btnEdit = new JButton("Rename");
		btnEdit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				tester.promptAndRenameCurrentCall();
			}
		});
		GridBagConstraints gbc_btnEdit = new GridBagConstraints();
		gbc_btnEdit.anchor = GridBagConstraints.SOUTH;
		gbc_btnEdit.insets = new Insets(0, 0, 5, 0);
		gbc_btnEdit.gridx = 2;
		gbc_btnEdit.gridy = 0;
		mainContentPanel.add(btnEdit, gbc_btnEdit);
		
		JButton btnDelete = new JButton("Delete");
		btnDelete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				tester.promptAndDeleteCurrentCall();
			}
		});
		GridBagConstraints gbc_btnDelete = new GridBagConstraints();
		gbc_btnDelete.insets = new Insets(0, 0, 5, 0);
		gbc_btnDelete.gridx = 2;
		gbc_btnDelete.gridy = 1;
		mainContentPanel.add(btnDelete, gbc_btnDelete);
		
		JLabel lblUrl = new JLabel("URL: ");
		GridBagConstraints gbc_lblUrl = new GridBagConstraints();
		gbc_lblUrl.insets = new Insets(0, 0, 5, 5);
		gbc_lblUrl.anchor = GridBagConstraints.EAST;
		gbc_lblUrl.gridx = 0;
		gbc_lblUrl.gridy = 2;
		mainContentPanel.add(lblUrl, gbc_lblUrl);
		
		urlField = new JTextField();
		GridBagConstraints gbc_urlField = new GridBagConstraints();
		gbc_urlField.gridwidth = 2;
		gbc_urlField.insets = new Insets(0, 0, 5, 0);
		gbc_urlField.fill = GridBagConstraints.HORIZONTAL;
		gbc_urlField.gridx = 1;
		gbc_urlField.gridy = 2;
		mainContentPanel.add(urlField, gbc_urlField);
		urlField.getDocument().addDocumentListener(tester.new UrlFieldDocListener());
		
		JLabel lblRequestType = new JLabel("Request Type:");
		GridBagConstraints gbc_lblRequestType = new GridBagConstraints();
		gbc_lblRequestType.anchor = GridBagConstraints.EAST;
		gbc_lblRequestType.insets = new Insets(0, 0, 5, 5);
		gbc_lblRequestType.gridx = 0;
		gbc_lblRequestType.gridy = 3;
		mainContentPanel.add(lblRequestType, gbc_lblRequestType);
		
		JPanel reqestTypesPanel = new JPanel();
		GridBagConstraints gbc_reqestTypesPanel = new GridBagConstraints();
		gbc_reqestTypesPanel.gridwidth = 2;
		gbc_reqestTypesPanel.insets = new Insets(0, 0, 5, 0);
		gbc_reqestTypesPanel.anchor = GridBagConstraints.NORTH;
		gbc_reqestTypesPanel.fill = GridBagConstraints.HORIZONTAL;
		gbc_reqestTypesPanel.gridx = 1;
		gbc_reqestTypesPanel.gridy = 3;
		mainContentPanel.add(reqestTypesPanel, gbc_reqestTypesPanel);
		reqestTypesPanel.setLayout(new BoxLayout(reqestTypesPanel, BoxLayout.X_AXIS));
		
		
		RequestTypeListener rtl = tester.new RequestTypeListener();
		
		JRadioButton rdbtnGet = new JRadioButton("GET");
		rdbtnGet.setSelected(true);
		requestTypeGroup.add(rdbtnGet);
		reqestTypesPanel.add(rdbtnGet);
		rdbtnGet.addActionListener(rtl);
		
		JRadioButton rdbtnPost = new JRadioButton("POST");
		requestTypeGroup.add(rdbtnPost);
		reqestTypesPanel.add(rdbtnPost);
		rdbtnPost.addActionListener(rtl);
		
		JRadioButton rdbtnDelete = new JRadioButton("DELETE");
		requestTypeGroup.add(rdbtnDelete);
		reqestTypesPanel.add(rdbtnDelete);
		rdbtnDelete.addActionListener(rtl);
		
		JRadioButton rdbtnPut = new JRadioButton("PUT");
		requestTypeGroup.add(rdbtnPut);
		reqestTypesPanel.add(rdbtnPut);
		rdbtnPut.addActionListener(rtl);
		
		JRadioButton rdbtnHead = new JRadioButton("HEAD");
		requestTypeGroup.add(rdbtnHead);
		reqestTypesPanel.add(rdbtnHead);
		rdbtnHead.addActionListener(rtl);
		
		JRadioButton rdbtnConnect = new JRadioButton("CONNECT");
		requestTypeGroup.add(rdbtnConnect);
		reqestTypesPanel.add(rdbtnConnect);
		rdbtnHead.addActionListener(rtl);
		
		JRadioButton rdbtnOptions = new JRadioButton("OPTIONS");
		requestTypeGroup.add(rdbtnOptions);
		reqestTypesPanel.add(rdbtnOptions);
		rdbtnOptions.addActionListener(rtl);
		
		JRadioButton rdbtnTrace = new JRadioButton("TRACE");
		requestTypeGroup.add(rdbtnTrace);
		reqestTypesPanel.add(rdbtnTrace);
		rdbtnTrace.addActionListener(rtl);
		
		JRadioButton rdbtnPatch = new JRadioButton("PATCH");
		requestTypeGroup.add(rdbtnPatch);
		reqestTypesPanel.add(rdbtnPatch);
		rdbtnPatch.addActionListener(rtl);
		
		JLabel lblHeaders = new JLabel("Headers:");
		GridBagConstraints gbc_lblHeaders = new GridBagConstraints();
		gbc_lblHeaders.anchor = GridBagConstraints.NORTHEAST;
		gbc_lblHeaders.insets = new Insets(0, 0, 5, 5);
		gbc_lblHeaders.gridx = 0;
		gbc_lblHeaders.gridy = 4;
		mainContentPanel.add(lblHeaders, gbc_lblHeaders);
		
		headersPanel = new EditableMapBackedTablePanel<String, String>();
		GridBagConstraints gbc_headersPanel = new GridBagConstraints();
		gbc_headersPanel.gridwidth = 2;
		gbc_headersPanel.fill = GridBagConstraints.BOTH;
		gbc_headersPanel.insets = new Insets(0, 0, 5, 0);
		gbc_headersPanel.gridx = 1;
		gbc_headersPanel.gridy = 4;
		mainContentPanel.add(headersPanel, gbc_headersPanel);
		headersPanel.setModel(new MapBackedTableModel<String, String>(null, "header", "value", "Header", "Value"));
		headersPanel.getModel().addTableModelListener(tester.new TableDataChangeListener());
		
		JLabel lblBody = new JLabel("Body:");
		GridBagConstraints gbc_lblBody = new GridBagConstraints();
		gbc_lblBody.anchor = GridBagConstraints.NORTHEAST;
		gbc_lblBody.insets = new Insets(0, 0, 5, 5);
		gbc_lblBody.gridx = 0;
		gbc_lblBody.gridy = 5;
		mainContentPanel.add(lblBody, gbc_lblBody);
		
		bodyTextArea = new JTextArea();
		bodyTextArea.setBorder(new LineBorder(Color.GREEN));
		GridBagConstraints gbc_bodyTextArea = new GridBagConstraints();
		gbc_bodyTextArea.gridwidth = 2;
		gbc_bodyTextArea.insets = new Insets(0, 0, 5, 0);
		gbc_bodyTextArea.fill = GridBagConstraints.BOTH;
		gbc_bodyTextArea.gridx = 1;
		gbc_bodyTextArea.gridy = 5;
		mainContentPanel.add(new JScrollPane(bodyTextArea), gbc_bodyTextArea);
		bodyTextArea.getDocument().addDocumentListener(tester.new BodyDocumentListener());
		
		JLabel lblResponse = new JLabel("Response:");
		GridBagConstraints gbc_lblResponse = new GridBagConstraints();
		gbc_lblResponse.anchor = GridBagConstraints.NORTHEAST;
		gbc_lblResponse.insets = new Insets(0, 0, 5, 5);
		gbc_lblResponse.gridx = 0;
		gbc_lblResponse.gridy = 6;
		mainContentPanel.add(lblResponse, gbc_lblResponse);
		
		JPanel responsePanel = new JPanel();
		GridBagConstraints gbc_responsePanel = new GridBagConstraints();
		gbc_responsePanel.gridwidth = 2;
		gbc_responsePanel.insets = new Insets(0, 0, 5, 0);
		gbc_responsePanel.fill = GridBagConstraints.BOTH;
		gbc_responsePanel.gridx = 1;
		gbc_responsePanel.gridy = 6;
		mainContentPanel.add(responsePanel, gbc_responsePanel);
		GridBagLayout gbl_responsePanel = new GridBagLayout();
		gbl_responsePanel.columnWidths = new int[]{17, 0};
		gbl_responsePanel.rowHeights = new int[]{0, 0, 0};
		gbl_responsePanel.columnWeights = new double[]{1.0, Double.MIN_VALUE};
		gbl_responsePanel.rowWeights = new double[]{0.0, 1.0, Double.MIN_VALUE};
		responsePanel.setLayout(gbl_responsePanel);
		
		dynResponseCode = new JLabel("<response code>");
		GridBagConstraints gbc_dynResponseCode = new GridBagConstraints();
		gbc_dynResponseCode.anchor = GridBagConstraints.WEST;
		gbc_dynResponseCode.insets = new Insets(0, 0, 5, 0);
		gbc_dynResponseCode.gridx = 0;
		gbc_dynResponseCode.gridy = 0;
		responsePanel.add(dynResponseCode, gbc_dynResponseCode);
		
		responseTextArea = new JTextArea();
		responseTextArea.setEditable(false);
		GridBagConstraints gbc_responseTextArea = new GridBagConstraints();
		gbc_responseTextArea.fill = GridBagConstraints.BOTH;
		gbc_responseTextArea.gridx = 0;
		gbc_responseTextArea.gridy = 1;
		responsePanel.add(new JScrollPane(responseTextArea), gbc_responseTextArea);
		
		JLabel lblVariables = new JLabel("Variables:");
		GridBagConstraints gbc_lblVariables = new GridBagConstraints();
		gbc_lblVariables.anchor = GridBagConstraints.NORTHEAST;
		gbc_lblVariables.insets = new Insets(0, 0, 0, 5);
		gbc_lblVariables.gridx = 0;
		gbc_lblVariables.gridy = 7;
		mainContentPanel.add(lblVariables, gbc_lblVariables);
		
		outputVarsTablePanel = new EditableMapBackedTablePanel<String, String>();
		GridBagConstraints gbc_outputVarsTablePanel = new GridBagConstraints();
		gbc_outputVarsTablePanel.gridwidth = 2;
		gbc_outputVarsTablePanel.fill = GridBagConstraints.BOTH;
		gbc_outputVarsTablePanel.gridx = 1;
		gbc_outputVarsTablePanel.gridy = 7;
		mainContentPanel.add(outputVarsTablePanel, gbc_outputVarsTablePanel);
		outputVarsTablePanel.setModel(new MapBackedTableModel<String, String>(null, "var", "path", "Variable", "Path"));
		outputVarsTablePanel.getModel().addTableModelListener(tester.new TableDataChangeListener());
	}
	
	@Override
	public void setVisible(boolean visible){
		super.setVisible(visible);
		callList.requestFocusInWindow();
	}

	public void setCallListIndex(int index) {
		if(callList.getSelectedIndex() == index) return;
		callList.setSelectedIndex(index);
	}

	public void setCall(ApiCall curCall) {
		dynCurCallLabel.setText("<html><h1>"+curCall.getName()+"</h1></html>");
		urlField.setText(curCall.getAddress());
		setRequestType(curCall.getRequestType());
		headersPanel.getModel().setMap(curCall.getRawHeader());
		bodyTextArea.setText(curCall.getRawBody().toString(1));
		setBodyParseStatus(true);
		updateResponse(curCall);
		outputVarsTablePanel.getModel().setMap(curCall.getOutputParams());
	}
	
	public void updateResponse(ApiCall curCall){
		String lastResponse = curCall.getLastResponse();
		if(lastResponse == null){
			dynResponseCode.setText("Run call to see results");
			dynResponseCode.setEnabled(false);
			responseTextArea.setEnabled(false);
		} else {
			int lastCode = curCall.getLastResponseCode();
			dynResponseCode.setText("<html><font color="+ (lastCode==200 ? "green" : "red") +">HTTP "+lastCode + "</font></html>");
			dynResponseCode.setEnabled(true);
			responseTextArea.setEnabled(true);
		}
		responseTextArea.setText(lastResponse);
		
		refreshVarsTable();
	}
	
	public void refreshVarsTable(){
		varsTable.getModel().fireTableDataChanged();
		defVarsTable.getModel().fireTableDataChanged();
	}
	
	private void setRequestType(String newType){
		Enumeration<AbstractButton> buttons = requestTypeGroup.getElements();
		while(buttons.hasMoreElements()){
			AbstractButton button = buttons.nextElement();
			button.setSelected(button.getText().equals(newType));
		}
	}

	public void setTestSuite(TestSuite suite) {
		dynCurSuiteLabel.setText("<html><h1>"+suite.getSuiteName()+"</h1></html>");
		DefaultListModel<String> model = new DefaultListModel<String>();
		for(ApiCall call : suite.getApiCalls()){
			model.addElement(call.getName());
		}
		callList.setModel(model);
	}

	public void setBodyParseStatus(boolean b) {
		bodyTextArea.setBorder(new LineBorder(b ? Color.green : Color.red));
	}

	public void setVarMap(HashMap<String, Object> vars) {
		System.out.println("Setting vars: "+vars);
		varsTable.getModel().setMap(vars);
	}
	
	public void switchToCurrentVars(){
		varsTabPane.setSelectedIndex(0);
	}

	public void setDefaultVarMap(Map<String, Object> predefinedVariables) {
		defVarsTable.getModel().setMap(predefinedVariables);
	}

}
