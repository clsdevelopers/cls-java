package ui;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;

@SuppressWarnings("serial")
public class EditableMapBackedTablePanel<K, V> extends JPanel {
	private JTable table;
	private JButton addButton, removeButton;
	private MapBackedTableModel<K, V> model;
	
	public JTable getTable(){
		return table;
	}
	
	public void setModel(MapBackedTableModel<K, V> newModel){
		if(model != null){
			addButton.removeActionListener(model);
			removeButton.removeActionListener(model);
		}
		this.model = newModel;
		model.setTable(table);
		table.setModel(model);
		addButton.addActionListener(model);
		removeButton.addActionListener(model);
	}
	
	public MapBackedTableModel<K, V> getModel(){
		return model;
	}
	
	public void setEnabled(boolean enabled){
		this.setEnabled(enabled);
		table.setEnabled(enabled);
		addButton.setEnabled(enabled);
		removeButton.setEnabled(enabled);
	}
	
	public EditableMapBackedTablePanel(){
		super();
		GridBagLayout gbl_headersPanel = new GridBagLayout();
		gbl_headersPanel.columnWidths = new int[]{831, 0, 0};
		gbl_headersPanel.rowHeights = new int[] {28, 28, 0};
		gbl_headersPanel.columnWeights = new double[]{1.0, 0.0, Double.MIN_VALUE};
		gbl_headersPanel.rowWeights = new double[]{1.0, 1.0, Double.MIN_VALUE};
		this.setLayout(gbl_headersPanel);
		
		table = new JTable();
		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		table.setCellSelectionEnabled(true);
		table.setFillsViewportHeight(true);
		table.getTableHeader().setEnabled(false);
		JScrollPane headerTableScrollPane = new JScrollPane(table);
		GridBagConstraints gbc_headerTableScrollPane = new GridBagConstraints();
		gbc_headerTableScrollPane.gridheight = 2;
		gbc_headerTableScrollPane.fill = GridBagConstraints.BOTH;
		gbc_headerTableScrollPane.gridx = 0;
		gbc_headerTableScrollPane.gridy = 0;
		this.add(headerTableScrollPane, gbc_headerTableScrollPane);
		
		addButton = new JButton("+");
		GridBagConstraints gbc_addButton = new GridBagConstraints();
		gbc_addButton.fill = GridBagConstraints.BOTH;
		gbc_addButton.gridx = 1;
		gbc_addButton.gridy = 0;
		this.add(addButton, gbc_addButton);
		
		removeButton = new JButton("-");
		GridBagConstraints gbc_removeButton = new GridBagConstraints();
		gbc_removeButton.fill = GridBagConstraints.BOTH;
		gbc_removeButton.gridx = 1;
		gbc_removeButton.gridy = 1;
		this.add(removeButton, gbc_removeButton);
	}

}
