package ui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.swing.JTable;
import javax.swing.table.AbstractTableModel;

@SuppressWarnings("serial")
public class MapBackedTableModel<K,V> extends AbstractTableModel implements ActionListener{
	private Map<K,V> map;
	private List<Map.Entry<K, V>> list;
	private boolean keysAreEditable = true;
	private String keyHeader, valueHeader;
	private JTable table;
	private K defaultKey;
	private V defaultValue;
	
	public MapBackedTableModel(Map<K,V> map, K defaultKey, V defaultValue, String keyHeader, String valueHeader){
		setMap(map);
		this.defaultKey = defaultKey;
		this.defaultValue = defaultValue;
		setKeyHeader(keyHeader);
		setValueHeader(valueHeader);
	}
	
	public JTable getTable() {
		return table;
	}

	public void setTable(JTable table) {
		this.table = table;
	}

	public void setMap(Map<K, V> newMap){
		this.map = newMap;
		fireTableDataChanged();
	}
	
	private void buildList(){
		if(map == null) return;
		list = new ArrayList<>(map.size()); 
		list.addAll(map.entrySet());
	}

	@Override
	public int getColumnCount() {
		return 2;
	}

	@Override
	public int getRowCount() {
		if(map == null || map.isEmpty()) return 0;
		return map.size();
	}

	@Override
	public Object getValueAt(int row, int col) {
		if(map == null || map.isEmpty()) return null;
		if(col == 0){
			return map.keySet().toArray()[row];
		} else {
			return map.values().toArray()[row];
		}
	}
	
	@Override
	public boolean isCellEditable(int row, int col) {
		if(col == 0){
			return keysAreEditable;
		} else {
			return true;
		}
	}
	
	@Override
	public String getColumnName(int column) {
		if(column == 0){
			return getKeyHeader();
		} else {
			return getValueHeader();
		}
	};
	
	@SuppressWarnings("unchecked")
	@Override
	public void setValueAt(Object newValue, int row, int col) {
		if(col == 0){
			K oldKey = list.get(row).getKey();
			V value = map.remove(oldKey);
			map.put((K) newValue, value);
		} else {
			K key = list.get(row).getKey();
			map.put(key, (V) newValue);
			list.get(row).setValue((V) newValue);
		}
		fireTableDataChanged();
	};

	public boolean areKeysEditable() {
		return keysAreEditable;
	}

	public void setKeysAreEditable(boolean keysAreEditable) {
		this.keysAreEditable = keysAreEditable;
	}

	public String getKeyHeader() {
		return keyHeader;
	}

	public void setKeyHeader(String keyHeader) {
		this.keyHeader = keyHeader;
	}

	public String getValueHeader() {
		return valueHeader;
	}

	public void setValueHeader(String valueHeader) {
		this.valueHeader = valueHeader;
	}
	
	@Override
	public void fireTableDataChanged() {
		buildList();
		super.fireTableDataChanged();
	};

	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getActionCommand().equals("+")){
			map.put(defaultKey, defaultValue);
		} else if(e.getActionCommand().equals("-")){
			int sel = table.getSelectedRow();
			if(sel == -1)return;
			Object key = getValueAt(sel, 0);
			map.remove(key);
		}
		fireTableDataChanged();
	}
	
	
}
