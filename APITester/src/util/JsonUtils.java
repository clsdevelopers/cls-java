package util;

import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;

public class JsonUtils {

	public static Map<String, Object> objectToMap(JSONObject obj){
		Map<String, Object> map = new HashMap<String, Object>();
		for(String key : obj.keySet()){
			map.put(key, obj.get(key));
		}
		return map;
	}
	
	public static Map<String, String> objectToStringMap(JSONObject obj){
		Map<String, String> map = new HashMap<String, String>();
		for(String key : obj.keySet()){
			map.put(key, obj.get(key).toString());
		}
		return map;
	}

	public static Object findValueFromPath(String value, JSONObject response) {
		int slashIndex = value.indexOf('/');
		if(slashIndex == -1) return response.get(value);
		return findValueFromPath(value.substring(slashIndex + 1, value.length()), response.getJSONObject(value.substring(0, slashIndex)));
	}
	
	public static void replaceAll(String var, Object replacement, JSONObject root){
		for(String key : root.keySet()){
			Object item = root.get(key);
			if(item instanceof JSONObject){
				replaceAll(var, replacement, (JSONObject) item);
			} else if(item instanceof JSONArray){
				replaceAll(var, replacement, (JSONArray) item);
			} else if(item instanceof String){
				if(item.equals(StringUtils.bracket(var))){
					root.put(key, replacement);
				} else {
					root.put(key,((String)item).replaceAll(StringUtils.quote(var), replacement.toString()));
				}
			}
		}
	}
	
	public static void replaceAll(String var, Object replacement, JSONArray root){
		for(int i = 0; i < root.length(); i++){
			Object item = root.get(i);
			if(item instanceof JSONObject){
				replaceAll(var, replacement, (JSONObject) item);
			} else if(item instanceof JSONArray){
				replaceAll(var, replacement, (JSONArray) item);
			} else if(item instanceof String){
				if(item.equals(StringUtils.bracket(var))){
					root.put(i, replacement);
				} else {
					root.put(i, ((String)item).replaceAll(StringUtils.quote(var), replacement.toString()));
				}
			}
		}
	}
	
	public static JSONObject mapToJson(Map<String, ?> map){
		JSONObject out = new JSONObject();
		for(String key : map.keySet()){
			out.put(key, map.get(key));
		}
		return out;
	}
	
//	public static List<String> arrayToList(JSONArray root){
//		List<String> out = new ArrayList<String>(root.length());
//		for(Object o : root){
//			out.add(o.toString());
//		}
//		return out;
//	}

}
