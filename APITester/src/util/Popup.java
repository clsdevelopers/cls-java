package util;

import java.awt.Dimension;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.function.Consumer;

import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

public class Popup {
	public static void popErrorWindow(Consumer<PrintWriter> consumer){
		popWindow("Error", JOptionPane.ERROR_MESSAGE, consumer);
	}
	
	public static void popWindow(String title, int messageType, Consumer<PrintWriter> consumer){
		StringWriter strw = new StringWriter();
		PrintWriter writer = new PrintWriter(strw);
		consumer.accept(writer);
		writer.flush();
		
		JTextArea area = new JTextArea();
		area.setText(strw.toString());
		area.setEditable(false);
		JScrollPane scrollpane = new JScrollPane(area);
		scrollpane.setMaximumSize(new Dimension(800, 500));
		scrollpane.setPreferredSize(new Dimension(800, 500));
		scrollpane.getViewport().setMaximumSize(new Dimension(800, 500));
		
		JOptionPane.showMessageDialog(null, scrollpane, title, messageType);
	}
	
	public static void popMessageWindow(Consumer<PrintWriter> consumer){
		popWindow("Message", JOptionPane.INFORMATION_MESSAGE, consumer);
	}
}
