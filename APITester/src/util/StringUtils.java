package util;

import java.util.regex.Pattern;

public class StringUtils {

	public static String quote(String in){
		return Pattern.quote(StringUtils.bracket(in));
	}

	public static String bracket(String in){
		return "<<"+in+">>";
	}

}
