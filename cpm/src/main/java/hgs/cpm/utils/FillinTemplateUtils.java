package hgs.cpm.utils;

import gov.dod.cls.db.ClauseRecord;
import hgs.cpm.ecfr.util.EcfrFillInUtils;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Entities.EscapeMode;
import org.jsoup.parser.Parser;
import org.jsoup.select.Elements;

import com.gargoylesoftware.htmlunit.javascript.host.Element;

class FillinInstructionRecord {


	private String sInstruction;
	private String sHtmlInstruction;
	private String sMarkerAndInstruction;
	private String sFillinCode;
	
	public void setMarkerAndInstruction(String sSubstring) {
		this.sMarkerAndInstruction = sSubstring;
	}
	
	public String getMarkerAndInstruction() {
		return sMarkerAndInstruction;
	}
	
	public void setInstruction(String sSubstring) {
		this.sInstruction = sSubstring;
	}
	
	public String getInstruction() {
		return sInstruction;
	}
	
	public void setHtmlInstruction(String htmlSubstring) {
		this.sHtmlInstruction = htmlSubstring;
	}
	
	public String getHtmlInstruction() {
		return sHtmlInstruction;
	}

	public void setFillinCode(String sFillinCode) {
		this.sFillinCode = sFillinCode;
	}	

}

/*

1. FillinTemplateUtils - search for FillinTemplateUtils in EcfrParseFillins.java. Comment in all occurrences.

2. Create an input file with clause and placeholder text.

Input File: /Users/bgreene/Desktop/clause_instructions.txt
Sample Input

52.246-19, [Contracting Officer shall insert period of time]
52.246-19, [Contracting Officer shall insert period of time]

3. Output file: /Users/bgreene/Desktop/clause_out.txt

 */

public final class FillinTemplateUtils {
	
	
	public static String parseFillinInstructions(String clauseNum, String html, ArrayList<String> clauseArray) {
				
		
		boolean bFoundClause = false;
		ArrayList<String> fillinArray = new ArrayList<String>();
		
		for (Integer i=0; i<clauseArray.size(); i++)
		{
			if (clauseArray.get(i).toUpperCase().startsWith(clauseNum.toUpperCase() + ",")) {
				
				fillinArray.add(clauseArray.get(i));
				bFoundClause = true;
			}
		}
		
		if (!bFoundClause)
			return html;
		
		writeLine("-------------");
		
		String sTmp = clauseNum;
		sTmp = sTmp.replace(".", "_");
		sTmp = sTmp.replace("-", "_");
		sTmp = sTmp.replace(" ", "_");
		sTmp = sTmp.replace("Alternate_", "Alt");
		sTmp = sTmp.replace("ALTERNATE_", "Alt");
		
		writeLine("case \"" + clauseNum.toUpperCase() + "\":");
		writeLine("html = EcfrFillInUtils.process" + sTmp + "(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);");
		writeLine("skipGenericProcessing = true;");
		writeLine("break;");
		
		writeLine("-------------");
		
		writeLine("private static String process" + sTmp + " (int iStg1OfficialClauseId, String html, ");
		writeLine("\tTemplateStg2ClauseFillInRecord oTemplateStg2ClauseFillInRecord, ");
		writeLine("\tPreparedStatement psFillInInsertClause) throws SQLException {");
		writeLine("");
		writeLine("\thtml = HtmlUtils.normalizeFillins(html);");
		
		html = HtmlUtils.normalizeHtml(html); //TO DO: remove this
		html = HtmlUtils.normalizeFillins(html);
		
		String htmlCleaned = html;
		
		for (Integer i=0; i<fillinArray.size(); i++) {
			
			String sInstruction = fillinArray.get(i);
			sInstruction = sInstruction.substring(sInstruction.indexOf(",") + 1);
			sInstruction = sInstruction.trim();
			sInstruction = sInstruction.replaceAll(Pattern.quote("“"), "&ldquo;");
			sInstruction = sInstruction.replaceAll(Pattern.quote("”"), "\"");
		
			FillinInstructionRecord rec1 = new FillinInstructionRecord();
			rec1.setInstruction(sInstruction);
			if (!getFillinHtmlInstruction (rec1, html))
			{
				writeLine ("oh oh, " + sInstruction + " was not found" );
				continue;
			}
			
			getFillinMarker (rec1, html);
			
			sTmp = clauseNum;
			sTmp = sTmp.replace(" ", "_");
			sTmp = sTmp.replace("Alternate_", "Alt");
			sTmp = sTmp.replace("ALTERNATE_", "Alt");
			sTmp = "textbox_man_" + sTmp + "["+ i.toString() + "]";
				
			writeLine("\thtml = html.replaceFirst(Pattern.quote(\"" + rec1.getMarkerAndInstruction() + "\"), \"{{" + sTmp + "}}\");");
			writeLine("");
			String sPlaceholder = rec1.getInstruction();
			
			if (sPlaceholder.indexOf("[") >= 0) {
				sPlaceholder = sPlaceholder.substring(1);
				sPlaceholder = sPlaceholder.substring (0, sPlaceholder.length()-1);
			}
			else if (sPlaceholder.indexOf("(") >= 0) {
				sPlaceholder = sPlaceholder.substring(1);
				sPlaceholder = sPlaceholder.substring (0, sPlaceholder.length()-1);
			}
			
			if (i==0)
				writeLine("\tString sPlaceholder = \"" + sPlaceholder + "\";");
			else
				writeLine("\tsPlaceholder = \"" + sPlaceholder + "\";");
			writeLine("");
			
			writeLine("\toTemplateStg2ClauseFillInRecord.setRegularFillin(iStg1OfficialClauseId,");
			writeLine("\t\t\"" + sTmp + "\", \"S\", 100, // piFillInMaxSize, ");
			writeLine("\t\tnull, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows");
			writeLine("\t\tpsFillInInsertClause);");
			writeLine("");	
			
			// remove the string, in case it checked again.
			html = html.replaceFirst(Pattern.quote(rec1.getMarkerAndInstruction()), "");
			
		}
		
		writeLine("");
		writeLine("\thtml = html.replace(_Fillin_Marker, \"{{textbox}}&nbsp;\");");
		writeLine("\thtml = EcfrFillInUtils.processTextAndCheck(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);");
		writeLine("\thtml = HtmlUtils.parseFillinsAndNormalize(html);");
		writeLine("");
		writeLine("\treturn html;");
		writeLine("}");
		
		return htmlCleaned;
	}
	
	private static Boolean getFillinMarker (FillinInstructionRecord rec, String html) {
		
		int iPos = html.indexOf(rec.getHtmlInstruction());
		if (iPos < 0)
			return false;
		
		boolean bFoundMarker = false;
		boolean bTagOn = false; 
		Integer i=iPos;
		
		for (; i>=0 && i>= iPos-10 && i>=0; i--) {
			if (html.charAt(i) == '>')
				bTagOn = true;
			
			while (bTagOn) {
				if (html.charAt(i) == '<') {
					bTagOn = false;
					
					i--;
					if ((i>=0) && (html.charAt(i) == '>')) {
						bTagOn = true;
						continue;
					}
				}
				else
					i--;
			} 
			
			while ((i>=0) && (html.charAt(i) == '_')) {
				i--;
				bFoundMarker = true;
			}
			if (bFoundMarker) {
				i++;
				break;
			}
		}
		
		if (bFoundMarker)
		{
			String sTmp = html.substring(i, iPos+rec.getHtmlInstruction().length());
			rec.setMarkerAndInstruction (sTmp);
			return true;
		}
		else {
			rec.setMarkerAndInstruction (rec.getHtmlInstruction());
			return false;
		}
	}
	
	private static Boolean getFillinHtmlInstruction (FillinInstructionRecord rec, String html) {
		boolean bContinue = true;
		boolean bTagOn = false; 
		
		String sInstruct = rec.getInstruction();
		
		Integer iBeg = html.indexOf(sInstruct.charAt(0), 0);
		if (iBeg < 0) 
		return false;
		
		String sTmp = "";
		Integer j = iBeg;
		
		while (bContinue) {

			for (Integer i=0; i<sInstruct.length() && j<html.length(); i++) {

				if (( j<html.length()) && (html.charAt(j) == '<'))
					bTagOn = true;

				while (bTagOn) {
					if (( j<html.length()) && (html.charAt(j) == '>')) {
						bTagOn = false;
						
						j++;
						if (( j<html.length()) && (html.charAt(j) == '<')) {
							bTagOn = true;
							continue;
						}
					}
					else
						j++;
				} 
				
				if (j<html.length())
					sTmp += html.charAt(j);
				j++;
			}

			if (sTmp.equals(sInstruct)) { 
				bContinue  = false;
			}
			else {
				sTmp = "";
				iBeg = html.indexOf(sInstruct.charAt(0), iBeg + 1);
				j = iBeg;
		
				if (iBeg < 0)
					bContinue = false;
			}
		}
		
		if (sTmp.equals(sInstruct)) {
			rec.setInstruction(sTmp);
			rec.setHtmlInstruction(html.substring (iBeg, j));
			return true;
		}
		
		return false;
	}
	
	public static ArrayList<String> ReadFillinList() {
		ArrayList<String> clauseArray = new ArrayList<String>();
		
		File fin = new File("/Users/bgreene/Desktop/clause_instructions.txt");
		
		// Construct BufferedReader from FileReader
		BufferedReader br;
		try {
			br = new BufferedReader(new FileReader(fin));
		
			String line = null;
		
			while ((line = br.readLine()) != null) {
				clauseArray.add(line);
				//System.out.println(line);
			}
			br.close();
			
		} catch (FileNotFoundException e) {
			
			e.printStackTrace();
		}catch (IOException e) {
			
			e.printStackTrace();
		}
		
	 return clauseArray;
		
	}
	
	private static void writeLine (String line) {
		FileWriter fw = null;
		BufferedWriter bw = null;
		PrintWriter out = null;
		try {
		    fw = new FileWriter("/Users/bgreene/Desktop/clause_out.txt", true);
		    bw = new BufferedWriter(fw);
		    out = new PrintWriter(bw);
		    out.println(line);
		    out.close();
		} catch (IOException e) {
		    //exception handling left as an exercise for the reader
		}
		finally {

		    if(out != null)
		            out.close();

		    try {
		        if(bw != null)
		            bw.close();
		    } catch (IOException e) {
		        //exception handling left as an exercise for the reader
		    }
		    try {
		        if(fw != null)
		            fw.close();
		    } catch (IOException e) {
		        //exception handling left as an exercise for the reader
		    }
		}
	}

	
}