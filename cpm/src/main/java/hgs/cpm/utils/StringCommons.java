package hgs.cpm.utils;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class StringCommons {

	public final static boolean isEmpty(String value) {
		return (value == null) || value.isEmpty();
	}

	public final static boolean isNotEmpty(String value) {
		return ! StringCommons.isEmpty(value);
	}
	
	public final static String iif(String value, String defaultValue) {
		return (StringCommons.isEmpty(value) ? defaultValue : value);
	}
	
	public final static String iif(String value) {
		return StringCommons.iif(value, "");
	}
	
	public static Date getNow() {
		Calendar calStart = Calendar.getInstance();
		return calStart.getTime();
	}
	
	public static String fixPrescription(String sPrescription) {
		if ((sPrescription == null))
			return null;
		sPrescription = sPrescription.trim();
		if (sPrescription.isEmpty())
			return sPrescription;
		sPrescription = sPrescription.replace(" (", "(").replace("CLASS ", "").replace("DEVIATION ", "").replace("DFARS ", "").replaceAll("'", "").replaceAll("\\xa0", "").trim();
		if (sPrescription.startsWith("(") && sPrescription.endsWith(")")) {
			sPrescription = sPrescription.substring(1, sPrescription.length() - 1);
		}
		if (sPrescription.startsWith("[") && sPrescription.endsWith("]")) {
			sPrescription = sPrescription.substring(1, sPrescription.length() - 1);
		}
		return sPrescription;
	}
	
	private static void addPrescription(ArrayList<String> arrayList, String sPrescription) {
		sPrescription = StringCommons.fixPrescription(sPrescription);
		// sPrescription = sPrescription.replace(" (", "(").replace("CLASS ", "").replace("DEVIATION ", "").replace("DFARS ", "").trim();
		sPrescription = sPrescription.replaceAll("'", "");
		sPrescription = sPrescription.replaceAll(" ", "");
		sPrescription = sPrescription.replaceAll("PGI", "PGI ");
		sPrescription = sPrescription.replaceAll("©", "(c)");
		if (!(sPrescription.isEmpty() || sPrescription.equals("-") || sPrescription.equals("."))) {
			if ((!"NOT APPLICABLE".equals(sPrescription)) &&
				(!"PRESCRIPTIONS INCLUDED IN FOLLOW-ON QUESTIONS".equals(sPrescription)))
				arrayList.add(sPrescription);
		}
	}
	
	public static String prescriptionStripSpecialChars(String sPrescription) {
		return sPrescription.replace("\\", "");
	}
	
	public static boolean prescriptionSplit(String sPrescription, String sOperator, ArrayList<String> arrayList) {
		sPrescription = sPrescription.trim();
		if (sPrescription.isEmpty())
			return true;
		
		if (sPrescription.indexOf(sOperator) > 0) {
			String[] aOrs = sPrescription.split(sOperator);
			for (String sOr : aOrs) {
				sOr = sOr.trim();
				if (sOr.charAt(sOr.length() - 1) == ',') {
					sOr = sOr.substring(0, sOr.length() - 1);
				}
				addPrescription(arrayList, prescriptionStripSpecialChars(sOr));
				//if (!sOr.isEmpty())
				//	arrayList.add(prescriptionStripSpecialChars(sOr));
			}
			return true;
		} else {
			addPrescription(arrayList, prescriptionStripSpecialChars(sPrescription));
			//arrayList.add(prescriptionStripSpecialChars(sPrescription));
			return false;
		}
	}

	/*
	public static void splitOr(String sPrescription, ArrayList<String> arrayList) {
		sPrescription = sPrescription.trim();
		if (sPrescription.isEmpty())
			return;
		
		if (sPrescription.indexOf(" or ") > 0) {
			String[] aOrs = sPrescription.split(" or ");
			for (String sOr : aOrs) {
				sOr = sOr.trim();
				if (sOr.charAt(sOr.length() - 1) == ',') {
					sOr = sOr.substring(0, sOr.length() - 1);
				}
				if (!sOr.isEmpty())
					arrayList.add(prescriptionStripSpecialChars(sOr));
			}
		} else {
			arrayList.add(prescriptionStripSpecialChars(sPrescription));
		}
	}
	*/
	
	public static String[] parsePrescriptions(String psPrescriptions) {
		ArrayList<String> arrayList = new ArrayList<String>();
		parsePrescriptions(psPrescriptions, arrayList);
		return arrayList.toArray(new String[arrayList.size()]);
	}
	
	public static void parsePrescriptions(String psPrescriptions, ArrayList<String> arrayList) {
		psPrescriptions = psPrescriptions.trim();
		//ArrayList<String> arrayList = new ArrayList<String>();
		String[] aPrescriptions = psPrescriptions.split("\n");
		for (String sPrescription: aPrescriptions) {
			sPrescription = sPrescription.trim();
			if (sPrescription.length() > 0) {
				if (sPrescription.indexOf(",") > 0) {
					String[] aAnds = sPrescription.split(",");
					for (String sAnd : aAnds) {
						sAnd = sAnd.trim();
						if (sAnd.startsWith("or ")) {
							sAnd = sAnd.substring(3);
						}
						prescriptionSplit(sAnd, " or ", arrayList); // splitOr(sAnd, arrayList);
					}
				} else if (sPrescription.indexOf(" or ") > 0) {
					prescriptionSplit(sPrescription, " or ", arrayList); // splitOr(sPrescription, arrayList);
				}
				else {
					if (sPrescription.indexOf(" AND ") > 0) {
						prescriptionSplit(sPrescription, " AND ", arrayList);
					} else if (sPrescription.indexOf(" and ") > 0) {
						prescriptionSplit(sPrescription, " and ", arrayList);
					} else
						addPrescription(arrayList, prescriptionStripSpecialChars(sPrescription));
						//arrayList.add(prescriptionStripSpecialChars(sPrescription));
				}
			}
		}
		// return arrayList.toArray(new String[arrayList.size()]);
	}
	
	public static String[] splitTrim(String psValue, String psDivider) {
		ArrayList<String> arrayList = new ArrayList<String>();
		String[] aLines = psValue.split(psDivider);
		for (String sLine: aLines) {
			sLine = sLine.trim();
			if (sLine.length() > 0) {
				arrayList.add(sLine);
			}
		}
		return arrayList.toArray(new String[arrayList.size()]);
	}
	
	public static String replaceAll(String value, String target, String replacement) {
		if (value == null)
			return null;
		
		String result = "";
		String[] aItems = value.split(target);
		if (aItems.length <= 1)
			return value;
		
		for (String item : aItems) {
			result += (result.isEmpty() ? "" : replacement) + item;
		}
		return result;
	}
	
	public static String convertUnicodeToHtml(String pHtml) {
		return HtmlUtils.convertUnicodeToHtml(pHtml);
	}
	
	public static Date monthYearToDate(String psMonthYear, String psClauseName, String sExcelRow) {
		if (psMonthYear == null) {
			System.out.println(psClauseName + ": empty effective date" + sExcelRow) ;
			return null;
		}
		String monthYear = psMonthYear.trim();
		if ("(Jan 2004)(Jan 2004)".equals(monthYear)) {
			monthYear = "JAN 2004";
		} else {
			if (monthYear.length() < 8) {
				System.out.println(psClauseName + ": unexpected effective date format '" + psMonthYear + "'" + sExcelRow);
			}
			if (monthYear.endsWith("."))
				monthYear = monthYear.substring(0, monthYear.length() - 1).trim();
			if (monthYear.startsWith("(") || monthYear.startsWith("[")) {
				monthYear = monthYear.substring(1);
			}
			if (monthYear.endsWith(")") || monthYear.endsWith("]")) {
				monthYear = monthYear.substring(0, monthYear.length() - 1);
			}
			if (monthYear.contains("-"))
				monthYear = monthYear.replace('-', ' ');
		}
		String[] aItems = monthYear.split(" ");
		if (aItems.length != 2) {
			System.out.println(psClauseName + ": unexpected effective date format '" + psMonthYear + "'" + sExcelRow);
			return null;
		}
		try {
			String sMonth = aItems[0].toUpperCase().trim();
			if (sMonth.endsWith("."))
				sMonth = sMonth.substring(0, sMonth.length() - 1).trim();
			int iMonth = 0;
			switch (sMonth)  {
			case "JAN":
			case "JANUARY":
				iMonth = 0; break;
		    case "FEB":
			case "FEBRUARY":
		    	iMonth = 1; break;
		    case "MAR":
		    case "MARCH":
		    	iMonth = 2; break;
		    case "APR":
		    case "APRIL":
		    	iMonth = 3; break;
		    case "MAY": iMonth = 4; break;
		    case "JUN":
		    case "JUNE":
		    	iMonth = 5; break;
		    case "JUL":
		    case "JULY":
		    	iMonth = 6; break;
		    case "AUG":
		    case "AUGUST":
		    	iMonth = 7; break;
		    case "SEP":
		    case "SEPT":
		    case "SEPTEMBER":
		    	iMonth = 8; break;
		    case "OCT":
		    case "OCTOBER":
		    	iMonth = 9; break;
		    case "NOV":
		    case "NOVEMBER":
		    	iMonth = 10; break;
		    case "DEC":
		    case "DECEMBER":
		    	iMonth = 11; break;
		    default:
		    	System.out.println(aItems[0] + " - Invalid Effective Month" + sExcelRow);
		    	break;
			}
			
			String sYear = aItems[1];
			if (sYear.endsWith(")") || sYear.endsWith("]")) {
				sYear = sYear.substring(0, sYear.length() - 1);
			}
			if (sYear.length() == 2)
				sYear = "20" + sYear;
			int iYear = Integer.parseInt(sYear);
			Calendar oCalendar = Calendar.getInstance();
			oCalendar.set(iYear, iMonth, 1);
			
			Date oDate = new Date(oCalendar.getTimeInMillis());
			return oDate;
			
		} catch (Exception oError) {
			oError.printStackTrace();
			System.out.println(psClauseName + ": monthYearToDate Error for '" + psMonthYear + "'" + sExcelRow);
			return null;
		}
	}
	
}
