package hgs.cpm.utils;

import gov.dod.cls.db.ClauseRecord;
import hgs.cpm.ecfr.util.EcfrFillInUtils;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Entities.EscapeMode;
import org.jsoup.parser.Parser;
import org.jsoup.select.Elements;

import com.gargoylesoftware.htmlunit.javascript.host.Element;

public final class HtmlUtils {

	public static String parseFillinsAndNormalize(String html) {
		if ((html == null) || html.isEmpty())
			return html;
		
		//String contents;
		String replace;
		
	//	html = html.replaceAll("<div class=\"fpdash\"><span class=\"fpdash\">([^<]*)</span></div>", "$1 {{textbox}}");
	//	html = html.replaceAll("<p>_+</p>", "<p>{{textbox}}</p>");
		
//		html = html.replaceAll("Attachment _", "Attachment {{textbox}}");
		
		// one offs where there are no leading underscores
//		replace = "\\[<span style=\"font-style:italic\">insert name of SBA's contractor</span>\\]";
//		html = html.replaceAll(replace, "{{textbox}} " + replace);
//		
//		replace = "\\[<span style=\"font-style:italic\">insert name of contracting agency</span>\\]";
//		html = html.replaceAll(replace, "{{textbox}} " + replace);
//		
//		replace = "\\[<span style=\"font-style:italic\">insert telephone number</span>\\]";
//		html = html.replaceAll(replace, "{{textbox}} " + replace);
//		
//		replace = "\\[<span style=\"font-style:italic\">state purpose</span>\\]";
//		html = html.replaceAll(replace, "{{textbox}} " + replace);
//		
//		replace = "\\[<span style=\"font-style:italic\">Insert current budget rate here.</span>\\]";
//		html = html.replaceAll(replace, "{{textbox}} " + replace);
//
//		replace = "\\[<span style=\"font-style:italic\">Contracting Officer to insert source of rate</span>\\]";
//		html = html.replaceAll(replace, "{{textbox}} " + replace);
//		
//		replace = "\\[<span style=\"font-style:italic\">Contracting Officer to insert applicable point of contact information cited in PGI 237.171-3(b).</span>\\]";
//		html = html.replaceAll(replace, "{{textbox}} " + replace);
//		
//		replace = "\\[<span style=\"font-style:italic\">Contracting Officer to list applicable excepted materials or indicate “none”</span>\\]";
//		html = html.replaceAll(replace, "{{textbox}} " + replace);
//		
//		replace = "\\[<span style=\"font-style:italic\">Insert portion of labor rate attributable to profit.</span>\\]";
//		html = html.replaceAll(replace, "{{textbox}} " + replace);
		
		html = HtmlUtils.normalizeHtml(html); //TO DO: remove this
		
		//start
		replace = "[<i>insert name of SBA's contractor</i>]";
		html = html.replace(replace, "{{textbox}} " + replace);
				
		replace = "[<i>insert name of contracting agency</i>]";
		html = html.replace(replace, "{{textbox}} " + replace);
				
		replace = "[<i>insert telephone number</i>]";
		html = html.replace(replace, "{{textbox}} " + replace);
				
		replace = "[<i>state purpose</i>]";
		html = html.replace(replace, "{{textbox}} " + replace);
				
		replace = "[<i>Insert current budget rate here.</i>]";
		html = html.replace(replace, "{{textbox}} " + replace);

		replace = "[<i>Contracting Officer to insert source of rate</i>]";
		html = html.replace(replace, "{{textbox}} " + replace);
				
		replace = "[<i>Contracting Officer to insert applicable point of contact information cited in PGI 237.171-3(b).</i>]";
		html = html.replace(replace, "{{textbox}} " + replace);
				
		replace = "[<i>Contracting Officer to list applicable excepted materials or indicate “none”</i>]";
		html = html.replace(replace, "{{textbox}} " + replace);
				
		replace = "[<i>Insert portion of labor rate attributable to profit.</i>]";
		html = html.replace(replace, "{{textbox}} " + replace);
		
		html = html.replace("Attachment _", "Attachment {{textbox}}");
		html = html.replaceAll("<p>_+</p>", "<p>{{textbox}}</p>");
		//end
		
		//52.236-27
		html = html.replace("<div>Name:</div><div>Address:</div><div>&nbsp;</div><div>Telephone:</div>",
				"<p>Name:<br>{{textbox}}</p><p>Address:<br>{{textbox}}<br>{{textbox}}</p><p>Telephone:<br>{textbox}}</p>");
		
		//					*********additions June 24, 2015 START*********
		//52.211-9 Alternate I
		html = html.replace("during the months __", "during the months {{textbox}};");
		html = html.replace("not sooner than __", "not sooner than {{textbox}}");
		html = html.replace("or later than __", "or later than {{textbox}}");
		
		//52.212-3 Alternate I
		html = html.replace("_Black American", "{{checkbox}} Black American");
		html = html.replace("_Hispanic American", "{{checkbox}} Hispanic American");
		html = html.replace("_Native American", "{{checkbox}} Native American");
		html = html.replace("_Asian-Pacific American", "{{checkbox}} Asian-Pacific American");
		html = html.replace("_Subcontinent Asian", "{{checkbox}} Subcontinent Asian");
		html = html.replace("_Individual/concern", "{{checkbox}} Individual/concern");
		
		//52.212-4 Alternate I
		//html = html.replace("[<i>Insert any subcontracts for services to be excluded from the hourly rates prescribed in the schedule.</i>]", "{{textbox}} [<i>Insert any subcontracts for services to be excluded from the hourly rates prescribed in the schedule.</i>]");
		//html = html.replace("[<i>Insert a fixed amount for the indirect costs and payment schedule.", "{{textbox}} [<i>Insert a fixed amount for the indirect costs and payment schedule.");
		
		//52.215-20 Alternates, 52.215-21 Alternates
		html = html.replace("[<i>Insert media format, e.g., electronic spreadsheet format,", "{{textbox}} [<i>Insert media format, e.g., electronic spreadsheet format,");
		html = html.replace("<i>[Insert description of the data and format that are required", "{{textbox}} <i>[Insert description of the data and format that are required");
		html = html.replace("[<i>Insert description of the data and the format that are required,", "{{textbox}} [<i>Insert description of the data and the format that are required,");
		html = html.replace("[<i>Insert description of the data and format that are required", "{{textbox}} [<i>Insert description of the data and format that are required");
		html = html.replace("<i>[Insert description of the data and the format that are required,", "{{textbox}} <i>[Insert description of the data and the format that are required,");
		html = html.replace(" [<i>Insert media format</i>]", "{{textbox}}  [<i>Insert media format</i>]");
	
		//52.219-1 Alternate I & 52.219-4 & 52.222-18
		html = html.replace("□", "{{checkbox}}");
		
		//52.227-11
		html = html.replace("(identify the contract)", "{{textbox}} (identify the contract)");
		html = html.replace("(identify the agency)", "{{textbox}} (identify the agency)");
		
		//52.227-7
		html = html.replace("(&nbsp;&nbsp;&nbsp;) Owner", "(&nbsp;{{checkbox}}&nbsp;) Owner");
		html = html.replace("(&nbsp;&nbsp;&nbsp;) Licensee", "(&nbsp;{{checkbox}}&nbsp;) Licensee");
		
		//52.252-3
		html = html.replace("Portions of this solicitation are altered as follows:", "Portions of this solicitation are altered as follows:<br>{{textbox}}");
		
		//52.252-4
		html = html.replace("Portions of this contract are altered as follows:", "Portions of this contract are altered as follows:<br>{{textbox}}");
		
		//252.204-7010
		// Moved to it's own process method.
		//html = html.replace("<i>[Contracting Officer to insert Program Manager's name, mailing address, e-mail address, telephone number, and facsimile number];</i>",
		//		"{{textbox}}<br><i>[Contracting Officer to insert Program Manager's name, mailing address, e-mail address, telephone number, and facsimile number];</i>");
		
		//252.215-7004
		html = html.replace("[<i>U.S. Contracting Officer to insert description of the data required in accordance with FAR 15.403-3(a)(1)</i>]",
				"{{textbox}}<br>[<i>U.S. Contracting Officer to insert description of the data required in accordance with FAR 15.403-3(a)(1)</i>]");
		html = html.replace("[<i>or higher dollar value specified by the U.S. Contracting Officer in the solicitation</i>]",
				"{{textbox}} [<i>or higher dollar value specified by the U.S. Contracting Officer in the solicitation</i>]");
		
		//252.217-7002
		// add fill in method
		//html = html.replace("(insert address)", "{{textbox}} (insert address)");
		//html = html.replace("(insert beginning and ending dates and insert hours during day)", "{{textbox}} (insert beginning and ending dates and insert hours during day)");
		
		//252.225-7044 Alternate I, 252.225-7044, 252.225-7045 Alternate I, 252.225-7045 Alternate II, 252.225-7045 Alternate III
		html = html.replace("[<i>Contracting Officer to list applicable excepted materials or indicate", 
				"{{textbox}}<br>[<i>Contracting Officer to list applicable excepted materials or indicate");
								
		//252.227-7005 Alternate II
		html = html.replace("____ day of ____ ,____;", "{{textbox}} day of {{textbox}}, {{textbox}};");
		
		//252.227-7012
		//html = html.replace("[month, year]","{{textbox}}, {{textbox}} [month, year]");
		
		//252.232-7014
		//html = html.replace("<i>[Insert current budget", "{{textbox}} <i>[Insert current budget");
		
		//252.235-7010
		html = html.replace("(name of contracting agency(ies))", "{{textbox}} (name of contracting agency(ies))");
		html = html.replace("(Contracting agency(ies) contract number(s))", "{{textbox}} (Contracting agency(ies) contract number(s))");
		
		//252.237-7019
		//html = html.replace("<i>[Contracting Officer to insert applicable point of contact information cited in PGI 237.171-3(b).]</i>", 
		//		"{{textbox}} <i>[Contracting Officer to insert applicable point of contact information cited in PGI 237.171-3(b).]</i>");
		
		//252.237-7023
		html = html.replace("attachment _", "attachment {{textbox}}");
		//					*********additions June 24, 2015 END*********
		
		//52.247-60
		html = html.replace("Palletized/skidded __ Yes __ No", "Palletized/skidded {{checkbox}} Yes {{checkbox}} No");
		
		//52.247-51
		html = html.replace("(&nbsp;&nbsp;&nbsp;) Paragraph (b)", "{{checkbox}} Paragraph (b)");
		html = html.replace("(&nbsp;&nbsp;&nbsp;) Paragraph (c)", "{{checkbox}} Paragraph (c)");
		html = html.replace("(&nbsp;&nbsp;&nbsp;) Paragraph (e)", "{{checkbox}} Paragraph (e)");
		
		//52.247-69
		html = html.replace("the final destination(s) for the supplies will be considered to be as follows:",
				"the final destination(s) for the supplies will be considered to be as follows:<br>{{textbox}}");
		 
		//52.246-11
		html = html.replace("[<i>Contracting Officer insert the title, number, date, and tailoring (if any) of the higher-level quality standards.</i>]",
				"<table><tr><th>Title</th><th>Number</th><th>Date</th><th>Tailoring</th></tr><tr><td>{{textbox}}</td><td>{{textbox}}</td><td>{{textbox}}</td><td>{{textbox}}</td></tr><tr><td>{{textbox}}</td><td>{{textbox}}</td><td>{{textbox}}</td><td>{{textbox}}</td></tr><tr><td>{{textbox}}</td><td>{{textbox}}</td><td>{{textbox}}</td><td>{{textbox}}</td></tr><tr><td>{{textbox}}</td><td>{{textbox}}</td><td>{{textbox}}</td><td>{{textbox}}</td></tr><tr><td>{{textbox}}</td><td>{{textbox}}</td><td>{{textbox}}</td><td>{{textbox}}</td></tr></table>"
				+ "[<i>Contracting Officer insert the title, number, date, and tailoring (if any) of the higher-level quality standards.</i>]");
		
		//52.226-3
		html = html.replace("it ___does ____ does not reside", "it {{checkbox}} does {{checkbox}} does not reside");
				
		//52.219-1 Alternate I
		html = html.replace("__ Black American.", "{{checkbox}} Black American.");
		html = html.replace("__ Hispanic American.", "{{checkbox}} Hispanic American.");
		html = html.replace("__ Native American", "{{checkbox}} Native American");
		html = html.replace("__ Asian-Pacific American", "{{checkbox}} Asian-Pacific American");
		html = html.replace("__ Subcontinent Asian", "{{checkbox}} Subcontinent Asian");
		html = html.replace("__ Individual/concern", "{{checkbox}} Individual/concern");
		
		//52.217-9
		html = html.replace("_ (months) (years)", "{{textbox}} {{textbox}} (months) (years))");
		
		//52.213-3 Alternate I
		html = html.replace("[_] Yes or [_] No", "{{checkbox}} Yes or {{checkbox}} No");
		
		//252.236-7001
		// CJ-1393, Fix incorrect HTML table header formatting for 252.236.7001
		html = html.replace("Title&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;File&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Drawing No.", 
				"<table><tr><th>Title</th><th>File</th><th>Drawing No.</th></tr><tr><td>{{textbox}}</td><td>{{textbox}}</td><td>{{textbox}}</td></td></tr><tr><td>{{textbox}}</td><td>{{textbox}}</td><td>{{textbox}}</td></td></tr><tr><td>{{textbox}}</td><td>{{textbox}}</td><td>{{textbox}}</td></td></tr><tr><td>{{textbox}}</td><td>{{textbox}}</td><td>{{textbox}}</td></td></tr><tr><td>{{textbox}}</td><td>{{textbox}}</td><td>{{textbox}}</td></td></tr></table>");
		
		//252.225-7046
		html = html.replace("<p>Name/Title of Duly Authorized Representative&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Date</p>",
				"<p>Name/Title of Duly Authorized Representative&nbsp;{{textbox}}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Date&nbsp;{{textbox}}</p>");
		
		//252.204-7007
		html = html.replace("__Use with Alternate", "{{checkbox}} Use with Alternate");
					
		//252.247-7021
		html = html.replace("{{textbox}}<p>(Insert the container types, sizes, capacities, and associated replacement values.)</p>",
				"<table><tr><th>Container Type</th><th>Size</th><th>Capacity</th><th>Replacement Value</th></tr><tr><td>{{textbox}}</td><td>{{textbox}}</td><td>{{textbox}}</td><td>{{textbox}}</td></tr><tr><td>{{textbox}}</td><td>{{textbox}}</td><td>{{textbox}}</td><td>{{textbox}}</td></tr><tr><td>{{textbox}}</td><td>{{textbox}}</td><td>{{textbox}}</td><td>{{textbox}}</td></tr><tr><td>{{textbox}}</td><td>{{textbox}}</td><td>{{textbox}}</td><td>{{textbox}}</td></tr><tr><td>{{textbox}}</td><td>{{textbox}}</td><td>{{textbox}}</td><td>{{textbox}}</td></tr></table><p>(Insert the container types, sizes, capacities, and associated replacement values.)");
		
		html = html.replace("__Alternate I", "{{checkbox}} Alternate I");
		
		//252.208-7000
		html = html.replace("<div><table border=\"1\" cellpadding=\"1\" cellspacing=\"1\" frame=\"void\" width=\"100%\"><tbody><tr><th scope=\"col\">Precious metal*</th><th scope=\"col\">Quantity</th><th scope=\"col\">Deliverable item (NSN and nomenclature)!!rs</th></tr><tr><td align=\"left\" colspan=\"3\" scope=\"row\">&nbsp;&nbsp;&nbsp;</td></tr></tbody></table></div>",
				"<div><table border=\"1\" cellpadding=\"1\" cellspacing=\"1\" frame=\"void\" width=\"100%\"><tbody><tr><th scope=\"col\">Precious metal*</th><th scope=\"col\">Quantity</th><th scope=\"col\">Deliverable item (NSN and nomenclature)</th></tr><tr><td>&nbsp;&nbsp;&nbsp;</td><td>&nbsp;&nbsp;&nbsp;</td><td>&nbsp;&nbsp;&nbsp;</td></tr><tr><td>&nbsp;&nbsp;&nbsp;</td><td>&nbsp;&nbsp;&nbsp;</td><td>&nbsp;&nbsp;&nbsp;</td></tr><tr><td>&nbsp;&nbsp;&nbsp;</td><td>&nbsp;&nbsp;&nbsp;</td><td>&nbsp;&nbsp;&nbsp;</td></tr><tr><td>&nbsp;&nbsp;&nbsp;</td><td>&nbsp;&nbsp;&nbsp;</td><td>&nbsp;&nbsp;&nbsp;</td></tr><tr><td>&nbsp;&nbsp;&nbsp;</td><td>&nbsp;&nbsp;&nbsp;</td><td>&nbsp;&nbsp;&nbsp;</td></tr></tbody></table></div>");
		
		//252.229-7003
		//html = html.replace("[<i>Contracting Officer must insert the applicable fiscal code(s) for military activities within Italy: 80028250241 for Army, 80156020630 for Navy, or 91000190933 for Air Force</i>]", "{{textbox}} [<i>Contracting Officer must insert the applicable fiscal code(s) for military activities within Italy: 80028250241 for Army, 80156020630 for Navy, or 91000190933 for Air Force</i>]");
		//252.229-7004
		html = html.replace("(<i>Contracting Officer insert amount at time of contract award)", "{{textbox}} (<i>Contracting Officer insert amount at time of contract award)");
		//252.232-7001
		//html = html.replace("(<i>insert the name of the disbursing office in the advance payment pool agreement</i>)", "{{textbox}} (<i>insert the name of the disbursing office in the advance payment pool agreement</i>");
		//252.232-7007
		//html = html.replace("Contract line item(s) <i>[Contracting Officer insert after negotiations]</i>", "Contract line item(s) {{textbox}} <i>[Contracting Officer insert after negotiations]</i>");
		//html = html.replace("(month) (day), (year)", "{{textbox}} (month) (day), (year)");
		//252.247-7001
		html = html.replace("in effect as of (<i>insert date</i>)", "in effect as of {{textbox}} (<i>insert date</i>)");
		//52.242-4
		html = html.replace("(<i>identify proposal and date</i>)", "{{textbox}} (<i>identify proposal and date</i>)");
		html = html.replace("(<i>identify period covered by rate</i>)", "{{textbox}} (<i>identify period covered by rate</i>)");
		//52.219-28
		html = html.replace("[<i>Contractor to sign and date and insert authorized signer's name and title</i>]", "Contractor Name: {{textbox}} Date: {{textbox}} Signer's Name: {{textbox}} Title: {{textbox}} <br>[<i>Contractor to sign and date and insert authorized signer's name and title</i>]");
		//52.215-7008
		html = html.replace("<i>[U.S. Contracting Officer to provide description of the data required in accordance with FAR 15.403-3(a)(1) with the notification].</i>", "{{textbox}} <i>[U.S. Contracting Officer to provide description of the data required in accordance with FAR 15.403-3(a)(1) with the notification].</i>");
		
		html = html.replace("&squ;", "{{checkbox}}");
		html = html.replace("&#x2610;", "{{checkbox}}");
		html = html.replaceAll("(_)+( )*\\((.{1,3})\\)", "{{checkbox}} ($3)");
		html = html.replaceAll("(_)+( )*\\(<i>(.{1,3})</i>\\)", "{{checkbox}} (<i>$3</i>)");
		html = html.replace("&#x25a1;", "{{checkbox}}");
		html = html.replace("[&nbsp;&nbsp;]", "{{checkbox}}");
		html = html.replaceAll("<td[^<]*></td>","<td>{{textbox}}</td>");
		html = html.replaceAll("<td[^<]*>&nbsp;&nbsp;&nbsp;</td>","<td>{{textbox}}</td>");
		html = html.replaceAll("<td[^<]*>&nbsp;&nbsp;</td>","<td>{{textbox}}</td>");
		html = html.replaceAll("<td[^<]*>&nbsp;</td>","<td>{{textbox}}</td>");
		html = html.replaceAll("<td[^<]*>\\(LIST\\)</td>","<td>{{textbox}}</td>");
		html = html.replaceAll("_(_)+", "{{textbox}}");
		html = html.replaceAll("_( )+", "{{textbox}} ");
		html = html.replaceAll("\\(   \\)", "{{checkbox}}");
		html = html.replaceAll("\\[( )+\\]", "{{checkbox}}");
		html = html.replaceAll("□", "{{checkbox}}");
		
		// html = html.replaceAll("☐", "{{checkbox}}");
		if (html.contains("☐"))
			html = html.replaceAll("☐", "{{checkbox}}");
		else {
			html = html.replaceAll("&squ;", "{{checkbox}}");
		}
		html = html.replaceAll("&#x25a1;", "{{checkbox}}"); // http://www.fileformat.info/info/unicode/char/25a1/index.htm
		
		html = html.replaceAll("\\[( )+\\]", "{{checkbox}}");
		html = html.replaceAll("\\[_+\\]", "{{checkbox}}");
		html = html.replaceAll("\\*(_)+", "{{textbox}}");

		html = html.replaceAll("″", "");	//CJ-838
		
		html = html.replaceAll("", "");	//CJ-838
		
		html = HtmlUtils.convertUnicodeToHtml(html);

		replace = "<meta http-equiv=\"content-type\" content=\"text/html; charset=utf-8\">";
		html = html.replaceAll(replace, "{{textbox}} " + replace);
		//if (html.contains("&#x") || html.contains("_"))
		//	System.out.println(html);
		return html;
	}
	
	public static String cleanupHtml(String html) {
		html = HtmlUtils.toISO(html);
		html = html.trim();
		while (html.contains("\n\n"))
			html = html.replaceAll("\n\n", "\n");
		while ((html.length() > 0) && (html.charAt(0) == '\n'))
			html = html.substring(1);
		int iLength = html.length();
		while (iLength > 0) {
			char ch = html.charAt(iLength - 1);
			if ((ch == '\n') || (ch == '\r')) {
				html = html.substring(0, (--iLength - 1));
			} else
				break;
		}
		html = html.replaceAll("—", "&mdash;").replaceAll("“", "&ldquo;");
		html = HtmlUtils.convertUnicodeToHtml(html);
		
		return html;
	}
	
	//The actual standardization. Uses jsoup to transform a string of html based on CLS conventions.
	public static String normalizeHtml(String html) {
		html = html.replaceAll("<div class=\"fpdash\"><span class=\"fpdash\">([^<]*)</span></div>", "$1 _____");
		html = html.replace("img src=\"/graphics/", "img src=\"http://www.ecfr.gov/graphics/");
		html = html.replace("a href=\"/graphics/", "a href=\"http://www.ecfr.gov/graphics/");
		org.jsoup.nodes.Document doc = Jsoup.parse(html, "", Parser.xmlParser());
		doc.select("span[style~=font-weight:\\s*bold]").tagName("b");
		doc.select("span[style~=font-style:\\s*italic]").tagName("i");
		Elements allElements = doc.select("*");
		allElements.removeAttr("class");
		allElements.removeAttr("style");
		allElements.removeAttr("font");
		allElements.removeAttr("span");
		doc.select("span").unwrap();
		doc.select("class").unwrap();
		doc.select("style").unwrap();
		doc.select("font").unwrap();

		doc.outputSettings().prettyPrint(false); //don't want extra linebreaks added.
		doc.outputSettings().charset("ISO-8859-2");
		doc.outputSettings().escapeMode(EscapeMode.extended);
		String result = doc.toString();
		if (result.startsWith("<h1>")) {
			int iPos = result.indexOf("</h1>");
			if (iPos > 1) {
				result = result.substring(iPos + "</h1>".length());
			}
		}
		else if ((result.startsWith("<h3>")) && !(result.startsWith("<h3>["))) {
			int iPos = result.indexOf("</h3>");
			if (iPos > 1) {
				result = result.substring(iPos + "</h3>".length());
			}
		}
		
		//52.209-3, 52.209-4
		result = result.replace("<h3>[<i>Contracting Officer shall insert details</i>]</h3>", "<p>[<i>Contracting Officer shall insert details</i>]</p>");
		//52.214-16
		result = result.replace("<h3>The bidder allows the following acceptance period: _ calendar days.</h3>", "<p>The bidder allows the following acceptance period: _ calendar days.</p>");
		//52.222-42
		result = result.replace("<h3>This Statement is for Information Only: It Is Not a Wage Determination</h3>","<p>This Statement is for Information Only: It Is Not a Wage Determination</p>");
		//52.227-20
		result = result.replace("<h1>SBIR Rights Notice (DEC 2007)</h1>", "<p>SBIR Rights Notice (DEC 2007)</p>");
		result = result.replace("<h3>(End of notice)</h3>", "<p>(End of notice)</p>");
		//52.227-21
		result = result.replace("<h3>Technical Data Declaration (JAN 1997)</h3>", "<p>Technical Data Declaration (JAN 1997)</p>");
		result = result.replace("<h3>(End of declaration)</h3>", "<p>(End of declaration)</p>");
		//52.227-3 Alternate II
		result = result.replace("<h3>List or identify the items to be included under this indemnity</h3>", "<p>(<i>List or identify the items to be included under this indemnity</i>)</p>");
		//52.247-2
		result = result.replace("<h3>(Name of regulatory body)</h3>", "<p>(Name of regulatory body)</p>");
		result = result.replace("<h3>(Authorization No.)</h3>","<p>(Authorization No.)</p>");
		//52.247-63
		result = result.replace("<h1>Statement of Unavailability of U.S.-Flag Air Carriers</h1>", "<p>Statement of Unavailability of U.S.-Flag Air Carriers</p>");
		result = result.replace("<h3>(End of statement)</h3>", "<p>(End of statement)</p>");
		//252.225-7000 Alternate I
		result = result.replace("<h3>Line Item Number</h3>", "<p>Line Item Number</p>");
		result = result.replace("<h3>Country of Origin</h3>", "<p>Country of Origin</p>");
		result = result.replace("<h3>Country of Origin (If known)</h3>", "<p>Country of Origin (If known)</p>");
		//252.227-7013
		result = result.replace("<h3>Government Purpose Rights</h3>", "<p>Government Purpose Rights</p>");
		result = result.replace("<h3>Limited Rights</h3>", "<p>Limited Rights</p>");
		result = result.replace("<h3>Special License Rights</h3>", "<p>Special License Rights</p>");
		//252.229-7001
		result = result.replace("<h3>NAME OF TAX: (Offeror Insert) RATE (PERCENTAGE): (Offeror Insert)</h3>", "<p>NAME OF TAX: (Offeror Insert) RATE (PERCENTAGE): (Offeror Insert)</p>");
		//52.222-18 
		result = result.replace("<h3>Listed End Product </h3>", "<p>Listed End Product </p>");
		result = result.replace("<h3>Listed Countries of Origin </h3>", "<p>Listed Countries of Origin </p>");
		
		// 508 Compliance
		result = result.replaceAll("<th scope=\"col\">&nbsp;&nbsp;&nbsp;</th>", "<th scope=\"col\"><span class=\"hide\">NoLabel</span></th>");
		result = result.replaceAll("<th scope=\"col\">&nbsp;&nbsp;</th>", "<th scope=\"col\"><span class=\"hide\">NoLabel</span></th>");
		
		// CJ-1314, Restore <thead> tags
		if ((result.indexOf("<tbody><tr><th") > 0) && (html.indexOf("</th></tr><tr><td") > 0)) {
			
			result = result.replaceAll("<tbody><tr><th", "<thead><tr><th"); 
			result = result.replaceAll("</th></tr><tr><td", "</th></tr></thead><tbody><tr><td");
		}
		
		
		// Look for (End of provision), prepended with an HTML tag delimeter
		// >(End of Provision)
		Integer iBeg = result.toUpperCase().indexOf(">(END OF PROVISION)");
		if (iBeg>0){
			String sReplace = result.substring(iBeg, iBeg + ">(END OF PROVISION)".length());
			result = result.replace(sReplace, ">");
			System.out.println("Provision>>" + sReplace);
		}
		return result;
	}
	
	public static String normalizeFillins(String html) {
		String result = html;
		
		result = result.replaceAll("____________________", "[CLS_FILLIN]"); // 20 spaces
		result = result.replaceAll("___________________", "[CLS_FILLIN]");
		result = result.replaceAll("__________________", "[CLS_FILLIN]");
		result = result.replaceAll("_________________", "[CLS_FILLIN]");
		result = result.replaceAll("________________", "[CLS_FILLIN]");
		result = result.replaceAll("_______________", "[CLS_FILLIN]");
		result = result.replaceAll("______________", "[CLS_FILLIN]");
		result = result.replaceAll("_____________", "[CLS_FILLIN]");
		result = result.replaceAll("____________", "[CLS_FILLIN]");
		result = result.replaceAll("___________", "[CLS_FILLIN]");
		result = result.replaceAll("__________", "[CLS_FILLIN]");
		result = result.replaceAll("_________", "[CLS_FILLIN]");
		result = result.replaceAll("________", "[CLS_FILLIN]");
		result = result.replaceAll("_______", "[CLS_FILLIN]");
		result = result.replaceAll("______", "[CLS_FILLIN]");
		result = result.replaceAll("_____", "[CLS_FILLIN]");
		result = result.replaceAll("____", "[CLS_FILLIN]");
		result = result.replaceAll("___", "[CLS_FILLIN]"); // 3 spaces
		
		// space the fillin with marker, followed by a blank space.
		// "[CLS_FILLIN]&nbsp;&nbsp;" ==> "[CLS_FILLIN] "
		
		while (result.indexOf("[CLS_FILLIN]&nbsp;") > 0)
		{
			String result2 = result.replace("[CLS_FILLIN]&nbsp;", "[CLS_FILLIN] ");
			result = result2;
		}
		
		while (result.indexOf("[CLS_FILLIN] &nbsp;") > 0)
		{
			String result2 = result.replace("[CLS_FILLIN] &nbsp;", "[CLS_FILLIN] ");
			result = result2;
		}	

		while (result.indexOf("[CLS_FILLIN]  ") > 0)
		{
			String result2 = result.replace("[CLS_FILLIN]  ", "[CLS_FILLIN] ");
			result = result2;
		}
		
		// finally, convert fillin to 4 underscores. This will be the standard.
		while (result.indexOf("[CLS_FILLIN]") > 0)
		{
			String result2 = result.replace("[CLS_FILLIN]", "____ ");
			result = result2;
		}
		
		// Strip quotes
		String resultQuote = result;
		resultQuote = resultQuote.replaceAll(Pattern.quote("&ldquo;"), "\"");
		resultQuote = resultQuote.replaceAll(Pattern.quote("&laquo;"), "\"");
		resultQuote = resultQuote.replaceAll(Pattern.quote("&raquo;"), "\"");
		resultQuote = resultQuote.replaceAll(Pattern.quote("&rdquor;"), "\"");
		resultQuote = resultQuote.replaceAll(Pattern.quote("&lsquo;"), "'");
		resultQuote = resultQuote.replaceAll(Pattern.quote("&rsquo;"), "'");
		resultQuote = resultQuote.replaceAll(Pattern.quote("&lsaquo;"), "'");
		resultQuote = resultQuote.replaceAll(Pattern.quote("&rsaquo;"), "'");
		result = resultQuote;
		
		return result;
	}
	
	public static String toISO(String html) {
		Document doc = Jsoup.parse(html, "", Parser.xmlParser());
		doc.outputSettings().prettyPrint(false); //don't want extra linebreaks added.
		doc.outputSettings().charset("ISO-8859-2");
		doc.outputSettings().escapeMode(EscapeMode.extended);
		return doc.toString();
	}
	
	// http://www.danshort.com/HTMLentities/
	public static final String[][] UNICODE_HTML_MAP = {
		{"&quot;", "&#x22;"},  //	Quotation mark
		{"&amp;", "&#x26;"},  //	Ampersand
		{"&lt;", "&#x3C;"},  //	Less-than sign
		{"&gt;", "&#x3E;"},  //	Greater-than sign
		{"&lsquo;", "&#x2018;"},  //	Open single quote
		{"&rsquo;", "&#x2019;"},  //	Close single quote
		{"&ldquo;", "&#x201C;"},  //	Open double quotes
		{"&ldquo;", "“"},
		{"&rdquo;", "&#x201D;"},  //	Close double quotes
		{"&sbquo;", "&#x201A;"},  //	Single low-9 quote
		{"&bdquo;", "&#x201E;"},  //	Double low-9 quote
		{"&prime;", "&#x2032;"},  //	Prime/minutes/feet
		{"&Prime;", "&#x2033;"},  //	Double prime/seconds/inches
		{"&nbsp;", "&#xA0;"},  //	Non-breaking space
		{"&ndash;", "&#x2010;"},  //	Hyphen "—"
		{"&ndash;", "&#x2013;"},  //	En dash
		{"&mdash;", "&#x2014;"},  //	Em dash
		{"&mdash;", "—"},
		{"&ensp;", "&#x2002;"},  //	En space
		{"&emsp;", "&#x2003;"},  //	Em space
		{"&thinsp;", "&#x2009;"},  //	Thin space
		{"&brvbar;", "&#xA6;"},  //	Broken vertical bar
		{"&bull;", "&#x2022;"},  //	Bullet
		//{"—", "&#x2023;"},  //	Triangular bullet
		{"&hellip;", "&#x2026;"},  //	Horizontal ellipsis
		{"&circ;", "&#x2C6;"},  //	Circumflex
		{"&uml;", "&#xA8;"},  //	Umlaut or dieresis
		{"&tilde;", "&#x2DC;"},  //	Small tilde
		{"&lsaquo;", "&#x2039;"},  //	Single left angle quote
		{"&rsaquo;", "&#x203A;"},  //	Single right angle quote
		{"&laquo;", "&#xAB;"},  //	Left angle quote; guillemotleft
		{"&raquo;", "&#xBB;"},  //	Right angle quote; guillemotright
		{"&oline;", "&#x203E;"},  //	Overline
		{"&iquest;", "&#xBF;"},  //	Inverted question mark
		{"&iexcl;", "&#xA1;"},  //	Inverted exclamation
		{"&deg;", "°"},
		{"&sect;", "§"},
		{"&times;", "×"},
		{"&eacute;", "é"},
		{"\"", "&ldquo;"}, // CJ-1304
		{"\"", "&laquo;"}, // CJ-672
		{"\"", "&raquo;"}, // CJ-672
		{"\"", "&rdquor;"}, // CJ-672
		{"____", "&squ;"} // CJ-672
	};
	
	public static String convertUnicodeToHtml(String pHtml) {
		if ((pHtml == null) || pHtml.isEmpty())
			return pHtml;
		pHtml = pHtml.replaceAll("protégé", "protege");
		pHtml = pHtml.replaceAll("prot&eacute;g&eacute;", "protege");
		for (String[] aSet : UNICODE_HTML_MAP) {
			String sUnicode = aSet[1];
			if (pHtml.contains(sUnicode))
				pHtml = pHtml.replaceAll(sUnicode, aSet[0]);
			sUnicode = sUnicode.toLowerCase();
			if (pHtml.contains(sUnicode))
				pHtml = pHtml.replaceAll(sUnicode, aSet[0]);
		}
		return pHtml;
	}
	
	public static int calcHtmlMaxRows(String psHtml, int piCharsPerLine, String[] paDivider, int piDividerPos) {
		if (piDividerPos >= paDivider.length) {
			return (psHtml.length() / piCharsPerLine) + 1;
		}
		String[] aLine = psHtml.split(paDivider[piDividerPos]);
		int iResult = 0;
		for (String sLine : aLine) {
			iResult += calcHtmlMaxRows(sLine, piCharsPerLine, paDivider, ++piDividerPos);
		}
		return iResult;
	}
	
	public static int calcHtmlMaxRows(String psHtml, int piCharsPerLine) {
		String[] aDivider = {"</p>", "</div>", "<br"};
		return calcHtmlMaxRows(psHtml, piCharsPerLine, aDivider, 0);
	}
	
}