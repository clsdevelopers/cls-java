package hgs.cpm.utils;

public class RomanNumeral {

	final static char SYMBOL[] = {'M','D','C','L','X','V','I'};
	final static int   VALUE[] = {1000,500,100,50,10,5,1};
	
	public static boolean isRomanNumeral(String roman)
	{
		if (roman.length() == 0)
			return false;

		for (char c: roman.toUpperCase().toCharArray()) {
			if (! RomanNumeral.isRomanNumeral(c))
				return false;
		}
		
		return true;
	}
	
	public static boolean isRomanNumeral(char c) {
		for (int i = 0; i < RomanNumeral.SYMBOL.length; i++)
		{
			if (c == RomanNumeral.SYMBOL[i])
				return true;
		}
		return false;
	}

	public static int valueOf(String roman)
	{
		if (roman.length() == 0)
			return 0;

		roman = roman.toUpperCase();

		for (int i = 0; i < RomanNumeral.SYMBOL.length; i++)
		{
			int pos = roman.indexOf(RomanNumeral.SYMBOL[i]) ;
			if (pos >= 0)
				return RomanNumeral.VALUE[i] - valueOf(roman.substring(0,pos)) + valueOf(roman.substring(pos+1));
		}
		throw new IllegalArgumentException("Invalid Roman Symbol.");
	}

	// ====================================
	private static int[]    NUMBERS = { 1000,  900,  500,  400,  100,   90,	50,   40,   10,    9,    5,    4,    1 };

	private static String[] LETTERS = { "M",  "CM",  "D",  "CD", "C",  "XC",	"L",  "XL",  "X",  "IX", "V",  "IV", "I" };

	public static String convertToRoman(int N)
	{
		String roman = "";
		for (int i = 0; i < RomanNumeral.NUMBERS.length; i++) {
			while (N >= RomanNumeral.NUMBERS[i]) {
				roman += RomanNumeral.LETTERS[i];
				N -= RomanNumeral.NUMBERS[i];
			}
		}
		return roman;
	}

}
