package hgs.cpm.utils;

import gov.dod.cls.utils.sql.SqlUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class SqlCommons {

	public static void executeForDocId(Connection conn, String sSql, Integer srcDocId) throws SQLException {
		PreparedStatement preparedStatement = null;
		try {
			preparedStatement = conn.prepareStatement(sSql);
			preparedStatement.setInt(1, srcDocId);
			preparedStatement.execute();
		} finally {
			SqlUtil.closeInstance(preparedStatement);
		}
	}
	
	/*
	 * Moved to StgSrcDocumentRecord.generateDocId()
	public static Integer generateDocId(Connection conn, String fileName, String srcDocTypeCode) throws SQLException {
		Integer srcDocId =  null;
		//File f = new File(this.excelFileName);
		//String fileName = f.getName();
		PreparedStatement psInsert = null;
		ResultSet res = null;
		try {
			String sSql
				= "INSERT INTO Stg_Src_Document (Src_File_Name, Src_Doc_Type_Cd)"
				+ " VALUES (?, ?)";
			psInsert = conn.prepareStatement(sSql, Statement.RETURN_GENERATED_KEYS);
			psInsert.setString(1, fileName);
			psInsert.setString(2, srcDocTypeCode);
			psInsert.execute();
			res = psInsert.getGeneratedKeys();
			if (res.next()) {
				srcDocId = res.getInt(1);
				System.out.println("New Stg_Src_Document: Src_Doc_Id(" + srcDocId +
						"), Src_File_Name(" + fileName + "), Src_Doc_Type_Cd(" + srcDocTypeCode + ")");
			}
		} finally {
			SqlUtil.closeInstance(res);
			SqlUtil.closeInstance(psInsert);
		}
		return srcDocId;
	}
	 */
	
	public static String quoteString(String psValue) {
		if ((psValue == null) || psValue.isEmpty())
			return "NULL";
		
		if (psValue.contains("\n")) {
			String[] aItems = psValue.split("\n");
			psValue = "";
			for (int index = 0; index < aItems.length; index++) {
				if (index > 0)
					psValue += "\\n";
				psValue += aItems[index];
			}
		}
		
		return "'" + psValue.replaceAll("'", "''") + "'"; //
	}
	
	public static String quoteString(Integer poValue) {
		if (poValue == null)
			return "NULL";
		else
			return poValue.toString();
	}
	
	public static String quoteString(Boolean poValue) {
		if (poValue == null)
			return "NULL";
		else
			return quoteString(poValue.booleanValue());
	}
	
	public static String quoteString(boolean poValue) {
		return (poValue ? "1" : "0");
	}
	
}
