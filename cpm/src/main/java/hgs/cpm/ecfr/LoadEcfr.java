package hgs.cpm.ecfr;

import gov.dod.cls.utils.CommonUtils;
import gov.dod.cls.utils.sql.SqlUtil;
import hgs.cpm.config.CpmConfig;
import hgs.cpm.db.Stg1OfficialClauseAltRecord;
import hgs.cpm.db.Stg1OfficialClauseRecord;
import hgs.cpm.db.StgSrcDocumentRecord;
import hgs.cpm.db.StgSrcDocumentSet;
import hgs.cpm.ecfr.util.EcfrFillInUtils;
import hgs.cpm.ecfr.util.Stg1RawClauseEditableSet;
import hgs.cpm.utils.HtmlUtils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;

import com.gargoylesoftware.htmlunit.BrowserVersion;

public class LoadEcfr {

	private static final String[] CLAUSE_INDEX_LINKS = {
		// FARs link - 52
		"http://www.ecfr.gov/cgi-bin/text-idx?SID=f8aabe9535f2d6107402fc92b6b6092c&mc=true&tpl=/ecfrbrowse/Title48/48cfr52_main_02.tpl"
		// DFARs link - 252
		, "http://www.ecfr.gov/cgi-bin/text-idx?SID=cb2ebc4e4c2358875acbdd0b0b468310&tpl=/ecfrbrowse/Title48/48cfr252_main_02.tpl"
	};
	
	// public static final String FILL_IN_TYPES = "('PCO', 'Y PCO/OFFEROR', 'PCO/OFFEROR', 'OFFEROR/PCO')";
	public static final String SQL_FILL_IN_CLAUSE_NUMBER = "SELECT Clause_Number FROM Stg1_Raw_Clauses"
			+ " WHERE Src_Doc_Id = ? AND Clause_Number IS NOT NULL"
			+ " AND Fill_In IS NOT NULL AND Fill_In LIKE '%PCO%'";
			// + " AND Fill_In IS NOT NULL AND Fill_In IN " + FILL_IN_TYPES;

	private static final String ALTERNATE = "Alternate";

	private Connection conn;
	private Integer srcDocId = null;
	private Integer clauseSrcDocId = null;
	private HtmlUnitDriver htmlUnitDriver; // = new HtmlUnitDriver(BrowserVersion.FIREFOX_24);
	private ArrayList<String> fillinClauseNames;
	private Stg1OfficialClauseRecord oClauseRecord = new Stg1OfficialClauseRecord();
	private Stg1RawClauseEditableSet stg1RawClauseEditableSet = null;
	
	public Integer getSrcDocId() { return srcDocId; }
	private boolean bFullEdit;

	public LoadEcfr(Connection conn, int clauseSrcDocId, boolean pbFullEdit) {
		this.conn = conn;
		this.clauseSrcDocId = clauseSrcDocId;
		this.fillinClauseNames = new ArrayList<String>();
		this.bFullEdit = pbFullEdit;
	}
	
	public Integer generateDocId() throws SQLException {
		if (this.srcDocId == null) {
			this.srcDocId = StgSrcDocumentRecord.generateDocId(this.conn, "EFCR", StgSrcDocumentRecord.TYPE_CD_ECFR);
		}
		return this.srcDocId;
	}
	
	private void loadExcelFillinType() throws SQLException {
		this.fillinClauseNames = EcfrParseFillins.getClauseNumbersforFillinType(this.clauseSrcDocId, this.conn);
		/*
		String sSql = LoadEcfr.SQL_FILL_IN_CLAUSE_NUMBER;
		PreparedStatement ps1 = this.conn.prepareStatement(sSql);
		ps1.setInt(1, this.clauseSrcDocId);	
		ResultSet rs1 = ps1.executeQuery();
		this.fillinClauseNames.clear();
		while (rs1.next()) {
			String rClauseName = rs1.getString(1);
			this.fillinClauseNames.add(rClauseName.toUpperCase());
		}
		System.out.println("LoadECFR Fillin clauses count from Excel: "+ this.fillinClauseNames.size());
		*/
	}

	public void downloadAll() throws Exception 
	{
		this.loadExcelFillinType();
		this.stg1RawClauseEditableSet = Stg1RawClauseEditableSet.load(this.clauseSrcDocId, this.conn).subsetForEditAll(true);
		this.oClauseRecord.srcDocId = this.generateDocId();
		PreparedStatement psClause = null;
		PreparedStatement psAlt = null;
		
		this.htmlUnitDriver = null;
		try {
			Stg1OfficialClauseRecord.deleteAll(conn, this.oClauseRecord.srcDocId);
			
			psClause = Stg1OfficialClauseRecord.createInsertStatement(conn);
			psAlt = Stg1OfficialClauseAltRecord.createInsertStatement(conn);
			
			this.htmlUnitDriver = new HtmlUnitDriver();
			//this.htmlUnitDriver = new HtmlUnitDriver(BrowserVersion.INTERNET_EXPLORER_11); 
			this.htmlUnitDriver.setJavascriptEnabled(true);
			
			List<String[]> clauses = this.getClauseLinks(CLAUSE_INDEX_LINKS);
			System.out.println("clause level: " + clauses.size());

			int iClauseIndex = 0;
			for (String[] clause: clauses) {
				this.pullClause(clause[0], clause[1]);
				this.oClauseRecord.insert(psClause, psAlt);
				System.out.println("[" + iClauseIndex++ + "/" + clauses.size() + "] " + this.oClauseRecord.getClauseNumbers());
			}
			System.out.println("LoadEcfr.downloadAll() finished");
		} finally {
			if (this.htmlUnitDriver != null)
				this.htmlUnitDriver.quit();
			SqlUtil.closeInstance(psClause);
			SqlUtil.closeInstance(psAlt);
		}
	}
	
	public List<String[]> getClauseLinks(String[] parentLinks) {
		List<String[]> nextLinks = new ArrayList<String[]>();
		//WebDriver htmlUnitDriver = null;
		try {
			//htmlUnitDriver = new HtmlUnitDriver();
			String expression = "//table[@width='120']//td/a";
			
			String[] entry;
			for (String parentLink: parentLinks) {
				this.htmlUnitDriver.get(parentLink);
				List<WebElement> nextAnchors = this.htmlUnitDriver.findElements(By.xpath(expression));
			
				for (WebElement el: nextAnchors) {
					entry = new String[2];
					entry[0] = el.getText();
					entry[1] = el.getAttribute("href");
					nextLinks.add(entry);
				}
			}
		}
		finally {
			//htmlUnitDriver.quit();
		}
		return nextLinks;
	}
	
	public static String getInnerHtml(WebDriver htmlUnitDriver, WebElement el) {
		String contents = (String)((JavascriptExecutor)htmlUnitDriver).executeScript("return arguments[0].innerHTML;", el);
		// System.out.println("*******" + contents + "*******");
		return contents;
	}
	
	private static String removeSidFromUrl(String psUrl) {
		int iSidPos = psUrl.indexOf("SID");
		if (iSidPos > 0) {
			String sLeft = psUrl.substring(0, iSidPos);
			String sRight = psUrl.substring(iSidPos);
			int iNextPos = sRight.indexOf('&');
			if (iNextPos > 0) {
				String sRest = sRight.substring(iNextPos + 1);
				psUrl = sLeft + sRest;
			}
		}
		return psUrl;
	}
	
	// -------------------------------------------------------------------------------------------------
	public void pullClause(String psClauseNumber, String clauseLink) 
	{
		this.oClauseRecord.clear();

		this.oClauseRecord.clauseNumber = psClauseNumber;
		this.oClauseRecord.uri = removeSidFromUrl(clauseLink);
		
		StringBuilder oStringBuilder = new StringBuilder(); // Value of the element being parsed
		String expression; // Expression to identity element in HTML
		String sClauseName = null; //used for checking against fill-in clauses from clause exce

		this.htmlUnitDriver.get(clauseLink);
		
		//----------------------------------------
		expression = "//div[@class='extract' and not(preceding-sibling::p[.//span[starts-with(., 'Alternate') or starts-with(., 'Alterate')]][1])"
				+ "and not(preceding-sibling::h3[contains(.,'End of clause')][1])]";
		List<WebElement> listWebElement = this.htmlUnitDriver.findElements(By.xpath(expression));
		List<WebElement> edgeCaseElements = new ArrayList<WebElement>();
		String html = "";
		for (WebElement el: listWebElement) {
			html += LoadEcfr.getInnerHtml(this.htmlUnitDriver, el);		
			oStringBuilder.append(el.getText()); // .replace("\"", "\"\"")
			WebElement nextEl = el.findElement(By.xpath("following-sibling::*"));
			if(nextEl != null) {
				if("img".equals(nextEl.getTagName())) 
					html += nextEl;
				else if("div".equals(nextEl.getTagName()) && "width:100%".equals(nextEl.getAttribute("style")))
					html += "<div>" + getInnerHtml(this.htmlUnitDriver, nextEl) + "</div>";
				
				edgeCaseElements.add(nextEl);
				oStringBuilder.append(nextEl.getText());
			}
		}
		
		expression = "//div[not(preceding-sibling::p[.//span[starts-with(., 'Alternate') or starts-with(., 'Alterate')]][1])"
				+ "and not(preceding-sibling::h3[contains(.,'End of clause')][1])"
				+ "and (preceding-sibling::div[@class='extract'][1])"
				+ "and not(preceding-sibling::img)]";
		List<WebElement> listWebElement2 = this.htmlUnitDriver.findElements(By.xpath(expression));
		int iDupe = 0;
		for (WebElement el: listWebElement2) {
			if (!listWebElement.contains(el) && !edgeCaseElements.contains(el)) {
				html += LoadEcfr.getInnerHtml(this.htmlUnitDriver, el);		
				oStringBuilder.append(el.getText()); // .replace("\"", "\"\"")
			} else {
				++iDupe;
			}
		}
		if (iDupe > 0)
			System.out.println("Skipped duplicate lines: " + iDupe);

		html = HtmlUtils.cleanupHtml(html);

		String sTextToCompare = "<p><span style=\"font-style:italic\">Alterate I.</span>";
		String sRemovedHtml = "";
		String sRemovedText = "";
		int iPos = html.indexOf(sTextToCompare); // 252.244-7001 (CJ-364)
		if (iPos > 0) {
			sRemovedHtml = html.substring(iPos);
			html = html.substring(0, iPos);
			iPos = oStringBuilder.indexOf("Alterate I.");
			if (iPos > 0) {
				sRemovedText = oStringBuilder.substring(iPos);
				oStringBuilder.delete(iPos, oStringBuilder.length() - 1);
			}
			System.out.println("[" + this.oClauseRecord.clauseNumber + "] Removed Alterate I.");
		}
		
		this.oClauseRecord.setContentHtml(html);
		
		String sHtmlFillIn;
		sClauseName = psClauseNumber;
		if (this.isFillInNeeded(sClauseName))
		//if (this.fillinClauseNames.contains(sClauseName.toUpperCase()) && 
			//	(this.stg1RawClauseEditableSet.isEditAll(sClauseName) == false))	
			sHtmlFillIn = EcfrFillInUtils.parseFillinsAndNormalize(sClauseName, html);
		else 
			sHtmlFillIn = HtmlUtils.normalizeHtml(html); //this was the original line of code
		
		this.oClauseRecord.contentHtmlFillIns = sHtmlFillIn;
		//end catching false flag fill-ins
		
		this.oClauseRecord.content = oStringBuilder.toString(); 
		
		//----------------------------------------
		Pattern pattern;
		Matcher matcher;
		expression = "//p[.//span[starts-with(., 'Alternate') or starts-with(., 'Alterate')] and parent::div[not(@class='extract')]]";
		listWebElement = this.htmlUnitDriver.findElements(By.xpath(expression));
		
		if (listWebElement.size() == 0) {
			expression = "//h1[contains(., 'Alternate') or contains(., 'Alterate')]";
			listWebElement = this.htmlUnitDriver.findElements(By.xpath(expression));
		}
		
		listWebElement2.clear();
		ArrayList<WebElement> prossedElements = new ArrayList<WebElement>(); 
		WebElement oWebElement;
		//for (WebElement oAltElement: listWebElement) {
		for (int iAltItem = listWebElement.size() - 1; iAltItem >= 0; iAltItem--) {
			WebElement oAltElement = listWebElement.get(iAltItem);
			prossedElements.add(oAltElement);
			Stg1OfficialClauseAltRecord oStg1OfficialClauseAltRecord = new Stg1OfficialClauseAltRecord();
			oStg1OfficialClauseAltRecord.clauseNumber = psClauseNumber;
			
			// Save Alternate Instructions
			oStringBuilder = new StringBuilder(); // .setLength(0);
			if (oAltElement.getText().trim().length() != 0) 
				oStringBuilder.append(oAltElement.getText()); // .replace("\"", "\"\"")
			// oStg1OfficialClauseAltRecord.instructions = oStringBuilder.toString();
			String instructions = "<p>" + oStringBuilder.toString() + "</p>";
			
			// Alternate Number
			pattern = Pattern.compile("(Alternate|Alterate) ([a-zA-Z]+)");
			matcher = pattern.matcher(oStringBuilder);
			if (matcher.find()) {
				String sAltNumber = matcher.group(2);
				oStg1OfficialClauseAltRecord.altNumber = ALTERNATE + " " + sAltNumber;
			}
			
			// Some Alternate Instructions don't have "extracts", so find immediate sibling and check if it's an extract
			oWebElement = oAltElement.findElement(By.xpath("following-sibling::*"));
			if (oWebElement != null) {
				String sTagName = oWebElement.getTagName();
				if (sTagName.equals("div")) {
					// Alternate Extract
					oStringBuilder = new StringBuilder(); // .setLength(0);
					if (oAltElement.getText().trim().length() != 0) 
						oStringBuilder.append(oAltElement.getText()); // .replace("\"", "\"\"")
					
					boolean bAddedExtra = false;
					html = ""; // instructions;
					// Alternate HTML
					while (!sTagName.isEmpty()) {
						prossedElements.add(oWebElement);
						if (sTagName.equals("div")) {
							if (html.isEmpty())
								html = instructions + LoadEcfr.getInnerHtml(this.htmlUnitDriver, oWebElement);
							else {
								html += "<div>" + LoadEcfr.getInnerHtml(this.htmlUnitDriver, oWebElement) + "</div>";
								bAddedExtra = true;
							}
						} else if ("p".equals(sTagName)) {
							String sText = oWebElement.getText().trim();
							// [79 FR 22038, Apr. 21, 2014, as amended at 80 FR 36898, June 26, 2015]
							if ((sText.startsWith("[") && sText.endsWith("]") && (sText.contains(", as amended at ") || sText.contains(" FR "))) ||
									"End of clause".equalsIgnoreCase(sText) || "(End of clause)".equalsIgnoreCase(sText) ||
									//"End of provision".equalsIgnoreCase(sText) || "(End of provision)".equalsIgnoreCase(sText) ||
									"Need assistance?".equalsIgnoreCase(sText)) {
								break;
							}
							
							oStringBuilder.append(sText);
							html += "<p>" + sText + "</p>";
							bAddedExtra = true;
						} else if ("h1".equals(sTagName)) {
							String sText = oWebElement.getText();
							oStringBuilder.append(sText);
							html += "<h1>" + sText + "</h1>";
							bAddedExtra = true;
						} else {
							String sText = oWebElement.getText().trim();
							if ("End of clause".equalsIgnoreCase(sText) || "(End of clause)".equalsIgnoreCase(sText) ||
								//	"End of provision".equalsIgnoreCase(sText) || "(End of provision)".equalsIgnoreCase(sText) ||
									"Need assistance?".equalsIgnoreCase(sText)){
								break;
							}
							html += oWebElement.getText();
							bAddedExtra = true;
						}
						
						try {
							oWebElement = oWebElement.findElement(By.xpath("following-sibling::*"));
							if (prossedElements.contains(oWebElement))
								sTagName = "";
							else
								sTagName = (oWebElement == null) ? "" : oWebElement.getTagName();
						}
						catch (NoSuchElementException oNoElement) {
							sTagName = "";
						}
					}
					
					oStg1OfficialClauseAltRecord.content = oStringBuilder.toString();

					html = HtmlUtils.cleanupHtml(html);
					oStg1OfficialClauseAltRecord.contentHtml = html;
					
					sClauseName = (oStg1OfficialClauseAltRecord.clauseNumber + " " + oStg1OfficialClauseAltRecord.altNumber).toUpperCase();
					if (this.isFillInNeeded(sClauseName))
					//if (this.fillinClauseNames.contains(sClauseName) && 
					//	(this.stg1RawClauseEditableSet.isEditAll(sClauseName) == false))
						oStg1OfficialClauseAltRecord.contentHtmlFillIns = EcfrFillInUtils.parseFillinsAndNormalize(oStg1OfficialClauseAltRecord.getClausFullNumber(), html);
					else
						oStg1OfficialClauseAltRecord.contentHtmlFillIns = HtmlUtils.normalizeHtml(html);

					if (bAddedExtra)
						System.out.println("[" + sClauseName  + "] Found additional text on <div>");
					
				} else if (sTagName.equals("p")) {
					oStringBuilder = new StringBuilder();
					if (!sRemovedText.isEmpty()) {
						oStringBuilder.append(sRemovedText);
						sRemovedText = "";
					}
					if (!sRemovedHtml.isEmpty()) {
						html = sRemovedHtml ;
						sRemovedHtml = "";
					} else
						html = instructions;
					while (!sTagName.isEmpty()) { // (sTagName.equals("p"))
						String sClass = oWebElement.getAttribute("class");
						if ("cita".equals(sClass))
							break;
						if ("div".equals(sTagName)) {
							String sContent = LoadEcfr.getInnerHtml(this.htmlUnitDriver, oWebElement);
							html += sContent;
						} else if ("p".equals(sTagName)) {
							String sText = oWebElement.getText();
							if (CommonUtils.isNotEmpty(sText)) { //  && !(sText.startsWith("Alternate"))
								if (((sText.startsWith("Alterate II ") || sText.startsWith("Alternate II ")) && 
										(ALTERNATE + " I").equals(oStg1OfficialClauseAltRecord.altNumber))) { // 252.227-7005 clause special case
									System.out.println("[" + this.oClauseRecord.clauseNumber + " " + oStg1OfficialClauseAltRecord.altNumber + "] Removed " + sText);
									break;
								}
								else if (((sText.startsWith("Alterate III ") || sText.startsWith("Alternate III ")) && 
										(ALTERNATE + " II").equals(oStg1OfficialClauseAltRecord.altNumber))) { // 52.249-2 clause special case
									System.out.println("[" + this.oClauseRecord.clauseNumber + " " + oStg1OfficialClauseAltRecord.altNumber + "] Removed " + sText);
									break;
								}
								else {
									oStringBuilder.append(sText);
									html += "<p>" + sText + "</p>";
								}
							}
						}
						try {
							oWebElement = oWebElement.findElement(By.xpath("following-sibling::*"));
							if (prossedElements.contains(oWebElement))
								sTagName = "";
							else {
								prossedElements.add(oWebElement);
								sTagName = (oWebElement == null) ? "" : oWebElement.getTagName();
							}
						}
						catch (NoSuchElementException oNoElement) {
							sTagName = "";
						}
					}
					oStg1OfficialClauseAltRecord.content = oStringBuilder.toString();
					html = HtmlUtils.cleanupHtml(html);
					oStg1OfficialClauseAltRecord.contentHtml = html;
					
					sClauseName = (oStg1OfficialClauseAltRecord.clauseNumber + " " + oStg1OfficialClauseAltRecord.altNumber).toUpperCase();
					if (this.isFillInNeeded(sClauseName))
					//if (this.fillinClauseNames.contains(sClauseName) && 
					//	(this.stg1RawClauseEditableSet.isEditAll(sClauseName) == false))
						oStg1OfficialClauseAltRecord.contentHtmlFillIns = EcfrFillInUtils.parseFillinsAndNormalize(oStg1OfficialClauseAltRecord.getClausFullNumber(), html);
					else
						oStg1OfficialClauseAltRecord.contentHtmlFillIns = HtmlUtils.normalizeHtml(html);
					
					System.out.println(oStg1OfficialClauseAltRecord.clauseNumber + " " + oStg1OfficialClauseAltRecord.altNumber);
					/*
					oWebElement = oAltElement.findElement(By.xpath("preceding-sibling::*"));
					if (oWebElement != null) {
						expression = "//p[contains(., '" + oStg1OfficialClauseAltRecord.altNumber + "')]";
						oWebElement = oAltElement.findElement(By.xpath(expression));
						if (oWebElement != null) {
							String sText = oWebElement.getText();
							if (sText.startsWith(oStg1OfficialClauseAltRecord.altNumber + " ")) {
								oStg1OfficialClauseAltRecord.instructions = sText;
							}
						}
					}
					*/
				} else { // 52.237-4 Alternate I
					html = instructions;
					oStg1OfficialClauseAltRecord.contentHtml = html;
				}
			}
			if (CommonUtils.isNotEmpty(oStg1OfficialClauseAltRecord.altNumber)) {
				if (!((this.oClauseRecord.title != null) &&
				    this.oClauseRecord.title.toUpperCase().startsWith(oStg1OfficialClauseAltRecord.altNumber.toUpperCase())))
				{
					this.oClauseRecord.add(oStg1OfficialClauseAltRecord);
				}
			}
		}
		
		/*
		elementName = ALTERNATE_EXTRACT;
		expression = "//p[.//span[starts-with(., 'Alternate')] and parent::div[not(@class='extract')]]/following-sibling::div[@class='extract'][1]";
		//expression = "//div[@class='extract' and (preceding-sibling::p[.//span[starts-with(., 'Alternate')]][1])]";
		listWebElement = this.htmlUnitDriver.findElements(By.xpath(expression));
     
		i = 0;
		for (WebElement el: listWebElement) {
			oStringBuilder = new StringBuilder(); // .setLength(0);
			oStringBuilder.append(el.getText()); // .replace("\"", "\"\"")
			results.put(elementName+"_"+(i++), oStringBuilder.toString());
		}
		*/
		//----------------------------------------
		
		expression = "//h2";
		listWebElement = this.htmlUnitDriver.findElements(By.xpath(expression));

		WebElement oTitleWebElement = null;
		oStringBuilder = new StringBuilder(); // .setLength(0);
		for (WebElement el: listWebElement) {
			if (el.getText().trim().length() != 0) {
				oStringBuilder.append(el.getText()); // .replace("\"", "\"\"")
				oTitleWebElement = el;
				break;
			}
			/*
			if (el.getText().trim().length() != 0)
				oStringBuilder.append(el.getText()); // .replace("\"", "\"\"")
			oTitleWebElement = el;
			*/
		}
		pattern = Pattern.compile("([a-zA-Z0-9]+)\\s+(.*)");
		matcher = pattern.matcher(oStringBuilder);
		if (matcher.find()) {
			String sTitle = matcher.group(2);
			this.oClauseRecord.title = sTitle;
		}
		
		//----------------------------------------
		expression = "//div[@class='extract']/preceding-sibling::p[starts-with(., 'As prescribed')][1]";
		listWebElement = this.htmlUnitDriver.findElements(By.xpath(expression));

		oStringBuilder = new StringBuilder(); // .setLength(0);
		for (WebElement el: listWebElement) {
			if (el.getText().trim().length()!=0)
				oStringBuilder.append(el.getText()); // .replace("\"", "\"\"")
		}
		pattern = Pattern.compile("As prescribed (in|at) (.*?)( |,)");
		matcher = pattern.matcher(oStringBuilder);
		if (matcher.find()) {
			String sPrescription = matcher.group(2);
			this.oClauseRecord.prescription = sPrescription; 
		}
		
		if (CommonUtils.isEmpty(this.oClauseRecord.content) && (oTitleWebElement != null)) {
			oStringBuilder = new StringBuilder();
			html = "";
			oWebElement = oTitleWebElement;
			while (oWebElement != null) {
				try {
					oWebElement = oWebElement.findElement(By.xpath("following-sibling::*"));
				}
				catch (NoSuchElementException oNoElement) {
					oWebElement = null;
				}
				if (oWebElement != null) {
					String sTagName = oWebElement.getTagName();
					if ("p".equals(sTagName) || "a".equals(sTagName) || "img".equals(sTagName)) {
						String sText = oWebElement.getText();
						if (CommonUtils.isNotEmpty(sText)) {
							if (sText.startsWith("(End of "))
								oWebElement = null;
							else if (sText.startsWith("[") && sText.endsWith("]"))
								oWebElement = null;
							else {
								oStringBuilder.append(sText);
								html += "<" + sTagName + ">" + sText + "</" + sTagName + ">";
							}
						}
					} else
						oWebElement = null;
				}
			}
			oClauseRecord.content = oStringBuilder.toString();
			html = HtmlUtils.cleanupHtml(html);
			oClauseRecord.setContentHtml(html);
			
			if (this.isFillInNeeded(sClauseName))
			// if (this.fillinClauseNames.contains(oClauseRecord.clauseNumber))
				oClauseRecord.contentHtmlFillIns = EcfrFillInUtils.parseFillinsAndNormalize(sClauseName, html);
			else
				oClauseRecord.contentHtmlFillIns = HtmlUtils.normalizeHtml(html);
		}
	}
	
	private boolean isFillInNeeded(String sClauseName) {
		boolean result = false;
		if (this.fillinClauseNames.contains(sClauseName.toUpperCase())) {
			result = true;
			if (this.bFullEdit) {
				if (this.stg1RawClauseEditableSet.isEditAll(sClauseName))
					result = false;
			}
		}
		return result;
	}
	
	// =====================================================
	public static void main( String[] args )
    {
		//String sClauseNumber = "252.232-7010";
		//String clauseLink = "http://www.ecfr.gov/cgi-bin/text-idx?SID=e8278e248a9cfe87dbdf12209cf10f01&mc=true&node=se48.3.252_1232_67010&rgn=div8";
		//String sClauseNumber = "252.244-7001";
		//String clauseLink = "http://www.ecfr.gov/cgi-bin/text-idx?SID=b427433acd24dd7f4a94d35102c664b9&mc=true&node=se48.3.252_1244_67001&rgn=div8";
		//String sClauseNumber = "52.000";
		//String clauseLink = "http://www.ecfr.gov/cgi-bin/text-idx?SID=afafa295f5803ae857163b0221c6522a&mc=true&node=se48.2.52_1000&rgn=div8";
		//String sClauseNumber = "52.100";
		//String clauseLink = "http://www.ecfr.gov/cgi-bin/text-idx?SID=afafa295f5803ae857163b0221c6522a&mc=true&node=se48.2.52_1100&rgn=div8";
		//String sClauseNumber = "52.101";
		//String clauseLink = "http://www.ecfr.gov/cgi-bin/text-idx?SID=afafa295f5803ae857163b0221c6522a&mc=true&node=se48.2.52_1101&rgn=div8";
		//String sClauseNumber = "52.203-2";
		//String clauseLink = "http://www.ecfr.gov/cgi-bin/text-idx?SID=afafa295f5803ae857163b0221c6522a&mc=true&node=se48.2.52_1203_62&rgn=div8";
		
		//String sClauseNumber = "52.208-4";
		//String clauseLink = "http://www.ecfr.gov/cgi-bin/text-idx?SID=afafa295f5803ae857163b0221c6522a&mc=true&node=se48.2.52_1208_64&rgn=div8";
		
		//String sClauseNumber = "52.226-2";
		//String clauseLink = "http://www.ecfr.gov/cgi-bin/text-idx?SID=afafa295f5803ae857163b0221c6522a&mc=true&node=se48.2.52_1226_62&rgn=div8";

		//String sClauseNumber = "52.214-14";
		//String clauseLink = "http://www.ecfr.gov/cgi-bin/text-idx?node=se48.2.52_1214_614&rgn=div8";
		
		//String sClauseNumber = "252.203-7002";
		//String clauseLink = "http://www.ecfr.gov/cgi-bin/text-idx?SID=4182a7d49106063f895f8db093222234&mc=true&node=se48.3.252_1203_67002&rgn=div8";
		
		String sClauseNumber = "252.232-7013";
		String clauseLink = "http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1232_67013&rgn=div8";
//		sClauseNumber = "52.237-4";
//		clauseLink = "http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1237_64&rgn=div8";
//		sClauseNumber = "52.237-4";
//		clauseLink = "http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1223_69&rgn=div8";
//		sClauseNumber = "252.204-7012";
//		clauseLink = "http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1204_67012&rgn=div8";
//		sClauseNumber = "252.244-7001";
//		clauseLink = "http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1244_67001&rgn=div8";
//		sClauseNumber = "252.227-7005";
//		clauseLink = "http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1227_67005&rgn=div8";
//		sClauseNumber = "252.227-7014";
//		clauseLink = "http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1227_67014&rgn=div8";
//		sClauseNumber = "252.227-7013";
//		clauseLink = "http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.3.252_1227_67013&rgn=div8";
//		sClauseNumber = "52.212-5";
//		clauseLink = "http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1212_65&rgn=div8";
//		sClauseNumber = "52.249-2";
//		clauseLink = "http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1249_62&rgn=div8";
//		sClauseNumber = "52.249-6";
//		clauseLink = "http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1249_66&rgn=div8";
		
		sClauseNumber = "52.227-14";
		clauseLink = "http://www.ecfr.gov/cgi-bin/text-idx?mc=true&node=se48.2.52_1227_614&rgn=div8";
		
		
		String sOutputFolder = "E:\\Projects\\ClauseLogic\\Tasks\\parser\\clause_output";
		
		boolean bProcessAll = false;
		
		CpmConfig cpmConfig = CpmConfig.getInstance();
		Connection conn = null;
		try {
			conn = cpmConfig.getDbConnection();
			
			StgSrcDocumentSet oStgSrcDocumentSet = StgSrcDocumentSet.getLatest(conn);
			LoadEcfr oLoadEcfr = new LoadEcfr(conn, oStgSrcDocumentSet.iClauseSrcDocId, false); //need to set this second param for further tests
			// oLoadEcfr.srcDocId = 151;                    //didn't want to import StgSrcDocument just so this wouldn't error out.
			if (bProcessAll) {
				oLoadEcfr.downloadAll();
			} else {
				oLoadEcfr.htmlUnitDriver = new HtmlUnitDriver();
				oLoadEcfr.htmlUnitDriver.setJavascriptEnabled(true);
				oLoadEcfr.pullClause(sClauseNumber, clauseLink);
				
				System.out.println(oLoadEcfr.oClauseRecord.clauseNumber);
				System.out.println(oLoadEcfr.oClauseRecord.contentHtmlFillIns);
				oLoadEcfr.oClauseRecord.generateFile(sOutputFolder, true);
			}
			
			System.out.println("Done");
		}
		catch (Exception oError) {
			oError.printStackTrace();
		}
		finally {
			/*
			if (oLoadEcfr.htmlUnitDriver != null)
				oLoadEcfr.htmlUnitDriver.quit();
			*/
			SqlUtil.closeInstance(conn);
		}
    }
	
}
