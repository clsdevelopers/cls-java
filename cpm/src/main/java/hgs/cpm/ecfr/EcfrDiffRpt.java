package hgs.cpm.ecfr;

import gov.dod.cls.db.ClauseVersionRecord;
import gov.dod.cls.utils.CommonUtils;
import gov.dod.cls.utils.sql.SqlUtil;
import hgs.cpm.config.CpmConfig;
import hgs.cpm.db.ParsedClauseRecord;
import hgs.cpm.db.StgVersioningRecord;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;

import org.apache.commons.io.FileUtils;

public class EcfrDiffRpt {

	private static String[] FILL_IN_FIELDS = {"Fill_In_Code", "Fill_In_Type", "Fill_In_Max_Size", "Fill_In_Group_Number", "Fill_In_Placeholder", "Fill_In_Default_Data", "Fill_In_Display_Rows", "Fill_In_For_Table", "Fill_In_Heading"};
	
	private static String SUBFOLDER_NEW = "/new";
	private static String SUBFOLDER_PREVIOUS = "/previous";
	
	private int previousClauseVersionId;
	private int iEcfrSrcDocIdPrevious;
	//private int iClauseSrcDocIdPrevious;
	private int iEcfrSrcDocIdNew;
	private int iClauseSrcDocIdNew;
	private String claueVersionName = null;
	private Connection conn;
	private String sqlOutputPath;
	private String subFolderPath;
	
	private DiffClauseDataSet diffClauseDataSet = new DiffClauseDataSet();
	
	public EcfrDiffRpt(int previousClauseVersionId, int iEcfrSrcDocIdNew, int iClauseSrcDocIdNew, Connection conn, String sqlOutputPath) {
		this.previousClauseVersionId = previousClauseVersionId;
		this.iEcfrSrcDocIdNew = iEcfrSrcDocIdNew;
		this.iClauseSrcDocIdNew = iClauseSrcDocIdNew;
		this.conn = conn;
		this.sqlOutputPath = sqlOutputPath;
	}
	
	private FileOutputStream createOutputStream() throws IOException {
		String sFilePrefix = "ClauseDataDiffReport-" + ParsedClauseRecord.formatDate.format(CommonUtils.getNow()) + "-"
				+ "cls.v." + this.previousClauseVersionId
				+ "-ECFR(" + this.iEcfrSrcDocIdNew + ")"
				+ "-Clause(" + this.iClauseSrcDocIdNew + ")";

		this.subFolderPath = this.sqlOutputPath + "/" + sFilePrefix;
		new File(this.subFolderPath).mkdir();
		
		String sFileName = sFilePrefix + ".html";
		String sFilePath = this.subFolderPath + "/" + sFileName;

		File file = new File(sFilePath);
		if (file.exists()) {
			file.delete();
			System.out.println("Deleted " + sFilePath);
		}
		file.createNewFile();
		FileOutputStream fileOutputStream = new FileOutputStream(file);
		
		System.out.println("Output file: " + sFilePath);
		
		new File(this.subFolderPath + SUBFOLDER_PREVIOUS).mkdir();
		new File(this.subFolderPath + SUBFOLDER_NEW).mkdir();
		
		return fileOutputStream;
	}
	
	private boolean getClauseVersionName(Statement statement) throws SQLException {
		String sSql = "SELECT " + ClauseVersionRecord.FIELD_CLAUSE_VERSION_NAME
				+ " FROM " + ClauseVersionRecord.TABLE_CLAUSE_VERSIONS
				+ " WHERE " + ClauseVersionRecord.FIELD_CLAUSE_VERSION_ID + " = " + this.previousClauseVersionId;
		ResultSet rs1 = null;
		try {
			rs1 = statement.executeQuery(sSql);
			if (rs1.next()) {
				this.claueVersionName = rs1.getString(1);
				return true;
			} else
				return false;
		} finally {
			SqlUtil.closeInstance(rs1);
		}
	}
	
	public void run() throws SQLException, IOException {
		Statement statement = null;
		PreparedStatement psStg2ClauseFillIns = null;
		PreparedStatement psStg2ClauseFillInsAlt = null;
		FileOutputStream fileOutputStream = null;
		try {
			statement = this.conn.createStatement();
			if (!this.getClauseVersionName(statement)) {
				System.out.println("EcfrDiffRpt.run(): No Clause_Versions Record found for " + this.previousClauseVersionId);
				return;
			}
			StgVersioningRecord stgVersioningRecord = StgVersioningRecord.findByClauseVersionId(this.previousClauseVersionId, statement);
			if (stgVersioningRecord == null) {
				System.out.println("EcfrDiffRpt.run(): No Stg Versioning Record found for " + this.previousClauseVersionId);
				return;
			}
			//this.iClauseSrcDocIdPrevious = stgVersioningRecord.getClauseSrcDocId();
			this.iEcfrSrcDocIdPrevious = stgVersioningRecord.getEcfrSrcDocId();
			
			this.diffClauseDataSet.collectDiffClauseData(this, statement);
			psStg2ClauseFillIns = this.diffClauseDataSet.genStg2ClauseFillIns(conn);
			psStg2ClauseFillInsAlt = this.diffClauseDataSet.genStg2ClauseFillInsAlt(conn);
			
			fileOutputStream = this.createOutputStream();
			this.diffClauseDataSet.genHtml(fileOutputStream, psStg2ClauseFillIns, psStg2ClauseFillInsAlt);
		} finally {
			SqlUtil.closeInstance(psStg2ClauseFillIns);
			SqlUtil.closeInstance(psStg2ClauseFillInsAlt);
			SqlUtil.closeInstance(statement);
			try { 
				if (fileOutputStream != null) {
					fileOutputStream.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	// ==============================================================================
	private class DiffClauseDataSet extends ArrayList<DiffClauseData> {

		private static final long serialVersionUID = 4575803403039994081L;
		
		private EcfrDiffRpt ecfrDiffRpt;

		private void collectDiffClauseData(EcfrDiffRpt ecfrDiffRpt, Statement statement) throws SQLException {
			this.ecfrDiffRpt = ecfrDiffRpt;
			String sSql
				= "SELECT 0 Is_Alt, A.Stg1_Official_Clause_Id Stg1IdNew, C.Stg1_Official_Clause_Id Stg1IdPrevious, A.Clause_Number, A.Manually_Added"
				+ "\n, A.Title TitleNew"
				+ "\n, A.URI, A.Prescription Prescription_New, C.Prescription Prescription_Previous, (IFNULL(A.Prescription, '') <> IFNULL(C.Prescription, '')) Prescription_Diff"
				+ "\n, NULL Instructions_New, NULL Instructions_Previous, 0 Instructions_Diff"
				+ "\n, A.Content_HTML Content_HTML_New, C.Content_HTML Content_HTML_Previous"
				+ "\nFROM Stg1_Official_Clause A"
				+ "\nLEFT JOIN Stg1_Official_Clause C ON (C.Src_Doc_Id = " + ecfrDiffRpt.iEcfrSrcDocIdPrevious + " AND C.Clause_Number = A.Clause_Number)"
				+ "\nWHERE A.Src_Doc_Id = " + ecfrDiffRpt.iEcfrSrcDocIdNew
				+ "\nAND EXISTS ("
				+ "\n	SELECT B.Clause_Number"
				+ "\n    FROM Stg1_Raw_Clauses B"
				+ "\n    WHERE B.Src_Doc_Id = " + ecfrDiffRpt.iClauseSrcDocIdNew
				+ "\n    AND B.Clause_Number = A.Clause_Number"
				+ "\n)"
				+ "\nAND ((C.Stg1_Official_Clause_Id IS NULL) OR (TRIM(A.Content_HTML) <> TRIM(C.Content_HTML)))"

				+ "\nUNION\n"

				+ "\nSELECT 1 Is_Alt, A.Stg1_Official_Clause_Alt_Id Stg1IdNew, C.Stg1_Official_Clause_Alt_Id Stg1IdPrevious, CONCAT(A.Clause_Number, ' ', A.Alt_Number) Clause_Number, A.Manually_Added"
				+ "\n, NULL TitleNew"
				+ "\n, D.URI, NULL Prescription_New, NULL Prescription_Previous, 0 Prescription_Diff"
				+ "\n, A.Instructions Instructions_New, C.Instructions Instructions_Previous, (IFNULL(A.Instructions, '') <> IFNULL(C.Instructions, '')) Instructions_Diff"
				+ "\n, A.Content_HTML Content_HTML_New, C.Content_HTML Content_HTML_Previous"
				+ "\nFROM Stg1_Official_Clause_Alt A"
				+ "\nLEFT JOIN Stg1_Official_Clause_Alt C ON (C.Src_Doc_Id = " + ecfrDiffRpt.iEcfrSrcDocIdPrevious + " AND C.Clause_Number = A.Clause_Number AND C.Alt_Number = A.Alt_Number)"
				+ "\nLEFT JOIN Stg1_Official_Clause D ON (D.Src_Doc_Id = A.Src_Doc_Id AND C.Clause_Number = A.Clause_Number)"
				+ "\nWHERE A.Src_Doc_Id = " + ecfrDiffRpt.iEcfrSrcDocIdNew
				+ "\nAND EXISTS ("
				+ "\n	SELECT B.Clause_Number"
				+ "\n    FROM Stg1_Raw_Clauses B"
				+ "\n    WHERE B.Src_Doc_Id = " + ecfrDiffRpt.iClauseSrcDocIdNew
				+ "\n    AND B.Clause_Number = CONCAT(A.Clause_Number, ' ', A.Alt_Number)"
				+ "\n)"
				+ "\nAND ((C.Stg1_Official_Clause_Alt_Id IS NULL) OR (TRIM(A.Content_HTML) <> TRIM(C.Content_HTML)))"
				// + "\nAND (TRIM(A.Content_HTML) <> TRIM(C.Content_HTML))"

				+ "\nORDER BY 1";
			ResultSet rs1 = null;
			try {
				rs1 = statement.executeQuery(sSql);
				this.clear();
				while (rs1.next()) {
					this.add(new DiffClauseData(rs1));
				}
			} finally {
				SqlUtil.closeInstance(rs1);
			}
		}
		
		public PreparedStatement genStg2ClauseFillIns(Connection conn) throws SQLException {
			String sSql
				= "SELECT Fill_In_Code, Fill_In_Type, Fill_In_Max_Size, Fill_In_Group_Number, Fill_In_Placeholder, Fill_In_Default_Data, Fill_In_Display_Rows, Fill_In_For_Table, Fill_In_Heading"
				+ " FROM Stg2_Clause_Fill_Ins"
				+ " WHERE Stg1_Official_Clause_Id = ?"
				+ " ORDER BY Stg2_Clause_Fillins_Id";
			return conn.prepareStatement(sSql);
		}
		
		public PreparedStatement genStg2ClauseFillInsAlt(Connection conn) throws SQLException {
			String sSql
				= "SELECT Fill_In_Code, Fill_In_Type, Fill_In_Max_Size, Fill_In_Group_Number, Fill_In_Placeholder, Fill_In_Default_Data, Fill_In_Display_Rows, Fill_In_For_Table, Fill_In_Heading"
				+ " FROM Stg2_Clause_Fill_Ins_Alt"
				+ " WHERE Stg1_Official_Clause_Alt_Id = ?"
				+ " ORDER BY Stg2_Clause_Fillins_Alt_Id";
			return conn.prepareStatement(sSql);
		}
		
		private String genHtmlHeader() {
			StringBuffer result = new StringBuffer();
			result.append("<!DOCTYPE html><html lang='en'><head>\n");

			result.append("<title>ECFR Clause Data Difference Report from " + this.ecfrDiffRpt.claueVersionName + "</title>\n");

			result.append("<style>\n");
			result.append("html,body,td,th{font-family:Verdana,sans-serif;font-size:12px;}\n"); // 15px;
			result.append("td {padding:2px;padding-left:5px;padding-right:5px;vertical-align: top;}\n");
			result.append(".tdLabel {text-align: right; font-weight: bold;}\n");
			result.append(".tdData, .tdClauseData {font-family: 'Courier New', Courier, monospace;font-size:15px;}\n");
			result.append(".tdClauseData {vertical-align: top;}\n");
			result.append(".hd1 {font-size: 130%;}\n");
			result.append(".tdClauseNumber {font-size: 150%; font-weight: bold;}\n");
			result.append(".borderBottom {border-bottom: 1px solid black;}\n");
			result.append(".borderLeft {border-left: 1px solid black;}\n");
			result.append(".normal {font-weight: normal;}\n");
			result.append("</style>\n");
			result.append("</head>\n");
			result.append("<body>\n");
			return result.toString();
		}

		private String genHtmlFooter() {
			return "</body></html>";
		}
		
		public void genHtml(FileOutputStream fileOutputStream, PreparedStatement psStg2ClauseFillIns, PreparedStatement psStg2ClauseFillInsAlt) 
				throws IOException, SQLException {
			String sHtml = this.genHtmlHeader();
			fileOutputStream.write(sHtml.getBytes());
			
			sHtml = "<a name='top'></a>\n<h1>ECFR Clause Data Different Report from Clause Version of " + this.ecfrDiffRpt.claueVersionName + "</h1>\n"
					+ "<div align='right'>"
					+ "<span class='tdLabel'>Report date/time: </span> <span class='tdData'>" + (new Date()) + "</span>"
					+ "</div>\n"
					+ "<div align='center'>\n"
					+ "("
					+ "<span class='tdLabel'>Previous Clause Version Id: </span> <span class='tdData'>" + ecfrDiffRpt.previousClauseVersionId + "</span>"
					+ ", <span class='tdLabel' style='margin-left:15px'>New ECFR Source Doc Id: </span> <span class='tdData'>" + ecfrDiffRpt.iClauseSrcDocIdNew + "</span>"
					+ ", <span class='tdLabel' style='margin-left:15px'>New Clause Source Doc Id: </span> <span class='tdData'>" + ecfrDiffRpt.iClauseSrcDocIdNew + "</span>"
					+ ")"
					+ "</div>\n"
					+ "<fieldset><legend>Different ECFR Clauses:</legend>\n<ul>\n";
			fileOutputStream.write(sHtml.getBytes());
			for (DiffClauseData diffClauseData : this) {
				sHtml = diffClauseData.genHtmlSummaryLi();
				fileOutputStream.write(sHtml.getBytes());
			}
			sHtml = "</ul>\n</fieldset><br />\n";
			fileOutputStream.write(sHtml.getBytes());
			
			for (DiffClauseData diffClauseData : this) {
				sHtml = diffClauseData.genHtmlDetails(psStg2ClauseFillIns, psStg2ClauseFillInsAlt);
				fileOutputStream.write(sHtml.getBytes());
				sHtml = "<br/><br/>\n";
				fileOutputStream.write(sHtml.getBytes());
				
				diffClauseData.saveCluaseDataToFile(this.ecfrDiffRpt.subFolderPath);
			}
			sHtml = this.genHtmlFooter();
			fileOutputStream.write(sHtml.getBytes());
		}
		
	}
	
	// ==============================================================================
	private class DiffClauseData {
		protected boolean isAlt;
		protected Integer stg1IdNew;
		protected Integer stg1IdPrevious;
		protected String clauseNumber;
		protected boolean manuallyAdded;
		protected String titleNew;
		protected String uri;
		protected String prescriptionNew;
		protected String prescriptionPrevious;
		protected boolean prescriptionDiff;
		protected String instructionsNew;
		protected String instructionsPrevious;
		protected boolean instructionsDiff;
		protected String contentHTMLNew;
		protected String contentHTMLPrevious;
		
		protected FillInSet fillInSetNew = new FillInSet();
		protected FillInSet fillInSetPrevious = new FillInSet();
		
		public DiffClauseData(ResultSet rs1) throws SQLException {
			this.isAlt = CommonUtils.toBoolean(rs1.getObject("Is_Alt"));
			this.stg1IdNew = rs1.getInt("Stg1IdNew");
			this.stg1IdPrevious = CommonUtils.toInteger(rs1.getObject("Stg1IdPrevious")); // rs1.getInt("Stg1IdPrevious");
			this.clauseNumber = rs1.getString("Clause_Number");
			this.manuallyAdded = CommonUtils.toBoolean(rs1.getObject("Manually_Added"));
			this.titleNew = rs1.getString("TitleNew");
			this.uri = rs1.getString("URI");
			this.prescriptionNew = rs1.getString("Prescription_New");
			this.prescriptionPrevious = rs1.getString("Prescription_Previous");
			this.prescriptionDiff = CommonUtils.toBoolean(rs1.getObject("Prescription_Diff"));
			this.instructionsNew = rs1.getString("Instructions_New");
			this.instructionsPrevious = rs1.getString("Instructions_Previous");
			this.instructionsDiff = CommonUtils.toBoolean(rs1.getObject("Instructions_Diff"));
			this.contentHTMLNew = rs1.getString("Content_HTML_New");
			this.contentHTMLPrevious = rs1.getString("Content_HTML_Previous");
		}
		
		public String genHtmlSummaryLi() {
			StringBuffer result = new StringBuffer();
			result.append("<li><a href='#");
			result.append(this.stg1IdNew);
			result.append("'>");
			result.append(this.clauseNumber);
			result.append("</a></li>\n");
			
			return result.toString();
		}
		
		public String genHtmlDetails(PreparedStatement psStg2ClauseFillIns, PreparedStatement psStg2ClauseFillInsAlt) throws SQLException {
			StringBuffer result = new StringBuffer();
			result.append("<a name='" + this.stg1IdNew + "'></a>\n");
			result.append("<table border='1' style='width:100%;border-collapse:collapse;'>\n");

			result.append("<tr>\n");
			result.append("<td class='tdLabel'>Clause Number</td><td class='tdData tdClauseNumber'>");
			result.append(this.clauseNumber);
			result.append("</td>\n");
			result.append("<td class='tdLabel'>URI</td><td class='tdData'>");
			result.append((this.uri == null) ? "" : "<a href='" + this.uri + "' target='_blank'>"  + this.uri + "</a>");
			if (manuallyAdded)
				result.append(" (manually added)");
			result.append("</td>\n");
			result.append("</tr>\n");
			
			result.append("<tr>\n");
			if (this.isAlt) {
				if (this.instructionsDiff) {
					result.append("<td class='tdLabel'>Instructions Old</td><td class='tdData' colspan='3'>");
					result.append((this.instructionsPrevious == null) ? "" : this.instructionsPrevious);
					result.append("</td>\n");
					result.append("</tr>\n");

					result.append("<tr>\n");
					result.append("<td class='tdLabel'>Instructions New</td><td class='tdData' colspan='3'>");
					result.append((this.instructionsNew == null) ? "" : this.instructionsNew);
					result.append("</td>\n");
				} else {
					result.append("<td class='tdLabel'>Instructions</td><td class='tdData' colspan='3'>");
					result.append((this.instructionsNew == null) ? "" : this.instructionsNew);
					result.append("</td>\n");
				}
			} else {
				result.append("<td class='tdLabel'>Title</td><td class='tdData' colspan='3'>");
				result.append((this.titleNew == null) ? "" : this.titleNew);
				result.append("</td>\n");
				result.append("</tr>\n");

				result.append("<tr>\n");
				if (this.prescriptionDiff) {
					result.append("<td class='tdLabel'>Prescription Old</td><td class='tdData' colspan='3'>");
					result.append((this.prescriptionPrevious == null) ? "" : this.prescriptionPrevious);
					result.append("</td>\n");
					result.append("</tr>\n");

					result.append("<tr>\n");
					result.append("<td class='tdLabel'>Prescription New</td><td class='tdData' colspan='3'>");
					result.append((this.prescriptionNew == null) ? "" : this.prescriptionNew);
					result.append("</td>\n");
				} else {
					result.append("<td class='tdLabel'>Prescription</td><td class='tdData' colspan='3'>");
					result.append((this.prescriptionNew == null) ? "" : this.prescriptionNew);
					result.append("</td>\n");
				}
			}
			result.append("</tr>\n");

			result.append("<tr>\n");
			result.append("<td colspan='4'>\n");

			result.append("<table style='width=100%'>");

			result.append("<tr>");
			result.append("<th class='borderBottom'>Clause Data Prevous <span class='normal'>(id: " + this.stg1IdPrevious + ")</span></th>\n");
			result.append("<th class='borderBottom borderLeft'>Clause Data New <span class='normal'>(id: " + this.stg1IdNew + ")</span></th>\n");
			result.append("</tr>\n");

			result.append("<tr><td class='tdClauseData borderBottom'>\n");
			result.append((this.contentHTMLPrevious == null) ? "" : this.contentHTMLPrevious);
			//<iframe src="previous/252.225-7040.html" style="border:none;" width='100%'></iframe>
			result.append("\n</td><td class='tdClauseData borderBottom borderLeft'>\n");
			result.append((this.contentHTMLNew == null) ? "" : this.contentHTMLNew);
			// <iframe src="new/252.225-7040.html" style="border:none;" width='100%'></iframe>
			result.append("\n</td></tr>");

			result.append("<tr><th>Fill-Ins Prevous</th><th class='borderLeft'>Fill-Ins New</th></tr>\n"); //  class='borderBottom'

			result.append("<tr><td>\n");
			this.fillInSetNew.load(this.stg1IdNew, (this.isAlt ? psStg2ClauseFillInsAlt : psStg2ClauseFillIns));
			this.fillInSetPrevious.load(this.stg1IdPrevious, (this.isAlt ? psStg2ClauseFillInsAlt : psStg2ClauseFillIns));
			this.fillInSetNew.compare(this.fillInSetPrevious);
			result.append(this.fillInSetPrevious.getHtml(false));
			result.append("</td><td>\n");
			result.append(this.fillInSetNew.getHtml(true));

			/*
			if (this.isAlt) {
				result.append(this.genHtmlFillInTable(this.stg1IdPrevious, psStg2ClauseFillInsAlt));
				result.append("</td><td>\n");
				result.append(this.genHtmlFillInTable(this.stg1IdNew, psStg2ClauseFillInsAlt));
			} else {
				result.append(this.genHtmlFillInTable(this.stg1IdPrevious, psStg2ClauseFillIns));
				result.append("</td><td>\n");
				result.append(this.genHtmlFillInTable(this.stg1IdNew, psStg2ClauseFillIns));
			}
			*/
			result.append("</td></tr>\n");
			result.append("</table>\n");
			
			result.append("</td>\n");
			result.append("</tr>\n");
			
			result.append("</table>\n");

			result.append("<div align='center'><a href='#top'>Go Top</a></div>\n");
			
			return result.toString();
		}
		
		private void saveToFile(String fileName, String sHtml) throws IOException {
			sHtml = sHtml.replaceAll("<p>", "\r\n<p>"); // .replaceAll("<span>", "\r\n<span>")
			FileUtils.writeStringToFile(new File(fileName), sHtml);
			System.out.println(fileName);
		}
		
		public void saveCluaseDataToFile(String subFolderPath) {
			try {
				if ((this.stg1IdPrevious != null) && (this.contentHTMLPrevious != null)) {
					String fileName = subFolderPath + SUBFOLDER_PREVIOUS + "/" + this.clauseNumber + ".html";
					saveToFile(fileName, this.contentHTMLPrevious);
				}
				if ((this.stg1IdNew != null) && (this.contentHTMLNew != null)) {
					String fileName = subFolderPath + SUBFOLDER_NEW + "/" + this.clauseNumber + ".html";
					saveToFile(fileName, this.contentHTMLNew);
				}
			} catch (Exception oError) {
				System.out.println(oError.getMessage());
			}
		}
		
		/*
		public String genHtmlFillInTable(Integer stg1Id, PreparedStatement psStg2ClauseFillIns) throws SQLException {
			if (stg1Id == null)
				return "(none)";
			String result = ""; // new StringBuffer();
			// Fill_In_Code, Fill_In_Type, Fill_In_Max_Size, Fill_In_Group_Number, Fill_In_Placeholder, Fill_In_Default_Data, Fill_In_Display_Rows, Fill_In_For_Table, Fill_In_Heading
			
			result += "<table border='1' style='width:100%;border-collapse:collapse;'>\n";
			result += "<tr>\n";
			result += "<th>Code</th><th>Type</th><th>Max Size</th><th>Group Number</th><th>Placeholder</th><th>Default Data</th><th>Display Rows</th><th>For Table</th><th>Heading</th>\n";
			result += "</tr>\n";
			
			ResultSet rs1 = null;
			try {
				psStg2ClauseFillIns.setInt(1, stg1Id);
				rs1 = psStg2ClauseFillIns.executeQuery();
				int iRows = 0;
				while (rs1.next()) {
					iRows++;
					result += "<tr>\n";
					for (String fieldName : FILL_IN_FIELDS) {
						result += this.getTD(rs1, fieldName);
					}
					// Fill_In_Code, Fill_In_Type, Fill_In_Max_Size, Fill_In_Group_Number, Fill_In_Placeholder, Fill_In_Default_Data, Fill_In_Display_Rows, Fill_In_For_Table, Fill_In_Heading
					result += "</tr>\n";
				}
				if (iRows == 0) {
					result += "<tr><td colspan='" + (FILL_IN_FIELDS.length + 1) + "'>Not Found</td></tr>\n";
				}
			} finally {
				SqlUtil.closeInstance(rs1);
			}
			
			result += "</table>\n";
			return result;
		}
		
		private String getTD(ResultSet rs1, String fieldName) throws SQLException {
			Object oValue = rs1.getObject(fieldName);
			return "<td class='tdData'>" + ((oValue == null) ? "" : oValue.toString()) + "</td>\n";
		}
		*/
	}
	
	// ==============================================================================
	private class FillInSet extends ArrayList<FillInItem> {

		private static final long serialVersionUID = 6443181632117050557L;

		private Integer stg1Id = null;
		public void load(Integer stg1Id, PreparedStatement psStg2ClauseFillIns) throws SQLException {
			this.clear();
			this.stg1Id = stg1Id;
			ResultSet rs1 = null;
			try {
				psStg2ClauseFillIns.setInt(1, stg1Id);
				rs1 = psStg2ClauseFillIns.executeQuery();
				while (rs1.next()) {
					FillInItem fillInItem = new FillInItem(rs1);
					this.add(fillInItem);
				}
			} finally {
				SqlUtil.closeInstance(rs1);
			}
		}
		
		public void compare(FillInSet origFillInSet) {
			for (FillInItem fillInItem : this) {
				FillInItem origFillInItem = origFillInSet.findByCode(fillInItem);
				fillInItem.checkDifference(origFillInItem);
			}
			for (FillInItem fillInItem : origFillInSet) {
				if (fillInItem.difference == null)
					fillInItem.difference = "removed";
			}
		}
		
		private FillInItem findByCode(FillInItem origFillInItem) {
			String fillInCode = origFillInItem.getFillInCode();
			for (FillInItem fillInItem : this) {
				if (fillInCode.equals(fillInItem.getFillInCode()))
					return fillInItem;
			}
			return null;
		}
		
		public String getHtml(boolean isNew) {
			if (this.stg1Id == null) {
				return "(none)";
			}
			StringBuffer result = new StringBuffer();
			result.append("<table border='1' style='width:100%;border-collapse:collapse;'>\n");
			result.append("<tr>\n");
			result.append("<th>Status</th>");
			result.append("<th>Code</th><th>Type</th><th>Max Size</th><th>Group Number</th><th>Placeholder</th><th>Default Data</th><th>Display Rows</th><th>For Table</th><th>Heading</th>\n");
			result.append("</tr>\n");
			
			if (this.isEmpty()) {
				result.append("<tr><td colspan='" + (FILL_IN_FIELDS.length + 1) + "'>Not Found</td></tr>\n");
			} else {
				for (FillInItem fillInItem : this) {
					result.append(fillInItem.getTR());
				}
			}
			
			result.append("</table>\n");
			return result.toString();
		}
		
	}
	
	// ==============================================================================
	private class FillInItem extends ArrayList<Object> {
		
		private static final long serialVersionUID = 1123632542639886128L;

		public String difference = null;
		
		public FillInItem(ResultSet rs1) throws SQLException {
			for (String fieldName : FILL_IN_FIELDS) {
				Object oValue = rs1.getObject(fieldName);
				this.add(oValue);
			}
		}

		public String getFillInCode() {
			return this.get(0).toString();
		}
		
		public String getTR() {
			StringBuffer result = new StringBuffer();
			result.append("<tr>\n");
			result.append("<td>" + this.difference + "</td>\n"); //  class='tdData'
			for (Object oValue : this) {
				result.append("<td>" + ((oValue == null) ? "" : oValue.toString()) + "</td>\n");
			}
			result.append("</tr>\n");
			return result.toString();
		}
		
		public boolean checkDifference(FillInItem origFillInItem) {
			if (origFillInItem == null) {
				this.difference = "new";
				return true;
			}
			for (int index = 0; index < this.size(); index++) {
				if (!CommonUtils.isSame(origFillInItem.get(index), this.get(index))) {
					this.difference = "diff";
					origFillInItem.difference = this.difference;
					return true;
				}
			}
			this.difference = "same";
			origFillInItem.difference = this.difference;
			return false;
		}
		
	}
	
	// ===================================================================
	public static void main( String[] args ) {
		CpmConfig cpmConfig = CpmConfig.getInstance();
		Connection conn = null;
		try {
    		conn = cpmConfig.getDbConnection();
    		EcfrDiffRpt ecfrDiffRpt = new EcfrDiffRpt(12, 366, 365, conn, "E:\\Projects\\ClauseLogic\\Tasks\\parser\\sql_output");
    		ecfrDiffRpt.run();
		}
		catch (Exception oError) {
    		oError.printStackTrace();
    	}
    	finally {
    		SqlUtil.closeInstance(conn);
    	}
	}
}
