package hgs.cpm.ecfr;

import gov.dod.cls.db.FillInTypeRefRecord;
import gov.dod.cls.utils.CommonUtils;
import gov.dod.cls.utils.sql.SqlUtil;
import hgs.cpm.config.CpmConfig;
import hgs.cpm.db.Stg1OfficialClauseAltRecord;
import hgs.cpm.db.Stg1OfficialClauseRecord;
import hgs.cpm.db.Stg2ClauseFillInsAltRecord;
import hgs.cpm.db.Stg2ClauseFillInsRecord;
import hgs.cpm.db.StgSrcDocumentSet;
import hgs.cpm.db.template.TemplateStg2ClauseFillInRecord;
import hgs.cpm.ecfr.util.EcfrFillInUtils;
import hgs.cpm.ecfr.util.EcfrProcessFullEdit;
import hgs.cpm.ecfr.util.Stg1RawClauseEditableRecord;
import hgs.cpm.ecfr.util.Stg1RawClauseEditableSet;
// import hgs.cpm.utils.FillinTemplateUtils;
import hgs.cpm.utils.HtmlUtils;
import hgs.cpm.utils.SqlCommons;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.regex.Pattern;

public class EcfrParseFillins {
	
	private Connection conn = null;
	private Integer iEcfrSrcDocId = null;
	private Integer iClauseSrcDocId = null;
	private Stg1RawClauseEditableSet stg1RawClauseEditableSet = null;
	private ArrayList<String> fillinClauseNames = null;
	private boolean bProcessFullEdit;
	
	public EcfrParseFillins(Connection conn, Integer iEcfrSrcDocId, Integer iClauseSrcDocId, boolean pbFullEdit) {
		this.conn = conn;
		this.iEcfrSrcDocId = iEcfrSrcDocId;
		this.iClauseSrcDocId = iClauseSrcDocId;
		this.bProcessFullEdit = pbFullEdit;
	}
	
	public void run() throws SQLException {
		this.prepare();

		this.addManualFillIns();
		
		this.generateRegularClauseFillIns();
		this.generateAltClauseFillIns();
		
		if (this.bProcessFullEdit)
			this.processFullEdit();

		this.replaceFillIns("textbox", FillInTypeRefRecord.TYPE_CD_STRING, 100);
		this.replaceAltFillIns("textbox", FillInTypeRefRecord.TYPE_CD_STRING, 100);

		this.replaceFillIns("checkbox", FillInTypeRefRecord.TYPE_CD_CHECKBOX, null);
		this.replaceAltFillIns("checkbox", FillInTypeRefRecord.TYPE_CD_CHECKBOX, null);

		this.replaceFillIns("memo", FillInTypeRefRecord.TYPE_CD_MEMO, null);
		this.replaceAltFillIns("memo", FillInTypeRefRecord.TYPE_CD_MEMO, null);
		
		if (this.bProcessFullEdit)
			processFullEditWithFillins();
	}
	
	private void generateAltClauseFillIns() throws SQLException {
		String sSql;
		Stg2ClauseFillInsAltRecord oStg2ClauseFillInsAltRecord = new Stg2ClauseFillInsAltRecord();
		PreparedStatement psSelectClause = null;
		PreparedStatement psUpdateClause = null;
		PreparedStatement psFillInInsertClause = null;
		ResultSet rs1 = null;
		try {
			
			//ArrayList<String> fillinArray = FillinTemplateUtils.ReadFillinList();
			
			psFillInInsertClause = oStg2ClauseFillInsAltRecord.prepareInsert(conn);
			
			sSql = "UPDATE " + Stg1OfficialClauseAltRecord.TABLE_STG1_OFFICIAL_CLAUSE_ALT
					+ " SET " + Stg1OfficialClauseAltRecord.FIELD_CONTENT_HTML_FILLINS + " = ?"
					+ ", " + Stg1OfficialClauseAltRecord.FIELD_CONTENT_HTML_PROCESSED + " = ?"
					+ ", " + Stg1OfficialClauseAltRecord.FIELD_EDIT_AND_FILLIN + " = ?"
					+ " WHERE " + Stg1OfficialClauseAltRecord.FIELD_STG1_OFFICIAL_CLAUSE_ALT_ID + " = ?";
			psUpdateClause = conn.prepareStatement(sSql);

			sSql = "SELECT " + Stg1OfficialClauseAltRecord.FIELD_STG1_OFFICIAL_CLAUSE_ALT_ID
					+ ", CONCAT(" + Stg1OfficialClauseAltRecord.FIELD_CLAUSE_NUMBER + ", ' ', " + Stg1OfficialClauseAltRecord.FIELD_Alt_Number + ")" 
					+ ", " + Stg1OfficialClauseAltRecord.FIELD_CONTENT_HTML
					+ " FROM " + Stg1OfficialClauseAltRecord.TABLE_STG1_OFFICIAL_CLAUSE_ALT
					+ " WHERE " + Stg1OfficialClauseAltRecord.FIELD_SRC_DOC_ID + " = ?"
					+ " AND " + Stg1OfficialClauseAltRecord.FIELD_CONTENT_HTML + " IS NOT NULL";
			psSelectClause = conn.prepareStatement(sSql);
			psSelectClause.setInt(1, iEcfrSrcDocId);
			rs1 = psSelectClause.executeQuery();
			while (rs1.next()) {
				int iStg1OfficialClauseId = rs1.getInt(Stg1OfficialClauseAltRecord.FIELD_STG1_OFFICIAL_CLAUSE_ALT_ID);
				String sClauseNumber = rs1.getString(2);
				String sClauseHtml = rs1.getString(Stg1OfficialClauseAltRecord.FIELD_CONTENT_HTML);
				
				String sFillInHtml = null;
				String sProcessedHtml = null;
				int iIsEditAndFillIn = 0;
				
				// HtmlUtils.normalizeHtml()
				
				boolean bFullEditProcessed = false;
				if (this.bProcessFullEdit) {
					if (this.stg1RawClauseEditableSet.isEditAll(sClauseNumber)) {
						bFullEditProcessed = true;
					}
				}
				
				// CJ-533
				if ((bFullEditProcessed == true) && this.fillinClauseNames.contains(sClauseNumber.toUpperCase())) {
					//FillinTemplateUtils.parseFillinInstructions(sClauseNumber, sClauseHtml, fillinArray);
					sFillInHtml = EcfrFillInUtils.produceFillins(sClauseNumber, iStg1OfficialClauseId, sClauseHtml,
							oStg2ClauseFillInsAltRecord, psFillInInsertClause);
					iIsEditAndFillIn = 1;
					System.out.println("Full Edit w. Fill-in: " + sClauseNumber);
					
				} else
				
				if ((bFullEditProcessed == false) && this.fillinClauseNames.contains(sClauseNumber.toUpperCase())) {
					//sFillInHtml = EcfrFillInUtils.parseFillinsAndNormalize(sClauseNumber, sClauseHtml);
					//FillinTemplateUtils.parseFillinInstructions(sClauseNumber, sClauseHtml, fillinArray);
					sFillInHtml = EcfrFillInUtils.produceFillins(sClauseNumber, iStg1OfficialClauseId, sClauseHtml,
							oStg2ClauseFillInsAltRecord, psFillInInsertClause);
				} else {
					sProcessedHtml = HtmlUtils.normalizeHtml(sClauseHtml);
					System.out.println("No Fill-in: " + sClauseNumber);
				}
				
				SqlUtil.setParam(psUpdateClause, 1, sFillInHtml, java.sql.Types.VARCHAR);
				SqlUtil.setParam(psUpdateClause, 2, sProcessedHtml, java.sql.Types.VARCHAR);
				SqlUtil.setParam(psUpdateClause, 3,iIsEditAndFillIn, java.sql.Types.SMALLINT);
				psUpdateClause.setInt(4, iStg1OfficialClauseId);
				psUpdateClause.execute();
			}
		} finally {
			SqlUtil.closeInstance(rs1, psSelectClause);
			SqlUtil.closeInstance(psUpdateClause);
		}
	}
	
	private void generateRegularClauseFillIns() throws SQLException {
		String sSql;
		Stg2ClauseFillInsRecord oStg2ClauseFillInsRecord = new Stg2ClauseFillInsRecord();
		PreparedStatement psSelectClause = null;
		PreparedStatement psUpdateClause = null;
		PreparedStatement psFillInInsertClause = null;
		ResultSet rs1 = null;
		try {
			//ArrayList<String> fillinArray = FillinTemplateUtils.ReadFillinList();
			
			psFillInInsertClause = oStg2ClauseFillInsRecord.prepareInsert(conn);
			
			sSql = "UPDATE " + Stg1OfficialClauseRecord.TABLE_STG1_OFFICIAL_CLAUSE
					+ " SET " + Stg1OfficialClauseRecord.FIELD_CONTENT_HTML_FILLINS + " = ?"
					+ ", " + Stg1OfficialClauseRecord.FIELD_CONTENT_HTML_PROCESSED + " = ?"
					+ ", " + Stg1OfficialClauseRecord.FIELD_EDIT_AND_FILLIN + " = ?"
					+ " WHERE " + Stg1OfficialClauseRecord.FIELD_STG1_OFFICIAL_CLAUSE_ID + " = ?";
			psUpdateClause = conn.prepareStatement(sSql);

			sSql = "SELECT " + Stg1OfficialClauseRecord.FIELD_STG1_OFFICIAL_CLAUSE_ID
					+ ", " + Stg1OfficialClauseRecord.FIELD_CLAUSE_NUMBER
					+ ", " + Stg1OfficialClauseRecord.FIELD_CONTENT_HTML
					+ " FROM " + Stg1OfficialClauseRecord.TABLE_STG1_OFFICIAL_CLAUSE
					+ " WHERE " + Stg1OfficialClauseRecord.FIELD_SRC_DOC_ID + " = ?"
					+ " AND " + Stg1OfficialClauseRecord.FIELD_CONTENT_HTML + " IS NOT NULL";
			psSelectClause = conn.prepareStatement(sSql);
			psSelectClause.setInt(1, iEcfrSrcDocId);
			rs1 = psSelectClause.executeQuery();
			while (rs1.next()) {
				int iStg1OfficialClauseId = rs1.getInt(Stg1OfficialClauseRecord.FIELD_STG1_OFFICIAL_CLAUSE_ID);
				String sClauseNumber = rs1.getString(Stg1OfficialClauseRecord.FIELD_CLAUSE_NUMBER);
				String sClauseHtml = rs1.getString(Stg1OfficialClauseRecord.FIELD_CONTENT_HTML);
				
				String sFillInHtml = null;
				String sProcessedHtml = null;
				int iIsEditAndFillIn = 0;
				
				// HtmlUtils.normalizeHtml()
				
				boolean bFullEditProcessed = false;
				if (this.bProcessFullEdit) {
					if (this.stg1RawClauseEditableSet.isEditAll(sClauseNumber)) {
						bFullEditProcessed = true;
					}
				}
				
				// CJ-533
				if ((bFullEditProcessed == true) && this.fillinClauseNames.contains(sClauseNumber.toUpperCase())) {
					//FillinTemplateUtils.parseFillinInstructions(sClauseNumber, sClauseHtml, fillinArray);
					sFillInHtml = EcfrFillInUtils.produceFillins(sClauseNumber, iStg1OfficialClauseId, sClauseHtml,
							oStg2ClauseFillInsRecord, psFillInInsertClause);
					iIsEditAndFillIn = 1;
					System.out.println("Full Edit w. Fill-in: " + sClauseNumber);
					
				} else
				if ((bFullEditProcessed == false) && this.fillinClauseNames.contains(sClauseNumber.toUpperCase())) {
					//sFillInHtml = EcfrFillInUtils.parseFillinsAndNormalize(sClauseNumber, sClauseHtml);
					//FillinTemplateUtils.parseFillinInstructions(sClauseNumber, sClauseHtml, fillinArray);
					sFillInHtml = EcfrFillInUtils.produceFillins(sClauseNumber, iStg1OfficialClauseId, sClauseHtml,
							oStg2ClauseFillInsRecord, psFillInInsertClause);
				} else {
					sProcessedHtml = HtmlUtils.normalizeHtml(sClauseHtml);
					System.out.println("No Fill-in: " + sClauseNumber);
				}
				
				SqlUtil.setParam(psUpdateClause, 1, sFillInHtml, java.sql.Types.VARCHAR);
				SqlUtil.setParam(psUpdateClause, 2, sProcessedHtml, java.sql.Types.VARCHAR);
				SqlUtil.setParam(psUpdateClause, 3,iIsEditAndFillIn, java.sql.Types.SMALLINT);
				psUpdateClause.setInt(4, iStg1OfficialClauseId);
				psUpdateClause.execute();
			}
		} finally {
			SqlUtil.closeInstance(rs1, psSelectClause);
			SqlUtil.closeInstance(psUpdateClause);
			SqlUtil.closeInstance(psFillInInsertClause);
		}
	}
	
	private void prepare() throws SQLException {
		String sSql;
		sSql = "DELETE FROM Stg2_Clause_Fill_Ins" +
				" WHERE Stg1_Official_Clause_Id IN" +
				" (SELECT Stg1_Official_Clause_Id FROM Stg1_Official_Clause WHERE Src_Doc_Id = ?)";
		SqlCommons.executeForDocId(this.conn, sSql, this.iEcfrSrcDocId);
		
		sSql = "DELETE FROM Stg2_Clause_Fill_Ins_Alt" +
				" WHERE Stg1_Official_Clause_Alt_Id IN" +
				" (SELECT Stg1_Official_Clause_Alt_Id FROM Stg1_Official_Clause_Alt WHERE Src_Doc_Id = ?)";
		SqlCommons.executeForDocId(this.conn, sSql, this.iEcfrSrcDocId);

		sSql = "UPDATE Stg1_Official_Clause SET Content_Html_Processed = NULL WHERE Src_Doc_Id = ?";
		SqlCommons.executeForDocId(this.conn, sSql, this.iEcfrSrcDocId);

		sSql = "UPDATE Stg1_Official_Clause_Alt SET Content_Html_Processed = NULL WHERE Src_Doc_Id = ?";
		SqlCommons.executeForDocId(this.conn, sSql, this.iEcfrSrcDocId);
		
		this.fillinClauseNames = EcfrParseFillins.getClauseNumbersforFillinType(this.iClauseSrcDocId, this.conn);
		this.stg1RawClauseEditableSet = Stg1RawClauseEditableSet.load(this.iClauseSrcDocId, this.conn);
	}
	
	private int processFillIns(ResultSet rs1, PreparedStatement psInsert, PreparedStatement psUpdate, 
			String paramName, String paramType, Integer paramSize, boolean pbIsAlt) throws SQLException {
		int iCount = 0;
		String srcParam = "{{" + paramName + "}}";
		while (rs1.next()) {
			int id = rs1.getInt(1);
			String htmlFillIns = rs1.getString(2);
			int isEditAndFillIn = rs1.getInt(3);
			String clauseNumber = rs1.getString(4);
			
			Stg1RawClauseEditableRecord oStg1RawClauseEditableRecord = this.stg1RawClauseEditableSet.findByClauseNumber(clauseNumber);
			if ((oStg1RawClauseEditableRecord != null) && oStg1RawClauseEditableRecord.isAllEditable() && this.bProcessFullEdit && (isEditAndFillIn==0)) 
				continue;
			
			if (CommonUtils.isNotEmpty(clauseNumber))
				clauseNumber = clauseNumber.replaceAll(" ", "_");
		
			int iParam = 0;
			String newHtmlFillIns = "";
			String[] aContents = htmlFillIns.split(Pattern.quote(srcParam));
			for (int iSplit = 0; iSplit < aContents.length; iSplit++) {
				String html = aContents[iSplit];
				if (iSplit > 0) {
					String tgtParam = paramName + "_" + clauseNumber + "[" + iParam++ + "]";
					newHtmlFillIns += "{{" + tgtParam + "}}";
					psInsert.setInt(1, id);
					psInsert.setString(2, tgtParam);
					psInsert.setString(3, paramType);
					if (paramSize == null)
						psInsert.setNull(4, java.sql.Types.INTEGER);
					else
						psInsert.setInt(4, paramSize);
					psInsert.execute();
					// newHtmlFillIns += tgtParam;
				}
				newHtmlFillIns += html;
			}
			if (htmlFillIns.endsWith(srcParam)) { //Added so fillin at end of clause is not skipped
				String tgtParam = paramName + "_" + clauseNumber + "[" + iParam++ + "]";
				newHtmlFillIns += "{{" + tgtParam + "}}";
				psInsert.setInt(1, id);
				psInsert.setString(2, tgtParam);
				psInsert.setString(3, paramType);
				if (paramSize == null)
					psInsert.setNull(4, java.sql.Types.INTEGER);
				else
					psInsert.setInt(4, paramSize);
				psInsert.execute();
			}
			psUpdate.setString(1, newHtmlFillIns);
			psUpdate.setInt(2, id);
			psUpdate.execute();
			iCount++;
		}
		return iCount;
	}

	private void processFullEdit() throws SQLException {
		//Stg1RawClauseEditableSet oEditAllSet = this.stg1RawClauseEditableSet.subsetForEditAll(true);
		if (this.stg1RawClauseEditableSet.size() == 0)
			return;
		
		String sSql;
		
		int iCount = 0;
		PreparedStatement psSelectClause = null;
		PreparedStatement psInsertClause = null;
		PreparedStatement psUpdateClause = null;

		PreparedStatement psSelectAlt = null;
		PreparedStatement psInsertAlt = null;
		PreparedStatement psUpdateAlt = null;
		
		Stg2ClauseFillInsRecord oStg2ClauseFillInsRecord = new Stg2ClauseFillInsRecord();
		Stg2ClauseFillInsAltRecord oStg2ClauseFillInsAltRecord = new Stg2ClauseFillInsAltRecord();
		
		ResultSet rs1 = null;
		String sFillInCode;
		String[] aItems;
		try {
			/*sSql = "INSERT INTO Stg2_Clause_Fill_Ins (Stg1_Official_Clause_Id, Fill_In_Code, Fill_In_Type, Fill_In_Max_Size)\n" +
				" VALUES (?, ?, ?, ?)";
			psInsertClause = this.conn.prepareStatement(sSql);*/
			psInsertClause = oStg2ClauseFillInsRecord.prepareInsert(conn);
			psInsertAlt = oStg2ClauseFillInsAltRecord.prepareInsert(conn);
			
			sSql = "UPDATE Stg1_Official_Clause SET Content_Html_Processed = ? WHERE Stg1_Official_Clause_Id = ?";
			psUpdateClause = this.conn.prepareStatement(sSql);

			sSql = "UPDATE Stg1_Official_Clause_Alt SET Content_Html_Processed = ? WHERE Stg1_Official_Clause_Alt_Id = ?";
			psUpdateAlt = this.conn.prepareStatement(sSql);
				
			sSql = "SELECT Stg1_Official_Clause_Id, IFNULL(Content_Html_Processed, Content_Html_FillIns) Content_Html_FillIns, IsEditAndFillIn\n" +
					"FROM Stg1_Official_Clause \n" +
					"WHERE Src_Doc_Id = ?\n" +
					"AND Clause_Number = ?";
			psSelectClause = this.conn.prepareStatement(sSql);
			psSelectClause.setInt(1, this.iEcfrSrcDocId);

			sSql = "SELECT Stg1_Official_Clause_Alt_Id, IFNULL(Content_Html_Processed, Content_Html_FillIns) Content_Html_FillIns, IsEditAndFillIn\n" +
					"FROM Stg1_Official_Clause_Alt \n" +
					"WHERE Src_Doc_Id = ?\n" +
					"AND CONCAT(Clause_Number, ' ', Alt_Number) = ?";
			psSelectAlt = this.conn.prepareStatement(sSql);
			psSelectAlt.setInt(1, this.iEcfrSrcDocId);

			for (Stg1RawClauseEditableRecord oEditableRecord : this.stg1RawClauseEditableSet) {
				String sClauseNumber = oEditableRecord.getClauseNumber();
				
				boolean bAlt = sClauseNumber.contains(" ");
				rs1 = null;
				try {
					PreparedStatement psSelect = (bAlt ? psSelectAlt : psSelectClause);
					PreparedStatement psInsert = (bAlt ? psInsertAlt : psInsertClause);
					PreparedStatement psUpdate = (bAlt ? psUpdateAlt : psUpdateClause);
					TemplateStg2ClauseFillInRecord oFillInRecord = (bAlt ? oStg2ClauseFillInsAltRecord : oStg2ClauseFillInsRecord);
					psSelect.setString(2, sClauseNumber);
					rs1 = psSelect.executeQuery();
					if (!rs1.next()) {
						System.out.println("AdjustEcfrContent.processFullEdit(): Not found '" + sClauseNumber + "' clause");
						continue;
					}
					
					int iClauseId = rs1.getInt(1);
					String sHtml = rs1.getString(2);
					int isEditAndFillIn = rs1.getInt(3);
					if (bAlt) {
						aItems = sClauseNumber.split(" ");
						sFillInCode = aItems[0];
					} else
						sFillInCode = sClauseNumber;
					sFillInCode = "memobox_" + sFillInCode + "[0]";
					
					oFillInRecord.clear();
					oFillInRecord.setStg1OfficialClauseId(iClauseId);
					oFillInRecord.setFillInMaxSize(32000);
					oFillInRecord.setFillInType(FillInTypeRefRecord.TYPE_CD_MEMO);
					oFillInRecord.setFillInCode(sFillInCode);

					if (oEditableRecord.isAllEditable()) {
						if (isEditAndFillIn==1) {
							sFillInCode = sFillInCode.replace("memobox_", "memobox_fe_");
							oFillInRecord.setFillInCode(sFillInCode);
							
							// Note, for edits w. fillins this is just a placeholder. 
							// Default data will be properly set in processFullEditWithFillins
							oFillInRecord.setFillInDefaultData(sHtml);	
							oFillInRecord.insert(psInsert);
							
							// Don't call psUpdate.execute();
							// It will overwrite clause data content for edits w. fillins
						}
						else {
							oFillInRecord.setFillInDefaultData(sHtml);	
							oFillInRecord.insert(psInsert);
							
							String sContentHtmlFillIns = "{{" + sFillInCode + "}}";
							psUpdate.setString(1, sContentHtmlFillIns);
							psUpdate.setInt(2, iClauseId);
							psUpdate.execute();
							iCount++;
							System.out.println("AdjustEcfrContent.processFullEdit() for (" + oFillInRecord.getStg2Id() + ") " + sClauseNumber + " with " + sFillInCode);
							
						}
					} else {
						if (EcfrProcessFullEdit.process(oEditableRecord, iClauseId, sHtml, sFillInCode, psInsert, psUpdate, oFillInRecord))
							iCount++;
					}
				} finally {
					SqlUtil.closeInstance(rs1);
					rs1 = null;
				}
			}

		}
		finally {
			SqlUtil.closeInstance(rs1);
			SqlUtil.closeInstance(psInsertClause);
			SqlUtil.closeInstance(psUpdateClause);
			SqlUtil.closeInstance(psSelectClause);
			SqlUtil.closeInstance(psInsertAlt);
			SqlUtil.closeInstance(psUpdateAlt);
			SqlUtil.closeInstance(psSelectAlt);
		}
		System.out.println("AdjustEcfrContent.processFullEdit(): " + iCount);
	}
	
	private void processFullEditWithFillins() throws SQLException {
		//Stg1RawClauseEditableSet oEditAllSet = this.stg1RawClauseEditableSet.subsetForEditAll(true);
		if (this.stg1RawClauseEditableSet.size() == 0)
			return;
		
		String sSql;
		
		int iCount = 0;
		PreparedStatement psSelectClause = null;
		PreparedStatement psUpdateClause = null;

		PreparedStatement psSelectAlt = null;
		PreparedStatement psUpdateAlt = null;

		PreparedStatement psUpdateClauseData = null;
		PreparedStatement psUpdateAltData = null;
		
		ResultSet rs1 = null;
		String sFillInCode;

		try {
			
			sSql = "UPDATE Stg2_Clause_Fill_Ins SET fill_in_default_data = ? "
					+ "WHERE Stg1_Official_Clause_Id = ? "
					+ "AND fill_in_code = ?";
			psUpdateClause = this.conn.prepareStatement(sSql);
			
			sSql = "UPDATE Stg2_Clause_Fill_Ins_Alt SET fill_in_default_data = ? "
					+ "WHERE Stg1_Official_Clause_Alt_Id = ? "
					+ "AND fill_in_code = ?";
			psUpdateAlt = this.conn.prepareStatement(sSql);
			
			sSql = "UPDATE Stg1_Official_Clause SET Content_Html_Processed = ? WHERE Stg1_Official_Clause_Id = ?";
			psUpdateClauseData = this.conn.prepareStatement(sSql);

			sSql = "UPDATE Stg1_Official_Clause_Alt SET Content_Html_Processed = ? WHERE Stg1_Official_Clause_Alt_Id = ?";
			psUpdateAltData = this.conn.prepareStatement(sSql);
			
			sSql = "SELECT Stg1_Official_Clause_Id, Content_Html_Processed, Content_Html_FillIns, IsEditAndFillIn\n" +
					"FROM Stg1_Official_Clause \n" +
					"WHERE Src_Doc_Id = ?\n" +
					"AND Clause_Number = ? " +
					"AND IsEditAndFillIn = 1";
			psSelectClause = this.conn.prepareStatement(sSql);
			psSelectClause.setInt(1, this.iEcfrSrcDocId);

			sSql = "SELECT Stg1_Official_Clause_Alt_Id, Content_Html_Processed, Content_Html_FillIns, IsEditAndFillIn\n" +
					"FROM Stg1_Official_Clause_Alt \n" +
					"WHERE Src_Doc_Id = ?\n" +
					"AND CONCAT(Clause_Number, ' ', Alt_Number) = ? " +
					"AND IsEditAndFillIn = 1";
			psSelectAlt = this.conn.prepareStatement(sSql);
			psSelectAlt.setInt(1, this.iEcfrSrcDocId);

			for (Stg1RawClauseEditableRecord oEditableRecord : this.stg1RawClauseEditableSet) {
				String sClauseNumber = oEditableRecord.getClauseNumber();
				
				if (!oEditableRecord.isAllEditable())
					continue;
				
				boolean bAlt = sClauseNumber.contains(" ");
				rs1 = null;
				try {
					PreparedStatement psSelect = (bAlt ? psSelectAlt : psSelectClause);
					PreparedStatement psUpdate = (bAlt ? psUpdateAlt : psUpdateClause);
					PreparedStatement psUpdateData = (bAlt ? psUpdateAltData : psUpdateClauseData);
					
					psSelect.setString(2, sClauseNumber);
					rs1 = psSelect.executeQuery();
					if (!rs1.next()) {
						System.out.println("AdjustEcfrContent.processFullEditWithFillins(): Not found '" + sClauseNumber + "' clause");
						continue;
					}

					int iClauseId = rs1.getInt(1);
					String sHtmlProcessed = rs1.getString(2);
					if (sHtmlProcessed == null)
						sHtmlProcessed = rs1.getString(3);
					
					if (bAlt) {
						String[] aItems = sClauseNumber.split(" ");
						sFillInCode = aItems[0];
					} else
						sFillInCode = sClauseNumber;
					sFillInCode = "memobox_fe_" + sFillInCode + "[0]";
					
					psUpdate.setString(1, sHtmlProcessed);
					psUpdate.setInt(2, iClauseId);
					psUpdate.setString(3, sFillInCode);
					psUpdate.execute();
					
					psUpdateData.setString(1, "{{" + sFillInCode + "}}");
					psUpdateData.setInt(2, iClauseId);
					psUpdateData.execute();
					
					System.out.println("AdjustEcfrContent.processFullEditWithFillins() for (" + sClauseNumber + ") " + sFillInCode + " with " + sHtmlProcessed);
					 
				} finally {
					SqlUtil.closeInstance(rs1);
					rs1 = null;
				}
			}

		}
		finally {
			SqlUtil.closeInstance(rs1);
			SqlUtil.closeInstance(psUpdateClause);
			SqlUtil.closeInstance(psSelectClause);
			SqlUtil.closeInstance(psUpdateAlt);
			SqlUtil.closeInstance(psSelectAlt);
		}
		System.out.println("AdjustEcfrContent.processFullEditWithFillins(): " + iCount);
	}
	private void replaceFillIns(String paramName, String paramType, Integer paramSize) throws SQLException {
		String sSql;
		
		int iCount = 0;
		PreparedStatement ps1 = null;
		PreparedStatement psInsert = null;
		PreparedStatement psUpdate = null;
		ResultSet rs1 = null;
		try {
			sSql = "INSERT INTO Stg2_Clause_Fill_Ins (Stg1_Official_Clause_Id, Fill_In_Code, Fill_In_Type, Fill_In_Max_Size)\n" +
				" VALUES (?, ?, ?, ?)";
			psInsert = this.conn.prepareStatement(sSql);
			
			sSql = "UPDATE Stg1_Official_Clause SET Content_Html_Processed = ?\n" +
					"WHERE Stg1_Official_Clause_Id = ?";/*\n" +
					"AND clause_number NOT IN\n" +
					"(SELECT clause_number FROM Stg_Clause_Fill_Ins_Add)";*/
			psUpdate = this.conn.prepareStatement(sSql);
				
			
			String srcParam = "{{" + paramName + "}}";	
			sSql = "SELECT Stg1_Official_Clause_Id, IFNULL(Content_Html_Processed, Content_Html_FillIns) Content_Html_FillIns, IsEditAndFillIn, Clause_Number\n" +
					"FROM Stg1_Official_Clause \n" +
					"WHERE Src_Doc_Id = ?\n" +
					"AND Content_Html_FillIns LIKE '%" + srcParam + "%'\n" +
					"AND Clause_Number IN (\n" + LoadEcfr.SQL_FILL_IN_CLAUSE_NUMBER + "\n)";
			ps1 = this.conn.prepareStatement(sSql);
			ps1.setInt(1, this.iEcfrSrcDocId);
			ps1.setInt(2, this.iClauseSrcDocId);
			rs1 = ps1.executeQuery();
			iCount = this.processFillIns(rs1, psInsert, psUpdate, paramName, paramType, paramSize, false);
		}
		finally {
			SqlUtil.closeInstance(psInsert);
			SqlUtil.closeInstance(psUpdate);
			SqlUtil.closeInstance(rs1, ps1, null);
		}
		System.out.println("AdjustEcfrContent.replaceFillIns('" + paramName + "'): " + iCount);
	}
	
	private void replaceAltFillIns(String paramName, String paramType, Integer paramSize) throws SQLException {
		String sSql;
		
		int iCount = 0;
		PreparedStatement ps1 = null;
		PreparedStatement psInsert = null;
		PreparedStatement psUpdate = null;
		ResultSet rs1 = null;

		try {
			sSql = "INSERT INTO Stg2_Clause_Fill_Ins_Alt (Stg1_Official_Clause_Alt_Id, Fill_In_Code, Fill_In_Type, Fill_In_Max_Size)\n" +
				" VALUES (?, ?, ?, ?)";
			psInsert = conn.prepareStatement(sSql);
			
			sSql = "UPDATE Stg1_Official_Clause_Alt SET Content_Html_Processed = ? WHERE Stg1_Official_Clause_Alt_Id = ?";
			psUpdate = this.conn.prepareStatement(sSql);
			
			String srcParam = "{{" + paramName + "}}";
			sSql = "SELECT Stg1_Official_Clause_Alt_Id, IFNULL(Content_Html_Processed, Content_Html_FillIns) Content_Html_FillIns, IsEditAndFillIn, Clause_Number\n" +
					"FROM Stg1_Official_Clause_Alt \n" +
					"WHERE Src_Doc_Id = ?\n" +
					"AND Content_Html_FillIns like '%" + srcParam + "%'\n" +
					"AND CONCAT(Clause_Number, ' ', Alt_Number) IN (\n" + LoadEcfr.SQL_FILL_IN_CLAUSE_NUMBER + "\n)";
			ps1 = this.conn.prepareStatement(sSql);
			ps1.setInt(1, this.iEcfrSrcDocId);
			ps1.setInt(2, this.iClauseSrcDocId);
			rs1 = ps1.executeQuery();
			iCount = this.processFillIns(rs1, psInsert, psUpdate, paramName, paramType, paramSize, true);
		}
		finally {
			SqlUtil.closeInstance(psInsert);
			SqlUtil.closeInstance(psUpdate);
			SqlUtil.closeInstance(rs1, ps1, null);
		}
		System.out.println("AdjustEcfrContent.replaceAltFillIns('" + paramName + "'): " + iCount);
	}
	
	private void addManualFillIns() throws SQLException {
		String sSql;
		PreparedStatement ps1 = null;
		try {
			sSql = "UPDATE Stg1_Official_Clause Q\n" +
					"JOIN Stg_Clause_Fill_Ins_Add M ON (Q.Clause_Number = M.Clause_Number)\n" +
					"SET Q.uri = M.URI\n" +
					", Q.Content_Html = M.Content_Html\n" +
					", Q.Content_Html_Fillins = M.Content_Html_Fillins\n" +
					", Q.Content_Html_Processed = M.Content_Html_Processed\n" +
					"WHERE Q.Src_Doc_Id = ?\n" +
					"AND Q.Clause_Number IN (SELECT Clause_Number FROM Stg_Clause_Fill_Ins_Add)" +
					"AND Q.Clause_Number IN (\n" + LoadEcfr.SQL_FILL_IN_CLAUSE_NUMBER + "\n)"
					;
			ps1 = this.conn.prepareStatement(sSql);
			ps1.setInt(1, this.iEcfrSrcDocId);
			ps1.setInt(2, this.iClauseSrcDocId);
			ps1.execute();
		}
		finally {
			SqlUtil.closeInstance(ps1);
		}
		System.out.println("AdjustEcfrContent.addManualFillIns()");
	}
	
	public static void process(Connection conn, Integer iEcfrSrcDocId, Integer iClauseSrcDocId, boolean pbFullEdit) throws SQLException {
		EcfrParseFillins ecfrReplaceFillins = new EcfrParseFillins(conn, iEcfrSrcDocId, iClauseSrcDocId, pbFullEdit);
		ecfrReplaceFillins.run();
		/*
		update Stg1_Official_Clause set Content_Html_Processed = replace(replace(Content_Html_Processed,'″','"'),'□','');
		update Stg1_Official_Clause_Alt set Content_Html_Processed = replace(Content_Html_Processed,'″','"');
		update Stg1_Official_Clause set Content_Html_Processed = replace(replace(Content_Html_Processed,'”','"'),'□','');
		update Stg1_Official_Clause_Alt set Content_Html_Processed = replace(Content_Html_Processed,'“','"');
		
		update Stg1_Official_Clause set Content_Html_Processed = replace(contentHTML,'″','"') where Content_Html_Processed is null;
		update Stg1_Official_Clause_Alt set Content_Html_Processed = replace(contentHTML,'″','"') where Content_Html_Processed is null;
		*/
	}
	
	public static ArrayList<String> getClauseNumbersforFillinType(int clauseSrcDocId, Connection conn) 
			throws SQLException {
		ArrayList<String> result = new ArrayList<String>();
		String sSql = LoadEcfr.SQL_FILL_IN_CLAUSE_NUMBER;
		PreparedStatement ps1 = conn.prepareStatement(sSql);
		ps1.setInt(1, clauseSrcDocId);	
		ResultSet rs1 = ps1.executeQuery();
		result.clear();
		while (rs1.next()) {
			String rClauseName = rs1.getString(1);
			result.add(rClauseName.toUpperCase());
		}
		System.out.println("LoadECFR Fillin clauses count from Excel: "+ result.size());
		return result;
	}
	
	// =====================================================
	public static void main( String[] args )
    {
		CpmConfig cpmConfig = CpmConfig.getInstance();
		Connection conn = null;
		try {
			conn = cpmConfig.getDbConnection();
			
			StgSrcDocumentSet oStgSrcDocumentSet = StgSrcDocumentSet.getLatest(conn);
			int iEfcrSrcDocId = oStgSrcDocumentSet.iEfcrSrcDocId;
			int iClauseSrcDocId = oStgSrcDocumentSet.iClauseSrcDocId;

			//EcfrReplaceFillins.addManualClauses(conn, iEfcrSrcDocId);
			EcfrParseFillins.process(conn, iEfcrSrcDocId, iClauseSrcDocId, true);
			
			System.out.println("iEfcrSrcDocId: " + iEfcrSrcDocId);
			System.out.println("iClauseSrcDocId: " + iClauseSrcDocId);
			System.out.println("Done");
		}
		catch (Exception oError) {
			oError.printStackTrace();
		}
		finally {
			SqlUtil.closeInstance(conn);
		}
    }
}
