package hgs.cpm.ecfr.util;

import java.sql.ResultSet;
import java.sql.SQLException;

import hgs.cpm.db.Stg1RawClauseRecord;

public class Stg1RawClauseEditableRecord {

	public static final String SQL_SELECT
		= "SELECT " + Stg1RawClauseRecord.FIELD_STG1_CLAUSE_ID
		+ ", " + Stg1RawClauseRecord.FIELD_CLAUSE_NUMBER
		+ ", " + Stg1RawClauseRecord.FIELD_EDITABLE
		+ " FROM " + Stg1RawClauseRecord.TABLE_STG1_RAW_CLAUSES
		+ " WHERE " + Stg1RawClauseRecord.FIELD_SRC_DOC_ID + " = ?"
		// + " AND Fill_In IS NOT NULL AND Fill_In LIKE '%PCO%'"
		+ " AND Editable IS NOT NULL AND Editable LIKE 'Y%'"
		+ " ORDER BY 2";
	
	// ========================================================
	private Integer stg1ClauseId;
	private String clauseNumber;
	private String editable;
	private String editableRemarks;
	
	public void read(ResultSet rs1) throws SQLException {
		this.stg1ClauseId = rs1.getInt(Stg1RawClauseRecord.FIELD_STG1_CLAUSE_ID);
		this.clauseNumber = rs1.getString(Stg1RawClauseRecord.FIELD_CLAUSE_NUMBER);
		this.editable = rs1.getString(Stg1RawClauseRecord.FIELD_EDITABLE);

		this.editableRemarks = this.editable.substring(1).trim();
		while (this.editableRemarks.startsWith("\n"))
			this.editableRemarks = this.editableRemarks.substring(1).trim();
		while (this.editableRemarks.endsWith("\n"))
			this.editableRemarks = this.editableRemarks.substring(0, this.editableRemarks.length() - 1).trim();
	}

	// ========================================================
	public Integer getStg1ClauseId() { return stg1ClauseId; }
	public void setStg1ClauseId(Integer stg1ClauseId) { this.stg1ClauseId = stg1ClauseId; }

	public String getClauseNumber() { return clauseNumber; }
	public void setClauseNumber(String clauseNumber) { this.clauseNumber = clauseNumber; }
	
	public String getEditable() { return editable; }
	public void setEditable(String editable) { this.editable = editable; }

	public String getEditableRemarks() { return editableRemarks; }
	public void setEditableRemarks(String editableRemarks) { this.editableRemarks = editableRemarks; }
	
	public boolean isAllEditable() {
		// return "ALL".equals(this.editableRemarks);
		return "ALL".equals(this.editableRemarks) || this.editableRemarks.startsWith("ALL", 0);
	}

}
