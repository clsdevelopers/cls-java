package hgs.cpm.ecfr.util;

import hgs.cpm.db.template.TemplateStg2ClauseFillInRecord;

import java.sql.PreparedStatement;
import java.sql.SQLException;


class EcfrFullEditLogRecord {

	private String processName;
	private String modifiedContent;

	public String getModifiedContent() {
		return modifiedContent;
	}

	public void setModifiedContent(String modifiedContent) {
		this.modifiedContent = modifiedContent;
	}

	public String getProcessName() {
		return processName;
	}

	public void setProcessName(String processName) {
		this.processName = processName;
	}
	
}

public class EcfrProcessFullEdit {

	public static boolean process(Stg1RawClauseEditableRecord oEditableRecord, int iClauseId, String sHtml, String sFillInCode, 
			PreparedStatement psInsert, PreparedStatement psUpdate, 
			TemplateStg2ClauseFillInRecord oFillInRecord) throws SQLException {
		switch (oEditableRecord.getClauseNumber().toUpperCase()) { // CJ-1075 added .toUpperCase() 
		case "52.216-29":
			return process52_216_29(oEditableRecord, iClauseId, sHtml, sFillInCode, psInsert, psUpdate, oFillInRecord);
		case "52.203-14":
			return process52_203_14(oEditableRecord, iClauseId, sHtml, sFillInCode, psInsert, psUpdate, oFillInRecord);
		case "52.207-3":
			return process52_207_3(oEditableRecord, iClauseId, sHtml, sFillInCode, psInsert, psUpdate, oFillInRecord);
		case "52.215-1 ALTERNATE II":
			return process52_215_1_Alt_II(oEditableRecord, iClauseId, sHtml, sFillInCode, psInsert, psUpdate, oFillInRecord);			
		case "52.216-11":
			return process52_216_11(oEditableRecord, iClauseId, sHtml, sFillInCode, psInsert, psUpdate, oFillInRecord);
		case "52.216-12":
			return process52_216_12(oEditableRecord, iClauseId, sHtml, sFillInCode, psInsert, psUpdate, oFillInRecord);
		case "52.216-25":
			return process52_216_25(oEditableRecord, iClauseId, sHtml, sFillInCode, psInsert, psUpdate, oFillInRecord);
		case "52.225-11":
			return process52_225_11(oEditableRecord, iClauseId, sHtml, sFillInCode, psInsert, psUpdate, oFillInRecord);
		case "52.225-11 ALTERNATE I":
			return process52_225_11_Alt1(oEditableRecord, iClauseId, sHtml, sFillInCode, psInsert, psUpdate, oFillInRecord);
		case "52.227-9":
			return process52_227_9(oEditableRecord, iClauseId, sHtml, sFillInCode, psInsert, psUpdate, oFillInRecord);
		case "52.228-15":
			return process52_228_15(oEditableRecord, iClauseId, sHtml, sFillInCode, psInsert, psUpdate, oFillInRecord);
		case "52.232-16":
			return process52_232_16(oEditableRecord, iClauseId, sHtml, sFillInCode, psInsert, psUpdate, oFillInRecord);
		case "52.232-22":
			return process52_232_22(oEditableRecord, iClauseId, sHtml, sFillInCode, psInsert, psUpdate, oFillInRecord);
		case "52.250-3 ALTERNATE II":
			return process52_250_3_AltII(oEditableRecord, iClauseId, sHtml, sFillInCode, psInsert, psUpdate, oFillInRecord);
		case "52.250-4 ALTERNATE II":
			return process52_250_4_AltII(oEditableRecord, iClauseId, sHtml, sFillInCode, psInsert, psUpdate, oFillInRecord);
		case "52.232-7":
			return process52_232_7(oEditableRecord, iClauseId, sHtml, sFillInCode, psInsert, psUpdate, oFillInRecord);
		//case "52.232-8":
		//	return process52_232_8(oEditableRecord, iClauseId, sHtml, sFillInCode, psInsert, psUpdate, oFillInRecord);
		case "52.232-20":
			return process52_232_20(oEditableRecord, iClauseId, sHtml, sFillInCode, psInsert, psUpdate, oFillInRecord);
		case "52.232-25":
			return process52_232_25(oEditableRecord, iClauseId, sHtml, sFillInCode, psInsert, psUpdate, oFillInRecord);
		case "52.232-26":
			return process52_232_26(oEditableRecord, iClauseId, sHtml, sFillInCode, psInsert, psUpdate, oFillInRecord);
		case "52.232-27":
			return process52_232_27(oEditableRecord, iClauseId, sHtml, sFillInCode, psInsert, psUpdate, oFillInRecord);
		case "52.243-3":
			return process52_243_3(oEditableRecord, iClauseId, sHtml, sFillInCode, psInsert, psUpdate, oFillInRecord);
		case "52.247-60":	// PCO - Provision
			return process52_247_60(oEditableRecord, iClauseId, sHtml, sFillInCode, psInsert, psUpdate, oFillInRecord);		
		case "52.248-1":
			return process52_248_1(oEditableRecord, iClauseId, sHtml, sFillInCode, psInsert, psUpdate, oFillInRecord);
		case "252.217-7001":
			return process252_217_7001(oEditableRecord, iClauseId, sHtml, sFillInCode, psInsert, psUpdate, oFillInRecord);
		case "252.217-7027":
			return process252_217_7027(oEditableRecord, iClauseId, sHtml, sFillInCode, psInsert, psUpdate, oFillInRecord);
		case "252.215-7004":
			return process252_215_7004(oEditableRecord, iClauseId, sHtml, sFillInCode, psInsert, psUpdate, oFillInRecord);
		case "252.225-7023":
			return process252_225_7023(oEditableRecord, iClauseId, sHtml, sFillInCode, psInsert, psUpdate, oFillInRecord);
		case "252.239-7014":
			return process252_239_7014(oEditableRecord, iClauseId, sHtml, sFillInCode, psInsert, psUpdate, oFillInRecord);	
		case "252.232-7007":
			return process252_232_7007(oEditableRecord, iClauseId, sHtml, sFillInCode, psInsert, psUpdate, oFillInRecord);	
		case "252.228-7001":
			return process252_228_7001(oEditableRecord, iClauseId, sHtml, sFillInCode, psInsert, psUpdate, oFillInRecord);	
		case "52.228-13":
			return process52_228_13(oEditableRecord, iClauseId, sHtml, sFillInCode, psInsert, psUpdate, oFillInRecord);	
		case "52.211-12":
			return process52_211_12(oEditableRecord, iClauseId, sHtml, sFillInCode, psInsert, psUpdate, oFillInRecord);		
		case "252.237-7017":
			return process252_237_7017(oEditableRecord, iClauseId, sHtml, sFillInCode, psInsert, psUpdate, oFillInRecord);	

		default:
			System.out.println("EcfrProcessFullEdit.process() skipped for (" + oEditableRecord.getClauseNumber() + ") " + oEditableRecord.getEditableRemarks());
			break;
		}
		return false;
	}	

	private static boolean process52_203_14(Stg1RawClauseEditableRecord oEditableRecord, int iClauseId, String sHtml, String sFillInCode, 
			PreparedStatement psInsert, PreparedStatement psUpdate, 
			TemplateStg2ClauseFillInRecord oFillInRecord) throws SQLException {
		
		//String sDefaultData = "5,000,000";
		//String sDefaultContent = "(d) <i>Subcontracts.</i> The Contractor shall include the substance of this clause, including this paragraph (d), in all subcontracts that exceed $5,000,000, except";
		String sDefaultData = "$5.5 million";
		String sDefaultContent = sDefaultData;
		
		String sRemarks = "Dollar Amount in Paragraph (d) per Agency policy";
		String sType = "S";
		// sFillInCode = sFillInCode.replace("memobox", "amountbox");
		//sFillInCode = getNextFillinCode ( sHtml, sFillInCode) ;
		sFillInCode = "subcontract_max_52.203-14";
		int iSize = 50;

		EcfrFullEditLogRecord oEcfrFullEditLogRecord = new EcfrFullEditLogRecord();
		oEcfrFullEditLogRecord.setProcessName("52_203_14");
		
		return processCustomFields( oEditableRecord,  iClauseId,  sHtml,  sFillInCode, psInsert,  psUpdate, oFillInRecord,
				 				sDefaultData,sDefaultContent,sRemarks, sType, iSize, oEcfrFullEditLogRecord) ;
	}
	
	private static boolean process52_207_3(Stg1RawClauseEditableRecord oEditableRecord, int iClauseId, String sHtml, String sFillInCode, 
			PreparedStatement psInsert, PreparedStatement psUpdate, 
			TemplateStg2ClauseFillInRecord oFillInRecord) throws SQLException {
		
		String sDefaultData = "10";
		String sDefaultContent = "(b) Within 10 days after contract award, the Contracting Officer will provide to the Contractor a list of all Government personnel who have been or will be adversely affected or separated as a result of award of this contract.";
		String sRemarks = "Only Paragraph (b) The 10-day period in the clause may be varied by the contracting officer up to a period of 90 days.";
		String sType = "N";
		//sFillInCode = sFillInCode.replace("memobox", "textbox");
		//sFillInCode = getNextFillinCode ( sHtml, sFillInCode) ;
		sFillInCode = "postcontract_days_52.207-3";
		int iSize = 4;
		
		EcfrFullEditLogRecord oEcfrFullEditLogRecord = new EcfrFullEditLogRecord();
		oEcfrFullEditLogRecord.setProcessName("52_207_3");
		
		return processCustomFields( oEditableRecord,  iClauseId,  sHtml,  sFillInCode, psInsert,  psUpdate, oFillInRecord,
				 				sDefaultData,sDefaultContent,sRemarks, sType, iSize, oEcfrFullEditLogRecord) ;
	}
	

	private static boolean process52_215_1_Alt_II(Stg1RawClauseEditableRecord oEditableRecord, int iClauseId, String sHtml, String sFillInCode, 
			PreparedStatement psInsert, PreparedStatement psUpdate, 
			TemplateStg2ClauseFillInRecord oFillInRecord) throws SQLException {
		
		String sDefaultData = "(9) Offerors may submit proposals that depart from stated requirements. Such proposals shall clearly identify why the acceptance of the proposal would be advantageous to the Government. Any deviations from the terms and conditions of the solicitation, as well as the comparative advantage to the Government, shall be clearly identified and explicitly defined. The Government reserves the right to amend the solicitation to allow all offerors an opportunity to submit revised proposals based on the revised requirements.";
		String sDefaultContent = sDefaultData;
		String sRemarks = "ONLY NEW PARAGRAPH (c) (9)";
		String sType = "M";
		sFillInCode = "paragraph_c9_52.215-1-AltII";
		int iSize = 1000;

		EcfrFullEditLogRecord oEcfrFullEditLogRecord = new EcfrFullEditLogRecord();
		oEcfrFullEditLogRecord.setProcessName("52_215_1_Alt_II");
		return processCustomFields( oEditableRecord,  iClauseId,  sHtml,  sFillInCode, psInsert,  psUpdate, oFillInRecord,
				 				sDefaultData,sDefaultContent,sRemarks, sType, iSize, oEcfrFullEditLogRecord) ;
	}
	
	private static boolean process52_216_11(Stg1RawClauseEditableRecord oEditableRecord, int iClauseId, String sHtml, String sFillInCode, 
			PreparedStatement psInsert, PreparedStatement psUpdate, 
			TemplateStg2ClauseFillInRecord oFillInRecord) throws SQLException {
		
		String sDefaultData = "100,000";
		String sDefaultContent = "(b) After payment of 80 percent of the total estimated cost shown in the Schedule, the Contracting Officer may withhold further payment of allowable cost until a reserve is set aside in an amount that the Contracting Officer considers necessary to protect the Government's interest. This reserve shall not exceed one percent of the total estimated cost shown in the Schedule or $100,000, whichever is less.";
		String sRemarks = "(SEE PREAMBLE TO CLAUSE)\nmay be modified by substituting “$10,000” in lieu of “$100,000” as the maximum reserve in paragraph (b) if the Contractor is a nonprofit organization";
		String sType = "$";
		//sFillInCode = sFillInCode.replace("memobox", "amountbox");
		//sFillInCode = getNextFillinCode ( sHtml, sFillInCode) ;
		sFillInCode = "schedule_est_cost_52.216-11";
		int iSize = 12;

		EcfrFullEditLogRecord oEcfrFullEditLogRecord = new EcfrFullEditLogRecord();
		oEcfrFullEditLogRecord.setProcessName("52_216_11");
		return processCustomFields( oEditableRecord,  iClauseId,  sHtml,  sFillInCode, psInsert,  psUpdate, oFillInRecord,
				 				sDefaultData,sDefaultContent,sRemarks, sType, iSize, oEcfrFullEditLogRecord) ;
	}
	
	private static boolean process252_215_7004(Stg1RawClauseEditableRecord oEditableRecord, int iClauseId, String sHtml, String sFillInCode, 
			PreparedStatement psInsert, PreparedStatement psUpdate, 
			TemplateStg2ClauseFillInRecord oFillInRecord) throws SQLException {
		
		String sDefaultData = "150,000";
		String sDefaultContent = "Canadian Commercial Corporation shall obtain and provide the following for modifications that exceed $150,000";
		String sRemarks = "PARAGRAPH (b) ONLY ONLY TO SPECIFY A HIGHER THRESHOLD";
		String sType = "$";
		sFillInCode = "modification_cost_252.215-7004";
		int iSize = 12;
		
		sHtml = sHtml.replace(" [<i>or higher dollar value specified by the U.S. Contracting Officer in the solicitation</i>]", "");
		String sPlaceholder = "or higher dollar value specified by the U.S. Contracting Officer in the solicitation";
		
		sHtml = sHtml.replace("[<i>U.S. Contracting Officer to insert description of the data required in accordance with FAR 15.403-3(a)(1)</i>]", "{{description_252.215-7004}}");
		
		// First insert the regular fill-in
		//oFillInRecord.setFillInDefaultData(null);
		oFillInRecord.setFillInType("S");
		oFillInRecord.setFillInCode("description_252.215-7004");
		oFillInRecord.setFillInMaxSize(250);
		oFillInRecord.setFillInDisplayRows(1);
		oFillInRecord.setFillInPlaceholder("U.S. Contracting Officer to insert description of the data required in accordance with FAR 15.403-3(a)(1)");
		oFillInRecord.insert(psInsert);
		
		
		EcfrFullEditLogRecord oEcfrFullEditLogRecord = new EcfrFullEditLogRecord();
		oEcfrFullEditLogRecord.setProcessName("252_215_7004");
		
		
		return processCustomFieldsAndPlaceHolder
				( oEditableRecord,  iClauseId,  sHtml,  sFillInCode, psInsert,  psUpdate, oFillInRecord,
				 				sDefaultData,sDefaultContent,sRemarks, sType, iSize, sPlaceholder, oEcfrFullEditLogRecord) ;
	}
	private static boolean process52_216_12(Stg1RawClauseEditableRecord oEditableRecord, int iClauseId, String sHtml, String sFillInCode, 
			PreparedStatement psInsert, PreparedStatement psUpdate, 
			TemplateStg2ClauseFillInRecord oFillInRecord) throws SQLException {
		
		String sDefaultData = "100,000";
		String sDefaultContent = "This reserve shall not exceed one percent of the Government's share of the total estimated cost shown in the Schedule or $100,000, whichever is less.";
		String sRemarks = "this clause may be modified by substituting “$10,000” in lieu of “$100,000” as the maximum reserve in paragraph (b) if the contract is with a nonprofit organization.";
		String sType = "$";
		//sFillInCode = sFillInCode.replace("memobox", "amountbox");
		//sFillInCode = getNextFillinCode ( sHtml, sFillInCode) ;
		sFillInCode = "schedule_est_cost_52.216-12";
		int iSize = 12;
		
		EcfrFullEditLogRecord oEcfrFullEditLogRecord = new EcfrFullEditLogRecord();
		oEcfrFullEditLogRecord.setProcessName("52_216_12");
		
		return processCustomFields( oEditableRecord,  iClauseId,  sHtml,  sFillInCode, psInsert,  psUpdate, oFillInRecord,
				 				sDefaultData,sDefaultContent,sRemarks, sType, iSize, oEcfrFullEditLogRecord) ;
	}
	
	private static boolean process52_216_25(Stg1RawClauseEditableRecord oEditableRecord, int iClauseId, String sHtml, String sFillInCode, 
			PreparedStatement psInsert, PreparedStatement psUpdate, 
			TemplateStg2ClauseFillInRecord oFillInRecord) throws SQLException {
		
		String sDefaultContent = "and certified cost or pricing data, in accordance with FAR 15.408, Table 15-2, supporting its proposal";
		String sDefaultData = sDefaultContent;
		String sRemarks = "ONLYTHE FOLLOWING WORDS IN PARAGRAPH (a )MAY BE DELETED - THE WORDS: “and certified cost or pricing data in accordance with FAR 15.408, Table 15-2 supporting its proposal”";
		String sType = "M";
		int iSize = oFillInRecord.getFillInMaxSize();
		//sFillInCode = getNextFillinCode ( sHtml, sFillInCode) ;
		sFillInCode = "cost_or_pricing_52.216-25";
		
		EcfrFullEditLogRecord oEcfrFullEditLogRecord = new EcfrFullEditLogRecord();
		oEcfrFullEditLogRecord.setProcessName("52_216_25");
		
		return processCustomFields( oEditableRecord,  iClauseId,  sHtml,  sFillInCode, psInsert,  psUpdate, oFillInRecord,
				 				sDefaultData,sDefaultContent,sRemarks, sType, iSize, oEcfrFullEditLogRecord) ;
	}
	
	private static boolean process52_225_11(Stg1RawClauseEditableRecord oEditableRecord, int iClauseId, String sHtml, String sFillInCode, 
			PreparedStatement psInsert, PreparedStatement psUpdate, 
			TemplateStg2ClauseFillInRecord oFillInRecord) throws SQLException {
		
		String sDefaultData = "6";
		String sDefaultContent = "(i) The cost of domestic construction material would be unreasonable. The cost of a particular domestic construction material subject to the restrictions of the Buy American statute is unreasonable when the cost of such material exceeds the cost of foreign material by more than 6 percent";
		String sRemarks = "ONLY PARAGRAPH (b)(4)(i) TO SUBSTITUTE A HIGHER PERCENTAGE, IF APPROVED";
		String sType = "N";
		//sFillInCode = sFillInCode.replace("memobox", "textbox");
		//sFillInCode = getNextFillinCode ( sHtml, sFillInCode) ;
		sFillInCode = "material_cost_percent_52.225-11";
		int iSize = 4;
		
		EcfrFullEditLogRecord oEcfrFullEditLogRecord = new EcfrFullEditLogRecord();
		oEcfrFullEditLogRecord.setProcessName("52_225_11");
		
		return processCustomFields( oEditableRecord,  iClauseId,  sHtml,  sFillInCode, psInsert,  psUpdate, oFillInRecord,
				 				sDefaultData,sDefaultContent,sRemarks, sType, iSize, oEcfrFullEditLogRecord) ;
	}
	
	private static boolean process52_225_11_Alt1(Stg1RawClauseEditableRecord oEditableRecord, int iClauseId, String sHtml, String sFillInCode, 
			PreparedStatement psInsert, PreparedStatement psUpdate, 
			TemplateStg2ClauseFillInRecord oFillInRecord) throws SQLException {
		
		String sDefaultData = "6";
		String sDefaultContent = "(i) The cost of domestic construction material would be unreasonable. The cost of a particular domestic construction material subject to the restrictions of the Buy American statute is unreasonable when the cost of such material exceeds the cost of foreign material by more than 6 percent";
		String sRemarks = "ONLY PARAGRAPH (b)(4)(i) TO SUBSTITUTE A HIGHER PERCENTAGE, IF APPROVED";
		String sType = "N";
		//sFillInCode = sFillInCode.replace("memobox", "textbox");
		//sFillInCode = getNextFillinCode ( sHtml, sFillInCode) ;
		sFillInCode = "material_cost_percent_52.225-11_Alt1";
		int iSize = 4;

		EcfrFullEditLogRecord oEcfrFullEditLogRecord = new EcfrFullEditLogRecord();
		oEcfrFullEditLogRecord.setProcessName("52_225_11_Alternate_1");
		
		return processCustomFields( oEditableRecord,  iClauseId,  sHtml,  sFillInCode, psInsert,  psUpdate, oFillInRecord,
				 				sDefaultData,sDefaultContent,sRemarks, sType, iSize, oEcfrFullEditLogRecord) ;
	}
	
	private static boolean process52_228_15(Stg1RawClauseEditableRecord oEditableRecord, int iClauseId, String sHtml, String sFillInCode, 
			PreparedStatement psInsert, PreparedStatement psUpdate, 
			TemplateStg2ClauseFillInRecord oFillInRecord) throws SQLException {
		
		String sDefaultData = "100";
		String sDefaultContent = "The penal amount of performance bonds at the time of contract award shall be 100 percent of the original contract price.";
		String sRemarks = "ONLY PARAGRAPHS (b)(1) AND/OR (b)(2) TO ESTABLISH A LOWER PERCENTAGE. IN ADDITION MUST ALLOW CO TO SET A PERIOD OF TIME FOR RETURN OF EXECUTED BONDS";
		String sType = "N";
		sFillInCode = "performance_bonds_percent_52.228-15";
		int iSize = 4;

		EcfrFullEditLogRecord oEcfrFullEditLogRecord = new EcfrFullEditLogRecord();
		oEcfrFullEditLogRecord.setProcessName("52_228_15");
				
		boolean bOkToGo =  processCustomFields( oEditableRecord,  iClauseId,  sHtml,  sFillInCode, psInsert,  psUpdate, oFillInRecord,
				 				sDefaultData,sDefaultContent,sRemarks, sType, iSize, oEcfrFullEditLogRecord) ;
		sHtml = oEcfrFullEditLogRecord.getModifiedContent();
		if (!bOkToGo) return false;
		
		sDefaultContent = "The penal amount of payment bonds at the time of contract award shall be 100 percent of the original contract price";
		sFillInCode = "payment_bonds_percent_52.228-15";
		
		bOkToGo =  processCustomFields( oEditableRecord,  iClauseId,  sHtml,  sFillInCode, psInsert,  psUpdate, oFillInRecord,
 				sDefaultData,sDefaultContent,sRemarks, sType, iSize, oEcfrFullEditLogRecord) ;
		sHtml = oEcfrFullEditLogRecord.getModifiedContent();
		if (!bOkToGo) return false;
		
		sDefaultData = "<i>Furnishing executed bonds.</i> The Contractor shall furnish all executed bonds, including any necessary reinsurance agreements, to the Contracting Officer, within the time period specified in the Bid Guarantee provision of the solicitation, or otherwise specified by the Contracting Officer, but in any event, before starting work.";
		sDefaultContent = sDefaultData;
		sType = "M";
		iSize = 500;
		sFillInCode = "paragraph_c_52.228-15";
		bOkToGo =  processCustomFields( oEditableRecord,  iClauseId,  sHtml,  sFillInCode, psInsert,  psUpdate, oFillInRecord,
 				sDefaultData,sDefaultContent,sRemarks, sType, iSize, oEcfrFullEditLogRecord) ;
		
		return bOkToGo;

	}
	 
	private static boolean process52_228_13(Stg1RawClauseEditableRecord oEditableRecord, int iClauseId, String sHtml, String sFillInCode, 
			PreparedStatement psInsert, PreparedStatement psUpdate, 
			TemplateStg2ClauseFillInRecord oFillInRecord) throws SQLException {
		
		String sDefaultData = "100";
		String sDefaultContent = "The amount of the payment protection shall be 100 percent of the contract price.";
		String sRemarks = oEditableRecord.getEditableRemarks();
		String sType = "N";
		sFillInCode = "protection_percent_52.228-13";
		int iSize = 4;

		EcfrFullEditLogRecord oEcfrFullEditLogRecord = new EcfrFullEditLogRecord();
		oEcfrFullEditLogRecord.setProcessName("52_228_13");
				
		return  processCustomFields( oEditableRecord,  iClauseId,  sHtml,  sFillInCode, psInsert,  psUpdate, oFillInRecord,
				 				sDefaultData,sDefaultContent,sRemarks, sType, iSize, oEcfrFullEditLogRecord) ;
	}
	 
	// case "52.250-3 ALTERNATE II":
	// process52_250_3_AltII(oEditableRecord, iClauseId, sHtml, sFillInCode, psInsert, psUpdate, oFillInRecord);
	 
	// Action: Find paragraph (f)(2)(i) and replace with memo box
	private static boolean process52_250_3_AltII(Stg1RawClauseEditableRecord oEditableRecord, int iClauseId, String sHtml, String sFillInCode,
		PreparedStatement psInsert, PreparedStatement psUpdate, 
		TemplateStg2ClauseFillInRecord oFillInRecord) throws SQLException {
	
		String sDefaultContent = "(i) Files a SAFETY Act designation (or SAFETY Act certification) application, limited to the scope of the applicable block designation (or block certification), within 15 days after submission of the proposal;";
		String sDefaultData = sDefaultContent;
		String sRemarks = "PARAGRAPH (f)(2)(i) ONLY";
		String sType = "M";
		//sFillInCode = sFillInCode.replace("memobox", "memobox");
		//sFillInCode = getNextFillinCode ( sHtml, sFillInCode) ;
		sFillInCode = "paragraph_f2i_52.250-3_AltII";
		int iSize = 32000;
		
		EcfrFullEditLogRecord oEcfrFullEditLogRecord = new EcfrFullEditLogRecord();
		oEcfrFullEditLogRecord.setProcessName("52_250_3_Alternate_II");
		
		return processCustomFields( oEditableRecord,  iClauseId,  sHtml,  sFillInCode, psInsert,  psUpdate, oFillInRecord, 
				sDefaultData,sDefaultContent,sRemarks, sType, iSize, oEcfrFullEditLogRecord); 
	}
	 
	// Action: Find paragraph (f)(2)(i) and replace with memo box
	private static boolean process52_250_4_AltII(Stg1RawClauseEditableRecord oEditableRecord, int iClauseId, String sHtml, String sFillInCode,
		PreparedStatement psInsert, PreparedStatement psUpdate, 
		TemplateStg2ClauseFillInRecord oFillInRecord) throws SQLException {
		
		String sDefaultContent = "(i) Files a SAFETY Act designation application, limited to the scope of the applicable prequalification designation notice, within 15 days after submission of the proposal;";
		String sDefaultData = sDefaultContent;
		String sRemarks = "PARAGRAPH (f)(2)(i) ONLY";
		String sType = "M";
		//sFillInCode = sFillInCode.replace("memobox", "memobox");
		//sFillInCode = getNextFillinCode ( sHtml, sFillInCode) ;
		sFillInCode = "paragraph_f2i_52.250-4_AltII";
		int iSize = 32000;
 
		EcfrFullEditLogRecord oEcfrFullEditLogRecord = new EcfrFullEditLogRecord();
		oEcfrFullEditLogRecord.setProcessName("52_250_4_Alternate_II");

		return processCustomFields( oEditableRecord,  iClauseId,  sHtml,  sFillInCode, psInsert,  psUpdate, oFillInRecord, 
				sDefaultData,sDefaultContent,sRemarks, sType, iSize, oEcfrFullEditLogRecord); 
	}
	 
	 
	// Action: Find five instances of the word "price" and replace with memo box
	private static boolean process52_227_9(Stg1RawClauseEditableRecord oEditableRecord, int iClauseId, String sHtml, String sFillInCode,
		PreparedStatement psInsert, PreparedStatement psUpdate, 
		TemplateStg2ClauseFillInRecord oFillInRecord) throws SQLException {
		
		String sDefaultData = "price";
		String sDefaultContent = "price";
		String sRemarks = oEditableRecord.getEditableRemarks();
		String sType = "S";
		//sFillInCode = sFillInCode.replace("memobox", "price_placeholder");
		//sFillInCode = getNextFillinCode ( sHtml, sFillInCode) ;
		sFillInCode = "term_placeholder_52.227-9[0]";
		int iSize = 200;
		boolean bOkToGo = true;
		
		EcfrFullEditLogRecord oEcfrFullEditLogRecord = new EcfrFullEditLogRecord();
		oEcfrFullEditLogRecord.setProcessName("52_227_9");
					
		while (bOkToGo && sHtml.contains(sDefaultContent)) {
			sFillInCode = getNextFillinCode ( sHtml, sFillInCode) ;
			bOkToGo = processCustomFields( oEditableRecord,  iClauseId,  sHtml,  sFillInCode, psInsert,  psUpdate, oFillInRecord, 
					sDefaultData,sDefaultContent, sRemarks, sType, iSize, oEcfrFullEditLogRecord); 
			sHtml = oEcfrFullEditLogRecord.getModifiedContent();
		}
		if (!bOkToGo) return false;
		
		return (!sHtml.contains("price"));
			
	}
	 
	// Action: Find 80 and replace with number box
	private static boolean process52_232_16(Stg1RawClauseEditableRecord oEditableRecord, int iClauseId, String sHtml, String sFillInCode,
		PreparedStatement psInsert, PreparedStatement psUpdate, 
		TemplateStg2ClauseFillInRecord oFillInRecord) throws SQLException {
		
		String sDefaultData = "80";
		String sDefaultContent = "(6) The total amount of progress payments shall not exceed 80 percent of the total contract price.";
		String sRemarks = "ONLY PARAGRAPH (a)(1) , (a)(6), AND (b) PROGRESS PAYMENT RATE";
		String sType = "N";
		sFillInCode = "total_contract_percent_52.232-16";
		int iSize = 4;
	
		EcfrFullEditLogRecord oEcfrFullEditLogRecord = new EcfrFullEditLogRecord();
		oEcfrFullEditLogRecord.setProcessName("52_232_16");
		
		boolean bOkToGo =  processCustomFields( oEditableRecord,  iClauseId,  sHtml,  sFillInCode, psInsert,  psUpdate, oFillInRecord, 
				sDefaultData,sDefaultContent,sRemarks, sType, iSize, oEcfrFullEditLogRecord); 
		if (!bOkToGo) return false;
		sHtml = oEcfrFullEditLogRecord.getModifiedContent();
		
		sDefaultContent = "Unless the Contractor requests a smaller amount, the Government will compute each progress payment as 80 percent of the Contractor's total costs incurred under this contract whether or not actually paid";
		sFillInCode = "total_costs_percent_52.232-16";
		
		bOkToGo =  processCustomFields( oEditableRecord,  iClauseId,  sHtml,  sFillInCode, psInsert,  psUpdate, oFillInRecord, 
				sDefaultData,sDefaultContent,sRemarks, sType, iSize, oEcfrFullEditLogRecord); 
		if (!bOkToGo) return false;
		sHtml = oEcfrFullEditLogRecord.getModifiedContent();
		
		sDefaultContent = "the unliquidated progress payments, or 80 percent of the amount invoiced, whichever is less";
		sFillInCode = "amount_invoiced_percent_52.232-16";
		bOkToGo =  processCustomFields( oEditableRecord,  iClauseId,  sHtml,  sFillInCode, psInsert,  psUpdate, oFillInRecord, 
				sDefaultData,sDefaultContent,sRemarks, sType, iSize, oEcfrFullEditLogRecord); 
		
		return bOkToGo;
	}
	
	// Action: Find 60 and replace with number box
	// Find all occurrences of Schedule and replace with input box
	private static boolean process52_232_20(Stg1RawClauseEditableRecord oEditableRecord, int iClauseId, String sHtml, String sFillInCode,
		PreparedStatement psInsert, PreparedStatement psUpdate, 
		TemplateStg2ClauseFillInRecord oFillInRecord) throws SQLException {
		
		String sDefaultData = "60";
		String sDefaultContent = "The costs the Contractor expects to incur under this contract in the next 60 days,";
		String sRemarks = "- SEE CLAUSE PREAMBLE\nThe 60-day period may be varied from 30 to 90 days and the 75 percent from 75 to 85 percent. “Task Order” or other appropriate designation may be substituted for “Schedule” wherever that word appears in the clause.";
		String sType = "N";
		//sFillInCode = sFillInCode.replace("memobox", "textbox");
		//sFillInCode = getNextFillinCode ( sHtml, sFillInCode) ;
		sFillInCode = "next_x_days_52.232-20";
		int iSize = 4;
		
		EcfrFullEditLogRecord oEcfrFullEditLogRecord = new EcfrFullEditLogRecord();
		oEcfrFullEditLogRecord.setProcessName("52_232_20");
		boolean bOkToGo =  processCustomFields( oEditableRecord,  iClauseId,  sHtml,  sFillInCode, psInsert,  psUpdate, oFillInRecord, 
				sDefaultData,sDefaultContent,sRemarks, sType, iSize, oEcfrFullEditLogRecord); 
		if (!bOkToGo) return false;
		sHtml = oEcfrFullEditLogRecord.getModifiedContent();
		
		sDefaultData = "75";
		sDefaultContent = "will exceed 75 percent of the estimated cost specified";
		sFillInCode = "cost_percent_52.232-20";
		oEcfrFullEditLogRecord.setProcessName("52_232_20");
		
		bOkToGo = processCustomFields( oEditableRecord,  iClauseId,  sHtml,  sFillInCode, psInsert,  psUpdate, oFillInRecord, 
				sDefaultData,sDefaultContent,sRemarks, sType, iSize, oEcfrFullEditLogRecord); 
		if (!bOkToGo) return false;
		sHtml = oEcfrFullEditLogRecord.getModifiedContent();
		
		// Replace Schedule strings here.
		sDefaultData = "Schedule";
		sDefaultContent = "Schedule";
		sType = "S";
		iSize = 100;
		sFillInCode = "term_placeholder_52.232-20[0]";

		while (bOkToGo && sHtml.contains(sDefaultContent)) {
			sFillInCode = getNextFillinCode ( sHtml, sFillInCode) ;
			bOkToGo = processCustomFields( oEditableRecord,  iClauseId,  sHtml,  sFillInCode, psInsert,  psUpdate, oFillInRecord, 
					sDefaultData,sDefaultContent,sRemarks, sType, iSize, oEcfrFullEditLogRecord); 
			sHtml = oEcfrFullEditLogRecord.getModifiedContent();
		}
		if (!bOkToGo) return false;
		
		return (!sHtml.contains("Schedule"));
			
	}
	 
	// Action: Find 60 and replace with number box
	// Find Schedule and replace with input box
	private static boolean process52_232_22(Stg1RawClauseEditableRecord oEditableRecord, int iClauseId, String sHtml, String sFillInCode,
		PreparedStatement psInsert, PreparedStatement psUpdate, 
		TemplateStg2ClauseFillInRecord oFillInRecord) throws SQLException {
		
		String sDefaultData = "60";
		String sDefaultContent = "c) The Contractor shall notify the Contracting Officer in writing whenever it has reason to believe that the costs it expects to incur under this contract in the next 60 days";
		String sRemarks = oEditableRecord.getEditableRemarks();
		String sType = "N";
		//sFillInCode = sFillInCode.replace("memobox", "textbox");
		//sFillInCode = getNextFillinCode ( sHtml, sFillInCode) ;
		sFillInCode = "incur_cost_days_52.232-22";
		int iSize = 4;
		
		EcfrFullEditLogRecord oEcfrFullEditLogRecord = new EcfrFullEditLogRecord();
		oEcfrFullEditLogRecord.setProcessName("52_232_22");
		
		boolean bOkToGo = processCustomFields( oEditableRecord,  iClauseId,  sHtml,  sFillInCode, psInsert,  psUpdate, oFillInRecord, 
				sDefaultData,sDefaultContent,sRemarks, sType, iSize, oEcfrFullEditLogRecord); 
		sHtml = oEcfrFullEditLogRecord.getModifiedContent();
		
		sDefaultData = "75";
		sDefaultContent = "will exceed 75 percent";
		sFillInCode = "exceed_percent_52.232-22";
		
		bOkToGo = processCustomFields( oEditableRecord,  iClauseId,  sHtml,  sFillInCode, psInsert,  psUpdate, oFillInRecord, 
				sDefaultData,sDefaultContent,sRemarks, sType, iSize, oEcfrFullEditLogRecord); 
		sHtml = oEcfrFullEditLogRecord.getModifiedContent();
		
		// Replace Schedule strings here.
		sDefaultData = "Schedule";
		sDefaultContent = "Schedule";
		sType = "S";
		iSize = 100;
		sFillInCode = "term_placeholder_52.232-22[0]";
		
		while (bOkToGo && sHtml.contains(sDefaultContent)) {
			sFillInCode = getNextFillinCode ( sHtml, sFillInCode) ;
			bOkToGo = processCustomFields( oEditableRecord,  iClauseId,  sHtml,  sFillInCode, psInsert,  psUpdate, oFillInRecord, 
					sDefaultData,sDefaultContent,sRemarks, sType, iSize, oEcfrFullEditLogRecord); 
			sHtml = oEcfrFullEditLogRecord.getModifiedContent();
		}
		if (!bOkToGo) return false;
		
		return (!sHtml.contains("Schedule"));
	}
	 
	// Action: Find 7th and replace with input box
	// Find 30th and replace with input box
	private static boolean process52_232_25(Stg1RawClauseEditableRecord oEditableRecord, int iClauseId, String sHtml, String sFillInCode,
		PreparedStatement psInsert, PreparedStatement psUpdate, 
		TemplateStg2ClauseFillInRecord oFillInRecord) throws SQLException {
			
		String sDefaultData = "7th";
		String sDefaultContent = "Government acceptance is deemed to occur constructively on the 7th day ";
		String sRemarks = oEditableRecord.getEditableRemarks();
		String sType = "S";
		//sFillInCode = sFillInCode.replace("memobox", "textbox");
		//sFillInCode = getNextFillinCode ( sHtml, sFillInCode) ;
		sFillInCode = "acceptance_days_52.232-25";
		int iSize = 10;
 
		EcfrFullEditLogRecord oEcfrFullEditLogRecord = new EcfrFullEditLogRecord();
		oEcfrFullEditLogRecord.setProcessName("52_232_25");
		
		boolean bOkToGo = processCustomFields( oEditableRecord,  iClauseId,  sHtml,  sFillInCode, psInsert,  psUpdate, oFillInRecord, 
				sDefaultData,sDefaultContent,sRemarks, sType, iSize, oEcfrFullEditLogRecord); 
		if (!bOkToGo) return false;
		
		sDefaultData = "30th";
		sDefaultContent = "The 30th day after the designated billing office receives a proper invoice";
		sHtml = oEcfrFullEditLogRecord.getModifiedContent();
		sFillInCode = "due_days_52.232-25[0]";
		bOkToGo = processCustomFields( oEditableRecord,  iClauseId,  sHtml,  sFillInCode, psInsert,  psUpdate, oFillInRecord, 
				sDefaultData,sDefaultContent,sRemarks, sType, iSize, oEcfrFullEditLogRecord); 
		if (!bOkToGo) return false;
		
		sDefaultData = "30th";
		sDefaultContent = "The 30th day after Government acceptance of supplies delivered or services performed.";
		sHtml = oEcfrFullEditLogRecord.getModifiedContent();
		sFillInCode = "due_days_52.232-25[1]";
		bOkToGo = processCustomFields( oEditableRecord,  iClauseId,  sHtml,  sFillInCode, psInsert,  psUpdate, oFillInRecord, 
				sDefaultData,sDefaultContent,sRemarks, sType, iSize, oEcfrFullEditLogRecord); 
		if (!bOkToGo) return false;
		
		sDefaultData = "30th";
		sDefaultContent = "payment due date is the 30th day after the date of the Contractor";
		sHtml = oEcfrFullEditLogRecord.getModifiedContent();
		//sFillInCode = getNextFillinCode ( sHtml, sFillInCode) ;
		sFillInCode = "due_days_52.232-25[2]";
		
		return processCustomFields( oEditableRecord,  iClauseId,  sHtml,  sFillInCode, psInsert,  psUpdate, oFillInRecord, 
		sDefaultData,sDefaultContent,sRemarks, sType, iSize, oEcfrFullEditLogRecord); 
	}
	 
	// Action: Find 7th and replace with input box
	// Find 30th and replace with input box
	private static boolean process52_232_26(Stg1RawClauseEditableRecord oEditableRecord, int iClauseId, String sHtml, String sFillInCode,
		PreparedStatement psInsert, PreparedStatement psUpdate, 
		TemplateStg2ClauseFillInRecord oFillInRecord) throws SQLException {
		
		String sDefaultData = "7th";
		String sDefaultContent = "Government acceptance is deemed to occur constructively on the 7th day ";
		String sRemarks = oEditableRecord.getEditableRemarks();
		String sType = "S";
		sFillInCode = "acceptance_days_52.232-26[0]";
		int iSize = 10;
 
		EcfrFullEditLogRecord oEcfrFullEditLogRecord = new EcfrFullEditLogRecord();
		oEcfrFullEditLogRecord.setProcessName("52_232_26");
		
		boolean bOkToGo = processCustomFields( oEditableRecord,  iClauseId,  sHtml,  sFillInCode, psInsert,  psUpdate, oFillInRecord, 
				sDefaultData,sDefaultContent,sRemarks, sType, iSize, oEcfrFullEditLogRecord); 
		if (!bOkToGo) return false;
		
		sDefaultData = "7th";
		sDefaultContent = "Government approval is deemed to occur on the 7th day after the designated billing office";
		sHtml = oEcfrFullEditLogRecord.getModifiedContent();
		sFillInCode = "acceptance_days_52.232-26[1]";
		
		bOkToGo = processCustomFields( oEditableRecord,  iClauseId,  sHtml,  sFillInCode, psInsert,  psUpdate, oFillInRecord, 
		sDefaultData,sDefaultContent,sRemarks, sType, iSize, oEcfrFullEditLogRecord); 

		
		//sDefaultData = "30th";
		//sDefaultContent = "payment due date is the 30th day after the date of the Contractor";
		//sHtml = oEcfrFullEditLogRecord.getModifiedContent();
		//sFillInCode = "due_days_52.232-26[0]";
		
		//bOkToGo = processCustomFields( oEditableRecord,  iClauseId,  sHtml,  sFillInCode, psInsert,  psUpdate, oFillInRecord, 
		//sDefaultData,sDefaultContent,sRemarks, sType, iSize, oEcfrFullEditLogRecord); 
		
		sDefaultData = "30th";
		sDefaultContent = "The 30th day after the designated billing office receives a proper invoice from the Contractor";
		sHtml = oEcfrFullEditLogRecord.getModifiedContent();
		//sFillInCode = getNextFillinCode ( sHtml, sFillInCode) ;
		sFillInCode = "due_days_52.232-26[1]";
		
		bOkToGo = processCustomFields( oEditableRecord,  iClauseId,  sHtml,  sFillInCode, psInsert,  psUpdate, oFillInRecord, 
		sDefaultData,sDefaultContent,sRemarks, sType, iSize, oEcfrFullEditLogRecord); 
		
		sDefaultData = "30th";
		sDefaultContent = "The 30th day after Government acceptance of the work or services completed by the Contractor.";
		sHtml = oEcfrFullEditLogRecord.getModifiedContent();
		//sFillInCode = getNextFillinCode ( sHtml, sFillInCode) ;
		sFillInCode = "due_days_52.232-26[2]";
		
		bOkToGo = processCustomFields( oEditableRecord,  iClauseId,  sHtml,  sFillInCode, psInsert,  psUpdate, oFillInRecord, 
		sDefaultData,sDefaultContent,sRemarks, sType, iSize, oEcfrFullEditLogRecord); 
		
		sDefaultData = "30th";
		sDefaultContent = "The due date for progress payments is the 30th day after Government approval of Contractor estimates of work or services accomplished.";
		sHtml = oEcfrFullEditLogRecord.getModifiedContent();
		//sFillInCode = getNextFillinCode ( sHtml, sFillInCode) ;
		sFillInCode = "due_days_52.232-26[3]";
		
		bOkToGo = processCustomFields( oEditableRecord,  iClauseId,  sHtml,  sFillInCode, psInsert,  psUpdate, oFillInRecord, 
		sDefaultData,sDefaultContent,sRemarks, sType, iSize, oEcfrFullEditLogRecord); 
		
		return bOkToGo;
	}
	 	 	 
	// Action: Find 7th and replace with input box
	// Find 30th and replace with input box
	private static boolean process52_232_27(Stg1RawClauseEditableRecord oEditableRecord, int iClauseId, String sHtml, String sFillInCode,
		PreparedStatement psInsert, PreparedStatement psUpdate, 
		TemplateStg2ClauseFillInRecord oFillInRecord) throws SQLException {
		
		String sDefaultData = "7th";
		String sDefaultContent = "(a)(1)(ii) of this clause, Government acceptance or approval is deemed to occur constructively on the 7th day ";
		String sRemarks = oEditableRecord.getEditableRemarks();
		String sType = "S";
		//sFillInCode = sFillInCode.replace("memobox", "textbox");
		//sFillInCode = getNextFillinCode ( sHtml, sFillInCode) ;
		sFillInCode = "approval_day_52.232-27";
		int iSize = 10;
 
		EcfrFullEditLogRecord oEcfrFullEditLogRecord = new EcfrFullEditLogRecord();
		oEcfrFullEditLogRecord.setProcessName("52_232_27");
		
		boolean bOkToGo = processCustomFields( oEditableRecord,  iClauseId,  sHtml,  sFillInCode, psInsert,  psUpdate, oFillInRecord, 
				sDefaultData,sDefaultContent,sRemarks, sType, iSize, oEcfrFullEditLogRecord); 
		if (!bOkToGo) return false;
		
		sType = "N";
		iSize = 4;
		sDefaultData = "14";
		sDefaultContent = "(A) The due date for making such payments is 14 days after the designated billing";
		sHtml = oEcfrFullEditLogRecord.getModifiedContent();
		//sFillInCode = getNextFillinCode ( sHtml, sFillInCode) ;
		sFillInCode = "due_days_52.232-27";
		
		bOkToGo = processCustomFields( oEditableRecord,  iClauseId,  sHtml,  sFillInCode, psInsert,  psUpdate, oFillInRecord, 
		sDefaultData,sDefaultContent,sRemarks, sType, iSize, oEcfrFullEditLogRecord); 
		if (!bOkToGo) return false;
		
		sType = "S";
		iSize = 10;
		sDefaultData = "14th";
		sDefaultContent = "the payment due date is the 14th day after the date of the Contractor";
		sHtml = oEcfrFullEditLogRecord.getModifiedContent();
		//sFillInCode = getNextFillinCode ( sHtml, sFillInCode) ;
		sFillInCode = "payment_days_52.232-27";
		
		return processCustomFields( oEditableRecord,  iClauseId,  sHtml,  sFillInCode, psInsert,  psUpdate, oFillInRecord, 
				sDefaultData,sDefaultContent,sRemarks, sType, iSize, oEcfrFullEditLogRecord); 

	}
		
	// Action: Find paragraph (a)(7) and replace with memo box
	private static boolean process52_232_7(Stg1RawClauseEditableRecord oEditableRecord, int iClauseId, String sHtml, String sFillInCode,
		PreparedStatement psInsert, PreparedStatement psUpdate, 
		TemplateStg2ClauseFillInRecord oFillInRecord) throws SQLException {

		String sDefaultContent = "(7) Unless otherwise prescribed in the Schedule, the Contracting Officer may unilaterally issue a contract modification requiring the Contractor to withhold amounts from its billings until a reserve is set aside in an amount that the Contracting Officer considers necessary to protect the Government's interests. The Contracting Officer may require a withhold of 5 percent of the amounts due under paragraph (a) of this clause, but the total amount withheld for the contract shall not exceed $50,000. The amounts withheld shall be retained until the Contractor executes and delivers the release required by paragraph (g) of this clause.";
		String sDefaultData = sDefaultContent;
		String sRemarks = "ONLY PARAGRAPH (a)(7)";
		String sType = "M";
		//sFillInCode = sFillInCode.replace("memobox", "memobox");
		//sFillInCode = getNextFillinCode ( sHtml, sFillInCode) ;
		sFillInCode = "paragraph_a7_52.232-7";
		int iSize = 5000;
 
		EcfrFullEditLogRecord oEcfrFullEditLogRecord = new EcfrFullEditLogRecord();
		oEcfrFullEditLogRecord.setProcessName("52_232_7");
		
		return processCustomFields( oEditableRecord,  iClauseId,  sHtml,  sFillInCode, psInsert,  psUpdate, oFillInRecord, 
		sDefaultData,sDefaultContent,sRemarks, sType, iSize, oEcfrFullEditLogRecord); 
	}

	/*
	// Action: Saturday, Sunday, or legal holiday when Federal Government offices are closed and Government business is not expected to be conducted, payment may be made on the following business day.
	private static boolean process52_232_8(Stg1RawClauseEditableRecord oEditableRecord, int iClauseId, String sHtml, String sFillInCode,
		PreparedStatement psInsert, PreparedStatement psUpdate, 
		TemplateStg2ClauseFillInRecord oFillInRecord) throws SQLException {
		
		String sDefaultData = "Saturday, Sunday, or legal holiday when Federal Government offices are closed and Government business is not expected to be conducted, payment may be made on the following business day.";
		String sDefaultContent = "(b) In connection with any discount offered for prompt payment, time shall be computed from the date of the invoice. If the Contractor has not placed a date on the invoice, the due date shall be calculated from the date the designated billing office receives a proper invoice, provided the agency annotates such invoice with the date of receipt at the time of receipt. For the purpose of computing the discount earned, payment shall be considered to have been made on the date that appears on the payment check or, for an electronic funds transfer, the specified payment date. When the discount date falls on a Saturday, Sunday, or legal holiday when Federal Government offices are closed and Government business is not expected to be conducted, payment may be made on the following business day.";
		String sRemarks = "PARAGRAPH (b): PAYMENT DUE DATES ONLY";
		String sType = "M";
		//sFillInCode = sFillInCode.replace("memobox", "memobox");
		//sFillInCode = getNextFillinCode ( sHtml, sFillInCode) ;
		sFillInCode = "days_closed_52.232-8";
		int iSize = 5000;
 
		EcfrFullEditLogRecord oEcfrFullEditLogRecord = new EcfrFullEditLogRecord();
		oEcfrFullEditLogRecord.setProcessName("52_232_8");
		
		return processCustomFields( oEditableRecord,  iClauseId,  sHtml,  sFillInCode, psInsert,  psUpdate, oFillInRecord, 
		sDefaultData,sDefaultContent,sRemarks, sType, iSize, oEcfrFullEditLogRecord); 
	}
	*/
	   
	// Action: Find 30 and replace with number box
	private static boolean process52_243_3(Stg1RawClauseEditableRecord oEditableRecord, int iClauseId, String sHtml, String sFillInCode,
		PreparedStatement psInsert, PreparedStatement psUpdate, 
		TemplateStg2ClauseFillInRecord oFillInRecord) throws SQLException {
		
		String sDefaultData = "30";
		String sDefaultContent = "The Contractor shall assert its right to an adjustment under this clause within 30 days from the date of receipt of the written order.";
		String sRemarks = "MAY VARY THE 30 DAY PERIOD IN PARAGRAPH (c)";
		String sType = "N";
		//sFillInCode = sFillInCode.replace("memobox", "textbox");
		sFillInCode = "adjustment_period_52.243-3";
		int iSize = 4;
 
		EcfrFullEditLogRecord oEcfrFullEditLogRecord = new EcfrFullEditLogRecord();
		oEcfrFullEditLogRecord.setProcessName("52_243_3");
		
		return processCustomFields( oEditableRecord,  iClauseId,  sHtml,  sFillInCode, psInsert,  psUpdate, oFillInRecord, 
		sDefaultData,sDefaultContent,sRemarks, sType, iSize, oEcfrFullEditLogRecord); 
	}
	 
	// case "52.247-60":
	// process52_247_60(oEditableRecord, iClauseId, sHtml, sFillInCode, psInsert, psUpdate, oFillInRecord);
	 
	// Action: Locate paragraph (a) and replace entire section with memo box
	
	// CJ-475, must be completed first.
	
	
	private static boolean process52_247_60(Stg1RawClauseEditableRecord oEditableRecord, int iClauseId, String sHtml, String sFillInCode,
		PreparedStatement psInsert, PreparedStatement psUpdate, 
		TemplateStg2ClauseFillInRecord oFillInRecord) throws SQLException {
		
		String sDefaultContent = "The offeror is requested to complete subparagraph (a)(1) of this clause, for each part or component which is packed or packaged separately. This information will be used to determine transportation costs for evaluation purposes. If the offeror does not furnish sufficient data in subparagraph (a)(1) of this clause, to permit determination by the Government of the item shipping costs, evaluation will be based on the shipping characteristics submitted by the offeror whose offer produces the highest transportation costs or in the absence thereof, by the Contracting Officer's best estimate of the actual transportation costs. If the item shipping costs, based on the actual shipping characteristics, exceed the item shipping costs used for evaluation purposes, the Contractor agrees that the contract price shall be reduced by an amount equal to the difference between the transportation costs actually incurred, and the costs which would have been incurred if the evaluated shipping characteristics had been accurate.</p><p>(1) To be completed by the offeror:</p><p>(i) Type of container: Wood Box {{textbox}}, Fiber Box {{textbox}}, Barrel {{textbox}}, Reel {{textbox}}, Drum {{textbox}}, Other (Specify) {{textbox}};</p><p>(ii) Shipping configuration: Knocked-down {{textbox}}, Set-up {{textbox}}, Nested {{textbox}}, Other (specify) {{textbox}};</p><p>(iii) Size of container {{textbox}}&Prime; (Length), &times; {{textbox}}&Prime; (Width), &times; {{textbox}}&Prime; (Height) = {{textbox}} Cubic FT;</p><p>(iv) Number of items per container {{textbox}} Each;</p><p>(v) Gross weight of container and contents{{textbox}} LBS</p><p>(vi) Palletized/skidded {{checkbox}} Yes {{checkbox}} No;</p><p>(vii) Number of containers per pallet/skid {{textbox}};</p><p>(viii) Weight of empty pallet bottom/skid and sides{{textbox}} LBS;</p><p>(ix) Size of pallet/skid and contents {{textbox}} LBS Cube {{textbox}};</p><p>(x) Number of containers or pallets/skid per railcar {{textbox}}*&mdash;</p><div><p>*Number of complete units (contract line item) to be shipped in carrier's equipment.</p></div><p>Size of railcar {{textbox}}</p><p>Type of railcar {{textbox}}</p><p>(xi) Number of containers or pallets/skids per trailer {{textbox}}*&mdash;</p><p>Size of trailer {{textbox}} FT</p><p>Type of trailer {{textbox}}</p><p>(2) To be completed by the Government after evaluation but before contract award:</p><p>(i) Rate used in evaluation {{textbox}};</p><p>(ii) Tender/Tariff {{textbox}};</p><p>(iii) Item {{textbox}};";
		String sDefaultTmp = sDefaultContent;
		
		String sType = "S";
		sFillInCode = "text_placeholder_52.247-60";
		int iSize = 100;
		int iCount = 0;
		while (sDefaultContent.contains("{{textbox}}")) {
			sFillInCode = "text_placeholder_52.247-60[" + iCount + "]";
			sDefaultContent = sDefaultContent.replaceFirst("\\{textbox\\}", "{" + sFillInCode + "}");
			
			System.out.println("Replace with " + sFillInCode);
			
			// insert first textbox
			oFillInRecord.setFillInType(sType);
			oFillInRecord.setFillInCode(sFillInCode);
			oFillInRecord.setFillInMaxSize(iSize);
			oFillInRecord.setFillInDisplayRows(null);
			oFillInRecord.insert(psInsert);
			
			iCount++;
		}
		
		while (sDefaultContent.contains("{{checkbox}}")) {
			sFillInCode = "check_placeholder_52.247-60[" + iCount + "]";
			sDefaultContent = sDefaultContent.replaceFirst("\\{checkbox\\}", "{" + sFillInCode + "}");
			
			System.out.println("Replace with " + sFillInCode);
			
			// insert first textbox
			oFillInRecord.setFillInType("C");
			oFillInRecord.setFillInCode(sFillInCode);
			//oFillInRecord.setFillInMaxSize(iSize);
			oFillInRecord.setFillInDisplayRows(null);
			oFillInRecord.insert(psInsert);
			
			iCount++;
		}
		
		// CJ-838, User unable to save or finalize interview when inch quotes included
		sDefaultContent = sDefaultContent.replace("&Prime;", " inch(es) ");
		sDefaultContent = sDefaultContent.replace("″", " inch(es) ");
		sDefaultContent = sDefaultContent.replace("&times;", "x");
		
		sHtml = sHtml.replace(sDefaultTmp, sDefaultContent);
		
		String sDefaultData = sDefaultContent;
		String sRemarks = oEditableRecord.getEditableRemarks(); //"PARENT\nPARAGRAPH (a) ONLY";
		sType = "M";
		//sFillInCode = sFillInCode.replace("memobox", "memobox");
		sFillInCode = "paragraph_a_52.247-60";
		iSize = 5000;
 
		EcfrFullEditLogRecord oEcfrFullEditLogRecord = new EcfrFullEditLogRecord();
		oEcfrFullEditLogRecord.setProcessName("52_247_60");
		
		return processCustomFields( oEditableRecord,  iClauseId,  sHtml,  sFillInCode, psInsert,  psUpdate, oFillInRecord, 
				sDefaultData,sDefaultContent,sRemarks, sType, iSize, oEcfrFullEditLogRecord);
	}
	
	
	 
	// Action: Find (i)(3)(i) and replace with memo box
	// Find (b) and replace with memo box
	// Find 50 in table and replace with number input box
	// Find 25 in table and replace with number input box
	// Find 15 in table and replace with number input box
	private static boolean process52_248_1(Stg1RawClauseEditableRecord oEditableRecord, int iClauseId, String sHtml, String sFillInCode,
		PreparedStatement psInsert, PreparedStatement psUpdate, 
		TemplateStg2ClauseFillInRecord oFillInRecord) throws SQLException {
		
		String sRemarks = oEditableRecord.getEditableRemarks();
		String sDefaultContent = "multiplying the future unit cost reduction by the number of future contract units scheduled for delivery during the sharing period";
		String sDefaultData = sDefaultContent;
		String sType = "M";
		//sFillInCode = sFillInCode.replace("memobox", "memobox");
		//sFillInCode = getNextFillinCode ( sHtml, sFillInCode) ;
		sFillInCode = "paragraph_i3i_52.248-1";
		int iSize = 500;
 
		EcfrFullEditLogRecord oEcfrFullEditLogRecord = new EcfrFullEditLogRecord();
		oEcfrFullEditLogRecord.setProcessName("52_248_1");
		
		boolean bOkToGo = processCustomFields( oEditableRecord,  iClauseId,  sHtml,  sFillInCode, psInsert,  psUpdate, oFillInRecord, 
				sDefaultData,sDefaultContent,sRemarks, sType, iSize, oEcfrFullEditLogRecord); 
		if (!bOkToGo) return false;
		sHtml = oEcfrFullEditLogRecord.getModifiedContent();
		
		// Find (b) and replace with memo box
		sHtml = sHtml.replace("&mdash;", "-");
		//sDefaultContent = "(3) Future contract savings, which are the product of the future unit cost reduction multiplied by the number of future contract units in the sharing base. On an instant contract, future contract savings include savings on increases in quantities after VECP acceptance that are due to contract modifications, exercise of options, additional orders, and funding of subsequent year requirements on a multiyear contract.";
		sDefaultContent = "(3) Future contract savings, which are the product of the future unit cost reduction multiplied by the number of future contract units in the sharing base.";
		sDefaultData = sDefaultContent;
		sFillInCode = "paragraph_a3_52.248-1";
		iSize = 750;
 
		bOkToGo = processCustomFields( oEditableRecord,  iClauseId,  sHtml,  sFillInCode, psInsert,  psUpdate, oFillInRecord, 
				sDefaultData,sDefaultContent,sRemarks, sType, iSize, oEcfrFullEditLogRecord); 
		if (!bOkToGo) return false;
		sHtml = oEcfrFullEditLogRecord.getModifiedContent();

		//sDefaultContent = "considers necessary for projected learning or changes in quantity during the sharing period";
		//sDefaultData = "during the sharing period";
		//sType = "S";
		//sFillInCode = "sharing_period_52.248-1[0]";
		//iSize = 100;
		
		//bOkToGo = processCustomFields( oEditableRecord,  iClauseId,  sHtml,  sFillInCode, psInsert,  psUpdate, oFillInRecord, 
		//		sDefaultData,sDefaultContent,sRemarks, sType, iSize, oEcfrFullEditLogRecord); 
		//if (!bOkToGo) return false;
		//sHtml = oEcfrFullEditLogRecord.getModifiedContent();
		
		// Find 50 in table and replace with number input box
		sDefaultContent = "</sup>50</td>" ;
		sDefaultData = "50";
		sType = "N";
		// sFillInCode = sFillInCode.replace("memobox", "textbox");
		sFillInCode = "number_5_0_placeholder_52.248-1[0]";
		iSize = 4;

		while (bOkToGo && sHtml.contains(sDefaultContent)) {
			sFillInCode = getNextFillinCode ( sHtml, sFillInCode) ;
			bOkToGo = processCustomFields( oEditableRecord,  iClauseId,  sHtml,  sFillInCode, psInsert,  psUpdate, oFillInRecord, 
					sDefaultData,sDefaultContent,sRemarks, sType, iSize, oEcfrFullEditLogRecord); 
			sHtml = oEcfrFullEditLogRecord.getModifiedContent();
		}
		if (!bOkToGo) return false;
		
		// Find 25 in table and replace with number input box
		sDefaultContent = ">25</td>" ;
		sDefaultData = "25";
		sFillInCode = "number_2_5_placeholder_52.248-1[0]";

		while (bOkToGo && sHtml.contains(sDefaultContent)) {
			sFillInCode = getNextFillinCode ( sHtml, sFillInCode) ;
			bOkToGo = processCustomFields( oEditableRecord,  iClauseId,  sHtml,  sFillInCode, psInsert,  psUpdate, oFillInRecord, 
					sDefaultData,sDefaultContent,sRemarks, sType, iSize, oEcfrFullEditLogRecord); 
			sHtml = oEcfrFullEditLogRecord.getModifiedContent();
		}
		if (!bOkToGo) return false;
		
		// Find 15 in table and replace with number input box
		sDefaultContent = ">15</td>" ;
		sDefaultData = "15";
		sFillInCode = "number_1_5_placeholder_52.248-1[0]";
		
		while (bOkToGo && sHtml.contains(sDefaultContent)) {
			sFillInCode = getNextFillinCode ( sHtml, sFillInCode) ;
			bOkToGo = processCustomFields( oEditableRecord,  iClauseId,  sHtml,  sFillInCode, psInsert,  psUpdate, oFillInRecord, 
					sDefaultData,sDefaultContent,sRemarks, sType, iSize, oEcfrFullEditLogRecord); 
			sHtml = oEcfrFullEditLogRecord.getModifiedContent();
		}
		if (!bOkToGo) return false;
		
		return bOkToGo;
	}
 
	/*
	// Bug... first we need to remove the fillins for this clause.
	
	// Action: Find 150,000 and replace with amount input box
	
	private static boolean process252_215_7004(Stg1RawClauseEditableRecord oEditableRecord, int iClauseId, String sHtml, String sFillInCode, 
			PreparedStatement psInsert, PreparedStatement psUpdate, 
			TemplateStg2ClauseFillInRecord oFillInRecord) throws SQLException {
		
		String sDefaultData = "150,000";
		String sDefaultContent = "(b) Canadian Commercial Corporation shall obtain and provide the following for modifications that exceed $150,000 ";
		String sRemarks = oEditableRecord.getEditableRemarks();
		String sType = "$";
		sFillInCode = sFillInCode.replace("memobox", "amountbox");
		sFillInCode = getNextFillinCode ( sHtml, sFillInCode) ;
		int iSize = 10;

		EcfrFullEditLogRecord oEcfrFullEditLogRecord = new EcfrFullEditLogRecord();
		oEcfrFullEditLogRecord.setProcessName("252_215_7004");
		
		return processCustomFields( oEditableRecord,  iClauseId,  sHtml,  sFillInCode, psInsert,  psUpdate, oFillInRecord,
				 				sDefaultData,sDefaultContent,sRemarks, sType, iSize, oEcfrFullEditLogRecord) ;
	}
	*/
	
	// 252.217-7001 Surge Option.
	
	// Action: Find 30 and replace with input box
	// Find 24 and replace with number input box
	private static boolean process252_217_7001(Stg1RawClauseEditableRecord oEditableRecord, int iClauseId, String sHtml, String sFillInCode,
		PreparedStatement psInsert, PreparedStatement psUpdate, 
		TemplateStg2ClauseFillInRecord oFillInRecord) throws SQLException {
		
		String sDefaultData = "30";
		String sDefaultContent = "(2) If there is no Production Surge Plan in the contract, the Contractor shall, within 30 days from the date of award, furnish the Contracting Officer a delivery schedule showing the maximum sustainable rate of delivery for items in this contract. This delivery schedule shall provide acceleration by month up to the maximum sustainable rate of delivery achievable within the Contractor's existing facilities, equipment, and subcontracting structure.";
		String sRemarks = oEditableRecord.getEditableRemarks();
		String sType = "N";
		//sFillInCode = sFillInCode.replace("memobox", "textbox");
		//sFillInCode = getNextFillinCode ( sHtml, sFillInCode) ;
		sFillInCode = "award_date_days_252.217-7001";
		int iSize = 4;
 
		EcfrFullEditLogRecord oEcfrFullEditLogRecord = new EcfrFullEditLogRecord();
		oEcfrFullEditLogRecord.setProcessName("252_217_7001");
		
		boolean bOkToGo = processCustomFields( oEditableRecord,  iClauseId,  sHtml,  sFillInCode, psInsert,  psUpdate, oFillInRecord, 
				sDefaultData,sDefaultContent,sRemarks, sType, iSize, oEcfrFullEditLogRecord); 
		if (!bOkToGo) return false;
		
		sDefaultData = "30";
		sDefaultContent = "(1) Unless the option cost or price was previously agreed upon, the Contractor shall, within 30 days from the date of option exercise, submit to the Contracting Officer a cost or price proposal (including a cost breakdown) for the added or accelerated items.";
		sHtml = oEcfrFullEditLogRecord.getModifiedContent();
		//sFillInCode = getNextFillinCode ( sHtml, sFillInCode) ;
		sFillInCode = "option_exercise_days_252.217-7001";
		
		bOkToGo = processCustomFields( oEditableRecord,  iClauseId,  sHtml,  sFillInCode, psInsert,  psUpdate, oFillInRecord, 
		sDefaultData,sDefaultContent,sRemarks, sType, iSize, oEcfrFullEditLogRecord); 
		if (!bOkToGo) return false;
		
		sDefaultData = "24";
		sDefaultContent = "of this clause, nor will the exercise of this option extend delivery more than 24 months beyond the scheduled final delivery.";
		sHtml = oEcfrFullEditLogRecord.getModifiedContent();
		//sFillInCode = getNextFillinCode ( sHtml, sFillInCode) ;
		sFillInCode = "extend_delivery_months_252.217-7001";
		
		return processCustomFields( oEditableRecord,  iClauseId,  sHtml,  sFillInCode, psInsert,  psUpdate, oFillInRecord, 
				sDefaultData,sDefaultContent,sRemarks, sType, iSize, oEcfrFullEditLogRecord); 

	}
		
	// Action: Find "and certified cost or pricing data" and replace with memo box
	private static boolean process252_217_7027(Stg1RawClauseEditableRecord oEditableRecord, int iClauseId, String sHtml, String sFillInCode, 
			PreparedStatement psInsert, PreparedStatement psUpdate, 
			TemplateStg2ClauseFillInRecord oFillInRecord) throws SQLException {
		
		String sDefaultData = "and certified cost or pricing data";
		String sDefaultContent = "proposal and certified cost or pricing data supporting its proposal";
		String sRemarks = oEditableRecord.getEditableRemarks();
		String sType = "S";
		//sFillInCode = sFillInCode.replace("memobox", "textbox");
		//sFillInCode = getNextFillinCode ( sHtml, sFillInCode) ;
		sFillInCode = "cost_or_price_252.217-7027";
		int iSize = 300;

		EcfrFullEditLogRecord oEcfrFullEditLogRecord = new EcfrFullEditLogRecord();
		oEcfrFullEditLogRecord.setProcessName("252_217_7027");
		
		return processCustomFields( oEditableRecord,  iClauseId,  sHtml,  sFillInCode, psInsert,  psUpdate, oFillInRecord,
				 				sDefaultData,sDefaultContent,sRemarks, sType, iSize, oEcfrFullEditLogRecord) ;
	}
	
	
	// Action: Find 50 and replace with number input box
	private static boolean process252_225_7023(Stg1RawClauseEditableRecord oEditableRecord, int iClauseId, String sHtml, String sFillInCode,
		PreparedStatement psInsert, PreparedStatement psUpdate, 
		TemplateStg2ClauseFillInRecord oFillInRecord) throws SQLException {
		
		String sDefaultData = "50";
		String sDefaultContent = "(d) <i>Evaluation.</i> For the purpose of evaluating competitive offers, the Contracting Officer will increase by 50 percent the prices of offers of products or services that are not products or services from Afghanistan.";
		String sRemarks = oEditableRecord.getEditableRemarks();
		String sType = "N";
		//sFillInCode = sFillInCode.replace("memobox", "textbox");
		//sFillInCode = getNextFillinCode ( sHtml, sFillInCode) ;
		sFillInCode = "percent_increase_252.225-7023";
		int iSize = 4;
 
		EcfrFullEditLogRecord oEcfrFullEditLogRecord = new EcfrFullEditLogRecord();
		oEcfrFullEditLogRecord.setProcessName("252_225_7023");
		
		return processCustomFields( oEditableRecord,  iClauseId,  sHtml,  sFillInCode, psInsert,  psUpdate, oFillInRecord, 
				sDefaultData,sDefaultContent,sRemarks, sType, iSize, oEcfrFullEditLogRecord); 

	}
	
	// Action: Find 50 and replace with number input box
	private static boolean process252_239_7014(Stg1RawClauseEditableRecord oEditableRecord, int iClauseId, String sHtml, String sFillInCode,
		PreparedStatement psInsert, PreparedStatement psUpdate, 
		TemplateStg2ClauseFillInRecord oFillInRecord) throws SQLException {
		
		String sDefaultData = "60";
		String sDefaultContent = "unless terminated by either party by 60 days written notice";
		String sRemarks = oEditableRecord.getEditableRemarks();
		String sType = "N";
		//sFillInCode = sFillInCode.replace("memobox", "textbox");
		//sFillInCode = getNextFillinCode ( sHtml, sFillInCode) ;
		sFillInCode = "num_of_days_252.239-7014";
		int iSize = 4;
 
		EcfrFullEditLogRecord oEcfrFullEditLogRecord = new EcfrFullEditLogRecord();
		oEcfrFullEditLogRecord.setProcessName("252_239_7014");
		
		return processCustomFields( oEditableRecord,  iClauseId,  sHtml,  sFillInCode, psInsert,  psUpdate, oFillInRecord, 
				sDefaultData,sDefaultContent,sRemarks, sType, iSize, oEcfrFullEditLogRecord); 

	}
	
	// Action: Find ninety and replace with input box
	private static boolean process252_232_7007(Stg1RawClauseEditableRecord oEditableRecord, int iClauseId, String sHtml, String sFillInCode,
		PreparedStatement psInsert, PreparedStatement psUpdate, 
		TemplateStg2ClauseFillInRecord oFillInRecord) throws SQLException {
		
		String sDefaultData = "ninety";
		String sDefaultContent = "ninety";
		String sRemarks = oEditableRecord.getEditableRemarks();
		String sType = "S";
		//sFillInCode = sFillInCode.replace("memobox", "textbox");
		//sFillInCode = getNextFillinCode ( sHtml, sFillInCode) ;
		sFillInCode = "num_of_days_252.232-7007";
		int iSize = 50;
 
		EcfrFullEditLogRecord oEcfrFullEditLogRecord = new EcfrFullEditLogRecord();
		oEcfrFullEditLogRecord.setProcessName("252_232_7007");
		
		return processCustomFields( oEditableRecord,  iClauseId,  sHtml,  sFillInCode, psInsert,  psUpdate, oFillInRecord, 
				sDefaultData,sDefaultContent,sRemarks, sType, iSize, oEcfrFullEditLogRecord); 

	}
		
	// Action: Find Aircraft and replace with memo box
	// Find In the open and replace with memo box
	// Find (e)(3) and replace with a memo box"

	private static boolean process252_228_7001(Stg1RawClauseEditableRecord oEditableRecord, int iClauseId, String sHtml, String sFillInCode,
		PreparedStatement psInsert, PreparedStatement psUpdate, 
		TemplateStg2ClauseFillInRecord oFillInRecord) throws SQLException {
		
		String sDefaultData = "In the open";
		String sDefaultContent = "In the open";
		String sRemarks = oEditableRecord.getEditableRemarks();
		String sType = "S";
		//sFillInCode = sFillInCode.replace("memobox", "textbox");
		//sFillInCode = getNextFillinCode ( sHtml, sFillInCode) ;
		sFillInCode = "in_the_open_252.228-7001";
		int iSize = 50;
		
		EcfrFullEditLogRecord oEcfrFullEditLogRecord = new EcfrFullEditLogRecord();
		oEcfrFullEditLogRecord.setProcessName("252_228_7001");
		
		boolean bOkToGo = processCustomFields( oEditableRecord,  iClauseId,  sHtml,  sFillInCode, psInsert,  psUpdate, oFillInRecord, 
				sDefaultData,sDefaultContent,sRemarks, sType, iSize, oEcfrFullEditLogRecord); 
		if (!bOkToGo) return false;
		sHtml = oEcfrFullEditLogRecord.getModifiedContent();
		
		sDefaultData = "Occurs in the course of transportation by rail, or by conveyance on public streets, highways, or waterways, except for Government-furnished property";
		sDefaultContent = sDefaultData;
		sType = "M";
		//sFillInCode = sFillInCode.replace("textbox", "memobox");
		//sFillInCode = getNextFillinCode ( sHtml, sFillInCode) ;
		sFillInCode = "paragraph_e3_252.228-7001";
		iSize = 500;
		bOkToGo = processCustomFields( oEditableRecord,  iClauseId,  sHtml,  sFillInCode, psInsert,  psUpdate, oFillInRecord, 
				sDefaultData,sDefaultContent,sRemarks, sType, iSize, oEcfrFullEditLogRecord); 
		if (!bOkToGo) return false;
		sHtml = oEcfrFullEditLogRecord.getModifiedContent();
		
		
		// Replace Schedule strings here.
		sDefaultData = "aircraft";
		sDefaultContent = "aircraft";
		//sFillInCode = sFillInCode.replace("memobox", "textbox");
		sFillInCode = "term_placeholder_252.228-7001[0]";
		sType = "S";
		iSize = 50;

		while (bOkToGo && sHtml.contains(sDefaultContent)) {
			sFillInCode = getNextFillinCode ( sHtml, sFillInCode) ;
			bOkToGo = processCustomFields( oEditableRecord,  iClauseId,  sHtml,  sFillInCode, psInsert,  psUpdate, oFillInRecord, 
					sDefaultData,sDefaultContent,sRemarks, sType, iSize, oEcfrFullEditLogRecord); 
			sHtml = oEcfrFullEditLogRecord.getModifiedContent();
		}
		if (!bOkToGo) return false;
		
		
		sDefaultData = "Aircraft";
		sDefaultContent = "Aircraft";

		while (bOkToGo && sHtml.contains(sDefaultContent)) {
			sFillInCode = getNextFillinCode ( sHtml, sFillInCode) ;
			bOkToGo = processCustomFields( oEditableRecord,  iClauseId,  sHtml,  sFillInCode, psInsert,  psUpdate, oFillInRecord, 
					sDefaultData,sDefaultContent,sRemarks, sType, iSize, oEcfrFullEditLogRecord); 
			sHtml = oEcfrFullEditLogRecord.getModifiedContent();
		}
		if (!bOkToGo) return false;
		
		sDefaultData = "in the open";
		sDefaultContent = "in the open";

		while (bOkToGo && sHtml.contains(sDefaultContent)) {
			sFillInCode = getNextFillinCode ( sHtml, sFillInCode) ;
			bOkToGo = processCustomFields( oEditableRecord,  iClauseId,  sHtml,  sFillInCode, psInsert,  psUpdate, oFillInRecord, 
					sDefaultData,sDefaultContent,sRemarks, sType, iSize, oEcfrFullEditLogRecord); 
			sHtml = oEcfrFullEditLogRecord.getModifiedContent();
		}
		if (!bOkToGo) return false;
	
		
		sDefaultData = "Contractor's premises";
		sDefaultContent = "Contractor's premises";

		while (bOkToGo && sHtml.contains(sDefaultContent)) {
			sFillInCode = getNextFillinCode ( sHtml, sFillInCode) ;
			bOkToGo = processCustomFields( oEditableRecord,  iClauseId,  sHtml,  sFillInCode, psInsert,  psUpdate, oFillInRecord, 
					sDefaultData,sDefaultContent,sRemarks, sType, iSize, oEcfrFullEditLogRecord); 
			sHtml = oEcfrFullEditLogRecord.getModifiedContent();
		}
		if (!bOkToGo) return false;
		
		return bOkToGo;
	}

	// NOTE: These are fillins within fillins!
	// PARAGRAPH (a) ONLY.
	private static boolean process52_211_12(Stg1RawClauseEditableRecord oEditableRecord, int iClauseId, String sHtml, String sFillInCode,
			PreparedStatement psInsert, PreparedStatement psUpdate, 
			TemplateStg2ClauseFillInRecord oFillInRecord) throws SQLException {
		
		String sType = "S";
		sFillInCode = "paragraph_a_text_52.211-12";
		int iSize = 100;
		
		// insert first textbox
		oFillInRecord.setFillInType(sType);
		oFillInRecord.setFillInCode(sFillInCode);
		oFillInRecord.setFillInMaxSize(iSize);
		oFillInRecord.setFillInDisplayRows(null);
		oFillInRecord.setFillInPlaceholder("Contracting Officer insert amount");
		oFillInRecord.insert(psInsert);
				
		// insert edit textbox
		sFillInCode = "paragraph_a_memo_52.211-12";
		oFillInRecord.setFillInDefaultData("(a) If the Contractor fails to complete the work within the time specified in the contract, the Contractor shall pay liquidated damages to the Government in the amount of {{paragraph_a_text_52.211-12}} for each calendar day of delay until the work is completed or accepted.");
		oFillInRecord.setFillInType("M");
		oFillInRecord.setFillInCode(sFillInCode);
		oFillInRecord.setFillInMaxSize(1000);
		oFillInRecord.insert(psInsert);		
		
		String sContent = 
				"<p>{{paragraph_a_memo_52.211-12}}</p><p>(b) If the Government terminates the Contractor''s right to proceed, liquidated damages will continue to accrue until the work is completed. These liquidated damages are in addition to excess costs of repurchase under the Termination clause.</p></p>";
		
		psUpdate.setString(1, sContent);
		psUpdate.setInt(2, iClauseId);
		psUpdate.execute();
		
		return true;
	}
	
	// NOTE: These are fillins within fillins!
	// ONLY paragraphs (d)(1) and (2) of the clause may be modified to meet local conditions.
	
	private static boolean process252_237_7017(Stg1RawClauseEditableRecord oEditableRecord, int iClauseId, String sHtml, String sFillInCode,
			PreparedStatement psInsert, PreparedStatement psUpdate, 
			TemplateStg2ClauseFillInRecord oFillInRecord) throws SQLException {
		
		String sType = "S";
		sFillInCode = "paragraph_d1_text_252.237-7017";
		int iSize = 100;
		
		// insert first textbox
		//oFillInRecord.setFillInDefaultData(null);
		oFillInRecord.setFillInType(sType);
		oFillInRecord.setFillInCode(sFillInCode);
		oFillInRecord.setFillInMaxSize(iSize);
		oFillInRecord.setFillInDisplayRows(null);
		oFillInRecord.insert(psInsert);
		
		// insert second textbox
		sFillInCode = "paragraph_d2_text_252.237-7017";
		//oFillInRecord.setFillInDefaultData(null);
		oFillInRecord.setFillInType(sType);
		oFillInRecord.setFillInCode(sFillInCode);
		oFillInRecord.setFillInMaxSize(iSize);
		oFillInRecord.setFillInDisplayRows(null);
		oFillInRecord.insert(psInsert);
		
		
		// insert edit textbox
		sFillInCode = "paragraph_d_memo_252.237-7017";
		oFillInRecord.setFillInDefaultData("(1) <i>Bundle consisting of 26 pieces, including laundry bag.</i> This bundle will contain approximately {{paragraph_d1_text_252.237-7017}} pieces of outer garments which shall be starched and pressed. Outer garments include, but are not limited to, shirts, trousers, jackets, dresses, and coats.</p><p>(2) <i>Bundle consisting of 13 pieces, including laundry bag.</i> This bundle will contain approximately {{paragraph_d2_text_252.237-7017}} pieces of outer garments which shall be starched and pressed. Outer garments include, but are not limited to, shirts, trousers, jackets, dresses, and coats.");
		oFillInRecord.setFillInType("M");
		oFillInRecord.setFillInCode(sFillInCode);
		oFillInRecord.setFillInMaxSize(1000);
		oFillInRecord.insert(psInsert);
		
		String sContent = 
				"<p>(a) The Contractor shall provide laundry service under this contract on both a unit bundle and on a piece-rate bundle basis for individual personnel.</p><p>(b) The total number of pieces listed in the ''Estimated Quantity'' column in the schedule is the estimated amount of individual laundry for this contract. The estimate is for information only and is not a representation of the amount of individual laundry to be ordered. Individuals may elect whether or not to use the laundry services.</p><p>(c) Charges for individual laundry will be on a per unit bundle or a piece-rate basis. The Contractor shall provide individual laundry bundle delivery tickets for use by the individuals in designating whether the laundry is a unit bundle or a piece-rate bundle. An individual laundry bundle will be accompanied by a delivery ticket listing the contents of the bundle.</p><p>(d) The maximum number of pieces to be allowed per bundle is as specified in the schedule and as follows-</p><p>{{paragraph_d_memo_252.237-7017}}</p>";
		
		psUpdate.setString(1, sContent);
		psUpdate.setInt(2, iClauseId);
		psUpdate.execute();
		
		return true;
	}
	
	private static String getNextFillinCode (String sHtml, String sFillInCode) 
	{
		int iStartCount = 0;
		int iLeftPos = sFillInCode.indexOf('_');
		if (iLeftPos>0)
		{
			String sTmpCode = "{{" + sFillInCode.substring(0, iLeftPos) + "}}";
			
			int fromIndex = 0;
			while (fromIndex >= 0) {
				fromIndex = sHtml.indexOf(sTmpCode, fromIndex+1);
				if (fromIndex > 0)
					iStartCount++;
			}
		}
		
		if (!sHtml.contains(sFillInCode) && (iStartCount == 0))
			return sFillInCode;
		
		// start at zero.
		iLeftPos = sFillInCode.indexOf('[');
		sFillInCode = sFillInCode.substring(0, iLeftPos) + "[" + iStartCount + "]";// + "[0]";
		if (!sHtml.contains(sFillInCode))
			return sFillInCode;
		
		// look for next available index.
		int iCnt=iStartCount+1;
		while (sHtml.contains(sFillInCode)) {
			sFillInCode = sFillInCode.replace("[" + (iCnt-1) + "]", "[" + iCnt + "]");
			iCnt++; 
		}
		
		return sFillInCode;
	}
	
	private static boolean processCustomFields(Stg1RawClauseEditableRecord oEditableRecord, int iClauseId, String sHtml, String sFillInCode, 
			PreparedStatement psInsert, PreparedStatement psUpdate, TemplateStg2ClauseFillInRecord oFillInRecord,
			 String sDefaultData, String sDefaultContent, String sRemarks, String sType, int iSize, EcfrFullEditLogRecord oLogRecord) 
		throws SQLException {
		
		String sContentHtmlFillIns = null;
		//oEditableRecord.setModifiedContent(sHtml);
		oLogRecord.setModifiedContent(sHtml);
		
		boolean bOkToGo = sRemarks.equals(oEditableRecord.getEditableRemarks());
		if (bOkToGo) {
			
			// Go to the position of the default content, then look for position of the default data.
			int iPos = sHtml.indexOf(sDefaultContent);
			if (iPos > 1) {
				iPos += sDefaultContent.indexOf(sDefaultData);
			}
			
			if (iPos > 1) {
				String sLeft = sHtml.substring(0, iPos);
				String sRight = (sDefaultData.length() > 0) 
									? sHtml.substring(iPos + sDefaultData.length())
									: sHtml.substring(iPos + sDefaultContent.length());
				sContentHtmlFillIns = sLeft + "{{" + sFillInCode + "}}" + sRight; 
			} else
				bOkToGo = false;
		}
		
		// CJ-838
		sDefaultData = sDefaultData.replaceAll("″", "");
		
		if (bOkToGo == false) {
			System.out.println("EcfrProcessFullEdit.process" + oLogRecord.getProcessName() + 
					"() skipped for (" + oEditableRecord.getClauseNumber() + ") " + 
					oEditableRecord.getEditableRemarks());
		} else {
			oFillInRecord.setFillInDefaultData(sDefaultData);
			oFillInRecord.setFillInType(sType);
			oFillInRecord.setFillInCode(sFillInCode);
			oFillInRecord.setFillInMaxSize(iSize);
			if (!"M".equals(sType))
				oFillInRecord.setFillInDisplayRows(null);
			oFillInRecord.insert(psInsert);
			
			psUpdate.setString(1, sContentHtmlFillIns);
			psUpdate.setInt(2, iClauseId);
			psUpdate.execute();
			
			//oEditableRecord.setModifiedContent(sContentHtmlFillIns);
			oLogRecord.setModifiedContent(sContentHtmlFillIns);
			
			System.out.println("EcfrProcessFullEdit.process" + oLogRecord.getProcessName() + 
					"() for (" + oFillInRecord.getStg2Id() + ") " + 
					oEditableRecord.getClauseNumber() + " with " + sFillInCode);
		}
		return bOkToGo;
	}
	
	private static boolean processCustomFieldsAndPlaceHolder(Stg1RawClauseEditableRecord oEditableRecord, int iClauseId, String sHtml, String sFillInCode, 
			PreparedStatement psInsert, PreparedStatement psUpdate, TemplateStg2ClauseFillInRecord oFillInRecord,
			 String sDefaultData, String sDefaultContent, String sRemarks, String sType, int iSize, String sPlaceholder, EcfrFullEditLogRecord oLogRecord) 
		throws SQLException {
		
		String sContentHtmlFillIns = null;
		//oEditableRecord.setModifiedContent(sHtml);
		oLogRecord.setModifiedContent(sHtml);
		
		boolean bOkToGo = sRemarks.equals(oEditableRecord.getEditableRemarks());
		if (bOkToGo) {
			
			// Go to the position of the default content, then look for position of the default data.
			int iPos = sHtml.indexOf(sDefaultContent);
			if (iPos > 1) {
				iPos += sDefaultContent.indexOf(sDefaultData);
			}
			
			if (iPos > 1) {
				String sLeft = sHtml.substring(0, iPos);
				String sRight = (sDefaultData.length() > 0) 
									? sHtml.substring(iPos + sDefaultData.length())
									: sHtml.substring(iPos + sDefaultContent.length());
				sContentHtmlFillIns = sLeft + "{{" + sFillInCode + "}}" + sRight; 
			} else
				bOkToGo = false;
		}
		
		// CJ-838
		sDefaultData = sDefaultData.replaceAll("″", "");
		
		if (bOkToGo == false) {
			System.out.println("EcfrProcessFullEdit.process" + oLogRecord.getProcessName() + 
					"() skipped for (" + oEditableRecord.getClauseNumber() + ") " + 
					oEditableRecord.getEditableRemarks());
		} else {
			oFillInRecord.setFillInDefaultData(sDefaultData);
			oFillInRecord.setFillInType(sType);
			oFillInRecord.setFillInCode(sFillInCode);
			oFillInRecord.setFillInMaxSize(iSize);
			oFillInRecord.setFillInPlaceholder(sPlaceholder);
			if (!"M".equals(sType))
				oFillInRecord.setFillInDisplayRows(null);
			oFillInRecord.insert(psInsert);
			
			psUpdate.setString(1, sContentHtmlFillIns);
			psUpdate.setInt(2, iClauseId);
			psUpdate.execute();
			
			//oEditableRecord.setModifiedContent(sContentHtmlFillIns);
			oLogRecord.setModifiedContent(sContentHtmlFillIns);
			
			System.out.println("EcfrProcessFullEdit.process" + oLogRecord.getProcessName() + 
					"() for (" + oFillInRecord.getStg2Id() + ") " + 
					oEditableRecord.getClauseNumber() + " with " + sFillInCode);
		}
		return bOkToGo;
	}
	
	private static boolean process52_216_29(Stg1RawClauseEditableRecord oEditableRecord, int iClauseId, String sHtml, String sFillInCode, 
			PreparedStatement psInsert, PreparedStatement psUpdate, 
			TemplateStg2ClauseFillInRecord oFillInRecord) throws SQLException {
		
		String sDefaultData = "<p>(1) Separate rates for each category of labor to be performed by each subcontractor and for each category of labor to be performed by the offeror, and for each category of labor to be transferred between divisions, subsidiaries, or affiliates of the offeror under a common control;</p><p>(2) Blended rates for each category of labor to be performed by the offeror, including labor transferred between divisions, subsidiaries, or affiliates of the offeror under a common control, and all subcontractors; or</p><p>(3) Any combination of separate and blended rates for each category of labor to be performed by the offeror, affiliates of the offeror under a common control, and subcontractors.</p>";
		String sRemarks = "may ONLY amend the provision to make mandatory one of the three approaches in paragraph (c) of the provision, and/or to require the identification of all subcontractors, divisons, subsidiaries, or affiliates included in a blended labor rate.";
		//String sContentHtmlFillIns = null;
		
		String sDefaultContent = sDefaultData;
		sFillInCode = "paragraph_c1_52_216_29";
		int iSize = 1000;
		String sType = "M";
		
		EcfrFullEditLogRecord oEcfrFullEditLogRecord = new EcfrFullEditLogRecord();
		oEcfrFullEditLogRecord.setProcessName("52_216_29");
		
		return processCustomFields( oEditableRecord,  iClauseId,  sHtml,  sFillInCode, psInsert,  psUpdate, oFillInRecord,
				 				sDefaultData,sDefaultContent,sRemarks, sType, iSize, oEcfrFullEditLogRecord) ;

		
		/*
		boolean bOkToGo = sRemarks.equals(oEditableRecord.getEditableRemarks());
		if (bOkToGo) {
			int iPos = sHtml.indexOf(sDefaultData);
			if (iPos > 1) {
				String sLeft = sHtml.substring(0, iPos);
				String sRight = sHtml.substring(iPos + sRemarks.length());
				sContentHtmlFillIns = sLeft + "{{" + sFillInCode + "}}" + sRight; 
			} else
				bOkToGo = false;
		}
			
		if (bOkToGo == false) {
			System.out.println("EcfrProcessFullEdit.process52_216_29() skipped for (" + oEditableRecord.getClauseNumber() + ") " + oEditableRecord.getEditableRemarks());
		} else {
			oFillInRecord.setFillInDefaultData(sDefaultData);
			oFillInRecord.insert(psInsert);
			
			psUpdate.setString(1, sContentHtmlFillIns);
			psUpdate.setInt(2, iClauseId);
			psUpdate.execute();
			System.out.println("EcfrProcessFullEdit.process52_216_29() for (" + oFillInRecord.getStg2Id() + ") " + oEditableRecord.getClauseNumber() + " with " + sFillInCode);
		}
		
		return bOkToGo;
		*/
	}
	
}
