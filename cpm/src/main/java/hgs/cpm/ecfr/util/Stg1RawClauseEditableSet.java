package hgs.cpm.ecfr.util;

import gov.dod.cls.utils.sql.SqlUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class Stg1RawClauseEditableSet extends ArrayList<Stg1RawClauseEditableRecord> {

	private static final long serialVersionUID = 1L;

	public static Stg1RawClauseEditableSet load(int iClauseSrcDocId, Connection conn) throws SQLException {
		Stg1RawClauseEditableSet result = new Stg1RawClauseEditableSet();
		PreparedStatement ps1 = null;
		ResultSet rs1 = null;
		try {
			ps1 = conn.prepareStatement(Stg1RawClauseEditableRecord.SQL_SELECT);
			ps1.setInt(1, iClauseSrcDocId);
			rs1 = ps1.executeQuery();
			result.clear();
			while (rs1.next()) {
				Stg1RawClauseEditableRecord oRecord = new Stg1RawClauseEditableRecord();
				oRecord.read(rs1);
				result.add(oRecord);
			}
		} finally {
			SqlUtil.closeInstance(rs1, ps1);
		}
		return result;
	}
	
	public boolean isEditAll(String psClauseNumber) {
		Stg1RawClauseEditableRecord oRecord = this.findByClauseNumber(psClauseNumber);
		return ((oRecord != null) && oRecord.isAllEditable());
	}
	
	public Stg1RawClauseEditableRecord findByClauseNumber(String psClauseNumber) {
		for (Stg1RawClauseEditableRecord oRecord : this) {
			if (oRecord.getClauseNumber().equalsIgnoreCase(psClauseNumber))
				return oRecord;
		}
		return null;
	}
	
	public Stg1RawClauseEditableSet subsetForEditAll(boolean isAll) {
		Stg1RawClauseEditableSet result = new Stg1RawClauseEditableSet();
		for (Stg1RawClauseEditableRecord oRecord : this) {
			if (oRecord.isAllEditable()) {
				if (isAll)
					result.add(oRecord);
			} else {
				if (isAll == false)
					result.add(oRecord);
			}
		}
		return result;
		
	}
	
}
