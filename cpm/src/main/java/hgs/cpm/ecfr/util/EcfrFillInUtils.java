package hgs.cpm.ecfr.util;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.regex.Pattern;

import hgs.cpm.db.template.TemplateStg2ClauseFillInRecord;
import hgs.cpm.utils.HtmlUtils;

public final class EcfrFillInUtils {

	public static String _Fillin_Marker = "____";
	
	public static String parseFillinsAndNormalize(String sClauseNumber, String html) {
		if ((html == null) || html.isEmpty())
			return html;

		html = HtmlUtils.parseFillinsAndNormalize(html);
		/*
		switch (sClauseNumber) {
		case "252.241-7000":
			// html = EcfrFillInUtils.process252_241_7000(html);
			break;
		}
		*/
		return html;
	}

	public static String produceFillins(String sClauseNumber, int iStg1OfficialClauseId, String html, TemplateStg2ClauseFillInRecord oTemplateStg2ClauseFillInRecord, PreparedStatement psFillInInsertClause) throws SQLException {
		if (html == null) {
			System.out.println("EcfrFillinUtils.produceFillin null html: " + sClauseNumber);
		}
		html = HtmlUtils.normalizeHtml(html);
		//System.out.println("Reg Fill-in: " + sClauseNumber);
		//HtmlUtils.normalizeFillins(html);
		
		boolean skipGenericProcessing = false;
		switch (sClauseNumber.toUpperCase()) { // CJ-1075
			case "252.241-7000":
				html = EcfrFillInUtils.process252_241_7000(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
				break;
			//clauses that should not be processed for fillins:
			case "52.204-8":
				html = EcfrFillInUtils.process52_204_8(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
				skipGenericProcessing = true;
				break;
			case "52.212-3":
				html = EcfrFillInUtils.process52_212_3(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
				skipGenericProcessing = true;
				break;	
			case "52.212-3 ALTERNATE I":
				html = EcfrFillInUtils.process52_212_3_Alt1(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
				skipGenericProcessing = true;
				break;	
			case "252.204-7007":
				html = EcfrFillInUtils.process252_204_7007(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
				skipGenericProcessing = true;
				break;
			case "252.216-7001":	
				html = EcfrFillInUtils.process252_216_7001(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
				skipGenericProcessing = true;
				break;				
			case "252.225-7046":
				html = EcfrFillInUtils.process252_225_7046(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
				skipGenericProcessing = true;
				break;
			case "252.225-7982 (DEVIATION 2016-00005)":
				html = EcfrFillInUtils.process252_225_7982_deviation_2016_00005(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
				skipGenericProcessing = true;
				break;
	
			case "52.247-2":
				html = EcfrFillInUtils.process52_247_2(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
				skipGenericProcessing = true;
				break;				
				
			case "52.219-4":
				html = EcfrFillInUtils.process52_219_4(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
				skipGenericProcessing = true;
				break;	
					
			case "52.242-4": // CJ-808, add line breaks;
				html = EcfrFillInUtils.process52_242_4(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
				skipGenericProcessing = true;
				break;	
				
			case "52.209-1": // CJ-808, add line breaks;
				html = EcfrFillInUtils.process52_209_1(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
				skipGenericProcessing = true;
				break;
					
			case "252.227-7014": // CJ-808, add line breaks;	
				html = EcfrFillInUtils.process252_227_7014(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
				skipGenericProcessing = true;
				break;
			case "252.211-7005": // CJ-808, add line breaks;
				html = EcfrFillInUtils.process252_211_7005(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
				skipGenericProcessing = true;
				break;
				
			case "252.208-7000": // The eCFR data is corrupted on the website. This incorrectly deletes all the fillins.
				html = EcfrFillInUtils.process252_208_7000(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
				skipGenericProcessing = true;
				break;	
				
			case "252.232-7006":
				html = EcfrFillInUtils.process252_232_7006(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
				//skipGenericProcessing = true;
				break;	
				
			// Placeholder fillins.
			case "252.203-7004":
				html = EcfrFillInUtils.process252_203_7004(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
				skipGenericProcessing = true;
				break;	
			case "252.204-7010":
				html = EcfrFillInUtils.process252_204_7010(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
				skipGenericProcessing = true;
				break;	
			case "252.209-7010":
				html = EcfrFillInUtils.process252_209_7010(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
				skipGenericProcessing = true;
				break;
			case "252.211-7002":
				html = EcfrFillInUtils.process252_211_7002(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
				skipGenericProcessing = true;
				break;
			case "252.215-7003":
				html = EcfrFillInUtils.process252_215_7003(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
				skipGenericProcessing = true;
				break;
			case "252.215-7008":
				html = EcfrFillInUtils.process252_215_7008(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
				skipGenericProcessing = true;
				break;
			case "252.216-7000":
				html = EcfrFillInUtils.process252_216_7000(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
				skipGenericProcessing = true;
				break;
			case "252.216-7006":
				html = EcfrFillInUtils.process252_216_7006(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
				skipGenericProcessing = true;
				break;
				
			case "252.216-7007":
				html = EcfrFillInUtils.process252_216_7007(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
				skipGenericProcessing = true;
				break;
			//case "252.216-7008":
				//html = EcfrFillInUtils.process252_216_7008(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
				//skipGenericProcessing = true;
				//break;
			case "252.217-7002":
				html = EcfrFillInUtils.process252_217_7002(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
				skipGenericProcessing = true;
				break;
			case "252.219-7009":
				html = EcfrFillInUtils.process252_219_7009(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
				skipGenericProcessing = true;
				break;
			case "252.219-7010":
				//html = EcfrFillInUtils.process252_219_7010(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
				skipGenericProcessing = true;
				break;
			//case "252.225-7018":
			//	html = EcfrFillInUtils.process252_225_7018(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
			//	skipGenericProcessing = true;
			//	break;
				
			case "252.225-7043":
				html = EcfrFillInUtils.process252_225_7043(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
				skipGenericProcessing = true;
				break;
			case "252.225-7044":
			case "252.225-7044 ALTERNATE I":
			case "252.225-7045":
			case "252.225-7045 ALTERNATE I":
			case "252.225-7045 ALTERNATE II":
			case "252.225-7045 ALTERNATE III":
				html = EcfrFillInUtils.process252_225_7044(sClauseNumber.toUpperCase(), iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
				skipGenericProcessing = true;
				break;
			case "252.225-7047":
				html = EcfrFillInUtils.process252_225_7047(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
				skipGenericProcessing = true;
				break;
			case "252.227-7012":
				html = EcfrFillInUtils.process252_227_7012(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
				skipGenericProcessing = true;
				break;
			// Alternate clause is not in the spreadsheet.
			case "252.227-7038 ALTERNATE I":
				html = EcfrFillInUtils.process252_227_7038_Alt_I(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
				skipGenericProcessing = true;
				break;
			case "252.229-7001 ALTERNATE I":
				html = EcfrFillInUtils.process252_229_7001_AltI(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
				skipGenericProcessing = true;
				break;	
			case "252.229-7001":
				html = EcfrFillInUtils.process252_229_7001(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
				skipGenericProcessing = true;
				break;
			case "252.229-7003":
				html = EcfrFillInUtils.process252_229_7003(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
				skipGenericProcessing = true;
				break;
			case "252.229-7004":
				html = EcfrFillInUtils.process252_229_7004(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
				skipGenericProcessing = true;
				break;
			case "252.232-7001":
				html = EcfrFillInUtils.process252_232_7001(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
				skipGenericProcessing = true;
				break;
			case "252.232-7007":
				html = EcfrFillInUtils.process252_232_7007(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
				skipGenericProcessing = true;
				break;
			case "252.232-7013":
				html = EcfrFillInUtils.process252_232_7013(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
				skipGenericProcessing = true;
				break;
			case "252.232-7014":
				html = EcfrFillInUtils.process252_232_7014(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
				skipGenericProcessing = true;
				break;		
			case "252.234-7002 (DEVIATION 2015-00017)":
				html = EcfrFillInUtils.process252_234_7002(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
				skipGenericProcessing = true;
				break;	
			case "252.235-7010":
				html = EcfrFillInUtils.process252_235_7010(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
				skipGenericProcessing = true;
				break;
			case "252.237-7019":
				html = EcfrFillInUtils.process252_237_7019(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
				skipGenericProcessing = true;
				break;	
				
			case "52.209-3":
				html = EcfrFillInUtils.process52_209_3(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
				skipGenericProcessing = true;
				break;	
			case "52.209-4":
				html = EcfrFillInUtils.process52_209_4(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
				skipGenericProcessing = true;
				break;	
			case "52.211-8":
				html = EcfrFillInUtils.process52_211_8(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
				skipGenericProcessing = true;
				break;	
			case "52.211-8 ALTERNATE I":
				html = EcfrFillInUtils.process52_211_8_AltI(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
				skipGenericProcessing = true;
				break;	
			case "52.211-8 ALTERNATE II":
				html = EcfrFillInUtils.process52_211_8_AltII(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
				skipGenericProcessing = true;
				break;	
			case "52.211-9":
				html = EcfrFillInUtils.process52_211_9(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
				skipGenericProcessing = true;
				break;	
			case "52.211-9 ALTERNATE II":
				html = EcfrFillInUtils.process52_211_9_AltII(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
				skipGenericProcessing = true;
				break;	
			case "52.211-9 ALTERNATE I":
				html = EcfrFillInUtils.process52_211_9_AltI(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
				skipGenericProcessing = true;
				break;	
			case "52.211-10":
				html = EcfrFillInUtils.process52_211_10(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
				skipGenericProcessing = true;
				break;	
			case "52.211-10 ALTERNATE I":
				html = EcfrFillInUtils.process52_211_10_AltI(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
				skipGenericProcessing = true;
				break;
			case "52.211-11":
				html = EcfrFillInUtils.process52_211_11(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
				skipGenericProcessing = true;
				break;	
			//case "52.211-12":
			//	html = EcfrFillInUtils.process52_211_12(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
			//	skipGenericProcessing = true;
			//	break;	
			case "52.211-14":
				html = EcfrFillInUtils.process52_211_14(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
				skipGenericProcessing = true;
				break;	
			case "52.211-16":
				html = EcfrFillInUtils.process52_211_16(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
				skipGenericProcessing = true;
				break;	
			case "52.212-4 ALTERNATE I":
				html = EcfrFillInUtils.process52_212_4_AltI(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
				skipGenericProcessing = true;
				break;	
			case "52.213-4":
				html = EcfrFillInUtils.process52_213_4(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
				skipGenericProcessing = true;
				break;
			case "52.214-15":
				html = EcfrFillInUtils.process52_214_15(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
				skipGenericProcessing = true;
				break;	
			case "52.214-16":
				html = EcfrFillInUtils.process52_214_16(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
				skipGenericProcessing = true;
				break;	
			case "52.214-25":
				html = EcfrFillInUtils.process52_214_25(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
				skipGenericProcessing = true;
				break;
			case "52.215-3":
				html = EcfrFillInUtils.process52_215_3(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
				skipGenericProcessing = true;
				break;
			case "52.215-5":
				html = EcfrFillInUtils.process52_215_5(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
				skipGenericProcessing = true;
				break;
			case "52.215-20 ALTERNATE I":
				html = EcfrFillInUtils.process52_215_20_AltI(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
				skipGenericProcessing = true;
				break;
			case "52.215-20 ALTERNATE III":
				html = EcfrFillInUtils.process52_215_20_AltIII(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
				skipGenericProcessing = true;
				break;
			case "52.215-20 ALTERNATE IV":
				html = EcfrFillInUtils.process52_215_20_AltIV(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
				skipGenericProcessing = true;
				break;
			case "52.214-20 ALTERNATE I":
				html = EcfrFillInUtils.process52_214_20_AltI(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
				skipGenericProcessing = true;
				break;
			case "52.214-20 ALTERNATE II":
				html = EcfrFillInUtils.process52_214_20_AltII(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
				skipGenericProcessing = true;
				break;
			case "52.215-21 ALTERNATE I":
				html = EcfrFillInUtils.process52_215_21_AltI(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
				skipGenericProcessing = true;
				break;
			case "52.215-21 ALTERNATE III":
				html = EcfrFillInUtils.process52_215_21_AltIII(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
				skipGenericProcessing = true;
				break;
			case "52.215-21 ALTERNATE IV":
				html = EcfrFillInUtils.process52_215_21_AltIV(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
				skipGenericProcessing = true;
				break;
			case "52.216-1":
				html = EcfrFillInUtils.process52_216_1(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
				skipGenericProcessing = true;
				break;
			case "52.216-10":
				html = EcfrFillInUtils.process52_216_10(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
				skipGenericProcessing = true;
				break;
			case "52.216-2":
				html = EcfrFillInUtils.process52_216_2(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
				skipGenericProcessing = true;
				break;
			case "52.216-3":
				html = EcfrFillInUtils.process52_216_3(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
				skipGenericProcessing = true;
				break;
			case "52.216-5":
				html = EcfrFillInUtils.process52_216_5(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
				skipGenericProcessing = true;
				break;
			case "52.216-6":
				html = EcfrFillInUtils.process52_216_6(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
				skipGenericProcessing = true;
				break;
			case "52.216-7":
				html = EcfrFillInUtils.process52_216_7(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
				skipGenericProcessing = true;
				break;
			case "52.216-16":
				html = EcfrFillInUtils.process52_216_16(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
				skipGenericProcessing = true;
				break;
			case "52.216-17":
				html = EcfrFillInUtils.process52_216_17(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
				skipGenericProcessing = true;
				break;
			case "52.216-18":
				html = EcfrFillInUtils.process52_216_18(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
				skipGenericProcessing = true;
				break;
			case "52.216-19":
				html = EcfrFillInUtils.process52_216_19(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
				skipGenericProcessing = true;
				break;
			case "52.216-20":
				html = EcfrFillInUtils.process52_216_20(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
				skipGenericProcessing = true;
				break;
			case "52.216-21":
				html = EcfrFillInUtils.process52_216_21(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
				skipGenericProcessing = true;
				break;
			case "52.216-22":
				html = EcfrFillInUtils.process52_216_22(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
				skipGenericProcessing = true;
				break;
			case "52.216-23":
				html = EcfrFillInUtils.process52_216_23(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
				skipGenericProcessing = true;
				break;
			case "52.216-25":
				html = EcfrFillInUtils.process52_216_25(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
				skipGenericProcessing = true;
				break;
			case "52.216-25 ALTERNATE I":
				html = EcfrFillInUtils.process52_216_25_AltI(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
				skipGenericProcessing = true;
				break;
			case "52.217-6":
				html = EcfrFillInUtils.process52_217_6(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
				skipGenericProcessing = true;
				break;
			case "52.217-7":
				html = EcfrFillInUtils.process52_217_7(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
				skipGenericProcessing = true;
				break;
			case "52.217-8":
				html = EcfrFillInUtils.process52_217_8(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
				skipGenericProcessing = true;
				break;
			case "52.217-9":
				html = EcfrFillInUtils.process52_217_9(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
				skipGenericProcessing = true;
				break;
			case "52.219-1":
				html = EcfrFillInUtils.process52_219_1(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
				skipGenericProcessing = true;
				break;
			case "52.219-1 ALTERNATE I":
			//	html = EcfrFillInUtils.process52_219_1_AltI(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
				skipGenericProcessing = true;
				break;
			case "52.219-10":
				html = EcfrFillInUtils.process52_219_10(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
				skipGenericProcessing = true;
				break;
			case "52.219-11":
				html = EcfrFillInUtils.process52_219_11(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
				skipGenericProcessing = true;
				break;
			case "52.219-12":
				html = EcfrFillInUtils.process52_219_12(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
				skipGenericProcessing = true;
				break;
			case "52.219-17":
				html = EcfrFillInUtils.process52_219_17(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
				skipGenericProcessing = true;
				break;
			case "52.219-18":
				html = EcfrFillInUtils.process52_219_18(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
				skipGenericProcessing = true;
				break;
			case "52.219-18 ALTERNATE I":
				html = EcfrFillInUtils.process52_219_18_AltI(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
				skipGenericProcessing = true;
				break;
			case "52.222-2":
				html = EcfrFillInUtils.process52_222_2(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
				skipGenericProcessing = true;
				break;
			case "52.219-28":
				html = EcfrFillInUtils.process52_219_28(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
				skipGenericProcessing = true;
				break;
			case "52.222-23":
				html = EcfrFillInUtils.process52_222_23(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
				skipGenericProcessing = true;
				break;
			case "52.222-31":
				html = EcfrFillInUtils.process52_222_31(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
				skipGenericProcessing = true;
				break;
			case "52.222-36 ALTERNATE I":
				html = EcfrFillInUtils.process52_222_36_AltI(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
				skipGenericProcessing = true;
				break;
			case "52.223-9":
				html = EcfrFillInUtils.process52_223_9(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
				skipGenericProcessing = true;
				break;
			case "52.225-11":
				html = EcfrFillInUtils.process52_225_11(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
				skipGenericProcessing = true;
				break;
			case "52.225-17":
				html = EcfrFillInUtils.process52_225_17(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
				skipGenericProcessing = true;
				break;
			case "52.225-19":
				html = EcfrFillInUtils.process52_225_19(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
				skipGenericProcessing = true;
				break;
			case "52.225-21":
				html = EcfrFillInUtils.process52_225_21(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
				skipGenericProcessing = true;
				break;
			case "52.225-23":
				html = EcfrFillInUtils.process52_225_23(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
				skipGenericProcessing = true;
				break;
			case "52.226-3":
				html = EcfrFillInUtils.process52_226_3(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
				skipGenericProcessing = true;
				break;
			case "52.226-4":
				html = EcfrFillInUtils.process52_226_4(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
				skipGenericProcessing = true;
				break;
			case "52.227-11":
				html = EcfrFillInUtils.process52_227_11(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
				skipGenericProcessing = true;
				break;
			case "52.227-11 ALTERNATE I":
				html = EcfrFillInUtils.process52_227_11_AltI(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
				skipGenericProcessing = true;
				break;
			case "52.227-7":
				html = EcfrFillInUtils.process52_227_7(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
				skipGenericProcessing = true;
				break;
			case "52.228-9":
				html = EcfrFillInUtils.process52_228_9(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
				skipGenericProcessing = true;
				break;
			case "52.229-10":
				html = EcfrFillInUtils.process52_229_10(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
				skipGenericProcessing = true;
				break;
			case "52.229-7":
				html = EcfrFillInUtils.process52_229_7(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
				skipGenericProcessing = true;
				break;
			case "52.229-8":
				html = EcfrFillInUtils.process52_229_8(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
				skipGenericProcessing = true;
				break;
			case "52.229-9":
				html = EcfrFillInUtils.process52_229_9(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
				skipGenericProcessing = true;
				break;
			case "52.232-12":
				html = EcfrFillInUtils.process52_232_12(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
				skipGenericProcessing = true;
				break;
			case "52.232-12 ALTERNATE II":
				html = EcfrFillInUtils.process52_232_12_AltII(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
				skipGenericProcessing = true;
				break;
			case "52.232-12 ALTERNATE V":
				html = EcfrFillInUtils.process52_232_12_AltV(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
				skipGenericProcessing = true;
				break;
			case "52.232-16":
				html = EcfrFillInUtils.process52_232_16(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
				skipGenericProcessing = true;
				break;
			case "52.232-16 ALTERNATE II":
				html = EcfrFillInUtils.process52_232_16_AltII(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
				skipGenericProcessing = true;
				break;
			case "52.232-32":
				html = EcfrFillInUtils.process52_232_32(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
				skipGenericProcessing = true;
				break;
			case "52.232-7":
				html = EcfrFillInUtils.process52_232_7(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
				skipGenericProcessing = true;
				break;
			case "52.232-34":
				html = EcfrFillInUtils.process52_232_34(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
				skipGenericProcessing = true;
				break;
			case "52.233-2":
				html = EcfrFillInUtils.process52_233_2(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
				skipGenericProcessing = true;
				break;
			case "52.234-4":
				html = EcfrFillInUtils.process52_234_4(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
				skipGenericProcessing = true;
				break;
			case "52.236-1":
				html = EcfrFillInUtils.process52_236_1(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
				skipGenericProcessing = true;
				break;
			case "52.236-4":
				html = EcfrFillInUtils.process52_236_4(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
				skipGenericProcessing = true;
				break;
			case "52.237-4":
				html = EcfrFillInUtils.process52_237_4(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
				skipGenericProcessing = true;
				break;
			case "52.237-5":
				html = EcfrFillInUtils.process52_237_5(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
				skipGenericProcessing = true;
				break;
			case "52.237-6":
				html = EcfrFillInUtils.process52_237_6(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
				skipGenericProcessing = true;
				break;
			case "52.241-3":
				html = EcfrFillInUtils.process52_241_3(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
				skipGenericProcessing = true;
				break;
			case "52.241-7":
				html = EcfrFillInUtils.process52_241_7(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
				skipGenericProcessing = true;
				break;
			case "52.241-8":
				html = EcfrFillInUtils.process52_241_8(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
				skipGenericProcessing = true;
				break;
			case "52.236-21 ALTERNATE II":
				html = EcfrFillInUtils.process52_236_21_AltII(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
				skipGenericProcessing = true;
				break;
			case "52.236-27 ALTERNATE I":
				html = EcfrFillInUtils.process52_236_27_AltI(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
				skipGenericProcessing = true;
				break;
			case "52.241-10":
				html = EcfrFillInUtils.process52_241_10(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
				skipGenericProcessing = true;
				break;
			case "52.241-12":
				html = EcfrFillInUtils.process52_241_12(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
				skipGenericProcessing = true;
				break;
			case "52.241-13":
				html = EcfrFillInUtils.process52_241_13(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
				skipGenericProcessing = true;
				break;
			case "52.246-11":
				html = EcfrFillInUtils.process52_246_11(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
				skipGenericProcessing = true;
				break;
			case "52.246-17":
				html = EcfrFillInUtils.process52_246_17(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
				skipGenericProcessing = true;
				break;	
			case "52.246-18":
				html = EcfrFillInUtils.process52_246_18(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
				skipGenericProcessing = true;
				break;
			case "52.246-19":
				html = EcfrFillInUtils.process52_246_19(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
				skipGenericProcessing = true;
				break;
			case "52.246-20":
				html = EcfrFillInUtils.process52_246_20(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
				skipGenericProcessing = true;
				break;
			case "52.247-51":
				html = EcfrFillInUtils.process52_247_51(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
				skipGenericProcessing = true;
				break;
			case "52.247-51 ALTERNATE III":
				html = EcfrFillInUtils.process52_247_51_AltIII(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
				skipGenericProcessing = true;
				break;
			case "52.247-66":
				html = EcfrFillInUtils.process52_247_66(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
				skipGenericProcessing = true;
				break;
			case "52.247-67":
				html = EcfrFillInUtils.process52_247_67(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
				skipGenericProcessing = true;
				break;
			case "52.252-1":
				html = EcfrFillInUtils.process52_252_1(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
				skipGenericProcessing = true;
				break;
			case "52.252-2":
				html = EcfrFillInUtils.process52_252_2(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
				skipGenericProcessing = true;
				break;
			case "52.252-5":
				html = EcfrFillInUtils.process52_252_5(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
				skipGenericProcessing = true;
				break;
			case "52.252-6":
				html = EcfrFillInUtils.process52_252_6(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
				skipGenericProcessing = true;
				break;
			case "252.227-7009":
				html = EcfrFillInUtils.process252_227_7009(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
				skipGenericProcessing = true;
				break;
			case "252.217-7000":
			html = EcfrFillInUtils.process252_217_7000(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
			skipGenericProcessing = true;
			break;
			
			case "252.217-7027":
			html = EcfrFillInUtils.process252_217_7027(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
			skipGenericProcessing = true;
			break;
			
			case "252.247-7001":
			html = EcfrFillInUtils.process252_247_7001(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
			skipGenericProcessing = true;
			break;
			
			case "252.247-7021":
			html = EcfrFillInUtils.process252_247_7021(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
			skipGenericProcessing = true;
			break;
			
			case "52.222-49":
			html = EcfrFillInUtils.process52_222_49(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
			skipGenericProcessing = true;
			break;
			
			
			case "52.223-7":
			html = EcfrFillInUtils.process52_223_7(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
			skipGenericProcessing = true;
			break;
			
			case "52.227-5":
			html = EcfrFillInUtils.process52_227_5(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
			skipGenericProcessing = true;
			break;

			case "52.222-50 ALTERNATE I":
				html = EcfrFillInUtils.process52_222_50_AltI(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
				skipGenericProcessing = true;
				break;
			case "52.223-3":
				html = EcfrFillInUtils.process52_223_3(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
				skipGenericProcessing = true;
				break;
			case "52.227-3 ALTERNATE I":
				html = EcfrFillInUtils.process52_227_3_AltI(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
				skipGenericProcessing = true;
				break;
			case "52.227-3 ALTERNATE II":
				html = EcfrFillInUtils.process52_227_3_AltII(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
				skipGenericProcessing = true;
				break;
			case "52.227-4 ALTERNATE I":
				html = EcfrFillInUtils.process52_227_4_AltI(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
				skipGenericProcessing = true;
				break;

			case "52.247-25":
				html = EcfrFillInUtils.process52_247_25(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
				skipGenericProcessing = true;
				break;
			case "52.203-14":
				html = EcfrFillInUtils.process52_203_14(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
				skipGenericProcessing = true;
				break;
			case "52.212-2":
				html = EcfrFillInUtils.process52_212_2(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
				skipGenericProcessing = true;
				break;
			case "52.212-5":
				html = EcfrFillInUtils.process52_212_5(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
				skipGenericProcessing = true;
				break;
			case "52.222-35 ALTERNATE I":
				html = EcfrFillInUtils.process52_222_35_AltI(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
				skipGenericProcessing = true;
				break;		
			// End Placeholder fillins.
				
			/*
			case "52.250-3 ALTERNATE II": // CJ-1075
				html = EcfrFillInUtils.process52_260_3_Alt_II(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
				break;
				
			case "52.250-4 ALTERNATE II": // CJ-1075
				html = EcfrFillInUtils.process52_260_4_Alt_II(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
				break;
			*/
			
			case "252.215-7004": // editable
			case "52.212-4":
			// case "52.212-4 ALTERNATE I":
			//case "52.214-16": // PCO - Provision
			//case "52.216-2": // PCO - Clause
			//case "52.219-1": // PCO - Provision
			//case "52.219-1 ALTERNATE I": // PCO - Provision
			case "252.225-7982": // PCO - Provision, originally ignored
			case "252.227-7017":// PCO - Provision, originally ignored
			case "252.204-7007 ALTERNATE A":	// PCO - Provision, originally ignored
			case "52.223-9 ALTERNATE I":
				skipGenericProcessing = true;
				break;
			// PCO/Offerors	
			case "52.247-60": // use editable.
			// case "52.247-2": special case.
			//case "52.242-4":
			// case "52.226-3": special case.
			//case "52.223-3":
			case "52.223-3 ALTERNATE I":
			//case "52.219-28":
			//case "52.214-16": // PCO - Provision
			// case "52.212-3": special case.
			// case "52.212-3 Alternate I": special case.
			//case "52.211-9":
			case "52.211-9 ALTERNATE III":
			//case "52.211-9 ALTERNATE II":
			//case "52.211-9 ALTERNATE I":

			case "52.211-8 ALTERNATE III":
			case "52.211-8 ALTERNATE II":
			case "52.211-8 ALTERNATE I":
			// case "52.204-8": special case.
			// case "252.229-7001": special case.
			// case "252.229-7001 Alternate I": special case.
			case "252.227-7014 ALTERNATE I":
			// case "252.225-7046": special case.
			case "252.223-7001":
			// case "252.216-7001": special case.
				html = EcfrFillInUtils.processTextAndCheck(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
				break;
		}
		
		if( skipGenericProcessing == true )
			return html;
		
		return HtmlUtils.parseFillinsAndNormalize(html);
	}
	
	private static String process252_225_7982_deviation_2016_00005(int iStg1OfficialClauseId, String html, TemplateStg2ClauseFillInRecord oTemplateStg2ClauseFillInRecord,
			PreparedStatement psFillInInsertClause) throws SQLException {
		
		html = html.replace("(Line Item Number)(Country of Origin)<br>_____  _____",
				"<table><tr><th>(Line Item Number)</th><th>(Country of Origin)</th></tr><tr><td>{{textbox}}</td><td>{{textbox}}</td></tr></table>");
		
		//html = html.replace("policy and contracting activity procedures &nbsp; __]",
		//		"policy and contracting activity procedures {{textbox}} ]");
		
		html = html.replaceFirst(Pattern.quote("[Contracting Officer to specify percent in accordance with the USAFRICOM Commander's policy and contracting activity procedures &nbsp; __]"), "{{textbox_man_252.225-7982_Dev1[0]}}");
		 		
		String sPlaceholder = "Contracting Officer to specify percent in accordance with the USAFRICOM Commander's policy and contracting activity procedures";
		
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
				"textbox_man_252.225-7982_Dev1[0]", "S", 100, // piFillInMaxSize,
				null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
				psFillInInsertClause);	
		
		return html;
		
	}
	
	private static String process252_225_7046(int iStg1OfficialClauseId, String html, TemplateStg2ClauseFillInRecord oTemplateStg2ClauseFillInRecord,
			PreparedStatement psFillInInsertClause) throws SQLException {
		//html = html.replace("[<i>Enter Contract Line Item Number(s) or enter &ldquo;None&rdquor;</i>]", "{{textbox}}<br>[<i>Enter Contract Line Item Number(s) or enter &ldquo;None&rdquor;</i>]");
		
		html = HtmlUtils.normalizeFillins(html);
		html = HtmlUtils.convertUnicodeToHtml(html);
		
		html = html.replace("____ <p>[<i>Enter Contract Line Item Number(s) or enter \"None\"</i>]</p>", "{{textbox_man_252.225-7046[0]}}");
		
		String sPlaceholder = "Enter Contract Line Item Number(s) or enter \"None\"";
		
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
				"textbox_man_252.225-7046[0]", "S", 100, // piFillInMaxSize,
				null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
				psFillInInsertClause);		

		//html = html.replace(_Fillin_Marker, "{{textbox}}&nbsp;");
		//html = EcfrFillInUtils.processTextAndCheck(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
		//html = HtmlUtils.parseFillinsAndNormalize(html);
		
		return html;
	}
	
	private static String process252_225_7047(int iStg1OfficialClauseId, String html, TemplateStg2ClauseFillInRecord oTemplateStg2ClauseFillInRecord,
			PreparedStatement psFillInInsertClause) throws SQLException {
		//html = html.replace("[<i>Enter Contract Line Item Number(s) or enter &ldquo;None&rdquor;</i>]", "{{textbox}}<br>[<i>Enter Contract Line Item Number(s) or enter &ldquo;None&rdquor;</i>]");
		
		html = HtmlUtils.normalizeFillins(html);
		html = HtmlUtils.convertUnicodeToHtml(html);
		
		html = html.replace("____ <p>[<i>Enter Contract Line Item Number(s) or enter \"None\"</i>]</p>", "{{textbox_man_252.225-7047[0]}}");
		
		String sPlaceholder = "Enter Contract Line Item Number(s) or enter \"None\"";
		
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
				"textbox_man_252.225-7047[0]", "S", 100, // piFillInMaxSize,
				null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
				psFillInInsertClause);		

		//html = html.replace(_Fillin_Marker, "{{textbox}}&nbsp;");
		//html = EcfrFillInUtils.processTextAndCheck(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
		//html = HtmlUtils.parseFillinsAndNormalize(html);
		
		return html;
	}

	private static String process252_204_7007(int iStg1OfficialClauseId, String html, TemplateStg2ClauseFillInRecord oTemplateStg2ClauseFillInRecord,
			PreparedStatement psFillInInsertClause) throws SQLException {
		
		html = html.replace("__(i) 252.209-7002", "{{checkbox_man_252.204-7007[0]}} (i) 252.209-7002");
		
		html = html.replace ("[<i>Contracting Officer check as appropriate.</i>]", "");
		String sPlaceholder = "Contracting Officer check as appropriate.";
		
		oTemplateStg2ClauseFillInRecord.set2(html, iStg1OfficialClauseId,
				"checkbox_man_252.204-7007[0]", // psFillInCode,
				"C", // psFillInType,
				1, // piFillInMaxSize,
				1, // piFillInGroupNumber,
				sPlaceholder, // psFillInPlaceholder,
				null, // psFillInDefaultData,
				1, // piFillInDisplayRows,
				false, // pbFillInForTable,
				null // psFillInHeading
				);
		if (psFillInInsertClause != null)
			oTemplateStg2ClauseFillInRecord.insert(psFillInInsertClause);
		
		html = html.replace("__(ii) 252.225-7000", "{{checkbox_man_252.204-7007[1]}} (ii) 252.225-7000");
		html = html.replace("__(iii) 252.225-7020", "{{checkbox_man_252.204-7007[2]}} (iii) 252.225-7020");
		html = html.replace("__(iv) 252.225-7031", "{{checkbox_man_252.204-7007[3]}} (iv) 252.225-7031");
		html = html.replace("__(v) 252.225-7035", "{{checkbox_man_252.204-7007[4]}} (v) 252.225-7035");
		html = html.replaceFirst("__Use with Alternate III", "{{checkbox_man_252.204-7007[5]}} use with Alternate");
		html = html.replaceFirst("__Use with Alternate II", "{{checkbox_man_252.204-7007[6]}} use with Alternate");
		html = html.replaceFirst("__Use with Alternate IV", "{{checkbox_man_252.204-7007[7]}} use with Alternate");
		html = html.replaceFirst("__Use with Alternate I", "{{checkbox_man_252.204-7007[8]}} use with Alternate");
		html = html.replaceFirst("__Use with Alternate I", "{{checkbox_man_252.204-7007[9]}} use with Alternate");
		html = html.replaceFirst("__Use with Alternate V", "{{checkbox_man_252.204-7007[10]}} use with Alternate");
	
		for (Integer ndx = 1; ndx < 11; ndx++)
		{
			String sCode = "checkbox_man_252.204-7007[" + ndx.toString() + "]";

			oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
					sCode, "C", 1, // piFillInMaxSize,
					1, null, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
					psFillInInsertClause);
		}
		
		/*
		// Add the table.
		html = html.replace("<table border=\"1\" cellpadding=\"1\" cellspacing=\"1\" frame=\"void\" width=\"100%\"><thead><tr><th scope=\"col\">FAR/DFARS provision No.</th><th scope=\"col\">Title</th><th scope=\"col\">Date</th><th scope=\"col\">Change</th></tr></thead><tbody><tr><td align=\"left\" scope=\"row\">&nbsp;&nbsp;&nbsp;</td><td align=\"left\"></td><td align=\"right\"></td><td align=\"right\"></td></tr></tbody></table>", 
				"{{tbl_252.209-7002[0]}}{{tbl_252.209-7002[1]}}{{tbl_252.209-7002[2]}}{{tbl_252.209-7002[3]}}");
				
		html = html.replace("[<i>offeror to insert changes, identifying change by provision number, title, date</i>]", "");
		
		oTemplateStg2ClauseFillInRecord.set2(html, iStg1OfficialClauseId,
				"tbl_252.209-7002[0]", // psFillInCode,
				"S", // psFillInType,
				100, // piFillInMaxSize,
				0, // piFillInGroupNumber,
				"Offeror to insert changes, identifying change by provision number, title, date.", // psFillInPlaceholder,
				null, // psFillInDefaultData,
				1, // piFillInDisplayRows,
				true, // pbFillInForTable,
				"FAR/DFARS Provision No." // psFillInHeading
				);
		oTemplateStg2ClauseFillInRecord.insert(psFillInInsertClause);
		
		oTemplateStg2ClauseFillInRecord.set2(html, iStg1OfficialClauseId,
				"tbl_252.209-7002[1]", // psFillInCode,
				"S", // psFillInType,
				300, // piFillInMaxSize,
				0, // piFillInGroupNumber,
				null, // psFillInPlaceholder,
				null, // psFillInDefaultData,
				1, // piFillInDisplayRows,
				true, // pbFillInForTable,
				"Title" // psFillInHeading
				);
		oTemplateStg2ClauseFillInRecord.insert(psFillInInsertClause);
		
		oTemplateStg2ClauseFillInRecord.set2(html, iStg1OfficialClauseId,
				"tbl_252.209-7002[2]", // psFillInCode,
				"S", // psFillInType,
				20, // piFillInMaxSize,
				0, // piFillInGroupNumber,
				null, // psFillInPlaceholder,
				null, // psFillInDefaultData,
				1, // piFillInDisplayRows,
				true, // pbFillInForTable,
				"Date" // psFillInHeading
				);
		oTemplateStg2ClauseFillInRecord.insert(psFillInInsertClause);
		
		oTemplateStg2ClauseFillInRecord.set2(html, iStg1OfficialClauseId,
				"tbl_252.209-7002[3]", // psFillInCode,
				"S", // psFillInType,
				100, // piFillInMaxSize,
				0, // piFillInGroupNumber,
				null, // psFillInPlaceholder,
				null, // psFillInDefaultData,
				1, // piFillInDisplayRows,
				true, // pbFillInForTable,
				"Change" // psFillInHeading
				);
		oTemplateStg2ClauseFillInRecord.insert(psFillInInsertClause);
		*/
		
		return html;
	}
	
	private static String process252_216_7001(int iStg1OfficialClauseId, String html, TemplateStg2ClauseFillInRecord oTemplateStg2ClauseFillInRecord,
			PreparedStatement psFillInInsertClause) throws SQLException {
		
		html = html.replace("including all applicable extras of $____", "including all applicable extras of ${{textbox}}");
		html = html.replace("per _____ (see note 1) for", "per {{textbox}} (see note 1) for");
		html = html.replace("_____ (see note 2) on the date set for bid", "{{textbox}} (see note 2) on the date set for bid");
		html = html.replace("including all applicable extras in effect ____", "including all applicable extras in effect {{textbox}}");
		html = html.replace("hourly earnings of the Contractor's employees in the ____", "hourly earnings of the Contractor's employees in the {{textbox}}");
		html = html.replace("shop of the Contractor's _____ plant", "shop of the Contractor's {{textbox}} plant");
		
		// CJ-494, Per Larry, shouldn't all blanks be fillins for 252.216-7001?
		html = html.replace("definition in paragraph (a) shall be ", "definition in paragraph (a) shall be {{textbox}}");
		html = html.replace("and the proportion of the contract unit price attributable to the cost of steel shall be ", 
				"and the proportion of the contract unit price attributable to the cost of steel shall be {{textbox}}");
		html = html.replace("The adjusted cost of labor (obtained by multiplying ", "The adjusted cost of labor (obtained by multiplying {{textbox}}");
		html = html.replace("The adjusted cost of steel (obtained by multiplying ", "The adjusted cost of steel (obtained by multiplying {{textbox}}");
		html = html.replace("(iii) The amount equal to ", "(iii) The amount equal to {{textbox}}");
		html = html.replace("___ percent", " percent");
		
		html = EcfrFillInUtils.processTextAndCheck(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);

		return html;
	}
	
	private static String process52_212_3(int iStg1OfficialClauseId, String html, TemplateStg2ClauseFillInRecord oTemplateStg2ClauseFillInRecord,
			PreparedStatement psFillInInsertClause) throws SQLException {
		
		html = HtmlUtils.normalizeFillins(html);
		/*
		html = html.replaceFirst(Pattern.quote("____ .</p><p>[<i>Offeror to identify the applicable paragraphs at (c) through (r) of this provision that the offeror has completed for the purposes of this solicitation only, if any."), "{{textbox_man_52.212-3[0]}}");
		html = html.replaceFirst(Pattern.quote("<p><i>These amended representation(s) and/or certification(s) are also incorporated in this offer and are current, accurate, and complete as of the date of this offer.</i></p><p><i>Any changes provided by the offeror are applicable to this solicitation only, and do not result in an update to the representations and certifications posted electronically on SAM.</i>]</p>"), "");

		String sPlaceholder = "Offeror to identify the applicable paragraphs at (c) through (r) of this provision that the offeror has completed for the purposes of this solicitation only, if any. These amended representation(s) and/or certification(s) are also incorporated in this offer and are current, accurate, and complete as of the date of this offer. Any changes provided by the offeror are applicable to this solicitation only, and do not result in an update to the representations and certifications posted electronically on SAM." ;
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.212-3[0]", "M", 1000, // piFillInMaxSize, 
			null, sPlaceholder, 5, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);

		html = html.replaceFirst(Pattern.quote("[<i>Complete only if the offeror represented itself as a small business concern in paragraph (c)(1) of this provision.</i>]"), "{{textbox_man_52.212-3[1]}}");

		sPlaceholder = "Complete only if the offeror represented itself as a small business concern in paragraph (c)(1) of this provision.";

		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.212-3[1]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);

		html = html.replaceFirst(Pattern.quote("[<i>Complete only if the offeror represented itself as a veteran-owned small business concern in paragraph (c)(2) of this provision.</i>]"), "{{textbox_man_52.212-3[2]}}");

		sPlaceholder = "Complete only if the offeror represented itself as a veteran-owned small business concern in paragraph (c)(2) of this provision.";

		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.212-3[2]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);

		html = html.replaceFirst(Pattern.quote("[Complete only if the offeror represented itself as a small business concern in paragraph (c)(1) of this provision.]"), "{{textbox_man_52.212-3[3]}}");

		sPlaceholder = "Complete only if the offeror represented itself as a small business concern in paragraph (c)(1) of this provision.";

		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.212-3[3]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);

		html = html.replaceFirst(Pattern.quote("[Complete only if the offeror represented itself as a small business concern in paragraph (c)(1) of this provision.]"), "{{textbox_man_52.212-3[4]}}");

		sPlaceholder = "Complete only if the offeror represented itself as a small business concern in paragraph (c)(1) of this provision.";

		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.212-3[4]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);

		html = html.replaceFirst(Pattern.quote("[<i>Complete only if the offeror represented itself as a women-owned small business concern in paragraph (c)(5) of this provision.</i>]"), "{{textbox_man_52.212-3[5]}}");

		sPlaceholder = "Complete only if the offeror represented itself as a women-owned small business concern in paragraph (c)(5) of this provision.";

		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.212-3[5]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);

		html = html.replaceFirst(Pattern.quote("[<i>Complete only if the offeror represented itself as a WOSB concern eligible under the WOSB Program in (c)(6) of this provision.</i>]"), "{{textbox_man_52.212-3[6]}}");
		
		sPlaceholder = "Complete only if the offeror represented itself as a WOSB concern eligible under the WOSB Program in (c)(6) of this provision.";

		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.212-3[6]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);

		html = html.replaceFirst(Pattern.quote("[Complete only if the offeror is a women-owned business concern and did not represent itself as a small business concern in paragraph (c)(1) of this provision.]"), "{{textbox_man_52.212-3[7]}}");

		sPlaceholder = "Complete only if the offeror is a women-owned business concern and did not represent itself as a small business concern in paragraph (c)(1) of this provision.";

		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.212-3[7]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);

		html = html.replaceFirst(Pattern.quote("[Complete only if the offeror represented itself as a small business concern in paragraph (c)(1) of this provision.]"), "{{textbox_man_52.212-3[8]}}");

		sPlaceholder = "Complete only if the offeror represented itself as a small business concern in paragraph (c)(1) of this provision.";

		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.212-3[8]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);

		html = html.replaceFirst(Pattern.quote("[<i>The offeror shall enter the name or names of the WOSB concern eligible under the WOSB Program and other small businesses that are participating in the joint venture:</i> ____ .]"), 
				"[<i>The offeror shall enter the name or names of the WOSB concern eligible under the WOSB Program and other small businesses that are participating in the joint venture:</i> {{textbox_man_52.212-3[20]}}.]");
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
				"textbox_man_52.212-3[20]", "S", 100, // piFillInMaxSize, 
				null, null, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
				psFillInInsertClause);	
		
		html = html.replaceFirst(Pattern.quote("[<i>The offeror shall enter the name or names of the EDWOSB concern and other small businesses that are participating in the joint venture:</i> ____ .]"), 
				"[<i>The offeror shall enter the name or names of the EDWOSB concern and other small businesses that are participating in the joint venture:</i> {{textbox_man_52.212-3[21]}}.]");
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
				"textbox_man_52.212-3[21]", "S", 100, // piFillInMaxSize, 
				null, null, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
				psFillInInsertClause);	
		
		html = html.replaceFirst(Pattern.quote("[<i>The offeror shall enter the names of each of the HUBZone small business concerns participating in the HUBZone joint venture: ____ .</i>]"), 
				"[<i>The offeror shall enter the names of each of the HUBZone small business concerns participating in the HUBZone joint venture:</i> {{textbox_man_52.212-3[22]}}.]");
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
				"textbox_man_52.212-3[22]", "S", 100, // piFillInMaxSize, 
				null, null, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
				psFillInInsertClause);	
		
		html = html.replaceFirst(Pattern.quote("[List as necessary]"), "{{textbox_man_52.212-3[9]}}");

		sPlaceholder = "List as necessary";

		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.212-3[9]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
		*/

		html = html.replaceFirst(Pattern.quote("[The Contracting Officer must list in paragraph (i)(1) any end products being acquired under this solicitation that are included in the List of Products Requiring Contractor Certification as to Forced or Indentured Child Labor, unless excluded at 22.1503(b).</i>]"), "{{textbox_man_52.212-3[10]}}");

		String sPlaceholder = "The Contracting Officer must list in paragraph (i)(1) any end products being acquired under this solicitation that are included in the List of Products Requiring Contractor Certification as to Forced or Indentured Child Labor, unless excluded at 22.1503(b).";

		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.212-3[10]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);

		html = html.replaceFirst(Pattern.quote("[<i>If the Contracting Officer has identified end products and countries of origin in paragraph (i)(1) of this provision, then the offeror must certify to either (i)(2)(i) or (i)(2)(ii) by checking the appropriate block.</i>]"), "{{textbox_man_52.212-3[11]}}");

		sPlaceholder = "If the Contracting Officer has identified end products and countries of origin in paragraph (i)(1) of this provision, then the offeror must certify to either (i)(2)(i) or (i)(2)(ii) by checking the appropriate block.";

		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.212-3[11]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);

		html = html.replaceFirst(Pattern.quote("[<i>The contracting officer is to check a box to indicate if paragraph (k)(1) or (k)(2) applies.</i>]"), "{{textbox_man_52.212-3[12]}}");

		sPlaceholder = "The contracting officer is to check a box to indicate if paragraph (k)(1) or (k)(2) applies.";

		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.212-3[12]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
		
		html = html.replace("(see FAR 4.1201), except for paragraphs _____", "(see FAR 4.1201), except for paragraphs {{textbox}}");
				
		// CJ-494, Replace all End Products/Countries with fillins
		html = html.replace("Listed End Product</h3>&nbsp; ____  ____ <p>Listed Countries of Origin </p>&nbsp; ____  ____", 
				"Listed End Product</h3>&nbsp; {{textbox}}&nbsp;  {{textbox}}<p>Listed Countries of Origin </p>&nbsp; {{textbox}}&nbsp; {{textbox}}");
	
		html = html.replace("The offeror ____  does ____  does not certify", 
				"The offeror {{checkbox}}&nbsp;&nbsp;does {{checkbox}}&nbsp;&nbsp;does not certify");
				
		html = html.replace("(1)____  Maintenance, calibration, or repair",
				"(1){{checkbox}}&nbsp;&nbsp; Maintenance, calibration, or repair");
		html = html.replace("(2)____  Certain services as described in FAR 22.1003-4(d)",
				"(2){{checkbox}}&nbsp;&nbsp;Certain services as described in FAR 22.1003-4(d)");
		
		//html = html.replace(_Fillin_Marker, "{{textbox}}&nbsp;");
		//html = EcfrFillInUtils.processTextAndCheck(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
		//html = HtmlUtils.parseFillinsAndNormalize(html);
		

		return html;
	}
	
	private static String process52_212_3_Alt1(int iStg1OfficialClauseId, String html, TemplateStg2ClauseFillInRecord oTemplateStg2ClauseFillInRecord,
			PreparedStatement psFillInInsertClause) throws SQLException {
		//52.212-3 Alternate I // changed &squ; to _____ for CJ-672
		html = html.replace("_Black American", "_____ Black American"); 
		html = html.replace("_Hispanic American", "_____ Hispanic American");
		html = html.replace("_Native American", "_____ Native American");
		html = html.replace("_Asian-Pacific American", "_____ Asian-Pacific American");
		html = html.replace("_Subcontinent Asian", "_____ Subcontinent Asian");
		html = html.replace("_Individual/concern", "_____ Individual/concern");
		
		html = html.replace("[_] Yes or [_] No", "_____ Yes or _____ No");
		html = html.replace("that it [_] has or [_] does not", "that it _____ has or _____ does not");
		
		return html;
	}
	
	private static String process52_204_8(int iStg1OfficialClauseId, String html, TemplateStg2ClauseFillInRecord oTemplateStg2ClauseFillInRecord,
			PreparedStatement psFillInInsertClause) throws SQLException {
		//html = html.replace("_______ [<i>insert NAICS code</i>]", "{{textbox}} [<i>insert NAICS code</i>]");
		//html = html.replace("_______ [<i>insert size standard</i>]", "{{textbox}} [<i>insert size standard</i>]");
		
		html = HtmlUtils.normalizeFillins(html);
		html = html.replaceFirst(Pattern.quote("____  [<i>insert NAICS code</i>]"), "{{textbox_man_52.204-8[0]}}");

		String sPlaceholder = "insert NAICS code";

		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.204-8[0]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);

		html = html.replaceFirst(Pattern.quote("____  [<i>insert size standard</i>]"), "{{textbox_man_52.204-8[1]}}");

		sPlaceholder = "insert size standard";

		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.204-8[1]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);

		/*
		html = html.replaceFirst(Pattern.quote("[<i>offeror to insert changes, identifying change by clause number, title, date</i>]"), "{{textbox_man_52.204-8[3]}}");

		sPlaceholder = "offeror to insert changes, identifying change by clause number, title, date";

		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.204-8[3]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
		*/ 
		
		html = html.replaceFirst(Pattern.quote("[<i>Contracting Officer check as appropriate.</i>]"), "");
				
		html = html.replace("_ (i) 52.204-17", "{{checkbox_man_52.204-8[0]}} (i) 52.204-17");
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
				"checkbox_man_52.204-8[0]", "C", 1, // piFillInMaxSize, 
				1, "Contracting Officer check as appropriate." , 1, 			// piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
				psFillInInsertClause);

		// Add the checkboxes
		
		Integer ndx=1;
		while (html.indexOf("_ (") > 0)
		{
			String sCode = "checkbox_man_52.204-8[" + ndx.toString() + "]";
			html = html.replaceFirst(Pattern.quote("_ ("), "{{" + sCode + "}}&nbsp;(");

			oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
					sCode, "C", 1, // piFillInMaxSize,
					1, null, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
					psFillInInsertClause);
			ndx++;
		}
		
		/*
		html = html.replace("_ (ii) 52.222-18", "{{checkbox}} (ii) 52.222-18");
		html = html.replace("_ (iii) 52.222-48", "{{checkbox}} (iii) 52.222-48");
		html = html.replace("_ (iv) 52.222-52", "{{checkbox}} (iv) 52.222-52");
		html = html.replace("_ (v) 52.223-9", "{{checkbox}} (v) 52.223-9");
		html = html.replace("_ (vi) 52.227-6", "{{checkbox}} (vi) 52.227-6");
		html = html.replace("_ (A) Basic", "{{checkbox}} (A) Basic");
		html = html.replace("_ (B) Alternate I", "{{checkbox}} (B) Alternate I");
		html = html.replace("_ (vii) 52.227-15", "{{checkbox}} (vii) 52.227-15");
		*/
		
		return html;
	}	
	

	private static String process252_241_7000(int iStg1OfficialClauseId, String html, TemplateStg2ClauseFillInRecord oTemplateStg2ClauseFillInRecord,
			PreparedStatement psFillInInsertClause) throws SQLException {
		/*
		// '<p>This contract supersedes contract No. {{textbox_252.241-7000[0]}}, dated {{textbox_252.241-7000[1]}} which provided similar services. Any capital credits accrued to the Government, any remaining credits due to the Government under the connection charge, or any termination liability are transferred to this contract, as follows:</p><h1>Capital Credits</h1><p>(List years and accrued credits by year and separate delivery points.)</p><h1>Outstanding Connection Charge Credits</h1><p>(List by month and year the amount credited and show the remaining amount of outstanding credits due the Government.)</p><h1>Termination Liability Charges</h1><p>(List by month and year the amount of monthly facility cost recovered and show the remaining amount of facility cost to be recovered.)</p>'
		String[] aMemoLabels = {
			"(List years and accrued credits by year and separate delivery points.)",
			"(List by month and year the amount credited and show the remaining amount of outstanding credits due the Government.)",
			"(List by month and year the amount of monthly facility cost recovered and show the remaining amount of facility cost to be recovered.)"
		};
		
		for (String sLabel : aMemoLabels) {
			if (html.contains(sLabel)) {
				//maxSize;fillInDisplayRows;fillInPlaceholder;fillInDefaultData;
				String sMemo = "<br/>{{memo}}";
				html = html.replace(sLabel, sLabel + sMemo);
			}
		}
		*/
		String sLookfor = "(List years and accrued credits by year and separate delivery points.)";
		if (html.contains(sLookfor)) {
			String sMemo = "<br/>{{capital_252.241-7000[0]}}{{capital_252.241-7000[1]}}{{capital_252.241-7000[2]}}";
			html = html.replace(sLookfor, sMemo);
			oTemplateStg2ClauseFillInRecord.set2(html, iStg1OfficialClauseId,
					"capital_252.241-7000[0]", // psFillInCode,
					"N", // psFillInType,
					4, // piFillInMaxSize,
					0, // piFillInGroupNumber,
					"List years and accrued credits by year and separate delivery points.", // psFillInPlaceholder,
					null, // psFillInDefaultData,
					5, // piFillInDisplayRows,
					true, // pbFillInForTable,
					"Year" // psFillInHeading
					);
			oTemplateStg2ClauseFillInRecord.insert(psFillInInsertClause);
			
			oTemplateStg2ClauseFillInRecord.set2(html, iStg1OfficialClauseId,
					"capital_252.241-7000[1]", // psFillInCode,
					"$", // psFillInType,
					12, // piFillInMaxSize,
					0, // piFillInGroupNumber,
					null, // psFillInPlaceholder,
					null, // psFillInDefaultData,
					5, // piFillInDisplayRows,
					true, // pbFillInForTable,
					"Accrued credits" // psFillInHeading
					);
			oTemplateStg2ClauseFillInRecord.insert(psFillInInsertClause);

			oTemplateStg2ClauseFillInRecord.set2(html, iStg1OfficialClauseId,
					"capital_252.241-7000[2]", // psFillInCode,
					"S", // psFillInType,
					60, // piFillInMaxSize,
					0, // piFillInGroupNumber,
					null, // psFillInPlaceholder,
					null, // psFillInDefaultData,
					5, // piFillInDisplayRows,
					true, // pbFillInForTable,
					"Delivery points" // psFillInHeading
					);
			oTemplateStg2ClauseFillInRecord.insert(psFillInInsertClause);
		}
			

		sLookfor = "(List by month and year the amount credited and show the remaining amount of outstanding credits due the Government.)";
		if (html.contains(sLookfor)) {
			String sMemo = "<br/>{{outstanding_252.241-7000[0]}}{{outstanding_252.241-7000[1]}}{{outstanding_252.241-7000[2]}}{{outstanding_252.241-7000[3]}}";
			html = html.replace(sLookfor, sMemo);
			oTemplateStg2ClauseFillInRecord.set2(html, iStg1OfficialClauseId
					, "outstanding_252.241-7000[0]" // fill_in_code
					, "S" // fill_in_type
					, 3 // fill_in_max_size
					, 1 // fill_in_group_number
					, "List by month and year the amount credited and show the remaining amount of outstanding credits due the Government." // fill_in_placeholder
					, null // fill_in_default_data
					, 5 // fill_in_display_rows
					, true // fill_in_for_table
					, "Month");
			oTemplateStg2ClauseFillInRecord.insert(psFillInInsertClause);
			
			oTemplateStg2ClauseFillInRecord.set2(html, iStg1OfficialClauseId
					, "outstanding_252.241-7000[1]" // fill_in_code
					, "N" // fill_in_type
					, 4 // fill_in_max_size
					, 1 // fill_in_group_number
					, null // fill_in_placeholder
					, null // fill_in_default_data
					, 5 // fill_in_display_rows
					, true // fill_in_for_table
					, "Year");
			oTemplateStg2ClauseFillInRecord.insert(psFillInInsertClause);

			oTemplateStg2ClauseFillInRecord.set2(html, iStg1OfficialClauseId
					, "outstanding_252.241-7000[2]" // fill_in_code
					, "$" // fill_in_type
					, 12 // fill_in_max_size
					, 1 // fill_in_group_number
					, null // fill_in_placeholder
					, null // fill_in_default_data
					, 5 // fill_in_display_rows
					, true // fill_in_for_table
					, "Amount credited");
			oTemplateStg2ClauseFillInRecord.insert(psFillInInsertClause);
			
			oTemplateStg2ClauseFillInRecord.set2(html, iStg1OfficialClauseId
					, "outstanding_252.241-7000[3]" // fill_in_code
					, "$" // fill_in_type
					, 12 // fill_in_max_size
					, 1 // fill_in_group_number
					, null // fill_in_placeholder
					, null // fill_in_default_data
					, 5 // fill_in_display_rows
					, true // fill_in_for_table
					, "Remaining amount");
			oTemplateStg2ClauseFillInRecord.insert(psFillInInsertClause);
		}

		sLookfor = "(List by month and year the amount of monthly facility cost recovered and show the remaining amount of facility cost to be recovered.)";
		if (html.contains(sLookfor)) {
			String sMemo = "<br/>{{termination_252.241-7000[0]}}{{termination_252.241-7000[1]}}{{termination_252.241-7000[2]}}{{termination_252.241-7000[3]}}";
			html = html.replace(sLookfor, sMemo);
			oTemplateStg2ClauseFillInRecord.set2(html, iStg1OfficialClauseId
					, "termination_252.241-7000[0]" // fill_in_code
					, "S" // fill_in_type
					, 3 // fill_in_max_size
					, 2 // fill_in_group_number
					, "List by month and year the amount of monthly facility cost recovered and show the remaining amount of facility cost to be recovered." // fill_in_placeholder
					, null // fill_in_default_data
					, 5 // fill_in_display_rows
					, true // fill_in_for_table
					, "Month");
			oTemplateStg2ClauseFillInRecord.insert(psFillInInsertClause);
			
			oTemplateStg2ClauseFillInRecord.set2(html, iStg1OfficialClauseId
					, "termination_252.241-7000[1]" // fill_in_code
					, "N" // fill_in_type
					, 4 // fill_in_max_size
					, 2 // fill_in_group_number
					, null // fill_in_placeholder
					, null // fill_in_default_data
					, 5 // fill_in_display_rows
					, true // fill_in_for_table
					, "Year");
			oTemplateStg2ClauseFillInRecord.insert(psFillInInsertClause);

			oTemplateStg2ClauseFillInRecord.set2(html, iStg1OfficialClauseId
					, "termination_252.241-7000[2]" // fill_in_code
					, "$" // fill_in_type
					, 12 // fill_in_max_size
					, 2 // fill_in_group_number
					, null // fill_in_placeholder
					, null // fill_in_default_data
					, 5 // fill_in_display_rows
					, true // fill_in_for_table
					, "Cost recovered");
			oTemplateStg2ClauseFillInRecord.insert(psFillInInsertClause);
			
			oTemplateStg2ClauseFillInRecord.set2(html, iStg1OfficialClauseId
					, "termination_252.241-7000[3]" // fill_in_code
					, "$" // fill_in_type
					, 12 // fill_in_max_size
					, 2 // fill_in_group_number
					, null // fill_in_placeholder
					, null // fill_in_default_data
					, 5 // fill_in_display_rows
					, true // fill_in_for_table
					, "Remaining amount");
			oTemplateStg2ClauseFillInRecord.insert(psFillInInsertClause);
		}
		return html;
	}
	
	private static String process52_247_2(int iStg1OfficialClauseId, String html, TemplateStg2ClauseFillInRecord oTemplateStg2ClauseFillInRecord,
			PreparedStatement psFillInInsertClause) throws SQLException {
		

		html = html.replace("does ____", "does {{checkbox}}");
		html = html.replace("does not ____", "does not {{checkbox}}");
		
		html = EcfrFillInUtils.processTextAndCheck(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);

		return html;
	}
	
	private static String process52_226_3(int iStg1OfficialClauseId, String html, TemplateStg2ClauseFillInRecord oTemplateStg2ClauseFillInRecord,
			PreparedStatement psFillInInsertClause) throws SQLException {
		
		html = HtmlUtils.normalizeFillins(html);
		html = html.replaceFirst(Pattern.quote("____  [<i>Contracting Officer to fill in with definite geographic boundaries.</i>]"), "{{textbox_man_52.226-3[0]}}");

		String sPlaceholder = "Contracting Officer to fill in with definite geographic boundaries.";

		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.226-3[0]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);

		//html = html.replace("does ____", "does {{checkbox}}");
		//html = html.replace("does not ____", "does not {{checkbox}}");
		
		//html = html.replace(_Fillin_Marker, "{{textbox}}&nbsp;");
		//html = EcfrFillInUtils.processTextAndCheck(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
		//html = HtmlUtils.parseFillinsAndNormalize(html);

		return html;
	}
	
	private static String process252_229_7001(int iStg1OfficialClauseId, String html, TemplateStg2ClauseFillInRecord oTemplateStg2ClauseFillInRecord,
			PreparedStatement psFillInInsertClause) throws SQLException {
				
		html = HtmlUtils.normalizeFillins(html);
		html = HtmlUtils.convertUnicodeToHtml(html);
		
		
		html = html.replace("<p>NAME OF TAX: (Offeror Insert) RATE (PERCENTAGE): (Offeror Insert)</p>",
				"<p>NAME OF TAX: {{textbox_man_252.229-7001[0]}} RATE (PERCENTAGE): {{textbox_man_252.229-7001[1]}}</p>");
		
		String sPlaceholder = "Offeror Insert";
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
				"textbox_man_252.229-7001[0]", "S", 100, // piFillInMaxSize,
				null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
				psFillInInsertClause);		
		
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
				"textbox_man_252.229-7001[1]", "S", 100, // piFillInMaxSize,
				null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
				psFillInInsertClause);	
		
		html = html.replace(_Fillin_Marker, "{{textbox}}&nbsp;");
		html = EcfrFillInUtils.processTextAndCheck(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
		html = HtmlUtils.parseFillinsAndNormalize(html);
		
		return html;
	}
	
	private static String process252_229_7001_AltI(int iStg1OfficialClauseId, String html, TemplateStg2ClauseFillInRecord oTemplateStg2ClauseFillInRecord,
			PreparedStatement psFillInInsertClause) throws SQLException {
				
		html = HtmlUtils.normalizeFillins(html);
		html = HtmlUtils.convertUnicodeToHtml(html);
		
		
		html = html.replace("<p>NAME OF TAX: <i>[Offeror insert]</i></p><p>RATE (PERCENTAGE): <i>[Offeror insert]</i></p>",
				"<p>NAME OF TAX: {{textbox_man_252.229-7001_AltI[0]}}</p><p>RATE (PERCENTAGE): {{textbox_man_252.229-7001_AltI[1]}}</p>");
		
		String sPlaceholder = "Offeror Insert";
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
				"textbox_man_252.229-7001_AltI[0]", "S", 100, // piFillInMaxSize,
				null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
				psFillInInsertClause);		
		
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
				"textbox_man_252.229-7001_AltI[1]", "S", 100, // piFillInMaxSize,
				null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
				psFillInInsertClause);	
		
		html = html.replace(_Fillin_Marker, "{{textbox}}&nbsp;");
		html = EcfrFillInUtils.processTextAndCheck(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
		html = HtmlUtils.parseFillinsAndNormalize(html);
		
		return html;
	}
	
	private static String process52_219_4(int iStg1OfficialClauseId, String html, TemplateStg2ClauseFillInRecord oTemplateStg2ClauseFillInRecord,
			PreparedStatement psFillInInsertClause) throws SQLException {
		

		html = html.replace("____&nbsp;&nbsp;Offeror elects to waive the evaluation preference.", "{{checkbox}} &nbsp;&nbsp;Offeror elects to waive the evaluation preference.");

		html = EcfrFillInUtils.processTextAndCheck(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);

		return html;
	}
	
	private static String process252_232_7006 (int iStg1OfficialClauseId, String html, 
			TemplateStg2ClauseFillInRecord oTemplateStg2ClauseFillInRecord, 
			PreparedStatement psFillInInsertClause) throws SQLException {

			html = HtmlUtils.normalizeFillins(html);
			html = html.replaceFirst(Pattern.quote("____ <p>(<i>Contracting Officer: Insert applicable document type(s). Note: If a \"Combo\" document type is identified but not supportable by the Contractor's business systems, an \"Invoice\" (stand-alone) and \"Receiving Report\" (stand-alone) document type may be used instead.</i>)"), "{{textbox_man_252.232-7006[0]}}");

			String sPlaceholder = "Contracting Officer: Insert applicable document type(s). Note: If a \"Combo\" document type is identified but not supportable by the Contractor's business systems, an \"Invoice\" (stand-alone) and \"Receiving Report\" (stand-alone) document type may be used instead.";

			oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
				"textbox_man_252.232-7006[0]", "M", 500, // piFillInMaxSize, 
				null, sPlaceholder, 5, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
				psFillInInsertClause);

			html = html.replaceFirst(Pattern.quote("____ <p>(<i>Contracting Officer: Insert inspection and acceptance locations or \"Not applicable.\"</i>)"), "{{textbox_man_252.232-7006[1]}}");

			sPlaceholder = "Contracting Officer: Insert inspection and acceptance locations or \"Not applicable.\"";

			oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
				"textbox_man_252.232-7006[1]", "M", 500, // piFillInMaxSize, 
				null, sPlaceholder, 5, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
				psFillInInsertClause);

			html = html.replaceFirst(Pattern.quote("(<i>Contracting Officer: Insert applicable DoDAAC information or \"See schedule\" if multiple ship to/acceptance locations apply, or \"Not applicable.\"</i>)"), "{{textbox_man_252.232-7006[2]}}");

			sPlaceholder = "Contracting Officer: Insert applicable DoDAAC information or \"See schedule\" if multiple ship to/acceptance locations apply, or \"Not applicable.\"";

			oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
				"textbox_man_252.232-7006[2]", "M", 500, // piFillInMaxSize, 
				null, sPlaceholder, 5, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
				psFillInInsertClause);
			
			html = html.replaceFirst(Pattern.quote("____ <p>(<i>Contracting Officer: Insert applicable email addresses or \"Not applicable.\"</i>)</p>"), "<p>{{textbox_man_252.232-7006[3]}}</p>");
			sPlaceholder = "Contracting Officer: Insert applicable email addresses or \"Not applicable.\"";

			oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
				"textbox_man_252.232-7006[3]", "M", 200, // piFillInMaxSize, 
				null, sPlaceholder, 2, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
				psFillInInsertClause);
			
			html = html.replaceFirst(Pattern.quote("____ <p>(<i>Contracting Officer: Insert applicable information or \"Not applicable.\"</i>)</p>"), "<p>{{textbox_man_252.232-7006[4]}}</p>");
			sPlaceholder = "Contracting Officer: Insert applicable information or \"Not applicable.\"";

			oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
				"textbox_man_252.232-7006[4]", "M", 500, // piFillInMaxSize, 
				null, sPlaceholder, 5, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
				psFillInInsertClause);
			
			html = html.replace(_Fillin_Marker, "{{textbox}}&nbsp;");
			html = EcfrFillInUtils.processTextAndCheck(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
			html = HtmlUtils.parseFillinsAndNormalize(html);

			return html;
		}

	
	private static String process52_209_1(int iStg1OfficialClauseId, String html, TemplateStg2ClauseFillInRecord oTemplateStg2ClauseFillInRecord,
			PreparedStatement psFillInInsertClause) throws SQLException {
		
		html = EcfrFillInUtils.processTextLines(html);

		html = html.replace("(Name)", "<p>(Name)");
		html = html.replace("(Address) ____", "</p><p>(Address) ____</p>");
		
		html = html.replace("Offeror's Name", "<p>Offeror's Name");
		html = html.replace("Manufacturer's Name ", "</p><p>Manufacturer's Name ");
		html = html.replace("Source's Name", "</p><p>Source's Name");
		html = html.replace("Item Name", "</p><p>Item Name ");
		html = html.replace("Service Identification", "</p><p>Service Identification");
		
		html = html.replace("Test Number ____", "</p><p>Test Number ____</p>");
		
		html = EcfrFillInUtils.processTextAndCheck(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
		//html = HtmlUtils.parseFillinsAndNormalize(html);
		
		return html;
	}
	
	private static String process52_242_4(int iStg1OfficialClauseId, String html, TemplateStg2ClauseFillInRecord oTemplateStg2ClauseFillInRecord,
			PreparedStatement psFillInInsertClause) throws SQLException {
		
		html = HtmlUtils.normalizeFillins(html);
		html = html.replaceFirst(Pattern.quote("(<i>identify proposal and date</i>)"), "{{textbox_man_52.242-4[0]}}");

		String sPlaceholder = "identify proposal and date";

		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.242-4[0]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);

		html = html.replaceFirst(Pattern.quote("(<i>identify period covered by rate</i>)"), "{{textbox_man_52.242-4[1]}}");

		sPlaceholder = "identify period covered by rate";

		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.242-4[1]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
		
		html = EcfrFillInUtils.processTextLines(html);
		
		html = html.replace("Firm:", "<p>Firm:");
		html = html.replace("Signature:", "</p><p>Signature:");
		html = html.replace("Name of Certifying Official:", "</p><p>Name of Certifying Official:");
		html = html.replace("Title:", "</p><p>Title:");
		html = html.replace("Date of Execution: ____", "</p><p>Date of Execution: ____</p>");
		
		html = html.replace(_Fillin_Marker, "{{textbox}}&nbsp;");
		html = EcfrFillInUtils.processTextAndCheck(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
		html = HtmlUtils.parseFillinsAndNormalize(html);
		
		return html;
	}

	private static String process252_211_7005(int iStg1OfficialClauseId, String html, TemplateStg2ClauseFillInRecord oTemplateStg2ClauseFillInRecord,
			PreparedStatement psFillInInsertClause) throws SQLException {

		html = EcfrFillInUtils.processTextLines(html);
		
		html = html.replace("SPI Process:", "<p>SPI Process:");
		html = html.replace("Facility:", "</p><p>Facility:");
		html = html.replace("Military or Federal Specification or Standard:", "</p><p>Military or Federal Specification or Standard:");
		html = html.replace("Affected Contract Line Item Number, Subline Item Number, Component, or Element: ____", 
				"</p><p>Affected Contract Line Item Number, Subline Item Number, Component, or Element: ____</p>");
		
		html = EcfrFillInUtils.processTextAndCheck(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
		html = HtmlUtils.parseFillinsAndNormalize(html);
		
		return html;
	}
	
	private static String process252_208_7000(int iStg1OfficialClauseId, String html, TemplateStg2ClauseFillInRecord oTemplateStg2ClauseFillInRecord,
			PreparedStatement psFillInInsertClause) throws SQLException {

		html = "<p>(a) The Government intends to furnish precious metals required in the manufacture of items to be delivered under the contract if the Contracting Officer determines it to be in the Government's best interest. The use of Government-furnished silver is mandatory when the quantity required is one hundred troy ounces or more. The precious metal(s) will be furnished pursuant to the Government Furnished Property clause of the contract.</p><p>(b) The Offeror shall cite the type (silver, gold, platinum, palladium, iridium, rhodium, and ruthenium) and quantity in whole troy ounces of precious metals required in the performance of this contract (including precious metals required for any first article or production sample), and shall specify the national stock number (NSN) and nomenclature, if known, of the deliverable item requiring precious metals.</p><div><div><table border=\"1\" cellpadding=\"1\" cellspacing=\"1\" frame=\"void\" width=\"100%\"><thead><tr><th scope=\"col\">Precious metal*</th><th scope=\"col\">Quantity</th><th scope=\"col\">Deliverable item (NSN and nomenclature)</th></tr><tr><td>{{textbox}}</td><td>{{textbox}}</td><td>{{textbox}}</td></tr></thead><tbody><tr><td>{{textbox}}</td><td>{{textbox}}</td><td>{{textbox}}</td></tr><tr><td>{{textbox}}</td><td>{{textbox}}</td><td>{{textbox}}</td></tr><tr><td>{{textbox}}</td><td>{{textbox}}</td><td>{{textbox}}</td></tr><tr><td>{{textbox}}</td><td>{{textbox}}</td><td>{{textbox}}</td></tr></tbody></table></div><div><p>*If platinum or palladium, specify whether sponge or granules are required.</p></div></div><p>(c) Offerors shall submit two prices for each deliverable item which contains precious metals-one based on the Government furnishing precious metals, and one based on the Contractor furnishing precious metals. Award will be made on the basis which is in the best interest of the Government.</p><p>(d) The Contractor agrees to insert this clause, including this paragraph (d), in solicitations for subcontracts and purchase orders issued in performance of this contract, unless the Contractor knows that the item being purchased contains no precious metals.</p>";
		
		return html;
	}
	
	/*
	private static String process52_260_3_Alt_II(int iStg1OfficialClauseId, String html, TemplateStg2ClauseFillInRecord oTemplateStg2ClauseFillInRecord,
			PreparedStatement psFillInInsertClause) throws SQLException { // CJ-1075
		// <p>Alternate II (FEB 2009). As prescribed in 50.206(b)(3), substitute the following paragraph (f):</p><p>(f)(1) Offerors are authorized to submit offers presuming that SAFETY Act designation (or SAFETY Act certification, if a block certification exists) will be obtained before or after award.</p><p>(2) An offeror is eligible for award only if the offeror&mdash;</p><p>(i) Files a SAFETY Act designation (or SAFETY Act certification) application, limited to the scope of the applicable block designation (or block certification), within 15 days after submission of the proposal;</p><p>(ii) Pursues its SAFETY Act designation (or SAFETY Act certification) application in good faith; and</p><p>(iii) Agrees to obtain the amount of insurance DHS requires for issuing the offeror's SAFETY Act designation (or SAFETY Act certification).</p><p>(3) If DHS has not issued a SAFETY Act designation (or SAFETY Act certification) to the successful offeror before contract award, the contracting officer will include the clause at 52.250-5 in the resulting contract.</p>
		String sFillText = "(i) Files a SAFETY Act designation (or SAFETY Act certification) application, limited to the scope of the applicable block designation (or block certification), within 15 days after submission of the proposal;";
		if (!html.contains(sFillText)) {
			System.out.println("EcfrFillinUtils.process52_260_4_Alt_II('52.250-3 ALTERNATE II'): Fill-in clause text not found");
			return html;
		}
		String sFillInCode = "info_item_52_260_3_Alt_II[0]";
		html = html.replace(sFillText, 
				"{{" + sFillInCode + "}}");
		oTemplateStg2ClauseFillInRecord.set2(html, iStg1OfficialClauseId,
				sFillInCode, // psFillInCode,
				"M", // psFillInType,
				500, // piFillInMaxSize,
				null, // piFillInGroupNumber,
				null, // psFillInPlaceholder,
				sFillText, // psFillInDefaultData,
				4, // piFillInDisplayRows,
				false, // pbFillInForTable,
				null // psFillInHeading
				);
		if (psFillInInsertClause != null)
			oTemplateStg2ClauseFillInRecord.insert(psFillInInsertClause);
		return html;
	}
	
	private static String process52_260_4_Alt_II(int iStg1OfficialClauseId, String html, TemplateStg2ClauseFillInRecord oTemplateStg2ClauseFillInRecord,
			PreparedStatement psFillInInsertClause) throws SQLException { // CJ-1075
		// <p>Alternate II (FEB 2009). As prescribed in 50.206(c)(3), substitute the following paragraph (f):</p><p>(f)(1) Offerors are authorized to submit proposals presuming SAFETY Act designation before or after award.</p><p>(2) An offeror is eligible for award only if the offeror&mdash;</p><p>(i) Files a SAFETY Act designation application, limited to the scope of the applicable prequalification designation notice, within 15 days after submission of the proposal;</p><p>(ii) Pursues its SAFETY Act designation application in good faith; and</p><p>(iii) Agrees to obtain the amount of insurance DHS requires for issuing the offeror's SAFETY Act designation.</p><p>(3) If DHS has not issued a SAFETY Act designation to the successful offeror before contract award, the contracting officer will include the clause at 52.250-5 in the resulting contract.</p>
		String sFillText = "(i) Files a SAFETY Act designation application, limited to the scope of the applicable prequalification designation notice, within 15 days after submission of the proposal;";
		if (!html.contains(sFillText)) {
			System.out.println("EcfrFillinUtils.process52_260_4_Alt_II('52.250-4 ALTERNATE II'): Fill-in clause text not found");
			return html;
		}
		
		String sFillInCode = "info_item_52_260_4_Alt_II[0]";
		html = html.replace(sFillText, 
				"{{" + sFillInCode + "}}");
		oTemplateStg2ClauseFillInRecord.set2(html, iStg1OfficialClauseId,
				sFillInCode, // psFillInCode,
				"M", // psFillInType,
				500, // piFillInMaxSize,
				null, // piFillInGroupNumber,
				null, // psFillInPlaceholder,
				sFillText, // psFillInDefaultData,
				4, // piFillInDisplayRows,
				false, // pbFillInForTable,
				null // psFillInHeading
				);
		if (psFillInInsertClause != null)
			oTemplateStg2ClauseFillInRecord.insert(psFillInInsertClause);
		return html;
	}
	*/
	
	private static String process252_227_7014(int iStg1OfficialClauseId, String html, TemplateStg2ClauseFillInRecord oTemplateStg2ClauseFillInRecord,
			PreparedStatement psFillInInsertClause) throws SQLException {

		html = HtmlUtils.normalizeFillins(html);
		html = html.replaceFirst(Pattern.quote("____ (Insert license identifier)"), "{{textbox_man_252.227-7014[0]}}");

		String sPlaceholder = "Insert license identifier";

		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_252.227-7014[0]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);

		html = html.replaceFirst(Pattern.quote("____ (Insert contract number)"), "{{textbox_man_252.227-7014[1]}}");

		sPlaceholder = "Insert contract number";

		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_252.227-7014[1]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);

		html = html.replace(_Fillin_Marker, "{{textbox}}&nbsp;");
		html = EcfrFillInUtils.processTextLines(html);
		
		html = html.replace(">Contract No.", "><p>Contract No.");
		html = html.replace("Contractor Name", "</p><p>Contractor Name");
		html = html.replace("Contractor Address ____&nbsp; ____", "</p><p>Contractor Address ____&nbsp;  ____");
		html = html.replace("Expiration Date ____", "</p><p>Expiration Date ____</p>");
		
		html = html.replace(">Date", "><p>Date");
		html = html.replace("Printed Name and Title", "</p><p>Printed Name and Title");
		html = html.replace("Signature ____", "</p><p>Signature ____</p>");
		
		
		html = EcfrFillInUtils.processTextAndCheck(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
		html = HtmlUtils.parseFillinsAndNormalize(html);
		
		return html;
	}
	
	private static String process252_203_7004(int iStg1OfficialClauseId, String html, TemplateStg2ClauseFillInRecord oTemplateStg2ClauseFillInRecord,
			PreparedStatement psFillInInsertClause) throws SQLException {

		html = HtmlUtils.normalizeFillins(html);
				
		html = html.replace("<p>____ </p><p><i>[Contracting Officer shall insert the appropriate DHS contact information or Web site.]</i></p>",
				"<p>{{textbox_man_252.203-7004[0]}}</p>");
		
		String sPlaceholder = "Contracting Officer shall insert the appropriate DHS contact information or Web site.";
		
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
				"textbox_man_252.203-7004[0]", "S", 200, // piFillInMaxSize,
				null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
				psFillInInsertClause);
		
		html = html.replace(_Fillin_Marker, "{{textbox}}&nbsp;");
		
		html = EcfrFillInUtils.processTextAndCheck(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
		html = HtmlUtils.parseFillinsAndNormalize(html);
		
		return html;
	}

	
	private static String process252_204_7010(int iStg1OfficialClauseId, String html, TemplateStg2ClauseFillInRecord oTemplateStg2ClauseFillInRecord,
			PreparedStatement psFillInInsertClause) throws SQLException {

		html = HtmlUtils.normalizeFillins(html);
				
		html = html.replace("<i>[Contracting Officer to insert Program Manager's name, mailing address, e-mail address, telephone number, and facsimile number];</i>",
				"{{textbox_man_252.204-7010[0]}}");
		
		String sPlaceholder = "Contracting Officer to insert Program Manager's name, mailing address, e-mail address, telephone number, and facsimile number.";
		
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
				"textbox_man_252.204-7010[0]", "M", 500, // piFillInMaxSize,
				null, sPlaceholder, 5, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
				psFillInInsertClause);
		
		html = html.replace(_Fillin_Marker, "{{textbox}}&nbsp;");
		
		html = EcfrFillInUtils.processTextAndCheck(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
		html = HtmlUtils.parseFillinsAndNormalize(html);
		
		return html;
	}
	
	private static String process252_209_7010(int iStg1OfficialClauseId, String html, TemplateStg2ClauseFillInRecord oTemplateStg2ClauseFillInRecord,
			PreparedStatement psFillInInsertClause) throws SQLException {

		html = HtmlUtils.normalizeFillins(html);
				
		html = html.replace("<p>(<i>Insert additional lines as necessary</i>)</p>", "");
		
		html = html.replace("____  ____  ____  ____", "{{tbl_man_252.209-7010[0]}}");
		
		String sPlaceholder = "Insert additional lines as necessary.";
		
		// Add the table.
		oTemplateStg2ClauseFillInRecord.set2(html, iStg1OfficialClauseId,
				"tbl_man_252.209-7010[0]", // psFillInCode,
				"S", // psFillInType,
				150, // piFillInMaxSize,
				0, // piFillInGroupNumber,
				sPlaceholder,
				null, // psFillInDefaultData,
				1, // piFillInDisplayRows,
				true, // pbFillInForTable,
				null // psFillInHeading
				);
		oTemplateStg2ClauseFillInRecord.insert(psFillInInsertClause);
		
		html = html.replace(_Fillin_Marker, "{{textbox}}&nbsp;");
		
		html = EcfrFillInUtils.processTextAndCheck(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
		html = HtmlUtils.parseFillinsAndNormalize(html);
		
		return html;
	}
	
	
	private static String process252_211_7002(int iStg1OfficialClauseId, String html, TemplateStg2ClauseFillInRecord oTemplateStg2ClauseFillInRecord,
			PreparedStatement psFillInInsertClause) throws SQLException {

		html = HtmlUtils.normalizeFillins(html);
				
		html = html.replace("____ <p>(Insert complete address)</p>",
				"{{textbox_man_252.211-7002[0]}}");
		
		String sPlaceholder = "Insert complete address.";
		
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
				"textbox_man_252.211-7002[0]", "S", 250, // piFillInMaxSize,
				null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
				psFillInInsertClause);
		
		html = html.replace(_Fillin_Marker, "{{textbox}}&nbsp;");
		
		html = EcfrFillInUtils.processTextAndCheck(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
		html = HtmlUtils.parseFillinsAndNormalize(html);
		
		return html;
	}
	
	private static String process252_215_7003(int iStg1OfficialClauseId, String html, TemplateStg2ClauseFillInRecord oTemplateStg2ClauseFillInRecord,
			PreparedStatement psFillInInsertClause) throws SQLException {

		html = HtmlUtils.normalizeFillins(html);
				
		html = html.replace("[<i>U.S. Contracting Officer to insert description of the data required in accordance with FAR 15.403-3(a)(1)</i>]",
				"{{textbox_man_252.215-7003[0]}}");
		
		String sPlaceholder = "U.S. Contracting Officer to insert description of the data required in accordance with FAR 15.403-3(a)(1).";
		
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
				"textbox_man_252.215-7003[0]", "M", 1000, // piFillInMaxSize,
				null, sPlaceholder, 2, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
				psFillInInsertClause);
		
		html = html.replace(_Fillin_Marker, "{{textbox}}&nbsp;");
		
		html = EcfrFillInUtils.processTextAndCheck(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
		html = HtmlUtils.parseFillinsAndNormalize(html);
		
		return html;
	}

	private static String process252_215_7008(int iStg1OfficialClauseId, String html, TemplateStg2ClauseFillInRecord oTemplateStg2ClauseFillInRecord,
			PreparedStatement psFillInInsertClause) throws SQLException {

		html = HtmlUtils.normalizeFillins(html);
				
		html = html.replace("<i>[U.S. Contracting Officer to provide description of the data required in accordance with FAR 15.403-3(a)(1) with the notification].</i>",
				"{{textbox_man_252.215-7008[0]}}");
		
		String sPlaceholder = "U.S. Contracting Officer to provide description of the data required in accordance with FAR 15.403-3(a)(1) with the notification.";
		
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
				"textbox_man_252.215-7008[0]", "M", 1000, // piFillInMaxSize,
				null, sPlaceholder, 2, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
				psFillInInsertClause);
		
		html = html.replace(_Fillin_Marker, "{{textbox}}&nbsp;");
		
		html = EcfrFillInUtils.processTextAndCheck(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
		html = HtmlUtils.parseFillinsAndNormalize(html);
		
		return html;
	}
	
	private static String process252_216_7000(int iStg1OfficialClauseId, String html, TemplateStg2ClauseFillInRecord oTemplateStg2ClauseFillInRecord,
			PreparedStatement psFillInInsertClause) throws SQLException {

		html = HtmlUtils.normalizeFillins(html);
				
		html = html.replace("____  (<i>Identify the item</i>)",
				"{{textbox_man_252.216-7000[0]}}");
		
		String sPlaceholder = "identify the item";
		
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
				"textbox_man_252.216-7000[0]", "S", 100, // piFillInMaxSize,
				null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
				psFillInInsertClause);
		
		html = html.replace(_Fillin_Marker, "{{textbox}}&nbsp;");
		
		html = EcfrFillInUtils.processTextAndCheck(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
		html = HtmlUtils.parseFillinsAndNormalize(html);
		
		return html;
	}
	
	private static String process252_216_7006(int iStg1OfficialClauseId, String html, TemplateStg2ClauseFillInRecord oTemplateStg2ClauseFillInRecord,
			PreparedStatement psFillInInsertClause) throws SQLException {

		html = HtmlUtils.normalizeFillins(html);
				
		html = html.replace("____  through ____  [<i>insert dates</i>]",
				"{{textbox_252.216-7006[0]}} through {{textbox_252.216-7006[1]}} ");
		
		String sPlaceholder = "insert date";
		
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
				"textbox_252.216-7006[0]", "S", 20, // piFillInMaxSize,
				null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
				psFillInInsertClause);
		
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
				"textbox_252.216-7006[1]", "S", 20, // piFillInMaxSize,
				null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
				psFillInInsertClause);
		
		html = html.replace(_Fillin_Marker, "{{textbox}}&nbsp;");
		
		html = EcfrFillInUtils.processTextAndCheck(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
		html = HtmlUtils.parseFillinsAndNormalize(html);
		
		return html;
	}
	
	private static String process252_216_7007(int iStg1OfficialClauseId, String html, TemplateStg2ClauseFillInRecord oTemplateStg2ClauseFillInRecord,
			PreparedStatement psFillInInsertClause) throws SQLException {

		html = HtmlUtils.normalizeFillins(html);
				
		html = html.replace("____  (<i>Identify the item</i>)",
				"{{textbox_man_252.216-7007[0]}}");
		
		String sPlaceholder = "identify the item";
		
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
				"textbox_man_252.216-7007[0]", "S", 100, // piFillInMaxSize,
				null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
				psFillInInsertClause);
		
		html = html.replace(_Fillin_Marker, "{{textbox}}&nbsp;");
		
		html = EcfrFillInUtils.processTextAndCheck(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
		html = HtmlUtils.parseFillinsAndNormalize(html);
		
		return html;
	}
	
	private static String process252_216_7008(int iStg1OfficialClauseId, String html, TemplateStg2ClauseFillInRecord oTemplateStg2ClauseFillInRecord,
			PreparedStatement psFillInInsertClause) throws SQLException {

		html = HtmlUtils.normalizeFillins(html);
				
		html = html.replace("____  (<i>Offeror insert name of host country</i>)",
				"{{textbox_man_252.216-7008[0]}}");
		
		String sPlaceholder = "Offeror insert name of host country";
		
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
				"textbox_man_252.216-7008[0]", "S", 100, // piFillInMaxSize,
				null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
				psFillInInsertClause);
		
		html = html.replace(_Fillin_Marker, "{{textbox}}&nbsp;");
		
		html = EcfrFillInUtils.processTextAndCheck(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
		html = HtmlUtils.parseFillinsAndNormalize(html);
		
		return html;
	}
	
	private static String process252_217_7002(int iStg1OfficialClauseId, String html, TemplateStg2ClauseFillInRecord oTemplateStg2ClauseFillInRecord,
			PreparedStatement psFillInInsertClause) throws SQLException {

		html = HtmlUtils.normalizeFillins(html);
				
		html = html.replace("(insert address)", "{{textbox_man_252.217-7002[0]}} ");
		
		String sPlaceholder = "insert address";
		
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
				"textbox_man_252.217-7002[0]", "S", 250, // piFillInMaxSize,
				null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
				psFillInInsertClause);
		
		html = html.replace("(insert beginning and ending dates and insert hours during day)", "{{textbox_man_252.217-7002[1]}} ");

		sPlaceholder = "insert beginning and ending dates and insert hours during day";
				
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
				"textbox_man_252.217-7002[1]", "S", 250, // piFillInMaxSize,
				null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
				psFillInInsertClause);
		
		html = html.replace(_Fillin_Marker, "{{textbox}}&nbsp;");
		
		html = EcfrFillInUtils.processTextAndCheck(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
		html = HtmlUtils.parseFillinsAndNormalize(html);
		
		return html;
	}
	
	
	private static String process252_219_7009(int iStg1OfficialClauseId, String html, TemplateStg2ClauseFillInRecord oTemplateStg2ClauseFillInRecord,
			PreparedStatement psFillInInsertClause) throws SQLException {

		html = HtmlUtils.normalizeFillins(html);
				
		html = html.replace(" ____  ____  ____  ____ <p>[<i>To be completed by the Contracting Officer at the time of award</i>] </p>",
				"{{textbox_man_252.219-7009[0]}}<br> &nbsp;{{textbox_man_252.219-7009[1]}}<br> &nbsp;{{textbox_man_252.219-7009[2]}}<br> &nbsp;{{textbox_man_252.219-7009[3]}}" );
		
		String sPlaceholder = "To be completed by the Contracting Officer at the time of award.";
		
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
				"textbox_man_252.219-7009[0]", "S", 100, // piFillInMaxSize,
				1, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
				psFillInInsertClause);
		
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
				"textbox_man_252.219-7009[1]", "S", 100, // piFillInMaxSize,
				1, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
				psFillInInsertClause);	
		
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
				"textbox_man_252.219-7009[2]", "S", 100, // piFillInMaxSize,
				1, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
				psFillInInsertClause);
		
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
				"textbox_man_252.219-7009[3]", "S", 100, // piFillInMaxSize,
				1, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
				psFillInInsertClause);
		
		html = html.replace(_Fillin_Marker, "{{textbox}}&nbsp;");
		
		html = EcfrFillInUtils.processTextAndCheck(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
		html = HtmlUtils.parseFillinsAndNormalize(html);
		
		return html;
	}
	

	private static String process252_219_7010(int iStg1OfficialClauseId, String html, TemplateStg2ClauseFillInRecord oTemplateStg2ClauseFillInRecord,
			PreparedStatement psFillInInsertClause) throws SQLException {

		html = HtmlUtils.normalizeFillins(html);
				
		html = html.replace("____ . <i>[Contracting Officer completes by inserting the appropriate SBA District and/or Regional Office(s) as identified by the SBA.]</i>",
				"{{textbox_man_252.219-7010[0]}}." );
		
		String sPlaceholder = "Contracting Officer completes by inserting the appropriate SBA District and/or Regional Office(s) as identified by the SBA";
		
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
				"textbox_man_252.219-7010[0]", "S", 100, // piFillInMaxSize,
				1, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
				psFillInInsertClause);	
		
		html = html.replace("The ____  <i>[insert name of SBA's contractor]</i>",  "The {{textbox_man_252.219-7010[1]}}");
		sPlaceholder = "insert name of SBA's contractor";
		
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
				"textbox_man_252.219-7010[1]", "S", 100, // piFillInMaxSize,
				1, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
				psFillInInsertClause);	
		
		html = html.replace("will notify the ____  <i>[insert name of contracting agency]</i>",  "will notify the {{textbox_man_252.219-7010[2]}}");
		sPlaceholder = "insert name of contracting agency";
		
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
				"textbox_man_252.219-7010[2]", "S", 100, // piFillInMaxSize,
				1, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
				psFillInInsertClause);		
		
		html = html.replace(_Fillin_Marker, "{{textbox}}&nbsp;");
		
		html = EcfrFillInUtils.processTextAndCheck(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
		html = HtmlUtils.parseFillinsAndNormalize(html);
		
		return html;
	}
	
	private static String process252_225_7018(int iStg1OfficialClauseId, String html, TemplateStg2ClauseFillInRecord oTemplateStg2ClauseFillInRecord,
			PreparedStatement psFillInInsertClause) throws SQLException {

		html = HtmlUtils.normalizeFillins(html);
				
		html = html.replace("[The offeror shall check the block and fill in the blank for one of the following paragraphs, based on the estimated value and the country of origin of photovoltaic devices to be utilized in performance of the contract:]</i></p><p>__(1) No photovoltaic devices",
				"</i></p><p>{{checkbox_man_252.225-7018[0]}} (1) No photovoltaic devices ");
		
		String sPlaceholder = "The offeror shall check the block and fill in the blank for one of the following paragraphs, based on the estimated value and the country of origin of photovoltaic devices to be utilized in performance of the contract:";
		
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
				"checkbox_man_252.225-7018[0]", "C", 1, // piFillInMaxSize,
				1, sPlaceholder, 1, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
				psFillInInsertClause);
		
		// Replace all occurrences of "Offeror to specify country of origin" with fill in.
		sPlaceholder = "Offeror to specify country of origin";
		html = html.replace("<i>[Offeror to specify country of origin__.]</i>", "{{textbox_man_252.225-7018[0]}}");
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
				"textbox_man_252.225-7018[0]", "S", 100, // piFillInMaxSize,
				null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
				psFillInInsertClause);
		/*
		Integer ndx=1;
		while (html.indexOf("<i>[Offeror to specify country of origin__];</i>") > 0)
		{
			String sCode = "textbox_man_252.225-7018[" + ndx.toString() + "]";
			html = html.replaceFirst(Pattern.quote("<i>[Offeror to specify country of origin__];</i>"), "{{" + sCode + "}}");

			oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
					sCode, "S", 100, // piFillInMaxSize,
					null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
					psFillInInsertClause);
			ndx++;
		}
		*/
		
		// Add the checkboxes
		
		Integer ndx=1;
		while (html.indexOf("__(") > 0)
		{
			String sCode = "checkbox_man_252.225-7018[" + ndx.toString() + "]";
			html = html.replaceFirst(Pattern.quote("__("), "{{" + sCode + "}}&nbsp;(");

			oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
					sCode, "C", 1, // piFillInMaxSize,
					1, null, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
					psFillInInsertClause);
			ndx++;
		}
		
		String sReplacement = "____ . <i>[Offeror to specify country of origin, if known, and provide documentation that the cost of a domestic photovoltaic device would be unreasonable in comparison to the cost of the proposed foreign photovoltaic device, i.e.</i><i>, that the price of the foreign photovoltaic device plus 50 percent is less than the price of a comparable domestic photovoltaic device.]</i>";
		sPlaceholder = "Offeror to specify country of origin, if known, and provide documentation that the cost of a domestic photovoltaic device would be unreasonable in comparison to the cost of the proposed foreign photovoltaic device, i.e., that the price of the foreign photovoltaic device plus 50 percent is less than the price of a comparable domestic photovoltaic device.";
		ndx=1;
		while (html.indexOf(sReplacement) > 0)
		{
			String sCode = "textbox_desc_252.225-7018[" + ndx.toString() + "]";
			html = html.replaceFirst(Pattern.quote(sReplacement), "{{" + sCode + "}}");

			oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
					sCode, "M", 500, // piFillInMaxSize,
					null, sPlaceholder, 4, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
					psFillInInsertClause);
			ndx++;
		}
		
		html = html.replace(_Fillin_Marker, "{{textbox}}&nbsp;");
		
		html = EcfrFillInUtils.processTextAndCheck(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
		html = HtmlUtils.parseFillinsAndNormalize(html);
		
		return html;
	}
	
	private static String process252_225_7043(int iStg1OfficialClauseId, String html, TemplateStg2ClauseFillInRecord oTemplateStg2ClauseFillInRecord,
			PreparedStatement psFillInInsertClause) throws SQLException {

		html = HtmlUtils.normalizeFillins(html);
				
		html = html.replace("[<i>Contracting Officer to insert applicable information cited in PGI 225.372-1</i>].",
				"[<i>Contracting Officer to insert applicable information cited in PGI 225.372-1</i>]");
		html = html.replace("[<i>Contracting Officer to insert applicable information cited in PGI 225.372-1</i>]", "{{textbox_man_252.225-7043[0]}}");
		
		String sPlaceholder = "Contracting Officer to insert applicable information cited in PGI 225.372-1";
		
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
				"textbox_man_252.225-7043[0]", "M", 250, // piFillInMaxSize,
				null, sPlaceholder, 1, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
				psFillInInsertClause);		

		html = html.replace(_Fillin_Marker, "{{textbox}}&nbsp;");
		
		html = EcfrFillInUtils.processTextAndCheck(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
		html = HtmlUtils.parseFillinsAndNormalize(html);
		
		return html;
	}
	
	private static String process252_225_7044(String sClauseNumber, int iStg1OfficialClauseId, String html, TemplateStg2ClauseFillInRecord oTemplateStg2ClauseFillInRecord,
			PreparedStatement psFillInInsertClause) throws SQLException {

		sClauseNumber = sClauseNumber.replaceAll(Pattern.quote(" "), "_");
		sClauseNumber = sClauseNumber.replaceAll(Pattern.quote("ALTERNATE_"), "Alt");
		
		html = HtmlUtils.normalizeFillins(html);
		
		// replace any extra period.
		html = html.replace("[<i>Contracting Officer to list applicable excepted materials or indicate \"none\"</i>].", 
				"[<i>Contracting Officer to list applicable excepted materials or indicate \"none\"</i>]");

		html = html.replace("[<i>Contracting Officer to list applicable excepted materials or indicate \"none\"</i>]", 
				"{{textbox_man_" + sClauseNumber + "[0]}}");
		
		String sPlaceholder = "Contracting Officer to list applicable excepted materials or indicate \"none\"";
		
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
				"textbox_man_" + sClauseNumber + "[0]", "M", 250, // piFillInMaxSize,
				null, sPlaceholder, 1, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
				psFillInInsertClause);		

		html = html.replace(_Fillin_Marker, "{{textbox}}&nbsp;");
		
		html = EcfrFillInUtils.processTextAndCheck(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
		html = HtmlUtils.parseFillinsAndNormalize(html);
		
		return html;
	}
	
	private static String process252_227_7012(int iStg1OfficialClauseId, String html, TemplateStg2ClauseFillInRecord oTemplateStg2ClauseFillInRecord,
			PreparedStatement psFillInInsertClause) throws SQLException {
		
		html = HtmlUtils.normalizeFillins(html);
		html = HtmlUtils.convertUnicodeToHtml(html);
		
		html = html.replace("[month, year]","{{textbox_man_252.227-7012[0]}}, {{textbox_man_252.227-7012[1]}}");
		
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
				"textbox_man_252.227-7012[0]", "S", 2, // piFillInMaxSize,
				null, "month", null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
				psFillInInsertClause);		
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
				"textbox_man_252.227-7012[1]", "S", 4, // piFillInMaxSize,
				null, "year", null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
				psFillInInsertClause);	
		
		html = html.replace(_Fillin_Marker, "{{textbox}}&nbsp;");
		html = EcfrFillInUtils.processTextAndCheck(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
		html = HtmlUtils.parseFillinsAndNormalize(html);
		
		return html;
	}
	
	// Alternate clause is not in the spreadsheet.
	 
	private static String process252_227_7038_Alt_I(int iStg1OfficialClauseId, String html, TemplateStg2ClauseFillInRecord oTemplateStg2ClauseFillInRecord,
			PreparedStatement psFillInInsertClause) throws SQLException {
		
		html = HtmlUtils.normalizeFillins(html);
		html = HtmlUtils.convertUnicodeToHtml(html);
		
		html = html.replace("","{{textbox_man_252.227-7038[0]}}");
		
		String sPlaceholder = "Contracting Officer to complete with the names of applicable existing treaties or international agreements. This paragraph is not intended to apply to treaties or agreements that are in effect on the date of the award but are not listed.";
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
				"textbox_man_252.227-7038[0]", "S", 2, // piFillInMaxSize,
				null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
				psFillInInsertClause);		
		
		html = html.replace(_Fillin_Marker, "{{textbox}}&nbsp;");
		html = EcfrFillInUtils.processTextAndCheck(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
		html = HtmlUtils.parseFillinsAndNormalize(html);
		
		return html;
	}
	
	private static String process252_229_7003(int iStg1OfficialClauseId, String html, TemplateStg2ClauseFillInRecord oTemplateStg2ClauseFillInRecord,
			PreparedStatement psFillInInsertClause) throws SQLException {
				
		html = HtmlUtils.normalizeFillins(html);
		html = HtmlUtils.convertUnicodeToHtml(html);
		
		html = html.replace("[<i>Contracting Officer must insert the applicable fiscal code(s) for military activities within Italy: 80028250241 for Army, 80156020630 for Navy, or 91000190933 for Air Force</i>].", 
				"{{textbox_man_252.229-7003[0]}} ");
		
		String sPlaceholder = "Contracting Officer must insert the applicable fiscal code(s) for military activities within Italy: 80028250241 for Army, 80156020630 for Navy, or 91000190933 for Air Force";
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
				"textbox_man_252.229-7003[0]", "S", 100, // piFillInMaxSize,
				null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
				psFillInInsertClause);		
		
		html = html.replace(_Fillin_Marker, "{{textbox}}&nbsp;");
		html = EcfrFillInUtils.processTextAndCheck(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
		html = HtmlUtils.parseFillinsAndNormalize(html);
		
		return html;
	}
	
	private static String process252_232_7001(int iStg1OfficialClauseId, String html, TemplateStg2ClauseFillInRecord oTemplateStg2ClauseFillInRecord,
			PreparedStatement psFillInInsertClause) throws SQLException {
				
		html = HtmlUtils.normalizeFillins(html);
		html = HtmlUtils.convertUnicodeToHtml(html);
		
		html = html.replace("(<i>insert the name of the disbursing office in the advance payment pool agreement</i>)", 
				"{{textbox_man_252.232-7001[0]}}");

		String sPlaceholder = "Insert the name of the disbursing office in the advance payment pool agreement";
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
				"textbox_man_252.232-7001[0]", "S", 100, // piFillInMaxSize,
				null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
				psFillInInsertClause);		
		
		html = html.replace(_Fillin_Marker, "{{textbox}}&nbsp;");
		html = EcfrFillInUtils.processTextAndCheck(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
		html = HtmlUtils.parseFillinsAndNormalize(html);
		
		return html;
	}
	
	private static String process252_232_7007(int iStg1OfficialClauseId, String html, TemplateStg2ClauseFillInRecord oTemplateStg2ClauseFillInRecord,
			PreparedStatement psFillInInsertClause) throws SQLException {
				
		html = HtmlUtils.normalizeFillins(html);
		html = HtmlUtils.convertUnicodeToHtml(html);
		
		html = html.replaceFirst(Pattern.quote("Contract line item(s) <i>[Contracting Officer insert after negotiations]</i>"), 
				"Contract line item(s) {{textbox_man_252.232-7007[0]}} ");
		html = html.replaceFirst(Pattern.quote("__ <i>[Contracting Officer insert after negotiations]"), 
				"{{textbox_man_252.232-7007[1]}} ");
		String sPlaceholder = "Contracting Officer insert after negotiations";
		
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
				"textbox_man_252.232-7007[0]", "S", 100, // piFillInMaxSize,
				null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
				psFillInInsertClause);	
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
				"textbox_man_252.232-7007[1]", "S", 100, // piFillInMaxSize,
				null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
				psFillInInsertClause);	
		
		html = html.replaceFirst(Pattern.quote("(month) (day), (year)"), "{{textbox_man_252.232-7007[2]}} ");
		html = html.replaceFirst(Pattern.quote("(month) (day), (year)"), "{{textbox_man_252.232-7007[3]}} ");
		html = html.replaceFirst(Pattern.quote("(month) (day), (year)"), "{{textbox_man_252.232-7007[4]}} ");
		
		sPlaceholder = "month day, year";
		
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
				"textbox_man_252.232-7007[2]", "S", 100, // piFillInMaxSize,
				null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
				psFillInInsertClause);	
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
				"textbox_man_252.232-7007[3]", "S", 100, // piFillInMaxSize,
				null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
				psFillInInsertClause);	
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
				"textbox_man_252.232-7007[4]", "S", 100, // piFillInMaxSize,
				null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
				psFillInInsertClause);	
		
		html = html.replace(_Fillin_Marker, "{{textbox}}&nbsp;");
		html = EcfrFillInUtils.processTextAndCheck(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
		html = HtmlUtils.parseFillinsAndNormalize(html);
		
		return html;
	}
	
	private static String process252_232_7013(int iStg1OfficialClauseId, String html, TemplateStg2ClauseFillInRecord oTemplateStg2ClauseFillInRecord,
			PreparedStatement psFillInInsertClause) throws SQLException {
				
		html = HtmlUtils.normalizeFillins(html);
		html = HtmlUtils.convertUnicodeToHtml(html);
		
		html = html.replaceFirst(Pattern.quote("[Contracting Officer insert applicable CLIN(s)]"), "{{textbox_man_252.232-7013[0]}}");
		html = html.replaceFirst(Pattern.quote("[Contracting Officer insert applicable CLIN(s)]"), "{{textbox_man_252.232-7013[1]}}");

		String sPlaceholder = "Contracting Officer insert applicable CLIN(s)";
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
				"textbox_man_252.232-7013[0]", "S", 100, // piFillInMaxSize,
				null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
				psFillInInsertClause);		
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
				"textbox_man_252.232-7013[1]", "S", 100, // piFillInMaxSize,
				null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
				psFillInInsertClause);	
		
		html = html.replace("Contract Attachment __", "Contract Attachment {{textbox}}");		
		html = html.replace(_Fillin_Marker, "{{textbox}}&nbsp;");
		html = EcfrFillInUtils.processTextAndCheck(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
		html = HtmlUtils.parseFillinsAndNormalize(html);
		
		return html;
	}
	
	private static String process252_232_7014(int iStg1OfficialClauseId, String html, TemplateStg2ClauseFillInRecord oTemplateStg2ClauseFillInRecord,
			PreparedStatement psFillInInsertClause) throws SQLException {
				
		html = HtmlUtils.normalizeFillins(html);
		html = HtmlUtils.convertUnicodeToHtml(html);
		
		html = html.replace("<i>[Insert current budget rate here.]</i>", "{{textbox_man_252.232-7014[0]}}");
	
		String sPlaceholder = "Insert current budget rate here";
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
				"textbox_man_252.232-7014[0]", "S", 100, // piFillInMaxSize,
				null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
				psFillInInsertClause);		
		
		html = html.replace(_Fillin_Marker, "{{textbox}}&nbsp;");
		html = EcfrFillInUtils.processTextAndCheck(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
		html = HtmlUtils.parseFillinsAndNormalize(html);
		
		return html;
	}
	
	private static String process252_234_7002(int iStg1OfficialClauseId, String html, TemplateStg2ClauseFillInRecord oTemplateStg2ClauseFillInRecord,
			PreparedStatement psFillInInsertClause) throws SQLException {
				
		html = HtmlUtils.normalizeFillins(html);
		html = HtmlUtils.convertUnicodeToHtml(html);
		
		html = html.replaceFirst(Pattern.quote("</p><p><em>&nbsp;[Contracting Officer to insert names of subcontractors (or subcontracted effort if subcontractors have not been selected) designated for application of the EVMS requirements of this clause.]</em></p><p>&nbsp; {{textbox}}</p><p>&nbsp; {{textbox}}</p><p>&nbsp; {{textbox}}</p><p>&nbsp; {{textbox}}</p>"),
				"<p>&nbsp; {{textbox_man_252.234-7002[0]}}</p><p>&nbsp; {{textbox_man_252.234-7002[1]}}</p><p>&nbsp; {{textbox_man_252.234-7002[2]}}</p><p>&nbsp; {{textbox_man_252.234-7002[3]}}</p>");
		html = html.replaceFirst(Pattern.quote("</p><p><em>&nbsp;[Contracting Officer to insert names of subcontractors (or subcontracted effort if subcontractors have not been selected) designated for application of the EVMS requirements of this clause.]</em></p><p>&nbsp; {{textbox}}</p><p>&nbsp; {{textbox}}</p><p>&nbsp; {{textbox}}</p><p>&nbsp; {{textbox}}</p>"),
				"<p>&nbsp; {{textbox_man_252.234-7002[4]}}</p><p>&nbsp; {{textbox_man_252.234-7002[5]}}</p><p>&nbsp; {{textbox_man_252.234-7002[6]}}</p><p>&nbsp; {{textbox_man_252.234-7002[7]}}</p>");

		String sPlaceholder = "Contracting Officer to insert names of subcontractors (or subcontracted effort if subcontractors have not been selected) designated for application of the EVMS requirements of this clause.";
		
		for (Integer ndx=0; ndx<=3; ndx++)
		{
			String sCode = "textbox_man_252.234-7002[" + ndx.toString() + "]";

			oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
					sCode, "S", 100, // piFillInMaxSize,
					1, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
					psFillInInsertClause);
		}
		for (Integer ndx=4; ndx<=7; ndx++)
		{
			String sCode = "textbox_man_252.234-7002[" + ndx.toString() + "]";

			oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
					sCode, "S", 100, // piFillInMaxSize,
					2, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
					psFillInInsertClause);
		}
		
		html = html.replace(_Fillin_Marker, "{{textbox}}&nbsp;");
		html = EcfrFillInUtils.processTextAndCheck(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
		html = HtmlUtils.parseFillinsAndNormalize(html);
		
		return html;
	}
	
	private static String process252_237_7019(int iStg1OfficialClauseId, String html, TemplateStg2ClauseFillInRecord oTemplateStg2ClauseFillInRecord,
			PreparedStatement psFillInInsertClause) throws SQLException {
				
		html = HtmlUtils.normalizeFillins(html);
		html = HtmlUtils.convertUnicodeToHtml(html);
		
		html = html.replaceFirst(Pattern.quote("<i>[Contracting Officer to insert applicable point of contact information cited in PGI 237.171-3(b).]</i>"), 
				"{{textbox_man_252.237-7019[0]}}");
		String sPlaceholder = "Contracting Officer to insert applicable point of contact information cited in PGI 237.171-3(b).";
		
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
				"textbox_man_252.237-7019[0]", "S", 100, // piFillInMaxSize,
				null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
				psFillInInsertClause);		
		
		html = html.replace(_Fillin_Marker, "{{textbox}}&nbsp;");
		html = EcfrFillInUtils.processTextAndCheck(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
		html = HtmlUtils.parseFillinsAndNormalize(html);
		
		return html;
	}
	
	private static String process52_209_3 (int iStg1OfficialClauseId, String html, 
		TemplateStg2ClauseFillInRecord oTemplateStg2ClauseFillInRecord, 
		PreparedStatement psFillInInsertClause) throws SQLException {

		html = HtmlUtils.normalizeFillins(html);
		html = html.replaceFirst(Pattern.quote("[<i>Contracting Officer shall insert details</i>]"), "{{textbox_man_52.209-3[0]}}");

		String sPlaceholder = "Contracting Officer shall insert details";

		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.209-3[0]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);

		html = html.replaceFirst(Pattern.quote("_ [<i>insert address of the Government activity to receive the report</i>]"), "{{textbox_man_52.209-3[1]}}");

		sPlaceholder = "insert address of the Government activity to receive the report";

		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.209-3[1]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);


		html = html.replace(_Fillin_Marker, "{{textbox}}&nbsp;");
		html = EcfrFillInUtils.processTextAndCheck(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
		html = HtmlUtils.parseFillinsAndNormalize(html);

		return html;
	}

	private static String process52_209_4 (int iStg1OfficialClauseId, String html, 
		TemplateStg2ClauseFillInRecord oTemplateStg2ClauseFillInRecord, 
		PreparedStatement psFillInInsertClause) throws SQLException {

		html = HtmlUtils.normalizeFillins(html);
		html = html.replaceFirst(Pattern.quote("[<i>Contracting Officer shall insert details</i>]"), "{{textbox_man_52.209-4[0]}}");
		//html = html.replaceFirst(Pattern.quote("<h3>[<i>Contracting Officer shall insert details</i>]</h3>"), "{{textbox_man_52.209-4[0]}}");
		String sPlaceholder = "[Contracting Officer shall insert details]";

		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.209-4[0]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);

		html = html.replaceFirst(Pattern.quote("____  [<i>insert name and address of the testing facility</i>]"), "{{textbox_man_52.209-4[1]}}");

		sPlaceholder = "insert name and address of the testing facility";

		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.209-4[1]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);


		html = html.replace(_Fillin_Marker, "{{textbox}}&nbsp;");
		html = EcfrFillInUtils.processTextAndCheck(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
		html = HtmlUtils.parseFillinsAndNormalize(html);

		return html;
	}

	private static String process52_211_8 (int iStg1OfficialClauseId, String html, 
		TemplateStg2ClauseFillInRecord oTemplateStg2ClauseFillInRecord, 
		PreparedStatement psFillInInsertClause) throws SQLException {

		html = HtmlUtils.normalizeFillins(html);

		// Add the table.
		html = html.replaceFirst(Pattern.quote("<table border=\"1\" cellpadding=\"1\" cellspacing=\"1\" frame=\"void\" width=\"100%\"><thead><tr><th scope=\"col\">ITEM NO.</th><th scope=\"col\">QUANTITY</th><th scope=\"col\">WITHIN DAYS AFTER DATE OF CONTRACT</th></tr></thead><tbody><tr><td align=\"left\" scope=\"row\">____ </td><td align=\"left\">____ </td><td align=\"left\">____ </td></tr><tr><td align=\"left\" scope=\"row\">____ </td><td align=\"left\">____ </td><td align=\"left\">____ </td></tr><tr><td align=\"left\" scope=\"row\">____ </td><td align=\"left\">____ </td><td align=\"left\">____ </td></tr></tbody></table>"), 
				"{{tbl_52.211-8[0]}}{{tbl_52.211-8[1]}}{{tbl_52.211-8[2]}}");
				

		html = html.replaceFirst(Pattern.quote("[Contracting Officer insert specific details]"), "");
		
		oTemplateStg2ClauseFillInRecord.set2(html, iStg1OfficialClauseId,
				"tbl_52.211-8[0]", // psFillInCode,
				"S", // psFillInType,
				100, // piFillInMaxSize,
				0, // piFillInGroupNumber,
				"Contracting Officer insert specific details.", // psFillInPlaceholder,
				null, // psFillInDefaultData,
				1, // piFillInDisplayRows,
				true, // pbFillInForTable,
				"ITEM NO." // psFillInHeading
				);
		oTemplateStg2ClauseFillInRecord.insert(psFillInInsertClause);
		
		oTemplateStg2ClauseFillInRecord.set2(html, iStg1OfficialClauseId,
				"tbl_52.211-8[1]", // psFillInCode,
				"S", // psFillInType,
				100, // piFillInMaxSize,
				0, // piFillInGroupNumber,
				null, // psFillInPlaceholder,
				null, // psFillInDefaultData,
				1, // piFillInDisplayRows,
				true, // pbFillInForTable,
				"QUANTITY" // psFillInHeading
				);
		oTemplateStg2ClauseFillInRecord.insert(psFillInInsertClause);
		
		oTemplateStg2ClauseFillInRecord.set2(html, iStg1OfficialClauseId,
				"tbl_52.211-8[2]", // psFillInCode,
				"S", // psFillInType,
				100, // piFillInMaxSize,
				0, // piFillInGroupNumber,
				null, // psFillInPlaceholder,
				null, // psFillInDefaultData,
				1, // piFillInDisplayRows,
				true, // pbFillInForTable,
				"WITHIN DAYS AFTER DATE OF CONTRACT" // psFillInHeading
				);
		oTemplateStg2ClauseFillInRecord.insert(psFillInInsertClause);
		
		// Add the table.
		html = html.replaceFirst(Pattern.quote("<table border=\"1\" cellpadding=\"1\" cellspacing=\"1\" frame=\"void\" width=\"100%\"><thead><tr><th scope=\"col\">ITEM NO.</th><th scope=\"col\">QUANTITY</th><th scope=\"col\">WITHIN DAYS AFTER DATE OF CONTRACT</th></tr></thead><tbody><tr><td align=\"left\" scope=\"row\">____ </td><td align=\"left\">____ </td><td align=\"left\">____ </td></tr><tr><td align=\"left\" scope=\"row\">____ </td><td align=\"left\">____ </td><td align=\"left\">____ </td></tr><tr><td align=\"left\" scope=\"row\">____ </td><td align=\"left\">____ </td><td align=\"left\">____ </td></tr></tbody></table>"), 
		"{{tbl_52.211-8[3]}}{{tbl_52.211-8[4]}}{{tbl_52.211-8[5]}}");
						

		oTemplateStg2ClauseFillInRecord.set2(html, iStg1OfficialClauseId,
				"tbl_52.211-8[3]", // psFillInCode,
				"S", // psFillInType,
				100, // piFillInMaxSize,
				1, // piFillInGroupNumber,
				"Contracting Officer insert specific details.", // psFillInPlaceholder,
				null, // psFillInDefaultData,
				1, // piFillInDisplayRows,
				true, // pbFillInForTable,
				"ITEM NO." // psFillInHeading
				);
		oTemplateStg2ClauseFillInRecord.insert(psFillInInsertClause);
		
		oTemplateStg2ClauseFillInRecord.set2(html, iStg1OfficialClauseId,
				"tbl_52.211-8[4]", // psFillInCode,
				"S", // psFillInType,
				100, // piFillInMaxSize,
				1, // piFillInGroupNumber,
				null, // psFillInPlaceholder,
				null, // psFillInDefaultData,
				1, // piFillInDisplayRows,
				true, // pbFillInForTable,
				"QUANTITY" // psFillInHeading
				);
		oTemplateStg2ClauseFillInRecord.insert(psFillInInsertClause);
		
		oTemplateStg2ClauseFillInRecord.set2(html, iStg1OfficialClauseId,
				"tbl_52.211-8[5]", // psFillInCode,
				"S", // psFillInType,
				100, // piFillInMaxSize,
				1, // piFillInGroupNumber,
				null, // psFillInPlaceholder,
				null, // psFillInDefaultData,
				1, // piFillInDisplayRows,
				true, // pbFillInForTable,
				"WITHIN DAYS AFTER DATE OF CONTRACT" // psFillInHeading
				);
		oTemplateStg2ClauseFillInRecord.insert(psFillInInsertClause);
		
		html = html.replace(_Fillin_Marker, "{{textbox}}&nbsp;");
		html = EcfrFillInUtils.processTextAndCheck(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
		html = HtmlUtils.parseFillinsAndNormalize(html);

		return html;
	}
	
	private static String process52_211_8_AltI (int iStg1OfficialClauseId, String html, 
		TemplateStg2ClauseFillInRecord oTemplateStg2ClauseFillInRecord, 
		PreparedStatement psFillInInsertClause) throws SQLException {

		html = HtmlUtils.normalizeFillins(html);

		html = html.replaceFirst(Pattern.quote("during the months ____ ; or not sooner than ____  or later than ____"),
				"during the months {{textbox}} ; or not sooner than {{textbox}}  or later than {{textbox}}");

		html = html.replaceFirst(Pattern.quote("__ [<i>Contracting Officer insert date</i>]"), "{{textbox_man_52.211-8_AltI[0]}}&nbsp;");
		
		String sPlaceholder = "Contracting Officer insert date";

		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.211-8_AltI[0]", "S", 20, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
		
		html = html.replace(_Fillin_Marker, "{{textbox}}&nbsp;");
		html = EcfrFillInUtils.processTextAndCheck(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
		html = HtmlUtils.parseFillinsAndNormalize(html);

		return html;
	}
	private static String process52_211_8_AltII (int iStg1OfficialClauseId, String html, 
			TemplateStg2ClauseFillInRecord oTemplateStg2ClauseFillInRecord, 
			PreparedStatement psFillInInsertClause) throws SQLException {

		html = HtmlUtils.normalizeFillins(html);
		
		html = html.replaceFirst(Pattern.quote("__ [<i>Contracting Officer insert date</i>]"), "{{textbox_man_52.211-8_AltII[0]}}&nbsp;");
		
		String sPlaceholder = "Contracting Officer insert date";

		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.211-8_AltII[0]", "S", 20, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
		
		
		html = html.replace(_Fillin_Marker, "{{textbox}}&nbsp;");
		html = EcfrFillInUtils.processTextAndCheck(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
		html = HtmlUtils.parseFillinsAndNormalize(html);

		return html;
	}
	
	private static String process52_211_10 (int iStg1OfficialClauseId, String html, 
			TemplateStg2ClauseFillInRecord oTemplateStg2ClauseFillInRecord, 
			PreparedStatement psFillInInsertClause) throws SQLException {

			html = HtmlUtils.normalizeFillins(html);
			html = html.replaceFirst(Pattern.quote("_ [<i>Contracting Officer insert number</i>]"), "{{textbox_man_52.211-10[0]}}");

			String sPlaceholder = "Contracting Officer insert number";

			oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
				"textbox_man_52.211-10[0]", "S", 100, // piFillInMaxSize, 
				null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
				psFillInInsertClause);

			html = html.replaceFirst(Pattern.quote("__*"), "{{textbox_man_52.211-10[1]}}");
			
			sPlaceholder = "The Contracting Officer shall specify either a number of days after the date the contractor receives the notice to proceed, or a calendar date.";

			oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
				"textbox_man_52.211-10[1]", "S", 100, // piFillInMaxSize, 
				null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
				psFillInInsertClause);
			
			html = html.replace(_Fillin_Marker, "{{textbox}}&nbsp;");
			html = EcfrFillInUtils.processTextAndCheck(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
			html = HtmlUtils.parseFillinsAndNormalize(html);

			return html;
		}

	private static String process52_211_10_AltI (int iStg1OfficialClauseId, String html, 
		TemplateStg2ClauseFillInRecord oTemplateStg2ClauseFillInRecord, 
		PreparedStatement psFillInInsertClause) throws SQLException {

		html = HtmlUtils.normalizeFillins(html);
		html = html.replaceFirst(Pattern.quote("__ [<i>Contracting Officer insert date</i>]"), "{{textbox_man_52.211-10_AltI[0]}}");

		String sPlaceholder = "Contracting Officer insert date";

		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.211-10_AltI[0]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);


		html = html.replace(_Fillin_Marker, "{{textbox}}&nbsp;");
		html = EcfrFillInUtils.processTextAndCheck(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
		html = HtmlUtils.parseFillinsAndNormalize(html);

		return html;
	}

	private static String process52_211_11 (int iStg1OfficialClauseId, String html, 
			TemplateStg2ClauseFillInRecord oTemplateStg2ClauseFillInRecord, 
			PreparedStatement psFillInInsertClause) throws SQLException {

			html = HtmlUtils.normalizeFillins(html);
			html = html.replaceFirst(Pattern.quote("[<i>Contracting Officer insert amount</i>]"), "{{textbox_man_52.211-11[0]}}");

			String sPlaceholder = "Contracting Officer insert amount";

			oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
				"textbox_man_52.211-11[0]", "S", 100, // piFillInMaxSize, 
				null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
				psFillInInsertClause);


			html = html.replace(_Fillin_Marker, "{{textbox}}&nbsp;");
			html = EcfrFillInUtils.processTextAndCheck(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
			html = HtmlUtils.parseFillinsAndNormalize(html);

			return html;
		}
		/*
	private static String process52_211_12 (int iStg1OfficialClauseId, String html, 
			TemplateStg2ClauseFillInRecord oTemplateStg2ClauseFillInRecord, 
			PreparedStatement psFillInInsertClause) throws SQLException {

			html = HtmlUtils.normalizeFillins(html);
			html = html.replaceFirst(Pattern.quote("__ [<i>Contracting Officer insert amount</i>]"), "{{textbox_man_52.211-12[0]}}");

			String sPlaceholder = "Contracting Officer insert amount";

			oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
				"textbox_man_52.211-12[0]", "S", 100, // piFillInMaxSize, 
				null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
				psFillInInsertClause);


			html = html.replace(_Fillin_Marker, "{{textbox}}&nbsp;");
			html = EcfrFillInUtils.processTextAndCheck(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
			html = HtmlUtils.parseFillinsAndNormalize(html);

			return html;
		}
		*/
	private static String process52_211_14 (int iStg1OfficialClauseId, String html, 
			TemplateStg2ClauseFillInRecord oTemplateStg2ClauseFillInRecord, 
			PreparedStatement psFillInInsertClause) throws SQLException {

			html = HtmlUtils.normalizeFillins(html);
			html = html.replaceFirst(Pattern.quote("[ ] DX rated order; [ ] DO rated order"), 
					"{{checkbox_man_52.211-14[0]}} DX rated order; {{checkbox_man_52.211-14[1]}} DO rated order");
	
			html = html.replaceFirst(Pattern.quote("[<i>Contracting Officer check appropriate box.</i>]"), "");

			String sPlaceholder = "Contracting Officer check appropriate box.";

			oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
				"checkbox_man_52.211-14[0]", "C", 1, // piFillInMaxSize, 
				1, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
				psFillInInsertClause);
			oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
				"checkbox_man_52.211-14[1]", "C", 1, // piFillInMaxSize, 
				1, null, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
				psFillInInsertClause);


			html = html.replace(_Fillin_Marker, "{{textbox}}&nbsp;");
			html = EcfrFillInUtils.processTextAndCheck(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
			html = HtmlUtils.parseFillinsAndNormalize(html);

			return html;
	}

	private static String process52_211_16 (int iStg1OfficialClauseId, String html, 
		TemplateStg2ClauseFillInRecord oTemplateStg2ClauseFillInRecord, 
		PreparedStatement psFillInInsertClause) throws SQLException {

		html = HtmlUtils.normalizeFillins(html);
		
		html = html.replaceFirst(Pattern.quote("_ Percent increase"), "Percent increase");
		html = html.replaceFirst(Pattern.quote("_ Percent decrease"), "Percent decrease");
		
		html = html.replaceFirst(Pattern.quote("[<i>Contracting Officer insert percentage</i>]"), "{{textbox_man_52.211-16[0]}}");

		String sPlaceholder = "Contracting Officer insert percentage";

		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.211-16[0]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);

		html = html.replaceFirst(Pattern.quote("[<i>Contracting Officer insert percentage</i>]"), "{{textbox_man_52.211-16[1]}}");

		sPlaceholder = "Contracting Officer insert percentage";

		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.211-16[1]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);

		html = html.replaceFirst(Pattern.quote("__*"), "{{textbox_man_52.211-16[2]}}");
		
		sPlaceholder = "Contracting Officer shall insert in the blank the designation(s) to which the percentages apply, such as (1) the total contract quantity, (2) item 1 only, (3) each quantity specified in the delivery schedule, (4) the total item quantity for each destination, or (5) the total quantity of each item without regard to destination.";

		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.211-16[2]", "M", 500, // piFillInMaxSize, 
			null, sPlaceholder, 2, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
		
		html = html.replace(_Fillin_Marker, "{{textbox}}&nbsp;");
		html = EcfrFillInUtils.processTextAndCheck(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
		html = HtmlUtils.parseFillinsAndNormalize(html);

		return html;
	}

	
	private static String process52_211_9 (int iStg1OfficialClauseId, String html, 
		TemplateStg2ClauseFillInRecord oTemplateStg2ClauseFillInRecord, 
		PreparedStatement psFillInInsertClause) throws SQLException {
	
		html = HtmlUtils.normalizeFillins(html);
		
		// Add the table.
		html = html.replaceFirst(Pattern.quote("<table border=\"1\" cellpadding=\"1\" cellspacing=\"1\" frame=\"void\" width=\"100%\"><thead><tr><th scope=\"col\">ITEM NO.</th><th scope=\"col\">QUANTITY</th><th scope=\"col\">WITHIN DAYS AFTER DATE OF CONTRACT</th></tr></thead><tbody><tr><td align=\"left\" scope=\"row\">____ </td><td align=\"left\">____ </td><td align=\"left\">____ </td></tr><tr><td align=\"left\" scope=\"row\">____ </td><td align=\"left\">____ </td><td align=\"left\">____ </td></tr><tr><td align=\"left\" scope=\"row\">____ </td><td align=\"left\">____ </td><td align=\"left\">____ </td></tr></tbody></table>"), 
				"{{tbl_man_52.211-9[0]}}{{tbl_man_52.211-9[1]}}{{tbl_man_52.211-9[2]}}");

		html = html.replaceFirst(Pattern.quote("[Contracting Officer insert specific details]"), "");
		
		oTemplateStg2ClauseFillInRecord.set2(html, iStg1OfficialClauseId,
				"tbl_man_52.211-9[0]", // psFillInCode,
				"S", // psFillInType,
				100, // piFillInMaxSize,
				0, // piFillInGroupNumber,
				"Contracting Officer insert specific details.", // psFillInPlaceholder,
				null, // psFillInDefaultData,
				1, // piFillInDisplayRows,
				true, // pbFillInForTable,
				"ITEM NO." // psFillInHeading
				);
		oTemplateStg2ClauseFillInRecord.insert(psFillInInsertClause);
		
		oTemplateStg2ClauseFillInRecord.set2(html, iStg1OfficialClauseId,
				"tbl_man_52.211-9[1]", // psFillInCode,
				"S", // psFillInType,
				100, // piFillInMaxSize,
				0, // piFillInGroupNumber,
				null, // psFillInPlaceholder,
				null, // psFillInDefaultData,
				1, // piFillInDisplayRows,
				true, // pbFillInForTable,
				"QUANTITY" // psFillInHeading
				);
		oTemplateStg2ClauseFillInRecord.insert(psFillInInsertClause);
		
		oTemplateStg2ClauseFillInRecord.set2(html, iStg1OfficialClauseId,
				"tbl_man_52.211-9[2]", // psFillInCode,
				"S", // psFillInType,
				100, // piFillInMaxSize,
				0, // piFillInGroupNumber,
				null, // psFillInPlaceholder,
				null, // psFillInDefaultData,
				1, // piFillInDisplayRows,
				true, // pbFillInForTable,
				"WITHIN DAYS AFTER DATE OF CONTRACT" // psFillInHeading
				);
		oTemplateStg2ClauseFillInRecord.insert(psFillInInsertClause);
		
		// Add the table.
		html = html.replaceFirst(Pattern.quote("<table border=\"1\" cellpadding=\"1\" cellspacing=\"1\" frame=\"void\" width=\"100%\"><thead><tr><th scope=\"col\">ITEM NO.</th><th scope=\"col\">QUANTITY</th><th scope=\"col\">WITHIN DAYS AFTER DATE OF CONTRACT</th></tr></thead><tbody><tr><td align=\"left\" scope=\"row\">____ </td><td align=\"left\">____ </td><td align=\"left\">____ </td></tr><tr><td align=\"left\" scope=\"row\">____ </td><td align=\"left\">____ </td><td align=\"left\">____ </td></tr><tr><td align=\"left\" scope=\"row\">____ </td><td align=\"left\">____ </td><td align=\"left\">____ </td></tr></tbody></table>"), 
				"{{tbl_man_52.211-9[3]}}{{tbl_man_52.211-9[4]}}{{tbl_man_52.211-9[5]}}");

		html = html.replaceFirst(Pattern.quote("[Contracting Officer insert specific details]"), "");
		
		oTemplateStg2ClauseFillInRecord.set2(html, iStg1OfficialClauseId,
				"tbl_man_52.211-9[3]", // psFillInCode,
				"S", // psFillInType,
				100, // piFillInMaxSize,
				1, // piFillInGroupNumber,
				"Contracting Officer insert specific details.", // psFillInPlaceholder,
				null, // psFillInDefaultData,
				1, // piFillInDisplayRows,
				true, // pbFillInForTable,
				"ITEM NO." // psFillInHeading
				);
		oTemplateStg2ClauseFillInRecord.insert(psFillInInsertClause);
		
		oTemplateStg2ClauseFillInRecord.set2(html, iStg1OfficialClauseId,
				"tbl_man_52.211-9[4]", // psFillInCode,
				"S", // psFillInType,
				100, // piFillInMaxSize,
				1, // piFillInGroupNumber,
				null, // psFillInPlaceholder,
				null, // psFillInDefaultData,
				1, // piFillInDisplayRows,
				true, // pbFillInForTable,
				"QUANTITY" // psFillInHeading
				);
		oTemplateStg2ClauseFillInRecord.insert(psFillInInsertClause);
		
		oTemplateStg2ClauseFillInRecord.set2(html, iStg1OfficialClauseId,
				"tbl_man_52.211-9[5]", // psFillInCode,
				"S", // psFillInType,
				100, // piFillInMaxSize,
				1, // piFillInGroupNumber,
				null, // psFillInPlaceholder,
				null, // psFillInDefaultData,
				1, // piFillInDisplayRows,
				true, // pbFillInForTable,
				"WITHIN DAYS AFTER DATE OF CONTRACT" // psFillInHeading
				);
		oTemplateStg2ClauseFillInRecord.insert(psFillInInsertClause);
		
		// Add the table.
		html = html.replaceFirst(Pattern.quote("<table border=\"1\" cellpadding=\"1\" cellspacing=\"1\" frame=\"void\" width=\"100%\"><thead><tr><th scope=\"col\">ITEM NO.</th><th scope=\"col\">QUANTITY</th><th scope=\"col\">WITHIN DAYS AFTER DATE OF CONTRACT</th></tr></thead><tbody><tr><td align=\"left\" scope=\"row\">____ </td><td align=\"left\">____ </td><td align=\"left\">____ </td></tr><tr><td align=\"left\" scope=\"row\">____ </td><td align=\"left\">____ </td><td align=\"left\">____ </td></tr><tr><td align=\"left\" scope=\"row\">____ </td><td align=\"left\">____ </td><td align=\"left\">____ </td></tr></tbody></table>"), 
				"{{tbl_man_52.211-9[6]}}{{tbl_man_52.211-9[7]}}{{tbl_man_52.211-9[8]}}");

		html = html.replaceFirst(Pattern.quote("[Contracting Officer insert specific details]"), "");
		
		oTemplateStg2ClauseFillInRecord.set2(html, iStg1OfficialClauseId,
				"tbl_man_52.211-9[6]", // psFillInCode,
				"S", // psFillInType,
				100, // piFillInMaxSize,
				2, // piFillInGroupNumber,
				null, // psFillInPlaceholder,
				null, // psFillInDefaultData,
				1, // piFillInDisplayRows,
				true, // pbFillInForTable,
				"ITEM NO." // psFillInHeading
				);
		oTemplateStg2ClauseFillInRecord.insert(psFillInInsertClause);
		
		oTemplateStg2ClauseFillInRecord.set2(html, iStg1OfficialClauseId,
				"tbl_man_52.211-9[7]", // psFillInCode,
				"S", // psFillInType,
				100, // piFillInMaxSize,
				2, // piFillInGroupNumber,
				null, // psFillInPlaceholder,
				null, // psFillInDefaultData,
				1, // piFillInDisplayRows,
				true, // pbFillInForTable,
				"QUANTITY" // psFillInHeading
				);
		oTemplateStg2ClauseFillInRecord.insert(psFillInInsertClause);
		
		oTemplateStg2ClauseFillInRecord.set2(html, iStg1OfficialClauseId,
				"tbl_man_52.211-9[8]", // psFillInCode,
				"S", // psFillInType,
				100, // piFillInMaxSize,
				2, // piFillInGroupNumber,
				null, // psFillInPlaceholder,
				null, // psFillInDefaultData,
				1, // piFillInDisplayRows,
				true, // pbFillInForTable,
				"WITHIN DAYS AFTER DATE OF CONTRACT" // psFillInHeading
				);
		oTemplateStg2ClauseFillInRecord.insert(psFillInInsertClause);
		html = html.replace(_Fillin_Marker, "{{textbox}}&nbsp;");
		html = EcfrFillInUtils.processTextAndCheck(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
		html = HtmlUtils.parseFillinsAndNormalize(html);
	
		return html;
	}
	
	private static String process52_211_9_AltI (int iStg1OfficialClauseId, String html, 
		TemplateStg2ClauseFillInRecord oTemplateStg2ClauseFillInRecord, 
		PreparedStatement psFillInInsertClause) throws SQLException {
	
		html = HtmlUtils.normalizeFillins(html);
		html = html.replaceFirst(Pattern.quote("__ [<i>Contracting Officer insert date</i>]"), "{{textbox_man_52.211-9_AltI[0]}}");
	
		String sPlaceholder = "Contracting Officer insert date";
	
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.211-9_AltI[0]", "S", 20, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
	
	
		html = html.replace(_Fillin_Marker, "{{textbox}}&nbsp;");
		html = EcfrFillInUtils.processTextAndCheck(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
		html = HtmlUtils.parseFillinsAndNormalize(html);
	
		return html;
	}
	
	private static String process52_211_9_AltII (int iStg1OfficialClauseId, String html, 
		TemplateStg2ClauseFillInRecord oTemplateStg2ClauseFillInRecord, 
		PreparedStatement psFillInInsertClause) throws SQLException {
	
		html = HtmlUtils.normalizeFillins(html);
		html = html.replaceFirst(Pattern.quote("__ [<i>Contracting Officer insert date</i>]"), "{{textbox_man_52.211-9_AltII[0]}}");
	
		String sPlaceholder = "Contracting Officer insert date";
	
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.211-9_AltII[0]", "S", 20, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
	
	
		html = html.replace(_Fillin_Marker, "{{textbox}}&nbsp;");
		html = EcfrFillInUtils.processTextAndCheck(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
		html = HtmlUtils.parseFillinsAndNormalize(html);
	
		return html;
	}
	
	private static String process52_212_4_AltI (int iStg1OfficialClauseId, String html, 
		TemplateStg2ClauseFillInRecord oTemplateStg2ClauseFillInRecord, 
		PreparedStatement psFillInInsertClause) throws SQLException {

		html = HtmlUtils.normalizeFillins(html);
		html = html.replaceFirst(Pattern.quote("[<i>Insert portion of labor rate attributable to profit.</i>]"), "{{textbox_man_52.212-4_AltI[0]}}");

		String sPlaceholder = "Insert portion of labor rate attributable to profit.";

		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.212-4_AltI[0]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);

		html = html.replaceFirst(Pattern.quote("[<i>Insert any subcontracts for services to be excluded from the hourly rates prescribed in the schedule.</i>]"), "{{textbox_man_52.212-4_AltI[1]}}");

		sPlaceholder = "Insert any subcontracts for services to be excluded from the hourly rates prescribed in the schedule.";

		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.212-4_AltI[1]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);

		html = html.replaceAll(Pattern.quote("&lsquo;"), "'");
		html = html.replaceAll(Pattern.quote("&rsquo;"), "'");
		
		Integer ibeg = html.indexOf("[Insert each element of other direct costs");
		Integer iend = html.indexOf( "]", ibeg+1);
		if (ibeg > 0 && iend > 0)
		{
			String s = html.substring(ibeg, iend+1);
			String html2 = html.replace (s, "{{textbox_man_52.212-4_AltI[2]}}");
			html = html2;
		}
		
		 ibeg = html.indexOf("[<i>Insert a fixed amount for the indirect costs and payment");
		 iend = html.indexOf( "]", ibeg+1);
		if (ibeg > 0 && iend > 0)
		{
			String s = html.substring(ibeg, iend+1);
			String html2 = html.replace (s, "{{textbox_man_52.212-4_AltI[3]}}");
			html = html2;
		}
		
		//html = html.replaceFirst(Pattern.quote("[Insert each element of other direct costs (<i>e.g., travel, computer usage charges, etc. Insert \"None\" if no reimbursement for other direct costs will be provided. If this is an indefinite delivery contract, the Contracting Officer may insert \"Each order must list separately the elements of other direct charge(s) for that order or, if no reimbursement for other direct costs will be provided, insert ''None''\".</i>]") ,
		//		"{{textbox_man_52.212-4_AltI[2]}}");
				
		sPlaceholder = "Insert each element of other direct costs (e.g., travel, computer usage charges, etc. Insert \"None\" if no reimbursement for other direct costs will be provided. If this is an indefinite delivery contract, the Contracting Officer may insert \"Each order must list separately the elements of other direct charge(s) for that order or, if no reimbursement for other direct costs will be provided, insert 'None'\".";
		
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
				"textbox_man_52.212-4_AltI[2]", "M", 250, // piFillInMaxSize, 
				null, sPlaceholder, 2, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
				psFillInInsertClause);
		
		//html = html.replaceFirst(Pattern.quote("[<i>Insert a fixed amount for the indirect costs and payment schedule. Insert \"$0\" if no fixed price reimbursement for indirect costs will be provided. (If this is an indefinite delivery contract, the Contracting Officer may insert \"Each order must list separately the fixed amount for the indirect costs and payment schedule or, if no reimbursement for indirect costs, insert ''None'').\"</i>]"),
		//		"{{textbox_man_52.212-4_AltI[3]}}");
		
		sPlaceholder = "Insert a fixed amount for the indirect costs and payment schedule. Insert \"$0\" if no fixed price reimbursement for indirect costs will be provided. (If this is an indefinite delivery contract, the Contracting Officer may insert \"Each order must list separately the fixed amount for the indirect costs and payment schedule or, if no reimbursement for indirect costs, insert 'None'\".";
		
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
				"textbox_man_52.212-4_AltI[3]", "M", 250, // piFillInMaxSize, 
				null, sPlaceholder, 2, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
				psFillInInsertClause);
		
		html = html.replace(_Fillin_Marker, "{{textbox}}&nbsp;");
		html = EcfrFillInUtils.processTextAndCheck(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
		html = HtmlUtils.parseFillinsAndNormalize(html);

		return html;
	}

	private static String process52_213_4 (int iStg1OfficialClauseId, String html, 
		TemplateStg2ClauseFillInRecord oTemplateStg2ClauseFillInRecord, 
		PreparedStatement psFillInInsertClause) throws SQLException {

		html = HtmlUtils.normalizeFillins(html);
		html = html.replaceFirst(Pattern.quote("____  ____ <p>[<i>Insert one or more Internet addresses</i>]"), "{{textbox_man_52.213-4[0]}}");

		String sPlaceholder = "Insert one or more Internet addresses";

		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.213-4[0]", "M", 250, // piFillInMaxSize, 
			null, sPlaceholder, 2, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);


		html = html.replace(_Fillin_Marker, "{{textbox}}&nbsp;");
		html = EcfrFillInUtils.processTextAndCheck(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
		html = HtmlUtils.parseFillinsAndNormalize(html);

		return html;
	}
	
	private static String process52_214_15 (int iStg1OfficialClauseId, String html, 
		TemplateStg2ClauseFillInRecord oTemplateStg2ClauseFillInRecord, 
		PreparedStatement psFillInInsertClause) throws SQLException {

		html = HtmlUtils.normalizeFillins(html);
		html = html.replaceFirst(Pattern.quote("_ calendar days (60 calendar days unless a different period is inserted by the bidder"), "{{textbox_man_52.214-15[0]}} calendar days ");

		String sPlaceholder = "60 calendar days unless a different period is inserted by the bidder";

		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.214-15[0]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);


		html = html.replace(_Fillin_Marker, "{{textbox}}&nbsp;");
		html = EcfrFillInUtils.processTextAndCheck(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
		html = HtmlUtils.parseFillinsAndNormalize(html);

		return html;
	}
	private static String process52_214_16 (int iStg1OfficialClauseId, String html, 
		TemplateStg2ClauseFillInRecord oTemplateStg2ClauseFillInRecord, 
		PreparedStatement psFillInInsertClause) throws SQLException {

		html = HtmlUtils.normalizeFillins(html);
		html = html.replaceFirst(Pattern.quote("_ calendar days [<i>the Contracting Officer shall insert the number of days</i>]"), 
				"{{textbox_man_52.214-16[0]}} calendar days ");

		String sPlaceholder = "the Contracting Officer shall insert the number of days";

		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.214-16[0]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);


		html = html.replace(_Fillin_Marker, "{{textbox}}&nbsp;");
		html = EcfrFillInUtils.processTextAndCheck(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
		html = HtmlUtils.parseFillinsAndNormalize(html);

		return html;
	}
	private static String process52_214_25 (int iStg1OfficialClauseId, String html, 
		TemplateStg2ClauseFillInRecord oTemplateStg2ClauseFillInRecord, 
		PreparedStatement psFillInInsertClause) throws SQLException {

		html = HtmlUtils.normalizeFillins(html);
		html = html.replaceFirst(Pattern.quote("__ [<i>the Contracting Officer shall insert the identification of the step-one request for technical proposals</i>]"), "{{textbox_man_52.214-25[0]}}");

		String sPlaceholder = "the Contracting Officer shall insert the identification of the step-one request for technical proposals";

		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.214-25[0]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);


		html = html.replace(_Fillin_Marker, "{{textbox}}&nbsp;");
		html = EcfrFillInUtils.processTextAndCheck(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
		html = HtmlUtils.parseFillinsAndNormalize(html);

		return html;
	}

	private static String process52_215_3 (int iStg1OfficialClauseId, String html, 
		TemplateStg2ClauseFillInRecord oTemplateStg2ClauseFillInRecord, 
		PreparedStatement psFillInInsertClause) throws SQLException {

		html = HtmlUtils.normalizeFillins(html);
		html = html.replaceFirst(Pattern.quote("[<i>state purpose</i>]"), "{{textbox_man_52.215-3[0]}}");

		String sPlaceholder = "state purpose";

		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.215-3[0]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);


		html = html.replace(_Fillin_Marker, "{{textbox}}&nbsp;");
		html = EcfrFillInUtils.processTextAndCheck(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
		html = HtmlUtils.parseFillinsAndNormalize(html);

		return html;
	}

	private static String process52_215_5 (int iStg1OfficialClauseId, String html, 
		TemplateStg2ClauseFillInRecord oTemplateStg2ClauseFillInRecord, 
		PreparedStatement psFillInInsertClause) throws SQLException {

		html = HtmlUtils.normalizeFillins(html);
		html = html.replaceFirst(Pattern.quote("[<i>insert telephone number</i>]"), "{{textbox_man_52.215-5[0]}}");

		String sPlaceholder = "insert telephone number";

		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.215-5[0]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);


		html = html.replace(_Fillin_Marker, "{{textbox}}&nbsp;");
		html = EcfrFillInUtils.processTextAndCheck(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
		html = HtmlUtils.parseFillinsAndNormalize(html);

		return html;
	}
	
	

	private static String process52_214_20_AltI (int iStg1OfficialClauseId, String html, 
		TemplateStg2ClauseFillInRecord oTemplateStg2ClauseFillInRecord, 
		PreparedStatement psFillInInsertClause) throws SQLException {

		html = HtmlUtils.normalizeFillins(html);
		html = html.replaceFirst(Pattern.quote("__ [<i>as appropriate, the Contracting Officer shall designate the contracting office or an alternate activity or office</i>]"), "{{textbox_man_52.214-20_AltI[0]}}");

		String sPlaceholder = "as appropriate, the Contracting Officer shall designate the contracting office or an alternate activity or office";

		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.214-20_AltI[0]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);


		html = html.replace(_Fillin_Marker, "{{textbox}}&nbsp;");
		html = EcfrFillInUtils.processTextAndCheck(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
		html = HtmlUtils.parseFillinsAndNormalize(html);

		return html;
	}

	private static String process52_214_20_AltII (int iStg1OfficialClauseId, String html, 
		TemplateStg2ClauseFillInRecord oTemplateStg2ClauseFillInRecord, 
		PreparedStatement psFillInInsertClause) throws SQLException {

		html = HtmlUtils.normalizeFillins(html);
		html = html.replaceFirst(Pattern.quote("__ [<i>as appropriate, the Contracting Officer shall designate the contracting office or an alternate activity or office</i>]"), "{{textbox_man_52.214-20_AltII[0]}}");

		String sPlaceholder = "as appropriate, the Contracting Officer shall designate the contracting office or an alternate activity or office";

		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.214-20_AltII[0]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);


		html = html.replace(_Fillin_Marker, "{{textbox}}&nbsp;");
		html = EcfrFillInUtils.processTextAndCheck(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
		html = HtmlUtils.parseFillinsAndNormalize(html);

		return html;
	}

	private static String process52_215_20_AltI (int iStg1OfficialClauseId, String html, 
		TemplateStg2ClauseFillInRecord oTemplateStg2ClauseFillInRecord, 
		PreparedStatement psFillInInsertClause) throws SQLException {

		html = HtmlUtils.normalizeFillins(html);
		html = html.replaceFirst(Pattern.quote("[Insert description of the data and format that are required, and include access to records necessary to permit an adequate evaluation of the proposed price in accordance with 15.408, Table 15-2, Note 2. The description may be inserted at the time of issuing the solicitation, or the Contracting Officer may specify that the offeror's format will be acceptable, or the description may be inserted as the result of negotiations.]"), "{{textbox_man_52.215-20_AltI[0]}}");

		String sPlaceholder = "Insert description of the data and format that are required, and include access to records necessary to permit an adequate evaluation of the proposed price in accordance with 15.408, Table 15-2, Note 2. The description may be inserted at the time of issuing the solicitation, or the Contracting Officer may specify that the offeror's format will be acceptable, or the description may be inserted as the result of negotiations.";

		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.215-20_AltI[0]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);


		html = html.replace(_Fillin_Marker, "{{textbox}}&nbsp;");
		html = EcfrFillInUtils.processTextAndCheck(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
		html = HtmlUtils.parseFillinsAndNormalize(html);

		return html;
	}
	private static String process52_215_20_AltIV (int iStg1OfficialClauseId, String html, 
			TemplateStg2ClauseFillInRecord oTemplateStg2ClauseFillInRecord, 
			PreparedStatement psFillInInsertClause) throws SQLException {

			html = HtmlUtils.normalizeFillins(html);
			html = html.replaceFirst(Pattern.quote("<i>[Insert description of the data and the format that are required, including the access to records necessary to permit an adequate evaluation of the proposed price in accordance with 15.403-3.]</i>"), 
					"{{textbox_man_52.215-20_AltIV[0]}}");

			String sPlaceholder = "Insert description of the data and the format that are required, including the access to records necessary to permit an adequate evaluation of the proposed price in accordance with 15.403-3.";

			oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
				"textbox_man_52.215-20_AltIV[0]", "M", 250, // piFillInMaxSize, 
				null, sPlaceholder, 2, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
				psFillInInsertClause);


			html = html.replace(_Fillin_Marker, "{{textbox}}&nbsp;");
			html = EcfrFillInUtils.processTextAndCheck(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
			html = HtmlUtils.parseFillinsAndNormalize(html);

			return html;
		}
	private static String process52_215_21_AltI (int iStg1OfficialClauseId, String html, 
		TemplateStg2ClauseFillInRecord oTemplateStg2ClauseFillInRecord, 
		PreparedStatement psFillInInsertClause) throws SQLException {

		html = HtmlUtils.normalizeFillins(html);
		html = html.replaceFirst(Pattern.quote("[Insert description of the data and format that are required and include access to records necessary to permit an adequate evaluation of the proposed price in accordance with 15.408, Table 15-2, Note 2. The description may be inserted at the time of issuing the solicitation, or the Contracting Officer may specify that the offeror's format will be acceptable, or the description may be inserted as the result of negotiations.]"), "{{textbox_man_52.215-21_AltI[0]}}");

		String sPlaceholder = "Insert description of the data and format that are required and include access to records necessary to permit an adequate evaluation of the proposed price in accordance with 15.408, Table 15-2, Note 2. The description may be inserted at the time of issuing the solicitation, or the Contracting Officer may specify that the offeror's format will be acceptable, or the description may be inserted as the result of negotiations.";

		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.215-21_AltI[0]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);


		html = html.replace(_Fillin_Marker, "{{textbox}}&nbsp;");
		html = EcfrFillInUtils.processTextAndCheck(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
		html = HtmlUtils.parseFillinsAndNormalize(html);

		return html;
	}

	private static String process52_215_21_AltIII (int iStg1OfficialClauseId, String html, 
		TemplateStg2ClauseFillInRecord oTemplateStg2ClauseFillInRecord, 
		PreparedStatement psFillInInsertClause) throws SQLException {

		html = HtmlUtils.normalizeFillins(html);
		html = html.replaceFirst(Pattern.quote("[<i>Insert media format</i>]"), "{{textbox_man_52.215-21_AltIII[0]}}");

		String sPlaceholder = "Insert media format";

		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.215-21_AltIII[0]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);


		html = html.replace(_Fillin_Marker, "{{textbox}}&nbsp;");
		html = EcfrFillInUtils.processTextAndCheck(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
		html = HtmlUtils.parseFillinsAndNormalize(html);

		return html;
	}

	private static String process52_215_21_AltIV (int iStg1OfficialClauseId, String html, 
		TemplateStg2ClauseFillInRecord oTemplateStg2ClauseFillInRecord, 
		PreparedStatement psFillInInsertClause) throws SQLException {

		html = HtmlUtils.normalizeFillins(html);
		html = html.replaceFirst(Pattern.quote("[Insert description of the data and the format that are required, including the access to records necessary to permit an adequate evaluation of the proposed price in accordance with 15.403-3.]"), "{{textbox_man_52.215-21_AltIV[0]}}");

		String sPlaceholder = "Insert description of the data and the format that are required, including the access to records necessary to permit an adequate evaluation of the proposed price in accordance with 15.403-3.";

		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.215-21_AltIV[0]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);


		html = html.replace(_Fillin_Marker, "{{textbox}}&nbsp;");
		html = EcfrFillInUtils.processTextAndCheck(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
		html = HtmlUtils.parseFillinsAndNormalize(html);

		return html;
	}
	
	private static String process52_216_1 (int iStg1OfficialClauseId, String html, 
		TemplateStg2ClauseFillInRecord oTemplateStg2ClauseFillInRecord, 
		PreparedStatement psFillInInsertClause) throws SQLException {
	
		html = HtmlUtils.normalizeFillins(html);
		html = html.replaceFirst(Pattern.quote("____  [<i>Contracting Officer insert specific type of contract</i>]"), "{{textbox_man_52.216-1[0]}}");
	
		String sPlaceholder = "Contracting Officer insert specific type of contract";
	
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.216-1[0]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
	
	
		html = html.replace(_Fillin_Marker, "{{textbox}}&nbsp;");
		html = EcfrFillInUtils.processTextAndCheck(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
		html = HtmlUtils.parseFillinsAndNormalize(html);
	
		return html;
	}
	
	private static String process52_216_10 (int iStg1OfficialClauseId, String html, 
		TemplateStg2ClauseFillInRecord oTemplateStg2ClauseFillInRecord, 
		PreparedStatement psFillInInsertClause) throws SQLException {
	
		html = HtmlUtils.normalizeFillins(html);
		html = html.replaceFirst(Pattern.quote("_ [<i>Contracting Officer insert Contractor's participation</i>]"), "{{textbox_man_52.216-10[0]}}");
	
		String sPlaceholder = "Contracting Officer insert Contractor's participation";
	
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.216-10[0]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
	
		html = html.replaceFirst(Pattern.quote("_ [<i>Contracting Officer insert Contractor's participation</i>]"), "{{textbox_man_52.216-10[1]}}");
	
		sPlaceholder = "Contracting Officer insert Contractor's participation";
	
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.216-10[1]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
	
		html = html.replaceFirst(Pattern.quote("_ [<i>Contracting Officer insert percentage</i>]"), "{{textbox_man_52.216-10[2]}}");
	
		sPlaceholder = "Contracting Officer insert percentage";
	
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.216-10[2]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
	
		html = html.replaceFirst(Pattern.quote("_ [<i>Contracting Officer insert percentage</i>]"), "{{textbox_man_52.216-10[3]}}");
	
		sPlaceholder = "Contracting Officer insert percentage";
	
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.216-10[3]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
	
	
		html = html.replace(_Fillin_Marker, "{{textbox}}&nbsp;");
		html = EcfrFillInUtils.processTextAndCheck(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
		html = HtmlUtils.parseFillinsAndNormalize(html);
	
		return html;
	}
	
	private static String process52_216_2 (int iStg1OfficialClauseId, String html, 
		TemplateStg2ClauseFillInRecord oTemplateStg2ClauseFillInRecord, 
		PreparedStatement psFillInInsertClause) throws SQLException {
	
		html = HtmlUtils.normalizeFillins(html);
		html = html.replaceFirst(Pattern.quote("__ [<i>offeror insert Schedule line item number</i>]"), "{{textbox_man_52.216-2[0]}}");
	
		String sPlaceholder = "offeror insert Schedule line item number";
	
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.216-2[0]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
	
	
		html = html.replace(_Fillin_Marker, "{{textbox}}&nbsp;");
		html = EcfrFillInUtils.processTextAndCheck(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
		html = HtmlUtils.parseFillinsAndNormalize(html);
	
		return html;
	}
	
	private static String process52_216_3 (int iStg1OfficialClauseId, String html, 
		TemplateStg2ClauseFillInRecord oTemplateStg2ClauseFillInRecord, 
		PreparedStatement psFillInInsertClause) throws SQLException {
	
		html = HtmlUtils.normalizeFillins(html);
		html = html.replaceFirst(Pattern.quote("__ [<i>offeror insert Schedule line item number</i>]"), "{{textbox_man_52.216-3[0]}}");
	
		String sPlaceholder = "offeror insert Schedule line item number";
	
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.216-3[0]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
	
		html = html.replace(_Fillin_Marker, "{{textbox}}&nbsp;");
		html = EcfrFillInUtils.processTextAndCheck(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
		html = HtmlUtils.parseFillinsAndNormalize(html);
	
		return html;
	}
	
	private static String process52_216_6 (int iStg1OfficialClauseId, String html, 
		TemplateStg2ClauseFillInRecord oTemplateStg2ClauseFillInRecord, 
		PreparedStatement psFillInInsertClause) throws SQLException {
	
		html = HtmlUtils.normalizeFillins(html);
		html = html.replaceFirst(Pattern.quote("__ [<i>insert dollar amount of ceiling price</i>]"), "{{textbox_man_52.216-6[0]}}");
	
		String sPlaceholder = "insert dollar amount of ceiling price";
	
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.216-6[0]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
	
		html = html.replaceFirst(Pattern.quote("_ [<i>Contracting Officer insert number of days</i>]"), "{{textbox_man_52.216-6[1]}}");
	
		sPlaceholder = "Contracting Officer insert number of days";
	
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.216-6[1]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
	
	
		html = html.replace(_Fillin_Marker, "{{textbox}}&nbsp;");
		html = EcfrFillInUtils.processTextAndCheck(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
		html = HtmlUtils.parseFillinsAndNormalize(html);
	
		return html;
	}
	
	private static String process52_216_7 (int iStg1OfficialClauseId, String html, 
		TemplateStg2ClauseFillInRecord oTemplateStg2ClauseFillInRecord, 
		PreparedStatement psFillInInsertClause) throws SQLException {
	
		html = HtmlUtils.normalizeFillins(html);
		html = html.replaceFirst(Pattern.quote("____ [Contracting Officer insert day as prescribed by agency head; if not prescribed, insert \"30th\"]"),
				"{{textbox_man_52.216-7[0]}}");
		html = html.replaceFirst(Pattern.quote("____ [Contracting Officer insert day as prescribed by agency head; if not prescribed, insert \"30th\"]"),
				"{{textbox_man_52.216-7[0]}}");
		String sPlaceholder = "Contracting Officer insert day as prescribed by agency head; if not prescribed, insert \"30th\"";
	
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.216-7[0]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
	
		html = html.replace(_Fillin_Marker, "{{textbox}}&nbsp;");
		html = EcfrFillInUtils.processTextAndCheck(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
		html = HtmlUtils.parseFillinsAndNormalize(html);
	
		return html;
	}
	
	private static String process52_216_16 (int iStg1OfficialClauseId, String html, 
		TemplateStg2ClauseFillInRecord oTemplateStg2ClauseFillInRecord, 
		PreparedStatement psFillInInsertClause) throws SQLException {
	
		html = HtmlUtils.normalizeFillins(html);
		html = html.replaceFirst(Pattern.quote("__ [<i>Contracting Officer insert Schedule line item numbers</i>]"), "{{textbox_man_52.216-16[0]}}");
	
		String sPlaceholder = "Contracting Officer insert Schedule line item numbers";
	
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.216-16[0]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
	
		html = html.replaceFirst(Pattern.quote("__ [<i>Contracting Officer insert number of days</i>]"), "{{textbox_man_52.216-16[1]}}");
	
		sPlaceholder = "Contracting Officer insert number of days";
	
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.216-16[1]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
	
		html = html.replaceFirst(Pattern.quote("_ [<i>Contracting Officer insert percent</i>]"), "{{textbox_man_52.216-16[2]}}");
	
		sPlaceholder = "Contracting Officer insert percent";
	
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.216-16[2]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
	
		html = html.replaceFirst(Pattern.quote("_ [<i>Contracting Officer insert percent</i>]"), "{{textbox_man_52.216-16[3]}}");
	
		sPlaceholder = "Contracting Officer insert percent";
	
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.216-16[3]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
	
	
		html = html.replace(_Fillin_Marker, "{{textbox}}&nbsp;");
		html = EcfrFillInUtils.processTextAndCheck(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
		html = HtmlUtils.parseFillinsAndNormalize(html);
	
		return html;
	}
	
	private static String process52_216_17 (int iStg1OfficialClauseId, String html, 
		TemplateStg2ClauseFillInRecord oTemplateStg2ClauseFillInRecord, 
		PreparedStatement psFillInInsertClause) throws SQLException {
	
		html = HtmlUtils.normalizeFillins(html);
		html = html.replaceFirst(Pattern.quote("__ [<i>Contracting Officer insert line item numbers</i>]"), "{{textbox_man_52.216-17[0]}}");
	
		String sPlaceholder = "Contracting Officer insert line item numbers";
	
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.216-17[0]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
	
		html = html.replaceFirst(Pattern.quote("_ [<i>Contracting Officer insert percent</i>]"), "{{textbox_man_52.216-17[1]}}");
	
		sPlaceholder = "Contracting Officer insert percent";
	
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.216-17[1]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
	
		html = html.replaceFirst(Pattern.quote("_ [<i>Contracting Officer insert number of days</i>]"), "{{textbox_man_52.216-17[2]}}");
	
		sPlaceholder = "Contracting Officer insert number of days";
	
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.216-17[2]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
	
		html = html.replaceFirst(Pattern.quote("__ [<i>see Note 1</i>]"), "{{textbox_man_52.216-17[3]}}");
	
		sPlaceholder = "Note: The degree of completion may be based on a percentage of contract performance or any other reasonable basis";
	
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.216-17[3]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
	
		html = html.replaceFirst(Pattern.quote("_ percent [<i>see Note 2</i>]"), " {{textbox_man_52.216-17[4]}} percent ");
	
		sPlaceholder = "Note: The language may be changed to describe a negotiated adjustment pattern under which the extent of adjustment is not the same for all levels of cost variation";
	
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.216-17[4]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
	
		html = html.replaceFirst(Pattern.quote("_ percent [<i>Contracting Officer insert percents</i>]"), "{{textbox_man_52.216-17[5]}}");
	
		sPlaceholder = "Contracting Officer insert percents";
	
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.216-17[5]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
	
		html = html.replaceFirst(Pattern.quote("__ [<i>Contracting Officer insert number of days</i>]"), "{{textbox_man_52.216-17[6]}}");
	
		sPlaceholder = "Contracting Officer insert number of days";
	
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.216-17[6]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
	
	
		html = html.replace(_Fillin_Marker, "{{textbox}}&nbsp;");
		html = EcfrFillInUtils.processTextAndCheck(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
		html = HtmlUtils.parseFillinsAndNormalize(html);
	
		return html;
	}
	
	private static String process52_216_18 (int iStg1OfficialClauseId, String html, 
		TemplateStg2ClauseFillInRecord oTemplateStg2ClauseFillInRecord, 
		PreparedStatement psFillInInsertClause) throws SQLException {
	
		html = HtmlUtils.normalizeFillins(html);
		html = html.replaceFirst(Pattern.quote("____  [<i>insert dates</i>]"), "{{textbox_man_52.216-18[0]}}");
		html = html.replaceFirst(Pattern.quote("____"), "{{textbox_man_52.216-18[1]}}");
		
		String sPlaceholder = "insert date";
	
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.216-18[0]", "S", 20, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.216-18[1]", "S", 20, // piFillInMaxSize, 
				null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
				psFillInInsertClause);
	
		html = html.replace(_Fillin_Marker, "{{textbox}}&nbsp;");
		html = EcfrFillInUtils.processTextAndCheck(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
		html = HtmlUtils.parseFillinsAndNormalize(html);
	
		return html;
	}
	
	private static String process52_216_19 (int iStg1OfficialClauseId, String html, 
		TemplateStg2ClauseFillInRecord oTemplateStg2ClauseFillInRecord, 
		PreparedStatement psFillInInsertClause) throws SQLException {
	
		html = HtmlUtils.normalizeFillins(html);
		html = html.replaceFirst(Pattern.quote("__ [<i>insert dollar figure or quantity</i>]"), "{{textbox_man_52.216-19[0]}}");
	
		String sPlaceholder = "insert dollar figure or quantity";
	
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.216-19[0]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
	
		html = html.replaceFirst(Pattern.quote("__ [<i>insert dollar figure or quantity</i>]"), "{{textbox_man_52.216-19[1]}}");
	
		sPlaceholder = "insert dollar figure or quantity";
	
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.216-19[1]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
	
		html = html.replaceFirst(Pattern.quote("__ [<i>insert dollar figure or quantity</i>]"), "{{textbox_man_52.216-19[2]}}");
	
		sPlaceholder = "insert dollar figure or quantity";
	
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.216-19[2]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
	
	
		html = html.replace(_Fillin_Marker, "{{textbox}}&nbsp;");
		html = EcfrFillInUtils.processTextAndCheck(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
		html = HtmlUtils.parseFillinsAndNormalize(html);
	
		return html;
	}
	
	private static String process52_216_20 (int iStg1OfficialClauseId, String html, 
		TemplateStg2ClauseFillInRecord oTemplateStg2ClauseFillInRecord, 
		PreparedStatement psFillInInsertClause) throws SQLException {
	
		html = HtmlUtils.normalizeFillins(html);
		html = html.replaceFirst(Pattern.quote("__ [<i>insert date</i>]"), "{{textbox_man_52.216-20[0]}}");
	
		String sPlaceholder = "insert date";
	
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.216-20[0]", "S", 20, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
	
	
		html = html.replace(_Fillin_Marker, "{{textbox}}&nbsp;");
		html = EcfrFillInUtils.processTextAndCheck(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
		html = HtmlUtils.parseFillinsAndNormalize(html);
	
		return html;
	}
	
	private static String process52_216_21 (int iStg1OfficialClauseId, String html, 
		TemplateStg2ClauseFillInRecord oTemplateStg2ClauseFillInRecord, 
		PreparedStatement psFillInInsertClause) throws SQLException {
	
		html = HtmlUtils.normalizeFillins(html);
		html = html.replaceFirst(Pattern.quote("__ [<i>insert date</i>]"), "{{textbox_man_52.216-21[0]}}");
	
		String sPlaceholder = "insert date";
	
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.216-21[0]", "S", 20, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
	
	
		html = html.replace(_Fillin_Marker, "{{textbox}}&nbsp;");
		html = EcfrFillInUtils.processTextAndCheck(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
		html = HtmlUtils.parseFillinsAndNormalize(html);
	
		return html;
	}
	
	private static String process52_216_22 (int iStg1OfficialClauseId, String html, 
		TemplateStg2ClauseFillInRecord oTemplateStg2ClauseFillInRecord, 
		PreparedStatement psFillInInsertClause) throws SQLException {
	
		html = HtmlUtils.normalizeFillins(html);
		html = html.replaceFirst(Pattern.quote("__ [<i>insert date</i>]"), "{{textbox_man_52.216-22[0]}}");
	
		String sPlaceholder = "insert date";
	
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.216-22[0]", "S", 20, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
	
	
		html = html.replace(_Fillin_Marker, "{{textbox}}&nbsp;");
		html = EcfrFillInUtils.processTextAndCheck(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
		html = HtmlUtils.parseFillinsAndNormalize(html);
	
		return html;
	}
	
	private static String process52_216_23 (int iStg1OfficialClauseId, String html, 
		TemplateStg2ClauseFillInRecord oTemplateStg2ClauseFillInRecord, 
		PreparedStatement psFillInInsertClause) throws SQLException {
	
		html = HtmlUtils.normalizeFillins(html);
		html = html.replaceFirst(Pattern.quote("__ [<i>insert date</i>]"), "{{textbox_man_52.216-23[0]}}");
	
		String sPlaceholder = "insert date";
	
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.216-23[0]", "S", 20, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
	
	
		html = html.replace(_Fillin_Marker, "{{textbox}}&nbsp;");
		html = EcfrFillInUtils.processTextAndCheck(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
		html = HtmlUtils.parseFillinsAndNormalize(html);
	
		return html;
	}
	
	private static String process52_216_25 (int iStg1OfficialClauseId, String html, 
		TemplateStg2ClauseFillInRecord oTemplateStg2ClauseFillInRecord, 
		PreparedStatement psFillInInsertClause) throws SQLException {
	
		html = HtmlUtils.normalizeFillins(html);
		html = html.replaceFirst(Pattern.quote("____  <i>[insert specific type of contract]"), "{{textbox_man_52.216-25[6]}}");
	
		String sPlaceholder = "insert specific type of contract";
	
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.216-25[6]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
	
		html = html.replaceFirst(Pattern.quote("____  <i>[insert specific type of proposal (e.g., fixed-price or cost-and-fee)]</i>"), "{{textbox_man_52.216-25[5]}}");
		
		sPlaceholder = "insert specific type of proposal (e.g., fixed-price or cost-and-fee)";
		
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.216-25[5]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
		
		html = html.replaceFirst(Pattern.quote(" [<i>insert target date for definitization of the contract and dates for submission of proposal, beginning of negotiations, and, if appropriate, submission of make-or-buy and subcontracting plans and certified cost or pricing data</i>]"), "");
		html = html.replaceFirst(Pattern.quote("____  ____  ____  ____ "), "{{textbox_man_52.216-25[0]}}<br>{{textbox_man_52.216-25[1]}}<br>{{textbox_man_52.216-25[2]}}<br>{{textbox_man_52.216-25[3]}}<br>");

		sPlaceholder = "insert target date for definitization of the contract and dates for submission of proposal, beginning of negotiations, and, if appropriate, submission of make-or-buy and subcontracting plans and certified cost or pricing data";
	
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.216-25[0]", "S", 20, // piFillInMaxSize, 
			0, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.216-25[1]", "S", 20, // piFillInMaxSize, 
			0, null, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.216-25[2]", "S", 20, // piFillInMaxSize, 
			0, null, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.216-25[3]", "S", 20, // piFillInMaxSize, 
			null, null, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
	
	
		html = html.replace(_Fillin_Marker, "{{textbox}}&nbsp;");
		html = EcfrFillInUtils.processTextAndCheck(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
		html = HtmlUtils.parseFillinsAndNormalize(html);
	
		return html;
	}

	private static String process52_216_25_AltI (int iStg1OfficialClauseId, String html, 
		TemplateStg2ClauseFillInRecord oTemplateStg2ClauseFillInRecord, 
		PreparedStatement psFillInInsertClause) throws SQLException {

		html = HtmlUtils.normalizeFillins(html);
		html = html.replaceFirst(Pattern.quote("__ [<i>insert price ceiling</i> or <i>firm fixed price</i>]"), "{{textbox_man_52.216-25_AltI[0]}}");

		String sPlaceholder = "insert price ceiling or firm fixed price";

		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.216-25_AltI[0]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);

		html = html.replaceFirst(Pattern.quote("__ [<i>insert the proposed price upon which the award was based</i>]"), "{{textbox_man_52.216-25_AltI[1]}}");

		sPlaceholder = "insert the proposed price upon which the award was based";

		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.216-25_AltI[1]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);


		html = html.replace(_Fillin_Marker, "{{textbox}}&nbsp;");
		html = EcfrFillInUtils.processTextAndCheck(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
		html = HtmlUtils.parseFillinsAndNormalize(html);

		return html;
	}

	private static String process52_217_6 (int iStg1OfficialClauseId, String html, 
		TemplateStg2ClauseFillInRecord oTemplateStg2ClauseFillInRecord, 
		PreparedStatement psFillInInsertClause) throws SQLException {

		html = HtmlUtils.normalizeFillins(html);
		html = html.replaceFirst(Pattern.quote("__ [<i>insert in the clause the period of time in which the Contracting Officer has to exercise the option</i>]"), "{{textbox_man_52.217-6[0]}}");

		String sPlaceholder = "insert in the clause the period of time in which the Contracting Officer has to exercise the option";

		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.217-6[0]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);


		html = html.replace(_Fillin_Marker, "{{textbox}}&nbsp;");
		html = EcfrFillInUtils.processTextAndCheck(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
		html = HtmlUtils.parseFillinsAndNormalize(html);

		return html;
	}

	private static String process52_217_7 (int iStg1OfficialClauseId, String html, 
		TemplateStg2ClauseFillInRecord oTemplateStg2ClauseFillInRecord, 
		PreparedStatement psFillInInsertClause) throws SQLException {

		html = HtmlUtils.normalizeFillins(html);
		html = html.replaceFirst(Pattern.quote("__ [<i>insert in the clause the period of time in which the Contracting Officer has to exercise the option</i>]"), "{{textbox_man_52.217-7[0]}}");

		String sPlaceholder = "insert in the clause the period of time in which the Contracting Officer has to exercise the option";

		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.217-7[0]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);


		html = html.replace(_Fillin_Marker, "{{textbox}}&nbsp;");
		html = EcfrFillInUtils.processTextAndCheck(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
		html = HtmlUtils.parseFillinsAndNormalize(html);

		return html;
	}

	private static String process52_217_8 (int iStg1OfficialClauseId, String html, 
		TemplateStg2ClauseFillInRecord oTemplateStg2ClauseFillInRecord, 
		PreparedStatement psFillInInsertClause) throws SQLException {

		html = HtmlUtils.normalizeFillins(html);
		html = html.replaceFirst(Pattern.quote("_ [insert the period of time within which the Contracting Officer may exercise the option]"), "{{textbox_man_52.217-8[0]}}");

		String sPlaceholder = "insert the period of time within which the Contracting Officer may exercise the option";

		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.217-8[0]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);


		html = html.replace(_Fillin_Marker, "{{textbox}}&nbsp;");
		html = EcfrFillInUtils.processTextAndCheck(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
		html = HtmlUtils.parseFillinsAndNormalize(html);

		return html;
	}

	private static String process52_217_9 (int iStg1OfficialClauseId, String html, 
		TemplateStg2ClauseFillInRecord oTemplateStg2ClauseFillInRecord, 
		PreparedStatement psFillInInsertClause) throws SQLException {

		html = HtmlUtils.normalizeFillins(html);
		html = html.replaceFirst(Pattern.quote("____  [insert the period of time within which the Contracting Officer may exercise the option]"), "{{textbox_man_52.217-9[0]}}");

		String sPlaceholder = "insert the period of time within which the Contracting Officer may exercise the option";

		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.217-9[0]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);

		html = html.replaceFirst(Pattern.quote("_ days [60 days unless a different number of days is inserted]"), "{{textbox_man_52.217-9[1]}} days");

		sPlaceholder = "60 days unless a different number of days is inserted";

		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.217-9[1]", "N", 12, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);

		html = html.replaceFirst(Pattern.quote("_ (months) (years)"), "{{textbox_man_52.217-9[2]}}&nbsp;{{textbox_man_52.217-9[3]}}");

		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.217-9[2]", "S", 100, // piFillInMaxSize, 
			null, "months", null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
		
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
				"textbox_man_52.217-9[3]", "S", 100, // piFillInMaxSize, 
				null, "years" , null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
				psFillInInsertClause);
		
		html = html.replace(_Fillin_Marker, "{{textbox}}&nbsp;");
		html = EcfrFillInUtils.processTextAndCheck(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
		html = HtmlUtils.parseFillinsAndNormalize(html);

		return html;
	}
	
	private static String process52_219_1 (int iStg1OfficialClauseId, String html, 
			TemplateStg2ClauseFillInRecord oTemplateStg2ClauseFillInRecord, 
			PreparedStatement psFillInInsertClause) throws SQLException {

			html = HtmlUtils.normalizeFillins(html);
			html = html.replaceFirst(Pattern.quote("____ [<i>insert NAICS code</i>]"), "{{textbox_man_52.219-1[0]}}");

			String sPlaceholder = "insert NAICS code";

			oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
				"textbox_man_52.219-1[0]", "S", 100, // piFillInMaxSize, 
				null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
				psFillInInsertClause);

			html = html.replaceFirst(Pattern.quote("____ [<i>insert size standard</i>]"), "{{textbox_man_52.219-1[1]}}");

			sPlaceholder = "insert size standard";

			oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
				"textbox_man_52.219-1[1]", "S", 100, // piFillInMaxSize, 
				null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
				psFillInInsertClause);
			/*

			html = html.replaceFirst(Pattern.quote("[<i>Complete only if the offeror represented itself as a small business concern in paragraph (c)(1) of this provision.</i>]"), "{{textbox_man_52.219-1[2]}}");

			sPlaceholder = "Complete only if the offeror represented itself as a small business concern in paragraph (c)(1) of this provision.";

			oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
				"textbox_man_52.219-1[2]", "S", 100, // piFillInMaxSize, 
				null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
				psFillInInsertClause);

			html = html.replaceFirst(Pattern.quote("[<i>Complete only if the offeror represented itself as a small business concern in paragraph (c)(1) of this provision.</i>]"), "{{textbox_man_52.219-1[3]}}");

			sPlaceholder = "Complete only if the offeror represented itself as a small business concern in paragraph (c)(1) of this provision.";

			oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
				"textbox_man_52.219-1[3]", "S", 100, // piFillInMaxSize, 
				null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
				psFillInInsertClause);

			html = html.replaceFirst(Pattern.quote("[<i>Complete only if the offeror represented itself as a women-owned small business concern in paragraph (c)(3) of this provision.</i>]"), "{{textbox_man_52.219-1[4]}}");

			sPlaceholder = "Complete only if the offeror represented itself as a women-owned small business concern in paragraph (c)(3) of this provision.";

			oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
				"textbox_man_52.219-1[4]", "S", 100, // piFillInMaxSize, 
				null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
				psFillInInsertClause);

			html = html.replaceFirst(Pattern.quote("[<i>Complete only if the offeror represented itself as a women-owned small business concern eligible under the WOSB Program in (c)(4) of this provision.</i>]"), "{{textbox_man_52.219-1[5]}}");

			sPlaceholder = "Complete only if the offeror represented itself as a women-owned small business concern eligible under the WOSB Program in (c)(4) of this provision.";

			oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
				"textbox_man_52.219-1[5]", "S", 100, // piFillInMaxSize, 
				null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
				psFillInInsertClause);

			html = html.replaceFirst(Pattern.quote("[<i>Complete only if the offeror represented itself as a small business concern in paragraph (c)(1) of this provision.</i>]"), "{{textbox_man_52.219-1[6]}}");

			sPlaceholder = "Complete only if the offeror represented itself as a small business concern in paragraph (c)(1) of this provision.";

			oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
				"textbox_man_52.219-1[6]", "S", 100, // piFillInMaxSize, 
				null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
				psFillInInsertClause);

			html = html.replaceFirst(Pattern.quote("[<i>Complete only if the offeror represented itself as a veteran-owned small business concern in paragraph (c)(6) of this provision.</i>]"), "{{textbox_man_52.219-1[7]}}");

			sPlaceholder = "Complete only if the offeror represented itself as a veteran-owned small business concern in paragraph (c)(6) of this provision.";

			oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
				"textbox_man_52.219-1[7]", "S", 100, // piFillInMaxSize, 
				null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
				psFillInInsertClause);

			html = html.replaceFirst(Pattern.quote("[<i>Complete only if the offeror represented itself as a small business concern in paragraph (c)(1) of this provision.</i>]"), "{{textbox_man_52.219-1[8]}}");

			sPlaceholder = "Complete only if the offeror represented itself as a small business concern in paragraph (c)(1) of this provision.";

			oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
				"textbox_man_52.219-1[8]", "S", 100, // piFillInMaxSize, 
				null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
				psFillInInsertClause);


			html = html.replace(_Fillin_Marker, "{{textbox}}&nbsp;");
			html = EcfrFillInUtils.processTextAndCheck(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
			html = HtmlUtils.parseFillinsAndNormalize(html);
			*/
			return html;
		}
	
	private static String process52_219_1_AltI (int iStg1OfficialClauseId, String html, 
		TemplateStg2ClauseFillInRecord oTemplateStg2ClauseFillInRecord, 
		PreparedStatement psFillInInsertClause) throws SQLException {
	
		html = HtmlUtils.normalizeFillins(html);
		html = html.replaceFirst(Pattern.quote("[<i>Complete if offeror represented itself as disadvantaged in paragraph (c)(2) of this provision."), "");
		html = html.replaceFirst(Pattern.quote("__ Black American"), "{{checkbox_man_52.219-1_AltI[0]}} Black American");
		html = html.replaceFirst(Pattern.quote("__ Hispanic American"), "{{checkbox_man_52.219-1_AltI[1]}} Hispanic American");
		html = html.replaceFirst(Pattern.quote("__ Native American"), "{{checkbox_man_52.219-1_AltI[2]}} Native American");
		html = html.replaceFirst(Pattern.quote("__ Asian-Pacific American"), "{{checkbox_man_52.219-1_AltI[3]}} Asian-Pacific American");
		html = html.replaceFirst(Pattern.quote("__ Subcontinent Asian"), "{{checkbox_man_52.219-1_AltI[4]}} Subcontinent Asian");
		html = html.replaceFirst(Pattern.quote("__ Individual/concern"), "{{checkbox_man_52.219-1_AltI[5]}} Individual/concern");		
		
		String sPlaceholder = "Complete if offeror represented itself as disadvantaged in paragraph (c)(2) of this provision.";
	
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"checkbox_man_52.219-1_AltI[0]", "C", 1, // piFillInMaxSize, 
			null, null, 1, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"checkbox_man_52.219-1_AltI[1]", "C", 1, // piFillInMaxSize, 
			null, null, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"checkbox_man_52.219-1_AltI[2]", "C", 1, // piFillInMaxSize, 
			null, null, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"checkbox_man_52.219-1_AltI[3]", "C", 1, // piFillInMaxSize, 
			null, null, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"checkbox_man_52.219-1_AltI[4]", "C", 1, // piFillInMaxSize, 
			null, null, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"checkbox_man_52.219-1_AltI[5]", "C", 1, // piFillInMaxSize, 
			null, null, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
		
		//html = html.replace(_Fillin_Marker, "{{textbox}}&nbsp;");
		//html = EcfrFillInUtils.processTextAndCheck(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
		//html = HtmlUtils.parseFillinsAndNormalize(html);
	
		return html;
	}
	
	private static String process52_219_10 (int iStg1OfficialClauseId, String html, 
		TemplateStg2ClauseFillInRecord oTemplateStg2ClauseFillInRecord, 
		PreparedStatement psFillInInsertClause) throws SQLException {
	
		html = HtmlUtils.normalizeFillins(html);
		html = html.replaceFirst(Pattern.quote("_ [<i>Contracting Officer to insert the appropriate number between 0 and 10</i>]"), "{{textbox_man_52.219-10[0]}}");
	
		String sPlaceholder = "Contracting Officer to insert the appropriate number between 0 and 10";
	
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.219-10[0]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
	
	
		html = html.replace(_Fillin_Marker, "{{textbox}}&nbsp;");
		html = EcfrFillInUtils.processTextAndCheck(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
		html = HtmlUtils.parseFillinsAndNormalize(html);
	
		return html;
	}
	
	private static String process52_219_11 (int iStg1OfficialClauseId, String html, 
		TemplateStg2ClauseFillInRecord oTemplateStg2ClauseFillInRecord, 
		PreparedStatement psFillInInsertClause) throws SQLException {
	
		html = HtmlUtils.normalizeFillins(html);
		html = html.replaceFirst(Pattern.quote("__ [<i>insert name of contracting agency</i>]"), "{{textbox_man_52.219-11[0]}}");
	
		String sPlaceholder = "insert name of contracting agency";
	
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.219-11[0]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
	
		html = html.replaceFirst(Pattern.quote("__ [<i>insert name of contracting agency</i>]"), "{{textbox_man_52.219-11[1]}}");
	
		sPlaceholder = "insert name of contracting agency";
	
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.219-11[1]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
	
		html = html.replaceFirst(Pattern.quote("__ [<i>insert name of contracting agency</i>]"), "{{textbox_man_52.219-11[2]}}");
	
		sPlaceholder = "insert name of contracting agency";
	
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.219-11[2]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
	
		html = html.replaceFirst(Pattern.quote("[<i>insert name of contracting agency</i>]"), "{{textbox_man_52.219-11[3]}}");
	
		sPlaceholder = "insert name of contracting agency";
	
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.219-11[3]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
	
	
		html = html.replace(_Fillin_Marker, "{{textbox}}&nbsp;");
		html = EcfrFillInUtils.processTextAndCheck(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
		html = HtmlUtils.parseFillinsAndNormalize(html);
	
		return html;
	}
	
	private static String process52_219_12 (int iStg1OfficialClauseId, String html, 
		TemplateStg2ClauseFillInRecord oTemplateStg2ClauseFillInRecord, 
		PreparedStatement psFillInInsertClause) throws SQLException {
	
		html = HtmlUtils.normalizeFillins(html);
		html = html.replaceFirst(Pattern.quote("__ [<i>insert number of contract</i>]"), "{{textbox_man_52.219-12[0]}}");
	
		String sPlaceholder = "insert number of contract";
	
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.219-12[0]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
	
		html = html.replaceFirst(Pattern.quote("__ [<i>insert name of contracting agency</i>]"), "{{textbox_man_52.219-12[1]}}");
	
		sPlaceholder = "insert name of contracting agency";
	
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.219-12[1]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
	
		html = html.replaceFirst(Pattern.quote("__ [<i>insert name of subcontractor</i>]"), "{{textbox_man_52.219-12[2]}}");
	
		sPlaceholder = "insert name of subcontractor";
	
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.219-12[2]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
	
		html = html.replaceFirst(Pattern.quote("__ [<i>insert number of contract</i>]"), "{{textbox_man_52.219-12[3]}}");
	
		sPlaceholder = "insert number of contract";
	
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.219-12[3]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
	
		html = html.replaceFirst(Pattern.quote("__ [<i>insert name of contracting agency</i>]"), "{{textbox_man_52.219-12[4]}}");
	
		sPlaceholder = "insert name of contracting agency";
	
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.219-12[4]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
	
		html = html.replaceFirst(Pattern.quote("__ [<i>insert name of contracting agency</i>]"), "{{textbox_man_52.219-12[5]}}");
	
		sPlaceholder = "insert name of contracting agency";
	
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.219-12[5]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
	
		html = html.replaceFirst(Pattern.quote("[<i>insert name of contracting agency</i>]"), "{{textbox_man_52.219-12[6]}}");
	
		sPlaceholder = "insert name of contracting agency";
	
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.219-12[6]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
	
		html = html.replaceFirst(Pattern.quote("__ [<i>insert name of contracting agency</i>]"), "{{textbox_man_52.219-12[7]}}");
	
		sPlaceholder = "insert name of contracting agency";
	
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.219-12[7]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
	
	
		html = html.replace(_Fillin_Marker, "{{textbox}}&nbsp;");
		html = EcfrFillInUtils.processTextAndCheck(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
		html = HtmlUtils.parseFillinsAndNormalize(html);
	
		return html;
	}
	
	private static String process52_219_17 (int iStg1OfficialClauseId, String html, 
		TemplateStg2ClauseFillInRecord oTemplateStg2ClauseFillInRecord, 
		PreparedStatement psFillInInsertClause) throws SQLException {
	
		html = HtmlUtils.normalizeFillins(html);
		html = html.replaceFirst(Pattern.quote("[<i>insert name of contracting agency</i>]"), "{{textbox_man_52.219-17[0]}}");
	
		String sPlaceholder = "insert name of contracting agency";
	
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.219-17[0]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
	
		html = html.replaceFirst(Pattern.quote("____  [<i>insert name of contracting agency</i>]"), "{{textbox_man_52.219-17[1]}}");
	
		sPlaceholder = "insert name of contracting agency";
	
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.219-17[1]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
	
		html = html.replaceFirst(Pattern.quote("__ (<i>insert name of contracting activity</i>)"), "{{textbox_man_52.219-17[2]}}");

		sPlaceholder = "insert name of contracting activity";

		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.219-17[2]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
		
		html = html.replace(_Fillin_Marker, "{{textbox}}&nbsp;");
		html = EcfrFillInUtils.processTextAndCheck(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
		html = HtmlUtils.parseFillinsAndNormalize(html);
	
		return html;
	}
	
	private static String process52_219_18 (int iStg1OfficialClauseId, String html, 
		TemplateStg2ClauseFillInRecord oTemplateStg2ClauseFillInRecord, 
		PreparedStatement psFillInInsertClause) throws SQLException {
	
		html = HtmlUtils.normalizeFillins(html);
		html = html.replaceFirst(Pattern.quote("[<i>insert name of SBA's contractor</i>]"), "{{textbox_man_52.219-18[0]}}");
	
		String sPlaceholder = "insert name of SBA's contractor";
	
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.219-18[0]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
	
		html = html.replaceFirst(Pattern.quote("[<i>insert name of contracting agency</i>]"), "{{textbox_man_52.219-18[1]}}");
	
		sPlaceholder = "insert name of contracting agency";
	
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.219-18[1]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
	
	
		html = html.replace(_Fillin_Marker, "{{textbox}}&nbsp;");
		html = EcfrFillInUtils.processTextAndCheck(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
		html = HtmlUtils.parseFillinsAndNormalize(html);
	
		return html;
	}
	
	private static String process52_219_28 (int iStg1OfficialClauseId, String html, 
		TemplateStg2ClauseFillInRecord oTemplateStg2ClauseFillInRecord, 
		PreparedStatement psFillInInsertClause) throws SQLException {
	
		html = HtmlUtils.normalizeFillins(html);
		html = html.replaceFirst(Pattern.quote("____ .</p><p>[<i>Contractor to sign and date and insert authorized signer's name and title</i>]"), "<br /><br />{{textbox_man_52.219-28[0]}}");
	
		String sPlaceholder = "Contractor to sign and date and insert authorized signer's name and title";
	
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.219-28[0]", "M", 250, // piFillInMaxSize, 
			null, sPlaceholder, 2, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
	
		html = html.replace(_Fillin_Marker, "{{textbox}}&nbsp;");
		html = EcfrFillInUtils.processTextAndCheck(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
		html = HtmlUtils.parseFillinsAndNormalize(html);
	
		return html;
	}
	
	
	private static String process52_222_23 (int iStg1OfficialClauseId, String html, 
		TemplateStg2ClauseFillInRecord oTemplateStg2ClauseFillInRecord, 
		PreparedStatement psFillInInsertClause) throws SQLException {
	
		html = HtmlUtils.normalizeFillins(html);
		html = html.replaceFirst(Pattern.quote("____  [<i>Contracting Officer shall insert description of the geographical areas where the contract is to be performed, giving the State, county, and city</i>]"), "{{textbox_man_52.222-23[0]}}");
	
		String sPlaceholder = "Contracting Officer shall insert description of the geographical areas where the contract is to be performed, giving the State, county, and city";
	
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.222-23[0]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
	
	
		// Add the table.
		html = html.replace("<table border=\"1\" cellpadding=\"1\" cellspacing=\"1\" frame=\"void\" width=\"100%\"><thead><tr><th scope=\"col\">Goals for minority participation for each trade</th><th scope=\"col\">Goals for female participation for each trade</th></tr></thead><tbody><tr><td align=\"left\" scope=\"row\">____ </td><td align=\"left\">____ </td></tr><tr><td align=\"left\" scope=\"row\">[<i>Contracting Officer shall insert goals</i>]</td><td align=\"left\">[<i>Contracting Officer shall insert goals</i>]</td></tr></tbody></table>", 
				"{{tbl_52.222-23[0]}}{{tbl_52.222-23[1]}}");
		
		oTemplateStg2ClauseFillInRecord.set2(html, iStg1OfficialClauseId,
				"tbl_52.222-23[0]", // psFillInCode,
				"S", // psFillInType,
				100, // piFillInMaxSize,
				0, // piFillInGroupNumber,
				"Contracting Officer shall insert goals", // psFillInPlaceholder,
				null, // psFillInDefaultData,
				1, // piFillInDisplayRows,
				true, // pbFillInForTable,
				"Goals for minority participation for each trade" // psFillInHeading
				);
		oTemplateStg2ClauseFillInRecord.insert(psFillInInsertClause);
		
		oTemplateStg2ClauseFillInRecord.set2(html, iStg1OfficialClauseId,
				"tbl_52.222-23[1]", // psFillInCode,
				"S", // psFillInType,
				100, // piFillInMaxSize,
				0, // piFillInGroupNumber,
				null, // psFillInPlaceholder,
				null, // psFillInDefaultData,
				1, // piFillInDisplayRows,
				true, // pbFillInForTable,
				"Goals for female participation for each trade" // psFillInHeading
				);
		oTemplateStg2ClauseFillInRecord.insert(psFillInInsertClause);
		
				
		html = html.replace(_Fillin_Marker, "{{textbox}}&nbsp;");
		html = EcfrFillInUtils.processTextAndCheck(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
		html = HtmlUtils.parseFillinsAndNormalize(html);
	
		return html;
	}
	
	private static String process52_222_31 (int iStg1OfficialClauseId, String html, 
		TemplateStg2ClauseFillInRecord oTemplateStg2ClauseFillInRecord, 
		PreparedStatement psFillInInsertClause) throws SQLException {
	
		html = HtmlUtils.normalizeFillins(html);
		html = html.replaceFirst(Pattern.quote("____  [<i>Contracting Officer insert percentage rate</i>]"), "{{textbox_man_52.222-31[0]}}");
	
		String sPlaceholder = "Contracting Officer insert percentage rate";
	
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.222-31[0]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
	
		html = html.replaceFirst(Pattern.quote("____ [<i>Contracting Officer insert publication</i>]"), "{{textbox_man_52.222-31[1]}}");
	
		sPlaceholder = "Contracting Officer insert publication";
	
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.222-31[1]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
	
	
		html = html.replace(_Fillin_Marker, "{{textbox}}&nbsp;");
		html = EcfrFillInUtils.processTextAndCheck(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
		html = HtmlUtils.parseFillinsAndNormalize(html);
	
		return html;
	}
	
	private static String process52_222_36_AltI (int iStg1OfficialClauseId, String html, 
		TemplateStg2ClauseFillInRecord oTemplateStg2ClauseFillInRecord, 
		PreparedStatement psFillInInsertClause) throws SQLException {
	
		html = HtmlUtils.normalizeFillins(html);
		html = html.replaceFirst(Pattern.quote("____ [<i>List term(s)</i>]"), "{{textbox_man_52.222-36_AltI[0]}}");
	
		String sPlaceholder = "List term(s)";
	
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.222-36_AltI[0]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
	
	
		html = html.replace(_Fillin_Marker, "{{textbox}}&nbsp;");
		html = EcfrFillInUtils.processTextAndCheck(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
		html = HtmlUtils.parseFillinsAndNormalize(html);
	
		return html;
	}
		
	private static String process52_223_9 (int iStg1OfficialClauseId, String html, 
		TemplateStg2ClauseFillInRecord oTemplateStg2ClauseFillInRecord, 
		PreparedStatement psFillInInsertClause) throws SQLException {
	
		html = HtmlUtils.normalizeFillins(html);
		html = html.replaceFirst(Pattern.quote("____  [<i>Contracting Officer complete in accordance with agency procedures</i>]"), "{{textbox_man_52.223-9[0]}}");
	
		String sPlaceholder = "Contracting Officer complete in accordance with agency procedures";
	
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.223-9[0]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
	
	
		html = html.replace(_Fillin_Marker, "{{textbox}}&nbsp;");
		html = EcfrFillInUtils.processTextAndCheck(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
		html = HtmlUtils.parseFillinsAndNormalize(html);
	
		return html;
	}
	
	private static String process52_225_11 (int iStg1OfficialClauseId, String html, 
		TemplateStg2ClauseFillInRecord oTemplateStg2ClauseFillInRecord, 
		PreparedStatement psFillInInsertClause) throws SQLException {
	
		html = HtmlUtils.normalizeFillins(html);
		html = html.replaceFirst(Pattern.quote("[Contracting Officer is to list applicable excepted materials or indicate \"none\"]"), "{{textbox_man_52.225-11[0]}}");
	
		String sPlaceholder = "Contracting Officer is to list applicable excepted materials or indicate \"none\"";
	
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.225-11[0]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
	
	
		html = html.replace(_Fillin_Marker, "{{textbox}}&nbsp;");
		html = EcfrFillInUtils.processTextAndCheck(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
		html = HtmlUtils.parseFillinsAndNormalize(html);
	
		return html;
	}
	
	private static String process52_225_17 (int iStg1OfficialClauseId, String html, 
		TemplateStg2ClauseFillInRecord oTemplateStg2ClauseFillInRecord, 
		PreparedStatement psFillInInsertClause) throws SQLException {
	
		html = HtmlUtils.normalizeFillins(html);
		html = html.replaceFirst(Pattern.quote("[<i>Contracting Officer to insert source of rate</i>]"), "{{textbox_man_52.225-17[0]}}");
	
		String sPlaceholder = "Contracting Officer to insert source of rate";
	
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.225-17[0]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
	
	
		html = html.replace(_Fillin_Marker, "{{textbox}}&nbsp;");
		html = EcfrFillInUtils.processTextAndCheck(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
		html = HtmlUtils.parseFillinsAndNormalize(html);
	
		return html;
	}
	
	private static String process52_225_19 (int iStg1OfficialClauseId, String html, 
		TemplateStg2ClauseFillInRecord oTemplateStg2ClauseFillInRecord, 
		PreparedStatement psFillInInsertClause) throws SQLException {
	
		html = HtmlUtils.normalizeFillins(html);
		html = html.replaceFirst(Pattern.quote("____  [<i>Contracting Officer to specify individual, e.g., Contracting Officer Representative, Regional Security Officer, etc,</i>]"), "{{textbox_man_52.225-19[0]}}");
	
		String sPlaceholder = "Contracting Officer to specify individual, e.g., Contracting Officer Representative, Regional Security Officer, etc,";
	
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.225-19[0]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
	
	
		html = html.replace(_Fillin_Marker, "{{textbox}}&nbsp;");
		html = EcfrFillInUtils.processTextAndCheck(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
		html = HtmlUtils.parseFillinsAndNormalize(html);
	
		return html;
	}
	
	private static String process52_225_21 (int iStg1OfficialClauseId, String html, 
		TemplateStg2ClauseFillInRecord oTemplateStg2ClauseFillInRecord, 
		PreparedStatement psFillInInsertClause) throws SQLException {
	
		html = HtmlUtils.normalizeFillins(html);
		html = html.replaceFirst(Pattern.quote("____ <p><i>[Contracting Officer to list applicable excepted materials or indicate \"none\"]"), "{{textbox_man_52.225-21[0]}}");
	
		String sPlaceholder = "Contracting Officer to list applicable excepted materials or indicate \"none\"";
	
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.225-21[0]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
	
		/*
		html = html.replaceFirst(Pattern.quote("[List name, address, telephone number, and contact for suppliers surveyed. Attach copy of reponse; if oral, attach summary.]"), "{{textbox_man_52.225-21[1]}}");
	
		sPlaceholder = "List name, address, telephone number, and contact for suppliers surveyed. Attach copy of reponse; if oral, attach summary.";
	
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.225-21[1]", "M", 250, // piFillInMaxSize, 
			null, sPlaceholder, 2, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
	
		html = html.replaceFirst(Pattern.quote("[Include other applicable supporting information.]"), "{{textbox_man_52.225-21[2]}}");
	
		sPlaceholder = "Include other applicable supporting information.";
	
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.225-21[2]", "M", 250, // piFillInMaxSize, 
			null, sPlaceholder, 2, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
		
		html = html.replaceFirst(Pattern.quote("*<i>Include all delivery costs to the construction site.]"), "{{textbox_man_52.225-21[3]}}");
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
				"textbox_man_52.225-21[3]", "S", 100, // piFillInMaxSize, 
				null, "Include all delivery costs to the construction site.", 2, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
				psFillInInsertClause);
		*/
		
		html = html.replace(_Fillin_Marker, "{{textbox}}&nbsp;");
		html = EcfrFillInUtils.processTextAndCheck(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
		html = HtmlUtils.parseFillinsAndNormalize(html);
	
		return html;
	}
	
	private static String process52_226_4 (int iStg1OfficialClauseId, String html, 
		TemplateStg2ClauseFillInRecord oTemplateStg2ClauseFillInRecord, 
		PreparedStatement psFillInInsertClause) throws SQLException {
	
		html = HtmlUtils.normalizeFillins(html);
		html = html.replaceFirst(Pattern.quote("____  [<i>Contracting Officer to fill in with definite geographic boundaries.</i>]"), "{{textbox_man_52.226-4[0]}}");
	
		String sPlaceholder = "Contracting Officer to fill in with definite geographic boundaries.";
	
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.226-4[0]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
	
	
		html = html.replace(_Fillin_Marker, "{{textbox}}&nbsp;");
		html = EcfrFillInUtils.processTextAndCheck(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
		html = HtmlUtils.parseFillinsAndNormalize(html);
	
		return html;
	}
	
	private static String process52_227_11 (int iStg1OfficialClauseId, String html, 
		TemplateStg2ClauseFillInRecord oTemplateStg2ClauseFillInRecord, 
		PreparedStatement psFillInInsertClause) throws SQLException {
	
		html = HtmlUtils.normalizeFillins(html);
		html = html.replaceFirst(Pattern.quote("[<i>Complete according to agency instructions.</i>]"), "{{textbox_man_52.227-11[0]}}");
	
		String sPlaceholder = "Complete according to agency instructions.";
	
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.227-11[0]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
	
		html = html.replaceFirst(Pattern.quote("(identify the contract)"), "{{textbox_man_52.227-11[1]}}");

		sPlaceholder = "identify the contract";

		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.227-11[1]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);

		html = html.replaceFirst(Pattern.quote("(identify the agency)"), "{{textbox_man_52.227-11[2]}}");

		sPlaceholder = "identify the agency";

		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.227-11[2]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
		html = html.replace(_Fillin_Marker, "{{textbox}}&nbsp;");
		html = EcfrFillInUtils.processTextAndCheck(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
		html = HtmlUtils.parseFillinsAndNormalize(html);
	
		return html;
	}
	
	private static String process52_227_11_AltI (int iStg1OfficialClauseId, String html, 
		TemplateStg2ClauseFillInRecord oTemplateStg2ClauseFillInRecord, 
		PreparedStatement psFillInInsertClause) throws SQLException {
	
		html = HtmlUtils.normalizeFillins(html);
		html = html.replaceFirst(Pattern.quote("____ *</p><p>[<i>*Contracting Officer complete with the names of applicable existing treaties or international agreements. The above language is not intended to apply to treaties or agreements that are in effect on the date of the award but are not listed.</i>]"), "{{textbox_man_52.227-11_AltI[0]}}");
	
		String sPlaceholder = "*Contracting Officer complete with the names of applicable existing treaties or international agreements. The above language is not intended to apply to treaties or agreements that are in effect on the date of the award but are not listed.";
	
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.227-11_AltI[0]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
	
	
		html = html.replace(_Fillin_Marker, "{{textbox}}&nbsp;");
		html = EcfrFillInUtils.processTextAndCheck(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
		html = HtmlUtils.parseFillinsAndNormalize(html);
	
		return html;
	}
	
	private static String process52_227_7 (int iStg1OfficialClauseId, String html, 
		TemplateStg2ClauseFillInRecord oTemplateStg2ClauseFillInRecord, 
		PreparedStatement psFillInInsertClause) throws SQLException {
	
		html = HtmlUtils.normalizeFillins(html);
		html = html.replaceFirst(Pattern.quote("__ [<i>Contracting Officer fill in</i>]"), "{{textbox_man_52.227-7[0]}}");
	
		String sPlaceholder = "Contracting Officer fill in";
	
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.227-7[0]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
	
		html = html.replaceFirst(Pattern.quote("__ [<i>Contracting Officer fill in</i>]"), "{{textbox_man_52.227-7[1]}}");
	
		sPlaceholder = "Contracting Officer fill in";
	
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.227-7[1]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
	
	
		html = html.replace(_Fillin_Marker, "{{textbox}}&nbsp;");
		html = EcfrFillInUtils.processTextAndCheck(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
		html = HtmlUtils.parseFillinsAndNormalize(html);
	
		return html;
	}
	
	private static String process52_228_9 (int iStg1OfficialClauseId, String html, 
		TemplateStg2ClauseFillInRecord oTemplateStg2ClauseFillInRecord, 
		PreparedStatement psFillInInsertClause) throws SQLException {
	
		html = HtmlUtils.normalizeFillins(html);
		html = html.replaceFirst(Pattern.quote("____  [<i>insert name of contracting agency</i>]"), "{{textbox_man_52.228-9[0]}}");
	
		String sPlaceholder = "insert name of contracting agency";
	
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.228-9[0]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
	
		html = html.replaceFirst(Pattern.quote("____  [<i>insert name of contracting agency</i>]"), "{{textbox_man_52.228-9[1]}}");
	
		sPlaceholder = "insert name of contracting agency";
	
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.228-9[1]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
	
		html = html.replaceFirst(Pattern.quote("____  [<i>insert name of contracting agency</i>]"), "{{textbox_man_52.228-9[2]}}");
	
		sPlaceholder = "insert name of contracting agency";
	
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.228-9[2]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
	
		html = html.replaceFirst(Pattern.quote("____  [<i>insert name and address of contracting agency</i>]"), "{{textbox_man_52.228-9[3]}}");
	
		sPlaceholder = "insert name and address of contracting agency";
	
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.228-9[3]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
	
		html = html.replaceFirst(Pattern.quote("____  [<i>insert name of contracting agency</i>]"), "{{textbox_man_52.228-9[4]}}");
	
		sPlaceholder = "insert name of contracting agency";
	
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.228-9[4]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
	
	
		html = html.replace(_Fillin_Marker, "{{textbox}}&nbsp;");
		html = EcfrFillInUtils.processTextAndCheck(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
		html = HtmlUtils.parseFillinsAndNormalize(html);
	
		return html;
	}
	
	private static String process52_229_10 (int iStg1OfficialClauseId, String html, 
		TemplateStg2ClauseFillInRecord oTemplateStg2ClauseFillInRecord, 
		PreparedStatement psFillInInsertClause) throws SQLException {
	
		html = HtmlUtils.normalizeFillins(html);
		html = html.replaceFirst(Pattern.quote("*__"),"{{textbox_man_52.229-10[0]}}");
		html = html.replaceFirst(Pattern.quote("*__"),"{{textbox_man_52.229-10[1]}}");
		html = html.replaceFirst(Pattern.quote("*__"),"{{textbox_man_52.229-10[2]}}");
		html = html.replaceFirst(Pattern.quote("*__"),"{{textbox_man_52.229-10[3]}}");
		html = html.replaceFirst(Pattern.quote("(*Insert appropriate agency name in blanks.)"), "");
	
		String sPlaceholder = "Insert appropriate agency name.";
	
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.229-10[0]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.229-10[1]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.229-10[2]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.229-10[3]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
	
		html = html.replace(_Fillin_Marker, "{{textbox}}&nbsp;");
		html = EcfrFillInUtils.processTextAndCheck(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
		html = HtmlUtils.parseFillinsAndNormalize(html);
	
		return html;
	}
	
	private static String process52_229_7 (int iStg1OfficialClauseId, String html, 
		TemplateStg2ClauseFillInRecord oTemplateStg2ClauseFillInRecord, 
		PreparedStatement psFillInInsertClause) throws SQLException {
	
		html = HtmlUtils.normalizeFillins(html);
		html = html.replaceFirst(Pattern.quote("__ [<i>insert name of the foreign government</i>]"), "{{textbox_man_52.229-7[0]}}");
	
		String sPlaceholder = "insert name of the foreign government";
	
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.229-7[0]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
	
		html = html.replaceFirst(Pattern.quote("__ [<i>insert name of country</i>]"), "{{textbox_man_52.229-7[1]}}");
	
		sPlaceholder = "insert name of country";
	
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.229-7[1]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
	
		html = html.replaceFirst(Pattern.quote("__ [<i>insert name of country</i>]"), "{{textbox_man_52.229-7[2]}}");
	
		sPlaceholder = "insert name of country";
	
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.229-7[2]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
	
		html = html.replaceFirst(Pattern.quote("__ [<i>insert name of the foreign government</i>]"), "{{textbox_man_52.229-7[3]}}");
	
		sPlaceholder = "insert name of the foreign government";
	
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.229-7[3]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
	
		html = html.replaceFirst(Pattern.quote("__ [<i>insert name of country</i>]"), "{{textbox_man_52.229-7[4]}}");
	
		sPlaceholder = "insert name of country";
	
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.229-7[4]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
	
	
		html = html.replace(_Fillin_Marker, "{{textbox}}&nbsp;");
		html = EcfrFillInUtils.processTextAndCheck(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
		html = HtmlUtils.parseFillinsAndNormalize(html);
	
		return html;
	}
	
	private static String process52_229_8 (int iStg1OfficialClauseId, String html, 
		TemplateStg2ClauseFillInRecord oTemplateStg2ClauseFillInRecord, 
		PreparedStatement psFillInInsertClause) throws SQLException {
	
		html = HtmlUtils.normalizeFillins(html);
		html = html.replaceFirst(Pattern.quote("__ [<i>insert name of the foreign government</i>]"), "{{textbox_man_52.229-8[0]}}");
	
		String sPlaceholder = "insert name of the foreign government";
	
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.229-8[0]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
	
		html = html.replaceFirst(Pattern.quote("__ [<i>insert name of country</i>]"), "{{textbox_man_52.229-8[1]}}");
	
		sPlaceholder = "insert name of country";
	
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.229-8[1]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
	
	
		html = html.replace(_Fillin_Marker, "{{textbox}}&nbsp;");
		html = EcfrFillInUtils.processTextAndCheck(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
		html = HtmlUtils.parseFillinsAndNormalize(html);
	
		return html;
	}
	
	private static String process52_229_9 (int iStg1OfficialClauseId, String html, 
		TemplateStg2ClauseFillInRecord oTemplateStg2ClauseFillInRecord, 
		PreparedStatement psFillInInsertClause) throws SQLException {
	
		html = HtmlUtils.normalizeFillins(html);
		html = html.replaceFirst(Pattern.quote("__ [<i>insert name of the foreign government</i>]"), "{{textbox_man_52.229-9[0]}}");
	
		String sPlaceholder = "insert name of the foreign government";
	
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.229-9[0]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
	
		html = html.replaceFirst(Pattern.quote("__ [<i>insert name of country</i>]"), "{{textbox_man_52.229-9[1]}}");
	
		sPlaceholder = "insert name of country";
	
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.229-9[1]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
	
	
		html = html.replace(_Fillin_Marker, "{{textbox}}&nbsp;");
		html = EcfrFillInUtils.processTextAndCheck(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
		html = HtmlUtils.parseFillinsAndNormalize(html);
	
		return html;
	}

	
	private static String process52_232_12 (int iStg1OfficialClauseId, String html, 
		TemplateStg2ClauseFillInRecord oTemplateStg2ClauseFillInRecord, 
		PreparedStatement psFillInInsertClause) throws SQLException {
	
		html = HtmlUtils.normalizeFillins(html);
		html = html.replaceFirst(Pattern.quote("____  [<i>Insert the name of the office designated under agency procedures</i>]"), "{{textbox_man_52.232-12[0]}}");
	
		String sPlaceholder = "Insert the name of the office designated under agency procedures";
	
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.232-12[0]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
	
		html = html.replaceFirst(Pattern.quote("____  [<i>insert the name of the financial institution</i>]"), "{{textbox_man_52.232-12[1]}}");
	
		sPlaceholder = "insert the name of the financial institution";
	
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.232-12[1]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
	
		html = html.replaceFirst(Pattern.quote("__ [<i>Insert an amount not higher than 10 percent of the stated contract amount inserted in this paragraph</i>]"), "{{textbox_man_52.232-12[2]}}");
	
		sPlaceholder = "Insert an amount not higher than 10 percent of the stated contract amount inserted in this paragraph";
	
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.232-12[2]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
	
	
		html = html.replace(_Fillin_Marker, "{{textbox}}&nbsp;");
		html = EcfrFillInUtils.processTextAndCheck(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
		html = HtmlUtils.parseFillinsAndNormalize(html);
	
		return html;
	}
	
	private static String process52_232_16 (int iStg1OfficialClauseId, String html, 
		TemplateStg2ClauseFillInRecord oTemplateStg2ClauseFillInRecord, 
		PreparedStatement psFillInInsertClause) throws SQLException {
	
		html = HtmlUtils.normalizeFillins(html);
		html = html.replaceFirst(Pattern.quote("____  [<i>Contracting Officer insert date as prescribed by agency head; if not prescribed, insert \"30th\"</i>]"), "{{textbox_man_52.232-16[0]}}");
	
		String sPlaceholder = "Contracting Officer insert date as prescribed by agency head; if not prescribed, insert \"30th\"";
	
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.232-16[0]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
	
	
		html = html.replace(_Fillin_Marker, "{{textbox}}&nbsp;");
		html = EcfrFillInUtils.processTextAndCheck(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
		html = HtmlUtils.parseFillinsAndNormalize(html);
	
		return html;
	}
	
	private static String process52_232_16_AltII (int iStg1OfficialClauseId, String html, 
		TemplateStg2ClauseFillInRecord oTemplateStg2ClauseFillInRecord, 
		PreparedStatement psFillInInsertClause) throws SQLException {
	
		html = HtmlUtils.normalizeFillins(html);
		html = html.replaceFirst(Pattern.quote("____  [<i>Contracting Officer specify dollar amount</i>]"), "{{textbox_man_52.232-16_AltII[0]}}");
	
		String sPlaceholder = "Contracting Officer specify dollar amount";
	
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.232-16_AltII[0]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
	
	
		html = html.replace(_Fillin_Marker, "{{textbox}}&nbsp;");
		html = EcfrFillInUtils.processTextAndCheck(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
		html = HtmlUtils.parseFillinsAndNormalize(html);
	
		return html;
	}
	
	private static String process52_232_32 (int iStg1OfficialClauseId, String html, 
		TemplateStg2ClauseFillInRecord oTemplateStg2ClauseFillInRecord, 
		PreparedStatement psFillInInsertClause) throws SQLException {
	
		html = HtmlUtils.normalizeFillins(html);
		html = html.replaceFirst(Pattern.quote("____  [<i>Contracting Officer insert day as prescribed by agency head; if not prescribed, insert \"30th\"</i>]"), "{{textbox_man_52.232-32[0]}}");
	
		String sPlaceholder = "Contracting Officer insert day as prescribed by agency head; if not prescribed, insert \"30th\"";
	
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.232-32[0]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
	
	
		html = html.replace(_Fillin_Marker, "{{textbox}}&nbsp;");
		html = EcfrFillInUtils.processTextAndCheck(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
		html = HtmlUtils.parseFillinsAndNormalize(html);
	
		return html;
	}
	
	private static String process52_232_7 (int iStg1OfficialClauseId, String html, 
		TemplateStg2ClauseFillInRecord oTemplateStg2ClauseFillInRecord, 
		PreparedStatement psFillInInsertClause) throws SQLException {
	
		html = HtmlUtils.normalizeFillins(html);
		html = html.replaceFirst(Pattern.quote("____  [<i>Contracting Officer insert day as prescribed by agency head; if not prescribed, insert \"30th\"</i>]"), "{{textbox_man_52.232-7[0]}}");
	
		String sPlaceholder = "Contracting Officer insert day as prescribed by agency head; if not prescribed, \"30th\"";
	
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.232-7[0]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
	
	
		html = html.replace(_Fillin_Marker, "{{textbox}}&nbsp;");
		html = EcfrFillInUtils.processTextAndCheck(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
		html = HtmlUtils.parseFillinsAndNormalize(html);
	
		return html;
	}
	
	private static String process52_232_34 (int iStg1OfficialClauseId, String html, 
		TemplateStg2ClauseFillInRecord oTemplateStg2ClauseFillInRecord, 
		PreparedStatement psFillInInsertClause) throws SQLException {
	
		html = HtmlUtils.normalizeFillins(html);
		html = html.replaceFirst(Pattern.quote("____  [<i>the Contracting Officer shall insert date, days after award, days before first request, the date specified for receipt of offers if the provision at 52.232-38 is utilized, or \"concurrent with first request\" as prescribed by the head of the agency; if not prescribed, insert \"no later than 15 days prior to submission of the first request for payment\"</i>]"), "{{textbox_man_52.232-34[0]}}");
	
		String sPlaceholder = "the Contracting Officer shall insert date, days after award, days before first request, the date specified for receipt of offers if the provision at 52.232-38 is utilized, or \"concurrent with first request\" as prescribed by the head of the agency; if not prescribed, insert \"no later than 15 days prior to submission of the first request for payment\"";
	
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.232-34[0]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
	
	
		html = html.replace(_Fillin_Marker, "{{textbox}}&nbsp;");
		html = EcfrFillInUtils.processTextAndCheck(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
		html = HtmlUtils.parseFillinsAndNormalize(html);
	
		return html;
	}
	
	private static String process52_233_2 (int iStg1OfficialClauseId, String html, 
		TemplateStg2ClauseFillInRecord oTemplateStg2ClauseFillInRecord, 
		PreparedStatement psFillInInsertClause) throws SQLException {
	
		html = HtmlUtils.normalizeFillins(html);
		html = html.replaceFirst(Pattern.quote("____ . [<i>Contracting Officer designate the official or location where a protest may be served on the Contracting Officer.</i>]"), "{{textbox_man_52.233-2[0]}}");
	
		String sPlaceholder = "Contracting Officer designate the official or location where a protest may be served on the Contracting Officer.";
	
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.233-2[0]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
	
	
		html = html.replace(_Fillin_Marker, "{{textbox}}&nbsp;");
		html = EcfrFillInUtils.processTextAndCheck(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
		html = HtmlUtils.parseFillinsAndNormalize(html);
	
		return html;
	}
	
	private static String process52_234_4 (int iStg1OfficialClauseId, String html, 
		TemplateStg2ClauseFillInRecord oTemplateStg2ClauseFillInRecord, 
		PreparedStatement psFillInInsertClause) throws SQLException {
	
		html = HtmlUtils.normalizeFillins(html);
		html = html.replaceFirst(Pattern.quote("[<i>Insert list of applicable subcontractors.</i>]"), "{{textbox_man_52.234-4[0]}}");
	
		String sPlaceholder = "Insert list of applicable subcontractors.";
	
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.234-4[0]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
	
	
		html = html.replace(_Fillin_Marker, "{{textbox}}&nbsp;");
		html = EcfrFillInUtils.processTextAndCheck(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
		html = HtmlUtils.parseFillinsAndNormalize(html);
	
		return html;
	}
	
	private static String process52_236_1 (int iStg1OfficialClauseId, String html, 
		TemplateStg2ClauseFillInRecord oTemplateStg2ClauseFillInRecord, 
		PreparedStatement psFillInInsertClause) throws SQLException {
	
		html = HtmlUtils.normalizeFillins(html);
		html = html.replaceFirst(Pattern.quote("____  [<i>insert the appropriate number in words followed by numerals in parentheses</i>]"), "{{textbox_man_52.236-1[0]}}");
	
		String sPlaceholder = "insert the appropriate number in words followed by numerals in parentheses";
	
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.236-1[0]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
	
	
		html = html.replace(_Fillin_Marker, "{{textbox}}&nbsp;");
		html = EcfrFillInUtils.processTextAndCheck(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
		html = HtmlUtils.parseFillinsAndNormalize(html);
	
		return html;
	}
	
	private static String process52_236_4 (int iStg1OfficialClauseId, String html, 
		TemplateStg2ClauseFillInRecord oTemplateStg2ClauseFillInRecord, 
		PreparedStatement psFillInInsertClause) throws SQLException {
	
		html = HtmlUtils.normalizeFillins(html);
		html = html.replaceFirst(Pattern.quote("____  [<i>insert a description of investigational methods used, such as surveys, auger borings, core borings, test pits, probings, test tunnels</i>]"), "{{textbox_man_52.236-4[0]}}");
	
		String sPlaceholder = "insert a description of investigational methods used, such as surveys, auger borings, core borings, test pits, probings, test tunnels";
	
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.236-4[0]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
	
		html = html.replaceFirst(Pattern.quote("____  [<i>insert a summary of weather records and warnings</i>]"), "{{textbox_man_52.236-4[1]}}");
	
		sPlaceholder = "insert a summary of weather records and warnings";
	
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.236-4[1]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
	
		html = html.replaceFirst(Pattern.quote("____  [<i>insert a summary of transportation facilities providing access from the site, including information about their availability and limitations</i>]"), "{{textbox_man_52.236-4[2]}}");
	
		sPlaceholder = "insert a summary of transportation facilities providing access from the site, including information about their availability and limitations";
	
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.236-4[2]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
	
		html = html.replaceFirst(Pattern.quote("____  [<i>Insert other pertinent information</i>]"), "{{textbox_man_52.236-4[3]}}");
	
		sPlaceholder = "Insert other pertinent information";
	
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.236-4[3]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
	
	
		html = html.replace(_Fillin_Marker, "{{textbox}}&nbsp;");
		html = EcfrFillInUtils.processTextAndCheck(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
		html = HtmlUtils.parseFillinsAndNormalize(html);
	
		return html;
	}
	
	private static String process52_236_21_AltII (int iStg1OfficialClauseId, String html, 
		TemplateStg2ClauseFillInRecord oTemplateStg2ClauseFillInRecord, 
		PreparedStatement psFillInInsertClause) throws SQLException {
	
		html = HtmlUtils.normalizeFillins(html);
		html = html.replaceFirst(Pattern.quote("____  [<i>Contracting Officer complete by inserting desired amount</i>]"), "{{textbox_man_52.236-21_AltII[0]}}");
	
		String sPlaceholder = "Contracting Officer complete by inserting desired amount";
	
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.236-21_AltII[0]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
	
	
		html = html.replace(_Fillin_Marker, "{{textbox}}&nbsp;");
		html = EcfrFillInUtils.processTextAndCheck(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
		html = HtmlUtils.parseFillinsAndNormalize(html);
	
		return html;
	}
	
	private static String process52_236_27_AltI (int iStg1OfficialClauseId, String html, 
		TemplateStg2ClauseFillInRecord oTemplateStg2ClauseFillInRecord, 
		PreparedStatement psFillInInsertClause) throws SQLException {
	
		html = HtmlUtils.normalizeFillins(html);
		html = html.replaceFirst(Pattern.quote("____ <p>[<i>Insert date and time</i>]"), "{{textbox_man_52.236-27_AltI[0]}}");
	
		String sPlaceholder = "Insert date and time";
	
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.236-27_AltI[0]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
	
		html = html.replaceFirst(Pattern.quote("____ <p>[<i>Insert location</i>]"), "{{textbox_man_52.236-27_AltI[1]}}");
	
		sPlaceholder = "Insert location";
	
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.236-27_AltI[1]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
	
	
		html = html.replace(_Fillin_Marker, "{{textbox}}&nbsp;");
		html = EcfrFillInUtils.processTextAndCheck(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
		html = HtmlUtils.parseFillinsAndNormalize(html);
	
		return html;
	}
	
	private static String process52_237_4 (int iStg1OfficialClauseId, String html, 
		TemplateStg2ClauseFillInRecord oTemplateStg2ClauseFillInRecord, 
		PreparedStatement psFillInInsertClause) throws SQLException {
	
		html = HtmlUtils.normalizeFillins(html);
		html = html.replaceFirst(Pattern.quote("__ [<i>insert</i> full <i>if Alternate I is used; otherwise insert</i> partial]"), "{{textbox_man_52.237-4[0]}}");
	
		String sPlaceholder = "insert full if Alternate I is used; otherwise insert partial";
	
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.237-4[0]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
	
		html = html.replaceFirst(Pattern.quote("__ [<i>fill in amount</i>]"), "{{textbox_man_52.237-4[1]}}");
	
		sPlaceholder = "fill in amount";
	
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.237-4[1]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
	
	
		html = html.replace(_Fillin_Marker, "{{textbox}}&nbsp;");
		html = EcfrFillInUtils.processTextAndCheck(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
		html = HtmlUtils.parseFillinsAndNormalize(html);
	
		return html;
	}
	
	private static String process52_237_5 (int iStg1OfficialClauseId, String html, 
		TemplateStg2ClauseFillInRecord oTemplateStg2ClauseFillInRecord, 
		PreparedStatement psFillInInsertClause) throws SQLException {
	
		html = HtmlUtils.normalizeFillins(html);
		html = html.replaceFirst(Pattern.quote("____  [<i>fill in amount</i>]"), "{{textbox_man_52.237-5[0]}}");
	
		String sPlaceholder = "fill in amount";
	
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.237-5[0]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
	
	
		html = html.replace(_Fillin_Marker, "{{textbox}}&nbsp;");
		html = EcfrFillInUtils.processTextAndCheck(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
		html = HtmlUtils.parseFillinsAndNormalize(html);
	
		return html;
	}
	
	private static String process52_237_6 (int iStg1OfficialClauseId, String html, 
		TemplateStg2ClauseFillInRecord oTemplateStg2ClauseFillInRecord, 
		PreparedStatement psFillInInsertClause) throws SQLException {
	
		html = HtmlUtils.normalizeFillins(html);
		html = html.replaceFirst(Pattern.quote("__ [<i>fill in amount</i>]"), "{{textbox_man_52.237-6[0]}}");
	
		String sPlaceholder = "fill in amount";
	
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.237-6[0]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
	
	
		html = html.replace(_Fillin_Marker, "{{textbox}}&nbsp;");
		html = EcfrFillInUtils.processTextAndCheck(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
		html = HtmlUtils.parseFillinsAndNormalize(html);
	
		return html;
	}
	
	private static String process52_241_3 (int iStg1OfficialClauseId, String html, 
		TemplateStg2ClauseFillInRecord oTemplateStg2ClauseFillInRecord, 
		PreparedStatement psFillInInsertClause) throws SQLException {
	
		html = HtmlUtils.normalizeFillins(html);
		html = html.replaceFirst(Pattern.quote("____ , [<i>insert period of service</i>]"), "{{textbox_man_52.241-3[0]}}");
	
		String sPlaceholder = "insert period of service";
	
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.241-3[0]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
	
		html = html.replaceFirst(Pattern.quote("____  [<i>insert type of service</i>]"), "{{textbox_man_52.241-3[1]}}");
	
		sPlaceholder = "insert type of service";
	
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.241-3[1]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
	
	
		html = html.replace(_Fillin_Marker, "{{textbox}}&nbsp;");
		html = EcfrFillInUtils.processTextAndCheck(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
		html = HtmlUtils.parseFillinsAndNormalize(html);
	
		return html;
	}
	
	private static String process52_241_7 (int iStg1OfficialClauseId, String html, 
		TemplateStg2ClauseFillInRecord oTemplateStg2ClauseFillInRecord, 
		PreparedStatement psFillInInsertClause) throws SQLException {
	
		html = HtmlUtils.normalizeFillins(html);
		html = html.replaceFirst(Pattern.quote("____  written notice"), "{{textbox_man_52.241-7[0]}} written notice");
	
		String sPlaceholder = "Insert language prescribed in 41.501(d)(1)";
	
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.241-7[0]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
	
	
		html = html.replace(_Fillin_Marker, "{{textbox}}&nbsp;");
		html = EcfrFillInUtils.processTextAndCheck(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
		html = HtmlUtils.parseFillinsAndNormalize(html);
	
		return html;
	}
	
	private static String process52_241_8 (int iStg1OfficialClauseId, String html, 
		TemplateStg2ClauseFillInRecord oTemplateStg2ClauseFillInRecord, 
		PreparedStatement psFillInInsertClause) throws SQLException {
	
		html = HtmlUtils.normalizeFillins(html);
		html = html.replaceFirst(Pattern.quote("____  [<i>insert date</i>]"), "{{textbox_man_52.241-8[0]}}");
	
		String sPlaceholder = "insert date";
	
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.241-8[0]", "S", 20, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
	
	
		html = html.replace(_Fillin_Marker, "{{textbox}}&nbsp;");
		html = EcfrFillInUtils.processTextAndCheck(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
		html = HtmlUtils.parseFillinsAndNormalize(html);
	
		return html;
	}
	
	private static String process52_241_10 (int iStg1OfficialClauseId, String html, 
		TemplateStg2ClauseFillInRecord oTemplateStg2ClauseFillInRecord, 
		PreparedStatement psFillInInsertClause) throws SQLException {
	
		html = HtmlUtils.normalizeFillins(html);
		html = html.replaceFirst(Pattern.quote("[<i>Insert negotiated duration.</i>]"), "{{textbox_man_52.241-10[0]}}");
	
		String sPlaceholder = "Insert negotiated duration.";
	
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.241-10[0]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
	
		html = html.replaceFirst(Pattern.quote("____ . [<i>Insert appropriate dollar amount.</i>]"), "{{textbox_man_52.241-10[1]}}");
	
		sPlaceholder = "Insert appropriate dollar amount.";
	
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.241-10[1]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
	
		html = html.replaceFirst(Pattern.quote("____ . [<i>Divide the net facility cost in paragraph (c) of this clause by the facility's cost recovery period in paragraph (b) of this clause and insert the resultant figure.</i>]"), "{{textbox_man_52.241-10[2]}}");
	
		sPlaceholder = "Divide the net facility cost in paragraph (c) of this clause by the facility's cost recovery period in paragraph (b) of this clause and insert the resultant figure.";
	
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.241-10[2]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
	
		html = html.replaceFirst(Pattern.quote("[<i>Multiply the remaining months of the facility's cost recovery period specified in paragraph (b) of this clause by the monthly facility cost recovery rate in paragraph (d) of this clause and insert the resultant figure.</i>]"), "{{textbox_man_52.241-10[3]}}");
	
		sPlaceholder = "Multiply the remaining months of the facility's cost recovery period specified in paragraph (b) of this clause by the monthly facility cost recovery rate in paragraph (d) of this clause and insert the resultant figure.";
	
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.241-10[3]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
	
	
		html = html.replace(_Fillin_Marker, "{{textbox}}&nbsp;");
		html = EcfrFillInUtils.processTextAndCheck(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
		html = HtmlUtils.parseFillinsAndNormalize(html);
	
		return html;
	}
	
	private static String process52_241_12 (int iStg1OfficialClauseId, String html, 
		TemplateStg2ClauseFillInRecord oTemplateStg2ClauseFillInRecord, 
		PreparedStatement psFillInInsertClause) throws SQLException {
	
		html = HtmlUtils.normalizeFillins(html);
		html = html.replaceFirst(Pattern.quote("[<i>specify dates or schedules</i>]"), "{{textbox_man_52.241-12[0]}}");
	
		String sPlaceholder = "specify dates or schedules";
	
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.241-12[0]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
	
	
		html = html.replace(_Fillin_Marker, "{{textbox}}&nbsp;");
		html = EcfrFillInUtils.processTextAndCheck(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
		html = HtmlUtils.parseFillinsAndNormalize(html);
	
		return html;
	}
	
	private static String process52_241_13 (int iStg1OfficialClauseId, String html, 
		TemplateStg2ClauseFillInRecord oTemplateStg2ClauseFillInRecord, 
		PreparedStatement psFillInInsertClause) throws SQLException {
	
		html = HtmlUtils.normalizeFillins(html);
		html = html.replaceFirst(Pattern.quote("____  [<i>insert cooperative name</i>]"), "{{textbox_man_52.241-13[5]}}");
	
		String sPlaceholder = "insert cooperative name";
	
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.241-13[5]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
	
		html = HtmlUtils.normalizeFillins(html);
		html = html.replaceFirst(Pattern.quote("____  basis [<i>insert period of time</i>]"), "{{textbox_man_52.241-13[0]}}");

		sPlaceholder = "insert period of time";

		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.241-13[0]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);

		html = html.replaceFirst(Pattern.quote("____  [<i>insert agency name</i>]"), "{{textbox_man_52.241-13[1]}}");

		sPlaceholder = "insert agency name";

		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.241-13[1]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);

		html = html.replaceFirst(Pattern.quote("____  [<i>insert agency address</i>]"), "{{textbox_man_52.241-13[2]}}");

		sPlaceholder = "insert agency address";

		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.241-13[2]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);

		html = html.replace(_Fillin_Marker, "{{textbox}}&nbsp;");
		html = EcfrFillInUtils.processTextAndCheck(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
		html = HtmlUtils.parseFillinsAndNormalize(html);
	
		return html;
	}

	
	private static String process52_246_11 (int iStg1OfficialClauseId, String html, 
		TemplateStg2ClauseFillInRecord oTemplateStg2ClauseFillInRecord, 
		PreparedStatement psFillInInsertClause) throws SQLException {
	
		html = HtmlUtils.normalizeFillins(html);
		html = html.replaceFirst(Pattern.quote("[<i>Contracting Officer insert the title, number, date, and tailoring (if any) of the higher-level quality standards.</i>]"), 
				"{{tbl_52.246-11[0]}}{{tbl_52.246-11[1]}}{{tbl_52.246-11[2]}}{{tbl_52.246-11[3]}}");
	
		String sPlaceholder = "Contracting Officer insert the title, number, date, and tailoring (if any) of the higher-level quality standards.";
		
		oTemplateStg2ClauseFillInRecord.set2(html, iStg1OfficialClauseId,
				"tbl_52.246-11[0]", "S", 100, // piFillInMaxSize,
				0, sPlaceholder, null,  1, // piFillInGroupNumber, psFillInPlaceholder, psFillInDefaultData, piFillInDisplayRows
				true, "Title"); // pbFillInForTable, psFillInHeading
		oTemplateStg2ClauseFillInRecord.insert(psFillInInsertClause);
		
		oTemplateStg2ClauseFillInRecord.set2(html, iStg1OfficialClauseId,
				"tbl_52.246-11[1]", "S", 100, // piFillInMaxSize,
				0, null, null,  1, // piFillInGroupNumber, psFillInPlaceholder, psFillInDefaultData, piFillInDisplayRows
				true, "Number"); // pbFillInForTable, psFillInHeading
		oTemplateStg2ClauseFillInRecord.insert(psFillInInsertClause);
		
		oTemplateStg2ClauseFillInRecord.set2(html, iStg1OfficialClauseId,
				"tbl_52.246-11[2]", "S", 20, // piFillInMaxSize,
				0, null, null,  1, // piFillInGroupNumber, psFillInPlaceholder, psFillInDefaultData, piFillInDisplayRows
				true, "Date"); // pbFillInForTable, psFillInHeading
		oTemplateStg2ClauseFillInRecord.insert(psFillInInsertClause);
		
		oTemplateStg2ClauseFillInRecord.set2(html, iStg1OfficialClauseId,
				"tbl_52.246-11[3]", "S", 100, // piFillInMaxSize,
				0, null, null,  1, // piFillInGroupNumber, psFillInPlaceholder, psFillInDefaultData, piFillInDisplayRows
				true, "Tailoring"); // pbFillInForTable, psFillInHeading
		oTemplateStg2ClauseFillInRecord.insert(psFillInInsertClause);
	
		html = html.replace(_Fillin_Marker, "{{textbox}}&nbsp;");
		html = EcfrFillInUtils.processTextAndCheck(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
		html = HtmlUtils.parseFillinsAndNormalize(html);
	
		return html;
	}
	
	private static String process52_246_17 (int iStg1OfficialClauseId, String html, 
		TemplateStg2ClauseFillInRecord oTemplateStg2ClauseFillInRecord, 
		PreparedStatement psFillInInsertClause) throws SQLException {
	
		html = HtmlUtils.normalizeFillins(html);
		html = html.replaceFirst(Pattern.quote("__ [<i>Contracting Officer shall state specific period of time after delivery, or the specified event whose occurrence will terminate the warranty period; e.g., the number of miles or hours of use, or combinations of any applicable events or periods of time</i>]"), "{{textbox_man_52.246-17[0]}}");
	
		String sPlaceholder = "Contracting Officer shall state specific period of time after delivery, or the specified event whose occurrence will terminate the warranty period; e.g., the number of miles or hours of use, or combinations of any applicable events or periods of time";
	
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.246-17[0]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
	
		html = html.replaceFirst(Pattern.quote("__ [<i>Contracting Officer shall insert specific period of time; e.g., \"45 days of the last delivery under this contract,\" or \"45 days after discovery of the defect\"</i>]"), "{{textbox_man_52.246-17[1]}}");
	
		sPlaceholder = "Contracting Officer shall insert specific period of time; e.g., \"45 days of the last delivery under this contract,\" or \"45 days after discovery of the defect\"";
	
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.246-17[1]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
	
	
		html = html.replace(_Fillin_Marker, "{{textbox}}&nbsp;");
		html = EcfrFillInUtils.processTextAndCheck(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
		html = HtmlUtils.parseFillinsAndNormalize(html);
	
		return html;
	}
	
	private static String process52_246_18 (int iStg1OfficialClauseId, String html, 
		TemplateStg2ClauseFillInRecord oTemplateStg2ClauseFillInRecord, 
		PreparedStatement psFillInInsertClause) throws SQLException {
	
		html = HtmlUtils.normalizeFillins(html);
		html = html.replaceFirst(Pattern.quote("__ [<i>Contracting Officer shall state the specific warranty period after delivery, or the specified event whose occurrence will terminate the warranty period; e.g., the number of miles or hours of use, or combinations of any applicable events or periods of time</i>]"), "{{textbox_man_52.246-18[0]}}");
	
		String sPlaceholder = "Contracting Officer shall state the specific warranty period after delivery, or the specified event whose occurrence will terminate the warranty period; e.g., the number of miles or hours of use, or combinations of any applicable events or periods of time";
	
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.246-18[0]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
	
		html = html.replaceFirst(Pattern.quote("__ [<i>Contracting Officer shall insert specific period of time in which notice shall be given to the Contractor; e.g., 45 days after delivery of the nonconforming supplies.</i>; <i>45 days of the last delivery under this contract.</i>; or <i>45 days after discovery of the defect.</i>]"), "{{textbox_man_52.246-18[1]}}");
	
		sPlaceholder = "Contracting Officer shall insert specific period of time in which notice shall be given to the Contractor; e.g., 45 days after delivery of the nonconforming supplies.; 45 days of the last delivery under this contract.; or 45 days after discovery of the defect.";
	
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.246-18[1]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
	
		html = html.replaceFirst(Pattern.quote("__ [<i>Contracting Officer shall insert period of time</i>]"), "{{textbox_man_52.246-18[2]}}");
	
		sPlaceholder = "Contracting Officer shall insert period of time";
	
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.246-18[2]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
	
		html = html.replaceFirst(Pattern.quote("__ [<i>Contracting Officer shall insert period within which the warranty remedies should be exercised</i>]"), "{{textbox_man_52.246-18[3]}}");
	
		sPlaceholder = "Contracting Officer shall insert period within which the warranty remedies should be exercised";
	
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.246-18[3]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
	
		html = html.replaceFirst(Pattern.quote("__ [<i>Contracting Officer shall insert period within which the Contractor must be notified of a breach as to corrected or replaced supplies</i>]"), "{{textbox_man_52.246-18[4]}}");
				
		sPlaceholder = "Contracting Officer shall insert period within which the Contractor must be notified of a breach as to corrected or replaced supplies";
		
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.246-18[4]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
		
		html = html.replaceFirst(Pattern.quote("__ [<i>Contracting Officer shall insert period within which the Contractor must be notified of a breach of warranty as to corrected or replaced supplies</i>]"), "{{textbox_man_52.246-18[5]}}");
		
		sPlaceholder = "Contracting Officer shall insert period within which the Contractor must be notified of a breach of warranty as to corrected or replaced supplies";
		
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.246-18[5]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
		
		html = html.replace(_Fillin_Marker, "{{textbox}}&nbsp;");
		html = EcfrFillInUtils.processTextAndCheck(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
		html = HtmlUtils.parseFillinsAndNormalize(html);
	
		return html;
	}
	
	private static String process52_246_19 (int iStg1OfficialClauseId, String html, 
		TemplateStg2ClauseFillInRecord oTemplateStg2ClauseFillInRecord, 
		PreparedStatement psFillInInsertClause) throws SQLException {
	
		html = HtmlUtils.normalizeFillins(html);
		html = html.replaceFirst(Pattern.quote("__ [<i>Contracting Officer shall state the warranty period; e.g., at the time of delivery; within 45 days after delivery,</i> or the specified event whose occurrence will terminate the warranty period; e.g., the number of miles or hours of use, or combination of any applicable events or periods of time.]"), "{{textbox_man_52.246-19[0]}}");
	
		String sPlaceholder = "Contracting Officer shall state the warranty period; e.g., at the time of delivery; within 45 days after delivery, or the specified event whose occurrence will terminate the warranty period; e.g., the number of miles or hours of use, or combination of any applicable events or periods of time.";
	
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.246-19[0]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
	
		html = html.replaceFirst(Pattern.quote("__ [<i>Contracting Officer shall insert the specific period of time in which notice shall be given to the Contractor; e.g., 30 days after delivery of the nonconforming supplies; 90 days of the last delivery under this contract;</i> or <i>90 days after discovery of the defect.</i>]"), "{{textbox_man_52.246-19[1]}}");
	
		sPlaceholder = "Contracting Officer shall insert the specific period of time in which notice shall be given to the Contractor; e.g., 30 days after delivery of the nonconforming supplies; 90 days of the last delivery under this contract; or 90 days after discovery of the defect.";
	
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.246-19[1]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
	
		html = html.replaceFirst(Pattern.quote("__ [<i>Contracting Officer shall insert period of time</i>]"), "{{textbox_man_52.246-19[2]}}");
	
		sPlaceholder = "Contracting Officer shall insert period of time";
	
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.246-19[2]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
	
		html = html.replaceFirst(Pattern.quote("__ [<i>Contracting Officer shall insert period of time</i>]"), "{{textbox_man_52.246-19[3]}}");
	
		sPlaceholder = "Contracting Officer shall insert period of time";
	
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.246-19[3]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
	
		html = html.replaceFirst(Pattern.quote("__ [<i>Contracting Officer shall insert period of time</i>]"), "{{textbox_man_52.246-19[4]}}");
	
		sPlaceholder = "Contracting Officer shall insert period of time";
	
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.246-19[4]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
	
		html = html.replaceFirst(Pattern.quote("__ [<i>Contracting Officer shall insert locations where corrections may be performed</i>]."), "{{textbox_man_52.246-19[5]}}");
	
		sPlaceholder = "Contracting Officer shall insert locations where corrections may be performed]";
	
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.246-19[5]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
	
	
		html = html.replace(_Fillin_Marker, "{{textbox}}&nbsp;");
		html = EcfrFillInUtils.processTextAndCheck(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
		html = HtmlUtils.parseFillinsAndNormalize(html);
	
		return html;
	}

	
	private static String process52_246_20 (int iStg1OfficialClauseId, String html, 
		TemplateStg2ClauseFillInRecord oTemplateStg2ClauseFillInRecord, 
		PreparedStatement psFillInInsertClause) throws SQLException {
	
		html = HtmlUtils.normalizeFillins(html);
		html = html.replaceFirst(Pattern.quote("__ [<i>Contracting Officer shall insert the specific period of time in which notice shall be given to the Contractor; e.g., within 30 days from the date of acceptance by the Government,</i>; within 1000 hours of use by the Government,; or other specified event whose occurrence will terminate the period of notice, or combination of any applicable events or period of time]"), "{{textbox_man_52.246-20[0]}}");
	
		String sPlaceholder = "Contracting Officer shall insert the specific period of time in which notice shall be given to the Contractor; e.g., within 30 days from the date of acceptance by the Government,; within 1000 hours of use by the Government,; or other specified event whose occurrence will terminate the period of notice, or combination of any applicable events or period of time";
	
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.246-20[0]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
	
		html = html.replace(_Fillin_Marker, "{{textbox}}&nbsp;");
		html = EcfrFillInUtils.processTextAndCheck(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
		html = HtmlUtils.parseFillinsAndNormalize(html);
	
		return html;
	}
	
	private static String process52_247_51 (int iStg1OfficialClauseId, String html, 
		TemplateStg2ClauseFillInRecord oTemplateStg2ClauseFillInRecord, 
		PreparedStatement psFillInInsertClause) throws SQLException {
	
		html = HtmlUtils.normalizeFillins(html);
		html = html.replaceFirst(Pattern.quote("____ <p>[<i>Offerors insert at least one of the ports listed in paragraph (d) below.</i>]"), "{{textbox_man_52.247-51[0]}}");
	
		String sPlaceholder = "Offerors insert at least one of the ports listed in paragraph (d) below.";
	
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.247-51[0]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
	
	
		html = html.replace(_Fillin_Marker, "{{textbox}}&nbsp;");
		html = EcfrFillInUtils.processTextAndCheck(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
		html = HtmlUtils.parseFillinsAndNormalize(html);
	
		return html;
	}
	
	private static String process52_247_51_AltIII (int iStg1OfficialClauseId, String html, 
		TemplateStg2ClauseFillInRecord oTemplateStg2ClauseFillInRecord, 
		PreparedStatement psFillInInsertClause) throws SQLException {
	
		html = HtmlUtils.normalizeFillins(html);
		html = html.replaceFirst(Pattern.quote("____ <p>[<i>Offerors insert at least one of the ports listed in paragraph (d) below.</i>]"), "{{textbox_man_52.247-51_AltIII[0]}}");
	
		String sPlaceholder = "Offerors insert at least one of the ports listed in paragraph (d) below.";
	
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.247-51_AltIII[0]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
	
	
		html = html.replace(_Fillin_Marker, "{{textbox}}&nbsp;");
		html = EcfrFillInUtils.processTextAndCheck(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
		html = HtmlUtils.parseFillinsAndNormalize(html);
	
		return html;
	}
	
	
	private static String process52_247_66 (int iStg1OfficialClauseId, String html, 
		TemplateStg2ClauseFillInRecord oTemplateStg2ClauseFillInRecord, 
		PreparedStatement psFillInInsertClause) throws SQLException {
	
		html = HtmlUtils.normalizeFillins(html);
		html = html.replaceFirst(Pattern.quote("__ days [<i>Contracting Officer shall insert number of days</i>]"), "{{textbox_man_52.247-66[0]}}");
	
		String sPlaceholder = "Contracting Officer shall insert number of days";
	
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.247-66[0]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
	
		html = html.replaceFirst(Pattern.quote("____  [<i>Contracting Officer shall insert dollar amount for rental, after evaluation of offers</i>]"), "{{textbox_man_52.247-66[1]}}");
	
		sPlaceholder = "Contracting Officer shall insert dollar amount for rental, after evaluation of offers";
	
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.247-66[1]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
	
		html = html.replaceFirst(Pattern.quote("____  [<i>Contracting Officer shall insert the cylinder types, sizes, capacities, and associated replacement values.</i>]"), "{{textbox_man_52.247-66[2]}}");
	
		sPlaceholder = "Contracting Officer shall insert the cylinder types, sizes, capacities, and associated replacement values.";
	
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.247-66[2]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
	
		html = html.replaceFirst(Pattern.quote("____  [<i>Contracting Officer shall insert number of days</i>]"), "{{textbox_man_52.247-66[3]}}");
	
		sPlaceholder = "Contracting Officer shall insert number of days";
	
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.247-66[3]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
	
	
		html = html.replace(_Fillin_Marker, "{{textbox}}&nbsp;");
		html = EcfrFillInUtils.processTextAndCheck(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
		html = HtmlUtils.parseFillinsAndNormalize(html);
	
		return html;
	}
	
	private static String process52_247_67 (int iStg1OfficialClauseId, String html, 
		TemplateStg2ClauseFillInRecord oTemplateStg2ClauseFillInRecord, 
		PreparedStatement psFillInInsertClause) throws SQLException {
	
		html = HtmlUtils.normalizeFillins(html);
		html = html.replaceFirst(Pattern.quote("<p>____ </p><p>____ </p><p>____ </p><p>[<i>To be filled in by Contracting Officer</i>]</p>"), 
				"<p>{{textbox_man_52.247-67[0]}} </p><p>{{textbox_man_52.247-67[1]}} </p><p>{{textbox_man_52.247-67[2]}} </p>");
	
		String sPlaceholder = "To be filled in by Contracting Officer";
	
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.247-67[0]", "S", 100, // piFillInMaxSize, 
			1, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
	
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
				"textbox_man_52.247-67[1]", "S", 100, // piFillInMaxSize, 
				1, null, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
				psFillInInsertClause);
		
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
				"textbox_man_52.247-67[2]", "S", 100, // piFillInMaxSize, 
				1, null, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
				psFillInInsertClause);
		
		html = html.replace(_Fillin_Marker, "{{textbox}}&nbsp;");
		html = EcfrFillInUtils.processTextAndCheck(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
		html = HtmlUtils.parseFillinsAndNormalize(html);
	
		return html;
	}
	
	private static String process52_252_1 (int iStg1OfficialClauseId, String html, 
		TemplateStg2ClauseFillInRecord oTemplateStg2ClauseFillInRecord, 
		PreparedStatement psFillInInsertClause) throws SQLException {
	
		html = HtmlUtils.normalizeFillins(html);
		html = html.replaceFirst(Pattern.quote("____  ____ <p>[<i>Insert one or more Internet addresses</i>]"), "{{textbox_man_52.252-1[0]}}");
	
		String sPlaceholder = "Insert one or more Internet addresses";
	
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.252-1[0]", "M", 250, // piFillInMaxSize, 
			null, sPlaceholder, 2, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
	
		html = html.replace(_Fillin_Marker, "{{textbox}}&nbsp;");
		html = EcfrFillInUtils.processTextAndCheck(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
		html = HtmlUtils.parseFillinsAndNormalize(html);
	
		return html;
	}
	
	private static String process52_252_2 (int iStg1OfficialClauseId, String html, 
		TemplateStg2ClauseFillInRecord oTemplateStg2ClauseFillInRecord, 
		PreparedStatement psFillInInsertClause) throws SQLException {
	
		html = HtmlUtils.normalizeFillins(html);
		html = html.replaceFirst(Pattern.quote("____  ____ <p>[<i>Insert one or more Internet addresses</i>]"), "{{textbox_man_52.252-2[0]}}");
	
		String sPlaceholder = "Insert one or more Internet addresses";
	
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.252-2[0]", "M", 250, // piFillInMaxSize, 
			null, sPlaceholder, 2, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
	
		html = html.replace(_Fillin_Marker, "{{textbox}}&nbsp;");
		html = EcfrFillInUtils.processTextAndCheck(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
		html = HtmlUtils.parseFillinsAndNormalize(html);
	
		return html;
	}
	
	private static String process52_252_5 (int iStg1OfficialClauseId, String html, 
		TemplateStg2ClauseFillInRecord oTemplateStg2ClauseFillInRecord, 
		PreparedStatement psFillInInsertClause) throws SQLException {
	
		html = HtmlUtils.normalizeFillins(html);
		html = html.replaceFirst(Pattern.quote("__ [<i>insert regulation name</i>]"), "{{textbox_man_52.252-5[0]}}");
	
		String sPlaceholder = "insert regulation name";
	
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.252-5[0]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
		
		html = html.replaceFirst(Pattern.quote("(48 CFR chapter _)"), "(48 CFR chapter {{textbox}})");
				
		html = html.replace(_Fillin_Marker, "{{textbox}}&nbsp;");
		html = EcfrFillInUtils.processTextAndCheck(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
		html = HtmlUtils.parseFillinsAndNormalize(html);
	
		return html;
	}
		
	
	private static String process252_229_7004 (int iStg1OfficialClauseId, String html, 
		TemplateStg2ClauseFillInRecord oTemplateStg2ClauseFillInRecord, 
		PreparedStatement psFillInInsertClause) throws SQLException {
	
		html = HtmlUtils.normalizeFillins(html);
		html = html.replaceFirst(Pattern.quote("(<i>Contracting Officer insert amount at time of contract award)"), "{{textbox_man_252.229-7004[0]}}");
	
		String sPlaceholder = "Contracting Officer insert amount at time of contract award";
	
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_252.229-7004[0]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
	
	
		html = html.replace(_Fillin_Marker, "{{textbox}}&nbsp;");
		html = EcfrFillInUtils.processTextAndCheck(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
		html = HtmlUtils.parseFillinsAndNormalize(html);
	
		return html;
	}
	
	private static String process252_235_7010 (int iStg1OfficialClauseId, String html, 
		TemplateStg2ClauseFillInRecord oTemplateStg2ClauseFillInRecord, 
		PreparedStatement psFillInInsertClause) throws SQLException {
	
		html = HtmlUtils.normalizeFillins(html);
		html = html.replaceFirst(Pattern.quote("(name of contracting agency(ies)"), "{{textbox_man_252.235-7010[0]}}");
	
		String sPlaceholder = "name of contracting agency(ies)";
	
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_252.235-7010[0]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
	
		// oh oh, *The frequency, date, and length of reporting periods should be selected as appropriate to the particular circumstances of the contract. was not found
	
		
		html = html.replaceFirst(Pattern.quote("(Contracting agency(ies) contract number(s))."), "{{textbox_man_252.235-7010[1]}}");
	
		sPlaceholder = "Contracting agency(ies) contract number(s).";
	
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_252.235-7010[1]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
	
		html = html.replaceFirst(Pattern.quote("(name of contracting agency(ies))"), "{{textbox_man_252.235-7010[2]}}");
	
		sPlaceholder = "name of contracting agency(ies)";
	
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_252.235-7010[2]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
	
	
		html = html.replace(_Fillin_Marker, "{{textbox}}&nbsp;");
		html = EcfrFillInUtils.processTextAndCheck(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
		html = HtmlUtils.parseFillinsAndNormalize(html);
	
		return html;
	}
	
	private static String process52_216_5 (int iStg1OfficialClauseId, String html, 
		TemplateStg2ClauseFillInRecord oTemplateStg2ClauseFillInRecord, 
		PreparedStatement psFillInInsertClause) throws SQLException {
	
		html = HtmlUtils.normalizeFillins(html);
		html = html.replaceFirst(Pattern.quote("__, [<i>see Note (1)</i>]"), "{{textbox_man_52.216-5[0]}}");
	
		String sPlaceholder = "Note: Express in terms of units delivered, or as a date; but in either case the period should end on the last day of a month";
	
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.216-5[0]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
	
		html = html.replaceFirst(Pattern.quote("__ [<i>insert appropriate number</i>]"), "{{textbox_man_52.216-5[1]}}");
	
		sPlaceholder = "insert appropriate number";
	
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.216-5[1]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
	
		html = html.replaceFirst(Pattern.quote("_ [<i>see Note (2)</i>]"), "{{textbox_man_52.216-5[2]}}");
	
		sPlaceholder = "Note: Insert the numbers of days chosen so that the Contractor's submission will be late enough to reflect recent cost experience (taking into account the Contractor's accounting system), but early enough to permit review, audit (if necessary), and negotiation before the start of the prospective period";
	
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.216-5[2]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
	
		html = html.replaceFirst(Pattern.quote("____  month (see Note (3))"), "{{textbox_man_52.216-5[3]}}");

		sPlaceholder = "Note: Insert first, except that second may be inserted if necessary to achieve compatibility with the Contractor's accounting system";
		
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.216-5[3]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);

		
		html = html.replace(_Fillin_Marker, "{{textbox}}&nbsp;");
		html = EcfrFillInUtils.processTextAndCheck(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
		html = HtmlUtils.parseFillinsAndNormalize(html);
	
		return html;
	}
	
	private static String process52_222_2 (int iStg1OfficialClauseId, String html, 
		TemplateStg2ClauseFillInRecord oTemplateStg2ClauseFillInRecord, 
		PreparedStatement psFillInInsertClause) throws SQLException {
	
		html = HtmlUtils.normalizeFillins(html);
		html = html.replaceFirst(Pattern.quote("__ or the overtime"), "{{textbox_man_52.222-2[0]}} or the overtime");
		html = html.replaceFirst(Pattern.quote("<p>*Insert either \"zero\" or the dollar amount agreed to during negotiations.</p>"),"");
		
		String sPlaceholder = "Insert either \"zero\" or the dollar amount agreed to during negotiations";
	
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.222-2[0]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
	
	
		html = html.replace(_Fillin_Marker, "{{textbox}}&nbsp;");
		html = EcfrFillInUtils.processTextAndCheck(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
		html = HtmlUtils.parseFillinsAndNormalize(html);
	
		return html;
	}
	
	private static String process52_225_23 (int iStg1OfficialClauseId, String html, 
		TemplateStg2ClauseFillInRecord oTemplateStg2ClauseFillInRecord, 
		PreparedStatement psFillInInsertClause) throws SQLException {
	
		html = HtmlUtils.normalizeFillins(html);
		html = html.replaceFirst(Pattern.quote("[<i>Contracting Officer to list applicable excepted materials or indicate \"none\".</i>]"), "{{textbox_man_52.225-23[0]}}");
	
		String sPlaceholder = "Contracting Officer to list applicable excepted materials or indicate \"none\".";
	
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.225-23[0]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
	
	
		html = html.replace(_Fillin_Marker, "{{textbox}}&nbsp;");
		html = EcfrFillInUtils.processTextAndCheck(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
		html = HtmlUtils.parseFillinsAndNormalize(html);
	
		return html;
	}
	
	private static String process52_252_6 (int iStg1OfficialClauseId, String html, 
		TemplateStg2ClauseFillInRecord oTemplateStg2ClauseFillInRecord, 
		PreparedStatement psFillInInsertClause) throws SQLException {
	
		html = HtmlUtils.normalizeFillins(html);
		html = html.replaceFirst(Pattern.quote("__ [<i>insert regulation name</i>]"), "{{textbox_man_52.252-6[0]}}");
	
		String sPlaceholder = "insert regulation name";
	
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.252-6[0]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
	
		html = html.replaceFirst(Pattern.quote("(48 CFR _)"), "(48 CFR {{textbox}})");
	
		html = html.replace(_Fillin_Marker, "{{textbox}}&nbsp;");
		html = EcfrFillInUtils.processTextAndCheck(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
		html = HtmlUtils.parseFillinsAndNormalize(html);
	
		return html;
	}
	
	private static String process52_215_20_AltIII (int iStg1OfficialClauseId, String html, 
		TemplateStg2ClauseFillInRecord oTemplateStg2ClauseFillInRecord, 
		PreparedStatement psFillInInsertClause) throws SQLException {
	
		html = HtmlUtils.normalizeFillins(html);
		html = html.replaceFirst(Pattern.quote("[<i>Insert media format, e.g., electronic spreadsheet format, electronic mail, etc.</i>]"), "{{textbox_man_52.215-20_AltIII[0]}}");
	
		String sPlaceholder = "Insert media format, e.g., electronic spreadsheet format, electronic mail, etc.";
	
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.215-20_AltIII[0]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
	
		html = html.replace(_Fillin_Marker, "{{textbox}}&nbsp;");
		html = EcfrFillInUtils.processTextAndCheck(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
		html = HtmlUtils.parseFillinsAndNormalize(html);
	
		return html;
	}
	
	private static String process52_219_18_AltI (int iStg1OfficialClauseId, String html, 
		TemplateStg2ClauseFillInRecord oTemplateStg2ClauseFillInRecord, 
		PreparedStatement psFillInInsertClause) throws SQLException {
	
		html = HtmlUtils.normalizeFillins(html);
		html = html.replaceFirst(Pattern.quote("____ * ____  (*<i>Contracting Officer completes by inserting the appropriate SBA District and/or Regional Office(s) as identified by the SBA</i>)"), "{{textbox_man_52.219-18_AltI[0]}}");
	
		String sPlaceholder = "*Contracting Officer completes by inserting the appropriate SBA District and/or Regional Office(s) as identified by the SBA";
	
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.219-18_AltI[0]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
	
	
		html = html.replace(_Fillin_Marker, "{{textbox}}&nbsp;");
		html = EcfrFillInUtils.processTextAndCheck(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
		html = HtmlUtils.parseFillinsAndNormalize(html);
	
		return html;
	}
	
	private static String process52_232_12_AltII (int iStg1OfficialClauseId, String html, 
		TemplateStg2ClauseFillInRecord oTemplateStg2ClauseFillInRecord, 
		PreparedStatement psFillInInsertClause) throws SQLException {
	
		html = HtmlUtils.normalizeFillins(html);
		html = html.replaceFirst(Pattern.quote("__ [<i>Insert an amount not higher than 10 percent of the stated estimated cost inserted in this paragraph.</i>]"), "{{textbox_man_52.232-12_AltII[1]}}");
	
		String sPlaceholder = "Insert an amount not higher than 10 percent of the stated estimated cost inserted in this paragraph.";
	
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.232-12_AltII[1]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
	
		html = html.replaceFirst(Pattern.quote("__ (not including fixed-fee, if any)"), "{{textbox_man_52.232-12_AltII[0]}}");

		sPlaceholder = "not including fixed-fee, if any";

		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.232-12_AltII[0]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
		
		html = html.replace(_Fillin_Marker, "{{textbox}}&nbsp;");
		html = EcfrFillInUtils.processTextAndCheck(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
		html = HtmlUtils.parseFillinsAndNormalize(html);
	
		return html;
	}
	
	private static String process52_232_12_AltV (int iStg1OfficialClauseId, String html, 
			TemplateStg2ClauseFillInRecord oTemplateStg2ClauseFillInRecord, 
			PreparedStatement psFillInInsertClause) throws SQLException {
		
			html = HtmlUtils.normalizeFillins(html);
			// CJ-1326, Add missing text
			html += "<p>If this Alternate is used in combination with Alternate III, insert the additional sentence set forth in Alternate III as the first sentence of paragraph (d) of this Alternate.<p></p>If this Alternate is used in combination with Alternate IV, insert the additional sentences set forth in Alternate IV as the beginning sentences of paragraph (e) of this Alternate.</p><p>Advance Payments Without Special Account (MAY 2001)</p><p>(a) Requirements for payment. Advance payments will be made under this contract (1) upon submission of properly certified invoices or vouchers by the contractor, and approval by the administering office, {{textbox_man_52.232-12_AltV[1]}}, or (2) under a letter of credit. The amount of the invoice or voucher submitted plus all advance payments previously approved shall not exceed $___. If a letter of credit is used, the Contractor shall withdraw cash only when needed for disbursements acceptable under this contract and report cash disbursements and balances as required by the administering office. The Contractor shall apply terms similar to this clause to any advance payments to subcontractors.</p><p>(b) Use of funds. The Contractor may use advance payment funds only to pay for properly allocable, allowable, and reasonable costs for direct materials, direct labor, and indirect costs. Determinations of whether costs are properly allocable, allowable, and reasonable shall be in accordance with generally accepted accounting principles, subject to any applicable subparts of part 31 of the Federal Acquisition Regulation.</p><p>(c) Repayment to the Government. At any time, the Contractor may repay all or any part of the funds advanced by the Government. Whenever requested in writing to do so by the administering office, the Contractor shall repay to the Government any part of unliquidated advance payments considered by the administering office to exceed the Contractor's current requirements or the amount specified in paragraph (a) of this clause.</p><p>(d) Maximum payment. When the sum of all unliquidated advance payments, unpaid interest charges, and other payments exceed __ percent of the contract price, the Government shall withhold further payments to the Contractor. On completion or termination of the contract, the Government shall deduct from the amount due to the Contractor all unliquidated advance payments and all interest charges payable. If previous payments to the Contractor exceed the amount due, the excess amount shall be paid to the Government on demand. For purposes of this paragraph, the contract price shall be considered to be the stated contract price of $___, less any subsequent price reductions under the contract, plus (1) any price increases resulting from any terms of this contract for price redetermination or escalation, and (2) any other price increases that do not, in the aggregate, exceed ${{textbox_man_52.232-12_AltV[2]}}. Any payments withheld under this paragraph shall be applied to reduce the unliquidated advance payments. If full liquidation has been made, payments under the contract shall resume.</p><p>(e) Interest. (1) The Contractor shall pay interest to the Government on the daily unliquidated advance payments at the daily rate in subparagraph (e)(3) of this clause. Interest shall be computed at the end of each calendar month for the actual number of days involved. For the purpose of computing the interest charge—</p><p>(i) Advance payments shall be considered as increasing the unliquidated balance as of the date of the advance payment check;</p><p>(ii) Repayments by Contractor check shall be considered as decreasing the unliquidated balance as of the date on which the check is received by the Government authority designated by the Contracting Officer; and</p><p>(iii) Liquidations by deductions from Government payments to the Contractor shall be considered as decreasing the unliquidated balance as of the date of the check for the reduced payment.</p><p>(2) Interest charges resulting from the monthly computation shall be deducted from payments, other than advance payments, due the Contractor. If the accrued interest exceeds the payment due, any excess interest shall be carried forward and deducted from subsequent payments. Interest carried forward shall not be compounded. Interest on advance payments shall cease to accrue upon satisfactory completion or termination of the contract for the convenience of the Government. The Contractor shall charge interest on advance payments to subcontractors in the manner described above and credit the interest to the Government. Interest need not be charged on advance payments to nonprofit educational or research subcontractors, for experimental, developmental, or research work.</p><p>(3) If interest is required under the contract, the Contracting Officer shall determine a daily interest rate based on the rate established by the Secretary of the Treasury under Pub. L. 92-41 (50 U.S.C. App., 1215(b)(2)). The Contracting Officer shall revise the daily interest rate during the contract period in keeping with any changes in the cited interest rate.</p><p>(4) If the full amount of interest charged under this paragraph has not been paid by deduction or otherwise upon completion or termination of this contract, the Contractor shall pay the remaining interest to the Government on demand.</p><p>(f) Lien on property under contract. (1) All advance payments under this contract, together with interest charges, shall be secured, when made, by a lien in favor of the Government, paramount to all other liens, on the supplies or other things covered by this contract and on all material and other property acquired for or allocated to the performance of this contract, except to the extent that the Government by virtue of any other terms of this contract, or otherwise, shall have valid title to the supplies, materials, or other property as against other creditors of the Contractor.</p><p>(2) The Contractor shall identify, by marking or segregation, all property that is subject to a lien in favor of the Government by virtue of any terms of this contract in such a way as to indicate that it is subject to a lien and that it has been acquired for or allocated to performing this contract. If, for any reason, the supplies, materials, or other property are not identified by marking or segregation, the Government shall be considered to have a lien to the extent of the Government's interest under this contract on any mass of property with which the supplies, materials, or other property are commingled. The Contractor shall maintain adequate accounting control over the property on its books and records.</p><p>(3) If, at any time during the progress of the work on the contract, it becomes necessary to deliver to a third person any items or materials on which the Government has a lien, the Contractor shall notify the third person of the lien and shall obtain from the third person a receipt in duplicate acknowledging the existence of the lien. The Contractor shall provide a copy of each receipt to the Contracting Officer.</p><p>(4) If, under the termination clause, the Contracting Officer authorizes the contractor to sell or retain termination inventory, the approval shall constitute a release of the Government's lien to the extent that—</p><p>(i) The termination inventory is sold or retained; and</p><p>(ii) The sale proceeds or retention credits are applied to reduce any outstanding advance payments.</p><p>(g) Insurance. (1) The Contractor shall maintain with responsible insurance carriers—</p><p>(i) Insurance on plant and equipment against fire and other hazards, to the extent that similar properties are usually insured by others operating plants and properties of similar character in the same general locality;</p><p></p><p>(ii) Adequate insurance against liability on account of damage to persons or property; and</p><p>(iii) Adequate insurance under all applicable workers' compensation laws.</p><p>(2) Until work under this contract has been completed and all advance payments made under the contract have been liquidated, the Contractor shall—</p><p>(i) Maintain this insurance;</p><p>(ii) Maintain adequate insurance on any materials, parts, assemblies, subassemblies, supplies, equipment, and other property acquired for or allocable to this contract and subject to the Government lien under paragraph (f) of this clause; and</p><p>(iii) Furnish any evidence with respect to its insurance that the administering office may require.</p><p>(h) Default. (1) If any of the following events occur, the Government may, by written notice to the Contractor, withhold further payments on this contract:</p><p>(i) Termination of this contract for a fault of the Contractor.</p><p>(ii) A finding by the administering office that the Contractor has failed to—</p><p>(A) Observe any of the conditions of the advance payment terms;</p><p>(B) Comply with any material term of this contract;</p><p>(C) Make progress or maintain a financial condition adequate for performance of this contract;</p><p>(D) Limit inventory allocated to this contract to reasonable requirements; or</p><p>(E) Avoid delinquency in payment of taxes or of the costs of performing this contract in the ordinary course of business.</p><p>(iii) The appointment of a trustee, receiver, or liquidator for all or a substantial part of the Contractor's property, or the institution of proceedings by or against the Contractor for bankruptcy, reorganization, arrangement, or liquidation.</p><p>(iv) The commission of an act of bankruptcy.</p><p>(2) If any of the events described in subparagraph (h)(1) of this clause continue for 30 days after the written notice to the Contractor, the Government may take any of the following additional actions:</p><p>(i) Charge interest, in the manner prescribed in paragraph (e) of this clause, on outstanding advance payments during the period of any event described in subparagraph (h)(1) of this clause.</p><p>(ii) Demand immediate repayment by the Contractor of the unliquidated balance of advance payments.</p><p>(iii) Take possession of and, with or without advertisement, sell at public or private sale all or any part of the property on which the Government has a lien under this contract and, after deducting any expenses incident to the sale, apply the net proceeds of the sale to reduce the unliquidated balance of advance payments or other Government claims against the Contractor.</p><p>(3) The Government may take any of the actions described in subparagraphs (h)(1) and (h)(2) of this clause it considers appropriate at its discretion and without limiting any other rights of the Government.</p><p>(i) Prohibition against assignment. Notwithstanding any other terms of this contract, the Contractor shall not assign this contract, any interest therein, or any claim under the contract to any party.</p><p>(j) Information and access to records. The Contractor shall furnish to the administering office (1) monthly or at other intervals as required, signed or certified balance sheets and profit and loss statements, and, (2) if requested, other information concerning the operation of the contractor's business. The Contractor shall provide the authorized Government representatives proper facilities for inspection of the Contractor's books, records, and accounts.</p><p>(k) Other security. The terms of this contract are considered to provide adequate security to the Government for advance payments; however, if the administering office considers the security inadequate, the Contractor shall furnish additional security satisfactory to the administering office, to the extent that the security is available.</p><p>(l) Representations. The Contractor represents the following:</p><p>(1) The balance sheet, the profit and loss statement, and any other supporting financial statements furnished to the administering office fairly reflect the financial condition of the Contractor at the date shown or the period covered, and there has been no subsequent materially adverse change in the financial condition of the Contractor.</p><p>(2) No litigation or proceedings are presently pending or threatened against the Contractor, except as shown in the financial statements.</p><p>(3) The Contractor has disclosed all contingent liabilities, except for liability resulting from the renegotiation of defense production contracts, in the financial statements furnished to the administering office.</p><p>(4) None of the terms in this clause conflict with the authority under which the Contractor is doing business or with the provision of any existing indenture or agreement of the Contractor.</p><p>(5) The Contractor has the power to enter into this contract and accept advance payments, and has takken all necessary action to authorize the acceptance under the terms of this contract.</p><p>(6) The assets of the Contractor are not subject to any lien or encumbrance of any character except for current taxes not delinquent, and except as shown in the financial statements furnished by the Contractor. There is no current assignment of claims under any contract affected by these advance payment provisions.</p><p>(7) All information furnished by the Contractor to the administering office in connection with each request for advance payments is true and correct.</p><p>(8) These representations shall be continuing and shall be considered to have been repeated by the submission of each invoice for advance payments.</p><p>(m) Covenants. To the extent the Government considers it necessary while any advance payments made under this contract remain outstanding, the Contractor, without the prior written consent of the administering office, shall not—</p><p>(1) Mortgage, pledge, or otherwise encumber or allow to be encumbered, any of the assets of the Contractor now owned or subsequently acquired, or permit any preexisting mortgages, liens, or other encumbrances to remain on or attach to any assets of the Contractor which are allocated to performing this contract and with respect to which the Government has a lien under this contract;</p><p>(2) Sell, assign, transfer, or otherwise dispose of accounts receivable, notes, or claims for money due or to become due;</p><p>(3) Declare or pay any dividends, except dividends payable in stock of the corporation, or make any other distribution on account of any shares of its capital stock, or purchase, redeem, or otherwise acquire for value any of its stock, except as required by sinking fund or redemption arrangements reported to the administering office incident to the establishment of these advance payment provisions;</p><p>(4) Sell, convey, or lease all or a substantial part of its assets;</p><p>(5) Acquire for value the stock or other securities of any corporation, municipality, or Governmental authority, except direct obligations of the United States;</p><p>(6) Make any advance or loan or incur any liability as guarantor, surety, or accommodation endorser for any party;</p><p>(7) Permit a writ of attachment or any similar process to be issued against its property without getting a release or bonding the property within 30 days after the entry of the writ of attachment or other process;</p><p>(8) Pay any remuneration in any form to its directors, officers, or key employees higher than rates provided in existing agreements of which notice has been given to the administering office; accure excess remuneration without first obtaining an agreement subordinating it to all claims of the Government; or employ any person at a rate of compensation over ___ a year.</p><p>(9) Change substantially the management, ownership, or control of the corporation;</p><p>(10) Merge or consolidate with any other firm or corporation, change the type of business, or engage in any transaction outside the ordinary course of the Contractor's business as presently conducted;</p><p>(11) Deposit any of its funds except in a bank or trust company insured by the Federal Deposit Insurance Corporation or a credit union insured by the National Credit Union Administration;</p><p>(12) Create or incur indebtedness for advances, other than advances to be made under the terms of this contract, or for borrowings;</p><p>(13) Make or covenant for capital expenditures exceeding $___ in total;</p><p>(14) Permit its net current assets, computed in accordance with generally accepted accounting principles, to become less than ___; or</p><p>(15) Make any payments on account of the obligations listed below, except in the manner and to the extent provided in this contract:</p><p>{{textbox_man_52.232-12_AltV[3]}}</p>";

			oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
					"textbox_man_52.232-12_AltV[1]", "S", 100, // piFillInMaxSize, 
					null, "insert the name of the office designated under agency procedures", null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
					psFillInInsertClause);
			oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
					"textbox_man_52.232-12_AltV[2]", "S", 100, // piFillInMaxSize, 
					null, "insert an amount not higher than 10 percent of the stated contract amount inserted in this paragraph", null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
					psFillInInsertClause);
			oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
					"textbox_man_52.232-12_AltV[3]", "M", 500, // piFillInMaxSize, 
					null, "List the pertinent obligations", 3, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
					psFillInInsertClause);
			
			html = html.replace(_Fillin_Marker, "{{textbox}}&nbsp;");
			html = EcfrFillInUtils.processTextAndCheck(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
			html = HtmlUtils.parseFillinsAndNormalize(html);
		
			return html;
		}
	
	private static String process252_227_7009 (int iStg1OfficialClauseId, String html, 
			TemplateStg2ClauseFillInRecord oTemplateStg2ClauseFillInRecord, 
			PreparedStatement psFillInInsertClause) throws SQLException {

			html = HtmlUtils.normalizeFillins(html);
			//html = html.replaceFirst(Pattern.quote("The frequency, date, and length of reporting periods should be selected as appropriate to the particular circumstances of the contract."), "{{textbox_man_252.227-7009[0]}}");
			
			html = html.replaceFirst(Pattern.quote("<p>*The frequency, date, and length of reporting periods should be selected as appropriate to the particular circumstances of the contract.</p>"), "");
			html = html.replaceFirst(Pattern.quote("ending ____  during"), "ending {{textbox_man_252.227-7009[0]}} during");
			html = html.replaceFirst(Pattern.quote("ending ____  shall"), "ending {{textbox_man_252.227-7009[1]}}  shall");
			
			String sPlaceholder = "*The frequency, date, and length of reporting periods should be selected as appropriate to the particular circumstances of the contract.";

			oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
				"textbox_man_252.227-7009[0]", "S", 100, // piFillInMaxSize, 
				null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
				psFillInInsertClause);
			oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
					"textbox_man_252.227-7009[1]", "S", 100, // piFillInMaxSize, 
					null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
					psFillInInsertClause);

			html = html.replace(_Fillin_Marker, "{{textbox}}&nbsp;");
			html = EcfrFillInUtils.processTextAndCheck(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
			html = HtmlUtils.parseFillinsAndNormalize(html);

			return html;
		}

	
	private static String process252_217_7000 (int iStg1OfficialClauseId, String html, 
		TemplateStg2ClauseFillInRecord oTemplateStg2ClauseFillInRecord, 
		PreparedStatement psFillInInsertClause) throws SQLException {
	
		html = HtmlUtils.normalizeFillins(html);
		html = html.replaceFirst(Pattern.quote("____ <p>(Insert name of country)"), "{{textbox_man_252.217-7000[0]}}");
	
		String sPlaceholder = "Insert name of country";
	
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_252.217-7000[0]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
	
		html = html.replaceFirst(Pattern.quote("____ <p>(Insert applicable CLIN)"), "{{textbox_man_252.217-7000[1]}}");
		
		sPlaceholder = "Insert Applicable CLIN";
	
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_252.217-7000[1]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
	
		html = html.replace(_Fillin_Marker, "{{textbox}}&nbsp;");
		html = EcfrFillInUtils.processTextAndCheck(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
		html = HtmlUtils.parseFillinsAndNormalize(html);
	
		return html;
	}
	
	private static String process252_217_7027 (int iStg1OfficialClauseId, String html, 
		TemplateStg2ClauseFillInRecord oTemplateStg2ClauseFillInRecord, 
		PreparedStatement psFillInInsertClause) throws SQLException {
	
		html = HtmlUtils.normalizeFillins(html);
		html = html.replaceFirst(Pattern.quote("____ (<i>insert specific type of contract action</i>)"), "{{textbox_man_252.217-7027[0]}}");
	
		String sPlaceholder = "insert specific type of contract action";
	
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_252.217-7027[0]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
	
		html = html.replaceFirst(Pattern.quote("____ (<i>insert type of proposal; e.g., fixed-price or cost-and-fee</i>)"), "{{textbox_man_252.217-7027[1]}}");
	
		sPlaceholder = "insert type of proposal; e.g., fixed-price or cost-and-fee";
	
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_252.217-7027[1]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
	
		html = html.replaceFirst(Pattern.quote("(<i>insert target date for definitization of the contract action and dates for submission of proposal, beginning of negotiations, and, if appropriate, submission of the make-or-buy and subcontracting plans and certified cost or pricing data).</i></p>&nbsp; ____  ____  ____  ____ "), "{{textbox_man_252.217-7027[2]}}");
	
		sPlaceholder = "insert target date for definitization of the contract action and dates for submission of proposal, beginning of negotiations, and, if appropriate, submission of the make-or-buy and subcontracting plans and certified cost or pricing data.";
	
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_252.217-7027[2]", "M", 500, // piFillInMaxSize, 
			null, sPlaceholder, 3, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
	
		html = html.replaceFirst(Pattern.quote("____  (<i>insert \"cost/price ceiling\" or \"firm-fixed price\"</i>)"), "{{textbox_man_252.217-7027[3]}}");
	
		sPlaceholder = "insert \"cost/price ceiling\" or \"firm-fixed price\"";
	
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_252.217-7027[3]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
	
		html = html.replaceFirst(Pattern.quote("____  (<i>insert the not-to-exceed amount)"), "{{textbox_man_252.217-7027[4]}}");
	
		sPlaceholder = "insert the not-to-exceed amount";
	
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_252.217-7027[4]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
	
	
		html = html.replace(_Fillin_Marker, "{{textbox}}&nbsp;");
		html = EcfrFillInUtils.processTextAndCheck(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
		html = HtmlUtils.parseFillinsAndNormalize(html);
	
		return html;
	}
	
	
	
	
	
	private static String process252_247_7001 (int iStg1OfficialClauseId, String html, 
		TemplateStg2ClauseFillInRecord oTemplateStg2ClauseFillInRecord, 
		PreparedStatement psFillInInsertClause) throws SQLException {
	
		html = HtmlUtils.normalizeFillins(html);
		html = html.replaceFirst(Pattern.quote("(<i>insert date</i>)"), "{{textbox_man_252.247-7001[0]}}");
	
		String sPlaceholder = "insert date";
	
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_252.247-7001[0]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
	
	
		html = html.replace(_Fillin_Marker, "{{textbox}}&nbsp;");
		html = EcfrFillInUtils.processTextAndCheck(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
		html = HtmlUtils.parseFillinsAndNormalize(html);
	
		return html;
	}
	
	private static String process252_247_7021 (int iStg1OfficialClauseId, String html, 
		TemplateStg2ClauseFillInRecord oTemplateStg2ClauseFillInRecord, 
		PreparedStatement psFillInInsertClause) throws SQLException {
	
		html = HtmlUtils.normalizeFillins(html);
		html = html.replaceFirst(Pattern.quote("____ (insert number of days)"), "{{textbox_man_252.247-7021[0]}}");
	
		String sPlaceholder = "insert number of days";
	
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_252.247-7021[0]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
	
		html = html.replaceFirst(Pattern.quote("____  (insert dollar amount for rental)"), "{{textbox_man_252.247-7021[1]}}");
	
		sPlaceholder = "(insert dollar amount for rental)";
	
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_252.247-7021[1]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
	
		html = html.replaceFirst(Pattern.quote("____ <p>(Insert the container types, sizes, capacities, and associated replacement values.)"), "{{textbox_man_252.247-7021[2]}}");
	
		sPlaceholder = "Insert the container types, sizes, capacities, and associated replacement values.";
	
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_252.247-7021[2]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
	
		html = html.replaceFirst(Pattern.quote("____  (insert number of days)"), "{{textbox_man_252.247-7021[3]}}");
	
		sPlaceholder = "insert number of days";
	
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_252.247-7021[3]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
	
	
		html = html.replace(_Fillin_Marker, "{{textbox}}&nbsp;");
		html = EcfrFillInUtils.processTextAndCheck(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
		html = HtmlUtils.parseFillinsAndNormalize(html);
	
		return html;
	}
	
	private static String process52_222_49 (int iStg1OfficialClauseId, String html, 
		TemplateStg2ClauseFillInRecord oTemplateStg2ClauseFillInRecord, 
		PreparedStatement psFillInInsertClause) throws SQLException {
	
		html = HtmlUtils.normalizeFillins(html);
		html = html.replaceFirst(Pattern.quote("____  (insert places or areas)"), "{{textbox_man_52.222-49[0]}}");
	
		String sPlaceholder = "insert places or areas";
	
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.222-49[0]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
	
		html = html.replaceFirst(Pattern.quote("____  (insert time and date)"), "{{textbox_man_52.222-49[1]}}");
	
		sPlaceholder = "insert time and date";
	
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.222-49[1]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
	
	
		html = html.replace(_Fillin_Marker, "{{textbox}}&nbsp;");
		html = EcfrFillInUtils.processTextAndCheck(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
		html = HtmlUtils.parseFillinsAndNormalize(html);
	
		return html;
	}
	
	private static String process52_223_3 (int iStg1OfficialClauseId, String html, 
		TemplateStg2ClauseFillInRecord oTemplateStg2ClauseFillInRecord, 
		PreparedStatement psFillInInsertClause) throws SQLException {
	
		html = HtmlUtils.normalizeFillins(html);
		html = html.replaceFirst(Pattern.quote("(If none, insert <i>None</i>)"), "");
	
		html = html.replaceFirst(Pattern.quote("____  ____  ____"), "{{textbox_man_52.223-3[0]}}");
		html = html.replaceFirst(Pattern.quote("____  ____  ____"), "{{textbox_man_52.223-3[1]}}");
		
		String sPlaceholder = "If none, insert None";
	
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.223-3[0]", "S", 250, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.223-3[1]", "S", 250, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
	
		html = html.replace(_Fillin_Marker, "{{textbox}}&nbsp;");
		html = EcfrFillInUtils.processTextAndCheck(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
		html = HtmlUtils.parseFillinsAndNormalize(html);
	
		return html;
	}
	
	private static String process52_223_7 (int iStg1OfficialClauseId, String html, 
		TemplateStg2ClauseFillInRecord oTemplateStg2ClauseFillInRecord, 
		PreparedStatement psFillInInsertClause) throws SQLException {
	
		html = HtmlUtils.normalizeFillins(html);
		html = html.replaceFirst(Pattern.quote("*The Contracting Officer shall insert the number of days required in advance of delivery of the item or completion of the servicing to assure that required licenses are obtained and appropriate personnel are notified to institute any necessary safety and health precautions. See FAR 23.601(d)"), "{{textbox_man_52.223-7[0]}}");
	
		String sPlaceholder = "*The Contracting Officer shall insert the number of days required in advance of delivery of the item or completion of the servicing to assure that required licenses are obtained and appropriate personnel are notified to institute any necessary safety and health precautions. See FAR 23.601(d)";
	
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.223-7[0]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
	
	
		html = html.replace(_Fillin_Marker, "{{textbox}}&nbsp;");
		html = EcfrFillInUtils.processTextAndCheck(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
		html = HtmlUtils.parseFillinsAndNormalize(html);
	
		return html;
	}
	
	private static String process52_227_5 (int iStg1OfficialClauseId, String html, 
		TemplateStg2ClauseFillInRecord oTemplateStg2ClauseFillInRecord, 
		PreparedStatement psFillInInsertClause) throws SQLException {
	
		html = HtmlUtils.normalizeFillins(html);
		html = html.replaceFirst(Pattern.quote("____ <p><i>Contracting Officer identify the patents by number or by other means if more appropriate"), "{{textbox_man_52.227-5[0]}}");
	
		String sPlaceholder = "Contracting Officer identify the patents by number or by other means if more appropriate";
	
		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.227-5[0]", "S", 100, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
	
	
		html = html.replace(_Fillin_Marker, "{{textbox}}&nbsp;");
		html = EcfrFillInUtils.processTextAndCheck(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
		html = HtmlUtils.parseFillinsAndNormalize(html);
	
		return html;
	}
	
	private static String process52_222_50_AltI (int iStg1OfficialClauseId, String html, 
		TemplateStg2ClauseFillInRecord oTemplateStg2ClauseFillInRecord, 
		PreparedStatement psFillInInsertClause) throws SQLException {

		html = HtmlUtils.normalizeFillins(html);
		html = html.replaceFirst(Pattern.quote("[<i>Contracting Officer shall insert title of directive/notice; indicate the document is attached or provide source (such as website link) for obtaining document; and, indicate the contract performance location outside the United States to which the document applies.</i>]"), 
				"{{textbox_man_52.222-50_AltI[0]}}{{textbox_man_52.222-50_AltI[1]}}{{textbox_man_52.222-50_AltI[2]}}");

		html = html.replaceFirst(Pattern.quote("<table border=\"1\" cellpadding=\"1\" cellspacing=\"1\" frame=\"void\" width=\"100%\"><thead><tr><th scope=\"col\">Document Title</th><th scope=\"col\">Document may be obtained from:</th><th scope=\"col\">Applies to performance in/at:</th></tr></thead><tbody><tr><td align=\"left\" scope=\"row\">____ </td><td align=\"left\">____ </td><td align=\"left\">____ </td></tr><tr><td align=\"left\" scope=\"row\">____ </td><td align=\"left\">____ </td><td align=\"left\">____ </td></tr></tbody></table>"), "");
		
		String sPlaceholder = "Contracting Officer shall insert title of directive/notice; indicate the document is attached or provide source (such as website link) for obtaining document; and, indicate the contract performance location outside the United States to which the document applies.";
		
		oTemplateStg2ClauseFillInRecord.set2(html, iStg1OfficialClauseId,
				"textbox_man_52.222-50_AltI[0]", // psFillInCode,
				"S", // psFillInType,
				100, // piFillInMaxSize,
				0, // piFillInGroupNumber,
				sPlaceholder, // psFillInPlaceholder,
				null, // psFillInDefaultData,
				1, // piFillInDisplayRows,
				true, // pbFillInForTable,
				"Document Title" // psFillInHeading
				);
		oTemplateStg2ClauseFillInRecord.insert(psFillInInsertClause);
		oTemplateStg2ClauseFillInRecord.set2(html, iStg1OfficialClauseId,
				"textbox_man_52.222-50_AltI[1]", // psFillInCode,
				"S", // psFillInType,
				100, // piFillInMaxSize,
				0, // piFillInGroupNumber,
				null, // psFillInPlaceholder,
				null, // psFillInDefaultData,
				1, // piFillInDisplayRows,
				true, // pbFillInForTable,
				"Document may be obtained from:" // psFillInHeading
				);
		oTemplateStg2ClauseFillInRecord.insert(psFillInInsertClause);
		oTemplateStg2ClauseFillInRecord.set2(html, iStg1OfficialClauseId,
				"textbox_man_52.222-50_AltI[2]", // psFillInCode,
				"S", // psFillInType,
				100, // piFillInMaxSize,
				0, // piFillInGroupNumber,
				null, // psFillInPlaceholder,
				null, // psFillInDefaultData,
				1, // piFillInDisplayRows,
				true, // pbFillInForTable,
				"Applies to performance in/at:" // psFillInHeading
				);
		oTemplateStg2ClauseFillInRecord.insert(psFillInInsertClause);
		html = html.replace(_Fillin_Marker, "{{textbox}}&nbsp;");
		html = EcfrFillInUtils.processTextAndCheck(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
		html = HtmlUtils.parseFillinsAndNormalize(html);

		return html;
	}
	




	private static String process52_227_3_AltI (int iStg1OfficialClauseId, String html, 
		TemplateStg2ClauseFillInRecord oTemplateStg2ClauseFillInRecord, 
		PreparedStatement psFillInInsertClause) throws SQLException {

		html = HtmlUtils.normalizeFillins(html);
		html = html.replaceFirst(Pattern.quote("____  [<i>Contracting Officer list and/or identify the items to be excluded from this indemnity</i>]"), "{{textbox_man_52.227-3_AltI[0]}}");

		String sPlaceholder = "Contracting Officer list and/or identify the items to be excluded from this indemnity";

		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.227-3_AltI[0]", "S", 250, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);


		html = html.replace(_Fillin_Marker, "{{textbox}}&nbsp;");
		html = EcfrFillInUtils.processTextAndCheck(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
		html = HtmlUtils.parseFillinsAndNormalize(html);

		return html;
	}
	
	private static String process52_227_3_AltII (int iStg1OfficialClauseId, String html, 
		TemplateStg2ClauseFillInRecord oTemplateStg2ClauseFillInRecord, 
		PreparedStatement psFillInInsertClause) throws SQLException {

		html = HtmlUtils.normalizeFillins(html);
		html = html.replaceFirst(Pattern.quote("____ </p><p>(<i>List or identify the items to be included under this indemnity</i>)"), "{{textbox_man_52.227-3_AltII[0]}}");

		String sPlaceholder = "List or identify the items to be included under this indemnity";

		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.227-3_AltII[0]", "S", 250, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);


		html = html.replace(_Fillin_Marker, "{{textbox}}&nbsp;");
		html = EcfrFillInUtils.processTextAndCheck(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
		html = HtmlUtils.parseFillinsAndNormalize(html);

		return html;
	}

	private static String process52_227_4_AltI (int iStg1OfficialClauseId, String html, 
		TemplateStg2ClauseFillInRecord oTemplateStg2ClauseFillInRecord, 
		PreparedStatement psFillInInsertClause) throws SQLException {

		html = HtmlUtils.normalizeFillins(html);
		html = html.replaceFirst(Pattern.quote("____ </p><p>[<i>Contracting Officer list the items to be excluded.</i>]"), "{{textbox_man_52.227-4_AltI[0]}}");

		String sPlaceholder = "Contracting Officer list the items to be excluded.";

		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.227-4_AltI[0]", "S", 250, // piFillInMaxSize, 
			null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);


		html = html.replace(_Fillin_Marker, "{{textbox}}&nbsp;");
		html = EcfrFillInUtils.processTextAndCheck(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
		html = HtmlUtils.parseFillinsAndNormalize(html);

		return html;
	}

	private static String process52_222_35_AltI (int iStg1OfficialClauseId, String html, 
			TemplateStg2ClauseFillInRecord oTemplateStg2ClauseFillInRecord, 
			PreparedStatement psFillInInsertClause) throws SQLException {

			html = HtmlUtils.normalizeFillins(html);
			html = html.replaceFirst(Pattern.quote("____ [<i>List term(s)</i>]"), "{{textbox_man_52.222-35_AltI[0]}}");

			String sPlaceholder = "List term(s)";

			oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
				"textbox_man_52.222-35_AltI[0]", "S", 100, // piFillInMaxSize, 
				null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
				psFillInInsertClause);


			html = html.replace(_Fillin_Marker, "{{textbox}}&nbsp;");
			html = EcfrFillInUtils.processTextAndCheck(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
			html = HtmlUtils.parseFillinsAndNormalize(html);

			return html;
		}
	
	private static String process52_247_25 (int iStg1OfficialClauseId, String html, 
			TemplateStg2ClauseFillInRecord oTemplateStg2ClauseFillInRecord, 
			PreparedStatement psFillInInsertClause) throws SQLException {

			html = HtmlUtils.normalizeFillins(html);
			html = html.replaceFirst(Pattern.quote("____  [<i>insert equipment; e.g., forklifts</i>]"), "{{textbox_man_52.247-25[0]}}");

			String sPlaceholder = "insert equipment; e.g., forklifts";

			oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
				"textbox_man_52.247-25[0]", "S", 100, // piFillInMaxSize, 
				null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
				psFillInInsertClause);

			html = html.replaceFirst(Pattern.quote("_ [<i>strike out with</i> or <i>without,</i> as applicable, and insert origin, destination, or both]"), "{{textbox_man_52.247-25[1]}}");

			sPlaceholder = "strike out with or without, as applicable, and insert origin, destination, or both";

			oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
				"textbox_man_52.247-25[1]", "S", 100, // piFillInMaxSize, 
				null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
				psFillInInsertClause);

			html = html.replaceFirst(Pattern.quote("__ [<i>insert loading, unloading, or both</i>]"), "{{textbox_man_52.247-25[2]}}");

			sPlaceholder = "insert loading, unloading, or both";

			oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
				"textbox_man_52.247-25[2]", "S", 100, // piFillInMaxSize, 
				null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
				psFillInInsertClause);


			html = html.replace(_Fillin_Marker, "{{textbox}}&nbsp;");
			html = EcfrFillInUtils.processTextAndCheck(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
			html = HtmlUtils.parseFillinsAndNormalize(html);

			return html;
		}

private static String process52_203_14 (int iStg1OfficialClauseId, String html, 
	TemplateStg2ClauseFillInRecord oTemplateStg2ClauseFillInRecord, 
	PreparedStatement psFillInInsertClause) throws SQLException {

	html = HtmlUtils.normalizeFillins(html);
	
	html = html.replaceFirst(Pattern.quote("<p>____  ____ </p><p>____  ____ </p><p>(<i>Contracting Officer shall insert</i>&mdash;(i) Appropriate agency name(s) and/or title of applicable Department of Homeland Security fraud hotline poster); and</p><p>(ii) The website(s) or other contact information for obtaining the poster(s).)"), 
			"<p>{{textbox_man_52.203-14[0]}}</p>");
	html = html.replaceFirst(Pattern.quote("<i>Poster(s)</i>"), "");
	html = html.replaceFirst(Pattern.quote("<i>Obtain from</i>"), "");
	
	String sPlaceholder = "Contracting Officer shall insert - (i) Appropriate agency name(s) and/or title of applicable Department of Homeland Security fraud hotline poster; and (ii) The website(s) or other contact information for obtaining the poster(s).";

	oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"textbox_man_52.203-14[0]", "M", 1000, // piFillInMaxSize, 
			null, sPlaceholder, 5, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);

	html = html.replace(_Fillin_Marker, "{{textbox}}&nbsp;");
	html = EcfrFillInUtils.processTextAndCheck(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
	html = HtmlUtils.parseFillinsAndNormalize(html);

	return html;
}

private static String process52_212_2 (int iStg1OfficialClauseId, String html, 
	TemplateStg2ClauseFillInRecord oTemplateStg2ClauseFillInRecord, 
	PreparedStatement psFillInInsertClause) throws SQLException {

	html = HtmlUtils.normalizeFillins(html);
	html = html.replaceFirst(Pattern.quote("____  (Contracting Officer state, in accordance with FAR 15.304, the relative importance of all other evaluation factors, when combined, when compared to price.)"), "{{textbox_man_52.212-2[0]}}");

	String sPlaceholder = "Contracting Officer state, in accordance with FAR 15.304, the relative importance of all other evaluation factors, when combined, when compared to price.";

	oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
		"textbox_man_52.212-2[0]", "S", 100, // piFillInMaxSize, 
		null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
		psFillInInsertClause);

	html = html.replaceFirst(Pattern.quote("____  ____ <p>(Contracting Officer shall insert the significant evaluation factors, such as (i) technical capability of the item offered to meet the Government requirement; (ii) price; (iii) past performance (see FAR 15.304); and include them in the relative order of importance of the evaluation factors, such as in descending order of importance.)"), "{{textbox_man_52.212-2[1]}}");

	sPlaceholder = "Contracting Officer shall insert the significant evaluation factors, such as (i) technical capability of the item offered to meet the Government requirement; (ii) price; (iii) past performance (see FAR 15.304); and include them in the relative order of importance of the evaluation factors, such as in descending order of importance.";

	oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
		"textbox_man_52.212-2[1]", "S", 100, // piFillInMaxSize, 
		null, sPlaceholder, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
		psFillInInsertClause);


	html = html.replace(_Fillin_Marker, "{{textbox}}&nbsp;");
	html = EcfrFillInUtils.processTextAndCheck(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
	html = HtmlUtils.parseFillinsAndNormalize(html);

	return html;
}

private static String process52_212_5 (int iStg1OfficialClauseId, String html, 
	TemplateStg2ClauseFillInRecord oTemplateStg2ClauseFillInRecord, 
	PreparedStatement psFillInInsertClause) throws SQLException {

	html = HtmlUtils.normalizeFillins(html);	
	
	html = html.replaceFirst(Pattern.quote("[Contracting Officer check as appropriate.]</p><p>____ (1)"), "</p><p> {{checkbox_man_52.212-5[0]}} (1)");
	oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"checkbox_man_52.212-5[0]", "C", 1, // piFillInMaxSize, 
			1, "Contracting Officer check as appropriate." , 1, 			// piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);

	// Add two groups of checkboxes
	Integer ndx=1;
	Integer iNextGroup = html.indexOf("Contracting Officer check as appropriate");
	Integer iCurrentGroup = html.indexOf("____");
	while ((iCurrentGroup > 0) && (iCurrentGroup < iNextGroup))
	{
		String sCode = "checkbox_man_52.212-5[" + ndx.toString() + "]";
		html = html.replaceFirst(Pattern.quote("____"), "{{" + sCode + "}}&nbsp;(");

		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
				sCode, "C", 1, // piFillInMaxSize,
				1, null, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
				psFillInInsertClause);
		ndx++;
		
		iCurrentGroup = html.indexOf("____");
	}
	
	html = html.replaceFirst(Pattern.quote("[Contracting Officer check as appropriate.]</p><p>____  (1)"), "</p><p> {{checkbox_man2_52.212-5[0]}} (1)");
	oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
			"checkbox_man2_52.212-5[0]", "C", 1, // piFillInMaxSize, 
			2, "Contracting Officer check as appropriate." , 1, 			// piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
			psFillInInsertClause);
	
	while ((iCurrentGroup > 0))
	{
		String sCode = "checkbox_man2_52.212-5[" + ndx.toString() + "]";
		html = html.replaceFirst(Pattern.quote("____"), "{{" + sCode + "}}&nbsp;(");

		oTemplateStg2ClauseFillInRecord.setRegularFillin(html, iStg1OfficialClauseId,
				sCode, "C", 1, // piFillInMaxSize,
				2, null, null, // piFillInGroupNumber, psFillInPlaceholder, piFillInDisplayRows
				psFillInInsertClause);
		ndx++;
		
		iCurrentGroup = html.indexOf("____");
	}
	
	html = html.replaceAll(Pattern.quote("}}&nbsp;(  ("), "}}&nbsp;(");
	html = html.replaceAll(Pattern.quote("}}&nbsp;( ("), "}}&nbsp;(");
		
	html = html.replace(_Fillin_Marker, "{{textbox}}&nbsp;");
	html = EcfrFillInUtils.processTextAndCheck(iStg1OfficialClauseId, html, oTemplateStg2ClauseFillInRecord, psFillInInsertClause);
	html = HtmlUtils.parseFillinsAndNormalize(html);

	return html;
}

	private static String processTextAndCheck(int iStg1OfficialClauseId, String html, TemplateStg2ClauseFillInRecord oTemplateStg2ClauseFillInRecord,
			PreparedStatement psFillInInsertClause) throws SQLException {
		
		html = html.replace("_________", "{{textbox}}");
		html = html.replace("________", "{{textbox}}");
		html = html.replace("_______", "{{textbox}}");
		html = html.replace("______", "{{textbox}}");
		html = html.replace("_____", "{{textbox}}");
		html = html.replace("____", "{{textbox}}");
		html = html.replace("[_]", "{{checkbox}}");
		html = html.replace("&squ;", "{{checkbox}}");
		html = html.replace("&#x2610;", "{{checkbox}}");
		return html;
	}
	
	private static String processTextLines( String html) throws SQLException {
		
		html = html.replace("_________", "____");
		html = html.replace("________", "____");
		html = html.replace("_______", "____");
		html = html.replace("______", "____");
		html = html.replace("_____", "____");
		html = html.replace("____", "____");
		
		return html;
	}
	
	
	// =====================================================
	public static void main( String[] args )
    {
		/*
		try {
			Stg2ClauseFillInsAltRecord oStg2ClauseFillInsAltRecord = new Stg2ClauseFillInsAltRecord();
			String sText = "<p>Alternate II (FEB 2009). As prescribed in 50.206(b)(3), substitute the following paragraph (f):</p><p>(f)(1) Offerors are authorized to submit offers presuming that SAFETY Act designation (or SAFETY Act certification, if a block certification exists) will be obtained before or after award.</p><p>(2) An offeror is eligible for award only if the offeror&mdash;</p><p>(i) Files a SAFETY Act designation (or SAFETY Act certification) application, limited to the scope of the applicable block designation (or block certification), within 15 days after submission of the proposal;</p><p>(ii) Pursues its SAFETY Act designation (or SAFETY Act certification) application in good faith; and</p><p>(iii) Agrees to obtain the amount of insurance DHS requires for issuing the offeror's SAFETY Act designation (or SAFETY Act certification).</p><p>(3) If DHS has not issued a SAFETY Act designation (or SAFETY Act certification) to the successful offeror before contract award, the contracting officer will include the clause at 52.250-5 in the resulting contract.</p>";
			String sHtml = process52_260_3_Alt_II(308, sText, oStg2ClauseFillInsAltRecord, null);
			System.out.println(sHtml);

			sText = "<p>Alternate II (FEB 2009). As prescribed in 50.206(c)(3), substitute the following paragraph (f):</p><p>(f)(1) Offerors are authorized to submit proposals presuming SAFETY Act designation before or after award.</p><p>(2) An offeror is eligible for award only if the offeror&mdash;</p><p>(i) Files a SAFETY Act designation application, limited to the scope of the applicable prequalification designation notice, within 15 days after submission of the proposal;</p><p>(ii) Pursues its SAFETY Act designation application in good faith; and</p><p>(iii) Agrees to obtain the amount of insurance DHS requires for issuing the offeror's SAFETY Act designation.</p><p>(3) If DHS has not issued a SAFETY Act designation to the successful offeror before contract award, the contracting officer will include the clause at 52.250-5 in the resulting contract.</p>";
			sHtml = process52_260_4_Alt_II(308, sText, oStg2ClauseFillInsAltRecord, null);
			System.out.println(sHtml);
		} catch (Exception oError) {
			
		}
		*/
    }
}
