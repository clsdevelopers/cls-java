package hgs.cpm.ecfr;

import hgs.cpm.utils.SqlCommons;

import java.sql.Connection;
import java.sql.SQLException;

public class EcfrManualClause {

	public static void addClauses(Connection conn, Integer srcDocId) throws SQLException {
		String sSql;
		sSql = "INSERT INTO Stg1_Official_Clause (clause_number, title, prescription, uri, content, Content_Html, Content_Html_Fillins, Src_Doc_Id, Manually_Added)\n" +
				"SELECT clause_number, title, prescription, uri, content, Content_Html, Content_Html_Fillins, " + srcDocId + ", 1\n" +
				"FROM Stg_Official_Clause_Add k\n" +
				"WHERE NOT EXISTS ( SELECT 1 FROM Stg1_Official_Clause WHERE Src_Doc_Id = ? and clause_number = k.clause_number )";
		SqlCommons.executeForDocId(conn, sSql, srcDocId);
		System.out.println("AdjustEcfrContent.addManualClauses() #3 using Stg_Official_Clause_Add");
		
		sSql = "INSERT INTO Stg1_Official_Clause_Alt (clause_number, alt_number, instructions, content, Content_Html, Content_Html_Fillins, Src_Doc_Id, Manually_Added)\n" +
				"SELECT clause_number, alt_number, instructions, content, Content_Html, Content_Html_Fillins, " + srcDocId + ", 1\n" +
				"FROM Stg_Official_Clause_Alt_Add k\n" +
				"WHERE NOT EXISTS ( SELECT 1 FROM Stg1_Official_Clause_Alt WHERE Src_Doc_Id = ? and clause_number = k.clause_number and alt_number = k.alt_number )";
		SqlCommons.executeForDocId(conn, sSql, srcDocId);
		System.out.println("AdjustEcfrContent.addManualClauses() #3 using Stg_Official_Clause_Alt_Add");
	}
	
}
