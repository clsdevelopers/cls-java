package hgs.cpm.versioning;

import gov.dod.cls.db.ClauseVersionRecord;
import gov.dod.cls.db.ClauseVersionTable;
import gov.dod.cls.db.PrescriptionTable;
import gov.dod.cls.utils.CommonUtils;
import gov.dod.cls.utils.sql.SqlUtil;
import hgs.cpm.config.CpmConfig;
import hgs.cpm.db.ParsedClauseRecord;
import hgs.cpm.db.ParsedPrescriptionSet;
import hgs.cpm.db.StgSrcDocumentSet;
import hgs.cpm.db.StgVersioningRecord;
import hgs.cpm.utils.SqlCommons;
import hgs.cpm.versioning.util.ClauseEvalSet;
import hgs.cpm.versioning.classes.RegsParams;
import hgs.cpm.versioning.classes.SATParams;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Connection;
import java.util.Date;

public class GenerateVersion {
	
	public static String dateToString(Date pDate) {
		if (pDate == null)
			return "NULL";
		return "'" + ParsedClauseRecord.formatDate.format(pDate) + "'";
	}
	
	public static void run(Integer iPreviousClauseVersionId, Integer iTargetClauseVersionId, String clsVersionName,
			boolean bProduceReport, boolean bProduceClause, boolean bProduceQuestion,
			RegsParams regulationParams,
			SATParams satParams)
    {
		ClauseEvalSet.reportCorrections = false;
		
		CpmConfig cpmConfig = CpmConfig.getInstance();
    	Connection conn = null;
		FileOutputStream fileOutputStream = null;
    	try {
    		conn = cpmConfig.getDbConnection();
    		
    		ClauseVersionRecord oClauseVersionRecord = ClauseVersionTable.getInstance(conn).getRecordById(iTargetClauseVersionId, true, clsVersionName, conn);
    		
    		StgSrcDocumentSet oStgSrcDocumentSet = StgSrcDocumentSet.getLatest(conn);
    		int iEfcrSrcDocId = oStgSrcDocumentSet.iEfcrSrcDocId;
    		// int iQuestionSequenceSrcDocId = oStgSrcDocumentSet.iQuestionSequenceSrcDocId;
    		int iQuestionSrcDocId = oStgSrcDocumentSet.iQuestionSrcDocId;
    		int iClauseSrcDocId = oStgSrcDocumentSet.iClauseSrcDocId;
    		
    		System.out.println("SrcDocId: ECFR(" + iEfcrSrcDocId + "), Clause(" + iClauseSrcDocId + "), Question(" + iQuestionSrcDocId + ")");

    		PrescriptionTable oPrescriptionTable = PrescriptionTable.getInstance(conn);
    		
    		ParsedPrescriptionSet parsedPrescriptionSet = new ParsedPrescriptionSet();
    		parsedPrescriptionSet.load(conn, iQuestionSrcDocId, iClauseSrcDocId);

    		String sFileName = ParsedClauseRecord.formatDate.format(CommonUtils.getNow()) + "-"
    				+ "cls.v." + iTargetClauseVersionId
    				+ (iPreviousClauseVersionId == null ? "" : "(v." + iPreviousClauseVersionId + ")")
    				+ "."
    				+ ((bProduceClause && bProduceQuestion) ? "clause-question" : (bProduceClause ? "clause" : "question"))
    				+ "." + (bProduceReport ? "html" : "sql"); // ".clause-question.sql"; 
    		String sFilePath = cpmConfig.getSqlOutputPath() + "/" + sFileName;
    		
			File file = new File(sFilePath);
			if (file.exists()) {
				file.delete();
				System.out.println("Deleted " + sFilePath);
			}
			file.createNewFile();
			fileOutputStream = new FileOutputStream(file);
			{
				String sLog = (bProduceReport ? "<h1>" : "/*\n") + "Clause Parser Run at " + CommonUtils.getNow() + (bProduceReport ? "</h1>" : "") + "\n";
				sLog += (bProduceReport ? "<h3><i>" : "") + (ClauseEvalSet.reportCorrections ? "(With" : "(Without") + " Correction Information)" + (bProduceReport ? "</i></h3>" : "");  
				if (!bProduceReport) {
					sLog += "\n*/\n";
				}
				if ((!bProduceReport) && (!iTargetClauseVersionId.equals(iPreviousClauseVersionId))) {
					sLog += GenerateVersion.generateRemoveVersionSql(oClauseVersionRecord, clsVersionName);
				}
				fileOutputStream.write(sLog.getBytes());
			}
 
    		ProduceQuestion populateQuestion = new ProduceQuestion(conn, iTargetClauseVersionId, 
    				iPreviousClauseVersionId, iQuestionSrcDocId, parsedPrescriptionSet, oPrescriptionTable);
    		populateQuestion.loadParsedRecord();
			if (bProduceQuestion) {
				populateQuestion.generateSql(fileOutputStream, bProduceReport); // this.storeSql();
	    		//populateQuestion.run(fileOutputStream, bProduceReport);
				populateQuestion = new ProduceQuestion(conn, iTargetClauseVersionId, 
	    				iPreviousClauseVersionId, iQuestionSrcDocId, parsedPrescriptionSet, oPrescriptionTable);
	    		populateQuestion.loadParsedRecord();
			}

			if (bProduceClause) {
	    		ProduceClause parsedClauseSet = new ProduceClause(iEfcrSrcDocId, iClauseSrcDocId, 
	    				iTargetClauseVersionId, iPreviousClauseVersionId, parsedPrescriptionSet);
	    		parsedClauseSet.run(populateQuestion, conn, fileOutputStream, bProduceReport);
			}
			
			//System.out.println("Fac Date:" + dateToString(oClauseVersionRecord.getFacDate()) + " vs. " + dateToString(dtFac));
			//System.out.println("Dac Date:" + dateToString(oClauseVersionRecord.getDacDate()) + " vs. " + dateToString(dtDac));
			//System.out.println("Dfars Date:" + dateToString(oClauseVersionRecord.getDFarsDate()) + " vs. " + dateToString(dtDFars));
			
			String sLog;
			if (regulationParams.bUpdateReq(oClauseVersionRecord))
			{
				sLog = "\nUPDATE Clause_Versions SET FAC_Number = " + SqlCommons.quoteString(regulationParams.sFacNumber) + 
						", DAC_Number = " + SqlCommons.quoteString(regulationParams.sDacNumber) + 
						", DFARS_Number = " + SqlCommons.quoteString(regulationParams.sDFarsNumber) + 
						",\n FAC_Date = " + dateToString(regulationParams.facDate) + 
						", DAC_Date = " + dateToString(regulationParams.dacDate) + 
						", DFARS_Date = " + dateToString(regulationParams.dfarsDate) + 
						"\n WHERE Clause_Version_Id = " + iTargetClauseVersionId + ";\n";
				fileOutputStream.write(sLog.getBytes());
			}
			// CJ-1360	
			if (satParams.bUpdateReq(oClauseVersionRecord))
			{
				sLog = "\nUPDATE Clause_Versions SET SAT_Conditions = " + SqlCommons.quoteString(satParams.satCondition) + 
						",\n SAT_300K_Rule = " + SqlCommons.quoteString(satParams.sat300KRule) + 
						",\n SAT_1Million_Rule = " + SqlCommons.quoteString(satParams.sat1MillionRule) + 
						"\n WHERE Clause_Version_Id = " + iTargetClauseVersionId + ";\n";
				fileOutputStream.write(sLog.getBytes());
			}
			
			// update audit logs.
			sLog = "\nDELETE FROM Audit_Events WHERE Created_At < (NOW() - INTERVAL 30 DAY);";
			fileOutputStream.write(sLog.getBytes());
			
			if ((!bProduceReport) && (!iTargetClauseVersionId.equals(iPreviousClauseVersionId))) {
				sLog = "\nUPDATE Clause_Versions SET Is_Active = IF (Clause_Version_Id = " + iTargetClauseVersionId + ", 1, 0);\n";
				fileOutputStream.write(sLog.getBytes());
			}
			
			sLog = "\n" + (bProduceReport ? "<h3>" : "/*\n") + "*** End of SQL Scripts at " + CommonUtils.getNow() + " ***" + (bProduceReport ? "</h3>" : "\n*/") + "\n";
			fileOutputStream.write(sLog.getBytes());
			
    		fileOutputStream.flush();
			fileOutputStream.close();
			
			if ((!bProduceReport) && ((iPreviousClauseVersionId == null) || (!iTargetClauseVersionId.equals(iPreviousClauseVersionId))))
				StgVersioningRecord.save(conn, oStgSrcDocumentSet, iTargetClauseVersionId, sFileName);
			
			System.out.println("*******************************************************************");
			System.out.println("Generated " + sFilePath);
			System.out.println("*******************************************************************");
    	}
    	catch (Exception oError) {
    		oError.printStackTrace();
    	}
    	finally {
    		SqlUtil.closeInstance(conn);
			try {
				if (fileOutputStream != null) {
					fileOutputStream.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
    	}
    }
	
	private static String generateRemoveVersionSql(ClauseVersionRecord oClauseVersionRecord, String clsVersionName) {
		if (clsVersionName == null)
			clsVersionName = oClauseVersionRecord.getName(); 
		int piTargetVersion = oClauseVersionRecord.getId();
		String sSql = 
				"DELETE FROM Document_Clauses\n" +
				"WHERE Document_Id in\n" +
				"(SELECt Document_Id FROM Documents WHERE Clause_Version_Id = " + piTargetVersion + ");\n" +
				"\n" +
				"DELETE FROM Document_Fill_Ins\n" +
				"WHERE Document_Id in\n" +
				"(SELECt Document_Id FROM Documents WHERE Clause_Version_Id = " + piTargetVersion + ");\n" +
				"\n" +
				"DELETE FROM Document_Answers\n" +
				"WHERE Document_Id in\n" +
				"(SELECt Document_Id FROM Documents WHERE Clause_Version_Id = " + piTargetVersion + ");\n" +
				"\n" +
				"DELETE FROM Documents WHERE Clause_Version_Id = " + piTargetVersion + " AND Solicitation_Id IS NOT NULL;\n" +
				"DELETE FROM Documents WHERE Clause_Version_Id = " + piTargetVersion + ";\n" +
				"\n" +
				"DELETE FROM Clause_Prescriptions\n" +
				"WHERE Clause_Id in\n" +
				"(SELECt Clause_Id FROM Clauses WHERE Clause_Version_Id = " + piTargetVersion + ");\n" +
				"\n" +
				"DELETE FROM Clause_Prescriptions\n" +
				"WHERE Clause_Id in\n" +
				"(SELECt Clause_Id FROM Clauses WHERE Clause_Version_Id = " + piTargetVersion + ");\n" +
				"\n" +
				"DELETE FROM Clauses WHERE Clause_Version_Id = " + piTargetVersion + ";\n" +
				"\n" +
				"DELETE FROM Question_Choice_Prescriptions\n" +
				"WHERE Question_Choce_Id in\n" +
				"(SELECT B.Question_Choce_Id FROM Questions A INNER JOIN Question_Choices B ON (B.Question_Id = A.Question_Id) WHERE A.Clause_Version_Id = " + piTargetVersion + ");\n" +
				"\n" +
				"DELETE FROM Question_Choices\n" +
				"WHERE Question_Id in\n" +
				"(SELECT A.Question_Id FROM Questions A WHERE A.Clause_Version_Id = " + piTargetVersion + ");\n" +
				"\n" +
				"DELETE FROM Question_Conditions\n" +
				"WHERE Question_Id in\n" +
				"(SELECT A.Question_Id FROM Questions A WHERE A.Clause_Version_Id = " + piTargetVersion + ");\n" +
				"\n" +
				"DELETE FROM Questions\n" +
				"WHERE Clause_Version_Id = " + piTargetVersion + ";\n" +
				"\n" +
				"DELETE FROM Clauses\n" +
				"WHERE clause_version_id = " + piTargetVersion + ";\n" +
				"\n" +
				"DELETE FROM Clause_Version_Changes\n" +
				"WHERE Clause_Version_Id = " + piTargetVersion + ";\n" +
		
				"INSERT INTO Clause_Versions (Clause_Version_Id, Clause_Version_Name, Clause_Version_Date, Is_Active)\n" +
				"VALUES (" + piTargetVersion + ", " + SqlUtil.quoteString(clsVersionName) + ", CURRENT_TIMESTAMP, false)\n" +
				"ON DUPLICATE KEY\n" +
				"UPDATE Clause_Version_Name = " + SqlUtil.quoteString(clsVersionName) + ", Clause_Version_Date = CURRENT_TIMESTAMP;\n";

		return sSql;
	}
	
	// ===================================================================
	public static void main( String[] args ) {
		Integer iPreviousClauseVersionId = 6; // cpmConfig.getVersionIdPreviousInteger();
		Integer iTargetClauseVersionId = 7; // cpmConfig.getVersionIdTargetInteger();
		
		boolean bProduceReport = false;
		boolean bProduceClause = true;
		boolean bProduceQuestion = true;
		
		ParsedPrescriptionSet.generateSql = false;

		GenerateVersion.run(iPreviousClauseVersionId, iTargetClauseVersionId, "3.8", bProduceReport, bProduceClause, bProduceQuestion, 
				null, 	// Regs Params (FAC/DAC/DFARS)
				null);	// SAT Object
	}
	
}

