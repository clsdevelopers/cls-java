package hgs.cpm.versioning.util;

import gov.dod.cls.utils.CommonUtils;
import hgs.cpm.db.ParsedClauseRecord;
import hgs.cpm.versioning.ProduceClause;
import hgs.cpm.versioning.ProduceQuestion;

import java.util.ArrayList;

public class ClauseEvalSet extends ArrayList<ClauseEvalItem> {

	private static final long serialVersionUID = 2883725619140582337L;
	
	public static boolean reportCorrections = true;
	
	public String rule; // 12859	(A || B) && C IF THIS CLAUSE APPLIES, USE IT INSTEAD OF FAR 52.232-23
	public boolean isExpressionValid = false;
	public ParsedClauseRecord clause;

	public String parseError;
	
	public boolean changed = false;
	
	public String parseConditions(ParsedClauseRecord oClauseItem, ProduceQuestion questionSet, ProduceClause clauseSet) {
		this.parseError = null;
		this.clause = oClauseItem;
		this.clear(); // conditions, rule
		
		String parseError = "";
		if ("52.215-23".equals(oClauseItem.clauseName))
			parseError = "";
		
		String conditions = oClauseItem.clauseConditions.replaceAll("\n", " ");

		String sLine; int iPos;
		ArrayList<String> aLines = new ArrayList<String>();
		
		String[] aTemp = conditions.split(" \\(");
		for (int iLine = 0; iLine < aTemp.length; iLine++) {
			sLine = aTemp[iLine];
			if (iLine == 0) {
				aLines.add(sLine);
			} else {
				iPos = sLine.indexOf(')');
				char c = sLine.charAt(0);
				if ((iPos < 0) || (iPos > 2) || (c < 'A') || (c > 'Z')) {
				//if ((sLine.length >= 3) && (')' != sLine.charAt(1)) && (')' != sLine.charAt(2))) {
					int index = aLines.size() - 1;
					aLines.set(index, aLines.get(index) + " (" + sLine);  
				} else
					aLines.add(sLine);
			}
		}
		
		this.isExpressionValid = true;
		ClauseEvalItem oItem = null;
		for (int iLine = 0; iLine < aLines.size(); iLine++) {
			sLine = aLines.get(iLine).trim();
			if (!sLine.isEmpty()) {
				iPos = sLine.indexOf(')');
				if ((iPos > 3) && (oItem != null)) {
					oItem.value += " (" + sLine;
				} else {
					if (sLine.charAt(0) != '(')
						sLine = '(' + sLine;
					oItem = new ClauseEvalItem();
					String error = oItem.parseLine(sLine, questionSet, clauseSet);
					if (oItem.corrected) {
						if (!error.isEmpty())
							error += "<br>\n";
						if (reportCorrections)
							error += "<table>\n<tr><th>Original</th><td>" + oItem.originalExpression + "</td></tr>\n" +
								"<tr><th>Correct</th><td>" + oItem.changedExpression + "</td></tr></table>\n";
						this.changed = true;
					} else if (!error.isEmpty())
						this.isExpressionValid = false;

					if (!error.isEmpty()) {
						if (!parseError.isEmpty())
							parseError += "<br />\n";
						parseError += error;
					}
					this.add(oItem);
				}
			}
		}
		
		if (CommonUtils.isNotEmpty(oClauseItem.clauseRule)) {
			String rule = oClauseItem.clauseRule;
			iPos = rule.indexOf("ADDITIONAL INSTRUCTION");
			if (iPos > 0)
				rule = rule.substring(0, iPos);
			iPos = rule.indexOf("\n");
			if (iPos > 0)
				rule = rule.substring(0, iPos);
			iPos = rule.indexOf("\\n");
			if (iPos > 0)
				rule = rule.substring(0, iPos);
			iPos = rule.indexOf(" IF THIS ");
			if (iPos > 0)
				rule = rule.substring(0, iPos);
			this.rule = rule; // .replace(/OR/g, "||").replace(/AND/g, "&&");

			/*
			this.conditions.sort(this.itemSorting);
			var sTestRuleError = this.testRule();
			if (sTestRuleError)
				parseError += (parseError ? "<br />\n" : "") + sTestRuleError;
			*/
		} else {
			if (!parseError.isEmpty())
				parseError += "<br />";
			parseError += "Empty Rule";
		}
		
		if (this.changed) {
			String sNewExpression = "";
			for (ClauseEvalItem oEvalItem : this) {
				if (sNewExpression.isEmpty())
					sNewExpression = oEvalItem.getExpression();
				else
					sNewExpression += "\\n" + oEvalItem.getExpression();
			}
			//System.out.println(oClauseItem.clauseId + "[" + oClauseItem.clauseName + "] " + (oClauseItem.clauseConditions.equals(sNewExpression) ? "SAME" : "Differnt"));
			//System.out.println("Old: " + oClauseItem.clauseConditions);
			oClauseItem.clauseConditions = sNewExpression; 
			//System.out.println("New: " + oClauseItem.clauseConditions);
		}
		if (parseError == "") {
			//if (SHOW_C_NO_ERROR == true)
			//	logOnConsole("\n<br /><strong>Clause</strong> (id: " + oClauseItem.id + ", Name: " + oClauseItem.name + "): NO ERROR");
		} else {
			//if (SHOW_C_ERROR == true)
			//	logOnConsole("\n<br /><strong>Clause</strong> (id: " + oClauseItem.id + ", Name: " + oClauseItem.name + "): "
			//			+ parseError
			//			+ "\n<pre>" + oClauseItem.conditions + "</pre>\n");
		}
		this.parseError = parseError;
		return parseError;
	};
	
	public static void merge(ParsedClauseRecord poRecord, ParsedClauseRecord poDupe) {
		if (poDupe == null)
			return;
		
		ClauseEvalSet poEvalSet = poRecord.oClauseEvalSet;
		ClauseEvalSet poDupeEvalSet = poDupe.oClauseEvalSet;
		if ((poDupeEvalSet == null) && (poEvalSet == null))
			return;
		
		if (CommonUtils.isEmpty(poRecord.additionalConditions) && CommonUtils.isNotEmpty(poDupe.additionalConditions)) {
			poRecord.additionalConditions = poDupe.additionalConditions;
			poDupe.additionalConditions = "";
			poRecord.oClauseEvalSet = poDupeEvalSet;
			poDupe.oClauseEvalSet = poEvalSet;
			poEvalSet = poRecord.oClauseEvalSet;
			poDupeEvalSet = poDupe.oClauseEvalSet;
		}
		
		poDupeEvalSet.rule = poDupeEvalSet.rule.toLowerCase().replaceAll(" and ", " AND ").replaceAll(" or ", " OR ");
		ArrayList<ClauseEvalItem> aNotProcessesDupes = new ArrayList<ClauseEvalItem>();
		for (ClauseEvalItem oDupeItem : poDupeEvalSet) {
			String sDupeCode = oDupeItem.code.toLowerCase();
			ClauseEvalItem oSameItem = poEvalSet.findSameCondition(oDupeItem);
			if (oSameItem != null) {
				poDupeEvalSet.rule = poDupeEvalSet.rule.replaceAll(sDupeCode, oSameItem.code);
			} else {
				oDupeItem.code = sDupeCode;
				aNotProcessesDupes.add(oDupeItem);
			}
		}
		
		//String sLastCode = this.getLastCode();
		String sDupeNewConditions = "";
		for (ClauseEvalItem oDupeItem : aNotProcessesDupes) {
			String sOrigDupeCode = oDupeItem.code;
			String sNewDupeCode = sOrigDupeCode.toUpperCase();
			if (poEvalSet.findByCode(sNewDupeCode) != null)
				sNewDupeCode += "2";
			poDupeEvalSet.rule = poDupeEvalSet.rule.replaceAll(sOrigDupeCode, sNewDupeCode);
			oDupeItem.code = sNewDupeCode;
			String sGenerateExpression = oDupeItem.generateExpression();
			sDupeNewConditions += "\\n" + sGenerateExpression; 
		}
		
		boolean bChanged = false;
		System.out.println("[" + poRecord.clauseName + "] Merged Condition/Rule ----------------");
		String sOrignalConditions = poRecord.clauseConditions;
		if (!sDupeNewConditions.isEmpty()) {
				poRecord.clauseConditions += sDupeNewConditions;	
			if (ClauseEvalSet.reportCorrections) {
				System.out.println("Conditions from:\n" + sOrignalConditions);
				System.out.println("to:\n" + poRecord.clauseConditions);
			}
			bChanged = true;
		}

		String sOriginalRule = poRecord.clauseRule;
		if (!sOriginalRule.equals(poDupeEvalSet.rule)) {
				poRecord.clauseRule = "(" + poRecord.clauseRule + ") OR (" + poDupeEvalSet.rule + ")"; 
			if (ClauseEvalSet.reportCorrections) {
				System.out.println("Rule from:\n" + sOriginalRule);
				System.out.println("to:\n" + poRecord.clauseRule);
			}
		} else if (!bChanged) {
			if (ClauseEvalSet.reportCorrections) {
				System.out.println("no changes");
			}
		}
	}
	
	private ClauseEvalItem findSameCondition(ClauseEvalItem oDupeItem) {
		for (ClauseEvalItem oItem : this) {
			if (oItem.isSameCondition(oDupeItem))
				return oItem;
		}
		return null;
	}
	
	private ClauseEvalItem findByCode(String psCode) {
		for (ClauseEvalItem oItem : this) {
			if (oItem.code.equals(psCode))
				return oItem;
		}
		return null;
	}
	
	/*
	private String getLastCode() {
		String result = "";
		for (ClauseEvalItem oItem : this) {
			String sCode = oItem.code;
			if (result.isEmpty())
				result = sCode;
			else {
				if (sCode.compareTo(result) > 0)
					result = sCode;
			}
		}
		return result;
	}
	*/
}
