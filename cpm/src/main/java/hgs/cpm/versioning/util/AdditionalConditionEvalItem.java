package hgs.cpm.versioning.util;

import gov.dod.cls.utils.CommonUtils;
import hgs.cpm.db.ParsedClauseRecord;
import hgs.cpm.db.ParsedQuestionRecord;

import java.util.ArrayList;

public class AdditionalConditionEvalItem {

	public static final int  OPR_DO_NOT_INCLUDE = 1;
	public static final int  OPR_INCLUDE = 2;
	public static final int  OPR_REPLACE_WITH = 3;

	public String rule = null;
	public ParsedQuestionRecord question = null;
	public String answer = null;
	public boolean notAnswer = false;
	
	public Integer oprType = null;
	public ArrayList<ParsedClauseRecord> andClauses = null;
	public ArrayList<ParsedClauseRecord> orClauses = null;

	public ArrayList<ParsedClauseRecord> oprClauses = null;
	
	public void addAndClause(ParsedClauseRecord oClause) {
		if (this.andClauses == null)
			this.andClauses = new ArrayList<ParsedClauseRecord>();
		this.andClauses.add(oClause);
	}

	public void addAndClauseSet(ArrayList<ParsedClauseRecord> aClauses) {
		for (int iItem = 0; iItem < aClauses.size(); iItem++) {
			this.addAndClause(aClauses.get(iItem));
		}
	}

	public void addOrClause(ParsedClauseRecord oClause) {
		if (this.orClauses == null)
			this.orClauses = new ArrayList<ParsedClauseRecord>();
		this.orClauses.add(oClause);
	}

	public void addOrClauseSet(ArrayList<ParsedClauseRecord> aClauses) {
		for (int iItem = 0; iItem < aClauses.size(); iItem++) {
			this.addOrClause(aClauses.get(iItem));
		}
	}

	/*
	public void evaluate(Object pClause, Object oFinalCheckSet, Object oPrescriptionChoices) {
		var okToGo;
		var aSelectedClauses = oPrescriptionChoices.clauses;
		if (this.rule) {
			if (pClause.oEvaluation == null)
				return false;
			var evalRule = this.rule;
			okToGo = pClause.oEvaluation.evalRule(aSelectedClauses, evalRule);
			if (okToGo == false)
				return false;
		}
		if (this.question) {
			var aAnswerValues = this.question.getAnswerValues();
			if (! aAnswerValues)
				return false;
			okToGo = (aAnswerValues.indexOf(this.answer) >= 0);
			if (this.notAnswer)
				okToGo = !okToGo;
			if (okToGo == false)
				return false;
		}

		var oClause;
		if (this.andClauses) {
			for (var item = 0; item < this.andClauses.length; item++) {
				oClause = this.andClauses[item];
				if (aSelectedClauses.indexOf(oClause) < 0)
					return false;
				if (oClause.isApplicable(oPrescriptionChoices, false) == false)
					return false;
			}
		}

		okToGo = false;
		if (this.orClauses) {
			for (var item = 0; item < this.orClauses.length; item++) {
				oClause = this.orClauses[item];
				if (aSelectedClauses.indexOf(oClause) >= 0) {
					if (oClause.isApplicable(oPrescriptionChoices, false))
						okToGo = true;
				}
			}
			if (okToGo == false)
				return false;
		}
		if (this.oprClauses) {
			var aApplied = new Array();
			switch (this.oprType) {
				case OPR_DO_NOT_INCLUDE:
					for (var item = 0; item < this.oprClauses.length; item++) {
						oClause = this.oprClauses[item];
						if (oPrescriptionChoices.hasClause(oClause)) {
							oPrescriptionChoices.updateCrossoverClause(oClause, false);
							oFinalCheckSet.addItem(oClause, false);
							oClause.show(false, oPrescriptionChoices);
						}
					}
					break;
				case OPR_INCLUDE:
					for (var item = 0; item < this.oprClauses.length; item++) {
						oClause = this.oprClauses[item];
						if (oPrescriptionChoices.hasClause(oClause) == false) {
							oPrescriptionChoices.updateCrossoverClause(oClause, true);
							oFinalCheckSet.addItem(oClause, true);
							oClause.show(true, oPrescriptionChoices);
						}
					}
					break;
				case OPR_REPLACE_WITH:
					if (oPrescriptionChoices.hasClause(pClause)) {
						oPrescriptionChoices.updateCrossoverClause(pClause, false);
						oFinalCheckSet.addItem(pClause, false);
						pClause.show(false, oPrescriptionChoices);
					}
					for (var item = 0; item < this.oprClauses.length; item++) {
						oClause = this.oprClauses[item];
						if (oPrescriptionChoices.hasClause(oClause) == false) {
							oPrescriptionChoices.updateCrossoverClause(oClause, true);
							oFinalCheckSet.addItem(oClause, true);
							oClause.show(true, oPrescriptionChoices);
						}
					}
					break;
			}
		}
	};
	*/
	
	public String debugHtml() {
		String sCondition = "";
		if (CommonUtils.isNotEmpty(this.rule)) {
			sCondition = this.rule;
		}
		if (this.question != null) {
			sCondition += (CommonUtils.isNotEmpty(sCondition) ? " AND " : "") +
					"[" + this.question.questionCode + "] IS: " + (this.notAnswer ? "NOT " : "") +
					"\"" + this.answer + "\"";
		}
		String sConditionClauses = "";
		if (this.andClauses != null) {
			sConditionClauses = "(AND) ";
			for (int item = 0; item < this.andClauses.size(); item++) {
				sConditionClauses += (item > 0 ? ", " : "") + this.andClauses.get(item).clauseName;
			}
		}
		if (this.orClauses != null) {
			sConditionClauses += " (OR) ";
			for (int item = 0; item < this.orClauses.size(); item++) {
				sConditionClauses += (item > 0 ? ", " : "") + this.orClauses.get(item).clauseName;
			}
		}
		
		String sOperator;
		switch (this.oprType) {
			case OPR_DO_NOT_INCLUDE:
				sOperator = "DO NOT INCLUDE";
				break;
			case OPR_INCLUDE:
				sOperator = "INCLUDE";
				break;
			case OPR_REPLACE_WITH:
				sOperator = "REPLACE WITH";
				break;
			 default:
				sOperator = "[null]";
		}
		
		String sOprClauses = "";
		if (this.oprClauses != null) {
			for (int item = 0; item < this.oprClauses.size(); item++) {
				sOprClauses += (item > 0 ? ", " : "") + this.oprClauses.get(item).clauseName;
			}
		}
		String sResult = "IF ";
		if (CommonUtils.isNotEmpty(sConditionClauses))
			sResult += sConditionClauses + " ADDED";
		if (CommonUtils.isNotEmpty(sCondition))
			if (CommonUtils.isNotEmpty(sConditionClauses))
				sResult += " AND " + sCondition;
			else
				sResult += sCondition;
		sResult += ", " + sOperator + " " + sOprClauses;
		return sResult;
	};

}
