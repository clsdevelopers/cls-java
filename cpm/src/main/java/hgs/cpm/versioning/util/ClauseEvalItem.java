package hgs.cpm.versioning.util;

import java.util.ArrayList;

import gov.dod.cls.utils.CommonUtils;
import hgs.cpm.db.ParsedClauseRecord;
import hgs.cpm.db.ParsedQuestionChoiceRecord;
import hgs.cpm.db.ParsedQuestionChoiceSet;
import hgs.cpm.db.ParsedQuestionRecord;
import hgs.cpm.versioning.ProduceClause;
import hgs.cpm.versioning.ProduceQuestion;

public class ClauseEvalItem {

	public static final int OPR_IS = 0;
	public static final int OPR_INCLUDED = 1;
	public static final int OPR_LESS_THAN_OR_EQUAL_TO = 2;
	public static final int OPR_GREATER_THAN = 3;
	public static final int OPR_LESS_THAN = 4;
	public static final int OPR_GREATER_THAN_OR_EQUAL_TO = 5;
	public static final int OPR_EQUAL_TO = 6;

	public static final String GREATER_THAN_OR_EQUAL_TO = "GREATER THAN OR EQUAL TO ";
	public static final String LESS_THAN_OR_EQUAL_TO = "LESS THAN OR EQUAL TO ";
	public static final String GREATER_THAN = "GREATER THAN ";
	public static final String LESS_THAN = "LESS THAN ";
	public static final String EQUAL_TO = "EQUAL TO ";

	public static final String SAT = "SAT";
	
	public String code = null;
	public String name = null;
	public String value = null;
	public Integer iValue = null;
	public boolean not = false;
	public int oprType = OPR_IS;
	public ParsedQuestionRecord oQuestion = null;
	public ParsedQuestionRecord oValueQuestion = null;
	public ParsedClauseRecord oClause = null;
	
	public String originalExpression;
	public String changedExpression = null;
	public boolean corrected = false;
	
	public static boolean isNumber(String str) {
        return str.matches("^[0-9]+$");
    }
	
	public boolean isSameCondition(ClauseEvalItem poDupeItem) {
		if (CommonUtils.isNotSame(this.name, poDupeItem.name))
			return false;
		if (this.not != poDupeItem.not)
			return false;
		if (this.oprType != poDupeItem.oprType)
			return false;
		if (CommonUtils.isNotSame(this.oQuestion, poDupeItem.oQuestion))
			return false;
		if (CommonUtils.isNotSame(this.oValueQuestion, poDupeItem.oValueQuestion))
			return false;
		if (CommonUtils.isNotSame(this.oClause, poDupeItem.oClause))
			return false;
		if (CommonUtils.isNotSame(this.value, poDupeItem.value))
			return false;
		
		return true;
	}
	
	public String parseLine(String conditionLine, ProduceQuestion questionSet, ProduceClause clauseSet) {
		String error = "";
		conditionLine = conditionLine.trim();
		this.originalExpression = conditionLine;
		
		if (conditionLine.isEmpty())
			return "Empty";
		int iPos = conditionLine.indexOf(")");
		if (iPos < 2)
			return "No identifier\n<pre>" + this.originalExpression + "</pre>";

		if (conditionLine.contains("GREATER OR EQUAL TO ")) {
			this.changedExpression = conditionLine.replace("GREATER OR EQUAL TO ", GREATER_THAN_OR_EQUAL_TO);
			conditionLine = changedExpression;
		} else if (conditionLine.contains("GREATER THAN ORQUAL TO ")) {
			this.changedExpression = conditionLine.replace("GREATER THAN ORQUAL TO ", GREATER_THAN_OR_EQUAL_TO);
			conditionLine = changedExpression;
		}
		
		this.code = conditionLine.substring(1, iPos);
		String rest = conditionLine.substring(iPos + 1).trim();
		String[] aValues = rest.split(" IS:");
		String sIsCorrected = "";
		if (aValues.length != 2) {
			if (aValues.length > 2)
				return "(" + this.code + ") More than one \" IS:\" found\n<pre>" + this.originalExpression + "</pre>";
			aValues = rest.split(" IS :");
			if (aValues.length != 2) {
				error = "(" + this.code + ") Not \" IS:\" syntax";
				aValues = rest.split(":");
				if (aValues.length != 2) {
					return "(" + this.code + ") No \" IS:\" found\n<pre>" + this.originalExpression + "</pre>";
				}
				sIsCorrected = error;
			}
		}
		this.name = aValues[0].trim();
		this.value = aValues[1].trim();
		if (this.value.startsWith("NOT ")) { // if (this.value.substr(0, 4) == "NOT ") {
			this.value = this.value.substring(4);
			this.not = true;
		}
		
		if (this.value.startsWith(GREATER_THAN_OR_EQUAL_TO)) { // (this.value.substring(0, GREATER_THAN_OR_EQUAL_TO.length()) == GREATER_THAN_OR_EQUAL_TO) {
			this.value = this.value.substring(GREATER_THAN_OR_EQUAL_TO.length());
			this.oprType = OPR_GREATER_THAN_OR_EQUAL_TO;
		} else
		if (this.value.startsWith(LESS_THAN_OR_EQUAL_TO)) { // (this.value.substring(0, LESS_THAN_OR_EQUAL_TO.length()) == LESS_THAN_OR_EQUAL_TO) {
			this.value = this.value.substring(LESS_THAN_OR_EQUAL_TO.length());
			this.oprType = OPR_LESS_THAN_OR_EQUAL_TO;
		} else
		if (this.value.startsWith(GREATER_THAN)) { // (this.value.substring(0, GREATER_THAN.length()) == GREATER_THAN) {
			this.value = this.value.substring(GREATER_THAN.length());
			this.oprType = OPR_GREATER_THAN;
		} else
		if (this.value.startsWith(LESS_THAN)) { // (this.value.substring(0, LESS_THAN.length()) == LESS_THAN) {
			this.value = this.value.substring(LESS_THAN.length());
			this.oprType = OPR_LESS_THAN;
		} else
		if (this.value.startsWith(EQUAL_TO)) { // (this.value.substring(0, LESS_THAN.length()) == LESS_THAN) {
			this.value = this.value.substring(EQUAL_TO.length());
			this.oprType = OPR_EQUAL_TO;
		} else
		if ("INCLUDED".equals(this.value))
			this.oprType = OPR_INCLUDED;

		if (this.oprType == OPR_INCLUDED) {
			if (CommonUtils.isNotEmpty(this.name)) {
				this.name = this.name.toUpperCase();
				this.oClause = clauseSet.findByName(this.name);
				if (this.oClause == null) {
					error += ((error == "") ? "" : "<br />") + "(" + this.code + ") Clause name not found: {" + this.name + "}";
				}
			} else {
				error += ((error == "") ? "" : "<br />") + "(" + this.code + ") Clause name is empty";
			}
		} else {
			this.oQuestion = questionSet.findByName("[" + this.name + "]");
			if (this.oQuestion != null)
				if ((this.oprType == OPR_LESS_THAN_OR_EQUAL_TO) || 
					(this.oprType == OPR_GREATER_THAN_OR_EQUAL_TO) || 
					(this.oprType == OPR_GREATER_THAN) || 
					(this.oprType == OPR_LESS_THAN) || 
					(this.oprType == OPR_EQUAL_TO)) {
					
					if (CommonUtils.isNotEmpty(this.value))
						this.value = this.value.replaceAll(",", ""); 
					if (CommonUtils.isEmpty(this.value)) {
						error += ((error == "") ? "" : "; ") + "(" + this.code + ") target value is empty";
					}
					else if (! isNumber(this.value)) {
						if (!SAT.equals(this.value)) {
							this.oValueQuestion = questionSet.findByName("[" + this.value + "]");
							if (this.oValueQuestion == null) {
								error += ((error == "") ? "" : "; ") 
									+ "(" + this.code + ") Value Question Name not found: [" + this.value + "]";
							}
						}
					} else {
						this.iValue = Integer.parseInt(this.value);
					}
					if (error.isEmpty()) {
						if ((this.changedExpression != null) && (!this.changedExpression.equals(this.originalExpression))) {
							this.corrected = true;
						}
						return "";
					}
					return error + "\n<pre>" + this.originalExpression + "</pre>";
				}
			
			if (this.oQuestion == null) {
				if (CommonUtils.isNotEmpty(this.value)) {
					error = this.findQuestion(questionSet);
					/*
					ArrayList<ParsedQuestionRecord> aQuestions = new ArrayList<ParsedQuestionRecord>();
					if ((questionSet.findByChoiceValue(this.value, aQuestions) == false) && (this.not == true)) {
						String notValue = "NOT " + this.value;
						if (questionSet.findByChoiceValue(notValue, aQuestions)) {
							this.value = notValue;
							this.not = false;
						}
					}
					if (aQuestions.size() > 0) {
						if (aQuestions.size() == 1) {
							this.oQuestion = aQuestions.get(0);
							this.changedExpression = "(" + this.code + ") " + this.oQuestion.getNameText() + " IS: " + (this.not ? "NOT " : "") + this.value;
							this.corrected = true;
							//System.out.println(" Original: " + this.originalExpression);
							//System.out.println("Changed: " + this.changedExpression);
						} else {
							error = ((error == "") ? "" : "; ") + "(" + this.code + ") [" + this.name + "]" + "Found multiple questions for choice value of \"" + this.value + "\":<br>\n";
							for (ParsedQuestionRecord oQuestion: aQuestions) {
								error += " " + oQuestion.questionCode;
							}
						}
					} else {
						error = ((error == "") ? "" : "; ") + "(" + this.code + ") [" + this.name + "]" + " Question Name not found";
					}
					*/
				} else {
					error += ((error == "") ? "" : "; ") + "(" + this.code + ") [" + this.name + "]" + " Question Name not found and not target value";
				}
			}
			else if (CommonUtils.isNotEmpty(this.value)) {
				String debug = this.findValue(this.oQuestion);
				boolean found = debug.isEmpty();
				
				if (found == false) {
					if ("SUPPLIES".equals(this.name) || "SERVICES".equals(this.name)) {
						String sNewName = "SUPPLIES OR SERVICES";
						ParsedQuestionRecord oParsedQuestionRecord = questionSet.findByName("[" + sNewName + "]");
						if (oParsedQuestionRecord != null) {
							String sTest = findValue(oParsedQuestionRecord);
							if (sTest.isEmpty()) {
								this.oQuestion = oParsedQuestionRecord;
								this.changedExpression = "(" + this.code + ") " + this.oQuestion.getNameText() + " IS: " + (this.not ? "NOT " : "") + this.value;
								this.corrected = true;
								debug = "";
								found = true;
							}
						}
					}
					if (found == false) {
						String sError2 = this.findQuestion(questionSet);
						if (sError2.isEmpty()) {
							debug = "";
							found = true;
						} else if (this.oQuestion.isNumberType()) {
							if (CommonUtils.isNotEmpty(this.value))
								this.value = this.value.replaceAll(",", ""); 
							if (isNumber(this.value)) {
								this.oprType = OPR_EQUAL_TO;
								debug = "";
								found = true;
							}
						}
					}
				}
				
				/*
				boolean found = false;
				String notValue = "NOT " + this.value;
				String debug = "(" + this.code + ") Value not found: \"" + this.value + "\" in " + this.oQuestion.questionCode + " choices. Available values: <ul>";
				if (this.oQuestion.containChoice(this.value)) // if (this.oQuestion.hasChoice(this.value)) // Hierarchy
					found = true;
				else if ((this.not == true) && this.oQuestion.containChoice(notValue)) { //  Hierarchy
					this.value = notValue;
					this.not = false;
					found = true;
				}
				else {
					ParsedQuestionChoiceSet choices = this.oQuestion.choices;
					for (int c = 0; c < choices.size(); c++) {
						ParsedQuestionChoiceRecord questionChoiceItem = choices.get(c);
						if (questionChoiceItem.choiceText.equals(this.value)) { // if (questionChoiceItem.choice.startsWith(this.value)) {
							found = true;
							break;
						} else if (this.not == true) {
							if (questionChoiceItem.choiceText.equals(notValue)) {
								this.value = notValue;
								this.not = false;
								found = true;
								break;
							}
						}
						debug += "<li>\"" + questionChoiceItem.choiceText + "\"</li>";
					}
					debug += "</ul>";
				}
				*/
				if (! found) {
					error += ((error == "") ? "" : "<br />") + debug;
				}
			} else {
				error += ((error == "") ? "" : "; ") + "(" + this.code + ") target value is empty";
			}
		}
		if (!error.isEmpty()) {
			if (error.equals(sIsCorrected) && (this.corrected == false)) {
				this.changedExpression = "(" + this.code + ") " + this.oQuestion.getNameText() + " IS: " + (this.not ? "NOT " : "") + this.value;
				this.corrected = true;
				error = "";
			} else {
				error += "\n<pre>" + this.originalExpression + "</pre>";
			}
		}
		return error;
	};
	
	private String findQuestion(ProduceQuestion questionSet) {
		String error = "";
		ArrayList<ParsedQuestionRecord> aQuestions = new ArrayList<ParsedQuestionRecord>();
		if ((questionSet.findByChoiceValue(this.value, aQuestions) == false) && (this.not == true)) {
			String notValue = "NOT " + this.value;
			if (questionSet.findByChoiceValue(notValue, aQuestions)) {
				this.value = notValue;
				this.not = false;
			}
		}
		if (aQuestions.size() > 0) {
			if (aQuestions.size() == 1) {
				this.oQuestion = aQuestions.get(0);
				if (this.not == true)
					this.oQuestion = this.oQuestion.getRootParent();
				this.changedExpression = "(" + this.code + ") " + this.oQuestion.getNameText() + " IS: " + (this.not ? "NOT " : "") + this.value;
				this.corrected = true;
				//System.out.println(" Original: " + this.originalExpression);
				//System.out.println("Changed: " + this.changedExpression);
			} else {
				error = ((error == "") ? "" : "; ") + "(" + this.code + ") [" + this.name + "]" + "Found multiple questions for choice value of \"" + this.value + "\":<br>\n";
				for (ParsedQuestionRecord oQuestion: aQuestions) {
					error += " " + oQuestion.questionCode;
				}
			}
		} else {
			error = ((error == "") ? "" : "; ") + "(" + this.code + ") [" + this.name + "]" + " Question Name not found";
		}
		return error;
	}
	
	private String findValue(ParsedQuestionRecord oParsedQuestionRecord) {
		boolean found = false;
		String notValue = "NOT " + this.value;
		String debug = "(" + this.code + ") Value not found: \"" + this.value + "\" in " + this.oQuestion.questionCode + " choices. Available values: <ul>";
		if (oParsedQuestionRecord.containChoice(this.value)) // if (oParsedQuestionRecord.hasChoice(this.value)) // Hierarchy
			found = true;
		else if ((this.not == true) && oParsedQuestionRecord.containChoice(notValue)) { //  Hierarchy
			this.value = notValue;
			this.not = false;
			found = true;
		}
		else {
			ParsedQuestionChoiceSet choices = oParsedQuestionRecord.choices;
			for (int c = 0; c < choices.size(); c++) {
				ParsedQuestionChoiceRecord questionChoiceItem = choices.get(c);
				if (questionChoiceItem.choiceText.equals(this.value)) { // if (questionChoiceItem.choice.startsWith(this.value)) {
					found = true;
					break;
				} else if (this.not == true) {
					if (questionChoiceItem.choiceText.equals(notValue)) {
						this.value = notValue;
						this.not = false;
						found = true;
						break;
					}
				}
				debug += "<li>\"" + questionChoiceItem.choiceText + "\"</li>";
			}
			debug += "</ul>";
		}
		if (! found) {
			return debug;
		}
		return "";
	}

	public String getExpression() {
		if (this.corrected)
			return this.changedExpression;
		else
			return this.originalExpression;
	}
	
	public String generateExpression() {
		this.changedExpression = "(" + this.code + ") ";
		if (this.oprType == OPR_INCLUDED) {
			this.changedExpression += this.oClause.clauseName + " IS: " + (this.not ? "NOT " : "") + "INCLUDED";
		} else {
			this.changedExpression += this.oQuestion.getNameText() + " IS: " + (this.not ? "NOT " : "");
			if (this.oprType == OPR_LESS_THAN_OR_EQUAL_TO)
				this.changedExpression += LESS_THAN_OR_EQUAL_TO + " " + this.value;
			else if (this.oprType == OPR_GREATER_THAN_OR_EQUAL_TO)
				this.changedExpression += GREATER_THAN_OR_EQUAL_TO + " " + this.value;
			else if (this.oprType == OPR_GREATER_THAN)
				this.changedExpression += GREATER_THAN + " " + this.value;
			else if (this.oprType == OPR_LESS_THAN)
				this.changedExpression += LESS_THAN + " " + this.value;
			else if (this.oprType == OPR_EQUAL_TO)
				this.changedExpression += EQUAL_TO + " " + this.value;
			else
				this.changedExpression += this.value;
		}

		this.corrected = true;
		return this.changedExpression;
	}
}
