package hgs.cpm.versioning.util;

import gov.dod.cls.utils.CommonUtils;
import hgs.cpm.db.ParsedClauseRecord;
import hgs.cpm.db.ParsedQuestionChoiceRecord;
import hgs.cpm.db.ParsedQuestionChoiceSet;
import hgs.cpm.db.ParsedQuestionRecord;
import hgs.cpm.versioning.ProduceClause;
import hgs.cpm.versioning.ProduceQuestion;

import java.util.ArrayList;

public class AdditionalConditionEvalSet extends ArrayList<AdditionalConditionEvalItem> {

	private static final long serialVersionUID = -3603884905239207166L;

	public static final boolean SHOW_AC_NO_ERROR = false;
	public static final boolean SHOW_AC_ERROR = true;
	
	boolean isExpressionValid = false;
	ParsedClauseRecord clause;
	
	public String parseConditions(ParsedClauseRecord oClauseItem, ProduceQuestion questionSet, ProduceClause clauseSet) {
		this.clause = oClauseItem;
		String pCondition = oClauseItem.additionalConditions;
		String parseError = "", sError;
		
		String[] aParts, aClauses;
		int iPos;
		String sOperations;
		String[] aIf = pCondition.toUpperCase().replaceAll("\n", " ").split("IF ");
		for (int iIf = 0; iIf < aIf.length; iIf++) {
			AdditionalConditionEvalItem oAdtnlCondItem = null;
			String sExpression = aIf[iIf].trim();
			if (CommonUtils.isNotEmpty(sExpression)) {
				sExpression = sExpression.replace("IS ", "");
				aParts = sExpression.split("ADDED");
				if (aParts.length > 1) {
					oAdtnlCondItem = new AdditionalConditionEvalItem();
					//if ("52.225-3" == oClauseItem.name) // ("252.232-7004" == oClauseItem.name) || ("52.232-36" == oClauseItem.name))
					//	logOnConsole(oClauseItem.name + "[" + iIf + "] "" + sExpression + "\"");
					for (int iPart = 0; iPart < aParts.length - 1; iPart++) {
						String sCondition = aParts[iPart].trim();
						if (CommonUtils.isNotEmpty(sCondition)) {
							aClauses = sCondition.split(" OR ");
							if (aClauses.length > 1) {
								oAdtnlCondItem.addOrClauseSet(this.resolveClauseSet(aClauses, clauseSet));
							} else {
								iPos = sCondition.indexOf("AND ");
								if (iPos >= 0) {
									if (iPos == 0) {
										oAdtnlCondItem.addAndClause(oClauseItem);
										sOperations = sCondition.substring("AND ".length() + iPos).trim();
										iPos = sOperations.indexOf(" IS:");
										if (iPos > 0) {
											logOnConsole(oClauseItem.clauseName + "[" + iIf + "][" + iPart + "] \"" + sCondition + "\"");
										} else {
											String[] aOr = sOperations.split(",");
											if (aOr.length > 1) {
												oAdtnlCondItem.addOrClauseSet(this.resolveClauseSet(aOr, clauseSet)); // aOr);
											} else {
												logOnConsole(oClauseItem.clauseName + "[" + iIf + "][" + iPart + "] \"" + sCondition + "\"");
											}
										}
									} else {
										/*
										var aAnd = sCondition.split(" AND ");
										for (var iAnd = 0; iAnd < aAnd.length; iAnd++) {
											var sItem = aAnd[iAnd];
											if (sItem) {
											}
										}
										*/
										parseError += (CommonUtils.isNotEmpty(parseError) ? "<br />\n" : "") + "[" + iIf + "][" + iPart + "] Unknown: \"" + sCondition + "\"";
										//logOnConsole(oClauseItem.clauseName + "[" + iIf + "][" + iPart + "] "" + sCondition + """);
									}
								} else {
									iPos = sCondition.indexOf(" IS:");
									if (iPos > 0) {
										//logOnConsole(oClauseItem.clauseName + "[" + iIf + "][" + iPart + "] IS: "" + sCondition + """);
										parseError += (CommonUtils.isNotEmpty(parseError) ? "<br />\n" : "") + oClauseItem.clauseName + "[" + iIf + "][" + iPart + "] IS: \"" + sCondition + "\"";
									} else {
										String[] aOr = sCondition.split(",");
										if (aOr.length > 1)
											oAdtnlCondItem.addOrClauseSet(this.resolveClauseSet(aOr, clauseSet)); // aOr);
										else
											oAdtnlCondItem.addAndClauseSet(this.resolveClauseSet(aOr, clauseSet)); // aOr);
									}
								}
							}
						} else {
							oAdtnlCondItem.addAndClause(oClauseItem);
						}
					} // for (int iPart = 0; iPart < aParts.length - 1; iPart++) {
					String sRemainedCondition = null;
					sOperations = aParts[aParts.length - 1].trim();
					iPos = sOperations.indexOf("DO NOT INCLUDE ");
					if (iPos >= 0) {
						sRemainedCondition = sOperations.substring(0, iPos).trim();
						String sCaluses = sOperations.substring("DO NOT INCLUDE ".length() + iPos).trim();
						this.addDoNotInclude(sCaluses, clauseSet, oAdtnlCondItem);
					} else {
						iPos = sOperations.indexOf("INCLUDE ");
						if (iPos >= 0) {
							sRemainedCondition = sOperations.substring(0, iPos).trim();
							String sCaluses = sOperations.substring("INCLUDE ".length() + iPos).trim();
							this.addInclude(sCaluses, clauseSet, oAdtnlCondItem);
						} else {
							iPos = sOperations.indexOf("REPLACE WITH ");
							if (iPos >= 0) {
								sRemainedCondition = sOperations.substring(0, iPos).trim();
								String sCaluses = sOperations.substring("REPLACE WITH ".length() + iPos).trim();
								this.addReplaceWidth(sCaluses, clauseSet, oAdtnlCondItem);
							} else {
								parseError += (CommonUtils.isNotEmpty(parseError) ? "<br />\n" : "") + "[" + iIf + "] ADDED: \"" + aParts[aParts.length - 1].trim() + "\"";
							}
						}
					}
					if (CommonUtils.isNotEmpty(sRemainedCondition) && (!sRemainedCondition.equals(","))) {
						String[] aQuestion = sRemainedCondition.split(" IS:");
						if (aQuestion.length == 2) {
							if (aQuestion[0].startsWith("AND "))
								aQuestion[0] = aQuestion[0].substring("AND ".length()).trim();
							else
								aQuestion[0] = aQuestion[0].trim();
							sError = this.assignQuestion(oAdtnlCondItem, aQuestion[0], aQuestion[1], questionSet);
							if (CommonUtils.isNotEmpty(sError))
								parseError += (CommonUtils.isNotEmpty(parseError) ? "<br />\n" : "") + "[" + iIf + "] " + sError;
							//oAdtnlCondItem.question = aQuestion[0];
							//oAdtnlCondItem.answer = aQuestion[1].trim();
						} else {
							parseError += (CommonUtils.isNotEmpty(parseError) ? "<br />\n" : "") + "[" + iIf + "] sRemainedCondition: \"" + sRemainedCondition + "\"";
						}
					}
					if (this.indexOf(oAdtnlCondItem) < 0)
						this.add(oAdtnlCondItem);
				} else {
					//if ("252.219-7003 ALTERNATE I" == oClauseItem.clauseName) { // ("252.232-7004" == oClauseItem.clauseName) || ("52.232-36" == oClauseItem.clauseName)) {
					//	logOnConsole(oClauseItem.clauseName + "[" + iIf + "] "" + sExpression + """);
					//}
					aParts = sExpression.split(" IS:");
					if (aParts.length > 0) {
						sError = this.parseQuestion(aParts, questionSet, clauseSet);
						if (CommonUtils.isNotEmpty(sError))
							parseError += (CommonUtils.isNotEmpty(parseError) ? "<br />\n" : "") + "[" + iIf + "] " + sError;
					} else
						parseError += (CommonUtils.isNotEmpty(parseError) ? "<br />\n" : "") + "[" + iIf + "] no ADDED: \"" + sExpression + "\"";
				}
			}
		}
		
		if (CommonUtils.isEmpty(parseError)) {
			this.isExpressionValid = true;
			/*
			if (SHOW_AC_NO_ERROR == true) {
				//logOnConsole("\n<br /><strong>Clause</strong> (id: " + oClauseItem.id + ", Name: " + oClauseItem.clauseName + "): NO ERROR");
				logOnConsole("\n<br /><strong>[AC] Clause</strong> (id: " + oClauseItem.clauseId + ", Name: " + oClauseItem.clauseName + "): NO ERROR<br />\n"
					+ this.debugHtml());
			}
			*/
		} else 
		if (SHOW_AC_ERROR == true) {
			parseError += "\n<pre>" + oClauseItem.additionalConditions + "</pre>\n";
			logOnConsole("\n<br /><strong>[AC] Clause</strong> (id: " + oClauseItem.clauseId + ", Name: " + oClauseItem.clauseName + "): " +
					parseError);
		}
		return "";
	};

	private String parseQuestion(String[] pParts, ProduceQuestion questionSet, ProduceClause clauseSet) {
		String sError = null;
		
		String sQustion = pParts[0].trim();
		if (sQustion.isEmpty())
			return "Question is not defined";

		if (pParts.length < 2)
			return "Question expression has less than 2 parts";

		String[] aParts2 = pParts[1].split(",");
		if (aParts2.length != 2)
			return "Question expression does not match with 1 character";

		String sAnswer = aParts2[0].trim();
		
		AdditionalConditionEvalItem oAdtnlCondItem = null;
		String sOperations = aParts2[1].trim();
		int iPos = sOperations.indexOf("DO NOT INCLUDE ");
		if (iPos >= 0) {
			String sCaluses = sOperations.substring("DO NOT INCLUDE ".length() + iPos).trim();
			oAdtnlCondItem = this.addDoNotInclude(sCaluses, clauseSet, oAdtnlCondItem);
		} else {
			iPos = sOperations.indexOf("INCLUDE ");
			if (iPos >= 0) {
				String sCaluses = sOperations.substring("INCLUDE ".length() + iPos).trim();
				oAdtnlCondItem = this.addInclude(sCaluses, clauseSet, oAdtnlCondItem);
			}
		}
		
		if (oAdtnlCondItem != null) {
			aParts2 = sQustion.split(" TRUE AND ");
			if (aParts2.length == 2) {
				oAdtnlCondItem.rule = aParts2[0].trim();
				sQustion = aParts2[1].trim();
			}
			//oAdtnlCondItem.question = sQustion;
			//oAdtnlCondItem.answer = sAnswer.trim();
			return this.assignQuestion(oAdtnlCondItem, sQustion, sAnswer, questionSet);
		}
		
		sError = "UNKNOWN OPERATION of " + pParts;
		
		return sError;
	}
	
	private String assignQuestion(AdditionalConditionEvalItem oAdtnlCondItem, String sQustion, String sAnswer, ProduceQuestion questionSet) {
		ParsedQuestionRecord oQuestion = questionSet.findByName("[" + sQustion + "]");
		if (oQuestion != null) {
			sAnswer = sAnswer.trim();
			if (sAnswer.charAt(sAnswer.length() - 1) == ',')
				sAnswer = sAnswer.substring(0, sAnswer.length() - 1).trim();
			boolean bFound = false;
			boolean bNot = false;
			String sNotAnswer = sAnswer;
			if (sNotAnswer.startsWith("NOT "))
				sNotAnswer = sNotAnswer.substring("NOT ".length()).trim();
			ParsedQuestionChoiceSet choices = oQuestion.choices;
			String debug = "Value not found: \"" + sAnswer + "\" in [" + sQustion + "] question. Available values are <ul>";
			for (int c = 0; c < choices.size(); c++) {
				ParsedQuestionChoiceRecord questionChoiceItem = choices.get(c);
				if (sAnswer.equals(questionChoiceItem.choiceText)) {
					bFound = true;
					break;
				}
				else if (sNotAnswer.equals(questionChoiceItem.choiceText)) {
					bFound = true;
					bNot = true;
					break;
				}
				debug += "<li>\"" + questionChoiceItem.choiceText + "\"</li>";
			}
			debug += "</ul>";
			if (bFound) {
				oAdtnlCondItem.question = oQuestion;
				oAdtnlCondItem.answer = sAnswer.trim();
				oAdtnlCondItem.notAnswer = bNot;
				return null;
			} else {
				return debug;
			}

		} else {
			return "Unknown Question [" + sQustion + "]";
		}
	};

	// ------------------------------------------
	private AdditionalConditionEvalItem addDoNotInclude(String pClauses, ProduceClause clauseSet, AdditionalConditionEvalItem oAdtnlCondItem) {
		ArrayList<ParsedClauseRecord> aClauses = this.parseClauses(pClauses, clauseSet);

		if (oAdtnlCondItem == null)
			oAdtnlCondItem = new AdditionalConditionEvalItem();
		oAdtnlCondItem.oprType = AdditionalConditionEvalItem.OPR_DO_NOT_INCLUDE;
		oAdtnlCondItem.oprClauses = aClauses;
		this.add(oAdtnlCondItem);
		return oAdtnlCondItem;
	}
	
	private AdditionalConditionEvalItem addInclude(String pClauses, ProduceClause clauseSet, AdditionalConditionEvalItem oAdtnlCondItem) {
		ArrayList<ParsedClauseRecord> aClauses = this.parseClauses(pClauses, clauseSet);

		if (oAdtnlCondItem == null)
			oAdtnlCondItem = new AdditionalConditionEvalItem();
		oAdtnlCondItem.oprType = AdditionalConditionEvalItem.OPR_INCLUDE;
		oAdtnlCondItem.oprClauses = aClauses;
		this.add(oAdtnlCondItem);
		return oAdtnlCondItem;
	};

	private void addReplaceWidth(String pClauses, ProduceClause clauseSet, AdditionalConditionEvalItem oAdtnlCondItem) {
		ArrayList<ParsedClauseRecord> aClauses = this.parseClauses(pClauses, clauseSet);

		if (oAdtnlCondItem == null)
			oAdtnlCondItem = new AdditionalConditionEvalItem();
		oAdtnlCondItem.oprType = AdditionalConditionEvalItem.OPR_REPLACE_WITH;
		oAdtnlCondItem.oprClauses = aClauses;
		this.add(oAdtnlCondItem);
	};

	// ----------------------------------------------------------------
	private ArrayList<ParsedClauseRecord> resolveClauseSet(String[] aClauses, ProduceClause clauseSet) {
		ArrayList<ParsedClauseRecord> aResult = new ArrayList<ParsedClauseRecord>();
		for (int item = 0; item < aClauses.length; item++) {
			String sClause = aClauses[item].trim();
			if (CommonUtils.isNotEmpty(sClause)) {
				ParsedClauseRecord oClause = clauseSet.findByName(sClause);
				if (oClause != null)
					aResult.add(oClause);
				else
					logOnConsole("\n<br /><strong>[AC] Clause</strong> (id: " + this.clause.clauseId + ", Name: " + this.clause.clauseName + "): " +
						" unknown clause \"" + sClause + "\"");
			}
		}
		return aResult;
	};

	private ParsedClauseRecord resolveClause(String sClause, ProduceClause clauseSet) {
		ParsedClauseRecord oClause = clauseSet.findByName(sClause);
		if (oClause == null)
			logOnConsole("\n<br /><strong>[AC] Clause</strong> (id: " + this.clause.clauseId + ", Name: " + this.clause.clauseName + "): " +
						" unknown clause \"" + sClause + "\"");
		return oClause;
	};

	// ----------------------------------------------------------------
	private ArrayList<ParsedClauseRecord> parseClauses(String pClauses, ProduceClause clauseSet) {
		pClauses = pClauses.replace("DFARS CLAUSE ", " ").replace("DFAR ", " ").replace("FAR ", " ");
		String[] aItems = pClauses.trim().split(",");
		ArrayList<ParsedClauseRecord> aResult = new ArrayList<ParsedClauseRecord>();
		for (int item = 0; item < aItems.length; item++) {
			String sClause = aItems[item].trim();
			if (CommonUtils.isNotEmpty(sClause)) {
				ParsedClauseRecord oClause = clauseSet.findByName(sClause);
				if (oClause != null)
					aResult.add(oClause);
				else
					logOnConsole("\n<br /><strong>[AC] Clause</strong> (id: " + this.clause.clauseId + ", Name: " + this.clause.clauseName + "): " +
						" unknown clause \"" + sClause + "\"");
			}
		}
		return aResult;
	};
	
	/*
	private void evaluate(oFinalCheckSet, oPrescriptionChoices) {
		if (this.isExpressionValid == false)
			return;
		for (var iEval = 0; iEval < this.conditions.length; iEval++) {
			var oAdtnlCondItem = this.conditions[iEval];
			oAdtnlCondItem.evaluate(this.clause, oFinalCheckSet, oPrescriptionChoices);
		}
	};
	*/
		
	// ----------------------------------------------------------------
	private String debugHtml() {
		String html = "";
		for (int item = 0; item < this.size(); item++) {
			if (item == 0)
				html = this.get(item).debugHtml();
			else
				html += "<hr/>" + this.get(item).debugHtml();
		}
		return html;
	};
	
	private void logOnConsole(String sLog) {
		System.out.println(sLog);
	}
	
}
