package hgs.cpm.versioning;

import gov.dod.cls.db.ClauseRecord;
import gov.dod.cls.db.ClauseTable;
import gov.dod.cls.db.ClauseVersionChangeRecord;
import gov.dod.cls.db.ClauseVersionChangeTable;
import gov.dod.cls.utils.CommonUtils;
import gov.dod.cls.utils.sql.SqlUtil;
import hgs.cpm.db.ParsedClauseFillInRecord;
import hgs.cpm.db.ParsedClausePrescriptionSet;
import hgs.cpm.db.ParsedClauseRecord;
import hgs.cpm.db.ParsedPrescriptionSet;
import hgs.cpm.db.Stg1OfficialClauseRecord;
import hgs.cpm.db.Stg1RawClauseRecord;
import hgs.cpm.db.Stg2ClauseFillInsRecord;
import hgs.cpm.db.Stg3ClauseCorrectionRecord;
import hgs.cpm.db.Stg3ClauseCorrectionSet;
import hgs.cpm.utils.StringCommons;

import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;

public class ProduceClause extends ArrayList<ParsedClauseRecord> {

	private static final long serialVersionUID = 2671672929846245379L;

	private int iEfcrSrcDocId;
	private int iClauseSrcDocId;

	private Integer iTargetClauseVersionId;
	private Integer iPreviousClauseVersionId;
	
	private ParsedPrescriptionSet parsedPrescriptionSet;
	private ParsedClausePrescriptionSet parsedClausePrescriptionSet = new ParsedClausePrescriptionSet();
	
	private Integer iSheet_Number, iRow_Number;
	
	private Connection conn;
	
	public ProduceClause(int iEfcrSrcDocId, int iClauseSrcDocId, Integer iTargetClauseVersionId, Integer iPreviousClauseVersionId, 
			ParsedPrescriptionSet parsedPrescriptionSet) {
		this.iEfcrSrcDocId = iEfcrSrcDocId;
		this.iClauseSrcDocId = iClauseSrcDocId;
		
		this.iTargetClauseVersionId = iTargetClauseVersionId;
		this.iPreviousClauseVersionId = iPreviousClauseVersionId;
		
		this.parsedPrescriptionSet = parsedPrescriptionSet;
	}
	
	public void run(ProduceQuestion oProduceQuestion, Connection conn, FileOutputStream fileOutputStream, boolean bReport) throws SQLException, ParseException, IOException {
		this.conn = conn;
		this.clear();
		this.parsedClausePrescriptionSet.load(conn, this.iClauseSrcDocId);

		this.clearFields(conn);
		this.load1(conn);
		this.loadAlt(conn);
		this.parsedClausePrescriptionSet.clear();
		
		this.resolveIssues(oProduceQuestion, conn, fileOutputStream, bReport);
		
		this.generateSql(fileOutputStream, bReport);
	}
	
	private void clearFields(Connection conn) throws SQLException {
		PreparedStatement ps1 = null;
		PreparedStatement ps2 = null;
		try {
			String sSql = "UPDATE Stg1_Official_Clause\n" +
					"SET Content_Html_FillIns = NULL\n" +
					"WHERE Src_Doc_Id = ?\n" +
					"AND Content_Html_FillIns IS NOT NULL\n" +
					"AND (Content_Html_FillIns = ''" +
					// " OR Clause_Number NOT IN (\n" + LoadEcfr.SQL_FILL_IN_CLAUSE_NUMBER + "\n)" +
					")";
			ps1 = conn.prepareStatement(sSql);
			ps1.setInt(1, this.iEfcrSrcDocId);
			//ps1.setInt(2, this.iClauseSrcDocId);
			ps1.execute();

			sSql = "UPDATE Stg1_Official_Clause_Alt\n" +
					"SET Content_Html_FillIns = NULL\n" +
					"WHERE Src_Doc_Id = ?\n" +
					"AND Content_Html_FillIns IS NOT NULL\n" +
					"AND (Content_Html_FillIns = ''" +
					// " OR CONCAT(Clause_Number, ' ', Alt_Number) NOT IN (\n" + LoadEcfr.SQL_FILL_IN_CLAUSE_NUMBER + "\n)" +
					")";
			ps2 = conn.prepareStatement(sSql);
			ps2.setInt(1, this.iEfcrSrcDocId);
			//ps2.setInt(2, this.iClauseSrcDocId);
			ps2.execute();
		}
		finally {
			SqlUtil.closeInstance(ps2);
			SqlUtil.closeInstance(ps1);
		}
	}
	
	private void load1(Connection conn) throws SQLException, ParseException {
		PreparedStatement ps1 = null;
		ResultSet rs1 = null;
		PreparedStatement ps2 = null;
		ResultSet rs2 = null;
		try {
			String sSql = "SELECT DISTINCT f." + Stg2ClauseFillInsRecord.FIELD_FILL_IN_CODE +
					", f." + Stg2ClauseFillInsRecord.FIELD_FILL_IN_TYPE +
					", f." + Stg2ClauseFillInsRecord.FIELD_FILL_IN_MAX_SIZE +
					", f." + Stg2ClauseFillInsRecord.FIELD_FILL_IN_GROUP_NUMBER +
					", f." + Stg2ClauseFillInsRecord.FIELD_FILL_IN_PLACEHOLDER +
					", f." + Stg2ClauseFillInsRecord.FIELD_FILL_IN_DEFAULT_DATA +
					", f." + Stg2ClauseFillInsRecord.FIELD_FILL_IN_DISPLAY_ROWS +
					", f." + Stg2ClauseFillInsRecord.FIELD_FILL_IN_FOR_TABLE +
					", f." + Stg2ClauseFillInsRecord.FIELD_FILL_IN_HEADING +
					" FROM " + Stg1OfficialClauseRecord.TABLE_STG1_OFFICIAL_CLAUSE + " C\n" +
					"JOIN " + Stg2ClauseFillInsRecord.TABLE_STG2_CLAUSE_FILL_INS + " F " +
					"ON (f." + Stg2ClauseFillInsRecord.FIELD_STG1_OFFICIAL_CLAUSE_ID +
					" = c." + Stg1OfficialClauseRecord.FIELD_STG1_OFFICIAL_CLAUSE_ID + ")\n" +
					"WHERE c." + Stg1OfficialClauseRecord.FIELD_SRC_DOC_ID + " = ? " +
					"AND c." + Stg1OfficialClauseRecord.FIELD_STG1_OFFICIAL_CLAUSE_ID + " = ?\n" + // c.clause_number = ?\n" +
					"ORDER BY f." + Stg2ClauseFillInsRecord.FIELD_STG2_CLAUSE_FILLINS_ID;
			ps2 = conn.prepareStatement(sSql);
			ps2.setInt(1, this.iEfcrSrcDocId);

			ps1 = conn.prepareStatement(this.getSql1());
			ps1.setInt(1, this.iEfcrSrcDocId);
			ps1.setInt(2, this.iClauseSrcDocId);
			// ps1.setInt(3, this.iClauseSrcDocId);
			rs1 = ps1.executeQuery();
			while (rs1.next()) {
				this.iSheet_Number = CommonUtils.toInteger(rs1.getObject("Sheet_Number"));
				this.iRow_Number = CommonUtils.toInteger(rs1.getObject("Row_Number"));
				
				ParsedClauseRecord oRecord = new ParsedClauseRecord();
				oRecord.clauseName = rs1.getString(1);
				oRecord.clauseSectionId = CommonUtils.toInteger(rs1.getObject(2));
				oRecord.inclusion = rs1.getString(3);
				oRecord.regulationId = rs1.getInt(4);
				oRecord.clauseTitle = ParsedClauseRecord.fixHtmlSpecialCodes(rs1.getString(5), true);
				oRecord.effectiveDate = monthYearToDate(rs1.getString("Effective_Date"), oRecord.clauseName); // 8, this.toDate2()
				
				oRecord.setClauseData(rs1.getString(6), false);
				
				oRecord.url = rs1.getString(7);
				oRecord.clauseConditions = rs1.getString(9);
				oRecord.clauseRule = rs1.getString(10);
				oRecord.additionalConditions = rs1.getString(11);
				oRecord.clauseId = rs1.getInt(12);
				
				oRecord.requiredOrOptional = rs1.getString("Required_Or_Optional");
				oRecord.editable = rs1.getString("Editable");
				oRecord.provisionOrClause = rs1.getString("Provision_Or_Clause");
				oRecord.commercialStatus = rs1.getString(Stg1RawClauseRecord.FIELD_COMMERCIAL_STATUS); // CJ-476
				oRecord.optionalStatus = rs1.getString(Stg1RawClauseRecord.FIELD_OPTIONAL_STATUS); // CJ-802
				oRecord.optionalConditions = rs1.getString(Stg1RawClauseRecord.FIELD_OPTIONAL_CONDITIONS); // CJ-802
				oRecord.officialTitle = rs1.getString(Stg1RawClauseRecord.FIELD_OFFICIAL_CLAUSE_TITLE); // CJ-916
				oRecord.isDfarsActivity = rs1.getBoolean(Stg1RawClauseRecord.FIELD_DFARS_ACTIVITY); // CJ-1072
				
				oRecord.setRawClauseTitle(rs1.getString("Raw_Clause_Title"));

				oRecord.setTitleInContent(rs1.getString(Stg1OfficialClauseRecord.FIELD_TITLE_IN_CONTENT)); // CJ-193
				
				this.parsedClausePrescriptionSet.loadPrescritions(oRecord);
				
				oRecord.adjustValues();
				
				Integer iStg1OfficialClauseId = CommonUtils.toInteger(rs1.getObject(13)); // o.Stg1_Official_Clause_Id
				
				if (iStg1OfficialClauseId != null) {
					ps2.setInt(2, iStg1OfficialClauseId); // ps2.setString(2, oRecord.clauseName);
					rs2 = ps2.executeQuery();
					while (rs2.next()) {
						ParsedClauseFillInRecord oFillInRecord = new ParsedClauseFillInRecord();
						oFillInRecord.code = rs2.getString(1);
						oFillInRecord.type = rs2.getString(2);
						oFillInRecord.maxSize = CommonUtils.toInteger(rs2.getObject(3));
						
						oFillInRecord.fillInGroupNumber = CommonUtils.toInteger(rs2.getObject("Fill_In_Group_Number"));
						oFillInRecord.fillInPlaceholder = rs2.getString("Fill_In_Placeholder");
						oFillInRecord.fillInDefaultData = rs2.getString("Fill_In_Default_Data");
						oFillInRecord.fillInDisplayRows = CommonUtils.toInteger(rs2.getObject("Fill_In_Display_Rows"));
						oFillInRecord.fillInForTable = rs2.getBoolean("Fill_In_For_Table"); //    boolean NOT NULL DEFAULT 0,
						oFillInRecord.fillInHeading = rs2.getString("Fill_In_Heading");

						oRecord.fillIns.add(oFillInRecord);
					}
					rs2.close();
					rs2 = null;
				} else {
					System.out.println(oRecord.clauseName + " - Missing Generated Stg1_Official_Clause_Id");
				}
				
				this.add(oRecord);
			}
		}
		finally {
			SqlUtil.closeInstance(rs2, ps2);
			SqlUtil.closeInstance(rs1, ps1);
		}
	}
	
	private void loadAlt(Connection conn) throws SQLException, ParseException {
		PreparedStatement ps1 = null;
		ResultSet rs1 = null;
		PreparedStatement ps2 = null;
		ResultSet rs2 = null;
		try {
			String sSql = "SELECT f.Fill_In_Code, f.Fill_In_Type, f.Fill_In_Max_Size, " +
					"f.Fill_In_Group_Number, f.Fill_In_Placeholder, f.Fill_In_Default_Data, f.Fill_In_Display_Rows, f.Fill_In_For_Table, f.Fill_In_Heading\n" +
					"FROM Stg1_Official_Clause_Alt c\n" +
					"JOIN Stg2_Clause_Fill_Ins_Alt f on (f.Stg1_Official_Clause_Alt_Id = c.Stg1_Official_Clause_Alt_Id)\n" +
					"WHERE c.Src_Doc_Id = ? AND c.Stg1_Official_Clause_Alt_Id = ?\n" + // c.clause_number = ?\n" +
					"ORDER BY Stg2_Clause_Fillins_Alt_Id";
			ps2 = conn.prepareStatement(sSql);
			ps2.setInt(1, this.iEfcrSrcDocId);
			
			ps1 = conn.prepareStatement(this.getSqlAlt());
			ps1.setInt(1, this.iEfcrSrcDocId);
			ps1.setInt(2, this.iClauseSrcDocId);
			//ps1.setInt(2, this.iEfcrSrcDocId);
			//ps1.setInt(3, this.iClauseSrcDocId);
			//ps1.setInt(4, this.iClauseSrcDocId);
			rs1 = ps1.executeQuery();
			while (rs1.next()) {
				this.iSheet_Number = CommonUtils.toInteger(rs1.getObject("Sheet_Number"));
				this.iRow_Number = CommonUtils.toInteger(rs1.getObject("Row_Number"));
				
				ParsedClauseRecord oRecord = new ParsedClauseRecord();
				oRecord.isAltClause = true;

				oRecord.clauseName = rs1.getString(1);
				oRecord.clauseSectionId = CommonUtils.toInteger(rs1.getObject(2));
				oRecord.inclusion = rs1.getString(3);
				oRecord.regulationId = rs1.getInt(4);
				oRecord.clauseTitle = ParsedClauseRecord.fixHtmlSpecialCodes(rs1.getString(5), true);
				// oRecord.effectiveDate = this.toDate2(rs1.getString(8)); // this.toDate(rs1.getObject(8));

				oRecord.setClauseData(rs1.getString(6), true);
				
				oRecord.url = rs1.getString(7);
				oRecord.clauseConditions = rs1.getString(9);
				oRecord.clauseRule = rs1.getString(10);
				oRecord.additionalConditions = rs1.getString(11);
				oRecord.clauseId = rs1.getInt(12);
				
				String sEffDate = rs1.getString("Effective_Date"); // 14
				Date dEffectiveDate2 = monthYearToDate(sEffDate, oRecord.clauseName);
				oRecord.effectiveDate = dEffectiveDate2;

				oRecord.requiredOrOptional = rs1.getString("Required_Or_Optional");
				oRecord.editable = rs1.getString("Editable");
				oRecord.provisionOrClause = rs1.getString("Provision_Or_Clause");
				oRecord.commercialStatus = rs1.getString(Stg1RawClauseRecord.FIELD_COMMERCIAL_STATUS); // CJ-476
				oRecord.optionalStatus = rs1.getString(Stg1RawClauseRecord.FIELD_OPTIONAL_STATUS); // CJ-802
				oRecord.optionalConditions = rs1.getString(Stg1RawClauseRecord.FIELD_OPTIONAL_CONDITIONS); // CJ-802
				oRecord.officialTitle = rs1.getString(Stg1RawClauseRecord.FIELD_OFFICIAL_CLAUSE_TITLE); // CJ-916
				oRecord.isDfarsActivity = rs1.getBoolean(Stg1RawClauseRecord.FIELD_DFARS_ACTIVITY); // CJ-1072
				
				oRecord.setRawClauseTitle(rs1.getString("Raw_Clause_Title"));
				
				//oRecord.setTitleInContent(rs1.getString(Stg1OfficialClauseRecord.FIELD_TITLE_IN_CONTENT)); // CJ-193

				oRecord.adjustValues();

				this.parsedClausePrescriptionSet.loadPrescritions(oRecord);
				
				Integer iStg1OfficialClauseAltId = CommonUtils.toInteger(rs1.getObject(13)); // o.Stg1_Official_Clause_Alt_Id
				if (iStg1OfficialClauseAltId != null) {
					ps2.setInt(2, iStg1OfficialClauseAltId); // ps2.setString(2, oRecord.clauseName);
					rs2 = ps2.executeQuery();
					while (rs2.next()) {
						ParsedClauseFillInRecord oFillInRecord = new ParsedClauseFillInRecord();
						oFillInRecord.code = rs2.getString(1);
						oFillInRecord.type = rs2.getString(2);
						oFillInRecord.maxSize = CommonUtils.toInteger(rs2.getObject(3));
						
						oFillInRecord.fillInGroupNumber = CommonUtils.toInteger(rs2.getObject("Fill_In_Group_Number"));
						oFillInRecord.fillInPlaceholder = rs2.getString("Fill_In_Placeholder");
						oFillInRecord.fillInDefaultData = rs2.getString("Fill_In_Default_Data");
						oFillInRecord.fillInDisplayRows = CommonUtils.toInteger(rs2.getObject("Fill_In_Display_Rows"));
						oFillInRecord.fillInForTable = rs2.getBoolean("Fill_In_For_Table"); //    boolean NOT NULL DEFAULT 0,
						oFillInRecord.fillInHeading = rs2.getString("Fill_In_Heading");

						oRecord.fillIns.add(oFillInRecord);
					}
					rs2.close();
					rs2 = null;
				}
				
				this.add(oRecord);
			}
		}
		finally {
			SqlUtil.closeInstance(rs2, ps2);
			SqlUtil.closeInstance(rs1, ps1);
		}
	}
	
	/*
	private Date toDate2(String psValue) {
		if (psValue == null)
			return null;
		String psSrc = psValue.substring(1, psValue.length() - 1);
		return monthYearToDate(psSrc);
	}
	*/
	private Date monthYearToDate(String psMonthYear, String psClauseName) {
		String sExcelRow = " [" + this.iSheet_Number + "/" + this.iRow_Number + "]";
		return StringCommons.monthYearToDate(psMonthYear, psClauseName, sExcelRow);
		/*
		if (psMonthYear == null) {
			System.out.println(psClauseName + ": empty effective date" + sExcelRow) ;
			return null;
		}
		String monthYear = psMonthYear.trim();
		if ("(Jan 2004)(Jan 2004)".equals(monthYear)) {
			monthYear = "JAN 2004";
		} else {
			if (monthYear.length() < 8) {
				System.out.println(psClauseName + ": unexpected effective date format '" + psMonthYear + "'" + sExcelRow);
			}
			if (monthYear.endsWith("."))
				monthYear = monthYear.substring(0, monthYear.length() - 1).trim();
			if (monthYear.startsWith("(") || monthYear.startsWith("[")) {
				monthYear = monthYear.substring(1);
			}
			if (monthYear.endsWith(")") || monthYear.endsWith("]")) {
				monthYear = monthYear.substring(0, monthYear.length() - 1);
			}
			if (monthYear.contains("-"))
				monthYear = monthYear.replace('-', ' ');
		}
		String[] aItems = monthYear.split(" ");
		if (aItems.length != 2) {
			System.out.println(psClauseName + ": unexpected effective date format '" + psMonthYear + "'" + sExcelRow);
			return null;
		}
		try {
			String sMonth = aItems[0].toUpperCase().trim();
			if (sMonth.endsWith("."))
				sMonth = sMonth.substring(0, sMonth.length() - 1).trim();
			int iMonth = 0;
			switch (sMonth)  {
			case "JAN":
			case "JANUARY":
				iMonth = 0; break;
		    case "FEB":
			case "FEBRUARY":
		    	iMonth = 1; break;
		    case "MAR":
		    case "MARCH":
		    	iMonth = 2; break;
		    case "APR":
		    case "APRIL":
		    	iMonth = 3; break;
		    case "MAY": iMonth = 4; break;
		    case "JUN":
		    case "JUNE":
		    	iMonth = 5; break;
		    case "JUL":
		    case "JULY":
		    	iMonth = 6; break;
		    case "AUG":
		    case "AUGUST":
		    	iMonth = 7; break;
		    case "SEP":
		    case "SEPT":
		    case "SEPTEMBER":
		    	iMonth = 8; break;
		    case "OCT":
		    case "OCTOBER":
		    	iMonth = 9; break;
		    case "NOV":
		    case "NOVEMBER":
		    	iMonth = 10; break;
		    case "DEC":
		    case "DECEMBER":
		    	iMonth = 11; break;
		    default:
		    	System.out.println(aItems[0] + " - Invalid Effective Month" + sExcelRow);
		    	break;
			}
			
			String sYear = aItems[1];
			if (sYear.endsWith(")") || sYear.endsWith("]")) {
				sYear = sYear.substring(0, sYear.length() - 1);
			}
			if (sYear.length() == 2)
				sYear = "20" + sYear;
			int iYear = Integer.parseInt(sYear);
			Calendar oCalendar = Calendar.getInstance();
			oCalendar.set(iYear, iMonth, 1);
			
			Date oDate = new Date(oCalendar.getTimeInMillis());
			return oDate;
			
		} catch (Exception oError) {
			oError.printStackTrace();
			System.out.println(psClauseName + ": monthYearToDate Error for '" + psMonthYear + "'" + sExcelRow);
			return null;
		}
		*/
	}
	
	/*
	private Date toDate(Object object) throws ParseException {
		if (object == null)
			return null;
		Date date = ParsedClauseRecord.formatDate.parse(object.toString());
		return date;
	}
	*/
	
	private String getSql1() {
		String sSql = "SELECT A.Clause_Number Clause_Name\n" +
				", B.Clause_Section_Id Clause_Section_Id\n" + // A.Section, IFNULL(B.Clause_Section_Id
				", case when A.IBR_Or_Full_Text = 'FULL TEXT' then 'F' else 'R' end inclusion_cd -- A.IBR_Or_Full_Text\n" +
				", case when A.Sheet_Number = 0 then 1 else 2 end regulation_id\n" + // , 1 regulation_id
				", C.Title clause_title\n" +
				", IFNULL(C.Content_HTML_Processed, IFNULL(C.Content_Html_FillIns, C.Content_HTML)) clause_data\n" +
				", C.uri clause_url\n" +
				", A.Effective_Clause_Date Effective_Date\n" +
				", IFNULL(D.Condition_Text_Processed, D.condition_text) clause_conditions -- IFNULL(c.processed_clause, r.conditions) clause_conditions\n" +
				", A.Rule clause_rule\n" +
				", A.Additional_Conditions\n" +
				", A.Stg1_Clause_Id\n" +
				", C.Stg1_Official_Clause_Id\n" +
				", A.Required_Or_Optional, A.Editable, A.Provision_Or_Clause\n" +
				", A.Sheet_Number, A.Row_Number\n" +
				", A.Clause AS Raw_Clause_Title\n" +
				", A." + Stg1RawClauseRecord.FIELD_COMMERCIAL_STATUS +
				", A." + Stg1RawClauseRecord.FIELD_OPTIONAL_STATUS +
				", A." + Stg1RawClauseRecord.FIELD_OPTIONAL_CONDITIONS +
				", A." + Stg1RawClauseRecord.FIELD_OFFICIAL_CLAUSE_TITLE +
				", C." + Stg1OfficialClauseRecord.FIELD_TITLE_IN_CONTENT + // CJ-193
				", A." + Stg1RawClauseRecord.FIELD_DFARS_ACTIVITY +		
				" FROM " + Stg1RawClauseRecord.TABLE_STG1_RAW_CLAUSES + " A\n" +
				"LEFT OUTER JOIN Clause_Sections B ON (B.Clause_Section_Code = A.Section)\n" +
				// "JOIN Stg1_Official_Clause C ON (C.Src_Doc_Id = ? AND (C.Clause_Number = A.Clause_Number OR C.Clause_Number LIKE CONCAT(A.Clause_Number, ' %')))\n" +
				"JOIN Stg1_Official_Clause C ON (C.Src_Doc_Id = ? AND UPPER(C.Clause_Number) = UPPER(A.Clause_Number))\n" +
				"LEFT OUTER JOIN Stg2_Clause_Conditions D ON (D.Stg1_Clause_Id = A.Stg1_Clause_Id)\n" +
				"WHERE A.Src_Doc_Id = ?\n" +
				"ORDER BY A.Clause_Number, A.Sheet_Number DESC, A.Stg1_Clause_Id";
				
		return sSql;
	}
	
	/*
	private String getSql1_old() {
		String sSql = 
			"SELECT \n" + // 1 clause_version_id,
				"o.clause_number clause_name,\n" + 
				"ifnull((\n" +
					"SELECT clause_section_id\n" +
			        "FROM Clause_Sections\n" +
			        "WHERE Clause_Section_Code = upper(trim(r.section))),9) clause_section_id,\n" +
			    "case when IBR_OR_FULL_TEXT = 'FULL TEXT' then 'F' else 'R' end inclusion_cd,\n" +
			    "1 regulation_id,\n" +
			    "o.title clause_title,\n" +
			    "ifNull(Content_HTML_Processed, Content_HTML) clause_data,\n" + //"convert(ifNull(Content_HTML_Processed, Content_HTML) using latin1) clause_data,\n" +
				"uri clause_url,\n" + 
				"concat(\n" +
					"substring_index(eff_date,' ',-1), '-',\n" +
			        "case substring_index(eff_date,' ',1)\n" +
						"when 'JAN' then '01'\n" +
			            "when 'FEB' then '02'\n" +
			            "when 'MAR' then '03'\n" +
			            "when 'APR' then '04'\n" +
			            "when 'MAY' then '05'\n" +
			            "when 'JUN' then '06'\n" +
			            "when 'JUL' then '07'\n" +
			            "when 'JULY' then '07'\n" +
			            "when 'AUG' then '08'\n" +
			            "when 'SEP' then '09'\n" +
			            "when 'OCT' then '10'\n" +
			            "when 'NOV' then '11'\n" +
			            "when 'DEC' then '12'\n" +
					"end, '-01'\n" +
			    ") effective_date,\n" +
			    "ifnull(c.processed_clause, r.conditions) clause_conditions,\n" +
			    "r.rule clause_rule,\n" +
			    "r.additional_conditions,\n" +
			    "r.Stg1_Clause_Id,\n" +
			    "o.Stg1_Official_Clause_Id\n" +
			"FROM Stg1_Raw_Clauses r\n" +
			"JOIN (\n" +
			    "SELECT\n" + 
					"case when effective_date regexp '[0-9][0-9]$' then effective_date end eff_date, o.*\n" +
			    "FROM (\n" +
			        "SELECT\n" +
					"case\n" + 
						"when substring_index(substring_index(content, ')', 1),'(',-1) regexp '[0-9][0-9]$'\n" +
							 "then substring_index(substring_index(content, ')', 1),'(',-1)\n" +
			            "else substring_index(substring_index(content, ')', 2),'(',-1)\n" +
					"end effective_date,\n" +
					"o.*\n" +
			        "FROM Stg1_Official_Clause o\n" +
					"WHERE o.Src_Doc_Id = ?\n" +
				") as o\n" +
			") AS o ON o.clause_number = upper(r.clause_number)\n" +
			"LEFT JOIN (\n" +
				"SELECT\n" +
					"Stg1_Clause_Id,\n" +
					"GROUP_CONCAT(\n" +
						"case\n" + 
							"when real_question_name is not null\n" + 
								"then concat(item_id, ' ', real_question_name,\n" + 
									"case when operator = '=' then ' IS: ' else ' IS: NOT ' end,\n" + 
									"ifnull(real_value,value))\n" +
							"when value = 'INCLUDED' and question_name regexp '([0-9]+)\\.+'\n" +
								"then concat(item_id, ' ', question_name,\n" + 
									"case when operator = '=' then ' IS: ' else ' IS: NOT ' end,\n" + 
									"ifnull(real_value,value))\n" +
							"when real_value is not null\n" + 
								"then concat(item_id, ' ', question_name,\n" + 
									"case when operator = '=' then ' IS: ' else ' IS: NOT ' end,\n" + 
									"ifnull(real_value,value))\n" +
							"else c.condition_text\n" + 
						"end order by item_id\n" +
						"SEPARATOR ' ')\n" + 
					"processed_clause\n" +
				"FROM (\n" +
					"SELECT\n" +
						"Stg1_Clause_Id,\n" + // 1
						"item_id,\n" + // 2
						"operator,\n" + // 3
						"replace(replace(question_name,']',''),'[','') question_name,\n" + // 4
						"replace(replace(real_question_name,']',''),'[','') real_question_name,\n" + // 5
						"condition_text,\n" + // 6
						"value,\n" + // 7
						"real_value\n" + // 8
					"FROM Stg2_Clause_Conditions\n" +
					"WHERE Stg1_Clause_Id IN (SELECT Stg1_Clause_Id FROM Stg1_Raw_Clauses WHERE Src_Doc_Id = ?)\n" +
				") c\n" +
				"GROUP BY Stg1_Clause_Id\n" +
			") AS c ON r.Stg1_Clause_Id = c.Stg1_Clause_Id\n" +
			"WHERE r.Src_Doc_Id = ?\n" +
			"ORDER BY o.clause_number";
		return sSql;
	}
	*/
	
	private String getSqlAlt() {
		String sSql = "SELECT concat(C.Clause_Number, ' ', C.alt_number) clause_name -- A.Clause_Number Clause_Name\n" +
				", B.Clause_Section_Id Clause_Section_Id -- A.Section,\n" + // IFNULL(B.Clause_Section_Id, 1)
				", case when A.IBR_Or_Full_Text = 'FULL TEXT' then 'F' else 'R' end inclusion_cd -- A.IBR_Or_Full_Text\n" +
				", case when A.Sheet_Number = 0 then 1 else 2 end regulation_id\n" + // , 1 regulation_id
				", E.Title clause_title\n" +
				", CONCAT(IFNULL(C.Instructions, ''), IFNULL(C.Content_HTML_Processed, IFNULL(C.Content_Html_FillIns, C.Content_HTML))) clause_data\n" +
				", E.uri clause_url\n" +
				", A.Effective_Clause_Date Effective_Date\n" +
				", IFNULL(D.Condition_Text_Processed, D.condition_text) clause_conditions -- IFNULL(c.processed_clause, r.conditions) clause_conditions\n" +
				", A.Rule clause_rule\n" +
				", A.Additional_Conditions\n" +
				", A.Stg1_Clause_Id\n" +
				", C.Stg1_Official_Clause_Alt_Id\n" +
				// ", A.Sheet_Number, A.Row_Number\n" +
				", case\n" +
				"	when substring_index(substring_index(IFNULL(C.Instructions, ''), ')', 1),'(',-1) regexp '[0-9][0-9]$'\n" +
				"	then substring_index(substring_index(IFNULL(C.Instructions, ''), ')', 1),'(',-1)\n" +
				"   else substring_index(substring_index(IFNULL(C.Instructions, ''), ')', 2),'(',-1)\n" +
				"end effective_date2\n" +
				", A.Required_Or_Optional, A.Editable, A.Provision_Or_Clause" +
				", A.Sheet_Number, A.Row_Number" +
				", A.Clause AS Raw_Clause_Title" +
				", A." + Stg1RawClauseRecord.FIELD_COMMERCIAL_STATUS + "\n" +
				", A." + Stg1RawClauseRecord.FIELD_OPTIONAL_STATUS + "\n" +
				", A." + Stg1RawClauseRecord.FIELD_OPTIONAL_CONDITIONS + "\n" +
				", A." + Stg1RawClauseRecord.FIELD_OFFICIAL_CLAUSE_TITLE + "\n" +
				//", E." + Stg1OfficialClauseRecord.FIELD_TITLE_IN_CONTENT + "\n" + // CJ-193
				", A." + Stg1RawClauseRecord.FIELD_DFARS_ACTIVITY + "\n" +
				"FROM " + Stg1RawClauseRecord.TABLE_STG1_RAW_CLAUSES + " A\n" +
				"LEFT OUTER JOIN Clause_Sections B ON (B.Clause_Section_Code = A.Section)\n" +
				"JOIN Stg1_Official_Clause_Alt C ON (C.Src_Doc_Id = ? AND UPPER(concat(C.Clause_Number, ' ', C.alt_number)) = UPPER(A.Clause_Number))\n" +
				"LEFT OUTER JOIN Stg2_Clause_Conditions D ON (D.Stg1_Clause_Id = A.Stg1_Clause_Id)\n" + // CJ-1340
				// "JOIN Stg2_Clause_Conditions D ON (D.Stg1_Clause_Id = A.Stg1_Clause_Id)\n" +
				"JOIN Stg1_Official_Clause E ON (E.Src_Doc_Id = C.Src_Doc_Id AND E.Clause_Number = C.Clause_Number)\n" +
				"WHERE A.Src_Doc_Id = ?\n" +
				"ORDER BY 1, A.Sheet_Number DESC, A.Stg1_Clause_Id";
		return sSql;
	}
	
	/*
	private String getSqlAlt_Old() {
		String sSql = 
			"SELECT\n" + 
				"clause_name,\n" + 
			    "ifnull((SELECT clause_section_id FROM Clause_Sections WHERE Clause_Section_Code = upper(trim(r.section))),9) clause_section_id,\n" +
			    "case when IBR_OR_FULL_TEXT = 'FULL TEXT' then 'F' else 'R' end inclusion_cd,\n" +
			    "1 regulation_id,\n" +
			    "o.title clause_title,\n" +
			    "concat(instructions, ' ', alt_Content_HTML) clause_data,\n" + // "convert(concat(instructions, ' ', alt_Content_HTML) using latin1) clause_data,\n" +
			    "uri clause_url,\n" + 
				"concat(\n" +
					"substring_index(eff_date,' ',-1), '-',\n" +
			        "case substring_index(eff_date,' ',1)\n" +
						"when 'JAN' then '01'\n" +
			            "when 'FEB' then '02'\n" +
			            "when 'MAR' then '03'\n" +
			            "when 'APR' then '04'\n" +
			            "when 'MAY' then '05'\n" +
			            "when 'JUN' then '06'\n" +
			            "when 'JUL' then '07'\n" +
						"when 'JULY' then '07'\n" +
			            "when 'AUG' then '08'\n" +
			            "when 'SEP' then '09'\n" +
			            "when 'OCT' then '10'\n" +
			            "when 'NOV' then '11'\n" +
			            "when 'DEC' then '12'\n" +
					"end, '-01'\n" +
			    ") effective_date,\n" +
				"ifnull(c.processed_clause, r.conditions) clause_conditions,\n" +
			    "r.rule clause_rule,\n" +
			    "r.additional_conditions,\n" +
			    "r.Stg1_Clause_Id,\n" +
			    "o.Stg1_Official_Clause_Alt_Id\n" +
			"FROM Stg1_Raw_Clauses r\n" + 
			"JOIN (\n" +
			    "SELECT\n" + 
					"case when effective_date regexp '[0-9][0-9]$' then effective_date end eff_date, o.*\n" +
			    "FROM (\n" +
			        "SELECT\n" + 
					"case\n" + 
						"when substring_index(substring_index(instructions, ')', 1),'(',-1) regexp '[0-9][0-9]$'\n" +
						"then substring_index(substring_index(instructions, ')', 1),'(',-1)\n" +
			            "else\n" +
							"substring_index(substring_index(instructions, ')', 2),'(',-1)\n" +
					"end effective_date,\n" +
			        "concat(o.clause_number, ' ', alt_number) clause_name,\n" +
					"o.*, a.alt_number, ifNull(a.Content_HTML_Processed, a.Content_HTML) alt_Content_HTML, a.instructions, a.Stg1_Official_Clause_Alt_Id\n" +
			        "FROM Stg1_Official_Clause_Alt a\n" +
			        "JOIN Stg1_Official_Clause o on (a.clause_number = o.clause_number and o.Src_Doc_Id = ?)\n" +
			        "WHERE a.Src_Doc_Id = ?\n" +
				") as o\n" +
			") as o on (o.clause_name = r.clause_number)\n" +
			"LEFT JOIN (\n" +
				"SELECT\n" + 
					"Stg1_Clause_Id,\n" +
					"GROUP_CONCAT(\n" +
						"case\n" + 
							"when real_question_name is not null\n" + 
								"then concat(item_id, ' ', real_question_name,\n" + 
									"case when operator = '=' then ' IS: ' else ' IS: NOT ' end,\n" + 
									"ifnull(real_value,value))\n" +
							"when value = 'INCLUDED' and question_name regexp '([0-9]+)\\.+'\n" +
								"then concat(item_id, ' ', question_name,\n" + 
									"case when operator = '=' then ' IS: ' else ' IS: NOT ' end,\n" + 
									"ifnull(real_value,value))\n" +
							"when real_value is not null\n" + 
								"then concat(item_id, ' ', question_name,\n" + 
									"case when operator = '=' then ' IS: ' else ' IS: NOT ' end,\n" + 
									"ifnull(real_value,value))\n" +
							"else c.condition_text\n" + 
						"end order by item_id\n" +
						"SEPARATOR ' ') processed_clause\n" +
				"FROM (\n" +
					"SELECT\n" + 
						"Stg1_Clause_Id, item_id, operator, replace(replace(question_name,']',''),'[','') question_name, replace(replace(real_question_name,']',''),'[','') real_question_name, condition_text, value, real_value\n" +
					"FROM Stg2_Clause_Conditions\n" +
			        "WHERE Stg1_Clause_Id IN (SELECT Stg1_Clause_Id FROM Stg1_Raw_Clauses WHERE Src_Doc_Id = ?)\n" +
				") c\n" +
				"group by Stg1_Clause_Id\n" +
			") as c on r.Stg1_Clause_Id = c.Stg1_Clause_Id\n" +
			"WHERE r.Src_Doc_Id = ?\n" +
			"ORDER BY r.clause_number";
		
		return sSql;
	}
	*/
	
/*
SELECT 1 clause_version_id,
	o.clause_number clause_name, 
	ifnull((
		SELECT clause_section_id
        FROM Clause_Sections
        WHERE Clause_Section_Code = upper(trim(r.section))),9) clause_section_id,
    case when IBR_OR_FULL_TEXT = 'FULL TEXT' then 'F' else 'R' end inclusion_cd,
    1 regulation_id,
    o.title clause_title,
    convert(ifNull(Content_HTML_Processed, Content_HTML) using latin1) clause_data, 
	uri clause_url, 
	concat(
		substring_index(eff_date,' ',-1), '-',
        case substring_index(eff_date,' ',1)
			when 'JAN' then '01'
            when 'FEB' then '02'
            when 'MAR' then '03'
            when 'APR' then '04'
            when 'MAY' then '05'
            when 'JUN' then '06'
            when 'JUL' then '07'
            when 'JULY' then '07'
            when 'AUG' then '08'
            when 'SEP' then '09'
            when 'OCT' then '10'
            when 'NOV' then '11'
            when 'DEC' then '12'
		end, '-01'
    ) effective_date,
    ifnull(c.processed_clause, r.conditions) clause_conditions,
    r.rule clause_rule,
    r.Stg1_Clause_Id,
    o.Stg1_Official_Clause_Id
FROM Stg1_Raw_Clauses r 
JOIN (
    SELECT 
		case when effective_date regexp '[0-9][0-9]$' then effective_date end eff_date, o.*
    FROM (
        SELECT
		case 
			when substring_index(substring_index(content, ')', 1),'(',-1) regexp '[0-9][0-9]$'
				 then substring_index(substring_index(content, ')', 1),'(',-1)
            else substring_index(substring_index(content, ')', 2),'(',-1)
		end effective_date,
		o.*
        FROM Stg1_Official_Clause o
        WHERE o.Src_Doc_Id = 25
	) as o
) AS o ON o.clause_number = upper(r.clause_number)
LEFT JOIN (
	SELECT
		Stg1_Clause_Id,
		GROUP_CONCAT(
			case 
				when real_question_name is not null 
					then concat(item_id, ' ', real_question_name, 
						case when operator = '=' then ' IS: ' else ' IS: NOT ' end, 
						ifnull(real_value,value))
				when value = 'INCLUDED' and question_name regexp '([0-9]+)\\.+'
					then concat(item_id, ' ', question_name, 
						case when operator = '=' then ' IS: ' else ' IS: NOT ' end, 
						ifnull(real_value,value))
				when real_value is not null 
					then concat(item_id, ' ', question_name, 
						case when operator = '=' then ' IS: ' else ' IS: NOT ' end, 
						ifnull(real_value,value))
				else c.condition_text 
			end order by item_id
			SEPARATOR ' ') 
		processed_clause
	FROM (
		SELECT
			Stg1_Clause_Id, item_id, operator, replace(replace(question_name,']',''),'[','') question_name, replace(replace(real_question_name,']',''),'[','') real_question_name, condition_text, value, real_value
		FROM Stg2_Clause_Conditions
        WHERE Stg1_Clause_Id IN (SELECT Stg1_Clause_Id FROM Stg1_Raw_Clauses WHERE Src_Doc_Id = 23)
	) c
	GROUP BY Stg1_Clause_Id
) AS c ON r.Stg1_Clause_Id = c.Stg1_Clause_Id
WHERE r.Src_Doc_Id = 23
;

-- insert into clauses (clause_version_id, clause_name, clause_section_id, inclusion_cd, regulation_id, clause_title, clause_data, clause_url, effective_date, clause_conditions, clause_rule, additional_conditions)
SELECT 
	clause_name, 
    ifnull((SELECT clause_section_id FROM Clause_Sections WHERE Clause_Section_Code = upper(trim(r.section))),9) clause_section_id,
    case when IBR_OR_FULL_TEXT = 'FULL TEXT' then 'F' else 'R' end inclusion_cd,
    1 regulation_id,
    o.title clause_title,
    convert(concat(instructions, ' ', alt_Content_HTML) using latin1) clause_data,
    uri clause_url, 
	concat(
		substring_index(eff_date,' ',-1), '-',
        case substring_index(eff_date,' ',1)
			when 'JAN' then '01'
            when 'FEB' then '02'
            when 'MAR' then '03'
            when 'APR' then '04'
            when 'MAY' then '05'
            when 'JUN' then '06'
            when 'JUL' then '07'
			when 'JULY' then '07'
            when 'AUG' then '08'
            when 'SEP' then '09'
            when 'OCT' then '10'
            when 'NOV' then '11'
            when 'DEC' then '12'
		end, '-01'
    ) effective_date,
	ifnull(c.processed_clause, r.conditions) clause_conditions,
    r.rule clause_rule,
    r.additional_conditions,
    r.Stg1_Clause_Id,
    o.Stg1_Official_Clause_Alt_Id
FROM Stg1_Raw_Clauses r 
JOIN (
    SELECT 
		case when effective_date regexp '[0-9][0-9]$' then effective_date end eff_date, o.*
    FROM (
        SELECT 
		case 
			when substring_index(substring_index(instructions, ')', 1),'(',-1) regexp '[0-9][0-9]$'
			then substring_index(substring_index(instructions, ')', 1),'(',-1)
            else
				substring_index(substring_index(instructions, ')', 2),'(',-1)
		end effective_date,
        concat(o.clause_number, ' ', alt_number) clause_name,
		o.*, a.alt_number, ifNull(a.Content_HTML_Processed, a.Content_HTML) alt_Content_HTML, a.instructions,
        a.Stg1_Official_Clause_Alt_Id
        FROM Stg1_Official_Clause_Alt a
        JOIN Stg1_Official_Clause o on (a.clause_number = o.clause_number and o.Src_Doc_Id = 25)
        WHERE a.Src_Doc_Id = 25
	) as o
) as o on (o.clause_name = r.clause_number)
LEFT JOIN (
	SELECT 
		Stg1_Clause_Id,
		GROUP_CONCAT(
			case 
				when real_question_name is not null 
					then concat(item_id, ' ', real_question_name, 
						case when operator = '=' then ' IS: ' else ' IS: NOT ' end, 
						ifnull(real_value,value))
				when value = 'INCLUDED' and question_name regexp '([0-9]+)\\.+'
					then concat(item_id, ' ', question_name, 
						case when operator = '=' then ' IS: ' else ' IS: NOT ' end, 
						ifnull(real_value,value))
				when real_value is not null 
					then concat(item_id, ' ', question_name, 
						case when operator = '=' then ' IS: ' else ' IS: NOT ' end, 
						ifnull(real_value,value))
				else c.condition_text 
			end order by item_id
			SEPARATOR ' ') processed_clause
	FROM (
		SELECT 
			Stg1_Clause_Id, item_id, operator, replace(replace(question_name,']',''),'[','') question_name, replace(replace(real_question_name,']',''),'[','') real_question_name, condition_text, value, real_value
		FROM Stg2_Clause_Conditions
        WHERE Stg1_Clause_Id IN (SELECT Stg1_Clause_Id FROM Stg1_Raw_Clauses WHERE Src_Doc_Id = 23)
	) c
	group by Stg1_Clause_Id
) as c on r.Stg1_Clause_Id = c.Stg1_Clause_Id
WHERE r.Src_Doc_Id = 23
;
*/

	// ======================================================
	public ParsedClauseRecord findByName(String psName) {
		for (ParsedClauseRecord oRecord : this) {
			if (CommonUtils.isSame(oRecord.clauseName, psName, true))
				return oRecord;
		}
		return null;
	}
	
	private void resolveIssues(ProduceQuestion oProduceQuestion, Connection conn, FileOutputStream fileOutputStream, boolean bReport)
			throws IOException, SQLException {
		if (bReport) {
			String sHtml = "<style>table { border:#d0d0d0 solid 1px; }\ntd, th { padding: 2px; border:#d0d0d0 solid 1px; } td {	font-family: Consolas, Andale Mono, Lucida Console, Lucida Sans Typewriter, Monaco, Courier New, monospace; } </style>\n";
			fileOutputStream.write(sHtml.getBytes());
		}

		Stg3ClauseCorrectionSet oStg3ClauseCorrectionSet = new Stg3ClauseCorrectionSet();
		oStg3ClauseCorrectionSet.load(conn, this.iTargetClauseVersionId);
		
		int iTotal = 0, iError = 0, iCorreted = 0, iMerged = 0;
		
		ArrayList<ParsedClauseRecord> aDupes = new ArrayList<ParsedClauseRecord>();
		ArrayList<String> aProcessedNames = new ArrayList<String>(); 

		for (ParsedClauseRecord oRecord : this) {
			iTotal++;
			Stg3ClauseCorrectionRecord oStg3ClauseCorrectionRecord = oStg3ClauseCorrectionSet.findByName(oRecord.clauseName);
			String sLog = oRecord.resolveIssues(oProduceQuestion, this, oStg3ClauseCorrectionRecord, bReport);
			if (!sLog.isEmpty()) {
				fileOutputStream.write(sLog.getBytes());
			}
			if (oRecord.isExpressionValid == false)
				iError++;
			if (oRecord.changed)
				iCorreted++;
			
			int iPos = aProcessedNames.indexOf(oRecord.clauseName);
			if (iPos >= 0) {
				if (oRecord.isExpressionValid) {
					ParsedClauseRecord oBase = this.get(iPos);
					aDupes.add(oRecord);
					oBase.merge(oRecord);
					iMerged++;
				} else {
					aDupes.add(oRecord); // Remove dupe, don't process dupe under any circumstance. Potential for dupe clauses in db.
					System.out.println("Skipped merging duplicate " + oRecord.clauseName + " due to its invalid condition(s).");
				}
			}
			aProcessedNames.add(oRecord.clauseName);
		}
		
		for (ParsedClauseRecord oDupe : aDupes) {
			this.remove(oDupe);
		}
		
		String sSummary = (bReport ? "\n<h3>" :"\n-- ") +
				"ProduceClause.resolveIssues() Summary: Total(" + iTotal + "); Merged(" + iMerged + "); Corrected(" + iCorreted + "); Error(" + iError + ")" +
				(bReport ? "</h3>" : "") +
				"\n";
		fileOutputStream.write(sSummary.getBytes());
	}
	
	public void generateSql(FileOutputStream fileOutputStream, boolean bReport) throws IOException {
		PreparedStatement psChangesRecord = null;
		boolean isSameVersion = false;
		if ((this.iPreviousClauseVersionId != null) && (this.iPreviousClauseVersionId.equals(this.iTargetClauseVersionId)))
			isSameVersion = true;

		String sHtmlBreak = (bReport ? "<br/>\n" : "");
		if (bReport) {
			String sHtml = "<style>table { border:#d0d0d0 solid 1px; }\ntd, th { padding: 2px; border:#d0d0d0 solid 1px; } td {	font-family: Consolas, Andale Mono, Lucida Console, Lucida Sans Typewriter, Monaco, Courier New, monospace; } </style>\n";
			fileOutputStream.write(sHtml.getBytes());
		}
		
		ClauseVersionChangeTable oClauseVersionChangeTable = new ClauseVersionChangeTable();
		ClauseVersionChangeRecord oClauseVersionChangeRecord = new ClauseVersionChangeRecord();

		ClauseTable foundExistingClauses = new ClauseTable(); 
		ClauseTable oClauseTable;
		if (this.iPreviousClauseVersionId == null)
			oClauseTable = new ClauseTable();
		else
			oClauseTable = ClauseTable.getInstance(this.iPreviousClauseVersionId, this.conn);
		
		try {
			if (this.parsedPrescriptionSet.isSqlGenerated() == false) {
				String sSql = this.parsedPrescriptionSet.genSql();
				fileOutputStream.write(sSql.getBytes());
			}
			
			System.out.println("Processing Clauses");
			String sSql = "/* ===============================================\n" + sHtmlBreak
					+ " * Clause\n" + sHtmlBreak
					+ "   =============================================== */\n" + sHtmlBreak;
			fileOutputStream.write(sSql.getBytes());
			int iInserted = 0, iUpdated = 0;
			for (ParsedClauseRecord oRecord : this) {
				oRecord.fixNulls();
				oClauseVersionChangeRecord.clear(this.iTargetClauseVersionId, this.iPreviousClauseVersionId);
				sSql = "";
				ClauseRecord oClauseRecord = null;
				if (isSameVersion) {
					oClauseRecord = oClauseTable.findById(oRecord.clauseId);
					if (oClauseRecord == null)
						oClauseRecord = oClauseTable.findByName(oRecord.clauseName, foundExistingClauses);
				} else
					oClauseRecord = oClauseTable.findByName(oRecord.clauseName, foundExistingClauses);
				
				if (oClauseRecord == null) {
					if (bReport)
						sSql = oRecord.getDifferences(this.iTargetClauseVersionId, this.parsedPrescriptionSet, oClauseVersionChangeRecord);
					else
						//sSql = oRecord.getInsertSql(this.iTargetClauseVersionId, this.parsedPrescriptionSet, oClauseVersionChangeRecord);
						sSql = oRecord.getInsertSql_wFilter(this.iTargetClauseVersionId, this.parsedPrescriptionSet, oClauseVersionChangeRecord);
					iInserted++;
				} else {
					if (bReport)
						sSql = oRecord.getDifferences(this.iTargetClauseVersionId, this.parsedPrescriptionSet, oClauseRecord, oClauseVersionChangeRecord);
					else {
						//sSql = oRecord.getUpdateSql(this.iTargetClauseVersionId, this.parsedPrescriptionSet, oClauseRecord, oClauseVersionChangeRecord);
						sSql = oRecord.getUpdateSql_wFilter(this.iTargetClauseVersionId, this.parsedPrescriptionSet, oClauseRecord, oClauseVersionChangeRecord);
						if (isSameVersion) {
							iUpdated++;
						}
						else {							
							//sSql = oRecord.getInsertSql(this.iTargetClauseVersionId, this.parsedPrescriptionSet, null);
							sSql = oRecord.getInsertSql_wFilter(this.iTargetClauseVersionId, this.parsedPrescriptionSet, null);
							iInserted++;
						}
					}
					foundExistingClauses.add(oClauseRecord);
				}
				if (!sSql.isEmpty()) {
					fileOutputStream.write('\n');
					fileOutputStream.write(sSql.getBytes()); // System.out.println(sSql + "\n");
					fileOutputStream.write('\n');
				}
				if (oClauseVersionChangeRecord.isNotEmpty()) {
					oClauseVersionChangeTable.add(oClauseVersionChangeRecord);
					oClauseVersionChangeRecord = new ClauseVersionChangeRecord();
				}
			}
			
			int iRemoved = 0;
			System.out.println("Removed Clauses++++++++++++++++++++++++++++++++++");
			oClauseVersionChangeRecord.clear(this.iTargetClauseVersionId, this.iPreviousClauseVersionId);
			for (ClauseRecord oClauseRecord : oClauseTable) {
				/*
				ClauseRecord oFindRecord;
				if (isSameVersion)
					oFindRecord = foundExistingClauses.findById(oClauseRecord.getId());
				else
					oFindRecord = foundExistingClauses.findByName(oClauseRecord.getClauseName());
				*/
				if ((!foundExistingClauses.contains(oClauseRecord))) { //  && (oFindRecord == null)
					if (bReport) {
						sSql = ParsedClauseRecord.getDeleteHtml(oClauseRecord, oClauseVersionChangeRecord);
					} else {
						sSql = ParsedClauseRecord.getDeleteSql_wFilter(oClauseRecord, this.iTargetClauseVersionId, oClauseVersionChangeRecord);
						if (isSameVersion == false)
							sSql = "";
					}
					oClauseVersionChangeTable.add(oClauseVersionChangeRecord);
					oClauseVersionChangeRecord = new ClauseVersionChangeRecord();
					oClauseVersionChangeRecord.clear(this.iTargetClauseVersionId, this.iPreviousClauseVersionId);
					System.out.println("'" + oClauseRecord.getClauseName() + "',");
					iRemoved++;
					if (!sSql.isEmpty()) {
						fileOutputStream.write('\n');
						fileOutputStream.write(sSql.getBytes());
					}
				}
			}
			
			if ((this.iPreviousClauseVersionId != null) && (!isSameVersion) && (oClauseVersionChangeTable.size() > 0)) {
				sSql = "\n\n-- Clause Version Change Scripts Start";
				for (ClauseVersionChangeRecord oRecord : oClauseVersionChangeTable) {
					sSql += oRecord.generateInsertSql();
				}
				sSql += "\n-- Clause Version Change Scripts End\n\n";
				fileOutputStream.write(sSql.getBytes());
			}
			
			System.out.println("+++++++++++++++++++++++++++++++++++++++++++++++++");
			System.out.println("Done: Total(" + this.size() + "), Inserts(" + iInserted + "), Updates(" + iUpdated + "), Removed(" + iRemoved + ")");
		}
		finally {
			SqlUtil.closeInstance(psChangesRecord);
		}
	}

}
