package hgs.cpm.versioning;

import gov.dod.cls.db.ClauseVersionChangeRecord;
import gov.dod.cls.db.ClauseVersionChangeTable;
import gov.dod.cls.db.PrescriptionTable;
import gov.dod.cls.db.QuestionConditionRecord;
import gov.dod.cls.db.QuestionConditionTable;
import gov.dod.cls.db.QuestionRecord;
import gov.dod.cls.db.QuestionTable;
import gov.dod.cls.utils.CommonUtils;
import gov.dod.cls.utils.sql.SqlUtil;
import hgs.cpm.db.ParsedPrescriptionSet;
import hgs.cpm.db.ParsedQuestionChoiceSet;
import hgs.cpm.db.ParsedQuestionRecord;
import hgs.cpm.question.util.Stg2QuestionRecord;

import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class ProduceQuestion extends ArrayList<ParsedQuestionRecord> {

	private static final long serialVersionUID = -2339514404390261762L;

	private Connection conn;
	private Integer iTargetClauseVersionId;
	private Integer iPreviousClauseVersionId;
	private Integer iQuestionSrcDocId;
	//private String sqlOutputPath;
	
	private ParsedQuestionChoiceSet parsedQuestionChoiceSet = new ParsedQuestionChoiceSet();
	private ParsedPrescriptionSet parsedPrescriptionSet;
	
	private PrescriptionTable prescriptionTable;
	
	public ProduceQuestion (Connection conn, Integer iTargetClauseVersionId, Integer iPreviousClauseVersionId, 
			Integer iQuestionSrcDocId, ParsedPrescriptionSet parsedPrescriptionSet, PrescriptionTable poPrescriptionTable) {
		this.conn = conn;
		this.iTargetClauseVersionId = iTargetClauseVersionId;
		this.iPreviousClauseVersionId = iPreviousClauseVersionId;
		this.iQuestionSrcDocId = iQuestionSrcDocId;
		//this.sqlOutputPath = sqlOutputPath;
		this.parsedPrescriptionSet = parsedPrescriptionSet;
		this.prescriptionTable = poPrescriptionTable;
	}
	
	/*
	public void run(FileOutputStream fileOutputStream, boolean bReport) throws SQLException, IOException {
		this.loadParsedRecord();
		this.generateSql(fileOutputStream, bReport); // this.storeSql();
	}
	*/
	
	public void loadParsedRecord() throws SQLException {
		this.clear();
		this.parsedQuestionChoiceSet.load(this.conn, this.iQuestionSrcDocId);
		String sSql = 
			"select q.Stg2_Question_Id,\n" +
				"q.question_name,\n" +
				"case\n" + 
					"when q.question_type = 'Numeric' then 'N'\n" +
					"when q.question_type = 'Single Answer' then '1'\n" +
					"when q.question_type = 'Multiple Answer' then 'M'\n" +
					"when q.question_type = 'Boolean' then 'B'\n" +
				"end question_type,\n" +
				"q.question_text,\n" + // replace(question_text,'''','''''') 
				"q.group_name,\n" + // "g.question_group\n" +
				"q.sequence, q.rule, q.Normal_Rule,\n" +
				"q.Parent_Stg2_Question_Id, p.question_name parent_question_name\n" +
				", q." + Stg2QuestionRecord.FIELD_DEFAULT_BASELINE + "\n" + // CJ-584
			"from " + Stg2QuestionRecord.TABLE_STG2_QUESTION + " q\n" +
			// "left join question_group_ref g on (g.question_group_name = trim(q.group_name))\n" +
			"left join " + Stg2QuestionRecord.TABLE_STG2_QUESTION + " p on (p.Stg2_Question_Id = q.Parent_Stg2_Question_Id)\n" +
			"where q.Src_Doc_Id = ?\n" +
			"order by q.Parent_Stg2_Question_Id, q.Stg2_Question_Id"; // question_name
		PreparedStatement ps1 = null;
		ResultSet rs1 = null;
		try {
			ps1 = conn.prepareStatement(sSql);
			ps1.setInt(1, this.iQuestionSrcDocId);
			rs1 = ps1.executeQuery();
			while (rs1.next()) {
				ParsedQuestionRecord oRecord = new ParsedQuestionRecord();
				oRecord.Stg2QuestionId = rs1.getInt(1);
				oRecord.questionCode = rs1.getString(2);
				oRecord.questionType = rs1.getString(3);
				oRecord.questionText = rs1.getString(4);
				oRecord.questionGroup = rs1.getString(5);
				oRecord.sequence = rs1.getInt(6); // q.sequence
				oRecord.originalConditionText = rs1.getString(7); // q.rule
				oRecord.conditionToQuestion = rs1.getString(8); // q.Normal_Rule\n" +
				oRecord.parentStg2QuestionId = CommonUtils.toInteger(rs1.getObject(9));
				oRecord.parentQuestionName = rs1.getString(10);
				oRecord.defaultBaseline = rs1.getBoolean(Stg2QuestionRecord.FIELD_DEFAULT_BASELINE); // CJ-584
				// q.rule
				// q.Normal_Rule\n" +
				this.add(oRecord);
				
				this.parsedQuestionChoiceSet.subset(oRecord.Stg2QuestionId, oRecord.choices);
			}
		} finally {
			SqlUtil.closeInstance(rs1, ps1);
		}
		this.buildHierarchy();
	}
	
	public void buildHierarchy() {
		for (ParsedQuestionRecord oChildRecord : this) {
			if (oChildRecord.parentStg2QuestionId != null) {
				ParsedQuestionRecord oParentRecord = this.findById(oChildRecord.parentStg2QuestionId);
				if (oParentRecord != null) {
					oParentRecord.addChild(oChildRecord);
				} else {
					System.out.println("ProduceQuestion.buildHierarchy() Unable to locate parent question id(" + oChildRecord.parentStg2QuestionId + ") for " + oChildRecord.questionCode);
				}
			}
		}
	}
	
	public ParsedQuestionRecord findById(Integer parentStg2QuestionId) {
		if (parentStg2QuestionId != null)
			for (ParsedQuestionRecord oParsedQuestionRecord : this) {
				if (oParsedQuestionRecord.Stg2QuestionId.equals(parentStg2QuestionId))
					return oParsedQuestionRecord;
			}
		return null;
	}
	
	public ParsedQuestionRecord findByName(String psName) {
		for (ParsedQuestionRecord oParsedQuestionRecord : this) {
			if (CommonUtils.isSame(oParsedQuestionRecord.questionCode, psName))
				return oParsedQuestionRecord;
		}
		return null;
	}
	
	public boolean findByChoiceValue(String psChoice, ArrayList<ParsedQuestionRecord> aQuestions) {
		for (ParsedQuestionRecord oParsedQuestionRecord : this) {
			if (oParsedQuestionRecord.hasChoice(psChoice))
				aQuestions.add(oParsedQuestionRecord);
		}
		return (aQuestions.size() > 0);
	}
	
	public void generateSql(FileOutputStream fileOutputStream, boolean bReport) throws SQLException, IOException {
		boolean isSameVersion = false;
		if ((this.iPreviousClauseVersionId != null) && (this.iPreviousClauseVersionId.equals(this.iTargetClauseVersionId)))
			isSameVersion = true;

		String sHtmlBreak = (bReport ? "<br/>\n" : "");
		if (bReport) {
			String sHtml = "<style>table { border:#d0d0d0 solid 1px; }\ntd, th { padding: 2px; border:#d0d0d0 solid 1px; } td {	font-family: Consolas, Andale Mono, Lucida Console, Lucida Sans Typewriter, Monaco, Courier New, monospace; } </style>\n";
			fileOutputStream.write(sHtml.getBytes());
		}

		ClauseVersionChangeTable oClauseVersionChangeTable = new ClauseVersionChangeTable();
		ClauseVersionChangeRecord oClauseVersionChangeRecord = new ClauseVersionChangeRecord();

		try {
			ParsedQuestionChoiceSet oQuestionChoiceSet = new ParsedQuestionChoiceSet(); 
 
			if ((this.parsedPrescriptionSet.isSqlGenerated() == false) && (!bReport)) {
				String sSql = this.parsedPrescriptionSet.genSql();
				fileOutputStream.write(sSql.getBytes());
			}
			
			String sSql = "/* ===============================================\n" + sHtmlBreak
					+ " * Questions\n" + sHtmlBreak
					+ "   =============================================== */\n" + sHtmlBreak;
			fileOutputStream.write(sSql.getBytes());
 
			ArrayList<QuestionRecord> foundRecords = new ArrayList<QuestionRecord>(); 
			QuestionTable questionTable;
			QuestionConditionTable questionConditionTable;
			if (this.iPreviousClauseVersionId == null) {
				questionTable = new QuestionTable();
				questionConditionTable = new QuestionConditionTable();
			} else {
				questionTable = QuestionTable.getSubsetByClauseVersion(this.iPreviousClauseVersionId, this.conn);
				questionConditionTable = QuestionConditionTable.getQuestionSubset(questionTable, this.conn);
			}

			for (ParsedQuestionRecord oParsedQuestionRecord : this) {
				oClauseVersionChangeRecord.clear(this.iTargetClauseVersionId, this.iPreviousClauseVersionId);
				sSql = null;
				oQuestionChoiceSet = oParsedQuestionRecord.choices; // this.parsedQuestionChoiceSet.subset(oParsedQuestionRecord.Stg2QuestionId, oQuestionChoiceSet);
				QuestionRecord oQuestionRecord = questionTable.findByCode(oParsedQuestionRecord.questionCode);
				if (oQuestionRecord == null) {
					if (bReport)
						sSql = oParsedQuestionRecord.getDifferences(this.iTargetClauseVersionId, oQuestionChoiceSet, this.parsedPrescriptionSet, oClauseVersionChangeRecord);
					else
						//sSql = oParsedQuestionRecord.getInsertSql(this.iTargetClauseVersionId, oQuestionChoiceSet, this.parsedPrescriptionSet, oClauseVersionChangeRecord);
						sSql = oParsedQuestionRecord.getInsertSql_wFilter(this.iTargetClauseVersionId, oQuestionChoiceSet, this.parsedPrescriptionSet, oClauseVersionChangeRecord);
				} else {
					QuestionConditionRecord oQuestionConditionRecord = questionConditionTable.findByQuestion(oQuestionRecord);
					if (bReport)
						sSql = oParsedQuestionRecord.getDifferences(oQuestionRecord, oQuestionConditionRecord, oQuestionChoiceSet, 
								this.parsedPrescriptionSet, questionTable, this.iTargetClauseVersionId, oClauseVersionChangeRecord,
								this.prescriptionTable);
					else {
						if (isSameVersion) {
							// NOTE: Only execute getUpdateSql for same version. Otherwise, the Question_choices and Question_choice_prescriptions
							// are not added during insert.
							//sSql = oParsedQuestionRecord.getUpdateSql(oQuestionRecord, oQuestionConditionRecord, oQuestionChoiceSet, 
							sSql = oParsedQuestionRecord.getUpdateSql_wFilter(oQuestionRecord, oQuestionConditionRecord, oQuestionChoiceSet, 
									this.parsedPrescriptionSet, questionTable, this.iTargetClauseVersionId, oClauseVersionChangeRecord,
									this.prescriptionTable);
							//iUpdated++;
						} else {
							oParsedQuestionRecord.checkSqlChanges(oQuestionRecord, oQuestionConditionRecord, oQuestionChoiceSet, 
									this.parsedPrescriptionSet, questionTable, this.iTargetClauseVersionId, oClauseVersionChangeRecord,
									this.prescriptionTable);
							//sSql = oParsedQuestionRecord.getInsertSql(this.iTargetClauseVersionId, oQuestionChoiceSet, this.parsedPrescriptionSet, null);
							sSql = oParsedQuestionRecord.getInsertSql_wFilter(this.iTargetClauseVersionId, oQuestionChoiceSet, this.parsedPrescriptionSet, null);
							//iInserted++;
						}
					}
					foundRecords.add(oQuestionRecord);
				}
				if ((sSql != null) && (!sSql.isEmpty())) {
					fileOutputStream.write('\n');
					sSql += '\n';
					fileOutputStream.write(sSql.getBytes());
				}
				if (oClauseVersionChangeRecord.isNotEmpty()) {
					oClauseVersionChangeTable.add(oClauseVersionChangeRecord);
					oClauseVersionChangeRecord = new ClauseVersionChangeRecord();
				}
			}
			
			oClauseVersionChangeRecord.clear(this.iTargetClauseVersionId, this.iPreviousClauseVersionId);
			for (QuestionRecord oQuestionRecord : questionTable) {
				if (!foundRecords.contains(oQuestionRecord)) {
					QuestionConditionRecord oQuestionConditionRecord = questionConditionTable.findByQuestion(oQuestionRecord);
					if (bReport) {
						sSql = ParsedQuestionRecord.getDeleteHtml(oQuestionRecord, oQuestionConditionRecord, oClauseVersionChangeRecord);
						oClauseVersionChangeTable.add(oClauseVersionChangeRecord);
						oClauseVersionChangeRecord = new ClauseVersionChangeRecord();
						oClauseVersionChangeRecord.clear(this.iTargetClauseVersionId, this.iPreviousClauseVersionId);
					} else {
						sSql = ParsedQuestionRecord.getDeleteSql_wFilter(oQuestionRecord, oQuestionConditionRecord, oClauseVersionChangeRecord, this.iTargetClauseVersionId);
						if (isSameVersion == false)
							sSql = "";
					}
					oClauseVersionChangeTable.add(oClauseVersionChangeRecord);
					oClauseVersionChangeRecord = new ClauseVersionChangeRecord();
					oClauseVersionChangeRecord.clear(this.iTargetClauseVersionId, this.iPreviousClauseVersionId);
					if (CommonUtils.isNotEmpty(sSql)) {
						fileOutputStream.write('\n');
						sSql += '\n';
						fileOutputStream.write(sSql.getBytes());
					}
				}
			}
			
			if ((this.iPreviousClauseVersionId != null) && (!isSameVersion) && (oClauseVersionChangeTable.size() > 0)) {
				sSql = "\n\n-- Question Version Change Scripts Start";
				for (ClauseVersionChangeRecord oRecord : oClauseVersionChangeTable) {
					sSql += oRecord.generateInsertSql();
				}
				sSql += "\n-- Question Version Change Scripts End\n\n";
				fileOutputStream.write(sSql.getBytes());
			}
			
			fileOutputStream.flush();
 
			System.out.println("Done");
 
		} finally {
		}
	}
	
}
