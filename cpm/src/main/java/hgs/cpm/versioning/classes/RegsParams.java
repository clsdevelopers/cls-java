package hgs.cpm.versioning.classes;

import gov.dod.cls.db.ClauseVersionRecord;
import hgs.cpm.db.ParsedClauseRecord;

import java.util.Date;


public class RegsParams {

	public String sFacNumber = "";
	public String sDacNumber = "";
	public String sDFarsNumber = "";
	
	public Date facDate;
	public Date dacDate;
	public Date dfarsDate;
	
	// Check if the spreadsheet has provided updated values.
	public boolean bUpdateReq ( ClauseVersionRecord oClauseVersionRecord) {
		boolean bresult = false;
		
		// Ensure all values are presented in the spreadsheet.
		if ((sFacNumber.length() == 0) 
		|| (sDacNumber.length() == 0) 
		|| (sDFarsNumber.length() == 0)
		|| (facDate == null) 
		|| (dacDate == null) 
		|| (dfarsDate == null))
			return false;
		
		if ((oClauseVersionRecord.getFacNumber() == null) 
		|| (!oClauseVersionRecord.getFacNumber().equals (sFacNumber)))
			return true;

		if ((oClauseVersionRecord.getDacNumber() == null) 
		|| (!oClauseVersionRecord.getDacNumber().equals (sDacNumber)))
			return true;
		
		if ((oClauseVersionRecord.getDFarsNumber() == null) 
		|| (!oClauseVersionRecord.getDFarsNumber().equals (sDFarsNumber)))
			return true;
		
		if ((oClauseVersionRecord.getFacDate() == null) 
		|| (!oClauseVersionRecord.getFacDate().equals(facDate)))
			return true;
		
		if ((oClauseVersionRecord.getDacDate() == null) 
		|| (!oClauseVersionRecord.getDacDate().equals(dacDate)))
			return true;
		
		if ((oClauseVersionRecord.getDFarsDate() == null) 
		|| (!oClauseVersionRecord.getDFarsDate().equals(dfarsDate)))
			return true;
		
		/*
		if ((oClauseVersionRecord.getFacDate() == null) 
		|| (!dateToString(oClauseVersionRecord.getFacDate()).equals(dateToString(facDate))))
			return true;
		
		if ((oClauseVersionRecord.getDacDate() == null) 
		|| (!dateToString(oClauseVersionRecord.getDacDate()).equals(dateToString(dacDate))))
			return true;
		
		if ((oClauseVersionRecord.getDFarsDate() == null) 
		|| (!dateToString(oClauseVersionRecord.getDFarsDate()).equals(dateToString(dfarsDate))))
			return true;
		*/
			
		return bresult;
	}
	
	public static String dateToString(Date pDate) {
		if (pDate == null)
			return "NULL";
		return "'" + ParsedClauseRecord.formatDate.format(pDate) + "'";
	}
}