package hgs.cpm.versioning.classes;

import gov.dod.cls.db.ClauseVersionRecord;


public class SATParams {
	
	public String satCondition = "";
	public String sat300KRule = "";
	public String sat1MillionRule = "";
	
	// Check if the spreadsheet has provided updated values.
	public boolean bUpdateReq ( ClauseVersionRecord oClauseVersionRecord) {
		boolean bresult = false;
		
		// Ensure all values are presented in the spreadsheet.
		if ((satCondition.length() == 0) 
		|| (sat300KRule.length() == 0) 
		|| (sat1MillionRule.length() == 0))
			return false;
		
		if ((oClauseVersionRecord.getSatCondition() == null) 
		|| (!oClauseVersionRecord.getSatCondition().equals (satCondition)))
			return true;
		
		if ((oClauseVersionRecord.getSat300KRule() == null) 
		|| (!oClauseVersionRecord.getSat300KRule().equals (sat300KRule)))
			return true;
		
		if ((oClauseVersionRecord.getSat1MillionRule() == null) 
		|| (!oClauseVersionRecord.getSat1MillionRule().equals (sat1MillionRule)))
			return true;
		return bresult;
	}
}