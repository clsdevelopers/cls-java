package hgs.cpm;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.Date;
import java.util.Scanner;

import gov.dod.cls.utils.sql.SqlUtil;
import hgs.cpm.clause.ClauseExcel;
import hgs.cpm.clause.ClauseConditionsParser;
import hgs.cpm.clause.FillinCorrectionReader;
import hgs.cpm.clause.FillinCorrectionWriter;
import hgs.cpm.clause.ParseClausePrescriptionRules;
import hgs.cpm.clause.ParseClausePrescriptions;
import hgs.cpm.config.CpmConfig;
import hgs.cpm.db.ParsedPrescriptionSet;
import hgs.cpm.db.StgSrcDocumentSet;
import hgs.cpm.ecfr.EcfrDiffRpt;
import hgs.cpm.ecfr.EcfrManualClause;
import hgs.cpm.ecfr.EcfrParseFillins;
import hgs.cpm.ecfr.LoadEcfr;
import hgs.cpm.question.QuestionExcel;
import hgs.cpm.question.QuestionSequenceExcel;
import hgs.cpm.utils.StringCommons;
import hgs.cpm.versioning.GenerateVersion;
import hgs.cpm.versioning.classes.RegsParams;
import hgs.cpm.versioning.classes.SATParams;

public class App 
{
	public static CpmConfig cpmConfig = CpmConfig.getInstance();
	
	public static StgSrcDocumentSet oStgSrcDocumentSet = null;

	private static boolean bClauseExcel = false;

	private static boolean bECFR = false; 
	private static boolean bDownloadECFR = true;
	private static boolean bFullEdit = true;
	
	private static boolean bQuestionSequence = false; 
	private static boolean bQuestionExcel = false; 
	
	// Manual Fill-in Review ----------------------------
	private static boolean bGenerateFillinCorrections = false;
	private static boolean bLoadFillinCorrections = false;
	
	// ECFR Clause Data Different Report ----------------------------
	private static boolean bEcfrClauseDataDifferentReport = false; // CJ-1180

	private static boolean bGenerateVersion = false;
	
	// Generate Version ----------------------------
	private static Integer iPreviousClauseVersionId = 3; // cpmConfig.getVersionIdPreviousInteger();
	private static Integer iTargetClauseVersionId = 3; // cpmConfig.getVersionIdTargetInteger();
	private static String sTargetClsVersionName = "3.4";
	
	private static boolean bProduceReport = false;
	private static boolean bProduceClause = true;
	private static boolean bProduceQuestion = true;
	
	private static RegsParams regulationParams = new RegsParams();	// FAC, DAC, DFARS
	private static SATParams satParams = new SATParams();
	
	private static String[] aArguments = {
			"ClauseExcel", 
			"ECFR", 
			"DownloadECFR",
			"FullEdit",
			"QuestionSequence", 
			"QuestionExcel", 
			"EcfrClauseDataDifferentReport",
			"GenerateFillinCorrections", 
			"LoadFillinCorrections", 
			"GenerateVersion", 
			"PreviousClauseVersionId", 
			"TargetClauseVersionId", 
			"TargetClsVersionName", 
			"ProduceReport", 
			"ProduceClause", 
			"ProduceQuestion", 
			"ProduceMasterPrescription"};

	public static void main(String[] args) {
		App.parseArgs(args);
		if (App.displayOptions() == false) {
			System.out.println("*** System terminated due to no valid action(s) selected. ***");
			return;
		}
		if (confirmToProceed())
			App.run();
	}
	
	private static void parseArgs(String[] args) {
		iPreviousClauseVersionId = cpmConfig.getVersionIdPreviousInteger();
		iTargetClauseVersionId = cpmConfig.getVersionIdTargetInteger();
		sTargetClsVersionName = cpmConfig.getVersionNameTarget();
		
		String sMsg = ""; // "ClauseExcel ECFR DownloadECFR QuestionSequence QuestionExcel GenerateFillinCorrections LoadFillinCorrections GenerateVersion PreviousClauseVersionId TargetClauseVersionId TargetClsVersionName ProduceReport ProduceClause ProduceQuestion";
		for (String sItem : aArguments) {
			sMsg += " " + sItem;
		}
		System.out.println("Optional parameters:" + sMsg);
		System.out.println("Each parameter can contain '=' for assigned value of [0|1], [y|n], [t|f], [true|false]");
		if (args.length == 0) {
			return;
		}
		for (String sArg : args) {
			String sParam = sArg;
			boolean bFlag = true;
			String sValue = null;
			if (sParam.contains("=")) {
				String[] aItems = sParam.split("=");
				sParam = aItems[0];
				sValue = aItems[1];
				if ("0".equals(sValue) || "false".equalsIgnoreCase(sValue) || "y".equalsIgnoreCase(sValue) || "f".equalsIgnoreCase(sValue))
					bFlag = false;
				else if ("1".equals(sValue) || "true".equalsIgnoreCase(sValue) || "n".equalsIgnoreCase(sValue) || "t".equalsIgnoreCase(sValue))
					bFlag = true;
			}
			switch (sParam) {
			case "ClauseExcel":
			case "clauseexcel":
				bClauseExcel = bFlag;
				System.out.println("Parameter: ClauseExcel(" + bFlag + ")");
				break;

			case "ECFR":
			case "ecfr":
				bECFR = bFlag;
				System.out.println("Parameter: ECFR(" + bFlag + ")");
				break;

			case "FullEdit":
			case "fulledit":
				bFullEdit = bFlag;
				System.out.println("Parameter: FullEdit(" + bFlag + ")");
				break;

			case "DownloadECFR":
			case "downloadecfr":
				bDownloadECFR = bFlag;
				System.out.println("Parameter: DownloadECFR(" + bFlag + ")");
				break;

			case "QuestionSequence":
			case "questionsequence":
				bQuestionSequence = bFlag;
				System.out.println("Parameter: QuestionSequence(" + bFlag + ")");
				break;

			case "QuestionExcel":
			case "questionexcel":
				bQuestionExcel = bFlag;
				System.out.println("Parameter: QuestionExcel(" + bFlag + ")");
				break;

			case "GenerateFillinCorrections":
			case "generatefillincorrections":
				bGenerateFillinCorrections = bFlag;
				System.out.println("Parameter: GenerateFillinCorrections(" + bFlag + ")");
				break;
				
			case "EcfrClauseDataDifferentReport": // CJ-1180
			case "ecfrclausedatadifferentreport":
				bEcfrClauseDataDifferentReport = bFlag;
				System.out.println("Parameter: EcfrClauseDataDifferentReport(" + bFlag + ")");
				break;

			case "LoadFillinCorrections":
			case "loadfillincorrections":
				bLoadFillinCorrections = bFlag;
				System.out.println("Parameter: LoadFillinCorrections(" + bFlag + ")");
				break;

			case "GenerateVersion":
			case "generateversion":
				bGenerateVersion = bFlag;
				System.out.println("Parameter: GenerateVersion(" + bFlag + ")");
				break;

			case "PreviousClauseVersionId":
			case "previousclauseversionid":
				iPreviousClauseVersionId = Integer.parseInt(sValue);
				System.out.println("Parameter: PreviousClauseVersionId(" + sValue + ")");
				break;

			case "TargetClauseVersionId":
			case "targetclauseversionid":
				iTargetClauseVersionId = Integer.parseInt(sValue);
				System.out.println("Parameter: TargetClauseVersionId(" + sValue + ")");
				break;

			case "TargetClsVersionName":
			case "targetclsversionname":
				sTargetClsVersionName = sValue;
				System.out.println("Parameter: TargetClsVersionName(" + sValue + ")");
				break;

			case "ProduceReport":
			case "producereport":
				bProduceReport = bFlag;
				System.out.println("Parameter: ProduceReport(" + bFlag + ")");
				break;

			case "ProduceClause":
			case "produceclause":
				bProduceClause = bFlag;
				System.out.println("Parameter: ProduceClause(" + bFlag + ")");
				break;

			case "ProduceQuestion":
			case "producequestion":
				bProduceQuestion = bFlag;
				System.out.println("Parameter: ProduceQuestion(" + bFlag + ")");
				break;
				
			case "ProduceMasterPrescription":
			case "producemasterprescription":
				ParsedPrescriptionSet.generateSql = bFlag;
				System.out.println("Parameter: ProduceMasterPrescription(" + bFlag + ")");
				break;
				
			default:
				System.out.println("Unknown Parameter: " + sArg);
				break;
			}
		}
	}
	
	private static boolean displayOptions() {
		boolean result = false;
		System.out.println("*** Parameters ***");
		//char cOption = 'a';
		for (String sItem : aArguments) {
			Object oValue = null;
			boolean bMajor = false;
			switch (sItem) {
			case "ClauseExcel":
				oValue = bClauseExcel + (bClauseExcel ? " " + cpmConfig.getExcelFarDfar() : "");
				result |= bClauseExcel;
				bMajor = true;
				break;
			case "ECFR":
				oValue = bECFR;
				result |= bECFR;
				bMajor = true;
				break;
			case "FullEdit":
				oValue = bFullEdit;
				break;
			case "DownloadECFR":
				oValue = bDownloadECFR;
				break;
			case "QuestionSequence":
				oValue = bQuestionSequence + (bQuestionSequence ? " " + cpmConfig.getExcelQuestionSequence() : "");
				result |= bQuestionSequence;
				bMajor = true;
				break;
			case "QuestionExcel":
				oValue = bQuestionExcel + (bQuestionExcel ? " " + cpmConfig.getExcelQuestion() : "");
				result |= bQuestionExcel;
				bMajor = true;
				break;
			case "EcfrClauseDataDifferentReport": // CJ-1180
				oValue = bEcfrClauseDataDifferentReport;
				if (bEcfrClauseDataDifferentReport) {
					oValue += "\n  - Previous Clause Version Id: " + cpmConfig.getVersionIdPrevious();
				}
				result |= bEcfrClauseDataDifferentReport;
				bMajor = true;
				break;
			case "GenerateFillinCorrections":
				oValue = bGenerateFillinCorrections;
				result |= bGenerateFillinCorrections;
				bMajor = true;
				break;
			case "LoadFillinCorrections":
				oValue = bLoadFillinCorrections;
				result |= bLoadFillinCorrections;
				bMajor = true;
				break;
			case "GenerateVersion":
				oValue = bGenerateVersion;
				result |= bGenerateVersion;
				bMajor = true;
				break;
			case "PreviousClauseVersionId":
				oValue = iPreviousClauseVersionId;
				break;
			case "TargetClauseVersionId":
				oValue = iTargetClauseVersionId;
				break;
			case "TargetClsVersionName":
				oValue = sTargetClsVersionName;
				break;
			case "ProduceReport":
				oValue = bProduceReport;
				break;
			case "ProduceClause":
				oValue = bProduceClause;
				break;
			case "ProduceQuestion":
				oValue = bProduceQuestion;
				break;
			case "ProduceMasterPrescription":
				oValue = ParsedPrescriptionSet.generateSql;
				break;
			}

			System.out.println((bMajor ? "" : "  - ") + sItem + ": " + oValue);
			//System.out.println("[" + cOption + "] " + sItem + ": " + oValue);
			//cOption += 1; 
		}
		System.out.println("version.id.previous=" + cpmConfig.getVersionIdPrevious()); // 12
		System.out.println("version.id.target=" + cpmConfig.getVersionIdTarget()); // 15
		System.out.println("version.name.target=" + cpmConfig.getVersionNameTarget()); // 3.16
		return result;
	}

	private static boolean confirmToProceed() {
		Scanner input = null;
		try {
			input = new Scanner(System.in);
			System.out.println("Do you want to proceed (y/n, default: y)?");
			String answer = input.nextLine();
			if ((answer == null) || answer.isEmpty())
				return true;
			String firstChar = answer.substring(0, 1).toLowerCase();
			if ("y".equals(firstChar))
				return true;
		} finally {
			if (input != null)
				input.close();
		}
		System.out.println("Cancelled...");
		return false;
	}
	
	private static void run()
    {
    	System.out.println("App.main(): start at " + StringCommons.getNow());
    	System.out.println("====================================================");
    	Connection conn = null;
    	// StgVersioningRecord oStgVersioningRecord;
    	try {
    		conn = cpmConfig.getDbConnection();
    		
    		if (bClauseExcel)
    			App.loadClauseExcel(conn);
    		
    		if (bECFR)
    			App.loadEcfr(conn);
    		
    		if (bQuestionSequence)
    			App.loadQuestionSequence(conn);
    		
    		if (bQuestionExcel)
    			App.loadQuestionExcel(conn);

    		if (bGenerateFillinCorrections)
    			App.generateFillinCorrections(conn);
    		if (bLoadFillinCorrections)
    			App.loadFillinCorrections(conn);

    		if (bEcfrClauseDataDifferentReport) // CJ-1180
    			generateEcfrDataDiffRpt(conn);
    		
    		if (bClauseExcel || bGenerateVersion) // KH: Added
    			App.generatePrescriptionsPerRule(conn);	// CJ-473, Prescriptions assigned per CLause Rules.
    		
    		if (bGenerateVersion)
    			App.generateVersoin();
        	/*
        	PopulateVersion oPopulateVersion = new PopulateVersion(conn,  1, iEfcrSrcDocId, iQuestionSrcDocId, iClauseSrcDocId);
        	oPopulateVersion.processQuery();
        	*/
    	}
    	catch (Exception oError) {
    		oError.printStackTrace();
    	}
    	finally {
    		SqlUtil.closeInstance(conn);
    	}
    	System.out.println("App.main(): end at " + StringCommons.getNow());
    	System.out.println("====================================================");
    }
    
    private static void loadEcfr(Connection conn) throws Exception {
    	oStgSrcDocumentSet = StgSrcDocumentSet.getLatest(conn);
    	int iEfcrSrcDocId;
    	if (bDownloadECFR) {
			// 4_load_clauses.sql
			LoadEcfr loadEcfr = new LoadEcfr(conn, oStgSrcDocumentSet.iClauseSrcDocId, bFullEdit);
			loadEcfr.downloadAll();
			iEfcrSrcDocId = loadEcfr.getSrcDocId();
    	} else {
    		iEfcrSrcDocId = oStgSrcDocumentSet.iEfcrSrcDocId;
    	}

		// 5_replace_fillins.sql
		// Removed // EcfrReplaceFillins.manualReplacesFillins(conn, iEfcrSrcDocId);
    	EcfrManualClause.addClauses(conn, iEfcrSrcDocId);
		EcfrParseFillins.process(conn, iEfcrSrcDocId, oStgSrcDocumentSet.iClauseSrcDocId, bFullEdit);
    }

    private static void loadClauseExcel(Connection conn) throws IOException, SQLException, ParseException {
		// 9_clause_parser.sql
		ClauseExcel clauseExcel = new ClauseExcel(cpmConfig.getExcelFarDfar(), conn);
		clauseExcel.read();
    	
    	int iClauseSrcDocId = clauseExcel.getSrcDocId();
		oStgSrcDocumentSet = StgSrcDocumentSet.getLatest(conn);

		regulationParams.sFacNumber = clauseExcel.getFacNumber();
		regulationParams.sDacNumber = clauseExcel.getDacNumber();
		regulationParams.sDFarsNumber = clauseExcel.getDfarsNumber();
		
		regulationParams.facDate = clauseExcel.getFacDate();
		regulationParams.dacDate = clauseExcel.getDacDate();
		regulationParams.dfarsDate = clauseExcel.getDfarsDate();
		
		satParams.satCondition = clauseExcel.getSatConditon();
		satParams.sat300KRule = clauseExcel.getSat300kRule();
		satParams.sat1MillionRule = clauseExcel.getSat1MilRule();
		
		// 10_parse_clause_prescriptions.sql
		ParseClausePrescriptions.normalizeClausePrescriptions(conn, iClauseSrcDocId);
		
		// 11_conditions_parser.sql
		ClauseConditionsParser.run(conn, iClauseSrcDocId);
    }
    
    private static void loadQuestionSequence(Connection conn) throws Exception {
		QuestionSequenceExcel questionSequenceExcel = new QuestionSequenceExcel(cpmConfig.getExcelQuestionSequence(), conn);
		questionSequenceExcel.run();
    	// iQuestionSequenceSrcDocId = questionSequenceExcel.getSrcDocId();
    }
    
    private static void loadQuestionExcel(Connection conn) throws IOException, SQLException {
    	oStgSrcDocumentSet = StgSrcDocumentSet.getLatest(conn);
    	int iQuestionSequenceSrcDocId = oStgSrcDocumentSet.iQuestionSequenceSrcDocId; 
    	QuestionExcel questionExcel = new QuestionExcel(cpmConfig.getExcelQuestion(), iQuestionSequenceSrcDocId, conn);
    	questionExcel.run();
    	//iQuestionSrcDocId = questionExcel.getSrcDocId();

		// Remove // 7_sequence_questions.sql
		// SequenceQuestions.run(conn, iQuestionSrcDocId);

		// Remove // 8_parse_answer_prescriptions.sql
    	// ParseAnswerPrescriptions.run(conn, iQuestionSrcDocId);
    }
    
    private static void generateFillinCorrections(Connection conn) throws IOException, SQLException {
		oStgSrcDocumentSet = StgSrcDocumentSet.getLatest(conn);
		int newEfcrSrcDocId = oStgSrcDocumentSet.iEfcrSrcDocId;
		int clauseSrcDocId = oStgSrcDocumentSet.iClauseSrcDocId;
		int oldECFRSorcDocId = 0; //TO DO: this should be set based on a lookup of old ECFR src doc based on previous version id
		FillinCorrectionWriter fillinCorrectionWriter = new FillinCorrectionWriter(cpmConfig.getClauseCorrectionPath(), 
 		cpmConfig.getVersionIdPreviousInteger(), oldECFRSorcDocId, newEfcrSrcDocId, clauseSrcDocId, conn);
		fillinCorrectionWriter.generate();
    }
 
    private static void loadFillinCorrections(Connection conn) throws IOException, SQLException {
    	FillinCorrectionReader fillinCorrectionReader = new FillinCorrectionReader(cpmConfig.getClauseCorrectionPath(), 
 		cpmConfig.getVersionIdTargetInteger(), conn);
    	fillinCorrectionReader.load(true); //true for clear Stg3_Clause_Corrections at start
    }
    
    private static void generatePrescriptionsPerRule(Connection conn) throws SQLException {
    	oStgSrcDocumentSet = StgSrcDocumentSet.getLatest(conn);
    	int clauseSrcDocId = oStgSrcDocumentSet.iClauseSrcDocId;
    	int iQuestionSequenceSrcDocId = oStgSrcDocumentSet.iQuestionSequenceSrcDocId; 
    	ParseClausePrescriptionRules.run(conn, clauseSrcDocId, iQuestionSequenceSrcDocId);
    }
    
    private static void generateEcfrDataDiffRpt(Connection conn) throws SQLException, IOException { // CJ-1180
    	String sError = "";
    	Integer lastClauseVersionId = cpmConfig.getVersionIdPreviousInteger(); // ClauseVersionRecord.getActiveClauseVersionId(conn);
    	if (lastClauseVersionId == null) {
    		sError = "No previous Clause Version.";
    	}
    	oStgSrcDocumentSet = StgSrcDocumentSet.getLatest(conn);
    	if (oStgSrcDocumentSet.iEfcrSrcDocId == null) {
    		sError = (sError.isEmpty() ? "" : " ") + "No parsed ECFR source.";
    	}
    	if (oStgSrcDocumentSet.iClauseSrcDocId == null) {
    		sError = (sError.isEmpty() ? "" : " ") + "No parsed Excel source.";
    	}
    	if ((cpmConfig.getClauseCorrectionPath() == null) || cpmConfig.getClauseCorrectionPath().isEmpty()) {
    		sError = (sError.isEmpty() ? "" : " ") + "No Clause Correction Path defined.";
    	}

    	if (sError.isEmpty()) {
    		EcfrDiffRpt ecfrDiffRpt = new EcfrDiffRpt(lastClauseVersionId, oStgSrcDocumentSet.iEfcrSrcDocId, oStgSrcDocumentSet.iClauseSrcDocId, conn, cpmConfig.getClauseCorrectionPath());
    		ecfrDiffRpt.run();
    	} else {
    		System.out.println("App.generateEcfrDataDiffRpt(): " + sError + " Process cancelled.");
    		
    	}
    }
    
    private static void generateVersoin() {
		GenerateVersion.run (iPreviousClauseVersionId, iTargetClauseVersionId, sTargetClsVersionName, bProduceReport, bProduceClause, bProduceQuestion, 
				regulationParams,
				satParams);
    }
    
}
