package hgs.cpm.db;

import gov.dod.cls.db.PrescriptionRecord;
import gov.dod.cls.db.PrescriptionTable;
import gov.dod.cls.db.QuestionChoicePrescriptionRecord;
import gov.dod.cls.db.QuestionChoiceTable;
import gov.dod.cls.db.QuestionChoiceRecord;
import gov.dod.cls.db.QuestionRecord;
import gov.dod.cls.utils.CommonUtils;
import gov.dod.cls.utils.sql.SqlUtil;
import hgs.cpm.question.util.Stg2PossibleAnswerRecord;
import hgs.cpm.utils.SqlCommons;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class ParsedQuestionChoiceSet extends ArrayList<ParsedQuestionChoiceRecord> {

	private static final long serialVersionUID = -6952072255392582183L;
	
	public void load(Connection conn, int iQuestionSrcDocId) throws SQLException {
		String sSql = "SELECT " + Stg2PossibleAnswerRecord.FIELD_STG2_POSSIBLE_ANSWER_ID
				+ ", " + Stg2PossibleAnswerRecord.FIELD_STG2_QUESTION_ID
				+ ", " + Stg2PossibleAnswerRecord.FIELD_CHOICE_TEXT
				+ ", " + Stg2PossibleAnswerRecord.FIELD_PROMPT_AFTER_SELECTION
				+ " FROM " + Stg2PossibleAnswerRecord.TABLE_STG2_POSSIBLE_ANSWER
				+ " WHERE " + Stg2PossibleAnswerRecord.FIELD_SRC_DOC_ID + " = ?"
				+ " ORDER BY " + Stg2PossibleAnswerRecord.FIELD_STG2_QUESTION_ID + ", " + Stg2PossibleAnswerRecord.FIELD_STG2_POSSIBLE_ANSWER_ID;
		PreparedStatement ps1 = null;
		ResultSet rs1 = null;
		PreparedStatement ps2 = null;
		ResultSet rs2 = null;
		try {
			ps1 = conn.prepareStatement(sSql);
			ps1.setInt(1, iQuestionSrcDocId);
			
			//sSql = "SELECT DISTINCT Prescription FROM Stg2_Possible_Answers_Prescription WHERE Stg2_Possible_Answer_Id = ? ORDER BY 1";
			// CJ-473, Assign Prescriptions, per clause rule.
			sSql = "SELECT DISTINCT P.Prescription_Name FROM Stg2_Clause_Question_Conditions CQC " 
					+ "INNER JOIN Stg2_Clause_Prescriptions_Xref CP ON CP.Stg1_Clause_Id = CQC.Stg1_Clause_Id " 
					+ "INNER JOIN Prescriptions P ON P.Prescription_Name = CP.Prescription " 
					+ "WHERE CQC.Stg2_Possible_Answer_Id = ?  " 
					+ "ORDER BY 1";
			ps2 = conn.prepareStatement(sSql);
			
			rs1 = ps1.executeQuery();
			this.clear();
			while (rs1.next()) {
				ParsedQuestionChoiceRecord oRecord = new ParsedQuestionChoiceRecord();
				oRecord.Stg2PossibleAnswerId = rs1.getInt(1);
				oRecord.stg2QuestionId = rs1.getInt(2);
				String sChoiceText = rs1.getString(3);
				while (sChoiceText.contains("  "))
					sChoiceText = sChoiceText.replaceAll("  ", " ") .trim();
				oRecord.choiceText = sChoiceText;
				oRecord.promptAfterSelection = rs1.getString(4);
				
				ps2.setInt(1, oRecord.Stg2PossibleAnswerId);
				rs2 = ps2.executeQuery();
				while (rs2.next()) {
					oRecord.addPrescription(rs2.getString(1));
					// oRecord.prescriptions.add(rs2.getString(1));
				}
				rs2.close();
				rs2 = null;
				
				this.add(oRecord);
				
			}
			
			sqlMapAnswerExceptions	(conn); // CJ-1153, 
			
		} finally {
			SqlUtil.closeInstance(rs2, ps2);
			SqlUtil.closeInstance(rs1, ps1);
		}
	}
	
	// CJ-1153, Map the rest of prescriptions for answers that are not referenced in the clause conditions:
	// NOTE: the set array will probably already contain a record for the answer.
	private void sqlMapAnswerExceptions(Connection conn) throws SQLException {
			
		PreparedStatement ps1 = null;
		PreparedStatement ps2 = null;
		ResultSet rs1 = null;
		ResultSet rs2 = null;
		
		try {
				
			String sSqlQuestions = 
					"SELECT DISTINCT C.Question_Name, A.Choice_Text, "
					+ 	" A.Stg2_Possible_Answer_Id, A.Stg2_Question_Id, A.Prompt_After_Selection "
					+ "FROM Stg2_Possible_Answer  A "
					+ "INNER JOIN Stg2_Question C "
					+ "ON C.Stg2_Question_id = A.Stg2_Question_Id "
					+ "AND C.Src_Doc_Id = (SELECT Src_Doc_Id FROM Stg_Src_Document WHERE Src_File_Name = 'CLS Questions Matrix.xlsm' ORDER BY Src_Doc_Id DESC LIMIT 1) "
					+ "INNER JOIN Stg2_Possible_Answers_Prescription D "
					+ "ON D.Stg2_Possible_Answer_Id = A.Stg2_Possible_Answer_Id "
					+ "WHERE C.Question_Name NOT IN (SELECT Question_Name FROM Stg2_Clause_Question_Conditions) "
					+ "OR D.Prescription like '%*' "
					+ "ORDER BY C.Question_Name, A.Choice_Text, D.Prescription";
			ps1 = conn.prepareStatement(sSqlQuestions);
			
			String sSql = "SELECT DISTINCT Prescription FROM Stg2_Possible_Answers_Prescription "
					+ " WHERE Stg2_Possible_Answer_Id = ? ORDER BY 1";
			ps2 = conn.prepareStatement(sSql);
			
			rs1 = ps1.executeQuery(); 
			while (rs1.next()) {
				//String qName = rs1.getString(1);
				//String sPrescript = "";
				
				String sChoiceText = rs1.getString(2);
				while (sChoiceText.contains("  "))
					sChoiceText = sChoiceText.replaceAll("  ", " ") .trim();
				
				// Use existing array record to make sure no conflicts occur later in the program.
				boolean bFoundRecord = false;
				ParsedQuestionChoiceRecord oRecord = null;
				for (ParsedQuestionChoiceRecord oTmpRecord : this) {
					if ((oTmpRecord.stg2QuestionId.equals(rs1.getInt(4)))
					&& (oTmpRecord.choiceText.equals(sChoiceText))) {
						oRecord = oTmpRecord;
						bFoundRecord = true;
						break;
					}
				}
				
				// If record not found, then add new one.
				if (oRecord == null) {
					oRecord = new ParsedQuestionChoiceRecord();
					oRecord.Stg2PossibleAnswerId = rs1.getInt(3);
					oRecord.stg2QuestionId = rs1.getInt(4);
					oRecord.choiceText = sChoiceText;
					oRecord.promptAfterSelection = rs1.getString(5);
				}
				
				ps2.setInt(1, oRecord.Stg2PossibleAnswerId);
				rs2 = ps2.executeQuery();
				
				while (rs2.next()) {
					String sTmp = rs2.getString(1);
					sTmp = sTmp.replaceAll("\\*", "");
					oRecord.addPrescription(sTmp);
					//sPrescript += sTmp + ", ";
				}
				rs2.close();
				rs2 = null;
				
				if (!bFoundRecord)
					this.add(oRecord);
				
				// System.out.println("Extra answer prescriptions >> " + qName + ", " + sChoiceText + ", " + sPrescript);
			}
		}
		
		finally {
			SqlUtil.closeInstance(rs2, ps2);
			SqlUtil.closeInstance(rs1, ps1);
		}
	}
		
	public void subset(Integer pStg2QuestionId, ParsedQuestionChoiceSet pSubset) {
		pSubset.clear();
		boolean bFound = false;
		for (ParsedQuestionChoiceRecord oRecord : this) {
			if (oRecord.stg2QuestionId.equals(pStg2QuestionId)) {
				pSubset.add(oRecord);
				bFound = true;
			} else if (bFound)
				break;
		}
		for (ParsedQuestionChoiceRecord oRecord : pSubset) {
			this.remove(oRecord);
		}
	}
	
	private void mergeDuplicates() {
		ParsedQuestionChoiceSet aOrginals = new ParsedQuestionChoiceSet();
		aOrginals.addAll(this);
		this.clear();
		for (ParsedQuestionChoiceRecord oParsedRecord : aOrginals) {
			ParsedQuestionChoiceRecord oAdded = null;
			for (int iIndex = 0; iIndex < this.size(); iIndex++) {
				ParsedQuestionChoiceRecord oItem = this.get(iIndex);
				if (oItem.choiceText.equals(oParsedRecord.choiceText)) {
					oAdded = oItem;
					break;
				}
			}
			if (oAdded == null) {
				this.add(oParsedRecord);
			} else {
				oAdded.mergePrescription(oParsedRecord);
			}
		}
	}
	
	public boolean hasChoice(String psChoice) {
		for (ParsedQuestionChoiceRecord oRecord : this) {
			if (oRecord.choiceText.equals(psChoice))
				return true;
		}
		return false;
	}
	
	public String getDifferences(QuestionChoiceTable pQuestionChoiceTable, ParsedPrescriptionSet parsedPrescriptionSet) {
		String sHtml = "\n<table><caption>Question Choices</caption>";
		boolean bFound = false;
		
		this.mergeDuplicates();
		
		QuestionChoiceRecord oExists = null;
		QuestionChoiceTable aFound = new QuestionChoiceTable();
		for (ParsedQuestionChoiceRecord oParsedRecord : this) {
			if (pQuestionChoiceTable != null)
				oExists = pQuestionChoiceTable.findByText(oParsedRecord.choiceText);
			else
				oExists = null;
			if (oExists == null) {
				sHtml += "\n<tr><th>New</th><td>" + oParsedRecord.choiceText + "</td></tr>";
				bFound = true;
			} else {
				if (CommonUtils.isNotSame(oParsedRecord.promptAfterSelection, oExists.getPromptAfterSelection(), false)) {
					sHtml += "\n<tr><th>Prompt Change</th><td>" + oParsedRecord.promptAfterSelection + "</td></tr>";
				}
				aFound.add(oExists);
			}
		}
		if (pQuestionChoiceTable != null) {
			for (QuestionChoiceRecord oRecord : pQuestionChoiceTable) {
				if (!aFound.contains(oRecord)) {
					sHtml += "\n<tr><th>Remove</th><td>" + oRecord.getChoiceText() + "</td></tr>";
					bFound = true;
				}
			}
		}
		if (bFound) {
			sHtml += "\n</table>";
			return sHtml;
		} else
			return "";
	}
	
	private ParsedQuestionChoiceRecord find(QuestionChoiceRecord pRecord) {
		for (ParsedQuestionChoiceRecord oRecord : this) {
			if (pRecord.getChoiceText().equals(oRecord.choiceText))
				return oRecord;
		}
		return null;
	}

	public String genSql(QuestionChoiceTable pQuestionChoiceTable, ParsedPrescriptionSet parsedPrescriptionSet,
			ParsedQuestionRecord parsedQuestionRecord, Integer piQuestionId, PrescriptionTable poPrescriptionTable) {
		String sSql = null;
		
		this.mergeDuplicates();

		if (pQuestionChoiceTable != null) {
			for (QuestionChoiceRecord pRecord : pQuestionChoiceTable) {
				ParsedQuestionChoiceRecord oRecord = this.find(pRecord);
				if (oRecord == null) {
					sSql = ((sSql == null) ? "" : sSql + "\n")
							+ "\nDELETE FROM " + QuestionChoiceRecord.TABLE_QUESTION_CHOICES
							+ " WHERE " + QuestionChoiceRecord.FIELD_QUESTION_ID + " = " + piQuestionId
							+ " AND " + QuestionChoiceRecord.FIELD_CHOICE_TEXT + " = " + SqlCommons.quoteString(pRecord.getChoiceText())
							+ "; -- " + parsedQuestionRecord.questionCode;
				} else {
					if (CommonUtils.isNotSame(oRecord.promptAfterSelection, pRecord.getPromptAfterSelection())) {
						sSql = ((sSql == null) ? "" : sSql + "\n")
								+ "\nUPDATE " + QuestionChoiceRecord.TABLE_QUESTION_CHOICES
								+ "\nSET " + QuestionChoiceRecord.FIELD_PROMPT_AFTER_SELECTION + " = " + SqlCommons.quoteString(oRecord.promptAfterSelection)
								+ "\nWHERE " + QuestionChoiceRecord.FIELD_QUESTION_ID + " = " + piQuestionId
								+ " AND " + QuestionChoiceRecord.FIELD_CHOICE_TEXT + " = " + SqlCommons.quoteString(pRecord.getChoiceText())
								+ "; -- " + parsedQuestionRecord.questionCode + " - " + SqlCommons.quoteString(pRecord.getPromptAfterSelection());
					}
					if (pRecord.getQuestionChoicePrescriptionTable() != null)
						for (QuestionChoicePrescriptionRecord oChoicePrescription : pRecord.getQuestionChoicePrescriptionTable()) {
							PrescriptionRecord poPrescriptionRecord = poPrescriptionTable.findById(oChoicePrescription.getPrescriptionId());
							if (poPrescriptionRecord != null) {
								int iPos = oRecord.prescriptionIndexOf(poPrescriptionRecord.getName());
								// int iPos = oRecord.prescriptions.indexOf(poPrescriptionRecord.getName());
								if (iPos >= 0) {
									oRecord.prescriptions.remove(iPos);
								} else {
									sSql = ((sSql == null) ? "" : sSql + "\n")
										+ "\nDELETE FROM " + QuestionChoicePrescriptionRecord.TABLE_QUESTION_CHOICE_PRESCRIPTIONS
										+ " WHERE " + QuestionChoicePrescriptionRecord.FIELD_QUESTION_CHOICE_PRESCRIPTION_ID
										+ " = " + oChoicePrescription.getId() + "; -- " + parsedQuestionRecord.questionCode
										+ " '"  + pRecord.getChoiceText() + "' " + poPrescriptionRecord.getName()
										;
								}
							}
						}
					String sPrescriptionInsert = oRecord.genInsertPrescriptionSql(pRecord.getId(), parsedQuestionRecord);
					if (CommonUtils.isNotEmpty(sPrescriptionInsert))
						sSql = ((sSql == null) ? "" : sSql + "\n") + sPrescriptionInsert;
					this.remove(oRecord);
				}
			}
		}
		
		for (ParsedQuestionChoiceRecord oRecord : this) {
			sSql = ((sSql == null) ? "" : sSql)
					+ oRecord.genInsertSql(parsedPrescriptionSet, piQuestionId, parsedQuestionRecord);
		}
		
		return sSql;
	}
	
	public String genSql_wFilter(QuestionChoiceTable pQuestionChoiceTable, ParsedPrescriptionSet parsedPrescriptionSet,
			ParsedQuestionRecord parsedQuestionRecord, Integer piQuestionId, PrescriptionTable poPrescriptionTable, Integer iClauseVersionId) {
		String sSql = null;
		
		this.mergeDuplicates();

		if (pQuestionChoiceTable != null) {
			for (QuestionChoiceRecord pRecord : pQuestionChoiceTable) {
				ParsedQuestionChoiceRecord oRecord = this.find(pRecord);
				if (oRecord == null) {
					sSql = ((sSql == null) ? "" : sSql + "\n")
							+ "\nDELETE FROM " + QuestionChoiceRecord.TABLE_QUESTION_CHOICES
							+ " WHERE " + QuestionChoiceRecord.FIELD_QUESTION_ID + " = " + piQuestionId
							+ " AND " + QuestionChoiceRecord.FIELD_CHOICE_TEXT + " = " + SqlCommons.quoteString(pRecord.getChoiceText())
							+ "; -- " + parsedQuestionRecord.questionCode;
				} else {
					if (CommonUtils.isNotSame(oRecord.promptAfterSelection, pRecord.getPromptAfterSelection())) {
						sSql = ((sSql == null) ? "" : sSql + "\n")
								+ "\nUPDATE " + QuestionChoiceRecord.TABLE_QUESTION_CHOICES
								+ "\nSET " + QuestionChoiceRecord.FIELD_PROMPT_AFTER_SELECTION + " = " + SqlCommons.quoteString(oRecord.promptAfterSelection)
								+ "\nWHERE " + QuestionChoiceRecord.FIELD_QUESTION_ID + " = " + piQuestionId
								+ " AND " + QuestionChoiceRecord.FIELD_CHOICE_TEXT + " = " + SqlCommons.quoteString(pRecord.getChoiceText())
								+ "; -- " + parsedQuestionRecord.questionCode + " - " + SqlCommons.quoteString(pRecord.getPromptAfterSelection());
					}
					
					if (pRecord.getQuestionChoicePrescriptionTable() != null)
						for (QuestionChoicePrescriptionRecord oChoicePrescription : pRecord.getQuestionChoicePrescriptionTable()) {

							PrescriptionRecord poPrescriptionRecord = poPrescriptionTable.findById(oChoicePrescription.getPrescriptionId());
							if (poPrescriptionRecord != null) {
								int iPos = oRecord.prescriptionIndexOf(poPrescriptionRecord.getName());
								// int iPos = oRecord.prescriptions.indexOf(poPrescriptionRecord.getName());
								if (iPos >= 0) {
									oRecord.prescriptions.remove(iPos);
								} else {
									/*
									sSql = ((sSql == null) ? "" : sSql + "\n")
										+ "\nDELETE FROM " + QuestionChoicePrescriptionRecord.TABLE_QUESTION_CHOICE_PRESCRIPTIONS
										+ " WHERE " + QuestionChoicePrescriptionRecord.FIELD_QUESTION_CHOICE_PRESCRIPTION_ID
										+ " = " + oChoicePrescription.getId() + "; -- " + parsedQuestionRecord.questionCode
										+ " '"  + pRecord.getChoiceText() + "' " + poPrescriptionRecord.getName()
										;*/
									
									sSql = ((sSql == null) ? "" : sSql + "\n")
									+ "\nDELETE " + QuestionChoicePrescriptionRecord.TABLE_QUESTION_CHOICE_PRESCRIPTIONS + " FROM " + QuestionChoicePrescriptionRecord.TABLE_QUESTION_CHOICE_PRESCRIPTIONS
									+ " INNER JOIN " + QuestionChoiceRecord.TABLE_QUESTION_CHOICES + " QC " 
									+ " ON QC." + QuestionChoiceRecord.FIELD_QUESTION_CHOCE_ID + " = " + QuestionChoicePrescriptionRecord.TABLE_QUESTION_CHOICE_PRESCRIPTIONS + "." + QuestionChoicePrescriptionRecord.FIELD_QUESTION_CHOCE_ID
									+ "\nAND QC." + QuestionChoiceRecord.FIELD_CHOICE_TEXT + " = " +  SqlCommons.quoteString(pRecord.getChoiceText())
									+ " INNER JOIN " + QuestionRecord.TABLE_QUESTIONS + " Q "
									+ " ON Q." + QuestionRecord.FIELD_QUESTION_CODE + " = " +  SqlCommons.quoteString(parsedQuestionRecord.questionCode)
									+ " AND Q." + QuestionRecord.FIELD_QUESTION_ID + " = QC." + QuestionChoiceRecord.FIELD_QUESTION_ID
									+ " AND Q." + QuestionRecord.FIELD_CLAUSE_VERSION_ID + " = " + iClauseVersionId 
									+ "\nINNER JOIN " + PrescriptionRecord.TABLE_PRESCRIPTIONS + " P " 
									+ " ON P." + PrescriptionRecord.FIELD_PRESCRIPTION_ID + " = " + QuestionChoicePrescriptionRecord.TABLE_QUESTION_CHOICE_PRESCRIPTIONS + "." + QuestionChoicePrescriptionRecord.FIELD_PRESCRIPTION_ID
									+ " AND P." + PrescriptionRecord.FIELD_PRESCRIPTION_NAME + " = " + SqlCommons.quoteString(poPrescriptionRecord.getName()) + ";";
								}
							}
						}
					String sPrescriptionInsert = oRecord.genInsertPrescriptionSql_wFilter( parsedQuestionRecord, iClauseVersionId);
					if (CommonUtils.isNotEmpty(sPrescriptionInsert))
						sSql = ((sSql == null) ? "" : sSql + "\n") + sPrescriptionInsert;
					this.remove(oRecord);
				}
			}
		}
		
		for (ParsedQuestionChoiceRecord oRecord : this) {
			sSql = ((sSql == null) ? "" : sSql)
					+ oRecord.genInsertSql_wFilter(parsedPrescriptionSet, piQuestionId, parsedQuestionRecord, iClauseVersionId);
		}
		
		return sSql;
	}
	
	// This function checkSql() is called on Insert, which does not modify the choice record/array.
	// The genSql() is called on Update, which modifies the actual record/array.
	public boolean checkSqlChanges(QuestionChoiceTable pQuestionChoiceTable, ParsedPrescriptionSet parsedPrescriptionSet,
			ParsedQuestionRecord parsedQuestionRecord, Integer piQuestionId, PrescriptionTable poPrescriptionTable,
			ChoiceChangeRecord ccRecord) {
		
		boolean bChanged = false;
		// this.mergeDuplicates();

		if (pQuestionChoiceTable == null) 
			return false;
		
		for (QuestionChoiceRecord pRecord : pQuestionChoiceTable) {
			ParsedQuestionChoiceRecord oRecord = this.find(pRecord);
			if (oRecord == null) {
				bChanged = true;
				ccRecord.setChoice(true);
			} else {
				if (pRecord.getQuestionChoicePrescriptionTable() != null)
					for (QuestionChoicePrescriptionRecord oChoicePrescription : pRecord.getQuestionChoicePrescriptionTable()) {
						PrescriptionRecord poPrescriptionRecord = poPrescriptionTable.findById(oChoicePrescription.getPrescriptionId());
						if (poPrescriptionRecord != null) {
							boolean bFound = false;
							String sTarget = poPrescriptionRecord.getName().toLowerCase();
							for (int iPre = 0; iPre < oRecord.prescriptions.size(); iPre++) {
								if (oRecord.prescriptions.get(iPre).toLowerCase().equals(sTarget)) {
									bFound = true;
									break;
								}
							}
							if (!bFound) {
								bChanged = true;
								ccRecord.setPrescription(true);
							}
							/*
							int iPos = oRecord.prescriptions.indexOf(poPrescriptionRecord.getName());
							if (iPos < 0) {
								bChanged = true;
							}
							*/
						}
					}
			}
		}
		
		
		return bChanged;
	}
	public String genSqlOld(QuestionChoiceTable pQuestionChoiceTable, ParsedPrescriptionSet parsedPrescriptionSet, ParsedQuestionRecord parsedQuestionRecord, Integer piQuestionId) {
		String sSql = null;
		String sChoiceIds = "";
		String sPrescriptionIds = "";
		
		String sContentPrevious = "";
		String sContentParsed = "";
		
		this.mergeDuplicates();
		
		if (pQuestionChoiceTable != null) {
			for (QuestionChoiceRecord oRecord : pQuestionChoiceTable) {
				sContentPrevious += '\n' + oRecord.getChoiceText();
				if (oRecord.getQuestionChoicePrescriptionTable() != null)
					for (QuestionChoicePrescriptionRecord oChoicePrescription : oRecord.getQuestionChoicePrescriptionTable()) {
						sPrescriptionIds += (sPrescriptionIds.isEmpty() ? "" : ", ") + oChoicePrescription.getId();
						sContentPrevious += '\t' + oChoicePrescription.getPrescriptionId();
					}
				sChoiceIds += (sChoiceIds.isEmpty() ? "" : ", ") + oRecord.getId();  
			}
			
			sSql = ((sSql == null) ? "" : sSql + "\n")
					+ "\nDELETE FROM " + QuestionChoiceRecord.TABLE_QUESTION_CHOICES
					+ " WHERE " + QuestionChoiceRecord.FIELD_QUESTION_ID + " = " + piQuestionId + "; -- "
					+ parsedQuestionRecord.questionCode + "\n";
			/*
			sSql = ((sSql == null) ? "" : sSql + "\n")
					+ "DELETE FROM " + QuestionChoiceRecord.TABLE_QUESTION_CHOICES
					+ " WHERE " + QuestionChoiceRecord.FIELD_QUESTION_CHOCE_ID + " IN (" + sChoiceIds + "); -- "
					+ parsedQuestionRecord.questionCode + "\n";
			*/
		}
		
		for (ParsedQuestionChoiceRecord oRecord : this) {
			sContentParsed += '\n' + oRecord.choiceText + oRecord.getPrescriptionIds(parsedPrescriptionSet);
			sSql = ((sSql == null) ? "" : sSql)
					+ oRecord.genInsertSql(parsedPrescriptionSet, piQuestionId, parsedQuestionRecord);
			/*
			sSql += "\nINSERT INTO " + QuestionChoiceRecord.TABLE_QUESTION_CHOICES
					+ " (" + QuestionChoiceRecord.FIELD_QUESTION_CHOCE_ID
					+ ", " +  QuestionChoiceRecord.FIELD_QUESTION_ID
					+ ", " +  QuestionChoiceRecord.FIELD_CHOICE_TEXT
					+ ") VALUES (" + oRecord.Stg2PossibleAnswerId
					+ ", " + oRecord.stg2QuestionId
					+ ", " + SqlCommons.quoteString(oRecord.choiceText)
					+ ");";
			*/
		}
		
		if (sContentParsed.equals(sContentPrevious))
			return "";
		
		return sSql;
	}

}
