package hgs.cpm.db;

import gov.dod.cls.utils.CommonUtils;
import gov.dod.cls.utils.sql.SqlUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class ParsedClausePrescriptionSet extends ArrayList<ParsedClausePrescriptionRecord> {

	private static final long serialVersionUID = -2285621161754913330L;
	
	public void load(Connection conn, int iClauseSrcDocId) throws SQLException {
		String sSql = 
				"SELECT r.clause_number, pres_xref.prescription\n" +
				"FROM Stg1_Raw_Clauses r\n" +
				"JOIN Stg2_Clause_Prescriptions_Xref pres_xref on pres_xref.Stg1_Clause_Id = r.Stg1_Clause_Id\n" +
				"WHERE r.Src_Doc_Id = ?\n" +
				"AND pres_xref.prescription NOT IN ('--','n','SEE FOLLOW-ON QUESTIONS')\n" +
				"ORDER BY 1, 2";
		PreparedStatement ps1 = null;
		ResultSet rs1 = null;
		try {
			ps1 = conn.prepareStatement(sSql);
			ps1.setInt(1, iClauseSrcDocId);
			rs1 = ps1.executeQuery();
			this.clear();
			while (rs1.next()) {
				ParsedClausePrescriptionRecord oRecord = new ParsedClausePrescriptionRecord();
				oRecord.clauseNumber = rs1.getString(1);
				oRecord.prescription = rs1.getString(2);
				this.add(oRecord);
			}
		}
		finally {
			SqlUtil.closeInstance(rs1, ps1);
		}
	}
	
	public void loadPrescritions(ParsedClauseRecord pParsedClauseRecord) {
		boolean bFound = false;
		String sClauseName = pParsedClauseRecord.clauseName;
		if (CommonUtils.isNotEmpty(sClauseName)) { 
			ParsedClausePrescriptionSet aFound = new ParsedClausePrescriptionSet();
			for (ParsedClausePrescriptionRecord oRecord : this) {
				if (CommonUtils.isNotEmpty(oRecord.clauseNumber) && oRecord.clauseNumber.equalsIgnoreCase(sClauseName)) {
					if (!pParsedClauseRecord.prescriptions.contains(oRecord.prescription))
						pParsedClauseRecord.prescriptions.add(oRecord.prescription);
					aFound.add(oRecord);
					bFound = true;
				} else if (bFound) {
					break;
				}
			}
			for (ParsedClausePrescriptionRecord oRecord : aFound) {
				this.remove(oRecord);
			}
		}
	}

}
