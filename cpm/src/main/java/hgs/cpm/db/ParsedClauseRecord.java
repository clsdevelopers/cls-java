package hgs.cpm.db;

import gov.dod.cls.db.ClauseFillInRecord;
import gov.dod.cls.db.ClauseFillInTable;
import gov.dod.cls.db.ClausePrescriptionTable;
import gov.dod.cls.db.ClauseRecord;
import gov.dod.cls.db.ClausePrescriptionRecord;
import gov.dod.cls.db.ClauseVersionChangeRecord;
import gov.dod.cls.db.DocumentClauseRecord;
import gov.dod.cls.db.PrescriptionRecord;
import gov.dod.cls.utils.CommonUtils;
import gov.dod.cls.utils.sql.SqlUtil;
import hgs.cpm.utils.HtmlUtils;
import hgs.cpm.utils.SqlCommons;
import hgs.cpm.utils.StringCommons;
import hgs.cpm.versioning.ProduceClause;
import hgs.cpm.versioning.ProduceQuestion;
import hgs.cpm.versioning.util.ClauseEvalSet;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.parser.Parser;
import org.jsoup.select.Elements;

public class ParsedClauseRecord {

	public static DateFormat formatDate = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
	
	public boolean isAltClause = false;

	public Integer clauseId;
	public String clauseName = null;
	public Integer clauseSectionId;
	public String inclusion;
	public Integer regulationId;
	public String clauseTitle;
	private String rawClauseTitle = null;
	public boolean isBasicClause = false;
	public String clauseData;
	public String url;
	public Date effectiveDate;
	public String clauseConditions = null;
	public String clauseRule = null;
	public String additionalConditions = null;
	public String clauseHtmlFillin = null;
	
	public String requiredOrOptional = null;
	public String editable = null;
	public String provisionOrClause = null;
	public String commercialStatus; // CJ-476
	public String optionalStatus = null; // CJ-802
	public String optionalConditions = null; // CJ-802
	public String officialTitle = null; // CJ-916
	public boolean isDfarsActivity;
	
	public ArrayList<String> prescriptions = new ArrayList<String>();
	public ArrayList<ParsedClauseFillInRecord> fillIns = new ArrayList<ParsedClauseFillInRecord>();
	// public Stg3ClauseFilInCorrectionSet oStg3ClauseFilInCorrectionSet = null;

	public boolean changed = false;
	public boolean isExpressionValid = false;
	
	public ClauseEvalSet oClauseEvalSet = null;

	public void merge(ParsedClauseRecord poDupe) {
		if (this.clauseSectionId == null) {
			this.clauseSectionId = poDupe.clauseSectionId;
			this.clauseTitle = poDupe.clauseTitle;
			this.inclusion = poDupe.inclusion;
		}
		if (this.provisionOrClause == null)
			this.provisionOrClause = poDupe.provisionOrClause;
		if (this.commercialStatus == null) // CJ-476
			this.commercialStatus = poDupe.commercialStatus;
		for (String sPrescription : poDupe.prescriptions) {
			if (!this.prescriptions.contains(sPrescription))
				this.prescriptions.add(sPrescription);
		}
		if (this.oClauseEvalSet == null) {
			this.clauseConditions = poDupe.clauseConditions;
			this.clauseRule = poDupe.clauseRule;
		} else if (poDupe.oClauseEvalSet != null) {
			ClauseEvalSet.merge(this, poDupe); // .oClauseEvalSet
		}
		// CJ-802, Merge duplicate commercial status records.
		if ((poDupe.commercialStatus != null) && (!poDupe.commercialStatus.equals("M"))) {
			this.commercialStatus = poDupe.commercialStatus;
			this.requiredOrOptional = poDupe.requiredOrOptional;
		}
		// CJ-802, Merge duplicate optional status records.
		if ((poDupe.optionalStatus != null) && (poDupe.optionalConditions != null)) {
			this.optionalStatus = poDupe.optionalStatus;
			this.optionalConditions = poDupe.optionalConditions;
			this.requiredOrOptional = poDupe.requiredOrOptional;
		}
	}
	
	public void fixNulls() {
		if (this.clauseSectionId == null)
			this.clauseSectionId = 1;
	}

	public boolean isOptional() {
		return "O".equals(this.requiredOrOptional);
	}
	
	public boolean isEditable() {
		if (this.editable == null)
			return false;
		return this.editable.startsWith("Y");
	}
	
	public String getEditableRemarks() {
		if (this.isEditable() == false)
			return null;
		String sResult = this.editable.substring(1).trim();
		while (sResult.startsWith("\n"))
			sResult = sResult.substring(1).trim();
		while (sResult.endsWith("\n"))
			sResult = sResult.substring(0, sResult.length() - 1).trim();
		return (sResult.isEmpty() ? null : sResult);
	}
	
	private boolean detectIsBasicClause(String psClauseTitle, boolean detectEffectiveDate) { // CJ-193
		if (!(this.isAltClause || CommonUtils.isEmpty(psClauseTitle))) {
			int iPos = psClauseTitle.toUpperCase().indexOf("-BASIC");
			if (iPos > 1) {
				String sRest = psClauseTitle.substring(iPos + "-BASIC".length()).trim();
				if (sRest.startsWith("(")) { // sRest.isEmpty() || 
					this.isBasicClause = true;
					if (detectEffectiveDate && sRest.endsWith(")")) {
						Date oDate = StringCommons.monthYearToDate(sRest, this.clauseName, "");
						if (oDate != null) {
							this.effectiveDate = oDate;
							//System.out.println("New Effective Date from '" + sRest + "' to '" + oDate.getMonth() + " " + oDate.getYear() + "'");
						}
					}
				}
			}
		}
		return this.isBasicClause;
	}
	
	public void setRawClauseTitle(String psValue) {
		this.rawClauseTitle = psValue;
		this.isBasicClause = false;
		this.detectIsBasicClause(this.rawClauseTitle, false); // CJ-193
	}
	
	public void setTitleInContent(String titleInContent) { // CJ-193
		this.detectIsBasicClause(titleInContent, true);
	}
	
	/*
	private static final String[] MONTHS = {
		"JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "JULY", "AUG", "SEP", "OCT", "NOV", "DEC"
	};
	*/
	
	public static String fixHtmlSpecialCodes(String psHtml, boolean pbClauseTitle) {
		String result = HtmlUtils.convertUnicodeToHtml(psHtml);
		if (pbClauseTitle) {
			if ((result != null) && (result.contains("&nbsp;"))) {
				result = result.replaceAll("&nbsp;", " ");
				System.out.println("fixHtmlSpecialCodes for clauseTitle fix: " + result);
			}
		}
		return result; // HtmlUtils.convertUnicodeToHtml(psHtml);
		/*
		if (psHtml == null)
			return null;
		String result = psHtml.replaceAll("—", "&mdash;");
		result = result.replaceAll("&#x2014;", "&mdash;");
		result = result.replaceAll("§", "&sect;");
		if (result.contains("×"))
			result = result.replaceAll("×", "&times;");
		if (result.contains("é"))
			result = result.replaceAll("é", "&eacute;");
		
		result = HtmlUtils.convertUnicodeToHtml(result);
		
		//result = StringCommons.replaceAll(result, Pattern.quote("&#x2014;"), Pattern.quote("&mdash;"));
		// result = StringCommons.replaceAll(result, "&#x2014;", "&mdash;");
		//while (result.contains("&#x2014;"))
		//	result = result.replaceAll(Pattern.quote("&#x2014;"), Pattern.quote("&mdash;"));
		
		return result;
		*/
	}
	
	public void setClauseData(String pData, boolean pbAltClause) {
		if (pData == null) {
			this.clauseData = null;
			return;
		}
		int iPos = pData.indexOf("</h1>");
		if (iPos > 0) {
			int iStartPos = pData.indexOf("<h1>");
			if ((iStartPos < 0) || (iStartPos > iPos)) {
				pData = pData.substring(0, iPos).trim() + pData.substring(iPos + "</h1>".length());
			} else {
				if (iStartPos < iPos) {
					String sH1 = pData.substring(iStartPos + "<h1>".length(), iPos).trim().toUpperCase();
					String sTitle = this.clauseTitle.toUpperCase();
					if (sTitle.endsWith("."))
						sTitle = sTitle.substring(0, sTitle.length() - 1);
					if (sH1.startsWith(sTitle) && sH1.endsWith(")") && sH1.contains("(")) {
						pData = pData.substring(0, iStartPos).trim() + pData.substring(iPos);
					}
				}
			}
		}
		this.clauseData = ParsedClauseRecord.fixHtmlSpecialCodes(pData, false);
		
		if (pbAltClause) {
			/*
			String[] aAltClauses =
				{ "252.227-7005 Alternate I"
				, "252.227-7005 Alternate II"
				, "52.211-9 Alternate III"
				, "52.216-11 Alternate I"
				, "52.216-12 Alternate I"
				, "52.219-12 Alternate II"
				, "52.219-18 Alternate I"
				, "52.233-3 Alternate I"
				, "52.242-15 Alternate I"
				, "52.247-3 Alternate I"
				, "52.248-1 Alterante II"
				, "52.248-3 Alternate I"
				, "52.249-3 Alternate I"};
			boolean bFound = false;
			for (String sTest : aAltClauses)
				if (sTest.equalsIgnoreCase(this.clauseName)) {
					bFound = true;
					break;
				}
			if (bFound) {
			*/
				/*
				[56 FR 36479, July 31, 1991, as amended at 66 FR 49861, Oct. 1, 2001]</p>
				[56 FR 36479, July 31, 1991, as amended at 66 FR 49861, Oct. 1, 2001]</p>
				[48 FR 42478, Sept. 19, 1983, as amended at 56 FR 41732, Aug. 22, 1991; 60 FR 34739, July 3, 1995. Redesignated and amended at 60 FR 48251, 48256, Sept. 18, 1995; 62 FR 40238, July 25, 1997]</p>
				[48 FR 42478, Sept. 19, 1983, as amended at 72 FR 27389, May 15, 2007]</p>
				[48 FR 42478, Sept. 19, 1983, as amended at 72 FR 27389, May 15, 2007]</p>
				[50 FR 25681, June 20, 1985, as amended at 54 FR 29284, July 11, 1989; 60 FR 48231, 48276, Sept. 18, 1995; 61 FR 41472, Aug. 8, 1996]</p>
				[48 FR 42478, Sept. 19, 1983, as amended at 50 FR 2272, Jan. 15, 1985; 54 FR 29283, July 11, 1989. Redesignated and amended at 60 FR 48251, 48256, Sept. 18, 1995]</p>
				[48 FR 42478, Sept. 19, 1983, as amended at 71 FR 207, Jan. 3, 2006]</p>
				[48 FR 42478, Sept. 19, 1983, as amended at 54 FR 5059, Jan. 31, 1989; 64 FR 72449, Dec. 27, 1999; 71 FR 57369, Sept. 28, 2006;75 FR 53135, Aug. 30, 2010]</p>
				[48 FR 42478, Sept. 19, 1983, as amended at 61 FR 39222, July 26, 1996; 69 FR 17750, Apr. 5, 2004; 77 FR 12947, Mar. 2, 2012]</p>
				 */
				// <p>[56 FR 36479, July 31, 1991, as amended at 66 FR 49861, Oct. 1, 2001]</p>
				if (this.clauseData.endsWith("]</p>")) {
					iPos = this.clauseData.lastIndexOf("<p>[");
					if (iPos > 0) {
						String sTest = this.clauseData.substring(iPos);
						if (sTest.contains(" FR ") && sTest.contains(", as amended at ")) {
							System.out.println("[" + this.clauseName + "] removed: " + sTest);
							this.clauseData = this.clauseData.substring(0, iPos);
						}
					}
				}
			//}
		}
		/*if (this.clauseName.equals("252.208-7000")) {
			iPos = this.clauseData.indexOf("â");
			if (iPos > 0) {
				//System.out.println(this.clauseData);
			}
		}*/
		
		if ((this.clauseData != null) && (!this.clauseData.isEmpty())) {
			this.clauseData = this.cleanupHtml(this.clauseData); 
		}
	}
	
	private String cleanupHtml(String html) {
		html = html.trim();
		while (html.contains("\n\n"))
			html = html.replaceAll("\n\n", "\n");
		while ((html.length() > 0) && (html.charAt(0) == '\n'))
			html = html.substring(1);
		int iLength = html.length();
		while (iLength > 0) {
			char ch = html.charAt(iLength - 1);
			if ((ch == '\n') || (ch == '\r')) {
				html = html.substring(0, (--iLength - 1));
			} else
				break;
		}
		
		html = normalizeHtml(html);
		html = html.replaceAll("—", "&mdash;").replaceAll("“", "&ldquo;");
		html = StringCommons.convertUnicodeToHtml(html);
		if (html.contains("§"))
			html = html.replaceAll("§", "&sect;");
		if (html.contains("×"))
			html = html.replaceAll("×", "&times;");
		if (html.contains("é"))
			html = html.replaceAll("é", "&eacute;");
		return html;
	}
	
	//The actual standardization. Uses jsoup to transform a string of html based on CLS conventions.
	private String normalizeHtml(String html) {
		html = html.replace("img src=\"/graphics/", "img src=\"http://www.ecfr.gov/graphics/");
		html = html.replace("a href=\"/graphics/", "a href=\"http://www.ecfr.gov/graphics/");
		Document doc = Jsoup.parse(html, "", Parser.xmlParser());
		doc.select("span[style~=font-weight:\\s*bold]").tagName("b");
		doc.select("span[style~=font-style:\\s*italic]").tagName("i");
		Elements allElements = doc.select("*");
		allElements.removeAttr("class");
		allElements.removeAttr("style");
		allElements.removeAttr("font");
		allElements.removeAttr("span");
		doc.select("span").unwrap();
		doc.select("class").unwrap();
		doc.select("style").unwrap();
		doc.select("font").unwrap();

		doc.outputSettings().prettyPrint(false); //don't want extra linebreaks added.
		doc.outputSettings().charset("ISO-8859-2");
		return doc.toString();
	}
	
	/*
	private static String fixHtmlSpecialCode2(String psValue, String psHtmlCode, String psToFix) {
		if (psValue.contains("&amp;mdash;"))
			return psValue.replaceAll(psHtmlCode, psToFix);
		else
			return psValue;
	}
	*/
	
    private String stripSpecialChar(String testingString) {
        String resultString= "";
        int length = testingString.length();
        for (int i = 0; i < length; i++) {
        	if ((Character.isDigit(testingString.charAt(i)))
        	|| (Character.isLetter(testingString.charAt(i)))
        	|| (testingString.charAt(i) == ' ')
        	|| (testingString.charAt(i) == '.')
        	|| (testingString.charAt(i) == '-')
        	|| (testingString.charAt(i) == '_')
        	|| (testingString.charAt(i) == '(')
        	|| (testingString.charAt(i) == ')')
        	|| (testingString.charAt(i) == ':')
        	|| (testingString.charAt(i) == '&')
        	|| (testingString.charAt(i) == '/')
        	|| (testingString.charAt(i) == ',')
        	|| (testingString.charAt(i) == '\''))
        	{
        		resultString = resultString + testingString.charAt(i);
        	}

        }

        return resultString;

    }

    
	public void adjustValues() {
		if (this.clauseName.startsWith("52"))
			this.regulationId = 1;
		else if (this.clauseName.startsWith("252"))
			this.regulationId = 2;
		
		if (CommonUtils.isNotEmpty(this.clauseTitle)) {
			if (this.clauseTitle.contains("&amp;mdash;")) this.clauseTitle = this.clauseTitle.replaceAll("&amp;mdash;", "&-");
			if (this.clauseTitle.contains("&amp;")) this.clauseTitle = this.clauseTitle.replaceAll("&amp;", "&");
			if (this.clauseTitle.contains("&mdash;")) this.clauseTitle = this.clauseTitle.replaceAll("&mdash;", "-");
			if (this.clauseTitle.contains("&ldquo;")) this.clauseTitle = this.clauseTitle.replaceAll("&ldquo;", "'");
			if (this.clauseTitle.contains("&rdquo;")) this.clauseTitle = this.clauseTitle.replaceAll("&rdquo;", "'");
			if (this.clauseTitle.contains("&nbsp;")) this.clauseTitle = this.clauseTitle.replaceAll("&nbsp;", " ");
			
			if (this.clauseTitle.contains("\n")) this.clauseTitle = this.clauseTitle.replaceAll("\n", " ");
			this.clauseTitle = this.clauseTitle.trim();
			if (this.clauseTitle.endsWith(".")) { // CJ-817
				this.clauseTitle = this.clauseTitle.substring(0, this.clauseTitle.length() - 1);
			}
		}
		// CJ-916
		if (CommonUtils.isNotEmpty(this.officialTitle)) {
			if (this.officialTitle.contains("&amp;mdash;")) this.officialTitle = this.officialTitle.replaceAll("&amp;mdash;", "&-");
			if (this.officialTitle.contains("&amp;")) this.officialTitle = this.officialTitle.replaceAll("&amp;", "&");
			if (this.officialTitle.contains("&mdash;")) this.officialTitle = this.officialTitle.replaceAll("&mdash;", "-");
			if (this.officialTitle.contains("&ldquo;")) this.officialTitle = this.officialTitle.replaceAll("&ldquo;", "'");
			if (this.officialTitle.contains("&rdquo;")) this.officialTitle = this.officialTitle.replaceAll("&rdquo;", "'");
			if (this.officialTitle.contains("&nbsp;")) this.officialTitle = this.officialTitle.replaceAll("&nbsp;", " ");
			
			if (this.officialTitle.contains("\n")) this.officialTitle = this.officialTitle.replaceAll("\n", " ");
			
			this.officialTitle = stripSpecialChar (this.officialTitle);
			this.clauseTitle = this.officialTitle;
		}
		if (CommonUtils.isNotEmpty(this.clauseData)) {
			if (this.clauseData.contains("&amp;mdash;")) this.clauseData = this.clauseData.replaceAll("&amp;mdash;", "&-");
			if (this.clauseData.contains("&amp;")) this.clauseData = this.clauseData.replaceAll("&amp;", "&");
			if (this.clauseData.contains("&mdash;")) this.clauseData = this.clauseData.replaceAll("&mdash;", "-");
			if (this.clauseData.contains("&ldquo;")) this.clauseData = this.clauseData.replaceAll("&ldquo;", "'");
			if (this.clauseData.contains("&rdquo;")) this.clauseData = this.clauseData.replaceAll("&rdquo;", "'");
			// 508 Compliance
			/* Appian can't handle.
			if (this.clauseData.contains("<th scope=\"col\">NoLabel</th>")) 
				this.clauseData = this.clauseData.replaceAll("<th scope=\"col\">NoLabel</th>", "<th scope=\"col\"><span class=\"hide\">NoLabel</span></th>");
			*/
		}
		
		if (this.clauseConditions != null) {
			this.clauseConditions = this.clauseConditions.replaceAll(": :", ":").replaceAll("::", ":").replaceAll("\\xa0", " "); // .replaceAll("\n", " ")
			while (this.clauseConditions.contains("  "))
				this.clauseConditions = this.clauseConditions.replaceAll("  ", " ");
			
			String[] aIsItems = this.clauseConditions.split(" IS:");
			if (aIsItems.length > 1) {
				this.clauseConditions = "";
				for (String sItem : aIsItems) {
					if (this.clauseConditions.isEmpty())
						this.clauseConditions = sItem;
					else {
						if (sItem.startsWith(" "))
							this.clauseConditions += " IS:" + sItem;
						else
							this.clauseConditions += " IS: " + sItem;
					}
				}
			}
		}
		
		if (CommonUtils.isNotEmpty(this.clauseRule)) {
			String[] aRules = clauseRule.split("\n");
			if (aRules.length > 1) {
				String sFixedRule = "";
				for (int iRule = 0; iRule < aRules.length; iRule++) {
					String sRule = aRules[iRule].trim();
					if (sRule.isEmpty())
						continue;
					if (sFixedRule.isEmpty())
						sFixedRule = sRule;
					else {
						if (sRule.startsWith("CLOSED PARENTHESIS") || sRule.startsWith("IF "))
							break;
						sFixedRule += " " + sRule;
					}
				}
				this.clauseRule = sFixedRule; 
			}
		}
	}
	
	public String resolveIssues(ProduceQuestion oProduceQuestion, ProduceClause oProduceClause, 
			Stg3ClauseCorrectionRecord oStg3ClauseCorrectionRecord, boolean bReport) {
		String sLog = "";
		//this.clauseName;
		//this.clauseConditions;
		//this.clauseRule;
		//this.additionalConditions = null;
		
		if (CommonUtils.isNotEmpty(this.clauseConditions)) {
			this.oClauseEvalSet = new ClauseEvalSet();
			String sError = this.oClauseEvalSet.parseConditions(this, oProduceQuestion, oProduceClause);
			if (CommonUtils.isNotEmpty(sError)) {
				sLog = "<h2>" + this.clauseName + "</h2>\n" + sError; 
			}
			else if (this.oClauseEvalSet.isExpressionValid == false) {
				sLog = "<h2>" + this.clauseName + "</h2>\nUnknown Error<br /><pre>" + this.clauseConditions + "</pre>\n"; 
			}
			this.changed = this.oClauseEvalSet.changed;
			this.isExpressionValid = this.oClauseEvalSet.isExpressionValid;
		}
		
		if (oStg3ClauseCorrectionRecord != null) {
			if (sLog.isEmpty())
				sLog = "<h2>" + this.clauseName + "</h2>\n";
			
			if (CommonUtils.isNotEmpty(oStg3ClauseCorrectionRecord.clauseData))
				this.clauseData = oStg3ClauseCorrectionRecord.clauseData;
			
			oStg3ClauseCorrectionRecord.aFillInSet.copy(this.fillIns);
			//this.oStg3ClauseFilInCorrectionSet = oStg3ClauseCorrectionRecord.aFillInSet;
			
			sLog = "<h2>" + this.clauseName + "</h2>\n";
			sLog += "Applied clause correction for fill-in";
		}
		
		if (!sLog.isEmpty() && (bReport == false))
			sLog = "/*\n" + sLog + "\n*/";
		
		return sLog;
	}

	/*
	public boolean active;
	//public String conditionToClause;
	public String additionalConditions;
	public boolean editable;
	public boolean optional;
	public Integer clauseVersionId;

	public Date createdAt;
	public Date updatedAt;
	
	public String inclusionName;
	public String clauseSectionCode;
	public String clauseSectionHeader;
	public String clauseVersionName;
	public String regulationName;

	*/

	/*
	public Integer clauseId;
	public String clauseName;
	public Integer clauseSectionId;
	public String inclusion;
	public Integer regulationId;
	public String clauseTitle;
	public String clauseData;
	public String url;
	public Date effectiveDate;
	public String clauseConditions;
	public String clauseRule;
	public String additionalConditions = null;
	*/
	
	public static String dateToString(Date pDate) {
		if (pDate == null)
			return "NULL";
		return "'" + ParsedClauseRecord.formatDate.format(pDate) + "'";
	}
	
	private String getPrescriptionDifferences(ClauseRecord oClauseRecord, ParsedPrescriptionSet parsedPrescriptionSet) {
		String sHtml = "\n<table><caption>Clause Prescriptions</caption>";
		boolean bFound = false;
		
		ClausePrescriptionTable oClausePrescriptionTable;
		if (oClauseRecord != null)
			oClausePrescriptionTable = oClauseRecord.getClausePrescriptionTable();
		else
			oClausePrescriptionTable = new ClausePrescriptionTable(); 
		ClausePrescriptionTable oFoundPrescriptionRecords = new ClausePrescriptionTable();
		
		for (String prescription : this.prescriptions) {
			Integer iId = parsedPrescriptionSet.getId(prescription);
			if (iId == null) {
				sHtml += "\n<tr><th>New (Unknown)</th><td>" + prescription + "</td></tr>";
				bFound = true;
				System.out.println("Unable to locate prescription: '" + prescription + "'");
			} else {
				ClausePrescriptionRecord oClausePrescriptionRecord = oClausePrescriptionTable.findByPrescriptonId(iId);
				if (oClausePrescriptionRecord == null) {
					sHtml += "\n<tr><th>New</th><td>" + prescription + "</td></tr>";
					bFound = true;
				} else {
					oFoundPrescriptionRecords.add(oClausePrescriptionRecord);
				}
			}
		}
		
		for (ClausePrescriptionRecord oClausePrescriptionRecord : oClausePrescriptionTable) {
			if (!oFoundPrescriptionRecords.contains(oClausePrescriptionRecord)) {
				String prescription = parsedPrescriptionSet.getName(oClausePrescriptionRecord.getPrescriptionId());
				if ((prescription != null) && (!prescription.isEmpty())) {
					sHtml += "\n<tr><th>Remove</th><td>" + prescription + "</td></tr>";
					bFound = true;
				}
			}
		}
		if (bFound) {
			sHtml += "\n</table>";
			return sHtml;
		} else
			return "";
	}
	
	private String genPrescriptionSql(ClauseRecord oClauseRecord, ParsedPrescriptionSet parsedPrescriptionSet) {
		String sSql = "";
		
		ClausePrescriptionTable oClausePrescriptionTable;
		if (oClauseRecord != null)
			oClausePrescriptionTable = oClauseRecord.getClausePrescriptionTable();
		else
			oClausePrescriptionTable = new ClausePrescriptionTable(); 
		ClausePrescriptionTable oFoundPrescriptionRecords = new ClausePrescriptionTable();
		
		String sSqlPrescriptions = "";
		for (String prescription : this.prescriptions) {
			Integer iId = parsedPrescriptionSet.getId(prescription);
			if (iId == null) {
				System.out.println("Unable to locate prescription: '" + prescription + "'");
			} else {
				ClausePrescriptionRecord oClausePrescriptionRecord = oClausePrescriptionTable.findByPrescriptonId(iId);
				if (oClausePrescriptionRecord == null) {
					sSqlPrescriptions += (sSqlPrescriptions.isEmpty() ? "" : ",") + SqlCommons.quoteString(prescription);
					/*
					sSql += "\nINSERT INTO " + ClausePrescriptionRecord.TABLE_CLAUSE_PRESCRIPTIONS
							+ " (" + ClausePrescriptionRecord.FIELD_CLAUSE_ID
							+ ", " +  ClausePrescriptionRecord.FIELD_PRESCRIPTION_ID
							+ ") VALUES (" + this.clauseId
							+ ", " + iId
							+ "); -- " + prescription;
					*/
				} else {
					oFoundPrescriptionRecords.add(oClausePrescriptionRecord);
				}
			}
		}
		
		if (!sSqlPrescriptions.isEmpty()) {
			/*
			sSql += "\nINSERT INTO Prescriptions (regulation_id, prescription_name)"
					+ " VALUES (1, '10.003')"
					+ " ON DUPLICATE KEY UPDATE regulation_id=regulation_id;";
			*/
			sSql += "\nINSERT INTO " + ClausePrescriptionRecord.TABLE_CLAUSE_PRESCRIPTIONS
					+ " (" + ClausePrescriptionRecord.FIELD_CLAUSE_ID
					+ ", " + ClausePrescriptionRecord.FIELD_PRESCRIPTION_ID
					+ ")" +
					"\nSELECT " + (oClauseRecord == null ? this.clauseId : oClauseRecord.getId()) + ", " + PrescriptionRecord.FIELD_PRESCRIPTION_ID +
					" FROM " + PrescriptionRecord.TABLE_PRESCRIPTIONS + 
					"\nWHERE " + PrescriptionRecord.FIELD_PRESCRIPTION_NAME + " IN (" + sSqlPrescriptions + "); -- " + this.clauseName;
		}
		
		sSqlPrescriptions = "";
		for (ClausePrescriptionRecord oClausePrescriptionRecord : oClausePrescriptionTable) {
			if (!oFoundPrescriptionRecords.contains(oClausePrescriptionRecord)) {
				String prescription = parsedPrescriptionSet.getName(oClausePrescriptionRecord.getPrescriptionId());
				if ((prescription != null) && (!prescription.isEmpty()))
					sSqlPrescriptions += (sSqlPrescriptions.isEmpty() ? "" : ",") + SqlCommons.quoteString(prescription);
				/*
				sSql += "\nDELETE FROM " + ClausePrescriptionRecord.TABLE_CLAUSE_PRESCRIPTIONS
						+ " WHERE " + ClausePrescriptionRecord.FIELD_CLAUSE_PRESCRIPTION_ID
						+ " = " +  oClausePrescriptionRecord.getId()
						+ ";" + ((prescription == null) ? "" : " -- " + prescription);
				*/
			}
		}
		if (!sSqlPrescriptions.isEmpty()) {
			sSql += "\nDELETE FROM " + ClausePrescriptionRecord.TABLE_CLAUSE_PRESCRIPTIONS
					+ " WHERE " + ClausePrescriptionRecord.FIELD_CLAUSE_ID
					+ " = " +  (oClauseRecord == null ? this.clauseId : oClauseRecord.getId())
					+ " AND " + ClausePrescriptionRecord.FIELD_PRESCRIPTION_ID + " IN " +
					"\n(SELECT " + PrescriptionRecord.FIELD_PRESCRIPTION_ID +
					" FROM " + PrescriptionRecord.TABLE_PRESCRIPTIONS + 
					" WHERE " + PrescriptionRecord.FIELD_PRESCRIPTION_NAME + " IN (" + sSqlPrescriptions + ")); -- " + this.clauseName;
		}

		return sSql;
	}
	
	private String genPrescriptionSql_wFilter(ClauseRecord oClauseRecord, ParsedPrescriptionSet parsedPrescriptionSet, int iClauseVersionId) {
		String sSql = "";
		
		ClausePrescriptionTable oClausePrescriptionTable;
		if (oClauseRecord != null)
			oClausePrescriptionTable = oClauseRecord.getClausePrescriptionTable();
		else
			oClausePrescriptionTable = new ClausePrescriptionTable(); 
		ClausePrescriptionTable oFoundPrescriptionRecords = new ClausePrescriptionTable();
		
		String sSqlPrescriptions = "";
		for (String prescription : this.prescriptions) {
			Integer iId = parsedPrescriptionSet.getId(prescription);
			if (iId == null) {
				System.out.println("Unable to locate prescription: '" + prescription + "'");
			} else {
				ClausePrescriptionRecord oClausePrescriptionRecord = oClausePrescriptionTable.findByPrescriptonId(iId);
				if (oClausePrescriptionRecord == null) {
					sSqlPrescriptions += (sSqlPrescriptions.isEmpty() ? "" : ",") + SqlCommons.quoteString(prescription);
					
				} else {
					oFoundPrescriptionRecords.add(oClausePrescriptionRecord);
				}
			}
		}
		
		if (!sSqlPrescriptions.isEmpty()) {

			// INSERT INTO Clause_Prescriptions (clause_id, prescription_id)
			// SELECT clause_id, prescription_id 
			// FROM Prescriptions P, Clauses C
			// WHERE prescription_name IN ('204.404-70(b)')
			// AND clause_id IN (SELECT clause_id FROM Clauses where clause_name = '252.204-7003' and clause_version_id = 7)
			
			sSql += "\nINSERT INTO " + ClausePrescriptionRecord.TABLE_CLAUSE_PRESCRIPTIONS
					+ " (" + ClausePrescriptionRecord.FIELD_CLAUSE_ID
					+ ", " + ClausePrescriptionRecord.FIELD_PRESCRIPTION_ID + ")" +
					"\nSELECT " + ClauseRecord.FIELD_CLAUSE_ID + ", " + PrescriptionRecord.FIELD_PRESCRIPTION_ID +
					" FROM " + PrescriptionRecord.TABLE_PRESCRIPTIONS + ", " + ClauseRecord.TABLE_CLAUSES + 
					"\nWHERE " + PrescriptionRecord.FIELD_PRESCRIPTION_NAME + " IN (" + sSqlPrescriptions + ") " +
					"\nAND " + ClausePrescriptionRecord.FIELD_CLAUSE_ID + 
					" IN (SELECT " + ClausePrescriptionRecord.FIELD_CLAUSE_ID + " FROM " + ClauseRecord.TABLE_CLAUSES + 
					" WHERE " + ClauseRecord.FIELD_CLAUSE_NAME + " = '" + this.clauseName + 
					"' AND " + ClauseRecord.FIELD_CLAUSE_VERSION_ID + " = " + iClauseVersionId + "); "
;		}
		
		sSqlPrescriptions = "";
		for (ClausePrescriptionRecord oClausePrescriptionRecord : oClausePrescriptionTable) {
			if (!oFoundPrescriptionRecords.contains(oClausePrescriptionRecord)) {
				String prescription = parsedPrescriptionSet.getName(oClausePrescriptionRecord.getPrescriptionId());
				if ((prescription != null) && (!prescription.isEmpty()))
					sSqlPrescriptions += (sSqlPrescriptions.isEmpty() ? "" : ",") + SqlCommons.quoteString(prescription);
			}
		}
		if (!sSqlPrescriptions.isEmpty()) {
			// DELETE FROM Clause_Prescriptions 
			// WHERE clause_id IN (SELECT clause_id FROM Clauses WHERE clause_name = '52.215-21' AND clause_version_id = 8) 
			// AND prescription_id IN (SELECT prescription_id FROM Prescriptions WHERE prescription_name IN ('15.408(i)','215.408(4)(II)')); -- 52.215-20
					
			sSql += "\nDELETE FROM " + ClausePrescriptionRecord.TABLE_CLAUSE_PRESCRIPTIONS
					+ "\nWHERE " + ClausePrescriptionRecord.FIELD_CLAUSE_ID
					+ " IN (SELECT " + ClausePrescriptionRecord.FIELD_CLAUSE_ID + " FROM " + ClauseRecord.TABLE_CLAUSES 
					+ " WHERE " + ClauseRecord.FIELD_CLAUSE_NAME + " = '" + this.clauseName 
					+ "' AND " + ClauseRecord.FIELD_CLAUSE_VERSION_ID + " = " + iClauseVersionId + ") "
					+ "\nAND " + ClausePrescriptionRecord.FIELD_PRESCRIPTION_ID + " IN " 
					+ "(SELECT " + PrescriptionRecord.FIELD_PRESCRIPTION_ID 
					+ " FROM " + PrescriptionRecord.TABLE_PRESCRIPTIONS 
					+ " WHERE " + PrescriptionRecord.FIELD_PRESCRIPTION_NAME + " IN (" + sSqlPrescriptions + ")); -- " + this.clauseName;
		}

		return sSql;
	}
	
	private String getFillInDifferences(ClauseRecord oClauseRecord) {
		String sHtml = "\n<table><caption>Clause Prescriptions</caption>";
		boolean bFound = false;
		
		ClauseFillInTable oClauseFillInTable;
		if (oClauseRecord != null)
			oClauseFillInTable = oClauseRecord.getClauseFillInTable();
		else
			oClauseFillInTable = new ClauseFillInTable();
		ClauseFillInTable oFoundFillInRecords = new ClauseFillInTable();
		
		for (ParsedClauseFillInRecord oParsedFillInRecord : this.fillIns) {
			ClauseFillInRecord oClauseFillInRecord = null;
			for (ClauseFillInRecord oFillInRecord : oClauseFillInTable) {
				if (oParsedFillInRecord.isMatch(oFillInRecord)) {
					oClauseFillInRecord = oFillInRecord;
					break;
				}
			}
			if (oClauseFillInRecord == null) {
				sHtml += "\n<tr><th>New</th><td>" + oParsedFillInRecord.code + "</td><td>" + oParsedFillInRecord.type + "</td><td>" + oParsedFillInRecord.maxSize + "</td></tr>";
				bFound = true;
			} else {
				oFoundFillInRecords.add(oClauseFillInRecord);
				if (oParsedFillInRecord.isDiffernt(oClauseFillInRecord)) {
					sHtml += "\n<tr><th>Changed</th><td>" + oParsedFillInRecord.code + "</td><td>" + oParsedFillInRecord.type + "</td><td>" + oParsedFillInRecord.maxSize + "</td></tr>";
					bFound = true;
				}
			}
		}
		
		for (ClauseFillInRecord oClauseFillInRecord : oClauseFillInTable) {
			if (!oFoundFillInRecords.contains(oClauseFillInRecord)) {
				sHtml += "\n<tr><th>Remove</th><td>" + oClauseFillInRecord.getFillInCode() + "</td><td>" + oClauseFillInRecord.getFillInType() + "</td><td>" + oClauseFillInRecord.getFillInMaxSize() + "</td></tr>";
				bFound = true;
			}
		}
		if (bFound) {
			sHtml += "\n</table>";
			return sHtml;
		} else
			return "";
	}

	// CJ-533
	private String genFillInSql(ClauseRecord oClauseRecord) {
		String sSql = "";
		ClauseFillInTable oClauseFillInTable;
		if (oClauseRecord != null)
			oClauseFillInTable = oClauseRecord.getClauseFillInTable();
		else
			oClauseFillInTable = new ClauseFillInTable();
		ClauseFillInTable oFoundFillInRecords = new ClauseFillInTable();
		
		for (ParsedClauseFillInRecord oParsedFillInRecord : this.fillIns) {
			ClauseFillInRecord oClauseFillInRecord = null;
			for (ClauseFillInRecord oFillInRecord : oClauseFillInTable) {
				if (oParsedFillInRecord.isMatch(oFillInRecord)) {
					oClauseFillInRecord = oFillInRecord;
					break;
				}
			}
			if (oClauseFillInRecord == null) {
				sSql += "\n" + oParsedFillInRecord.genSqlInsert((oClauseRecord == null) ? this.clauseId : oClauseRecord.getId());
				/*
				sSql += "\nINSERT INTO " + ClauseFillInRecord.TABLE_CLAUSE_FILL_INS
						+ " (" + ClauseFillInRecord.FIELD_CLAUSE_ID
						+ ", " +  ClauseFillInRecord.FIELD_FILL_IN_CODE
						+ ", " +  ClauseFillInRecord.FIELD_FILL_IN_TYPE
						+ ", " +  ClauseFillInRecord.FIELD_FILL_IN_MAX_SIZE
						+ ") VALUES (" + ((oClauseRecord == null) ? this.clauseId : oClauseRecord.getId())  
						+ ", " + SqlCommons.quoteString(oParsedFillInRecord.code)
						+ ", " + SqlCommons.quoteString(oParsedFillInRecord.type)
						+ ", " + oParsedFillInRecord.maxSize
						+ ");";
				*/
			} else {
				oFoundFillInRecords.add(oClauseFillInRecord);
				sSql += oParsedFillInRecord.genSqlUpdate(oClauseFillInRecord);
			}
		}
		
		for (ClauseFillInRecord oClauseFillInRecord : oClauseFillInTable) {
			if (!oFoundFillInRecords.contains(oClauseFillInRecord)) {
				sSql += "\nDELETE FROM Document_Fill_Ins WHERE Clause_Fill_In_Id\n"
						+ "IN (SELECT Clause_Fill_In_Id FROM Clause_Fill_Ins WHERE clause_id = " + ((oClauseRecord == null) ? this.clauseId.toString() : oClauseRecord.getId().toString())
						+ " AND " + ClauseFillInRecord.FIELD_FILL_IN_CODE + "=" + SqlUtil.quoteString(oClauseFillInRecord.getFillInCode()) + ");";
				sSql += "\nDELETE FROM " + ClauseFillInRecord.TABLE_CLAUSE_FILL_INS
						+ " WHERE " + ClauseFillInRecord.FIELD_CLAUSE_ID
						+ " = " + ((oClauseRecord == null) ? this.clauseId.toString() : oClauseRecord.getId().toString())
						+ " AND " + ClauseFillInRecord.FIELD_FILL_IN_CODE + "=" + SqlUtil.quoteString(oClauseFillInRecord.getFillInCode()) + ";";
						/*
						+ " WHERE " + ClauseFillInRecord.FIELD_CLAUSE_FILL_IN_ID
						+ " = " +  oClauseFillInRecord.getId()
						+ "; -- " + oClauseFillInRecord.getFillInCode();
						*/
			}
		}
		
		return sSql;
	}

	private String genFillInSql_wFilter(ClauseRecord oClauseRecord, int iClauseVersionId) {
		String sSql = "";
		ClauseFillInTable oClauseFillInTable;
		if (oClauseRecord != null)
			oClauseFillInTable = oClauseRecord.getClauseFillInTable();
		else
			oClauseFillInTable = new ClauseFillInTable();
		ClauseFillInTable oFoundFillInRecords = new ClauseFillInTable();
		
		for (ParsedClauseFillInRecord oParsedFillInRecord : this.fillIns) {
			ClauseFillInRecord oClauseFillInRecord = null;
			for (ClauseFillInRecord oFillInRecord : oClauseFillInTable) {
				if (oParsedFillInRecord.isMatch(oFillInRecord)) {
					oClauseFillInRecord = oFillInRecord;
					break;
				}
			}
			if (oClauseFillInRecord == null) {
				sSql += "\n" + oParsedFillInRecord.genSqlInsert_wFilter
						((oClauseRecord == null) ? this.clauseId : oClauseRecord.getId(), 
						(oClauseRecord == null) ? this.clauseName : oClauseRecord.getClauseName(),
						iClauseVersionId);
			} else {
				oFoundFillInRecords.add(oClauseFillInRecord);
				sSql += oParsedFillInRecord.genSqlUpdate_wFilter(oClauseFillInRecord, this.clauseName, iClauseVersionId);
			}
		}
		
		for (ClauseFillInRecord oClauseFillInRecord : oClauseFillInTable) {
			if (!oFoundFillInRecords.contains(oClauseFillInRecord)) {
				/*
					DELETE FROM Document_Fill_Ins WHERE Clause_Fill_In_Id IN
					(SELECT clause_fill_in_id FROM Clause_Fill_Ins CF INNER JOIN Clauses C  ON C.clause_id= CF.clause_id
					WHERE clause_name = '52.203-14' AND clause_version_id = 7 AND fill_in_code='subcontract_max_52.203-14');
					
					DELETE FROM Clause_Fill_Ins WHERE clause_id IN 
					(SELECT clause_id FROM Clauses WHERE clause_name = '52.203-14' AND clause_version_id = 7)  AND fill_in_code='subcontract_max_52.203-14';

				*/
				
				sSql += "\nDELETE FROM Document_Fill_Ins WHERE " + ClauseFillInRecord.FIELD_CLAUSE_FILL_IN_ID + 
					" IN (SELECT " + ClauseFillInRecord.FIELD_CLAUSE_FILL_IN_ID + 
					" FROM " + ClauseFillInRecord.TABLE_CLAUSE_FILL_INS + " CF " + 
					"INNER JOIN " + ClauseRecord.TABLE_CLAUSES + " C " + 
					" ON C." + ClauseRecord.FIELD_CLAUSE_ID +  "= CF." + ClauseFillInRecord.FIELD_CLAUSE_ID + 
					"\nWHERE clause_name = " + SqlUtil.quoteString((oClauseRecord == null) ? this.clauseName : oClauseRecord.getClauseName()) +
					" AND clause_version_id = " + iClauseVersionId + 
					" AND " + ClauseFillInRecord.FIELD_FILL_IN_CODE + "=" + SqlUtil.quoteString(oClauseFillInRecord.getFillInCode()) + ");"; ;
					
				sSql += "\nDELETE FROM " + ClauseFillInRecord.TABLE_CLAUSE_FILL_INS +
					" WHERE " + ClauseFillInRecord.FIELD_CLAUSE_ID +
					" IN (SELECT " + ClausePrescriptionRecord.FIELD_CLAUSE_ID + " FROM " + ClauseRecord.TABLE_CLAUSES +
					" \nWHERE " + ClauseRecord.FIELD_CLAUSE_NAME + " = '" + this.clauseName +
					"' AND " + ClauseRecord.FIELD_CLAUSE_VERSION_ID + " = " + iClauseVersionId + ") " +
					" AND " + ClauseFillInRecord.FIELD_FILL_IN_CODE + "=" + SqlUtil.quoteString(oClauseFillInRecord.getFillInCode()) + ";";
						
			}
		}
		
		return sSql;
	}
	
	public String getDifferences(int iClauseVersionId, ParsedPrescriptionSet parsedPrescriptionSet,
			ClauseVersionChangeRecord oClauseVersionChangeRecord) {
		String sHtml = "\n<table><tr><th>Field</th><th>New</th></tr>" +
				//"\n<tr><td>" + QuestionRecord.FIELD_CLAUSE_VERSION_ID + "</td><td>" + iClauseVersionId + " </td></tr>" + 
				"\n<tr><td>" + ClauseRecord.FIELD_CLAUSE_ID + "</td><td>" + this.clauseId + "</td></tr>" +
				"\n<tr><td>" + ClauseRecord.FIELD_CLAUSE_NAME + "</td><td>" + this.clauseName + "</td></tr>" +
				"\n<tr><td>" + ClauseRecord.FIELD_CLAUSE_SECTION_ID + "</td><td>" + this.clauseSectionId + "</td></tr>" +
				"\n<tr><td>" + ClauseRecord.FIELD_INCLUSION_CD + "</td><td>" + this.inclusion + "</td></tr>" +
				"\n<tr><td>" + ClauseRecord.FIELD_REGULATION_ID + "</td><td>" + this.regulationId + "</td></tr>" +
				"\n<tr><td>" + ClauseRecord.FIELD_CLAUSE_TITLE + "</td><td>" + this.clauseTitle + "</td></tr>" +
				"\n<tr><td>" + ClauseRecord.FIELD_CLAUSE_URL + "</td><td>" + this.url + "</td></tr>" +
				"\n<tr><td>" + ClauseRecord.FIELD_EFFECTIVE_DATE + "</td><td>" + dateToString(this.effectiveDate) + "</td></tr>" +
				"\n<tr><td>" + ClauseRecord.FIELD_PROVISON_OR_CLAUSE + "</td><td>" + this.provisionOrClause + "</td></tr>" +

				"\n<tr><td>" + ClauseRecord.FIELD_IS_OPTIONAL + "</td><td>" + this.isOptional() + "</td></tr>" +
				"\n<tr><td>" + ClauseRecord.FIELD_IS_EDITABLE + "</td><td>" + this.isEditable() + "</td></tr>" +
				"\n<tr><td>" + ClauseRecord.FIELD_EDITABLE_REMARKS + "</td><td>" + this.getEditableRemarks() + "</td></tr>" +
				"\n<tr><td>" + ClauseRecord.FIELD_IS_BASIC_CLAUSE + "</td><td>" + this.isBasicClause + "</td></tr>" +
				"\n<tr><td>" + ClauseRecord.FIELD_IS_DFARS_ACTIVITY + "</td><td>" + this.isDfarsActivity + "</td></tr>" +

				"\n<tr><td>" + ClauseRecord.FIELD_COMMERCIAL_STATUS + "</td><td>" + this.commercialStatus + "</td></tr>" +
				"\n<tr><td>" + ClauseRecord.FIELD_OPTIONAL_STATUS + "</td><td>" + this.optionalStatus + "</td></tr>" +
				"\n<tr><td>" + ClauseRecord.FIELD_OPTIONAL_CONDITIONS + "</td><td>" + this.optionalConditions + "</td></tr>" +

				"\n<tr><td>" + ClauseRecord.FIELD_CLAUSE_CONDITIONS + "</td><td>" + this.clauseConditions + "</td></tr>" +
				"\n<tr><td>" + ClauseRecord.FIELD_CLAUSE_RULE + "</td><td>" + this.clauseRule + "</td></tr>" +
				"\n<tr><td>" + ClauseRecord.FIELD_ADDITIONAL_CONDITIONS + "</td><td>" + this.additionalConditions + "</td></tr>" +
				"\n<tr><td>" + ClauseRecord.FIELD_CLAUSE_DATA + "</td><td>" + this.clauseData + "</td></tr>" +
				"\n</table>";
		
		sHtml += this.getPrescriptionDifferences(null, parsedPrescriptionSet);
		sHtml += this.getFillInDifferences(null);
		
		oClauseVersionChangeRecord.setClause(this.clauseName);
		oClauseVersionChangeRecord.setAdded();
		
		return "\n<h2>" + this.clauseName + " (New)</h2>" + sHtml;
	}
	
	public String getInsertSql(int iClauseVersionId, ParsedPrescriptionSet parsedPrescriptionSet,
			ClauseVersionChangeRecord oClauseVersionChangeRecord) {
		String sSql = "INSERT INTO " + ClauseRecord.TABLE_CLAUSES +
				//public static final String FIELD_QUESTION_ID = "question_id";
				" (" + ClauseRecord.FIELD_CLAUSE_VERSION_ID +
				", " + ClauseRecord.FIELD_CLAUSE_ID +
				", " + ClauseRecord.FIELD_CLAUSE_NAME +
				", " + ClauseRecord.FIELD_CLAUSE_SECTION_ID +
				", " + ClauseRecord.FIELD_INCLUSION_CD +
				", " + ClauseRecord.FIELD_REGULATION_ID + 
				", " + ClauseRecord.FIELD_CLAUSE_TITLE + 
				", " + ClauseRecord.FIELD_CLAUSE_URL + 
				", " + ClauseRecord.FIELD_EFFECTIVE_DATE + 
				", " + ClauseRecord.FIELD_CLAUSE_CONDITIONS + 
				", " + ClauseRecord.FIELD_CLAUSE_RULE + 
				", " + ClauseRecord.FIELD_ADDITIONAL_CONDITIONS + 
				", " + ClauseRecord.FIELD_IS_ACTIVE + 
				", " + ClauseRecord.FIELD_PROVISON_OR_CLAUSE + 
				", " + ClauseRecord.FIELD_COMMERCIAL_STATUS +
				", " + ClauseRecord.FIELD_OPTIONAL_STATUS +
				", " + ClauseRecord.FIELD_OPTIONAL_CONDITIONS +

				", " + ClauseRecord.FIELD_IS_OPTIONAL + 
				", " + ClauseRecord.FIELD_IS_EDITABLE + 
				", " + ClauseRecord.FIELD_EDITABLE_REMARKS + 
				", " + ClauseRecord.FIELD_IS_BASIC_CLAUSE +
				", " + ClauseRecord.FIELD_IS_DFARS_ACTIVITY + 
				
				", " + ClauseRecord.FIELD_CLAUSE_DATA + 
				")" +
				"\nVALUES (" + iClauseVersionId +
				", " + this.clauseId +
				", " + SqlCommons.quoteString(this.clauseName) +
				", " + this.clauseSectionId +
				", " + SqlCommons.quoteString(this.inclusion) +
				", " + this.regulationId + 
				", " + SqlCommons.quoteString(this.clauseTitle) + 
				", " + SqlCommons.quoteString(this.url) + 
				", " + dateToString(this.effectiveDate) + 
				", " + SqlCommons.quoteString(this.clauseConditions) + 
				", " + SqlCommons.quoteString(this.clauseRule) + 
				", " + SqlCommons.quoteString(this.additionalConditions) +
				", 1" +
				", " + SqlCommons.quoteString(this.provisionOrClause) +
				", " + SqlCommons.quoteString(this.commercialStatus) +
				", " + SqlCommons.quoteString(this.optionalStatus) +
				", " + SqlCommons.quoteString(this.optionalConditions) +

				", " + SqlCommons.quoteString(this.isOptional()) + // ClauseRecord.FIELD_IS_OPTIONAL
				", " + SqlCommons.quoteString(this.isEditable()) + // ClauseRecord.FIELD_IS_EDITABLE
				", " + SqlCommons.quoteString(this.getEditableRemarks()) + // ClauseRecord.FIELD_EDITABLE_REMARKS
				", " + SqlCommons.quoteString(this.isBasicClause) +
				", " + SqlCommons.quoteString(this.isDfarsActivity) +

				"\n, " + SqlCommons.quoteString(this.clauseData) + 
				");";
		
		sSql += this.genPrescriptionSql(null, parsedPrescriptionSet);
		sSql += this.genFillInSql(null);
		
		if (oClauseVersionChangeRecord != null) {
			oClauseVersionChangeRecord.setClause(this.clauseName);
			oClauseVersionChangeRecord.setAdded();
		}
		
		return sSql;
	}
	
	public String getInsertSql_wFilter(int iClauseVersionId, ParsedPrescriptionSet parsedPrescriptionSet,
			ClauseVersionChangeRecord oClauseVersionChangeRecord) {
		String sSql = "INSERT INTO " + ClauseRecord.TABLE_CLAUSES +
				//public static final String FIELD_QUESTION_ID = "question_id";
				" (" + ClauseRecord.FIELD_CLAUSE_VERSION_ID +
				//", " + ClauseRecord.FIELD_CLAUSE_ID +
				", " + ClauseRecord.FIELD_CLAUSE_NAME +
				", " + ClauseRecord.FIELD_CLAUSE_SECTION_ID +
				", " + ClauseRecord.FIELD_INCLUSION_CD +
				", " + ClauseRecord.FIELD_REGULATION_ID + 
				", " + ClauseRecord.FIELD_CLAUSE_TITLE + 
				", " + ClauseRecord.FIELD_CLAUSE_URL + 
				", " + ClauseRecord.FIELD_EFFECTIVE_DATE + 
				", " + ClauseRecord.FIELD_CLAUSE_CONDITIONS + 
				", " + ClauseRecord.FIELD_CLAUSE_RULE + 
				", " + ClauseRecord.FIELD_ADDITIONAL_CONDITIONS + 
				", " + ClauseRecord.FIELD_IS_ACTIVE + 
				", " + ClauseRecord.FIELD_PROVISON_OR_CLAUSE + 
				", " + ClauseRecord.FIELD_COMMERCIAL_STATUS +
				", " + ClauseRecord.FIELD_OPTIONAL_STATUS +
				", " + ClauseRecord.FIELD_OPTIONAL_CONDITIONS +
				", " + ClauseRecord.FIELD_IS_OPTIONAL + 
				", " + ClauseRecord.FIELD_IS_EDITABLE + 
				", " + ClauseRecord.FIELD_EDITABLE_REMARKS + 
				", " + ClauseRecord.FIELD_IS_BASIC_CLAUSE +
				", " + ClauseRecord.FIELD_IS_DFARS_ACTIVITY +
				
				", " + ClauseRecord.FIELD_CLAUSE_DATA + 
				")" +
				"\nVALUES (" + iClauseVersionId +
				//", " + this.clauseId +
				", " + SqlCommons.quoteString(this.clauseName) +
				", " + this.clauseSectionId +
				", " + SqlCommons.quoteString(this.inclusion) +
				", " + this.regulationId + 
				", " + SqlCommons.quoteString(this.clauseTitle) + 
				", " + SqlCommons.quoteString(this.url) + 
				", " + dateToString(this.effectiveDate) + 
				", " + SqlCommons.quoteString(this.clauseConditions) + 
				", " + SqlCommons.quoteString(this.clauseRule) + 
				", " + SqlCommons.quoteString(this.additionalConditions) +
				", 1" +
				", " + SqlCommons.quoteString(this.provisionOrClause) +
				", " + SqlCommons.quoteString(this.commercialStatus) +
				", " + SqlCommons.quoteString(this.optionalStatus) +
				", " + SqlCommons.quoteString(this.optionalConditions) +

				", " + SqlCommons.quoteString(this.isOptional()) + // ClauseRecord.FIELD_IS_OPTIONAL
				", " + SqlCommons.quoteString(this.isEditable()) + // ClauseRecord.FIELD_IS_EDITABLE
				", " + SqlCommons.quoteString(this.getEditableRemarks()) + // ClauseRecord.FIELD_EDITABLE_REMARKS
				", " + SqlCommons.quoteString(this.isBasicClause) +
				", " + SqlCommons.quoteString(this.isDfarsActivity) +

				"\n, " + SqlCommons.quoteString(this.clauseData) + 
				");";
		
		sSql += this.genPrescriptionSql_wFilter(null, parsedPrescriptionSet, iClauseVersionId);
		sSql += this.genFillInSql_wFilter(null, iClauseVersionId);
		
		if (oClauseVersionChangeRecord != null) {
			oClauseVersionChangeRecord.setClause(this.clauseName);
			oClauseVersionChangeRecord.setAdded();
		}
		
		return sSql;
	}
	
	public static String getDeleteHtml(ClauseRecord oClauseRecord, ClauseVersionChangeRecord oClauseVersionChangeRecord) {
		String sHtml = "\n<table><tr><th>Field</th><th>Value</th></tr>" +
				//"\n<tr><td>" + QuestionRecord.FIELD_CLAUSE_VERSION_ID + "</td><td>" + iClauseVersionId + "</td></tr>" + 
				"\n<tr><td>" + ClauseRecord.FIELD_CLAUSE_ID + "</td><td>" + oClauseRecord.getId() + "</td></tr>" +
				"\n<tr><td>" + ClauseRecord.FIELD_CLAUSE_NAME + "</td><td>" + oClauseRecord.getClauseName() + "</td></tr>" +
				"\n<tr><td>" + ClauseRecord.FIELD_CLAUSE_SECTION_ID + "</td><td>" + oClauseRecord.getClauseSectionId() + "</td></tr>" +
				"\n<tr><td>" + ClauseRecord.FIELD_INCLUSION_CD + "</td><td>" + oClauseRecord.getClauseSectionId() + "</td></tr>" +
				"\n<tr><td>" + ClauseRecord.FIELD_REGULATION_ID + "</td><td>" + oClauseRecord.getRegulationId() + "</td></tr>" +
				"\n<tr><td>" + ClauseRecord.FIELD_CLAUSE_TITLE + "</td><td>" + oClauseRecord.getClauseTitle() + "</td></tr>" +
				"\n<tr><td>" + ClauseRecord.FIELD_CLAUSE_URL + "</td><td>" + oClauseRecord.getUrl() + "</td></tr>" +
				"\n<tr><td>" + ClauseRecord.FIELD_EFFECTIVE_DATE + "</td><td>" + dateToString(oClauseRecord.getEffectiveDate()) + "</td></tr>" +
				"\n<tr><td>" + ClauseRecord.FIELD_PROVISON_OR_CLAUSE + "</td><td>" + oClauseRecord.getProvisonOrClause() + "</td></tr>" +
				"\n<tr><td>" + ClauseRecord.FIELD_CLAUSE_CONDITIONS + "</td><td>" + oClauseRecord.getClauseConditions() + "</td></tr>" +
				"\n<tr><td>" + ClauseRecord.FIELD_CLAUSE_RULE + "</td><td>" + oClauseRecord.getClauseRule() + "</td></tr>" +
				"\n<tr><td>" + ClauseRecord.FIELD_ADDITIONAL_CONDITIONS + "</td><td>" + oClauseRecord.getAdditionalConditions() + "</td></tr>" +
				"\n<tr><td>" + ClauseRecord.FIELD_COMMERCIAL_STATUS + "</td><td>" + oClauseRecord.getCommercialStatus() + "</td></tr>" +
				"\n<tr><td>" + ClauseRecord.FIELD_OPTIONAL_STATUS + "</td><td>" + oClauseRecord.getOptionalStatus() + "</td></tr>" +
				"\n<tr><td>" + ClauseRecord.FIELD_OPTIONAL_CONDITIONS + "</td><td>" + oClauseRecord.getOptionalConditions() + "</td></tr>" +
				"\n<tr><td>" + ClauseRecord.FIELD_IS_OPTIONAL + "</td><td>" + oClauseRecord.isOptional() + "</td></tr>" +
				"\n<tr><td>" + ClauseRecord.FIELD_IS_EDITABLE + "</td><td>" + oClauseRecord.isEditable() + "</td></tr>" +
				"\n<tr><td>" + ClauseRecord.FIELD_EDITABLE_REMARKS + "</td><td>" + oClauseRecord.getEditableRemarks() + "</td></tr>" +
				"\n<tr><td>" + ClauseRecord.FIELD_IS_BASIC_CLAUSE + "</td><td>" + oClauseRecord.isBasicClause() + "</td></tr>" +
				"\n<tr><td>" + ClauseRecord.FIELD_IS_DFARS_ACTIVITY + "</td><td>" + oClauseRecord.getIsDfarsActivity() + "</td></tr>" +
				
				"\n<tr><td>" + ClauseRecord.FIELD_CLAUSE_DATA + "</td><td>" + oClauseRecord.getClauseData() + "</td></tr>" +
				"\n</table>";
		
		//sHtml += oQuestionChoiceSet.getDifferences(null, parsedPrescriptionSet);
		oClauseVersionChangeRecord.setClause(oClauseRecord.getClauseName());
		oClauseVersionChangeRecord.setRemoved();
		
		return "\n<h2>" + oClauseRecord.getClauseName() + " (Remove)</h2>" + sHtml;
	}
	
	public static String getDeleteSql(ClauseRecord oClauseRecord, ClauseVersionChangeRecord oClauseVersionChangeRecord) {
		String sSql = "\nDELETE FROM Document_Fill_Ins WHERE Clause_Fill_In_Id\n"
				+ "IN (SELECT Clause_Fill_In_Id FROM Clause_Fill_Ins WHERE clause_id = " + oClauseRecord.getId() + "); -- " + oClauseRecord.getClauseName();
		sSql += "\nDELETE FROM " + ClauseFillInRecord.TABLE_CLAUSE_FILL_INS
				+ " WHERE " + ClauseFillInRecord.FIELD_CLAUSE_ID + " = " + oClauseRecord.getId() + "; -- " + oClauseRecord.getClauseName();
		sSql +=	"\nDELETE FROM " + DocumentClauseRecord.TABLE_DOCUMENT_CLAUSES +
				" WHERE " + DocumentClauseRecord.FIELD_CLAUSE_ID + " = " + oClauseRecord.getId() + "; -- " + oClauseRecord.getClauseName();
		sSql += "\nDELETE FROM " + ClauseRecord.TABLE_CLAUSES +
				" WHERE " + ClauseRecord.FIELD_CLAUSE_ID + " = " + oClauseRecord.getId() + "; -- " + oClauseRecord.getClauseName();
		oClauseVersionChangeRecord.setClause(oClauseRecord.getClauseName());
		oClauseVersionChangeRecord.setRemoved();
		return sSql;
	}
	
	public static String getDeleteSql_wFilter(ClauseRecord oClauseRecord, int iClauseVersionId, ClauseVersionChangeRecord oClauseVersionChangeRecord) {
		String sSql = "\nDELETE FROM Document_Fill_Ins WHERE Clause_Fill_In_Id\n"
				+ "IN (SELECT Clause_Fill_In_Id FROM Clause_Fill_Ins WHERE clause_id IN\n " 
				+ "(SELECT Clause_id FROM Clauses WHERE Clause_Name = " + SqlCommons.quoteString(oClauseRecord.getClauseName()) + " AND Clause_Version_Id = " + iClauseVersionId + "));"; 
		sSql += "\nDELETE FROM " + ClauseFillInRecord.TABLE_CLAUSE_FILL_INS
				+ " WHERE " + ClauseFillInRecord.FIELD_CLAUSE_ID + " IN\n " 
				+ "(SELECT Clause_id FROM Clauses WHERE Clause_Name = " + SqlCommons.quoteString(oClauseRecord.getClauseName()) + " AND Clause_Version_Id = " + iClauseVersionId + ");"; 
		sSql +=	"\nDELETE FROM " + DocumentClauseRecord.TABLE_DOCUMENT_CLAUSES +
				" WHERE " + DocumentClauseRecord.FIELD_CLAUSE_ID + " IN\n " 
						+ "(SELECT Clause_id FROM Clauses WHERE Clause_Name = " + SqlCommons.quoteString(oClauseRecord.getClauseName()) + " AND Clause_Version_Id = " + iClauseVersionId + ");"; 
		sSql += "\nDELETE FROM " + ClauseRecord.TABLE_CLAUSES +
				" WHERE Clause_Name = " + SqlCommons.quoteString(oClauseRecord.getClauseName()) + " AND Clause_Version_Id = " + iClauseVersionId + ";";
		oClauseVersionChangeRecord.setClause(oClauseRecord.getClauseName());
		oClauseVersionChangeRecord.setRemoved();
		return sSql;
	}

	private String genTr(String psNew, String psPrevious, String psField,
			ClauseVersionChangeRecord oClauseVersionChangeRecord) {
		if (psNew != null)
			psNew = psNew.trim();
		if (psPrevious != null)
			psPrevious = psPrevious.trim();
		if (CommonUtils.isNotSame(psNew, psPrevious)) {
			oClauseVersionChangeRecord.addChangeBrief(psField);
			return "\n<tr><td rowspan='2'>" + psField + "</th>" + 
					    "\n<td>new</td>     <td>" + psNew + "</td></tr>" +
					"<tr>\n<td>Previous</td><td>" + psPrevious + "</td></tr>";
		} else
			return "";
	}
	
	private String genTr(Boolean pbNew, Boolean pbPrevious, String psField,
			ClauseVersionChangeRecord oClauseVersionChangeRecord) {
		String sNew = (pbNew == null) ? "0" : (pbNew.booleanValue() ? "1" : "0");
		String sPrevious = (pbPrevious == null) ? "0" : (pbPrevious.booleanValue() ? "1" : "0");
		return genTr(sNew, sPrevious, psField, oClauseVersionChangeRecord);
	}
	
	private String genTr(Integer psNew, Integer psPrevious, String psField,
			ClauseVersionChangeRecord oClauseVersionChangeRecord) {
		if (CommonUtils.isNotSame(psNew, psPrevious)) {
			oClauseVersionChangeRecord.addChangeBrief(psField);
			return "\n<tr><td rowspan='2'>" + psField + "</th><td>new</td><td>" + psNew + "</td></tr>" +
						"\n<tr><td>Previous</td><td>" + psPrevious + "</td></tr>";
		} else
			return "";
	}
	
	public String getDifferences(int iClauseVersionId, ParsedPrescriptionSet parsedPrescriptionSet, ClauseRecord oClauseRecord,
			ClauseVersionChangeRecord oClauseVersionChangeRecord) {
		String sHtml = ""; // "\n[" + this.questionCode + "]\n<table><tr><th>Field</th><th>New</th><th>Previous</th></tr>";
		
		sHtml += genTr(this.clauseSectionId, oClauseRecord.getClauseSectionId(), ClauseRecord.FIELD_CLAUSE_SECTION_ID, oClauseVersionChangeRecord);

		sHtml += genTr(this.inclusion, oClauseRecord.getInclusion(), ClauseRecord.FIELD_INCLUSION_CD, oClauseVersionChangeRecord);
		sHtml += genTr(this.regulationId, oClauseRecord.getRegulationId(), ClauseRecord.FIELD_REGULATION_ID, oClauseVersionChangeRecord);
		sHtml += genTr(this.clauseTitle, oClauseRecord.getClauseTitle(), ClauseRecord.FIELD_CLAUSE_TITLE, oClauseVersionChangeRecord);
		// sHtml += genTr(this.url, oClauseRecord.getUrl(), ClauseRecord.FIELD_CLAUSE_URL);
 
		String sNewEffective = dateToString(this.effectiveDate);
		String sOldEffecive = dateToString(oClauseRecord.getEffectiveDate());
		sHtml += genTr(sNewEffective, sOldEffecive, ClauseRecord.FIELD_EFFECTIVE_DATE, oClauseVersionChangeRecord);
		sHtml += genTr(this.provisionOrClause, oClauseRecord.getProvisonOrClause(), ClauseRecord.FIELD_PROVISON_OR_CLAUSE, oClauseVersionChangeRecord);
		sHtml += genTr(this.commercialStatus, oClauseRecord.getCommercialStatus(), ClauseRecord.FIELD_COMMERCIAL_STATUS, oClauseVersionChangeRecord);
		sHtml += genTr(this.optionalStatus, oClauseRecord.getOptionalStatus(), ClauseRecord.FIELD_OPTIONAL_STATUS, oClauseVersionChangeRecord);
		sHtml += genTr(this.optionalConditions, oClauseRecord.getOptionalConditions(), ClauseRecord.FIELD_OPTIONAL_CONDITIONS, oClauseVersionChangeRecord);

		sHtml += genTr(SqlCommons.quoteString(this.clauseConditions), SqlCommons.quoteString(oClauseRecord.getClauseConditions()), ClauseRecord.FIELD_CLAUSE_CONDITIONS, oClauseVersionChangeRecord);
		sHtml += genTr(this.clauseRule, oClauseRecord.getClauseRule(), ClauseRecord.FIELD_CLAUSE_RULE, oClauseVersionChangeRecord);
		sHtml += genTr(this.additionalConditions, oClauseRecord.getAdditionalConditions(), ClauseRecord.FIELD_ADDITIONAL_CONDITIONS, oClauseVersionChangeRecord);

		sHtml += genTr(this.isOptional(), oClauseRecord.isOptional(), ClauseRecord.FIELD_IS_OPTIONAL, oClauseVersionChangeRecord);
		sHtml += genTr(this.isEditable(), oClauseRecord.isEditable(), ClauseRecord.FIELD_IS_EDITABLE, oClauseVersionChangeRecord);
		sHtml += genTr(this.getEditableRemarks(), oClauseRecord.getEditableRemarks(), ClauseRecord.FIELD_EDITABLE_REMARKS, oClauseVersionChangeRecord);
		sHtml += genTr(this.isBasicClause, oClauseRecord.isBasicClause(), ClauseRecord.FIELD_IS_BASIC_CLAUSE, oClauseVersionChangeRecord);
		sHtml += genTr(this.isDfarsActivity, oClauseRecord.getIsDfarsActivity(), ClauseRecord.FIELD_IS_DFARS_ACTIVITY, oClauseVersionChangeRecord);
		
		sHtml += genTr(this.clauseData, oClauseRecord.getClauseData(), ClauseRecord.FIELD_CLAUSE_DATA, oClauseVersionChangeRecord);

		if (!sHtml.isEmpty())
			sHtml = "\n<table><tr><th>Field</th><th>New</th><th>Previous</th></tr>" +
					sHtml +
					"\n</table>";
			
		sHtml += this.getPrescriptionDifferences(oClauseRecord, parsedPrescriptionSet);
		String sFillInChanges = this.getFillInDifferences(oClauseRecord);
		if (CommonUtils.isNotEmpty(sFillInChanges)) {
			sHtml += sFillInChanges;
			oClauseVersionChangeRecord.addChangeBrief("Fill-In");
		}

		if (!sHtml.isEmpty()) {
			oClauseVersionChangeRecord.setClause(this.clauseName);
			oClauseVersionChangeRecord.setChanged();
			return "\n<h2>" + this.clauseName + "</h2>" + sHtml;
		} else
			return "";
	}
	
	public String getUpdateSql(int iClauseVersionId, ParsedPrescriptionSet parsedPrescriptionSet, ClauseRecord oClauseRecord,
			ClauseVersionChangeRecord oClauseVersionChangeRecord) {
		String sSql = "";
		if (!this.clauseSectionId.equals(oClauseRecord.getClauseSectionId())) {
			sSql += (sSql.isEmpty() ? "\nSET " : "\n, ") + ClauseRecord.FIELD_CLAUSE_SECTION_ID + " = " + this.clauseSectionId
					+ " -- " + oClauseRecord.getClauseSectionId();
			oClauseVersionChangeRecord.addChangeBrief("Section");
		}
		if (!this.inclusion.equals(oClauseRecord.getInclusion())) {
			sSql = (sSql.isEmpty() ? "\nSET " : sSql + "\n, ") + ClauseRecord.FIELD_INCLUSION_CD + " = " + SqlCommons.quoteString(this.inclusion)
					+ " -- " + oClauseRecord.getInclusion(); 
			oClauseVersionChangeRecord.addChangeBrief("Inclusion");
		}
		if (!this.regulationId.equals(oClauseRecord.getRegulationId())) {
			sSql += (sSql.isEmpty() ? "\nSET " : "\n, ") + ClauseRecord.FIELD_REGULATION_ID + " = " + this.regulationId
					+ " -- " + oClauseRecord.getRegulationId();
			oClauseVersionChangeRecord.addChangeBrief("Regulation");
		}
		if (CommonUtils.isNotSame(this.clauseTitle, oClauseRecord.getClauseTitle())) {
			sSql = (sSql.isEmpty() ? "\nSET " : sSql + "\n, ") + ClauseRecord.FIELD_CLAUSE_TITLE + " = " + SqlCommons.quoteString(this.clauseTitle)
					+ " -- " + SqlCommons.quoteString(oClauseRecord.getClauseTitle());
			oClauseVersionChangeRecord.addChangeBrief("Title");
		}
		if (CommonUtils.isNotSame(this.url, oClauseRecord.getUrl())) {
			sSql = (sSql.isEmpty() ? "\nSET " : sSql + "\n, ") + ClauseRecord.FIELD_CLAUSE_URL + " = " + SqlCommons.quoteString(this.url)
					+ " -- " + SqlCommons.quoteString(oClauseRecord.getUrl());
			oClauseVersionChangeRecord.addChangeBrief("URL");
		}
		String sNewData = dateToString(this.effectiveDate);
		String sOldData = dateToString(oClauseRecord.getEffectiveDate());
		if (CommonUtils.isNotSame(sNewData, sOldData)) {
			sSql = (sSql.isEmpty() ? "\nSET " : sSql + "\n, ") + ClauseRecord.FIELD_EFFECTIVE_DATE + " = " + sNewData
					+ " -- " + sOldData;
			oClauseVersionChangeRecord.addChangeBrief("Effective Date");
		}

		if (CommonUtils.isNotSame(this.provisionOrClause, oClauseRecord.getProvisonOrClause())) {
			sSql = (sSql.isEmpty() ? "\nSET " : sSql + "\n, ") + ClauseRecord.FIELD_PROVISON_OR_CLAUSE + " = " + SqlCommons.quoteString(this.provisionOrClause)
					+ " -- " + SqlCommons.quoteString(oClauseRecord.getProvisonOrClause());
			oClauseVersionChangeRecord.addChangeBrief("Provision Or Clause");
		}

		if (CommonUtils.isNotSame(this.commercialStatus, oClauseRecord.getCommercialStatus())) {
			sSql = (sSql.isEmpty() ? "\nSET " : sSql + "\n, ") + ClauseRecord.FIELD_COMMERCIAL_STATUS + " = " + SqlCommons.quoteString(this.commercialStatus)
					+ " -- " + SqlCommons.quoteString(oClauseRecord.getCommercialStatus());
			oClauseVersionChangeRecord.addChangeBrief("Commercial Status");
		}

		if (CommonUtils.isNotSame(this.optionalStatus, oClauseRecord.getOptionalStatus())) {
			sSql = (sSql.isEmpty() ? "\nSET " : sSql + "\n, ") + ClauseRecord.FIELD_OPTIONAL_STATUS + " = " + SqlCommons.quoteString(this.optionalStatus)
					+ " -- " + SqlCommons.quoteString(oClauseRecord.getOptionalStatus());
			oClauseVersionChangeRecord.addChangeBrief("Optional Status");
		}
		
		if (CommonUtils.isNotSame(this.optionalConditions, oClauseRecord.getOptionalConditions())) {
			sSql = (sSql.isEmpty() ? "\nSET " : sSql + "\n, ") + ClauseRecord.FIELD_OPTIONAL_CONDITIONS + " = " + SqlCommons.quoteString(this.optionalConditions)
					+ " -- " + SqlCommons.quoteString(oClauseRecord.getOptionalConditions());
			oClauseVersionChangeRecord.addChangeBrief("Optional Conditions");
		}
		
		sNewData = SqlCommons.quoteString(this.clauseConditions);
		sOldData = SqlCommons.quoteString(oClauseRecord.getClauseConditions());
		if (CommonUtils.isNotSame(sNewData, sOldData)) {
			sSql = (sSql.isEmpty() ? "\nSET " : sSql + "\n, ") + ClauseRecord.FIELD_CLAUSE_CONDITIONS + " = " + sNewData
					+ " -- " + sOldData; 
			oClauseVersionChangeRecord.addChangeBrief("Condition");
		}
		if (CommonUtils.isNotSame(this.clauseRule, oClauseRecord.getClauseRule())) {
			sSql = (sSql.isEmpty() ? "\nSET " : sSql + "\n, ") + ClauseRecord.FIELD_CLAUSE_RULE + " = " + SqlCommons.quoteString(this.clauseRule)
					+ " -- " + SqlCommons.quoteString(oClauseRecord.getClauseRule());
			oClauseVersionChangeRecord.addChangeBrief("Rule");
		}
		if (CommonUtils.isNotSame(this.additionalConditions, oClauseRecord.getAdditionalConditions())) {
			sSql = (sSql.isEmpty() ? "\nSET " : sSql + "\n, ") + ClauseRecord.FIELD_ADDITIONAL_CONDITIONS + " = " + SqlCommons.quoteString(this.additionalConditions)
					+ " -- " + SqlCommons.quoteString(oClauseRecord.getAdditionalConditions());
			oClauseVersionChangeRecord.addChangeBrief("Additional Instruction");
		}

		if (CommonUtils.isNotSame(this.isOptional(), oClauseRecord.isOptional())) {
			sSql = (sSql.isEmpty() ? "\nSET " : sSql + "\n, ") + ClauseRecord.FIELD_IS_OPTIONAL + " = " + SqlCommons.quoteString(this.isOptional());
			oClauseVersionChangeRecord.addChangeBrief("Is Optional");
		}
		
		if (CommonUtils.isNotSame(this.isEditable(), oClauseRecord.isEditable())) {
			sSql = (sSql.isEmpty() ? "\nSET " : sSql + "\n, ") + ClauseRecord.FIELD_IS_EDITABLE + " = " + SqlCommons.quoteString(this.isEditable());
			oClauseVersionChangeRecord.addChangeBrief("Is Editable");
		}
		
		if (CommonUtils.isNotSame(this.getEditableRemarks(), oClauseRecord.getEditableRemarks())) {
			sSql = (sSql.isEmpty() ? "\nSET " : sSql + "\n, ") + ClauseRecord.FIELD_EDITABLE_REMARKS + " = " + SqlCommons.quoteString(this.getEditableRemarks())
					+ " -- " + SqlCommons.quoteString(oClauseRecord.getEditableRemarks());
			oClauseVersionChangeRecord.addChangeBrief("Editable Remarks");
		}
		
		if (CommonUtils.isNotSame(this.isBasicClause, oClauseRecord.isBasicClause())) {
			sSql = (sSql.isEmpty() ? "\nSET " : sSql + "\n, ") + ClauseRecord.FIELD_IS_BASIC_CLAUSE + " = " + SqlCommons.quoteString(this.isBasicClause);
			oClauseVersionChangeRecord.addChangeBrief("Is Basic");
		}

		if (CommonUtils.isNotSame(this.isDfarsActivity, oClauseRecord.getIsDfarsActivity())) {
			sSql = (sSql.isEmpty() ? "\nSET " : sSql + "\n, ") + ClauseRecord.FIELD_IS_DFARS_ACTIVITY + " = " + SqlCommons.quoteString(this.isDfarsActivity);
			//oClauseVersionChangeRecord.addChangeBrief("Is Basic");
		}

		sNewData = SqlCommons.quoteString(this.clauseData);
		sOldData = SqlCommons.quoteString(oClauseRecord.getClauseData());
		
		if (CommonUtils.isNotSame(sNewData, sOldData)) {
			sSql = (sSql.isEmpty() ? "\nSET " : sSql + "\n, ") + ClauseRecord.FIELD_CLAUSE_DATA + " = " + sNewData
					+ "\n--               " + sOldData;
			oClauseVersionChangeRecord.addChangeBrief("Context");
		}

		if (!sSql.isEmpty()) {
			sSql = "UPDATE " + ClauseRecord.TABLE_CLAUSES + 
					sSql +
					"\nWHERE " + ClauseRecord.FIELD_CLAUSE_VERSION_ID + " = " + iClauseVersionId + //oClauseRecord.getClauseVersionId() +
					" AND " + ClauseRecord.FIELD_CLAUSE_NAME + " = " + SqlCommons.quoteString(this.clauseName) + ";";
					// "\nWHERE " + ClauseRecord.FIELD_CLAUSE_ID + " = " + oClauseRecord.getId() + "; -- " + this.clauseName;
		}

		String sPrescriptions = this.genPrescriptionSql(oClauseRecord, parsedPrescriptionSet);
		if (CommonUtils.isNotEmpty(sPrescriptions)) {
			sSql += sPrescriptions;
			oClauseVersionChangeRecord.addChangeBrief("Prescription");
		}
		String sFillIns = this.genFillInSql(oClauseRecord);
		if (CommonUtils.isNotEmpty(sFillIns)) {
			sSql += "\n" + sFillIns;
			oClauseVersionChangeRecord.addChangeBrief("Fill-in");
		}
		
		if (!sSql.isEmpty()) {
			oClauseVersionChangeRecord.setClause(this.clauseName);
			oClauseVersionChangeRecord.setChanged();
		}

		return sSql;
	}

	public String getUpdateSql_wFilter(int iClauseVersionId, ParsedPrescriptionSet parsedPrescriptionSet, ClauseRecord oClauseRecord,
			ClauseVersionChangeRecord oClauseVersionChangeRecord) {
		String sSql = "";
		if (!this.clauseSectionId.equals(oClauseRecord.getClauseSectionId())) {
			sSql += (sSql.isEmpty() ? "\nSET " : "\n, ") + ClauseRecord.FIELD_CLAUSE_SECTION_ID + " = " + this.clauseSectionId
					+ " -- " + oClauseRecord.getClauseSectionId();
			oClauseVersionChangeRecord.addChangeBrief("Section");
		}
		if (!this.inclusion.equals(oClauseRecord.getInclusion())) {
			sSql = (sSql.isEmpty() ? "\nSET " : sSql + "\n, ") + ClauseRecord.FIELD_INCLUSION_CD + " = " + SqlCommons.quoteString(this.inclusion)
					+ " -- " + oClauseRecord.getInclusion(); 
			oClauseVersionChangeRecord.addChangeBrief("Inclusion");
		}
		if (!this.regulationId.equals(oClauseRecord.getRegulationId())) {
			sSql += (sSql.isEmpty() ? "\nSET " : "\n, ") + ClauseRecord.FIELD_REGULATION_ID + " = " + this.regulationId
					+ " -- " + oClauseRecord.getRegulationId();
			oClauseVersionChangeRecord.addChangeBrief("Regulation");
		}
		if (CommonUtils.isNotSame(this.clauseTitle, oClauseRecord.getClauseTitle())) {
			sSql = (sSql.isEmpty() ? "\nSET " : sSql + "\n, ") + ClauseRecord.FIELD_CLAUSE_TITLE + " = " + SqlCommons.quoteString(this.clauseTitle)
					+ " -- " + SqlCommons.quoteString(oClauseRecord.getClauseTitle());
			oClauseVersionChangeRecord.addChangeBrief("Title");
		}
		if (CommonUtils.isNotSame(this.url, oClauseRecord.getUrl())) {
			sSql = (sSql.isEmpty() ? "\nSET " : sSql + "\n, ") + ClauseRecord.FIELD_CLAUSE_URL + " = " + SqlCommons.quoteString(this.url)
					+ " -- " + SqlCommons.quoteString(oClauseRecord.getUrl());
			oClauseVersionChangeRecord.addChangeBrief("URL");
		}
		String sNewData = dateToString(this.effectiveDate);
		String sOldData = dateToString(oClauseRecord.getEffectiveDate());
		if (CommonUtils.isNotSame(sNewData, sOldData)) {
			sSql = (sSql.isEmpty() ? "\nSET " : sSql + "\n, ") + ClauseRecord.FIELD_EFFECTIVE_DATE + " = " + sNewData
					+ " -- " + sOldData;
			oClauseVersionChangeRecord.addChangeBrief("Effective Date");
		}

		if (CommonUtils.isNotSame(this.provisionOrClause, oClauseRecord.getProvisonOrClause())) {
			sSql = (sSql.isEmpty() ? "\nSET " : sSql + "\n, ") + ClauseRecord.FIELD_PROVISON_OR_CLAUSE + " = " + SqlCommons.quoteString(this.provisionOrClause)
					+ " -- " + SqlCommons.quoteString(oClauseRecord.getProvisonOrClause());
			oClauseVersionChangeRecord.addChangeBrief("Provision Or Clause");
		}

		if (CommonUtils.isNotSame(this.commercialStatus, oClauseRecord.getCommercialStatus())) {
			sSql = (sSql.isEmpty() ? "\nSET " : sSql + "\n, ") + ClauseRecord.FIELD_COMMERCIAL_STATUS + " = " + SqlCommons.quoteString(this.commercialStatus)
					+ " -- " + SqlCommons.quoteString(oClauseRecord.getCommercialStatus());
			oClauseVersionChangeRecord.addChangeBrief("Commercial Status");
		}
		
		if (CommonUtils.isNotSame(this.optionalStatus, oClauseRecord.getOptionalStatus())) {
			sSql = (sSql.isEmpty() ? "\nSET " : sSql + "\n, ") + ClauseRecord.FIELD_OPTIONAL_STATUS + " = " + SqlCommons.quoteString(this.optionalStatus)
					+ " -- " + SqlCommons.quoteString(oClauseRecord.getOptionalStatus());
			oClauseVersionChangeRecord.addChangeBrief("Optional Status");
		}
		
		if (CommonUtils.isNotSame(this.optionalConditions, oClauseRecord.getOptionalConditions())) {
			sSql = (sSql.isEmpty() ? "\nSET " : sSql + "\n, ") + ClauseRecord.FIELD_OPTIONAL_CONDITIONS + " = " + SqlCommons.quoteString(this.optionalConditions)
					+ " -- " + SqlCommons.quoteString(oClauseRecord.getOptionalConditions());
			oClauseVersionChangeRecord.addChangeBrief("Optional Conditions");
		}
		
		sNewData = SqlCommons.quoteString(this.clauseConditions);
		sOldData = SqlCommons.quoteString(oClauseRecord.getClauseConditions());
		if (CommonUtils.isNotSame(sNewData, sOldData)) {
			sSql = (sSql.isEmpty() ? "\nSET " : sSql + "\n, ") + ClauseRecord.FIELD_CLAUSE_CONDITIONS + " = " + sNewData
					+ " -- " + sOldData; 
			oClauseVersionChangeRecord.addChangeBrief("Condition");
		}
		if (CommonUtils.isNotSame(this.clauseRule, oClauseRecord.getClauseRule())) {
			sSql = (sSql.isEmpty() ? "\nSET " : sSql + "\n, ") + ClauseRecord.FIELD_CLAUSE_RULE + " = " + SqlCommons.quoteString(this.clauseRule)
					+ " -- " + SqlCommons.quoteString(oClauseRecord.getClauseRule());
			oClauseVersionChangeRecord.addChangeBrief("Rule");
		}
		if (CommonUtils.isNotSame(this.additionalConditions, oClauseRecord.getAdditionalConditions())) {
			sSql = (sSql.isEmpty() ? "\nSET " : sSql + "\n, ") + ClauseRecord.FIELD_ADDITIONAL_CONDITIONS + " = " + SqlCommons.quoteString(this.additionalConditions)
					+ " -- " + SqlCommons.quoteString(oClauseRecord.getAdditionalConditions());
			oClauseVersionChangeRecord.addChangeBrief("Additional Instruction");
		}

		if (CommonUtils.isNotSame(this.isOptional(), oClauseRecord.isOptional())) {
			sSql = (sSql.isEmpty() ? "\nSET " : sSql + "\n, ") + ClauseRecord.FIELD_IS_OPTIONAL + " = " + SqlCommons.quoteString(this.isOptional());
			oClauseVersionChangeRecord.addChangeBrief("Is Optional");
		}
		
		if (CommonUtils.isNotSame(this.isEditable(), oClauseRecord.isEditable())) {
			sSql = (sSql.isEmpty() ? "\nSET " : sSql + "\n, ") + ClauseRecord.FIELD_IS_EDITABLE + " = " + SqlCommons.quoteString(this.isEditable());
			oClauseVersionChangeRecord.addChangeBrief("Is Editable");
		}
		
		if (CommonUtils.isNotSame(this.getEditableRemarks(), oClauseRecord.getEditableRemarks())) {
			sSql = (sSql.isEmpty() ? "\nSET " : sSql + "\n, ") + ClauseRecord.FIELD_EDITABLE_REMARKS + " = " + SqlCommons.quoteString(this.getEditableRemarks())
					+ " -- " + SqlCommons.quoteString(oClauseRecord.getEditableRemarks());
			oClauseVersionChangeRecord.addChangeBrief("Editable Remarks");
		}
		
		if (CommonUtils.isNotSame(this.isBasicClause, oClauseRecord.isBasicClause())) {
			sSql = (sSql.isEmpty() ? "\nSET " : sSql + "\n, ") + ClauseRecord.FIELD_IS_BASIC_CLAUSE + " = " + SqlCommons.quoteString(this.isBasicClause);
			oClauseVersionChangeRecord.addChangeBrief("Is Basic");
		}
		
		if (CommonUtils.isNotSame(this.isDfarsActivity, oClauseRecord.getIsDfarsActivity())) {
			sSql = (sSql.isEmpty() ? "\nSET " : sSql + "\n, ") + ClauseRecord.FIELD_IS_DFARS_ACTIVITY + " = " + SqlCommons.quoteString(this.isDfarsActivity);
			//oClauseVersionChangeRecord.addChangeBrief("Is Basic");
		}

		sNewData = SqlCommons.quoteString(this.clauseData);
		sOldData = SqlCommons.quoteString(oClauseRecord.getClauseData());
		if (CommonUtils.isNotSame(sNewData, sOldData)) {
			sSql = (sSql.isEmpty() ? "\nSET " : sSql + "\n, ") + ClauseRecord.FIELD_CLAUSE_DATA + " = " + sNewData
					+ "\n--               " + sOldData;
			oClauseVersionChangeRecord.addChangeBrief("Context");
		}

		if (!sSql.isEmpty()) {
			sSql = "UPDATE " + ClauseRecord.TABLE_CLAUSES + 
					sSql +
					"\nWHERE " + ClauseRecord.FIELD_CLAUSE_VERSION_ID + " = " + iClauseVersionId + //oClauseRecord.getClauseVersionId() +
					" AND " + ClauseRecord.FIELD_CLAUSE_NAME + " = " + SqlCommons.quoteString(this.clauseName) + ";";
					// "\nWHERE " + ClauseRecord.FIELD_CLAUSE_ID + " = " + oClauseRecord.getId() + "; -- " + this.clauseName;
		}

		String sPrescriptions = this.genPrescriptionSql_wFilter(oClauseRecord, parsedPrescriptionSet, iClauseVersionId);
		if (CommonUtils.isNotEmpty(sPrescriptions)) {
			sSql += sPrescriptions;
			oClauseVersionChangeRecord.addChangeBrief("Prescription");
		}
		String sFillIns = this.genFillInSql_wFilter(oClauseRecord, iClauseVersionId);
		if (CommonUtils.isNotEmpty(sFillIns)) {
			sSql += "\n" + sFillIns;
			oClauseVersionChangeRecord.addChangeBrief("Fill-in");
		}
		
		if (!sSql.isEmpty()) {
			oClauseVersionChangeRecord.setClause(this.clauseName);
			oClauseVersionChangeRecord.setChanged();
		}

		return sSql;
	}
}
