package hgs.cpm.db;

import gov.dod.cls.utils.CommonUtils;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

public class Stg3ClauseFilInCorrectionRecord {
	
	public static final String TABLE_STG3_CLAUSE_FILL_IN_CORRECTIONS = "Stg3_Clause_Fill_In_Corrections";

	public static final String FIELD_STG3_CLAUSE_FILL_IN_ID = "Stg3_Clause_Fill_In_Id";
	public static final String FIELD_STG3_CLAUSE_ID       = "Stg3_Clause_Id";
	public static final String FIELD_PREVIOUS_FILL_IN_CODE = "Previous_Fill_In_Code";
	public static final String FIELD_FILL_IN_CODE         = "Fill_In_Code";
	public static final String FIELD_FILL_IN_TYPE         = "Fill_In_Type";
	public static final String FIELD_FILL_IN_MAX_SIZE     = "Fill_In_Max_Size";
	public static final String FIELD_FILL_IN_GROUP_NUMBER = "Fill_In_Group_Number";
	public static final String FIELD_FILL_IN_PLACEHOLDER  = "Fill_In_Placeholder";
	public static final String FIELD_FILL_IN_DEFAULT_DATA = "Fill_In_Default_Data";
	public static final String FIELD_FILL_IN_DISPLAY_ROWS = "Fill_In_Display_Rows";
	public static final String FIELD_FILL_IN_FOR_TABLE    = "Fill_In_For_Table";
	public static final String FIELD_FILL_IN_HEADING      = "Fill_In_Heading";
	public static final String FIELD_CREATED_AT           = "Created_At";
	public static final String FIELD_UPDATED_AT			= "Updated_At";
	
	public static final String SQL_SELECT
		= "SELECT " + FIELD_STG3_CLAUSE_FILL_IN_ID
		+ ", " + FIELD_STG3_CLAUSE_ID
		+ ", " + FIELD_PREVIOUS_FILL_IN_CODE
		+ ", " + FIELD_FILL_IN_CODE
		+ ", " + FIELD_FILL_IN_TYPE
		+ ", " + FIELD_FILL_IN_MAX_SIZE
		+ ", " + FIELD_FILL_IN_GROUP_NUMBER
		+ ", " + FIELD_FILL_IN_PLACEHOLDER
		+ ", " + FIELD_FILL_IN_DEFAULT_DATA
		+ ", " + FIELD_FILL_IN_DISPLAY_ROWS
		+ ", " + FIELD_FILL_IN_FOR_TABLE
		+ ", " + FIELD_FILL_IN_HEADING
		+ ", " + FIELD_CREATED_AT
		+ ", " + FIELD_UPDATED_AT
		+ " FROM " + TABLE_STG3_CLAUSE_FILL_IN_CORRECTIONS
		+ " WHERE " + FIELD_STG3_CLAUSE_ID + " = ?"
		+ " ORDER BY " + FIELD_STG3_CLAUSE_FILL_IN_ID;

	public Integer stg3ClauseFillInId;
	public Integer stg3ClauseId;
	public String previousFillInCode;
	public String fillInCode;
	public String fillInType;
	public Integer fillInMaxSize;
	public Integer fillInGroupNumber;
	public String fillInPlaceholder;
	public String fillInDefaultData;
	public Integer fillInDisplayRows;
	public boolean fillInForTable; //    boolean NOT NULL DEFAULT 0,
	public String fillInHeading;
	public Date createdAt;
	public Date cpdatedAt;

	public void read(ResultSet rs1) throws SQLException {
		this.stg3ClauseFillInId = rs1.getInt(FIELD_STG3_CLAUSE_FILL_IN_ID);
		this.stg3ClauseId = rs1.getInt(FIELD_STG3_CLAUSE_ID);
		this.previousFillInCode = rs1.getString(FIELD_PREVIOUS_FILL_IN_CODE);
		this.fillInCode = rs1.getString(FIELD_FILL_IN_CODE);
		this.fillInType = rs1.getString(FIELD_FILL_IN_TYPE);
		this.fillInMaxSize = CommonUtils.toInteger(rs1.getObject(FIELD_FILL_IN_MAX_SIZE));
		this.fillInGroupNumber = CommonUtils.toInteger(rs1.getObject(FIELD_FILL_IN_GROUP_NUMBER));
		this.fillInPlaceholder = rs1.getString(FIELD_FILL_IN_PLACEHOLDER);
		this.fillInDefaultData = rs1.getString(FIELD_FILL_IN_DEFAULT_DATA);
		this.fillInDisplayRows = CommonUtils.toInteger(rs1.getObject(FIELD_FILL_IN_DISPLAY_ROWS));
		this.fillInForTable = rs1.getBoolean(FIELD_FILL_IN_FOR_TABLE); //    boolean NOT NULL DEFAULT 0,
		this.fillInHeading = rs1.getString(FIELD_FILL_IN_HEADING);
		this.createdAt = CommonUtils.toDate(rs1.getObject(FIELD_CREATED_AT));
		this.cpdatedAt = CommonUtils.toDate(rs1.getObject(FIELD_UPDATED_AT));
	}

	public ParsedClauseFillInRecord copy() {
		ParsedClauseFillInRecord oRecord = new ParsedClauseFillInRecord();
		
		oRecord.code = this.fillInCode;
		oRecord.type = this.fillInType;
		oRecord.maxSize = this.fillInMaxSize;
		oRecord.id = null;

		oRecord.fillInGroupNumber = this.fillInGroupNumber;
		oRecord.fillInPlaceholder = this.fillInPlaceholder;
		oRecord.fillInDefaultData = this.fillInDefaultData;
		oRecord.fillInDisplayRows = this.fillInDisplayRows;
		oRecord.fillInForTable = this.fillInForTable; //    boolean NOT NULL DEFAULT 0,
		oRecord.fillInHeading = this.fillInHeading;
		
		return oRecord;
	}
	
}
