package hgs.cpm.db;

import hgs.cpm.db.template.TemplateStg2ClauseFillInRecord;

public class Stg2ClauseFillInsAltRecord extends TemplateStg2ClauseFillInRecord {

	public static final String TABLE_STG2_CLAUSE_FILL_INS_ALT = "Stg2_Clause_Fill_Ins_Alt";

	public static final String FIELD_STG2_CLAUSE_FILLINS_ALT_ID = "Stg2_Clause_Fillins_Alt_Id";
	public static final String FIELD_STG1_OFFICIAL_CLAUSE_ALT_ID = "Stg1_Official_Clause_Alt_Id";

	public String getTableName() { return TABLE_STG2_CLAUSE_FILL_INS_ALT; }
	public String getIdFieldName() { return FIELD_STG2_CLAUSE_FILLINS_ALT_ID; }
	public String getClauseIdFieldName() { return FIELD_STG1_OFFICIAL_CLAUSE_ALT_ID; }
	
	/*
	public static final String FIELD_FILL_IN_CODE         = "Fill_In_Code";
	public static final String FIELD_FILL_IN_TYPE         = "Fill_In_Type";
	public static final String FIELD_FILL_IN_MAX_SIZE     = "Fill_In_Max_Size";
	public static final String FIELD_FILL_IN_GROUP_NUMBER = "Fill_In_Group_Number";
	public static final String FIELD_FILL_IN_PLACEHOLDER  = "Fill_In_Placeholder";
	public static final String FIELD_FILL_IN_DEFAULT_DATA = "Fill_In_Default_Data";
	public static final String FIELD_FILL_IN_DISPLAY_ROWS = "Fill_In_Display_Rows";
	public static final String FIELD_FILL_IN_FOR_TABLE    = "Fill_In_For_Table";
	public static final String FIELD_FILL_IN_HEADING      = "Fill_In_Heading";
	
	public static final String SQL_SELECT
		= "SELECT " + FIELD_STG2_CLAUSE_FILLINS_ALT_ID
		+ ", " + FIELD_STG1_OFFICIAL_CLAUSE_ID
		+ ", " + FIELD_FILL_IN_CODE
		+ ", " + FIELD_FILL_IN_TYPE
		+ ", " + FIELD_FILL_IN_MAX_SIZE
		+ ", " + FIELD_FILL_IN_GROUP_NUMBER
		+ ", " + FIELD_FILL_IN_PLACEHOLDER
		+ ", " + FIELD_FILL_IN_DEFAULT_DATA
		+ ", " + FIELD_FILL_IN_DISPLAY_ROWS
		+ ", " + FIELD_FILL_IN_FOR_TABLE
		+ ", " + FIELD_FILL_IN_HEADING
		+ " FROM " + TABLE_STG2_CLAUSE_FILL_INS_ALT;
	
	public static PreparedStatement prepareInsert(Connection conn) throws SQLException {
		String sSql = "INSERT INTO " + TABLE_STG2_CLAUSE_FILL_INS_ALT
				+ " (" + FIELD_STG1_OFFICIAL_CLAUSE_ID
				+ ", " + FIELD_FILL_IN_CODE
				+ ", " + FIELD_FILL_IN_TYPE
				+ ", " + FIELD_FILL_IN_MAX_SIZE
				+ ", " + FIELD_FILL_IN_GROUP_NUMBER
				+ ", " + FIELD_FILL_IN_PLACEHOLDER
				+ ", " + FIELD_FILL_IN_DEFAULT_DATA
				+ ", " + FIELD_FILL_IN_DISPLAY_ROWS
				+ ", " + FIELD_FILL_IN_FOR_TABLE
				+ ", " + FIELD_FILL_IN_HEADING + ")"
				+ " VALUES (?" // FIELD_STG1_OFFICIAL_CLAUSE_ID
				+ ",?" // FIELD_FILL_IN_CODE
				+ ",?" // FIELD_FILL_IN_TYPE
				+ ",?" // FIELD_FILL_IN_MAX_SIZE
				+ ",?" // FIELD_FILL_IN_GROUP_NUMBER
				+ ",?" // FIELD_FILL_IN_PLACEHOLDER
				+ ",?" // FIELD_FILL_IN_DEFAULT_DATA
				+ ",?" // FIELD_FILL_IN_DISPLAY_ROWS
				+ ",?" // FIELD_FILL_IN_FOR_TABLE
				+ ",?" // FIELD_FILL_IN_HEADING
				+ ")";
		return conn.prepareStatement(sSql, Statement.RETURN_GENERATED_KEYS);
	}

	// =================================================================
	private Integer stg2ClauseFillinsAltId;
	private Integer stg1OfficialClauseId;
	private String fillInCode;
	private String fillInType;
	private Integer fillInMaxSize;
	private Integer fillInGroupNumber;
	private String fillInPlaceholder;
	private String fillInDefaultData;
	private Integer fillInDisplayRows;
	private boolean fillInForTable;
	private String fillInHeading;
	
	public void clear() {
		this.stg2ClauseFillinsAltId = null;
		this.stg1OfficialClauseId = null;
		this.fillInCode = null;
		this.fillInType = null;
		this.fillInMaxSize = null;
		this.fillInGroupNumber = null;
		this.fillInPlaceholder = null;
		this.fillInDefaultData = null;
		this.fillInDisplayRows = null;
		this.fillInForTable = false;
		this.fillInHeading = null;
	}
	
	public Integer insert(PreparedStatement ps1) throws SQLException {
		ResultSet res = null;
		try {
			SqlUtil.setParam(ps1, 1, this.stg1OfficialClauseId, java.sql.Types.INTEGER);
			SqlUtil.setParam(ps1, 2, this.fillInCode, java.sql.Types.VARCHAR);
			SqlUtil.setParam(ps1, 3, this.fillInType, java.sql.Types.VARCHAR);
			SqlUtil.setParam(ps1, 4, this.fillInMaxSize, java.sql.Types.INTEGER);
			SqlUtil.setParam(ps1, 5, this.fillInGroupNumber, java.sql.Types.INTEGER);
			SqlUtil.setParam(ps1, 6, this.fillInPlaceholder, java.sql.Types.VARCHAR);
			SqlUtil.setParam(ps1, 7, this.fillInDefaultData, java.sql.Types.VARCHAR);
			SqlUtil.setParam(ps1, 8, this.fillInDisplayRows, java.sql.Types.INTEGER);
			SqlUtil.setParam(ps1, 9, this.fillInForTable, java.sql.Types.BOOLEAN);
			SqlUtil.setParam(ps1, 10, this.fillInHeading, java.sql.Types.VARCHAR);
			res = ps1.getGeneratedKeys();
			if (res.next()) {
				this.stg2ClauseFillinsAltId = res.getInt(1);
			}
		} finally {
			SqlUtil.closeInstance(res);
		}
		return this.stg2ClauseFillinsAltId;
	}
	
	public void read(ResultSet res) throws SQLException {
		this.stg2ClauseFillinsAltId = res.getInt(FIELD_STG2_CLAUSE_FILLINS_ALT_ID);
		this.stg1OfficialClauseId = res.getInt(FIELD_STG1_OFFICIAL_CLAUSE_ID);
		this.fillInCode = res.getString(FIELD_FILL_IN_CODE);
		this.fillInType = res.getString(FIELD_FILL_IN_TYPE);
		this.fillInMaxSize = CommonUtils.toInteger(res.getObject(FIELD_FILL_IN_MAX_SIZE));
		this.fillInGroupNumber = CommonUtils.toInteger(res.getObject(FIELD_FILL_IN_GROUP_NUMBER));
		this.fillInPlaceholder = res.getString(FIELD_FILL_IN_PLACEHOLDER);
		this.fillInDefaultData = res.getString(FIELD_FILL_IN_DEFAULT_DATA);
		this.fillInDisplayRows = CommonUtils.toInteger(res.getObject(FIELD_FILL_IN_DISPLAY_ROWS));
		this.fillInForTable = res.getBoolean(FIELD_FILL_IN_FOR_TABLE);
		this.fillInHeading = res.getString(FIELD_FILL_IN_HEADING);
	}
	
	// =================================================================
	public Integer getStg2ClauseFillinsAltId() {
		return stg2ClauseFillinsAltId;
	}
	public void setStg2ClauseFillinsAltId(Integer stg2ClauseFillinsAltId) {
		this.stg2ClauseFillinsAltId = stg2ClauseFillinsAltId;
	}
	public Integer getStg1OfficialClauseId() {
		return stg1OfficialClauseId;
	}
	public void setStg1OfficialClauseId(Integer stg1OfficialClauseId) {
		this.stg1OfficialClauseId = stg1OfficialClauseId;
	}
	public String getFillInCode() {
		return fillInCode;
	}
	public void setFillInCode(String fillInCode) {
		this.fillInCode = fillInCode;
	}
	public String getFillInType() {
		return fillInType;
	}
	public void setFillInType(String fillInType) {
		this.fillInType = fillInType;
	}
	public Integer getFillInMaxSize() {
		return fillInMaxSize;
	}
	public void setFillInMaxSize(Integer fillInMaxSize) {
		this.fillInMaxSize = fillInMaxSize;
	}
	public Integer getFillInGroupNumber() {
		return fillInGroupNumber;
	}
	public void setFillInGroupNumber(Integer fillInGroupNumber) {
		this.fillInGroupNumber = fillInGroupNumber;
	}
	public String getFillInPlaceholder() {
		return fillInPlaceholder;
	}
	public void setFillInPlaceholder(String fillInPlaceholder) {
		this.fillInPlaceholder = fillInPlaceholder;
	}
	public String getFillInDefaultData() {
		return fillInDefaultData;
	}
	public void setFillInDefaultData(String fillInDefaultData) {
		this.fillInDefaultData = fillInDefaultData;
	}
	public Integer getFillInDisplayRows() {
		return fillInDisplayRows;
	}
	public void setFillInDisplayRows(Integer fillInDisplayRows) {
		this.fillInDisplayRows = fillInDisplayRows;
	}
	public boolean isFillInForTable() {
		return fillInForTable;
	}
	public void setFillInForTable(boolean fillInForTable) {
		this.fillInForTable = fillInForTable;
	}
	public String getFillInHeading() {
		return fillInHeading;
	}
	public void setFillInHeading(String fillInHeading) {
		this.fillInHeading = fillInHeading;
	}
	*/

}
