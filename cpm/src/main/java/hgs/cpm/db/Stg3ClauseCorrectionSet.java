package hgs.cpm.db;

import gov.dod.cls.utils.sql.SqlUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class Stg3ClauseCorrectionSet extends ArrayList<Stg3ClauseCorrectionRecord> {

	private static final long serialVersionUID = -253589725617087496L;
	
	public void load(Connection conn, int piClauseVersionId) throws SQLException {
		String sSql;
		PreparedStatement psClause = null;
		PreparedStatement psFillIn = null;
		ResultSet rsClause = null;
		try {
			sSql = Stg3ClauseFilInCorrectionRecord.SQL_SELECT; 
			psFillIn = conn.prepareStatement(sSql);
			
			sSql = Stg3ClauseCorrectionRecord.SQL_SELECT;
			psClause = conn.prepareStatement(sSql);
			psClause.setInt(1, piClauseVersionId);
			rsClause = psClause.executeQuery();
			this.clear();
			while (rsClause.next()) {
				Stg3ClauseCorrectionRecord oRecord = new Stg3ClauseCorrectionRecord();
				oRecord.read(rsClause, psFillIn);
				this.add(oRecord);
			}
		}
		finally {
			SqlUtil.closeInstance(rsClause, psClause);
			SqlUtil.closeInstance(psFillIn);
		}
	}
	
	public Stg3ClauseCorrectionRecord findByName(String psClauseName) {
		for (Stg3ClauseCorrectionRecord oRecord : this) {
			if (oRecord.clauseName.equals(psClauseName))
				return oRecord;
		}
		return null;
	}

}
