package hgs.cpm.db;

public class Stg2ClauseQuestionConditionRecord {

	public static final String TABLE_STG1_CLAUSE_QUESTION_CONDITIONS = "Stg2_Clause_Question_Conditions";
	
	public static final String FIELD_STG2_CLAUSE_QUESTION_CONDITION_ID 	= "Stg2_Clause_Question_Condition_Id";
	public static final String FIELD_STG1_CLAUSE_ID 					= "Stg1_Clause_Id";
	public static final String FIELD_STG2_QUESTION_ID 					= "Stg2_Question_Id";
	public static final String FIELD_STG2_POSSIBLE_ANSWER_ID 			= "Stg2_Possible_Answer_Id";
	public static final String FIELD_CONDITION_TEXT 					= "Condition_Text";
	public static final String FIELD_QUESTION_NAME 						= "Question_Name";
	public static final String FIELD_QUESTION_ANSWER 					= "Question_Answer";
	
	
	// =======================================================
	private Integer stg1ClauseId = 0;
	private Integer stg2QuestionId = 0;
	private Integer stg2AnswerId = 0;
	private Integer stg2ClauseQuestionConditionId = 0;
	private Integer cntChoicePrescription = 0;	// Count of Prescription assigned using the QuestionsMatrix.
	
	private String condition = null;
	private String questionName = null;
	private String questionAnswer = null;
	
	// =======================================================
	public Integer getStg2ClauseQuestionConditionId() {
		return stg2ClauseQuestionConditionId;
	}
	public void setStg2ClauseQuestionConditionId(Integer stg2ClauseQuestionConditionId) {
		this.stg2ClauseQuestionConditionId = stg2ClauseQuestionConditionId;
	}
	public Integer getStg1ClauseId() {
		return stg1ClauseId;
	}
	public void setStg1ClauseId(Integer stg1ClauseId) {
		this.stg1ClauseId = stg1ClauseId;
	}
	public Integer getStg2QuestionId() {
		return stg2QuestionId;
	}
	public void setStg2QuestionId(Integer stg2QuestionId) {
		this.stg2QuestionId = stg2QuestionId;
	}
	public Integer getStg2AnswerId() {
		return stg2AnswerId;
	}
	public void setStg2AnswerId(Integer stg2AnswerId) {
		this.stg2AnswerId = stg2AnswerId;
	}

	public Integer getCntChoicePrescription() {
		return cntChoicePrescription;
	}
	public void setCntChoicePrescription(Integer cntChoicePrescription) {
		this.cntChoicePrescription = cntChoicePrescription;
	}

	
	public String getCondition() {
		return condition;
	}
	public void setCondition(String condition) {
		this.condition = condition;
	}
	public String getQuestionName() {
		return questionName;
	}
	public void setQuestionName(String questionName) {
		this.questionName = questionName;
	}
	public String getQuestionAnswer() {
		return questionAnswer;
	}
	public void setQuestionAnswer(String questionAnswer) {
		this.questionAnswer = questionAnswer;
	}

}
