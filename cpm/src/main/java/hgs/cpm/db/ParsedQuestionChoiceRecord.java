package hgs.cpm.db;

import gov.dod.cls.db.PrescriptionRecord;
import gov.dod.cls.db.QuestionChoicePrescriptionRecord;
import gov.dod.cls.db.QuestionChoiceRecord;
import gov.dod.cls.db.QuestionRecord;
import gov.dod.cls.utils.CommonUtils;
import hgs.cpm.utils.SqlCommons;

import java.util.ArrayList;

public class ParsedQuestionChoiceRecord {

	public Integer Stg2PossibleAnswerId;
	public Integer stg2QuestionId; // questionId;
	public String choiceText; // choiceText;
	public String promptAfterSelection = null;
	
	public ArrayList<String> prescriptions = new ArrayList<String>();
	
	public String getPrescriptionIds(ParsedPrescriptionSet parsedPrescriptionSet) {
		String result = "";
		for (String prescription : this.prescriptions) {
			Integer iId = parsedPrescriptionSet.getId(prescription);
			if (iId == null) {
				System.out.println("Unable to locate prescription: '" + prescription + "'");
			} else {
				result += '\t' + iId;
			}
		}
		return result;
	}
	
	public int prescriptionIndexOf(String psPrescription) {
		if (CommonUtils.isNotEmpty(psPrescription)) {
			for (int iPre = 0; iPre < this.prescriptions.size(); iPre++) {
				if (psPrescription.equalsIgnoreCase(this.prescriptions.get(iPre)))
					return iPre;
			}
		}
		return -1;
	}
	
	public boolean addPrescription(String sPrescription) {
		if (sPrescription == null)
			return false;
		while (sPrescription.contains("  "))
			sPrescription = sPrescription.replaceAll("  ", " ") .trim();
		if (sPrescription.isEmpty())
			return false;
		if (this.prescriptions.indexOf(sPrescription) < 0) {
			this.prescriptions.add(sPrescription);
			return true;
		} else
			return false;
	}

	public void mergePrescription(ParsedQuestionChoiceRecord oParsedQuestionChoiceRecord) {
		for (String sPrescription: oParsedQuestionChoiceRecord.prescriptions) {
			this.addPrescription(sPrescription);
		}
	}
	
	public String genInsertSql(ParsedPrescriptionSet parsedPrescriptionSet, Integer piQuestionId, ParsedQuestionRecord parsedQuestionRecord) {
		String sSql = "\nINSERT INTO " + QuestionChoiceRecord.TABLE_QUESTION_CHOICES
				+ " (" + QuestionChoiceRecord.FIELD_QUESTION_CHOCE_ID
				+ ", " +  QuestionChoiceRecord.FIELD_QUESTION_ID
				+ ", " +  QuestionChoiceRecord.FIELD_CHOICE_TEXT
				+ ", " +  QuestionChoiceRecord.FIELD_PROMPT_AFTER_SELECTION
				+ ") VALUES (" + this.Stg2PossibleAnswerId
				+ ", " + piQuestionId // this.stg2QuestionId
				+ ", " + SqlCommons.quoteString(this.choiceText)
				+ ", " + SqlCommons.quoteString(this.promptAfterSelection)
				+ ");"
				+ " -- [" + parsedQuestionRecord.getNameText() + "]";
		String sSqlPrescriptions = "";
		for (String prescription : this.prescriptions) {
			Integer iId = parsedPrescriptionSet.getId(prescription);
			if (iId == null) {
				//System.out.println("Unable to locate prescription: '" + prescription + "'");
			} else {
				sSqlPrescriptions += (sSqlPrescriptions.isEmpty() ? "" : ",") + SqlCommons.quoteString(prescription);
				/*
				sSql += "\nINSERT INTO " + QuestionChoicePrescriptionRecord.TABLE_QUESTION_CHOICE_PRESCRIPTIONS
						+ " (" + QuestionChoicePrescriptionRecord.FIELD_QUESTION_CHOCE_ID
						+ ", " +  QuestionChoicePrescriptionRecord.FIELD_PRESCRIPTION_ID
						+ ") VALUES (" + this.Stg2PossibleAnswerId
						+ ", " + iId
						+ "); -- " + prescription;
				*/
			}
		}
		if (!sSqlPrescriptions.isEmpty()) {
			sSql += "\nINSERT INTO " + QuestionChoicePrescriptionRecord.TABLE_QUESTION_CHOICE_PRESCRIPTIONS
					+ " (" + QuestionChoicePrescriptionRecord.FIELD_QUESTION_CHOCE_ID
					+ ", " + QuestionChoicePrescriptionRecord.FIELD_PRESCRIPTION_ID
					+ ")" +
					"\nSELECT " + this.Stg2PossibleAnswerId + ", " + PrescriptionRecord.FIELD_PRESCRIPTION_ID +
					" FROM " + PrescriptionRecord.TABLE_PRESCRIPTIONS + 
					" WHERE " + PrescriptionRecord.FIELD_PRESCRIPTION_NAME + " IN (" + sSqlPrescriptions + ");"
					+ " -- [" + parsedQuestionRecord.getNameText() + "]";
		}
		
		return sSql;
	}
	
	public String genInsertSql_wFilter(ParsedPrescriptionSet parsedPrescriptionSet, Integer piQuestionId, ParsedQuestionRecord parsedQuestionRecord, Integer iClauseVersionId) {
		String sSql = "\nINSERT INTO " + QuestionChoiceRecord.TABLE_QUESTION_CHOICES
				+ "( " +  QuestionChoiceRecord.FIELD_QUESTION_ID
				+ ", " +  QuestionChoiceRecord.FIELD_CHOICE_TEXT
				+ ", " +  QuestionChoiceRecord.FIELD_PROMPT_AFTER_SELECTION
				+ ")\nSELECT "  + QuestionRecord.FIELD_QUESTION_ID //piQuestionId // this.stg2QuestionId
				+ ", " + SqlCommons.quoteString(this.choiceText)
				+ ", " + SqlCommons.quoteString(this.promptAfterSelection)
				+ " FROM " + QuestionRecord.TABLE_QUESTIONS
				+ " WHERE " + QuestionRecord.FIELD_CLAUSE_VERSION_ID + " = " + iClauseVersionId
				+ " AND " + QuestionRecord.FIELD_QUESTION_CODE + " = " + SqlCommons.quoteString(parsedQuestionRecord.questionCode) + ";"
				+ " -- [" + parsedQuestionRecord.getNameText() + "]";
		String sSqlPrescriptions = "";
		for (String prescription : this.prescriptions) {
			Integer iId = parsedPrescriptionSet.getId(prescription);
			if (iId == null) {
				//System.out.println("Unable to locate prescription: '" + prescription + "'");
			} else {
				sSqlPrescriptions += (sSqlPrescriptions.isEmpty() ? "" : ",") + SqlCommons.quoteString(prescription);
			}
		}
		if (!sSqlPrescriptions.isEmpty()) {
			sSql += genInsertPrescriptionSql_wFilter (parsedQuestionRecord, iClauseVersionId);
		}
		
		return sSql;
	}
	public String genInsertPrescriptionSql(Integer piQuestionChoiceId, ParsedQuestionRecord parsedQuestionRecord) {
		String sSqlPrescriptions = "";
		for (String prescription : this.prescriptions) {
			sSqlPrescriptions += (sSqlPrescriptions.isEmpty() ? "" : ",") + SqlCommons.quoteString(prescription);
		}
		if (!sSqlPrescriptions.isEmpty()) {
			return "\nINSERT INTO " + QuestionChoicePrescriptionRecord.TABLE_QUESTION_CHOICE_PRESCRIPTIONS
					+ " (" + QuestionChoicePrescriptionRecord.FIELD_QUESTION_CHOCE_ID
					+ ", " + QuestionChoicePrescriptionRecord.FIELD_PRESCRIPTION_ID
					+ ")" +
					"\nSELECT " + ((piQuestionChoiceId == null) ? this.Stg2PossibleAnswerId : piQuestionChoiceId)
					+ ", " + PrescriptionRecord.FIELD_PRESCRIPTION_ID +
					" FROM " + PrescriptionRecord.TABLE_PRESCRIPTIONS + 
					" WHERE " + PrescriptionRecord.FIELD_PRESCRIPTION_NAME + " IN (" + sSqlPrescriptions + ");"
					+ " -- [" + parsedQuestionRecord.getNameText() + "] '" + this.choiceText + "'";
		}
		else
			return "";
	}
	
	public String genInsertPrescriptionSql_wFilter(ParsedQuestionRecord parsedQuestionRecord, Integer iClauseVersionId) {
		String sSqlPrescriptions = "";
		for (String prescription : this.prescriptions) {
			sSqlPrescriptions += (sSqlPrescriptions.isEmpty() ? "" : ",") + SqlCommons.quoteString(prescription);
		}
		if (!sSqlPrescriptions.isEmpty()) {
			
			String sSql = "\nINSERT INTO " + QuestionChoicePrescriptionRecord.TABLE_QUESTION_CHOICE_PRESCRIPTIONS
					+ " (" + QuestionChoicePrescriptionRecord.FIELD_QUESTION_CHOCE_ID
					+ ", " + QuestionChoicePrescriptionRecord.FIELD_PRESCRIPTION_ID
					+ ")" 
					+ "\nSELECT " + QuestionChoicePrescriptionRecord.FIELD_QUESTION_CHOCE_ID + ", " + PrescriptionRecord.FIELD_PRESCRIPTION_ID 
					+ " FROM " + QuestionChoiceRecord.TABLE_QUESTION_CHOICES  + ", " + PrescriptionRecord.TABLE_PRESCRIPTIONS
					+ "\n  WHERE " + PrescriptionRecord.FIELD_PRESCRIPTION_NAME + " IN (" + sSqlPrescriptions + ") "
					+ "\n  AND " + QuestionChoiceRecord.FIELD_CHOICE_TEXT + " = " + SqlCommons.quoteString(this.choiceText)
					+ " AND  " + QuestionRecord.FIELD_QUESTION_ID 
					+ " IN (SELECT " + QuestionRecord.FIELD_QUESTION_ID + " FROM " + QuestionRecord.TABLE_QUESTIONS 
					+ " WHERE " + QuestionRecord.FIELD_CLAUSE_VERSION_ID + " = " + iClauseVersionId
							+ " AND " + QuestionRecord.FIELD_QUESTION_CODE + " = " 
							+ SqlCommons.quoteString(parsedQuestionRecord.questionCode) + ");"
							+ " -- [" + parsedQuestionRecord.getNameText() + "] '" + this.choiceText + "'";

			return sSql;
		}
		else
			return "";
	}
	
}
