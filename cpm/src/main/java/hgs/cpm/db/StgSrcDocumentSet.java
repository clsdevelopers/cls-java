package hgs.cpm.db;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class StgSrcDocumentSet extends ArrayList<StgSrcDocumentRecord> {

	private static final long serialVersionUID = -6616208066040269005L;
	
	public Integer iEfcrSrcDocId = null; // 104;
	public Integer iQuestionSequenceSrcDocId = null; // 112;
	public Integer iClauseSrcDocId = null; // 111;
	public Integer iQuestionSrcDocId = null; // 113;

	public Integer getSrcDocId(String srcDocTypeCd) {
		StgSrcDocumentRecord oRecord = this.find(srcDocTypeCd);
		if (oRecord == null)
			return null;
		return oRecord.srcDocId;
	}

	public StgSrcDocumentRecord find(String srcDocTypeCd) {
		for (StgSrcDocumentRecord oRecord : this) {
			if (oRecord.srcDocTypeCd.equals(srcDocTypeCd)) {
				return oRecord;
			}
		}
		return null;
	}

    public void detectLatestSrcDocIds() {
		this.iEfcrSrcDocId = this.getSrcDocId(StgSrcDocumentRecord.TYPE_CD_ECFR);
		this.iQuestionSequenceSrcDocId = this.getSrcDocId(StgSrcDocumentRecord.TYPE_CD_QUESTION_SEQUENCE);
		this.iClauseSrcDocId = this.getSrcDocId(StgSrcDocumentRecord.TYPE_CD_CLAUSE);
		this.iQuestionSrcDocId = this.getSrcDocId(StgSrcDocumentRecord.TYPE_CD_QUESTION);
    }

    // =============================================================================
	public static StgSrcDocumentSet getLatest(Connection conn) throws SQLException {
		StgSrcDocumentSet result = new StgSrcDocumentSet();
		String sSql = 
				"SELECT A.Src_Doc_Id, A.Src_File_Name, A.Src_Doc_Type_Cd, A.Src_Vesion, A.Version_Date, A.Reson_For_Update, A.Created_Date\n" +
				"FROM " + StgSrcDocumentRecord.TABLE_STG_SRC_DOCUMENT + " A\n" +
				"INNER JOIN (\n" +
					"SELECT B.Src_Doc_Type_Cd, MAX(b.Src_Doc_Id) Src_Doc_Id\n" +
				    "FROM " + StgSrcDocumentRecord.TABLE_STG_SRC_DOCUMENT + " B\n" +
				    "GROUP BY B.Src_Doc_Type_Cd\n" +
				") R ON (R.Src_Doc_Type_Cd = A.Src_Doc_Type_Cd AND R.Src_Doc_Id = A.Src_Doc_Id)";
		
		java.sql.Statement statement = null;
		ResultSet rs = null;
		try {
			statement = conn.createStatement();
			rs = statement.executeQuery(sSql);
			while (rs.next()) {
				StgSrcDocumentRecord oRecord = new StgSrcDocumentRecord();
				oRecord.read(rs);
				result.add(oRecord);
			}
			result.detectLatestSrcDocIds();
		}
		finally {
			if (statement != null)
				statement.close();
		}
		
		return result;
	}
	
}
