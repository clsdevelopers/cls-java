package hgs.cpm.db;

import gov.dod.cls.utils.CommonUtils;
import gov.dod.cls.utils.sql.SqlUtil;
import hgs.cpm.config.CpmConfig;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;

public class StgSrcDocumentRecord {
	
	public static final String TYPE_CD_ECFR = "E";
	public static final String TYPE_CD_CLAUSE = "F";
	public static final String TYPE_CD_QUESTION = "Q";
	public static final String TYPE_CD_QUESTION_SEQUENCE = "S";
	
	public static final String TABLE_STG_SRC_DOCUMENT = "Stg_Src_Document";

	public static final String FIELD_SRC_DOC_ID           = "Src_Doc_Id";
	public static final String FIELD_SRC_FILE_NAME        = "Src_File_Name";
	public static final String FIELD_SRC_DOC_TYPE_CD      = "Src_Doc_Type_Cd";
	public static final String FIELD_SRC_VESION           = "Src_Vesion";
	public static final String FIELD_VERSION_DATE         = "Version_Date";
	public static final String FIELD_RESON_FOR_UPDATE     = "Reson_For_Update";
	public static final String FIELD_CREATED_DATE         = "Created_Date";
	
	public Integer srcDocId = null;
	public String srcFileName = null;
	public String srcDocTypeCd = null;
	public String srcVesion = null;
	public Date versionDate = null;
	public String resonForUpdate = null;
	public Date createdDate = null;

	public static Integer generateDocId(Connection conn, String fileName, String srcDocTypeCode) throws SQLException {
		Integer srcDocId =  null;
		//File f = new File(this.excelFileName);
		//String fileName = f.getName();
		boolean bCreateConnection = (conn == null);
		PreparedStatement psInsert = null;
		ResultSet res = null;
		try {
			if (bCreateConnection)
				conn = CpmConfig.dbConnection();
			String sSql
				= "INSERT INTO " + TABLE_STG_SRC_DOCUMENT
				+ " (" + FIELD_SRC_FILE_NAME + ", " + FIELD_SRC_DOC_TYPE_CD + ")"
				+ " VALUES (?, ?)";
			psInsert = conn.prepareStatement(sSql, Statement.RETURN_GENERATED_KEYS);
			psInsert.setString(1, fileName);
			psInsert.setString(2, srcDocTypeCode);
			psInsert.execute();
			res = psInsert.getGeneratedKeys();
			if (res.next()) {
				srcDocId = res.getInt(1);
				System.out.println("New Stg_Src_Document: Src_Doc_Id(" + srcDocId +
						"), Src_File_Name(" + fileName + "), Src_Doc_Type_Cd(" + srcDocTypeCode + ")");
			}
		} finally {
			SqlUtil.closeInstance(res);
			SqlUtil.closeInstance(psInsert);
			if (bCreateConnection)
				SqlUtil.closeInstance(conn);
		}
		return srcDocId;
	}
	
	public void read(ResultSet rs) throws SQLException {
		this.srcDocId = CommonUtils.toInteger(rs.getObject(FIELD_SRC_DOC_ID));
		this.srcFileName = rs.getString(FIELD_SRC_FILE_NAME);
		this.srcDocTypeCd = rs.getString(FIELD_SRC_DOC_TYPE_CD);
		this.srcVesion = rs.getString(FIELD_SRC_VESION);
		this.versionDate = CommonUtils.toDate(rs.getObject(FIELD_VERSION_DATE));
		this.resonForUpdate = rs.getString(FIELD_RESON_FOR_UPDATE);
		this.createdDate = CommonUtils.toDate(rs.getObject(FIELD_CREATED_DATE));
		
	}
	
}
