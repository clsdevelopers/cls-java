package hgs.cpm.db;

import gov.dod.cls.utils.CommonUtils;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

public class Stg3ClauseCorrectionRecord {
	
	public static final String TABLE_STG3_CLAUSE_CORRECTIONS = "Stg3_Clause_Corrections";
	
	public static final String FIELD_STG3_CLAUSE_ID       = "Stg3_Clause_Id";
	public static final String FIELD_CLAUSE_VERSION_ID    = "Clause_Version_Id";
	public static final String FIELD_CLAUSE_NAME          = "Clause_Name";
	public static final String FIELD_CLAUSE_DATA          = "Clause_Data";
	public static final String FIELD_CREATED_AT           = "Created_At";
	public static final String FIELD_UPDATED_AT			= "Updated_At";
	
	public static final String SQL_SELECT
		= "SELECT " + FIELD_STG3_CLAUSE_ID
		+ ", " + FIELD_CLAUSE_VERSION_ID
		+ ", " + FIELD_CLAUSE_NAME
		+ ", " + FIELD_CLAUSE_DATA
		+ ", " + FIELD_CREATED_AT
		+ ", " + FIELD_UPDATED_AT
		+ " FROM " + TABLE_STG3_CLAUSE_CORRECTIONS
		+ " WHERE " + FIELD_CLAUSE_VERSION_ID + " = ?";

	public Integer stg3ClauseId;
	public Integer clauseVersionId;
	public String clauseName;
	public String clauseData;
	public Date createdAt;
	public Date updatedAt;
	
	public Stg3ClauseFilInCorrectionSet aFillInSet = new Stg3ClauseFilInCorrectionSet();
	
	public void read(ResultSet rs1) throws SQLException {
		this.stg3ClauseId = rs1.getInt(FIELD_STG3_CLAUSE_ID);
		this.clauseVersionId = rs1.getInt(FIELD_CLAUSE_VERSION_ID);
		this.clauseName = rs1.getString(FIELD_CLAUSE_NAME);
		this.clauseData = rs1.getString(FIELD_CLAUSE_DATA);
		this.createdAt = CommonUtils.toDate(rs1.getObject(FIELD_CREATED_AT));
		this.updatedAt = CommonUtils.toDate(rs1.getObject(FIELD_UPDATED_AT));
		this.aFillInSet.clear();
	}

	public void read(ResultSet rs1, PreparedStatement ps) throws SQLException {
		this.read(rs1);
		if (ps != null)
			this.aFillInSet.load(ps, this.stg3ClauseId);
	}
}
