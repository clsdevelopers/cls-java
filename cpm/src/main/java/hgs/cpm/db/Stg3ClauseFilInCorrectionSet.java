package hgs.cpm.db;

import gov.dod.cls.utils.sql.SqlUtil;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class Stg3ClauseFilInCorrectionSet extends ArrayList<Stg3ClauseFilInCorrectionRecord> {

	private static final long serialVersionUID = -8339890057377163982L;
	
	public void load(PreparedStatement ps, int piStg3ClauseId) throws SQLException {
		ResultSet rs1 = null;
		try {
			ps.setInt(1, piStg3ClauseId);
			rs1 = ps.executeQuery();
			this.clear();
			while (rs1.next()) {
				Stg3ClauseFilInCorrectionRecord oRecord = new Stg3ClauseFilInCorrectionRecord();
				oRecord.read(rs1);
				this.add(oRecord);
			}
		} finally {
			SqlUtil.closeInstance(rs1);
		}
	}
	
	public void copy(ArrayList<ParsedClauseFillInRecord> fillIns) {
		fillIns.clear();
		for (Stg3ClauseFilInCorrectionRecord oRecord : this) {
			fillIns.add(oRecord.copy());
		}
	}

}
