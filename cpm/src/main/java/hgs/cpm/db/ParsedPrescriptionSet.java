package hgs.cpm.db;

import gov.dod.cls.db.PrescriptionRecord;
import gov.dod.cls.utils.CommonUtils;
import gov.dod.cls.utils.sql.SqlUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class ParsedPrescriptionSet extends ArrayList<ParsedPrescriptionRecord> {

	private static final long serialVersionUID = -2173370039561635575L;
	
	public static boolean generateSql = true;
	
	private boolean sqlGenerated = false;
	
	public void load(Connection conn, int iQuestionSrcDocId, int iClauseSrcDocId) throws SQLException {
		String sSql = "SELECT Parsed.Prescription, B.Prescription_Id\n" +
				"FROM (\n" +
					"SELECT DISTINCT Prescription\n" +
				    "FROM (\n" +
						"SELECT DISTINCT Prescription\n" +
						"FROM Stg2_Possible_Answers_Prescription\n" +
						"WHERE Stg2_Possible_Answer_Id IN (\n" +
							"SELECT Stg2_Possible_Answer_Id\n" +
							"FROM Stg2_Possible_Answer\n" +
							"WHERE Src_Doc_Id = ?\n" +
						")\n" +
						"UNION\n" +
						"SELECT DISTINCT Prescription\n" +
						"FROM Stg2_Clause_Prescriptions_Xref\n" +
						"WHERE Stg1_Clause_Id IN (\n" +
							"SELECT Stg1_Clause_Id\n" +
							"FROM Stg1_Raw_Clauses\n" +
							"WHERE Src_Doc_Id = ?\n" +
						")\n" +
					") Src\n" +
				") Parsed\n" +
				"LEFT OUTER JOIN Prescriptions B ON (B.Prescription_Name = Parsed.Prescription)\n" +
				"ORDER BY 1";
		PreparedStatement ps1 = null;
		ResultSet rs1 = null;
		PreparedStatement ps2 = null;
		ResultSet rs2 = null;
		try {
			String sSql2 = "SELECT MAX(" + PrescriptionRecord.FIELD_PRESCRIPTION_ID + ") + 1"
						+ " FROM " + PrescriptionRecord.TABLE_PRESCRIPTIONS;
			ps2 = conn.prepareStatement(sSql2);
			rs2 = ps2.executeQuery();
			int iNextId = 1;
			if (rs2.next())
				iNextId = rs2.getInt(1);
			
			ps1 = conn.prepareStatement(sSql);
			ps1.setInt(1, iQuestionSrcDocId);
			ps1.setInt(2, iClauseSrcDocId);
			rs1 = ps1.executeQuery();
			this.clear();
			while (rs1.next()) {
				ParsedPrescriptionRecord oRecord = new ParsedPrescriptionRecord();
				oRecord.read(rs1);
				oRecord.prescription = rs1.getString(1);
				oRecord.prescription = oRecord.prescription.replaceAll("\\*", "");
				
				oRecord.id = CommonUtils.toInteger(rs1.getObject(2));
				oRecord.isNew = (oRecord.id == null);
				if (oRecord.isNew)
					oRecord.id = iNextId++;				
				this.add(oRecord);
			}
		}
		finally {
			SqlUtil.closeInstance(rs2, ps2);
			SqlUtil.closeInstance(rs1, ps1);
		}
	}
	
	public String genSql() {
		if (this.sqlGenerated || (!ParsedPrescriptionSet.generateSql))
			return "";
		
		String sSql = "/* ===============================================\n"
				+ " * Prescriptions\n"
				+ "   =============================================== */\n";
		boolean bFound = false;
		for (ParsedPrescriptionRecord oRecord : this) {
			sSql += oRecord.genSql();
			if (oRecord.isNew) {
				bFound = true;
			}
		}
		
		if (bFound) {
			sSql += "\nUPDATE Prescriptions\n" +
					"JOIN (\n" +
					"	SELECT\n" + 
					"		prescription_id,\n" +
					"		concat('http://www.ecfr.gov/cgi-bin/text-idx?node=se48.', volume , '.', part, '_1', subpart, replace(suffix, '-', '_6')) uri\n" +
					"	FROM (\n" +
					"		SELECT\n" + 
					"			prescription_id,\n" +
					"			prescription_name,\n" +
					"			subpart,\n" +
					"			part,\n" +
					"			case when suffix like '%(%' then left(suffix,locate('(',suffix)-1) else suffix end suffix,\n" +
					"			case when part regexp '^[0-9]+$'\n" +
					"				then\n" + 
					"					case\n" + 
					"						when cast(part as UNSIGNED) >= '1' and cast(part as UNSIGNED) <= '51' then 1\n" + 
					"						when cast(part as UNSIGNED) >= '52' and cast(part as UNSIGNED) <= '99' then 2\n" +
					"						when cast(part as UNSIGNED) >= '200' and cast(part as UNSIGNED) <= '299' then 3\n" +
					"					end\n" + 
					"			end volume\n" +
					"		FROM (\n" +
					"			SELECT\n" +
					"				prescription_id,\n" +
					"				prescription_name,\n" +
					"				substring_index(substring_index(prescription_name,'(', 1),'.',1) part,\n" +
					"				substring_index(substring_index(substring_index(prescription_name,'(', 1),'.',-1),'-',1) subpart,\n" +
					"				case\n" + 
					"					when locate('-',prescription_name) != 0\n" + 
					"						then right(prescription_name, length(prescription_name) - locate('-',prescription_name)+1)\n" + 
					"					else ''\n" +
					"				end suffix\n" +
					"			FROM Prescriptions\n" +
					"		) Prescriptions\n" +
					"	) Prescriptions\n" +
					") sub ON (sub.prescription_id = Prescriptions.prescription_id)\n" +
					"SET Prescriptions.prescription_url = ifNull(sub.uri,'')\n" +
					"WHERE ifNull(prescription_url, '') = '';\n\n";
		}
		
		this.sqlGenerated = true;
		return sSql;
	}
	
	public Integer getId(String pPrescription) {
		for (ParsedPrescriptionRecord oRecord : this) {
			if (oRecord.prescription.equalsIgnoreCase(pPrescription))
				return oRecord.id;
		}
		return null;
	}
	
	public String getName(Integer pId) {
		for (ParsedPrescriptionRecord oRecord : this) {
			if (oRecord.id.equals(pId))
				return oRecord.prescription;
		}
		return null;
	}

	public boolean isSqlGenerated() {
		return sqlGenerated;
	}

}
