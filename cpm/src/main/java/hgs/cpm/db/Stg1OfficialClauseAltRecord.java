package hgs.cpm.db;

import gov.dod.cls.utils.sql.SqlUtil;
import hgs.cpm.utils.SqlCommons;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class Stg1OfficialClauseAltRecord {

	public static final String TABLE_STG1_OFFICIAL_CLAUSE_ALT = "Stg1_Official_Clause_Alt";
	
	public static final String FIELD_STG1_OFFICIAL_CLAUSE_ALT_ID = "Stg1_Official_Clause_Alt_Id";
	public static final String FIELD_CLAUSE_NUMBER        = "Clause_Number"; // VARCHAR(100) NULL,
	public static final String FIELD_Alt_Number           = "Alt_Number"; // VARCHAR(100) NULL,
	public static final String FIELD_Instructions         = "Instructions"; // TEXT NULL,
	public static final String FIELD_CONTENT              = "Content"; // TEXT NULL,
	public static final String FIELD_CONTENT_HTML         = "Content_HTML"; // TEXT NULL,
	public static final String FIELD_CONTENT_HTML_FILLINS = "Content_Html_FillIns"; // TEXT NULL,
	public static final String FIELD_CONTENT_HTML_PROCESSED = "Content_Html_Processed"; // TEXT NULL,
	public static final String FIELD_SRC_DOC_ID           = "Src_Doc_Id"; // SMALLINT NOT NULL,
	public static final String FIELD_EDIT_AND_FILLIN      = "IsEditAndFillIn"; // SMALLINT NOT NULL,
	
	public static PreparedStatement createInsertStatement(Connection conn) throws SQLException {
		String sSql =  
			"INSERT INTO " + TABLE_STG1_OFFICIAL_CLAUSE_ALT + "\n" +
			"(" + FIELD_CLAUSE_NUMBER + 
			", " + FIELD_Alt_Number + 
			", " + FIELD_Instructions + 
			", " + FIELD_CONTENT + 
			", " + FIELD_CONTENT_HTML + 
			", " + FIELD_CONTENT_HTML_FILLINS + 
			", " + FIELD_SRC_DOC_ID + ")\n" +
			"VALUES (?, ?, ?, ?, ?, ?, ?)";
		return conn.prepareStatement(sSql);
	}
	/*
	sSql = "INSERT INTO Stg1_Official_Clause_Alt\n" +
			"(Clause_Number, Alt_Number, Instructions, Content, Content_HTML, Content_Html_FillIns, Src_Doc_Id)\n" +
			"VALUES (?, ?, ?, ?, ?, ?, ?)";
	*/

	public static void deleteAll(Connection conn, Integer srcDocId) throws SQLException {
		if (srcDocId == null)
			return;
		
		String sSql = "DELETE FROM " + Stg1OfficialClauseAltRecord.TABLE_STG1_OFFICIAL_CLAUSE_ALT + " WHERE " + Stg1OfficialClauseAltRecord.FIELD_SRC_DOC_ID + " = ?";
		PreparedStatement ps2 = null;
		try {
			ps2 = conn.prepareStatement(sSql);
			ps2.setInt(1, srcDocId);
			ps2.execute();
		}
		finally {
			SqlUtil.closeInstance(ps2);
		}
	}

	
	public Integer stg1OfficialClauseAltId;
	public String clauseNumber;
	public String altNumber = null;
	public String instructions;
	public String content;
	public String contentHtml;
	public String contentHtmlFillIns;
	public String contentHtmlProcessed;
	public Integer srcDocId;
	
	public String getClausFullNumber() {
		return this.clauseNumber + ((this.altNumber == null) ? "" : " " + this.altNumber);
	}

	public void insert(PreparedStatement ps) throws SQLException {
		SqlUtil.setParam(ps, 1, this.clauseNumber, java.sql.Types.VARCHAR);
		SqlUtil.setParam(ps, 2, this.altNumber, java.sql.Types.VARCHAR);
		SqlUtil.setParam(ps, 3, this.instructions, java.sql.Types.VARCHAR);
		SqlUtil.setParam(ps, 4, this.content, java.sql.Types.VARCHAR);
		SqlUtil.setParam(ps, 5, this.contentHtml, java.sql.Types.VARCHAR);
		SqlUtil.setParam(ps, 6, this.contentHtmlFillIns, java.sql.Types.VARCHAR);
		SqlUtil.setParam(ps, 7, this.srcDocId, java.sql.Types.INTEGER);
		ps.execute();
	}
	
	public void clear() {
		this.stg1OfficialClauseAltId = null;
		this.clauseNumber = null;
		this.altNumber = null;
		this.instructions = null;
		this.content = null;
		this.contentHtml = null;
		this.contentHtmlFillIns = null;
		this.contentHtmlProcessed = null;
	}
	
	public void generateFile(String sOutputFolder, boolean bSql) throws IOException {
		String sFileName = sOutputFolder + "\\" + this.clauseNumber + " " + this.altNumber + "." + (bSql ? "sql" : "html");
		FileOutputStream fileOutputStream = null;
    	try {
    		File file = new File(sFileName);
    		if (file.exists()) {
    			file.delete();
    			System.out.println("Deleted " + sFileName);
    		}
    		file.createNewFile();

    		fileOutputStream = new FileOutputStream(file);
    		if (bSql) {
    			String sSql =  
    					"INSERT INTO " + TABLE_STG1_OFFICIAL_CLAUSE_ALT + "\n" +
    					"(" + FIELD_CLAUSE_NUMBER + 
    					", " + FIELD_Alt_Number + 
    					", " + FIELD_Instructions + 
    					", " + FIELD_CONTENT + 
    					", " + FIELD_CONTENT_HTML + 
    					", " + FIELD_CONTENT_HTML_FILLINS + 
    					", " + FIELD_SRC_DOC_ID + ")\n" +
    					"VALUES (" + SqlCommons.quoteString(this.clauseNumber) + 
        				", " + SqlCommons.quoteString(this.altNumber) + 
        				", " + SqlCommons.quoteString(this.instructions) + 
        				",\n" + SqlCommons.quoteString(this.content) + 
        				",\n" + SqlCommons.quoteString(this.contentHtml) + 
        				",\n" + SqlCommons.quoteString(this.contentHtmlFillIns) + 
        				",\n" + this.srcDocId + 
        				")\n";
    			fileOutputStream.write(sSql.getBytes());
    		} else {
        		String sContent;
        		if (this.contentHtmlProcessed != null)
        			sContent = this.contentHtmlProcessed;
        		else if (this.contentHtmlFillIns != null)
        			sContent = this.contentHtmlFillIns;
        		else if (this.contentHtml != null)
        			sContent = this.contentHtml;
        		else
        			sContent = this.content;
    			fileOutputStream.write(sContent.getBytes());
    		}
		}
    	finally {
			try {
				if (fileOutputStream != null) {
					fileOutputStream.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
    	}
	}
	
}
