package hgs.cpm.db;

import gov.dod.cls.db.ClauseFillInRecord;
import gov.dod.cls.db.ClausePrescriptionRecord;
import gov.dod.cls.db.ClauseRecord;
import gov.dod.cls.utils.CommonUtils;
import hgs.cpm.utils.SqlCommons;

public class ParsedClauseFillInRecord {

	public String code;
	public String type;
	public Integer maxSize;
	public Integer id;

	public Integer fillInGroupNumber = null;
	public String fillInPlaceholder = null;
	public String fillInDefaultData = null;
	public Integer fillInDisplayRows = null;
	public boolean fillInForTable = false; //    boolean NOT NULL DEFAULT 0,
	public String fillInHeading = null;
	
	public boolean isMatch(ClauseFillInRecord oClauseFillInRecord) {
		if (CommonUtils.isSame(oClauseFillInRecord.getFillInCode(), this.code))
			return true;
		/*
		if (CommonUtils.isSame(oClauseFillInRecord.getFillInCode(), this.code)
			&& CommonUtils.isSame(oClauseFillInRecord.getFillInType(), this.type)
			&& CommonUtils.isSame(oClauseFillInRecord.getFillInMaxSize(), this.maxSize))
			return true;
		*/
		return false;
	}
	
	public boolean isDiffernt(ClauseFillInRecord oClauseFillInRecord) {
		if (CommonUtils.isNotSame(oClauseFillInRecord.getFillInCode(), this.code)
			|| CommonUtils.isNotSame(oClauseFillInRecord.getFillInType(), this.type)
			|| CommonUtils.isNotSame(oClauseFillInRecord.getFillInMaxSize(), this.maxSize)
			|| CommonUtils.isNotSame(oClauseFillInRecord.getFillInGroupNumber(), this.fillInGroupNumber)
			|| CommonUtils.isNotSame(oClauseFillInRecord.getFillInPlaceholder(), this.fillInPlaceholder)
			|| CommonUtils.isNotSame(oClauseFillInRecord.getFillInDefaultData(), this.fillInDefaultData)
			|| CommonUtils.isNotSame(oClauseFillInRecord.getFillInDisplayRows(), this.fillInDisplayRows)
			|| CommonUtils.isNotSame(oClauseFillInRecord.isFillInForTable(), this.fillInForTable)
			|| CommonUtils.isNotSame(oClauseFillInRecord.getFillInHeading(), this.fillInHeading)
			)
			return true;
		
		return false;
	}
	
	public String genSqlInsert(int clauseId) {
		String result
				= "INSERT INTO " + ClauseFillInRecord.TABLE_CLAUSE_FILL_INS
				+ " (" + ClauseFillInRecord.FIELD_CLAUSE_ID
				+ ", " +  ClauseFillInRecord.FIELD_FILL_IN_CODE
				+ ", " +  ClauseFillInRecord.FIELD_FILL_IN_TYPE
				+ ", " +  ClauseFillInRecord.FIELD_FILL_IN_MAX_SIZE

				+ ", " + ClauseFillInRecord.FIELD_FILL_IN_GROUP_NUMBER
				+ ", " + ClauseFillInRecord.FIELD_FILL_IN_PLACEHOLDER
				+ ", " + ClauseFillInRecord.FIELD_FILL_IN_DEFAULT_DATA
				+ ", " + ClauseFillInRecord.FIELD_FILL_IN_DISPLAY_ROWS
				+ ", " + ClauseFillInRecord.FIELD_FILL_IN_FOR_TABLE
				+ ", " + ClauseFillInRecord.FIELD_FILL_IN_HEADING
				
				+ ") VALUES (" + clauseId  
				+ ", " + SqlCommons.quoteString(this.code)
				+ ", " + SqlCommons.quoteString(this.type)
				+ ", " + SqlCommons.quoteString(this.maxSize)

				+ ", " + SqlCommons.quoteString(this.fillInGroupNumber)
				+ ", " + SqlCommons.quoteString(this.fillInPlaceholder)
				+ ", " + SqlCommons.quoteString(this.fillInDefaultData)
				+ ", " + SqlCommons.quoteString(this.fillInDisplayRows)
				+ ", " + SqlCommons.quoteString(this.fillInForTable)
				+ ", " + SqlCommons.quoteString(this.fillInHeading)
				
				+ ");";
		
		return result;
	}
	
	public String genSqlInsert_wFilter(int clauseId, String clauseName, int iClauseVersionId) {
		String result
				= "INSERT INTO " + ClauseFillInRecord.TABLE_CLAUSE_FILL_INS
				+ " (" + ClauseFillInRecord.FIELD_CLAUSE_ID
				+ ", " +  ClauseFillInRecord.FIELD_FILL_IN_CODE
				+ ", " +  ClauseFillInRecord.FIELD_FILL_IN_TYPE
				+ ", " +  ClauseFillInRecord.FIELD_FILL_IN_MAX_SIZE

				+ ", " + ClauseFillInRecord.FIELD_FILL_IN_GROUP_NUMBER
				+ ", " + ClauseFillInRecord.FIELD_FILL_IN_PLACEHOLDER
				+ ", " + ClauseFillInRecord.FIELD_FILL_IN_DEFAULT_DATA
				+ ", " + ClauseFillInRecord.FIELD_FILL_IN_DISPLAY_ROWS
				+ ", " + ClauseFillInRecord.FIELD_FILL_IN_FOR_TABLE
				+ ", " + ClauseFillInRecord.FIELD_FILL_IN_HEADING
				
				+ ") \nSELECT " + ClauseFillInRecord.FIELD_CLAUSE_ID  
				+ ", " + SqlCommons.quoteString(this.code)
				+ ", " + SqlCommons.quoteString(this.type)
				+ ", " + SqlCommons.quoteString(this.maxSize)

				+ ", " + SqlCommons.quoteString(this.fillInGroupNumber)
				+ ", " + SqlCommons.quoteString(this.fillInPlaceholder)
				+ ", " + SqlCommons.quoteString(this.fillInDefaultData)
				+ ", " + SqlCommons.quoteString(this.fillInDisplayRows)
				+ ", " + SqlCommons.quoteString(this.fillInForTable)
				+ ", " + SqlCommons.quoteString(this.fillInHeading)
				+ " FROM " + ClauseRecord.TABLE_CLAUSES
				+ " WHERE " + ClauseRecord.FIELD_CLAUSE_NAME + " = '" + clauseName 
				+ "' AND " + ClauseRecord.FIELD_CLAUSE_VERSION_ID + " = " + iClauseVersionId + "; ";
				
		
		return result;
	}
	
	public String genSqlUpdate(ClauseFillInRecord oClauseFillInRecord) {
		String result = "";
		if (CommonUtils.isNotSame(oClauseFillInRecord.getFillInCode(), this.code))
			result += (result.isEmpty() ? "SET " : ", ") + 	ClauseFillInRecord.FIELD_FILL_IN_CODE + " = " + SqlCommons.quoteString(this.code);

		if (CommonUtils.isNotSame(oClauseFillInRecord.getFillInType(), this.type))
			result += (result.isEmpty() ? "SET " : ", ") + 	ClauseFillInRecord.FIELD_FILL_IN_TYPE + " = " + SqlCommons.quoteString(this.type);

		if (CommonUtils.isNotSame(oClauseFillInRecord.getFillInMaxSize(), this.maxSize))
			result += (result.isEmpty() ? "SET " : ", ") + 	ClauseFillInRecord.FIELD_FILL_IN_MAX_SIZE + " = " + SqlCommons.quoteString(this.maxSize);
		
		if (CommonUtils.isNotSame(oClauseFillInRecord.getFillInGroupNumber(), this.fillInGroupNumber))
			result += (result.isEmpty() ? "SET " : ", ") + 	ClauseFillInRecord.FIELD_FILL_IN_GROUP_NUMBER + " = " + SqlCommons.quoteString(this.fillInGroupNumber);
		
		if (CommonUtils.isNotSame(oClauseFillInRecord.getFillInPlaceholder(), this.fillInPlaceholder))
			result += (result.isEmpty() ? "SET " : ", ") + 	ClauseFillInRecord.FIELD_FILL_IN_PLACEHOLDER + " = " + SqlCommons.quoteString(this.fillInPlaceholder);
		
		if (CommonUtils.isNotSame(oClauseFillInRecord.getFillInDefaultData(), this.fillInDefaultData))
			result += (result.isEmpty() ? "SET " : ", ") + 	ClauseFillInRecord.FIELD_FILL_IN_DEFAULT_DATA + " = " + SqlCommons.quoteString(this.fillInDefaultData);
		
		if (CommonUtils.isNotSame(oClauseFillInRecord.getFillInDisplayRows(), this.fillInDisplayRows))
			result += (result.isEmpty() ? "SET " : ", ") + 	ClauseFillInRecord.FIELD_FILL_IN_DISPLAY_ROWS + " = " + SqlCommons.quoteString(this.fillInDisplayRows);

		if (CommonUtils.isNotSame(oClauseFillInRecord.isFillInForTable(), this.fillInForTable))
			result += (result.isEmpty() ? "SET " : ", ") + 	ClauseFillInRecord.FIELD_FILL_IN_FOR_TABLE + " = " + SqlCommons.quoteString(this.fillInForTable);

		if (CommonUtils.isNotSame(oClauseFillInRecord.getFillInHeading(), this.fillInHeading))
			result += (result.isEmpty() ? "SET " : ", ") + 	ClauseFillInRecord.FIELD_FILL_IN_HEADING + " = " + SqlCommons.quoteString(this.fillInHeading);
		
		if (!result.isEmpty()) {
			result = "\nUPDATE " + ClauseFillInRecord.TABLE_CLAUSE_FILL_INS + " " + result
					+ " WHERE " + ClauseFillInRecord.FIELD_CLAUSE_FILL_IN_ID + " = " + SqlCommons.quoteString(oClauseFillInRecord.getId()) + ";";
		}

		return result;
	}
	
	public String genSqlUpdate_wFilter(ClauseFillInRecord oClauseFillInRecord, String clauseName, Integer iClauseVersionId) {
		String result = "";
		if (CommonUtils.isNotSame(oClauseFillInRecord.getFillInCode(), this.code))
			result += (result.isEmpty() ? "SET " : ", ") + 	ClauseFillInRecord.FIELD_FILL_IN_CODE + " = " + SqlCommons.quoteString(this.code);

		if (CommonUtils.isNotSame(oClauseFillInRecord.getFillInType(), this.type))
			result += (result.isEmpty() ? "SET " : ", ") + 	ClauseFillInRecord.FIELD_FILL_IN_TYPE + " = " + SqlCommons.quoteString(this.type);

		if (CommonUtils.isNotSame(oClauseFillInRecord.getFillInMaxSize(), this.maxSize))
			result += (result.isEmpty() ? "SET " : ", ") + 	ClauseFillInRecord.FIELD_FILL_IN_MAX_SIZE + " = " + SqlCommons.quoteString(this.maxSize);
		
		if (CommonUtils.isNotSame(oClauseFillInRecord.getFillInGroupNumber(), this.fillInGroupNumber))
			result += (result.isEmpty() ? "SET " : ", ") + 	ClauseFillInRecord.FIELD_FILL_IN_GROUP_NUMBER + " = " + SqlCommons.quoteString(this.fillInGroupNumber);
		
		if (CommonUtils.isNotSame(oClauseFillInRecord.getFillInPlaceholder(), this.fillInPlaceholder))
			result += (result.isEmpty() ? "SET " : ", ") + 	ClauseFillInRecord.FIELD_FILL_IN_PLACEHOLDER + " = " + SqlCommons.quoteString(this.fillInPlaceholder);
		
		if (CommonUtils.isNotSame(oClauseFillInRecord.getFillInDefaultData(), this.fillInDefaultData))
			result += (result.isEmpty() ? "SET " : ", ") + 	ClauseFillInRecord.FIELD_FILL_IN_DEFAULT_DATA + " = " + SqlCommons.quoteString(this.fillInDefaultData);
		
		if (CommonUtils.isNotSame(oClauseFillInRecord.getFillInDisplayRows(), this.fillInDisplayRows))
			result += (result.isEmpty() ? "SET " : ", ") + 	ClauseFillInRecord.FIELD_FILL_IN_DISPLAY_ROWS + " = " + SqlCommons.quoteString(this.fillInDisplayRows);

		if (CommonUtils.isNotSame(oClauseFillInRecord.isFillInForTable(), this.fillInForTable))
			result += (result.isEmpty() ? "SET " : ", ") + 	ClauseFillInRecord.FIELD_FILL_IN_FOR_TABLE + " = " + SqlCommons.quoteString(this.fillInForTable);

		if (CommonUtils.isNotSame(oClauseFillInRecord.getFillInHeading(), this.fillInHeading))
			result += (result.isEmpty() ? "SET " : ", ") + 	ClauseFillInRecord.FIELD_FILL_IN_HEADING + " = " + SqlCommons.quoteString(this.fillInHeading);
		
		if (!result.isEmpty()) {
			result = "\nUPDATE " + ClauseFillInRecord.TABLE_CLAUSE_FILL_INS + " " + result
					+ " WHERE " + ClauseFillInRecord.FIELD_FILL_IN_CODE + " = " + SqlCommons.quoteString(oClauseFillInRecord.getFillInCode())  
					+ "\nAND " + ClauseFillInRecord.FIELD_CLAUSE_ID +
					" IN (SELECT " + ClausePrescriptionRecord.FIELD_CLAUSE_ID + " FROM " + ClauseRecord.TABLE_CLAUSES +
					" \nWHERE " + ClauseRecord.FIELD_CLAUSE_NAME + " = '" + clauseName +
					"' AND " + ClauseRecord.FIELD_CLAUSE_VERSION_ID + " = " + iClauseVersionId + ") ;";
		}

		return result;
	}
}
