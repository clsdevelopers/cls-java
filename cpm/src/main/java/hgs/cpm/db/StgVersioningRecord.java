package hgs.cpm.db;

import gov.dod.cls.utils.CommonUtils;
import gov.dod.cls.utils.sql.SqlUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;

public class StgVersioningRecord {

	public static final String TABLE_STG_VERSIONING = "Stg_Versioning";
	
	public static final String FIELD_STG_VERSIONING_ID = "Stg_Versioning_Id";
	public static final String FIELD_QUESTION_SRC_DOC_ID  = "Question_Src_Doc_Id";
	public static final String FIELD_CLAUSE_SRC_DOC_ID    = "Clause_Src_Doc_Id";
	public static final String FIELD_ECFR_SRC_DOC_ID      = "ECFR_Src_Doc_Id";
	public static final String FIELD_CLAUSE_VERSION_ID    = "Clause_Version_Id";
	public static final String FIELD_SQL_SCRIPT_FILE_NAME = "Sql_Script_File_Name";
	public static final String FIELD_UPDATED_AT = "Updated_At";
	
	public static final String SQL_SELECT
		= "SELECT " + FIELD_STG_VERSIONING_ID
		+ ", " + FIELD_QUESTION_SRC_DOC_ID
		+ ", " + FIELD_CLAUSE_SRC_DOC_ID
		+ ", " + FIELD_ECFR_SRC_DOC_ID
		+ ", " + FIELD_CLAUSE_VERSION_ID
		+ ", " + FIELD_SQL_SCRIPT_FILE_NAME
		+ ", " + FIELD_UPDATED_AT
		+ " FROM " + TABLE_STG_VERSIONING;
	
	public static StgVersioningRecord find(int stgVersioningId, Statement statement) throws SQLException {
		StgVersioningRecord result = null;
		String sSql = SQL_SELECT
				+ " WHERE " + FIELD_STG_VERSIONING_ID + " = " + stgVersioningId;
		ResultSet rs1 = null;
		try {
			rs1 = statement.executeQuery(sSql);
			if (rs1.next()) {
				result = new StgVersioningRecord();
				result.read(rs1);
			}
		} finally {
			SqlUtil.closeInstance(rs1);
		}
		return result;
	}
	
	public static StgVersioningRecord findByClauseVersionId(int clauseVersioningId, Statement statement) throws SQLException {
		StgVersioningRecord result = null;
		String sSql = SQL_SELECT
				+ " WHERE " + FIELD_CLAUSE_VERSION_ID + " = " + clauseVersioningId;
		ResultSet rs1 = null;
		try {
			rs1 = statement.executeQuery(sSql);
			if (rs1.next()) {
				result = new StgVersioningRecord();
				result.read(rs1);
			}
		} finally {
			SqlUtil.closeInstance(rs1);
		}
		return result;
	}
	
	private Integer stgVersioningId = null;
	private Integer questionSrcDocId = null;
	private Integer clauseSrcDocId = null;
	private Integer ecfrSrcDocId = null;
	private Integer clauseVersionId = null;
	private String sqlScriptFileName;
	private Date updatedAt = null;
	
	public void read(ResultSet rs1) throws SQLException {
		this.stgVersioningId = rs1.getInt(FIELD_STG_VERSIONING_ID);
		this.questionSrcDocId = CommonUtils.toInteger(rs1.getObject(FIELD_QUESTION_SRC_DOC_ID));
		this.clauseSrcDocId = CommonUtils.toInteger(rs1.getInt(FIELD_CLAUSE_SRC_DOC_ID));
		this.ecfrSrcDocId = CommonUtils.toInteger(rs1.getInt(FIELD_ECFR_SRC_DOC_ID));
		this.clauseVersionId = CommonUtils.toInteger(rs1.getInt(FIELD_CLAUSE_VERSION_ID));
		this.updatedAt = CommonUtils.toDateTime(rs1, FIELD_UPDATED_AT);
	}
	
	public void generateVersioningId(Connection conn) throws SQLException {
		if (this.stgVersioningId == null) {
			String sSql = "INSERT INTO " + TABLE_STG_VERSIONING +
				" (" + FIELD_QUESTION_SRC_DOC_ID + 
				", " + FIELD_CLAUSE_SRC_DOC_ID +
				", " + FIELD_ECFR_SRC_DOC_ID + 
				", " + FIELD_CLAUSE_VERSION_ID + ")" +
				" VALUES (?, ?, ?, ?)";
			PreparedStatement ps1 = null;
			ResultSet rs1 = null;
			try {
				ps1 = conn.prepareStatement(sSql, Statement.RETURN_GENERATED_KEYS);
				ps1.setInt(1, this.questionSrcDocId); // Question_Src_Doc_Id
				ps1.setInt(2, this.clauseSrcDocId); // Clause_Src_Doc_Id, 
				ps1.setInt(3, this.ecfrSrcDocId); // EFCR_Src_Doc_Id, 
				ps1.setInt(4, this.clauseVersionId); // Clause_Version_Id
				ps1.execute();
				rs1 = ps1.getGeneratedKeys();
				if (rs1.next()) {
					this.stgVersioningId = rs1.getInt(1);
				}
			} finally {
				SqlUtil.closeInstance(rs1, ps1);
			}
		}
	}
	
	// ====================================================
	public Integer getStgVersioningId() {
		return stgVersioningId;
	}

	public Integer getQuestionSrcDocId() {
		return questionSrcDocId;
	}
	public void setQuestionSrcDocId(Integer questionSrcDocId) {
		this.questionSrcDocId = questionSrcDocId;
	}

	public Integer getClauseSrcDocId() {
		return clauseSrcDocId;
	}
	public void setClauseSrcDocId(Integer clauseSrcDocId) {
		this.clauseSrcDocId = clauseSrcDocId;
	}

	public Integer getEcfrSrcDocId() {
		return ecfrSrcDocId;
	}
	public void settEcfrSrcDocId(Integer ecfrSrcDocId) {
		this.ecfrSrcDocId = ecfrSrcDocId;
	}

	public Integer getClauseVersionId() {
		return clauseVersionId;
	}
	public void setClauseVersionId(Integer clauseVersionId) {
		this.clauseVersionId = clauseVersionId;
	}

	public String getSqlScriptFileName() {
		return sqlScriptFileName;
	}
	public void setSqlScriptFileName(String sqlScriptFileName) {
		this.sqlScriptFileName = sqlScriptFileName;
	}

	public Date getUpdatedAt() {
		return updatedAt;
	}

	// ================================================================================
	public static StgVersioningRecord getLast(Connection conn, Integer pClauseVersionId) throws SQLException {
		StgVersioningRecord result = null;
		Statement statement = null;
		try {
			statement = conn.createStatement();
			result = StgVersioningRecord.getLast(statement, pClauseVersionId);
			if (result == null) {
				StgVersioningRecord.createRecord(conn, pClauseVersionId);
				result = StgVersioningRecord.getLast(statement, pClauseVersionId);
			}
		}
		finally {
			SqlUtil.closeInstance(statement);
		}
		return result;
	}
	
	public static StgVersioningRecord getLast(Statement statement, Integer pClauseVersionId) throws SQLException {
		StgVersioningRecord result = null;
		ResultSet rs1 = null;
		String sSql = "SELECT * FROM " + TABLE_STG_VERSIONING +
				" WHERE " + FIELD_STG_VERSIONING_ID + " = " +
				" (SELECT MAX(" + FIELD_STG_VERSIONING_ID + ") FROM " + TABLE_STG_VERSIONING +
				((pClauseVersionId == null) ? "" : " WHERE " + FIELD_CLAUSE_VERSION_ID + " = " + pClauseVersionId) +
				")"; 
		try {
			rs1 = statement.executeQuery(sSql);
			if (rs1.next()) {
				result = new StgVersioningRecord();
				result.read(rs1);
			}
		}
		finally {
			SqlUtil.closeInstance(rs1);
		}
		return result;
	}
	
	public static void createRecord(Connection conn, Integer pClauseVersionId) throws SQLException {
		Statement statement = null;
		String sSql = "INSERT INTO " + TABLE_STG_VERSIONING +
				" (" + FIELD_CLAUSE_VERSION_ID + ") VALUES (" +
				((pClauseVersionId == null) ? "NULL" : pClauseVersionId) + ")"; 
		try {
			statement = conn.createStatement();
			statement.execute(sSql);
		}
		finally {
			SqlUtil.closeInstance(statement);
		}
	}
	
	public static void save(Connection conn, StgSrcDocumentSet oStgSrcDocumentSet, Integer iClauseVersionId, String sFileName) 
			throws SQLException 
	{
		StgVersioningRecord oRecord = getLast(conn, iClauseVersionId);
		String sSql = "UPDATE " + TABLE_STG_VERSIONING 
				+ " SET " + FIELD_QUESTION_SRC_DOC_ID + " = ?"
				+ ", " + FIELD_CLAUSE_SRC_DOC_ID + " = ?"
				+ ", " + FIELD_ECFR_SRC_DOC_ID + " = ?"
				+ ", " + FIELD_CLAUSE_VERSION_ID + " = ?"
				+ ", " + FIELD_SQL_SCRIPT_FILE_NAME + " = ?"
				+ " WHERE " + FIELD_STG_VERSIONING_ID + " = ?";
		PreparedStatement ps = null;
		try {
			ps = conn.prepareStatement(sSql);
			ps.setInt(1, oStgSrcDocumentSet.iQuestionSrcDocId);
			ps.setInt(2, oStgSrcDocumentSet.iClauseSrcDocId);
			ps.setInt(3, oStgSrcDocumentSet.iEfcrSrcDocId);
			ps.setInt(4, iClauseVersionId);
			ps.setString(5, sFileName);
			ps.setInt(6, oRecord.stgVersioningId);
			ps.execute();
		}
		finally {
			SqlUtil.closeInstance(ps);
		}
	}
	
}
