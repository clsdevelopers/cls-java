package hgs.cpm.db;

import java.util.ArrayList;

import gov.dod.cls.db.ClauseRecord;
import gov.dod.cls.db.ClauseVersionChangeRecord;
import gov.dod.cls.db.DocumentAnswerRecord;
import gov.dod.cls.db.PrescriptionTable;
import gov.dod.cls.db.QuestionChoicePrescriptionRecord;
import gov.dod.cls.db.QuestionChoiceRecord;
import gov.dod.cls.db.QuestionConditionRecord;
import gov.dod.cls.db.QuestionRecord;
import gov.dod.cls.db.QuestionTable;
import gov.dod.cls.utils.CommonUtils;
import hgs.cpm.utils.SqlCommons;

class ChoiceChangeRecord {

	private boolean bChoice;
	private boolean bPrescription;

	public void init() {
		bChoice = false;
		bPrescription = false;
	}

	public void setChoice(boolean b ) {
		this.bChoice = b;
	}
	public boolean getChoice() {
		return this.bChoice;
	}

	public void setPrescription(boolean b ) {
		this.bPrescription = b;
	}
	
	public boolean getPrescription() {
		return this.bPrescription;
	}
	
	
}

public class ParsedQuestionRecord {

	public Integer Stg2QuestionId;
	public String questionCode;
	public String questionType;
	public String questionText;
	public String questionGroup;
	
	public Integer sequence;
	public String originalConditionText; // q.rule
	public String conditionToQuestion; // q.Normal_Rule\n" +
	
	public Integer parentStg2QuestionId;
	public String parentQuestionName;
	
	public boolean defaultBaseline = false; // CJ-584
	
	public ParsedQuestionChoiceSet choices = new ParsedQuestionChoiceSet();
	
	public ArrayList<ParsedQuestionRecord> children = null;
	public ParsedQuestionRecord parent = null;
	
	public void addChild(ParsedQuestionRecord oChild) {
		if ((oChild != null) && (oChild != this)) {
			if (this.children == null)
				this.children = new ArrayList<ParsedQuestionRecord>();
			this.children.add(oChild);
		}
		oChild.parent = this;
	}
	
	public ParsedQuestionRecord getRootParent() {
		if (this.parent != null)
			return this.parent.getRootParent();
		else
			return this;
	}
	
	public String getNameText() {
		return this.questionCode.substring(1, this.questionCode.length() - 1);
	}
	
	public boolean hasChoice(String psChoice) {
		return this.choices.hasChoice(psChoice);
	}
	
	public boolean containChoice(String psChoice) {
		if (this.hasChoice(psChoice))
			return true;
		if (this.children != null) {
			for (ParsedQuestionRecord oChild : this.children) {
				if (oChild.containChoice(psChoice))
					return true;
			}
		}
		return false;
	};
	
	public boolean isNumberType() {
		return "N".equals(this.questionType);
	};
	
	public String getInsertSql(int iClauseVersionId, ParsedQuestionChoiceSet oQuestionChoiceSet, ParsedPrescriptionSet parsedPrescriptionSet,
			ClauseVersionChangeRecord oClauseVersionChangeRecord) {
		String sSql = "INSERT INTO " + QuestionRecord.TABLE_QUESTIONS +
				//public static final String FIELD_QUESTION_ID = "question_id";
				" (" + QuestionRecord.FIELD_CLAUSE_VERSION_ID +
				", " + QuestionRecord.FIELD_QUESTION_ID +
				", " + QuestionRecord.FIELD_QUESTION_CODE +
				", " + QuestionRecord.FIELD_QUESTION_TYPE +
				", " + QuestionRecord.FIELD_QUESTION_GROUP +
				", " + QuestionRecord.FIELD_QUESTION_TEXT +
				")\nVALUES (" + iClauseVersionId +
				", " + this.Stg2QuestionId +
				", " + SqlCommons.quoteString(this.questionCode) +
				", " + SqlCommons.quoteString(this.questionType) +
				", " + SqlCommons.quoteString(this.questionGroup) +
				", " + SqlCommons.quoteString(this.questionText) +
				");";
		
		sSql += "\nINSERT INTO " + QuestionConditionRecord.TABLE_QUESTION_CONDITIONS
				+ " (" + QuestionConditionRecord.FIELD_QUESTION_CONDITION_SEQUENCE
				+ ", " + QuestionConditionRecord.FIELD_QUESTION_ID
				+ ", " + QuestionConditionRecord.FIELD_PARENT_QUESTION_ID
				+ ", " + QuestionConditionRecord.FIELD_DEFAULT_BASELINE // ================
				+ ", " + QuestionConditionRecord.FIELD_ORIGINAL_CONDITION_TEXT
				+ ", " + QuestionConditionRecord.FIELD_CONDITION_TO_QUESTION + ")"
				+ "\nVALUES (" + this.sequence
				+ ", " + this.Stg2QuestionId
				+ ", " + ((this.parentStg2QuestionId == null) ? "NULL" : this.parentStg2QuestionId)
				+ ", " + SqlCommons.quoteString(this.defaultBaseline)
				+ ((this.originalConditionText == null) ? ", NULL" : ("\n, " + SqlCommons.quoteString(this.originalConditionText)))
				+ ((this.conditionToQuestion == null) ? ", NULL" : ("\n, " + SqlCommons.quoteString(this.conditionToQuestion)))
				+ ");";
		
		String sChoceSql = oQuestionChoiceSet.genSql(null, parsedPrescriptionSet, this, this.Stg2QuestionId, null);
		if (sChoceSql != null)
			sSql += sChoceSql;
		
		if (oClauseVersionChangeRecord != null) {
			oClauseVersionChangeRecord.setQuestion(this.questionCode);
			oClauseVersionChangeRecord.setAdded();
		}
		
		return sSql;
	}
	
	// Populate question records using code/version filter, as opposed to ids.
		public String getInsertSql_wFilter(int iClauseVersionId, ParsedQuestionChoiceSet oQuestionChoiceSet, ParsedPrescriptionSet parsedPrescriptionSet,
				ClauseVersionChangeRecord oClauseVersionChangeRecord) {
			
			String sParent = "NULL ";
			if (this.parentStg2QuestionId != null) {
				sParent = "(SELECT " + QuestionRecord.FIELD_QUESTION_ID  
						+ " FROM " + QuestionRecord.TABLE_QUESTIONS
						+ " WHERE " + QuestionRecord.FIELD_CLAUSE_VERSION_ID + " = " + iClauseVersionId
						+ " AND " + QuestionRecord.FIELD_QUESTION_CODE + " = " + SqlCommons.quoteString(this.parentQuestionName) + ") ";
			}
			
			String sSql = "INSERT INTO " + QuestionRecord.TABLE_QUESTIONS +
					" (" + QuestionRecord.FIELD_CLAUSE_VERSION_ID +
					", " + QuestionRecord.FIELD_QUESTION_CODE +
					", " + QuestionRecord.FIELD_QUESTION_TYPE +
					", " + QuestionRecord.FIELD_QUESTION_GROUP +
					", " + QuestionRecord.FIELD_QUESTION_TEXT + ")" +
					"\nVALUES (" + iClauseVersionId +
					", " + SqlCommons.quoteString(this.questionCode) +
					", " + SqlCommons.quoteString(this.questionType) +
					", " + SqlCommons.quoteString(this.questionGroup) +
					", " + SqlCommons.quoteString(this.questionText) + ");";
			
			sSql += "\nINSERT INTO " + QuestionConditionRecord.TABLE_QUESTION_CONDITIONS
					+ " (" + QuestionConditionRecord.FIELD_QUESTION_CONDITION_SEQUENCE
					+ ", " + QuestionConditionRecord.FIELD_QUESTION_ID
					+ ", " + QuestionConditionRecord.FIELD_PARENT_QUESTION_ID
					+ ", " + QuestionConditionRecord.FIELD_DEFAULT_BASELINE // ================
					+ ", " + QuestionConditionRecord.FIELD_ORIGINAL_CONDITION_TEXT
					+ ", " + QuestionConditionRecord.FIELD_CONDITION_TO_QUESTION + ")"
					+ "\nSELECT " + this.sequence
					+ ", " + QuestionRecord.FIELD_QUESTION_ID + ", " + sParent 
					+ ", " + SqlCommons.quoteString(this.defaultBaseline)
					+ ((this.originalConditionText == null) ? ", NULL" : ("\n, " + SqlCommons.quoteString(this.originalConditionText)))
					+ ((this.conditionToQuestion == null) ? ", NULL" : ("\n, " + SqlCommons.quoteString(this.conditionToQuestion)))
					+ " FROM " + QuestionRecord.TABLE_QUESTIONS 
					+ " WHERE " + QuestionRecord.FIELD_CLAUSE_VERSION_ID + " = " + iClauseVersionId
					+ " AND " + QuestionRecord.FIELD_QUESTION_CODE + " = " + SqlCommons.quoteString(this.questionCode) + ";";
			
			/*
			if (this.parentStg2QuestionId != null) {
				
				sSql += "\nUPDATE " + QuestionConditionRecord.TABLE_QUESTION_CONDITIONS + " QC, " + QuestionRecord.TABLE_QUESTIONS + " Q"
					+ " SET " + QuestionConditionRecord.FIELD_PARENT_QUESTION_ID 
					+ " = Q."  + QuestionRecord.FIELD_QUESTION_ID 
					+ "\nWHERE " + QuestionRecord.FIELD_CLAUSE_VERSION_ID + " = " + iClauseVersionId
					+ " AND " + QuestionRecord.FIELD_QUESTION_CODE + " = " + SqlCommons.quoteString(this.parentQuestionName) + ";";

			}*/
			
			String sChoceSql = oQuestionChoiceSet.genSql_wFilter(null, parsedPrescriptionSet, this, this.Stg2QuestionId, null, iClauseVersionId);
			if (sChoceSql != null)
				sSql += sChoceSql;
			
			if (oClauseVersionChangeRecord != null) {
				oClauseVersionChangeRecord.setQuestion(this.questionCode);
				oClauseVersionChangeRecord.setAdded();
			}
			
			return sSql;
		}
		
	private String genTr(String psNew, String psPrevious, String psField,
			ClauseVersionChangeRecord oClauseVersionChangeRecord) {
		if (CommonUtils.isNotSame(psNew, psPrevious)) {
			oClauseVersionChangeRecord.addChangeBrief(psField);
			return "\n<tr><td rowspan='2'>" + psField + "</th><td>new</td><td>" + psNew + "</td></tr>" +
						"\n<tr><td>Previous</td><td>" + psPrevious + "</td></tr>";
		} else
			return "";
	}
	
	public String getDifferences(int iClauseVersionId, ParsedQuestionChoiceSet oQuestionChoiceSet, ParsedPrescriptionSet parsedPrescriptionSet,
			ClauseVersionChangeRecord oClauseVersionChangeRecord) {
		String sHtml = "\n<table><tr><th>Field</th><th>New</th></tr>" +
				//"\n<tr><td>" + QuestionRecord.FIELD_CLAUSE_VERSION_ID + "</td><td>" + iClauseVersionId + "</td></tr>" + 
				"\n<tr><td>" + QuestionRecord.FIELD_QUESTION_ID + "</td><td>" + this.Stg2QuestionId + "</td></tr>" +
				"\n<tr><td>" + QuestionRecord.FIELD_QUESTION_CODE + "</td><td>" + this.questionCode + "</td></tr>" +
				"\n<tr><td>" + QuestionRecord.FIELD_QUESTION_TYPE + "</td><td>" + this.questionType + "</td></tr>" +
				"\n<tr><td>" + QuestionRecord.FIELD_QUESTION_GROUP + "</td><td>" + this.questionGroup + "</td></tr>" +
				"\n<tr><td>" + QuestionRecord.FIELD_QUESTION_TEXT + "</td><td>" + this.questionText + "</td></tr>" +
				"\n<tr><td>" + QuestionConditionRecord.FIELD_QUESTION_CONDITION_SEQUENCE + "</td><td>" + this.sequence + "</td></tr>" +
				"\n<tr><td>" + QuestionConditionRecord.FIELD_PARENT_QUESTION_ID + "</td><td>" + ((this.parentStg2QuestionId == null) ? "" : this.parentStg2QuestionId) + "</td></tr>" +
				"\n<tr><td>" + QuestionConditionRecord.FIELD_ORIGINAL_CONDITION_TEXT + "</td><td>" + ((this.originalConditionText == null) ? "" : this.originalConditionText) + "</td></tr>" +
				"\n<tr><td>" + QuestionConditionRecord.FIELD_CONDITION_TO_QUESTION + "</td><td>" + ((this.conditionToQuestion == null) ? "" : this.conditionToQuestion) + "</td></tr>" +
				"\n<tr><td>" + QuestionConditionRecord.FIELD_DEFAULT_BASELINE + "</td><td>" + this.defaultBaseline + "</td></tr>" +
				"\n</table>";
		
		sHtml += oQuestionChoiceSet.getDifferences(null, parsedPrescriptionSet);
		
		oClauseVersionChangeRecord.setQuestion(this.questionCode);
		oClauseVersionChangeRecord.setAdded();
		
		return "\n<h2>" + this.questionCode + " (New)</h2>" + sHtml;
	}
	
	public static String getDeleteHtml(QuestionRecord oQuestionRecord, QuestionConditionRecord oQuestionConditionRecord,
			ClauseVersionChangeRecord oClauseVersionChangeRecord) {
		String sHtml = "\n<table><tr><th>Field</th><th>New</th></tr>" +
				"\n<tr><td>" + QuestionRecord.FIELD_CLAUSE_VERSION_ID + "</td><td>" + oQuestionRecord.getClauseVersionId() + "</td></tr>" + 
				"\n<tr><td>" + QuestionRecord.FIELD_QUESTION_ID + "</td><td>" + oQuestionRecord.getId() + "</td></tr>" +
				"\n<tr><td>" + QuestionRecord.FIELD_QUESTION_CODE + "</td><td>" + oQuestionRecord.getQuestionCode() + "</td></tr>" +
				"\n<tr><td>" + QuestionRecord.FIELD_QUESTION_TYPE + "</td><td>" + oQuestionRecord.getQuestionType() + "</td></tr>" +
				"\n<tr><td>" + QuestionRecord.FIELD_QUESTION_GROUP + "</td><td>" + oQuestionRecord.getQuestionGroup() + "</td></tr>" +
				"\n<tr><td>" + QuestionRecord.FIELD_QUESTION_TEXT + "</td><td>" + oQuestionRecord.getQuestionText() + "</td></tr>" +
				"\n<tr><td>" + QuestionConditionRecord.FIELD_QUESTION_CONDITION_SEQUENCE + "</td><td>" + oQuestionConditionRecord.getSequence() + "</td></tr>" +
				"\n<tr><td>" + QuestionConditionRecord.FIELD_PARENT_QUESTION_ID + "</td><td>" + ((oQuestionConditionRecord.getParentQuestionId() == null) ? "" : oQuestionConditionRecord.getParentQuestionId()) + "</td></tr>" +
				"\n<tr><td>" + QuestionConditionRecord.FIELD_ORIGINAL_CONDITION_TEXT + "</td><td>" + ((oQuestionConditionRecord.getOriginalConditionText() == null) ? "" : oQuestionConditionRecord.getOriginalConditionText()) + "</td></tr>" +
				"\n<tr><td>" + QuestionConditionRecord.FIELD_CONDITION_TO_QUESTION + "</td><td>" + ((oQuestionConditionRecord.getConditionToQuestion() == null) ? "" : oQuestionConditionRecord.getConditionToQuestion()) + "</td></tr>" +
				"\n<tr><td>" + QuestionConditionRecord.FIELD_DEFAULT_BASELINE + "</td><td>" + oQuestionConditionRecord.getDefaultBaseline() + "</td></tr>" +
				"\n</table>";
		
		//sHtml += oQuestionChoiceSet.getDifferences(null, parsedPrescriptionSet);
		oClauseVersionChangeRecord.setQuestion(oQuestionRecord.getQuestionCode());
		oClauseVersionChangeRecord.setRemoved();
		
		return "\n<h2>" + oQuestionRecord.getQuestionCode() + " (Remove)</h2>" + sHtml;
	}

	public static String getDeleteSql(QuestionRecord oQuestionRecord, QuestionConditionRecord oQuestionConditionRecord,
			ClauseVersionChangeRecord oClauseVersionChangeRecord) {
		Integer iId = oQuestionRecord.getId();
		String sSql = "\n-- " + oQuestionRecord.getQuestionCode() +
				"\nDELETE FROM " + QuestionChoicePrescriptionRecord.TABLE_QUESTION_CHOICE_PRESCRIPTIONS + "\n" +
				"WHERE Question_Choce_Id in\n" +
				"(SELECT Question_Choce_Id" +
				" FROM " + QuestionChoiceRecord.TABLE_QUESTION_CHOICES +
				" WHERE " + QuestionRecord.FIELD_QUESTION_ID + " = " + iId + ");";
		
		sSql += "\nDELETE FROM " + QuestionChoiceRecord.TABLE_QUESTION_CHOICES +
				" WHERE " + QuestionChoiceRecord.FIELD_QUESTION_ID + " = " + iId + ";";

		sSql += "\nDELETE FROM " + QuestionConditionRecord.TABLE_QUESTION_CONDITIONS +
				" WHERE " + QuestionConditionRecord.FIELD_QUESTION_ID + " = " + iId + ";";

		sSql += "\nDELETE FROM " + QuestionRecord.TABLE_QUESTIONS +
				" WHERE " + QuestionRecord.FIELD_QUESTION_ID + " = " + iId + ";";

		oClauseVersionChangeRecord.setQuestion(oQuestionRecord.getQuestionCode());
		oClauseVersionChangeRecord.setRemoved();
		return sSql;
	}
	
	public static String getDeleteSql_wFilter(QuestionRecord oQuestionRecord, QuestionConditionRecord oQuestionConditionRecord,
			ClauseVersionChangeRecord oClauseVersionChangeRecord, int iClauseVersionId) {
		String sSql = "\n-- " + oQuestionRecord.getQuestionCode() +
				"\nDELETE FROM " + QuestionChoicePrescriptionRecord.TABLE_QUESTION_CHOICE_PRESCRIPTIONS + "\n" +
				"WHERE Question_Choce_Id in\n" +
				" (SELECT Question_Choce_Id" +
				" FROM " + QuestionChoiceRecord.TABLE_QUESTION_CHOICES +
				" WHERE " + QuestionRecord.FIELD_QUESTION_ID + " IN " + "\n" +
				" (SELECT " + QuestionRecord.FIELD_QUESTION_ID + " FROM " +  QuestionRecord.TABLE_QUESTIONS + 
				" WHERE " + QuestionRecord.FIELD_CLAUSE_VERSION_ID + " = " + iClauseVersionId + 
				" AND " + QuestionRecord.FIELD_QUESTION_CODE + " = " + SqlCommons.quoteString(oQuestionRecord.getQuestionCode()) + ")); ";
		
		sSql += "\nDELETE FROM " + QuestionChoiceRecord.TABLE_QUESTION_CHOICES +
				" WHERE " + QuestionRecord.FIELD_QUESTION_ID + " IN " + "\n" +
				" (SELECT " + QuestionRecord.FIELD_QUESTION_ID + " FROM " +  QuestionRecord.TABLE_QUESTIONS + 
				" WHERE " + QuestionRecord.FIELD_CLAUSE_VERSION_ID + " = " + iClauseVersionId + 
				" AND " + QuestionRecord.FIELD_QUESTION_CODE + " = " + SqlCommons.quoteString(oQuestionRecord.getQuestionCode()) + "); ";

		sSql += "\nDELETE FROM " + QuestionConditionRecord.TABLE_QUESTION_CONDITIONS +
				" WHERE " + QuestionRecord.FIELD_QUESTION_ID + " IN " + "\n" +
				" (SELECT " + QuestionRecord.FIELD_QUESTION_ID + " FROM " +  QuestionRecord.TABLE_QUESTIONS + 
				" WHERE " + QuestionRecord.FIELD_CLAUSE_VERSION_ID + " = " + iClauseVersionId + 
				" AND " + QuestionRecord.FIELD_QUESTION_CODE + " = " + SqlCommons.quoteString(oQuestionRecord.getQuestionCode()) + "); ";

		sSql += "\nDELETE FROM " + DocumentAnswerRecord.TABLE_DOCUMENT_ANSWERS +
				" WHERE " + QuestionRecord.FIELD_QUESTION_ID + " IN " + "\n" +
				" (SELECT " + QuestionRecord.FIELD_QUESTION_ID + " FROM " +  QuestionRecord.TABLE_QUESTIONS + 
				" WHERE " + QuestionRecord.FIELD_CLAUSE_VERSION_ID + " = " + iClauseVersionId + 
				" AND " + QuestionRecord.FIELD_QUESTION_CODE + " = " + SqlCommons.quoteString(oQuestionRecord.getQuestionCode()) + "); ";

		sSql += "\nDELETE FROM " + QuestionRecord.TABLE_QUESTIONS + "\n" +
				" WHERE " + QuestionRecord.FIELD_CLAUSE_VERSION_ID + " = " + iClauseVersionId + 
				" AND " + QuestionRecord.FIELD_QUESTION_CODE + " = " + SqlCommons.quoteString(oQuestionRecord.getQuestionCode()) + "; ";

		oClauseVersionChangeRecord.setQuestion(oQuestionRecord.getQuestionCode());
		oClauseVersionChangeRecord.setRemoved();
		return sSql;
	}
	
	private static String booleanSqlValue(boolean pbValue) {
		return (pbValue ? "1" : "0");
	}
	
	public String getDifferences(
			QuestionRecord oQuestionRecord, 
			QuestionConditionRecord oQuestionConditionRecord,
			ParsedQuestionChoiceSet oQuestionChoiceSet,
			ParsedPrescriptionSet parsedPrescriptionSet,
			QuestionTable questionTable,
			int iClauseVersionId,
			ClauseVersionChangeRecord oClauseVersionChangeRecord,
			PrescriptionTable poPrescriptionTable) {
		
		String sHtml = ""; // "\n[" + this.questionCode + "]\n<table><tr><th>Field</th><th>New</th><th>Previous</th></tr>";
		
		sHtml += genTr(this.questionType, oQuestionRecord.getQuestionType(), QuestionRecord.FIELD_QUESTION_TYPE, oClauseVersionChangeRecord);
		sHtml += genTr(this.questionGroup, oQuestionRecord.getQuestionGroup(), QuestionRecord.FIELD_QUESTION_GROUP, oClauseVersionChangeRecord);
		sHtml += genTr(this.questionText, oQuestionRecord.getQuestionText(), QuestionRecord.FIELD_QUESTION_TEXT, oClauseVersionChangeRecord);

		//if (!this.sequence.equals(oQuestionConditionRecord.getSequence()))
		//	sHtml += "\n<tr><th>" + QuestionConditionRecord.FIELD_QUESTION_CONDITION_SEQUENCE + "</th><td>" + this.sequence + "</td><td>" + oQuestionConditionRecord.getSequence() + "</td></tr>"; 
		sHtml += genTr(this.originalConditionText, oQuestionConditionRecord.getOriginalConditionText(), QuestionConditionRecord.FIELD_ORIGINAL_CONDITION_TEXT, oClauseVersionChangeRecord);
		sHtml += genTr(this.conditionToQuestion, oQuestionConditionRecord.getConditionToQuestion(), QuestionConditionRecord.FIELD_CONDITION_TO_QUESTION, oClauseVersionChangeRecord);
		
		sHtml += genTr(booleanSqlValue(this.defaultBaseline), booleanSqlValue(oQuestionConditionRecord.getDefaultBaseline()), QuestionConditionRecord.FIELD_DEFAULT_BASELINE, oClauseVersionChangeRecord);
		
		//if (CommonUtils.isNotSame(oQuestionConditionRecord.getParentQuestionCode(), this.parentQuestionName))
		
		if (!sHtml.isEmpty())
			sHtml = "\n<table><tr><th>Field</th><th>New</th><th>Previous</th></tr>" +
					sHtml +
					"\n</table>";
		
		String sChoiceDiff = oQuestionChoiceSet.getDifferences(oQuestionRecord.getQuestionChoiceTable(), parsedPrescriptionSet);
		if (CommonUtils.isNotEmpty(sChoiceDiff)) {
			oClauseVersionChangeRecord.addChangeBrief("Choices");
			sHtml += sChoiceDiff;
		}
		
		if (!sHtml.isEmpty()) {
			oClauseVersionChangeRecord.setQuestion(this.questionCode);
			oClauseVersionChangeRecord.setChanged();
			return "\n<h2>" + this.questionCode + "</h2>" + sHtml;
		} else
			return "";
	}

	public String getUpdateSql(
			QuestionRecord oQuestionRecord, 
			QuestionConditionRecord oQuestionConditionRecord,
			ParsedQuestionChoiceSet oQuestionChoiceSet,
			ParsedPrescriptionSet parsedPrescriptionSet,
			QuestionTable questionTable,
			int iClauseVersionId,
			ClauseVersionChangeRecord oClauseVersionChangeRecord,
			PrescriptionTable poPrescriptionTable) {
		String sSql = null;
		
		if (!this.questionType.equals(oQuestionRecord.getQuestionType())) {
			sSql = ((sSql == null) ? "\nSET " : sSql + "\n, ") + QuestionRecord.FIELD_QUESTION_TYPE + " = " + SqlCommons.quoteString(this.questionType)
					+ " -- " + oQuestionRecord.getQuestionType(); 
			oClauseVersionChangeRecord.addChangeBrief("Type");
		}
		if (!this.questionGroup.equals(oQuestionRecord.getQuestionGroup())) {
			sSql = ((sSql == null) ? "\nSET " : sSql + "\n, ") + QuestionRecord.FIELD_QUESTION_GROUP + " = " + SqlCommons.quoteString(this.questionGroup)
					+ " -- " + oQuestionRecord.getQuestionGroup(); 
			oClauseVersionChangeRecord.addChangeBrief("Group");
		}
		//String sSqlValue = SqlCommons.quoteString(this.questionText);
		if (!this.questionText.equals(oQuestionRecord.getQuestionText())) {
			sSql = ((sSql == null) ? "\nSET " : sSql + "\n, ") + QuestionRecord.FIELD_QUESTION_TEXT + " = " + SqlCommons.quoteString(this.questionText)
					+ " -- " + SqlCommons.quoteString(oQuestionRecord.getQuestionText()); 
			oClauseVersionChangeRecord.addChangeBrief("Text");
		}
		
		if (sSql != null)
			sSql = "UPDATE " + QuestionRecord.TABLE_QUESTIONS + 
					sSql +
					"\nWHERE " + QuestionRecord.FIELD_QUESTION_ID + " = " + oQuestionRecord.getId() + "; -- " + this.questionCode;
		
		String sItems = null;
		if (!this.sequence.equals(oQuestionConditionRecord.getSequence()))
			sItems = "\nSET " + QuestionConditionRecord.FIELD_QUESTION_CONDITION_SEQUENCE + " = " + this.sequence
					+ " -- " + oQuestionConditionRecord.getSequence(); 
		if (CommonUtils.isNotSame(oQuestionConditionRecord.getOriginalConditionText(), this.originalConditionText)) {
			sItems = ((sItems == null) ? "\nSET " : sItems + "\n, ") + QuestionConditionRecord.FIELD_ORIGINAL_CONDITION_TEXT + " = " + SqlCommons.quoteString(this.originalConditionText)
					+ " -- " + SqlCommons.quoteString(oQuestionConditionRecord.getOriginalConditionText());
		}
		if (CommonUtils.isNotSame(oQuestionConditionRecord.getConditionToQuestion(), this.conditionToQuestion)) {
			sItems = ((sItems == null) ? "\nSET " : sItems + "\n, ") + QuestionConditionRecord.FIELD_CONDITION_TO_QUESTION + " = " + SqlCommons.quoteString(this.conditionToQuestion)
					+ " -- " + SqlCommons.quoteString(oQuestionConditionRecord.getConditionToQuestion());
			oClauseVersionChangeRecord.addChangeBrief("Condition");
		}
		if (oQuestionConditionRecord.getDefaultBaseline() != this.defaultBaseline) {
			sItems = ((sItems == null) ? "\nSET " : sItems + "\n, ") + QuestionConditionRecord.FIELD_DEFAULT_BASELINE + " = " + booleanSqlValue(this.defaultBaseline);
			oClauseVersionChangeRecord.addChangeBrief("Default Baseline");
		}
		if (CommonUtils.isNotSame(oQuestionConditionRecord.getParentQuestionCode(), this.parentQuestionName)) {
			String sParentId = null;
			if (this.parentStg2QuestionId == null) {
				sParentId = "NULL";
			} else {
				QuestionRecord parentQuestionRecord = questionTable.findByCode(this.parentQuestionName);
				if (parentQuestionRecord == null) {
					System.out.println("Unable to locate " + this.parentQuestionName);
				} else {
					sParentId = parentQuestionRecord.getId().toString();
				}
			}
			if (sParentId != null) {
				sItems = ((sItems == null) ? "\nSET " : sItems + "\n, ") + QuestionConditionRecord.FIELD_PARENT_QUESTION_ID + " = " + sParentId
						+ " -- " + this.parentQuestionName; 
			}
		}
			
		if (sItems != null)
			sSql = (sSql == null ? "" : sSql + "\n") +
					"UPDATE " + QuestionConditionRecord.TABLE_QUESTION_CONDITIONS + 
					sItems +
					"\nWHERE " + QuestionConditionRecord.FIELD_QUESTION_CONDITION_ID + " = " + oQuestionConditionRecord.getId() + "; -- " + this.questionCode;
		
		String sChoceSql = oQuestionChoiceSet.genSql(oQuestionRecord.getQuestionChoiceTable(), parsedPrescriptionSet, 
				this, oQuestionRecord.getId(), poPrescriptionTable);
		if (CommonUtils.isNotEmpty(sChoceSql)) {
			sSql = (sSql == null ? "" : sSql) + sChoceSql;
			oClauseVersionChangeRecord.addChangeBrief("Choices");
		}
		
		if (sSql != null) {
			oClauseVersionChangeRecord.setQuestion(this.questionCode);
			oClauseVersionChangeRecord.setChanged();
		}
		
		return sSql;
	}
	
	public String getUpdateSql_wFilter(
			QuestionRecord oQuestionRecord, 
			QuestionConditionRecord oQuestionConditionRecord,
			ParsedQuestionChoiceSet oQuestionChoiceSet,
			ParsedPrescriptionSet parsedPrescriptionSet,
			QuestionTable questionTable,
			int iClauseVersionId,
			ClauseVersionChangeRecord oClauseVersionChangeRecord,
			PrescriptionTable poPrescriptionTable) {
		String sSql = null;
		
		if (!this.questionType.equals(oQuestionRecord.getQuestionType())) {
			sSql = ((sSql == null) ? "\nSET " : sSql + "\n, ") + QuestionRecord.FIELD_QUESTION_TYPE + " = " + SqlCommons.quoteString(this.questionType)
					+ " -- " + oQuestionRecord.getQuestionType(); 
			oClauseVersionChangeRecord.addChangeBrief("Type");
		}
		if (!this.questionGroup.equals(oQuestionRecord.getQuestionGroup())) {
			sSql = ((sSql == null) ? "\nSET " : sSql + "\n, ") + QuestionRecord.FIELD_QUESTION_GROUP + " = " + SqlCommons.quoteString(this.questionGroup)
					+ " -- " + oQuestionRecord.getQuestionGroup(); 
			oClauseVersionChangeRecord.addChangeBrief("Group");
		}
		//String sSqlValue = SqlCommons.quoteString(this.questionText);
		if (!this.questionText.equals(oQuestionRecord.getQuestionText())) {
			sSql = ((sSql == null) ? "\nSET " : sSql + "\n, ") + QuestionRecord.FIELD_QUESTION_TEXT + " = " + SqlCommons.quoteString(this.questionText)
					+ " -- " + SqlCommons.quoteString(oQuestionRecord.getQuestionText()); 
			oClauseVersionChangeRecord.addChangeBrief("Text");
		}
		
		if (sSql != null)
			sSql = "UPDATE " + QuestionRecord.TABLE_QUESTIONS + 
					sSql +
					// "\nWHERE " + QuestionRecord.FIELD_QUESTION_ID + " = " + oQuestionRecord.getId() + "; -- " + this.questionCode;
					 "\nWHERE " + QuestionRecord.FIELD_QUESTION_CODE + " = " + SqlCommons.quoteString(oQuestionRecord.getQuestionCode()) +
					" AND " + QuestionRecord.FIELD_CLAUSE_VERSION_ID + " = " + iClauseVersionId + "; -- " + this.questionCode;
					
		
		String sItems = null;
		if (!this.sequence.equals(oQuestionConditionRecord.getSequence()))
			sItems = "\nSET " + QuestionConditionRecord.FIELD_QUESTION_CONDITION_SEQUENCE + " = " + this.sequence
					+ " -- " + oQuestionConditionRecord.getSequence(); 
		if (CommonUtils.isNotSame(oQuestionConditionRecord.getOriginalConditionText(), this.originalConditionText)) {
			sItems = ((sItems == null) ? "\nSET " : sItems + "\n, ") + QuestionConditionRecord.FIELD_ORIGINAL_CONDITION_TEXT + " = " + SqlCommons.quoteString(this.originalConditionText)
					+ " -- " + SqlCommons.quoteString(oQuestionConditionRecord.getOriginalConditionText());
		}
		if (CommonUtils.isNotSame(oQuestionConditionRecord.getConditionToQuestion(), this.conditionToQuestion)) {
			sItems = ((sItems == null) ? "\nSET " : sItems + "\n, ") + QuestionConditionRecord.FIELD_CONDITION_TO_QUESTION + " = " + SqlCommons.quoteString(this.conditionToQuestion)
					+ " -- " + SqlCommons.quoteString(oQuestionConditionRecord.getConditionToQuestion());
			oClauseVersionChangeRecord.addChangeBrief("Condition");
		}
		if (oQuestionConditionRecord.getDefaultBaseline() != this.defaultBaseline) {
			sItems = ((sItems == null) ? "\nSET " : sItems + "\n, ") + QuestionConditionRecord.FIELD_DEFAULT_BASELINE + " = " + booleanSqlValue(this.defaultBaseline);
			oClauseVersionChangeRecord.addChangeBrief("Default Baseline");
		}
		if (CommonUtils.isNotSame(oQuestionConditionRecord.getParentQuestionCode(), this.parentQuestionName)) {

			String sParent = "NULL ";
			if (this.parentStg2QuestionId != null) {
				sParent = "(SELECT " + QuestionRecord.FIELD_QUESTION_ID  
						+ " FROM " + QuestionRecord.TABLE_QUESTIONS
						+ " WHERE " + QuestionRecord.FIELD_CLAUSE_VERSION_ID + " = " + iClauseVersionId
						+ " AND " + QuestionRecord.FIELD_QUESTION_CODE + " = " + SqlCommons.quoteString(this.parentQuestionName) + ") ";
			}
			sItems = ((sItems == null) ? "\nSET " : sItems + "\n, ") + QuestionConditionRecord.FIELD_PARENT_QUESTION_ID + " = " + sParent;
					
		}
			
		if (sItems != null)
			sSql = (sSql == null ? "" : sSql + "\n") +
					"UPDATE " + QuestionConditionRecord.TABLE_QUESTION_CONDITIONS + 
					sItems +					
					"\nWHERE " + QuestionRecord.FIELD_QUESTION_ID + " IN "+
					"\n  (SELECT " + QuestionRecord.FIELD_QUESTION_ID + " FROM " + QuestionRecord.TABLE_QUESTIONS +
					" WHERE " + QuestionRecord.FIELD_QUESTION_CODE + " = " + SqlCommons.quoteString(this.questionCode) + 
					" AND " + QuestionRecord.FIELD_CLAUSE_VERSION_ID + " = " + iClauseVersionId + ")"
					+ "; -- " + this.questionCode;
		
		String sChoceSql = oQuestionChoiceSet.genSql_wFilter(oQuestionRecord.getQuestionChoiceTable(), parsedPrescriptionSet, 
				this, oQuestionRecord.getId(), poPrescriptionTable, iClauseVersionId);
		if (CommonUtils.isNotEmpty(sChoceSql)) {
			sSql = (sSql == null ? "" : sSql) + sChoceSql;
			oClauseVersionChangeRecord.addChangeBrief("Choices");
		}
		
		if (sSql != null) {
			oClauseVersionChangeRecord.setQuestion(this.questionCode);
			oClauseVersionChangeRecord.setChanged();
		}
		
		return sSql;
	}
	
	public void checkSqlChanges(
			QuestionRecord oQuestionRecord, 
			QuestionConditionRecord oQuestionConditionRecord,
			ParsedQuestionChoiceSet oQuestionChoiceSet,
			ParsedPrescriptionSet parsedPrescriptionSet,
			QuestionTable questionTable,
			int iClauseVersionId,
			ClauseVersionChangeRecord oClauseVersionChangeRecord,
			PrescriptionTable poPrescriptionTable) {
		boolean bChanged = false;
		
		if (!this.questionType.equals(oQuestionRecord.getQuestionType())) {
			bChanged = true;
			oClauseVersionChangeRecord.addChangeBrief("Type");
		}
		if (!this.questionGroup.equals(oQuestionRecord.getQuestionGroup())) {
			bChanged = true;
			oClauseVersionChangeRecord.addChangeBrief("Group");
		}
		if (!this.questionText.equals(oQuestionRecord.getQuestionText())) {
			bChanged = true;
			oClauseVersionChangeRecord.addChangeBrief("Text");
		}
				
		if (oQuestionConditionRecord.getDefaultBaseline() != this.defaultBaseline) {
			bChanged = true;
			oClauseVersionChangeRecord.addChangeBrief("Default Baseline");
		}			

		if (CommonUtils.isNotSame(oQuestionConditionRecord.getConditionToQuestion(), this.conditionToQuestion)) {
			bChanged = true;
			oClauseVersionChangeRecord.addChangeBrief("Condition");
		}			
		
		ChoiceChangeRecord ccRecord = new ChoiceChangeRecord();
		ccRecord.init();
		
		if (oQuestionChoiceSet.checkSqlChanges(oQuestionRecord.getQuestionChoiceTable(), parsedPrescriptionSet, 
				this, oQuestionRecord.getId(), poPrescriptionTable, ccRecord)) {
			bChanged = true;
			if (ccRecord.getChoice())
				oClauseVersionChangeRecord.addChangeBrief("Choices");
			if (ccRecord.getPrescription())
				oClauseVersionChangeRecord.addChangeBrief("Prescriptions");
		}
		
		if (bChanged) {
			oClauseVersionChangeRecord.setQuestion(this.questionCode);
			oClauseVersionChangeRecord.setChanged();
		}
	
	}
}
