package hgs.cpm.db;

import java.sql.ResultSet;
import java.sql.SQLException;

import gov.dod.cls.db.PrescriptionRecord;
import gov.dod.cls.utils.CommonUtils;
import hgs.cpm.utils.SqlCommons;

public class ParsedPrescriptionRecord {

	public String prescription;
	public Integer id;
	public boolean isNew;
	
	public void read(ResultSet rs) throws SQLException {
		this.prescription = rs.getString(1);
		this.id = CommonUtils.toInteger(rs.getObject(2));
		this.isNew = (this.id == null);
	}
	
	public String genSql() {
		//if (this.isNew) {
			return "INSERT INTO " + PrescriptionRecord.TABLE_PRESCRIPTIONS
					+ " (" + PrescriptionRecord.FIELD_REGULATION_ID
					+ ", " + PrescriptionRecord.FIELD_PRESCRIPTION_NAME
					+ ") VALUES (1"
					+ ", " + SqlCommons.quoteString(this.prescription)
					+ ") ON DUPLICATE KEY UPDATE " + PrescriptionRecord.FIELD_REGULATION_ID + "=" + PrescriptionRecord.FIELD_REGULATION_ID
					+ ";\n";
			/*
			return "INSERT INTO " + PrescriptionRecord.TABLE_PRESCRIPTIONS
					+ " (" + PrescriptionRecord.FIELD_PRESCRIPTION_ID
					+ ", " + PrescriptionRecord.FIELD_REGULATION_ID
					+ ", " + PrescriptionRecord.FIELD_PRESCRIPTION_NAME
					+ ") VALUES (" + this.id
					+ ", 1"
					+ ", " + SqlCommons.quoteString(this.prescription)
					+ ") ON DUPLICATE KEY UPDATE " + PrescriptionRecord.FIELD_REGULATION_ID + "=" + PrescriptionRecord.FIELD_REGULATION_ID
					+ ";\n";
			*/
		//} else
			//	return "";
	}
	
}
