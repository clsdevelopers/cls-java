package hgs.cpm.db.template;

import gov.dod.cls.utils.CommonUtils;
import gov.dod.cls.utils.sql.SqlUtil;
import hgs.cpm.utils.HtmlUtils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public abstract class TemplateStg2ClauseFillInRecord {
	
	public static final String FIELD_STG1_OFFICIAL_CLAUSE_ID = "Stg1_Official_Clause_Id";
	public static final String FIELD_FILL_IN_CODE         = "Fill_In_Code";
	public static final String FIELD_FILL_IN_TYPE         = "Fill_In_Type";
	public static final String FIELD_FILL_IN_MAX_SIZE     = "Fill_In_Max_Size";
	public static final String FIELD_FILL_IN_GROUP_NUMBER = "Fill_In_Group_Number";
	public static final String FIELD_FILL_IN_PLACEHOLDER  = "Fill_In_Placeholder";
	public static final String FIELD_FILL_IN_DEFAULT_DATA = "Fill_In_Default_Data";
	public static final String FIELD_FILL_IN_DISPLAY_ROWS = "Fill_In_Display_Rows";
	public static final String FIELD_FILL_IN_FOR_TABLE    = "Fill_In_For_Table";
	public static final String FIELD_FILL_IN_HEADING      = "Fill_In_Heading";
	
	private Integer stg2Id;
	private Integer stg1OfficialClauseId;
	private String fillInCode;
	private String fillInType;
	private Integer fillInMaxSize;
	private Integer fillInGroupNumber;
	private String fillInPlaceholder;
	private String fillInDefaultData;
	private Integer fillInDisplayRows;
	private boolean fillInForTable;
	private String fillInHeading;
	
	public abstract String getTableName();
	public abstract String getIdFieldName();
	public abstract String getClauseIdFieldName();

	public void clear() {
		this.stg2Id = null;
		this.stg1OfficialClauseId = null;
		this.fillInCode = null;
		this.fillInType = null;
		this.fillInMaxSize = null;
		this.fillInGroupNumber = null;
		this.fillInPlaceholder = null;
		this.fillInDefaultData = null;
		this.fillInDisplayRows = null;
		this.fillInForTable = false;
		this.fillInHeading = null;
	}
	
	public String getSqlSelect() {
		return "SELECT " + this.getIdFieldName()
			+ ", " + this.getClauseIdFieldName()
			+ ", " + FIELD_FILL_IN_CODE
			+ ", " + FIELD_FILL_IN_TYPE
			+ ", " + FIELD_FILL_IN_MAX_SIZE
			+ ", " + FIELD_FILL_IN_GROUP_NUMBER
			+ ", " + FIELD_FILL_IN_PLACEHOLDER
			+ ", " + FIELD_FILL_IN_DEFAULT_DATA
			+ ", " + FIELD_FILL_IN_DISPLAY_ROWS
			+ ", " + FIELD_FILL_IN_FOR_TABLE
			+ ", " + FIELD_FILL_IN_HEADING
			+ " FROM " + this.getTableName();
	}

	public PreparedStatement prepareInsert(Connection conn) throws SQLException {
		String sSql = "INSERT INTO " + this.getTableName()
				+ " (" + this.getClauseIdFieldName()
				+ ", " + FIELD_FILL_IN_CODE
				+ ", " + FIELD_FILL_IN_TYPE
				+ ", " + FIELD_FILL_IN_MAX_SIZE
				+ ", " + FIELD_FILL_IN_GROUP_NUMBER
				+ ", " + FIELD_FILL_IN_PLACEHOLDER
				+ ", " + FIELD_FILL_IN_DEFAULT_DATA
				+ ", " + FIELD_FILL_IN_DISPLAY_ROWS
				+ ", " + FIELD_FILL_IN_FOR_TABLE
				+ ", " + FIELD_FILL_IN_HEADING + ")"
				+ " VALUES (?" // FIELD_STG1_OFFICIAL_CLAUSE_ID
				+ ",?" // FIELD_FILL_IN_CODE
				+ ",?" // FIELD_FILL_IN_TYPE
				+ ",?" // FIELD_FILL_IN_MAX_SIZE
				+ ",?" // FIELD_FILL_IN_GROUP_NUMBER
				+ ",?" // FIELD_FILL_IN_PLACEHOLDER
				+ ",?" // FIELD_FILL_IN_DEFAULT_DATA
				+ ",?" // FIELD_FILL_IN_DISPLAY_ROWS
				+ ",?" // FIELD_FILL_IN_FOR_TABLE
				+ ",?" // FIELD_FILL_IN_HEADING
				+ ")";
		return conn.prepareStatement(sSql, Statement.RETURN_GENERATED_KEYS);
	}

	public Integer insert(PreparedStatement ps1) throws SQLException {
		ResultSet res = null;
		try {
			SqlUtil.setParam(ps1, 1, this.stg1OfficialClauseId, java.sql.Types.INTEGER);
			SqlUtil.setParam(ps1, 2, this.fillInCode, java.sql.Types.VARCHAR);
			SqlUtil.setParam(ps1, 3, this.fillInType, java.sql.Types.VARCHAR);
			SqlUtil.setParam(ps1, 4, this.fillInMaxSize, java.sql.Types.INTEGER);
			SqlUtil.setParam(ps1, 5, this.fillInGroupNumber, java.sql.Types.INTEGER);
			SqlUtil.setParam(ps1, 6, this.fillInPlaceholder, java.sql.Types.VARCHAR);
			SqlUtil.setParam(ps1, 7, this.fillInDefaultData, java.sql.Types.VARCHAR);
			SqlUtil.setParam(ps1, 8, this.fillInDisplayRows, java.sql.Types.INTEGER);
			SqlUtil.setParam(ps1, 9, this.fillInForTable, java.sql.Types.BOOLEAN);
			SqlUtil.setParam(ps1, 10, this.fillInHeading, java.sql.Types.VARCHAR);
			ps1.execute();
			res = ps1.getGeneratedKeys();
			if (res.next()) {
				this.stg2Id = res.getInt(1);
			}
		} finally {
			SqlUtil.closeInstance(res);
		}
		return this.stg2Id;
	}
	
	public void read(ResultSet res) throws SQLException {
		this.stg2Id = res.getInt(this.getIdFieldName());
		this.stg1OfficialClauseId = res.getInt(this.getClauseIdFieldName());
		this.fillInCode = res.getString(FIELD_FILL_IN_CODE);
		this.fillInType = res.getString(FIELD_FILL_IN_TYPE);
		this.fillInMaxSize = CommonUtils.toInteger(res.getObject(FIELD_FILL_IN_MAX_SIZE));
		this.fillInGroupNumber = CommonUtils.toInteger(res.getObject(FIELD_FILL_IN_GROUP_NUMBER));
		this.fillInPlaceholder = res.getString(FIELD_FILL_IN_PLACEHOLDER);
		this.fillInDefaultData = res.getString(FIELD_FILL_IN_DEFAULT_DATA);
		this.fillInDisplayRows = CommonUtils.toInteger(res.getObject(FIELD_FILL_IN_DISPLAY_ROWS));
		this.fillInForTable = res.getBoolean(FIELD_FILL_IN_FOR_TABLE);
		this.fillInHeading = res.getString(FIELD_FILL_IN_HEADING);
	}
	
	public void set(int iStg1OfficialClauseId, 
			String psFillInCode,
			String psFillInType,
			Integer piFillInMaxSize,
			Integer piFillInGroupNumber,
			String psFillInPlaceholder,
			String psFillInDefaultData,
			Integer piFillInDisplayRows,
			boolean pbFillInForTable,
			String psFillInHeading) {
		this.clear();
		this.stg1OfficialClauseId = iStg1OfficialClauseId;
		this.fillInCode = psFillInCode;
		this.fillInType = psFillInType;
		this.fillInMaxSize = piFillInMaxSize;
		this.fillInGroupNumber = piFillInGroupNumber;
		this.fillInPlaceholder = psFillInPlaceholder;
		this.fillInDefaultData = psFillInDefaultData;
		this.fillInDisplayRows = piFillInDisplayRows;
		this.fillInForTable = pbFillInForTable;
		this.fillInHeading = psFillInHeading;
	}
	
	public void set2(String html, int iStg1OfficialClauseId, 
			String psFillInCode,
			String psFillInType,
			Integer piFillInMaxSize,
			Integer piFillInGroupNumber,
			String psFillInPlaceholder,
			String psFillInDefaultData,
			Integer piFillInDisplayRows,
			boolean pbFillInForTable,
			String psFillInHeading) {
		this.clear();
		this.stg1OfficialClauseId = iStg1OfficialClauseId;
		this.fillInCode = psFillInCode;
		this.fillInType = psFillInType;
		this.fillInMaxSize = piFillInMaxSize;
		this.fillInGroupNumber = piFillInGroupNumber;
		this.fillInPlaceholder = psFillInPlaceholder;
		this.fillInDefaultData = psFillInDefaultData;
		this.fillInDisplayRows = piFillInDisplayRows;
		this.fillInForTable = pbFillInForTable;
		this.fillInHeading = psFillInHeading;
		
		// let me know, if error occurs.
		if (!html.contains(psFillInCode)) {
			System.out.println(psFillInCode + " is missing from clause text!"); 
		}
	}
	public void setRegularFillin(String html, int iStg1OfficialClauseId, 
			String psFillInCode,
			String psFillInType,
			Integer piFillInMaxSize,
			Integer piFillInGroupNumber,
			String psFillInPlaceholder,
			Integer piFillInDisplayRows,
			PreparedStatement psFillInInsertClause) {
		this.clear();
		this.stg1OfficialClauseId = iStg1OfficialClauseId;
		this.fillInCode = psFillInCode;
		this.fillInType = psFillInType;
		this.fillInMaxSize = piFillInMaxSize;
		this.fillInGroupNumber = piFillInGroupNumber;
		this.fillInPlaceholder = psFillInPlaceholder;
		this.fillInDefaultData = null;
		this.fillInDisplayRows = piFillInDisplayRows;
		this.fillInForTable = false;
		this.fillInHeading = null;
		
		// let me know, if error occurs.
		if (!html.contains(psFillInCode)) {
			System.out.println(psFillInCode + " is missing from clause text!"); 
		}
		
		if (psFillInInsertClause != null)
			try {
				this.insert(psFillInInsertClause);
			} catch (SQLException e) {
				e.printStackTrace();
			}
	}
	// =================================================================
	public Integer getStg2Id() {
		return stg2Id;
	}
	public void setStg2Id(Integer stg2Id) {
		this.stg2Id = stg2Id;
	}

	public Integer getStg1OfficialClauseId() {
		return stg1OfficialClauseId;
	}
	public void setStg1OfficialClauseId(Integer stg1OfficialClauseId) {
		this.stg1OfficialClauseId = stg1OfficialClauseId;
	}
	public String getFillInCode() {
		return fillInCode;
	}
	public void setFillInCode(String fillInCode) {
		this.fillInCode = fillInCode;
	}
	public String getFillInType() {
		return fillInType;
	}
	public void setFillInType(String fillInType) {
		this.fillInType = fillInType;
	}
	public Integer getFillInMaxSize() {
		return fillInMaxSize;
	}
	public void setFillInMaxSize(Integer fillInMaxSize) {
		this.fillInMaxSize = fillInMaxSize;
	}
	public Integer getFillInGroupNumber() {
		return fillInGroupNumber;
	}
	public void setFillInGroupNumber(Integer fillInGroupNumber) {
		this.fillInGroupNumber = fillInGroupNumber;
	}
	public String getFillInPlaceholder() {
		return fillInPlaceholder;
	}
	public void setFillInPlaceholder(String fillInPlaceholder) {
		this.fillInPlaceholder = fillInPlaceholder;
	}
	public String getFillInDefaultData() {
		return fillInDefaultData;
	}
	public void setFillInDefaultData(String fillInDefaultData) {
		this.fillInDefaultData = fillInDefaultData;
		if (this.fillInDisplayRows == null) {
			int iRows = HtmlUtils.calcHtmlMaxRows(fillInDefaultData, 120);
			if (iRows < 12)
				iRows = 12;
			else if (iRows > 40)
				iRows = 40;
			this.fillInDisplayRows = iRows;
		}
	}
	public Integer getFillInDisplayRows() {
		return fillInDisplayRows;
	}
	public void setFillInDisplayRows(Integer fillInDisplayRows) {
		this.fillInDisplayRows = fillInDisplayRows;
	}
	public boolean isFillInForTable() {
		return fillInForTable;
	}
	public void setFillInForTable(boolean fillInForTable) {
		this.fillInForTable = fillInForTable;
	}
	public String getFillInHeading() {
		return fillInHeading;
	}
	public void setFillInHeading(String fillInHeading) {
		this.fillInHeading = fillInHeading;
	}

}
