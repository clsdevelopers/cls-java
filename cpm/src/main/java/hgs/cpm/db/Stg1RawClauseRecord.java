package hgs.cpm.db;

public class Stg1RawClauseRecord {

	public static final String TABLE_STG1_RAW_CLAUSES = "Stg1_Raw_Clauses";

	public static final String FIELD_STG1_CLAUSE_ID       = "Stg1_Clause_Id";
	public static final String FIELD_SECTION              = "Section";
	public static final String FIELD_IBR_OR_FULL_TEXT     = "IBR_Or_Full_Text";
	public static final String FIELD_FILL_IN              = "Fill_In";
	public static final String FIELD_CLAUSE               = "Clause";
	public static final String FIELD_EFFECTIVE_CLAUSE_DATE = "Effective_Clause_Date";
	public static final String FIELD_CLAUSE_PRESCRIPTION  = "Clause_Prescription";
	public static final String FIELD_PRESCRIPTION_TEXT    = "Prescription_Text";
	public static final String FIELD_CONDITIONS           = "Conditions";
	public static final String FIELD_RULE                 = "Rule";
	public static final String FIELD_PRESC_OR_CLAUSE      = "Presc_Or_Clause";
	public static final String FIELD_CLAUSE_NUMBER        = "Clause_Number";
	public static final String FIELD_ADDITIONAL_CONDITIONS = "Additional_Conditions";
	public static final String FIELD_SRC_DOC_ID           = "Src_Doc_Id";
	public static final String FIELD_SHEET_NUMBER         = "Sheet_Number";
	public static final String FIELD_ROW_NUMBER           = "Row_Number";
	public static final String FIELD_EDITABLE             = "Editable";
	public static final String FIELD_REQUIRED_OR_OPTIONAL = "Required_Or_Optional";
	public static final String FIELD_PROVISION_OR_CLAUSE  = "Provision_Or_Clause";
	public static final String FIELD_COMMERCIAL_STATUS    = "Commercial_Status"; // CJ-476
	public static final String FIELD_OPTIONAL_STATUS 		= "Optional_Status";	// CJ-802 
	public static final String FIELD_OPTIONAL_CONDITIONS 	= "Optional_Conditions";	// CJ-802 
	public static final String FIELD_OFFICIAL_CLAUSE_TITLE 	= "Official_Clause_Title";	// CJ-916
	public static final String FIELD_DFARS_ACTIVITY 	= "Is_Dfars_Activity";	// CJ-1072
	
	
	// =======================================================
	private Integer stg1ClauseId;
	private String section;
	private String ibrOrFullText;
	private String fillIn;
	private String clause;
	private String effectiveClauseDate;
	private String clausePrescription;
	private String prescriptionText;
	private String conditions;
	private String rule;
	private String prescOrClause;
	private String clauseNumber;
	private String additionalConditions;
	private Integer srcDocId;
	private Integer sheetNumber;
	private Integer rowNumber;
	private String editable;
	private String requiredOrOptional;
	private String provisionOrClause;
	private String commercialStatus; // CJ-476
	private String optionalStatus; // CJ-802
	private String optionalConditions; // CJ-802
	private String officialTitle; // CJ-916
	private Boolean isDfarsActvity; // CJ-1072
	
	// =======================================================
	public Integer getStg1ClauseId() {
		return stg1ClauseId;
	}
	public void setStg1ClauseId(Integer stg1ClauseId) {
		this.stg1ClauseId = stg1ClauseId;
	}
	public String getSection() {
		return section;
	}
	public void setSection(String section) {
		this.section = section;
	}
	public String getIbrOrFullText() {
		return ibrOrFullText;
	}
	public void setIbrOrFullText(String ibrOrFullText) {
		this.ibrOrFullText = ibrOrFullText;
	}
	public String getFillIn() {
		return fillIn;
	}
	public void setFillIn(String fillIn) {
		this.fillIn = fillIn;
	}
	public String getClause() {
		return clause;
	}
	public void setClause(String clause) {
		this.clause = clause;
	}
	public String getEffectiveClauseDate() {
		return effectiveClauseDate;
	}
	public void setEffectiveClauseDate(String effectiveClauseDate) {
		this.effectiveClauseDate = effectiveClauseDate;
	}
	public String getClausePrescription() {
		return clausePrescription;
	}
	public void setClausePrescription(String clausePrescription) {
		this.clausePrescription = clausePrescription;
	}
	public String getPrescriptionText() {
		return prescriptionText;
	}
	public void setPrescriptionText(String prescriptionText) {
		this.prescriptionText = prescriptionText;
	}
	public String getConditions() {
		return conditions;
	}
	public void setConditions(String conditions) {
		this.conditions = conditions;
	}
	public String getRule() {
		return rule;
	}
	public void setRule(String rule) {
		this.rule = rule;
	}
	public String getPrescOrClause() {
		return prescOrClause;
	}
	public void setPrescOrClause(String prescOrClause) {
		this.prescOrClause = prescOrClause;
	}
	public String getClauseNumber() {
		return clauseNumber;
	}
	public void setClauseNumber(String clauseNumber) {
		this.clauseNumber = clauseNumber;
	}
	public String getAdditionalConditions() {
		return additionalConditions;
	}
	public void setAdditionalConditions(String additionalConditions) {
		this.additionalConditions = additionalConditions;
	}
	public Integer getSrcDocId() {
		return srcDocId;
	}
	public void setSrcDocId(Integer srcDocId) {
		this.srcDocId = srcDocId;
	}
	public Integer getSheetNumber() {
		return sheetNumber;
	}
	public void setSheetNumber(Integer sheetNumber) {
		this.sheetNumber = sheetNumber;
	}
	public Integer getRowNumber() {
		return rowNumber;
	}
	public void setRowNumber(Integer rowNumber) {
		this.rowNumber = rowNumber;
	}
	public String getEditable() {
		return editable;
	}
	public void setEditable(String editable) {
		this.editable = editable;
	}
	public String getRequiredOrOptional() {
		return requiredOrOptional;
	}
	public void setRequiredOrOptional(String requiredOrOptional) {
		this.requiredOrOptional = requiredOrOptional;
	}
	public String getProvisionOrClause() {
		return provisionOrClause;
	}
	public void setProvisionOrClause(String provisionOrClause) {
		this.provisionOrClause = provisionOrClause;
	}
	public String getCommercialStatus() {
		return this.commercialStatus;
	}
	public void setCommercialStatus(String commercialStatus) {
		this.commercialStatus = commercialStatus;
	}
	
	public String getOptionalStatus() {
		return this.optionalStatus;
	}

	public void setOptionalStatus(String optionalStatus) {
		this.optionalStatus = optionalStatus;
	}
	
	public String getOptionalConditions() {
		return this.optionalConditions;
	}

	public void setOptionalConditions(String optionalConditions) {
		this.optionalConditions = optionalConditions;
	}
	
	public String getOfficialTitle() {
		return this.officialTitle;
	}

	public void setOfficialTitle(String officialTitle) {
		this.officialTitle = officialTitle;
	}
	
	public Boolean getDfarsActvity() {
		return this.isDfarsActvity;
	}

	public void setDfarsActvity(boolean isDfarsActvity) {
		this.isDfarsActvity = isDfarsActvity;
	}

}
