package hgs.cpm.db;

import gov.dod.cls.utils.CommonUtils;
import gov.dod.cls.utils.sql.SqlUtil;
import hgs.cpm.utils.HtmlUtils;
import hgs.cpm.utils.SqlCommons;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;

public class Stg1OfficialClauseRecord {

	public static final String TABLE_STG1_OFFICIAL_CLAUSE = "Stg1_Official_Clause";
	
	public static final String FIELD_STG1_OFFICIAL_CLAUSE_ID = "Stg1_Official_Clause_Id";
	public static final String FIELD_CLAUSE_NUMBER        = "Clause_Number"; // VARCHAR(100) NULL,
	public static final String FIELD_TITLE                = "Title"; // VARCHAR(500) NULL,
	public static final String FIELD_PRESCRIPTION         = "Prescription"; // VARCHAR(100) NULL,
	public static final String FIELD_URI                  = "URI"; // VARCHAR(500) NULL,
	public static final String FIELD_CONTENT              = "Content"; // TEXT NULL,
	public static final String FIELD_TITLE_IN_CONTENT     = "Title_In_Content"; // VARCHAR(500) NULL, // CJ-193
	public static final String FIELD_CONTENT_HTML         = "Content_HTML"; // TEXT NULL,
	public static final String FIELD_CONTENT_HTML_FILLINS = "Content_Html_FillIns"; // TEXT NULL,
	public static final String FIELD_CONTENT_HTML_PROCESSED = "Content_Html_Processed"; // TEXT NULL,
	public static final String FIELD_SRC_DOC_ID           = "Src_Doc_Id"; // SMALLINT NOT NULL,
	public static final String FIELD_EDIT_AND_FILLIN      = "IsEditAndFillIn"; // SMALLINT NOT NULL,
	
	public static PreparedStatement createInsertStatement(Connection conn) throws SQLException {
		String sSql =  
			"INSERT INTO " + TABLE_STG1_OFFICIAL_CLAUSE + "\n" +
			"(" + FIELD_CLAUSE_NUMBER + 
			", " + FIELD_TITLE + 
			", " + FIELD_PRESCRIPTION + 
			", " + FIELD_CONTENT + 
			", " + FIELD_TITLE_IN_CONTENT + // CJ-193
			", " + FIELD_CONTENT_HTML + 
			", " + FIELD_CONTENT_HTML_FILLINS + 
			", " + FIELD_URI + 
			", " + FIELD_SRC_DOC_ID + ")\n" +
			"VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";
		return conn.prepareStatement(sSql);
	}
	/*
	String sSql = "INSERT INTO Stg1_Official_Clause\n" +
			"(clause_number, title, prescription, Content, Content_HTML, Content_Html_FillIns, uri, Src_Doc_Id)\n" +
			"VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
	*/
	
	public static void deleteAll(Connection conn, Integer srcDocId) throws SQLException {
		if (srcDocId == null)
			return;
		
		String sSql = "DELETE FROM " + TABLE_STG1_OFFICIAL_CLAUSE + " WHERE " + FIELD_SRC_DOC_ID + " = ?";
		PreparedStatement ps1 = null;
		try {
			ps1 = conn.prepareStatement(sSql);
			ps1.setInt(1, srcDocId);
			ps1.execute();
		
			Stg1OfficialClauseAltRecord.deleteAll(conn, srcDocId);
		}
		finally {
			SqlUtil.closeInstance(ps1);
		}
	}

	public Integer stg1OfficialClauseId;
	public String clauseNumber;
	public String title;
	public String prescription;
	public String uri;
	public String content;
	private String titleInContent; // CJ-193
	private String contentHtml;
	public String contentHtmlFillIns;
	public String contentHtmlProcessed;
	public Integer srcDocId;
	
	public ArrayList<Stg1OfficialClauseAltRecord> oAltSet = new ArrayList<Stg1OfficialClauseAltRecord>();

	public String getContentHtml() { // CJ-193
		return this.contentHtml;
	}
	public void setContentHtml(String contentHtml) { // CJ-193
		this.contentHtml = contentHtml;
		if (CommonUtils.isNotEmpty(this.contentHtml)) {
			int iPos = this.contentHtml.indexOf("</h1>");
			if (iPos > 1) {
				int iStart = this.contentHtml.indexOf("<h1");
				if (iStart < 0) {
					iStart = 0;
				} else {
					iStart = this.contentHtml.indexOf(">") + 1;
				}
				this.titleInContent = HtmlUtils.cleanupHtml(this.contentHtml.substring(iStart, iPos)).replaceAll("&mdash;", "-");
			}
		}
	}
	
	public String getTitleInContent() { // CJ-193
		return this.titleInContent;
	}
	public void setTitleInContent(String titleInContent) { // CJ-193
		this.titleInContent = titleInContent;
	}
	
	public void insert(PreparedStatement psClause, PreparedStatement psAlt) throws SQLException {
		int iParam = 0; 
		SqlUtil.setParam(psClause, ++iParam, this.clauseNumber, java.sql.Types.VARCHAR);
		SqlUtil.setParam(psClause, ++iParam, this.title, java.sql.Types.VARCHAR);
		SqlUtil.setParam(psClause, ++iParam, this.prescription, java.sql.Types.VARCHAR);
		SqlUtil.setParam(psClause, ++iParam, this.content, java.sql.Types.VARCHAR);
		SqlUtil.setParam(psClause, ++iParam, this.titleInContent, java.sql.Types.VARCHAR); // CJ-193
		SqlUtil.setParam(psClause, ++iParam, this.contentHtml, java.sql.Types.VARCHAR);
		SqlUtil.setParam(psClause, ++iParam, this.contentHtmlFillIns, java.sql.Types.VARCHAR);
		SqlUtil.setParam(psClause, ++iParam, this.uri, java.sql.Types.VARCHAR);
		SqlUtil.setParam(psClause, ++iParam, this.srcDocId, java.sql.Types.INTEGER);
		psClause.execute();
	
		for (Stg1OfficialClauseAltRecord oAltClause : this.oAltSet) {
			oAltClause.clauseNumber = this.clauseNumber;
			oAltClause.srcDocId = this.srcDocId;
			oAltClause.insert(psAlt);
		}
	}
	
	public String getClauseNumbers() {
		String result = this.clauseNumber;
		for (Stg1OfficialClauseAltRecord oAltClause : this.oAltSet) {
			result += ", " + oAltClause.altNumber; 
		}
		return result;
	}
	
	public void clear() {
		this.stg1OfficialClauseId = null;
		this.clauseNumber = null;
		this.title = null;
		this.prescription = null;
		this.uri = null;
		this.content = null;
		this.titleInContent = null; // CJ-193
		this.contentHtml = null;
		this.contentHtmlFillIns = null;
		this.contentHtmlProcessed = null;
		
		this.oAltSet.clear();
	}
	
	public void add(Stg1OfficialClauseAltRecord oAltRecord) {
		this.oAltSet.add(oAltRecord);
	}
	
	public void generateFile(String sOutputFolder, boolean bSql) throws IOException {
		String sFileName = sOutputFolder + "\\" + this.clauseNumber + "." + (bSql ? "sql" : "html");
		FileOutputStream fileOutputStream = null;
    	try {
    		File file = new File(sFileName);
    		if (file.exists()) {
    			file.delete();
    			System.out.println("Deleted " + sFileName);
    		}
    		file.createNewFile();

    		fileOutputStream = new FileOutputStream(file);
    		if (bSql) {
        		String sSql =
        				"INSERT INTO " + TABLE_STG1_OFFICIAL_CLAUSE + "\n" +
        				"(" + FIELD_CLAUSE_NUMBER + 
        				", " + FIELD_TITLE + 
        				", " + FIELD_PRESCRIPTION + 
        				", " + FIELD_CONTENT_HTML + 
        				// ", " + FIELD_CONTENT_HTML_FILLINS + 
        				", " + FIELD_URI + 
        				", " + FIELD_SRC_DOC_ID + ")\n" +
        				"VALUES (" + SqlCommons.quoteString(this.clauseNumber) + 
        				", " + SqlCommons.quoteString(this.title) + 
        				", " + SqlCommons.quoteString(this.prescription) + 
        				",\n" + SqlCommons.quoteString(this.contentHtml) + 
        				",\n" + SqlCommons.quoteString(this.uri) + 
        				", " + this.srcDocId + 
        				");\n";
    			fileOutputStream.write(sSql.getBytes());
    		} else {
        		String sContent;
        		if (this.contentHtmlProcessed != null)
        			sContent = this.contentHtmlProcessed;
        		else if (this.contentHtmlFillIns != null)
        			sContent = this.contentHtmlFillIns;
        		else if (this.contentHtml != null)
        			sContent = this.contentHtml;
        		else
        			sContent = this.content;

    			fileOutputStream.write(sContent.getBytes());
    		}
		}
    	finally {
			try {
				if (fileOutputStream != null) {
					fileOutputStream.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
    	}
    	
		for (Stg1OfficialClauseAltRecord oAltClause : this.oAltSet) {
			oAltClause.generateFile(sOutputFolder, bSql);
		}
	}
	
	// =====================================================
	public static void main( String[] args )
    {
		String content = "<h1 class=\"hd1\">Requirements&mdash;Basic (APR 2014)</h1><p>(a) This is a requirements contract for the supplies or services specified and effective for the period stated in the Schedule. The quantities of supplies or services specified in the Schedule are estimates only and are not purchased by this contract. Except as this contract may otherwise provide, if the Government's requirements do not result in orders in the quantities described as &ldquo;estimated\" or &ldquo;maximum\" in the Schedule, that fact shall not constitute the basis for an equitable price adjustment.</p><p>(b) Delivery or performance shall be made only as authorized by orders issued in accordance with the Ordering clause. Subject to any limitations in the Order Limitations clause or elsewhere in this contract, the Contractor shall furnish to the Government all supplies or services specified in the Schedule and called for by orders issued in accordance with the Ordering clause. The Government may issue orders requiring delivery to multiple destinations or performance at multiple locations.</p><p>(c) Except as this contract otherwise provides, the Government shall order from the Contractor all the supplies or services specified in the Schedule that are required to be purchased by the Government activity or activities specified in the Schedule.</p><p>(d) The Government is not required to purchase from the Contractor requirements in excess of any limit on total orders under this contract.</p><p>(e) If the Government urgently requires delivery of any quantity of an item before the earliest date that delivery may be specified under this contract, and if the Contractor will not accept an order providing for the accelerated delivery, the Government may acquire the urgently required goods or services from another source.</p><p>(f) Orders issued during the effective period of this contract and not completed within that time shall be completed by the Contractor within the time specified in the order. The rights and obligations of the Contractor and the Government for those orders shall be governed by the terms of this contract to the same extent as if completed during the effective period.</p>";
		content = "<h1 class=\"hd1\">Exercise of Option To Fulfill Foreign Military Sales Commitments&mdash;Basic (NOV 2014)</h1><p>(a) The Government may exercise the option(s) of this contract to fulfill foreign military sales commitments.</p><p>(b) The foreign military sales commitments are for:</p><div class=\"fpdash\"><span class=\"fpdash\">&nbsp;</span></div><p class=\"fp\">(Insert name of country)</p><div class=\"fpdash\"><span class=\"fpdash\">&nbsp;</span></div><p class=\"fp\">(Insert applicable CLIN)</p>";
		Stg1OfficialClauseRecord stg1OfficialClauseRecord = new Stg1OfficialClauseRecord();
		stg1OfficialClauseRecord.setContentHtml(content);
		System.out.println(stg1OfficialClauseRecord.titleInContent);
		
		ParsedClauseRecord parsedClauseRecord = new ParsedClauseRecord();
		parsedClauseRecord.setTitleInContent(stg1OfficialClauseRecord.titleInContent);
		System.out.println(parsedClauseRecord.isBasicClause);
    }
	
}
