package hgs.cpm.config;

import hgs.cpm.utils.StringCommons;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ResourceBundle;

public class CpmConfig {

	private static CpmConfig cpmConfig = null; 
	public synchronized static CpmConfig getInstance() {
		//CpmConfig cpmConfig = ContextHelper.getInstance().getSpringBean(CpmConfig.class, "cpmConfig");
		if (CpmConfig.cpmConfig == null) {
			CpmConfig.cpmConfig = new CpmConfig();
		}
		return CpmConfig.cpmConfig;
	}
	
	public static Connection dbConnection() {
		CpmConfig cpmConfig = CpmConfig.getInstance();
		return cpmConfig.getDbConnection();
	}
	
	// ====================================================================
	private final static String MYSQL_DRIVER = "com.mysql.jdbc.Driver";
	
	// ====================================================================
	private ResourceBundle dbLoaderResource;
	
	private String host;
	private String user;
	private String password;
	private String schema;
	
	private String excelQuestion;
	private String excelQuestionSequence;
	private String excelFarDfar;

	private String versionIdPrevious;
	private String versionIdTarget;
	private String versionNameTarget;
	
	private String sqlOutputPath;
	
	private String clauseCorrectionPath;
			
	private boolean jdbcDriverLoaded = false;
	
	public CpmConfig() {
		super();
		try {
			this.dbLoaderResource = ResourceBundle.getBundle("cpm");
			this.load();
		} catch (Exception oError) {
			oError.printStackTrace();
		}
	}
	
	private void load() {
		this.host = this.dbLoaderResource.getString("db.host");
		this.user = this.dbLoaderResource.getString("db.user");
		this.password = dbLoaderResource.getString("db.password");

		this.schema = this.dbLoaderResource.getString("db.schema");

		this.excelQuestion = this.dbLoaderResource.getString("excel.question");
		this.excelQuestionSequence = this.dbLoaderResource.getString("excel.question_sequence");
		this.excelFarDfar = this.dbLoaderResource.getString("excel.fardfar");
		
		this.versionIdPrevious = this.dbLoaderResource.getString("version.id.previous");
		this.versionIdTarget = this.dbLoaderResource.getString("version.id.target");
		this.versionNameTarget = this.dbLoaderResource.getString("version.name.target");
		
		this.clauseCorrectionPath = this.dbLoaderResource.getString("clause_correction_path");
		this.sqlOutputPath = this.dbLoaderResource.getString("sql_output_path").trim();

		this.loadJdbcDriver();
	}
	
	public boolean loadJdbcDriver() {
		if (!this.jdbcDriverLoaded)
			try { // The newInstance() call is a work around for some broken Java implementations
	            Class.forName(MYSQL_DRIVER).newInstance();
	            this.jdbcDriverLoaded = true;
	        } catch (Exception ex) {
			    System.out.println("SQLException: " + ex.getMessage());
	        	ex.printStackTrace();
	        	this.jdbcDriverLoaded = false;
	        }
		return this.jdbcDriverLoaded;
	}
	
	public Connection getDbConnection() {
		return this.getConnection(this.schema);
	}
	
	public Connection getConnection(String pSchema) {
		Connection conn = null;
		if (StringCommons.isEmpty(this.host) || StringCommons.isEmpty(this.user) ||
			StringCommons.isEmpty(this.password) || StringCommons.isEmpty(pSchema)) {
			return null;
		}
		
		// http://www.mkyong.com/jdbc/how-to-connect-to-mysql-with-jdbc-driver-java/
		// .getConnection("jdbc:mysql://localhost:3306/mkyongcom","root", "password");
		try {
			this.loadJdbcDriver();
			
			String sUrl = "jdbc:mysql://" + this.host + ":3306/" + pSchema;
			conn = DriverManager.getConnection(sUrl, this.user, this.password);

		} catch (SQLException ex) {
		    System.out.println("SQLException: " + ex.getMessage());
		    System.out.println("SQLState: " + ex.getSQLState());
		    System.out.println("VendorError: " + ex.getErrorCode());
		    return null;
		}
		catch (Exception oError) {
			oError.printStackTrace();
			return null;
		}
		return conn;
	}
	
	public String getHost() {
		return this.host;
	}
	public void setHost(String host) {
		this.host = host;
	}

	public String getUser() {
		return this.user;
	}
	public void setUser(String user) {
		this.user = user;
	}

	public String getPassword() {
		return this.password;
	}
	public void setPassword(String password) {
		this.password = password;
	}

	public String getSchema() {
		return this.schema;
	}
	public void setSchema(String schema) {
		this.schema = schema;
	}

	public String getExcelQuestion() {
		return excelQuestion;
	}
	public void setExcelQuestion(String excelQuestion) {
		this.excelQuestion = excelQuestion;
	}

	public String getExcelQuestionSequence() {
		return excelQuestionSequence;
	}
	public void setExcelQuestionSequence(String excelQuestionSequence) {
		this.excelQuestionSequence = excelQuestionSequence;
	}

	public String getExcelFarDfar() {
		return excelFarDfar;
	}
	public void setExcelFarDfar(String excelFarDfar) {
		this.excelFarDfar = excelFarDfar;
	}

	public String getVersionIdTarget() {
		return versionIdTarget;
	}
	public Integer getVersionIdTargetInteger() {
		try {
			return Integer.parseInt(this.versionIdTarget);
		}
		catch (Exception oError) {
			oError.printStackTrace();
			return null;
		}
	}
	
	public String getVersionNameTarget() {
		return versionNameTarget;
	}
	public void setVersionNameTarget(String versionNameTarget) {
		this.versionNameTarget = versionNameTarget;
	}

	public void setVersionIdTarget(String versionIdTarget) {
		this.versionIdTarget = versionIdTarget;
	}

	public String getVersionIdPrevious() {
		return versionIdPrevious;
	}
	public Integer getVersionIdPreviousInteger() {
		try {
			return Integer.parseInt(this.versionIdPrevious);
		}
		catch (Exception oError) {
			oError.printStackTrace();
			return null;
		}
	}
	public void setVersionIdPrevious(String versionIdPrevious) {
		this.versionIdPrevious = versionIdPrevious;
	}

	public String getSqlOutputPath() {
		return sqlOutputPath;
	}
	public void setSqlOutputPath(String sqlOutputPath) {
		this.sqlOutputPath = sqlOutputPath;
	}
	
	public String getClauseCorrectionPath() {
		return clauseCorrectionPath;
	}

}
