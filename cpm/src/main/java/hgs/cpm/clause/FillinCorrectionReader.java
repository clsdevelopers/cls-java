package hgs.cpm.clause;

import gov.dod.cls.utils.sql.SqlUtil;
import hgs.cpm.db.Stg3ClauseCorrectionRecord;
import hgs.cpm.db.Stg3ClauseCorrectionSet;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Scanner;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class FillinCorrectionReader {
	
	private static final String FILLIN_TYPE_AMOUNT = "$";
	private static final String FILLIN_TYPE_CHECKBOX = "C";
	private static final String FILLIN_TYPE_DATE = "D";
	private static final String FILLIN_TYPE_EMAIL = "E";
	private static final String FILLIN_TYPE_MEMO = "M";
	private static final String FILLIN_TYPE_NUMBER = "N";
	private static final String FILLIN_TYPE_RADIO_BUTTON = "R";
	private static final String FILLIN_TYPE_STRING = "S";
	private static final String FILLIN_TYPE_URL = "U";
	
	private static final String NO_FILLIN_DATA_LABEL = "No fillin data: ";

	private String inputFolder;
	private Connection conn;
	private Integer versionId = null;
	private PreparedStatement psFillinCorrection = null;
	private PreparedStatement psClauseCorrection = null;
	private String folderSlash;
	private HashSet<String> clauseNames;
	private Stg3ClauseCorrectionSet clauseCorrectionSet;
	public Connection getConn() { return conn; }
	public void setConn(Connection conn) { this.conn = conn; }
	
	public FillinCorrectionReader (String inputFolder, int versionId, Connection conn) {
		this.inputFolder = inputFolder;
		this.conn = conn;
		this.versionId = versionId;
		this.clauseNames = new HashSet<String>();
		this.clauseCorrectionSet = new Stg3ClauseCorrectionSet();
		folderSlash = System.getProperty ("os.name").startsWith ("Windows") ? "\\" : "/";
		if(!inputFolder.substring(inputFolder.length() - 1).equals(folderSlash))
			this.inputFolder += folderSlash;
	}

	public void load(boolean clearDatabase) throws IOException, SQLException {
		System.out.println("\nFillinCorrectionReader start of load");
		FileInputStream fileInputStream = null;
		XSSFWorkbook workbook = null;
		try {
			if(clearDatabase)
				this.clearClauseCorrections();
			this.prepareFillinCorrectionStatement();
			this.prepareClauseCorrectionSatement();
			fileInputStream = new FileInputStream(new File(this.inputFolder+"Fillin_Corrections.xlsx"));
			workbook = new XSSFWorkbook(fileInputStream);
			for (int iSheet = 0; iSheet < workbook.getNumberOfSheets(); iSheet++ ) {
				this.readCorrectionExcel(workbook, iSheet);
			}
		}
		finally {
			if (workbook != null)
				workbook.close();
			if (fileInputStream != null)
				try {
					fileInputStream.close();
				} catch (Exception oIgnore) {
					oIgnore.printStackTrace();
				}	
			this.closeFillinCorrectionStatement();
			this.closeClauseCorrectionStatement();
		}
		System.out.println("FillinCorrectionReader end of load\n");
	}

	private int iClauseName;
	private int iFillinMatchCode;
	private int iTempNewFillinCode;
	private int iNewFillinCode;
	private int iTempNewFillinType;
	private int iFillinType;
	private int iTempNewFillinSize;
	private int iFillinMaxSize;
	private int iFillinGroupNumber;
	private int iFillinPlaceholder;
	private int iFillinDefaultData;
	private int iFillinDisplayRows;
	private int iFillinForTable;
	private int iFillinHeading;

	private void readCorrectionExcel(XSSFWorkbook workbook, int iSheet) throws IOException, SQLException {
			XSSFSheet sheet = workbook.getSheetAt(iSheet);
			//Get iterator to all the rows in current sheet
			Iterator<Row> rowIterator = sheet.iterator();
			
			if (!rowIterator.hasNext()) {
				return;
			}
			this.iClauseName = -1;
			this.iFillinMatchCode = -1;
			this.iTempNewFillinCode = -1;
			this.iNewFillinCode = -1;
			this.iTempNewFillinType = -1;
			this.iFillinType = -1;
			this.iTempNewFillinSize = -1;
			this.iFillinMaxSize = -1;
			this.iFillinGroupNumber = -1;
			this.iFillinPlaceholder = -1;
			this.iFillinDefaultData = -1;
			this.iFillinDisplayRows = -1;
			this.iFillinForTable = -1;
			this.iFillinHeading = -1;
				
			Row row = rowIterator.next();
			for (int iCell = row.getFirstCellNum(); iCell < row.getLastCellNum(); iCell++) {
				Cell oCell = row.getCell(iCell);
				String value = oCell.getStringCellValue();
				switch (value) {
				case "CLAUSE NAME":
					this.iClauseName = iCell; 
					break;
				case "FILLIN MATCH CODE":	
					this.iFillinMatchCode = iCell;
					break;
				case "TEMP NEW FILLIN CODE":	
					this.iTempNewFillinCode = iCell;
					break;
				case "CUSTOM FILLIN CODE":
					this.iNewFillinCode = iCell;
					break;
				case "TEMP NEW FILLIN TYPE":
					this.iTempNewFillinType = iCell;
					break;
				case "FILLIN TYPE":
					this.iFillinType = iCell;
					break;
				case "TEMP NEW FILLIN SIZE":
					this.iTempNewFillinSize = iCell;
					break;
				case "FILLIN MAX SIZE":
					this.iFillinMaxSize = iCell;
					break;
				case "FILLIN GROUP NUMBER":
					this.iFillinGroupNumber = iCell;
					break;
				case "FILLIN PLACEHOLDER":	
					this.iFillinPlaceholder = iCell;
					break;
				case "FILLIN DEFAULT DATA":	
					this.iFillinDefaultData = iCell;
					break;
				case "FILLIN DISPLAY ROWS":	
					this.iFillinDisplayRows = iCell;
					break;
				case "FILLIN FOR TABLE":	
					this.iFillinForTable = iCell;
					break;
				case "FILLIN HEADING":
					this.iFillinHeading = iCell;
					break;	
				}
			}	
			
			while (rowIterator.hasNext()) {
	            row = rowIterator.next();
	            this.loadClauseName(row, iSheet);
			}			
			
			for(String clauseName : clauseNames)
				this.loadClauseCorrectionData(clauseName);
			
			System.out.println("FillinCorrectionReader clause's with data loaded: "+clauseNames.size());
			
			clauseCorrectionSet.load(this.conn, this.versionId);
			
			rowIterator = sheet.iterator();
			rowIterator.next();
			int rowsRead = 1;
			while (rowIterator.hasNext()) {
	            row = rowIterator.next();
	            this.loadFillinCorrection(row, iSheet);
	            rowsRead++;
			}
			System.out.println("FillinCorrectionReader total rows read from Excel: "+rowsRead);
	}
	
	private String getCellValue(Row row, int iColumn) {
		if (iColumn < 0)
			return "";
		Cell cell = row.getCell(iColumn);
		if (cell == null || cell.getCellType() == Cell.CELL_TYPE_BLANK) {
			   return "";
		}
		if(cell.getCellType() == cell.CELL_TYPE_NUMERIC)
			return cell.getNumericCellValue() + "";
		return cell.getStringCellValue();
	}
	
	private void loadClauseName(Row row, int iSheet) {
		String sClauseName = getCellValue(row, this.iClauseName);
		if(sClauseName != null && !sClauseName.equals("") && !sClauseName.startsWith(NO_FILLIN_DATA_LABEL) )
			this.clauseNames.add(sClauseName);
	}
	
	private void loadFillinCorrection(Row row, int iSheet) 
			throws SQLException
	{
		String sClauseName = getCellValue(row, this.iClauseName);
		if(sClauseName.equals("") || sClauseName.startsWith(NO_FILLIN_DATA_LABEL)) //extra space row between clauses or extra row where 
			return;				   //old version clause has more fillins that new version clause
		
		String sFillinMatchCode = getCellValue(row, this.iFillinMatchCode);
		String sTempNewFillinCode = getCellValue(row, this.iTempNewFillinCode);
		String sNewFillinCode = getCellValue(row, this.iNewFillinCode);
		String sTempNewFillinType = getCellValue(row, this.iTempNewFillinType);
		String sFillinType = getCellValue(row, this.iFillinType);
		String sTempNewFillinMaxSize = getCellValue(row, this.iTempNewFillinSize);
		String sFillinMaxSize = getCellValue(row, this.iFillinMaxSize);
		String sFillinGroupNumber = getCellValue(row, this.iFillinGroupNumber);
		String sFillinPlaceholder = getCellValue(row, this.iFillinPlaceholder);
		String sFillinDefaultData = getCellValue(row, this.iFillinDefaultData);
		String sFillinDisplayRows = getCellValue(row, this.iFillinDisplayRows);
		String sFillinForTable = getCellValue(row, this.iFillinForTable);
		String sFillinHeading = getCellValue(row, this.iFillinHeading); 
		
		Stg3ClauseCorrectionRecord oClauseCorrectionRecord = this.clauseCorrectionSet.findByName(sClauseName);
		Integer sClauseId = oClauseCorrectionRecord.stg3ClauseId;

		//cannot be null: clause id, code, type, fillin_for_Table
		if(sNewFillinCode == null || sNewFillinCode.equals("")) {
			sNewFillinCode = sTempNewFillinCode;
		}
		
		switch(sFillinType.toUpperCase()) {
			case "$":
			case "CURRENCY":
			case "MONEY":
			case "AMOUNT":
				sFillinType = FILLIN_TYPE_AMOUNT;
			case "C":
			case "CHECKBOX":
			case "CHECK BOX":
				sFillinType = FILLIN_TYPE_CHECKBOX;
			case "DATE":
			case "D":
			case "DAY":
				sFillinType = FILLIN_TYPE_DATE;
			case "E":
			case "EMAIL":
			case "E-MAIL":
				sFillinType = FILLIN_TYPE_EMAIL;
			case "M":
			case "MEMO":
				sFillinType = FILLIN_TYPE_MEMO;
			case "N":
			case "NUMBER":
			case "INTEGER":
				sFillinType = FILLIN_TYPE_NUMBER;
			case "R":
			case "RADIO":
				sFillinType = FILLIN_TYPE_RADIO_BUTTON;
			case "U":
			case "URL":
			case "LINK":
				sFillinType = FILLIN_TYPE_URL;
			case "TEXT":
			case "T":
			case "STRING":
				sFillinType = FILLIN_TYPE_STRING;
			default: 
				sFillinType = sTempNewFillinType;
		}

		if (sFillinForTable.toUpperCase().equals("Y") || sFillinForTable.toUpperCase().equals("YES")
				|| sFillinForTable.toUpperCase().equals("1") || sFillinForTable.toUpperCase().equals("T") || sFillinForTable.toUpperCase().equals("TRUE"))
			sFillinForTable = "1";
		else
			sFillinForTable = "0";
		
		if(sFillinMaxSize == null || sFillinMaxSize.equals(""))
			sFillinMaxSize = sTempNewFillinMaxSize;
		
		this.setFillinCorrectionParam(1, sClauseId.toString());
		this.setFillinCorrectionParam(2, sFillinMatchCode);
		this.setFillinCorrectionParam(3, sNewFillinCode);
		this.setFillinCorrectionParam(4, sFillinType); 
		this.setFillinCorrectionParam(5, sFillinMaxSize);
		this.setFillinCorrectionParam(6, sFillinGroupNumber);
		this.setFillinCorrectionParam(7, sFillinPlaceholder); 
		this.setFillinCorrectionParam(8, sFillinDefaultData);
		this.setFillinCorrectionParam(9, sFillinDisplayRows); 
		this.setFillinCorrectionParam(10, sFillinForTable);
		this.setFillinCorrectionParam(11, sFillinHeading);
		this.psFillinCorrection.execute();
	}
	
	private void clearClauseCorrections() throws SQLException{
		String sSql = "DELETE FROM Stg3_Clause_Corrections\n"
					 +"WHERE Clause_Version_Id = ?	";
		PreparedStatement psClearClauseCorrections = this.conn.prepareStatement(sSql);
		psClearClauseCorrections.setInt(1, this.versionId);
		psClearClauseCorrections.execute();
		psClearClauseCorrections.close();
		
		System.out.println("FillinCorrectionWriter removed clause correction entries for " + this.versionId);
	}
	
	private void setFillinCorrectionParam(int piIndex, String psValue) throws SQLException {
		if ((psValue == null) || psValue.isEmpty())
			this.psFillinCorrection.setNull(piIndex, java.sql.Types.VARCHAR);
		else {
			this.psFillinCorrection.setString(piIndex, psValue);
		}
	}
	
	private void setClauseCorrectionParam(int piIndex, String psValue) throws SQLException {
		if ((psValue == null) || psValue.isEmpty())
			this.psClauseCorrection.setNull(piIndex, java.sql.Types.VARCHAR);
		else {
			this.psClauseCorrection.setString(piIndex, psValue);
		}
	}
	
	private void prepareFillinCorrectionStatement() throws SQLException {
		String sSql
			= "INSERT INTO Stg3_Clause_Fill_In_Corrections (Stg3_Clause_Id, Previous_Fill_In_Code, Fill_In_Code, Fill_In_Type, Fill_In_Max_Size,\n" +
			"Fill_In_Group_Number, Fill_In_Placeholder, Fill_In_Default_Data, Fill_In_Display_Rows, Fill_In_For_Table,\n" +
			"Fill_In_Heading)\n" +
			" VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
		this.psFillinCorrection = this.conn.prepareStatement(sSql);
	}
	
	private void prepareClauseCorrectionSatement() throws SQLException {
		String sSql
			="INSERT INTO Stg3_Clause_Corrections (Clause_Version_Id, Clause_Name, Clause_Data)\n"+
			 "VALUES (?, ?, ?)";
		this.psClauseCorrection = this.conn.prepareStatement(sSql);
	}
	
	private void closeFillinCorrectionStatement() {
		SqlUtil.closeInstance(this.psFillinCorrection);
		this.psFillinCorrection = null;
	}
	
	private void closeClauseCorrectionStatement() {
		SqlUtil.closeInstance(this.psClauseCorrection);
		this.psClauseCorrection = null;
	}
	
	private void loadClauseCorrectionData(String clauseName) throws IOException, SQLException {
		String clauseFolder = inputFolder + clauseName + folderSlash;
		String newClauseFileName = clauseFolder + "NEW_" + clauseName + ".HTML";
		String correctedClauseData = new Scanner(new File(newClauseFileName)).useDelimiter("\\Z").next();
		
		this.setClauseCorrectionParam(1, this.versionId.toString());
		this.setClauseCorrectionParam(2, clauseName);
		this.setClauseCorrectionParam(3, correctedClauseData); //new clause data
		this.psClauseCorrection.execute();
	}
}