package hgs.cpm.clause;

import gov.dod.cls.utils.sql.SqlUtil;
import hgs.cpm.db.ParsedQuestionChoiceRecord;
import hgs.cpm.db.Stg2ClauseQuestionConditionRecord;
import hgs.cpm.question.util.Stg2PossibleAnswerPrescriptionRecord;
import hgs.cpm.utils.SqlCommons;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class ParseClausePrescriptionRules {
	
	// Stg2_Clause_Conditions only maintains the entire condition, per clause.
	// CJ-473, We need to build a table that stores each condition separately.
	public static void run(Connection conn, Integer srcCDocId, Integer srcQDocId) throws SQLException {
		
		String sSql = "DELETE FROM Stg2_Clause_Question_Conditions\n"
				+ "WHERE Stg1_Clause_Id IN (\n"
				+ "SELECT Stg1_Clause_Id FROM Stg1_Raw_Clauses WHERE Src_Doc_Id = ?)";
		SqlCommons.executeForDocId(conn, sSql, srcCDocId);
		System.out.println("ParseClausePrescriptionRules.run(): DELETE FROM Stg2_Clause_Question_Conditions");
		
		int iCount = 0;
		PreparedStatement ps1 = null;
		ResultSet rs1 = null;
		try {
			
			sSql = "SELECT Stg1_Clause_Id, Conditions, Clause_Number\n" 
					+ "FROM Stg1_Raw_Clauses\n" 
					+ "WHERE Src_Doc_Id = ?\n" 
					+ "AND Conditions IS NOT NULL\n" 
					+ "AND Conditions != ''";
			ps1 = conn.prepareStatement(sSql);
			ps1.setInt(1, srcCDocId);
			rs1 = ps1.executeQuery();
			while (rs1.next()) {
				Integer v_id = rs1.getInt("Stg1_Clause_Id");
				String[] aConditions = rs1.getString("Conditions").split("\n");
				
				//String[] aConditions = CommonUtils.parseConditions(rs1.getString("Conditions"));
				String sParsedConditions = "";
				for (String sCondition : aConditions) {
					sCondition = sCondition.trim();
					if (!sCondition.isEmpty()) {
						String sProcessed = ClauseConditionsParser.convertToConditionTextProcessed(sCondition);
						if (sParsedConditions.isEmpty())
							sParsedConditions = sProcessed;
						else
							sParsedConditions += " " + sProcessed;
						insertCondition (v_id,  sProcessed, srcCDocId, conn) ;
					}
				}
			}
		}
		finally {
			SqlUtil.closeInstance(rs1, ps1, null);
		}
		System.out.println("ParseClausePrescriptionRules.run(): " + iCount);
	}
	

	/*
	public static void insertConditionOrig (Integer clauseId, String sCondition, Integer srcDocId, Connection conn) throws SQLException
	{	
		// skip these questions
		if ((sCondition.indexOf("DOCUMENT TYPE IS: SOLICITATION") > 0)
		|| (sCondition.indexOf("DOCUMENT TYPE IS: AWARD") > 0)
		|| (sCondition.indexOf("IS: INCLUDED") > 0)
		|| (sCondition.indexOf("IS: NOT INCLUDED") > 0))
			return;
		
		sCondition = sCondition.replace("IS: NOT " , "IS: ");	// ignore NOT
		
		String sSqlQuestions = "INSERT INTO Stg2_Clause_Question_Conditions (Stg1_Clause_Id, Condition_Text, Question_Name, Question_Answer) "
				+ "VALUES (?, ?, concat('[',trim(substring_index(substring_index(Condition_Text, 'IS: ', 1), ') ', -1)),']'), "
				+ "trim(substring_index(Condition_Text, 'IS: ', -1))) ";
		
		PreparedStatement psInsert = conn.prepareStatement(sSqlQuestions, Statement.RETURN_GENERATED_KEYS);

		psInsert.setInt(1, clauseId);
		psInsert.setString(2, sCondition);
		psInsert.execute();
		
		ResultSet res = psInsert.getGeneratedKeys();
		Integer clauseConditionId = 0;
		if (res.next()) {
			clauseConditionId = res.getInt(1);
		}
		
		if (clauseConditionId == 0)
			return;
		
		sSqlQuestions = "SELECT Question_Name, Question_Answer FROM Stg2_Clause_Question_Conditions WHERE Stg2_Clause_Question_Condition_Id = ?";
		
		psInsert = conn.prepareStatement(sSqlQuestions);
		psInsert.setInt(1, clauseConditionId);
		res = psInsert.executeQuery(); 
		if (!res.next()) 
			return;
			
		String qName = res.getString(1);
		String qAnswer = res.getString(2);

		sSqlQuestions = "SELECT Stg2_Question_Id FROM Stg2_Question WHERE Question_Name =  ? "
				+ "AND Stg2_Question_Id IN (SELECT Stg2_Question_Id FROM Stg2_Possible_Answer WHERE Choice_Text = ?) "
				+ "AND Src_Doc_Id = (SELECT Src_Doc_Id FROM Stg_Src_Document WHERE Src_Doc_Type_Cd = 'Q' ORDER BY Src_Doc_Id DESC LIMIT 1) ";
		psInsert = conn.prepareStatement(sSqlQuestions);
		psInsert.setString(1, qName);
		psInsert.setString(2, qAnswer);
		res = psInsert.executeQuery(); 
		Integer iQuestionId = 0;
		if (res.next()) {
			iQuestionId = res.getInt(1);
		}
		
		// One level down.
		if (iQuestionId == 0) {
			sSqlQuestions = "SELECT Stg2_Question_Id FROM Stg2_Question WHERE Question_Name =  ? "
					+ "AND Src_Doc_Id = (SELECT Src_Doc_Id FROM Stg_Src_Document WHERE Src_Doc_Type_Cd = 'Q' ORDER BY Src_Doc_Id DESC LIMIT 1) ";
			psInsert = conn.prepareStatement(sSqlQuestions);
			psInsert.setString(1, qName);
			res = psInsert.executeQuery(); 
			Integer iParentQuestionId = 0;
			if (res.next()) {
				iParentQuestionId = res.getInt(1);
			}
			
			sSqlQuestions = "SELECT Stg2_Question_Id, Question_Name FROM Stg2_Question "
					 + "WHERE Parent_Stg2_Question_Id =  ? "
					+ "AND Stg2_Question_Id IN (SELECT Stg2_Question_Id FROM Stg2_Possible_Answer WHERE Choice_Text = ?) "
					+ "AND Src_Doc_Id = (SELECT Src_Doc_Id FROM Stg_Src_Document WHERE Src_Doc_Type_Cd = 'Q' ORDER BY Src_Doc_Id DESC LIMIT 1) ";
			psInsert = conn.prepareStatement(sSqlQuestions);
			psInsert.setInt(1, iParentQuestionId);
			psInsert.setString(2, qAnswer);
			res = psInsert.executeQuery(); 
			if (res.next()) {
				iQuestionId = res.getInt(1);
				qName = res.getString(2);
			}
			
			// Two level down.
			if (iQuestionId == 0) {
				
				sSqlQuestions = "SELECT Q.Stg2_Question_Id, Q.Question_Name  "
					 + 	"FROM Stg2_Question Q "
					 + 	"INNER JOIN Stg2_Possible_Answer PA "
					 + 	"ON PA.Stg2_Question_Id = Q.Stg2_Question_Id "
					 + 	"AND PA.Choice_Text = ? "
					 + 	"AND Q.Src_Doc_Id = (SELECT Src_Doc_Id FROM Stg_Src_Document  "
					 + 	"WHERE Src_Doc_Type_Cd = 'Q' ORDER BY Src_Doc_Id DESC LIMIT 1)  ";
				
				psInsert = conn.prepareStatement(sSqlQuestions);
				psInsert.setString(1, qAnswer );
				res = psInsert.executeQuery(); 
				if (res.next()) {
					iQuestionId = res.getInt(1);
					qName = res.getString (2);
				}
			}
		}
		
		sSqlQuestions = "SELECT A.Stg2_Possible_Answer_Id  "
				+ "FROM Stg2_Possible_Answer A "
				+ "INNER JOIN Stg2_Question B "
				+ "ON B.Stg2_Question_Id = A.Stg2_Question_Id "
				+ "WHERE A.Choice_text = ?  "
				+ "AND B.Question_Name = ?  "
				+ "AND B.Src_Doc_Id = (SELECT Src_Doc_Id FROM Stg_Src_Document WHERE Src_Doc_Type_Cd = 'Q' ORDER BY Src_Doc_Id DESC LIMIT 1) ";

		psInsert = conn.prepareStatement(sSqlQuestions);
		psInsert.setString(1, qAnswer);
		psInsert.setString(2, qName);
		res = psInsert.executeQuery(); 
		Integer iPossibleAnswerId = 0;
		if (res.next()) {
			iPossibleAnswerId = res.getInt(1);
		}
		
		sSqlQuestions = "SELECT COUNT(*) FROM Stg2_Possible_Answers_Prescription "
				+ "WHERE Stg2_Possible_Answer_Id IN "
				+ "(SELECT A.Stg2_Possible_Answer_Id  "
				+ "FROM Stg2_Possible_Answer A "
				+ "INNER JOIN Stg2_Question B "
				+ "ON B.Stg2_Question_Id = A.Stg2_Question_Id "
				+ "WHERE A.Choice_text = ?  "
				+ "AND B.Question_Name = ?  "
				+ "AND B.Src_Doc_Id = (SELECT Src_Doc_Id FROM Stg_Src_Document WHERE Src_Doc_Type_Cd = 'Q' ORDER BY Src_Doc_Id DESC LIMIT 1)) ";
		
		psInsert = conn.prepareStatement(sSqlQuestions);
		psInsert.setString(1, qAnswer);
		psInsert.setString(2, qName);
		res = psInsert.executeQuery(); 
		Integer iCurrentPrescriptionCnt = 0;
		if (res.next()) {
			iCurrentPrescriptionCnt = res.getInt(1);
		}
	
		sSqlQuestions = "UPDATE Stg2_Clause_Question_Conditions "
				+ "SET ChoicePrescriptionCnt = ?, Stg2_Question_Id = ?, Question_Name = ?, Stg2_Possible_Answer_Id = ? "
				+ "WHERE Stg2_Clause_Question_Condition_Id = ?";
		psInsert = conn.prepareStatement(sSqlQuestions);
		psInsert.setInt(1, iCurrentPrescriptionCnt);
		psInsert.setInt(2, iQuestionId);
		psInsert.setString(3, qName);
		psInsert.setInt(4, iPossibleAnswerId);
		psInsert.setInt(5, clauseConditionId);
		psInsert.execute(); 
		
	}
	*/

	public static void insertCondition (Integer clauseId, String sCondition, Integer srcDocId, Connection conn) throws SQLException
	{	
		// skip these questions
		if ((sCondition.indexOf("DOCUMENT TYPE IS: SOLICITATION") > 0)
		|| (sCondition.indexOf("DOCUMENT TYPE IS: AWARD") > 0)
		|| (sCondition.indexOf("IS: INCLUDED") > 0)
		|| (sCondition.indexOf("IS: NOT INCLUDED") > 0))
			return;
		
		sCondition = sCondition.replace("IS: NOT " , "IS: ");	// ignore NOT
		
		PreparedStatement psInsert = null;
		ResultSet res = null;
		Stg2ClauseQuestionConditionRecord oRecord = new Stg2ClauseQuestionConditionRecord();
		
		sqlInsertQuestionCondition (clauseId, sCondition, srcDocId, oRecord, psInsert, res, conn);
		
		if ((oRecord.getStg2ClauseQuestionConditionId() == 0) || (oRecord.getQuestionName() == null) || (oRecord.getQuestionAnswer() == null))
			return;
				
		sqlMapAnswersToParentQuestions  (oRecord, psInsert, res, conn);
		sqlGetAnswerInfo (oRecord, psInsert, res, conn);
		sqlUpdateQuestionCondition	(oRecord, psInsert, res, conn);
		
	}
	
	private static void sqlInsertQuestionCondition (Integer clauseId, String sCondition, Integer srcDocId, Stg2ClauseQuestionConditionRecord oRecord, 
			PreparedStatement psInsert, ResultSet res, Connection conn) throws SQLException
	{	
		String sSqlQuestions = "INSERT INTO Stg2_Clause_Question_Conditions (Stg1_Clause_Id, Condition_Text, Question_Name, Question_Answer) "
				+ "VALUES (?, ?, concat('[',trim(substring_index(substring_index(Condition_Text, 'IS: ', 1), ') ', -1)),']'), "
				+ "trim(substring_index(Condition_Text, 'IS: ', -1))) ";
		
		psInsert = conn.prepareStatement(sSqlQuestions, Statement.RETURN_GENERATED_KEYS);

		psInsert.setInt(1, clauseId);
		psInsert.setString(2, sCondition);
		psInsert.execute();
		
		res = psInsert.getGeneratedKeys();
		Integer clauseConditionId = 0;
		if (res.next()) {
			clauseConditionId = res.getInt(1);
		}
		
		if (clauseConditionId == 0)
			return;
		
		sSqlQuestions = "SELECT Question_Name, Question_Answer FROM Stg2_Clause_Question_Conditions WHERE Stg2_Clause_Question_Condition_Id = ?";
		
		psInsert = conn.prepareStatement(sSqlQuestions);
		psInsert.setInt(1, clauseConditionId);
		res = psInsert.executeQuery(); 
		if (!res.next()) 
			return;
			
		String qName = res.getString(1);
		String qAnswer = res.getString(2);
		
		sSqlQuestions = "SELECT Stg2_Question_Id FROM Stg2_Question WHERE Question_Name =  ? "
				+ "AND Stg2_Question_Id IN (SELECT Stg2_Question_Id FROM Stg2_Possible_Answer WHERE Choice_Text = ?) "
				+ "AND Src_Doc_Id = (SELECT Src_Doc_Id FROM Stg_Src_Document WHERE Src_Doc_Type_Cd = 'Q' ORDER BY Src_Doc_Id DESC LIMIT 1) ";
		psInsert = conn.prepareStatement(sSqlQuestions);
		psInsert.setString(1, qName);
		psInsert.setString(2, qAnswer);
		res = psInsert.executeQuery(); 
		Integer iQuestionId = 0;
		if (res.next()) {
			iQuestionId = res.getInt(1);
		}
		
		// Save the new fields to the Stg2ClauseQuestionConditionRecord
		oRecord.setStg2ClauseQuestionConditionId(clauseConditionId);
		oRecord.setStg1ClauseId(clauseId);
		oRecord.setStg2QuestionId(iQuestionId);
		oRecord.setQuestionName(qName);
		oRecord.setQuestionAnswer(qAnswer);
	}
	
	private static void sqlMapAnswersToParentQuestions (Stg2ClauseQuestionConditionRecord oRecord, 
			PreparedStatement psInsert, ResultSet res, Connection conn) throws SQLException {
		
		// One level down.		
		if (oRecord.getStg2QuestionId() > 0) 
			return;
		
		String sSqlQuestions = "SELECT Stg2_Question_Id FROM Stg2_Question WHERE Question_Name =  ? "
				+ "AND Src_Doc_Id = (SELECT Src_Doc_Id FROM Stg_Src_Document WHERE Src_Doc_Type_Cd = 'Q' ORDER BY Src_Doc_Id DESC LIMIT 1) ";
		psInsert = conn.prepareStatement(sSqlQuestions);
		psInsert.setString(1, oRecord.getQuestionName());
		res = psInsert.executeQuery(); 
		Integer iParentQuestionId = 0;
		if (res.next()) {
			iParentQuestionId = res.getInt(1);
		}
		
		sSqlQuestions = "SELECT Stg2_Question_Id, Question_Name FROM Stg2_Question "
				 + "WHERE Parent_Stg2_Question_Id =  ? "
				+ "AND Stg2_Question_Id IN (SELECT Stg2_Question_Id FROM Stg2_Possible_Answer WHERE Choice_Text = ?) "
				+ "AND Src_Doc_Id = (SELECT Src_Doc_Id FROM Stg_Src_Document WHERE Src_Doc_Type_Cd = 'Q' ORDER BY Src_Doc_Id DESC LIMIT 1) ";
		psInsert = conn.prepareStatement(sSqlQuestions);
		psInsert.setInt(1, iParentQuestionId);
		psInsert.setString(2, oRecord.getQuestionAnswer());
		res = psInsert.executeQuery(); 
		if (res.next()) {
			oRecord.setStg2QuestionId(res.getInt(1));
			oRecord.setQuestionName(res.getString(2));
		}
		
		// Two level down.
		if (oRecord.getStg2QuestionId() == 0) {
			
			sSqlQuestions = "SELECT Q.Stg2_Question_Id, Q.Question_Name  "
				 + 	"FROM Stg2_Question Q "
				 + 	"INNER JOIN Stg2_Possible_Answer PA "
				 + 	"ON PA.Stg2_Question_Id = Q.Stg2_Question_Id "
				 + 	"AND PA.Choice_Text = ? "
				 + 	"AND Q.Src_Doc_Id = (SELECT Src_Doc_Id FROM Stg_Src_Document  "
				 + 	"WHERE Src_Doc_Type_Cd = 'Q' ORDER BY Src_Doc_Id DESC LIMIT 1)  ";
			
			psInsert = conn.prepareStatement(sSqlQuestions);
			psInsert.setString(1, oRecord.getQuestionAnswer() );
			res = psInsert.executeQuery(); 
			if (res.next()) {
				oRecord.setStg2QuestionId(res.getInt(1));
				oRecord.setQuestionName(res.getString(2));
			}
		}
		
	}
	
	private static void sqlGetAnswerInfo (Stg2ClauseQuestionConditionRecord oRecord, 
			PreparedStatement psInsert, ResultSet res, Connection conn) throws SQLException {
		
		String sSqlQuestions = "SELECT A.Stg2_Possible_Answer_Id  "
				+ "FROM Stg2_Possible_Answer A "
				+ "INNER JOIN Stg2_Question B "
				+ "ON B.Stg2_Question_Id = A.Stg2_Question_Id "
				+ "WHERE A.Choice_text = ?  "
				+ "AND B.Question_Name = ?  "
				+ "AND B.Src_Doc_Id = (SELECT Src_Doc_Id FROM Stg_Src_Document WHERE Src_Doc_Type_Cd = 'Q' ORDER BY Src_Doc_Id DESC LIMIT 1) ";

		psInsert = conn.prepareStatement(sSqlQuestions);
		psInsert.setString(1, oRecord.getQuestionAnswer());
		psInsert.setString(2, oRecord.getQuestionName());
		res = psInsert.executeQuery(); 
		if (res.next()) {
			oRecord.setStg2AnswerId(res.getInt(1));
		}
		
		sSqlQuestions = "SELECT COUNT(*) FROM Stg2_Possible_Answers_Prescription "
				+ "WHERE Stg2_Possible_Answer_Id IN "
				+ "(SELECT A.Stg2_Possible_Answer_Id  "
				+ "FROM Stg2_Possible_Answer A "
				+ "INNER JOIN Stg2_Question B "
				+ "ON B.Stg2_Question_Id = A.Stg2_Question_Id "
				+ "WHERE A.Choice_text = ?  "
				+ "AND B.Question_Name = ?  "
				+ "AND B.Src_Doc_Id = (SELECT Src_Doc_Id FROM Stg_Src_Document WHERE Src_Doc_Type_Cd = 'Q' ORDER BY Src_Doc_Id DESC LIMIT 1)) ";
		
		psInsert = conn.prepareStatement(sSqlQuestions);
		psInsert.setString(1, oRecord.getQuestionAnswer());
		psInsert.setString(2, oRecord.getQuestionName());
		res = psInsert.executeQuery(); 
		
		if (res.next()) {
			oRecord.setCntChoicePrescription(res.getInt(1));
		}
	}
	
	private static void sqlUpdateQuestionCondition (Stg2ClauseQuestionConditionRecord oRecord, 
			PreparedStatement psInsert, ResultSet res, Connection conn) throws SQLException {
		
		String sSqlQuestions = "UPDATE Stg2_Clause_Question_Conditions "
				+ "SET ChoicePrescriptionCnt = ?, Stg2_Question_Id = ?, Question_Name = ?, Stg2_Possible_Answer_Id = ? "
				+ "WHERE Stg2_Clause_Question_Condition_Id = ?";
		psInsert = conn.prepareStatement(sSqlQuestions);
		psInsert.setInt(1, oRecord.getCntChoicePrescription());
		psInsert.setInt(2, oRecord.getStg2QuestionId());
		psInsert.setString(3, oRecord.getQuestionName());
		psInsert.setInt(4, oRecord.getStg2AnswerId());
		psInsert.setInt(5, oRecord.getStg2ClauseQuestionConditionId());
		psInsert.execute();
	}
}
