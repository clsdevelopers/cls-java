package hgs.cpm.clause;

import gov.dod.cls.db.ClauseFillInRecord;
import gov.dod.cls.db.ClauseFillInTable;
import gov.dod.cls.db.ClauseRecord;
import gov.dod.cls.db.ClauseTable;
import hgs.cpm.clause.util.ClauseExportItem;
import hgs.cpm.clause.util.ClauseFillInExportItem;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class FillinCorrectionWriter {
	private static final String FILL_IN_TYPES = "('PCO', 'Y PCO/OFFEROR', 'PCO/OFFEROR', 'OFFEROR/PCO')";
	
	private static final String HEADING[] = {"CLAUSE NAME", "OLD FILLIN CODE", "OLD FILLIN TYPE",
		"OLD FILLIN MAX SIZE", "OLD FILLIN GROUP NUMBER", "OLD FILLIN PLACEHOLDER", "OLD FILLIN DEFAULT DATA",
		"OLD FILLIN DISPLAY ROWS", "OLD FILLIN FOR TABLE", "OLD FILLIN HEADING", "FILLIN MATCH CODE",
		"TEMP NEW FILLIN CODE", "CUSTOM FILLIN CODE","TEMP NEW FILLIN TYPE", "FILLIN TYPE", "TEMP NEW FILLIN SIZE", 
		"FILLIN MAX SIZE", "FILLIN GROUP NUMBER", "FILLIN PLACEHOLDER", "FILLIN DEFAULT DATA",
		"FILLIN DISPLAY ROWS", "FILLIN FOR TABLE", "FILLIN HEADING"};
	
	private static final String NO_FILLIN_DATA_LABEL = "No fillin data: ";
	
	private static final int CLAUSE_NAME = 0;
	private static final int OLD_FILLIN_CODE = 1;
	private static final int OLD_FILLIN_TYPE = 2;
	private static final int OLD_FILLIN_MAX_SIZE = 3;
	private static final int OLD_FILLIN_GROUP_NUMBER = 4;
	private static final int OLD_FILLIN_PLACEHOLDER = 5;
	private static final int OLD_FILLIN_DEFAULT_DATA = 6;
	private static final int OLD_FILLIN_DISPLAY_ROWS = 7;
	private static final int OLD_FILLIN_FOR_TABLE = 8;
	private static final int OLD_FILLIN_HEADING = 9;
//	private static final int FILLIN_CODE_MATCH = 10;
	private static final int TEMP_NEW_FILLIN_CODE = 11;
	private static final int TEMP_NEW_FILLIN_TYPE = 13;
	private static final int TEMP_NEW_FILLIN_MAX_SIZE = 15;

//	private static final int NEW_FILLIN_CODE = 12;
//	private static final int FILLIN_TYPE = 13;
//	private static final int FILLIN_MAX_SIZE = 14;
//	private static final int FILLIN_GROUP_NUMBER = 15;
//	private static final int FILLIN_PLACEHOLDER = 16;
//	private static final int FILLIN_DEFAULT_DATA = 17;
//	private static final int FILLIN_DISPLAY_ROWS = 18;
//	private static final int FILLIN_FOR_TABLE = 19;
//	private static final int FILLIN_HEADING = 20;
	
	private String outputFolder;
	private Connection conn;
	private Integer newEcfrSrcDocId;
	private Integer oldEcfrSrcDocId;
	private String folderSlash;
	private Integer clauseSrcDocId;

	private ArrayList<ClauseExportItem> newClauses;	
	private HashMap<String, String> oldRawClauseData;
	private ClauseTable oldAppClauseTable;
		
	public FillinCorrectionWriter(String outputFolder, int previousAppVersionId, 
			int oldEcfrSrcDocId, int newEcfrSrcDocId, int clauseSrcDocId, Connection conn) {
		this.conn = conn;
		this.oldEcfrSrcDocId = oldEcfrSrcDocId;
		if(this.oldEcfrSrcDocId < 1)
			this.oldEcfrSrcDocId = 0;
		this.newEcfrSrcDocId = newEcfrSrcDocId;
		this.clauseSrcDocId = clauseSrcDocId;		
		
		this.newClauses = new ArrayList<ClauseExportItem>();
		this.oldRawClauseData = new HashMap<String, String>();				
		this.oldAppClauseTable = ClauseTable.getByClauseVersion(previousAppVersionId, conn, false);
		
		this.folderSlash = System.getProperty("os.name").startsWith("Windows") ? "\\" : "/";
		this.outputFolder = outputFolder;
		if(!outputFolder.substring(outputFolder.length() - 1).equals(folderSlash))
			this.outputFolder += folderSlash;
	}
	
	public void generate() throws IOException, SQLException {
		System.out.println("\nFillinCorrectionWriter start generate");
		XSSFWorkbook workbook = null;
		FileOutputStream fileOutputStream = null;
		
		try {
			this.loadNewClauseData(false);
			this.loadNewClauseData(true);
			this.loadNewClauseFillins();
			
			this.loadOldRawClauseData(false);
			this.loadOldRawClauseData(true);
			
			fileOutputStream = new FileOutputStream(this.outputFolder + "Fillin_Corrections.xlsx");
			workbook = new XSSFWorkbook(); 
			generateFillinChangesExcel(workbook);		
		} catch (SQLException e) {
			e.printStackTrace();
		}
		finally {
			if (workbook != null) {
				workbook.write(fileOutputStream);
				workbook.close();
			}
			if (fileOutputStream != null)
				try {
					fileOutputStream.close();
				} catch (Exception oIgnore) {
					oIgnore.printStackTrace();
				}
		}
		System.out.println("FillinCorrectionWriter end generate\n");
	}
	
	public void generateFillinChangesExcel(XSSFWorkbook workbook) throws IOException, SQLException {
		System.out.println("FillinCorrectionWriter old fillin clause count: " + this.oldRawClauseData.size());
		System.out.println("FillinCorrectionWriter new fillin clause count: " + this.newClauses.size());
		int iRow = 0;
		Sheet sheet = workbook.createSheet("new sheet");
		Row headerRow = sheet.createRow(iRow++);
		for(int iCol = 0; iCol < HEADING.length; iCol++) {
			Cell iCell = headerRow.createCell(iCol);
			
			XSSFFont font = workbook.createFont();
			font.setColor(HSSFColor.BLUE.index);
			font.setBold(true);
			//Set font into style
			   
		    XSSFCellStyle style = workbook.createCellStyle();
			style.setFont(font);
			// Create a cell with a value and set style to it.
			      
			iCell.setCellStyle(style);
			iCell.setCellValue(HEADING[iCol]);
			sheet.autoSizeColumn(iCol);
		}
		
		ArrayList<String> zeroFillinClauses = new ArrayList<String>();
		
		for(ClauseExportItem oNewClauseRecord : this.newClauses) {
			String sClauseName = oNewClauseRecord.clauseNumber;
			String sOldRawData = this.oldRawClauseData.get(sClauseName);
			String sNewRawData = oNewClauseRecord.contentHtml;
			
			if(sNewRawData == null) { //removed clause in new version or parser error
				System.out.println("FillinCorrectionWriter NULL value for new raw data on clause: " + sClauseName);
			}
			else if ((sOldRawData == null) || (!sNewRawData.equals(sOldRawData))) { //Clause needs to be entered into spreadsheet
				System.out.println("FillinCorrectionWriter diff detected on: " + sClauseName);
				if(sOldRawData == null) { //New clause not present in old version OR no original version
					System.out.println("FillinCorrectionWriter NULL value for old raw data on clause: " + sClauseName);
					this.createClauseFiles(sClauseName, false); //create file for new clause
				}
				else { // There is a diff on the clause data: (!sNewRawData.equals(sOldRawData)
					this.createClauseFiles(sClauseName, true); //create file for new AND old clause
				}
				
				//*****populate new (and optionally old) clause data in spreadsheet*****
				
				ClauseRecord oOldClauseRecord = this.oldAppClauseTable.findByName(sClauseName);
				
				//figure out rows needed and load fill-ins from old clause table if applicable
				int rowsNeeded = 0;
				ClauseFillInTable oOldClauseFillins = null;
				if(sOldRawData == null || oOldClauseRecord == null)
					rowsNeeded = oNewClauseRecord.fillIns.size();
				else {
					oOldClauseFillins = ClauseFillInTable.getInstance(this.conn);
					oOldClauseFillins = oOldClauseFillins.subsetByClauseId(oOldClauseRecord.getId());
					rowsNeeded = Math.max(oOldClauseFillins.size(), oNewClauseRecord.fillIns.size());
				}
				if(rowsNeeded == 0)
					zeroFillinClauses.add(sClauseName);
				else {
					ArrayList<ClauseFillInExportItem> oNewClauseFillins = oNewClauseRecord.fillIns;
					for(int iFillin = 0; iFillin < rowsNeeded; iFillin++) {
						Row row = sheet.createRow(iRow++);
						if(oNewClauseFillins.size() > iFillin ) {
							ClauseFillInExportItem oNewFillinRecord = oNewClauseRecord.fillIns.get(iFillin);
							Cell cell = row.createCell(CLAUSE_NAME);
							cell.setCellValue(sClauseName);
								
							cell = row.createCell(TEMP_NEW_FILLIN_CODE);
							cell.setCellValue(oNewFillinRecord.code);
							
							cell = row.createCell(TEMP_NEW_FILLIN_TYPE);
							cell.setCellValue(oNewFillinRecord.type);
							
							cell = row.createCell(TEMP_NEW_FILLIN_MAX_SIZE);
							cell.setCellValue(oNewFillinRecord.maxSize);
						}
						if((oOldClauseFillins != null) && oOldClauseFillins.size() > iFillin) {//populate old clause fill in if applicable
							ClauseFillInRecord oOldFillin =  oOldClauseFillins.get(iFillin);
							
							Cell cell = row.createCell(OLD_FILLIN_CODE);
							cell.setCellValue(oOldFillin.getFillInCode());
							
							cell = row.createCell(OLD_FILLIN_TYPE);
							cell.setCellValue(oOldFillin.getFillInType());
							
							cell = row.createCell(OLD_FILLIN_MAX_SIZE);
							String sOldMaxSize = oOldFillin.getFillInMaxSize() == null? "" : oOldFillin.getFillInMaxSize().toString();
							cell.setCellValue(sOldMaxSize);
							
							Integer sFillinGroupNumber = oOldFillin.getFillInGroupNumber();
							cell = row.createCell(OLD_FILLIN_GROUP_NUMBER);
							cell.setCellValue(sFillinGroupNumber == null ? "" : sFillinGroupNumber.toString());
							
							cell = row.createCell(OLD_FILLIN_PLACEHOLDER);
							cell.setCellValue(oOldFillin.getFillInPlaceholder());						
							
							cell = row.createCell(OLD_FILLIN_DEFAULT_DATA);
							cell.setCellValue(oOldFillin.getFillInDefaultData());
							
							Integer sFillinDisplayRows = oOldFillin.getFillInDisplayRows();
							cell = row.createCell(OLD_FILLIN_DISPLAY_ROWS);
							cell.setCellValue(sFillinDisplayRows == null ? "" : sFillinDisplayRows.toString());
							
							cell = row.createCell(OLD_FILLIN_FOR_TABLE);
							cell.setCellValue(oOldFillin.isFillInForTable() ? "YES" : "NO");
							
							String sOldFillinHeading = oOldFillin.getFillInHeading();
							cell = row.createCell(OLD_FILLIN_HEADING);
							cell.setCellValue(sOldFillinHeading == null ? "" : sOldFillinHeading);
						}
					}
					//Spacing row for readability in spreadsheet
					Row spacingRow = sheet.createRow(iRow++);
					for(int iCol = 0; iCol < HEADING.length; iCol++) {
						XSSFCellStyle spacingStyle = workbook.createCellStyle();
						spacingStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND );
					//	spacingStyle.setFillBackgroundColor(new HSSFColor.BLACK().getIndex());
						
						Cell spacingCell = spacingRow.createCell(iCol);
						spacingCell.setCellStyle(spacingStyle);
					}
				}
			}
		}
		//Log clauses with no fillin data
		for(String sClauseName : zeroFillinClauses) {
			Row row = sheet.createRow(iRow++);
			Cell cell = row.createCell(CLAUSE_NAME);
			XSSFFont font = workbook.createFont();
			font.setColor(HSSFColor.RED.index);
			font.setBold(true);					   
		    XSSFCellStyle style = workbook.createCellStyle();
			style.setFont(font);
			cell.setCellStyle(style);
			cell.setCellValue(NO_FILLIN_DATA_LABEL + sClauseName);
		}		
	}
	
	private void loadNewClauseData(boolean loadAlternates) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		String sSql = null;
		
		if(loadAlternates) 
			sSql = "SELECT Clause_Number, Content_HTML, Content_Html_FillIns, Content_Html_Processed, Alt_Number\n" +
				   "FROM Stg1_Official_Clause_Alt\n" +
				   "WHERE Src_Doc_Id = ?\n" +
				   "AND CONCAT(Clause_Number, ' ', Alt_Number) IN\n" +
				      "(SELECT Clause_Number\n" +
					  "FROM Stg1_Raw_Clauses\n" +
					  "WHERE Src_Doc_Id = ?\n" +
					  "AND FILL_IN IN " + FILL_IN_TYPES + ")";
		else
			sSql = "SELECT Clause_Number, Content_HTML, Content_Html_FillIns, Content_Html_Processed\n" +
				   "FROM Stg1_Official_Clause\n" +
				   "WHERE Src_Doc_Id = ?\n" +
				   "AND Clause_Number IN\n" +
				      "(SELECT Clause_Number\n" +
				      "FROM Stg1_Raw_Clauses\n" +
				      "WHERE Src_Doc_Id = ?\n" +
				      "AND FILL_IN IN " + FILL_IN_TYPES + ")";
		
		ps = conn.prepareStatement(sSql);
		ps.setInt(1, this.newEcfrSrcDocId);	 // ecfr id
		ps.setInt(2, this.clauseSrcDocId); 	 // excel id
		rs = ps.executeQuery();
		
		while(rs.next()) {
			String rClauseNumber = rs.getString(1);
			String rContentHtml = rs.getString(2);
			String rContentHtmlFillIns = rs.getString(3);
			String rContentHtmlProcessed = rs.getString(4);
			if(loadAlternates) {
				String sAltNumber = rs.getString(5);
				rClauseNumber += " " + sAltNumber;
			}
			ClauseExportItem oClauseExportItem = new  ClauseExportItem();
			oClauseExportItem.clauseNumber = rClauseNumber;
			oClauseExportItem.contentHtml= rContentHtml;
			oClauseExportItem.contentHtmlFillIns = rContentHtmlFillIns;
			oClauseExportItem.contentHtmlProcessed = rContentHtmlProcessed;
			this.newClauses.add(oClauseExportItem);
		}
	}
	
	private void loadNewClauseFillins() throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		String sSql = null;
		for(ClauseExportItem oParsedClauseRecord : this.newClauses) {
			String sClauseName = oParsedClauseRecord.clauseNumber.toUpperCase();
			if(sClauseName.contains("ALTERNATE")) {
				String sAltNumber = sClauseName.substring(sClauseName.indexOf("ALTERNATE"));
				String sClauseNumber = sClauseName.split(" ")[0];
				sSql = "SELECT Fill_In_Code, Fill_In_Type, Fill_In_Max_Size\n" +
					   "FROM Stg1_Official_Clause_Alt JOIN Stg2_Clause_Fill_Ins_Alt\n" + 
					   "ON Stg1_Official_Clause_Alt.Stg1_Official_Clause_Alt_Id = Stg2_Clause_Fill_Ins_Alt.Stg1_Official_Clause_Alt_Id\n" +
					   "WHERE Stg1_Official_Clause_Alt.Clause_Number = ?\n" +
					   "AND Stg1_Official_Clause_Alt.Alt_Number = ?\n" +
					   "AND Stg1_Official_Clause_Alt.Src_Doc_Id = ?";
				ps = conn.prepareStatement(sSql);
				ps.setString(1,  sClauseNumber);
				ps.setString(2,  sAltNumber);
				ps.setInt(3, this.newEcfrSrcDocId);
			}
			else {
				sSql = "SELECT Fill_In_Code, Fill_In_Type, Fill_In_Max_Size\n" +
					   "FROM Stg1_Official_Clause join Stg2_Clause_Fill_Ins on\n" +
					   "Stg1_Official_Clause.Stg1_Official_Clause_Id = Stg2_Clause_Fill_Ins.Stg1_Official_Clause_Id\n" +
					   " WHERE Stg1_Official_Clause.Clause_Number = ?\n" + 
					   " AND Stg1_Official_Clause.Src_Doc_Id = ?";
				ps = conn.prepareStatement(sSql);
				ps.setString(1, sClauseName);
				ps.setInt(2, this.newEcfrSrcDocId);
			}
				
			rs = ps.executeQuery();
			while (rs.next()) {
				String rFillinCode = rs.getString(1);
				String rFillinType = rs.getString(2);
				int rFillinMaxSize = rs.getInt(3);

				ClauseFillInExportItem aFillin = new ClauseFillInExportItem();
				aFillin.code = rFillinCode;
				aFillin.type = rFillinType;
				aFillin.maxSize = rFillinMaxSize;
				oParsedClauseRecord.fillIns.add(aFillin);				
			}
		}
	}
	
	private void loadOldRawClauseData(boolean loadAlternates) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		String sSql = null;
		
		if(loadAlternates) 
			sSql = "SELECT Clause_Number, Content_HTML, Alt_Number\n" +
				   "FROM Stg1_Official_Clause_Alt\n" +
				   "WHERE Src_Doc_Id = ?\n" +
				   "AND CONCAT(Clause_Number, ' ', Alt_Number) IN\n" +
				      "(SELECT Clause_Number\n" +
					  "FROM Stg1_Raw_Clauses\n" +
					  "WHERE Src_Doc_Id = ?\n" +
					  "AND FILL_IN IN " + FILL_IN_TYPES + ")";
		else
			sSql = "SELECT Clause_Number, Content_HTML\n" +
				   "FROM Stg1_Official_Clause\n" +
				   "WHERE Src_Doc_Id = ?\n" +
				   "AND Clause_Number IN\n" +
				      "(SELECT Clause_Number\n" +
				      "FROM Stg1_Raw_Clauses\n" +
				      "WHERE Src_Doc_Id = ?\n" +
				      "AND FILL_IN IN " + FILL_IN_TYPES + ")";
		
		ps = conn.prepareStatement(sSql);
		ps.setInt(1, this.oldEcfrSrcDocId);
		ps.setInt(2, this.clauseSrcDocId);
		rs = ps.executeQuery();
		
		while(rs.next()) {
			String rClauseNumber = rs.getString(1);
			String rRawClauseData = rs.getString(2);
			if(loadAlternates) {
				String sAltNumber = rs.getString(3);
				rClauseNumber += " " + sAltNumber;
			}
			this.oldRawClauseData.put(rClauseNumber, rRawClauseData);
		}
	}
	
	private ClauseExportItem findNewClause(String clauseName) {
		for(ClauseExportItem oClauseExportItem : this.newClauses) {
			if(oClauseExportItem.clauseNumber.toUpperCase().equals(clauseName.toUpperCase()))
				return oClauseExportItem;
		}
		return null;
	}
	
	private void createClauseFiles(String clauseName, boolean bOldClauseExists) throws IOException {
		String sOutputPath = this.outputFolder + clauseName + this.folderSlash;
		new File(this.outputFolder + clauseName).mkdir();
		
		ClauseExportItem oClauseExportItem = this.findNewClause(clauseName);
		//String newClauseData = oClauseExportItem.contentHtmlProcessed == null? oClauseExportItem.contentHtmlFillIns : oClauseExportItem.contentHtmlProcessed;
		String newClauseData = oClauseExportItem.contentHtmlProcessed == null? "" : oClauseExportItem.contentHtmlProcessed;

		String newClauseDataFileName = sOutputPath + "NEW_" + clauseName + ".html";
		Files.write(Paths.get(newClauseDataFileName), newClauseData.getBytes());

		if(bOldClauseExists) {
			ClauseRecord oldClause = this.oldAppClauseTable.findByName(clauseName);
			if(oldClause == null)
				System.out.println("FillinCorrectWriter cannot find in application table: " + clauseName);
			String oldClauseData = oldClause == null? "" : oldClause.getClauseData();
			String oldClauseDataFileName = sOutputPath + "OLD_" + clauseName + ".html";
			Files.write(Paths.get(oldClauseDataFileName),oldClauseData.getBytes());
		}
	}
}