package hgs.cpm.clause.util;
import java.util.ArrayList;

public class ClauseExportItem {
	public String clauseNumber = null;
	public String contentHtml = null;
	public String contentHtmlFillIns = null;
	public String contentHtmlProcessed = null;
	public ArrayList<ClauseFillInExportItem> fillIns = new ArrayList<ClauseFillInExportItem>();
}