package hgs.cpm.clause;

import gov.dod.cls.utils.sql.SqlUtil;
import hgs.cpm.question.util.Stg2PossibleAnswerPrescriptionRecord;
import hgs.cpm.utils.SqlCommons;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class ClauseConditionsParser {
	
	private static String replaceEndWith(String sProcessed, String sSource, String sTarget) {
		if (sProcessed.endsWith(sSource)) {
			int iPos = sProcessed.indexOf(sSource);
			return sProcessed.substring(iPos) + sTarget;
		} else
			return sProcessed;
	}
	
	public static String convertToConditionTextProcessed(String sCondition) {
		String sProcessed = sCondition;
		int iPos = sProcessed.indexOf(')');
		if ((iPos > 1) && (iPos <= 3)) { // '(C )P'
			if (sProcessed.charAt(iPos - 1) == ' ') {
				sProcessed = sProcessed.substring(0, --iPos) + sProcessed.substring(iPos + 1);
			}
			if (sProcessed.charAt(iPos + 1) != ' ') {
				sProcessed = sProcessed.substring(0, iPos + 1) + " " + sProcessed.substring(iPos + 1);
			}
		}
		iPos = sProcessed.indexOf(" IS ");
		if (iPos > 0) {
			int iPos2 = sProcessed.indexOf(':');
			if (iPos2 < 0) {
				sProcessed = sProcessed.substring(0, iPos + 3) + ':' + sProcessed.substring(iPos + 3);
			}
			else if (iPos < iPos2)
				sProcessed = sProcessed.substring(0, iPos + 3) + ':' + sProcessed.substring(iPos + 3);
			else if (iPos2 == (iPos - 1)) {
				sProcessed = sProcessed.replace(": IS ", " IS: ");
			}
		}
		sProcessed = sProcessed.replace("GREATER THAN THE SAT", "GREATER THAN SAT");
		sProcessed = sProcessed.replace("GREATER THAT SAT", "GREATER THAN SAT");
		sProcessed = sProcessed.replace("::", ":");
		
		String[] aIsItems = sProcessed.split(" IS:");
		if (aIsItems.length > 1) {
			sProcessed = "";
			for (String sItem : aIsItems) {
				if (sProcessed.isEmpty())
					sProcessed = sItem;
				else {
					if (sItem.startsWith(" "))
						sProcessed += " IS:" + sItem;
					else
						sProcessed += " IS: " + sItem;
				}
			}
		}

		sProcessed = sProcessed.replace("SOLICIATION", "SOLICITATION");
		sProcessed = sProcessed.replace("SOLISITATION", "SOLICITATION");
		sProcessed = sProcessed.replace("SOLCITATION", "SOLICITATION");
		sProcessed = sProcessed.replace("SOLIITATION", "SOLICITATION");

		// [Incorrect] sProcessed = sProcessed.replace("(E) FUNDING FISCAL YEAR IS: FY2012 FUNDS", "(E)FUNDING FISCAL YEAR IS: FY2012 FUNDS");

		// [Incorrect] sProcessed = sProcessed.replace("IS: DEFENSE", "IS: DEPARTMENT OF DEFENSE') where condition_text_processed not like '%DEFENSE ADVANCED RESEARCH PROJECTS AGENCY';
		// [Incorrect] sProcessed = sProcessed.replace("IS: DEFENSE", "IS: DEPARTMENT OF DEFENSE') where condition_text_processed not like '%DEFENSE ADVANCED RESEARCH PROJECTS AGENCY';


		sProcessed = sProcessed.replace("INVITATION OF BIDS", "INVITATION FOR BIDS");
		// [Duplicated] sProcessed = sProcessed.replace("INVITATION OF BIDS", "INVITATION FOR BIDS");

		// [Not Found] sProcessed = sProcessed.replace("SET-ASIDED", "SET-ASIDE");
		 sProcessed = sProcessed.replace("NEGOTIATIONS (FAR PART 15)", "NEGOTIATION (FAR PART 15)");
		// [Not Found] sProcessed = sProcessed.replace("DEVELOPEMENT", "DEVELOPMENT");
		sProcessed = sProcessed.replace("NORTHERN MARIANA ISLANDS", "NORTHERN MARINA ISLANDS");

		sProcessed = sProcessed.replace("ARCHITECT & ENGINEERING", "ARCHITECT-ENGINEERING");

		if (sProcessed.equals("(B) DOCUMENT TYPE IS: AWAR"))
			sProcessed = "(B) DOCUMENT TYPE IS: AWARD"; // sProcessed = sProcessed.replace("(B) DOCUMENT TYPE IS: AWARD' where condition_text = '(B) DOCUMENT TYPE IS: AWAR';
		// [Not Found] sProcessed = sProcessed.replace("(D) PROCEDURES IS: NOT COMMERCIAL PROCEDURES (FAR PART 12)' where condition_text = '(D PROCEDURES IS: NOT COMMERCIAL PROCEDURES (FAR PART 12)';

		// [Not Found] sProcessed = sProcessed.replace("(FAR PART 15) (FAR PART 15)", "(FAR PART 15)");

		// [TO-DO]
		// update Stg2_Clause_Conditions set question_name = '[FUNDING FY]' where question_name = '[FUNDING FISCAL YEAR]';

		// -- Kyle's Additions
		sProcessed = replaceEndWith(sProcessed, " NONPROFIT ORGANIZATION", " NON-PROFIT ORGANIZATION");
		// replace("(F) BUSINESS TYPE IS: NON-PROFIT ORGANIZATION' where condition_text = '(F) BUSINESS TYPE IS: NONPROFIT ORGANIZATION';
		// replace("(J) BUSINESS TYPE IS: NON-PROFIT ORGANIZATION' where condition_text = '(J) BUSINESS TYPE IS: NONPROFIT ORGANIZATION';
		// replace("(D) BUSINESS TYPE IS: NON-PROFIT ORGANIZATION' where condition_text = '(D) BUSINESS TYPE IS: NONPROFIT ORGANIZATION';
		// replace("(H) BUSINESS TYPE IS: NON-PROFIT ORGANIZATION' where condition_text = '(H) BUSINESS TYPE IS: NONPROFIT ORGANITATION';

		sProcessed = replaceEndWith(sProcessed, " HISTORICALLY UNDERUTILIZED BUSINESS ZONE", " HISTORICALLY UNDERUTILIZED BUSINESS ZONE (HUBZone)");
		// replace("(E) BUSINESS TYPE IS: HISTORICALLY UNDERUTILIZED BUSINESS ZONE (HUBZone)' where condition_text = '(E) BUSINESS TYPE IS: HISTORICALLY UNDERUTILIZED BUSINESS ZONE ';

		// [Not Found] replace("(C) BUSINESS TYPE IS: FOREIGN SOURCE' where condition_text = '(C) BUSINESS TYPE IS: FOREIGN';

		sProcessed = replaceEndWith(sProcessed, " CONTRACT TYPE IS: TIME AND MATERIAL", " CONTRACT TYPE IS: TIME AND MATERIALS");
		// replace("(D) CONTRACT TYPE IS: TIME AND MATERIALS' where condition_text = '(D) CONTRACT TYPE IS: TIME AND MATERIAL';
		// replace("(I) CONTRACT TYPE IS: TIME AND MATERIALS' where condition_text = '(I) CONTRACT TYPE IS: TIME AND MATERIAL';

		// Correct question should be [SOLICITATION TYPE] other than [DOCUMENT TYPE]
		sProcessed = replaceEndWith(sProcessed, " DOCUMENT TYPE IS: INVITATION FOR BID", " DOCUMENT TYPE IS: INVITATION FOR BIDS");
		// replace("(B) DOCUMENT TYPE IS: INVITATION FOR BIDS' where condition_text = '(B) DOCUMENT TYPE IS: INVITATION FOR BID';

		// sProcessed = replaceEndWith(sProcessed, " CONTRACT TYPE IS: FIXED PRICE WITH ECONOMIC PRICE ADJUSTMENT-ACTUAL COSTS", " CONTRACT TYPE IS: FIXED PRICE WITH ECONOMIC PRICE ADJUSTMENT -- ACTUAL COSTS");
		// replace("(J) CONTRACT TYPE IS: FIXED PRICE WITH ECONOMIC PRICE ADJUSTMENT -- ACTUAL COSTS' where condition_text = '(J) CONTRACT TYPE IS: FIXED PRICE WITH ECONOMIC PRICE ADJUSTMENT-ACTUAL COSTS';

		// Correct question should be [AWARD TYPE] other than [CONTRACT TYPE]
		sProcessed = replaceEndWith(sProcessed, " CONTRACT TYPE IS: INDEFINITE DELIVERY", " CONTRACT TYPE IS: INDEFINITE DELIVERY CONTRACT");
		// replace("(F) CONTRACT TYPE IS: INDEFINITE DELIVERY CONTRACT' where condition_text = '(F) CONTRACT TYPE IS: INDEFINITE DELIVERY';

		// replace("(H) MULTIPLE OR MULTI YEAR IS: MULTIPLE YEAR CONTRACT' where condition_text = '(H) MULTIPLE OR MULTI YEAR IS: MULTIPLE YEAR';
		// replace("(I) MULTIPLE OR MULTI YEAR IS: NOT MULTIPLE YEAR CONTRACT' where condition_text = '(I) MULTIPLE OR MULTI YEAR IS: NOT MULTIPLE YEAR';
		// replace("(G) MULTIPLE OR MULTI YEAR IS: MULTIPLE YEAR CONTRACT' where condition_text = '(G) MULTIPLE OR MULTI YEAR: IS MULTIPLE';
		// replace("(C) MULTIPLE OR MULTI YEAR IS: MULTI-YEAR CONTRACT' where condition_text = '(C) MULTIPLE OR MULTI-YEAR IS: MULTI-YEAR';
		// replace("(B) PERFORMANCE REQUIREMENTS IS: NOT LISTED IN THE DOD ACQUISITION STREAMLINING AND STANDARDIZATION INFORMATION SYSTEM (ASSIST) NOT FURNISHED WITH THE SOLICITATION' where condition_text = '(B) PERFORMANCE REQUIREMENTS IS: NOT LISTED IN THE DOD ACQUISITION STREAMLINING AND STANDARDIZATION INFORMATION SYSTEM (ASSIST)';
		// replace("(F) PLACE TYPE IS: GOVERNMENT OWNED OR LEASED FACILITIES/INSTALLATIONS' where condition_text = '(F) PLACE OF PERFORMANCE GOVERNMENT OWNED OR LEASED FACILITIES/INSTALLATIONS';
		// replace("(E) PLACE OF USE IS: FOR USE INSIDE THE UNITED STATES (50 STATES, DISTRICT OF COLUMBIA, OR UNITED STATES OUTLYING AREAS)' where condition_text = '(E) PLACE OF USE IS: FOR USE INSIDE THE UNITED STATES';

		// replace("CONTRACT TYPE IS: COST NO FEE' where condition_text like '%CONTRACT TYPE IS: COST';
		// replace("DOCUMENT TYPE IS: AWARD' where condition_text like '%DOCUMENT TYPE IS: AWAR';

		// sProcessed = sProcessed.replace("LABOR-HOUR", "LABOR HOUR");
		// sProcessed = sProcessed.replace("FIXED PRICE INCENTIVE (SUCCESSIVE TARGET)", "FIXED PRICE INCENTIVE (SUCCESSIVE TARGETS)");
		// sProcessed = sProcessed.replace("FIXED PRICE RE-DERMINATION PROSPECTIVE", "FIXED PRICE RE-DETERMINATION PROSPECTIVE");
		// sProcessed = sProcessed.replace("FIXED PRICE RE-DERMINATION PROSPECTIVE", "FIXED PRICE RE-DETERMINATION PROSPECTIVE");
		// sProcessed = sProcessed.replace("(A: DOCUMENT TYPE IS: SOLICITATION", "(A) DOCUMENT TYPE IS: SOLICITATION");
		// sProcessed = sProcessed.replace("(C)EXCEPTIONS", "(C) EXCEPTIONS");
		// sProcessed = sProcessed.replace("(C)SERVICES", "(C) SERVICES");
		// sProcessed = sProcessed.replace("(D)PLACE OF PERFORMANCE", "(D) PLACE OF PERFORMANCE");
		// sProcessed = sProcessed.replace("(D)SECURITY REQUIREMENTS", "(D) SECURITY REQUIREMENTS");
		// sProcessed = sProcessed.replace("(E)FUNDING FISCAL YEAR]", "(E) FUNDING FISCAL YEAR");
		// sProcessed = sProcessed.replace("AWARD SCENARIO\'S", "AWARD SCENARIOS");
		// sProcessed = sProcessed.replace("AWARD AFTER DISCUSSIONS WITH CONTRACTORS IN THE COMPETITIVE RANGE", "AWARD AFTER DISCUSSIONS WITH OFFERORS IN THE COMPETITIVE RANGE");
		// sProcessed = sProcessed.replace("BID GUARANTEE OR BOND IS:PAYMENT BONDS APPLY", "BID GUARANTEE OR BOND IS: PAYMENT BONDS APPLY");
		// sProcessed = sProcessed.replace("BID GUARANTEE OR BOND IS:PERFORMANCE BONDS APPLY", "BID GUARANTEE OR BOND IS: PERFORMANCE BONDS APPLY");
		// sProcessed = sProcessed.replace("OGVERNMENTAL", "GOVERNMENTAL");
		// sProcessed = sProcessed.replace("DELIVERD", "DELIVERED");
		// sProcessed = sProcessed.replace("BUSINESS ORGANIZATION IS: STATE GOVERNMENT", "BUSINESS TYPE IS: STATE GOVERNMENT");
		// sProcessed = sProcessed.replace("BUSINESS ORGANIZATION IS: LOCAL GOVERNMENT", "BUSINESS TYPE IS: LOCAL GOVERNMENT");
		// sProcessed = sProcessed.replace("UNDEROMB", "UNDER OMB");
		// sProcessed = sProcessed.replace("BUSINESS TYPE IS: NONPROFIT ORGANIZATION", "BUSINESS TYPE IS: NON-PROFIT ORGANIZATION");
		// sProcessed = sProcessed.replace("FOREIGN CORPORATION WHOLLY OWNED BY FOREIGN GOVERNMENTS", "FOREIGN CORPORATION WHOLLY OWNED BY A FOREIGN GOVERNMENT");
		// sProcessed = sProcessed.replace("REPRESENTATIVE OF FOREIGN GOVERNMENT", "REPRESENTATIVE OF A FOREIGN GOVERNMENT");
		// sProcessed = sProcessed.replace("COMMERCIAL ITEMS IS:NOT THE SUPPLIES ARE COMMERCIAL ITEMS", "COMMERCIAL ITEMS IS: NOT THE SUPPLIES ARE COMMERCIAL ITEMS");
		// sProcessed = sProcessed.replace("(U) COMMERCIAL ITEMS IS: THE SUPPLIES ARE COMMERCIAL ITEM", "(U) COMMERCIAL ITEMS IS: THE SUPPLIES ARE COMMERCIAL ITEMS");
		// sProcessed = sProcessed.replace("COMMERCIAL SERVICES IS:NO", "COMMERCIAL SERVICES IS: NO");
		// sProcessed = sProcessed.replace("CONSTRACT TYPE", "CONTRACT TYPE");
		// sProcessed = sProcessed.replace("(E)FUNDING FISCAL YEAR", "(E) FUNDING FISCAL YEAR");
		// sProcessed = sProcessed.replace("CONTRACT TYPE IS: LETTER CONTRACT", "AWARD TYPE IS: LETTER CONTRACT");
		// sProcessed = sProcessed.replace("LABOR HOURS", "LABOR HOUR");
		// sProcessed = sProcessed.replace("CONTRACT VALUE IS:100000000", "CONTRACT VALUE IS: 100000000");
		// sProcessed = sProcessed.replace("VALUE: IS", "VALUE IS:");
		// sProcessed = sProcessed.replace("IS:YES", "IS: YES");
		// sProcessed = sProcessed.replace("FIXED PRICE WITH ECONOMIC PRICE ADJUSTMENT --COST INDICIES", "FIXED PRICE WITH ECONOMIC PRICE ADJUSTMENT -- COST INDICIES");
		// sProcessed = sProcessed.replace("IS : AWARD", "IS: AWARD");
		// sProcessed = sProcessed.replace("IS:SOLICITATION", "IS: SOLICITATION");
		// sProcessed = sProcessed.replace("CONTRACT PRICE IS:", "CONTRACT TYPE IS:");
		// sProcessed = sProcessed.replace("DEILIVERY BASIS", "DELIVERY BASIS");
		// sProcessed = sProcessed.replace("INFORMATION IS SUBMITTED TO OTHER THAN THEPAYMENT OFFICE", "INFORMATION IS SUBMITTED TO OTHER THAN THE PAYMENT OFFICE");
		// sProcessed = sProcessed.replace("TOEVALUATE", "TO EVALUATE");
		// sProcessed = sProcessed.replace("(R) EXCEPTIONS IS: 5.202 -- EXCEPTION TO THE BUY AMERICAN STATUTE", "(R) EXCEPTIONS IS: 25.202 -- EXCEPTION TO THE BUY AMERICAN STATUTE");
		// sProcessed = sProcessed.replace("223.7102 -- NOT EXCEPTION TO 223.7102 (STORAGE AND DISPOSAL OF TOXIC AND HAZARDOUS MATERIALS)", "NOT 223.7102 -- EXCEPTION TO 223.7102 (STORAGE AND DISPOSAL OF TOXIC AND HAZARDOUS MATERIALS)");
		// -- // sProcessed = sProcessed.replace("EXCESSIVE PASS-THROUGH CHARGES", "EXCESS PASS-TRHOUGH CHARGES");
		// sProcessed = sProcessed.replace("22.1003-3/4 -- NOT EXEMPT FROM APPLICATION OF THE SERVICE CONTRACT ACT", "NOT 22.1003-3/4 -- EXEMPT FROM APPLICATION OF THE SERVICE CONTRACT ACT");
		// sProcessed = sProcessed.replace("22.1203-2 EXEMPT FROM 52.222-17, "NONDISPLACEMENT OF QUALIFIED WORKERS"", "22.1203-2 -- EXEMPT FROM 52.222-17, "NONDISPLACEMENT OF QUALIFIED WORKERS"");
		// sProcessed = sProcessed.replace("22.1603 -- SECRETARIAL EXEMPTION TO THE  REQUIREMENT TO INCLUDE THE CLAUSE 52.222-40,  "NOTIFICATION OF EMPLOYEE RIGHTS UNDER THE NATIONAL LABOR RELATIONS ACT"', '22.1603 -- SECRETARIAL EXEMPTION TO THE REQUIREMENT TO INCLUDE THE CLAUSE 52.222-40, "NOTIFICATION OF EMPLOYEE RIGHTS UNDER THE NATIONAL LABOR RELATIONS ACT");
		// sProcessed = sProcessed.replace("23.204 -- ENERGY STAR® OR FEMP-DESIGNATED PRODUCT EXEMPTION", "23.204 -- ENERGY STAR OR FEMP-DESIGNATED PRODUCT EXEMPTION");
		// sProcessed = sProcessed.replace("F.O.B. ORIGIN DESIGNATIONS", "FOB ORIGIN DESIGNATIONS");
		// sProcessed = sProcessed.replace("(C) F.O.B. ORIGIN FREIGHT PREPAID", "(C) FOB ORIGIN DESIGNATIONS IS: F.O.B. ORIGIN FREIGHT PREPAID");
		// sProcessed = sProcessed.replace("(D) FINANCING ARRANGEMENTS IS: ADVANCED PAYMENTS", "(D) FINANCING ARRANGEMENTS IS: ADVANCE PAYMENTS");
		// sProcessed = sProcessed.replace("FIRST ARTICLE CONDITIONS IS:contractor IS AUTHORIZED TO PURCHASE MATERIAL OR COMMENCE PRODUCTION BEFORE FIRST ARTICLE APPROVAL", "FIRST ARTICLE CONDITIONS IS: CONTRACTOR IS AUTHORIZED TO PURCHASE MATERIAL OR COMMENCE PRODUCTION BEFORE FIRST ARTICLE APPROVAL");
		// sProcessed = sProcessed.replace("TESTING IS REQUIRED.", "TESTING IS REQUIRED");
		// sProcessed = sProcessed.replace("FIXED PRICE INCENTIVE (FIRM TARGETS)", "FIXED PRICE INCENTIVE (FIRM TARGET)");
		// sProcessed = sProcessed.replace("FIRST ARTICLE TESTER", "FIRST ARTICLE CONDITIONS");
		// sProcessed = sProcessed.replace("FOB DISIGNATION", "FOB DESIGNATION");
		// sProcessed = sProcessed.replace("SF1447", "SF 1447");
		// sProcessed = sProcessed.replace("DOCUMENT VALUE", "CONTRACT VALUE");
		// sProcessed = sProcessed.replace("6.302-5   AUTHORIZED OR REQUIRED BY STATUTE.", "6.302-5   AUTHORIZED OR REQUIRED BY STATUTE");
		// sProcessed = sProcessed.replace("6.302-2 UNSUAL AND COMPELLING URGENCY", "6.302-2   UNSUAL AND COMPELLING URGENCY");
		// sProcessed = sProcessed.replace("FUNDING FISCAL YEAR IS:", "FUNDING FY IS:");
		// sProcessed = sProcessed.replace("FUNDING FY IS:FY", "FUNDING FY IS: FY");
		// sProcessed = sProcessed.replace("SUPPLY SOUCE", "SUPPLY SOURCE");
		// sProcessed = sProcessed.replace("(D) INCLUSION OF SERVICES: YES", "(D) INCLUSION OF SERVICES IS: YES");
		// sProcessed = sProcessed.replace("INSPECTION AND ACCEPTANCE REQUIREMENTS IS: GOVERNMENT REQUIRES AN EXPLICIT UNDERSTANDING OF THE CONTRACTOR\'S INSPECTION RESPONSIBILITIES", "INSPECTION REQUIREMENTS IS: GOVERNMENT REQUIRES AN EXPLICIT UNDERSTANDING OF THE CONTRACTOR\'S INSPECTION RESPONSIBILITIES");
		// sProcessed = sProcessed.replace("INSPECTION AND ACCEPTANCE REQUIREMENTS IS: A HIGHER LIEVEL CONTRACT QUALITY REQUIREMENT IS APPROPRIATE", "INSPECTION REQUIREMENTS IS: A HIGHER LIEVEL CONTRACT QUALITY REQUIREMENT IS APPROPRIATE");
		// sProcessed = sProcessed.replace("INSPECTION AND ACCEPTANCE REQUIREMENTS IS: GOVERNMENT WILL RELY ON CONTRACTOR INSPECTION", "INSPECTION REQUIREMENTS IS: GOVERNMENT WILL RELY ON CONTRACTOR INSPECTION");
		// sProcessed = sProcessed.replace("INSPECTION AND ACCEPTANCE REQUIREMENTS IS: CERTIFICATE OF CONFORMANCE IS IN THE BEST INTEREST OF THE GOVERNMENT IN LIEU OF SOURCE INSPECTION", "ACCEPTANCE REQUIREMENTS IS: CERTIFICATE OF CONFORMANCE IS IN THE BEST INTEREST OF THE GOVERNMENT IN LIEU OF SOURCE INSPECTION");
		// sProcessed = sProcessed.replace("INSPECTION AND ACCEPTANCE REQUIREMENTS IS: GOVERNMENT INSPECTION AND ACCEPTANCE ARE TO BE PERFORMED AT THE CONTRACTOR\'S PLANT", "ACCEPTANCE REQUIREMENTS IS: GOVERNMENT INSPECTION AND ACCEPTANCE ARE TO BE PERFORMED AT THE CONTRACTOR\'S PLANT");
		// sProcessed = sProcessed.replace("INSPECTION AND ACCEPTANCE REQUIREMENTS IS: INSPECTION AND ACCEPTANCE WILL OCCUR AT ORIGIN", "ACCEPTANCE REQUIREMENTS IS: INSPECTION AND ACCEPTANCE REQUIREMENTS IS: INSPECTION AND ACCEPTANCE WILL OCCUR AT ORIGIN");
		// sProcessed = sProcessed.replace("INSPECTION AND ACCEPTANCE REQUIREMENTS IS: CONTRACTOR WILL PERFORM INFORMATION ASSURANCE FUNCTION", "ACCEPTANCE REQUIREMENTS IS: CONTRACTOR WILL PERFORM INFORMATION ASSURANCE FUNCTION");
		// sProcessed = sProcessed.replace("INTEREST ON PARTIAL PAYMENTS: YES", "INTEREST ON PARTIAL PAYMENTS IS: YES");
		// sProcessed = sProcessed.replace("ISSUING OFFICEY", "ISSUING OFFICE");
		// sProcessed = sProcessed.replace("ISSING OFFICE", "ISSUING OFFICE");
		// sProcessed = sProcessed.replace("ISSUING OFFICE IS:DEPARTMENT", "ISSUING OFFICE IS: DEPARTMENT");
		// sProcessed = sProcessed.replace("DEPARTMENT OF DEFENSE THREAT REDUCTION AGENCY", "DEFENSE THREAT REDUCTION AGENCY");
		// sProcessed = sProcessed.replace("SUBMISSION OF LABOR AGREEMENT FROM SUCCESSFUL CONTRACTOR PRIOR TO AWARD", "SUBMISSION OF LABOR AGREEMENT FROM SUCCESSFUL OFFEROR PRIOR TO AWARD");
		// sProcessed = sProcessed.replace("RECRUITMENT IS MADE OUTSIDE THE UNITED STATES (50 PLUS DISTRICT OF COLUMBIA)", "RECRUITMENT IS MADE OUTSIDE THE UNITED STATES (50 STATES PLUS DISTRICT OF COLUMBIA)");
		// sProcessed = sProcessed.replace("RECRUITMENT IS MADE OUTSIDE THE UNITED STATES (50 PLUS DISTRICT OF COLUMBIA), PUERTO RICO, THE NORTHERN MARINA ISLANDS, AMERICAN SAMOA, GUAM, THE U.S VIRGIN ISLANDS AND WAKE ISLAND", "RECRUITMENT IS MADE OUTSIDE THE UNITED STATES (50 STATES PLUS DISTRICT OF COLUMBIA), PUERTO RICO, THE NORTHERN MARINA ISLANDS, AMERICAN SAMOA, GUAM, THE U.S VIRGIN ISLANDS AND WAKE ISLAND");
		// sProcessed = sProcessed.replace("THE CONTRACTOR INTENDS TO USE MEMBERS OF THE SELECTIVE RESERVE", "THE CONTRACTOR INTENDS TO USE MEMBERS OF THE SELECTIVE RESERVE AND THIS WAS AN EVALUATION FACTOR') where condition_text = 'THE CONTRACTOR INTENDS TO USE MEMBERS OF THE SELECTIVE RESERVE';
		// sProcessed = sProcessed.replace("(C) LAUNDRY AND DRY CLEANING (BULK WEIGHT BASIS)", "(C) LAUNDRY AND DRY CLEANING IS: LAUNDRY AND DRY CLEANING (BULK WEIGHT BASIS)");
		// sProcessed = sProcessed.replace("(C) LAUNDRY AND DRY CLEANING (COUNT BASIS)", "(C) LAUNDRY AND DRY CLEANING IS: LAUNDRY AND DRY CLEANING (COUNT BASIS)");
		// sProcessed = sProcessed.replace("LAUNDRY AND DRY CLEANING IS:LAUNDRY", "LAUNDRY AND DRY CLEANING IS: LAUNDRY");
		// sProcessed = sProcessed.replace("(D) LEGISLATIVE SOURCE OF FUNDS IS: RECOVERY ACT FUNDS", "(D) LEGISLATIVE SOURCE OF FUNDS IS: RECOVERY ACT FUNDS");
		// sProcessed = sProcessed.replace("(I) LEGISLATIVE SOURCE OF FUNDS IS: NOT RECOVERY ACT FUNDS", "(I) LEGISLATIVE SOURCE OF FUNDS IS: NOT RECOVERY ACT FUNDS");
		// sProcessed = sProcessed.replace("FUNDS APPROPRIATED BY THE DEPARTMENT OF DEFENSE APPROPRIATIONS ACT, 2014 (PUBL L. 113-76, DIVISIONS C AND J)", "FUNDS APPROPRIATED BY THE DEPARTMENT OF DEFENSE APPROPRIATIONS ACT, 2014 AND BY THE MILITARY CONSTRUCTION AND VETERANS AFFAIRS AND RELATED AGENCIES APPROPRIATIONS ACT, 2014 (PUBL L. 113-76, DIVISIONS C AND J)");
		// sProcessed = sProcessed.replace("FUNDS MADE AVAILABLE BY THE CONTINUING APPROPRIATIONS RESOLUTION, 2013 (PUB. L 112-175), DOD APPROPRIATIONS", "FUNDS MADE AVAILABLE BY THE CONTINUING APPROPRIATIONS RESOLUTION, 2013  (PUB. L 112-175), DOD MILITARY CONSTRUCTION APPROPRIATIONS");
		// sProcessed = sProcessed.replace("MATERIAL CONTENT:", "MATERIAL CONTENT IS:");
		// sProcessed = sProcessed.replace("MISCELLANEOUS PROGRAM CHARACTERISTICS: THE", "MISCELLANEOUS PROGRAM CHARACTERISTICS IS: THE");
		// sProcessed = sProcessed.replace("MISCELLANEOUS PROGRAM CHARACTERISTICS: THIS", "MISCELLANEOUS PROGRAM CHARACTERISTICS IS: THIS");
		// sProcessed = sProcessed.replace("MISCELLANEOUS PROGRAM INFORMATION", "MISCELLANEOUS PROGRAM CHARACTERISTICS");
		// sProcessed = sProcessed.replace("MISCELLANEOUS PROGRAMS IS:", "MISCELLANEOUS PROGRAM CHARACTERISTICS IS:");
		// sProcessed = sProcessed.replace("NO TIME TO PROCESS BUY AMERICAN DETERMINATION: IS YES", "NO TIME TO PROCESS BUY AMERICAN DETERMINATION IS: YES");
		// sProcessed = sProcessed.replace("STANDARD PAYMENT INSTRUCTIONS APPLY (PGI 4204.7108)", "STANDARD PAYMENT INSTRUCTIONS APPLY (PGI 204.7108)");
		// sProcessed = sProcessed.replace("(C) PERFORMANCE REQUIREMENTS: EARNED VALUE MANAGEMENT SYSTEM", "GOVERNMENT REVIEWS IS: EARNED VALUE MANAGEMENT SYSTEM");
		// sProcessed = sProcessed.replace("(C) PERFORMANCE REQUIREMENTS: ITEM IDENTIFICATION, VALUATION, OR BOTH ARE REQUIRED", "GOVERNMENT REVIEWS IS: "ITEM IDENTIFICATION, VALUATION, OR BOTH ARE  REQUIRED");
		// sProcessed = sProcessed.replace("THE ACQUISITION IS SUBJECT TO A QUALIFICATION REQUIREMENT.", "THE ACQUISITION IS SUBJECT TO A QUALIFICATION REQUIREMENT");
		// sProcessed = sProcessed.replace("PERFORMANCE REQUIREMENTS IS: FIRST ARTICLE APPROVAL IS REQUIRED", "GOVERNMENT REVIEWS IS: FIRST ARTICLE APPROVAL IS REQUIRED");
		// sProcessed = sProcessed.replace("PERFORMANCE REQUIREMENTS IS: MAKE OR BUY PROGRAM", "GOVERNMENT REVIEWS IS: MAKE OR BUY PROGRAM");
		// sProcessed = sProcessed.replace("PERFORMANCE REQUIREMENTS IS: INTEGRATED BASELINE REVIEW (PRIOR TO AWARD)", "GOVERNMENT REVIEWS IS: INTEGRATED BASELINE REVIEW (PRIOR TO AWARD)");
		// sProcessed = sProcessed.replace("PERFORMANCE REQUIREMENTS IS: INTEGRATED BASELINE REVIEW (AFTER AWARD)", "GOVERNMENT REVIEWS IS: INTEGRATED BASELINE REVIEW (AFTER AWARD)");
		// sProcessed = sProcessed.replace("PERFORMANCE REQUIREMENTS IS: EARNED VALUE MANAGEMENT SYSTEM", "GOVERNMENT REVIEWS IS: EARNED VALUE MANAGEMENT SYSTEM");
		// sProcessed = sProcessed.replace("PERFORMANCE REQUIREMENTS IS: RADIO FREQUENCY IDENTIFICATION (RFID) IS REQUIRED", "GOVERNMENT REVIEWS IS: RADIO FREQUENCY IDENTIFICATION (RFID) IS REQUIRED");
		// sProcessed = sProcessed.replace("PERFORMANCE REQUIREMENTS IS: USE OF A LEAD SYSTEMS INTEGRATOR", "GOVERNMENT REVIEWS IS: USE OF A LEAD SYSTEMS INTEGRATOR");
		// sProcessed = sProcessed.replace("PLACE OF ISSUANCE: ", "PLACE OF ISSUANCE IS: ");
		// sProcessed = sProcessed.replace("PERFORMACE", "PERFORMANCE");
		// sProcessed = sProcessed.replace("(E) PLACE OF PERFORMANCE IS : NOT UNITED STATES OUTLYING AREAS", "(E) PLACE OF PERFORMANCE IS: NOT UNITED STATES OUTLYING AREAS");
		// sProcessed = sProcessed.replace("(I) PLACE OF PERFORMANCE IS :AMERICAN SAMOA", "(I) PLACE OF PERFORMANCE IS: AMERICAN SAMOA");
		// sProcessed = sProcessed.replace("(L) PLACE OF PERFORMANCE IS:AMERICA SAMOA", "(L) PLACE OF PERFORMANCE IS: AMERICA SAMOA");
		// sProcessed = sProcessed.replace("(K) PLACE OF PERFORMANCE IS:GUAM", "(K) PLACE OF PERFORMANCE IS: GUAM");
		// sProcessed = sProcessed.replace("(J) PLACE OF PERFORMANCE IS:NORTHERN MARINA ISLANDS", "(J) PLACE OF PERFORMANCE IS: NORTHERN MARINA ISLANDS");
		// sProcessed = sProcessed.replace("(E) PLACE OF PERFORMANCE IS:PUERTO RICO", "(E) PLACE OF PERFORMANCE IS: PUERTO RICO");
		// sProcessed = sProcessed.replace("(K) PLACE OF PERFORMANCE IS:U.S. VIRGIN ISLANDS", "(K) PLACE OF PERFORMANCE IS: U.S. VIRGIN ISLANDS");
		// sProcessed = sProcessed.replace("(J) PLACE OF PERFORMANCE IS:UNITED STATES VIRGIN ISLANDS", "(J) PLACE OF PERFORMANCE IS: UNITED STATES VIRGIN ISLANDS");
		// sProcessed = sProcessed.replace("PLACE OF PERFORMANCE IS: UNITED STATES (50 PLUS DISCRICT OF COLUMBIA)", "PLACE OF PERFORMANCE IS: UNITED STATES (50 STATES PLUS DISTRICT OF COLUMBIA)");
		// sProcessed = sProcessed.replace("PLACE OF PERFORMANCE IS: UNITED STATES( 50 STATES PLUS DISTRICT OF COLUMBIA)", "PLACE OF PERFORMANCE IS: UNITED STATES (50 STATES PLUS DISTRICT OF COLUMBIA)");
		// sProcessed = sProcessed.replace("PLACE OF PERFORMANCE IS: UNITED STATES(50 STATES PLUS DISTRICT OF COLUMBIA)", "PLACE OF PERFORMANCE IS: UNITED STATES (50 STATES PLUS DISTRICT OF COLUMBIA)");
		// sProcessed = sProcessed.replace("PLACE OF PERFORMANCE IS: UNITED STATES( 50 PLUS DISTRICT OF COLUMBIA)", "PLACE OF PERFORMANCE IS: UNITED STATES (50 STATES PLUS DISTRICT OF COLUMBIA)");
		// sProcessed = sProcessed.replace("PLACE OF PERFORMANCE IS: UNITED STATES (50 PLUS DISTRICT OF COLUMBIA)", "PLACE OF PERFORMANCE IS: UNITED STATES (50 STATES PLUS DISTRICT OF COLUMBIA)");
		// sProcessed = sProcessed.replace("PLACE OF PERFORMANCE IS: UNITED STATES (50 STAPLUS DISTRICT OF COLUMBIA)", "PLACE OF PERFORMANCE IS: UNITED STATES (50 STATES PLUS DISTRICT OF COLUMBIA)");
		// sProcessed = sProcessed.replace("PLACE OF PERFORMANCE IS: CONTRACTOR OPERATED (GOCO)", "PLACE TYPE IS: CONTRACTOR OPERATED (GOCO)");
		// sProcessed = sProcessed.replace("PLACE OF PERFORMANCE IS: GOVERNMENT OPERATED", "PLACE TYPE IS: GOVERNMENT OPERATED");
		// sProcessed = sProcessed.replace("PLACE OF PERFORMANCE IS: GOVERNMENT OWNED OR LEASED FACILITIES/INSTALLATIONS", "PLACE TYPE IS: GOVERNMENT OWNED OR LEASED FACILITIES/INSTALLATIONS");
		// sProcessed = sProcessed.replace("PLACE OF PERFORMANCE IS: UNITED STATES(50 PLUS DISTRICT OF COLUMBIA)", "PLACE OF PERFORMANCE IS: UNITED STATES (50 STATES PLUS DISTRICT OF COLUMBIA)");
		// sProcessed = sProcessed.replace("PLACE OF PERFORMANCE IS: UNITED STATES VIRGIN ISLANDS", "PLACE OF PERFORMANCE IS: VIRGIN ISLANDS");
		// sProcessed = sProcessed.replace("ASSOCIATED TERRITORIAL WATERS AND AIRSPACE.", "ASSOCIATED TERRITORIAL WATERS AND AIR SPACE");
		// sProcessed = sProcessed.replace("ASSOCIATED TERRITORIAL WATERS AND AIRSPACE", "ASSOCIATED TERRITORIAL WATERS AND AIR SPACE");
		// sProcessed = sProcessed.replace("PLACE OF PERFORMANCE IS: VEHICLES, GOVERNMENT OWNED OR LEASED", "PLACE TYPE IS: VEHICLES, GOVERNMENT OWNED OR LEASED");
		// sProcessed = sProcessed.replace("UNITED STATES OULYING AREAS", "UNITED STATES OUTLYING AREAS");
		// sProcessed = sProcessed.replace("AMERICA SAMOA", "AMERICAN SAMOA");
		// sProcessed = sProcessed.replace("(C) PLACE OF PERFORMANCE IS: OVERNMENT OWNED OR LEASED FACILITIES/INSTALLATIONS", "(C) PLACE TYPE IS: GOVERNMENT OWNED OR LEASED FACILITIES/INSTALLATIONS");
		// sProcessed = sProcessed.replace("(G) PLACE OF PERFORMANCE IS: NOT UNITED STATES OUTLYING AREA", "(G) PLACE OF PERFORMANCE IS: NOT UNITED STATES OUTLYING AREAS");
		// sProcessed = sProcessed.replace("PLACE OF PERFORMANCE IS: SPANISH-AMERICAN INSTALLATIONS AND FACILITIES", "PLACE TYPE IS: SPANISH-AMERICAN INSTALLATIONS AND FACILITIES");
		// sProcessed = sProcessed.replace("PLACE OF PERFORMANCE IS: NEAR OR ON NAVIGABLE WATERWAYS", "PLACE TYPE IS: NEAR OR ON NAVIGABLE WATERWAYS");
		// sProcessed = sProcessed.replace("PLACE OF PERFORMANCE IS: AT OR NEAR AIRFIELDS", "PLACE TYPE IS: AT OR NEAR AIRFIELDS");
		// sProcessed = sProcessed.replace("PLACE OF PERFORMANCE IS: FACILITY HOLDING DETAINEES", "PLACE TYPE IS: FACILITY HOLDING DETAINEES");
		// sProcessed = sProcessed.replace("PRICING REQUIREMENT IS", "PRICING REQUIREMENTS IS");
		// sProcessed = sProcessed.replace("(D) PLACE OF USE:", "(D) PLACE OF USE IS: ");
		// sProcessed = sProcessed.replace("PERFORMANE", "PERFORMANCE");
		// sProcessed = sProcessed.replace("REQUIRING ACTIVITY IS : ", "REQUIRING ACTIVITY IS: ");
		// sProcessed = sProcessed.replace("PRIICING", "PRICING");
		// sProcessed = sProcessed.replace("(B) PRICING REQUIREMENTS IS: PRICING IS: CONSISTANT WITH FAR PART 31.2, "CONTRACTS WITH COMMERCIAL ORGANIZATIONS"", "(B) REGULATION PRICING IS: CONSISTANT WITH FAR PART 31.2, "CONTRACTS WITH COMMERCIAL ORGANIZATIONS"");
		// sProcessed = sProcessed.replace("PRICING REQUIREMENTS IS: CONSISTANT WITH FAR PART 31.2, "CONTRACTS WITH COMMERCIAL ORGANIZATIONS"", "REGULATION PRICING IS: CONSISTANT WITH FAR PART 31.2, "CONTRACTS WITH COMMERCIAL ORGANIZATIONS"");
		// sProcessed = sProcessed.replace("PRICING REQUIREMENTS IS: FORMAT IN TABLE 15.2 IS USED", "REGULATION PRICING IS: FORMAT IN TABLE 15.2 IS USED");
		// sProcessed = sProcessed.replace("PRICING REQUIREMENTS IS: NOT FORMAT IN TABLE 15.2 IS USED", "REGULATION PRICING IS: NOT FORMAT IN TABLE 15.2 IS USED");
		// sProcessed = sProcessed.replace("PRICING REQUIREMENTS IS: PREDETERMINED INDIRECT COST RATES ARE USED", "INDIRECT COST IS: PREDETERMINED INDIRECT COST RATES ARE USED");
		// sProcessed = sProcessed.replace("PRICING REQUIREMENTS IS: ESTABLISHED CATALOG OR MARKET PRICE", "PRICING LIMITATIONS IS: ESTABLISHED CATALOG OR MARKET PRICE");
		// sProcessed = sProcessed.replace("PRICING REQUIREMENTS IS: ONE OR MORE IDENTIFIABLE LABOR OR MATERIAL COST FACTORS ARE SUBJECT TO CHANGE", "DIRECT COST IS: ONE OR MORE IDENTIFIABLE LABOR OR MATERIAL COST FACTORS ARE SUBJECT TO CHANGE");
		// sProcessed = sProcessed.replace("PRICING REQUIREMENTS IS: FREIGHT IS SHIPPED UNDER RATES SUBJECT TO RELEASED OR DECLARED VALUE", "TRANSPORTATION COST IS: FREIGHT IS SHIPPED UNDER RATES SUBJECT TO RELEASED OR DECLARED VALUE");
		// sProcessed = sProcessed.replace("PRICING REQUIREMENTS IS: NOT FREIGHT IS SHIPPED UNDER RATES SUBJECT TO RELEASED OR DECLARED VALUE", "TRANSPORTATION COST IS: NOT FREIGHT IS SHIPPED UNDER RATES SUBJECT TO RELEASED OR DECLARED VALUE");
		// sProcessed = sProcessed.replace("PRICING REQUIREMENTS IS: SUBJECT TO CAS", "CAS IS: SUBJECT TO CAS");
		// sProcessed = sProcessed.replace("PRICING REQUIREMENTS IS: NOT EXEMPT FROM CAS", "CAS IS: NOT EXEMPT FROM CAS");
		// sProcessed = sProcessed.replace("PRICING REQUIREMENTS IS: MODIFIED CAS IS AUTHRIZED", "CAS IS: MODIFIED CAS IS AUTHORIZED");
		// sProcessed = sProcessed.replace("PRICING REQUIREMENTS IS: NOT MODIFIED CAS IS AUTHRIZED", "CAS IS: NOT MODIFIED CAS IS AUTHORIZED");
		// sProcessed = sProcessed.replace("PRICING REQUIREMENTS IS: SOLICITATION/AWARD CALLS FOR ESTABLISHMENT OF FINAL INDIRECT COST RATES", "INDIRECT COST IS: SOLICITATION/AWARD CALLS FOR ESTABLISHMENT OF FINAL INDIRECT COST RATES");
		// sProcessed = sProcessed.replace("DOCUMEN TYPE", "DOCUMENT TYPE");
		// sProcessed = sProcessed.replace("PERIOD OF PERFORMANCE IN DAYS IS:", "PERIOD OF PERFORMANCE IS:");
		// sProcessed = sProcessed.replace("PRICING REQUIREMENTS IS: PRICES ARE SET BY LAW OR REGULATION", "PRICING LIMITATIONS IS: PRICES ARE SET BY LAW OR REGULATION");
		// sProcessed = sProcessed.replace("PRICING REQUIREMENTS IS: REGULATED TRANSPORTATION IS INVOLVED", "TRANSPORTATION COST IS: REGULATED TRANSPORTATION IS INVOLVED");
		// sProcessed = sProcessed.replace("PRICING REQUIREMENTS IS: GOVERNMENT IS RESPONSIBLE FOR GFP TRANSPORTATION ARRANGEMENTS AND COSTS", "SHIPPING RESPONSIBILITIES GOVERNMENT IS: GOVERNMENT IS RESPONSIBLE FOR GFP TRANSPORTATION ARRANGEMENTS AND COSTS");
		// sProcessed = sProcessed.replace("PROCEDURE IS:", "PROCEDURES IS:");
		// sProcessed = sProcessed.replace("PRICING REQUIREMENTS IS: CONTRACTORS MAY HAVE POTENTIAL TRANSIT CREDITS AVAIILABLE AND THE GOVERNMENT MAY REDUCE TRANSPORTATION COSTS", "TRANSPORTATION COST IS: OFFERORS MAY HAVE POTENTIAL TRANSIT CREDITS AVAILABLE AND THE GOVERNMENT MAY REDUCE TRANSPORTATION COSTS");
		// sProcessed = sProcessed.replace("PROCEDURE IS:", "PROCEDURES IS:");
		// sProcessed = sProcessed.replace("PRICING REQUIREMENTS IS: REIMBURSEMENT OF TRANSPORTATION AS A DIRECT CHARGE IS AUTHORIZED", "TRANSPORTATION COST IS: REIMBURSEMENT OF TRANSPORTATION AS A DIRECT CHARGE IS AUTHORIZED");
		// sProcessed = sProcessed.replace("PRICING REQUIREMENTS IS: PRICING IS CONSISTANT WITH FAR PART 31.1, "APPLICABILITY"", "REGULATION PRICING IS: PRICING IS CONSISTANT WITH FAR PART 31.1, "APPLICABILITY"");
		// sProcessed = sProcessed.replace("PRICING REQUIREMENTS IS: PRICING IS CONSISTANT WITH FAR PART 31.2, "CONTRACTS WITH COMMERCIAL ORGANIZATIONS"", "REGULATION PRICING IS: PRICING IS CONSISTANT WITH FAR PART 31.2, "CONTRACTS WITH COMMERCIAL ORGANIZATIONS"");
		// sProcessed = sProcessed.replace("PRICING REQUIREMENTS IS: PRICING IS CONSISTANT WITH FAR PART 31.6, "CONTRACTS WITH STATE, LOCAL, AND FEDERALLY RECOGNIZED INDIAN TRIBAL GOVERNMENTS"", "REGULATION PRICING IS: PRICING IS CONSISTANT WITH FAR PART 31.6, "CONTRACTS WITH STATE, LOCAL, AND FEDERALLY RECOGNIZED INDIAN TRIBAL GOVERNMENTS"");
		// sProcessed = sProcessed.replace("PRICING REQUIREMENTS IS: PRICING IS CONSISTANT WITH FAR PART 31.7, "CONTRACTS WITH NONPROFIT ORGANIZATIONS"", "REGULATION PRICING IS: PRICING IS CONSISTANT WITH FAR PART 31.7, "CONTRACTS WITH NONPROFIT ORGANIZATIONS"");
		// sProcessed = sProcessed.replace("PRICING REQUIREMENTS IS: STATUTORY COST LIMITATIONS APPLY", "PRICING LIMITATIONS IS: STATUTORY COST LIMITATIONS APPLY");
		// sProcessed = sProcessed.replace("PRICING REQUIREMENTS IS: TRANSPORTATION WITH FUEL-RELATED ADJUSTMENTS", "TRANSPORTATION COST IS: TRANSPORTATION WITH FUEL-RELATED ADJUSTMENTS");
		// sProcessed = sProcessed.replace("PRICING IS: CONSISTANT WITH FAR PART 31.2, "CONTRACTS WITH COMMERCIAL ORGANIZATIONS",", "REGULATION PRICING IS: PRICING IS CONSISTANT WITH FAR PART 31.2, "CONTRACTS WITH COMMERCIAL ORGANIZATIONS"");
		// sProcessed = sProcessed.replace("PROCEDURES IS: SEALED BIDDING", "PROCEDURES IS: SEALED BIDDING (FAR PART 14)') where condition_text like '%PROCEDURES IS: SEALED BIDDING';
		// sProcessed = sProcessed.replace("PROCEDURES IS: NOT FEDERAL SUPPLY SCHEDULE (FAR PART 6)", "PROCEDURES IS: NOT FEDERAL SUPPLY SCHEDULE (FAR PART 8.4)");
		// sProcessed = sProcessed.replace("PROCEDURES IS: TOTAL SMALL BUSINESS SET ASIDE", "PROCEDURES IS: TOTAL SMALL BUSINESS SET-ASIDE");
		// sProcessed = sProcessed.replace("PROCEDURES IS: FULL AND OPEN COMPETITION", "PROCEDURES IS: FULL AND OPEN COMPETITION (FAR PART 6)");
		// sProcessed = sProcessed.replace("PROCEDURES IS: NEGOTIATION", "PROCEDURES IS: NEGOTIATION (FAR PART 15)') where condition_text like '%PROCEDURES IS: NEGOTIATION';
		// sProcessed = sProcessed.replace("(B) PROCEDURES IS: NOT FULL AND OPEN COMPETITION", "(B) PROCEDURES IS: NOT FULL AND OPEN COMPETITION");
		// sProcessed = sProcessed.replace("BUSINESS TYPE IS: PROCUREMENT LIST MAINTAINED BY THE COMMITTEE FOR PURCHASE FROM PEOPLE WHO ARE BLIND OR SEVERELY DISABLED", "GOVERNMENT SOURCE IS: PROCUREMENT LIST MAINTAINED BY THE COMMITTEE FOR PURCHASE FROM PEOPLE WHO ARE BLIND OR SEVERELY DISABLED");
		// sProcessed = sProcessed.replace("(F) BUSINESS TYPE IS: NOT-FOR-PROFIT ORGANIZATION", "(F) MISCELLANEOUS SOURCE IS: NOT-FOR-PROFIT ORGANIZATION");
		// sProcessed = sProcessed.replace("(H) BUSINESS TYPE IS: NOT NONPROFIT ORGANIZATIONS", "(H) BUSINESS TYPE IS: NOT NON-PROFIT ORGANIZATION");
		// sProcessed = sProcessed.replace("(D) SERVICES IS: SERVICE CONTRACT REPORTING IS REQUIRED", "(D) SERVICE CONTRACT REPORTING IS: YES");
		// sProcessed = sProcessed.replace("THEUSE", "THE USE");
		
		return sProcessed;
	}

	private static void normalizeConditions(Connection conn, Integer srcDocId) throws SQLException {
		String sSql = "DELETE FROM Stg2_Clause_Conditions\n"
				+ "WHERE Stg1_Clause_Id IN (\n"
				+ "SELECT Stg1_Clause_Id FROM Stg1_Raw_Clauses WHERE Src_Doc_Id = ?\n"
				+ ")";
		SqlCommons.executeForDocId(conn, sSql, srcDocId);
		System.out.println("ClauseConditionsParser.normalizeConditions(): DELETE FROM Stg2_Clause_Conditions");
		
		int iCount = 0;
		PreparedStatement ps1 = null;
		PreparedStatement psInsert = null;
		PreparedStatement psInsertQuestions = null;
		ResultSet rs1 = null;
		try {
			sSql = "INSERT INTO Stg2_Clause_Conditions (Stg1_Clause_Id, Condition_Text, Condition_Text_Processed) VALUES (?, ?, ?)";
			psInsert = conn.prepareStatement(sSql);
			
			sSql = "SELECT Stg1_Clause_Id, Conditions, Clause_Number\n" +
					"FROM Stg1_Raw_Clauses\n" +
					"WHERE Src_Doc_Id = ?\n" +
					"AND Conditions IS NOT NULL\n" +
					"AND Conditions != ''";
			ps1 = conn.prepareStatement(sSql);
			ps1.setInt(1, srcDocId);
			rs1 = ps1.executeQuery();
			while (rs1.next()) {
				Integer v_id = rs1.getInt("Stg1_Clause_Id");
				boolean bChaned = false;
				String sOrigCondition = rs1.getString("Conditions");
				String[] aConditions = rs1.getString("Conditions").split("\n");
				String sClauseNumber = rs1.getString("Clause_Number");
				
				if ("52.215-23".equals(sClauseNumber)) {
					bChaned = false;
				}
				//String[] aConditions = CommonUtils.parseConditions(rs1.getString("Conditions"));
				String sParsedConditions = "";
				for (String sCondition : aConditions) {
					sCondition = sCondition.trim();
					if (!sCondition.isEmpty()) {
						String sProcessed = convertToConditionTextProcessed(sCondition);
						if (sParsedConditions.isEmpty())
							sParsedConditions = sProcessed;
						else
							sParsedConditions += " " + sProcessed;
								
						/*
						psInsert.setInt(1, v_id);
						psInsert.setString(2, sCondition);
						psInsert.setString(3, sProcessed);
						psInsert.execute();
						iCount++;
						if (!sProcessed.equals(sCondition)) {
							if (bChaned == false) {
								bChaned = true;
								System.out.println("[" + v_id + "] " + rs1.getString("Clause_Number"));
							}
							System.out.println(sCondition + "\n" + sProcessed);
						}
						*/
					}
				}
				if (!sParsedConditions.isEmpty()) {
					psInsert.setInt(1, v_id);
					psInsert.setString(2, sOrigCondition);
					psInsert.setString(3, sParsedConditions);
					psInsert.execute();
					iCount++;
				}
			}
		}
		finally {
			SqlUtil.closeInstance(psInsert);
			SqlUtil.closeInstance(rs1, ps1, null);
		}
		System.out.println("ClauseConditionsParser.normalizeConditions(): " + iCount);
	}
	
	public static void run(Connection conn, Integer srcDocId) throws SQLException {
		normalizeConditions(conn, srcDocId);
		
		String sSql = "UPDATE Stg2_Clause_Conditions\n" +
			"SET question_name = concat('[',trim(substring_index(substring_index(condition_text_processed, 'IS: ', 1), ') ', -1)),']'),\n" +
				"value = trim(substring_index(condition_text_processed, 'IS: ', -1))\n" +
			"WHERE Stg1_Clause_Id IN (\n" +
				"SELECT Stg1_Clause_Id FROM Stg1_Raw_Clauses WHERE Src_Doc_Id = ?\n" +
			")";
		SqlCommons.executeForDocId(conn, sSql, srcDocId);
		System.out.println("ClauseConditionsParser.run(): UPDATE Stg2_Clause_Conditions SET question_name");
		
		sSql = "update Stg2_Clause_Conditions\n" + 
			"set operator = \n" +
			"case \n" +
				"when value like 'NOT %'\n" + 
					"and (Stg1_Clause_Id not in (\n" +
						"select Stg1_Clause_Id from Stg1_Raw_Clauses where clause_number in ('52.211-3','52.211-4'))\n" +
					") -- clause conditions with legitimate 'NOT' in answers\n" +
					"then '!='\n" + 
				"else '='\n" +
			"end\n" +
			"WHERE Stg1_Clause_Id IN (\n" +
				"SELECT Stg1_Clause_Id FROM Stg1_Raw_Clauses WHERE Src_Doc_Id = ?\n" +
			")";
		SqlCommons.executeForDocId(conn, sSql, srcDocId);
		System.out.println("ClauseConditionsParser.run(): UPDATE Stg2_Clause_Conditions SET operator");
		
		sSql = "update Stg2_Clause_Conditions\n" +
			"set value = right(value, length(value)-4) \n" +
			"where Stg1_Clause_Id IN (\n" +
				"SELECT Stg1_Clause_Id FROM Stg1_Raw_Clauses WHERE Src_Doc_Id = ?\n" +
			")\n" +
			"and value like 'NOT %'\n" +
			"and (Stg1_Clause_Id not in (\n" +
				"select Stg1_Clause_Id from Stg1_Raw_Clauses where clause_number in ('52.211-3','52.211-4'))\n" +
			//")\n" +
			")";
		SqlCommons.executeForDocId(conn, sSql, srcDocId);
		System.out.println("ClauseConditionsParser.run(): UPDATE Stg2_Clause_Conditions SET value");

		sSql = "update Stg2_Clause_Conditions\n" +
			"set question_name = '[SUPPLIES OR SERVICES]'\n" +
			"where Stg1_Clause_Id IN (\n" +
				"SELECT Stg1_Clause_Id FROM Stg1_Raw_Clauses WHERE Src_Doc_Id = ?\n" +
			")\n" +
			"and question_name in ('[SERVICES]', '[SUPPLIES]')";
		SqlCommons.executeForDocId(conn, sSql, srcDocId);
		System.out.println("ClauseConditionsParser.run(): UPDATE Stg2_Clause_Conditions SET question_name WHERE question_name in ('[SERVICES]', '[SUPPLIES]')");
		
		sSql = "update Stg2_Clause_Conditions set real_question_id = null, real_question_name = null\n" +
				"where Stg1_Clause_Id IN ( SELECT Stg1_Clause_Id FROM Stg1_Raw_Clauses WHERE Src_Doc_Id = ? )";
		SqlCommons.executeForDocId(conn, sSql, srcDocId);
		System.out.println("ClauseConditionsParser.run(): UPDATE Stg2_Clause_Conditions SET real_question_id, real_question_name");
			
		sSql = "update Stg2_Clause_Conditions\n" +
			"join stg2_question q\n" +
				"on Stg2_Clause_Conditions.question_name = q.question_name\n" +
				"and q.question_type != 'Numeric'\n" +
			"set real_question_id = (\n" +
				"select min(q2.Stg2_Question_Id)\n" +
				"from stg2_question q2\n" +
				"join Stg2_Possible_Answer a2\n" +
					"on q2.Stg2_Question_Id = a2.Stg2_Question_Id\n" +
				"where q2.root_question_id = q.Stg2_Question_Id\n" +
					"and Stg2_Clause_Conditions.value = a2.choice_text\n" +
					"and a2.Stg2_Possible_Answer_Id\n" +
			")\n" +
			"where Stg2_Clause_Conditions.Stg1_Clause_Id IN ( SELECT Stg1_Clause_Id FROM Stg1_Raw_Clauses WHERE Src_Doc_Id = ? )\n" +
			"and not exists (\n" +
			    "select 1\n" +
			    "from Stg2_Possible_Answer a\n" +
			    "where a.Stg2_Question_Id = q.Stg2_Question_Id\n" +
					"and Stg2_Clause_Conditions.value = a.choice_text\n" +
			")";
		SqlCommons.executeForDocId(conn, sSql, srcDocId);
		System.out.println("ClauseConditionsParser.run(): update Stg2_Clause_Conditions.real_question_id #1");
		
		sSql = "update Stg2_Clause_Conditions\n" +
			"join stg2_question q\n" +
				"on Stg2_Clause_Conditions.question_name = q.question_name\n" +
				"and q.question_type != 'Numeric'\n" +
			"set real_question_id = q.Stg2_Question_Id\n" +
			"where Stg1_Clause_Id IN ( SELECT Stg1_Clause_Id FROM Stg1_Raw_Clauses WHERE Src_Doc_Id = ? )\n" +
			"and exists (\n" +
			    "select 1\n" +
			    "from Stg2_Possible_Answer a\n" +
			    "where a.Stg2_Question_Id = q.Stg2_Question_Id\n" +
					"and Stg2_Clause_Conditions.value = a.choice_text\n" +
			")";
		SqlCommons.executeForDocId(conn, sSql, srcDocId);
		System.out.println("ClauseConditionsParser.run(): UPDATE Stg2_Clause_Conditions SET real_question_id #2");
			
		sSql = "update Stg2_Clause_Conditions\n" +
			"set real_question_name = ( select question_name from stg2_question where Stg2_Question_Id = Stg2_Clause_Conditions.real_question_id)\n" +
			"where Stg1_Clause_Id IN ( SELECT Stg1_Clause_Id FROM Stg1_Raw_Clauses WHERE Src_Doc_Id = ? )";
		SqlCommons.executeForDocId(conn, sSql, srcDocId);
		System.out.println("ClauseConditionsParser.run(): UPDATE Stg2_Clause_Conditions");
			
		sSql = "update Stg2_Clause_Conditions c\n" +
			"join\n" +
				"(\n" +
			    "select a.choice_text, q.Stg2_Question_Id, q.question_name\n" +
			    "from Stg2_Possible_Answer a\n" +
				"join stg2_question q\n" +
					"on q.Stg2_Question_Id = a.Stg2_Question_Id\n" +
				"where q.question_name in (\n" +
			        "'[SUPPLIES SHIPPED]',\n" +
					"'[SHIPPING DESTINATIONS]',\n" +
					"'[SHIPPING RESPONSIBILITIES CONTRACTOR]',\n" +
					"'[SHIPPING RESPONSIBILITIES GOVERNMENT]',\n" +
					"'[MODE OF TRANSPORTATION]',\n" +
					"'[SHIPPING DETERMINATIONS]',\n" +
					"'[SHIPPING MULTIPLES]',\n" +
					"'[SHIPPING METHOD]',\n" +
					"'[EXPORT REQUIREMENTS]',\n" +
					"'[IMPORT REQUIREMENTS]',\n" +
					"'[DUTY DETERMINATIONS]'\n" +
			        ")\n" +
				") a\n" +
			    "on a.choice_text = c.value\n" +
			"set real_question_name = a.question_name,\n" +
				"real_question_id = a.Stg2_Question_Id\n" +
			"where c.Stg1_Clause_Id IN ( SELECT Stg1_Clause_Id FROM Stg1_Raw_Clauses WHERE Src_Doc_Id = ? )\n" +
			"and c.value is not null\n" + 
				"and c.real_question_name is null\n" + 
			    "and c.question_name not regexp '^\\[[0-9]+'\n" + 
			    "and c.question_name in ('[TRANSPORTATION REQUIREMENTS]')";
		SqlCommons.executeForDocId(conn, sSql, srcDocId);
		System.out.println("ClauseConditionsParser.run(): UPDATE Stg2_Clause_Conditions");
		
		sSql = "update Stg2_Clause_Conditions c\n" +
			"join\n" +
				"(\n" +
			    "select a.choice_text, q.Stg2_Question_Id, q.question_name\n" +
			    "from Stg2_Possible_Answer a\n" +
				"join stg2_question q\n" +
					"on q.Stg2_Question_Id = a.Stg2_Question_Id\n" +
				"where q.question_name in\n" +
					"(\n" +
			        "'[GOVERNMENT REVIEWS]',\n" +
					"'[RECRUITMENT POLICY]',\n" +
					"'[LABOR RATES]'\n" +
			        ")\n" +
				") a\n" +
			    "on a.choice_text = c.value\n" +
			"set real_question_name = a.question_name,\n" +
				"real_question_id = a.Stg2_Question_Id\n" +
			"where c.Stg1_Clause_Id IN ( SELECT Stg1_Clause_Id FROM Stg1_Raw_Clauses WHERE Src_Doc_Id = ? )\n" +
			"and c.value is not null\n" + 
				"and c.real_question_name is null\n" + 
			    "and c.question_name not regexp '^\\[[0-9]+'\n" + 
			    "and c.question_name in ('[LABOR REQUIREMENTS]')";
		SqlCommons.executeForDocId(conn, sSql, srcDocId);
		System.out.println("ClauseConditionsParser.run(): UPDATE Stg2_Clause_Conditions");
		
		sSql = "update Stg2_Clause_Conditions c\n" +
			"join Stg2_Possible_Answer a\n" +
				"on choice_text = c.value\n" +
			"join stg2_question q\n" +
				"on a.Stg2_Question_Id = q.Stg2_Question_Id\n" +
			"set real_question_name = q.question_name,\n" +
				"real_question_id = q.Stg2_Question_Id\n" +
			"where c.Stg1_Clause_Id IN ( SELECT Stg1_Clause_Id FROM Stg1_Raw_Clauses WHERE Src_Doc_Id = ? )\n" +
			"and c.value is not null\n" + 
			"and c.real_question_name is null\n" + 
			"and c.question_name not regexp '^\\[[0-9]+'\n" + 
			"and c.value not in ('NO', 'YES')\n" +
			"and (\n" +
				"select count(1)\n" +
				"from Stg2_Possible_Answer\n" +
				"where choice_text = c.value\n" +
			") = 1";
		SqlCommons.executeForDocId(conn, sSql, srcDocId);
		System.out.println("ClauseConditionsParser.run(): UPDATE Stg2_Clause_Conditions");
		
		sSql = "update Stg2_Possible_Answer\n" +
			"set soundex_hash = soundex(choice_text)\n" +
			"WHERE Src_Doc_Id = ?";
		SqlCommons.executeForDocId(conn, sSql, srcDocId);
		System.out.println("ClauseConditionsParser.run(): UPDATE Stg2_Possible_Answer set soundex_hash");
		
		sSql = "update Stg2_Clause_Conditions c\n" +
			"join Stg2_Possible_Answer a\n" +
				"on soundex_hash= soundex(c.value)\n" +
			"join stg2_question q\n" +
				"on a.Stg2_Question_Id = q.Stg2_Question_Id\n" +
			"set real_question_name = q.question_name,\n" +
				"real_question_id = q.Stg2_Question_Id,\n" +
			    "real_value = a.choice_text\n" +
			"where c.Stg1_Clause_Id IN ( SELECT Stg1_Clause_Id FROM Stg1_Raw_Clauses WHERE Src_Doc_Id = ? )\n" +
			"and c.value is not null\n" + 
			"and c.real_question_name is null\n" + 
			"and c.question_name not regexp '^\\[[0-9]+'\n" + 
			"and c.value not in ('NO', 'YES')\n" +
			"and (\n" +
				"select count(1)\n" +
				"from Stg2_Possible_Answer\n" +
				"where soundex_hash = soundex(c.value)\n" +
			") = 1";
		SqlCommons.executeForDocId(conn, sSql, srcDocId);
		System.out.println("ClauseConditionsParser.run(): UPDATE Stg2_Clause_Conditions set real_question_name");

//		sSql = "update Stg2_Clause_Conditions set real_value = 'GREATER THAN 150000'\n" +
//			"where Stg1_Clause_Id IN ( SELECT Stg1_Clause_Id FROM Stg1_Raw_Clauses WHERE Src_Doc_Id = ? )\n" +
//			"AND value = 'GREATER THAN SAT'";
//		SqlCommons.executeForDocId(conn, sSql, srcDocId);
//		System.out.println("ClauseConditionsParser.run(): UPDATE Stg2_Clause_Conditions set real_value");
//		
//		sSql = "update Stg2_Clause_Conditions set real_value = 'GREATER THAN OR EQUAL TO 150000'\n" +
//				"where Stg1_Clause_Id IN ( SELECT Stg1_Clause_Id FROM Stg1_Raw_Clauses WHERE Src_Doc_Id = ? )\n" +
//				"and value = 'GREATER THAN OR EQUAL TO SAT'";
//		SqlCommons.executeForDocId(conn, sSql, srcDocId);
//		System.out.println("ClauseConditionsParser.run(): UPDATE Stg2_Clause_Conditions set real_value");
//			
//		sSql = "update Stg2_Clause_Conditions set real_value = 'LESS THAN OR EQUAL TO 150000'\n" +
//				"where Stg1_Clause_Id IN ( SELECT Stg1_Clause_Id FROM Stg1_Raw_Clauses WHERE Src_Doc_Id = ? )\n" +
//				"and value = 'LESS THAN OR EQUAL TO SAT'";
//		SqlCommons.executeForDocId(conn, sSql, srcDocId);
//		System.out.println("ClauseConditionsParser.run(): UPDATE Stg2_Clause_Conditions set real_value");
//			
//		sSql = "update Stg2_Clause_Conditions set real_value = 'GREATER THAN 150000'\n" +
//				"where Stg1_Clause_Id IN ( SELECT Stg1_Clause_Id FROM Stg1_Raw_Clauses WHERE Src_Doc_Id = ? )\n" +
//				"and value = 'GREATER THAN SAP'";
//		SqlCommons.executeForDocId(conn, sSql, srcDocId);
//		System.out.println("ClauseConditionsParser.run(): UPDATE Stg2_Clause_Conditions set real_value");
//			
//		sSql = "update Stg2_Clause_Conditions set real_value = 'LESS THAN OR EQUAL TO 150000'\n" +
//				"where Stg1_Clause_Id IN ( SELECT Stg1_Clause_Id FROM Stg1_Raw_Clauses WHERE Src_Doc_Id = ? )\n" +
//				"and value = 'LESS THAN OR EQUAL TO SAP'";
//		SqlCommons.executeForDocId(conn, sSql, srcDocId);
//		System.out.println("ClauseConditionsParser.run(): UPDATE Stg2_Clause_Conditions set real_value");
			
		sSql = "update Stg2_Clause_Conditions\n" +
			"set real_value = concat('GREATER THAN OR EQUAL TO ', right(value, length(value)-length('GREATER OR EQUAL TO ')))\n" +
			"where Stg1_Clause_Id IN ( SELECT Stg1_Clause_Id FROM Stg1_Raw_Clauses WHERE Src_Doc_Id = ? )\n" +
			"and value regexp '^GREATER OR EQUAL TO [0-9]+$'";
		SqlCommons.executeForDocId(conn, sSql, srcDocId);
		System.out.println("ClauseConditionsParser.run(): UPDATE Stg2_Clause_Conditions set real_value");
			
		sSql = "update Stg2_Clause_Conditions\n" +
				"set real_value = replace(value,',','')\n" +
				"where Stg1_Clause_Id IN ( SELECT Stg1_Clause_Id FROM Stg1_Raw_Clauses WHERE Src_Doc_Id = ? )\n" +
				"and value regexp '^GREATER THAN OR EQUAL TO ([0-9](,)*)+$' and question_name = '[CONTRACT VALUE]'";
		SqlCommons.executeForDocId(conn, sSql, srcDocId);
		System.out.println("ClauseConditionsParser.run(): UPDATE Stg2_Clause_Conditions set real_value Strip out commas");
			
	}
}
