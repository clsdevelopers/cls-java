package hgs.cpm.clause;

import gov.dod.cls.utils.sql.SqlUtil;
import hgs.cpm.config.CpmConfig;
import hgs.cpm.utils.StringCommons;
import hgs.cpm.utils.SqlCommons;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class ParseClausePrescriptions {
	
	public static void normalizeClausePrescriptions(Connection conn, Integer srcDocId) throws SQLException {
		String sSql = "DELETE FROM Stg2_Clause_Prescriptions_Xref\n"
				+ "WHERE Stg1_Clause_Id IN (\n"
				+ "SELECT Stg1_Clause_Id FROM Stg1_Raw_Clauses WHERE Src_Doc_Id = ?\n"
				+ ")";
		SqlCommons.executeForDocId(conn, sSql, srcDocId);
		System.out.println("ParseClausePrescriptions.normalize(): DELETE FROM Stg2_Clause_Prescriptions_Xref");
		
		int iCount = 0;
		PreparedStatement ps1 = null;
		PreparedStatement psInsert = null;
		ResultSet rs1 = null;
		try {
			sSql = "INSERT INTO Stg2_Clause_Prescriptions_Xref (Stg1_Clause_Id, Prescription) VALUES (?, ?)";
			psInsert = conn.prepareStatement(sSql);
			
			sSql = "SELECT Stg1_Clause_Id, clause_prescription\n" +
					"FROM Stg1_Raw_Clauses\n" +
					"WHERE Src_Doc_Id = ?\n" +
					"AND clause_prescription IS NOT NULL\n" +
					"AND clause_prescription != ''";
			ps1 = conn.prepareStatement(sSql);
			ps1.setInt(1, srcDocId);
			rs1 = ps1.executeQuery();
			ArrayList<String> aAdded = new ArrayList<String>(); 
			while (rs1.next()) {
				aAdded.clear();
				//String[] aPrescriptions = rs1.getString("clause_prescription").split("\n");
				String[] aPrescriptions = StringCommons.parsePrescriptions(rs1.getString("clause_prescription"));
				for (String sPrescription : aPrescriptions) {
					sPrescription = StringCommons.fixPrescription(sPrescription);
					if (!sPrescription.isEmpty() && (!aAdded.contains(sPrescription))) {
						Integer v_id = rs1.getInt("Stg1_Clause_Id");
						psInsert.setInt(1, v_id);
						psInsert.setString(2, sPrescription);
						psInsert.execute();
						iCount++;
						aAdded.add(sPrescription);
					}
				}
			}
		}
		finally {
			SqlUtil.closeInstance(psInsert);
			SqlUtil.closeInstance(rs1, ps1, null);
		}
		System.out.println("ParseClausePrescriptions.normalize(): count(" + iCount + ")");
	}

	// ===================================================================
	public static void main( String[] args )
    {
		/*
		String sClausePrescription = "225.1101(2)(i)\n225.1101(2)(ii)";
		String[] aPrescriptions = StringCommons.parsePrescriptions(sClausePrescription);
		for (String sItem : aPrescriptions) {
			System.out.println(sItem);
		}
		*/
		CpmConfig cpmConfig = CpmConfig.getInstance();
		Connection conn = null;
		try {
			Integer srcDocId = 286;
			conn = cpmConfig.getDbConnection();
			normalizeClausePrescriptions(conn, srcDocId);
		} catch (Exception oError) {
			oError.printStackTrace();
		} finally {
			SqlUtil.closeInstance(conn);
		}
    }
}
