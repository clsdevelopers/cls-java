package hgs.cpm.clause;

import gov.dod.cls.utils.CommonUtils;
import gov.dod.cls.utils.sql.SqlUtil;
import hgs.cpm.config.CpmConfig;
import hgs.cpm.db.Stg1RawClauseRecord;
import hgs.cpm.db.StgSrcDocumentRecord;
import hgs.cpm.utils.RomanNumeral;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.regex.Pattern;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ClauseExcel {

	private String excelFileName;
	private Connection conn;
	private Integer srcDocId = null;
	private PreparedStatement psInsert = null;
	
	private PreparedStatement psCountFars = null;
	private PreparedStatement psUpdateDFars = null;
	
	public Integer getSrcDocId() { return srcDocId; }
	public void setSrcDocId(Integer srcDocId) { this.srcDocId = srcDocId; }

	public Connection getConn() { return conn; }
	public void setConn(Connection conn) { this.conn = conn; }

	public ClauseExcel(String excelFileName, Connection conn) {
		this.excelFileName = excelFileName;
		this.conn = conn;
	}
	
	public Integer generateDocId() throws SQLException {
		if (this.srcDocId == null) {
			System.out.println(this.excelFileName);
			File f = new File(this.excelFileName);
			String fileName = f.getName();
			this.srcDocId = StgSrcDocumentRecord.generateDocId(this.conn, fileName, StgSrcDocumentRecord.TYPE_CD_CLAUSE);
		}
		return this.srcDocId;
	}
	
	public void read() throws IOException, SQLException, ParseException {
		this.generateDocId();
		FileInputStream fileInputStream = null;
		XSSFWorkbook workbook = null;
		try {
			this.prepareStatement();
			fileInputStream = new FileInputStream(new File(this.excelFileName));
			workbook = new XSSFWorkbook (fileInputStream);
			for (int iSheet = 0; iSheet < 2; iSheet++ ) { // workbook.getNumberOfSheets()
				this.read(workbook, iSheet);
			}
			
			this.readFACDAC(workbook, 4);
			this.readSAT (workbook, 5);
		}
		finally {
			if (workbook != null)
				workbook.close();

			if (fileInputStream != null)
				try {
					fileInputStream.close();
				} catch (Exception oIgnore) {
					oIgnore.printStackTrace();
				}
			
			this.closeStatement();
		}
		
		/*
		String sSql = "update Stg1_Raw_Clauses\n" +
			"set clause_number = (\n" +
				"select ifnull(alternate_number,main_clause_number)\n" +
				"from (\n" +
					"select\n" + 
						"Stg1_Clause_Id,\n" +
						"substring_index(clause,' ',1) main_clause_number, clause,\n" +
						"case\n" + 
							"when upper(substring_index(clause,' ',3)) like '%ALTERNATE%' and clause not like '%ALTERNATE PRESERV%' and clause not like '%ALTERNATE A%'\n" + 
			                    "then substring_index(clause,' ',3)\n" +  
							"when substring_index(clause,'ALTERNATE ',-1) not like '% %'\n" + 
								"then concat(substring_index(clause,' ',1),' ALTERNATE ', substring_index(clause,'ALTERNATE ', -1))\n" +
						"end alternate_number\n" +
					"from Stg1_Raw_Clauses\n" +
				") as sub\n" +
				"where Stg1_Raw_Clauses.Stg1_Clause_Id = sub.Stg1_Clause_Id\n" +
			")\n" +
			"WHERE Src_Doc_Id = ?";
		this.executeForDocId(sSql);
		
		String sSql = "UPDATE stg1_raw_clauses\n" +
				"SET Clause_Number = SUBSTRING_INDEX(Clause_Number, ',', 1)\n" +
				"WHERE Src_Doc_Id = ? AND Clause_Number like '%,'";
		this.executeForDocId(sSql);
		*/
	}
	
	private int iUCF_Section = -1;
	private int iIBR_Or_Full_Text = -1;
	private int iFill_In = -1;
	private int iClause = -1;
	private int iEffective_Clause_Date = -1;
	private int iClause_Prescription = -1;
	private int iPrescription_Text = -1;
	private int iConditions = -1;
	private int iRule = -1;
	private int iPresc_Or_Clause = -1;
	//private int iClause_Number = -1;
	private int iAdditional_Conditions = -1;

	private int iEditable = -1; 
	private int iRequired_Or_Optional = -1;
	private int iProvision_Or_Clause = -1;
	private int iCommercial_Status = -1; // CJ-476
	private int iOptional_Status = -1; // CJ-802
	private int iOptional_Condition = -1; // CJ-802
	private int iOfficial_Clause_Title = -1; // CJ-916
	
	private void read(XSSFWorkbook workbook, int iSheet) throws SQLException {
		XSSFSheet sheet = workbook.getSheetAt(iSheet);
		//Get iterator to all the rows in current sheet
		Iterator<Row> rowIterator = sheet.iterator();
		
		if (!rowIterator.hasNext()) {
			return;
		}
		this.iUCF_Section = -1;
		this.iIBR_Or_Full_Text = -1;
		this.iFill_In = -1;
		this.iClause = -1;
		this.iEffective_Clause_Date = -1;
		this.iClause_Prescription = -1;
		this.iPrescription_Text = -1;
		this.iConditions = -1;
		this.iRule = -1;
		this.iPresc_Or_Clause = -1;
		//this.iClause_Number = -1;
		this.iAdditional_Conditions = -1; 

		this.iEditable = -1; 
		this.iRequired_Or_Optional = -1;
		this.iProvision_Or_Clause = -1;
		this.iCommercial_Status = -1; // CJ-476
		
		this.iOptional_Status = -1; // CJ-802
		this.iOptional_Condition = -1; // CJ-802
		this.iOfficial_Clause_Title = -1;	// CJ-916
		
		
		int iColFound = 0;
		
		Row row = rowIterator.next();
		for (int iCell = row.getFirstCellNum(); iCell < row.getLastCellNum(); iCell++) {
			Cell oCell = row.getCell(iCell);
			String value = getCellValue(oCell);
			/*
			int iPos = value.indexOf('\n');
			if (iPos >= 0)
				value = value.substring(0, iPos).trim();
			*/
			switch (value.toUpperCase()) {
			case "UNIFORM CONTRACT FORMAT SECTION":
			case "UCF SECTION":
				this.iUCF_Section = iCell; iColFound++; 
				break; // SECTION
			case "IBR OR FULL TEXT":	
				this.iIBR_Or_Full_Text = iCell; iColFound++; 
				break; // IBR_OR_FULL_TEXT
			case "FILL-IN":	
				this.iFill_In = iCell; iColFound++;
				break; // FILL_IN
			case "FAR CLAUSE":
			case "DFARS CLAUSE":
				this.iClause = iCell; iColFound++;
				break; // CLAUSE
			case "EFFECTIVE CLAUSE DATE":
			case "CLAUSE EFFECTIVE DATE":
				this.iEffective_Clause_Date = iCell; iColFound++;
				break; // EFFECTIVE_CLAUSE_DATE
			case "CLAUSE PRESCRIPTION":	
			case "DFARS\nPRESCRIPTION CITED IN CLAUSE":
				this.iClause_Prescription = iCell; iColFound++;
				break; // CLAUSE_PRESCRIPTION
			case "PRESCRIPTION TEXT":	
				this.iPrescription_Text = iCell; iColFound++;
				break; // PRESCRIPTION_TEXT
			case "CONDITIONS":	
				this.iConditions = iCell; iColFound++;
				break; // CONDITIONS
			case "RULE":	
				this.iRule = iCell; iColFound++;
				break; // RULE
			//case "PRESCRIPTION OR CLAUSE?\n\n(FAR MATRIX)\n":	this.iPresc_Or_Clause = iCell; iColFound++; break;
			case "ADDITIONAL CONDITIONS":	
				this.iAdditional_Conditions = iCell; iColFound++;
				break; // ADDITIONAL_CONDITIONS
			case "EDITABLE":
				this.iEditable = iCell; iColFound++;
				break;
			case "REQUIRED OR OPTIONAL":
				this.iRequired_Or_Optional = iCell; iColFound++; 
				break;
			case "PROVISION OR CLAUSE":
			case "PROVISION OR CLAUSE?":
				this.iProvision_Or_Clause = iCell; iColFound++; 
				break;
			case "COMMERCIAL STATUS": // CJ-476
				this.iCommercial_Status = iCell; iColFound++;
				break;
			case "OPTIONAL STATUS": // CJ-802
				this.iOptional_Status = iCell; iColFound++;
				break;
			case "OPTIONAL CONDITIONS": // CJ-802
				this.iOptional_Condition = iCell; iColFound++;
				break;
			case "OFFICIAL CLAUSE TITLE": // CJ-916
				this.iOfficial_Clause_Title = iCell; iColFound++;
				break;
			}
		}
		
		System.out.println("ClauseExcel.read() found columns: " + iColFound + " / 16");

		while (rowIterator.hasNext()) {
            row = rowIterator.next();
    		this.load(row, iSheet);
        }
	}
	
	private String getCellValue(Row row, int iColumn) {
		if (iColumn < 0)
			return "";
		Cell cell = row.getCell(iColumn);
		return this.getCellValue(cell);
	}
	
	private void load(Row row, int iSheet) 
			throws SQLException 
	{
		String sSection = getCellValue(row, this.iUCF_Section);
		String sIBR_Or_Full_Text = getCellValue(row, this.iIBR_Or_Full_Text);
		String sFill_In = getCellValue(row, this.iFill_In);
		String sClause = getCellValue(row, this.iClause);
		String sEffective_Clause_Date = getCellValue(row, this.iEffective_Clause_Date);
		String sClause_Prescription = getCellValue(row, this.iClause_Prescription);
		String sPrescription_Text = getCellValue(row, this.iPrescription_Text);
		String sConditions = getCellValue(row, this.iConditions);
		String sRule = getCellValue(row, this.iRule);
		String sPresc_Or_Clause = getCellValue(row, this.iPresc_Or_Clause);
		String sAdditional_Conditions = getCellValue(row, this.iAdditional_Conditions); 

		String sRequired_Or_Optional = getCellValue(row, this.iRequired_Or_Optional); 
		String sEditable = getCellValue(row, this.iEditable); 
		String sProvisionOrClause = getCellValue(row, this.iProvision_Or_Clause);

		String sCommercialStatus = getCellValue(row, this.iCommercial_Status); // CJ-476
		
		String sOptionalStatus = getCellValue(row, this.iOptional_Status); // CJ-802
		String sOptionalConditions = getCellValue(row, this.iOptional_Condition); // CJ-802
		String sOfficialTitle = getCellValue(row, this.iOfficial_Clause_Title); // CJ-916
		
		if (sConditions != null) {
			sConditions = sConditions.replaceAll("\\xa0", " ");
			while (sConditions.contains("  "))
				sConditions = sConditions.replaceAll("  ", " ");
		}
		
		boolean bAlternate = false, bDeviation = false, bStartDeviation = false;
		int iPos;
		// String sTempClause = sClause.replaceAll("\n", " ").replace("\r", " ");

		String sTempClause = sClause.replaceAll("\\x0d|\\x0a", " ").replaceAll("\\xa0", " ");
		
		/*boolean bFarOnDfarTab = false;
		if ((iSheet == 1) && (sTempClause.startsWith("FAR"))) {
			bFarOnDfarTab = true;
		}*/
		
		int iSheetFAR = iSheet;
		if ((iSheet == 1) && (sTempClause.startsWith("FAR"))) {
			if (sConditions.indexOf("REQUIRING ACTIVITY IS: DEPARTMENT OF DEFENSE") > 0)  {
				iSheetFAR = 0;
				//bFarOnDfarTab = false;				
			}
		}
		
		if (sTempClause.startsWith("FAR CLAUSE"))
			sTempClause = sTempClause.substring("FAR CLAUSE".length() + 1).trim();
		else if (sTempClause.startsWith("FAR"))
			sTempClause = sTempClause.substring("FAR".length() + 1).trim();

		String[] aItems = sTempClause.split(" ");

		String sClauseNumber = "";
		for (iPos = 0; iPos < aItems.length; iPos++) {
			String sItem = aItems[iPos];
			if (iPos == 0) {
				if ("DEVIATION".equalsIgnoreCase(sItem))
					bStartDeviation = true;
				else
					sClauseNumber = sItem;
			} else {
				if (sItem.endsWith(","))
					sItem = sItem.substring(0, sItem.length() - 1).trim();
				if (!sItem.isEmpty()) {
					if ("Alternate".equalsIgnoreCase(sItem) || "ATERNATE".equalsIgnoreCase(sItem)) { //252.216-7010 Alternate I
						sClauseNumber += ' ' + sItem;
						bAlternate = true;
					}
					else if (bAlternate) {
						if (RomanNumeral.isRomanNumeral(sItem))
							sClauseNumber += ' ' + sItem;
						break;
					} // 252.203-7998 (DEVIATION 2015-00010)
					else if ("(DEVIATION".equalsIgnoreCase(sItem)) {
						sClauseNumber += ' ' + sItem;
						bDeviation = true;
					}
					else if (bDeviation) {
						sClauseNumber += ' ' + sItem;
						if (sItem.endsWith(")"))
							break;
					} else if (bStartDeviation) {
						sClauseNumber = sItem;
						bStartDeviation = false;
					} else
						break;
				}
			}
		}
		
		sClauseNumber = sClauseNumber.replaceAll(",", "");
		if ((!bAlternate) && (!bDeviation) && (!bStartDeviation) && (aItems.length > 3)) {
			String sLast = aItems[aItems.length - 1];
			if (RomanNumeral.isRomanNumeral(sLast)) {
				String sBefore = aItems[aItems.length - 2];
				if (sBefore.toUpperCase().endsWith("ALTERNATE")) {
					sClauseNumber += " ALTERNATE " + sLast; 
				}
			}
			/*
			else if (sLast.endsWith(")")) { // DEVIATION at last
				String sPrevious = aItems[aItems.length - 2];
				iPos = sPrevious.indexOf("(DEVIATION");
				if (iPos >= 0) {
					String sDeviation = sPrevious.substring(iPos);
					sLast = sLast.replaceAll("\\[", "").replaceAll("\\]", "");
					sDeviation += " " + sLast;
					sClauseNumber += " " + sDeviation;
					System.out.println(sClauseNumber);
				}
			}
			*/
		}

		if (sClauseNumber.endsWith(","))
			sClauseNumber = sClauseNumber.substring(0, sClauseNumber.length() - 1).trim();
		if (sClauseNumber.endsWith("-PROHIBITION"))
			sClauseNumber = sClauseNumber.substring(0, sClauseNumber.length() - "-PROHIBITION".length()).trim();
		
		iPos = sClauseNumber.toUpperCase().indexOf(" ALTERNATE");
		if (iPos > 0) {
			if (sClause.toUpperCase().contains(" ALTERNATE A")) {
				sClauseNumber = sClauseNumber.substring(0, iPos) + sClauseNumber.substring(iPos + " ALTERNATE".length()); 
			}
		}

		if (aItems.length > 3) {
			String sLast = aItems[aItems.length - 1];
			if (sLast.endsWith(")")) { // DEVIATION at last
				String sPrevious = aItems[aItems.length - 2].toUpperCase();
				iPos = sPrevious.indexOf("(DEVIATION");
				if (iPos >= 0) {
					String sDeviation = sPrevious.substring(iPos);
					sLast = sLast.replaceAll("\\[", "").replaceAll("\\]", "");
					sDeviation += " " + sLast;
					sClauseNumber += " " + sDeviation;
					System.out.println(sClauseNumber);
				}
			}
		}
		
		if ("252.204-7007 Alternate".equalsIgnoreCase(sClauseNumber))
			sClauseNumber = "252.204-7007"; 
		
		if (sClauseNumber.isEmpty())
			System.out.println("[" + iSheet + "/" + (row.getRowNum() + 1) + "] NO Cluase Number: " + sClause);
		else {
			iPos = sClauseNumber.indexOf("(DEVIATION");
			if (iPos > 0) {
				String sLeft = sClauseNumber.substring(0, iPos + "(DEVIATION".length());
				String sRight = sClauseNumber.substring(iPos + "(DEVIATION".length());
				sRight = sRight.replace('O', '0');
				sClauseNumber = sLeft + sRight;
			}
		}
		
		if ((sConditions != null) && sConditions.startsWith("NOT APPLICABLE"))
			sConditions = "";
		else
			sConditions = ClauseExcel.fixDeviationNumber(sConditions);

		sAdditional_Conditions = ClauseExcel.fixDeviationNumber(sAdditional_Conditions);
		if ((sRule != null) && sRule.startsWith("NOT APPLICABLE"))
			sRule = "";
		
		if ("252.225-7035".equals(sClauseNumber)) {
			System.out.println("[" + iSheet + "/" + (row.getRowNum() + 1) + "] " + sClauseNumber + " '" + sClause_Prescription);
		}
		
		// CJ-1072, If FAR clause included on DFAR tab...
		/*boolean bUpdateDFarActivity = false;
		if (bFarOnDfarTab) { 

			// Check if FAR clause has already been added.
			this.psCountFars.setString(1,  sClauseNumber);
			this.psCountFars.setInt(2, this.srcDocId); 
			if (this.psCountFars.execute()) { 
					ResultSet rs1 = this.psCountFars.getResultSet();
					rs1.next();
					if (rs1.getInt(1) > 0)
						bUpdateDFarActivity = true;
			}	
		}
		
		The way duplicates sort, the first record on the FARS tab would never be first anyway.
		So, the is_Dfaf_Activity for this record would never be used.
		if (bFarOnDfarTab && (iSheetFAR > 0)) {
			this.psUpdateDFars.setString(1,  sClauseNumber);
			this.psUpdateDFars.setInt(2, this.srcDocId); 
			this.psUpdateDFars.execute();
		}*/
		
			this.setParam(1, sSection); // 1 Section
			this.setParam(2, sIBR_Or_Full_Text); // 2 IBR_Or_Full_Text, 
			this.setParam(3, sFill_In); // 3 Fill_In, 
			this.setParam(4, sClause); // 4 Clause, 
			this.setParam(5, sEffective_Clause_Date); // 5 Effective_Clause_Date,
			this.setParam(6, sClause_Prescription); // 6 Clause_Prescription, 
			this.setParam(7, sPrescription_Text); // 7 Prescription_Text, 
			this.setParam(8, sConditions); // 8 Conditions, 
			this.setParam(9, sRule); // 9 Rule, 
			this.setParam(10, sPresc_Or_Clause); // 10 Presc_Or_Clause,
			//this.setParam(11, sClause_Number); // 11 Clause_Number,
			this.setParam(11, sAdditional_Conditions); // 12 Additional_Conditions, 
			this.psInsert.setInt(12, this.srcDocId); // 13 Src_Doc_Id
			this.psInsert.setInt(13, iSheet); // 14 Sheet_Number
			this.psInsert.setInt(14, row.getRowNum() + 1); // 15 Row_Number
			this.setParam(15, sClauseNumber);
	
			this.setParam(16, trim(sRequired_Or_Optional, 2000));
			this.setParam(17, trim(sEditable, 2000));
			this.setParam(18, trim(sProvisionOrClause, 1));
	
			this.setParam(19, trim(sCommercialStatus, 50)); // CJ-476
	
			this.setParam(20, trim(sOptionalStatus, 50)); // CJ-802
			this.setParam(21, trim(sOptionalConditions, 250)); // CJ-802
			this.setParam(22, trim(sOfficialTitle, 500)); // CJ-916
			this.psInsert.setInt(23, iSheetFAR); // CJ-1072
			
			this.psInsert.execute();
		
	}
	
	public static String fixDeviationNumber(String psText) {
		if (CommonUtils.isNotEmpty(psText)) {
			String[] aItems = psText.split(Pattern.quote("(DEVIATION"));
			if (aItems.length > 1) {
				String result = "";
				for (int iIndex = 0; iIndex < aItems.length; iIndex++) {
					if (iIndex == 0)
						result = aItems[iIndex];
					else {
						result += "(DEVIATION";
						String sItem = aItems[iIndex];
						int iPos = sItem.indexOf(')');
						if (iPos > 0) {
							String sNumber = sItem.substring(0, iPos + 1);
							String sRight = sItem.substring(iPos + 1);
							sNumber = sNumber.replace('O', '0');
							sItem = sNumber + sRight;
						}
						result += sItem;
					}
				}
				psText = result;
			}
		}
		return psText;
	}
	
	public static String trim(String psValue, int piSize) {
		if ((psValue != null) && (psValue.length() > piSize)) {
			return psValue.substring(0, piSize - 1);
		}
		return psValue;
	}
	
	public void setParam(int piIndex, String psValue) throws SQLException {
		if ((psValue == null) || psValue.isEmpty())
			this.psInsert.setNull(piIndex, java.sql.Types.VARCHAR);
		else {
			this.psInsert.setString(piIndex, psValue);
		}
	}
	
	private void prepareStatement() throws SQLException {
		String sSql
			= "INSERT INTO " + Stg1RawClauseRecord.TABLE_STG1_RAW_CLAUSES
			+ " (" + Stg1RawClauseRecord.FIELD_SECTION // 1
			+ ", " + Stg1RawClauseRecord.FIELD_IBR_OR_FULL_TEXT // 2
			+ ", " + Stg1RawClauseRecord.FIELD_FILL_IN // 3
			+ ", " + Stg1RawClauseRecord.FIELD_CLAUSE // 4
			+ ", " + Stg1RawClauseRecord.FIELD_EFFECTIVE_CLAUSE_DATE // 5
			+ ", " + Stg1RawClauseRecord.FIELD_CLAUSE_PRESCRIPTION // 6
			+ ", " + Stg1RawClauseRecord.FIELD_PRESCRIPTION_TEXT // 7
			+ ", " + Stg1RawClauseRecord.FIELD_CONDITIONS // 8
			+ ", " + Stg1RawClauseRecord.FIELD_RULE // 9
			+ ", " + Stg1RawClauseRecord.FIELD_PRESC_OR_CLAUSE // 10
			+ ", " + Stg1RawClauseRecord.FIELD_ADDITIONAL_CONDITIONS // 11
			+ ", " + Stg1RawClauseRecord.FIELD_SRC_DOC_ID // 12
			+ ", " + Stg1RawClauseRecord.FIELD_SHEET_NUMBER // 13
			+ ", " + Stg1RawClauseRecord.FIELD_ROW_NUMBER // 14
			+ ", " + Stg1RawClauseRecord.FIELD_CLAUSE_NUMBER // 15
			+ ", " + Stg1RawClauseRecord.FIELD_REQUIRED_OR_OPTIONAL // 16
			+ ", " + Stg1RawClauseRecord.FIELD_EDITABLE // 17
			+ ", " + Stg1RawClauseRecord.FIELD_PROVISION_OR_CLAUSE // 18
			+ ", " + Stg1RawClauseRecord.FIELD_COMMERCIAL_STATUS // 19
			+ ", " + Stg1RawClauseRecord.FIELD_OPTIONAL_STATUS // 20
			+ ", " + Stg1RawClauseRecord.FIELD_OPTIONAL_CONDITIONS // 21
			+ ", " + Stg1RawClauseRecord.FIELD_OFFICIAL_CLAUSE_TITLE // 22
			+ ", " + Stg1RawClauseRecord.FIELD_DFARS_ACTIVITY // 23
			+ ")\n" +
			" VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
		this.psInsert = this.conn.prepareStatement(sSql);
		
		sSql = "SELECT COUNT(*) FROM " + Stg1RawClauseRecord.TABLE_STG1_RAW_CLAUSES + 
				" WHERE " + Stg1RawClauseRecord.FIELD_CLAUSE_NUMBER +  "  =  ? " + 
				" AND " + Stg1RawClauseRecord.FIELD_SRC_DOC_ID + " = ? ";
		
		this.psCountFars = this.conn.prepareStatement(sSql);
		
		sSql = "UPDATE " + Stg1RawClauseRecord.TABLE_STG1_RAW_CLAUSES + 
				" SET " + Stg1RawClauseRecord.FIELD_DFARS_ACTIVITY + " = 1 " +
				" WHERE " + Stg1RawClauseRecord.FIELD_CLAUSE_NUMBER +  "  =  ? " + 
				" AND " + Stg1RawClauseRecord.FIELD_SRC_DOC_ID + " = ? ";
		
		this.psUpdateDFars = this.conn.prepareStatement(sSql);
	}
	
	private void closeStatement() {
		SqlUtil.closeInstance(this.psInsert);
		this.psInsert = null;
	}
	
	private String getCellValue(Cell cell) {
		String result = null;

		if (cell != null) {
	        switch (cell.getCellType()) {
		        case Cell.CELL_TYPE_BOOLEAN:
		        	result = cell.getBooleanCellValue() + "";
		            break;
		        case Cell.CELL_TYPE_NUMERIC:
		        	result = cell.getNumericCellValue() + "";
		            break;
		        case Cell.CELL_TYPE_STRING:
		        	result = cell.getStringCellValue();
		            break;
		    }
		}
		
		if (result == null)
			return "";
		result = result.trim();
		
		if (result.isEmpty())
			return result;
		
		while (result.contains("\n\n"))
			result = result.replaceAll("\n\n", "\n");
		
		while ((result.length() > 0) && (result.charAt(0) == '\n'))
			result = result.substring(1);

		int iLength = result.length();
		while (iLength > 0) {
			if (result.charAt(iLength - 1) == '\n') {
				result = result.substring(0, (--iLength - 1));
			} else
				break;
		}
		
		result = result.replaceAll("\\xa0", " "); // replaceAll("\\x0d|\\x0a", " ").
		while (result.contains("  "))
			result = result.replaceAll("  ", " ");
		
		return result.trim();
	}
	
	// ===================================================================
	// CJ-477
	// Read the FAC and DAC numbers from the INCLUDED FAC AND DAC Tab in the spreadsheet.
	
	String facNumber = "";
	String dacNumber = "";
	String dfarsNumber = "";
	Date facDate;
	Date dacDate;
	Date dfarsDate;
	
	private int includedType = -1;
	private int includedNumber = -1;
	private int includedDate = -1;
	
	private void readFACDAC(XSSFWorkbook workbook, int iSheet) throws SQLException, ParseException {
		XSSFSheet sheet = workbook.getSheetAt(iSheet);
		//Get iterator to all the rows in current sheet
		Iterator<Row> rowIterator = sheet.iterator();
		
		if (!rowIterator.hasNext()) {
			return;
		}
		this.includedType = -1;
		this.includedNumber = -1;
		this.includedDate = -1;
		
		int iColFound = 0;
		
		Row row = rowIterator.next();
		for (int iCell = row.getFirstCellNum(); iCell < row.getLastCellNum(); iCell++) {
			Cell oCell = row.getCell(iCell);
			String value = getCellValue(oCell);

			switch (value.toUpperCase()) {
			case "TYPE":
				this.includedType = iCell; iColFound++; 
				break; // TYPE
			case "NUMBER":
				this.includedNumber = iCell; iColFound++; 
				break; // Number
			case "DATE":
				this.includedDate = iCell; iColFound++; 
				break; // Number	
			
			}
		}
		
		System.out.println("ClauseExcel.readFACDAC() found columns: " + iColFound + " / 2");

		int maxRows = 0;
		while (rowIterator.hasNext() && maxRows < 3) {
            row = rowIterator.next();
    		this.loadFACDAC(row, iSheet);
            maxRows++;
        }
	}
	
	
	private void loadFACDAC(Row row, int iSheet) 
			throws SQLException, ParseException 
	{
		
		String sType = getCellValue(row, this.includedType);
		String sNumber = getCellValue(row, this.includedNumber);
		String  sDate = getCellValue(row, this.includedDate);
		sDate = sDate.replace("(", "");
		sDate = sDate.replace(")", "");
		DateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");

		Date aDate = formatter.parse(sDate);
		
		if (sNumber.isEmpty())
			System.out.println("[" + iSheet + "/" + (row.getRowNum() + 1) + "] NO FAC/DAC/DFARS Number: " + sNumber);
		else {
			System.out.println("[" + iSheet + "/" + (row.getRowNum() + 1) + "] Type = " + sType + ", Number = " + sNumber);
			
		}

		sNumber = sNumber.replace("FAC-", "");
		sNumber = sNumber.replace("FAC", "");
		sNumber = sNumber.replace("DAC-", "");
		sNumber = sNumber.replace("DAC", "");
		sNumber = sNumber.trim();
		
		if (sType.equals("FAC")) {
			this.facNumber = sNumber;
			this.facDate = aDate;
		}
		else if (sType.equals("DAC")) {
			this.dacNumber = sNumber;
			this.dacDate = aDate;
		}
		else if (sType.equals("DFARS")) {
			this.dfarsNumber = sNumber;
			this.dfarsDate = aDate;
		}
	}
	
	public String getFacNumber() {
		return this.facNumber;
	}
	
	public String getDacNumber() {
		return this.dacNumber;
	}
	
	public String getDfarsNumber() {
		return this.dfarsNumber;
	}
	
	public Date getFacDate() {
		return this.facDate;
	}
	public Date getDacDate() {
		return this.dacDate;
	}
	
	public Date getDfarsDate() {
		return this.dfarsDate;
	}
	
	// ===================================================================
	// CJ-1360
	// Read the SAT conditions and rules in the spreadsheet.

	String satCondition = "";
	String sat300KRule = "";
	String sat1MillionRule = "";

	private int includedSatCondition = -1;
	private int includedSat300KRule = -1;
	private int includedSat1MillionRule = -1;
	
	private void readSAT(XSSFWorkbook workbook, int iSheet) throws SQLException, ParseException {
		XSSFSheet sheet = workbook.getSheetAt(iSheet);
		//Get iterator to all the rows in current sheet
		Iterator<Row> rowIterator = sheet.iterator();
		
		if (!rowIterator.hasNext()) {
			return;
		}
		this.includedSatCondition = -1;
		this.includedSat300KRule = -1;
		this.includedSat1MillionRule = -1;
		
		int iColFound = 0;
		
		Row row = rowIterator.next();
		for (int iCell = row.getFirstCellNum(); iCell < row.getLastCellNum(); iCell++) {
			Cell oCell = row.getCell(iCell);
			String value = getCellValue(oCell);

			switch (value.toUpperCase()) {
			case "SAT_CONDITION":
				this.includedSatCondition = iCell; iColFound++; 
				break; 
			case "SAT_300K_RULE":
				this.includedSat300KRule = iCell; iColFound++; 
				break; 
			case "SAT_1MILLION_RULE":
				this.includedSat1MillionRule = iCell; iColFound++; 
				break; 
			
			}
		}
		
		System.out.println("ClauseExcel.readSAT() found columns: " + iColFound + " / 3");

		
		int maxRows = 0;
		while (rowIterator.hasNext() && maxRows < 3) {
            row = rowIterator.next();
    		this.loadSAT(row, iSheet);
            maxRows++;
        }
        
	}
	
	private void loadSAT(Row row, int iSheet) 
			throws SQLException, ParseException 
	{

		String sSatCondition = getCellValue(row, this.includedSatCondition);
		String sSat300KRule = getCellValue(row, this.includedSat300KRule);
		String sSat1MillionRule = getCellValue(row, this.includedSat1MillionRule);
		
		if (sSatCondition.isEmpty())
			System.out.println("[" + iSheet + "/" + (row.getRowNum() + 1) + "] NO SAT Condition: ");
		else {
			System.out.println("[" + iSheet + "/" + (row.getRowNum() + 1) + "] SAT Condition = " + sSatCondition + ",\nSat300KRule = " + sSat300KRule + ",\nSat1MillionRule = " + sSat1MillionRule);
			
		}

		this.satCondition = sSatCondition;
		this.sat300KRule = sSat300KRule;
		this.sat1MillionRule = sSat1MillionRule;
	}
	
	public String getSatConditon() {
		return this.satCondition;
	}
	public String getSat300kRule() {
		return this.sat300KRule;
	}
	
	public String getSat1MilRule() {
		return this.sat1MillionRule;
	}
	/*
	private void executeForDocId(String sSql) throws SQLException {
		SqlCommons.executeForDocId(this.conn, sSql, this.srcDocId);
	}
	*/
	// ===================================================================
	public static void main( String[] args )
    {
		/*
		String sSrc = "IF 252.225-7995 (DEVIATION 2015-O0009) ADDED,\nDO NOT INCLUDE 252.225-7040";
		String sChangd = ClauseExcel.fixDeviationNumber(sSrc);
		if (sChangd.equals(sSrc)) {
			System.out.println("Not changed");
		} else {
			System.out.println(sChangd);
		}
		*/
		CpmConfig cpmConfig = CpmConfig.getInstance();
		String excelFileName = "E:\\Projects\\ClauseLogic\\documents\\SharePoint\\Data\\CLS- FAR DFAR Rules Sheet - 2015.xlsx";
		Connection conn = null;
		try {
			conn = cpmConfig.getDbConnection();
			ClauseExcel oClauseExcel = new ClauseExcel(excelFileName, conn);
			oClauseExcel.read();
		} catch (Exception oError) {
			oError.printStackTrace();
		} finally {
			SqlUtil.closeInstance(conn);
		}
    }
}
