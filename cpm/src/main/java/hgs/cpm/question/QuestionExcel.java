package hgs.cpm.question;

import gov.dod.cls.utils.sql.SqlUtil;
import hgs.cpm.db.StgSrcDocumentRecord;
import hgs.cpm.question.util.Stg1QuestionSequenceSet;
import hgs.cpm.question.util.StgRawQuestionTable;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Iterator;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class QuestionExcel {

	private String questionExcelFileName;
	private Connection conn;
	private Integer srcDocId = null;
	private int iQuestionSequenceSrcDocId;
	private PreparedStatement psInsert = null;
	
	public Integer getSrcDocId() { return srcDocId; }
	public void setSrcDocId(Integer srcDocId) { this.srcDocId = srcDocId; }

	public Connection getConn() { return conn; }
	public void setConn(Connection conn) { this.conn = conn; }

	public QuestionExcel(String questionExcelFileName, int iQuestionSequenceSrcDocId, Connection conn) {
		this.questionExcelFileName = questionExcelFileName;
		this.conn = conn;
		this.iQuestionSequenceSrcDocId = iQuestionSequenceSrcDocId;
	}
	
	public Integer generateDocId() throws SQLException {
		if (this.srcDocId == null) {
			File f = new File(this.questionExcelFileName);
			String fileName = f.getName();
			this.srcDocId = StgSrcDocumentRecord.generateDocId(this.conn, fileName, StgSrcDocumentRecord.TYPE_CD_QUESTION);
		}
		return this.srcDocId;
	}
	
	public void run() throws IOException, SQLException {
		this.read();

		StgRawQuestionTable stgRawQuestionTable = new StgRawQuestionTable(conn, this.srcDocId);
		stgRawQuestionTable.loadAndParse();
		
		Stg1QuestionSequenceSet stg1QuestionSequenceSet = new Stg1QuestionSequenceSet();
		stg1QuestionSequenceSet.load(conn, this.iQuestionSequenceSrcDocId);
		
		stgRawQuestionTable.stg2QuestionSet.assignSequence(stg1QuestionSequenceSet);
		stgRawQuestionTable.stg2QuestionSet.save(conn);
		/*
		// this.setSrcDocId(13);
		this.populateLevel1Questions();
		this.populateLevel2Questions();
		this.populateLevel3Questions();
		this.populateLevel4Questions();
		this.cleanupChoiceText();
		this.setStg2QuestionGroupName();
		this.normalizeDisplayRules();
		this.correctParsedRules();
		this.normalizeQuestionDependencies();
		*/
	}
	
	public void read() throws IOException, SQLException {
		this.generateDocId();
		FileInputStream fileInputStream = null;
		XSSFWorkbook workbook = null;
		try {
			this.prepareStatement();
			fileInputStream = new FileInputStream(new File(this.questionExcelFileName));
			workbook = new XSSFWorkbook (fileInputStream);
			for (int iSheet = 0; iSheet < workbook.getNumberOfSheets(); iSheet++ ) {
				this.read(workbook, iSheet, true);
				this.read(workbook, iSheet, false);
			}
		}
		finally {
			if (workbook != null)
				workbook.close();

			if (fileInputStream != null)
				try {
					fileInputStream.close();
				} catch (Exception oIgnore) {
					oIgnore.printStackTrace();
				}
			
			this.closeStatement();
		}
	}
	
	private void read(XSSFWorkbook workbook, int iSheet, boolean bBaseline) throws SQLException {
		XSSFSheet sheet = workbook.getSheetAt(iSheet);
		//Get iterator to all the rows in current sheet
		Iterator<Row> rowIterator = sheet.iterator();
		
		int iRows = 0;
		while (rowIterator.hasNext()) {
            Row row = rowIterator.next();
            if (bBaseline) {
	            Cell cell0 = row.getCell(0);
	            Cell cell1 = row.getCell(1);
	            this.read(cell0, cell1, true, row.getRowNum(), iSheet);
            } else {
	            Cell cell3 = row.getCell(3);
	            Cell cell4 = row.getCell(4);
	            this.read(cell3, cell4, false, row.getRowNum(), iSheet);
            }
            iRows++;
        }
		System.out.println("[" + iSheet + "," + iRows + "," + bBaseline + "]");
	}
	
	private static String[][] FIX_WORDS =
		{
			{"[TRANSLATION TO ANOTHER LANGUAGE[", "[TRANSLATION TO ANOTHER LANGUAGE]"},
			{"[TRADE AGREEMENT APPLICABILITY\n", "[TRADE AGREEMENT APPLICABILITY]\n"},
			{"[REPORTS AND DRAWINGS\n", "[REPORTS AND DRAWINGS]\n"},
			{"\nINTRASTATE MOVEMENT]", "\n[INTRASTATE MOVEMENT]"},
			{"[DISMANTLING PAYMENT ARRANGEMENT[", "[DISMANTALLING PAYMENT ARRANGEMENT]"},
			{"\nFULL AND OPEN EXCEPTION]", "\n[FULL AND OPEN EXCEPTION]"}
		};

	private void read(Cell cellLeft, Cell cellRight, boolean bBaseline, Integer rowNumber, int iSheet) 
			throws SQLException 
	{
		//if ((cellLeft == null) && (cellRight == null))
		///	return;
		
		this.sCondition = (cellLeft == null) ? "" : this.getCellValue(cellLeft);
		String sPrescriptions = (cellRight == null) ? "" : this.getCellValue(cellRight);
		if ((this.sCondition.length() == 0) && (sPrescriptions.length() == 0)) {
			//return;
		}
		
		//System.out.println("[" + iSheet + "," + rowNumber + "," + bBaseline + "]");
		if (this.sCondition.length() > 0)
			for (int iFix = 0; iFix < FIX_WORDS.length; iFix++) {
				if (this.fixWord(FIX_WORDS[iFix][0], FIX_WORDS[iFix][1]))
					break;
			}

		this.psInsert.setInt(1, this.srcDocId);
		this.psInsert.setInt(2, iSheet);
		this.psInsert.setInt(3, rowNumber);
		this.psInsert.setBoolean(4, bBaseline);
		this.psInsert.setString(5, this.sCondition);
		this.psInsert.setString(6, sPrescriptions);
		this.psInsert.execute();
	}
	
	private String sCondition;
	private boolean fixWord(String from, String to) {
		int iPos = this.sCondition.indexOf(from);
		if (iPos >= 0) {
			this.sCondition = this.sCondition.replace(from, to);
			System.out.println("Fixed: '" + from + "'");
			return true;
		}
		return false;
	}
	
	private void prepareStatement() throws SQLException {
		String sSql
			= "INSERT INTO Stg1_Question (Src_Doc_Id, Sheet_Id, Row_Number, Is_Baseline, Question, Prescription)"
			+ " VALUES (?, ?, ?, ?, ?, ?)";
		this.psInsert = this.conn.prepareStatement(sSql);
	}
	
	private void closeStatement() {
		SqlUtil.closeInstance(this.psInsert);
		this.psInsert = null;
	}
	
	private String getCellValue(Cell cell) {
		String result = null;

		if (cell != null) {
	        switch (cell.getCellType()) {
		        case Cell.CELL_TYPE_BOOLEAN:
		        	result = cell.getBooleanCellValue() + "";
		            break;
		        case Cell.CELL_TYPE_NUMERIC:
		        	result = cell.getNumericCellValue() + "";
		            break;
		        case Cell.CELL_TYPE_STRING:
		        	result = cell.getStringCellValue();
		            break;
		        case Cell.CELL_TYPE_BLANK:
		        	result = "";
		            break;
		        default:
		        	System.out.println("[" + cell.getRowIndex() + "," + cell.getColumnIndex() + "]: Unprocessed type of " + cell.getCellType());
		        	break;
		    }
		}
		
		if (result == null)
			return "";
		result = result.trim();
		
		if (result.isEmpty())
			return result;
		
		while (result.contains("\n\n"))
			result = result.replaceAll("\n\n", "\n");
		
		while ((result.length() > 0) && (result.charAt(0) == '\n'))
			result = result.substring(1);

		int iLength = result.length();
		while (iLength > 0) {
			if (result.charAt(iLength - 1) == '\n') {
				result = result.substring(0, (--iLength - 1));
			} else
				break;
		}
		
		result = result.replaceAll("\\xa0", " "); // replaceAll("\\x0d|\\x0a", " ").
		while (result.contains("  "))
			result = result.replaceAll("  ", " ");
		
		return result.trim();
	}
	
	/*
	private void executeForDocId(String sSql) throws SQLException {
		SqlCommons.executeForDocId(this.conn, sSql, this.srcDocId);
	}
	
	public void populateLevel1Questions() throws SQLException {
		String sSql = "DELETE FROM Stg2_Question WHERE Src_Doc_Id = ?";
		this.executeForDocId(sSql);
		System.out.println("DELETE FROM Stg2_Question");

		sSql =
			"INSERT INTO Stg2_Question\n" +
			"(Stg1_Question_Id, question_name, question_text, question_type, rule, normal_rule, type, category, Is_Baseline, Src_Doc_Id, Parse_Level)\n" +
			"select\n" + 
				 "Stg1_Question_Id, question_name, trim(question_text) question_text,\n" +
				 "case\n" + 
					"when\n" + 
						"(\n" +
							"select trim(upper(Question))\n" +
			                "from stg1_question\n" +
			                "where Stg1_Question_Id = layer1.Stg1_Question_Id + 1\n" +
			            ") in ('NO', 'YES')\n" +
			            "then 'Boolean'\n" +
					"when type like '%SELECT ONE OR MORE%' then 'Multiple Answer'\n" + // Added
					"when\n" + 
						"( type regexp '.*(MAY|MUST|HAS TO) SELECT ONE.*' and type not like '%OR MORE%' )\n" +
						"or type regexp '.*(MAY|MUST|HAS TO) SELECT EITHER.*'\n" + 
						"then 'Single Answer'\n" +
			        "when type regexp '.*((MAY|MUST|HAS TO) )*SELECT ONE OR MORE.*'\n" + 
						"or type regexp '.*(MAY|MUST|HAS TO) SELECT AT LEAST ONE.*'\n" + 
			            "or type like '%CAN SELECT ALL%'\n" + 
			            "or type like '%MANY%'\n" + 
			            "or type like '%AT LEAST%'\n" +
						"then 'Multiple Answer'\n" +
					"when type regexp '.*(MAY|MUST|HAS TO) FILL IN A (MIN|NUMBER)*.*' then 'Numeric'\n" +
					"else 'Single Answer'\n" +
				 "end question_type,\n" +
			     "rule,\n" +
				 "rule normal_rule,\n" +
				 "type,\n" +
			     "category, Is_Baseline, Src_Doc_Id, 0\n" +
			"from\n" +
				"(\n" +
				"select \n" +
					"Stg1_Question_Id,\n" +
					"substr(Question,instr(Question,'['),instr(Question,']')-instr(Question,'[')+1) question_name,\n" + 
					"case \n" +
						"when\n" + 
							"trim(Question) not like 'ASK%'\n" + 
			                "and trim(Question) not like '(ASK%' \n" +
							"and trim(Question) not like 'ONLY ASK%'\n" +
			                "then substr(Question,1, instr(Question,'\\n')-1)\n" +
						"else\n" +
							"substring_index(substring_index(Question,'\\n', 2),'\\n',-1)\n" +
					"end question_text,\n" + 
					"substring_index(Question, 'USER ', -1) type,\n" +
					"case\n" + 
						"when trim(Question) like 'ASK%' or trim(Question) like '(ASK%'\n" + 
						"then trim(substring_index(Question,'\\n', 1))\n" +
					"end rule,\n" +
					"Question, Prescription,\n" +
			        "case\n" +
						"WHEN Is_Baseline = 1 THEN 'Baseline'\n" +
			            "ELSE 'Follow-on'\n" +
					"END category,\n" +
			        "Is_Baseline, Src_Doc_Id\n" +
				"from stg1_question q\n" +
				"where Src_Doc_Id = ?\n" +
			    "and Prescription ='' and Question != ''\n" +
					"and Question LIKE '%[%]%'\n" +
					"and exists\n" +
					"(\n" +
					"select 1\n" +
					"from stg1_question prev\n" +
					"where prev.Stg1_Question_Id + 1 = q.Stg1_Question_Id\n" + 
						"and Question = '' and Prescription = ''\n" +
					")\n" +
			 ") as layer1";
		this.executeForDocId(sSql);
		System.out.println("populateLevel1Questions()\n" + sSql);

		sSql =
			"INSERT INTO stg2_possible_answer (Stg2_Question_Id, Choice_Text, Stg1_Question_Id, Src_Doc_Id)\n" +
			"SELECT\n" +
				"q.Stg2_Question_Id, TRIM(choice.Question) 'choice text', choice.Stg1_Question_Id, q.Src_Doc_Id\n" +
			"FROM stg2_question q\n" +
			"JOIN stg1_question r_q ON (q.Stg1_Question_Id = r_q.Stg1_Question_Id)\n" +
			"JOIN stg1_question choice ON (choice.Stg1_Question_Id > r_q.Stg1_Question_Id)\n" +
			    "AND choice.Stg1_Question_Id <\n" +
					"(	SELECT MIN(next_question.Stg1_Question_Id)\n" +
				        "FROM stg1_question next_question\n" +
				        "WHERE next_question.Stg1_Question_Id > q.Stg1_Question_Id\n" +
				        "AND (next_question.Is_Baseline = q.Is_Baseline)\n" +
						"AND (Question = '' and Prescription = '' )\n" +
			        ")\n" +
				"and not exists\n" +
					"(\n" +
			        "select 1\n" +
					"FROM stg1_question next_line\n" +
			        "where Question REGEXP '^[0-9]+\\. ' = 1\n" +
			            "and next_line.Stg1_Question_Id = r_q.Stg1_Question_Id + 1\n" +
			        ")\n" +
			"WHERE q.Src_Doc_Id = ? AND IFNULL(question_type,'') NOT IN ('Numeric')"; // TRIM(choice.Question) <> '' AND 
		this.executeForDocId(sSql);
		System.out.println("populateLevel1Questions() PossibleAnswer #1\n" + sSql);

		sSql =
			"INSERT INTO stg2_possible_answer (Stg2_Question_Id, Choice_Text, Stg1_Question_Id, Src_Doc_Id)\n" +
			"select \n" +
				"q.Stg2_Question_Id, trim(substring_index(choice.Question,'\n',1)) 'choice text', choice.Stg1_Question_Id, q.Src_Doc_Id\n" +
			"from stg2_question q\n" +
			"join stg1_question r_q\n" +
				"on q.Stg1_Question_Id = r_q.Stg1_Question_Id\n" +
			    "and exists  (\n" +
					"select 1\n" +
					"from stg1_question next_line\n" +
			        "where Question like '1. %'\n" +
			        "and next_line.Stg1_Question_Id = r_q.Stg1_Question_Id + 1\n" +
				")\n" +
			"join stg1_question choice\n" +
				"on choice.Stg1_Question_Id > r_q.Stg1_Question_Id\n" +
			    "and ( choice.Question REGEXP '^[0-9]+\\. ' = 1 )\n" +
			    "and choice.Stg1_Question_Id <\n" +
					"(\n" +
			        "select min(next_question.Stg1_Question_Id)\n" +
			        "from stg1_question next_question\n" +
			        "where ( Question = '' and Prescription = '' )\n" +
						"and next_question.Stg1_Question_Id > q.Stg1_Question_Id\n" +
			        ")\n" +
			"WHERE q.Src_Doc_Id = ?"; //  AND trim(substring_index(choice.Question,'\n',1)) <> ''
		this.executeForDocId(sSql);
		System.out.println("populateLevel1Questions() PossibleAnswer #2");
		
		sSql = "delete from stg2_possible_answer where Src_Doc_Id = ? AND trim(choice_text) = ''";
		this.executeForDocId(sSql);
		System.out.println("populateLevel1Questions() PossibleAnswer #3 remove temp_possible_answers for empty choice_text\n" + sSql);

		sSql =	"update stg2_possible_answer\n" +
				"set choice_text = trim(right(choice_text, length(choice_text) - locate('. ', choice_text)))\n" + 
				"where Src_Doc_Id = ? AND choice_text regexp '^([[:digit:]]|[[:alpha:]])+\\. '";
		this.executeForDocId(sSql);
		System.out.println("populateLevel1Questions() PossibleAnswer #3\n" + sSql);

		sSql =	"update stg2_possible_answer\n" +
				"set choice_text = replace( trim(replace(choice_text,'\n','')), '  ', ' ')\n" +
				"where Src_Doc_Id = ? AND choice_text regexp '\n'";
		this.executeForDocId(sSql);
		System.out.println("populateLevel1Questions() PossibleAnswer #4\n" + sSql);
	}
		
	public void populateLevel2Questions() throws SQLException {
		String sSql =
			"INSERT INTO Stg2_Question\n" +
			"(Stg1_Question_Id, question_name, question_text, question_type, rule, normal_rule, type, category, Is_Baseline, Src_Doc_Id, Parse_Level)\n" +
			"select \n" +
				"Stg1_Question_Id,\n" + 
			    "case when question_name='' then concat('[', q_name, ']') else question_name end,\n" + 
			    "question_text,\n" + 
			    "case\n" + 
			    	"when type like '%SELECT ONE OR MORE%' then 'Multiple Answer'\n" + // Added
					"when\n" + 
						"( type regexp '.*(MAY|MUST|HAS TO) SELECT ONE.*' and type not like '%OR MORE%' )\n" +
						"or type regexp '.*(MAY|MUST|HAS TO) SELECT EITHER.*'\n" + 
						"then 'Single Answer'\n" +
			        "when type regexp '.*((MAY|MUST|HAS TO) )*SELECT ONE OR MORE.*'\n" + 
						"or type regexp '.*(MAY|MUST|HAS TO) SELECT AT LEAST ONE.*'\n" + 
			            "or type like '%CAN SELECT ALL%'\n" + 
			            "or type like '%MANY%'\n" + 
			            "or type like '%AT LEAST%'\n" +
						"then 'Multiple Answer'\n" +
					"when type regexp '.*(MUST|HAS TO) FILL IN A (MIN|NUMBER)*.*' then 'Numeric'\n" +

					// Added extra
					"else 'Single Answer'\n" +
					
				"end question_type , parsed_rule, rule, type, category, Is_Baseline, Src_Doc_Id, 1\n" +
			"from\n" +
				"(\n" +
				"select \n" +
					"distinct \n" +
					"choice.Stg1_Question_Id,\n" + 
			        "substr(choice.Question,instr(choice.Question,'['),instr(choice.Question,']')-instr(choice.Question,'[')+1) question_name,\n" +
					"concat('Select ', lower(trim(substring_index(substring_index(choice.Question,'\\n',1),'. ',-1))),' subtype(s):') question_text,\n" +
					"concat('\"', ans.choice_text,'\" = ', q.question_name) parsed_rule,\n" +
			        "substring_index(choice.Question, 'USER ', -1) type,\n" +
			        "choice.Question rule,\n" +
			        "upper(trim(left(substring_index(substring_index(choice.Question,'\\n',1),'. ',-1),48))) q_name,\n" +
					"case\n" +
						"WHEN r_q.Is_Baseline = 1 THEN 'Baseline'\n" +
						"ELSE 'Follow-on'\n" +
					"END category,\n" +
					"substring_index(r_q.Question, 'USER ', -1) type2,\n" + // Added extra
					"choice.Is_Baseline, choice.Src_Doc_Id\n" +
				"from stg2_question q\n" +
				"join stg1_question r_q\n" +
					"on q.Stg1_Question_Id = r_q.Stg1_Question_Id and r_q.Src_Doc_Id = ?\n" +
				"join stg1_question next_line\n" +
					 "on next_line.Question like '1. %'\n" +
					 "and next_line.Stg1_Question_Id = r_q.Stg1_Question_Id + 1\n" +
				"join stg1_question choice\n" +
					"on choice.Stg1_Question_Id > r_q.Stg1_Question_Id\n" +
					"and ( choice.Question REGEXP '^[0-9]+\\. ' = 1 )\n" +
					"and choice.Stg1_Question_Id <\n" +
						"(\n" +
						"select min(next_question.Stg1_Question_Id)\n" +
						"from stg1_question next_question\n" +
						"where ( Question = '' and Prescription = '' )\n" +
							"and next_question.Stg1_Question_Id > q.Stg1_Question_Id\n" +
						")\n" +
				"join stg2_possible_answer ans\n" +
					"on ans.Stg1_Question_Id = choice.Stg1_Question_Id\n" +
				"join stg1_question choice_2\n" +
					"on choice_2.Stg1_Question_Id > choice.Stg1_Question_Id\n" +
					"and ( choice_2.Question REGEXP '^[a-z]\\. ' = 1 )\n" +
					"and choice_2.Stg1_Question_Id <\n" +
						"(\n" +
						"select min(next_choice.Stg1_Question_Id)\n" +
						"from stg1_question next_choice\n" +
						"where\n" + 
							"next_choice.Stg1_Question_Id > choice.Stg1_Question_Id\n" +
							"and\n" +
								"(\n" +
								"( next_choice.Question REGEXP '^[0-9]+\\. ' = 1 )\n" + 
								"or ( next_choice.Question = '' and next_choice.Prescription = '' )\n" +
							")\n" +
						")\n" + 
				") as sub";
		this.executeForDocId(sSql);
		System.out.println("populateLevel2Questions() #1 Layer 2 Questions\n" + sSql);
		
		sSql =
			"insert into stg2_possible_answer (Stg2_Question_Id, Choice_Text, Stg1_Question_Id, Src_Doc_Id)\n" +
			"select quest.Stg2_Question_Id,\n" +
			    "trim(right(substring_index(choice_2.Question,'\n',1), length(substring_index(choice_2.Question,'\n',1)) - locate('. ', choice_2.Question))) 'choice 2 text',\n" +
				"choice_2.Stg1_Question_Id, choice_2.Src_Doc_Id\n" +
			"from stg2_question q\n" +
			"join stg1_question r_q on q.Stg1_Question_Id = r_q.Stg1_Question_Id\n" +
			"join stg1_question next_line on next_line.Question like '1. %' and next_line.Stg1_Question_Id = r_q.Stg1_Question_Id + 1\n" +
			"join stg1_question choice\n" +
				"on choice.Stg1_Question_Id > r_q.Stg1_Question_Id\n" +
			    "and ( choice.Question REGEXP '^[0-9]+\\. ' = 1 )\n" +
			    "and choice.Stg1_Question_Id <\n" +
					"(\n" +
			        "select min(next_question.Stg1_Question_Id)\n" +
			        "from stg1_question next_question\n" +
			        "where ( Question = '' and Prescription = '' )\n" +
			            "and next_question.Stg1_Question_Id > q.Stg1_Question_Id\n" +
			        ")\n" +
			"join stg2_question quest\n" +
				"on quest.Stg1_Question_Id = choice.Stg1_Question_Id\n" +
			"join stg1_question choice_2\n" +
				"on choice_2.Stg1_Question_Id > choice.Stg1_Question_Id\n" +
			    "and ( choice_2.Question REGEXP '^[a-z]\\. ' = 1 )\n" +
			    "and choice_2.Stg1_Question_Id < (\n" +
			        "select min(next_choice.Stg1_Question_Id)\n" +
			        "from stg1_question next_choice\n" +
			        "where\n" + 
						"next_choice.Stg1_Question_Id > choice.Stg1_Question_Id\n" +
						"and\n" +
							"(\n" +
			                "( next_choice.Question REGEXP '^[0-9]+\\. ' = 1 )\n" + 
							"or ( next_choice.Question = '' and next_choice.Prescription = '' )\n" +
						")\n" +
			    ")\n" +
			"WHERE q.Src_Doc_Id = ?";
		this.executeForDocId(sSql);
		System.out.println("populateLevel2Questions() #2 Level 2 possible answers\n" + sSql);

		sSql = "update stg2_possible_answer set choice_text = trim(right(choice_text, length(choice_text) - locate('. ', choice_text))) where choice_text regexp '^([[:digit:]]|[[:alpha:]])+\\. '" +
				" AND Src_Doc_Id = ?";
		this.executeForDocId(sSql);
			
		sSql = "update stg2_possible_answer set choice_text = replace( trim(replace(choice_text,'\n',' ')), '  ', ' ') where choice_text regexp '\n'" +
				" AND Src_Doc_Id = ?";
		this.executeForDocId(sSql);
	}
	
	public void populateLevel3Questions() throws SQLException {
		String sSql =
			"INSERT INTO Stg2_Question\n" +
			"(Stg1_Question_Id, question_name, question_text, question_type, rule, normal_rule, type, category, Is_Baseline, Src_Doc_Id, Parse_Level)\n" +
			"select\n" + 
				"Stg1_Question_Id,\n" + 
			    "case when question_name='' then concat('[', q_name, ']') else question_name end,\n" + 
			    "question_text,\n" + 
			    "case\n" + 
			    	"when type like '%SELECT ONE OR MORE%' then 'Multiple Answer'\n" + // Added
					"when\n" + 
						"( type regexp '.*(MAY|MUST|HAS TO) SELECT ONE.*' and type not like '%OR MORE%' )\n" +
						"or type regexp '.*(MAY|MUST|HAS TO) SELECT EITHER.*'\n" + 
						"then 'Single Answer'\n" +
			        "when type regexp '.*((MAY|MUST|HAS TO) )*SELECT ONE OR MORE.*'\n" + 
						"or type regexp '.*(MAY|MUST|HAS TO) SELECT AT LEAST ONE.*'\n" + 
			            "or type like '%CAN SELECT ALL%'\n" + 
			            "or type like '%MANY%'\n" + 
			            "or type like '%AT LEAST%'\n" +
						"then 'Multiple Answer'\n" +
					"when type regexp '.*(MAY|MUST|HAS TO) FILL IN A (MIN|NUMBER)*.*' then 'Numeric'\n" +
					"else 'Single Answer'\n" + // Added
					// "when type like '%SELECT ONE ONLY%' then 'Single Answer'\n" + // Added
				 
				 "end question_type , parsed_rule, rule, type, category, Is_Baseline, Src_Doc_Id, 2\n" +
			"from\n" +
				"(\n" +
				"select \n" +
					"distinct \n" +
					"choice.Stg1_Question_Id,\n" + 
			        "substr(choice.Question,instr(choice.Question,'['),instr(choice.Question,']')-instr(choice.Question,'[')+1) question_name,\n" +
					"concat('Select ', lower(trim(substring_index(substring_index(choice.Question,'\\n',1),'. ',-1))),' subtype(s):') question_text,\n" +
					"concat('\"', ans.choice_text,'\" = ', q.question_name) parsed_rule,\n" +
			        "substring_index(choice.Question, 'USER ', -1) type,\n" +
			        "choice.Question rule,\n" +
			        "upper(trim(left(substring_index(substring_index(choice.Question,'\\n',1),'. ',-1),48))) q_name,\n" + 
			        "case\n" +
						"WHEN r_q.Is_Baseline = 1 THEN 'Baseline'\n" +
			            "ELSE 'Follow-on'\n" +
					"END category,\n" +
			        "r_q.Is_Baseline, r_q.Src_Doc_Id\n" +
				"from stg2_question q\n" +
				"join stg1_question r_q\n" +
					"on q.Stg1_Question_Id = r_q.Stg1_Question_Id\n" +
				"join stg1_question next_line\n" +
					 "on next_line.Question like 'a. %'\n" +
					 "and next_line.Stg1_Question_Id = r_q.Stg1_Question_Id + 1\n" +
				"join stg1_question choice\n" +
					"on choice.Stg1_Question_Id > r_q.Stg1_Question_Id\n" +
					"and ( choice.Question REGEXP '^[a-z]\\. ' = 1 )\n" +
					"and choice.Stg1_Question_Id <\n" +
						"(\n" +
						"select min(next_question.Stg1_Question_Id)\n" +
						"from stg1_question next_question\n" +
						"where\n" + 
							"(\n" +
			                "( Question = '' and Prescription = '' )\n" +
							"or ( Question REGEXP '^[0-9]+\\. ' = 1 )\n" +
			                ")\n" +
							"and next_question.Stg1_Question_Id > q.Stg1_Question_Id\n" +
						")\n" +
				"join stg2_possible_answer ans\n" +
					"on ans.Stg1_Question_Id = choice.Stg1_Question_Id\n" +
				"join stg1_question choice_2\n" +
					"on choice_2.Stg1_Question_Id > choice.Stg1_Question_Id\n" +
					"and ( choice_2.Question REGEXP '^\\((XC|XL|L?X{0,3})(IX|IV|V?I{0,3})\\) ' = 1 )\n" +
					"and choice_2.Stg1_Question_Id <\n" +
						"(\n" +
						"select min(next_choice.Stg1_Question_Id)\n" +
						"from stg1_question next_choice\n" +
						"where\n" + 
							"next_choice.Stg1_Question_Id > choice.Stg1_Question_Id\n" +
							"and\n" +
								"(\n" +
								"( next_choice.Question REGEXP '^[a-z]\\. ' = 1 )\n" + 
			                    "or ( next_choice.Question REGEXP '^[0-9]+\\. ' = 1 )\n" + 
								"or ( next_choice.Question = '' and next_choice.Prescription = '' )\n" +
							")\n" +
						")\n" +
				"where r_q.Src_Doc_Id = ?\n" +
				") as sub";
		this.executeForDocId(sSql);
		System.out.println("populateLevel3Questions() #1 Level 3 questions\n" + sSql);
		
		sSql = 
			"insert into stg2_possible_answer (Stg2_Question_Id, Choice_Text, Stg1_Question_Id, Src_Doc_Id)\n" +
			"select \n" +
				"quest.Stg2_Question_Id,\n" +
			    "trim(right(substring_index(choice_2.Question,'\n',1), length(substring_index(choice_2.Question,'\n',1)) - locate(') ', choice_2.Question))) 'choice 2 text',\n" +
				"choice_2.Stg1_Question_Id, choice_2.Src_Doc_Id\n" +
			"from Stg2_Question q\n" +
			"join Stg1_Question r_q\n" +
				"on q.Stg1_Question_Id = r_q.Stg1_Question_Id\n" +
			"join Stg1_Question next_line\n" +
				"on next_line.Question like 'a. %'\n" +
			    "and next_line.Stg1_Question_Id = r_q.Stg1_Question_Id + 1\n" +
			"join Stg1_Question choice\n" +
				"on choice.Stg1_Question_Id > r_q.Stg1_Question_Id\n" +
			    "and ( choice.Question REGEXP '^[a-z]\\. ' = 1 )\n" +
			    "and choice.Stg1_Question_Id <\n" +
					"(\n" +
			        "select min(next_question.Stg1_Question_Id)\n" +
						"from Stg1_Question next_question\n" +
						"where(\n" +
			                "( Question = '' and Prescription = '' )\n" +
							"or ( Question REGEXP '^[0-9]+\\. ' = 1 )\n" +
			                ")\n" +
							"and next_question.Stg1_Question_Id > q.Stg1_Question_Id\n" +
			        ")\n" +
			"join Stg2_Question quest\n" +
				"on quest.Stg1_Question_Id = choice.Stg1_Question_Id\n" +
			"join Stg1_Question choice_2\n" +
				"on choice_2.Stg1_Question_Id > choice.Stg1_Question_Id\n" +
			    "and ( choice_2.Question REGEXP '^\\((XC|XL|L?X{0,3})(IX|IV|V?I{0,3})\\) ' = 1 )\n" +
			    "and choice_2.Stg1_Question_Id <\n" +
					"(\n" +
			        "select min(next_choice.Stg1_Question_Id)\n" +
			        "from Stg1_Question next_choice\n" +
			        "where\n" + 
						"next_choice.Stg1_Question_Id > choice.Stg1_Question_Id\n" +
						"and\n" +
							"(\n" +
			                "( next_choice.Question REGEXP '^[a-z]\\. ' = 1 )\n" + 
			                "or ( next_choice.Question REGEXP '^[0-9]+\\. ' = 1 ) \n" +
							"or ( next_choice.Question = '' and next_choice.Prescription = '' )\n" +
						")\n" +
			        ")\n" +
			" WHERE choice_2.Src_Doc_Id = ?";
		System.out.println("populateLevel3Questions() #2 Level 3 possible answers\n" + sSql);
	
		sSql = "update stg2_possible_answer set choice_text = trim(right(choice_text, length(choice_text) - locate('. ', choice_text))) where choice_text regexp '^([[:digit:]]|[[:alpha:]])+\\. '" +
				" AND Src_Doc_Id = ?";
		this.executeForDocId(sSql);
			
		sSql = "update stg2_possible_answer set choice_text = replace( trim(replace(choice_text,'\n','')), '  ', ' ') where choice_text regexp '\n'" +
				" AND Src_Doc_Id = ?";
		this.executeForDocId(sSql);
	}
	
	public void populateLevel4Questions() throws SQLException {
		String sSql = "INSERT INTO Stg2_Question\n" +
			"(Stg1_Question_Id, question_name, question_text, question_type, rule, normal_rule, type, Is_Baseline, Src_Doc_Id, Parse_Level)\n" +
			"select\n" + 
				"Stg1_Question_Id,\n" + 
			    "case when question_name='' then concat('[', q_name, ']') else question_name end,\n" + 
			    "question_text,\n" + 
			    "case\n" + 
			    	"when type like '%SELECT ONE OR MORE%' then 'Multiple Answer'\n" + // Added
					"when\n" +
						"( type regexp '.*(MAY|MUST|HAS TO) SELECT ONE.*' and type not like '%OR MORE%' )\n" +
						"or type regexp '.*(MAY|MUST|HAS TO) SELECT EITHER.*'\n" + 
						"then 'Single Answer'\n" +
			        "when type regexp '.*((MAY|MUST|HAS TO) )*SELECT ONE OR MORE.*'\n" + 
						"or type regexp '.*(MAY|MUST|HAS TO) SELECT AT LEAST ONE.*'\n" + 
			            "or type like '%CAN SELECT ALL%'\n" + 
			            "or type like '%MANY%'\n" + 
			            "or type like '%AT LEAST%'\n" +
						"then 'Multiple Answer'\n" +
					"when type regexp '.*(MAY|MUST|HAS TO) FILL IN A (MIN|NUMBER)*.*' then 'Numeric'\n" +

					// added
					"else 'Single Answer'\n" +
				"end question_type , parsed_rule, rule, type, Is_Baseline, Src_Doc_Id, 3\n" +
			"from\n" +
				"(\n" +
				"select \n" +
					"distinct choice.Stg1_Question_Id,\n" + 
			        "substr(choice.Question,instr(choice.Question,'['),instr(choice.Question,']')-instr(choice.Question,'[')+1) question_name,\n" +
					"concat('Select ', lower(trim(substring_index(substring_index(choice.Question,'\n',1),') ',-1))),' subtype(s):') question_text,\n" +
					"concat('\"', ans.choice_text,'\" = ', q.question_name) parsed_rule,\n" +
			        "substring_index(choice.Question, 'USER ', -1) type,\n" +
					"substring_index(r_q.Question, 'USER ', -1) type2,\n" + // added
			        "choice.Question rule,\n" +
			        "upper(trim(left(substring_index(substring_index(choice.Question,'\n',1),') ',-1),48))) q_name,\n" +
			        "choice.Is_Baseline, choice.Src_Doc_Id\n" +
				"from Stg2_Question q\n" +
				"join Stg1_Question r_q\n" +
					"on q.Stg1_Question_Id = r_q.Stg1_Question_Id\n" +
				"join Stg1_Question next_line\n" +
					 "on next_line.Question like '(i) %'\n" +
					 "and next_line.Stg1_Question_Id = r_q.Stg1_Question_Id + 1\n" +
				"join Stg1_Question choice\n" +
					"on choice.Stg1_Question_Id > r_q.Stg1_Question_Id\n" +
					"and ( choice.Question REGEXP '^\\((XC|XL|L?X{0,3})(IX|IV|V?I{0,3})\\) ')\n" +
					"and choice.Stg1_Question_Id <\n" +
						"(\n" +
						"select min(next_question.Stg1_Question_Id)\n" +
						"from Stg1_Question next_question\n" +
						"where\n" + 
							"(\n" +
			                "( Question = '' and Prescription = '' )\n" +
							"or ( Question REGEXP '^[0-9]+\\. ' )\n" +
			                "or ( Question REGEXP '^[a-z]\\. ' )\n" +
			                ")\n" +
							"and next_question.Stg1_Question_Id > q.Stg1_Question_Id\n" +
						")\n" +
				"join stg2_possible_answer ans\n" +
					"on ans.Stg1_Question_Id = choice.Stg1_Question_Id\n" +
				"join Stg1_Question choice_2\n" +
					"on choice_2.Stg1_Question_Id > choice.Stg1_Question_Id\n" +
					"and ( choice_2.Question REGEXP '^\\(A\\) ' )\n" +
					"and choice_2.Stg1_Question_Id <\n" +
						"(\n" +
						"select min(next_choice.Stg1_Question_Id)\n" +
						"from Stg1_Question next_choice\n" +
						"where\n" + 
							"next_choice.Stg1_Question_Id > choice.Stg1_Question_Id\n" +
							"and\n" +
								"(\n" +
								"( next_choice.Question REGEXP '^[a-z]\\. ' )\n" + 
			                    "or ( next_choice.Question REGEXP '^[0-9]+\\. ' )\n" + 
			                    "or ( next_choice.Question REGEXP '^\\((XC|XL|L?X{0,3})(IX|IV|V?I{0,3})\\) ' )\n" +
								"or ( next_choice.Question = '' and next_choice.Prescription = '' )\n" +
							")\n" +
						")\n" + 
				" WHERE choice.Src_Doc_Id = ?\n" +
				") as sub";
		this.executeForDocId(sSql);
		System.out.println("populateLevel4Questions() #1 Level 4 questions\n" + sSql);
		
		sSql = "insert into stg2_possible_answer (Stg2_Question_Id, Choice_Text, Stg1_Question_Id, Src_Doc_Id)\n" +
				"select \n" +
					"quest.Stg2_Question_Id,\n" +
				    "trim(right(substring_index(choice_2.Question,'\n',1), length(substring_index(choice_2.Question,'\n',1)) - locate(') ', choice_2.Question))) 'choice 2 text',\n" +
					"choice_2.Stg1_Question_Id, choice_2.Src_Doc_Id\n" +
				"from Stg2_Question q\n" +
					"join Stg1_Question r_q\n" +
						"on q.Stg1_Question_Id = r_q.Stg1_Question_Id\n" +
					"join Stg1_Question next_line\n" +
						 "on next_line.Question like '(i) %'\n" +
						 "and next_line.Stg1_Question_Id = r_q.Stg1_Question_Id + 1\n" +
					"join Stg1_Question choice\n" +
						"on choice.Stg1_Question_Id > r_q.Stg1_Question_Id\n" +
						"and ( choice.Question REGEXP '^\\((XC|XL|L?X{0,3})(IX|IV|V?I{0,3})\\) ')\n" +
						"and choice.Stg1_Question_Id <\n" +
							"(\n" +
							"select min(next_question.Stg1_Question_Id)\n" +
							"from Stg1_Question next_question\n" +
							"where\n" + 
								"(\n" +
				                "( Question = '' and Prescription = '' )\n" +
								"or ( Question REGEXP '^[0-9]+\\. ' )\n" +
				                "or ( Question REGEXP '^[a-z]\\. ' )\n" +
				                ")\n" +
								"and next_question.Stg1_Question_Id > q.Stg1_Question_Id\n" +
							")\n" +
					"join Stg2_Question quest\n" +
						"on quest.Stg1_Question_Id = choice.Stg1_Question_Id\n" +
					"join Stg1_Question choice_2\n" +
						"on choice_2.Stg1_Question_Id > choice.Stg1_Question_Id\n" +
						"and ( choice_2.Question REGEXP '^\\([A-Z]) ' )\n" +
						"and choice_2.Stg1_Question_Id <\n" +
							"(\n" +
							"select min(next_choice.Stg1_Question_Id)\n" +
							"from Stg1_Question next_choice\n" +
							"where\n" + 
								"next_choice.Stg1_Question_Id > choice.Stg1_Question_Id\n" +
								"and\n" +
									"(\n" +
									"( next_choice.Question REGEXP '^[a-z]\\. ' )\n" + 
				                    "or ( next_choice.Question REGEXP '^[0-9]+\\. ' )\n" + 
				                    "or ( next_choice.Question REGEXP '^\\((IX|IV|V?I{0,3})\\) ' )\n" +
									"or ( next_choice.Question = '' and next_choice.Prescription = '' )\n" +
								")\n" +
							")\n" +
					" WHERE choice_2.Src_Doc_Id = ?";
		this.executeForDocId(sSql);
		System.out.println("populateLevel4Questions() #2 Level 4 possible answers\n" + sSql);
		
		sSql = "UPDATE Stg2_Question\n" +
				"SET category = if(Is_Baseline=1,'Baseline','Follow-on')\n" +
				"WHERE Src_Doc_Id = ?";
		this.executeForDocId(sSql);
		System.out.println("populateLevel4Questions() #3 SET category");
	}
	
	public void cleanupChoiceText() throws SQLException {
		String sSql = "update stg2_possible_answer\n" +
				"set choice_text = trim(right(choice_text, length(choice_text) - locate('. ', choice_text)))\n" +
				"WHERE Src_Doc_Id = ? AND choice_text  REGEXP '^[a-z]\\. '";
		this.executeForDocId(sSql);
		System.out.println("cleanupChoiceText() #1");
		
		sSql = "update stg2_possible_answer\n" +
				"set choice_text = trim(right(choice_text, length(choice_text) - locate('. ', choice_text)))\n" +
				"WHERE Src_Doc_Id = ? AND choice_text  REGEXP '^[0-9]+\\. '";
		this.executeForDocId(sSql);
		System.out.println("cleanupChoiceText() #2");
		
		sSql = "update stg2_possible_answer\n" +
				"set choice_text = trim(right(choice_text, length(choice_text) - locate(') ', choice_text)))\n" +
				"WHERE Src_Doc_Id = ? AND choice_text REGEXP '^\\((XC|XL|L?X{0,3})(IX|IV|V?I{0,3})\\) '";
		this.executeForDocId(sSql);
		System.out.println("cleanupChoiceText() #3");
		
		sSql = "update stg2_possible_answer\n" +
				"set choice_text = trim(right(choice_text, length(choice_text) - locate(') ', choice_text)))\n" +
				"WHERE Src_Doc_Id = ? AND choice_text  REGEXP '^\\([A-Z]\\) '";
		this.executeForDocId(sSql);
		System.out.println("cleanupChoiceText() #4");
		
		// CALL normalize_string('Stg2_Question','question_text', ' ');
		
		sSql = "update stg2_possible_answer\n" +
				"set choice_text = trim(choice_text)\n" +
				"WHERE Src_Doc_Id = ?";
		this.executeForDocId(sSql);
		System.out.println("cleanupChoiceText() #5");
	}
	
	public void setStg2QuestionGroupName() throws SQLException {
		String sSql = "UPDATE Stg2_Question\n" +
			"SET group_name = (\n" +
				"SELECT stg_question_sheet_ref.Sheet_Name\n" +
			    "FROM Stg1_Question\n" +
			    "INNER JOIN stg_question_sheet_ref ON (stg_question_sheet_ref.Sheet_Id = Stg1_Question.Sheet_Id)\n" +
			    "WHERE Stg1_Question_Id = Stg2_Question.Stg1_Question_Id\n" +
			")\n" +
			"WHERE Src_Doc_Id = ?";
		this.executeForDocId(sSql);
		System.out.println("setStg2QuestionGroupName() #1");
		
		sSql = "update Stg2_Question q\n" +
			"set category = 'Waterfall'\n" +
			"where Src_Doc_Id = ?\n" +
			"and (\n" +
			    "select 1\n" +
			    "from Stg1_Question\n" +
			    "where Stg1_Question_Id = q.Stg1_Question_Id\n" +
					"and (\n" +
			            "( trim(Question) REGEXP '^[0-9]+\\. ' )\n" + 
			            "or ( trim(Question) REGEXP '^[a-z]\\. ' )\n" + 
			            "or ( trim(Question) REGEXP '^\\((IX|IV|V?I{0,3})\\) ' )\n" +
			            ")\n" +
					")\n" +
					"or (\n" +
						"category = 'Follow-on'\n" +
						"and exists (\n" +
							"select 1\n" +
							"from Stg1_Question\n" +
							"where Stg1_Question_Id = q.Stg1_Question_Id + 1\n" +
								"and (\n" +
									"( trim(Question) REGEXP '^[0-9]+\\. ' )\n" + 
									"or ( trim(Question) REGEXP '^[a-z]\\. ' )\n" + 
									"or ( trim(Question) REGEXP '^\\((IX|IV|V?I{0,3})\\) '\n" + 
								")\n" +
						")\n" +
					")\n" +
			")";
		this.executeForDocId(sSql);
		System.out.println("setStg2QuestionGroupName() #2 Waterfall");
		
	}
	
	public void normalizeDisplayRules() throws SQLException {
		String sSql = "UPDATE Stg2_Question\n" +
				"SET normal_rule =\n" + 
					"REPLACE(normal_rule,\n" +
						"'ASK THE QUESTION ONLY AFTER THE USER SELECTS ONE OR MORE SUPPLY ITEMS',\n" +
						"'ASK ONLY IF THE USER SELECTS \"YES\" TO \"SUPPLIES\"')\n" +
				"WHERE Src_Doc_Id = ?\n" +
				"AND normal_rule like '%ASK THE QUESTION ONLY AFTER THE USER SELECTS ONE OR MORE SUPPLY ITEMS%'";
		this.executeForDocId(sSql);
		System.out.println("normalizeDisplayRules() #1");

		sSql = "UPDATE Stg2_Question\n" + 
				"SET normal_rule =\n" + 
					"REPLACE(normal_rule,\n" +
						"'ASK THE QUESTION ONLY AFTER THE USER SELECTS ONE OR MORE SERVICE ITEMS',\n" +
						"'ASK ONLY IF THE USER SELECTS \"YES\" TO \"SERVICES\"')\n" +
				"WHERE Src_Doc_Id = ?\n" +
				"AND normal_rule like '%ASK THE QUESTION ONLY AFTER THE USER SELECTS ONE OR MORE SERVICE ITEMS%'";
		this.executeForDocId(sSql);
		System.out.println("normalizeDisplayRules() #2");

		sSql = "UPDATE Stg2_Question\n" +
				"SET normal_rule = 'ASK ONLY AFTER USER SELECTS \"COMPETITION\" UNDER \"PROCEDURES\" AND \"SOLICITATION\" UNDER \"DOCUMENT TYPE\"'\n" +
				"WHERE Src_Doc_Id = ?\n" +
				"AND question_name = '[UK OFFER]'";
		this.executeForDocId(sSql);
		System.out.println("normalizeDisplayRules() #3");

		sSql = "UPDATE Stg2_Question\n" +
				"SET normal_rule = REPLACE(normal_rule, '\"SUPPLIES CLASSIFICATION\"','\"SUPPLY CLASSIFICATION\"')\n" +
				"WHERE Src_Doc_Id = ?\n" +
				"AND question_name = '[RECOVERED MATERIAL ESTIMATES]'";
		this.executeForDocId(sSql);
		System.out.println("normalizeDisplayRules() #4");

		sSql = "UPDATE Stg2_Question\n" +
				"SET normal_rule = 'ASK ONLY IF USER SELECTS \"GOVERNMENT SUPPLY SOURCE INFORMATION/AUTHORIZATION APPLIES\" UNDER \"CONTRACTOR MATERIAL\"'\n" +
				"WHERE Src_Doc_Id = ?\n" +
				"AND question_name = '[GOVERNMENT SUPPLY STATEMENT]'";
		this.executeForDocId(sSql);
		System.out.println("normalizeDisplayRules() #5");
	}
	
	public void correctParsedRules() throws SQLException {
		String sSql = "update Stg2_Question\n" +
				"set parsed_rule = '[AGREEMENT (INCLUDING BASIC AND LOAN] IS NOT NULL OR \"REQUIREMENTS\" = [TYPE OF INDEFINITE DELIVERY]'\n" +
				"WHERE Src_Doc_Id = ?\n" +
				"AND question_name = '[ORDER VALUE]'";
		this.executeForDocId(sSql);
		System.out.println("correctParsedRules() #1");

		sSql = "update Stg2_Question\n" +
				"set parsed_rule = '[AGREEMENT (INCLUDING BASIC AND LOAN] = \"LEASE AGREEMENT\"'\n" +
				"WHERE Src_Doc_Id = ?\n" +
				"AND question_name = '[LEASE OPTION]'";
		this.executeForDocId(sSql);
		System.out.println("correctParsedRules() #2");

		sSql = "update Stg2_Question\n" +
				"set parsed_rule = '[PROCEDURES] IS NOT NULL AND \"FULL AND OPEN COMPETITION (FAR PART 6)\" <> [COMPETITION TYPE]'\n" +
				"WHERE Src_Doc_Id = ?\n" +
				"AND question_name = '[FULL AND OPEN EXCEPTION]'";
		this.executeForDocId(sSql);
		System.out.println("correctParsedRules() #3");

		sSql = "update Stg2_Question\n" +
				"set parsed_rule = '(\"MAJOR DEFENSE PROGRAM\" = [TYPE OF SYSTEM] OR \"MAJOR INFORMATION SYSTEMS PROGRAM\" = [TYPE OF SYSTEM]) AND \"SOLICITATION\" = [DOCUMENT TYPE]'\n" +
				"WHERE Src_Doc_Id = ?\n" +
				"AND question_name = '[OSD APPROVAL FOR DATA SYSTEM]'";
		this.executeForDocId(sSql);
		System.out.println("correctParsedRules() #4");
		
		sSql = "update Stg2_Question\n" +
				"set parsed_rule = \n" +
				"(\n" +
					"select concat(group_concat(question_name separator ' IS NOT NULL AND '),' IS NOT NULL')\n" +
					"from ( select question_name from Stg2_Question ) sub\n" +
					"where question_name like concat('[%', substring_index(substring_index(Stg2_Question.rule,'\"',2),'\"',-1) ,'%]')\n" +
				")\n" +
				"WHERE Src_Doc_Id = ?\n" +
				"AND normal_rule like '%RESPONSES ARE COMPLETE%'";
		this.executeForDocId(sSql);
		System.out.println("correctParsedRules() #5");
		
		sSql = "update Stg2_Question\n" +
				"set parsed_rule = \n" +
					"case\n" + 
						"when\n" + 
							"normal_rule REGEXP 'ASK [[:print:]]* \"[[:print:]]*\" IS COMPLETED (AND|OR) [[:print:]]* NOT SELECTED \"[[:print:]]*\" (IS SELECTED UNDER|TO|TO THE QUESTION|WITHIN|UNDER|FOR) \"[[:print:]]*\"'\n" +
				            "then\n" + 
								"concat('[',\n" +
									"substring_index(substring_index(normal_rule,'\"',2),'\"',-1),\n" +
									"'] IS NOT NULL ',\n" +
				                    "substring_index(substring_index(normal_rule,'\" IS COMPLETED ',-1),' ',1),\n" +
				                    "' \"',\n" +
									"substring_index(substring_index(normal_rule,'\"',4),'\"',-1),\n" +
									"'\" <> [',\n" +
				                    "substring_index(substring_index(normal_rule,'\"',6),'\"',-1),\n" +
				                    "']')\n" +
						"when\n" + 
							"normal_rule REGEXP 'ASK [[:print:]]* \"[[:print:]]*\" IS COMPLETED (AND|OR) [[:print:]]* (SELECTS|SELECTED) \"[[:print:]]*\" (IS SELECTED UNDER|TO|TO THE QUESTION|WITHIN|UNDER|FOR) \"[[:print:]]*\"'\n" +
				            "then\n" + 
								"concat('[',\n" +
									"substring_index(substring_index(normal_rule,'\"',2),'\"',-1),\n" +
									"'] IS NOT NULL ',\n" +
				                    "substring_index(substring_index(normal_rule,'\" IS COMPLETED ',-1),' ',1),\n" +
				                    "' \"',\n" +
									"substring_index(substring_index(normal_rule,'\"',4),'\"',-1),\n" +
									"'\" = [',\n" +
				                    "substring_index(substring_index(normal_rule,'\"',6),'\"',-1),\n" +
				                    "']')\n" +
						"when\n" + 
				            "normal_rule REGEXP 'ASK [[:print:]]* \"[[:print:]]*\" OR \"[[:print:]]*\"" +
				            	" OR \"[[:print:]]*\" (TO|TO THE QUESTION|WITHIN|UNDER|FOR)" +
				            	" \"[[:print:]]*\"\\.*'\n" +
				            "then\n" + 
								"concat(\n" +
									"'\"',\n" +
									"substring_index(substring_index(normal_rule,'\"',2),'\"',-1),\n" +
									"'\" = [',\n" +
				                    "substring_index(substring_index(normal_rule,'\"',8),'\"',-1),\n" +
				                    "'] OR \"',\n" +
				                    "substring_index(substring_index(normal_rule,'\"',4),'\"',-1),\n" +
									"'\" = [',\n" +
				                    "substring_index(substring_index(normal_rule,'\"',8),'\"',-1),\n" +
				                    "'] OR \"',\n" +
				                    "substring_index(substring_index(normal_rule,'\"',6),'\"',-1),\n" +
									"'\" = [',\n" +
				                    "substring_index(substring_index(normal_rule,'\"',8),'\"',-1),\n" +
				                    "']')\n" +
						"when normal_rule REGEXP\n" + 
								"'ASK [[:print:]]* \"[[:print:]]*\" (IS SELECTED UNDER|TO|TO THE QUESTION|WITHIN|UNDER|FOR) \"[[:print:]]*\" (AND|OR) \"[[:print:]]*\" (IS SELECTED UNDER|TO|TO THE QUESTION|WITHIN|UNDER|FOR) \"[[:print:]]*\"\\.*'\n" +
				            "then\n" + 
								"concat(\n" +
									"'\"',\n" +
				                    "substring_index(substring_index(normal_rule,'\"',2),'\"',-1),\n" +
									"'\" = [',\n" +
				                    "substring_index(substring_index(normal_rule,'\"',4),'\"',-1),']',\n" +
				                    "substring_index(substring_index(normal_rule,'\"',5),'\"',-1),'\"',\n" +
				                    "substring_index(substring_index(normal_rule,'\"',6),'\"',-1),\n" +
									"'\" = [',\n" +
				                    "substring_index(substring_index(normal_rule,'\"',8),'\"',-1),\n" +
				                    "']')\n" +
						"when\n" + 
				            "normal_rule REGEXP 'ASK [[:print:]]* \"[[:print:]]*\" OR \"[[:print:]]*\" (TO|TO THE QUESTION|WITHIN|UNDER|FOR) \"[[:print:]]*\"\\.*'\n" +
				            "then\n" + 
								"concat(\n" +
									"'\"',\n" +
				                    "substring_index(substring_index(normal_rule,'\"',2),'\"',-1),\n" +
									"'\" = [',\n" +
				                    "substring_index(substring_index(normal_rule,'\"',6),'\"',-1),\n" +
				                    "'] OR \"',\n" +
				                    "substring_index(substring_index(normal_rule,'\"',4),'\"',-1),\n" +
									"'\" = [',\n" +
				                    "substring_index(substring_index(normal_rule,'\"',6),'\"',-1),\n" +
				                    "']')\n" +
						"when normal_rule REGEXP\n" + 
								"'ASK [[:print:]]* \"[[:print:]]*\" (TO|TO THE QUESTION|WITHIN|UNDER|FOR) \"[[:print:]]*\"$'\n" +
				            "then\n" + 
								"concat('\"',\n" +
									"substring_index(substring_index(normal_rule,'\"',2),'\"',-1),\n" +
									"'\" = [',\n" +
				                    "substring_index(substring_index(normal_rule,'\"',4),'\"',-1),\n" +
				                    "']')\n" +
						"when\n" + 
							"(\n" +
				            "normal_rule like 'ASK% AFTER %USER ANSWERS THE QUESTION \"%'\n" +
							"or normal_rule like 'ASK AFTER USER SELECT% THE \"%\"'\n" +
				            "or normal_rule like 'ASK % \"%\" %COMPLETED'\n" +
				            "or normal_rule like 'ASK % \"%\" %COMPLETE'\n" +
				            "or normal_rule like 'ASK % ANSWERED FOR \"%\"'\n" +
				            "or normal_rule like 'ASK % \"%\" % SELECTED'\n" +
				            "or normal_rule like 'ASK % \"%\" %HAS BEEN ANSWERED'\n" +
							")\n" +
				            "then\n" + 
								"concat(\n" +
									"'[', substring_index(substring_index(rule,'\"',2),'\"',-1),\n" +
				                    "'] IS NOT NULL'\n" +
				                    ")\n" +
						"when\n" + 
							"normal_rule REGEXP 'ASK [[:print:]]* \"[[:print:]]*\" (IS SELECTED UNDER|TO|TO THE QUESTION|WITHIN|UNDER|FOR) \"[[:print:]]*\"\\.*'\n" +
				            "then\n" + 
								"concat('\"',\n" +
									"substring_index(substring_index(normal_rule,'\"',2),'\"',-1),\n" +
									"'\" = [',\n" +
				                    "substring_index(substring_index(normal_rule,'\"',4),'\"',-1),\n" +
				                    "']')\n" +
				    "end\n" +
				"WHERE Src_Doc_Id = ?\n" +
				"AND parsed_rule is null";
		this.executeForDocId(sSql);
		System.out.println("correctParsedRules() #6");
	}
	
	public void normalizeQuestionDependencies() throws SQLException {
		NormalizeQuestionDependencies.normalize(this.conn, this.srcDocId);
		
		String sSql = "UPDATE Stg2_Question_Dependency d\n" +
			"SET referenced_question_id = (\n" +
				"SELECT Stg2_Question_Id\n" +
			    "FROM Stg2_Question\n" +
			    "WHERE question_name = d.referenced_question_name\n" +
			    "LIMIT 1\n" +
			")\n" +
			"WHERE Stg2_Question_Id IN (\n" +
				"SELECT Stg2_Question_Id FROM Stg2_Question WHERE Src_Doc_Id = ?\n" +
			")";
		this.executeForDocId(sSql);
		System.out.println("normalizeQuestionDependencies() #2");
		
		sSql = "update Stg2_Question q\n" +
			"set q.latest_parent_question_id = (\n" +
			     "select sub.latest_parent_question_id\n" +
			     "from (\n" +
			        "select\n" + 
						"sub.Stg2_Question_Id question_id,\n" + 
						"latest_q.Stg2_Question_Id latest_parent_question_id\n" +
			        "from (\n" +
						"select d.Stg2_Question_Id, max(referenced_q.Stg2_Question_Id) latest_parent_question_id\n" +
						"from Stg2_Question_Dependency d\n" +
						"join Stg2_Question referenced_q on referenced_q.Stg2_Question_Id = d.referenced_question_id\n" +
						"group by d.Stg2_Question_Id\n" +
					") sub\n" +
					"join Stg2_Question latest_q on sub.latest_parent_question_id = latest_q.Stg2_Question_Id\n" +
			 	") sub\n" +
				"where sub.question_id = q.Stg2_Question_Id\n" +
			")\n" +
			"WHERE Src_Doc_Id = ?";
		this.executeForDocId(sSql);
		System.out.println("normalizeQuestionDependencies() #3");
		
		sSql = "update Stg2_Question\n" +
				"set root_question_id = Stg2_Question_Id\n" +
				"where Src_Doc_Id = ?";
		this.executeForDocId(sSql);
		System.out.println("normalizeQuestionDependencies() #4");
		
		sSql = "update Stg2_Question\n" +
				"set root_question_id = latest_parent_question_id\n" +
				"where Src_Doc_Id = ? and latest_parent_question_id is not null";
		this.executeForDocId(sSql);
		System.out.println("normalizeQuestionDependencies() #5");
		
		sSql = "update Stg2_Question\n" +
				"set root_question_id = ( \n" +
				    "select sub.latest_parent_question_id\n" +
				    "from (\n" + 
				        "select * from Stg2_Question\n" + 
				    ") sub\n" +
				    "where sub.Stg2_Question_Id = Stg2_Question.root_question_id\n" + 
				")\n" +
				"where Src_Doc_Id = ?\n" +
				"and exists (\n" + 
				    "select sub.latest_parent_question_id\n" +
				    "from (\n" + 
				        "select * from Stg2_Question where latest_parent_question_id is not null\n" +
				    ") sub\n" +
				    "where sub.Stg2_Question_Id = Stg2_Question.root_question_id\n" + 
				")";
		this.executeForDocId(sSql);
		System.out.println("normalizeQuestionDependencies() #6 Layer 1");
		
		sSql = "update Stg2_Question\n" +
			"set root_question_id = ( \n" +
			    "select sub.latest_parent_question_id\n" +
			    "from (\n" + 
			        "select * from Stg2_Question\n" + 
			    ") sub\n" +
			    "where sub.Stg2_Question_Id = Stg2_Question.root_question_id\n" + 
			")\n" +
			"where Src_Doc_Id = ?\n" +
			"and exists (\n" + 
			    "select sub.latest_parent_question_id\n" +
			    "from (\n" + 
			        "select * from Stg2_Question where latest_parent_question_id is not null\n" +
			    ") sub\n" +
			    "where sub.Stg2_Question_Id = Stg2_Question.root_question_id\n" + 
			")";
		this.executeForDocId(sSql);
		System.out.println("normalizeQuestionDependencies() #7 Layer 2");
		
		sSql = "update Stg2_Question\n" +
			"set root_question_id = (\n" + 
			    "select sub.latest_parent_question_id\n" +
			    "from ( \n" +
			        "select * from Stg2_Question\n" + 
			    ") sub\n" +
			    "where sub.Stg2_Question_Id = Stg2_Question.root_question_id\n" + 
			")\n" +
			"where Src_Doc_Id = ?\n" +
			"and exists ( \n" +
			    "select sub.latest_parent_question_id\n" +
			    "from (\n" + 
			        "select * from Stg2_Question where latest_parent_question_id is not null\n" +
			    ") sub\n" +
			    "where sub.Stg2_Question_Id = Stg2_Question.root_question_id\n" + 
			")";
		
		this.executeForDocId(sSql);
		System.out.println("normalizeQuestionDependencies() #8 Layer 3");
		
		sSql = "update Stg2_Question q\n" +
			"join Stg2_Question parent\n" +
				"on q.root_question_id = parent.Stg2_Question_Id\n" +
			"set q.question_type = parent.question_type\n" +
			"where q.Src_Doc_Id = ?\n" +
			"and q.category = 'Waterfall' and q.question_type is null";
		this.executeForDocId(sSql);
		System.out.println("normalizeQuestionDependencies() #9 For child waterfall questions, take root baseline question type as default");
	}
	*/
}
