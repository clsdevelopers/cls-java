package hgs.cpm.question;

import gov.dod.cls.utils.sql.SqlUtil;
import hgs.cpm.utils.SqlCommons;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public final class NormalizeQuestionDependencies {

	public static void normalize(Connection conn, Integer srcDocId) throws SQLException {
		String sSql = "DELETE FROM Stg2_Question_Dependency\n"
				+ "WHERE Stg2_Question_Id IN (\n"
				+ "SELECT Stg2_Question_Id FROM Stg2_Question WHERE Src_Doc_Id = ?\n"
				+ ")";
		SqlCommons.executeForDocId(conn, sSql, srcDocId);
		System.out.println("NormalizeQuestionDependencies.normalize(): DELETE FROM Stg2_Question_Dependency");
		
		int iCount = 0;
		PreparedStatement ps1 = null;
		PreparedStatement psInsert = null;
		ResultSet rs1 = null;
		try {
			sSql = "INSERT INTO Stg2_Question_Dependency (Stg2_Question_Id, referenced_question_name) VALUES (?, ?)";
			psInsert = conn.prepareStatement(sSql);
			
			sSql = "SELECT Stg2_Question_Id, parsed_rule\n" +
					"FROM Stg2_Question\n" +
					"WHERE Src_Doc_Id = ?\n" +
					"AND parsed_rule IS NOT NULL\n" +
					"AND parsed_rule LIKE '%[%]%'";
			ps1 = conn.prepareStatement(sSql);
			ps1.setInt(1, srcDocId);
			rs1 = ps1.executeQuery();
			while (rs1.next()) {
				String v_dependencies = rs1.getString("parsed_rule");
				int iStart = v_dependencies.indexOf('[');
				if (iStart >= 0) {
					int iEnd = v_dependencies.indexOf(']');
					if (iEnd > iStart) {
						String slice = v_dependencies.substring(iStart, iEnd + 1);
						if (slice.length() > 0) {
							Integer v_id = rs1.getInt("Stg2_Question_Id");
							psInsert.setInt(1, v_id);
							psInsert.setString(2, slice);
							psInsert.execute();
							iCount++;
						}
					}
				}
			}
		}
		finally {
			SqlUtil.closeInstance(psInsert);
			SqlUtil.closeInstance(rs1, ps1, null);
		}
		System.out.println("NormalizeQuestionDependencies.normalize(): " + iCount);
	}
}
