package hgs.cpm.question;

import gov.dod.cls.utils.CommonUtils;
import gov.dod.cls.utils.sql.SqlUtil;
import hgs.cpm.db.StgSrcDocumentRecord;
import hgs.cpm.question.util.Stg1QuestionSequenceRecord;

import java.io.File;
import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Iterator;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class QuestionSequenceExcel {

	private String questionSequenceExcelFileName;
	private Connection conn;
	private Integer srcDocId = null;
	private PreparedStatement psInsert = null;
	
	public Integer getSrcDocId() { return srcDocId; }
	public void setSrcDocId(Integer srcDocId) { this.srcDocId = srcDocId; }

	public Connection getConn() { return conn; }
	public void setConn(Connection conn) { this.conn = conn; }

	public QuestionSequenceExcel(String questionSequenceExcelFileName, Connection conn) {
		this.questionSequenceExcelFileName = questionSequenceExcelFileName;
		this.conn = conn;
	}
	
	public Integer generateDocId() throws SQLException {
		if (this.srcDocId == null) {
			File f = new File(this.questionSequenceExcelFileName);
			String fileName = f.getName();
			this.srcDocId = StgSrcDocumentRecord.generateDocId(this.conn, fileName, StgSrcDocumentRecord.TYPE_CD_QUESTION_SEQUENCE);
		}
		return this.srcDocId;
	}
	
	public void run() throws Exception {
		this.read();
	}
	
	public void read() throws Exception {
		System.out.println("QuestionSequenceExcel: " + this.questionSequenceExcelFileName);
		this.generateDocId();
		FileInputStream fileInputStream = null;
		XSSFWorkbook workbook = null;
		Row row = null;
		try {
			this.prepareStatement();
			fileInputStream = new FileInputStream(new File(this.questionSequenceExcelFileName));
			workbook = new XSSFWorkbook (fileInputStream);
			XSSFSheet sheet = workbook.getSheetAt(0);
			Iterator<Row> rowIterator = sheet.iterator();
			
			int iRecords = 0;
			while (rowIterator.hasNext()) {
	            row = rowIterator.next();
	            
	            Object cell0 = this.getCellValue(row.getCell(0));
	            Object cell1 = this.getCellValue(row.getCell(1));
	            Object cell3 = this.getCellValue(row.getCell(3)); // Default Baseline

	            // Bev, if rows of empty cells appear at the bottom of the spreadsheet,
	            // the error would occur.
	            String sQuestionName = (cell0 != null) ? cell0.toString() : "";
	            if (CommonUtils.isNotEmpty(sQuestionName) && (sQuestionName.charAt(0) == '[')) {
		            Integer iSequence = ((Double)cell1).intValue();
		            String sDefaultBaseline = (cell3 == null) ? "" : cell3.toString();
		            if (CommonUtils.isEmpty(sDefaultBaseline))
		            	sDefaultBaseline = null;
		            else if (sDefaultBaseline.toUpperCase().equals("YES"))
		            	sDefaultBaseline = "1";
		            else if (sDefaultBaseline.toUpperCase().equals("NO"))
		            	sDefaultBaseline = "0";
		            
		            this.psInsert.setInt(1, this.srcDocId);
		            this.psInsert.setString(2, sQuestionName);
		            this.psInsert.setInt(3, iSequence);
		            SqlUtil.setParam(this.psInsert, 4, sDefaultBaseline, java.sql.Types.VARCHAR); // CJ-584
		            this.psInsert.execute();
		            iRecords++;
	            }
	        }
			System.out.println("End of QuestionSequenceExcel. Records: " + iRecords);
		}
		catch (Exception oError) {
			String sMessage = "QuestionSequenceExcel.run() Error: " + oError.getMessage();
			if (row != null)
				sMessage += "\nat Row(" + row.getRowNum() + ")";
			System.out.println(sMessage);
			throw oError;
		}
		finally {
			if (workbook != null)
				workbook.close();

			if (fileInputStream != null)
				try {
					fileInputStream.close();
				} catch (Exception oIgnore) {
					oIgnore.printStackTrace();
				}
			
			this.closeStatement();
		}
	}
	
	private Object getCellValue(Cell cell) {
		Object result = null;

		if (cell != null) {
	        switch (cell.getCellType()) {
		        case Cell.CELL_TYPE_BOOLEAN:
		        	result = cell.getBooleanCellValue();
		            break;
		        case Cell.CELL_TYPE_NUMERIC:
		        	result = cell.getNumericCellValue();
		            break;
		        case Cell.CELL_TYPE_STRING:
		        	result = cell.getStringCellValue();
		            break;
		        case Cell.CELL_TYPE_FORMULA:
		        	result = cell.getNumericCellValue();
		        	System.out.println("[" + cell.getRowIndex() + "," + cell.getColumnIndex() + "]: Unprocessed type of " + cell.getCellType());
		        	break;
		        case Cell.CELL_TYPE_BLANK:
		        	result = "";
		            break;
		        default:
		        	System.out.println("[" + cell.getRowIndex() + "," + cell.getColumnIndex() + "]: Unprocessed type of " + cell.getCellType());
		        	break;
		    }
		}
		return result;
	}
	
	
	private void prepareStatement() throws SQLException {
		String sSql
			= "INSERT INTO " + Stg1QuestionSequenceRecord.TABLE_STG1_QUESTOIN_SEQUENCE
			+ " (" + Stg1QuestionSequenceRecord.FIELD_SRC_DOC_ID
			+ ", " + Stg1QuestionSequenceRecord.FIELD_QUESTION_NAME
			+ ", " + Stg1QuestionSequenceRecord.FIELD_SEQUENCE
			+ ", " + Stg1QuestionSequenceRecord.FIELD_DEFAULT_BASELINE // CJ-584
			+ ")"
			+ " VALUES (?, ?, ?, ?)";
		this.psInsert = this.conn.prepareStatement(sSql);
	}
	
	private void closeStatement() {
		SqlUtil.closeInstance(this.psInsert);
		this.psInsert = null;
	}
	
}
