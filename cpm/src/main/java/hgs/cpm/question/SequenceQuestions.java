package hgs.cpm.question;

import hgs.cpm.utils.SqlCommons;

import java.sql.Connection;
import java.sql.SQLException;

public class SequenceQuestions {
	
	public static void run(Connection conn, Integer srcDocId) throws SQLException {
		String sSql;
		
		sSql = "update Stg2_Question set sequence = null where Src_Doc_Id = ?";
		SqlCommons.executeForDocId(conn, sSql, srcDocId);
		System.out.println("SequenceQuestions.run() #1 clear sequence");
		
		sSql = "update Stg2_Question\n" +
			"set sequence = (\n" +
			    "select count(*)\n" +
				"from (\n" +
					"select Stg1_Question_Id, Stg2_Question_Id, g.Question_Group\n" +
					"from Stg2_Question q\n" +
			        "join Question_Group_Ref g\n" +
						"on q.group_name = g.Question_Group_Name\n" +
				") as sub\n" +
				"where sub.Stg1_Question_Id <= Stg2_Question.Stg1_Question_Id\n" +
			")\n" +
			"where Src_Doc_Id = ? and category = 'Baseline'";
		SqlCommons.executeForDocId(conn, sSql, srcDocId);
		System.out.println("SequenceQuestions.run() #2 Baseline");
		
		sSql = "update Stg2_Question\n" +
			"set sequence = (\n" +
			    "select count(*)\n" +
				"from (\n" +
					"select Stg1_Question_Id, Stg2_Question_Id, Question_Group\n" +
					"from Stg2_Question q\n" +
			        "join Question_Group_Ref g\n" +
						"on q.group_name = g.Question_Group_Name\n" +
				") as sub\n" +
				"where\n" + 
					"( select Question_Group from Question_Group_Ref g where Stg2_Question.group_name = g.Question_Group_Name )\n" + 
			        "> sub.Question_Group\n" +
			") + (\n" +
			    "select count(*)\n" +
				"from (\n" +
					"select sequence, Stg1_Question_Id, Stg2_Question_Id, group_name\n" +
					"from Stg2_Question q\n" +
				") as sub\n" +
				"where sub.sequence <= Stg2_Question.sequence\n" +
					"and Stg2_Question.group_name = sub.group_name\n" +
			")\n" +
			"where Src_Doc_Id = ? and category = 'Baseline'";
		SqlCommons.executeForDocId(conn, sSql, srcDocId);
		System.out.println("SequenceQuestions.run() #3 Baseline");
		
		sSql = "update Stg2_Question set sequence = sequence * 100 where Src_Doc_Id = ?";
		SqlCommons.executeForDocId(conn, sSql, srcDocId);
		System.out.println("SequenceQuestions.run() #4");
		
		sSql = "update Stg2_Question\n" +
			"set sequence = (\n" +
			    "select parent.sequence\n" +
			    "from (\n" +
			        "select *\n" +
			        "from Stg2_Question\n" +
			    ") parent\n" +
				"where Stg2_Question.root_question_id = parent.Stg2_Question_Id\n" +
			") + (\n" +
			    "select count(*)\n" +
			    "from (\n" +
			        "select *\n" +
			        "from Stg2_Question\n" +
			        "where category = 'Waterfall'\n" +
			    ") sub\n" +
				"where root_question_id = Stg2_Question.root_question_id\n" + 
					"and Stg1_Question_Id <= Stg2_Question.Stg1_Question_Id\n" +
			")\n" +
			"where Src_Doc_Id = ? and category = 'Waterfall'";
		SqlCommons.executeForDocId(conn, sSql, srcDocId);
		System.out.println("SequenceQuestions.run() #5 Waterfall");
		
		sSql = "update Stg2_Question\n" +
			"set sequence = (\n" +
			    "select max(sequence)\n" +
			    "from (\n" +
			        "select *\n" +
			        "from Stg2_Question\n" +
			        "where sequence is not null\n" +
			    ") sibling\n" +
				"where Stg2_Question.root_question_id = sibling.root_question_id\n" +
					"and Stg2_Question.group_name = sibling.group_name\n" +
			") + (\n" +
			    "select count(*)\n" +
			    "from (\n" +
			        "select *\n" +
			        "from Stg2_Question\n" +
			        "where category = 'Follow-on'\n" +
			    ") sub\n" +
				"where root_question_id = Stg2_Question.root_question_id\n" + 
					"and Stg1_Question_Id <= Stg2_Question.Stg1_Question_Id\n" +
			 ")\n" +
			"where Src_Doc_Id = ? and category = 'Follow-on'";
		SqlCommons.executeForDocId(conn, sSql, srcDocId);
		System.out.println("SequenceQuestions.run() #6 Follow-on");
	}

}
