package hgs.cpm.question;

import gov.dod.cls.utils.sql.SqlUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public final class NormalizeString {

	public static void normalize(Connection conn, String table_name, String column_name, String string_to_normalize) 
			throws SQLException 
	{
		String sSql = "update " + table_name + 
		        " set " + column_name +
		        " = replace(" + column_name + ",'" + string_to_normalize +
		        string_to_normalize + "', '" + string_to_normalize + "')";
		PreparedStatement psUpdate = null;
		try {
			psUpdate = conn.prepareStatement(sSql);
			psUpdate.execute();
		} finally {
			SqlUtil.closeInstance(psUpdate);
		}
	}
	
}
