package hgs.cpm.question.util;

import gov.dod.cls.utils.CommonUtils;
import hgs.cpm.utils.RomanNumeral;
import hgs.cpm.utils.StringCommons;

import java.sql.ResultSet;
import java.sql.SQLException;

public class StgRawQuestionRecord {

	public static final String TABLE_STG1_QUESTION = "Stg1_Question";
	
	public static final String FIELD_STG1_QUESTION_ID = "Stg1_Question_Id";
	public static final String FIELD_SRC_DOC_ID = "Src_Doc_Id";
	public static final String FIELD_SHEET_ID = "Sheet_Id";
	public static final String FIELD_ROW_NUMBER = "Row_Number";
	public static final String FIELD_IS_BASELINE = "Is_Baseline";
	public static final String FIELD_QUESTION = "Question";
	public static final String FIELD_PRESCRIPTION = "Prescription";
	
	public static final String SQL_SELECT
		= "SELECT " + FIELD_STG1_QUESTION_ID
		+ ", " + FIELD_SRC_DOC_ID
		+ ", " + FIELD_SHEET_ID
		+ ", " + FIELD_ROW_NUMBER
		+ ", " + FIELD_IS_BASELINE
		+ ", " + FIELD_QUESTION
		+ ", " + FIELD_PRESCRIPTION
		+ " FROM " + TABLE_STG1_QUESTION
		+ " WHERE " + FIELD_SRC_DOC_ID + " = ?"
		+ " ORDER BY " + FIELD_SHEET_ID + ", " + FIELD_IS_BASELINE + " DESC, " + FIELD_ROW_NUMBER;
	
	// ==============================================================================
	private StgRawQuestionTable questionRawTable;

	private int stg1QuestionId;
	private int srcDocId;
	private int sheetId;
	private int rowNumber;
	private boolean isBaseline;
	private String question;
	private String prescription;
	
	public StgRawQuestionRecord(StgRawQuestionTable questionRawTable) {
		this.questionRawTable = questionRawTable;
	}

	public void read(ResultSet rs) throws SQLException {
		this.stg1QuestionId = rs.getInt(FIELD_STG1_QUESTION_ID);
		this.srcDocId = rs.getInt(FIELD_SRC_DOC_ID);
		this.sheetId = rs.getInt(FIELD_SHEET_ID);
		this.rowNumber = rs.getInt(FIELD_ROW_NUMBER);
		this.isBaseline = rs.getBoolean(FIELD_IS_BASELINE);
		this.question = rs.getString(FIELD_QUESTION);
		this.prescription = rs.getString(FIELD_PRESCRIPTION);
	}

	// ==============================================================================
	public int getStg1QuestionId() { return stg1QuestionId; }
	public void setStg1QuestionId(int stg1QuestionId) { this.stg1QuestionId = stg1QuestionId; }

	public int getSrcDocId() { return srcDocId; }
	public void setSrcDocId(int srcDocId) { this.srcDocId = srcDocId; }

	public int getSheetId() { return sheetId; }
	public void setSheetId(int sheetId) { this.sheetId = sheetId; }

	public int getRowNumber() { return rowNumber; }
	public void setRowNumber(int rowNumber) { this.rowNumber = rowNumber; }

	public boolean isBaseline() { return isBaseline; }
	public void setBaseline(boolean isBaseline) { this.isBaseline = isBaseline; }

	public String getQuestion() { return question; }
	public void setQuestion(String question) { this.question = question; }

	public String getPrescription() { return prescription; }
	public void setPrescription(String prescription) { this.prescription = prescription; }
	
	public String getSheetInfo() {
		return "[" + this.getSheetId() + ": " + (this.getRowNumber() + 1) + "/" + this.isBaseline() + "]";
	}
	
	public StgRawQuestionTable getQuestionRawTable() { return questionRawTable; }
	public void setQuestionRawTable(StgRawQuestionTable questionRawTable) {
		this.questionRawTable = questionRawTable;
	}

	// ------------------------------
	public boolean isEmpty() {
		return (CommonUtils.isEmpty(this.question) || "x".equals(this.question)) &&
				CommonUtils.isEmpty(this.prescription);
	}
	
	private static String trim(String psValue) {
		String result = psValue.replace('\n', ' ');
		while (result.contains("  "))
			result = result.replaceAll("  ", " ");

		result = result.replace(" ]", "]"); // CJ-1125
		return result.trim(); // CJ-1125
	}
	
	private int level = -1;
	private String questionCode = null;
	private Stg2QuestionRecord stg2QuestionRecord = null;
	private String answer = null;
	private String promptAfterSelection = null;

	public int getLevel() { return level; }
	public String getQuestionCode() { return questionCode; }

	public Stg2QuestionRecord getStg2QuestionRecord() { return this.stg2QuestionRecord; }
	public String getAnswer() { return answer; }
	public String getPromptAfterSelection() { return promptAfterSelection; }
	
	public boolean parseCodeAndLevel() {
		if (CommonUtils.isEmpty(this.question))
			return false;
		
		int iPos = this.question.indexOf('[');
		if (iPos > 0) {
			int iEnd = this.question.indexOf(']');
			if (iEnd > iPos) {
				this.questionCode = trim(this.question.substring(iPos, iEnd + 1)); // .replace('\n', ' ');
				//while (this.questionCode.contains("  "))
				//	this.questionCode = this.questionCode.replaceAll("  ", " ");
				return true;
			}
		}
		else if (this.level > 0) {
			this.questionCode = "[" + this.answer.trim() + "]"; // CJ-1125
		}
		return false;
	}
	
	public boolean parseQuestionCode() {
		if (CommonUtils.isEmpty(this.question))
			return false;
		
		int iPos = this.question.indexOf('[');
		if (iPos > 0) {
			int iEnd = this.question.indexOf(']');
			if (iEnd > iPos) {
				if (this.question.contains("USER TO FILL IN ")) {
					return false;
				} else {
					this.questionCode = trim(this.question.substring(iPos, iEnd + 1)); // .replace('\n', ' ');
					//while (this.questionCode.contains("  "))
					//	this.questionCode = this.questionCode.replaceAll("  ", " ");
					return true;
				}
			}
		}
		//else if (this.level > 0) {
		//	this.questionCode = "[" + this.answer + "]";
		//	return true;
		//}
		return false;
	}
	
	public int parseLevel() {
		if (!CommonUtils.isEmpty(this.question)) {
			int iPos = this.question.indexOf('\n');
			String sFirstLine = (iPos > 0) ? this.question.substring(0, iPos).trim() : this.question;
			if ("h GENERAL SERVICES ADMINISTRATION".equals(sFirstLine)) {
				this.level = 2;
				this.answer = "GENERAL SERVICES ADMINISTRATION";
				return this.level;
			}
			if (this.level < 0) {
				if (sFirstLine.startsWith("F.O.B. ") || sFirstLine.startsWith("U.S. ")) {
					this.level = -1;
				} else {
					iPos = sFirstLine.indexOf('.'); // 11. 2 -STEP SEALED BIDDING
					int iSpace = sFirstLine.indexOf(' '); // check space character
					if (iPos == 1) { // ((sFirstLine.length() > 1) && (sFirstLine.charAt(1) == '.')) {
						if (iSpace == (iPos + 1)) {
							char c = sFirstLine.charAt(0);
							if (Character.isDigit(c))
								this.level = 1;
							else if (((c >= 'a') && (c <= 'z')) || ((c >= 'A') && (c <= 'Z')))
								this.level = 2;
							else
								this.level = -1;
						} else {
							this.level = -1;
						}
					} else if (iPos == 2) {
						if (iSpace == (iPos + 1)) {
							char c1 = sFirstLine.charAt(0);
							char c2 = sFirstLine.charAt(1);
							if (Character.isDigit(c1) && Character.isDigit(c2))
								this.level = 1;
							else if ((((c1 >= 'a') && (c1 <= 'z')) || ((c1 >= 'A') && (c1 <= 'Z')))
									&& (((c2 >= 'a') && (c2 <= 'z')) || ((c2 >= 'A') && (c2 <= 'Z'))))
								this.level = 2;
							else
								this.level = -1;
						} else {
							this.level = -1;
						}
					} else {
						if (sFirstLine.charAt(0) == '(') {
							iPos = sFirstLine.indexOf(')');
							if (iPos > 1) {
								String sValue = sFirstLine.substring(1, iPos);
								char c1 = sValue.charAt(0);
								if ((sValue.length() == 1) && (c1 >= 'A') && (c1 <= 'Z'))
									this.level = 4;
								else if (RomanNumeral.isRomanNumeral(sValue))
									this.level = 3;
								else if (sValue.length() == 1)
									this.level = 4;
								else
									this.level = -1;
							} else
								this.level = -1;
						}
						else if (this.parseQuestionCode())
							this.level = 0;
						else
							this.level = -1;
					}
				}
			}
			if ((this.answer == null) && (this.level != 0)) {
				if (this.level < 0) {
					this.answer = this.question.replaceAll("\n", " ").replaceAll("  ", " ");
				} else {
					iPos = sFirstLine.indexOf(' ');
					if (iPos > 0)
						this.answer = sFirstLine.substring(iPos).trim();
					else
						this.answer = sFirstLine;
				}
				iPos = this.answer.indexOf(" - NOTE: ");
				if (iPos < 0)
					iPos = this.answer.indexOf(" NOTE: ");
				if (iPos > 0)
					this.answer = this.answer.substring(0, iPos);
				this.answer = this.answer.trim();
			}
			if (CommonUtils.isNotEmpty(this.answer)) {
				String[] aLines = this.question.split("\n");
				if (aLines.length > 1) {
					for (int item = 0; item < aLines.length; item++) {
						String sLine = aLines[item];
						if (sLine.startsWith("PROMPT AFTER SELECTION:")) {
							this.promptAfterSelection = sLine.substring("PROMPT AFTER SELECTION:".length()).trim();
							break;
						}
					}
				}
			}
		}
		return this.level;
	}
	
	private boolean parseQuestionType(String sTypeText) {
		sTypeText = trim(sTypeText);
		if (sTypeText.contains("SELECT ONE OR MORE") ||
			sTypeText.contains("SELECT ONE OR MANY") ||
			sTypeText.contains("SELECT AT LEAST ") ||
			sTypeText.contains("SELECT ALL") ||
			sTypeText.contains("SELECT NONE OR MANY")) {
			this.stg2QuestionRecord.setQuestionType(Stg2QuestionRecord.QUESTION_TYPE_MULTIPLE_ANSWER);
			return true;
		} else {
			if (sTypeText.contains("FILL IN A ") ||
				sTypeText.contains("FILL IN NUMERICAL ")) {
				this.stg2QuestionRecord.setQuestionType(Stg2QuestionRecord.QUESTION_TYPE_NUMERIC);
				return true;
			} else {
				if (sTypeText.contains("SELECT ONE") ||
					sTypeText.contains("SELECT EITHER ")) {
					this.stg2QuestionRecord.setQuestionType(Stg2QuestionRecord.QUESTION_TYPE_SINGLE_ANSWER);
					return true;
				} else {
					return false; // System.out.println(this.getSheetInfo() + " Unknown Type Pattern: " + sTypeText);
				}
			}
		}
	}
	
	public char calcGroupCode() {
		char a = 'A';
		if (this.sheetId > 0) {
			byte b = (byte)a;
			for (int i = 0; i < this.sheetId; i++)
				b++;
			a = (char)b;
		}
		return a;
	}
	
	public void populate(Stg2QuestionRecord stg2QuestionRecord, boolean bSetAnswerQuestionCode) {
		this.stg2QuestionRecord = stg2QuestionRecord;
		this.stg2QuestionRecord.setStgRawQuestionRecord(this);
		stg2QuestionRecord.setStg1QuestionId(this.stg1QuestionId);

		if (bSetAnswerQuestionCode)
			this.questionCode = "[" + this.answer.trim() + "]"; // CJ-1125
		else {
			this.parseLevel();
		}

		if (this.questionCode.endsWith(" ]")) 
			this.questionCode = this.questionCode + "";

		stg2QuestionRecord.setQuestionName(this.questionCode);
		//if ("[FULL AND OPEN EXCEPTION]".equals(this.questionCode))
		//	System.out.println(this.questionCode + " " + this.question);
		
		stg2QuestionRecord.setParseLevel(this.level);
		stg2QuestionRecord.setIsBaseline(this.isBaseline);
		stg2QuestionRecord.setGroupName(this.calcGroupCode() + "");
		if (this.isBaseline) {
			if (this.level > 0)
				stg2QuestionRecord.setCategory("Waterfall");
			else
				stg2QuestionRecord.setCategory("Baseline");
		} else {
			stg2QuestionRecord.setCategory("Follow-on");
		}
		
		int iPos = this.question.indexOf("USER ");
		if (iPos >= 0) {
			String sTypeText = this.question.substring(iPos + 5);
			if (! this.parseQuestionType(sTypeText)) {
				iPos = sTypeText.indexOf("USER ");
				if (iPos > 0) {
					sTypeText = sTypeText.substring(iPos + 5);
					if (! this.parseQuestionType(sTypeText)) {
						System.out.println(this.getSheetInfo() + " Unknown Type Pattern: " + sTypeText);
					}
				}
			}
		}
		
		String sQuestionText = null;
		String sRule = null, sSpecialRule = null;
		String[] aLines = StringCommons.splitTrim(this.question, "\n");
		String sFirstLine = aLines[0];
		//iPos = this.question.indexOf('\n');
		
		//if ("[FULL AND OPEN EXCEPTION]".equals(this.questionCode))
		//	System.out.println(this.question);
		
		if (sFirstLine.startsWith("ASK ") || sFirstLine.startsWith("(ASK ") ||
			sFirstLine.startsWith("DO NOT ASK ") || sFirstLine.startsWith("ONLY ASK ")) {
			sRule = sFirstLine;
			// System.out.println("RULE: " + sRule);
			sQuestionText = aLines[1]; 
		} else {
			sQuestionText = sFirstLine;
			if (this.level > 0) {
				iPos = sQuestionText.indexOf(" ");
				if (iPos > 0) {
					sQuestionText = sQuestionText.substring(iPos);
					sQuestionText = "Select " + sQuestionText.trim().toLowerCase() + " subtype(s):";
				}
			}
			for (iPos = 1; iPos < aLines.length; iPos++) {
				String sLine = aLines[iPos];
				if (sLine.startsWith("IF ") && sLine.contains("TOGETHER")) {
					sSpecialRule = sLine.replaceAll("\"\"", "\"").trim();
					String sTest = "IF \"SET-ASIDE\" IS SELECTED, THE USER CAN ";
					if (sSpecialRule.startsWith(sTest))
						sSpecialRule = sSpecialRule.substring(sTest.length());
					if ((sSpecialRule != null) && sSpecialRule.endsWith("TOGETHER\""))
						sSpecialRule = sSpecialRule.substring(0, sSpecialRule.length() - 1);
					break;
				}
			}
		}
		iPos = sQuestionText.indexOf(". ");
		if (iPos == 1)
			sQuestionText = sQuestionText.substring(3);
		//System.out.println(sQuestionText);
		stg2QuestionRecord.setQuestionText((sQuestionText == null) ? null : sQuestionText.trim());

		stg2QuestionRecord.assignRule(sRule);
		if (sSpecialRule != null) {
			stg2QuestionRecord.setRule(sSpecialRule);
			System.out.println(">>> " + this.questionCode + " Special Rule: " + sSpecialRule);
		}
	}
	
}
