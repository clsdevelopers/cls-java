package hgs.cpm.question.util;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;

public class Stg2PossibleAnswerPrescriptionRecord {
	
	public static final String TABLE_STG2_POSSIBLE_ANSWERS_PRESCRIPTION = "Stg2_Possible_Answers_Prescription";
	
	public static final String FIELD_STG2_POSSIBLE_ANSWER_ID = "Stg2_Possible_Answer_Id";
	public static final String FIELD_PRESCRIPTION         = "Prescription";
	
	public static final String SQL_INSERT
		= "INSERT INTO " + TABLE_STG2_POSSIBLE_ANSWERS_PRESCRIPTION
		+ " (" + FIELD_STG2_POSSIBLE_ANSWER_ID
		+ ", " + FIELD_PRESCRIPTION
		+ ") VALUES (?, ?)";
	
	public static PreparedStatement createPrepareStatement(Connection conn) throws SQLException {
		return conn.prepareStatement(Stg2PossibleAnswerPrescriptionRecord.SQL_INSERT, Statement.RETURN_GENERATED_KEYS);
	}

	private Integer stg2PossibleAnswerId;
	private String prescription;

	
	// ===============================================================================
	public Integer getStg2PossibleAnswerId() {
		return stg2PossibleAnswerId;
	}
	public void setStg2PossibleAnswerId(Integer stg2PossibleAnswerId) {
		this.stg2PossibleAnswerId = stg2PossibleAnswerId;
	}
	public String getPrescription() {
		return prescription;
	}
	public void setPrescription(String prescription) {
		this.prescription = prescription;
	}

}
