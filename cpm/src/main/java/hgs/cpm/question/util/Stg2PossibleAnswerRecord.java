package hgs.cpm.question.util;

import gov.dod.cls.utils.CommonUtils;
import gov.dod.cls.utils.sql.SqlUtil;
import hgs.cpm.utils.StringCommons;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;

public class Stg2PossibleAnswerRecord {
	
	public static final String TABLE_STG2_POSSIBLE_ANSWER = "Stg2_Possible_Answer";
	
	public static final String FIELD_STG2_POSSIBLE_ANSWER_ID = "Stg2_Possible_Answer_Id";
	public static final String FIELD_STG1_QUESTION_ID     = "Stg1_Question_Id";
	public static final String FIELD_STG2_QUESTION_ID     = "Stg2_Question_Id";
	public static final String FIELD_CHOICE_TEXT          = "Choice_Text";
	public static final String FIELD_CHOICE_TEXT2         = "Choice_Text2";
	public static final String FIELD_SOUNDEX_HASH         = "Soundex_Hash";
	public static final String FIELD_SRC_DOC_ID           = "Src_Doc_Id";
	public static final String FIELD_PROMPT_AFTER_SELECTION = "Prompt_After_Selection";

	public static final String SQL_INSERT
		= "INSERT INTO " + TABLE_STG2_POSSIBLE_ANSWER
		+ " (" + FIELD_STG1_QUESTION_ID
		+ ", " + FIELD_STG2_QUESTION_ID
		+ ", " + FIELD_CHOICE_TEXT
		+ ", " + FIELD_CHOICE_TEXT2
		+ ", " + FIELD_SOUNDEX_HASH
		+ ", " + FIELD_SRC_DOC_ID
		+ "," + FIELD_PROMPT_AFTER_SELECTION
		+ ") VALUES "
		+ "( ?" // 1, stg1QuestionId, java.sql.Types.INTEGER
		+ ", ?" // 2, stg2QuestionId, java.sql.Types.INTEGER
		+ ", ?" // 3, choiceText, java.sql.Types.VARCHAR
		+ ", ?" // 4, choiceText2, java.sql.Types.VARCHAR
		+ ", ?" // 5, soundexHash, java.sql.Types.VARCHAR
		+ ", ?" // 6, srcDocId, java.sql.Types.INTEGER
		+ ", ?" // 7, FIELD_PROMPT_AFTER_SELECTION, java.sql.Types.VARCHAR
		+ ")";
	
	
	private Integer stg2PossibleAnswerId = null;
	private Integer stg1QuestionId = null;
	private Integer stg2QuestionId = null;
	private String choiceText = null;
	private String choiceText2 = null;
	private String soundexHash = null;
	private Integer srcDocId = null;
	private String promptAfterSelection = null;

	private ArrayList<String> prescriptions = new ArrayList<String>();
	
	public void assignPrescription(String psPrescriptions) {
		if (CommonUtils.isEmpty(psPrescriptions))
			return;
		StringCommons.parsePrescriptions(psPrescriptions, this.prescriptions);
	}
	
	public void setPreparedStatement(PreparedStatement ps) throws SQLException {
		SqlUtil.setParam(ps, 1, stg1QuestionId, java.sql.Types.INTEGER);
		SqlUtil.setParam(ps, 2, stg2QuestionId, java.sql.Types.INTEGER);
		SqlUtil.setParam(ps, 3, choiceText, java.sql.Types.VARCHAR);
		SqlUtil.setParam(ps, 4, choiceText2, java.sql.Types.VARCHAR);
		SqlUtil.setParam(ps, 5, soundexHash, java.sql.Types.VARCHAR);
		SqlUtil.setParam(ps, 6, srcDocId, java.sql.Types.INTEGER);
		SqlUtil.setParam(ps, 7, promptAfterSelection, java.sql.Types.VARCHAR);
	}
	
	public void savePrescriptions(PreparedStatement ps) throws SQLException {
		ArrayList<String> aSaved = new ArrayList<String>();
		String sDuplicats = "";
		for (String sPrescription : this.prescriptions) {
			if (aSaved.contains(sPrescription)) {
				sDuplicats += (sDuplicats.isEmpty() ? "" : ", ") + sPrescription; 
			} else {
				SqlUtil.setParam(ps, 1, this.stg2PossibleAnswerId, java.sql.Types.INTEGER);
				SqlUtil.setParam(ps, 2, sPrescription, java.sql.Types.VARCHAR);
				ps.execute();
				aSaved.add(sPrescription);
			}
		}
		if (!sDuplicats.isEmpty())
			System.out.println("  - (" + this.choiceText + ") Duplicated Prescription: " + sDuplicats);
	}
	
	public String debug() {
		String result = choiceText;
		if (prescriptions.size() > 0) {
			String sPrescriptions = "";
			for (String sItem : this.prescriptions) {
				sPrescriptions += (sPrescriptions.isEmpty() ? "" : ", ") + sItem;
			}
//			result += " [" + sPrescriptions + "]";
		}
		return result;
	}
	
	// ==============================================================
	public Integer getStg2PossibleAnswerId() {
		return stg2PossibleAnswerId;
	}
	public void setStg2PossibleAnswerId(Integer stg2PossibleAnswerId) {
		this.stg2PossibleAnswerId = stg2PossibleAnswerId;
	}
	
	public Integer getStg1QuestionId() {
		return stg1QuestionId;
	}
	public void setStg1QuestionId(Integer stg1QuestionId) {
		this.stg1QuestionId = stg1QuestionId;
	}
	
	public Integer getStg2QuestionId() {
		return stg2QuestionId;
	}
	public void setStg2QuestionId(Integer stg2QuestionId) {
		this.stg2QuestionId = stg2QuestionId;
	}
	
	public String getChoiceText() {
		return choiceText;
	}
	public void setChoiceText(String choiceText) {
		this.choiceText = choiceText;
	}
	
	public String getChoiceText2() {
		return choiceText2;
	}
	public void setChoiceText2(String choiceText2) {
		this.choiceText2 = choiceText2;
	}

	public String getPromptAfterSelection() {
		return promptAfterSelection;
	}
	public void setPromptAfterSelection(String promptAfterSelection) {
		this.promptAfterSelection = promptAfterSelection;
	}

	public String getSoundexHash() {
		return soundexHash;
	}
	public void setSoundexHash(String soundexHash) {
		this.soundexHash = soundexHash;
	}
	
	public Integer getSrcDocId() {
		return srcDocId;
	}
	public void setSrcDocId(Integer srcDocId) {
		this.srcDocId = srcDocId;
	}
	
	public ArrayList<String> getPrescriptions() {
		return prescriptions;
	}
	
}
