package hgs.cpm.question.util;

import gov.dod.cls.utils.sql.SqlUtil;
import hgs.cpm.config.CpmConfig;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class Stg1QuestionSequenceSet extends ArrayList<Stg1QuestionSequenceRecord> {

	private static final long serialVersionUID = 5411520003119764726L;
	
	private Stg1QuestionSequenceSet foundSet = null;

	public void load(Connection conn, Integer srcDocId) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = conn.prepareStatement(Stg1QuestionSequenceRecord.SQL_SELECT);
			ps.setInt(1, srcDocId);
			rs = ps.executeQuery();
			this.clear();
			while (rs.next()) {
				Stg1QuestionSequenceRecord oRecord = new Stg1QuestionSequenceRecord();
				oRecord.load(rs);
				this.add(oRecord);
			}
		}
		finally {
			SqlUtil.closeInstance(rs, ps);
		}
	}
	
	public Stg1QuestionSequenceRecord findByName(String sQuestionName) {
		if (this.foundSet == null)
			this.foundSet = new Stg1QuestionSequenceSet();
		for (Stg1QuestionSequenceRecord oRecord : this) {
			if (oRecord.getQuestionName().equals(sQuestionName)) {
				this.foundSet.add(oRecord);
				this.remove(oRecord);
				return oRecord;
			}
		}
		return null;
	}

	// ===================================================================
	public static void main( String[] args )
    {
		CpmConfig cpmConfig = CpmConfig.getInstance();
    	Connection conn = null;
    	try {
    		conn = cpmConfig.getDbConnection();
    		
    		Stg1QuestionSequenceSet stg1QuestionSequenceSet = new Stg1QuestionSequenceSet();
    		stg1QuestionSequenceSet.load(conn, 48);
    		System.out.println("Count: " + stg1QuestionSequenceSet.size());
    	}
    	catch (Exception oError) {
    		oError.printStackTrace();
    	}
    	finally {
    		SqlUtil.closeInstance(conn);
    	}
    }
	
}
