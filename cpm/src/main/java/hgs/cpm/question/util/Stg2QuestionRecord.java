package hgs.cpm.question.util;

import gov.dod.cls.utils.CommonUtils;
import gov.dod.cls.utils.sql.SqlUtil;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;

public class Stg2QuestionRecord {

	public static final String QUESTION_TYPE_MULTIPLE_ANSWER = "Multiple Answer";
	public static final String QUESTION_TYPE_NUMERIC = "Numeric"; // "Numericr";
	public static final String QUESTION_TYPE_SINGLE_ANSWER = "Single Answer";
	public static final String QUESTION_TYPE_BOOLEAN = "Boolean";
	
	public static final String TABLE_STG2_QUESTION = "Stg2_Question";

	public static final String FIELD_STG2_QUESTION_ID = "Stg2_Question_Id";
	public static final String FIELD_STG1_QUESTION_ID     = "Stg1_Question_Id";
	public static final String FIELD_QUESTION_NAME        = "Question_Name";
	public static final String FIELD_QUESTION_TEXT        = "Question_Text";
	public static final String FIELD_QUESTION_TYPE        = "Question_Type";
	public static final String FIELD_PARSED_RULE          = "Parsed_Rule";
	public static final String FIELD_RULE                 = "Rule";
	public static final String FIELD_TYPE                 = "Type";
	public static final String FIELD_GROUP_NAME           = "Group_Name";
	public static final String FIELD_IS_BASELINE          = "Is_Baseline";
	public static final String FIELD_CATEGORY 			 = "Category";
	public static final String FIELD_SEQUENCE             = "Sequence";
	public static final String FIELD_ROOT_QUESTION_ID     = "Root_Question_Id";
	public static final String FIELD_NORMAL_RULE          = "Normal_Rule";
	public static final String FIELD_LATEST_PARENT_QUESTION_ID = "Latest_Parent_Question_Id";
	public static final String FIELD_SRC_DOC_ID           = "Src_Doc_Id";
	public static final String FIELD_PARSE_LEVEL          = "Parse_Level";
	public static final String FIELD_PARENT_STG2_QUESTION_ID = "Parent_Stg2_Question_Id";
	public static final String FIELD_DEFAULT_BASELINE      = "Default_Baseline"; // CJ-584

	public static final String SQL_INSERT
		= "INSERT INTO " + TABLE_STG2_QUESTION
		+ "(" + FIELD_STG1_QUESTION_ID
		+ "," + FIELD_QUESTION_NAME
		+ "," + FIELD_QUESTION_TEXT
		+ "," + FIELD_QUESTION_TYPE
		+ "," + FIELD_PARSED_RULE
		+ "," + FIELD_RULE
		+ "," + FIELD_TYPE
		+ "," + FIELD_GROUP_NAME
		+ "," + FIELD_IS_BASELINE
		+ "," + FIELD_CATEGORY
		+ "," + FIELD_SEQUENCE
		+ "," + FIELD_ROOT_QUESTION_ID
		+ "," + FIELD_NORMAL_RULE
		+ "," + FIELD_LATEST_PARENT_QUESTION_ID
		+ "," + FIELD_SRC_DOC_ID
		+ "," + FIELD_PARSE_LEVEL
		+ "," + FIELD_PARENT_STG2_QUESTION_ID
		+ "," + FIELD_DEFAULT_BASELINE
		+ ") VALUES "
		+ "(?" // 1, stg1QuestionId, java.sql.Types.INTEGER
		+ ",?" // 2, questionName, java.sql.Types.VARCHAR
		+ ",?" // 3, questionText, java.sql.Types.VARCHAR
		+ ",?" // 4, questionType, java.sql.Types.VARCHAR
		+ ",?" // 5, parsedRule, java.sql.Types.VARCHAR
		+ ",?" // 6, rule, java.sql.Types.VARCHAR
		+ ",?" // 7, type, java.sql.Types.VARCHAR
		+ ",?" // 8, groupName, java.sql.Types.VARCHAR
		+ ",?" // 9, isBaseline
		+ ",?" // 10, category, java.sql.Types.VARCHAR
		+ ",?" // 11, sequence, java.sql.Types.INTEGER
		+ ",?" // 12, rootQuestionId, java.sql.Types.INTEGER
		+ ",?" // 13, normalRule, java.sql.Types.VARCHAR
		+ ",?" // 14, latestParentQuestionId, java.sql.Types.INTEGER
		+ ",?" // 15, srcDocId, java.sql.Types.INTEGER
		+ ",?" // 16, parseLevel, java.sql.Types.INTEGER
		+ ",?" // 17, Stg2QuestionRecord parent, java.sql.Types.INTEGER
		+ ",?" // 18, defaultBaseline boolean
		+ ")";

	// =====================================================================
	private Integer stg2QuestionId = null;
	private Integer stg1QuestionId = null;
	private String questionName = null;
	private String questionText = null;
	private String questionType = null;
	private String rule = null;
	private String parsedRule = null;
	private String normalRule = null;
	private String type = null;
	private String groupName = null;
	private Boolean isBaseline = null;
	private String category = null;
	private Integer sequence = null;
	private Integer rootQuestionId = null;
	private Integer latestParentQuestionId = null;
	private Integer srcDocId = null;
	private Integer parseLevel = null;
	
	private boolean defaultBaseline = false; // CJ-584

	private Stg2QuestionRecord parent = null;
	private ArrayList<Stg2QuestionRecord> children = null;
	private Stg2PossibleAnswerSet choices;
	private StgRawQuestionRecord stgRawQuestionRecord = null;
	
	public Stg2QuestionRecord() {
		this.choices = new Stg2PossibleAnswerSet();
		this.choices.setParent(this);
	}
	
	public void setPreparedStatement(PreparedStatement ps) throws SQLException {
		SqlUtil.setParam(ps, 1, stg1QuestionId, java.sql.Types.INTEGER);
		SqlUtil.setParam(ps, 2, questionName, java.sql.Types.VARCHAR);
		SqlUtil.setParam(ps, 3, questionText, java.sql.Types.VARCHAR);
		SqlUtil.setParam(ps, 4, questionType, java.sql.Types.VARCHAR);
		SqlUtil.setParam(ps, 5, parsedRule, java.sql.Types.VARCHAR);
		SqlUtil.setParam(ps, 6, rule, java.sql.Types.VARCHAR);
		SqlUtil.setParam(ps, 7, type, java.sql.Types.VARCHAR);
		SqlUtil.setParam(ps, 8, groupName, java.sql.Types.VARCHAR);
		SqlUtil.setParam(ps, 9, isBaseline, java.sql.Types.BLOB);
		SqlUtil.setParam(ps, 10, category, java.sql.Types.VARCHAR);
		SqlUtil.setParam(ps, 11, sequence, java.sql.Types.INTEGER);
		SqlUtil.setParam(ps, 12, rootQuestionId, java.sql.Types.INTEGER);
		SqlUtil.setParam(ps, 13, normalRule, java.sql.Types.VARCHAR);
		SqlUtil.setParam(ps, 14, latestParentQuestionId, java.sql.Types.INTEGER);
		SqlUtil.setParam(ps, 15, srcDocId, java.sql.Types.INTEGER);
		SqlUtil.setParam(ps, 16, parseLevel, java.sql.Types.INTEGER);
		Integer iParent = null;
		if (this.parent != null)
			iParent = this.parent.stg2QuestionId;
		SqlUtil.setParam(ps, 17, iParent, java.sql.Types.INTEGER);
		ps.setBoolean(18, this.defaultBaseline); // CJ-584
	}
	
	// =====================================================================
	public Integer getStg2QuestionId() {
		return stg2QuestionId;
	}
	public void setStg2QuestionId(Integer stg2QuestionId) {
		this.stg2QuestionId = stg2QuestionId;
	}
	public Integer getStg1QuestionId() {
		return stg1QuestionId;
	}
	public void setStg1QuestionId(Integer stg1QuestionId) {
		this.stg1QuestionId = stg1QuestionId;
	}
	public String getQuestionName() {
		return questionName;
	}
	public void setQuestionName(String questionName) {
		this.questionName = questionName;
	}
	public String getQuestionText() {
		return questionText;
	}
	public void setQuestionText(String questionText) {
		this.questionText = questionText;
	}
	public String getQuestionType() {
		return questionType;
	}
	public void setQuestionType(String questionType) {
		this.questionType = questionType;
	}
	public String getParsedRule() {
		return parsedRule;
	}
	public void setParsedRule(String parsedRule) {
		this.parsedRule = parsedRule;
	}
	public String getRule() {
		return rule;
	}
	public void setRule(String rule) {
		this.rule = rule;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getGroupName() {
		return groupName;
	}
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
	public Boolean getIsBaseline() {
		return isBaseline;
	}
	public void setIsBaseline(Boolean isBaseline) {
		this.isBaseline = isBaseline;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public Integer getSequence() {
		return sequence;
	}
	public void setSequence(Integer sequence) {
		this.sequence = sequence;
	}
	public Integer getRootQuestionId() {
		return rootQuestionId;
	}
	public void setRootQuestionId(Integer rootQuestionId) {
		this.rootQuestionId = rootQuestionId;
	}
	public String getNormalRule() {
		return normalRule;
	}
	public void setNormalRule(String normalRule) {
		this.normalRule = normalRule;
	}
	public Integer getLatestParentQuestionId() {
		return latestParentQuestionId;
	}
	public void setLatestParentQuestionId(Integer latestParentQuestionId) {
		this.latestParentQuestionId = latestParentQuestionId;
	}
	public Integer getSrcDocId() {
		return srcDocId;
	}
	public void setSrcDocId(Integer srcDocId) {
		this.srcDocId = srcDocId;
	}
	public boolean isDefaultBaseline() {
		return defaultBaseline;
	}

	public void setDefaultBaseline(boolean defaultBaseline) {
		this.defaultBaseline = defaultBaseline;
	}

	public Integer getParseLevel() {
		return parseLevel;
	}
	public void setParseLevel(Integer parseLevel) {
		this.parseLevel = parseLevel;
	}

	public Stg2QuestionRecord getParent() {
		return parent;
	}
	public void setParent(Stg2QuestionRecord parent) {
		this.parent = parent;
	}

	public ArrayList<Stg2QuestionRecord> getChildren() {
		return children;
	}
	public void setChildren(ArrayList<Stg2QuestionRecord> children) {
		this.children = children;
	}
	public void addChild(Stg2QuestionRecord stg2QuestionRecord) {
		if (this.children == null)
			this.children = new ArrayList<Stg2QuestionRecord>();
		this.children.add(stg2QuestionRecord);
	}

	public Stg2PossibleAnswerSet getChoices() {
		return choices;
	}
	
	public void addChoice(String psText, String psPromptAfterSelection, String psPrescriptions, int stg1QuestionId) {
		this.choices.addChoice(psText, psPromptAfterSelection, psPrescriptions, stg1QuestionId);
	}

	public StgRawQuestionRecord getStgRawQuestionRecord() {
		return stgRawQuestionRecord;
	}

	public void setStgRawQuestionRecord(StgRawQuestionRecord stgRawQuestionRecord) {
		this.stgRawQuestionRecord = stgRawQuestionRecord;
	}

	// =====================================================================
	public boolean setCustomRule() {
		switch (this.questionName) {
		case "[ORDER VALUE]":
			this.parsedRule = "[AGREEMENT (INCLUDING BASIC AND LOAN)] IS NOT NULL OR \"REQUIREMENTS\" = [TYPE OF INDEFINITE DELIVERY]";
			return true;

		case "[LEASE OPTION]":
			this.parsedRule = "\"LEASE AGREEMENT\" = [AGREEMENT (INCLUDING BASIC AND LOAN)]"; 
			return true;

		/* Removed for CJ-550
		case "[FULL AND OPEN EXCEPTION]":
			this.parsedRule = "[PROCEDURES] IS NOT NULL AND \"FULL AND OPEN COMPETITION (FAR PART 6)\" <> [COMPETITION TYPE]"; 
			return true;
		*/

		case "[OSD APPROVAL FOR DATA SYSTEM]":
			this.parsedRule = "(\"MAJOR DEFENSE PROGRAM\" = [TYPE OF SYSTEM] OR \"MAJOR INFORMATION SYSTEMS PROGRAM\" = [TYPE OF SYSTEM]) AND \"SOLICITATION\" = [DOCUMENT TYPE]"; 
			return true;
		}
		return false;
	}
	
	/*
	private static String changeQuoteForQuestionName(String psCondition, boolean trimLeft) {
		if (trimLeft) {
			int iIndex = psCondition.indexOf('"');
			if (iIndex > 0)
				psCondition = psCondition.substring(iIndex);
		}
		String result = "";
		boolean bFist = true;
		for (int iPos = 0; iPos < psCondition.length(); iPos++) {
			char c = psCondition.charAt(iPos);
			if (c == '"') {
				result += (bFist ? '[' : ']');
				bFist = !bFist;
			} else
				result += c;
		}
		return result;
	}
	*/
	
	private boolean addExpression(ArrayList<String> aLeft, String sOperator, String sRight, boolean bNotOperation) {
		if (aLeft.size() > 0) {
			String sExpression = "";
			if (aLeft.size() > 1) {
				sRight = "[" + sRight + "]";
				sExpression = "";
				for (String sValue : aLeft)
					sExpression += (sExpression.isEmpty() ? "" : " OR ") + "\"" + sValue.trim() + "\" = " + sRight;
				sExpression = "(" + sExpression + ")";
				/*
				String sItems = "";
				for (String sValue : aLeft)
					sItems += (sItems.isEmpty() ? "" : ",") + "\"" + sValue + "\"";
				sExpression = "[" + sRight + "] CONTAINS (" + sItems + ")";
				*/
			} else {
				String sLeft = aLeft.get(0).trim();
				if (sOperator.startsWith(" =  IS")) { // CJ-1184
					String sNewOperator = sOperator.substring(" =  IS".length()).trim();
					int iPos = sNewOperator.lastIndexOf(' ');
					String sTargetOperator = sNewOperator.substring(0, iPos).trim();
					String sTargetValue = sNewOperator.substring(iPos).trim();
					sTargetValue = sTargetValue.replace("$", "").replaceAll(",", "");
					sExpression = "[" + sLeft + "] IS " + (bNotOperation ? "NOT " : "") + sTargetOperator + " " + sTargetValue;
				} else {
					if (bNotOperation)
						if (sOperator.equals(" = "))
							sOperator = " <> ";
					sExpression = "\"" + sLeft + "\"" + sOperator + "[" + sRight + "]";
				}
			}
			aLeft.clear();
			this.parsedRule += sExpression;
			return true;
		} else
			return false;
	}
	
	private static final String DO_NO_ASK_COMMERCIAL = "DO NOT ASK IF THE USER SELECTED \"COMMERCIAL PROCEDURES (FAR PART 12)\" UNDER \"PROCEDURES\"";
	private static final String DO_NO_ASK_COMMERCIAL_2 = "DO NOT ASK IF THE USER SELECTED \"COMMERCIAL PROCEDURES FAR PART (12)\" UNDER \"PROCEDURES\"";
	private static final String AND_DO_NO_ASK_COMMERCIAL = " AND " + DO_NO_ASK_COMMERCIAL;
	private static final String DOT_DO_NO_ASK_COMMERCIAL = ". " + DO_NO_ASK_COMMERCIAL;

	private static final String AND_DO_NO_ASK_COMMERCIAL_2 = " AND " + DO_NO_ASK_COMMERCIAL_2;
	private static final String DOT_DO_NO_ASK_COMMERCIAL_2 = ". " + DO_NO_ASK_COMMERCIAL_2;
	
	public void assignRule(String psRule) {
		this.rule = psRule;
		this.parsedRule = null;
		this.normalRule = null;
		
		boolean bCustomeRule = this.setCustomRule();
		
		//if ("[FULL AND OPEN EXCEPTION]".equals(this.questionName))
		//	this.normalRule = null;

		if (CommonUtils.isEmpty(psRule) || bCustomeRule)
			return;
		
		boolean bDoNotAsk = false;
		if (psRule.startsWith("DO NOT ASK ")) {
			bDoNotAsk = true;
			psRule = psRule.substring("DO NOT ASK ".length());
		}
		String sRest;
		String[] aItems;
		psRule = psRule.replaceAll(" IS COMPLETED", " IS NOT NULL")
				.replaceAll(" ANSWERS ARE COMPLETED", " IS NOT NULL")
				.replaceAll(" RESPONSES ARE COMPLETED", " IS NOT NULL")
				.replaceAll(" HAS BEEN SELECTED", " IS NOT NULL")
				.replaceAll(" RESPONSES HAVE BEEN COMPLETED", " IS NOT NULL")
				.replaceAll(" TO THE QUESTION ", " UNDER ")
				.replaceAll(" IS SELECTED UNDER ", " UNDER ")
				.replaceAll("\" FOR \"", "\" UNDER \"")
				.replaceAll("\"\"", "\"");

		boolean bNotOperation = false;
		String sLeft;
		int iPos = psRule.indexOf('"');
		if (iPos >= 0) {
			sLeft = psRule.substring(0, iPos).trim();
			if (sLeft.endsWith(" NOT")) // CJ-600 "ASK IF NOT "
				bNotOperation = true;
			psRule = psRule.substring(iPos);
		} else
			System.out.println(psRule);

		if (psRule.endsWith(":"))
			psRule = psRule.substring(0, psRule.length() - 1);
		
		if (this.questionName.contains("LEASE OPTION") || this.questionName.contains("ORDER VALUE")) {
			iPos = 0;
		}
		
		sRest = psRule.replaceAll("IF USER SELECTS ", "")
				.replaceAll("AFTER USE SELECTS ", "")
				.replaceAll("IF THE USER SELECTS ", "")
				.replaceAll("USER SELECTS ", "") 
				.replaceAll("IS SELECTED ", "")
				.replaceAll("USER SELECTS ", "").trim();

		if (sRest.charAt(0) == '"')
			sRest = sRest.substring(1);
		boolean bProcureDoNoAskCommercial = sRest.endsWith(AND_DO_NO_ASK_COMMERCIAL);
		if (bProcureDoNoAskCommercial) {
			sRest = sRest.substring(0, sRest.length() - AND_DO_NO_ASK_COMMERCIAL.length());
		} else { // CJ-1184
			bProcureDoNoAskCommercial = sRest.endsWith(DOT_DO_NO_ASK_COMMERCIAL);
			if (bProcureDoNoAskCommercial)
				sRest = sRest.substring(0, sRest.length() - DOT_DO_NO_ASK_COMMERCIAL.length());
			else {
				bProcureDoNoAskCommercial = sRest.endsWith(AND_DO_NO_ASK_COMMERCIAL_2);
				if (bProcureDoNoAskCommercial) {
					sRest = sRest.substring(0, sRest.length() - AND_DO_NO_ASK_COMMERCIAL_2.length());
				} else {
					bProcureDoNoAskCommercial = sRest.endsWith(DOT_DO_NO_ASK_COMMERCIAL_2);
					if (bProcureDoNoAskCommercial)
						sRest = sRest.substring(0, sRest.length() - DOT_DO_NO_ASK_COMMERCIAL_2.length());
				}
			}
		}

	aItems = sRest.split("\"");
		String sExpression = "";
		ArrayList<String> aLeft = new ArrayList<String>();
		String sRight = "", sOperator = "";
		int iItemType = 0;
		this.parsedRule = "";
		for (String sItem : aItems) {
			switch (iItemType++) {
			case 0:
				aLeft.add(sItem);
				break;
			case 1:
				if (" OR ".equals(sItem)) {
					iItemType = 0;
				}
				else if (sItem.startsWith(" IS NOT NULL")) {
					sExpression = "";
					for (String sCode : aLeft)
						sExpression += "[" + sCode + "]";
					aLeft.clear();
					//sOperator = "";
					if (sItem.equals(" IS NOT NULL AND USER HAS NOT SELECTED ")) {
						sItem = " IS NOT NULL AND ";
						bNotOperation = true;
						//sOperator = " AND ";
						iItemType++;
					}
					else if (sItem.equals(" IS NOT NULL AND ") || sItem.equals(" IS NOT NULL OR ")) {
						iItemType++;
					}
					else
						bNotOperation = false;
					sExpression += sItem;
					this.parsedRule += sExpression; // + sOperator;
					iItemType++;
				}
				else if (" UNDER ".equals(sItem))
					sOperator = " = ";
				else
					sOperator += sItem;
				break;
			case 2:
				sRight = sItem;
				break;
			default:
				if (this.addExpression(aLeft, sOperator, sRight, (bDoNotAsk ? !bNotOperation : bNotOperation)))
					bNotOperation = false;
				if (bDoNotAsk)
					if (" OR ".equals(sItem))
						sItem = " AND ";
					else if (" AND ".equals(sItem))
					sItem = " OR ";
				iPos = sItem.indexOf("ASK IF");
				if (iPos > 0) { // debug
					sItem = sItem.substring(0, iPos).trim();
					iPos = sItem.indexOf("DO NOT");
					if (iPos > 0) {
						sItem = sItem.substring(0, iPos).trim();
						bNotOperation = true;
					}
					if (this.parsedRule.contains(" OR ")) {
						if (!(this.parsedRule.startsWith("(") && this.parsedRule.endsWith(")"))) {
							this.parsedRule = "(" + this.parsedRule + ")";
						}
					}
					sItem = " " + sItem + " ";
				}
				if (" OR NOT ".equals(sItem)) {
					sItem = " OR ";
					bNotOperation = true;
				}
				this.parsedRule += sItem;
				//sExpression += sItem;
			}
			if (iItemType > 3)
				iItemType = 0;
		}
		this.addExpression(aLeft, sOperator, sRight, (bDoNotAsk ? !bNotOperation : bNotOperation));

		if (bProcureDoNoAskCommercial) {
			if (this.parsedRule.contains(" OR "))
				if (!(this.parsedRule.startsWith("(") && this.parsedRule.endsWith(")")))
					this.parsedRule = "(" + this.parsedRule + ")";
			this.parsedRule += " AND \"COMMERCIAL PROCEDURES (FAR PART 12)\" <> [PROCEDURES]";
		}
	}
	
	public void resolveRest() {
		if (this.questionType == null) {
			if (this.parent != null)
				this.questionType = this.parent.questionType;
			else {
				this.questionType = QUESTION_TYPE_SINGLE_ANSWER;
				System.out.println(this.questionName + ": Unable to resolve question type. Assigned [" + this.questionType + "]");
			}
		}
		this.normalRule = this.getPreceedingRule(null); // this.calcNormalRule();
	}
	
	public String getPreceedingRule(Stg2QuestionRecord poChild) {
		String result = this.parsedRule;
		if (poChild != null) {
			String sChoice = this.choices.getChoice(poChild.stg1QuestionId);
			if (sChoice != null) {
				if ("[DOCUMENT TYPE]".equals(this.questionName) && "AWARD".equals(sChoice)) {
					result = this.questionName + " IS NOT NULL"
							+ (CommonUtils.isEmpty(result) ? "" : " AND " + result);
				} else {
					result = "\"" + sChoice + "\" = " + this.questionName
							+ (CommonUtils.isEmpty(result) ? "" : " AND " + result);
				}
			}
		}
		if (this.parent != null) {
			String sPreceedingRule = this.parent.getPreceedingRule(this);
			if (sPreceedingRule != null) {
				result = sPreceedingRule + (CommonUtils.isEmpty(result) ? "" : " AND " + result);
			}
		}
		return result;
	}

	public void adjustBeforeSave() {
		if (QUESTION_TYPE_SINGLE_ANSWER.equals(this.questionType)) {
			if (this.choices.size() > 0) {
				if (this.choices.size() == 2) {
					boolean bYes = false, bNo = false;
					for (Stg2PossibleAnswerRecord oRecord : this.choices) {
						if ("YES".equalsIgnoreCase(oRecord.getChoiceText()))
							bYes = true;
						else if ("NO".equalsIgnoreCase(oRecord.getChoiceText()))
							bNo = true;
					}
					if (bYes && bNo)
						this.questionType = QUESTION_TYPE_BOOLEAN;
				}
			} else {
				System.out.println(this.questionName + " type is " + QUESTION_TYPE_SINGLE_ANSWER + " without choices");
			}
			
		}
	}
	
	public String debug() {
		String children = "";
		if (this.children != null) {
			for (Stg2QuestionRecord child : this.children) {
				children += (children.isEmpty() ? "\n  children: " : ", ") + child.getQuestionName();
			}
		}
		String result = "Name: " + this.questionName + " at level(" + this.parseLevel + ")" + ((stgRawQuestionRecord == null) ? "" : " " + stgRawQuestionRecord.getSheetInfo())
				+ "\n  category: " + this.category // + (this.isBaseline ? " (baseline)" : "")
				+ ((this.parent == null) ? "" : "\n    parent: " + parent.questionName)
				+ children
				//+ "\n     group: " + this.groupName
				+ "\n     qType: " + this.questionType
				// + "\n      type: " + this.type
				+ "\n      text: " + this.questionText
				+ ((this.rule == null) ? "" :       "\n      rule: " + this.rule)
				+ ((this.parsedRule == null) ? "" : "\nparsedRule: " + this.parsedRule)
				+ ((this.normalRule == null) ? "" : "\nnormalRule: " + this.normalRule)
				;
		if (this.choices.size() > 0) {
			result += "\n   choices: " + this.choices.debug();  
		}

		/*
		private Integer stg2QuestionId = null;
		private Integer stg1QuestionId = null;
		private String parsedRule = null;
		private String normalRule = null;
		private Integer sequence = null;
		private Integer rootQuestionId = null;
		private Integer latestParentQuestionId = null;
		private Integer srcDocId = null;
		*/
		
		return result + "\n";
	}
	
}
