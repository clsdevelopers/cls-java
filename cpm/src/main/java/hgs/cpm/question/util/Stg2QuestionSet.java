package hgs.cpm.question.util;

import gov.dod.cls.utils.sql.SqlUtil;
import hgs.cpm.utils.SqlCommons;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class Stg2QuestionSet extends ArrayList<Stg2QuestionRecord> {

	private static final long serialVersionUID = 3541800697449007182L;

	private int srcDocId;
	
	public Stg2QuestionSet(int srcDocId) {
		this.srcDocId = srcDocId;
	}
	
	public Stg2QuestionRecord findParent(int iLevel) {
		Stg2QuestionRecord oRecord = null;
		int iTargetLevel = iLevel - 1;
		int iIndex;
		for (iIndex = this.size() - 1; iIndex >= 0; iIndex--) {
			oRecord = this.get(iIndex);
			int iRecLevel = oRecord.getParseLevel();
			if (iRecLevel >= iLevel)
				continue;

			if (iTargetLevel == iRecLevel)
				return oRecord;

			if (iTargetLevel > iRecLevel)
				return null;
		}
		return null;
	}

	public void assignSequence(Stg1QuestionSequenceSet stg1QuestionSequenceSet) {
		ArrayList<Stg2QuestionRecord> aToResolve = new ArrayList<Stg2QuestionRecord>();
		Stg2QuestionRecord oPrevious = null;
		for (int iIndex = 0; iIndex < this.size(); iIndex++) {
			Stg2QuestionRecord oRecord = this.get(iIndex);
			Stg1QuestionSequenceRecord stg1QuestionSequenceRecord = stg1QuestionSequenceSet.findByName(oRecord.getQuestionName());
			if (stg1QuestionSequenceRecord != null) {
				oRecord.setSequence(stg1QuestionSequenceRecord.getSequence());
				String sValue = stg1QuestionSequenceRecord.getDefaultBaseline();
				oRecord.setDefaultBaseline("YES".equalsIgnoreCase(sValue) || "1".equals(sValue)); // Changed from bellow
				// oRecord.setDefaultBaseline("YES".equalsIgnoreCase(stg1QuestionSequenceRecord.getDefaultBaseline())); // CJ-584
				if (aToResolve.size() == 0) {
					oPrevious = oRecord;
				} else {
					Integer iPrevious = oPrevious.getSequence();
					Integer iNext = oRecord.getSequence();
					Integer iDiff = iNext - iPrevious;
					Integer iGap = iDiff / (aToResolve.size() + 1);
					for (Stg2QuestionRecord oResolve : aToResolve) {
						iPrevious += iGap;
						oResolve.setSequence(iPrevious);
						System.out.println(oResolve.getQuestionName() + " Set sequence(" + iPrevious + ")");
					}
					aToResolve.clear();
				}
			} else {
				aToResolve.add(oRecord);
				//System.out.println(oRecord.getQuestionName() + " - Not found sequence");
			}
		}
		if (aToResolve.size() > 0) {
			Integer iPrevious = oPrevious.getSequence();
			for (Stg2QuestionRecord oResolve : aToResolve) {
				iPrevious += 100;
				oResolve.setSequence(iPrevious);
				System.out.println(oResolve.getQuestionName() + " Set sequence(" + iPrevious + ")");
			}
		}
	}

	public void save(Connection conn) throws SQLException {
		String sSql;
		sSql = "DELETE FROM " + Stg2QuestionRecord.TABLE_STG2_QUESTION
				+ " WHERE " + Stg2QuestionRecord.FIELD_SRC_DOC_ID + " = ?";
		SqlCommons.executeForDocId(conn, sSql, this.srcDocId);
		
		sSql = "DELETE FROM " + Stg2PossibleAnswerRecord.TABLE_STG2_POSSIBLE_ANSWER
				+ " WHERE " + Stg2QuestionRecord.FIELD_SRC_DOC_ID + " = ?";
		SqlCommons.executeForDocId(conn, sSql, this.srcDocId);
		
		PreparedStatement psQuestion = null;
		PreparedStatement psPossibleAnswer = null;
		PreparedStatement psAnswerPrescription = null;
		ResultSet res = null;
		try {
			System.out.println("Stg2QuestionSet.save() Start ===============================");
			psQuestion = conn.prepareStatement(Stg2QuestionRecord.SQL_INSERT, Statement.RETURN_GENERATED_KEYS);
			psPossibleAnswer = Stg2PossibleAnswerSet.createPrepareStatement(conn);
			psAnswerPrescription = Stg2PossibleAnswerPrescriptionRecord.createPrepareStatement(conn);
			for (Stg2QuestionRecord stg2QuestionRecord : this) {
				stg2QuestionRecord.setSrcDocId(this.srcDocId);
				stg2QuestionRecord.adjustBeforeSave();
				stg2QuestionRecord.setPreparedStatement(psQuestion);
				System.out.println(stg2QuestionRecord.getQuestionName());
				psQuestion.execute();
				res = psQuestion.getGeneratedKeys();
				if (res.next()) {
					int stg2QuestionId = res.getInt(1);
					stg2QuestionRecord.setStg2QuestionId(stg2QuestionId);
					stg2QuestionRecord.getChoices().save(psPossibleAnswer, stg2QuestionId, this.srcDocId, psAnswerPrescription);
				}
				SqlUtil.closeInstance(res);
			}
			System.out.println("Stg2QuestionSet.save() End =================================");
		} finally {
			SqlUtil.closeInstance(psAnswerPrescription);
			SqlUtil.closeInstance(psPossibleAnswer);
			SqlUtil.closeInstance(psQuestion);
		}
	}
	
	// ==============================================================
	public int getSrcDocId() { return srcDocId; }
	public void setSrcDocId(int srcDocId) { this.srcDocId = srcDocId; }
	
}
