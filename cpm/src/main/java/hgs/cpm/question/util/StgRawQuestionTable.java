package hgs.cpm.question.util;

import gov.dod.cls.utils.sql.SqlUtil;
import hgs.cpm.config.CpmConfig;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class StgRawQuestionTable extends ArrayList<StgRawQuestionRecord> {

	private static final long serialVersionUID = 7565898123556643969L;

	private Connection conn;
	private int srcDocId;
	
	public Stg2QuestionSet stg2QuestionSet;
	
	public StgRawQuestionTable(Connection conn, int srcDocId) {
		this.conn = conn;
		this.srcDocId = srcDocId;
		this.stg2QuestionSet = new Stg2QuestionSet(this.srcDocId);
	}
	
	public StgRawQuestionRecord findParent(int iLevel) {
		StgRawQuestionRecord oRecord = null;
		int iTargetLevel = iLevel - 1;
		int iIndex;
		for (iIndex = this.size() - 1; iIndex >= 0; iIndex--) {
			oRecord = this.get(iIndex);
			if (!oRecord.isEmpty()) {
				int iRecLevel = oRecord.parseLevel();
				if (iRecLevel >= iLevel)
					continue;
	
				if (iTargetLevel == iRecLevel)
					return oRecord;
			}
		}
		return null;
	}
	
	public void loadAndParse() throws SQLException {
		this.clear();
		this.stg2QuestionSet.clear();
		
		PreparedStatement ps1 = null;
		ResultSet rs1 = null;
		try {
			String sSql = StgRawQuestionRecord.SQL_SELECT;
			ps1 = conn.prepareStatement(sSql);
			ps1.setInt(1, this.srcDocId);
			rs1 = ps1.executeQuery();
			StgRawQuestionRecord oPrevious = null;
			while (rs1.next()) {
				StgRawQuestionRecord oRecord = new StgRawQuestionRecord(this);
				oRecord.read(rs1);
				this.add(oRecord);
				if (oPrevious != null) {
					if ((oPrevious.getSheetId() != oRecord.getSheetId()) || 
						//(oPrevious.getRowNumber() != oRecord.getRowNumber()) || 
						(oPrevious.isBaseline() != oRecord.isBaseline())) {
						this.clear();
						oPrevious = null;
						//System.out.println("=================================");
					}
				}
				if (!oRecord.isEmpty()) {
					Stg2QuestionRecord stg2QuestionRecord = null;
					//if (oRecord.getQuestion().startsWith("(i) FOOD")) { // ("2. CIVILIAN".equals(oRecord.getQuestion())) { // a. COAST GUARD // a. DEFENSE LOGISTICS AGENCY
					//	String sAnswer = oRecord.getAnswer();
					//	sAnswer = "";
					//}
					int iLevel = oRecord.parseLevel();
					if (oRecord.parseQuestionCode()) {
						//String sQuestonCode = oRecord.getQuestionCode();
						//System.out.println("\n" + sQuestonCode + " " + oRecord.getSheetInfo() + " (" + iLevel + ")"); //  + " " + iLevel
						stg2QuestionRecord = new Stg2QuestionRecord();
						oRecord.populate(stg2QuestionRecord, false);
						
						Stg2QuestionRecord parent = null;
						if ((oPrevious != null) && (!oPrevious.isEmpty()) &&
								(oPrevious.getLevel() == (iLevel - 1))) 
						{
							if (oPrevious.getStg2QuestionRecord() == null) {
								StgRawQuestionRecord oParentRecord = oPrevious;
								parent = new Stg2QuestionRecord();
								oParentRecord.populate(parent, true);
								
								int iParentLevel = (iLevel - 1);
								Stg2QuestionRecord grandparent = this.resolveParents(iParentLevel);
								if (grandparent != null) {
									parent.setParent(grandparent);
									grandparent.addChild(parent);
								}

								this.stg2QuestionSet.add(parent);
								parent.resolveRest();
							} else {
								parent = oPrevious.getStg2QuestionRecord();
							}
						} else {
							parent = this.resolveParents(iLevel);
						}
						
						if (parent != null) {
							stg2QuestionRecord.setParent(parent);
							parent.addChild(stg2QuestionRecord);
						}
						
						this.stg2QuestionSet.add(stg2QuestionRecord);
						//System.out.println(stg2QuestionRecord.debug());
					} else {
						//System.out.println("   " + iLevel);
					}
					//if (oRecord.isBaseline()) {
						String sAnswer = oRecord.getAnswer();
						if (iLevel == 0) {
						} else if (iLevel < 0) {
							if (this.stg2QuestionSet.size() > 0) {
								if (sAnswer != null) {
									Stg2QuestionRecord parent = this.stg2QuestionSet.get(this.stg2QuestionSet.size() - 1);
									if (sAnswer.contains("USER TO FILL IN NUMERICAL")) {
										String sOldType = parent.getQuestionType();
										if (!Stg2QuestionRecord.QUESTION_TYPE_NUMERIC.equals(sOldType)) {
											parent.setQuestionType(Stg2QuestionRecord.QUESTION_TYPE_NUMERIC);
											//	if (Stg2QuestionRecord.QUESTION_TYPE_NUMERIC.equals(parent.getQuestionType()) && sAnswer.startsWith("[")) {
											System.out.println(oRecord.getSheetInfo() + " Changed question type from " + sOldType + " to Number Type for (" + sAnswer + ")");
										}
									} else {
										if (!("x".equals(sAnswer) && ((oRecord.getRowNumber() - parent.getStgRawQuestionRecord().getRowNumber()) >= 1)))  
											parent.addChoice(sAnswer, oRecord.getPromptAfterSelection(), oRecord.getPrescription(), oRecord.getStg1QuestionId());
									}
								}
							} else {
								System.out.println("NOT FOUND Parent Question");
							}
						}
						else if (sAnswer != null) {
							Stg2QuestionRecord parent = null;
							if ((oPrevious != null) && (!oPrevious.isEmpty()) &&
									(oPrevious.getLevel() == (iLevel - 1))) {
								if (oPrevious.getStg2QuestionRecord() == null) {
									StgRawQuestionRecord oParentRecord = oPrevious;
									parent = new Stg2QuestionRecord();
									oParentRecord.populate(parent, true);
									
									int iParentLevel = (iLevel - 1);
									Stg2QuestionRecord grandparent = this.resolveParents(iParentLevel);
									if (grandparent != null) {
										parent.setParent(grandparent);
										grandparent.addChild(parent);
									}
									this.stg2QuestionSet.add(parent);
									parent.resolveRest();
								} else {
									parent = oPrevious.getStg2QuestionRecord();
								}
							} else {
								parent = this.stg2QuestionSet.findParent(iLevel);
								if (parent == null) {
									if ("COAST GUARD".equals(sAnswer))
										parent = null;
									StgRawQuestionRecord oParentRecord = this.findParent(iLevel);
									if (oParentRecord != null) {
										if (oParentRecord.getStg2QuestionRecord() == null) {
											parent = new Stg2QuestionRecord();
											oParentRecord.populate(parent, true);

											int iParentLevel = (iLevel - 1);
											Stg2QuestionRecord grandparent = this.resolveParents(iParentLevel);
											if (grandparent != null) {
												parent.setParent(grandparent);
												grandparent.addChild(parent);
											}
											
											/*
											if (parent.getParent() == null) {
												Stg2QuestionRecord grandparent = this.stg2QuestionSet.findParent(iLevel - 1);
												if (grandparent != null) {
													parent.setParent(grandparent);
													grandparent.addChild(parent);
												}
											}
											*/
											
											this.stg2QuestionSet.add(parent);
											parent.resolveRest();
										} else {
											parent = oParentRecord.getStg2QuestionRecord();
										}
										//stg2QuestionRecord = parent;
									}
								} else {
									if (stg2QuestionRecord != null) {
										stg2QuestionRecord.setParent(parent);
										parent.addChild(stg2QuestionRecord);
									}
								}
							}
							if (parent != null) {
								if (sAnswer.contains("USER TO FILL IN NUMERICAL")) {
									String sOldType = parent.getQuestionType();
									if (!Stg2QuestionRecord.QUESTION_TYPE_NUMERIC.equals(sOldType)) {
										parent.setQuestionType(Stg2QuestionRecord.QUESTION_TYPE_NUMERIC);
										//	if (Stg2QuestionRecord.QUESTION_TYPE_NUMERIC.equals(parent.getQuestionType()) && sAnswer.startsWith("[")) {
										System.out.println(oRecord.getSheetInfo() + " Changed question type from " + sOldType + " to Number Type for (" + sAnswer + ")");
									}
								} else
									parent.addChoice(sAnswer, oRecord.getPromptAfterSelection(), oRecord.getPrescription(), oRecord.getStg1QuestionId());
							} else
								System.out.println(oRecord.getSheetInfo() + " NOT FOUND Parent Question for (" + sAnswer + ")");
						}
					//}
					if (stg2QuestionRecord != null)
						stg2QuestionRecord.resolveRest();
					//if (oRecord.getAnswer() != null)
					//	System.out.println("  (" + iLevel + ") Answer: " + oRecord.getAnswer());
				}
				oPrevious = oRecord; 
			}
			
			/*
			String[] aCodes = {"[BRAND NAME BASIS]",
				"[MAJOR DESIGN OR DEVELOPMENT]",
				"[PCO EPA DETERMINATION]",
				"[GOVERNMENT SUPPLY STATEMENT]",
				"[INAPPROPRIATE CONTINGENCY]",
				"[CURRENCY]"
			};
			*/
			System.out.println("==================================================================");
			for (Stg2QuestionRecord stg2QuestionRecord : this.stg2QuestionSet) {
				if ("D".equals(stg2QuestionRecord.getGroupName()))
					System.out.println(stg2QuestionRecord.debug());
				if (stg2QuestionRecord.getRule() != null) { //  && (stg2QuestionRecord.getParsedRule() == null)
					boolean bFound = false;
					/*
					String sQuestionName = stg2QuestionRecord.getQuestionName();
					for (String sCode : aCodes)
						if (sCode.equals(sQuestionName)) {
							bFound = true;
							break;
						}
					*/
					if (bFound)
						System.out.println(stg2QuestionRecord.getQuestionName() + "\t" + stg2QuestionRecord.getRule() + "\t" + (stg2QuestionRecord.getParsedRule() == null ? "" : stg2QuestionRecord.getParsedRule()) );
				}
			}
		} finally {
			SqlUtil.closeInstance(rs1, ps1);
		}
		
	}
	
	private Stg2QuestionRecord resolveParents(int iLevel) {
		if (iLevel <= 0)
			return null;
		Stg2QuestionRecord parent = this.stg2QuestionSet.findParent(iLevel);
		if (parent == null) {
			StgRawQuestionRecord oParentRaw = this.findParent(iLevel);
			if (oParentRaw != null) {
				if (oParentRaw.getStg2QuestionRecord() == null) {
					parent = new Stg2QuestionRecord();
					oParentRaw.populate(parent, true);
					Stg2QuestionRecord oGrandQuestion = this.resolveParents(oParentRaw.getLevel());
					if (oGrandQuestion != null) {
						oGrandQuestion.addChild(parent);
						parent.setParent(oGrandQuestion);
					}
					this.stg2QuestionSet.add(parent);
					parent.resolveRest();
				} else {
					parent = oParentRaw.getStg2QuestionRecord();
				}
				//stg2QuestionRecord = parent;
			}
		}
		return parent;
	}
	
	// ===================================================================
	public static void main( String[] args )
    {
		CpmConfig cpmConfig = CpmConfig.getInstance();
		int iQuestionSequenceSrcDocId = 376;
		int iQuestionSrcDocId = 408;

		Connection conn = null;
    	try {
    		conn = cpmConfig.getDbConnection();
    		
    		StgRawQuestionTable stgRawQuestionTable = new StgRawQuestionTable(conn, iQuestionSrcDocId);
    		stgRawQuestionTable.loadAndParse();
    		
    		Stg1QuestionSequenceSet stg1QuestionSequenceSet = new Stg1QuestionSequenceSet();
    		stg1QuestionSequenceSet.load(conn, iQuestionSequenceSrcDocId);
    		
    		stgRawQuestionTable.stg2QuestionSet.assignSequence(stg1QuestionSequenceSet);
    		stgRawQuestionTable.stg2QuestionSet.save(conn);
    	}
    	catch (Exception oError) {
    		oError.printStackTrace();
    	}
    	finally {
    		SqlUtil.closeInstance(conn);
    	}
    }
	
}
