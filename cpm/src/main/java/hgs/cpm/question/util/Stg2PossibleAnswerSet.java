package hgs.cpm.question.util;

import gov.dod.cls.utils.sql.SqlUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class Stg2PossibleAnswerSet extends ArrayList<Stg2PossibleAnswerRecord> {

	private static final long serialVersionUID = 263398939588653853L;
	
	public static PreparedStatement createPrepareStatement(Connection conn) throws SQLException {
		return conn.prepareStatement(Stg2PossibleAnswerRecord.SQL_INSERT, Statement.RETURN_GENERATED_KEYS);
	}

	private Stg2QuestionRecord parent = null;

	public void addChoice(String psText, String psPromptAfterSelection, String psPrescriptions, int stg1QuestionId) {
		Stg2PossibleAnswerRecord oRecord = new Stg2PossibleAnswerRecord();
		oRecord.setChoiceText(psText);
		oRecord.setStg1QuestionId(stg1QuestionId);
		oRecord.assignPrescription(psPrescriptions);
		oRecord.setPromptAfterSelection(psPromptAfterSelection);
		this.add(oRecord);
	}
	
	public String getChoice(int stg1QuestionId) {
		for (Stg2PossibleAnswerRecord oRecord : this) {
			if (oRecord.getStg1QuestionId().intValue() == stg1QuestionId)
				return oRecord.getChoiceText();
		}
		return null;
	}
	
	public void save(PreparedStatement psStg2PossibleAnswer, int stg2QuestionId, int srcDocId, PreparedStatement psAnswerPrescription) throws SQLException {
		for (Stg2PossibleAnswerRecord oRecord : this) {
			ResultSet res = null;
			try {
				oRecord.setStg2QuestionId(stg2QuestionId);
				oRecord.setSrcDocId(srcDocId);
				oRecord.setPreparedStatement(psStg2PossibleAnswer);
				psStg2PossibleAnswer.execute();
				res = psStg2PossibleAnswer.getGeneratedKeys();
				if (res.next()) {
					oRecord.setStg2PossibleAnswerId(res.getInt(1));
					oRecord.savePrescriptions(psAnswerPrescription);
				}
			}
			finally {
				SqlUtil.closeInstance(res);
			}
		}
	}
	
	public String debug() {
		String result = "";
		for (int iIndex = 0; iIndex < this.size(); iIndex++) {
			result += "\n   - " + this.get(iIndex).debug();
		}
		return result;
	}
	
	// ============================================================
	public Stg2QuestionRecord getParent() { return parent; }
	public void setParent(Stg2QuestionRecord parent) { this.parent = parent; }
	
}
