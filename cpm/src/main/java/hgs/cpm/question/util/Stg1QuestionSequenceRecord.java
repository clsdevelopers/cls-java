package hgs.cpm.question.util;

import java.sql.ResultSet;
import java.sql.SQLException;

public class Stg1QuestionSequenceRecord {

	public static final String TABLE_STG1_QUESTOIN_SEQUENCE = "Stg1_Questoin_Sequence";
	
	public static final String FIELD_SG1_QUESTIION_SEQ_ID	= "Sg1_Question_Seq_Id";
	public static final String FIELD_SRC_DOC_ID            = "Src_Doc_Id";
	public static final String FIELD_QUESTION_NAME         = "Question_Name";
	public static final String FIELD_SEQUENCE              = "Sequence";
	public static final String FIELD_DEFAULT_BASELINE      = "Default_Baseline"; // CJ-584
	
	public static final String SQL_SELECT
		= "SELECT " + FIELD_QUESTION_NAME + ", " + FIELD_SEQUENCE + ", " + FIELD_DEFAULT_BASELINE // CJ-584
		+ " FROM " + TABLE_STG1_QUESTOIN_SEQUENCE
		+ " WHERE " + FIELD_SRC_DOC_ID + " = ?"
		+ " ORDER BY " + FIELD_SEQUENCE;

	// private Integer sg1QuestionSeqId = null;
	//private Integer srcDocId = null;
	private String questionName = null;
	private Integer sequence = null;
	private String defaultBaseline = null; // CJ-584
	
	public void load(ResultSet rs) throws SQLException {
		this.questionName = rs.getString(1);
		this.sequence = rs.getInt(2);
		this.defaultBaseline = rs.getString(3); // CJ-584
	}
	
	// =================================================================
	public String getQuestionName() {
		return questionName;
	}
	public void setQuestionName(String questionName) {
		this.questionName = questionName;
	}

	public Integer getSequence() {
		return sequence;
	}
	public void setSequence(Integer sequence) {
		this.sequence = sequence;
	}

	public String getDefaultBaseline() {
		return defaultBaseline;
	}
	public void setDefaultBaseline(String defaultBaseline) {
		this.defaultBaseline = defaultBaseline;
	}

}
