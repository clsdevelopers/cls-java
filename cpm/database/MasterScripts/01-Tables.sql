-- =====================================================================
CREATE TABLE Stg_Src_Doc_Type_Ref
(
	Src_Doc_Type_Cd      CHAR(1) NOT NULL,
	Src_Doc_Type_Name    VARCHAR(20) NOT NULL
);

ALTER TABLE Stg_Src_Doc_Type_Ref
ADD PRIMARY KEY (Src_Doc_Type_Cd);

CREATE UNIQUE INDEX XAKStg_Src_Doc_Type_Ref_Name ON Stg_Src_Doc_Type_Ref
(
	Src_Doc_Type_Name
);

INSERT INTO Stg_Src_Doc_Type_Ref (Src_Doc_Type_Cd, Src_Doc_Type_Name) VALUES ('Q', 'Question');
INSERT INTO Stg_Src_Doc_Type_Ref (Src_Doc_Type_Cd, Src_Doc_Type_Name) VALUES ('F', 'FAR/DFAR');
INSERT INTO Stg_Src_Doc_Type_Ref (Src_Doc_Type_Cd, Src_Doc_Type_Name) VALUES ('E', 'EFCR');
INSERT INTO Stg_Src_Doc_Type_Ref (Src_Doc_Type_Cd, Src_Doc_Type_Name) VALUES ('S', 'Question Sequence');

-- =====================================================================
CREATE TABLE Stg_Src_Document
(
	Src_Doc_Id           SMALLINT NOT NULL AUTO_INCREMENT,
	Src_File_Name        VARCHAR(80) NULL,
	Src_Doc_Type_Cd      CHAR(1) NOT NULL,
	Src_Vesion           VARCHAR(20) NULL,
	Version_Date         DATE NULL,
	Reson_For_Update     VARCHAR(255) NULL,
	Created_Date         DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY (Src_Doc_Id)
);


CREATE INDEX XIFStg_Src_Doc_Src_Doc_Type ON Stg_Src_Document
(
	Src_Doc_Type_Cd
);

-- =====================================================================
CREATE TABLE Stg_Question_Sheet_Ref
(
	Sheet_Id             NUMERIC(3) NOT NULL,
	Sheet_Name           VARCHAR(40) NOT NULL,
	PRIMARY KEY (Sheet_Id)
);

CREATE UNIQUE INDEX XAKSrc_Doc_Sheet_Ref_Name ON Stg_Question_Sheet_Ref
(
	Sheet_Name
);

INSERT INTO Stg_Question_Sheet_Ref (Sheet_Id, Sheet_Name) VALUES (0, 'Basic Questions');
INSERT INTO Stg_Question_Sheet_Ref (Sheet_Id, Sheet_Name) VALUES (1, 'Acquisition Planning');
INSERT INTO Stg_Question_Sheet_Ref (Sheet_Id, Sheet_Name) VALUES (2, 'Solicitation-Contract');
INSERT INTO Stg_Question_Sheet_Ref (Sheet_Id, Sheet_Name) VALUES (3, 'Supplies or Svcs and Prices');
INSERT INTO Stg_Question_Sheet_Ref (Sheet_Id, Sheet_Name) VALUES (4, 'Description or Specifications');
INSERT INTO Stg_Question_Sheet_Ref (Sheet_Id, Sheet_Name) VALUES (5, 'Packaging and Marking');
INSERT INTO Stg_Question_Sheet_Ref (Sheet_Id, Sheet_Name) VALUES (6, 'Inspection and Acceptance');
INSERT INTO Stg_Question_Sheet_Ref (Sheet_Id, Sheet_Name) VALUES (7, 'Deliveries and Performance');
INSERT INTO Stg_Question_Sheet_Ref (Sheet_Id, Sheet_Name) VALUES (8, 'Transportation');
INSERT INTO Stg_Question_Sheet_Ref (Sheet_Id, Sheet_Name) VALUES (9, 'Contract Admin Data');
INSERT INTO Stg_Question_Sheet_Ref (Sheet_Id, Sheet_Name) VALUES (10, 'Insurance and Claims');
INSERT INTO Stg_Question_Sheet_Ref (Sheet_Id, Sheet_Name) VALUES (11, 'Contract Clauses');
INSERT INTO Stg_Question_Sheet_Ref (Sheet_Id, Sheet_Name) VALUES (12, 'Pricing');
INSERT INTO Stg_Question_Sheet_Ref (Sheet_Id, Sheet_Name) VALUES (13, 'Evaluation Factors for Award');

-- =====================================================================
CREATE TABLE Stg1_Question
(
	Stg1_Question_Id     INTEGER AUTO_INCREMENT,
	Src_Doc_Id           SMALLINT NOT NULL,
	Sheet_Id             NUMERIC(3) NOT NULL,
	Row_Number           SMALLINT NULL,
	Is_Baseline          boolean NOT NULL,
	Question             VARCHAR(2000) NULL,
	Prescription         VARCHAR(2000) NULL,
	PRIMARY KEY (Stg1_Question_Id)
);

CREATE UNIQUE INDEX XAKStg1_Questions_Src_Row ON Stg1_Question
(
	Src_Doc_Id,
	Row_Number,
	Sheet_Id,
	Is_Baseline
);

CREATE INDEX XIFStg1_Question_Src_Doc_Id ON Stg1_Question
(
	Src_Doc_Id
);

CREATE INDEX XIF2Stg1_Question ON Stg1_Question
(
	Sheet_Id
);

-- =================================================
ALTER TABLE Stg_Src_Document
ADD FOREIGN KEY R_Stg_Src_Doc_Type_Stg_Src_Doc (Src_Doc_Type_Cd) REFERENCES Stg_Src_Doc_Type_Ref (Src_Doc_Type_Cd);

ALTER TABLE Stg1_Question
ADD FOREIGN KEY R_Stg_Src_Doc_Stg1_Question (Src_Doc_Id) REFERENCES Stg_Src_Document (Src_Doc_Id)
		ON DELETE CASCADE;

ALTER TABLE Stg1_Question
ADD FOREIGN KEY R_Stg_Question_Sheet_Ref_Question (Sheet_Id) REFERENCES Stg_Question_Sheet_Ref (Sheet_Id);

-- =================================================
CREATE TABLE Stg1_Questoin_Sequence
(
	Sg1_Question_Seq_Id  INTEGER AUTO_INCREMENT,
	Src_Doc_Id           SMALLINT NOT NULL,
	Question_Name        VARCHAR(100) NOT NULL,
	Sequence             INTEGER NOT NULL,
	Commercial_Status    VARCHAR(50) NULL,
	Default_Baseline     VARCHAR(3) NULL,
	PRIMARY KEY (Sg1_Question_Seq_Id)
);

CREATE INDEX XIE1Stg1_Questoin_Sequence ON Stg1_Questoin_Sequence
(
	Src_Doc_Id,
	Sequence
);

CREATE INDEX XIF1Starging_1_Questoin_Sequence ON Stg1_Questoin_Sequence
(
	Src_Doc_Id
);

ALTER TABLE Stg1_Questoin_Sequence
ADD FOREIGN KEY R_Stg_Src_Document_Stg1_Question_Sequence (Src_Doc_Id) REFERENCES Stg_Src_Document (Src_Doc_Id)
		ON DELETE CASCADE;

-- =================================================
CREATE TABLE Stg2_Question
(
	Stg2_Question_Id     INTEGER AUTO_INCREMENT,
	Stg1_Question_Id     INTEGER NULL,
	Question_Name        VARCHAR(100) NULL,
	Question_Text        VARCHAR(1000) NULL,
	Question_Type        VARCHAR(100) NULL,
	Parsed_Rule          VARCHAR(1000) NULL,
	Rule                 VARCHAR(1000) NULL,
	Type                 VARCHAR(1000) NULL,
	Group_Name           VARCHAR(100) NULL,
	Is_Baseline          boolean NOT NULL,
	Category 			 VARCHAR(100) NULL,
	Sequence             INTEGER NULL,
	Root_Question_Id     INTEGER NULL,
	Normal_Rule          VARCHAR(1000) NULL,
	Latest_Parent_Question_Id INTEGER NULL,
	Src_Doc_Id           SMALLINT NULL,
	Parse_Level          TINYINT NULL,
	Parent_Stg2_Question_Id INTEGER NULL,
	Default_Baseline     boolean NOT NULL DEFAULT 0,
 	PRIMARY KEY (Stg2_Question_Id)
);

CREATE INDEX XIFStg2_Question_Stg1_Question ON Stg2_Question
(
	Stg1_Question_Id
);

CREATE INDEX XIFStg2_Question_Src_Doc_Id ON Stg2_Question
(
	Src_Doc_Id
);

CREATE INDEX XIFStg2_Question_Parent ON Stg2_Question
(
	Parent_Stg2_Question_Id
);

ALTER TABLE Stg2_Question
ADD FOREIGN KEY R_Stg1_Question_Stg2_Parent (Stg1_Question_Id) REFERENCES Stg1_Question (Stg1_Question_Id)
		ON DELETE CASCADE;

ALTER TABLE Stg2_Question
ADD FOREIGN KEY R_Stg_Src_Document_Stg2_Queston (Src_Doc_Id) REFERENCES Stg_Src_Document (Src_Doc_Id)
		ON DELETE CASCADE;

ALTER TABLE Stg2_Question
ADD FOREIGN KEY R_Stg2_Question_Parent (Parent_Stg2_Question_Id) REFERENCES Stg2_Question (Stg2_Question_Id)
		ON DELETE SET NULL;

-- =================================================
CREATE TABLE Stg2_Possible_Answer
(
	Stg2_Possible_Answer_Id INTEGER AUTO_INCREMENT,
	Stg1_Question_Id     INTEGER NULL,
	Stg2_Question_Id     INTEGER NULL,
	Choice_Text          VARCHAR(500) NULL,
	Choice_Text2         VARCHAR(500) NULL,
	Prompt_After_Selection VARCHAR(255) NULL,
	Soundex_Hash         VARCHAR(100) NULL,
	Src_Doc_Id           SMALLINT NULL,
	PRIMARY KEY (Stg2_Possible_Answer_Id)
);

CREATE INDEX XIF1Stg2_Possible_Answer ON Stg2_Possible_Answer
(
	Stg1_Question_Id
);

CREATE INDEX XIF2Stg2_Possible_Answer ON Stg2_Possible_Answer
(
	Stg2_Question_Id
);

CREATE INDEX XIF3Stg2_Possible_Answer ON Stg2_Possible_Answer
(
	Src_Doc_Id
);

ALTER TABLE Stg2_Possible_Answer
ADD FOREIGN KEY R_Stg1_Question_Stg2_Possible_Answer (Stg1_Question_Id) REFERENCES Stg1_Question (Stg1_Question_Id)
		ON DELETE CASCADE;

ALTER TABLE Stg2_Possible_Answer
ADD FOREIGN KEY R_Stg2_Question_Stg2_Possible_Answer (Stg2_Question_Id) REFERENCES Stg2_Question (Stg2_Question_Id)
		ON DELETE CASCADE;

ALTER TABLE Stg2_Possible_Answer
ADD FOREIGN KEY R_Stg_Srouce_Document_Stg2_Possible_Answer (Src_Doc_Id) REFERENCES Stg_Src_Document (Src_Doc_Id)
		ON DELETE CASCADE;

-- =================================================
CREATE TABLE Stg2_Possible_Answers_Prescription
(
	Stg2_Possible_Answer_Id INTEGER NOT NULL,
	Prescription         VARCHAR(100) NOT NULL,
	PRIMARY KEY (Stg2_Possible_Answer_Id, Prescription)
);

ALTER TABLE Stg2_Possible_Answers_Prescription
ADD FOREIGN KEY R_Stg2_Possible_Answer_Prescription (Stg2_Possible_Answer_Id) REFERENCES Stg2_Possible_Answer (Stg2_Possible_Answer_Id)
		ON DELETE CASCADE;

-- =================================================
CREATE TABLE Stg2_Question_Dependency
(
	Stg2_Dependency_Id INTEGER AUTO_INCREMENT,
	Stg2_Question_Id     INTEGER NOT NULL,
	Referenced_Question_Name VARCHAR(500) NULL,
	Referenced_Question_Id INTEGER NULL,
	PRIMARY KEY (Stg2_Dependency_Id)
);

CREATE INDEX XIF1Stg2_Question_Dependency ON Stg2_Question_Dependency
(
	Stg2_Question_Id
);

ALTER TABLE Stg2_Question_Dependency
ADD FOREIGN KEY R_Stg2_Question_Stg2_Dependency (Stg2_Question_Id) REFERENCES Stg2_Question (Stg2_Question_Id)
		ON DELETE CASCADE;

		
-- ==============================================
CREATE TABLE Stg1_Official_Clause
(
	Stg1_Official_Clause_Id INTEGER AUTO_INCREMENT,
	Clause_Number        VARCHAR(100) NULL,
	Title                VARCHAR(500) NULL,
	Prescription         VARCHAR(100) NULL,
	URI                  VARCHAR(500) NULL,
	Content              TEXT NULL,
	Title_In_Content     VARCHAR(500) NULL,
	Content_HTML         LONGTEXT NULL,
	Content_Html_FillIns LONGTEXT NULL,
	Content_Html_Processed LONGTEXT NULL,
	Src_Doc_Id           SMALLINT NOT NULL,
	Manually_Added       boolean NOT NULL DEFAULT 0,
	IsEditAndFillIn 	 SMALLINT NOT NULL DEFAULT 0,
	PRIMARY KEY (Stg1_Official_Clause_Id)
);

CREATE INDEX XIE1Stg1_Official_Clause_Number ON Stg1_Official_Clause
(
	Src_Doc_Id,
	Clause_Number
);

CREATE INDEX XIF1Stg1_Official_Clause ON Stg1_Official_Clause
(
	Src_Doc_Id
);

ALTER TABLE Stg1_Official_Clause
ADD FOREIGN KEY R_Stg_Source_Document_Stg2_Official_Clause (Src_Doc_Id) REFERENCES Stg_Src_Document (Src_Doc_Id)
		ON DELETE CASCADE;

-- ==============================================
CREATE TABLE Stg2_Clause_Fill_Ins
(
	Stg2_Clause_Fillins_Id INTEGER AUTO_INCREMENT,
	Stg1_Official_Clause_Id INTEGER NOT NULL,
	Fill_In_Code         VARCHAR(200) NULL,
	Fill_In_Type         VARCHAR(100) NULL,
	Fill_In_Max_Size     SMALLINT NULL,
	Fill_In_Group_Number TINYINT NULL,
	Fill_In_Placeholder  VARCHAR(1000) NULL,
	Fill_In_Default_Data TEXT NULL,
	Fill_In_Display_Rows NUMERIC(2) NULL,
	Fill_In_For_Table    boolean NOT NULL DEFAULT 0,
	Fill_In_Heading      VARCHAR(80) NULL,
	PRIMARY KEY (Stg2_Clause_Fillins_Id)
);

CREATE INDEX XIF1Stg2_Clause_Fill_Ins ON Stg2_Clause_Fill_Ins
(
	Stg1_Official_Clause_Id
);

ALTER TABLE Stg2_Clause_Fill_Ins
ADD FOREIGN KEY R_Stg2_Official_Clause_Stg2_Clause_Fill_Ins (Stg1_Official_Clause_Id) REFERENCES Stg1_Official_Clause (Stg1_Official_Clause_Id)
		ON DELETE CASCADE;

-- ==============================================
CREATE TABLE Stg1_Official_Clause_Alt
(
	Stg1_Official_Clause_Alt_Id INTEGER AUTO_INCREMENT,
	Clause_Number        VARCHAR(100) NULL,
	Alt_Number           VARCHAR(100) NULL,
	Instructions         TEXT NULL,
	Content              TEXT NULL,
	Content_HTML         TEXT NULL,
	Content_Html_FillIns TEXT NULL,
	Content_Html_Processed TEXT NULL,
	Src_Doc_Id           SMALLINT NOT NULL,
	Manually_Added       boolean NOT NULL DEFAULT 0,
	IsEditAndFillIn 	 SMALLINT NOT NULL DEFAULT 0,
	PRIMARY KEY (Stg1_Official_Clause_Alt_Id)
);

CREATE INDEX XIE1Stg1_Official_Clause_Alt_Number ON Stg1_Official_Clause_Alt
(
	Src_Doc_Id,
	Clause_Number,
	Alt_Number
);

CREATE INDEX XIF1Stg1_Official_Clause_Alt ON Stg1_Official_Clause_Alt
(
	Src_Doc_Id
);

ALTER TABLE Stg1_Official_Clause_Alt
ADD FOREIGN KEY R_Stg_Src_Document_Stg1_Official_Clause_Alt (Src_Doc_Id) REFERENCES Stg_Src_Document (Src_Doc_Id)
		ON DELETE CASCADE;

-- ==============================================
CREATE TABLE Stg2_Clause_Fill_Ins_Alt
(
	Stg2_Clause_Fillins_Alt_Id INTEGER AUTO_INCREMENT,
	Stg1_Official_Clause_Alt_Id INTEGER NOT NULL,
	Fill_In_Code         VARCHAR(200) NULL,
	Fill_In_Type         VARCHAR(100) NULL,
	Fill_In_Max_Size     SMALLINT NULL,
	Fill_In_Group_Number TINYINT NULL,
	Fill_In_Placeholder  VARCHAR(1000) NULL,
	Fill_In_Default_Data TEXT NULL,
	Fill_In_Display_Rows NUMERIC(2) NULL,
	Fill_In_For_Table    boolean NOT NULL DEFAULT 0,
	Fill_In_Heading      VARCHAR(80) NULL,
	PRIMARY KEY (Stg2_Clause_Fillins_Alt_Id)
);

CREATE INDEX XIF1Stg2_Clause_Fill_Ins_Alt ON Stg2_Clause_Fill_Ins_Alt
(
	Stg1_Official_Clause_Alt_Id
);

ALTER TABLE Stg2_Clause_Fill_Ins_Alt
ADD FOREIGN KEY R_Stg1_Official_Clause_Alt_Stg2_Clause_Fill_Ins_Alt (Stg1_Official_Clause_Alt_Id) REFERENCES Stg1_Official_Clause_Alt (Stg1_Official_Clause_Alt_Id)
		ON DELETE CASCADE;

-- ==============================================
CREATE TABLE Stg2_Answers_Prescriptions_Xref
(
	Stg2_Answer_Prescription_Xref_Id INTEGER AUTO_INCREMENT,
	Stg1_Question_Id     INTEGER NOT NULL,
	Prescription         VARCHAR(100) NULL,
	PRIMARY KEY (Stg2_Answer_Prescription_Xref_Id)
);

CREATE INDEX XIF1Stg2_Answers_Prescription_Xref ON Stg2_Answers_Prescriptions_Xref
(
	Stg1_Question_Id
);

ALTER TABLE Stg2_Answers_Prescriptions_Xref
ADD FOREIGN KEY R_Stg1_Question_Stg2_Answers_Prescriptions_Xref (Stg1_Question_Id) REFERENCES Stg1_Question (Stg1_Question_Id)
		ON DELETE CASCADE;

-- ==============================================
CREATE TABLE Stg1_Raw_Clauses
(
	Stg1_Clause_Id       INTEGER AUTO_INCREMENT,
	Section              VARCHAR(10) NULL,
	IBR_Or_Full_Text     VARCHAR(200) NULL,
	Fill_In              VARCHAR(200) NULL,
	Clause               TEXT NULL,
	Effective_Clause_Date VARCHAR(100) NULL,
	Clause_Prescription  VARCHAR(1000) NULL,
	Prescription_Text    TEXT NULL,
	Conditions           TEXT NULL,
	Rule                 VARCHAR(2000) NULL,
	Presc_Or_Clause      VARCHAR(100) NULL,
	Clause_Number        VARCHAR(100) NULL,
	Additional_Conditions VARCHAR(2000) NULL,
	Src_Doc_Id           SMALLINT NOT NULL,
	Sheet_Number         SMALLINT NOT NULL,
	Row_Number           SMALLINT NOT NULL,
	Is_Dfars_Activity 	boolean NOT NULL DEFAULT 0,
	Editable             VARCHAR(2000) NULL,
	Required_Or_Optional VARCHAR(2000) NULL,
	Provision_Or_Clause  VARCHAR(1) NULL,
	Commercial_Status    VARCHAR(50) NULL,
	Optional_Status      VARCHAR(50) NULL,
	Optional_Conditions  VARCHAR(250) NULL,
	Official_Clause_Title  VARCHAR(500) NULL,
	PRIMARY KEY (Stg1_Clause_Id)
);

CREATE INDEX XIF1Staging_1_Raw_Clauses ON Stg1_Raw_Clauses
(
	Src_Doc_Id
);

ALTER TABLE Stg1_Raw_Clauses
ADD FOREIGN KEY R_Stg_Src_Document_Stg1_Raw_Clauses (Src_Doc_Id) REFERENCES Stg_Src_Document (Src_Doc_Id)
		ON DELETE CASCADE;

-- ==============================================
CREATE TABLE Stg2_Clause_Prescriptions_Xref
(
	Stg2_Clause_Prescrption_Xref_Id INTEGER AUTO_INCREMENT,
	Stg1_Clause_Id       INTEGER NOT NULL,
	Prescription         VARCHAR(100) NOT NULL,
	PRIMARY KEY (Stg2_Clause_Prescrption_Xref_Id)
);

CREATE INDEX XIF1Stg2_Clause_Prescriptions_Xref ON Stg2_Clause_Prescriptions_Xref
(
	Stg1_Clause_Id
);

ALTER TABLE Stg2_Clause_Prescriptions_Xref
ADD FOREIGN KEY R_Stg1_Raw_Clauses_Stg2_Clause_Prescriptions_Xref (Stg1_Clause_Id) REFERENCES Stg1_Raw_Clauses (Stg1_Clause_Id)
		ON DELETE CASCADE;

-- ==============================================
CREATE TABLE Stg2_Clause_Conditions
(
	Stg2_Clause_Condition_Id INTEGER AUTO_INCREMENT,
	Stg1_Clause_Id       INTEGER NOT NULL,
	Condition_Text       TEXT NULL,
	Condition_Text_Processed TEXT NULL,
	Question_Name        VARCHAR(500) NULL,
	Value                VARCHAR(500) NULL,
	Item_Id              VARCHAR(100) NULL,
	Real_Question_Name   VARCHAR(200) NULL,
	Real_Question_Id     INTEGER NULL,
	Operator             VARCHAR(10) NULL,
	Real_Value           VARCHAR(500) NULL,
	PRIMARY KEY (Stg2_Clause_Condition_Id)
);

CREATE INDEX XIF1Stg2_Clause_Conditions ON Stg2_Clause_Conditions
(
	Stg1_Clause_Id
);

ALTER TABLE Stg2_Clause_Conditions
ADD FOREIGN KEY R_Stg1_Raw_Clauses_Stg2_Clause_Conditions (Stg1_Clause_Id) REFERENCES Stg1_Raw_Clauses (Stg1_Clause_Id)
		ON DELETE CASCADE;

-- ==============================================
CREATE TABLE Stg_Official_Clause_Add
(
	Stg_Official_Clause_Add_Id SMALLINT AUTO_INCREMENT,
	Clause_Number        VARCHAR(100) NULL,
	Title                VARCHAR(500) NULL,
	Prescription         VARCHAR(100) NULL,
	URI                  VARCHAR(500) NULL,
	Content              TEXT NULL,
	Content_HTML         TEXT NULL,
	Content_Html_FillIns TEXT NULL,
	Content_Html_Processed TEXT NULL,
	Created_At           DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY (Stg_Official_Clause_Add_Id)
);

CREATE INDEX XIEStg_Official_Clause_Add_Number ON Stg_Official_Clause_Add
(
	Clause_Number
);

-- ==============================================
CREATE TABLE Stg_Official_Clause_Alt_Add
(
	Stg_Official_Clause_Alt_Add_Id SMALLINT AUTO_INCREMENT,
	Clause_Number        VARCHAR(100) NULL,
	Alt_Number           VARCHAR(100) NULL,
	Instructions         TEXT NULL,
	Content              TEXT NULL,
	Content_HTML         TEXT NULL,
	Content_Html_FillIns TEXT NULL,
	Content_Html_Processed TEXT NULL,
	Created_At           DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY (Stg_Official_Clause_Alt_Add_Id)
);

CREATE INDEX XIE1Stg_Official_Clause_Alt_Add_Number ON Stg_Official_Clause_Alt_Add
(
	Clause_Number,
	Alt_Number
);

-- ==============================================
CREATE TABLE Stg_Clause_Fill_Ins_Add
(
	Stg_Clause_Fill_Ins_Add_Id SMALLINT AUTO_INCREMENT,
	Clause_Number        VARCHAR(100) NULL,
	URI                  VARCHAR(500) NULL,
	Content_HTML         TEXT NULL,
	Content_Html_FillIns TEXT NULL,
	Content_Html_Processed TEXT NULL,
	Created_At           DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY (Stg_Clause_Fill_Ins_Add_Id)
);

CREATE INDEX XIEStg_Official_Clause_Add_Number ON Stg_Clause_Fill_Ins_Add
(
	Clause_Number
);

-- ==============================================
CREATE TABLE Stg_Versioning
(
	Stg_Versioning_Id    SMALLINT AUTO_INCREMENT,
	Question_Src_Doc_Id  SMALLINT NULL,
	Clause_Src_Doc_Id    SMALLINT NULL,
	ECFR_Src_Doc_Id      SMALLINT NULL,
	Clause_Version_Id    SMALLINT NULL,
	Sql_Script_File_Name VARCHAR(80) NULL,
	Updated_At           DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (Stg_Versioning_Id)
);

ALTER TABLE Stg_Versioning
ADD FOREIGN KEY R_Stg_Src_Document_Stg_Versioning_Question (Question_Src_Doc_Id) REFERENCES Stg_Src_Document (Src_Doc_Id);

ALTER TABLE Stg_Versioning
ADD FOREIGN KEY R_Stg_Src_Document_Stg_Versioning_Clause (Clause_Src_Doc_Id) REFERENCES Stg_Src_Document (Src_Doc_Id);

ALTER TABLE Stg_Versioning
ADD FOREIGN KEY R_Stg_Src_Document_Stg_Versioning_EFCR (ECFR_Src_Doc_Id) REFERENCES Stg_Src_Document (Src_Doc_Id);

ALTER TABLE Stg_Versioning
ADD FOREIGN KEY R_Clause_Versions_Stg_Versioning (Clause_Version_Id) REFERENCES Clause_Versions (Clause_Version_Id)
		ON DELETE CASCADE;

-- =====================================================================
CREATE TABLE Stg3_Clause_Corrections
(
	Stg3_Clause_Id       integer AUTO_INCREMENT,
	Clause_Version_Id    SMALLINT NOT NULL,
	Clause_Name          varchar(50) NOT NULL,
	Clause_Data          text NOT NULL,
	Created_At           TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	Updated_At           TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	PRIMARY KEY (Stg3_Clause_Id)
);

CREATE UNIQUE INDEX XAK1Stg3_Clause_Corrections ON Stg3_Clause_Corrections
(
	Clause_Version_Id,
	Clause_Name
);

CREATE INDEX XIF1Stg3_Clause_Corrections ON Stg3_Clause_Corrections
(
	Clause_Version_Id
);

ALTER TABLE Stg3_Clause_Corrections
ADD FOREIGN KEY R_Clause_Versions_Stg3_Clause_Corrections (Clause_Version_Id) REFERENCES Clause_Versions (Clause_Version_Id)
		ON DELETE CASCADE;

-- =====================================================================
CREATE TABLE Stg3_Clause_Fill_In_Corrections
(
	Stg3_Clause_Fill_In_Id integer AUTO_INCREMENT,
	Stg3_Clause_Id       integer NOT NULL,
	Previous_Fill_In_Code VARCHAR(50) NULL,
	Fill_In_Code         varchar(50) NOT NULL,
	Fill_In_Type         VARCHAR(1) NOT NULL,
	Fill_In_Max_Size     SMALLINT NULL,
	Fill_In_Group_Number TINYINT NULL,
	Fill_In_Placeholder  VARCHAR(1000) NULL,
	Fill_In_Default_Data VARCHAR(1000) NULL,
	Fill_In_Display_Rows NUMERIC(2) NULL,
	Fill_In_For_Table    boolean NOT NULL DEFAULT 0,
	Fill_In_Heading      VARCHAR(80) NULL,
	Created_At           TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	Updated_At           TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	PRIMARY KEY (Stg3_Clause_Fill_In_Id)
);

CREATE INDEX XIE1Stg3_Clause_Fill_In_Corrections ON Stg3_Clause_Fill_In_Corrections
(
	Stg3_Clause_Id,
	Fill_In_Code,
	Fill_In_Group_Number
);

CREATE INDEX XIFClause_Fill_Ins_Clause_Id ON Stg3_Clause_Fill_In_Corrections
(
	Stg3_Clause_Id
);

ALTER TABLE Stg3_Clause_Fill_In_Corrections
ADD FOREIGN KEY R_Stg3_Clause_Fill_In_Corrections (Stg3_Clause_Id) REFERENCES Stg3_Clause_Corrections (Stg3_Clause_Id)
		ON DELETE CASCADE;
		
CREATE TABLE Stg2_Clause_Question_Conditions (
  Stg2_Clause_Question_Condition_Id INTEGER NOT NULL AUTO_INCREMENT,
  Stg1_Clause_Id INTEGER NOT NULL,
  Stg2_Question_Id INTEGER NOT NULL DEFAULT 0,
  Stg2_Possible_Answer_Id INTEGER NOT NULL DEFAULT 0,
  Condition_Text TEXT,
  Question_Name VARCHAR(500) DEFAULT NULL,
  Question_Answer VARCHAR(500) DEFAULT NULL,
  ChoicePrescriptionCnt INTEGER NOT NULL DEFAULT 0,
  PRIMARY KEY (Stg2_Clause_Question_Condition_Id));
  
