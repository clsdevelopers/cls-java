ALTER TABLE Stg2_Clause_Fill_Ins 
CHANGE COLUMN Fill_In_Heading Fill_In_Heading VARCHAR(80) NULL DEFAULT NULL ;

ALTER TABLE Stg2_Clause_Fill_Ins_Alt 
CHANGE COLUMN Fill_In_Heading Fill_In_Heading VARCHAR(80) NULL DEFAULT NULL ;

ALTER TABLE Stg3_Clause_Fill_In_Corrections 
CHANGE COLUMN Fill_In_Heading Fill_In_Heading VARCHAR(80) NULL DEFAULT NULL ;
