ALTER TABLE stg_versioning 
DROP FOREIGN KEY stg_versioning_ibfk_1,
DROP FOREIGN KEY stg_versioning_ibfk_2,
DROP FOREIGN KEY stg_versioning_ibfk_3,
DROP FOREIGN KEY stg_versioning_ibfk_4;

ALTER TABLE stg_versioning
ADD CONSTRAINT stg_versioning_ibfk_1
  FOREIGN KEY (Question_Src_Doc_Id)
  REFERENCES stg_src_document (Src_Doc_Id),
ADD CONSTRAINT stg_versioning_ibfk_2
  FOREIGN KEY (Clause_Src_Doc_Id)
  REFERENCES stg_src_document (Src_Doc_Id),
ADD CONSTRAINT stg_versioning_ibfk_3
  FOREIGN KEY (ECFR_Src_Doc_Id)
  REFERENCES stg_src_document (Src_Doc_Id),
ADD CONSTRAINT stg_versioning_ibfk_4
  FOREIGN KEY (Clause_Version_Id)
  REFERENCES clause_versions (Clause_Version_Id)
  ON DELETE CASCADE;
