ALTER TABLE Stg2_Clause_Fill_Ins
ADD COLUMN Fill_In_Group_Number TINYINT NULL,
ADD COLUMN Fill_In_Placeholder  VARCHAR(80) NULL,
ADD COLUMN Fill_In_Default_Data TEXT NULL,
ADD COLUMN Fill_In_Display_Rows NUMERIC(2) NULL,
ADD COLUMN Fill_In_For_Table    boolean NOT NULL DEFAULT 0,
ADD COLUMN Fill_In_Heading      VARCHAR(40) NULL;

ALTER TABLE Stg2_Clause_Fill_Ins_Alt
ADD COLUMN Fill_In_Group_Number TINYINT NULL,
ADD COLUMN Fill_In_Placeholder  VARCHAR(80) NULL,
ADD COLUMN Fill_In_Default_Data TEXT NULL,
ADD COLUMN Fill_In_Display_Rows NUMERIC(2) NULL,
ADD COLUMN Fill_In_For_Table    boolean NOT NULL DEFAULT 0,
ADD COLUMN Fill_In_Heading      VARCHAR(40) NULL;
