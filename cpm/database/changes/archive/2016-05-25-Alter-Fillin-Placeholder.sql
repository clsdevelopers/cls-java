ALTER TABLE Stg2_Clause_Fill_Ins
CHANGE COLUMN Fill_In_Placeholder Fill_In_Placeholder VARCHAR(1000) NULL DEFAULT NULL ;

ALTER TABLE Stg2_Clause_Fill_Ins_Alt
CHANGE COLUMN Fill_In_Placeholder Fill_In_Placeholder VARCHAR(1000) NULL DEFAULT NULL ;

ALTER TABLE Stg3_Clause_Fill_In_Corrections
CHANGE COLUMN Fill_In_Placeholder Fill_In_Placeholder VARCHAR(1000) NULL DEFAULT NULL ;



