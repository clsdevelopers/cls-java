CREATE TABLE Stg2_Clause_Question_Conditions (
  Stg2_Clause_Question_Condition_Id INTEGER NOT NULL AUTO_INCREMENT,
  Stg1_Clause_Id INTEGER NOT NULL,
  Stg2_Question_Id INTEGER NOT NULL DEFAULT 0,
  Stg2_Possible_Answer_Id INTEGER NOT NULL DEFAULT 0,
  Condition_Text TEXT,
  Question_Name VARCHAR(500) DEFAULT NULL,
  Question_Answer VARCHAR(500) DEFAULT NULL,
  ChoicePrescriptionCnt INTEGER NOT NULL DEFAULT 0,
  PRIMARY KEY (Stg2_Clause_Question_Condition_Id));