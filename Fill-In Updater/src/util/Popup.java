package util;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.function.Consumer;

import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

public class Popup {
	public static void popErrorWindow(Consumer<PrintWriter> consumer){
		StringWriter strw = new StringWriter();
		PrintWriter writer = new PrintWriter(strw);
		consumer.accept(writer);
		writer.flush();
		
		JTextArea area = new JTextArea();
		area.setText(strw.toString());
		area.setEditable(false);
		JScrollPane scrollpane = new JScrollPane(area);
		
		JOptionPane.showMessageDialog(null, scrollpane, "Error", JOptionPane.ERROR_MESSAGE);
	}
}
