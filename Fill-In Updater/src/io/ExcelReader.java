package io;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import autoUpdate.ClauseLibrary;
import autoUpdate.Fillin;

public class ExcelReader {
	private static final String NO_FILLIN_DATA_LABEL = "No fillin data: ";
	
	private static final List<String> IGNORED_TAGS = Arrays.asList(new String[]{Fillin.COL_NEW_TEMP_FILLIN_SIZE, Fillin.COL_NEW_TEMP_FILLIN_TYPE});

	private File inputFile;
	
	public ExcelReader (File inputFile) {
		this.inputFile = inputFile;
	}

	public ClauseLibrary load() throws IOException {
		System.out.println("\nExcelReader start of load");
		FileInputStream fileInputStream = null;
		XSSFWorkbook workbook = null;
		ClauseLibrary library = new ClauseLibrary();
		try {
			fileInputStream = new FileInputStream(this.inputFile);
			workbook = new XSSFWorkbook(fileInputStream);
			for (int iSheet = 0; iSheet < workbook.getNumberOfSheets(); iSheet++ ) {
				this.readCorrectionExcel(workbook, iSheet, library);
			}
		}
		finally {
			if (workbook != null)
				workbook.close();
			if (fileInputStream != null)
				try {
					fileInputStream.close();
				} catch (Exception oIgnore) {
					oIgnore.printStackTrace();
				}	
		}
		System.out.println("ExcelReader end of load\n");
		library.sortFillins();
		return library;
	}
	
	private int clauseNameColNumber = -1;
	private int oldFillinCodeColNum = -1;
	private int newFillinCodeColNum = -1;
	private HashMap<String, Integer> newColMap;
	private HashMap<String, Integer> oldColMap;

	private ClauseLibrary readCorrectionExcel(XSSFWorkbook workbook, int iSheet, ClauseLibrary library) throws IOException {
			XSSFSheet sheet = workbook.getSheetAt(iSheet);
			//Get iterator to all the rows in current sheet
			Iterator<Row> rowIterator = sheet.iterator();
			
			if (!rowIterator.hasNext()) {
				return null;
			}
			
			//let's start off by recording the column headers, and their corresponding column number
			newColMap = new HashMap<>();
			oldColMap = new HashMap<>();
			Row row = rowIterator.next();
			for (int iCell = row.getFirstCellNum(); iCell < row.getLastCellNum(); iCell++) {
				Cell oCell = row.getCell(iCell);
				String value = oCell.getStringCellValue();
				if(!IGNORED_TAGS.contains(value)){
					if(value.equals(Fillin.COL_CLAUSE_NAME)) {
						clauseNameColNumber = iCell;
					}else if(value.startsWith(Fillin.OLD_PREFIX)) {
						oldColMap.put(value, iCell);
					} else {
						newColMap.put(value, iCell);
					}
					if(value.equals(Fillin.COL_OLD_FILLIN_CODE)) oldFillinCodeColNum = iCell;
					if(value.equals(Fillin.COL_NEW_TEMP_FILLIN_CODE)) newFillinCodeColNum = iCell;
				}
			}
			
			int rowsRead = 1;
			while (rowIterator.hasNext()) {
	            row = rowIterator.next();
	            Fillin newFillin = readNew(row);
	            if(newFillin!=null) library.addFillin(newFillin);
	            Fillin oldFillin = readOld(row);
	            if(oldFillin != null) library.addFillin(oldFillin);
	            rowsRead++;
			}
			System.out.println("ExcelReader total rows read from Excel: "+rowsRead);
			return library;
	}
	
	private Fillin readOld(Row row){
		//first we need to check that this row actually contains a valid old clause
		String clauseName = getCellValue(row, this.clauseNameColNumber);
		if(clauseName.equals("") || clauseName.startsWith(NO_FILLIN_DATA_LABEL)) return null;
		if(getCellValue(row, this.oldFillinCodeColNum).equals("")) return null;
		Fillin fillin = new Fillin(clauseName, true);
		readFillin(row, fillin, oldColMap);
		return fillin;
	}
	
	private Fillin readNew(Row row){
		//first we need to check that this row actually contains a valid old clause
		String clauseName = getCellValue(row, this.clauseNameColNumber);
		if(clauseName.equals("") || clauseName.startsWith(NO_FILLIN_DATA_LABEL)) return null;
		if(getCellValue(row, this.newFillinCodeColNum).equals("")) return null;
		Fillin fillin = new Fillin(clauseName, false);
		readFillin(row, fillin, newColMap);
		return fillin;
	}

	private static void readFillin(Row row, Fillin fillin, HashMap<String, Integer> colMap) {
		HashMap<String, Fillin.Property> props = fillin.getProperties();
		colMap.forEach((String key, Integer col) -> {props.put(key, new Fillin.Property(getCellValue(row, col), row.getRowNum(), col));}); //java 8
	}
	
	
	
	private static String getCellValue(Row row, int iColumn) {
		if (iColumn < 0)
			return "";
		Cell cell = row.getCell(iColumn);
		if (cell == null || cell.getCellType() == Cell.CELL_TYPE_BLANK) {
			   return "";
		}
		if(cell.getCellType() == Cell.CELL_TYPE_NUMERIC)
			return cell.getNumericCellValue() + "";
		return cell.getStringCellValue();
	}
	
	public static void main(String... args){
		ExcelReader reader = new ExcelReader(new File("C:\\Users\\smiller\\Downloads\\clause_corrections\\Fillin_Corrections.xlsx"));
		try {
			System.out.println(reader.load());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}