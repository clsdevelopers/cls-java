package io;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Pattern;

import autoUpdate.ClauseLibrary;
import autoUpdate.ClauseSet;
import autoUpdate.Fillin;

public class HtmlData {
	private HashMap<String, String> newData, oldData;
	
	public HtmlData(){
		newData = new HashMap<>();
		oldData = new HashMap<>();
	}
	
	public void loadAllClauses(ClauseLibrary lib, File workingDir){
		for(ClauseSet set : lib.getAllClauses()){
			loadClause(set, workingDir);
		}
	}
	
	public void loadClause(ClauseSet set, File workingDir){
		String html = null;
		try{
			load(workingDir, newData, set.getName(), "NEW_", ".html");
			load(workingDir, oldData, set.getName(), "OLD_", ".html");
			//if any fillins have a custom code, we change it back so that we can always find it
			//when we go to save, we will change it to the custom code
			html = newData.get(set.getName());
			for(Fillin f : set.getNewFillins()){
				String newName = f.getProperty(Fillin.COL_NEW_CUSTOM_FILLIN_CODE);
				if(newName != null && !newName.isEmpty()){
					html = html.replaceAll(quote(newName), bracket(f.getFillinCode()));
				}
			}
		} catch (IOException e){
			//NOP?
		}
		newData.put(set.getName(), html);
	}
	
	public String getNewFormattedHtml(String clause, String curFillin, List<Fillin> newFillins){
		String html = newData.get(clause);
		if(html == null) return null;
		return HtmlFormatter.formatNewHtml(html, curFillin, newFillins);
	}
	
	public String getOldFormattedHtml(String clause, String curMatch, List<Fillin> oldFillins, List<String> matches){
		String html = oldData.get(clause);
		if(html == null) return null;
		return HtmlFormatter.formatOldHtml(html, curMatch, oldFillins, matches);
	}
	
	public void saveAll(File workingDir, ClauseLibrary lib) throws IOException{
		for(ClauseSet set : lib.getAllClauses()){
			saveClause(workingDir, set);
		}
	}
	
	public void saveClause(File workingDir, ClauseSet clauseSet) throws IOException {
		File folder = new File(workingDir, clauseSet.getName() + File.separator);
		if(!folder.exists())folder.mkdirs();
		saveOldHtml(folder, clauseSet.getName());
		String html = newData.get(clauseSet.getName());
		if(html == null) return;
		for(Fillin f : clauseSet.getNewFillins()){
			String newName = f.getProperty(Fillin.COL_NEW_CUSTOM_FILLIN_CODE);
			if(newName != null && !newName.isEmpty()){
				html = html.replaceAll(quote(f.getFillinCode()), bracket(newName));
			}
		}
		
		File clauseFile = new File(folder, "NEW_"+clauseSet.getName()+".html");
		Files.write(clauseFile.toPath(), html.getBytes());
	}
	
	private void saveOldHtml(File folder, String clauseName) throws IOException{
		File clauseFile = new File(folder, "OLD_" + clauseName + ".html");
		String html = oldData.get(clauseName);
		if(html != null) Files.write(clauseFile.toPath(), html.getBytes());
	}
	
	private static void load(File workingDir, HashMap<String, String> data, String clause, String prefix, String suffix) throws IOException{
		String html = new String(Files.readAllBytes(Paths.get(new File(workingDir, clause + File.separator + prefix + clause + suffix).toURI())));
		data.put(clause, html);
	}
	
	private static String quote(String name){
		return Pattern.quote(bracket(name));
	}
	
	private static String bracket(String name){
		return "{{"+name+"}}";
	}
}
