package io;

import java.util.List;
import java.util.regex.Pattern;

import autoUpdate.Fillin;

public class HtmlFormatter {
	private static final String FORMAT_SELECTED_FILLIN = "<span style=\"background-color:#C0C0C0\"><b>%1$s</b></span>";
	private static final String FORMAT_NOT_MATCHED = "<font color=\"red\">%1$s</font>";
	private static final String FORMAT_MATCHED = "<font color=\"green\">%1$s</font>";
	
	private static final String FORMAT_RENAME = "<i>%1$s</i>";

	public static String formatNewHtml(String html, String curFillin, List<Fillin> newFillins) {
		int fillinNumber = 0;
		for(Fillin cur : newFillins){
			String name = cur.getFillinCode();
			String matchCode = cur.getMatchCode();
			
			html = html.replaceAll(quote(name), "<a href=\""+fillinNumber+"\">"+bracket(name)+"</a>");
			if(name.equals(curFillin)){
				html = html.replaceAll(quote(name), String.format(FORMAT_SELECTED_FILLIN, bracket(name)));
			}
			if(matchCode == null || matchCode.isEmpty()){
				html = html.replaceAll(quote(name), String.format(FORMAT_NOT_MATCHED, bracket(name)));
			} else {
				html = html.replaceAll(quote(name), String.format(FORMAT_MATCHED, bracket(name)));
			}
			
			String customCode = cur.getProperty(Fillin.COL_NEW_CUSTOM_FILLIN_CODE);
			if(customCode != null && !customCode.isEmpty()){
				html = html.replaceAll(quote(name), String.format(FORMAT_RENAME, bracket(customCode)));
			}
			
			fillinNumber++;
		}
		return html;
	}

	public static String formatOldHtml(String html, String curMatch, List<Fillin> oldFillins, List<String> matches) {
		for(Fillin cur : oldFillins){
			String name = cur.getFillinCode();
			if(curMatch != null && name.equals(curMatch)){
				html = html.replaceAll(quote(name), String.format(FORMAT_SELECTED_FILLIN, bracket(name)));
			}
			if(matches.contains(name)){
				html = html.replaceAll(quote(name), String.format(FORMAT_MATCHED, bracket(name)));
			} else {
				html = html.replaceAll(quote(name), String.format(FORMAT_NOT_MATCHED, bracket(name)));
			}
			html = html.replaceAll(quote(name), String.format("<a href=\"%2$s\">%1$s</a>", bracket(name), name));
		}
		
		return html;
	}
	
	private static String quote(String name){
		return Pattern.quote(bracket(name));
	}
	
	private static String bracket(String name){
		return "{{"+name+"}}";
	}
}
