package io;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.Set;

import javax.swing.JOptionPane;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

import autoUpdate.ClauseLibrary;
import autoUpdate.ClauseSet;
import autoUpdate.Fillin;

public class ExcelWriter {
	public static void write(File outputFile, File inputFile, ClauseLibrary library) throws IOException, InvalidFormatException{
		Workbook workbook = null;
		FileOutputStream fos = null;
		try{
			workbook = WorkbookFactory.create(new FileInputStream(inputFile));
			Sheet sheet = workbook.getSheetAt(0);
			for(ClauseSet cs : library.getAllClauses()){
				for(Fillin f : cs.getNewFillins()){
					//write properties
					for(String propName : f.getProperties().keySet()){
						Fillin.Property prop = f.getProperties().get(propName);
						System.out.println("storing " + propName + "="+prop.getValue()+" at row:"+prop.getRow()+" col:"+prop.getCol());
						Row row = sheet.getRow(prop.getRow());
						Cell cell = row.getCell(prop.getCol());
						if(cell == null) cell = row.createCell(prop.getCol());
						System.out.println(cell);
						String value = prop.getValue();
						if(value == null || value.isEmpty()) value = null;
						cell.setCellValue(value);
					}
				}
			}
			fos = new FileOutputStream(outputFile);
			workbook.write(fos);
		}catch(FileNotFoundException e){
			JOptionPane.showMessageDialog(null, "Could not save file:\n"+e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
		} finally {
			if(workbook!=null) workbook.close();
			if(fos != null) fos.close();
		}
		Fillin.unsavedChanges = false;
	}
	
	public static void copyFillinProperties(ClauseLibrary lib){
		for(ClauseSet set : lib.getAllClauses()){
			List<Fillin> oldFillins = set.getOldFillins();
			List<Fillin> newFillins = set.getNewFillins();
			
			for(Fillin cur : newFillins){
				String matchCode = cur.getMatchCode();
				if(matchCode != null && !matchCode.isEmpty()){
					Fillin oldFillin = findByName(oldFillins, matchCode);
					copyProps(cur, oldFillin);
				}
			}
		}
	}
	
	private static void copyProps(Fillin newFillin, Fillin oldFillin){
		Set<String> newProps = newFillin.getProperties().keySet();
		Set<String> oldProps = oldFillin.getProperties().keySet();
		for(String prop : newProps){
			String oldProp = Fillin.OLD_PREFIX + prop;
			if(oldProps.contains(oldProp) && newFillin.getProperty(prop).isEmpty()){
				newFillin.setProperty(prop, oldFillin.getProperty(oldProp));
			}
		}
	}
	
	private static Fillin findByName(List<Fillin> list, String name){
		for(Fillin cur : list){
			if(cur.getFillinCode().equals(name))return cur;
		}
		return null;
	}
}
