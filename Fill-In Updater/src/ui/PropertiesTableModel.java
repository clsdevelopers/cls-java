package ui;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.ListIterator;

import javax.swing.table.AbstractTableModel;

import autoUpdate.Fillin;

@SuppressWarnings("serial")
public class PropertiesTableModel extends AbstractTableModel {
	
	private static final List<String> IGNORED_KEYS = Arrays.asList(new String[]{
			Fillin.COL_CLAUSE_NAME, Fillin.COL_MATCH_CODE, Fillin.COL_OLD_FILLIN_CODE, Fillin.COL_NEW_TEMP_FILLIN_CODE, Fillin.COL_NEW_CUSTOM_FILLIN_CODE
	});
	
	private Fillin fillin;
	
	private List<String> keys;
	
	public PropertiesTableModel(Fillin f){
		fillin = f;
		generateData();
	}
	
	private void generateData(){
		if(fillin == null) return;
		keys = new ArrayList<>(fillin.getProperties().keySet());
		ListIterator<String> iterator = keys.listIterator();
		while(iterator.hasNext()){
			String cur = iterator.next();
			if(IGNORED_KEYS.contains(cur)) iterator.remove();
		}
		Collections.sort(keys);
	}

	@Override
	public int getColumnCount() {
		if(fillin == null) return 0;
		return 2;
	}

	@Override
	public int getRowCount() {
		if(fillin == null) return 0;
		return keys.size();
	}

	@Override
	public Object getValueAt(int row, int col) {
		if(fillin == null) return null;
		if(col == 0){
			return keys.get(row);
		} else {
			return fillin.getProperty(keys.get(row));
		}
	}
	
	@Override
	public void setValueAt(Object aValue, int row, int col) {
		if(fillin == null) return;
		fillin.setProperty(keys.get(row), aValue.toString());
	}
	
	@Override
	public boolean isCellEditable(int row, int col){
		return col != 0 && !fillin.isOld();
	}
	
	@Override
	public String getColumnName(int column) {
		return column==0 ? "Property" : "Value";
	}

}
