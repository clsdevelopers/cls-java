package ui;



import java.awt.Component;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.util.List;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JEditorPane;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;

import autoUpdate.Fillin;
import autoUpdate.ProcessController;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.KeyStroke;
import java.awt.event.KeyEvent;
import java.awt.event.WindowEvent;
import java.awt.event.InputEvent;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.border.TitledBorder;
import javax.swing.text.DefaultCaret;
import javax.swing.border.EtchedBorder;
import javax.swing.JCheckBox;
import javax.swing.JTextField;
import java.awt.GridLayout;
import java.awt.Color;
import java.awt.BorderLayout;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.event.WindowAdapter;

public class MainWindow {

	private static final String CLAUSE_FAILED_TO_LOAD_MESSAGE = "<html><h1>[Clause HTML failed to load]</h1></html>";
	private JFrame frame;
	private JTable newPropertiesTable;
	private JTable oldPropertiesTable;
	private JComboBox<String> comboBox;
	private JButton btnPreviousFillin;
	private JButton btnNextFillin;
	private JEditorPane txtpnnewClauseHtml;
	private JEditorPane txtpnoldClauseHtml;
	private JLabel lblDynCurClause;
	
	private ProcessController controller;
	private JButton btnPreviousClause;
	private JButton btnNextClause;
	private JScrollPane oldClauseScrollPane;
	private JScrollPane newClauseScrollPane;
	private JLabel lblDynCurFillin;
	private JCheckBox chkbtnNew;
	private JTextField customFillinCodeField;
	private JCheckBox chckbxAssignNewName;

	/**
	 * Create the application.
	 */
	public MainWindow(ProcessController controller) {
		this.controller = controller;
		initialize();
		frame.setLocationRelativeTo(null);
	}
	
	
	public void pop(){
		frame.getContentPane().validate();
		frame.setVisible(true);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent arg0) {
				if(checkForUnsavedChanges()) frame.dispose();
			}
		});
		frame.setTitle("Fill-In Updater");
		frame.setBounds(100, 100, 1153, 830);
		frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.rowHeights = new int[] {400, 223};
		gridBagLayout.columnWeights = new double[]{0.0};
		gridBagLayout.rowWeights = new double[]{1.0, 0.0};
		frame.getContentPane().setLayout(gridBagLayout);
		
		JPanel clausePanel = new JPanel();
		clausePanel.setPreferredSize(new Dimension(402, 502));
		GridBagConstraints gbc_clausePanel = new GridBagConstraints();
		gbc_clausePanel.weighty = 1.0;
		gbc_clausePanel.weightx = 1.0;
		gbc_clausePanel.fill = GridBagConstraints.BOTH;
		gbc_clausePanel.insets = new Insets(0, 0, 5, 0);
		gbc_clausePanel.gridx = 0;
		gbc_clausePanel.gridy = 0;
		frame.getContentPane().add(clausePanel, gbc_clausePanel);
		GridBagLayout gbl_clausePanel = new GridBagLayout();
		gbl_clausePanel.columnWidths = new int[]{1137};
		gbl_clausePanel.rowHeights = new int[]{32, 460};
		gbl_clausePanel.columnWeights = new double[]{0.0};
		gbl_clausePanel.rowWeights = new double[]{0.0, 1.0};
		clausePanel.setLayout(gbl_clausePanel);
		
		JPanel titleBarPanel = new JPanel();
		GridBagConstraints gbc_titleBarPanel = new GridBagConstraints();
		gbc_titleBarPanel.fill = GridBagConstraints.HORIZONTAL;
		gbc_titleBarPanel.insets = new Insets(0, 0, 5, 0);
		gbc_titleBarPanel.gridx = 0;
		gbc_titleBarPanel.gridy = 0;
		clausePanel.add(titleBarPanel, gbc_titleBarPanel);
		titleBarPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		titleBarPanel.setLayout(new BoxLayout(titleBarPanel, BoxLayout.X_AXIS));
		
		btnPreviousClause = new JButton("Previous Clause");
		btnPreviousClause.setMnemonic('r');
		titleBarPanel.add(btnPreviousClause);
		btnPreviousClause.addActionListener(controller.new PreviousClauseActionListener());
		
		Component horizontalGlue = Box.createHorizontalGlue();
		titleBarPanel.add(horizontalGlue);
		
		JLabel lblCurClause = new JLabel("Current Clause: ");
		titleBarPanel.add(lblCurClause);
		
		lblDynCurClause = new JLabel("<CLAUSE>");
		titleBarPanel.add(lblDynCurClause);
		
		Component horizontalGlue_1 = Box.createHorizontalGlue();
		titleBarPanel.add(horizontalGlue_1);
		
		btnNextClause = new JButton("Next Clause");
		btnNextClause.setMnemonic('e');
		titleBarPanel.add(btnNextClause);
		btnNextClause.addActionListener(controller.new NextClauseActionListener());
		
		JPanel htmlPanel = new JPanel();
		htmlPanel.setBorder(null);
		GridBagConstraints gbc_htmlPanel = new GridBagConstraints();
		gbc_htmlPanel.fill = GridBagConstraints.BOTH;
		gbc_htmlPanel.weightx = 1.0;
		gbc_htmlPanel.weighty = 1.0;
		gbc_htmlPanel.gridx = 0;
		gbc_htmlPanel.gridy = 1;
		clausePanel.add(htmlPanel, gbc_htmlPanel);
		htmlPanel.setLayout(new GridLayout(0, 2, 2, 0));
		
		txtpnnewClauseHtml = new JEditorPane();
		txtpnnewClauseHtml.setAutoscrolls(false);
		((DefaultCaret) txtpnnewClauseHtml.getCaret()).setUpdatePolicy(DefaultCaret.NEVER_UPDATE);
		txtpnnewClauseHtml.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
		txtpnnewClauseHtml.setContentType("text/html");
		txtpnnewClauseHtml.setEditable(false);
		txtpnnewClauseHtml.setText(CLAUSE_FAILED_TO_LOAD_MESSAGE);
		txtpnnewClauseHtml.addHyperlinkListener(controller.new HttpNewClauseSelectionListiner());
		newClauseScrollPane = new JScrollPane(txtpnnewClauseHtml);
		newClauseScrollPane.setBorder(new TitledBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null), "New Clause Text", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		htmlPanel.add(newClauseScrollPane);
		newClauseScrollPane.setViewportBorder(null);
		
		txtpnoldClauseHtml = new JEditorPane();
		txtpnoldClauseHtml.setAutoscrolls(false);
		((DefaultCaret) txtpnoldClauseHtml.getCaret()).setUpdatePolicy(DefaultCaret.NEVER_UPDATE);
		txtpnoldClauseHtml.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
		txtpnoldClauseHtml.setContentType("text/html");
		txtpnoldClauseHtml.setEditable(false);
		txtpnoldClauseHtml.setText(CLAUSE_FAILED_TO_LOAD_MESSAGE);
		txtpnoldClauseHtml.addHyperlinkListener(controller.new HttpOldClauseSelectionListiner());
		oldClauseScrollPane = new JScrollPane(txtpnoldClauseHtml);
		oldClauseScrollPane.setBorder(new TitledBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null), "Old Clause Text", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		htmlPanel.add(oldClauseScrollPane);
		oldClauseScrollPane.setViewportBorder(null);
		
		newClauseScrollPane.getVerticalScrollBar().addAdjustmentListener(new ScrollBarSynchronizer(oldClauseScrollPane.getVerticalScrollBar()));
		newClauseScrollPane.getHorizontalScrollBar().addAdjustmentListener(new ScrollBarSynchronizer(oldClauseScrollPane.getHorizontalScrollBar()));
		oldClauseScrollPane.getVerticalScrollBar().addAdjustmentListener(new ScrollBarSynchronizer(newClauseScrollPane.getVerticalScrollBar()));
		oldClauseScrollPane.getHorizontalScrollBar().addAdjustmentListener(new ScrollBarSynchronizer(newClauseScrollPane.getHorizontalScrollBar()));
		
		JPanel fillinPanel = new JPanel();
		fillinPanel.setBorder(new TitledBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null), "Fillin", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		GridBagConstraints gbc_fillinPanel = new GridBagConstraints();
		gbc_fillinPanel.anchor = GridBagConstraints.SOUTH;
		gbc_fillinPanel.weightx = 1.0;
		gbc_fillinPanel.fill = GridBagConstraints.HORIZONTAL;
		gbc_fillinPanel.gridx = 0;
		gbc_fillinPanel.gridy = 1;
		frame.getContentPane().add(fillinPanel, gbc_fillinPanel);
		GridBagLayout gbl_fillinPanel = new GridBagLayout();
		gbl_fillinPanel.rowHeights = new int[]{0, 0, 0, 142};
		gbl_fillinPanel.columnWeights = new double[]{0.0};
		gbl_fillinPanel.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0};
		fillinPanel.setLayout(gbl_fillinPanel);
		
		
		JPanel dividerPanel = new JPanel();
		GridBagConstraints gbc_dividerPanel = new GridBagConstraints();
		gbc_dividerPanel.weightx = 1.0;
		gbc_dividerPanel.fill = GridBagConstraints.HORIZONTAL;
		gbc_dividerPanel.insets = new Insets(0, 0, 5, 0);
		gbc_dividerPanel.gridx = 0;
		gbc_dividerPanel.gridy = 0;
		fillinPanel.add(dividerPanel, gbc_dividerPanel);
		dividerPanel.setBorder(new EmptyBorder(3, 3, 3, 3));
		dividerPanel.setLayout(new BoxLayout(dividerPanel, BoxLayout.X_AXIS));
		
		btnPreviousFillin = new JButton("Previous Fill-in");
		btnPreviousFillin.setMnemonic('p');
		dividerPanel.add(btnPreviousFillin);
		btnPreviousFillin.addActionListener(controller.new PreviousFillinActionListener());
		
		Component horizontalGlue_4 = Box.createHorizontalGlue();
		dividerPanel.add(horizontalGlue_4);
		
		JLabel lblCurFillin = new JLabel("Selected Fill-In: ");
		dividerPanel.add(lblCurFillin);
		
		lblDynCurFillin = new JLabel("<FILL-IN>");
		dividerPanel.add(lblDynCurFillin);
		
		Component horizontalGlue_3 = Box.createHorizontalGlue();
		dividerPanel.add(horizontalGlue_3);
		
		btnNextFillin = new JButton("Next Fill-in");
		btnNextFillin.setMnemonic('n');
		dividerPanel.add(btnNextFillin);
		btnNextFillin.addActionListener(controller.new NextFillinActionListener());
		
		JPanel renamePanel = new JPanel();
		GridBagConstraints gbc_renamePanel = new GridBagConstraints();
		gbc_renamePanel.weightx = 1.0;
		gbc_renamePanel.fill = GridBagConstraints.HORIZONTAL;
		gbc_renamePanel.insets = new Insets(0, 0, 5, 0);
		gbc_renamePanel.gridx = 0;
		gbc_renamePanel.gridy = 1;
		fillinPanel.add(renamePanel, gbc_renamePanel);
		renamePanel.setBorder(new EmptyBorder(3, 3, 3, 3));
		renamePanel.setLayout(new BoxLayout(renamePanel, BoxLayout.X_AXIS));
		
		chckbxAssignNewName = new JCheckBox("Assign new fillin code");
		chckbxAssignNewName.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				customFillinCodeField.setEnabled(chckbxAssignNewName.isSelected());
			}
		});
		chckbxAssignNewName.addItemListener(controller.new NewFillinCodeCheckBoxListener());
		renamePanel.add(chckbxAssignNewName);
		
		customFillinCodeField = new JTextField();
		customFillinCodeField.setEnabled(false);
		renamePanel.add(customFillinCodeField);
//		customFillinCodeField.setMaximumSize(new Dimension(Integer.MAX_VALUE, customFillinCodeField.getPreferredSize().height));
		customFillinCodeField.getDocument().addDocumentListener(controller.new NewFillinCodeDocListener());
		
		Component horizontalGlue_5 = Box.createHorizontalGlue();
		renamePanel.add(horizontalGlue_5);
		
		JPanel matchPanel = new JPanel();
		GridBagConstraints gbc_matchPanel = new GridBagConstraints();
		gbc_matchPanel.weightx = 1.0;
		gbc_matchPanel.fill = GridBagConstraints.HORIZONTAL;
		gbc_matchPanel.insets = new Insets(0, 0, 5, 0);
		gbc_matchPanel.gridx = 0;
		gbc_matchPanel.gridy = 2;
		fillinPanel.add(matchPanel, gbc_matchPanel);
		matchPanel.setBorder(new EmptyBorder(3, 3, 3, 3));
		matchPanel.setLayout(new BoxLayout(matchPanel, BoxLayout.X_AXIS));
		
		JLabel lblMatch = new JLabel("Matching fill-in in old clause:");
		matchPanel.add(lblMatch);
		
		Component horizontalStrut = Box.createHorizontalStrut(20);
		matchPanel.add(horizontalStrut);
		
		comboBox = new JComboBox<String>();
		comboBox.setEnabled(false);
		comboBox.addActionListener(controller.new ComboBoxActionListener());
		matchPanel.add(comboBox);
//		comboBox.setMaximumSize(new Dimension(Integer.MAX_VALUE, comboBox.getPreferredSize().height));
		
		Component horizontalStrut_1 = Box.createHorizontalStrut(20);
		matchPanel.add(horizontalStrut_1);
		
		chkbtnNew = new JCheckBox("None (This is a new fill-in)");
		chkbtnNew.setSelected(true);
		chkbtnNew.addItemListener(controller.new NewFillinCheckBoxListener());
		matchPanel.add(chkbtnNew);
		
		Component horizontalGlue_2 = Box.createHorizontalGlue();
		matchPanel.add(horizontalGlue_2);
		
		JPanel propertiesPanel = new JPanel();
		propertiesPanel.setBorder(new TitledBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null), "Fill-In Properties", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		GridBagConstraints gbc_propertiesPanel = new GridBagConstraints();
		gbc_propertiesPanel.weightx = 1.0;
		gbc_propertiesPanel.weighty = 1.0;
		gbc_propertiesPanel.fill = GridBagConstraints.BOTH;
		gbc_propertiesPanel.gridx = 0;
		gbc_propertiesPanel.gridy = 3;
		fillinPanel.add(propertiesPanel, gbc_propertiesPanel);
		propertiesPanel.setLayout(new GridLayout(0, 2, 2, 0));
		
		JPanel newPropertiesTablePanel = new JPanel();
		newPropertiesTablePanel.setBorder(new TitledBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null), "New Fill-In Properties", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		propertiesPanel.add(newPropertiesTablePanel);
		newPropertiesTablePanel.setLayout(new BorderLayout(0, 0));
		
		newPropertiesTable = new JTable();
		newPropertiesTable.setPreferredSize(new Dimension(200, 200));
		newPropertiesTablePanel.add(newPropertiesTable);
		newPropertiesTable.setAutoResizeMode(JTable.AUTO_RESIZE_LAST_COLUMN);
		newPropertiesTable.getTableHeader().setEnabled(false);
		newPropertiesTablePanel.add(newPropertiesTable.getTableHeader(), BorderLayout.NORTH);
		
		JPanel oldPropertiesTablePanel = new JPanel();
		oldPropertiesTablePanel.setBorder(new TitledBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null), "Old (Inherited) Fill-In Properties", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		propertiesPanel.add(oldPropertiesTablePanel);
		oldPropertiesTablePanel.setLayout(new BorderLayout(0, 0));
		
		oldPropertiesTable = new JTable();
		oldPropertiesTable.setPreferredSize(new Dimension(200, 200));
		oldPropertiesTablePanel.add(oldPropertiesTable);
		oldPropertiesTable.setEnabled(false);
		oldPropertiesTable.getTableHeader().setEnabled(false);
		oldPropertiesTablePanel.add(oldPropertiesTable.getTableHeader(), BorderLayout.NORTH);
		
		JMenuBar menuBar = new JMenuBar();
		
		frame.setJMenuBar(menuBar);
		
		JMenu mnFile = new JMenu("File");
		menuBar.add(mnFile);
		
		JMenuItem mntmOpen = new JMenuItem("Open...");
		mntmOpen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(checkForUnsavedChanges()){
					frame.dispose();
					controller.promptAndLoad();
				}
			}
		});
		mntmOpen.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_O, Toolkit.getDefaultToolkit().getMenuShortcutKeyMask()));
		mnFile.add(mntmOpen);
		
		mnFile.addSeparator();
		
		JMenuItem mntmSave = new JMenuItem("Save");
		mntmSave.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, Toolkit.getDefaultToolkit().getMenuShortcutKeyMask()));
		mntmSave.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				controller.saveOverwrite();
			}
		});
		mnFile.add(mntmSave);
		
		JMenuItem mntmSaveAs = new JMenuItem("Save As...");
		mntmSaveAs.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, InputEvent.SHIFT_MASK | Toolkit.getDefaultToolkit().getMenuShortcutKeyMask()));
		mntmSaveAs.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				controller.promptAndSave();
			}
		});
		mnFile.add(mntmSaveAs);
		
		JMenuItem mntmRevert = new JMenuItem("Reload");
		mntmRevert.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_R, Toolkit.getDefaultToolkit().getMenuShortcutKeyMask()));
		mntmRevert.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if(checkForUnsavedChanges())
					controller.refresh();
			}
		});
		mnFile.add(mntmRevert);
		
		mnFile.addSeparator();
		
		JMenuItem mntmQuit = new JMenuItem("Quit");
		mntmQuit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frame.dispatchEvent(new WindowEvent(frame, WindowEvent.WINDOW_CLOSING));
			}
		});
		mntmQuit.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Q, Toolkit.getDefaultToolkit().getMenuShortcutKeyMask()));
		mnFile.add(mntmQuit);
		
		
	}
	
	public JFrame getFrame(){return frame;}
	
	private boolean checkForUnsavedChanges() {
		if(Fillin.unsavedChanges){
			int retval = JOptionPane.showConfirmDialog(frame, "You have unsaved changes. Would you like to save?");
			if(retval == JOptionPane.YES_OPTION){
				controller.saveOverwrite();
				return true;
			} else if (retval == JOptionPane.NO_OPTION){
				return true;
			}else return false;
		} else {
			return true;
		}
	}
	
	public void setHtml(String newHtml, String oldHtml){
		int newHtmlScrollV = newClauseScrollPane.getVerticalScrollBar().getValue();
		int newHtmlScrollH = newClauseScrollPane.getHorizontalScrollBar().getValue();

		if(newHtml != null){
			txtpnnewClauseHtml.setText(newHtml);
		} else {
			txtpnnewClauseHtml.setText(CLAUSE_FAILED_TO_LOAD_MESSAGE);
		}
		
		newClauseScrollPane.getVerticalScrollBar().setValue(newHtmlScrollV);
		newClauseScrollPane.getHorizontalScrollBar().setValue(newHtmlScrollH);
		
		int oldHtmlScrollV = oldClauseScrollPane.getVerticalScrollBar().getValue();
		int oldHtmlScrollH = oldClauseScrollPane.getHorizontalScrollBar().getValue();
		
		if(oldHtml != null){
			txtpnoldClauseHtml.setText(oldHtml);
		} else {
			txtpnoldClauseHtml.setText(CLAUSE_FAILED_TO_LOAD_MESSAGE);
		}

		oldClauseScrollPane.getVerticalScrollBar().setValue(oldHtmlScrollV);
		oldClauseScrollPane.getHorizontalScrollBar().setValue(oldHtmlScrollH);
	}
	
	public void setClause(String clauseName){
		comboBox.setEnabled(false);
		lblDynCurClause.setText(clauseName);
		comboBox.removeAllItems();
		List<Fillin> oldFillins = controller.getOldFillins();
		oldFillins.forEach(f -> {comboBox.addItem(f.getFillinCode());});
	}
	
	public void setClauseButtonsEnabled(boolean previousClause, boolean nextClause){
		btnPreviousClause.setEnabled(previousClause);
		btnNextClause.setEnabled(nextClause);
	}

	public void setFillinButtonsEnabled(boolean previousFillin, boolean nextFillin){
		btnPreviousFillin.setEnabled(previousFillin);
		btnNextFillin.setEnabled(nextFillin);
	}
	
	public void setFillin(Fillin newFillin, Fillin oldFillin){
		refreshSelectedFillinText(newFillin);
		newPropertiesTable.setModel(new PropertiesTableModel(newFillin));
		if(oldFillin == null){
			chkbtnNew.setSelected(true);
			comboBox.setEnabled(false);
			oldPropertiesTable.setEnabled(false);
			oldPropertiesTable.setVisible(false);
		} else {
			chkbtnNew.setSelected(false);
			comboBox.setEnabled(true);
			comboBox.setSelectedItem(oldFillin.getFillinCode());
			oldPropertiesTable.setEnabled(true);
			oldPropertiesTable.setVisible(true);
			oldPropertiesTable.setModel(new PropertiesTableModel(oldFillin));
		}
		String rename = newFillin.getProperty(Fillin.COL_NEW_CUSTOM_FILLIN_CODE);
		if(rename == null || rename.isEmpty()){
			chckbxAssignNewName.setSelected(false);
			customFillinCodeField.setEnabled(false);
			customFillinCodeField.setText("");
		} else {
			chckbxAssignNewName.setSelected(true);
			customFillinCodeField.setEnabled(true);
			customFillinCodeField.setText(rename);
		}
	}
	
	public void refreshSelectedFillinText(Fillin newFillin){
		String dyn = newFillin.getFillinCode();
		String rename = newFillin.getProperty(Fillin.COL_NEW_CUSTOM_FILLIN_CODE);
		if(rename!=null && !rename.isEmpty()) dyn = dyn + " -> " + rename;
		lblDynCurFillin.setText(dyn);
	}
	
	public String getCurrentComboBoxSelection(){
		return comboBox.getSelectedItem().toString();
	}
	
	public boolean isNewFillinCode(){
		return chckbxAssignNewName.isSelected();
	}
	
	public boolean isNewFillin(){
		return chkbtnNew.isSelected();
	}
	
	public String getNewFillinCode(){
		return customFillinCodeField.getText();
	}

	public void resetHtmlScroll() {
		txtpnnewClauseHtml.setCaretPosition(0);
		txtpnoldClauseHtml.setCaretPosition(0);
	}
	
}
