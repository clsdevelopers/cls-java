package ui;

import java.awt.event.AdjustmentEvent;
import java.awt.event.AdjustmentListener;

import javax.swing.JScrollBar;

public class ScrollBarSynchronizer implements AdjustmentListener {
	
	private JScrollBar target;
	
	public ScrollBarSynchronizer(JScrollBar target) {
		this.target = target;
	}

	@Override
	public void adjustmentValueChanged(AdjustmentEvent e) {
		target.setValue(e.getValue());
	}

}
