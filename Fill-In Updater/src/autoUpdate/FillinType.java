package autoUpdate;

public enum FillinType {
	AMOUNT("Money", "$"),
	CHECKBOX("Checkbox", "C"),
	DATE("Date", "D"),
	EMAIL("E-Mail", "E"),
	MEMO("Memo", "M"),
	NUMBER("Number", "N"),
	RADIO("Radio", "R"),
	URL("URL", "U"),
	STRING("Text", "S");
	
	public final String desc, code;
	private FillinType(String desc, String code){
		this.desc = desc;
		this.code = code;
	}
	
	public static FillinType parse(String in){
		in = in.toUpperCase();
		for(FillinType f : values()){
			if(in.equals(f.code)) return f;
		}
		return null;
	}
}
