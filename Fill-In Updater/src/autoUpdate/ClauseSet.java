package autoUpdate;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ClauseSet {
	private List<Fillin> oldFillins, newFillins;
	private String name;
	


	public ClauseSet(String clauseName, List<Fillin> oldFillins, List<Fillin> newFillins){
		this.name = clauseName;
		this.oldFillins = oldFillins;
		this.newFillins = newFillins;
	}

	public ClauseSet(String clause) {
		this(clause, new ArrayList<>(), new ArrayList<>());
	}

	public List<Fillin> getOldFillins() {
		return oldFillins;
	}

	public List<Fillin> getNewFillins() {
		return newFillins;
	}
	
	public void addNewFillin(Fillin f){
		newFillins.add(f);
	}
	
	public void addOldFillin(Fillin f){
		oldFillins.add(f);
	}
	
	public void addFillin(Fillin f){
		if(f.isOld()){
			addOldFillin(f);
		} else {
			addNewFillin(f);
		}
	}

	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	@Override
	public String toString(){
		StringBuilder out = new StringBuilder();
		out.append("\told:\n");
		oldFillins.forEach(f -> {
			out.append("\t\t");
			out.append(f.toLongString());
			out.append("\n");
		});
		
		out.append("\tnew:\n");
		
		newFillins.forEach(f -> {
			out.append("\t\t");
			out.append(f.toLongString());
			out.append("\n");
		});
		return out.toString();
	}

	public void sortFillins() {
		Collections.sort(oldFillins, new util.NaturalOrderComparator<Fillin>());
		Collections.sort(newFillins, new util.NaturalOrderComparator<Fillin>());
	}
}
