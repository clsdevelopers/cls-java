package autoUpdate;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Set;

public class ClauseLibrary {
	private HashMap<String, ClauseSet> lib;
	
	public ClauseLibrary(){
		lib = new HashMap<>();
	}
	
	public void addFillin(Fillin f){
		if(f==null) return;
		String clause = f.getClauseName();
		
		if(!lib.containsKey(clause)){
			lib.put(clause, new ClauseSet(clause));
		}
		
		ClauseSet set = lib.get(clause);
		
		set.addFillin(f);
	}
	
	public ClauseSet getClause(String clauseName){
		return lib.get(clauseName);
	}
	
	public Collection<ClauseSet> getAllClauses(){
		return lib.values();
	}
	
	public ListIterator<String> getClauseNameIterator(){
		Set<String> keySet = lib.keySet();
		LinkedList<String> list = new LinkedList<>(keySet);
		return list.listIterator();
	}
	
	public List<String> getClauseList(){
		return new ArrayList<String>(lib.keySet());
	}
	
	@Override
	public String toString(){
		StringBuilder out = new StringBuilder();
		lib.forEach((String s, ClauseSet clause) -> {
			out.append(s+":\n");
			out.append(clause);
		});
		return out.toString();
	}
	
	public void sortFillins(){
		for(ClauseSet cs : lib.values()){
			cs.sortFillins();
		}
	}

}
