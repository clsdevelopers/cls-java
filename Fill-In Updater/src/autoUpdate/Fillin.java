package autoUpdate;

import java.awt.Point;
import java.util.HashMap;

public class Fillin implements Comparable<Fillin>{
	public static boolean unsavedChanges = false;
	
	private HashMap<String, Property> properties;
	private String clauseName;
	private boolean old;
	
	public Fillin(String clauseName, boolean old){
		properties = new HashMap<>();
		this.clauseName = clauseName;
		this.old = old;
	}
	
	public HashMap<String, Property> getProperties() {
		return properties;
	}
	public String getClauseName() {
		return clauseName;
	}
	public boolean isOld() {
		return old;
	}
	
	public void setProperty(String propName, String value){
		if(getProperty(propName).equals(value)) return;
		properties.get(propName).setValue(value);
		unsavedChanges = true;
	}
	
	public String getProperty(String propName){
		return properties.get(propName).getValue();
	}
	
	public String getFillinCode(){
		if(isOld()){
			return getProperty(COL_OLD_FILLIN_CODE);
		} else {
			return getProperty(COL_NEW_TEMP_FILLIN_CODE);
		}
	}
	
	public String getMatchCode(){
		if(isOld())return null;
		return properties.get(COL_MATCH_CODE).getValue();
	}
	
	public String toLongString(){
		StringBuilder out = new StringBuilder();
		out.append(getFillinCode()).append("\n");
		properties.forEach((key, value) -> {
			out.append("\t\t\t").append(key).append("=").append(value.getValue()).append("; from (").append(value.getCol()).append(",").append(value.getRow()).append(")\n");
		});
		return out.toString();
	}
	
	@Override
	public String toString(){
		return getFillinCode();
	}
	
	public static class Property{
		private String value;
		private Point cellPos;
		
		public Property(String value, Point cellPos){
			this.setValue(value);
			this.cellPos = cellPos;
		}
		
		public Property(String value, int row, int column){
			this(value, new Point(column, row));
		}

		public String getValue() {
			return value;
		}

		public void setValue(String value) {
			this.value = value;
		}

		public Point getCellPos() {
			return cellPos;
		}
		
		public int getRow(){
			return cellPos.y;
		}
		
		public int getCol(){
			return cellPos.x;
		}
		
		@Override
		public String toString(){
			return value;
		}
		
	}





	public static final String COL_CLAUSE_NAME = "CLAUSE NAME";
	public static final String COL_MATCH_CODE = "FILLIN MATCH CODE";
	public static final String COL_FILLIN_TYPE = "FILLIN TYPE";
	public static final String COL_OLD_FILLIN_CODE = "OLD FILLIN CODE";
	
	public static final String COL_NEW_TEMP_FILLIN_CODE = "TEMP NEW FILLIN CODE";
	public static final String COL_NEW_CUSTOM_FILLIN_CODE = "CUSTOM FILLIN CODE";
	public static final String COL_NEW_TEMP_FILLIN_TYPE = "TEMP NEW FILLIN TYPE";
	public static final String COL_NEW_TEMP_FILLIN_SIZE = "TEMP NEW FILLIN SIZE";
	public static final String COL_NEW_FILLIN_MAX_SIZE = "FILLIN MAX SIZE";
	public static final String COL_NEW_GROUP_NUMBER = "FILLIN GROUP NUMBER";
	public static final String COL_NEW_FILLIN_PLACEHOLDER = "FILLIN PLACEHOLDER";
	public static final String COL_NEW_DEFAULT_DATA = "FILLIN DEFAULT DATA";
	public static final String COL_NEW_DISPLAY_ROWS = "FILLIN DISPLAY ROWS";
	public static final String COL_NEW_FILLIN_FOR_TABLE = "FILLIN FOR TABLE";
	public static final String COL_NEW_FILLIN_HEADING = "FILLIN HEADING";
	
	public static final String OLD_PREFIX = "OLD ";

	@Override
	public int compareTo(Fillin other) {
		return getFillinCode().compareTo(other.getFillinCode());
	}
}
