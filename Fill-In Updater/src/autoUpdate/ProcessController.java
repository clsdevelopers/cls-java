package autoUpdate;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.prefs.Preferences;

import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.UIManager;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.text.BadLocationException;

import io.ExcelReader;
import io.ExcelWriter;
import io.HtmlData;
import ui.MainWindow;

public class ProcessController {
	private static final FileNameExtensionFilter XLSX_FILTER = new FileNameExtensionFilter("Excel files", "xlsx");
	private JFileChooser jfc;
	
	private File workingFolder;
	private File loadedFile;
	private MainWindow window;
	
	private ClauseLibrary library;
	private ExcelReader reader;
	private HtmlData htmlData;
	
	private List<String> clauseNameList;
	private String currentClause;
	private int currentClauseIndex = 0;
	private List<Fillin> oldFillins;
	
	private List<Fillin> newFillinList;
	private int currentFillinIndex = 0;
	
	public ProcessController(){
		jfc = new JFileChooser();
		jfc.setFileFilter(XLSX_FILTER);
		window = new MainWindow(this);
	}
	
	public ProcessController(MainWindow window){
		this.window = window;

	}
	
	public File getWorkingFolder(){
		return workingFolder;
	}
	
	public boolean promptForFile(){
		File startingFolder = null;
		String startingPath = Preferences.userNodeForPackage(this.getClass()).get("lastFolder", "");
		if(!startingPath.equals("")){
			startingFolder = new File(startingPath);
		}
		jfc.setCurrentDirectory(startingFolder);
		int retval = jfc.showOpenDialog(window.getFrame());
		if(retval == JFileChooser.APPROVE_OPTION){
			setLoadedFile(jfc.getSelectedFile());
			Preferences.userNodeForPackage(this.getClass()).put("lastFolder", jfc.getCurrentDirectory().getAbsolutePath());
		} else {
			return false;
		}
		return true;
	}
	
	public void promptAndLoad(){
		if(promptForFile()){
			load();
		}
	}
	
	public void refresh(){
		int clause = currentClauseIndex;
		int fillin = currentFillinIndex;
		load();
		setClauseByIndex(clause);
		setFillinByIndex(fillin);
		Fillin.unsavedChanges = false;
	}
	
	public void setLoadedFile(File newLoadedFile){
		this.loadedFile = newLoadedFile;
		this.workingFolder = newLoadedFile.getParentFile();
	}
	
	public void load(){
		reader = new ExcelReader(loadedFile);
		try {
			library = reader.load();
			clauseNameList = library.getClauseList();
			htmlData = new HtmlData();
			htmlData.loadAllClauses(library, workingFolder);
			setClauseByIndex(0);
		} catch (IOException e) {
			util.Popup.popErrorWindow(writer ->{
				writer.println("An Exception occured while loading the file:");
				e.printStackTrace(writer);
			});
		}
		Fillin.unsavedChanges = false;
		window.pop();
	}
	
	public void setClauseByIndex(int newIndex){
		currentClauseIndex = newIndex;
		currentClause = clauseNameList.get(currentClauseIndex);
		oldFillins = library.getClause(currentClause).getOldFillins();
		newFillinList = library.getClause(currentClause).getNewFillins();
		refreshClauseButtons();
		window.setClause(currentClause);
		setFillinByIndex(0);
		window.resetHtmlScroll();
	}
	
	private void loadHtml(){
		String oldHtml = htmlData.getOldFormattedHtml(currentClause, getCurrentFillin().getMatchCode(), oldFillins, getMatchedOldFillins());
		String newHtml = htmlData.getNewFormattedHtml(currentClause, getCurrentFillin().getFillinCode(), newFillinList);
		window.setHtml(newHtml, oldHtml);
	}
	
	private List<String> getMatchedOldFillins(){
		LinkedList<String> out = new LinkedList<String>();
		for(Fillin cur : newFillinList){
			String matchCode = cur.getMatchCode();
			if(matchCode != null && !matchCode.isEmpty()){
				out.add(matchCode);
			}
		}
		return out;
	}
	
	public void setFillinByIndex(int newIndex){
		currentFillinIndex = newIndex;
		Fillin newFillin = getCurrentFillin();
		Fillin oldFillin = getOldFillinWithName(newFillin.getMatchCode());
		window.setFillin(newFillin, oldFillin);
		refreshFillinButtons();
		loadHtml();
	}
	
	private void refreshClauseButtons(){
		window.setClauseButtonsEnabled(currentClauseIndex > 0, currentClauseIndex < (clauseNameList.size() - 1));
	}
	
	private void refreshFillinButtons(){
		window.setFillinButtonsEnabled(currentFillinIndex > 0, currentFillinIndex < (newFillinList.size() - 1));
	}

	public List<Fillin> getOldFillins() {
		return oldFillins;
	}
	
	public void setOldFillin(String fillinName){
		Fillin newFillin = getCurrentFillin();
		newFillin.setProperty(Fillin.COL_MATCH_CODE, fillinName == null ? "" : fillinName);
		Fillin oldFillin = getOldFillinWithName(fillinName);
		window.setFillin(newFillin, oldFillin);
	}
	
	private Fillin getOldFillinWithName(String name){
		if(name == null || name.equals("")) return null;
		for(Fillin f : oldFillins){
			if(f.getFillinCode().equals(name)) return f;
		}
		return null;
	}
	
	public void saveOverwrite(){
		saveAs(loadedFile);
	}
	
	public void saveAs(File out){
		ExcelWriter.copyFillinProperties(library);
		try {
			ExcelWriter.write(out, loadedFile, library);
			htmlData.saveAll(out.getParentFile(), library);
			setLoadedFile(out);
			refresh();
		} catch (Exception e) {
			util.Popup.popErrorWindow(writer -> {
				writer.println("An exception occured while saving:");
				e.printStackTrace(writer);
			});
		}
	}
	
	public void promptAndSave(){
		int retval = jfc.showSaveDialog(null);
		if(retval == JFileChooser.APPROVE_OPTION){
			saveAs(jfc.getSelectedFile());
		}
	}
	
	private Fillin getCurrentFillin() {
		return newFillinList.get(currentFillinIndex);
	}

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
					ProcessController p = new ProcessController();
					if(args.length == 1){
						p.setLoadedFile(new File(args[0]));
						p.load();
					}else{
						p.promptAndLoad();
					}
				} catch (Exception e) {
					util.Popup.popErrorWindow(w->{
						w.println("An Exception occured:");
						e.printStackTrace(w);
					});
				}
			}
		});
	}

	public class NextClauseActionListener implements ActionListener{
		@Override
		public void actionPerformed(ActionEvent e) {
			setClauseByIndex(currentClauseIndex + 1);
		}
	}
	
	public class PreviousClauseActionListener implements ActionListener{
		@Override
		public void actionPerformed(ActionEvent e) {
			setClauseByIndex(currentClauseIndex - 1);
		}
	}
	
	public class NextFillinActionListener implements ActionListener{
		@Override
		public void actionPerformed(ActionEvent e) {
			setFillinByIndex(currentFillinIndex + 1);
		}
	}
	
	public class PreviousFillinActionListener implements ActionListener{
		@Override
		public void actionPerformed(ActionEvent e) {
			setFillinByIndex(currentFillinIndex - 1);
		}
	}
	
	public class ComboBoxActionListener implements ActionListener{
		@Override
		public void actionPerformed(ActionEvent e) {
			@SuppressWarnings("unchecked")
			JComboBox<String> source = (JComboBox<String>) e.getSource();
			if(source.isEnabled()){
				setOldFillin(source.getSelectedItem().toString());
				loadHtml();
			}
		}
	}
	
	public class NewFillinCheckBoxListener implements ItemListener{
		@Override
		public void itemStateChanged(ItemEvent e) {
			JCheckBox source = (JCheckBox) e.getSource();
			if(!source.isSelected()){
				String matchCode = getCurrentFillin().getMatchCode();
				if(matchCode == null || matchCode.isEmpty()){
					setOldFillin(window.getCurrentComboBoxSelection());
				}else{
					setOldFillin(matchCode);
				}
			} else {
				setOldFillin(null);
			}
			loadHtml();
		}
	}
	
	public class NewFillinCodeCheckBoxListener implements ItemListener{
		@Override
		public void itemStateChanged(ItemEvent e) {
			if(e.getStateChange() == ItemEvent.DESELECTED){
				getCurrentFillin().setProperty(Fillin.COL_NEW_CUSTOM_FILLIN_CODE, "");
			} else {
				getCurrentFillin().setProperty(Fillin.COL_NEW_CUSTOM_FILLIN_CODE, window.getNewFillinCode());
			}
			loadHtml();
			window.refreshSelectedFillinText(getCurrentFillin());
		}
	}
	
	public class HttpNewClauseSelectionListiner implements HyperlinkListener{
		@Override
		public void hyperlinkUpdate(HyperlinkEvent e) {
			if(e.getEventType() == HyperlinkEvent.EventType.ACTIVATED){
				setFillinByIndex(Integer.parseInt(e.getDescription()));
			}
		}
	}
	
	public class HttpOldClauseSelectionListiner implements HyperlinkListener{
		@Override
		public void hyperlinkUpdate(HyperlinkEvent e) {
			if(e.getEventType() == HyperlinkEvent.EventType.ACTIVATED){
				setOldFillin(e.getDescription());
			}
		}
	}
	
	public class NewFillinCodeDocListener implements DocumentListener{
		@Override
		public void changedUpdate(DocumentEvent e) {}
		
		@Override
		public void insertUpdate(DocumentEvent e) {
			try {
				getCurrentFillin().setProperty(Fillin.COL_NEW_CUSTOM_FILLIN_CODE, e.getDocument().getText(0, e.getDocument().getLength()));
				loadHtml();
				window.refreshSelectedFillinText(getCurrentFillin());
			} catch (BadLocationException e1) {
				e1.printStackTrace();
			}
			
		}

		@Override
		public void removeUpdate(DocumentEvent e) {
			try {
				getCurrentFillin().setProperty(Fillin.COL_NEW_CUSTOM_FILLIN_CODE, e.getDocument().getText(0, e.getDocument().getLength()));
				loadHtml();
				window.refreshSelectedFillinText(getCurrentFillin());
			} catch (BadLocationException e1) {
				e1.printStackTrace();
			}
			
		}
		
	}
}
